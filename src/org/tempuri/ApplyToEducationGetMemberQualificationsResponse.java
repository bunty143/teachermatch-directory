package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplyToEducation_GetMemberQualificationsResult" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;any/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "applyToEducationGetMemberQualificationsResult" })
@XmlRootElement(name = "ApplyToEducation_GetMemberQualificationsResponse")
public class ApplyToEducationGetMemberQualificationsResponse {

	@XmlElementRef(name = "ApplyToEducation_GetMemberQualificationsResult", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<ApplyToEducationGetMemberQualificationsResponse.ApplyToEducationGetMemberQualificationsResult> applyToEducationGetMemberQualificationsResult;

	/**
	 * Gets the value of the applyToEducationGetMemberQualificationsResult
	 * property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}
	 *         {@link ApplyToEducationGetMemberQualificationsResponse.ApplyToEducationGetMemberQualificationsResult }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<ApplyToEducationGetMemberQualificationsResponse.ApplyToEducationGetMemberQualificationsResult> getApplyToEducationGetMemberQualificationsResult() {
		return applyToEducationGetMemberQualificationsResult;
	}

	/**
	 * Sets the value of the applyToEducationGetMemberQualificationsResult
	 * property.
	 * 
	 * @param value
	 *            allowed object is {@link JAXBElement }{@code <}
	 *            {@link ApplyToEducationGetMemberQualificationsResponse.ApplyToEducationGetMemberQualificationsResult }
	 *            {@code >}
	 * 
	 */
	public void setApplyToEducationGetMemberQualificationsResult(
			JAXBElement<ApplyToEducationGetMemberQualificationsResponse.ApplyToEducationGetMemberQualificationsResult> value) {
		this.applyToEducationGetMemberQualificationsResult = ((JAXBElement<ApplyToEducationGetMemberQualificationsResponse.ApplyToEducationGetMemberQualificationsResult>) value);
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;any/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "any" })
	public static class ApplyToEducationGetMemberQualificationsResult {

		@XmlAnyElement(lax = true)
		protected Object any;

		/**
		 * Gets the value of the any property.
		 * 
		 * @return possible object is {@link Object }
		 * 
		 */
		public Object getAny() {
			return any;
		}

		/**
		 * Sets the value of the any property.
		 * 
		 * @param value
		 *            allowed object is {@link Object }
		 * 
		 */
		public void setAny(Object value) {
			this.any = value;
		}

	}

}
