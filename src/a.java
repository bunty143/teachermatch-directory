import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
 
public class a {
	public String updateRecords(Date date,Integer districtId,Integer center,Integer noOf,Integer timeZoneId,Integer userId){
		
		NavigableMap<Integer, String> timeStamp = new TreeMap<Integer, String>();	
		Map<String, String> tableContent = new LinkedHashMap<String, String>();		
		timeStamp.put(1,"8:00 AM");
		timeStamp.put(2,"8:30 AM");
		timeStamp.put(3,"9:00 AM");
		timeStamp.put(4,"9:30 AM");
		timeStamp.put(5,"10:00 AM");
		timeStamp.put(6,"10:30 AM");
		timeStamp.put(7,"11:00 AM");
		timeStamp.put(8,"11:30 AM");
		timeStamp.put(9,"12:00 PM");
		timeStamp.put(10,"12:30 PM");
		timeStamp.put(11,"1:00 PM");
		timeStamp.put(12,"1:30 PM");
		timeStamp.put(13,"2:00 PM");
		timeStamp.put(14,"2:30 PM");
		timeStamp.put(15,"3:00 PM");
		timeStamp.put(16,"3:30 PM");
		timeStamp.put(17,"4:00 PM");
		timeStamp.put(18,"4:30 PM");
		timeStamp.put(19,"5:00 PM");
		
		Calendar examCalendar = Calendar.getInstance();
		examCalendar.setTime(date);
		String dateFor="";
		String valStr="INSERT INTO `centerschedulemaster` ( `districtId`, `centerId`, `csTime`, `csDate`, `timeZoneId`, `availability`, `noOfApplicantsScheduled`, `reservedForJobCategory`, `scheduleCreatedBy`, `timeOrder`, `scheduleCreatedDateTime`) VALUES ";
		int y=0;
		for(int i=0;i<noOf;i++){
			if(y==5 || y==6){
				if(y==6){
					y=0;
				}else{
					y++;
				}
			}else{
				int examDte=0;
				int examMonth=0;
				if(i==0){
					 examDte = examCalendar.get(Calendar.DATE);
					 examMonth = examCalendar.get(Calendar.MONTH)+1;
				}else{
					examDte = examCalendar.get(Calendar.DATE)+i;
					examMonth = examCalendar.get(Calendar.MONTH)+1;	
				}
				int examYear = examCalendar.get(Calendar.YEAR);
				dateFor=examYear+"-"+examMonth+"-"+examDte;
				for( Map.Entry<Integer, String> time : timeStamp.entrySet() )
				{
					valStr+="("+districtId+","+center+",'"+time.getValue()+"', '"+dateFor+"',"+timeZoneId+", NULL, 0, NULL,"+userId+","+time.getKey()+",'2015-03-01 18:41:29'),";
	
				}
				y++;
			}
			
		}
		return valStr;
	}
	public static void main(String[] args) throws IOException {
		
		Set<Integer> teacherIdsSet=new TreeSet<Integer>();
		teacherIdsSet.add(222);
		
		
		System.out.println(teacherIdsSet.contains(2));
		System.out.println(teacherIdsSet.contains(222));
		System.out.println(teacherIdsSet.contains(22));
		
		
		
		Date date=new Date();
	    Integer districtId=4218990;
	    Integer center=1;
	    Integer noOf=25;
	    Integer timeZoneId=1;
	    Integer userId=1;
		a a=new a();
		System.out.println(a.updateRecords(date,districtId,center,noOf,timeZoneId,userId));	
	
	/*String returnTime="8:30 AM";
	SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
	SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
   
	Date date=new Date();
		try {
			date = parseFormat.parse(returnTime);
			
			Calendar examCalendar = Calendar.getInstance();
			examCalendar.setTime(date);
			int examDte = examCalendar.get(Calendar.MINUTE)+120;
			System.out.println("examDte::::"+examDte);
			returnTime= displayFormat.format(date);
			System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
   */
   
	}
	
}