package tm.mq.event;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.star.uno.Exception;

import tm.bean.TeacherDetail;
import tm.bean.mq.MQEvent;
import tm.bean.mq.MQEventHistory;
import tm.bean.user.UserMaster;
import tm.dao.TeacherDetailDAO;
import tm.dao.mq.MQEventDAO;
import tm.dao.mq.MQEventHistoryDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.IPAddressUtility;
import tm.utility.TMCommonUtil;

public class DisconnectHandler implements MessageListener{

	@Autowired
	private UserMasterDAO usermasterDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private MQEventHistoryDAO mqEventHistoryDAO;
	
	@Autowired
	private MQEventDAO mqEventDAO;
	
	@Override
	public void onMessage(Message message) {/*
		
		System.out.println(" disconnect onMessage cal::::::::::::::::::");
		MapMessage map = (MapMessage) message;
		WebContext context;
		context = WebContextFactory.get();
		try {
			SessionFactory sessionFactory=mqEventDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			Transaction txOpen =statelesSsession.beginTransaction();
			
			String tmTransactionId = map.getString("TMTransactionID");
			Integer mqEventHistoryID = map.getInt("MQEventHistoryID");
			Integer mqEventID = map.getInt("MQEventID");
			Integer userId=map.getInt("userId");
			String talentId = map.getString("teacherId");
			System.out.println(" talentId ::: "+talentId);
			//String action = map.getString("Action");
			String ksnId = map.getString("KSNID");
			System.out.println(" ksnId ::::: "+ksnId);
			String branchCode = map.getString("branchCode");
			List<Integer> teachers = new ArrayList<Integer>();
			TeacherDetail teacherDetail = null;
			UserMaster userMaster=usermasterDAO.findById(userId,false,false);
			if(talentId!=null && talentId.length()>0){
				
				try {
						teacherDetail = teacherDetailDAO.findById(Integer.parseInt(talentId), false, false);
				} catch (NumberFormatException e1) {
					e1.printStackTrace();
				}
				
				if(teacherDetail!=null)
					try {
							//String retrunValue = TMCommonUtil.disconnectTalent(teacherDetail,tmTransactionId,ksnId,branchCode,userMaster,mqEventID,mqEventHistoryID,mqEventHistoryDAO);
							System.out.println("Response get on Link To KSN Talent  " + retrunValue);
							
							InputSource inputSource = new InputSource();
							inputSource.setCharacterStream(new StringReader(retrunValue));
							
							Document doc = null;
							DocumentBuilder docBuilder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
								
							doc = docBuilder.newDocument();
							doc = docBuilder.parse(inputSource);
							NodeList disconnectDetailsTM = doc.getElementsByTagName("TeacherMatch");
							if(disconnectDetailsTM.getLength()>0){
								Element element = (Element) disconnectDetailsTM.item(0);
							    String status = element.getElementsByTagName("Status").item(0).getTextContent();
							    String messageResponse = element.getElementsByTagName("Message").item(0).getTextContent();
							    System.out.println("Status: "+status);
							    System.out.println("messageResponse: "+messageResponse);
							    
							    //if(status.equalsIgnoreCase("Success")){
							    	MQEvent mqEvent =	mqEventDAO.findById(mqEventID, false, false);
							    	if(status.equalsIgnoreCase("success"))
							    		mqEvent.setStatus("C");
							    	else
							    		mqEvent.setStatus("F");
							        mqEvent.setAckStatus(status);
							        mqEvent.setMsgStatus(messageResponse);
							        statelesSsession.update(mqEvent);
							        
							        MQEventHistory mqHistoryEvent =	mqEventHistoryDAO.findById(mqEventHistoryID, false, false);
							        mqHistoryEvent.setMqEvent(mqEvent);
							        statelesSsession.update(mqHistoryEvent);
							   // }
						}
						txOpen.commit();
						statelesSsession.close();
						
					} catch (ParserConfigurationException e) {
						e.printStackTrace();
					} catch (SAXException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}	
		} catch (JMSException e) {
			e.printStackTrace();
		}
	*/}

}
