package tm.mq.event;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.MapMessage;
import javax.jms.Message;

import javax.jms.MessageListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.datacontract.schemas._2004._07.KSN_Entities_Response_KSNData.InviteTalentToWorkflowResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.jersey.api.core.InjectParam;

import tm.bean.JobForTeacher;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.StatusNodeColorHistory;
import tm.bean.master.SecondaryStatus;
import tm.bean.mq.MQEvent;
import tm.bean.mq.MQEventHistory;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.hqbranchesmaster.StatusNodeColorHistoryDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.mq.MQEventDAO;
import tm.dao.mq.MQEventHistoryDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.CommonService;
import tm.services.mq.MQService;
import tm.services.report.CandidateGridService;
import tm.utility.TMCommonUtil;

public class ERegistrationHandler  implements MessageListener {

	
	@Autowired
	private UserMasterDAO usermasterDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private MQEventHistoryDAO mqEventHistoryDAO;
	
	@Autowired
	private MQEventDAO mqEventDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private MQService mqService;
	
	@Autowired
	private StatusNodeColorHistoryDAO statusNodeColorHistoryDAO;
	
	@Autowired
	private CandidateGridService candidateGridService;
	
	/*@InjectParam
	private static MQService mqService;*/
	
	
	
	
	@Override
	public void onMessage(Message message) {
		
		System.out.println("--------ERegistrationHandler-----------");
		MapMessage map = (MapMessage) message;
		StatusNodeColorHistory stausNodeColorHistory =null;
		MQEvent mqEvent = null;
		Integer mqEventHistoryID = 0;
		Integer mqEventID = 0;
		InviteTalentToWorkflowResponse response = null;
		try {
			Long jobForTeacherId = map.getLong("jobForTeacherId");
			String tmTransactionId = map.getString("TMTransactionID");
			mqEventHistoryID = map.getInt("MQEventHistoryID");
			mqEventID = map.getInt("MQEventID");
			Integer userId=map.getInt("userId");
			Integer secondaryStatusId = map.getInt("secondaryStatusId");
			String workFlowID = map.getString("WorkFlowID");
			System.out.println("WorkFlowID ------ "+workFlowID );
			Integer statusNodeColorHistoryId = map.getInt("statusNodeColorHistoryId");
			
			JobForTeacher jobForTeacher=jobForTeacherDAO.findById(jobForTeacherId, false, false);
			SecondaryStatus secondaryStatus=secondaryStatusDAO.findById(secondaryStatusId, false, false);
			UserMaster userMaster=usermasterDAO.findById(userId,false,false);
			System.out.println("jobForTeacherId:::: "+jobForTeacher);
			stausNodeColorHistory = statusNodeColorHistoryDAO.findById(statusNodeColorHistoryId,false,false);
			
			response = TMCommonUtil.updateERegistration(jobForTeacher,jobForTeacherDAO , tmTransactionId , mqEventHistoryID ,  mqEventHistoryDAO , workFlowID );
			
			/*StatusNodeColorHistory statusNodeColorHistory = null;
			 Map<String,MQEvent> mapNodeStatus= new HashMap<String, MQEvent>();
			 List<MQEvent> mqEventsList=new ArrayList<MQEvent>();*/
			 
			System.out.println("Response get on ERegistrationHandler  " + response.getResponseBase().getResponseStatus());
			
			if (response != null && response.getResponseBase() != null) {
			    String status = response.getResponseBase().getResponseStatus().toString();
			    mqEvent =	mqEventDAO.findById(mqEventID, false, false);
			    ApplicationContext context0 =  WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
			    if(status.contains("Success")){
			    	mqEvent.setStatus("R");
			        mqEvent.setAckStatus(status);
			        mqEvent.setMsgStatus(status);
			        mqEvent.setTmInitiate(true);
			        mqEvent.setWorkFlowStatusId(null);
			        mqEventDAO.makePersistent(mqEvent);
			        
			        MQEventHistory mqHistoryEvent =	mqEventHistoryDAO.findById(mqEventHistoryID, false, false);
			        mqHistoryEvent.setMqEvent(mqEvent);
			        mqEventHistoryDAO.makePersistent(mqHistoryEvent);
			    }else{
			    	  mqEvent.setStatus("R");
				      mqEvent.setAckStatus(status);
				      mqEvent.setMsgStatus(status);
				      //mqEvent.setTmInitiate(true);
				      mqEvent.setWorkFlowStatusId(null);
				      mqEventDAO.makePersistent(mqEvent);
				      
				      MQEventHistory mqHistoryEvent =	mqEventHistoryDAO.findById(mqEventHistoryID, false, false);
				      mqHistoryEvent.setMqEvent(mqEvent);
				      mqEventHistoryDAO.makePersistent(mqHistoryEvent);
				      
				      org.datacontract.schemas._2004._07.KSN_Entities_Data.Message[] responseMes = response.getResponseBase().getResponseMessages();
					  for(org.datacontract.schemas._2004._07.KSN_Entities_Data.Message mes : responseMes){
			        		if(mes != null)
			        		mqService.statusUpdate(jobForTeacher,null,mqEvent.getSecondaryStatus(),userMaster,context0,mes.getMessageDescription(),false);
			          }
			      
			    }
			    
			    /**
			        * add to track node color history
			        */
				try {
					if(stausNodeColorHistory!=null){
						   String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
						   System.out.println(" newColor :: "+newColor);
						   stausNodeColorHistory.setCurrentColor(newColor);
						   stausNodeColorHistory.setUpdateDateTime(new Date());
						   statusNodeColorHistoryDAO.updatePersistent(stausNodeColorHistory);
					   }
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				/**
				 * end
				 */
			}
		
		} catch (Exception e) {
			e.printStackTrace();
				
			mqEvent =	mqEventDAO.findById(mqEventID, false, false);
			mqEvent.setAckStatus(null);
	        mqEvent.setMsgStatus(null);
	        mqEvent.setStatus("R");
	        mqEventDAO.makePersistent(mqEvent);
	        
	        MQEventHistory mqHistoryEvent =	mqEventHistoryDAO.findById(mqEventHistoryID, false, false);
	        mqHistoryEvent.setMqEvent(mqEvent);
	        mqEventHistoryDAO.makePersistent(mqHistoryEvent);
	        
			try {
				if(stausNodeColorHistory!=null){
					   String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
					   System.out.println(" newColor :: "+newColor);
					   stausNodeColorHistory.setCurrentColor(newColor);
					   stausNodeColorHistory.setUpdateDateTime(new Date());
					   statusNodeColorHistoryDAO.updatePersistent(stausNodeColorHistory);
				   }
			} catch (Exception exx) {
				exx.printStackTrace();
			}
		}    

		
		
		
	}
	
}
