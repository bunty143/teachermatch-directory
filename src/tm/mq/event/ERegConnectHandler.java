package tm.mq.event;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.springframework.beans.factory.annotation.Autowired;
import tm.bean.TeacherDetail;
import tm.dao.TeacherDetailDAO;
import tm.dao.mq.MQEventDAO;
import tm.dao.mq.MQEventHistoryDAO;
import tm.utility.TMCommonUtil;

public class ERegConnectHandler implements MessageListener{

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private MQEventHistoryDAO mqEventHistoryDAO;
	
	@Autowired
	private MQEventDAO mqEventDAO;
	
	@Override
	@SuppressWarnings("unused")
	public void onMessage(Message message) {
		
		System.out.println(" ERegConnectHandler cal:::::::::::");
		MapMessage map = (MapMessage) message;
		WebContext context = WebContextFactory.get();
		try {
			Integer mqEventHistoryID = map.getInt("MQEventHistoryID");
			Integer mqEventID = map.getInt("MQEventID");
			String talentId = map.getString("teacherId");
			String ksnId = map.getString("KSNID");
			TeacherDetail teacherDetail = null;
			System.out.println(" mqEventHistoryID :: "+mqEventHistoryID);
			System.out.println(" mqEventID :: "+mqEventID);
			System.out.println(" talentId :: "+talentId);
			System.out.println(" ksnId :: "+ksnId);
			if(talentId!=null && talentId.length()>0){
				try {
						teacherDetail = teacherDetailDAO.findById(Integer.parseInt(talentId), false, false);
				} catch (NumberFormatException e1) {
					e1.printStackTrace();
				}
			}	
			try {
				//TMCommonUtil.singleSignOn(teacherDetail, mqEventID, mqEventHistoryID,mqEventDAO,mqEventHistoryDAO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}

