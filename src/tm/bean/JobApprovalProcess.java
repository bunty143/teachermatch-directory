package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
@Entity
@Table(name="approvalprocess")
public class JobApprovalProcess implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5020201082745932617L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer jobApprovalProcessId;
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtId;
	
	private String jobApprovalProcess;
	private String jobApprovalGroups;
	private Integer approvalRequired;
	private Integer status;
	private Date createdDateTime;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBy;
	
	public Integer getJobApprovalProcessId() {
		return jobApprovalProcessId;
	}
	public void setJobApprovalProcessId(Integer jobApprovalProcessId) {
		this.jobApprovalProcessId = jobApprovalProcessId;
	}
	public String getJobApprovalProcess() {
		return jobApprovalProcess;
	}
	public void setJobApprovalProcess(String jobApprovalProcess) {
		this.jobApprovalProcess = jobApprovalProcess;
	}
	public DistrictMaster getDistrictId() {
		return districtId;
	}
	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}
	public String getJobApprovalGroups() {
		return jobApprovalGroups;
	}
	public void setJobApprovalGroups(String jobApprovalGroups) {
		this.jobApprovalGroups = jobApprovalGroups;
	}	
	public Integer getApprovalRequired() {
		return approvalRequired;
	}
	public void setApprovalRequired(Integer approvalRequired) {
		this.approvalRequired = approvalRequired;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public UserMaster getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(UserMaster createdBy) {
		this.createdBy = createdBy;
	}
	
}
