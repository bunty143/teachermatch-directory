package tm.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.SchoolMaster;

@Entity
@Table(name="strengthopportunitysummary")
public class StrengthOpportunitySummary implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6212926207012307485L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer strengthOpportunityId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	private String competency;
	private String strength;
	private String growthOpportunity;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	private String recommendSchoolReason;
	private String questionForInterview;
	private Date createdDateTime;
	
	public Integer getStrengthOpportunityId() {
		return strengthOpportunityId;
	}
	public void setStrengthOpportunityId(Integer strengthOpportunityId) {
		this.strengthOpportunityId = strengthOpportunityId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public String getCompetency() {
		return competency;
	}
	public void setCompetency(String competency) {
		this.competency = competency;
	}
	public String getStrength() {
		return strength;
	}
	public void setStrength(String strength) {
		this.strength = strength;
	}
	public String getGrowthOpportunity() {
		return growthOpportunity;
	}
	public void setGrowthOpportunity(String growthOpportunity) {
		this.growthOpportunity = growthOpportunity;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public String getRecommendSchoolReason() {
		return recommendSchoolReason;
	}
	public void setRecommendSchoolReason(String recommendSchoolReason) {
		this.recommendSchoolReason = recommendSchoolReason;
	}
	public String getQuestionForInterview() {
		return questionForInterview;
	}
	public void setQuestionForInterview(String questionForInterview) {
		this.questionForInterview = questionForInterview;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
}
