package tm.bean.hrmspostionstext;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="hrmspositionstextdetail")
public class HrmsPositionsTextDetail 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int hrmsPositionsTextDetailId;
	private String fileName;
	private int recordReceived;
	private int recordInserted;
	private int recordUpdated;
	private int exceptionCout;
	private String status;
	private Date createdDateTime;
	private Date updatedDateTime;
	private String folderPath;
	private boolean uploaded;
	private Date uploadedDateTime;
	private Date fileCreatedDateTime;
	private Date fileModifiedDateTime;
	private long fileSize;
	private int lineNumber;
	private String message;
	
	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	public int getHrmsPositionsTextDetailId() {
		return hrmsPositionsTextDetailId;
	}
	public void setHrmsPositionsTextDetailId(int hrmsPositionsTextDetailId) {
		this.hrmsPositionsTextDetailId = hrmsPositionsTextDetailId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getRecordReceived() {
		return recordReceived;
	}
	public void setRecordReceived(int recordReceived) {
		this.recordReceived = recordReceived;
	}
	
	public int getRecordInserted() {
		return recordInserted;
	}
	public void setRecordInserted(int recordInserted) {
		this.recordInserted = recordInserted;
	}
	public int getRecordUpdated() {
		return recordUpdated;
	}
	public void setRecordUpdated(int recordUpdated) {
		this.recordUpdated = recordUpdated;
	}
	public int getExceptionCout() {
		return exceptionCout;
	}
	public void setExceptionCout(int exceptionCout) {
		this.exceptionCout = exceptionCout;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	public String getFolderPath() {
		return folderPath;
	}
	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}
	public boolean isUploaded() {
		return uploaded;
	}
	public void setUploaded(boolean uploaded) {
		this.uploaded = uploaded;
	}
	public Date getUploadedDateTime() {
		return uploadedDateTime;
	}
	public void setUploadedDateTime(Date uploadedDateTime) {
		this.uploadedDateTime = uploadedDateTime;
	}
	public Date getFileCreatedDateTime() {
		return fileCreatedDateTime;
	}
	public void setFileCreatedDateTime(Date fileCreatedDateTime) {
		this.fileCreatedDateTime = fileCreatedDateTime;
	}
	public Date getFileModifiedDateTime() {
		return fileModifiedDateTime;
	}
	public void setFileModifiedDateTime(Date fileModifiedDateTime) {
		this.fileModifiedDateTime = fileModifiedDateTime;
	}
	public int getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	

}
