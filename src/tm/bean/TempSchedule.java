package tm.bean;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TempSchedule")
public class TempSchedule implements Serializable{

private static final long serialVersionUID = -1114622195166303256L;
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
private int scheduleId;
public int getScheduleId() {
	return scheduleId;
}
public String getStarttimezone() {
	return starttimezone;
}
public void setStarttimezone(String starttimezone) {
	this.starttimezone = starttimezone;
}
public String getEndtimezone() {
	return endtimezone;
}
public void setEndtimezone(String endtimezone) {
	this.endtimezone = endtimezone;
}
public void setScheduleId(int scheduleId) {
	this.scheduleId = scheduleId;
}
private String date;
private String startTime,endTime,starttimezone,endtimezone;
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}
public String getStartTime() {
	return startTime;
}
public void setStartTime(String startTime) {
	this.startTime = startTime;
}
public String getEndTime() {
	return endTime;
}
public void setEndTime(String endTime) {
	this.endTime = endTime;
}
}
