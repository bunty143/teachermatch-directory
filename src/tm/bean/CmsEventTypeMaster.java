package tm.bean;

import java.util.Date;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name="cmseventtypemaster")
public class CmsEventTypeMaster implements Serializable{

private static final long serialVersionUID = -1878527095767514532L;

@Id
@GeneratedValue(strategy=GenerationType.AUTO)
private Integer eventTypeId;
private String eventTypeName;
private String status;
@ManyToOne
@JoinColumn(name="createdBy",referencedColumnName="userId")
private UserMaster createdBY;

private Date createdDateTime;
public Integer getEventTypeId() {
	return eventTypeId;
}

public UserMaster getCreatedBY() {
	return createdBY;
}

public void setCreatedBY(UserMaster createdBY) {
	this.createdBY = createdBY;
}

public static long getSerialversionuid() {
	return serialVersionUID;
}
public void setEventTypeId(Integer eventTypeId) {
	this.eventTypeId = eventTypeId;
}
public String getEventTypeName() {
	return eventTypeName;
}
public void setEventTypeName(String eventTypeName) {
	this.eventTypeName = eventTypeName;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}

public Date getCreatedDateTime() {
	return createdDateTime;
}
public void setCreatedDateTime(Date createdDateTime) {
	this.createdDateTime = createdDateTime;
}

@Override	
public boolean equals(Object object){
    if (object == null) return false;
    if (object == this) return true;
    if (!(object instanceof CmsEventTypeMaster))return false;
    CmsEventTypeMaster cmsEventTypeMaster = (CmsEventTypeMaster)object;
  
    if(this.eventTypeId.equals(cmsEventTypeMaster.getEventTypeId()))
    {
    	return true;
    }
    else
    {
    	return false;
    }
}

@Override	
public int hashCode() 
{	
	return new Integer(""+eventTypeId);
}




}
