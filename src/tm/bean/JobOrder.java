package tm.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4QuestionSets;
import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.master.TimeZoneMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="joborder")
public class JobOrder implements Serializable
{
	
	/**
	 *  Fire trigger on this table for Jeffco_804800(trigger Name: UPDATE_JobStart_JobEnd_DATE)
	 */
	private static final long serialVersionUID = 518769106411290998L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer jobId;  

	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;

	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	
	@ManyToOne
	@JoinColumn(name="subjectId",referencedColumnName="subjectId")
	private SubjectMaster subjectMaster;
	
	@ManyToOne
	@JoinColumn(name="geoZoneId",referencedColumnName="geoZoneId")
	private GeoZoneMaster geoZoneMaster;
	
	private Boolean offerVirtualVideoInterview;
	
	@ManyToOne
	@JoinColumn(name="vviQuestionSet",referencedColumnName="ID")
	private I4QuestionSets i4QuestionSets;
	
	private Integer maxScoreForVVI;
	private Boolean sendAutoVVILink;
	
	@ManyToOne
	@JoinColumn(name="statusIdForAutoVVILink",referencedColumnName="statusId")
	private StatusMaster statusMaster ;
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusIdForAutoVVILink",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus ;
	
	
	private Integer timeAllowedPerQuestion;
	private Integer VVIExpiresInDays;
	private Boolean jobCompletedVVILink;
	
	
	private Boolean offerAssessmentInviteOnly;
	
	private String jobTitle;     
	private Date jobStartDate;     
	private Date jobEndDate;     
	private String jobDescription;
	private String jobQualification;
	private Boolean isExpHireNotEqualToReqNo; 
	private Integer noOfExpHires;
	private Integer noofhires;
	private String requisitionNumber; 
	private Boolean writePrivilegeToSchool;
	private Boolean writePrivilegeToBranch;
	private String entryURL;     
	private String exitURL;     
	private String exitMessage; 
	private Integer flagForURL; 
	private Integer flagForMessage; 
	private Boolean isJobAssessment;
	private Boolean isPortfolioNeeded;
	private String assessmentDocument;
	private String jobStatus;
	// Deepak
	private Integer jobApplicationStatus;
	private Date positionStartDate;
	private Date positionEndDate;
	private Integer tempType;
	@OneToOne
	@JoinColumn(name="estechnicianId",referencedColumnName="estechnicianId")
	private EmploymentServicesTechnician employmentServicesTechnician;
	private String salaryRange;
	private String additionalDocumentPath;
	private String fte;
	private String payGrade;
	private String daysWorked;
	private String fsalary;
	private String fsalaryA;
	private String ssalary;
	
	//--------------------------
	private String jobType;
	private String pathOfJobDescription;  
	private String status;     
	private Integer createdBy;	
	private Integer createdByEntity;
	private Integer createdForEntity;
	private String apiJobId;
	private Date createdDateTime;     
	private String ipAddress;
	private Integer isPoolJob;
	private String jobInternalNotes; 
	private String districtAttachment;
	private String hoursPerDay;
	private Integer jobAssessmentStatus;
	private String positionStart;
	private String gradeLevel;
	private String salaryAdminPlan;
	private String approvalCode;
	private String step;
	private String positionType;
    private String mSalary;
    private String accountCode;
    private String monthsPerYear;
   // private String departmentApproval;
    private String jobCode;
    private String jobStartTime;
    private String jobEndTime;
    
	public String getJobStartTime() {
		return jobStartTime;
	}
	public void setJobStartTime(String jobStartTime) {
		this.jobStartTime = jobStartTime;
	}
	public String getJobEndTime() {
		return jobEndTime;
	}
	public void setJobEndTime(String jobEndTime) {
		this.jobEndTime = jobEndTime;
	}
	public String getGradeLevel() {
		return gradeLevel;
	}
	public void setGradeLevel(String gradeLevel) {
		this.gradeLevel = gradeLevel;
	}
	
	private Integer jobTimeZoneId;
	

	public Integer getJobTimeZoneId() {
		return jobTimeZoneId;
	}
	public void setJobTimeZoneId(Integer jobTimeZoneId) {
		this.jobTimeZoneId = jobTimeZoneId;
	}

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "schoolinjoborder", joinColumns = { @JoinColumn(name = "jobId") }, inverseJoinColumns = { @JoinColumn(name = "schoolId") })
	private List<SchoolMaster> school = new ArrayList<SchoolMaster>(0);
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "teacherassessmentattempt", joinColumns = { @JoinColumn(name = "jobId") }, inverseJoinColumns = { @JoinColumn(name = "teacherId") })
	private List< TeacherDetail> teacherDetail = new ArrayList<TeacherDetail>(0);

	public List<TeacherDetail> getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(List<TeacherDetail> teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	private Integer allSchoolsInDistrict;
	private Integer allGrades;
	private Integer selectedSchoolsInDistrict;
	private Integer pkOffered;
	private Integer kgOffered;
	private Integer g01Offered;
	private Integer g02Offered;
	private Integer g03Offered;
	private Integer g04Offered;
	private Integer g05Offered;
	private Integer g06Offered;
	private Integer g07Offered;
	private Integer g08Offered;
	private Integer g09Offered;
	private Integer g10Offered;
	private Integer g11Offered;
	private Integer g12Offered;
	private Integer attachDefaultHeadQuarterPillar;
	private Integer attachDefaultBranchPillar;
	private Integer attachDefaultDistrictPillar;
	private Integer attachDefaultSchoolPillar;
	private Integer attachDefaultJobSpecificPillar;
	private Integer attachNewPillar;
	private Integer noSchoolAttach;
	private Integer approvalBeforeGoLive;
	//isJobAssessment mukesh
	private Boolean isInviteOnly;
	private Boolean hiddenJob;
	private Boolean isVacancyJob;
	private Boolean notificationToschool;
	@Transient
	private String jobDescriptionHTML;
	
	@Transient
	private String jobQualificationHTML;
	
	private String specialEdFlag;
	
	public Boolean getIsExpHireNotEqualToReqNo() {
		return isExpHireNotEqualToReqNo;
	}
	public void setIsExpHireNotEqualToReqNo(Boolean isExpHireNotEqualToReqNo) {
		this.isExpHireNotEqualToReqNo = isExpHireNotEqualToReqNo;
	}

	@Transient
	private Integer fitScore;
	
	@Transient
	private String demoClass;
	
	@Transient
	private String schoolName;
	
	private Integer batchJobId;
	
	@Transient
	private Double normScore = -0.0;
	
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}
	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}
	
	public SubjectMaster getSubjectMaster() {
		return subjectMaster;
	}
	public void setSubjectMaster(SubjectMaster subjectMaster) {
		this.subjectMaster = subjectMaster;
	}
	public Integer getBatchJobId() {
		return batchJobId;
	}
	public void setBatchJobId(Integer batchJobId) {
		this.batchJobId = batchJobId;
	}
	
	public Integer getNoSchoolAttach() {
		return noSchoolAttach;
	}

	public void setNoSchoolAttach(Integer noSchoolAttach) {
		this.noSchoolAttach = noSchoolAttach;
	}

	public Integer getAllSchoolsInDistrict() {
		return allSchoolsInDistrict;
	}

	public void setAllSchoolsInDistrict(Integer allSchoolsInDistrict) {
		this.allSchoolsInDistrict = allSchoolsInDistrict;
	}
	public Integer getFlagForURL() {
		return flagForURL;
	}

	public void setFlagForURL(Integer flagForURL) {
		this.flagForURL = flagForURL;
	}

	public Integer getFlagForMessage() {
		return flagForMessage;
	}

	public void setFlagForMessage(Integer flagForMessage) {
		this.flagForMessage = flagForMessage;
	}

	public Integer getAllGrades() {
		return allGrades;
	}

	public void setAllGrades(Integer allGrades) {
		this.allGrades = allGrades;
	}

	public Integer getSelectedSchoolsInDistrict() {
		return selectedSchoolsInDistrict;
	}

	public void setSelectedSchoolsInDistrict(Integer selectedSchoolsInDistrict) {
		this.selectedSchoolsInDistrict = selectedSchoolsInDistrict;
	}

	public Integer getPkOffered() {
		return pkOffered;
	}

	public void setPkOffered(Integer pkOffered) {
		this.pkOffered = pkOffered;
	}

	public Integer getKgOffered() {
		return kgOffered;
	}

	public void setKgOffered(Integer kgOffered) {
		this.kgOffered = kgOffered;
	}

	public Integer getG01Offered() {
		return g01Offered;
	}

	public void setG01Offered(Integer g01Offered) {
		this.g01Offered = g01Offered;
	}

	public Integer getG02Offered() {
		return g02Offered;
	}

	public void setG02Offered(Integer g02Offered) {
		this.g02Offered = g02Offered;
	}

	public Integer getG03Offered() {
		return g03Offered;
	}

	public void setG03Offered(Integer g03Offered) {
		this.g03Offered = g03Offered;
	}

	public Integer getG04Offered() {
		return g04Offered;
	}

	public void setG04Offered(Integer g04Offered) {
		this.g04Offered = g04Offered;
	}

	public Integer getG05Offered() {
		return g05Offered;
	}

	public void setG05Offered(Integer g05Offered) {
		this.g05Offered = g05Offered;
	}

	public Integer getG06Offered() {
		return g06Offered;
	}

	public void setG06Offered(Integer g06Offered) {
		this.g06Offered = g06Offered;
	}

	public Integer getG07Offered() {
		return g07Offered;
	}

	public void setG07Offered(Integer g07Offered) {
		this.g07Offered = g07Offered;
	}

	public Integer getG08Offered() {
		return g08Offered;
	}

	public void setG08Offered(Integer g08Offered) {
		this.g08Offered = g08Offered;
	}

	public Integer getG09Offered() {
		return g09Offered;
	}

	public void setG09Offered(Integer g09Offered) {
		this.g09Offered = g09Offered;
	}

	public Integer getG10Offered() {
		return g10Offered;
	}

	public void setG10Offered(Integer g10Offered) {
		this.g10Offered = g10Offered;
	}

	public Integer getG11Offered() {
		return g11Offered;
	}

	public void setG11Offered(Integer g11Offered) {
		this.g11Offered = g11Offered;
	}

	public Integer getG12Offered() {
		return g12Offered;
	}

	public void setG12Offered(Integer g12Offered) {
		this.g12Offered = g12Offered;
	}

	public Integer getAttachDefaultHeadQuarterPillar() {
		return attachDefaultHeadQuarterPillar;
	}
	public void setAttachDefaultHeadQuarterPillar(
			Integer attachDefaultHeadQuarterPillar) {
		this.attachDefaultHeadQuarterPillar = attachDefaultHeadQuarterPillar;
	}
	public Integer getAttachDefaultBranchPillar() {
		return attachDefaultBranchPillar;
	}
	public void setAttachDefaultBranchPillar(Integer attachDefaultBranchPillar) {
		this.attachDefaultBranchPillar = attachDefaultBranchPillar;
	}
	public Integer getAttachDefaultDistrictPillar() {
		return attachDefaultDistrictPillar;
	}

	public void setAttachDefaultDistrictPillar(Integer attachDefaultDistrictPillar) {
		this.attachDefaultDistrictPillar = attachDefaultDistrictPillar;
	}

	public Integer getAttachDefaultSchoolPillar() {
		return attachDefaultSchoolPillar;
	}

	public void setAttachDefaultSchoolPillar(Integer attachDefaultSchoolPillar) {
		this.attachDefaultSchoolPillar = attachDefaultSchoolPillar;
	}

	public Integer getAttachDefaultJobSpecificPillar() {
		return attachDefaultJobSpecificPillar;
	}
	public void setAttachDefaultJobSpecificPillar(
			Integer attachDefaultJobSpecificPillar) {
		this.attachDefaultJobSpecificPillar = attachDefaultJobSpecificPillar;
	}
	public Integer getAttachNewPillar() {
		return attachNewPillar;
	}

	public void setAttachNewPillar(Integer attachNewPillar) {
		this.attachNewPillar = attachNewPillar;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getJobId() {
		return jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}


	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public Date getJobStartDate() {
		return jobStartDate;
	}

	public void setJobStartDate(Date jobStartDate) {
		this.jobStartDate = jobStartDate;
	}

	public Date getJobEndDate() {
		return jobEndDate;
	}

	public void setJobEndDate(Date jobEndDate) {
		this.jobEndDate = jobEndDate;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;		
	}

	public Integer getNoOfExpHires() {
		return noOfExpHires;
	}

	public void setNoOfExpHires(Integer noOfExpHires) {
		this.noOfExpHires = noOfExpHires;
	}
	
	public Integer getNoOfHires() {
		return noofhires;
	}

	public void setNoOfHires(Integer noofhires) {
		this.noofhires = noofhires;
	}

	public String getEntryURL() {
		return entryURL;
	}
	public String getRequisitionNumber() {
		return requisitionNumber;
	}
	public void setRequisitionNumber(String requisitionNumber) {
		this.requisitionNumber = requisitionNumber;
	}
	public Boolean getWritePrivilegeToSchool() {
		return writePrivilegeToSchool;
	}
	public void setWritePrivilegeToSchool(Boolean writePrivilegeToSchool) {
		this.writePrivilegeToSchool = writePrivilegeToSchool;
	}
	public Boolean getWritePrivilegeToBranch() {
		return writePrivilegeToBranch;
	}
	public void setWritePrivilegeToBranch(Boolean writePrivilegeToBranch) {
		this.writePrivilegeToBranch = writePrivilegeToBranch;
	}
	public void setEntryURL(String entryURL) {
		this.entryURL = entryURL;
	}

	public String getExitURL() {
		return exitURL;
	}

	public void setExitURL(String exitURL) {
		this.exitURL = exitURL;
	}

	public String getExitMessage() {
		return exitMessage;
	}

	public void setExitMessage(String exitMessage) {
		this.exitMessage = exitMessage;
	}

	public String getAssessmentDocument() {
		return assessmentDocument;
	}

	public void setAssessmentDocument(String assessmentDocument) {
		this.assessmentDocument = assessmentDocument;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getJobType() {
		return jobType;
	}
	
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getCreatedByEntity() {
		return createdByEntity;
	}

	public void setCreatedByEntity(Integer createdByEntity) {
		this.createdByEntity = createdByEntity;
	}

	public Integer getCreatedForEntity() {
		return createdForEntity;
	}

	public void setCreatedForEntity(Integer createdForEntity) {
		this.createdForEntity = createdForEntity;
	}
	public String getApiJobId() {
		return apiJobId;
	}
	public void setApiJobId(String apiJobId) {
		this.apiJobId = apiJobId;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getJobQualification() {
		return jobQualification;
	}
	public void setJobQualification(String jobQualification) {
		this.jobQualification = jobQualification;
	}
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public List<SchoolMaster> getSchool() {
		return school;
	}

	public void setSchool(List<SchoolMaster> school) {
		this.school = school;
	}
	public Boolean getIsJobAssessment() {
		return isJobAssessment;
	}

	public void setIsJobAssessment(Boolean isJobAssessment) {
		this.isJobAssessment = isJobAssessment;
	}
	public Boolean getIsPortfolioNeeded() {
		return isPortfolioNeeded;
	}
	public void setIsPortfolioNeeded(Boolean isPortfolioNeeded) {
		this.isPortfolioNeeded = isPortfolioNeeded;
	}
	public String getJobDescriptionHTML() 
	{
		return jobDescription;
	}
	
	public String getjobQualificationHTML() 
	{
		return jobQualification;
	}
	public void setDemoClass(String demoClass) {
		this.demoClass = demoClass;
	}
	
	public String getDemoClass() {
		return demoClass;
	}
	public void setFitScore(Integer fitScore) {
		this.fitScore = fitScore;
	}
	public Integer getFitScore() {
		return fitScore;
	}
	
	public String getPathOfJobDescription() {
		return pathOfJobDescription;
	}
	public void setPathOfJobDescription(String pathOfJobDescription) {
		this.pathOfJobDescription = pathOfJobDescription;
	}
	
	public GeoZoneMaster getGeoZoneMaster() {
		return geoZoneMaster;
	}
	public void setGeoZoneMaster(GeoZoneMaster geoZoneMaster) {
		this.geoZoneMaster = geoZoneMaster;
	}
	
	public Integer getApprovalBeforeGoLive() {
		return approvalBeforeGoLive;
	}
	public void setApprovalBeforeGoLive(Integer approvalBeforeGoLive) {
		this.approvalBeforeGoLive = approvalBeforeGoLive;
	}
	
	public Double getNormScore() {
		return normScore;
	}
	public void setNormScore(Double normScore) {
		this.normScore = normScore;
	}
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof JobOrder))return false;
	    JobOrder jobOrder = (JobOrder)object;
	  
	    if(this.jobId.equals(jobOrder.getJobId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+jobId);
	}

	

	@Override
	public String toString() 
	{
		return "["+jobId+" : "+jobTitle+"]";
	}
	
	
	public static Comparator<JobOrder> jobOrderComparator  = new Comparator<JobOrder>() {
		public int compare(JobOrder job1, JobOrder job2) {	
		//descending order
		return job1.getJobTitle().compareTo(job2.getJobTitle());
		}		
	};
	
	public static Comparator<JobOrder> jobOrderComparatorFitScoreDesc  = new Comparator<JobOrder>(){
		public int compare(JobOrder job1, JobOrder job2) {			
			//int c = job2.getFitScore().compareTo(job1.getFitScore());
			return job2.getFitScore().compareTo(job1.getFitScore());
		}		
	};
	
	public static Comparator<JobOrder> jobOrderComparatorFitScoreAsc  = new Comparator<JobOrder>(){
		public int compare(JobOrder job1, JobOrder job2) {			
			//int c = job1.getFitScore().compareTo(job2.getFitScore());
			return job1.getFitScore().compareTo(job2.getFitScore());
		}		
	};
	public static Comparator<JobOrder> jobOrderComparatorDemoDesc  = new Comparator<JobOrder>(){
		public int compare(JobOrder job1, JobOrder job2) {			
			int c = job2.getDemoClass().compareTo(job1.getDemoClass());
			return c;
		}		
	};
	public static Comparator<JobOrder> jobOrderComparatorDemoAsc  = new Comparator<JobOrder>(){
		public int compare(JobOrder job1, JobOrder job2) {			
			int c = job1.getDemoClass().compareTo(job2.getDemoClass());
			return c;
		}		
	};
	public static Comparator<JobOrder> jobOrderComparatorPoolJob  = new Comparator<JobOrder>(){
		public int compare(JobOrder job1, JobOrder job2) {			
			Integer cScore1 = job1.getIsPoolJob();
			Integer cScore2 = job2.getIsPoolJob();		
			int c = cScore2.compareTo(cScore1);
			return c;
		}		
	};
	

	public Integer getIsPoolJob() {
		return isPoolJob;
	}
	public void setIsPoolJob(Integer isPoolJob) {
		this.isPoolJob = isPoolJob;
	}
	public String getDistrictAttachment() {
		return districtAttachment;
	}
	public void setDistrictAttachment(String districtAttachment) {
		this.districtAttachment = districtAttachment;
	}
	public Integer getJobAssessmentStatus() {
		return jobAssessmentStatus;
	}
	public void setJobAssessmentStatus(Integer jobAssessmentStatus) {
		this.jobAssessmentStatus = jobAssessmentStatus;
	}
	public Boolean getOfferVirtualVideoInterview() {
		return offerVirtualVideoInterview;
	}
	public void setOfferVirtualVideoInterview(Boolean offerVirtualVideoInterview) {
		this.offerVirtualVideoInterview = offerVirtualVideoInterview;
	}
	public I4QuestionSets getI4QuestionSets() {
		return i4QuestionSets;
	}
	public void setI4QuestionSets(I4QuestionSets i4QuestionSets) {
		this.i4QuestionSets = i4QuestionSets;
	}
	public Integer getMaxScoreForVVI() {
		return maxScoreForVVI;
	}
	public void setMaxScoreForVVI(Integer maxScoreForVVI) {
		this.maxScoreForVVI = maxScoreForVVI;
	}
	public Boolean getSendAutoVVILink() {
		return sendAutoVVILink;
	}
	public void setSendAutoVVILink(Boolean sendAutoVVILink) {
		this.sendAutoVVILink = sendAutoVVILink;
	}
	public StatusMaster getStatusMaster() {
		return statusMaster;
	}
	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}
	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}
	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}
	public Integer getTimeAllowedPerQuestion() {
		return timeAllowedPerQuestion;
	}
	public void setTimeAllowedPerQuestion(Integer timeAllowedPerQuestion) {
		this.timeAllowedPerQuestion = timeAllowedPerQuestion;
	}
	public Integer getVVIExpiresInDays() {
		return VVIExpiresInDays;
	}
	public void setVVIExpiresInDays(Integer vVIExpiresInDays) {
		VVIExpiresInDays = vVIExpiresInDays;
	}
	public Boolean getHiddenJob() {
		return hiddenJob;
	}
	public void setHiddenJob(Boolean hiddenJob) {
		this.hiddenJob = hiddenJob;
	}
	public Boolean getIsInviteOnly() {
		return isInviteOnly;
	}
	public void setIsInviteOnly(Boolean isInviteOnly) {
		this.isInviteOnly = isInviteOnly;
	}
	public Boolean getIsVacancyJob() {
		return isVacancyJob;
	}
	public void setIsVacancyJob(Boolean isVacancyJob) {
		this.isVacancyJob = isVacancyJob;
	}
	public Boolean getOfferAssessmentInviteOnly() {
		return offerAssessmentInviteOnly;
	}
	public void setOfferAssessmentInviteOnly(Boolean offerAssessmentInviteOnly) {
		this.offerAssessmentInviteOnly = offerAssessmentInviteOnly;
	}

	public Boolean getNotificationToschool() {
		return notificationToschool;
	}
	public void setNotificationToschool(Boolean notificationToschool) {
		this.notificationToschool = notificationToschool;
	}

	public String getJobInternalNotes() {
		return jobInternalNotes;
	}
	public void setJobInternalNotes(String jobInternalNotes) {
		this.jobInternalNotes = jobInternalNotes;
	}
	public Integer getJobApplicationStatus() {
		return jobApplicationStatus;
	}
	public void setJobApplicationStatus(Integer jobApplicationStatus) {
		this.jobApplicationStatus = jobApplicationStatus;
	}
	public Date getPositionStartDate() {
		return positionStartDate;
	}
	public void setPositionStartDate(Date positionStartDate) {
		this.positionStartDate = positionStartDate;
	}
	public Date getPositionEndDate() {
		return positionEndDate;
	}
	public void setPositionEndDate(Date positionEndDate) {
		this.positionEndDate = positionEndDate;
	}
	public Integer getTempType() {
		return tempType;
	}
	public void setTempType(Integer tempType) {
		this.tempType = tempType;
	}
	public EmploymentServicesTechnician getEmploymentServicesTechnician() {
		return employmentServicesTechnician;
	}
	public void setEmploymentServicesTechnician(
			EmploymentServicesTechnician employmentServicesTechnician) {
		this.employmentServicesTechnician = employmentServicesTechnician;
	}
	public String getSalaryRange() {
		return salaryRange;
	}
	public void setSalaryRange(String salaryRange) {
		this.salaryRange = salaryRange;
	}
	public String getAdditionalDocumentPath() {
		return additionalDocumentPath;
	}
	public void setAdditionalDocumentPath(String additionalDocumentPath) {
		this.additionalDocumentPath = additionalDocumentPath;
	}
	public String getHoursPerDay() {
		return hoursPerDay;
	}
	public void setHoursPerDay(String hoursPerDay) {
		this.hoursPerDay = hoursPerDay;
	}	
	public String getSpecialEdFlag() {
		return specialEdFlag;
	}
	public void setSpecialEdFlag(String specialEdFlag) {
		this.specialEdFlag = specialEdFlag;
	}
	public String getFte() {
		return fte;
	}
	public void setFte(String fte) {
		this.fte = fte;
	}
	public String getPayGrade() {
		return payGrade;
	}
	public void setPayGrade(String payGrade) {
		this.payGrade = payGrade;
	}
	public String getDaysWorked() {
		return daysWorked;
	}
	public void setDaysWorked(String daysWorked) {
		this.daysWorked = daysWorked;
	}
	public String getFsalary() {
		return fsalary;
	}
	public void setFsalary(String fsalary) {
		this.fsalary = fsalary;
	}
	public String getFsalaryA() {
		return fsalaryA;
	}
	public void setFsalaryA(String fsalaryA) {
		this.fsalaryA = fsalaryA;
	}
	public String getSsalary() {
		return ssalary;
	}
	public void setSsalary(String ssalary) {
		this.ssalary = ssalary;
	}
	public String getPositionStart() {
		return positionStart;
	}
	public void setPositionStart(String positionStart) {
		this.positionStart = positionStart;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getSalaryAdminPlan() {
		return salaryAdminPlan;
	}
	public void setSalaryAdminPlan(String salaryAdminPlan) {
		this.salaryAdminPlan = salaryAdminPlan;
	}
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public String getStep() {
		return step;
	}
	public void setStep(String step) {
		this.step = step;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public String getAccountCode() {
		return accountCode;
	}
	public void setPositionType(String positionType) {
		this.positionType = positionType;
	}
	public String getPositionType() {
		return positionType;
	}
	public void setmSalary(String mSalary) {
		this.mSalary = mSalary;
	}
	public String getmSalary() {
		return mSalary;
	}
	public String getMonthsPerYear() {
		return monthsPerYear;
	}
	public void setMonthsPerYear(String monthsPerYear) {
		this.monthsPerYear = monthsPerYear;
	}
	/*public void setDepartmentApproval(String departmentApproval) {
		this.departmentApproval = departmentApproval;
	}
	public String getDepartmentApproval() {
		return departmentApproval;
	}*/
	public Boolean getJobCompletedVVILink() {
		return jobCompletedVVILink;
	}
	public void setJobCompletedVVILink(Boolean jobCompletedVVILink) {
		this.jobCompletedVVILink = jobCompletedVVILink;
	}
	public String getJobCode() {
		return jobCode;
	}
	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}
	
}
