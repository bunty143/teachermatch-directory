package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;

@Entity(name="teacherstrikelog")
public class TeacherStrikeLog implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8475658614681233167L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer strikeId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="teacherAssessmentId",referencedColumnName="teacherAssessmentId")
	private TeacherAssessmentdetail teacherAssessmentdetail;
	@ManyToOne
	@JoinColumn(name="teacherAssessmentQuestionId",referencedColumnName="teacherAssessmentQuestionId")
	private TeacherAssessmentQuestion teacherAssessmentQuestion;
	private String ipAddress;
	private Date createdDateTime;
	
	public Integer getStrikeId() {
		return strikeId;
	}
	public void setStrikeId(Integer strikeId) {
		this.strikeId = strikeId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public TeacherAssessmentdetail getTeacherAssessmentdetail() {
		return teacherAssessmentdetail;
	}
	public void setTeacherAssessmentdetail(
			TeacherAssessmentdetail teacherAssessmentdetail) {
		this.teacherAssessmentdetail = teacherAssessmentdetail;
	}
	public TeacherAssessmentQuestion getTeacherAssessmentQuestion() {
		return teacherAssessmentQuestion;
	}
	public void setTeacherAssessmentQuestion(
			TeacherAssessmentQuestion teacherAssessmentQuestion) {
		this.teacherAssessmentQuestion = teacherAssessmentQuestion;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
}
