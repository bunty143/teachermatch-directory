package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="regionmaster")
public class RegionMaster implements Serializable
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3141867676678659224L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer regionId;
	private String regionName;
	private Integer parentRegionId;
	private String status;
	
	
	public Integer getRegionId() {
		return regionId;
	}
	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public Integer getParentRegionId() {
		return parentRegionId;
	}
	public void setParentRegionId(Integer parentRegionId) {
		this.parentRegionId = parentRegionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
