package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.user.UserMaster;
import tm.utility.Utility;

@Entity
@Table(name="districtkeycontact")
public class DistrictKeyContact implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5396090183855912824L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer keyContactId;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	private Integer districtId;
	@ManyToOne
	@JoinColumn(name="keyContactTypeId",referencedColumnName="contactTypeId")
	private ContactTypeMaster keyContactTypeId;
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster userMaster;
	private String keyContactFirstName;
	private String keyContactLastName;
	private String keyContactEmailAddress;
	private String keyContactPhoneNumber;
	private String keyContactTitle;
	
	
	public String getKeyContactFirstName() {
		return keyContactFirstName;
	}
	public void setKeyContactFirstName(String keyContactFirstName) {
		this.keyContactFirstName = keyContactFirstName;
	}
	public String getKeyContactLastName() {
		return keyContactLastName;
	}
	public void setKeyContactLastName(String keyContactLastName) {
		this.keyContactLastName = keyContactLastName;
	}
	public Integer getKeyContactId() {
		return keyContactId;
	}
	public void setKeyContactId(Integer keyContactId) {
		this.keyContactId = keyContactId;
	}
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public ContactTypeMaster getKeyContactTypeId() {
		return keyContactTypeId;
	}
	public void setKeyContactTypeId(ContactTypeMaster keyContactTypeId) {
		this.keyContactTypeId = keyContactTypeId;
	}
	public String getKeyContactEmailAddress() {
		return keyContactEmailAddress;
	}
	public void setKeyContactEmailAddress(String keyContactEmailAddress) {
		this.keyContactEmailAddress = Utility.trim(keyContactEmailAddress);
	}
	public String getKeyContactPhoneNumber() {
		return keyContactPhoneNumber;
	}
	public void setKeyContactPhoneNumber(String keyContactPhoneNumber) {
		this.keyContactPhoneNumber = Utility.trim(keyContactPhoneNumber);
	}
	public String getKeyContactTitle() {
		return keyContactTitle;
	}
	public void setKeyContactTitle(String keyContactTitle) {
		this.keyContactTitle = Utility.trim(keyContactTitle);
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
}
