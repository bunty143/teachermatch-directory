package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="questiontypemaster")
public class QuestionTypeMaster implements Serializable{

	private static final long serialVersionUID = -7637665800064504824L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer questionTypeId;
	private String questionType;
	private String questionTypeShortName;
	private String status;
	
	public Integer getQuestionTypeId() {
		return questionTypeId;
	}
	public void setQuestionTypeId(Integer questionTypeId) {
		this.questionTypeId = questionTypeId;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getQuestionTypeShortName() {
		return questionTypeShortName;
	}
	public void setQuestionTypeShortName(String questionTypeShortName) {
		this.questionTypeShortName = questionTypeShortName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
