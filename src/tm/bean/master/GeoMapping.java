package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="geomapping")
public class GeoMapping implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5894155705501356418L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer actualGeoId;
	
	@ManyToOne
	@JoinColumn(name="geoId",referencedColumnName="geoId")
	private GeographyMaster geoId;
	
	private String actualGeoName;
	private String status;
	
	public Integer getActualGeoId() {
		return actualGeoId;
	}
	public void setActualGeoId(Integer actualGeoId) {
		this.actualGeoId = actualGeoId;
	}
	public GeographyMaster getGeoId() {
		return geoId;
	}
	public void setGeoId(GeographyMaster geoId) {
		this.geoId = geoId;
	}
	public String getActualGeoName() {
		return actualGeoName;
	}
	public void setActualGeoName(String actualGeoName) {
		this.actualGeoName = actualGeoName;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
	
}
