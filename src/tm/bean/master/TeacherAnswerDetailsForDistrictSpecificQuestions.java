package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;

@Entity
@Table(name="teacheranswerdetailsfordistrictspecificquestions")
public class TeacherAnswerDetailsForDistrictSpecificQuestions implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4801094296567726849L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer answerId; 	
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="questionId",referencedColumnName="questionId")
	private DistrictSpecificQuestions  districtSpecificQuestions;
	
	@ManyToOne
	 @JoinColumn(name="QuestionSetID",referencedColumnName="ID")
	 private QqQuestionSets questionSets;
	
	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster  questionTypeMaster;
	
	private String selectedOptions;
	
	private String insertedText;
	private Boolean isValidAnswer;
	private Date createdDateTime;
	private String question;
	private String questionOption;
	private String questionType;
	private Boolean isActive;
	private Boolean isonboarding;
	private String selectedoptionmlsel;
	
	public Integer getAnswerId() {
		return answerId;
	}
	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public DistrictSpecificQuestions getDistrictSpecificQuestions() {
		return districtSpecificQuestions;
	}
	public void setDistrictSpecificQuestions(
			DistrictSpecificQuestions districtSpecificQuestions) {
		this.districtSpecificQuestions = districtSpecificQuestions;
	}
	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}
	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}
	public String getSelectedOptions() {
		return selectedOptions;
	}
	public void setSelectedOptions(String selectedOptions) {
		this.selectedOptions = selectedOptions;
	}
	public String getInsertedText() {
		return insertedText;
	}
	public void setInsertedText(String insertedText) {
		this.insertedText = insertedText;
	}
	public Boolean getIsValidAnswer() {
		return isValidAnswer;
	}
	public void setIsValidAnswer(Boolean isValidAnswer) {
		this.isValidAnswer = isValidAnswer;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getQuestionOption() {
		return questionOption;
	}
	public void setQuestionOption(String questionOption) {
		this.questionOption = questionOption;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public QqQuestionSets getQuestionSets() {
		return questionSets;
	}
	public void setQuestionSets(QqQuestionSets questionSets) {
		this.questionSets = questionSets;
	}
	
	
	public Boolean getIsonboarding() {
		return isonboarding;
	}
	public void setIsonboarding(Boolean isonboarding) {
		this.isonboarding = isonboarding;
	}
	public String getSelectedoptionmlsel() {
		return selectedoptionmlsel;
	}
	public void setSelectedoptionmlsel(String selectedoptionmlsel) {
		this.selectedoptionmlsel = selectedoptionmlsel;
	}
	
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof TeacherAnswerDetailsForDistrictSpecificQuestions))return false;
	    TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestions = (TeacherAnswerDetailsForDistrictSpecificQuestions)object;
	  
	    if(this.answerId.equals(teacherAnswerDetailsForDistrictSpecificQuestions.getAnswerId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+answerId);
	}
	
}
