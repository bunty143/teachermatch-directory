package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="gendermaster")
public class GenderMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8968556015560069181L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer genderId;     
	private String genderName; 
	private Integer orderBy; 
	private String status;
	
	public Integer getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}
	public Integer getGenderId() {
		return genderId;
	}
	public void setGenderId(Integer genderId) {
		this.genderId = genderId;
	}
	public String getGenderName() {
		return genderName;
	}
	public void setGenderName(String genderName) {
		this.genderName = genderName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}	
	
}
