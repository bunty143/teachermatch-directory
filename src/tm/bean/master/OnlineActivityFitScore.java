package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherDetail;

@Entity
@Table(name="onlineactivityfitscore")
public class OnlineActivityFitScore implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4655487554436314400L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer onlineActivityFitScoreId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	
	@ManyToOne
	@JoinColumn(name="teacherId", referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	private Integer minScore;
	private Integer maxScore;
	private Date createdDateTime;

	public Integer getOnlineActivityFitScoreId() {
		return onlineActivityFitScoreId;
	}
	public void setOnlineActivityFitScoreId(Integer onlineActivityFitScoreId) {
		this.onlineActivityFitScoreId = onlineActivityFitScoreId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}
	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public Integer getMinScore() {
		return minScore;
	}
	public void setMinScore(Integer minScore) {
		this.minScore = minScore;
	}
	public Integer getMaxScore() {
		return maxScore;
	}
	public void setMaxScore(Integer maxScore) {
		this.maxScore = maxScore;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
}
