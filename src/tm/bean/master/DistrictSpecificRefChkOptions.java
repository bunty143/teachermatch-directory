package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name="districtspecificrefchkoptions")
public class DistrictSpecificRefChkOptions implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6896363297210030601L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer optionId; 	
	
	@ManyToOne
	@JoinColumn(name="questionId",referencedColumnName="questionId")
	private DistrictSpecificRefChkQuestions districtSpecificRefChkQuestions;
	
	private String questionOption;
	private Boolean validOption;
	
	private Integer rank;                      
	private Double score;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private Date createdDateTime;
	
	private String status;

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Integer getOptionId() {
		return optionId;
	}

	public void setOptionId(Integer optionId) {
		this.optionId = optionId;
	}

	public DistrictSpecificRefChkQuestions getDistrictSpecificRefChkQuestions() {
		return districtSpecificRefChkQuestions;
	}

	public void setDistrictSpecificRefChkQuestions(
			DistrictSpecificRefChkQuestions districtSpecificRefChkQuestions) {
		this.districtSpecificRefChkQuestions = districtSpecificRefChkQuestions;
	}

	public String getQuestionOption() {
		return questionOption;
	}

	public void setQuestionOption(String questionOption) {
		this.questionOption = questionOption;
	}

	public Boolean getValidOption() {
		return validOption;
	}

	public void setValidOption(Boolean validOption) {
		this.validOption = validOption;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
