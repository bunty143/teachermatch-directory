package tm.bean.master;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.user.UserMaster;

@Entity
@Table(name="statusnodemaster")
public class StatusNodeMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3755332024672761415L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer statusNodeId;
	
	private String statusNodeName;
	
	private Integer orderNumber;
	
	private String status;
	

	public Integer getStatusNodeId() {
		return statusNodeId;
	}

	public void setStatusNodeId(Integer statusNodeId) {
		this.statusNodeId = statusNodeId;
	}

	public String getStatusNodeName() {
		return statusNodeName;
	}

	public void setStatusNodeName(String statusNodeName) {
		this.statusNodeName = statusNodeName;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
