package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="certificationtypemaster")
public class CertificationTypeMaster implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1774469282972357605L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Integer certificationTypeMasterId;

	String certificationType;
	String status;
	
	public Integer getCertificationTypeMasterId() {
		return certificationTypeMasterId;
	}
	public void setCertificationTypeMasterId(Integer certificationTypeMasterId) {
		this.certificationTypeMasterId = certificationTypeMasterId;
	}
	public String getCertificationType() {
		return certificationType;
	}
	public void setCertificationType(String certificationType) {
		this.certificationType = certificationType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
