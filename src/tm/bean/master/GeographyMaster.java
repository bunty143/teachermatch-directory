package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="geographymaster")
public class GeographyMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1221359459951454527L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer geoId; 
	private String geoName;
	private String status;
	public Integer getGeoId() {
		return geoId;
	}
	public void setGeoId(Integer geoId) {
		this.geoId = geoId;
	}
	public String getGeoName() {
		return geoName;
	}
	public void setGeoName(String geoName) {
		this.geoName = geoName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
	
}
