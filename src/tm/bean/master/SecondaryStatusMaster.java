package tm.bean.master;

import java.io.Serializable;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherSecondaryStatus;

@Entity
@Table(name="secondarystatusmaster")
public class SecondaryStatusMaster implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7161207004651390041L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer secStatusId;
	private String secStatusName;
	private String status;
	
	private Integer headQuarterId;
	private Integer branchId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster  districtMaster;
	
	private String pathOfTagsIcon;
	
	public String getPathOfTagsIcon() {
		return pathOfTagsIcon;
	}
	public void setPathOfTagsIcon(String pathOfTagsIcon) {
		this.pathOfTagsIcon = pathOfTagsIcon;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public Integer getSecStatusId() {
		return secStatusId;
	}
	public void setSecStatusId(Integer secStatusId) {
		this.secStatusId = secStatusId;
	}
	public String getSecStatusName() {
		return secStatusName;
	}
	public void setSecStatusName(String secStatusName) {
		this.secStatusName = secStatusName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	} 
	
	public Integer getHeadQuarterId() {
		return headQuarterId;
	}
	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof SecondaryStatusMaster))return false;
	    SecondaryStatusMaster secondaryStatusMaster = (SecondaryStatusMaster)object;
	  
	    if(this.secStatusId.equals(secondaryStatusMaster.getSecStatusId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+secStatusId);
	}
	
}
