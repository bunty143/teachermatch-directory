package tm.bean.master;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;

@Entity
@Table(name="pdstrengthsandopportunities")
public class PdStrengthsAndOpportunities implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4068028605260548182L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer statementId;
	@ManyToOne
	@JoinColumn(name="objectiveId",referencedColumnName="objectiveId")
	private ObjectiveMaster objectiveMaster;
	private String statement;
	@ManyToOne
	@JoinColumn(name="parentStatementId",referencedColumnName="statementId")
	private PdStrengthsAndOpportunities strengthsAndOpportunities;
	
	/*@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "pdstrengthsandopportunities", joinColumns = { @JoinColumn(name = "statementId") }, inverseJoinColumns = { @JoinColumn(name = "parentStatementId") })
	private List<PdStrengthsAndOpportunities> pdStrengthsAndOpportunitiesChildList = new ArrayList<PdStrengthsAndOpportunities>(0);*/
	
	private String statementType;
	private String status;
	public Integer getStatementId() {
		return statementId;
	}
	public void setStatementId(Integer statementId) {
		this.statementId = statementId;
	}
	public ObjectiveMaster getObjectiveMaster() {
		return objectiveMaster;
	}
	public void setObjectiveMaster(ObjectiveMaster objectiveMaster) {
		this.objectiveMaster = objectiveMaster;
	}
	public String getStatement() {
		return statement;
	}
	public void setStatement(String statement) {
		this.statement = statement;
	}
	public PdStrengthsAndOpportunities getStrengthsAndOpportunities() {
		return strengthsAndOpportunities;
	}
	public void setStrengthsAndOpportunities(
			PdStrengthsAndOpportunities strengthsAndOpportunities) {
		this.strengthsAndOpportunities = strengthsAndOpportunities;
	}
	public String getStatementType() {
		return statementType;
	}
	public void setStatementType(String statementType) {
		this.statementType = statementType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	/*public List<PdStrengthsAndOpportunities> getPdStrengthsAndOpportunitiesChildList() {
		return pdStrengthsAndOpportunitiesChildList;
	}
	public void setPdStrengthsAndOpportunitiesChildList(List<PdStrengthsAndOpportunities> pdStrengthsAndOpportunitiesChildList) {
		this.pdStrengthsAndOpportunitiesChildList = pdStrengthsAndOpportunitiesChildList;
	}*/
	
	
	
}
