package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;

@Entity
@Table(name="dspqjobwisestatus")
public class DspqJobWiseStatus implements Serializable{

	private static final long serialVersionUID = 844512918808900862L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer dspqJobWiseStatus;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	private Integer jobCategoryId;
	private Boolean isCoverLetterRequired;
	private Boolean coverLetter;
	private Boolean isPersonaInfoRequired;
	private Boolean personaInfo;
	private Boolean isAcademicRequired;
	private Boolean academic;
	private Boolean isCredentialsRequired;
	private Boolean credentials;
	private Boolean isProfessionalRequired;
	private Boolean professional;
	private Boolean isAffidaviteRequired;
	private Boolean affidavite;
	private Boolean portfolioComplete;
	private Boolean isPreScreenRequired;
	private Boolean preScreen;
	private Boolean isEPIRequired;
	private Boolean EPI;
	private Boolean isJSIRequired;
	private Boolean JSI;
	private Boolean jobComplete;
	private Date jobCompleteDate;
	private String iPAddress;
	private Date createdDataTime;
	
	public Integer getDspqJobWiseStatus() {
		return dspqJobWiseStatus;
	}
	public void setDspqJobWiseStatus(Integer dspqJobWiseStatus) {
		this.dspqJobWiseStatus = dspqJobWiseStatus;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public Integer getJobCategoryId() {
		return jobCategoryId;
	}
	public void setJobCategoryId(Integer jobCategoryId) {
		this.jobCategoryId = jobCategoryId;
	}
	public Boolean getIsCoverLetterRequired() {
		return isCoverLetterRequired;
	}
	public void setIsCoverLetterRequired(Boolean isCoverLetterRequired) {
		this.isCoverLetterRequired = isCoverLetterRequired;
	}
	public Boolean getCoverLetter() {
		return coverLetter;
	}
	public void setCoverLetter(Boolean coverLetter) {
		this.coverLetter = coverLetter;
	}
	public Boolean getIsPersonaInfoRequired() {
		return isPersonaInfoRequired;
	}
	public void setIsPersonaInfoRequired(Boolean isPersonaInfoRequired) {
		this.isPersonaInfoRequired = isPersonaInfoRequired;
	}
	public Boolean getPersonaInfo() {
		return personaInfo;
	}
	public void setPersonaInfo(Boolean personaInfo) {
		this.personaInfo = personaInfo;
	}
	public Boolean getIsAcademicRequired() {
		return isAcademicRequired;
	}
	public void setIsAcademicRequired(Boolean isAcademicRequired) {
		this.isAcademicRequired = isAcademicRequired;
	}
	public Boolean getAcademic() {
		return academic;
	}
	public void setAcademic(Boolean academic) {
		this.academic = academic;
	}
	public Boolean getIsCredentialsRequired() {
		return isCredentialsRequired;
	}
	public void setIsCredentialsRequired(Boolean isCredentialsRequired) {
		this.isCredentialsRequired = isCredentialsRequired;
	}
	public Boolean getCredentials() {
		return credentials;
	}
	public void setCredentials(Boolean credentials) {
		this.credentials = credentials;
	}
	public Boolean getIsProfessionalRequired() {
		return isProfessionalRequired;
	}
	public void setIsProfessionalRequired(Boolean isProfessionalRequired) {
		this.isProfessionalRequired = isProfessionalRequired;
	}
	public Boolean getProfessional() {
		return professional;
	}
	public void setProfessional(Boolean professional) {
		this.professional = professional;
	}
	public Boolean getIsAffidaviteRequired() {
		return isAffidaviteRequired;
	}
	public void setIsAffidaviteRequired(Boolean isAffidaviteRequired) {
		this.isAffidaviteRequired = isAffidaviteRequired;
	}
	public Boolean getAffidavite() {
		return affidavite;
	}
	public void setAffidavite(Boolean affidavite) {
		this.affidavite = affidavite;
	}
	public Boolean getPortfolioComplete() {
		return portfolioComplete;
	}
	public void setPortfolioComplete(Boolean portfolioComplete) {
		this.portfolioComplete = portfolioComplete;
	}
	public Boolean getIsPreScreenRequired() {
		return isPreScreenRequired;
	}
	public void setIsPreScreenRequired(Boolean isPreScreenRequired) {
		this.isPreScreenRequired = isPreScreenRequired;
	}
	public Boolean getPreScreen() {
		return preScreen;
	}
	public void setPreScreen(Boolean preScreen) {
		this.preScreen = preScreen;
	}
	public Boolean getIsEPIRequired() {
		return isEPIRequired;
	}
	public void setIsEPIRequired(Boolean isEPIRequired) {
		this.isEPIRequired = isEPIRequired;
	}
	public Boolean getEPI() {
		return EPI;
	}
	public void setEPI(Boolean ePI) {
		EPI = ePI;
	}
	public Boolean getIsJSIRequired() {
		return isJSIRequired;
	}
	public void setIsJSIRequired(Boolean isJSIRequired) {
		this.isJSIRequired = isJSIRequired;
	}
	public Boolean getJSI() {
		return JSI;
	}
	public void setJSI(Boolean jSI) {
		JSI = jSI;
	}
	public Boolean getJobComplete() {
		return jobComplete;
	}
	public void setJobComplete(Boolean jobComplete) {
		this.jobComplete = jobComplete;
	}
	public Date getJobCompleteDate() {
		return jobCompleteDate;
	}
	public void setJobCompleteDate(Date jobCompleteDate) {
		this.jobCompleteDate = jobCompleteDate;
	}
	public String getiPAddress() {
		return iPAddress;
	}
	public void setiPAddress(String iPAddress) {
		this.iPAddress = iPAddress;
	}
	public Date getCreatedDataTime() {
		return createdDataTime;
	}
	public void setCreatedDataTime(Date createdDataTime) {
		this.createdDataTime = createdDataTime;
	}
	
	
	
}
