package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="skillattributesmaster")
public class SkillAttributesMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6261870282569304429L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer skillAttributeId;
	private String skillName;
	private Integer createdBy;
	private Date createdDateTime;
	private String status;
	
	private Boolean baseStatus;
	
	public Integer getSkillAttributeId() {
		return skillAttributeId;
	}
	public void setSkillAttributeId(Integer skillAttributeId) {
		this.skillAttributeId = skillAttributeId;
	}
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getBaseStatus() {
		return baseStatus;
	}
	public void setBaseStatus(Boolean baseStatus) {
		this.baseStatus = baseStatus;
	}
	
	
	
	
}
