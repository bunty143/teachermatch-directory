package tm.bean.master;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="secondarystatus")
public class SecondaryStatus implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7519237231729734389L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer secondaryStatusId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;

	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;

	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;

	private String secondaryStatusName;
	@ManyToOne
	@JoinColumn(name="parentStatusId",referencedColumnName="statusId")
	private StatusMaster statusMaster;
	
	@ManyToOne
	@JoinColumn(name="parentSecondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus;
	
	
	@ManyToOne
	@JoinColumn(name="copySecondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus_copy;
	
	private Integer orderNumber;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster usermaster;
	
	private Date createdDateTime;
	
	@OneToMany()
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="parentSecondaryStatusId",referencedColumnName="secondaryStatusId",insertable=false,updatable=false)
	@OrderBy("createdDateTime")
	private List<SecondaryStatus> children;
	
	@ManyToOne
	@JoinColumn(name="parentSecondaryStatusId",referencedColumnName="secondaryStatusId",insertable=false,updatable=false)
	private SecondaryStatus parentSecondaryStatus;
	
	private String status;
	
	@ManyToOne
	@JoinColumn(name="statusNodeId",referencedColumnName="statusNodeId")
	private StatusNodeMaster statusNodeMaster;
	
	@Transient
	private Double maxFitScore;
	
	private String slcName;
	
	
	/*@Transient
	private String statusShortName;

	public String getStatusShortName() {
		return statusShortName;
	}

	public void setStatusShortName(String statusShortName) {
		this.statusShortName = statusShortName;
	}*/
	
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}

	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}

	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}

	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}
	
	
	
	public Double getMaxFitScore() {
		return maxFitScore;
	}

	public void setMaxFitScore(Double maxFitScore) {
		this.maxFitScore = maxFitScore;
	}

	public SecondaryStatus getParentSecondaryStatus() {
		return parentSecondaryStatus;
	}

	public void setParentSecondaryStatus(SecondaryStatus parentSecondaryStatus) {
		this.parentSecondaryStatus = parentSecondaryStatus;
	}

	public StatusNodeMaster getStatusNodeMaster() {
		return statusNodeMaster;
	}

	public void setStatusNodeMaster(StatusNodeMaster statusNodeMaster) {
		this.statusNodeMaster = statusNodeMaster;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<SecondaryStatus> getChildren() {
		return children;
	}

	public void setChildren(List<SecondaryStatus> children) {
		this.children = children;
	}

	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}

	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}

	public Integer getSecondaryStatusId() {
		return secondaryStatusId;
	}

	public void setSecondaryStatusId(Integer secondaryStatusId) {
		this.secondaryStatusId = secondaryStatusId;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}

	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}

	public String getSecondaryStatusName() {
		return secondaryStatusName;
	}

	public void setSecondaryStatusName(String secondaryStatusName) {
		this.secondaryStatusName = secondaryStatusName;
	}

	public StatusMaster getStatusMaster() {
		return statusMaster;
	}

	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public UserMaster getUsermaster() {
		return usermaster;
	}

	public void setUsermaster(UserMaster usermaster) {
		this.usermaster = usermaster;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	public SecondaryStatus getSecondaryStatus_copy() {
		return secondaryStatus_copy;
	}

	public void setSecondaryStatus_copy(SecondaryStatus secondaryStatusCopy) {
		secondaryStatus_copy = secondaryStatusCopy;
	}
	
	public String getSlcName() {
		return slcName;
	}
	public void setSlcName(String slcName) {
		this.slcName = slcName;
	}
	
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof SecondaryStatus))return false;
	    SecondaryStatus secondaryStatus = (SecondaryStatus)object;
	  
	    if(this.secondaryStatusId.equals(secondaryStatus.getSecondaryStatusId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+secondaryStatusId);
	}

	public static Comparator<SecondaryStatus> secondaryStatusOrder  = new Comparator<SecondaryStatus>(){
		public int compare(SecondaryStatus od1, SecondaryStatus od2) {			
			return od1.getOrderNumber().compareTo(od2.getOrderNumber());
		}		
	};

	@Override
	public String toString() {
		return secondaryStatusName;
	}
	
	
	
	
}
