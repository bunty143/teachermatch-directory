package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="referencequestionsetquestions")
public class ReferenceQuestionSetQuestions implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3761999422047260647L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer ID;
	
	@ManyToOne
	@JoinColumn(name="QuestionSetID",referencedColumnName="ID")
	private ReferenceQuestionSets questionSets;
	
	@ManyToOne
	@JoinColumn(name="QuestionID",referencedColumnName="questionId")
	private DistrictSpecificRefChkQuestions districtSpecificRefChkQuestions;
	
	private Integer QuestionSequence;
	
	public Integer getQuestionSequence() {
		return QuestionSequence;
	}

	public void setQuestionSequence(Integer questionSequence) {
		QuestionSequence = questionSequence;
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public ReferenceQuestionSets getQuestionSets() {
		return questionSets;
	}

	public void setQuestionSets(ReferenceQuestionSets questionSets) {
		this.questionSets = questionSets;
	}

	public DistrictSpecificRefChkQuestions getDistrictSpecificRefChkQuestions() {
		return districtSpecificRefChkQuestions;
	}

	public void setDistrictSpecificRefChkQuestions(
			DistrictSpecificRefChkQuestions districtSpecificRefChkQuestions) {
		this.districtSpecificRefChkQuestions = districtSpecificRefChkQuestions;
	} 
	
}
