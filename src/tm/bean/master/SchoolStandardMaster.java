package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="schoolstandardmaster")
public class SchoolStandardMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5927586560656340258L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer schoolStandardId;     
	private String schoolStandardName;     
	private String status;
	public Integer getSchoolStandardId() {
		return schoolStandardId;
	}
	public void setSchoolStandardId(Integer schoolStandardId) {
		this.schoolStandardId = schoolStandardId;
	}
	public String getSchoolStandardName() {
		return schoolStandardName;
	}
	public void setSchoolStandardName(String schoolStandardName) {
		this.schoolStandardName = schoolStandardName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
