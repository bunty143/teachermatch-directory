package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="contacttypemaster")
public class ContactTypeMaster implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2720777816196999158L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer contactTypeId;
	private String contactType;
	private String status;

	public Integer getContactTypeId()
	{
		return contactTypeId;
	}
	public void setContactTypeId(Integer contactTypeId) 
	{
		this.contactTypeId = contactTypeId;
	}
	public String getContactType() 
	{
		return contactType;
	}
	public void setContactType(String contactType) 
	{
		this.contactType = contactType;
	}
	public String getStatus() 
	{
		return status;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}
}
