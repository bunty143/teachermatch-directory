package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="universitymaster")
public class UniversityMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2343685775887191708L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
 	private Integer universityId; 	
	private String universityName;
	private String collegeCode;
	private String iheCode;
	private Integer stateId;
	private Integer  cityId;
	private Integer countryId;
	 
	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	public Integer getCountryId() {
		return countryId;
	}
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	public String getIheCode() {
		return iheCode;
	}
	public void setIheCode(String iheCode) {
		this.iheCode = iheCode;
	}

	private String status;
	public Integer getUniversityId() {
		return universityId;
	}
	public void setUniversityId(Integer universityId) {
		this.universityId = universityId;
	}
	public String getUniversityName() {
		return universityName;
	}
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
	public String getCollegeCode() {
		return collegeCode;
	}
	public void setCollegeCode(String collegeCode) {
		this.collegeCode = collegeCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof UniversityMaster))return false;
	    UniversityMaster universityMaster = (UniversityMaster)object;
	  
	    if(this.universityId.equals(universityMaster.getUniversityId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+universityId);
	}
}	