package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Amit
 * @date 20-Apr-15
 */
@Entity
@Table(name="sdpcodeswithcertificates")
public class SdpCodesWithCertificates implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9167174662557728859L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer sdpCodeCertId;
	private String sdpCode;
	private String sdpCodeTitle;
	
	@ManyToOne
	@JoinColumn(name="stateId", referencedColumnName="stateId")
	private StateMaster stateMaster;
	
	@ManyToOne
	@JoinColumn(name="certTypeId", referencedColumnName="certTypeId")
	private CertificateTypeMaster certificateTypeMaster;
	
	private String status;
	
	public Integer getSdpCodeCertId() {
		return sdpCodeCertId;
	}
	public void setSdpCodeCertId(Integer sdpCodeCertId) {
		this.sdpCodeCertId = sdpCodeCertId;
	}
	public String getSdpCode() {
		return sdpCode;
	}
	public void setSdpCode(String sdpCode) {
		this.sdpCode = sdpCode;
	}
	public String getSdpCodeTitle() {
		return sdpCodeTitle;
	}
	public void setSdpCodeTitle(String sdpCodeTitle) {
		this.sdpCodeTitle = sdpCodeTitle;
	}
	public StateMaster getStateMaster() {
		return stateMaster;
	}
	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}
	public CertificateTypeMaster getCertificateTypeMaster() {
		return certificateTypeMaster;
	}
	public void setCertificateTypeMaster(CertificateTypeMaster certificateTypeMaster) {
		this.certificateTypeMaster = certificateTypeMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}