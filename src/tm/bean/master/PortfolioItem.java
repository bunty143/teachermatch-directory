package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="portfolioitems")
public class PortfolioItem implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7493947384362830226L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Integer portfolioItemId;
	String portfolioItemName;
	Integer quantityRequired;
	String status;
	
	public Integer getPortfolioItemId() {
		return portfolioItemId;
	}
	public void setPortfolioItemId(Integer portfolioItemId) {
		this.portfolioItemId = portfolioItemId;
	}
	public String getPortfolioItemName() {
		return portfolioItemName;
	}
	public void setPortfolioItemName(String portfolioItemName) {
		this.portfolioItemName = portfolioItemName;
	}
	public Integer getQuantityRequired() {
		return quantityRequired;
	}
	public void setQuantityRequired(Integer quantityRequired) {
		this.quantityRequired = quantityRequired;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
