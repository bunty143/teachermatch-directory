package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="certificatetypemaster")
public class CertificateTypeMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 717256916412870291L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)	
	private Integer certTypeId;   
	@ManyToOne
	@JoinColumn(name="stateId" , referencedColumnName="stateId")
	private StateMaster stateId;     
	private String certType;
	private String certTypeCode;
	private String status;
	public Integer getCertTypeId() {
		return certTypeId;
	}
	public void setCertTypeId(Integer certTypeId) {
		this.certTypeId = certTypeId;
	}
	public StateMaster getStateId() {
		return stateId;
	}
	public void setStateId(StateMaster stateId) {
		this.stateId = stateId;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = certType;
	}
	public String getCertTypeCode() {
		return certTypeCode;
	}
	public void setCertTypeCode(String certTypeCode) {
		this.certTypeCode = certTypeCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
