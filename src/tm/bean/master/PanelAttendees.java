package tm.bean.master;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobForTeacher;
import tm.bean.user.UserMaster;

@Entity
@Table(name="panelattendees")
public class PanelAttendees implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4062644222341893134L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer panelattendeeId;
	
	@ManyToOne
	@JoinColumn(name="panelId",referencedColumnName="panelId")
	private PanelSchedule panelSchedule;
	
	@ManyToOne
	@JoinColumn(name="panelInviteeId",referencedColumnName="userId")
	private UserMaster panelInviteeId;
	
	@ManyToOne
	@JoinColumn(name="hostId",referencedColumnName="userId")
	private UserMaster hostId;
	
	private String panelStatus;
	private Date panelDate;
	public Integer getPanelattendeeId() {
		return panelattendeeId;
	}
	public void setPanelattendeeId(Integer panelattendeeId) {
		this.panelattendeeId = panelattendeeId;
	}
	
	public UserMaster getPanelInviteeId() {
		return panelInviteeId;
	}
	public void setPanelInviteeId(UserMaster panelInviteeId) {
		this.panelInviteeId = panelInviteeId;
	}
	public UserMaster getHostId() {
		return hostId;
	}
	public void setHostId(UserMaster hostId) {
		this.hostId = hostId;
	}
	public String getPanelStatus() {
		return panelStatus;
	}
	public void setPanelStatus(String panelStatus) {
		this.panelStatus = panelStatus;
	}
	public Date getPanelDate() {
		return panelDate;
	}
	public void setPanelDate(Date panelDate) {
		this.panelDate = panelDate;
	}
	public PanelSchedule getPanelSchedule() {
		return panelSchedule;
	}
	public void setPanelSchedule(PanelSchedule panelSchedule) {
		this.panelSchedule = panelSchedule;
	}
	
	public static Comparator<PanelAttendees> panelAttendeesUserNameAsc  = new Comparator<PanelAttendees>(){
		public int compare(PanelAttendees panelAttendees1, PanelAttendees panelAttendees2) {
			
			String sFirstName1 = panelAttendees1.getPanelInviteeId().getFirstName().toUpperCase();
		    String sLastName1 = panelAttendees1.getPanelInviteeId().getLastName().toUpperCase();
		    String sName1="";
		    if(sFirstName1!=null && !sFirstName1.equals(""))
		    	sName1=sFirstName1.trim();
		    if(sLastName1!=null && !sLastName1.equals(""))
		    	sName1=sName1+" "+sLastName1.trim();
		    sName1.trim();
		    
		    String sFirstName2 = panelAttendees2.getPanelInviteeId().getFirstName().toUpperCase();
		    String sLastName2 = panelAttendees2.getPanelInviteeId().getLastName().toUpperCase();
		    String sName2="";
		    if(sFirstName2!=null && !sFirstName2.equals(""))
		    	sName2=sFirstName2.trim();
		    if(sLastName2!=null && !sLastName2.equals(""))
		    	sName2=sName2+" "+sLastName2.trim();
		    sName2.trim();
		    
		    //ascending order
		      return sName1.compareTo(sName2);
		      
		    //descending  order
		      //return sName2.compareTo(sName1);
			
		}		
	};
	
	
	
}
