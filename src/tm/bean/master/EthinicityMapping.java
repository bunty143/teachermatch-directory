package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ethinicitymapping")
public class EthinicityMapping implements Serializable
{
	private static final long serialVersionUID = 5479140107976941808L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer ethinicitymappingId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private String mappingName;
	
	@ManyToOne
	@JoinColumn(name="ethinicityId",referencedColumnName="ethnicityId")
	private EthinicityMaster ethinicityMaster;

	public Integer getEthinicitymappingId() {
		return ethinicitymappingId;
	}

	public void setEthinicitymappingId(Integer ethinicitymappingId) {
		this.ethinicitymappingId = ethinicitymappingId;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public String getMappingName() {
		return mappingName;
	}

	public void setMappingName(String mappingName) {
		this.mappingName = mappingName;
	}

	public EthinicityMaster getEthinicityMaster() {
		return ethinicityMaster;
	}

	public void setEthinicityMaster(EthinicityMaster ethinicityMaster) {
		this.ethinicityMaster = ethinicityMaster;
	}
	
}
