package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="mastersubjectmaster")
public class MasterSubjectMaster implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8216559548118893648L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer masterSubjectId;
	
	private String masterSubjectName;
	private String status;
	public Integer getMasterSubjectId() {
		return masterSubjectId;
	}
	public void setMasterSubjectId(Integer masterSubjectId) {
		this.masterSubjectId = masterSubjectId;
	}
	public String getMasterSubjectName() {
		return masterSubjectName;
	}
	public void setMasterSubjectName(String masterSubjectName) {
		this.masterSubjectName = masterSubjectName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
}
