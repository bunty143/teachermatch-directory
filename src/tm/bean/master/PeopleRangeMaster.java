package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="peoplerangemaster")
public class PeopleRangeMaster implements Serializable
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7004626742960939418L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer rangeId;     
	private String range;
	public Integer getRangeId() {
		return rangeId;
	}
	public void setRangeId(Integer rangeId) {
		this.rangeId = rangeId;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
}
