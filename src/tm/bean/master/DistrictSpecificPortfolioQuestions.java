package tm.bean.master;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.JobOrder;
import tm.bean.user.UserMaster;

@Entity
@Table(name="districtspecificportfolioquestions")
public class DistrictSpecificPortfolioQuestions implements Serializable
{
	
	private static final long serialVersionUID = 5726209663702851045L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer questionId; 	
	
	/*@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;*/
	private Integer headQuarterId; 
	
	/*@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private  BranchMaster branchMaster;*/
	private Integer branchId; 
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster  jobCategoryMaster;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="groupId",referencedColumnName="groupId")
	private DspqGroupMaster dspqGroupMaster;
	
	@ManyToOne
	@JoinColumn(name="sectionId",referencedColumnName="sectionId")
	private DspqSectionMaster dspqSectionMaster;
	
	@ManyToOne
	@JoinColumn(name="dspqPortfolioNameId",referencedColumnName="dspqPortfolioNameId")
	private DspqPortfolioName dspqPortfolioName;
	
	private String question;
	private String questionExplanation;
	private String tooltip;
	private String fieldDisplayLabel;
	private String applicantType;
	private Integer parentQuestionId;	
	private Integer questionOrder;
	public Integer getParentQuestionId() {
		return parentQuestionId;
	}

	public void setParentQuestionId(Integer parentQuestionId) {
		this.parentQuestionId = parentQuestionId;
	}

	public String getApplicantType() {
		return applicantType;
	}

	public void setApplicantType(String applicantType) {
		this.applicantType = applicantType;
	}

	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster  questionTypeMaster;
	
	private String questionInstructions;
	private int isRequired;
	private Double maxMarks;
	private Integer maxSliderScore;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;	
	
	
	private String status;	
	private Boolean isCustom;


	public DspqPortfolioName getDspqPortfolioName() {
		return dspqPortfolioName;
	}

	public void setDspqPortfolioName(DspqPortfolioName dspqPortfolioName) {
		this.dspqPortfolioName = dspqPortfolioName;
	}

	public Boolean getIsCustom() {
		return isCustom;
	}
	
	public void setIsCustom(Boolean isCustom) {
		this.isCustom = isCustom;
	}

	private Date createdDateTime;
	private boolean provideTag;
	@ManyToOne
	@JoinColumn(name="secStatusId",referencedColumnName="secStatusId")
	private SecondaryStatusMaster secondaryStatusMaster;
	
	private boolean isDisabledPrincipal; 
	
	
	public int getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(int isRequired) {
		this.isRequired = isRequired;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public DspqGroupMaster getDspqGroupMaster() {
		return dspqGroupMaster;
	}

	public void setDspqGroupMaster(DspqGroupMaster dspqGroupMaster) {
		this.dspqGroupMaster = dspqGroupMaster;
	}

	public DspqSectionMaster getDspqSectionMaster() {
		return dspqSectionMaster;
	}

	public void setDspqSectionMaster(DspqSectionMaster dspqSectionMaster) {
		this.dspqSectionMaster = dspqSectionMaster;
	}

	public Double getMaxMarks() {
		return maxMarks;
	}

	public void setMaxMarks(Double maxMarks) {
		this.maxMarks = maxMarks;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="questionId",referencedColumnName="questionId",insertable=false,updatable=false)
	private List<DistrictSpecificPortfolioOptions> questionOptions;
	

	public List<DistrictSpecificPortfolioOptions> getQuestionOptions() {
		return questionOptions;
	}

	public void setQuestionOptions(
			List<DistrictSpecificPortfolioOptions> questionOptions) {
		this.questionOptions = questionOptions;
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}

	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}
	
	public String getQuestionExplanation() {
		return questionExplanation;
	}

	public void setQuestionExplanation(String questionExplanation) {
		this.questionExplanation = questionExplanation;
	}

	public String getTooltip() {
		return tooltip;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}

	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}

	public String getQuestionInstructions() {
		return questionInstructions;
	}

	public void setQuestionInstructions(String questionInstructions) {
		this.questionInstructions = questionInstructions;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getMaxSliderScore() {
		return maxSliderScore;
	}

	public void setMaxSliderScore(Integer maxSliderScore) {
		this.maxSliderScore = maxSliderScore;
	}

	public Boolean getProvideTag() {
		return provideTag;
	}

	public void setProvideTag(Boolean provideTag) {
		this.provideTag = provideTag;
	}

	public SecondaryStatusMaster getSecondaryStatusMaster() {
		return secondaryStatusMaster;
	}

	public void setSecondaryStatusMaster(SecondaryStatusMaster secondaryStatusMaster) {
		this.secondaryStatusMaster = secondaryStatusMaster;
	}

	public boolean isDisabledPrincipal() {
		return isDisabledPrincipal;
	}

	public void setDisabledPrincipal(boolean isDisabledPrincipal) {
		this.isDisabledPrincipal = isDisabledPrincipal;
	}
	
	public String getFieldDisplayLabel() {
		return fieldDisplayLabel;
	}

	public void setFieldDisplayLabel(String fieldDisplayLabel) {
		this.fieldDisplayLabel = fieldDisplayLabel;
	}

	public Integer getHeadQuarterId() {
		return headQuarterId;
	}

	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getQuestionOrder() {
		return questionOrder;
	}

	public void setQuestionOrder(Integer questionOrder) {
		this.questionOrder = questionOrder;
	}

	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof DistrictSpecificPortfolioQuestions))return false;
	    DistrictSpecificPortfolioQuestions jobOrder = (DistrictSpecificPortfolioQuestions)object;
	  
	    if(this.questionId.equals(jobOrder.getQuestionId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+questionId);
	}
	
}
