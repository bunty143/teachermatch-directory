package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="fieldmaster")
public class FieldMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3046653057106779119L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer fieldId;     
	private String fieldName;     
	private String status;
	public Integer getFieldId() {
		return fieldId;
	}
	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}	
}
