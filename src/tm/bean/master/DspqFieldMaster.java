package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import java.util.Date;

@Entity
@Table(name="dspqfieldmaster",uniqueConstraints = { @UniqueConstraint( columnNames = { "sectionId","dspqFieldName","status","isRequired" } ) } )
public class DspqFieldMaster implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3204846505083588013L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer dspqFieldId;
	
	@ManyToOne
	@JoinColumn(name="sectionId",referencedColumnName="sectionId")
	private DspqSectionMaster dspqSectionMaster;
	
	private String dspqFieldName;
	private Integer isRequired;
	private Integer numberRequired;
	private Boolean isAdditionalField;
	private String isDependantField;
	private String fieldDataType;
	private Integer fieldByOrder;
	private String status;
	private Date createdDateTime;
	public Integer getDspqFieldId() {
		return dspqFieldId;
	}
	public void setDspqFieldId(Integer dspqFieldId) {
		this.dspqFieldId = dspqFieldId;
	}
	public DspqSectionMaster getDspqSectionMaster() {
		return dspqSectionMaster;
	}
	public void setDspqSectionMaster(DspqSectionMaster dspqSectionMaster) {
		this.dspqSectionMaster = dspqSectionMaster;
	}
	public String getDspqFieldName() {
		return dspqFieldName;
	}
	public void setDspqFieldName(String dspqFieldName) {
		this.dspqFieldName = dspqFieldName;
	}
	public Integer getIsRequired() {
		return isRequired;
	}
	public void setIsRequired(Integer isRequired) {
		this.isRequired = isRequired;
	}
	public Integer getNumberRequired() {
		return numberRequired;
	}
	public void setNumberRequired(Integer numberRequired) {
		this.numberRequired = numberRequired;
	}
	public Boolean getIsAdditionalField() {
		return isAdditionalField;
	}
	public void setIsAdditionalField(Boolean isAdditionalField) {
		this.isAdditionalField = isAdditionalField;
	}	
	public String getIsDependantField() {
		return isDependantField;
	}
	public void setIsDependantField(String isDependantField) {
		this.isDependantField = isDependantField;
	}
	public String getFieldDataType() {
		return fieldDataType;
	}
	public void setFieldDataType(String fieldDataType) {
		this.fieldDataType = fieldDataType;
	}	
	public Integer getFieldByOrder() {
		return fieldByOrder;
	}
	public void setFieldByOrder(Integer fieldByOrder) {
		this.fieldByOrder = fieldByOrder;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
}
