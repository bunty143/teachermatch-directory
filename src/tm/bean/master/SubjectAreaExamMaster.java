package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="subjectareaexammaster")
public class SubjectAreaExamMaster implements Serializable {

	private static final long serialVersionUID = -3958852606449375040L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer subjectAreaExamId;
	
	private String subjectAreaExamCode;
	private String subjectAreaExamName;
	private String status;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private Date createdDateTime;

	public Integer getSubjectAreaExamId() {
		return subjectAreaExamId;
	}

	public void setSubjectAreaExamId(Integer subjectAreaExamId) {
		this.subjectAreaExamId = subjectAreaExamId;
	}

	public String getSubjectAreaExamCode() {
		return subjectAreaExamCode;
	}

	public void setSubjectAreaExamCode(String subjectAreaExamCode) {
		this.subjectAreaExamCode = subjectAreaExamCode;
	}

	public String getSubjectAreaExamName() {
		return subjectAreaExamName;
	}

	public void setSubjectAreaExamName(String subjectAreaExamName) {
		this.subjectAreaExamName = subjectAreaExamName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	

}
