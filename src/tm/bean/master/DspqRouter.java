package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="dspqrouter")
public class DspqRouter implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6631718743665590819L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer dspqRouterId;
	
	@ManyToOne
	@JoinColumn(name="sectionId",referencedColumnName="sectionId")
	private DspqSectionMaster dspqSectionMaster;
	
	@ManyToOne
	@JoinColumn(name="dspqFieldId",referencedColumnName="dspqFieldId")
	private DspqFieldMaster dspqFieldMaster;
	
	@ManyToOne
	@JoinColumn(name="dspqPortfolioNameId",referencedColumnName="dspqPortfolioNameId")
	private DspqPortfolioName dspqPortfolioName;
	
	private String displayName;
	private Boolean isRequired;
	private String applicantType;
	private Integer numberRequired;
	private String instructions;
	private String tooltip;

	private String status;
	private String applicantTypeStatus;
	private Integer sectionOrder;
	private String othersAttribute;

	private Date createdDateTime;
	public Integer getDspqRouterId() {
		return dspqRouterId;
	}
	public void setDspqRouterId(Integer dspqRouterId) {
		this.dspqRouterId = dspqRouterId;
	}
	public DspqSectionMaster getDspqSectionMaster() {
		return dspqSectionMaster;
	}
	public void setDspqSectionMaster(DspqSectionMaster dspqSectionMaster) {
		this.dspqSectionMaster = dspqSectionMaster;
	}
	public DspqFieldMaster getDspqFieldMaster() {
		return dspqFieldMaster;
	}
	public void setDspqFieldMaster(DspqFieldMaster dspqFieldMaster) {
		this.dspqFieldMaster = dspqFieldMaster;
	}
	public DspqPortfolioName getDspqPortfolioName() {
		return dspqPortfolioName;
	}
	public void setDspqPortfolioName(DspqPortfolioName dspqPortfolioName) {
		this.dspqPortfolioName = dspqPortfolioName;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public Boolean getIsRequired() {
		return isRequired;
	}
	public void setIsRequired(Boolean isRequired) {
		this.isRequired = isRequired;
	}
	public String getApplicantType() {
		return applicantType;
	}
	public void setApplicantType(String applicantType) {
		this.applicantType = applicantType;
	}
	public Integer getNumberRequired() {
		return numberRequired;
	}
	public void setNumberRequired(Integer numberRequired) {
		this.numberRequired = numberRequired;
	}
	public String getInstructions() {
		return instructions;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	public String getTooltip() {
		return tooltip;
	}
	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getApplicantTypeStatus() {
		return applicantTypeStatus;
	}
	public void setApplicantTypeStatus(String applicantTypeStatus) {
		this.applicantTypeStatus = applicantTypeStatus;
	}	
	public String getOthersAttribute() {
		return othersAttribute;
	}
	public void setOthersAttribute(String othersAttribute) {
		this.othersAttribute = othersAttribute;
	}
	public Integer getSectionOrder() {
		return sectionOrder;
	}
	public void setSectionOrder(Integer sectionOrder) {
		this.sectionOrder = sectionOrder;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return dspqRouterId+" : "+displayName+" : "+applicantType +" : "+isRequired+" : "+dspqFieldMaster.getDspqFieldId();
	}
	
}
