package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;

@Entity
@Table(name="democlassschedule")
public class DemoClassSchedule implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4842781032928730188L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer demoId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtId;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolId;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherId;
	
	@ManyToOne
	@JoinColumn(name="timeZoneId",referencedColumnName="timeZoneId")
	private TimeZoneMaster timeZoneId;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBy;	
	
	
	private Date demoDate;
	private Date demoStandardTime; 
	private String demoTime;
	private String timeFormat;
	private String demoClassAddress;
	private String demoDescription;
	private String demoStatus;
	private Date createdDateTime;
	
	
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public Integer getDemoId() {
		return demoId;
	}
	public void setDemoId(Integer demoId) {
		this.demoId = demoId;
	}
	public DistrictMaster getDistrictId() {
		return districtId;
	}
	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}
	public SchoolMaster getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(SchoolMaster schoolId) {
		this.schoolId = schoolId;
	}
	public TeacherDetail getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}
	public TimeZoneMaster getTimeZoneId() {
		return timeZoneId;
	}
	public void setTimeZoneId(TimeZoneMaster timeZoneId) {
		this.timeZoneId = timeZoneId;
	}
	public Date getDemoDate() {
		return demoDate;
	}
	public void setDemoDate(Date demoDate) {
		this.demoDate = demoDate;
	}
	public String getDemoTime() {
		return demoTime;
	}
	public void setDemoTime(String demoTime) {
		this.demoTime = demoTime;
	}
	public String getTimeFormat() {
		return timeFormat;
	}
	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}
	public String getDemoClassAddress() {
		return demoClassAddress;
	}
	public void setDemoClassAddress(String demoClassAddress) {
		this.demoClassAddress = demoClassAddress;
	}
	public String getDemoDescription() {
		return demoDescription;
	}
	public void setDemoDescription(String demoDescription) {
		this.demoDescription = demoDescription;
	}
	public String getDemoStatus() {
		return demoStatus;
	}
	public void setDemoStatus(String demoStatus) {
		this.demoStatus = demoStatus;
	}
	
	public UserMaster getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(UserMaster createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Date getDemoStandardTime() {
		return demoStandardTime;
	}
	public void setDemoStandardTime(Date demoStandardTime) {
		this.demoStandardTime = demoStandardTime;
	}
	
}
