package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="dspqportfolioname")
public class DspqPortfolioName implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -562264989623776075L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer dspqPortfolioNameId;	
	private String portfolioName;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	
	private DistrictMaster districtMaster;		
	private String status;
	private Date createdDateTime;
	public Integer getDspqPortfolioNameId() {
		return dspqPortfolioNameId;
	}
	public void setDspqPortfolioNameId(Integer dspqPortfolioNameId) {
		this.dspqPortfolioNameId = dspqPortfolioNameId;
	}
	public String getPortfolioName() {
		return portfolioName;
	}
	public void setPortfolioName(String portfolioName) {
		this.portfolioName = portfolioName;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
}
