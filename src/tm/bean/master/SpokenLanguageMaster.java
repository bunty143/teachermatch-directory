package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="spokenlanguagemaster")
public class SpokenLanguageMaster implements Serializable
{
	private static final long serialVersionUID = -8164242257365063565L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer spokenlanguagemasterid;
	
	private String languageName;
	private String status;
	
	public Integer getSpokenlanguagemasterid() {
		return spokenlanguagemasterid;
	}
	public void setSpokenlanguagemasterid(Integer spokenlanguagemasterid) {
		this.spokenlanguagemasterid = spokenlanguagemasterid;
	}
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
