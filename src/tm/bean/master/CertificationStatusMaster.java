package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="certificationstatusmaster")
public class CertificationStatusMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4784595831821293693L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer certificationStatusId;     
	private String certificationStatusName;     
	private String status;
	
	
	public Integer getCertificationStatusId() {
		return certificationStatusId;
	}
	public void setCertificationStatusId(Integer certificationStatusId) {
		this.certificationStatusId = certificationStatusId;
	}
	public String getCertificationStatusName() {
		return certificationStatusName;
	}
	public void setCertificationStatusName(String certificationStatusName) {
		this.certificationStatusName = certificationStatusName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
