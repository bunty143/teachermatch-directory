package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name="optionsfordistrictspecificquestions")
public class OptionsForDistrictSpecificQuestions implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3975888822359036315L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer optionId; 	
	
	@ManyToOne
	@JoinColumn(name="questionId",referencedColumnName="questionId")
	private DistrictSpecificQuestions districtSpecificQuestions;
	
	private String questionOption;
	private Boolean validOption;

	private Boolean requiredExplanation;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private Date createdDateTime;
	
	private String status;
	
	public Integer getOptionId() {
		return optionId;
	}

	public void setOptionId(Integer optionId) {
		this.optionId = optionId;
	}

	public DistrictSpecificQuestions getDistrictSpecificQuestions() {
		return districtSpecificQuestions;
	}

	public void setDistrictSpecificQuestions(
			DistrictSpecificQuestions districtSpecificQuestions) {
		this.districtSpecificQuestions = districtSpecificQuestions;
	}

	public String getQuestionOption() {
		return questionOption;
	}

	public void setQuestionOption(String questionOption) {
		this.questionOption = questionOption;
	}

	public Boolean getValidOption() {
		return validOption;
	}

	public Boolean getRequiredExplanation() {
		return requiredExplanation;
	}

	public void setRequiredExplanation(Boolean requiredExplanation) {
		this.requiredExplanation = requiredExplanation;
	}

	public void setValidOption(Boolean validOption) {
		this.validOption = validOption;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
}
