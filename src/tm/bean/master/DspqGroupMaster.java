package tm.bean.master;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="dspqgroupmaster",uniqueConstraints = { @UniqueConstraint( columnNames = { "groupName"} ) } )
public class DspqGroupMaster implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3043546753488590423L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer groupId;
	private String groupName;
	private String status;
	private Date createdDateTime;
	
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
}
