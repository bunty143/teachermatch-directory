package tm.bean.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tfaaffiliatemaster")
public class TFAAffiliateMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8979896821996930966L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer tfaAffiliateId;
	private String tfaAffiliateName;	
	private String tfaShortAffiliateName;
	private String status; 
	
	public String getTfaShortAffiliateName() {
		return tfaShortAffiliateName;
	}
	public void setTfaShortAffiliateName(String tfaShortAffiliateName) {
		this.tfaShortAffiliateName = tfaShortAffiliateName;
	}
	public Integer getTfaAffiliateId() {
		return tfaAffiliateId;
	}
	public void setTfaAffiliateId(Integer tfaAffiliateId) {
		this.tfaAffiliateId = tfaAffiliateId;
	}
	public String getTfaAffiliateName() {
		return tfaAffiliateName;
	}
	public void setTfaAffiliateName(String tfaAffiliateName) {
		this.tfaAffiliateName = tfaAffiliateName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
