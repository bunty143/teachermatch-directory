package tm.bean.master;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="userfolderstructure")
public class UserFolderStructure implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6526557230459831587L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer folderId;

	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster usermaster;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;

	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	private String folderName;//parentFolderId
	@ManyToOne
	@JoinColumn(name="parentFolderId",referencedColumnName="folderId")
	private UserFolderStructure userFolderStructure;
	private Integer orderNumber;
	private Integer isDefault;
	
	@OneToMany()
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="parentFolderId",referencedColumnName="folderId",insertable=false,updatable=false)
	private List<UserFolderStructure> children;
	
	public Integer getFolderId() {
		return folderId;
	}
	public void setFolderId(Integer folderId) {
		this.folderId = folderId;
	}
	public UserMaster getUsermaster() {
		return usermaster;
	}
	public void setUsermaster(UserMaster usermaster) {
		this.usermaster = usermaster;
	}
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public String getFolderName() {
		return folderName;
	}
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	public UserFolderStructure getUserFolderStructure() {
		return userFolderStructure;
	}
	public void setUserFolderStructure(UserFolderStructure userFolderStructure) {
		this.userFolderStructure = userFolderStructure;
	}
	public Integer getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Integer getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}
	public List<UserFolderStructure> getChildren() {
		return children;
	}
	public void setChildren(List<UserFolderStructure> children) {
		this.children = children;
	}
	
}
