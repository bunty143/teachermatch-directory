package tm.bean.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teacheranswerhistory")
public class TeacherAnswerHistory implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3551497648723002982L;	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer answerId; 	
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="questionId",referencedColumnName="questionId")
	private DistrictSpecificQuestions  districtSpecificQuestions;
	
	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster  questionTypeMaster;
	@ManyToOne
	@JoinColumn(name="teacherAssessmentQuestionId",referencedColumnName="teacherAssessmentQuestionId")
	private TeacherAssessmentQuestion teacherAssessmentQuestion;
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentDetail;
	@ManyToOne
	@JoinColumn(name="teacherAssessmentId",referencedColumnName="teacherAssessmentId")
	private TeacherAssessmentdetail teacherAssessmentdetail;
	
	private String selectedOptions;
	
	private String insertedText;
	private Integer typeId;
	private Boolean isValidAnswer;
	private Date createdDateTime;
	private String question;
	private String questionOption;
	private String questionType;
	private Boolean isActive;
	
	public Integer getAnswerId() {
		return answerId;
	}
	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public DistrictSpecificQuestions getDistrictSpecificQuestions() {
		return districtSpecificQuestions;
	}
	public void setDistrictSpecificQuestions(
			DistrictSpecificQuestions districtSpecificQuestions) {
		this.districtSpecificQuestions = districtSpecificQuestions;
	}
	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}
	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}
	public String getSelectedOptions() {
		return selectedOptions;
	}
	public void setSelectedOptions(String selectedOptions) {
		this.selectedOptions = selectedOptions;
	}
	public String getInsertedText() {
		return insertedText;
	}
	public void setInsertedText(String insertedText) {
		this.insertedText = insertedText;
	}
	public TeacherAssessmentQuestion getTeacherAssessmentQuestion() {
		return teacherAssessmentQuestion;
	}
	public void setTeacherAssessmentQuestion(
			TeacherAssessmentQuestion teacherAssessmentQuestion) {
		this.teacherAssessmentQuestion = teacherAssessmentQuestion;
	}
	public AssessmentDetail getAssessmentDetail() {
		return assessmentDetail;
	}
	public void setAssessmentDetail(AssessmentDetail assessmentDetail) {
		this.assessmentDetail = assessmentDetail;
	}
	public TeacherAssessmentdetail getTeacherAssessmentdetail() {
		return teacherAssessmentdetail;
	}
	public void setTeacherAssessmentdetail(
			TeacherAssessmentdetail teacherAssessmentdetail) {
		this.teacherAssessmentdetail = teacherAssessmentdetail;
	}
	public Integer getTypeId() {
		return typeId;
	}
	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	public Boolean getIsValidAnswer() {
		return isValidAnswer;
	}
	public void setIsValidAnswer(Boolean isValidAnswer) {
		this.isValidAnswer = isValidAnswer;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getQuestionOption() {
		return questionOption;
	}
	public void setQuestionOption(String questionOption) {
		this.questionOption = questionOption;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
}
