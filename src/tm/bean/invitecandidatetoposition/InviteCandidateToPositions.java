package tm.bean.invitecandidatetoposition;

/**********************************
 ***********************
 *  @Author Dhananjay Verma  *
**********************
********************************/

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="invitecandidatetoposition")
public class InviteCandidateToPositions implements Serializable {
		
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		@Column(name="inviteCandidateId")
		@GeneratedValue(strategy=GenerationType.AUTO)
		@Id
		private Integer inviteCandidateId;
		
		@ManyToOne(fetch = FetchType.LAZY)
		@JoinColumn(name="teacherId",referencedColumnName="teacherId")
		private TeacherDetail teacherId;
		
		@ManyToOne(fetch = FetchType.LAZY)
		@JoinColumn(name="districtId",referencedColumnName="districtId")
		private DistrictMaster districtId;
		
		@ManyToOne(fetch = FetchType.LAZY)
		@JoinColumn(name="jobId",referencedColumnName="jobId")
		private JobOrder jobId; 
		
		@Column(name="createdDateTime", columnDefinition="date and time when the record is created")
		private Date createdDateTime;
		
		@ManyToOne(fetch = FetchType.LAZY)
		@JoinColumn(name="createdBy",referencedColumnName="userId")
		private UserMaster createdBy;
		
		@Column(name="positionId")
		private int positionId;
		
		@Column(name="serviceProvider")
		private String serviceProvider;
		
		@Column(name="status")
		private String status;
		
		@Column(name="ipAddress")
		private String ipAddress;
		
		@Column(name="interviewId")
		private String interviewId;
		
		@Column(name="interviewCode")
		private String interviewCode;

		public String getInterviewId() {
			return interviewId;
		}

		public void setInterviewId(String interviewId) {
			this.interviewId = interviewId;
		}

		public String getInterviewCode() {
			return interviewCode;
		}

		public void setInterviewCode(String interviewCode) {
			this.interviewCode = interviewCode;
		}

		public void setInviteCandidateId(Integer inviteCandidateId) {
			this.inviteCandidateId = inviteCandidateId;
		}

		public Integer getInviteCandidateId() {
			return inviteCandidateId;
		}

		public void setTeacherId(TeacherDetail teacherId) {
			this.teacherId = teacherId;
		}

		public TeacherDetail getTeacherId() {
			return teacherId;
		}

		public void setDistrictId(DistrictMaster districtId) {
			this.districtId = districtId;
		}

		public DistrictMaster getDistrictId() {
			return districtId;
		}

		public void setJobId(JobOrder jobId) {
			this.jobId = jobId;
		}

		public JobOrder getJobId() {
			return jobId;
		}

		public void setCreatedDateTime(Date createdDateTime) {
			this.createdDateTime = createdDateTime;
		}

		public Date getCreatedDateTime() {
			return createdDateTime;
		}


		public void setPositionId(int positionId) {
			this.positionId = positionId;
		}

		public int getPositionId() {
			return positionId;
		}

		public void setServiceProvider(String serviceProvider) {
			this.serviceProvider = serviceProvider;
		}

		public String getServiceProvider() {
			return serviceProvider;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getStatus() {
			return status;
		}

		public void setIpAddress(String ipAddress) {
			this.ipAddress = ipAddress;
		}

		public String getIpAddress() {
			return ipAddress;
		}

		public void setCreatedBy(UserMaster createdBy) {
			this.createdBy = createdBy;
		}

		public UserMaster getCreatedBy() {
			return createdBy;
		}
}