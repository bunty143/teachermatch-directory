package tm.bean;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="districtdocumentupload")
public class DistrictDocumentUpload implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 95269824642418322L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer uploadId;
	
	@ManyToOne
	@JoinColumn(name="documentId",referencedColumnName="documentId")
	private DistrictSpecificDocuments districtSpecificDocuments;
	
	private String uploadedFileName;

	public Integer getUploadId() {
		return uploadId;
	}

	public void setUploadId(Integer uploadId) {
		this.uploadId = uploadId;
	}

	public DistrictSpecificDocuments getDistrictSpecificDocuments() {
		return districtSpecificDocuments;
	}

	public void setDistrictSpecificDocuments(
			DistrictSpecificDocuments districtSpecificDocuments) {
		this.districtSpecificDocuments = districtSpecificDocuments;
	}

	public String getUploadedFileName() {
		return uploadedFileName;
	}

	public void setUploadedFileName(String uploadedFileName) {
		this.uploadedFileName = uploadedFileName;
	}

	
}
