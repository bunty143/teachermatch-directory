package tm.bean;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;

@Entity
@Table(name="portfolioremindershistory")
public class PortfolioRemindersHistory implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5522338540006552018L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer portfolioremindersId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private Integer salutation;
	private Integer firstName;
	private Integer lastName;
	private Integer ethnicOriginId;
	private Integer ethnicityId;
	private Integer raceId;
	private Integer genderId;
	private Integer addressLine1;
	private Integer addressLine2;
	private Integer countryId;
	private Integer zipCode;
	private Integer stateId;
	private Integer cityId;
	private Integer phoneNumber;
	private Integer mobileNumber;
	private Integer SSN;
	private Integer dob;
	private Integer retirementnumber;
	private Integer retirementdate;
	private Integer veteran;
	private Integer academics;
	private Integer resume;
	private Integer honors;
	private Integer videoLink;
	private Integer coverLetter;
	private Integer academicTranscript;
	private Integer certification;
	private Integer proofOfCertification;
	private Integer referenceLettersOfRecommendation;
	private Integer tfaAffiliate;
	private Integer willingAsSubstituteTeacher;
	private Integer candidateType;
	private Integer expCertTeacherTraining;
	private Integer nationalBoardCert;
	private Integer affidavit;
	private Integer employment;
	private Integer formeremployee;
	private Integer generalKnowledgeExam;
	private Integer subjectAreaExam;
	private Integer involvement;
	private Date createdDateTime;
	
	private Integer highNeedSchool;
	
	private Integer certificationPortfolio;
	private Integer employmentHistory;
	private Integer additionalDocuments;
	private Integer referencesPortfolio;
	private Integer tfaAffiliatePortfolio;
	private Integer substituteTeacher;
	private Integer academicDspq;
	private Integer referenceDspq;
	
	public Integer getReferenceDspq() {
		return referenceDspq;
	}
	public void setReferenceDspq(Integer referenceDspq) {
		this.referenceDspq = referenceDspq;
	}
	
	public Integer getAcademicDspq() {
		return academicDspq;
	}
	public void setAcademicDspq(Integer academicDspq) {
		this.academicDspq = academicDspq;
	}
	
	
	public Integer getSubstituteTeacher() {
		return substituteTeacher;
	}
	public void setSubstituteTeacher(Integer substituteTeacher) {
		this.substituteTeacher = substituteTeacher;
	}
	
	
	public Integer getReferencesPortfolio() {
		return referencesPortfolio;
	}
	public void setReferencesPortfolio(Integer referencesPortfolio) {
		this.referencesPortfolio = referencesPortfolio;
	}
	public Integer getTfaAffiliatePortfolio() {
		return tfaAffiliatePortfolio;
	}
	public void setTfaAffiliatePortfolio(Integer tfaAffiliatePortfolio) {
		this.tfaAffiliatePortfolio = tfaAffiliatePortfolio;
	}
	public Integer getAdditionalDocuments() {
		return additionalDocuments;
	}
	public void setAdditionalDocuments(Integer additionalDocuments) {
		this.additionalDocuments = additionalDocuments;
	}
	
	public Integer getCertificationPortfolio() {
		return certificationPortfolio;
	}
	public void setCertificationPortfolio(Integer certificationPortfolio) {
		this.certificationPortfolio = certificationPortfolio;
	}
	public Integer getEmploymentHistory() {
		return employmentHistory;
	}
	public void setEmploymentHistory(Integer employmentHistory) {
		this.employmentHistory = employmentHistory;
	}
	public Integer getHighNeedSchool() {
		return highNeedSchool;
	}
	public void setHighNeedSchool(Integer highNeedSchool) {
		this.highNeedSchool = highNeedSchool;
	}
	public Integer getPortfolioremindersId() {
		return portfolioremindersId;
	}
	public void setPortfolioremindersId(Integer portfolioremindersId) {
		this.portfolioremindersId = portfolioremindersId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public Integer getSalutation() {
		return salutation;
	}
	public void setSalutation(Integer salutation) {
		this.salutation = salutation;
	}
	public Integer getFirstName() {
		return firstName;
	}
	public void setFirstName(Integer firstName) {
		this.firstName = firstName;
	}
	public Integer getLastName() {
		return lastName;
	}
	public void setLastName(Integer lastName) {
		this.lastName = lastName;
	}
	public Integer getEthnicOriginId() {
		return ethnicOriginId;
	}
	public void setEthnicOriginId(Integer ethnicOriginId) {
		this.ethnicOriginId = ethnicOriginId;
	}
	public Integer getEthnicityId() {
		return ethnicityId;
	}
	public void setEthnicityId(Integer ethnicityId) {
		this.ethnicityId = ethnicityId;
	}
	public Integer getRaceId() {
		return raceId;
	}
	public void setRaceId(Integer raceId) {
		this.raceId = raceId;
	}
	public Integer getGenderId() {
		return genderId;
	}
	public void setGenderId(Integer genderId) {
		this.genderId = genderId;
	}
	public Integer getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(Integer addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public Integer getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(Integer addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public Integer getCountryId() {
		return countryId;
	}
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	public Integer getZipCode() {
		return zipCode;
	}
	public void setZipCode(Integer zipCode) {
		this.zipCode = zipCode;
	}
	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	public Integer getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Integer getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(Integer mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public Integer getSSN() {
		return SSN;
	}
	public void setSSN(Integer sSN) {
		SSN = sSN;
	}
	public Integer getDob() {
		return dob;
	}
	public void setDob(Integer dob) {
		this.dob = dob;
	}
	public Integer getRetirementnumber() {
		return retirementnumber;
	}
	public void setRetirementnumber(Integer retirementnumber) {
		this.retirementnumber = retirementnumber;
	}
	public Integer getRetirementdate() {
		return retirementdate;
	}
	public void setRetirementdate(Integer retirementdate) {
		this.retirementdate = retirementdate;
	}
	public Integer getVeteran() {
		return veteran;
	}
	public void setVeteran(Integer veteran) {
		this.veteran = veteran;
	}
	public Integer getAcademics() {
		return academics;
	}
	public void setAcademics(Integer academics) {
		this.academics = academics;
	}
	public Integer getResume() {
		return resume;
	}
	public void setResume(Integer resume) {
		this.resume = resume;
	}
	public Integer getHonors() {
		return honors;
	}
	public void setHonors(Integer honors) {
		this.honors = honors;
	}
	public Integer getVideoLink() {
		return videoLink;
	}
	public void setVideoLink(Integer videoLink) {
		this.videoLink = videoLink;
	}
	public Integer getCoverLetter() {
		return coverLetter;
	}
	public void setCoverLetter(Integer coverLetter) {
		this.coverLetter = coverLetter;
	}
	public Integer getAcademicTranscript() {
		return academicTranscript;
	}
	public void setAcademicTranscript(Integer academicTranscript) {
		this.academicTranscript = academicTranscript;
	}
	public Integer getCertification() {
		return certification;
	}
	public void setCertification(Integer certification) {
		this.certification = certification;
	}
	public Integer getProofOfCertification() {
		return proofOfCertification;
	}
	public void setProofOfCertification(Integer proofOfCertification) {
		this.proofOfCertification = proofOfCertification;
	}
	public Integer getReferenceLettersOfRecommendation() {
		return referenceLettersOfRecommendation;
	}
	public void setReferenceLettersOfRecommendation(
			Integer referenceLettersOfRecommendation) {
		this.referenceLettersOfRecommendation = referenceLettersOfRecommendation;
	}
	public Integer getTfaAffiliate() {
		return tfaAffiliate;
	}
	public void setTfaAffiliate(Integer tfaAffiliate) {
		this.tfaAffiliate = tfaAffiliate;
	}
	public Integer getWillingAsSubstituteTeacher() {
		return willingAsSubstituteTeacher;
	}
	public void setWillingAsSubstituteTeacher(Integer willingAsSubstituteTeacher) {
		this.willingAsSubstituteTeacher = willingAsSubstituteTeacher;
	}
	public Integer getCandidateType() {
		return candidateType;
	}
	public void setCandidateType(Integer candidateType) {
		this.candidateType = candidateType;
	}
	public Integer getExpCertTeacherTraining() {
		return expCertTeacherTraining;
	}
	public void setExpCertTeacherTraining(Integer expCertTeacherTraining) {
		this.expCertTeacherTraining = expCertTeacherTraining;
	}
	public Integer getNationalBoardCert() {
		return nationalBoardCert;
	}
	public void setNationalBoardCert(Integer nationalBoardCert) {
		this.nationalBoardCert = nationalBoardCert;
	}
	public Integer getAffidavit() {
		return affidavit;
	}
	public void setAffidavit(Integer affidavit) {
		this.affidavit = affidavit;
	}
	public Integer getEmployment() {
		return employment;
	}
	public void setEmployment(Integer employment) {
		this.employment = employment;
	}
	public Integer getFormeremployee() {
		return formeremployee;
	}
	public void setFormeremployee(Integer formeremployee) {
		this.formeremployee = formeremployee;
	}
	public Integer getGeneralKnowledgeExam() {
		return generalKnowledgeExam;
	}
	public void setGeneralKnowledgeExam(Integer generalKnowledgeExam) {
		this.generalKnowledgeExam = generalKnowledgeExam;
	}
	public Integer getSubjectAreaExam() {
		return subjectAreaExam;
	}
	public void setSubjectAreaExam(Integer subjectAreaExam) {
		this.subjectAreaExam = subjectAreaExam;
	}
	public Integer getInvolvement() {
		return involvement;
	}
	public void setInvolvement(Integer involvement) {
		this.involvement = involvement;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
}
