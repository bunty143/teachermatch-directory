package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="questionuploadtemp ")
public class QuestionUploadTemp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6462410613459908850L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long questiontempid;
	private String domain_id;      
	private String competency_id;  
	private String objective_id;  
	private String question;
	private String question_weightage;
	private String max_marks;
	private String instructions;
	private String question_type;
	private String assessmentId;
	private String sectionId;
	private String option1;
	private String score1;
	private String rank1;
	private String option2;
	private String score2;
	private String rank2;
	private String option3;
	private String score3;
	private String rank3;
	private String option4;
	private String score4;
	private String rank4;
	private String option5;
	private String score5;
	private String rank5;
	private String option6;
	private String score6;
	private String rank6;
	private String itemCode;
	private String isExperimental;
	
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	private String errortext;
	private String sessionid;  
	private Date date;
	private String stage1;
	private String stage2;
	private String stage3;
	private String domain_name;
	private String competency_name;
	private String objective_name;
	private String stage1_name;
	private String stage2_name;
	private String stage3_name;
	private String question_type_name;
	
	public String getQuestion_type_name() {
		return question_type_name;
	}
	public void setQuestion_type_name(String questionTypeName) {
		question_type_name = questionTypeName;
	}
	public String getDomain_name() {
		return domain_name;
	}
	public void setDomain_name(String domainName) {
		domain_name = domainName;
	}
	public String getCompetency_name() {
		return competency_name;
	}
	public void setCompetency_name(String competencyName) {
		competency_name = competencyName;
	}
	public String getObjective_name() {
		return objective_name;
	}
	public void setObjective_name(String objectiveName) {
		objective_name = objectiveName;
	}
	public String getStage1_name() {
		return stage1_name;
	}
	public void setStage1_name(String stage1Name) {
		stage1_name = stage1Name;
	}
	public String getStage2_name() {
		return stage2_name;
	}
	public void setStage2_name(String stage2Name) {
		stage2_name = stage2Name;
	}
	public String getStage3_name() {
		return stage3_name;
	}
	public void setStage3_name(String stage3Name) {
		stage3_name = stage3Name;
	}
	public Long getQuestiontempid() {
		return questiontempid;
	}
	public void setQuestiontempid(Long questiontempid) {
		this.questiontempid = questiontempid;
	}
	
	public String getDomain_id() {
		return domain_id;
	}
	public void setDomain_id(String domainId) {
		domain_id = domainId;
	}
	public String getCompetency_id() {
		return competency_id;
	}
	public void setCompetency_id(String competencyId) {
		competency_id = competencyId;
	}
	public String getObjective_id() {
		return objective_id;
	}
	public void setObjective_id(String objectiveId) {
		objective_id = objectiveId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getQuestion_weightage() {
		return question_weightage;
	}
	public void setQuestion_weightage(String questionWeightage) {
		question_weightage = questionWeightage;
	}
	public String getMax_marks() {
		return max_marks;
	}
	public void setMax_marks(String maxMarks) {
		max_marks = maxMarks;
	}
	public String getInstructions() {
		return instructions;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	public String getQuestion_type() {
		return question_type;
	}
	public void setQuestion_type(String questionType) {
		question_type = questionType;
	}
	public String getAssessmentId() {
		return assessmentId;
	}
	public void setAssessmentId(String assessmentId) {
		this.assessmentId = assessmentId;
	}
	public String getSectionId() {
		return sectionId;
	}
	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}
	public String getOption1() {
		return option1;
	}
	public void setOption1(String option1) {
		this.option1 = option1;
	}
	public String getScore1() {
		return score1;
	}
	public void setScore1(String score1) {
		this.score1 = score1;
	}
	public String getRank1() {
		return rank1;
	}
	public void setRank1(String rank1) {
		this.rank1 = rank1;
	}
	public String getOption2() {
		return option2;
	}
	public void setOption2(String option2) {
		this.option2 = option2;
	}
	public String getScore2() {
		return score2;
	}
	public void setScore2(String score2) {
		this.score2 = score2;
	}
	public String getRank2() {
		return rank2;
	}
	public void setRank2(String rank2) {
		this.rank2 = rank2;
	}
	public String getOption3() {
		return option3;
	}
	public void setOption3(String option3) {
		this.option3 = option3;
	}
	public String getScore3() {
		return score3;
	}
	public void setScore3(String score3) {
		this.score3 = score3;
	}
	public String getRank3() {
		return rank3;
	}
	public void setRank3(String rank3) {
		this.rank3 = rank3;
	}
	public String getOption4() {
		return option4;
	}
	public void setOption4(String option4) {
		this.option4 = option4;
	}
	public String getScore4() {
		return score4;
	}
	public void setScore4(String score4) {
		this.score4 = score4;
	}
	public String getRank4() {
		return rank4;
	}
	public void setRank4(String rank4) {
		this.rank4 = rank4;
	}
	public String getOption5() {
		return option5;
	}
	public void setOption5(String option5) {
		this.option5 = option5;
	}
	public String getScore5() {
		return score5;
	}
	public void setScore5(String score5) {
		this.score5 = score5;
	}
	public String getRank5() {
		return rank5;
	}
	public void setRank5(String rank5) {
		this.rank5 = rank5;
	}
	public String getOption6() {
		return option6;
	}
	public void setOption6(String option6) {
		this.option6 = option6;
	}
	public String getScore6() {
		return score6;
	}
	public void setScore6(String score6) {
		this.score6 = score6;
	}
	public String getRank6() {
		return rank6;
	}
	public void setRank6(String rank6) {
		this.rank6 = rank6;
	}
	public String getErrortext() {
		return errortext;
	}
	public void setErrortext(String errortext) {
		this.errortext = errortext;
	}
	public String getSessionid() {
		return sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getStage1() {
		return stage1;
	}
	public void setStage1(String stage1) {
		this.stage1 = stage1;
	}
	public String getStage2() {
		return stage2;
	}
	public void setStage2(String stage2) {
		this.stage2 = stage2;
	}
	public String getStage3() {
		return stage3;
	}
	public void setStage3(String stage3) {
		this.stage3 = stage3;
	}
	public String getIsExperimental() {
		return isExperimental;
	}
	public void setIsExperimental(String isExperimental) {
		this.isExperimental = isExperimental;
	}
}
