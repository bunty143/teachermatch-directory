package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name="assessmentquestions")
public class AssessmentQuestions implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -716876125557107409L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer assessmentQuestionId;
	
	@ManyToOne
	@JoinColumn(name="questionId",referencedColumnName="questionId")
	private QuestionsPool questionsPool;
	
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentDetail;
	
	@ManyToOne
	@JoinColumn(name="sectionId",referencedColumnName="sectionId")
	private AssessmentSections assessmentSections;
	private Boolean IsMandatory;
	private Integer questionPosition;
	private Double questionWeightage;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	private String status;
	private Date createdDateTime;
	private Boolean isExperimental;
	
	public Integer getAssessmentQuestionId() {
		return assessmentQuestionId;
	}
	public void setAssessmentQuestionId(Integer assessmentQuestionId) {
		this.assessmentQuestionId = assessmentQuestionId;
	}
	public QuestionsPool getQuestionsPool() {
		return questionsPool;
	}
	public void setQuestionsPool(QuestionsPool questionsPool) {
		this.questionsPool = questionsPool;
	}
	public AssessmentDetail getAssessmentDetail() {
		return assessmentDetail;
	}
	
	public void setAssessmentDetail(AssessmentDetail assessmentDetail) {
		this.assessmentDetail = assessmentDetail;
	}
	
	public AssessmentSections getAssessmentSections() {
		return assessmentSections;
	}
	public void setAssessmentSections(AssessmentSections assessmentSections) {
		this.assessmentSections = assessmentSections;
	}
	public Boolean getIsMandatory() {
		return IsMandatory;
	}
	public void setIsMandatory(Boolean isMandatory) {
		IsMandatory = isMandatory;
	}
	public Integer getQuestionPosition() {
		return questionPosition;
	}
	public void setQuestionPosition(Integer questionPosition) {
		this.questionPosition = questionPosition;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Double getQuestionWeightage() {
		return questionWeightage;
	}
	public void setQuestionWeightage(Double questionWeightage) {
		this.questionWeightage = questionWeightage;
	}
	public Boolean getIsExperimental() {
		return isExperimental;
	}
	public void setIsExperimental(Boolean isExperimental) {
		this.isExperimental = isExperimental;
	}
}
