package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;
import tm.utility.Utility;

@Entity
@Table(name="assessmentsections")
public class AssessmentSections implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -3635260417897374665L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer sectionId;
	
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentDetail;
	private String sectionName;
	private String sectionDescription;
	private String sectionInstructions;
	private String sectionVideoUrl;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	//@Column(updatable=false)
	private String status;
	//@Column(updatable=false)
	private String ipaddress;
	
	@Column(updatable=false)
	private Date createdDateTime;
	public Integer getSectionId() {
		return sectionId;
	}
	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}
	public AssessmentDetail getAssessmentDetail() {
		return assessmentDetail;
	}
	public void setAssessmentDetail(AssessmentDetail assessmentDetail) {
		this.assessmentDetail = assessmentDetail;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = Utility.trim(sectionName);
	}
	public String getSectionDescription() {
		return sectionDescription;
	}
	public void setSectionDescription(String sectionDescription) {
		this.sectionDescription = Utility.trim(sectionDescription);
	}
	public String getSectionInstructions() {
		return sectionInstructions;
	}
	public void setSectionInstructions(String sectionInstructions) {
		this.sectionInstructions = Utility.trim(sectionInstructions);
	}
	public String getSectionVideoUrl() {
		return sectionVideoUrl;
	}
	public void setSectionVideoUrl(String sectionVideoUrl) {
		this.sectionVideoUrl = sectionVideoUrl;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
}
