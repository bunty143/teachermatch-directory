package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="stagethreestatus")
public class StageThreeStatus implements Serializable{

	private static final long serialVersionUID = -6526075033401264717L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer stage3Id;
	private String name;
	private String status;
	private String shortName;
	private Date createdDateTime;

	public Integer getStage3Id() {
		return stage3Id;
	}
	public void setStage3Id(Integer stage3Id) {
		this.stage3Id = stage3Id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

}
