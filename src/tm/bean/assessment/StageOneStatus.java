package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="stageonestatus")
public class StageOneStatus implements Serializable{

	private static final long serialVersionUID = 210220766968631362L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer stage1Id;
	private String name;
	private String status;
	private String shortName;
	private Date createdDateTime;

	public Integer getStage1Id() {
		return stage1Id;
	}
	public void setStage1Id(Integer stage1Id) {
		this.stage1Id = stage1Id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

}
