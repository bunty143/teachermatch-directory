package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.ObjectiveMaster;
import tm.bean.master.QuestionTypeMaster;
import tm.bean.user.UserMaster;
import tm.utility.Utility;

@Entity
@Table(name="questionspool")
public class QuestionsPool implements Serializable{

	private static final long serialVersionUID = -3709190269037547912L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer questionId;
	
	private String questionUId;
	
	@ManyToOne
	@JoinColumn(name="domainId",referencedColumnName="domainId")
	private DomainMaster domainMaster;
	
	@ManyToOne
	@JoinColumn(name="competencyId",referencedColumnName="competencyId")
	private CompetencyMaster competencyMaster;
	
	@ManyToOne
	@JoinColumn(name="objectiveId",referencedColumnName="objectiveId")
	private ObjectiveMaster objectiveMaster;
	
	@ManyToOne
	@JoinColumn(name="stage1Id",referencedColumnName="stage1Id")
	private StageOneStatus stageOneStatus;
	
	@ManyToOne
	@JoinColumn(name="stage2Id",referencedColumnName="stage2Id")
	private StageTwoStatus stageTwoStatus;
	
	@ManyToOne
	@JoinColumn(name="stage3Id",referencedColumnName="stage3Id")
	private StageThreeStatus stageThreeStatus;
	
	private String question;
	private Double questionWeightage;
	private Double maxMarks;
	private String questionInstruction;
	private Integer questionTaken;
	
	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster questionTypeMaster;
	
	private String validationType;
	private Integer fieldLength;
	
	private String itemCode;
	
	
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private String status;
	private Date createdDateTime;
	private String ipaddress;
	
	@OneToMany()
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="questionId",referencedColumnName="questionId",insertable=false,updatable=false)
	private List<QuestionOptions> questionOptions;
	
	public List<QuestionOptions> getQuestionOptions() {
		return questionOptions;
	}
	public void setQuestionOptions(List<QuestionOptions> questionOptions) {
		this.questionOptions = questionOptions;
	}
	
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	public String getQuestionUId() {
		return questionUId;
	}
	public void setQuestionUId(String questionUId) {
		this.questionUId = questionUId;
	}
	public DomainMaster getDomainMaster() {
		return domainMaster;
	}
	public void setDomainMaster(DomainMaster domainMaster) {
		this.domainMaster = domainMaster;
	}
	public CompetencyMaster getCompetencyMaster() {
		return competencyMaster;
	}
	public void setCompetencyMaster(CompetencyMaster competencyMaster) {
		this.competencyMaster = competencyMaster;
	}
	public ObjectiveMaster getObjectiveMaster() {
		return objectiveMaster;
	}
	public void setObjectiveMaster(ObjectiveMaster objectiveMaster) {
		this.objectiveMaster = objectiveMaster;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = Utility.trim(question);
	}
	
	public Double getQuestionWeightage() {
		return questionWeightage;
	}
	public void setQuestionWeightage(Double questionWeightage) {
		this.questionWeightage = questionWeightage;
	}
	public String getQuestionInstruction() {
		return questionInstruction;
	}
	public void setQuestionInstruction(String questionInstruction) {
		this.questionInstruction = Utility.trim(questionInstruction);
	}
	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}
	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}
	public String getValidationType() {
		return validationType;
	}
	public void setValidationType(String validationType) {
		this.validationType = validationType;
	}
	public Integer getFieldLength() {
		return fieldLength;
	}
	public void setFieldLength(Integer fieldLength) {
		this.fieldLength = fieldLength;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	public Double getMaxMarks() {
		return maxMarks;
	}
	public void setMaxMarks(Double maxMarks) {
		this.maxMarks = maxMarks;
	}
	public StageOneStatus getStageOneStatus() {
		return stageOneStatus;
	}
	public void setStageOneStatus(StageOneStatus stageOneStatus) {
		this.stageOneStatus = stageOneStatus;
	}
	public StageTwoStatus getStageTwoStatus() {
		return stageTwoStatus;
	}
	public void setStageTwoStatus(StageTwoStatus stageTwoStatus) {
		this.stageTwoStatus = stageTwoStatus;
	}
	public StageThreeStatus getStageThreeStatus() {
		return stageThreeStatus;
	}
	public void setStageThreeStatus(StageThreeStatus stageThreeStatus) {
		this.stageThreeStatus = stageThreeStatus;
	}
	public void setQuestionTaken(Integer questionTaken) {
		this.questionTaken = questionTaken;
	}
	public Integer getQuestionTaken() {
		return questionTaken;
	}
	
}
