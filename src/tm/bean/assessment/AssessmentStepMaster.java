package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name="assessmentstepmaster")
public class AssessmentStepMaster implements Serializable {

	private static final long serialVersionUID = 7934317683568110534L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer assessmentStepId;
	private String stepName;
	private String stepShortName;
	private String status;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	private Date createdDateTime;
	private String ipaddress;
	
	public Integer getAssessmentStepId() {
		return assessmentStepId;
	}
	public void setAssessmentStepId(Integer assessmentStepId) {
		this.assessmentStepId = assessmentStepId;
	}
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public String getStepShortName() {
		return stepShortName;
	}
	public void setStepShortName(String stepShortName) {
		this.stepShortName = stepShortName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
}
