package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="stagetwostatus")
public class StageTwoStatus implements Serializable{

	private static final long serialVersionUID = 5758820770660242632L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer stage2Id;
	private String name;
	private String status;
	private String shortName;
	private Date createdDateTime;

	public Integer getStage2Id() {
		return stage2Id;
	}
	public void setStage2Id(Integer stage2Id) {
		this.stage2Id = stage2Id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

}
