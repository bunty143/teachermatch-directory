package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.user.UserMaster;

@Entity
@Table(name="assessmentjobrelation")
public class AssessmentJobRelation implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1034764826570661215L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer assessmentJobRelationId;
	
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentId;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobId;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBy;
	private String status;
	private Date createdDateTime;
	private String ipaddress;
	
	public Integer getAssessmentJobRelationId() {
		return assessmentJobRelationId;
	}
	public void setAssessmentJobRelationId(Integer assessmentJobRelationId) {
		this.assessmentJobRelationId = assessmentJobRelationId;
	}
	public JobOrder getJobId() {
		return jobId;
	}
	public void setJobId(JobOrder jobId) {
		this.jobId = jobId;
	}
	public UserMaster getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(UserMaster createdBy) {
		this.createdBy = createdBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	public AssessmentDetail getAssessmentId() {
		return assessmentId;
	}
	public void setAssessmentId(AssessmentDetail assessmentId) {
		this.assessmentId = assessmentId;
	}
	
	
	
}
