package tm.bean.assessment;

import java.util.Date;

import javax.mail.search.DateTerm;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/* @Author: Indra Jeet Singh
 * @Discription: PDP Detail.
 */

@Entity
@Table(name="pdpmaster")
public class PdpDetail {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer pdpdId;	
	private String pdpname;
	private String status;
	private Date createdDateTime;
	private Integer createdBy;
	
	
	public Integer getPdpdId() {
		return pdpdId;
	}
	public void setPdpdId(Integer pdpdId) {
		this.pdpdId = pdpdId;
	}
	public String getPdpname() {
		return pdpname;
	}
	public void setPdpname(String pdpname) {
		this.pdpname = pdpname;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	
}
