package tm.bean.assessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="inventoryinvitationstemp")
public class InventoryInvitationsTemp implements Serializable
{
	private static final long serialVersionUID = -3883240586030966825L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long inventoryInvitationsTempId;
	private String candidateFirstName;
	private String candidateLastName;
	private String candidateEmail;
	private String sessionId;
	private String errorText;
	private Integer existTeacherId;
	private String orgType;
	private String inventoryType;
	private String groupName;
	private Integer districtId;
	private Date date;
	
	public Long getInventoryInvitationsTempId() {
		return inventoryInvitationsTempId;
	}
	public void setInventoryInvitationsTempId(Long inventoryInvitationsTempId) {
		this.inventoryInvitationsTempId = inventoryInvitationsTempId;
	}
	public String getCandidateFirstName() {
		return candidateFirstName;
	}
	public void setCandidateFirstName(String candidateFirstName) {
		this.candidateFirstName = candidateFirstName;
	}
	public String getCandidateLastName() {
		return candidateLastName;
	}
	public void setCandidateLastName(String candidateLastName) {
		this.candidateLastName = candidateLastName;
	}
	public String getCandidateEmail() {
		return candidateEmail;
	}
	public void setCandidateEmail(String candidateEmail) {
		this.candidateEmail = candidateEmail;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getErrorText() {
		return errorText;
	}
	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}
	public Integer getExistTeacherId() {
		return existTeacherId;
	}
	public void setExistTeacherId(Integer existTeacherId) {
		this.existTeacherId = existTeacherId;
	}
	public String getOrgType() {
		return orgType;
	}
	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}
	public String getInventoryType() {
		return inventoryType;
	}
	public void setInventoryType(String inventoryType) {
		this.inventoryType = inventoryType;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}