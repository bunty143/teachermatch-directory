package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="teachernormscore")
public class TeacherNormScore implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2144949881670905805L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherNorScoreId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="teacherAssessmentId",referencedColumnName="teacherAssessmentId")
	private TeacherAssessmentdetail teacherAssessmentdetail;
	private Integer teacherNormScore;
	private Double minDecileValue;
	private Double maxDecileValue;
	private String decileColor;
	private Double percentile;
	private Date createdDateTime;
	private Date assessmentDateTime;
	private Date updateDateTime;
	
	public Integer getTeacherNorScoreId() {
		return teacherNorScoreId;
	}
	public void setTeacherNorScoreId(Integer teacherNorScoreId) {
		this.teacherNorScoreId = teacherNorScoreId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public Integer getTeacherNormScore() {
		return teacherNormScore;
	}
	public void setTeacherNormScore(Integer teacherNormScore) {
		this.teacherNormScore = teacherNormScore;
	}
	public Double getMinDecileValue() {
		return minDecileValue;
	}
	public void setMinDecileValue(Double minDecileValue) {
		this.minDecileValue = minDecileValue;
	}
	public Double getMaxDecileValue() {
		return maxDecileValue;
	}
	public void setMaxDecileValue(Double maxDecileValue) {
		this.maxDecileValue = maxDecileValue;
	}
	public String getDecileColor() {
		return decileColor;
	}
	public void setDecileColor(String decileColor) {
		this.decileColor = decileColor;
	}
	public Double getPercentile() {
		return percentile;
	}
	public void setPercentile(Double percentile) {
		this.percentile = percentile;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public TeacherAssessmentdetail getTeacherAssessmentdetail() {
		return teacherAssessmentdetail;
	}
	public void setTeacherAssessmentdetail(TeacherAssessmentdetail teacherAssessmentdetail) {
		this.teacherAssessmentdetail = teacherAssessmentdetail;
	}
	public Date getAssessmentDateTime() {
		return assessmentDateTime;
	}
	public void setAssessmentDateTime(Date assessmentDateTime) {
		this.assessmentDateTime = assessmentDateTime;
	}
	public Date getUpdateDateTime() {
		return updateDateTime;
	}
	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
}
