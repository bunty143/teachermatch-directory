package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.mq.MQEvent;

@Entity
@Table(name="talentksndetail")
public class TalentKSNDetail implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7584574530627284143L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer ksnDetailId;
	private String firstName;
	private String lastName;
	private String emailAddress;
	private Integer KSNID;
	private String status;
	private String isSelected;
	private String manageBranchCode;
	private String jobCode;
	private Date createdDateTime;
	private String lastFourSSN;
	
	@ManyToOne
	@JoinColumn(name="eventId",referencedColumnName="eventId")
	private MQEvent eventId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherId;
	
	private Integer tmTalentId;


	public Integer getTmTalentId() {
		return tmTalentId;
	}


	public void setTmTalentId(Integer tmTalentId) {
		this.tmTalentId = tmTalentId;
	}


	public Integer getKsnDetailId() {
		return ksnDetailId;
	}


	public void setKsnDetailId(Integer ksnDetailId) {
		this.ksnDetailId = ksnDetailId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmailAddress() {
		return emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public Integer getKSNID() {
		return KSNID;
	}


	public void setKSNID(Integer kSNID) {
		KSNID = kSNID;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getIsSelected() {
		return isSelected;
	}


	public void setIsSelected(String isSelected) {
		this.isSelected = isSelected;
	}


	public String getManageBranchCode() {
		return manageBranchCode;
	}


	public void setManageBranchCode(String manageBranchCode) {
		this.manageBranchCode = manageBranchCode;
	}


	public String getJobCode() {
		return jobCode;
	}


	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}


	
	public Date getCreatedDateTime() {
		return createdDateTime;
	}


	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}


	public MQEvent getEventId() {
		return eventId;
	}


	public void setEventId(MQEvent eventId) {
		this.eventId = eventId;
	}


	public TeacherDetail getTeacherId() {
		return teacherId;
	}


	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}


	public String getLastFourSSN() {
		return lastFourSSN;
	}


	public void setLastFourSSN(String lastFourSSN) {
		this.lastFourSSN = lastFourSSN;
	}


	
	
	

}
