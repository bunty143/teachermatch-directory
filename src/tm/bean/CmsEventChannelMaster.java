package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name="cmseventchannelmaster")
public class CmsEventChannelMaster implements Serializable{
		private static final long serialVersionUID = 6200907785107854295L;

		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Integer eventChannelId;
		private String eventChannelName;
		private String status;
		@ManyToOne
		@JoinColumn(name="eventTypeId",referencedColumnName="eventTypeId")
		private  CmsEventTypeMaster cmsEventTypeMaster;
		public CmsEventTypeMaster getCmsEventTypeMaster() {
			return cmsEventTypeMaster;
		}
		public void setCmsEventTypeMaster(CmsEventTypeMaster cmsEventTypeMaster) {
			this.cmsEventTypeMaster = cmsEventTypeMaster;
		}

		@ManyToOne
		@JoinColumn(name="createdBy",referencedColumnName="userId")
		private UserMaster createdBY;
		private Date createdDateTime;

		public Integer getEventChannelId() {
			return eventChannelId;
		}
		public void setEventChannelId(Integer eventChannelId) {
			this.eventChannelId = eventChannelId;
		}
		public String getEventChannelName() {
			return eventChannelName;
		}
		public void setEventChannelName(String eventChannelName) {
			this.eventChannelName = eventChannelName;
		}
		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public UserMaster getCreatedBY() {
			return createdBY;
		}

		public void setCreatedBY(UserMaster createdBY) {
			this.createdBY = createdBY;
		}

		public Date getCreatedDateTime() {
			return createdDateTime;
		}

		public void setCreatedDateTime(Date createdDateTime) {
			this.createdDateTime = createdDateTime;
		}

		
}
