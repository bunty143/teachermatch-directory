package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teachernotes")
public class TeacherNotes implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2224915807059816003L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer noteId;	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;	
	private String note;
	@ManyToOne
	@JoinColumn(name="parentNoteId",referencedColumnName="noteId")
	private TeacherNotes parentNoteId;	
	
	private Integer notesOrder;
	@ManyToOne
	@JoinColumn(name="noteCreatedByUser",referencedColumnName="userId")
	private UserMaster noteCreatedByUser;
	private Integer noteCreatedByEntity;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	private String status;
	private Date createdDateTime;
	private String ipaddress;
	private String noteFileName;
	
	
	public String getNoteFileName() {
		return noteFileName;
	}
	public void setNoteFileName(String noteFileName) {
		this.noteFileName = noteFileName;
	}
	public Integer getNoteId() {
		return noteId;
	}
	public void setNoteId(Integer noteId) {
		this.noteId = noteId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public TeacherNotes getParentNoteId() {
		return parentNoteId;
	}
	public void setParentNoteId(TeacherNotes parentNoteId) {
		this.parentNoteId = parentNoteId;
	}
	public Integer getNotesOrder() {
		return notesOrder;
	}
	public void setNotesOrder(Integer notesOrder) {
		this.notesOrder = notesOrder;
	}
	public UserMaster getNoteCreatedByUser() {
		return noteCreatedByUser;
	}
	public void setNoteCreatedByUser(UserMaster noteCreatedByUser) {
		this.noteCreatedByUser = noteCreatedByUser;
	}
	public Integer getNoteCreatedByEntity() {
		return noteCreatedByEntity;
	}
	public void setNoteCreatedByEntity(Integer noteCreatedByEntity) {
		this.noteCreatedByEntity = noteCreatedByEntity;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
}
