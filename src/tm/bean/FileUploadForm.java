package tm.bean;
import java.io.Serializable;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class FileUploadForm implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7582211491294318662L;
	private String generalKnowledgeExamStatus;
	private String generalKnowledgeExamDate;
	
	private String examStatus;
	private String examDate;
	private String subjectId;
	
	public String getGeneralKnowledgeExamStatus() {
		return generalKnowledgeExamStatus;
	}

	public void setGeneralKnowledgeExamStatus(String generalKnowledgeExamStatus) {
		this.generalKnowledgeExamStatus = generalKnowledgeExamStatus;
	}

	public String getGeneralKnowledgeExamDate() {
		return generalKnowledgeExamDate;
	}

	public void setGeneralKnowledgeExamDate(String generalKnowledgeExamDate) {
		this.generalKnowledgeExamDate = generalKnowledgeExamDate;
	}

	public String getExamStatus() {
		return examStatus;
	}

	public void setExamStatus(String examStatus) {
		this.examStatus = examStatus;
	}

	public String getExamDate() {
		return examDate;
	}

	public void setExamDate(String examDate) {
		this.examDate = examDate;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	private List<MultipartFile> files;

	
	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}
}
