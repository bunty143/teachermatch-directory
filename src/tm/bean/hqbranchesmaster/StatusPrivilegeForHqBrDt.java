package tm.bean.hqbranchesmaster;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
@Entity
@Table(name="statusprivilegeforhqbrdt")
public class StatusPrivilegeForHqBrDt implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7138806125282758830L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer statusPrivilegeId;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="statusId",referencedColumnName="statusId")
	private StatusMaster statusMaster;
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus;
	
	private Integer canBranchSetStatus;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private Date createdDateTime;

	public Integer getStatusPrivilegeId() {
		return statusPrivilegeId;
	}

	public void setStatusPrivilegeId(Integer statusPrivilegeId) {
		this.statusPrivilegeId = statusPrivilegeId;
	}

	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}

	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public StatusMaster getStatusMaster() {
		return statusMaster;
	}

	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}

	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}

	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}

	public Integer getCanBranchSetStatus() {
		return canBranchSetStatus;
	}

	public void setCanBranchSetStatus(Integer canBranchSetStatus) {
		this.canBranchSetStatus = canBranchSetStatus;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	@Override
	public String toString() {
		return " statusPrivilegeId :: "+this.statusPrivilegeId;
	}
}
