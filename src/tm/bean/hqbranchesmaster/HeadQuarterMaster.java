package tm.bean.hqbranchesmaster;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.commons.CommonsMultipartFile;
import tm.bean.i4.I4QuestionSets;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;

@Entity
@Table(name="headquartermaster")
public class HeadQuarterMaster implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1547296362437428802L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer headQuarterId;
	private String headQuarterName;
	private String headQuarterCode;
	@ManyToOne
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private StateMaster stateId;
	private String cityName;
	private String authKey;
	private String exitURL;
	private Integer noOfReminder;
	private Integer reminderFrequencyInDays;
	private Integer reminderOfFirstFrequencyInDays;
	private Integer exclusivePeriod;
	private String completionMessage;
	private String websiteUrl;
	private String apiRedirectUrl;
	private String locationCode;
	private String phoneNumber;
	private String faxNumber;
	private String address; 
	private String zipCode; 
	private String type;                                             
	private String latcod;     
	private String loncod;
	private String description;     
	private String dmName;     
	private String dmEmailAddress;     
	private String dmPhoneNumber;     
	private String dmPassword;     
	private String acName;     
	private String acEmailAddress;     
	private String acPhoneNumber; 
	private String amName;     
	private String amEmailAddress;     
	private String amPhoneNumber; 
	
	
	private Boolean isBranchExist;
	
	private int jobApplicationCriteriaForProspects;
	private Boolean displayTMDefaultJobCategory;
	private Boolean areAllBranchesInContract;
	private Boolean writePrivilegeToBranch;
	private Boolean resetQualificationIssuesPrivilegeToBranch;
	private Boolean approvalBeforeGoLive;
	private Boolean autoNotifyCandidateOnAttachingWithJob;
	private Boolean autoNotifyOnStatusChange;
	private Boolean setAssociatedStatusToSetDPoints;
	private Integer sendReminderToIcompTalent;
	private Integer sendReferenceOnJobComplete;
	private Integer postingOnHQWall;
	private Integer postingOnTMWall;
	private String assessmentUploadURL;
	private Integer canTMApproach;
	private Integer allowMessageTeacher;
	private String hiringAuthority;
	private Integer hQApproval;
	private Boolean isPortfolioNeeded;
	private Integer isWeeklyCgReport;
	private String emailForTeacher;
	
	private Integer totalNoOfBranches;
	private Integer branchesUnderContract;
	private Integer districtsUnderContract;
	private Integer noBranchUnderContract;
	private Integer noDistrictUnderContract; 
	private Integer allBranchesUnderContract;
	private Integer allDistrictsUnderContract;
	private Integer selectedBranchesUnderContract;
	private Integer selectedDistrictsUnderContract;
	private Date contractStartDate;     
	private Date contractEndDate;
	private Double annualSubsciptionAmount;
	
	private String logoPath;
	private CommonsMultipartFile 	logoPathFile;
	private CommonsMultipartFile 	assessmentUploadURLFile;
	
	private Integer candidateFeedNormScore;
	private String distributionEmail;
	private Integer candidateFeedDaysOfNoActivity;
	private Boolean canBranchOverrideCandidateFeed;	
	private Boolean statusPrivilegeForHeadquarter;
	private Boolean statusPrivilegeForBranches;	
	
	private Integer jobFeedCriticalJobActiveDays;
	private Integer jobFeedAttentionJobActiveDays;
	private Integer jobFeedCriticalCandidateRatio;
	private Boolean jobFeedAttentionJobNotFilled;
	
	@ManyToOne
	@JoinColumn(name="statusIdReminderExpireAction",referencedColumnName="statusId")
	private StatusMaster statusIdReminderExpireAction;
	
	@ManyToOne
	@JoinColumn(name="statusIdForBranchPrivilege",referencedColumnName="statusId")
	private StatusMaster statusMaster;
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusIdForBranchPrivilege",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus;
	
	private Integer communicationsAccess;
	
	
	private Boolean displayCGPA;
	private Boolean displayAchievementScore;
	private Boolean displayTFA;
	private Boolean displayDemoClass;
	private Boolean displayJSI;
	private Boolean displayYearsTeaching;
	private Boolean displayExpectedSalary;
	private Boolean displayFitScore;
	private Boolean displayPhoneInterview;
	
	private String textForReferenceInstruction;
	private String textForDistrictSpecificQuestions;
	
	private Boolean offerDistrictSpecificItems;
	private Boolean offerQualificationItems;
	private Boolean offerEPI;
	private Boolean offerJSI;
	private Boolean offerPortfolioNeeded;
	private Boolean offerVirtualVideoInterview;
	private Boolean offerAssessmentInviteOnly;
	
	@ManyToOne
	@JoinColumn(name="vviQuestionSet",referencedColumnName="ID")
	private I4QuestionSets i4QuestionSets;
	private Integer maxScoreForVVI;	
	private Boolean sendAutoVVILink;
	private Integer timeAllowedPerQuestion;
	private Integer VVIExpiresInDays;
	@Column(name="jobCompletionDate")
	private Boolean jobCompletionNeeded;
	
	@ManyToOne
	@JoinColumn(name="statusIdForAutoVVILink",referencedColumnName="statusId")
	private StatusMaster statusMasterForVVI;
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusIdForAutoVVILink",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatusForVVI;
	
	private Integer candidateConsiderationStatusId;
	
	private Integer candidateConsiderationSecondaryStatusId;
	
	private Boolean isTeacherPoolOnLoad;
	private Boolean noEPI;
	private Boolean statusNotes;
	private String status;
	private Date createdDateTime;
	
	@Transient
	String [] chkStatusMaster;
	
	@Transient
	String [] chkSecondaryStatusName;
	
	@Transient
	String [] chkDStatusMaster;
	
	@Transient
	String [] chkStatusMasterEml;
	
	@Transient
	String [] chkSecondaryStatusNameEml;
	
	@Transient
	boolean autoEmailRequired;
	
	@Transient
	String [] chkHQStatusMaster;
	
	@Transient
	String [] chkHQSecondaryStatusName;
	
	public Integer getCandidateConsiderationStatusId() {
		return candidateConsiderationStatusId;
	}

	public void setCandidateConsiderationStatusId(
			Integer candidateConsiderationStatusId) {
		this.candidateConsiderationStatusId = candidateConsiderationStatusId;
	}

	public Integer getCandidateConsiderationSecondaryStatusId() {
		return candidateConsiderationSecondaryStatusId;
	}

	public void setCandidateConsiderationSecondaryStatusId(
			Integer candidateConsiderationSecondaryStatusId) {
		this.candidateConsiderationSecondaryStatusId = candidateConsiderationSecondaryStatusId;
	}

	@ManyToOne
	@JoinColumn(name="statusIdForeReferenceFinalize",referencedColumnName="statusId")
	private StatusMaster statusMasterForReference;
	
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusIdForeReferenceFinalize",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatusForReference;
		
	public Integer getExclusivePeriod() {
		return exclusivePeriod;
	}

	public void setExclusivePeriod(Integer exclusivePeriod) {
		this.exclusivePeriod = exclusivePeriod;
	}

	public Integer getHeadQuarterId() {
		return headQuarterId;
	}

	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}

	public String getHeadQuarterName() {
		return headQuarterName;
	}

	public void setHeadQuarterName(String headQuarterName) {
		this.headQuarterName = headQuarterName;
	}

	public String getHeadQuarterCode() {
		return headQuarterCode;
	}

	public void setHeadQuarterCode(String headQuarterCode) {
		this.headQuarterCode = headQuarterCode;
	}

	public StateMaster getStateId() {
		return stateId;
	}

	public void setStateId(StateMaster stateId) {
		this.stateId = stateId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}

	public String getExitURL() {
		return exitURL;
	}

	public void setExitURL(String exitURL) {
		this.exitURL = exitURL;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getApiRedirectUrl() {
		return apiRedirectUrl;
	}

	public void setApiRedirectUrl(String apiRedirectUrl) {
		this.apiRedirectUrl = apiRedirectUrl;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLatcod() {
		return latcod;
	}

	public void setLatcod(String latcod) {
		this.latcod = latcod;
	}

	public String getLoncod() {
		return loncod;
	}

	public void setLoncod(String loncod) {
		this.loncod = loncod;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDmName() {
		return dmName;
	}

	public void setDmName(String dmName) {
		this.dmName = dmName;
	}

	public String getDmEmailAddress() {
		return dmEmailAddress;
	}

	public void setDmEmailAddress(String dmEmailAddress) {
		this.dmEmailAddress = dmEmailAddress;
	}

	public String getDmPhoneNumber() {
		return dmPhoneNumber;
	}

	public void setDmPhoneNumber(String dmPhoneNumber) {
		this.dmPhoneNumber = dmPhoneNumber;
	}

	public String getDmPassword() {
		return dmPassword;
	}

	public void setDmPassword(String dmPassword) {
		this.dmPassword = dmPassword;
	}

	public String getAcName() {
		return acName;
	}

	public void setAcName(String acName) {
		this.acName = acName;
	}

	public String getAcEmailAddress() {
		return acEmailAddress;
	}

	public void setAcEmailAddress(String acEmailAddress) {
		this.acEmailAddress = acEmailAddress;
	}

	public String getAcPhoneNumber() {
		return acPhoneNumber;
	}

	public void setAcPhoneNumber(String acPhoneNumber) {
		this.acPhoneNumber = acPhoneNumber;
	}

	public String getAmName() {
		return amName;
	}

	public void setAmName(String amName) {
		this.amName = amName;
	}

	public String getAmEmailAddress() {
		return amEmailAddress;
	}

	public void setAmEmailAddress(String amEmailAddress) {
		this.amEmailAddress = amEmailAddress;
	}

	public String getAmPhoneNumber() {
		return amPhoneNumber;
	}

	public void setAmPhoneNumber(String amPhoneNumber) {
		this.amPhoneNumber = amPhoneNumber;
	}

	public Boolean getIsBranchExist() {
		return isBranchExist;
	}

	public void setIsBranchExist(Boolean isBranchExist) {
		this.isBranchExist = isBranchExist;
	}

	public int getJobApplicationCriteriaForProspects() {
		return jobApplicationCriteriaForProspects;
	}

	public void setJobApplicationCriteriaForProspects(
			int jobApplicationCriteriaForProspects) {
		this.jobApplicationCriteriaForProspects = jobApplicationCriteriaForProspects;
	}

	public Boolean getDisplayTMDefaultJobCategory() {
		return displayTMDefaultJobCategory;
	}

	public void setDisplayTMDefaultJobCategory(Boolean displayTMDefaultJobCategory) {
		this.displayTMDefaultJobCategory = displayTMDefaultJobCategory;
	}

	public Boolean getAreAllBranchesInContract() {
		return areAllBranchesInContract;
	}

	public void setAreAllBranchesInContract(Boolean areAllBranchesInContract) {
		this.areAllBranchesInContract = areAllBranchesInContract;
	}

	public Boolean getWritePrivilegeToBranch() {
		return writePrivilegeToBranch;
	}

	public void setWritePrivilegeToBranch(Boolean writePrivilegeToBranch) {
		this.writePrivilegeToBranch = writePrivilegeToBranch;
	}

	public Boolean getResetQualificationIssuesPrivilegeToBranch() {
		return resetQualificationIssuesPrivilegeToBranch;
	}

	public void setResetQualificationIssuesPrivilegeToBranch(
			Boolean resetQualificationIssuesPrivilegeToBranch) {
		this.resetQualificationIssuesPrivilegeToBranch = resetQualificationIssuesPrivilegeToBranch;
	}

	public Boolean getApprovalBeforeGoLive() {
		return approvalBeforeGoLive;
	}

	public void setApprovalBeforeGoLive(Boolean approvalBeforeGoLive) {
		this.approvalBeforeGoLive = approvalBeforeGoLive;
	}

	public Boolean getAutoNotifyCandidateOnAttachingWithJob() {
		return autoNotifyCandidateOnAttachingWithJob;
	}

	public void setAutoNotifyCandidateOnAttachingWithJob(
			Boolean autoNotifyCandidateOnAttachingWithJob) {
		this.autoNotifyCandidateOnAttachingWithJob = autoNotifyCandidateOnAttachingWithJob;
	}

	public Boolean getAutoNotifyOnStatusChange() {
		return autoNotifyOnStatusChange;
	}

	public void setAutoNotifyOnStatusChange(Boolean autoNotifyOnStatusChange) {
		this.autoNotifyOnStatusChange = autoNotifyOnStatusChange;
	}

	public Boolean getSetAssociatedStatusToSetDPoints() {
		return setAssociatedStatusToSetDPoints;
	}

	public void setSetAssociatedStatusToSetDPoints(
			Boolean setAssociatedStatusToSetDPoints) {
		this.setAssociatedStatusToSetDPoints = setAssociatedStatusToSetDPoints;
	}

	public Integer getSendReminderToIcompTalent() {
		return sendReminderToIcompTalent;
	}

	public void setSendReminderToIcompTalent(Integer sendReminderToIcompTalent) {
		this.sendReminderToIcompTalent = sendReminderToIcompTalent;
	}

	public Integer getSendReferenceOnJobComplete() {
		return sendReferenceOnJobComplete;
	}

	public void setSendReferenceOnJobComplete(Integer sendReferenceOnJobComplete) {
		this.sendReferenceOnJobComplete = sendReferenceOnJobComplete;
	}

	public Integer getPostingOnHQWall() {
		return postingOnHQWall;
	}

	public void setPostingOnHQWall(Integer postingOnHQWall) {
		this.postingOnHQWall = postingOnHQWall;
	}

	public Integer getPostingOnTMWall() {
		return postingOnTMWall;
	}

	public void setPostingOnTMWall(Integer postingOnTMWall) {
		this.postingOnTMWall = postingOnTMWall;
	}

	public String getAssessmentUploadURL() {
		return assessmentUploadURL;
	}

	public void setAssessmentUploadURL(String assessmentUploadURL) {
		this.assessmentUploadURL = assessmentUploadURL;
	}

	public Integer getCanTMApproach() {
		return canTMApproach;
	}

	public void setCanTMApproach(Integer canTMApproach) {
		this.canTMApproach = canTMApproach;
	}

	public Integer getAllowMessageTeacher() {
		return allowMessageTeacher;
	}

	public void setAllowMessageTeacher(Integer allowMessageTeacher) {
		this.allowMessageTeacher = allowMessageTeacher;
	}

	public String getHiringAuthority() {
		return hiringAuthority;
	}

	public void setHiringAuthority(String hiringAuthority) {
		this.hiringAuthority = hiringAuthority;
	}

	public Integer gethQApproval() {
		return hQApproval;
	}

	public void sethQApproval(Integer hQApproval) {
		this.hQApproval = hQApproval;
	}

	public Boolean getIsPortfolioNeeded() {
		return isPortfolioNeeded;
	}

	public void setIsPortfolioNeeded(Boolean isPortfolioNeeded) {
		this.isPortfolioNeeded = isPortfolioNeeded;
	}

	public Integer getIsWeeklyCgReport() {
		return isWeeklyCgReport;
	}

	public void setIsWeeklyCgReport(Integer isWeeklyCgReport) {
		this.isWeeklyCgReport = isWeeklyCgReport;
	}

	public String getEmailForTeacher() {
		return emailForTeacher;
	}

	public void setEmailForTeacher(String emailForTeacher) {
		this.emailForTeacher = emailForTeacher;
	}

	public Integer getTotalNoOfBranches() {
		return totalNoOfBranches;
	}

	public void setTotalNoOfBranches(Integer totalNoOfBranches) {
		this.totalNoOfBranches = totalNoOfBranches;
	}

	public Integer getBranchesUnderContract() {
		return branchesUnderContract;
	}

	public void setBranchesUnderContract(Integer branchesUnderContract) {
		this.branchesUnderContract = branchesUnderContract;
	}

	public Integer getDistrictsUnderContract() {
		return districtsUnderContract;
	}

	public void setDistrictsUnderContract(Integer districtsUnderContract) {
		this.districtsUnderContract = districtsUnderContract;
	}

	public Integer getNoBranchUnderContract() {
		return noBranchUnderContract;
	}

	public void setNoBranchUnderContract(Integer noBranchUnderContract) {
		this.noBranchUnderContract = noBranchUnderContract;
	}

	public Integer getNoDistrictUnderContract() {
		return noDistrictUnderContract;
	}

	public void setNoDistrictUnderContract(Integer noDistrictUnderContract) {
		this.noDistrictUnderContract = noDistrictUnderContract;
	}

	public Integer getAllBranchesUnderContract() {
		return allBranchesUnderContract;
	}

	public void setAllBranchesUnderContract(Integer allBranchesUnderContract) {
		this.allBranchesUnderContract = allBranchesUnderContract;
	}

	public Integer getAllDistrictsUnderContract() {
		return allDistrictsUnderContract;
	}

	public void setAllDistrictsUnderContract(Integer allDistrictsUnderContract) {
		this.allDistrictsUnderContract = allDistrictsUnderContract;
	}

	public Integer getSelectedBranchesUnderContract() {
		return selectedBranchesUnderContract;
	}

	public void setSelectedBranchesUnderContract(
			Integer selectedBranchesUnderContract) {
		this.selectedBranchesUnderContract = selectedBranchesUnderContract;
	}

	public Integer getSelectedDistrictsUnderContract() {
		return selectedDistrictsUnderContract;
	}

	public void setSelectedDistrictsUnderContract(
			Integer selectedDistrictsUnderContract) {
		this.selectedDistrictsUnderContract = selectedDistrictsUnderContract;
	}

	public Date getContractStartDate() {
		return contractStartDate;
	}

	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
	}

	public Date getContractEndDate() {
		return contractEndDate;
	}

	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}

	public Double getAnnualSubsciptionAmount() {
		return annualSubsciptionAmount;
	}

	public void setAnnualSubsciptionAmount(Double annualSubsciptionAmount) {
		this.annualSubsciptionAmount = annualSubsciptionAmount;
	}

	public String getLogoPath() {
		return logoPath;
	}

	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	public CommonsMultipartFile getLogoPathFile() {
		return logoPathFile;
	}

	public void setLogoPathFile(CommonsMultipartFile logoPathFile) {
		this.logoPathFile = logoPathFile;
	}

	public CommonsMultipartFile getAssessmentUploadURLFile() {
		return assessmentUploadURLFile;
	}

	public void setAssessmentUploadURLFile(
			CommonsMultipartFile assessmentUploadURLFile) {
		this.assessmentUploadURLFile = assessmentUploadURLFile;
	}

	public Integer getCandidateFeedNormScore() {
		return candidateFeedNormScore;
	}

	public void setCandidateFeedNormScore(Integer candidateFeedNormScore) {
		this.candidateFeedNormScore = candidateFeedNormScore;
	}

	public String getDistributionEmail() {
		return distributionEmail;
	}

	public void setDistributionEmail(String distributionEmail) {
		this.distributionEmail = distributionEmail;
	}

	public Integer getCandidateFeedDaysOfNoActivity() {
		return candidateFeedDaysOfNoActivity;
	}

	public void setCandidateFeedDaysOfNoActivity(
			Integer candidateFeedDaysOfNoActivity) {
		this.candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity;
	}

	public Boolean getCanBranchOverrideCandidateFeed() {
		return canBranchOverrideCandidateFeed;
	}

	public void setCanBranchOverrideCandidateFeed(
			Boolean canBranchOverrideCandidateFeed) {
		this.canBranchOverrideCandidateFeed = canBranchOverrideCandidateFeed;
	}

	public Boolean getStatusPrivilegeForBranches() {
		return statusPrivilegeForBranches;
	}

	public void setStatusPrivilegeForBranches(Boolean statusPrivilegeForBranches) {
		this.statusPrivilegeForBranches = statusPrivilegeForBranches;
	}

	public StatusMaster getStatusMaster() {
		return statusMaster;
	}

	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}

	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}

	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}

	public Integer getCommunicationsAccess() {
		return communicationsAccess;
	}

	public void setCommunicationsAccess(Integer communicationsAccess) {
		this.communicationsAccess = communicationsAccess;
	}

	public Boolean getDisplayCGPA() {
		return displayCGPA;
	}

	public void setDisplayCGPA(Boolean displayCGPA) {
		this.displayCGPA = displayCGPA;
	}

	public Boolean getDisplayAchievementScore() {
		return displayAchievementScore;
	}

	public void setDisplayAchievementScore(Boolean displayAchievementScore) {
		this.displayAchievementScore = displayAchievementScore;
	}

	public Boolean getDisplayTFA() {
		return displayTFA;
	}

	public void setDisplayTFA(Boolean displayTFA) {
		this.displayTFA = displayTFA;
	}

	public Boolean getDisplayDemoClass() {
		return displayDemoClass;
	}

	public void setDisplayDemoClass(Boolean displayDemoClass) {
		this.displayDemoClass = displayDemoClass;
	}

	public Boolean getDisplayJSI() {
		return displayJSI;
	}

	public void setDisplayJSI(Boolean displayJSI) {
		this.displayJSI = displayJSI;
	}

	public Boolean getDisplayYearsTeaching() {
		return displayYearsTeaching;
	}

	public void setDisplayYearsTeaching(Boolean displayYearsTeaching) {
		this.displayYearsTeaching = displayYearsTeaching;
	}

	public Boolean getDisplayExpectedSalary() {
		return displayExpectedSalary;
	}

	public void setDisplayExpectedSalary(Boolean displayExpectedSalary) {
		this.displayExpectedSalary = displayExpectedSalary;
	}

	public Boolean getDisplayFitScore() {
		return displayFitScore;
	}

	public void setDisplayFitScore(Boolean displayFitScore) {
		this.displayFitScore = displayFitScore;
	}

	public Boolean getDisplayPhoneInterview() {
		return displayPhoneInterview;
	}

	public void setDisplayPhoneInterview(Boolean displayPhoneInterview) {
		this.displayPhoneInterview = displayPhoneInterview;
	}
	

	public String getTextForReferenceInstruction() {
		return textForReferenceInstruction;
	}

	public void setTextForReferenceInstruction(String textForReferenceInstruction) {
		this.textForReferenceInstruction = textForReferenceInstruction;
	}

	public String getTextForDistrictSpecificQuestions() {
		return textForDistrictSpecificQuestions;
	}

	public void setTextForDistrictSpecificQuestions(
			String textForDistrictSpecificQuestions) {
		this.textForDistrictSpecificQuestions = textForDistrictSpecificQuestions;
	}

	public Boolean getOfferDistrictSpecificItems() {
		return offerDistrictSpecificItems;
	}

	public void setOfferDistrictSpecificItems(Boolean offerDistrictSpecificItems) {
		this.offerDistrictSpecificItems = offerDistrictSpecificItems;
	}

	public Boolean getOfferQualificationItems() {
		return offerQualificationItems;
	}

	public void setOfferQualificationItems(Boolean offerQualificationItems) {
		this.offerQualificationItems = offerQualificationItems;
	}

	public Boolean getOfferEPI() {
		return offerEPI;
	}

	public void setOfferEPI(Boolean offerEPI) {
		this.offerEPI = offerEPI;
	}

	public Boolean getOfferJSI() {
		return offerJSI;
	}

	public void setOfferJSI(Boolean offerJSI) {
		this.offerJSI = offerJSI;
	}

	public Boolean getOfferPortfolioNeeded() {
		return offerPortfolioNeeded;
	}

	public void setOfferPortfolioNeeded(Boolean offerPortfolioNeeded) {
		this.offerPortfolioNeeded = offerPortfolioNeeded;
	}

	public Boolean getOfferVirtualVideoInterview() {
		return offerVirtualVideoInterview;
	}

	public void setOfferVirtualVideoInterview(Boolean offerVirtualVideoInterview) {
		this.offerVirtualVideoInterview = offerVirtualVideoInterview;
	}

	public Boolean getOfferAssessmentInviteOnly() {
		return offerAssessmentInviteOnly;
	}

	public void setOfferAssessmentInviteOnly(Boolean offerAssessmentInviteOnly) {
		this.offerAssessmentInviteOnly = offerAssessmentInviteOnly;
	}

	public I4QuestionSets getI4QuestionSets() {
		return i4QuestionSets;
	}

	public void setI4QuestionSets(I4QuestionSets i4QuestionSets) {
		this.i4QuestionSets = i4QuestionSets;
	}

	public Integer getMaxScoreForVVI() {
		return maxScoreForVVI;
	}

	public void setMaxScoreForVVI(Integer maxScoreForVVI) {
		this.maxScoreForVVI = maxScoreForVVI;
	}

	public Boolean getSendAutoVVILink() {
		return sendAutoVVILink;
	}

	public void setSendAutoVVILink(Boolean sendAutoVVILink) {
		this.sendAutoVVILink = sendAutoVVILink;
	}

	public Integer getTimeAllowedPerQuestion() {
		return timeAllowedPerQuestion;
	}

	public void setTimeAllowedPerQuestion(Integer timeAllowedPerQuestion) {
		this.timeAllowedPerQuestion = timeAllowedPerQuestion;
	}

	public Integer getVVIExpiresInDays() {
		return VVIExpiresInDays;
	}

	public void setVVIExpiresInDays(Integer vVIExpiresInDays) {
		VVIExpiresInDays = vVIExpiresInDays;
	}

	public StatusMaster getStatusMasterForVVI() {
		return statusMasterForVVI;
	}

	public void setStatusMasterForVVI(StatusMaster statusMasterForVVI) {
		this.statusMasterForVVI = statusMasterForVVI;
	}

	public SecondaryStatus getSecondaryStatusForVVI() {
		return secondaryStatusForVVI;
	}

	public void setSecondaryStatusForVVI(SecondaryStatus secondaryStatusForVVI) {
		this.secondaryStatusForVVI = secondaryStatusForVVI;
	}

	public Boolean getIsTeacherPoolOnLoad() {
		return isTeacherPoolOnLoad;
	}
	

	public void setIsTeacherPoolOnLoad(Boolean isTeacherPoolOnLoad) {
		this.isTeacherPoolOnLoad = isTeacherPoolOnLoad;
	}

	public Boolean getNoEPI() {
		return noEPI;
	}

	public void setNoEPI(Boolean noEPI) {
		this.noEPI = noEPI;
	}

	public Boolean getStatusNotes() {
		return statusNotes;
	}

	public void setStatusNotes(Boolean statusNotes) {
		this.statusNotes = statusNotes;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String[] getChkStatusMaster() {
		return chkStatusMaster;
	}

	public void setChkStatusMaster(String[] chkStatusMaster) {
		this.chkStatusMaster = chkStatusMaster;
	}

	public String[] getChkSecondaryStatusName() {
		return chkSecondaryStatusName;
	}

	public void setChkSecondaryStatusName(String[] chkSecondaryStatusName) {
		this.chkSecondaryStatusName = chkSecondaryStatusName;
	}

	public String[] getChkDStatusMaster() {
		return chkDStatusMaster;
	}

	public void setChkDStatusMaster(String[] chkDStatusMaster) {
		this.chkDStatusMaster = chkDStatusMaster;
	}

	public String[] getChkStatusMasterEml() {
		return chkStatusMasterEml;
	}

	public void setChkStatusMasterEml(String[] chkStatusMasterEml) {
		this.chkStatusMasterEml = chkStatusMasterEml;
	}

	public String[] getChkSecondaryStatusNameEml() {
		return chkSecondaryStatusNameEml;
	}

	public void setChkSecondaryStatusNameEml(String[] chkSecondaryStatusNameEml) {
		this.chkSecondaryStatusNameEml = chkSecondaryStatusNameEml;
	}

	public boolean isAutoEmailRequired() {
		return autoEmailRequired;
	}

	public void setAutoEmailRequired(boolean autoEmailRequired) {
		this.autoEmailRequired = autoEmailRequired;
	}
	
	
	
	
	public StatusMaster getStatusMasterForReference() {
		return statusMasterForReference;
	}

	public void setStatusMasterForReference(StatusMaster statusMasterForReference) {
		this.statusMasterForReference = statusMasterForReference;
	}

	public SecondaryStatus getSecondaryStatusForReference() {
		return secondaryStatusForReference;
	}

	public void setSecondaryStatusForReference(
			SecondaryStatus secondaryStatusForReference) {
		this.secondaryStatusForReference = secondaryStatusForReference;
	}


	public String getCompletionMessage() {
		return completionMessage;
	}

	public void setCompletionMessage(String completionMessage) {
		this.completionMessage = completionMessage;
	}
	

	public Integer getNoOfReminder() {
		return noOfReminder;
	}

	public void setNoOfReminder(Integer noOfReminder) {
		this.noOfReminder = noOfReminder;
	}

	public Integer getReminderFrequencyInDays() {
		return reminderFrequencyInDays;
	}

	public void setReminderFrequencyInDays(Integer reminderFrequencyInDays) {
		this.reminderFrequencyInDays = reminderFrequencyInDays;
	}

	public Integer getReminderOfFirstFrequencyInDays() {
		return reminderOfFirstFrequencyInDays;
	}

	public void setReminderOfFirstFrequencyInDays(
			Integer reminderOfFirstFrequencyInDays) {
		this.reminderOfFirstFrequencyInDays = reminderOfFirstFrequencyInDays;
	}
	

	public StatusMaster getStatusIdReminderExpireAction() {
		return statusIdReminderExpireAction;
	}

	public void setStatusIdReminderExpireAction(
			StatusMaster statusIdReminderExpireAction) {
		this.statusIdReminderExpireAction = statusIdReminderExpireAction;
	}

	public Integer getJobFeedCriticalJobActiveDays() {
		return jobFeedCriticalJobActiveDays;
	}

	public void setJobFeedCriticalJobActiveDays(Integer jobFeedCriticalJobActiveDays) {
		this.jobFeedCriticalJobActiveDays = jobFeedCriticalJobActiveDays;
	}

	public Integer getJobFeedAttentionJobActiveDays() {
		return jobFeedAttentionJobActiveDays;
	}

	public void setJobFeedAttentionJobActiveDays(
			Integer jobFeedAttentionJobActiveDays) {
		this.jobFeedAttentionJobActiveDays = jobFeedAttentionJobActiveDays;
	}

	public Integer getJobFeedCriticalCandidateRatio() {
		return jobFeedCriticalCandidateRatio;
	}

	public void setJobFeedCriticalCandidateRatio(
			Integer jobFeedCriticalCandidateRatio) {
		this.jobFeedCriticalCandidateRatio = jobFeedCriticalCandidateRatio;
	}

	public Boolean getJobFeedAttentionJobNotFilled() {
		return jobFeedAttentionJobNotFilled;
	}

	public void setJobFeedAttentionJobNotFilled(Boolean jobFeedAttentionJobNotFilled) {
		this.jobFeedAttentionJobNotFilled = jobFeedAttentionJobNotFilled;
	}

	public Boolean getJobCompletionNeeded() {
		return jobCompletionNeeded;
	}

	public void setJobCompletionNeeded(Boolean jobCompletionNeeded) {
		this.jobCompletionNeeded = jobCompletionNeeded;
	}

	public Boolean getStatusPrivilegeForHeadquarter() {
		return statusPrivilegeForHeadquarter;
	}

	public void setStatusPrivilegeForHeadquarter(
			Boolean statusPrivilegeForHeadquarter) {
		this.statusPrivilegeForHeadquarter = statusPrivilegeForHeadquarter;
	}

	public String[] getChkHQStatusMaster() {
		return chkHQStatusMaster;
	}

	public void setChkHQStatusMaster(String[] chkHQStatusMaster) {
		this.chkHQStatusMaster = chkHQStatusMaster;
	}

	public String[] getChkHQSecondaryStatusName() {
		return chkHQSecondaryStatusName;
	}

	public void setChkHQSecondaryStatusName(String[] chkHQSecondaryStatusName) {
		this.chkHQSecondaryStatusName = chkHQSecondaryStatusName;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj==null) return false;
		if(obj==this)return true;
		if(!(obj instanceof HeadQuarterMaster)) return false;
		HeadQuarterMaster headQuarterMaster = (HeadQuarterMaster) obj;
		if(this.headQuarterId.equals(headQuarterMaster.headQuarterId))
		{
			return true;
		}
		else
		{
			return false;
		}		
	}
	
	@Override
	public int hashCode() {
		return headQuarterId;
	}
	
	@Override
	public String toString() {
		//return "HeadQuarterMasterId :: "+this.headQuarterId+" HeadQuarterName :: "+this.headQuarterName;
		return this.headQuarterName;
	}
	
}
