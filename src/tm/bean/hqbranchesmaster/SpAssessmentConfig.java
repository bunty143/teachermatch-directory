package tm.bean.hqbranchesmaster;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.master.DistrictMaster;
import tm.bean.master.StateMaster;

@Entity
@Table(name="spassessmentconfig")
public class SpAssessmentConfig implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1409121702072482816L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer spAssessmentConfigId;
	
	@ManyToOne
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private StateMaster stateMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private String assessmentName;
	
	private String assessmentCode;

	public Integer getSpAssessmentConfigId() {
		return spAssessmentConfigId;
	}

	public void setSpAssessmentConfigId(Integer spAssessmentConfigId) {
		this.spAssessmentConfigId = spAssessmentConfigId;
	}

	public StateMaster getStateMaster() {
		return stateMaster;
	}

	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public String getAssessmentName() {
		return assessmentName;
	}

	public void setAssessmentName(String assessmentName) {
		this.assessmentName = assessmentName;
	}

	public String getAssessmentCode() {
		return assessmentCode;
	}

	public void setAssessmentCode(String assessmentCode) {
		this.assessmentCode = assessmentCode;
	}

}
