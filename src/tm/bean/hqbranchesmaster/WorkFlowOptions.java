package tm.bean.hqbranchesmaster;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="workflowoptions")
public class WorkFlowOptions implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2914215763943930174L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer flowOptionId;
	
	private String flowOptionName;

	public Integer getFlowOptionId() {
		return flowOptionId;
	}

	public void setFlowOptionId(Integer flowOptionId) {
		this.flowOptionId = flowOptionId;
	}

	public String getFlowOptionName() {
		return flowOptionName;
	}

	public void setFlowOptionName(String flowOptionName) {
		this.flowOptionName = flowOptionName;
	}
	
}
