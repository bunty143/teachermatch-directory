package tm.bean.hqbranchesmaster;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.QuestionOptions;
import tm.bean.assessment.QuestionsPool;

@Entity
@Table(name="linktoksntalentstemp")
public class LinkToKsnTalentsTemp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3164310493217515497L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long talenttempid;
	private String talentId; 
	private String createdDateTime;
	private String jobId;      
	private String talent_first_name;  
	private String talent_last_name;  
	private String talent_email;  
	private String ksnId;
	private String tmksnId;
	
	private String errortext; 
	private String sessionid;  
	private Date date;
	
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getTalentId() {
		return talentId;
	}
	public void setTalentId(String talentId) {
		this.talentId = talentId;
	}
	public String getErrortext() {
		return errortext;
	}
	public void setErrortext(String errortext) {
		this.errortext = errortext;
	}
	public Long getTalenttempid() {
		return talenttempid;
	}
	public void setTalenttempid(Long talenttempid) {
		this.talenttempid = talenttempid;
	}
	public String getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(String createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	public String getTalent_first_name() {
		return talent_first_name;
	}
	public void setTalent_first_name(String talentFirstName) {
		talent_first_name = talentFirstName;
	}
	public String getTalent_last_name() {
		return talent_last_name;
	}
	public void setTalent_last_name(String talentLastName) {
		talent_last_name = talentLastName;
	}
	public String getTalent_email() {
		return talent_email;
	}
	public void setTalent_email(String talentEmail) {
		talent_email = talentEmail;
	}
	public String getKsnId() {
		return ksnId;
	}
	public void setKsnId(String ksnId) {
		this.ksnId = ksnId;
	}
	public void setTmksnId(String tmksnId) {
		this.tmksnId = tmksnId;
	}
	public String getTmksnId() {
		return tmksnId;
	}
	public String getSessionid() {
		return sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	

}
