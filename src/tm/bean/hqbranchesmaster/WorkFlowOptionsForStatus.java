package tm.bean.hqbranchesmaster;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="workflowoptionsforstatus")
public class WorkFlowOptionsForStatus implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7076369682916687839L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer flowoptionsforstatusId;   
	
	private String flowOptionIds;
	
	private Integer  teacherId;
	
	private Integer  jobId;
	
	private Integer  statusId;
	
	private Integer  secondaryStatusId;
	
	private Integer  createdBy;
	
	private String status;
	
	private Date updatedDateTime; 
	private Date createdDateTime;
	
	public String getFlowOptionIds() {
		return flowOptionIds;
	}
	public void setFlowOptionIds(String flowOptionIds) {
		this.flowOptionIds = flowOptionIds;
	}
	public Integer getFlowoptionsforstatusId() {
		return flowoptionsforstatusId;
	}
	public void setFlowoptionsforstatusId(Integer flowoptionsforstatusId) {
		this.flowoptionsforstatusId = flowoptionsforstatusId;
	}
	public Integer getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}
	public Integer getJobId() {
		return jobId;
	}
	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public Integer getSecondaryStatusId() {
		return secondaryStatusId;
	}
	public void setSecondaryStatusId(Integer secondaryStatusId) {
		this.secondaryStatusId = secondaryStatusId;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
}
