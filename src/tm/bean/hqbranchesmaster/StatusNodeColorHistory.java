package tm.bean.hqbranchesmaster;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.mq.MQEvent;
import tm.bean.user.UserMaster;



@Entity
@Table(name="statusnodecolorhistory")
public class StatusNodeColorHistory implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 4292880346094825995L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer statusNodeColorHistoryId;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;
	
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	private Integer statusId;
	private Integer secondaryStatusId;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	@ManyToOne
	@JoinColumn(name="mqEventId",referencedColumnName="eventId")
	private MQEvent mqEvent;
	
	private String prevColor;
	private String currentColor;
	private Date createdDateTime;
	private Date updateDateTime;
	private String ipAddress;
	private String milestoneName;
	
	public String getMilestoneName() {
		return milestoneName;
	}
	public void setMilestoneName(String milestoneName) {
		this.milestoneName = milestoneName;
	}
	public Integer getStatusNodeColorHistoryId() {
		return statusNodeColorHistoryId;
	}
	public void setStatusNodeColorHistoryId(Integer statusNodeColorHistoryId) {
		this.statusNodeColorHistoryId = statusNodeColorHistoryId;
	}
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public MQEvent getMqEvent() {
		return mqEvent;
	}
	public void setMqEvent(MQEvent mqEvent) {
		this.mqEvent = mqEvent;
	}
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public Integer getSecondaryStatusId() {
		return secondaryStatusId;
	}
	public void setSecondaryStatusId(Integer secondaryStatusId) {
		this.secondaryStatusId = secondaryStatusId;
	}
	public String getPrevColor() {
		return prevColor;
	}
	public void setPrevColor(String prevColor) {
		this.prevColor = prevColor;
	}
	public String getCurrentColor() {
		return currentColor;
	}
	public void setCurrentColor(String currentColor) {
		this.currentColor = currentColor;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Date getUpdateDateTime() {
		return updateDateTime;
	}
	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	
	@Override
	public String toString() {
		return this.statusNodeColorHistoryId+"";
	}
	
	@Override
	public int hashCode() {
		return this.statusNodeColorHistoryId;
	}
	
	public static Comparator<StatusNodeColorHistory> statusNodeColorCreatedDateComparatorDesc  = new Comparator<StatusNodeColorHistory>(){
		public int compare(StatusNodeColorHistory s1, StatusNodeColorHistory s2) {			
			return s2.getCreatedDateTime().compareTo(s1.getCreatedDateTime());
		}		
	};
	
	public static Comparator<StatusNodeColorHistory> statusNodeColorUpdatedDateComparatorDesc  = new Comparator<StatusNodeColorHistory>(){
		public int compare(StatusNodeColorHistory s1, StatusNodeColorHistory s2) {			
			return s2.getUpdateDateTime().compareTo(s1.getUpdateDateTime());
		}		
	};
}
