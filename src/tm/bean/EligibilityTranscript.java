package tm.bean;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.master.DistrictMaster;
import tm.bean.master.EligibilityMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="eligibilitytranscript")
public class EligibilityTranscript implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2481397997315389287L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer transcriptId;
	private Date createdDateTime;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;

	@ManyToOne
	@JoinColumn(name="eligibilityId",referencedColumnName="eligibilityId")
	private EligibilityMaster eligibilityMaster;
	
	@ManyToOne
	@JoinColumn(name="activityDoneBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	@ManyToOne
	@JoinColumn(name="eligibilityStatusId",referencedColumnName="eligibilityStatusId")
	private EligibilityStatusMaster eligibilityStatusMaster;
	
	@ManyToOne
	@JoinColumn(name="academicId",referencedColumnName="academicId")
	private TeacherAcademics teacherAcademics;

	public Integer getTranscriptId() {
		return transcriptId;
	}

	public void setTranscriptId(Integer transcriptId) {
		this.transcriptId = transcriptId;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public EligibilityMaster getEligibilityMaster() {
		return eligibilityMaster;
	}

	public void setEligibilityMaster(EligibilityMaster eligibilityMaster) {
		this.eligibilityMaster = eligibilityMaster;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public EligibilityStatusMaster getEligibilityStatusMaster() {
		return eligibilityStatusMaster;
	}

	public void setEligibilityStatusMaster(
			EligibilityStatusMaster eligibilityStatusMaster) {
		this.eligibilityStatusMaster = eligibilityStatusMaster;
	}

	public TeacherAcademics getTeacherAcademics() {
		return teacherAcademics;
	}

	public void setTeacherAcademics(TeacherAcademics teacherAcademics) {
		this.teacherAcademics = teacherAcademics;
	}

}
