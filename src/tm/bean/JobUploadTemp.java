package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.QuestionOptions;
import tm.bean.assessment.QuestionsPool;
import tm.bean.hqbranchesmaster.BranchMaster;

@Entity
@Table(name="jobuploadtemp")
public class JobUploadTemp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5770093133464462134L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long jobtempid;
	private String created_date;
	private String job_id;      
	private String job_type;  
	private String name;  
	private String job_start_date;  
	private String job_end_date;  
	private String no_exp_hires;  
	private String school_name;
	private String errortext;
	private Integer exist_jobId;
	private String sessionid;
	private Integer districtId;
	private String read_write_previlege;
	private Integer schoolId;
	private String job_status;
	private Date date;
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	
	public String getJob_status() {
		return job_status;
	}
	public void setJob_status(String jobStatus) {
		job_status = jobStatus;
	}
	
	public String getRead_write_previlege() {
		return read_write_previlege;
	}
	public void setRead_write_previlege(String readWritePrevilege) {
		read_write_previlege = readWritePrevilege;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String createdDate) {
		created_date = createdDate;
	}
	public String getJob_start_date() {
		return job_start_date;
	}
	public void setJob_start_date(String jobStartDate) {
		job_start_date = jobStartDate;
	}
	public String getJob_end_date() {
		return job_end_date;
	}
	public void setJob_end_date(String jobEndDate) {
		job_end_date = jobEndDate;
	}
	public String getSessionid() {
		return sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public Integer getExist_jobId() {
		return exist_jobId;
	}
	public void setExist_jobId(Integer existJobId) {
		exist_jobId = existJobId;
	}
	public Long getJobtempid() {
		return jobtempid;
	}
	public void setJobtempid(Long jobtempid) {
		this.jobtempid = jobtempid;
	}
	
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String jobId) {
		job_id = jobId;
	}
	public String getJob_type() {
		return job_type;
	}
	public void setJob_type(String jobType) {
		job_type = jobType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNo_exp_hires() {
		return no_exp_hires;
	}
	public void setNo_exp_hires(String noExpHires) {
		no_exp_hires = noExpHires;
	}
	public String getSchool_name() {
		return school_name;
	}
	public void setSchool_name(String schoolName) {
		school_name = schoolName;
	}
	public String getErrortext() {
		return errortext;
	}
	public void setErrortext(String errortext) {
		this.errortext = errortext;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	
	

}
