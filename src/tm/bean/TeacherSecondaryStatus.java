package tm.bean;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teachersecondarystatus")
public class TeacherSecondaryStatus implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8244810687986547939L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherStatusId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="secStatusId",referencedColumnName="secStatusId")
	private SecondaryStatusMaster secondaryStatusMaster;
	
	@ManyToOne
	@JoinColumn(name="createdByUser",referencedColumnName="userId")
	private UserMaster createdByUser;
	
	private Integer createdByEntity;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private Integer headQuarterId;
	private Integer branchId;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	private Date createdDateTime;

	public Integer getTeacherStatusId() {
		return teacherStatusId;
	}
	public void setTeacherStatusId(Integer teacherStatusId) {
		this.teacherStatusId = teacherStatusId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public SecondaryStatusMaster getSecondaryStatusMaster() {
		return secondaryStatusMaster;
	}
	public void setSecondaryStatusMaster(SecondaryStatusMaster secondaryStatusMaster) {
		this.secondaryStatusMaster = secondaryStatusMaster;
	}
	public UserMaster getCreatedByUser() {
		return createdByUser;
	}
	public void setCreatedByUser(UserMaster createdByUser) {
		this.createdByUser = createdByUser;
	}
	public Integer getCreatedByEntity() {
		return createdByEntity;
	}
	public void setCreatedByEntity(Integer createdByEntity) {
		this.createdByEntity = createdByEntity;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	public Integer getHeadQuarterId() {
		return headQuarterId;
	}
	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof TeacherSecondaryStatus))return false;
	    TeacherSecondaryStatus teacherSecondaryStatus = (TeacherSecondaryStatus)object;
	  
	    if(this.teacherStatusId.equals(teacherSecondaryStatus.getTeacherStatusId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+teacherStatusId);
	}
}
