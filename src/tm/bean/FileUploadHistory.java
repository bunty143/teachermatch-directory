package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.UserUploadFolderAccess;

@Entity
@Table(name="fileuploadhistory")
public class FileUploadHistory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2858319540271372700L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer excelUploadHistoryId;
	
	@ManyToOne
	@JoinColumn(name="useuploadFolderId",referencedColumnName="useuploadFolderId")
	private UserUploadFolderAccess useuploadFolderId;	
	private Integer insertRecord;
	private Integer updateRecord;
	private Integer numOfError;
	private Date dateTime;
	
	public Integer getExcelUploadHistoryId() {
		return excelUploadHistoryId;
	}
	public void setExcelUploadHistoryId(Integer excelUploadHistoryId) {
		this.excelUploadHistoryId = excelUploadHistoryId;
	}
	public UserUploadFolderAccess getUseuploadFolderId() {
		return useuploadFolderId;
	}
	public void setUseuploadFolderId(UserUploadFolderAccess useuploadFolderId) {
		this.useuploadFolderId = useuploadFolderId;
	}
	public Integer getInsertRecord() {
		return insertRecord;
	}
	public void setInsertRecord(Integer insertRecord) {
		this.insertRecord = insertRecord;
	}
	public Integer getUpdateRecord() {
		return updateRecord;
	}
	public void setUpdateRecord(Integer updateRecord) {
		this.updateRecord = updateRecord;
	}
	public Integer getNumOfError() {
		return numOfError;
	}
	public void setNumOfError(Integer numOfError) {
		this.numOfError = numOfError;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
	
	

}
