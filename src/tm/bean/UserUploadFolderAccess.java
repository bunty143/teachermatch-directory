package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;

@Entity
@Table(name="useruploadfolderaccessdetails")
public class UserUploadFolderAccess implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4162506152439216565L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer useuploadFolderId;
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtId;
	
    private String folderPath;
    private String nameOfLastFileUploaded;
    private Date lastUploadDateTime;
    private String status;
    private String functionality;
	public Integer getUseuploadFolderId() {
		return useuploadFolderId;
	}
	public void setUseuploadFolderId(Integer useuploadFolderId) {
		this.useuploadFolderId = useuploadFolderId;
	}
	public DistrictMaster getDistrictId() {
		return districtId;
	}
	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}
	public String getFolderPath() {
		return folderPath;
	}
	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}
	public String getNameOfLastFileUploaded() {
		return nameOfLastFileUploaded;
	}
	public void setNameOfLastFileUploaded(String nameOfLastFileUploaded) {
		this.nameOfLastFileUploaded = nameOfLastFileUploaded;
	}
	public Date getLastUploadDateTime() {
		return lastUploadDateTime;
	}
	public void setLastUploadDateTime(Date lastUploadDateTime) {
		this.lastUploadDateTime = lastUploadDateTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFunctionality() {
		return functionality;
	}
	public void setFunctionality(String functionality) {
		this.functionality = functionality;
	}

    
}
