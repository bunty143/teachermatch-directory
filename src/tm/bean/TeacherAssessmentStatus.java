package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teacherassessmentstatus")
public class TeacherAssessmentStatus implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7875017791267588649L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherAssessmentStatusId;  
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;   
	
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentDetail;     
	
	@ManyToOne
	@JoinColumn(name="teacherAssessmentId",referencedColumnName="teacherAssessmentId")
	private TeacherAssessmentdetail teacherAssessmentdetail;  
	
	private Integer assessmentType;     
	
	@ManyToOne
	@JoinColumn(name="statusId",referencedColumnName="statusId")
	private StatusMaster statusMaster;   
	private String pass;  
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="updatedBy",referencedColumnName="userId")
	private UserMaster updatedBy; 
	private Integer updatedByEntity; 
	private Date updatedDate; 
	private Boolean cgUpdated;
	private Date createdDateTime;
	private Date assessmentCompletedDateTime;
	private Integer assessmentTakenCount;
	private Date createdDateTimeOld;
	
	public Integer getTeacherAssessmentStatusId() {
		return teacherAssessmentStatusId;
	}
	public void setTeacherAssessmentStatusId(Integer teacherAssessmentStatusId) {
		this.teacherAssessmentStatusId = teacherAssessmentStatusId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public AssessmentDetail getAssessmentDetail() {
		return assessmentDetail;
	}
	public void setAssessmentDetail(AssessmentDetail assessmentDetail) {
		this.assessmentDetail = assessmentDetail;
	}
	
	public TeacherAssessmentdetail getTeacherAssessmentdetail() {
		return teacherAssessmentdetail;
	}
	public void setTeacherAssessmentdetail(
			TeacherAssessmentdetail teacherAssessmentdetail) {
		this.teacherAssessmentdetail = teacherAssessmentdetail;
	}
	public Integer getAssessmentType() {
		return assessmentType;
	}
	public void setAssessmentType(Integer assessmentType) {
		this.assessmentType = assessmentType;
	}
	
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public StatusMaster getStatusMaster() {
		return statusMaster;
	}
	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public UserMaster getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(UserMaster updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getUpdatedByEntity() {
		return updatedByEntity;
	}
	public void setUpdatedByEntity(Integer updatedByEntity) {
		this.updatedByEntity = updatedByEntity;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Boolean getCgUpdated() {
		return cgUpdated;
	}
	public void setCgUpdated(Boolean cgUpdated) {
		this.cgUpdated = cgUpdated;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Date getAssessmentCompletedDateTime() {
        return assessmentCompletedDateTime;
	}
	public void setAssessmentCompletedDateTime(Date assessmentCompletedDateTime) {
        this.assessmentCompletedDateTime = assessmentCompletedDateTime;
	}
	public Integer getAssessmentTakenCount() {
		return assessmentTakenCount;
	}
	public void setAssessmentTakenCount(Integer assessmentTakenCount) {
		this.assessmentTakenCount = assessmentTakenCount;
	}
	public Date getCreatedDateTimeOld() {
		return createdDateTimeOld;
	}
	public void setCreatedDateTimeOld(Date createdDateTimeOld) {
		this.createdDateTimeOld = createdDateTimeOld;
	}
	
}
