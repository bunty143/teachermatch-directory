package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="statuswiseautoemailsend")
public class StatusWiseAutoEmailSend implements Serializable {

	
	private static final long serialVersionUID = 2923050057790965462L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer autoStatusEmailId;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster  headQuarterMaster;
	
	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster  branchMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster  districtId;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster usermaster;
	
	private Integer statusId;
	
	private Integer secondaryStatusId;
	
	private Integer  autoEmailRequired;
	
	private Date createdDateTime;

	public Integer getAutoStatusEmailId() {
		return autoStatusEmailId;
	}

	public void setAutoStatusEmailId(Integer autoStatusEmailId) {
		this.autoStatusEmailId = autoStatusEmailId;
	}

	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}

	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}

	public DistrictMaster getDistrictId() {
		return districtId;
	}

	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}

	public UserMaster getUsermaster() {
		return usermaster;
	}

	public void setUsermaster(UserMaster usermaster) {
		this.usermaster = usermaster;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getSecondaryStatusId() {
		return secondaryStatusId;
	}

	public void setSecondaryStatusId(Integer secondaryStatusId) {
		this.secondaryStatusId = secondaryStatusId;
	}

	public Integer getAutoEmailRequired() {
		return autoEmailRequired;
	}

	public void setAutoEmailRequired(Integer autoEmailRequired) {
		this.autoEmailRequired = autoEmailRequired;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}  
	

}
