package tm.bean.cgreport;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="percentilenationalcompositetscore")
public class PercentileNationalCompositeTScore implements Comparable<PercentileNationalCompositeTScore>,Serializable{
	
	private static final long serialVersionUID = 5550247624973380582L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;	
	private Double compositeTValue;
	private Integer frequency;
	private Integer cumulativeFrequency;
	private Date createdDateTime;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getCompositeTValue() {
		return compositeTValue;
	}
	public void setCompositeTValue(Double compositeTValue) {
		this.compositeTValue = compositeTValue;
	}
	public Integer getFrequency() {
		return frequency;
	}
	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}
	public Integer getCumulativeFrequency() {
		return cumulativeFrequency;
	}
	public void setCumulativeFrequency(Integer cumulativeFrequency) {
		this.cumulativeFrequency = cumulativeFrequency;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}	
	public int compareTo(PercentileNationalCompositeTScore pcal) {
		return this.compositeTValue .compareTo(pcal.compositeTValue);		
	}
}
