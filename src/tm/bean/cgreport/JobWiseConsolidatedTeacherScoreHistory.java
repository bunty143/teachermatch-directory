package tm.bean.cgreport;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;

@Entity
@Table(name="jobwiseconsolidatedteacherscorehistory")
public class JobWiseConsolidatedTeacherScoreHistory implements Serializable
{
	
	private static final long serialVersionUID = 1316202145844251449L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherConsolidatedScoreId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	private Integer jobWiseConsolidatedScore;
	private Integer jobWiseMaxScore;
	private Date updatedDateTime;

	public Integer getJobWiseMaxScore() {
		return jobWiseMaxScore;
	}

	public void setJobWiseMaxScore(Integer jobWiseMaxScore) {
		this.jobWiseMaxScore = jobWiseMaxScore;
	}

	public Integer getTeacherConsolidatedScoreId() {
		return teacherConsolidatedScoreId;
	}

	public void setTeacherConsolidatedScoreId(Integer teacherConsolidatedScoreId) {
		this.teacherConsolidatedScoreId = teacherConsolidatedScoreId;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public Integer getJobWiseConsolidatedScore() {
		return jobWiseConsolidatedScore;
	}

	public void setJobWiseConsolidatedScore(Integer jobWiseConsolidatedScore) {
		this.jobWiseConsolidatedScore = jobWiseConsolidatedScore;
	}

	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	
	
}
