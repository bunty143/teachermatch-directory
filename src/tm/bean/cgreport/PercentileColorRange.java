package tm.bean.cgreport;

import java.io.Serializable;

public class PercentileColorRange implements Serializable
{
	private static final long serialVersionUID = -5379810772257847157L;
	private double LScore;
	private double UScore;
	private double TScore;
	private int frequency;     
	private int cumulativeFrequency;
	private int colorName;
	private double widthDecile;
	private double LDecile;
	private double UDecile;
	
	public double getLDecile() {
		return LDecile;
	}
	public void setLDecile(double lDecile) {
		LDecile = lDecile;
	}
	public double getUDecile() {
		return UDecile;
	}
	public void setUDecile(double uDecile) {
		UDecile = uDecile;
	}
	public double getWidthDecile() {
		return widthDecile;
	}
	public void setWidthDecile(double widthDecile) {
		this.widthDecile = widthDecile;
	}
	public int getColorName() {
		return colorName;
	}
	public void setColorName(int colorName) {
		this.colorName = colorName;
	}
	public double getLScore() {
		return LScore;
	}
	public void setLScore(double lScore) {
		LScore = lScore;
	}
	public double getUScore() {
		return UScore;
	}
	public void setUScore(double uScore) {
		UScore = uScore;
	}
	public double getTScore() {
		return TScore;
	}
	public void setTScore(double tScore) {
		TScore = tScore;
	}
	public int getFrequency() {
		return frequency;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public int getCumulativeFrequency() {
		return cumulativeFrequency;
	}
	public void setCumulativeFrequency(int cumulativeFrequency) {
		this.cumulativeFrequency = cumulativeFrequency;
	}
	
	
}
