package tm.bean.cgreport;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="zscore")
public class ZScore implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7927685484322246424L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id; 
	private Double percentile; 	
	private Double zScore;
	private Double tScore;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getPercentile() {
		return percentile;
	}
	public void setPercentile(Double percentile) {
		this.percentile = percentile;
	}
	public Double getzScore() {
		return zScore;
	}
	public void setzScore(Double zScore) {
		this.zScore = zScore;
	}
	public Double gettScore() {
		return tScore;
	}
	public void settScore(Double tScore) {
		this.tScore = tScore;
	}
}
