package tm.bean.cgreport;

import java.io.Serializable;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.utility.Utility;


public class UserMailSend implements Serializable
{
	private static final long serialVersionUID = 5774723825707602348L;
	private String email;
	private String fromUserName;   
	private int isUserOrTeacherFlag;// For Any User :2, Teacher : =4
	private int isEmailTemplateChangedTeacher;
	private String toUserName;     
	private String statusNote;
	private String teacherName;
	private String jobTitle;
	private String gridURL;
	private String statusShortName;
	private String statusName;
	private String previousName;
	UserMaster userMaster;
	private String subject;
	private String mailBody;
	private String offerAcceptURL;
	private String offerDeclineURL;
	private UserMaster userMasterForOther;
	private String bccEmail[];
	private String locationCode;
	private String panelExist;
	
	/* ==== Adding Fields for savinng message in message to teacher table ======*/
	private TeacherDetail teacherDetail;
	private JobOrder jobOrder;
	private String requisitionNumber;
	private String schoolLocation;
	private Integer flag;
	private EmailerService emailService;
	private Integer internal; 
	

	public String getPanelExist() {
		return panelExist;
	}
	public void setPanelExist(String panelExist) {
		this.panelExist = panelExist;
	}
	public Integer getInternal() {
		return internal;
	}
	public void setInternal(Integer internal) {
		this.internal = internal;
	}
	public String[] getBccEmail() {
		return bccEmail;
	}
	public void setBccEmail(String[] bccEmail) {
		this.bccEmail = bccEmail;
	}
	public UserMaster getUserMasterForOther() {
		return userMasterForOther;
	}
	public void setUserMasterForOther(UserMaster userMasterForOther) {
		this.userMasterForOther = userMasterForOther;
	}
	public String getSchoolLocation() {
		return schoolLocation;
	}
	public void setSchoolLocation(String schoolLocation) {
		this.schoolLocation = schoolLocation;
	}
	public String getRequisitionNumber() {
		return requisitionNumber;
	}
	public void setRequisitionNumber(String requisitionNumber) {
		this.requisitionNumber = requisitionNumber;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public String getOfferAcceptURL() {
		return offerAcceptURL;
	}
	public void setOfferAcceptURL(String offerAcceptURL) {
		this.offerAcceptURL = offerAcceptURL;
	}
	public String getOfferDeclineURL() {
		return offerDeclineURL;
	}
	public void setOfferDeclineURL(String offerDeclineURL) {
		this.offerDeclineURL = offerDeclineURL;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getPreviousName() {
		return previousName;
	}
	public void setPreviousName(String previousName) {
		this.previousName = previousName;
	}
	public String getGridURL() {
		return gridURL;
	}
	public void setGridURL(String gridURL) {
		this.gridURL = gridURL;
	}
	public String getStatusShortName() {
		return statusShortName;
	}
	public void setStatusShortName(String statusShortName) {
		this.statusShortName = statusShortName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFromUserName() {
		return fromUserName;
	}
	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}
	public int getIsUserOrTeacherFlag() {
		return isUserOrTeacherFlag;
	}
	public void setIsUserOrTeacherFlag(int isUserOrTeacherFlag) {
		this.isUserOrTeacherFlag = isUserOrTeacherFlag;
	}
	public int getIsEmailTemplateChangedTeacher() {
		return isEmailTemplateChangedTeacher;
	}
	public void setIsEmailTemplateChangedTeacher(int isEmailTemplateChangedTeacher) {
		this.isEmailTemplateChangedTeacher = isEmailTemplateChangedTeacher;
	}
	public String getToUserName() {
		return toUserName;
	}
	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}
	public String getStatusNote() {
		return statusNote;
	}
	public void setStatusNote(String statusNote) {
		this.statusNote = statusNote;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMailBody() {
		return mailBody;
	}
	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}     

	public EmailerService getEmailService() {
		return emailService;
	}
	public void setEmailService(EmailerService emailService) {
		this.emailService = emailService;
	}
	
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	
		
}
