package tm.bean.cgreport;

import java.io.BufferedReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import sun.nio.cs.StandardCharsets;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teacherphonecallhistory")
public class TeacherPhoneCallHistory implements Serializable
{

	private static final long serialVersionUID = 3499073619913664839L;


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherPhoneCallId;  
	
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster userMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	private String phoneCallDetail;
	private String phoneFileName;
	private Date callDateTime;
	
	
	
	public String getPhoneFileName() {
		return phoneFileName;
	}
	public void setPhoneFileName(String phoneFileName) {
		this.phoneFileName = phoneFileName;
	}
	public Integer getTeacherPhoneCallId() {
		return teacherPhoneCallId;
	}
	public void setTeacherPhoneCallId(Integer teacherPhoneCallId) {
		this.teacherPhoneCallId = teacherPhoneCallId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public String getPhoneCallDetail() {
		return phoneCallDetail;
	}
	public void setPhoneCallDetail(String phoneCallDetail) {
		this.phoneCallDetail = phoneCallDetail;
	}
	public Date getCallDateTime() {
		return callDateTime;
	}
	public void setCallDateTime(Date callDateTime) {
		this.callDateTime = callDateTime;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}  
	
}
