package tm.bean.cgreport;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;

@Entity
@Table(name="rawdataforcompetency")
public class RawDataForCompetency implements Serializable{

	private static final long serialVersionUID = -4542177348732157264L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer rawDataId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")	
	private TeacherDetail teacherDetail;	
	@ManyToOne
	@JoinColumn(name="teacherAssessmentId",referencedColumnName="teacherAssessmentId")
	private TeacherAssessmentdetail teacherAssessmentdetail;
	@ManyToOne
	@JoinColumn(name="domainId",referencedColumnName="domainId")	
	private DomainMaster DomainMaster;	
	@ManyToOne
	@JoinColumn(name="competencyId",referencedColumnName="competencyId")
	private CompetencyMaster competencyMaster;	
	private Double score; 
	private Double maxMarks;
	@ManyToOne
	@JoinColumn(name="assessmentId",referencedColumnName="assessmentId")
	private AssessmentDetail assessmentDetail;
	private Integer assessmentType;
	private Integer assessmentTakenCount;
	private Date createdDateTime;
	private Date assessmentDateTime;
	private Date updateDateTime;
	
	public Integer getRawDataId() {
		return rawDataId;
	}
	public void setRawDataId(Integer rawDataId) {
		this.rawDataId = rawDataId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public DomainMaster getDomainMaster() {
		return DomainMaster;
	}
	public void setDomainMaster(DomainMaster domainMaster) {
		DomainMaster = domainMaster;
	}
	public CompetencyMaster getCompetencyMaster() {
		return competencyMaster;
	}
	public void setCompetencyMaster(CompetencyMaster competencyMaster) {
		this.competencyMaster = competencyMaster;
	}
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		this.score = score;
	}
	public Double getMaxMarks() {
		return maxMarks;
	}
	public void setMaxMarks(Double maxMarks) {
		this.maxMarks = maxMarks;
	}	
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public AssessmentDetail getAssessmentDetail() {
		return assessmentDetail;
	}
	public void setAssessmentDetail(AssessmentDetail assessmentDetail) {
		this.assessmentDetail = assessmentDetail;
	}
	public Integer getAssessmentType() {
		return assessmentType;
	}
	public void setAssessmentType(Integer assessmentType) {
		this.assessmentType = assessmentType;
	}
	public Integer getAssessmentTakenCount() {
		return assessmentTakenCount;
	}
	public void setAssessmentTakenCount(Integer assessmentTakenCount) {
		this.assessmentTakenCount = assessmentTakenCount;
	}
	public TeacherAssessmentdetail getTeacherAssessmentdetail() {
		return teacherAssessmentdetail;
	}
	public void setTeacherAssessmentdetail(
			TeacherAssessmentdetail teacherAssessmentdetail) {
		this.teacherAssessmentdetail = teacherAssessmentdetail;
	}
	public Date getAssessmentDateTime() {
		return assessmentDateTime;
	}
	public void setAssessmentDateTime(Date assessmentDateTime) {
		this.assessmentDateTime = assessmentDateTime;
	}
	public Date getUpdateDateTime() {
		return updateDateTime;
	}
	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
}