package tm.bean.cgreport;

import java.io.Serializable;
import java.util.Comparator;

public class PercentileZscoreTscore implements Serializable
{
	private static final long serialVersionUID = 688901026587985333L;
	private Double percentile;
	private Double zScore;
	private Double tScore;
	
	public Double gettScore() {
		return tScore;
	}
	public void settScore(Double tScore) {
		this.tScore = tScore;
	}
	public Double getPercentile() {
		return percentile;
	}
	public void setPercentile(Double percentile) {
		this.percentile = percentile;
	}
	public Double getzScore() {
		return zScore;
	}
	public void setzScore(Double zScore) {
		this.zScore = zScore;
	}
	public static Comparator<PercentileZscoreTscore> percentileZscoreTscore  = new Comparator<PercentileZscoreTscore>() {
		public int compare(PercentileZscoreTscore p1, PercentileZscoreTscore p2) {	
		return p1.getPercentile().compareTo(p2.getPercentile());
		}		
	};
	
		
}
