package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teacherachievementscore")
public class TeacherAchievementScore implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2441251487746310314L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherAchievementScoreId;
	
	@ManyToOne
	@JoinColumn(name="teacherId" ,referencedColumnName="teacherId")
	TeacherDetail teacherDetail;
	
	
	@ManyToOne
	@JoinColumn(name="districtId", referencedColumnName="districtId")
	DistrictMaster districtMaster;
	
	private Integer academicAchievementScore;
	
	private Integer academicAchievementMaxScore;
	private Integer leadershipAchievementScore;
	private Integer leadershipAchievementMaxScore;
	
	private Integer totalAchievementScore;
	private Integer achievementMaxScore;
	@ManyToOne
	@JoinColumn(name="scoredBy",referencedColumnName="userId")
	private UserMaster scoredBy;
	
	private Date scoredDateTime;

	public Integer getAcademicAchievementMaxScore() {
		return academicAchievementMaxScore;
	}

	public void setAcademicAchievementMaxScore(Integer academicAchievementMaxScore) {
		this.academicAchievementMaxScore = academicAchievementMaxScore;
	}

	public Integer getLeadershipAchievementMaxScore() {
		return leadershipAchievementMaxScore;
	}

	public void setLeadershipAchievementMaxScore(
			Integer leadershipAchievementMaxScore) {
		this.leadershipAchievementMaxScore = leadershipAchievementMaxScore;
	}

	public UserMaster getScoredBy() {
		return scoredBy;
	}

	public void setScoredBy(UserMaster scoredBy) {
		this.scoredBy = scoredBy;
	}

	public Integer getTeacherAchievementScoreId() {
		return teacherAchievementScoreId;
	}

	public void setTeacherAchievementScoreId(Integer teacherAchievementScoreId) {
		this.teacherAchievementScoreId = teacherAchievementScoreId;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public Integer getAcademicAchievementScore() {
		return academicAchievementScore;
	}

	public void setAcademicAchievementScore(Integer academicAchievementScore) {
		this.academicAchievementScore = academicAchievementScore;
	}

	public Integer getLeadershipAchievementScore() {
		return leadershipAchievementScore;
	}

	public void setLeadershipAchievementScore(Integer leadershipAchievementScore) {
		this.leadershipAchievementScore = leadershipAchievementScore;
	}

	public Integer getTotalAchievementScore() {
		return totalAchievementScore;
	}

	public void setTotalAchievementScore(Integer totalAchievementScore) {
		this.totalAchievementScore = totalAchievementScore;
	}

	public Integer getAchievementMaxScore() {
		return achievementMaxScore;
	}

	public void setAchievementMaxScore(Integer achievementMaxScore) {
		this.achievementMaxScore = achievementMaxScore;
	}

	public Date getScoredDateTime() {
		return scoredDateTime;
	}

	public void setScoredDateTime(Date scoredDateTime) {
		this.scoredDateTime = scoredDateTime;
	}	
	
}
