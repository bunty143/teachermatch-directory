package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;
@Entity
@Table(name="cmseventformatmaster")
public class CmsEventFormatMaster implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9113899385915899938L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer eventFormatId;
	private String eventFormatName;
	@ManyToOne
	@JoinColumn(name="eventTypeId",referencedColumnName="eventTypeId")
	private  CmsEventTypeMaster cmsEventTypeMaster;
	public Integer getEventFormatId() {
		return eventFormatId;
	}

	public void setEventFormatId(Integer interactionFormatId) {
		this.eventFormatId = interactionFormatId;
	}

	public String getEventFormatName() {
		return eventFormatName;
	}

	public CmsEventTypeMaster getCmsEventTypeMaster() {
		return cmsEventTypeMaster;
	}

	public void setCmsEventTypeMaster(CmsEventTypeMaster cmsEventTypeMaster) {
		this.cmsEventTypeMaster = cmsEventTypeMaster;
	}

	public void setEventFormatName(String interactionFormatName) {
		this.eventFormatName = interactionFormatName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public UserMaster getCreatedBY() {
		return createdBY;
	}

	public void setCreatedBY(UserMaster createdBY) {
		this.createdBY = createdBY;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	private String status;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBY;

	private Date createdDateTime;
}
