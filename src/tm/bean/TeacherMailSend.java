package tm.bean;

import java.io.Serializable;

import tm.bean.user.UserMaster;


public class TeacherMailSend implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1431189523719191612L;
	private String epiStatus;
	private UserMaster userMaster;   
	private String mailTo;     
	private TeacherDetail teacherDetail;
	private String jobTitle;     
	private boolean newUser;     
	private String schoolDistrictName;
	private MailToCandidateOnImport mailToCandidateOnImport;

	public String getEpiStatus() {
		return epiStatus;
	}
	public void setEpiStatus(String epiStatus) {
		this.epiStatus = epiStatus;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getMailTo() {
		return mailTo;
	}
	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public boolean isNewUser() {
		return newUser;
	}
	public void setNewUser(boolean newUser) {
		this.newUser = newUser;
	}
	public String getSchoolDistrictName() {
		return schoolDistrictName;
	}
	public void setSchoolDistrictName(String schoolDistrictName) {
		this.schoolDistrictName = schoolDistrictName;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public MailToCandidateOnImport getMailToCandidateOnImport() {
		return mailToCandidateOnImport;
	}
	public void setMailToCandidateOnImport(
			MailToCandidateOnImport mailToCandidateOnImport) {
		this.mailToCandidateOnImport = mailToCandidateOnImport;
	}
		
}
