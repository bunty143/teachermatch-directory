package tm.bean;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teacherstatushistoryforjobhistory")
public class TeacherStatusHistoryForJobHistory implements Serializable
{
	private static final long serialVersionUID = -4669637821202384225L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long teacherStatusHistoryForJobId;     
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;     

	@ManyToOne
	@JoinColumn(name="statusId",referencedColumnName="statusId")
	private StatusMaster statusMaster; 
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private String status;
	
	private Boolean override;
	@ManyToOne
	@JoinColumn(name="overrideBy",referencedColumnName="userId")
	private UserMaster overrideBy;
	
	private Date updatedDateTime; 
	private Date createdDateTime;
	private Date hiredByDate;
	
	private String actionTakenFrom;
	
	@Transient
	private Double normScore = -0.0;
	
	@Transient
	private String  requisitionNumber;
	
	
	public Date getHiredByDate() {
		return hiredByDate;
	}
	public void setHiredByDate(Date hiredByDate) {
		this.hiredByDate = hiredByDate;
	}
	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}
	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public Long getTeacherStatusHistoryForJobId() {
		return teacherStatusHistoryForJobId;
	}
	public void setTeacherStatusHistoryForJobId(Long teacherStatusHistoryForJobId) {
		this.teacherStatusHistoryForJobId = teacherStatusHistoryForJobId;
	}
	
	public StatusMaster getStatusMaster() {
		return statusMaster;
	}
	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Boolean getOverride() {
		return override;
	}
	public void setOverride(Boolean override) {
		this.override = override;
	}
	public UserMaster getOverrideBy() {
		return overrideBy;
	}
	public void setOverrideBy(UserMaster overrideBy) {
		this.overrideBy = overrideBy;
	}
	public Double getNormScore() {
		return normScore;
	}
	public void setNormScore(Double normScore) {
		this.normScore = normScore;
	}
	public String getRequisitionNumber() {
		return requisitionNumber;
	}
	public void setRequisitionNumber(String requisitionNumber) {
		this.requisitionNumber = requisitionNumber;
	}
	public String getActionTakenFrom() {
		return actionTakenFrom;
	}
	public void setActionTakenFrom(String actionTakenFrom) {
		this.actionTakenFrom = actionTakenFrom;
	}
	
}
