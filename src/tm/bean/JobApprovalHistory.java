package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import tm.bean.user.UserMaster;
@Entity
@Table(name="jobapprovalhistory")
public class JobApprovalHistory implements Serializable{
	
	private static final long serialVersionUID = -4678413602762774845L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer jobApprovalId;
	@ManyToOne
	@JoinColumn(name="approvedBy",referencedColumnName="userId")
	private UserMaster userMaster;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	private Date createdDateTime; 
	private String ipAddress;
	
	@ManyToOne
	@JoinColumn(name="districtApprovalGroupsId",referencedColumnName="districtApprovalGroupsId")
	private DistrictApprovalGroups districtApprovalGroups;
	
	public DistrictApprovalGroups getDistrictApprovalGroups() {
		return districtApprovalGroups;
	}
	public void setDistrictApprovalGroups(
			DistrictApprovalGroups districtApprovalGroups) {
		this.districtApprovalGroups = districtApprovalGroups;
	}
	public Integer getJobApprovalId() {
		return jobApprovalId;
	}
	public void setJobApprovalId(Integer jobApprovalId) {
		this.jobApprovalId = jobApprovalId;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	

}
