package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="panelmembers")
public class PanelMembers implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6396723897706492615L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer panelMemberId;
	
	@ManyToOne
	@JoinColumn(name="panelId",referencedColumnName="panelId")
	private DistrictPanelMaster districtPanelMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="userId")
	private UserMaster userMaster;

	public Integer getPanelMemberId() {
		return panelMemberId;
	}

	public void setPanelMemberId(Integer panelMemberId) {
		this.panelMemberId = panelMemberId;
	}

	public DistrictPanelMaster getDistrictPanelMaster() {
		return districtPanelMaster;
	}

	public void setDistrictPanelMaster(DistrictPanelMaster districtPanelMaster) {
		this.districtPanelMaster = districtPanelMaster;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}

	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	
}
