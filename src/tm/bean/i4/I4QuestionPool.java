package tm.bean.i4;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;

@Entity
@Table(name="questionpool")
public class I4QuestionPool implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1807301154750805245L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer ID;
	
	@ManyToOne
	@JoinColumn(name="DistrictID",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private Integer headQuarterId;
	
	
	private Integer branchId;
	
	private String QuestionText;
	private String I4QuestionID;
	private Date DateCreated;
	private String Status;
	
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	
	 
 	public Integer getHeadQuarterId() {
		return headQuarterId;
	}
	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	public String getQuestionText() {
	return QuestionText;
	}
	public void setQuestionText(String questionText) {
		QuestionText = questionText;
	}
	public String getI4QuestionID() {
		return I4QuestionID;
	}
	public void setI4QuestionID(String i4QuestionID) {
		I4QuestionID = i4QuestionID;
	}
	public Date getDateCreated() {
		return DateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		DateCreated = dateCreated;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}

	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof I4QuestionPool))return false;
	    I4QuestionPool jobOrder = (I4QuestionPool)object;
	  
	    if(this.ID.equals(jobOrder.getID()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+ID);
	}
}
