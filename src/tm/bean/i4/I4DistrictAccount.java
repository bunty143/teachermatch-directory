package tm.bean.i4;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;

@Entity
@Table(name="i4_districtaccount")
public class I4DistrictAccount implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -763914435557038849L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer ID;
	
	@ManyToOne
	@JoinColumn(name="DistrictID",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	
	private Integer headQuarterId;
	
	private Integer branchId;
	
	
	private String I4username;
	private String I4password;
	private Date CreatedDate;
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	 
 
	public Integer getHeadQuarterId() {
		return headQuarterId;
	}
	public void setHeadQuarterId(Integer headQuarterId) {
		this.headQuarterId = headQuarterId;
	}
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	public String getI4username() {
		return I4username;
	}
	public void setI4username(String i4username) {
		I4username = i4username;
	}
	public String getI4password() {
		return I4password;
	}
	public void setI4password(String i4password) {
		I4password = i4password;
	}
	public Date getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(Date createdDate) {
		CreatedDate = createdDate;
	}
}
