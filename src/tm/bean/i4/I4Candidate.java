package tm.bean.i4;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;

@Entity
@Table(name="i4_candidate")
public class I4Candidate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1721064685941331178L;	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer CandidateID;
	
	@ManyToOne
	@JoinColumn(name="TMdistrictId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="TMID",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	private String I4CandidateID;
	private Date DateCreated;
	private String userName;
	private String password;
	
	public Integer getCandidateID() {
		return CandidateID;
	}
	public void setCandidateID(Integer candidateID) {
		CandidateID = candidateID;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public String getI4CandidateID() {
		return I4CandidateID;
	}

	public void setI4CandidateID(String i4CandidateID) {
		I4CandidateID = i4CandidateID;
	}

	public Date getDateCreated() {
		return DateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		DateCreated = dateCreated;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
