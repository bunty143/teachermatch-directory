package tm.bean.i4;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;

@Entity
@Table(name="vviresponse")
public class VVIResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5400539903134889094L;

	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private String responseId;
	private String i4InviteId;
	private String interviewQuestionId;
	private String answerFile;
	private String answerUrl;
	private String duration;
	private String mp4Link;
	private String webmLink;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getResponseId() {
		return responseId;
	}
	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}
	
	public String getInterviewQuestionId() {
		return interviewQuestionId;
	}
	public void setInterviewQuestionId(String interviewQuestionId) {
		this.interviewQuestionId = interviewQuestionId;
	}
	public String getAnswerFile() {
		return answerFile;
	}
	public void setAnswerFile(String answerFile) {
		this.answerFile = answerFile;
	}
	public String getAnswerUrl() {
		return answerUrl;
	}
	public void setAnswerUrl(String answerUrl) {
		this.answerUrl = answerUrl;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getMp4Link() {
		return mp4Link;
	}
	public void setMp4Link(String mp4Link) {
		this.mp4Link = mp4Link;
	}
	public String getWebmLink() {
		return webmLink;
	}
	public void setWebmLink(String webmLink) {
		this.webmLink = webmLink;
	}
	public String getI4InviteId() {
		return i4InviteId;
	}
	public void setI4InviteId(String i4InviteId) {
		this.i4InviteId = i4InviteId;
	}
	
	
}
