package tm.bean;


import java.io.Serializable;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.master.DistrictMaster;
import tm.bean.TeacherDetail;


@Entity
@Table(name="districtspecificteachertfaoptions")
public class DistrictSpecificTeacherTfaOptions implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2190934097242701339L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtSpecificTeacherTfaOptionsId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	private String tfaOptions;
	private String othersText;
	
	
	public Integer getDistrictSpecificTeacherTfaOptionsId() {
		return districtSpecificTeacherTfaOptionsId;
	}
	public void setDistrictSpecificTeacherTfaOptionsId(
			Integer districtSpecificTeacherTfaOptionsId) {
		this.districtSpecificTeacherTfaOptionsId = districtSpecificTeacherTfaOptionsId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getTfaOptions() {
		return tfaOptions;
	}
	public void setTfaOptions(String tfaOptions) {
		this.tfaOptions = tfaOptions;
	}
	public String getOthersText() {
		return othersText;
	}
	public void setOthersText(String othersText) {
		this.othersText = othersText;
	}
	
}
