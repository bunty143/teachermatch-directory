package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.SchoolMaster;

@Entity
@Table(name="jobrequisitionnumbers")
public class JobRequisitionNumbers implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1731396793381066194L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer jobRequisitionId;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	@ManyToOne
	@JoinColumn(name="districtRequisitionId",referencedColumnName="districtRequisitionId")
	private DistrictRequisitionNumbers districtRequisitionNumbers;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	private Integer status;
	
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getJobRequisitionId() {
		return jobRequisitionId;
	}

	public void setJobRequisitionId(Integer jobRequisitionId) {
		this.jobRequisitionId = jobRequisitionId;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public DistrictRequisitionNumbers getDistrictRequisitionNumbers() {
		return districtRequisitionNumbers;
	}

	public void setDistrictRequisitionNumbers(
			DistrictRequisitionNumbers districtRequisitionNumbers) {
		this.districtRequisitionNumbers = districtRequisitionNumbers;
	}

	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}

	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	
	
}
