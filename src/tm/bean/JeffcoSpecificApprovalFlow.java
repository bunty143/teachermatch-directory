package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;

@Entity
@Table(name="jeffcospecificapprovalflow")
public class JeffcoSpecificApprovalFlow implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int jeffcoSpecificApprovalFlowId;
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	private String specialEdFlag;
	private String approvalFlow;
	private Date createdDateTime;
	private String status;
	
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public int getJeffcoSpecificApprovalFlowId() {
		return jeffcoSpecificApprovalFlowId;
	}
	public void setJeffcoSpecificApprovalFlowId(int jeffcoSpecificApprovalFlowId) {
		this.jeffcoSpecificApprovalFlowId = jeffcoSpecificApprovalFlowId;
	}
	public String getSpecialEdFlag() {
		return specialEdFlag;
	}
	public void setSpecialEdFlag(String specialEdFlag) {
		this.specialEdFlag = specialEdFlag;
	}
	public String getApprovalFlow() {
		return approvalFlow;
	}
	public void setApprovalFlow(String approvalFlow) {
		this.approvalFlow = approvalFlow;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}