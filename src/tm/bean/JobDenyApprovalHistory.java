package tm.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

/*
 CREATE TABLE `jobdenyapprovalhistory` (
  `jobDenyApprovalId` int(10) NOT NULL auto_increment,
  `jobApprovalId` int(10) NOT NULL,
  `approvedBy` int(5) NOT NULL COMMENT 'Foreign key of usermaster',
  `deniedBy` int(5) NOT NULL COMMENT 'Foreign key of usermaster',
  `jobId` int(6) NOT NULL COMMENT 'Foreign key of joborder',
  `ipAddress` varchar(200) default NULL,
  `denyIpAddress` varchar(200) default NULL,
  `comments` text default NULL,
  `createdDateTime` datetime NOT NULL,
  `createdDateTimeDeny` datetime NOT NULL,   
  `districtApprovalGroupsId` int(11) default NULL,
  PRIMARY KEY  (`jobDenyApprovalId`),
  KEY `districtApprovalGroupsId` (`districtApprovalGroupsId`),
  CONSTRAINT `jobapprovalhistory_bfk_1` FOREIGN KEY (`districtApprovalGroupsId`) REFERENCES `districtapprovalgroups` (`districtApprovalGroupsId`)
);
 */
@Entity
@Table(name="jobdenyapprovalhistory")
public class JobDenyApprovalHistory {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer jobDenyApprovalId;	
	private Integer jobApprovalId;
	@ManyToOne
	@JoinColumn(name="approvedBy",referencedColumnName="userId")
	private UserMaster userMaster;
	@ManyToOne
	@JoinColumn(name="deniedBy",referencedColumnName="userId")
	private UserMaster userMasterDeny;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	private String ipAddress;
	private String denyIpAddress;
	private String comments;
	private Date createdDateTime; 
	private Date createdDateTimeDeny; 	
	@ManyToOne
	@JoinColumn(name="districtApprovalGroupsId",referencedColumnName="districtApprovalGroupsId")
	private DistrictApprovalGroups districtApprovalGroups;
	public Integer getJobDenyApprovalId() {
		return jobDenyApprovalId;
	}
	public void setJobDenyApprovalId(Integer jobDenyApprovalId) {
		this.jobDenyApprovalId = jobDenyApprovalId;
	}
	public Integer getJobApprovalId() {
		return jobApprovalId;
	}
	public void setJobApprovalId(Integer jobApprovalId) {
		this.jobApprovalId = jobApprovalId;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public UserMaster getUserMasterDeny() {
		return userMasterDeny;
	}
	public void setUserMasterDeny(UserMaster userMasterDeny) {
		this.userMasterDeny = userMasterDeny;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getDenyIpAddress() {
		return denyIpAddress;
	}
	public void setDenyIpAddress(String denyIpAddress) {
		this.denyIpAddress = denyIpAddress;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Date getCreatedDateTimeDeny() {
		return createdDateTimeDeny;
	}
	public void setCreatedDateTimeDeny(Date createdDateTimeDeny) {
		this.createdDateTimeDeny = createdDateTimeDeny;
	}
	public DistrictApprovalGroups getDistrictApprovalGroups() {
		return districtApprovalGroups;
	}
	public void setDistrictApprovalGroups(
			DistrictApprovalGroups districtApprovalGroups) {
		this.districtApprovalGroups = districtApprovalGroups;
	}
	
	
}
