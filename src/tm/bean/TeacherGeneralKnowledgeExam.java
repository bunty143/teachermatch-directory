package tm.bean;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.master.*;

@Entity
@Table(name="teachergeneralknowledgeexam")
public class TeacherGeneralKnowledgeExam implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3410674151671421370L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer generalKnowledgeExamId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	private String generalKnowledgeExamStatus;
	
	private Date generalKnowledgeExamDate;
	
	private String generalKnowledgeScoreReport;
	
	private String generalExamNote;
	
	private Date createdDateTime;

	
	public Integer getGeneralKnowledgeExamId() {
		return generalKnowledgeExamId;
	}

	public void setGeneralKnowledgeExamId(Integer generalKnowledgeExamId) {
		this.generalKnowledgeExamId = generalKnowledgeExamId;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}


	public String getGeneralKnowledgeExamStatus() {
		return generalKnowledgeExamStatus;
	}

	public void setGeneralKnowledgeExamStatus(String generalKnowledgeExamStatus) {
		this.generalKnowledgeExamStatus = generalKnowledgeExamStatus;
	}

	public Date getGeneralKnowledgeExamDate() {
		return generalKnowledgeExamDate;
	}

	public void setGeneralKnowledgeExamDate(Date generalKnowledgeExamDate) {
		this.generalKnowledgeExamDate = generalKnowledgeExamDate;
	}

	public String getGeneralKnowledgeScoreReport() {
		return generalKnowledgeScoreReport;
	}

	public void setGeneralKnowledgeScoreReport(String generalKnowledgeScoreReport) {
		this.generalKnowledgeScoreReport = generalKnowledgeScoreReport;
	}
	
	public String getGeneralExamNote() {
		return generalExamNote;
	}

	public void setGeneralExamNote(String generalExamNote) {
		this.generalExamNote = generalExamNote;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
}
