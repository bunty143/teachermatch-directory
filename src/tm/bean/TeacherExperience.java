package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.TFAAffiliateMaster;
import tm.bean.master.TFARegionMaster;

@Entity
@Table(name="teacherexperience")
public class TeacherExperience implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1806529023088088722L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer expId;
	
	@ManyToOne
	@JoinColumn(name="teacherId", referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;     
	private Double expCertTeacherTraining;
	private Boolean isNonTeacher;
	private Boolean nationalBoardCert;     
	private Integer nationalBoardCertYear;     
	private String resume;
	
	@ManyToOne
	@JoinColumn(name="tfaAffiliateId", referencedColumnName="tfaAffiliateId")
	private TFAAffiliateMaster tfaAffiliateMaster;  
	
	private Integer corpsYear;
	private Integer potentialQuality;
	private Boolean knowOtherLanguages;
	private Boolean isDocumentOnFile;
	private String customquestion;

	public String getCustomquestion() {
		return customquestion;
	}
	public void setCustomquestion(String customquestion) {
		this.customquestion = customquestion;
	}
	@ManyToOne
	@JoinColumn(name="tfaRegionId", referencedColumnName="tfaRegionId")
	private TFARegionMaster tfaRegionMaster;  
	
	private Date createdDateTime;
	
	private Integer canServeAsSubTeacher;
	
	public Integer getExpId() {
		return expId;
	}
	public void setExpId(Integer expId) {
		this.expId = expId;
	}
	public TeacherDetail getTeacherId() {
		return teacherDetail;
	}
	public void setTeacherId(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	
	public Double getExpCertTeacherTraining() {
		return expCertTeacherTraining;
	}
	public void setExpCertTeacherTraining(Double expCertTeacherTraining) {
		this.expCertTeacherTraining = expCertTeacherTraining;
	}
	
	public Boolean getIsNonTeacher() {
		return isNonTeacher;
	}
	public void setIsNonTeacher(Boolean isNonTeacher) {
		this.isNonTeacher = isNonTeacher;
	}
	
	public Boolean getNationalBoardCert() {
		return nationalBoardCert;
	}
	public void setNationalBoardCert(Boolean nationalBoardCert) {
		this.nationalBoardCert = nationalBoardCert;
	}
	public Integer getNationalBoardCertYear() {
		return nationalBoardCertYear;
	}
	public void setNationalBoardCertYear(Integer nationalBoardCertYear) {
		this.nationalBoardCertYear = nationalBoardCertYear;
	}
	public String getResume() {
		return resume;
	}
	public void setResume(String resume) {
		this.resume = resume;
	}	
	public TFAAffiliateMaster getTfaAffiliateMaster() {
		return tfaAffiliateMaster;
	}
	public void setTfaAffiliateMaster(TFAAffiliateMaster tfaAffiliateMaster) {
		this.tfaAffiliateMaster = tfaAffiliateMaster;
	}
	public TFARegionMaster getTfaRegionMaster() {
		return tfaRegionMaster;
	}
	public void setTfaRegionMaster(TFARegionMaster tfaRegionMaster) {
		this.tfaRegionMaster = tfaRegionMaster;
	}
	public Integer getCorpsYear() {
		return corpsYear;
	}
	public void setCorpsYear(Integer corpsYear) {
		this.corpsYear = corpsYear;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Integer getCanServeAsSubTeacher() {
		return canServeAsSubTeacher;
	}
	public void setCanServeAsSubTeacher(Integer canServeAsSubTeacher) {
		this.canServeAsSubTeacher = canServeAsSubTeacher;
	}
	public Integer getPotentialQuality() {
		return potentialQuality;
	}
	public void setPotentialQuality(Integer potentialQuality) {
		this.potentialQuality = potentialQuality;
	}
	public Boolean getKnowOtherLanguages() {
		return knowOtherLanguages;
	}
	public void setKnowOtherLanguages(Boolean knowOtherLanguages) {
		this.knowOtherLanguages = knowOtherLanguages;
	}
	public Boolean getIsDocumentOnFile() {
		return isDocumentOnFile;
	}
	public void setIsDocumentOnFile(Boolean isDocumentOnFile) {
		this.isDocumentOnFile = isDocumentOnFile;
	}
	
	
}
