package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;

@Entity
@Table(name="districtassessmentquestionoptions")
public class DistrictAssessmentQuestionOptions implements Serializable 
{

	private static final long serialVersionUID = 2310151621595873905L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer optionId;
	
	@ManyToOne
	@JoinColumn(name="districtAssessmentQuestionId",referencedColumnName="districtAssessmentQuestionId")
	private DistrictAssessmentQuestions districtAssessmentQuestions;
	private String questionOption;
	private Integer rank;
	private Double score;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	private String status;
	private Date createdDateTime;
	private String ipaddress;
	public Integer getOptionId() {
		return optionId;
	}
	public void setOptionId(Integer optionId) {
		this.optionId = optionId;
	}
	public DistrictAssessmentQuestions getDistrictAssessmentQuestions() {
		return districtAssessmentQuestions;
	}
	public void setDistrictAssessmentQuestions(
			DistrictAssessmentQuestions districtAssessmentQuestions) {
		this.districtAssessmentQuestions = districtAssessmentQuestions;
	}
	public String getQuestionOption() {
		return questionOption;
	}
	public void setQuestionOption(String questionOption) {
		this.questionOption = questionOption;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		this.score = score;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	
}
