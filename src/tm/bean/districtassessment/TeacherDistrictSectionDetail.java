package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentDetail;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teacherdistrictsectiondetails")
public class TeacherDistrictSectionDetail implements Serializable 
{
	private static final long serialVersionUID = 5657319784124200138L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherDistrictSectionId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail; 
	
	@ManyToOne
	@JoinColumn(name="teacherDistrictAssessmentId",referencedColumnName="teacherDistrictAssessmentId")
	private TeacherDistrictAssessmentDetail teacherDistrictAssessmentdetail; 
	
	private String sectionName;
	private String sectionDescription;
	private String sectionInstructions;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	private String status;
	private Date createdDateTime; 
	private String ipAddress;
	public Integer getTeacherDistrictSectionId() {
		return teacherDistrictSectionId;
	}
	public void setTeacherDistrictSectionId(Integer teacherDistrictSectionId) {
		this.teacherDistrictSectionId = teacherDistrictSectionId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public TeacherDistrictAssessmentDetail getTeacherDistrictAssessmentdetail() {
		return teacherDistrictAssessmentdetail;
	}
	public void setTeacherDistrictAssessmentdetail(
			TeacherDistrictAssessmentDetail teacherDistrictAssessmentdetail) {
		this.teacherDistrictAssessmentdetail = teacherDistrictAssessmentdetail;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getSectionDescription() {
		return sectionDescription;
	}
	public void setSectionDescription(String sectionDescription) {
		this.sectionDescription = sectionDescription;
	}
	public String getSectionInstructions() {
		return sectionInstructions;
	}
	public void setSectionInstructions(String sectionInstructions) {
		this.sectionInstructions = sectionInstructions;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	
}
