package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="districtassessmentdetail")
public class DistrictAssessmentDetail implements Serializable 
{

	private static final long serialVersionUID = -7271860043202778475L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtAssessmentId;
	private String districtAssessmentName;
	private String districtAssessmentDescription;
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	
	private Integer questionSessionTime;
	private Integer assessmentSessionTime;
	private Boolean questionRandomization;
	private Boolean optionRandomization;
	
	
	@OneToMany()
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="districtAssessmentId",referencedColumnName="districtAssessmentId",insertable=false,updatable=false)
	private List<DistrictAssessmentJobRelation> districtAssessmentJobRelations;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private String status;
	private java.util.Date createdDateTime;
	private String ipaddress;
	
	public Integer getDistrictAssessmentId() {
		return districtAssessmentId;
	}
	public void setDistrictAssessmentId(Integer districtAssessmentId) {
		this.districtAssessmentId = districtAssessmentId;
	}
	public String getDistrictAssessmentName() {
		return districtAssessmentName;
	}
	public void setDistrictAssessmentName(String districtAssessmentName) {
		this.districtAssessmentName = districtAssessmentName;
	}
	public String getDistrictAssessmentDescription() {
		return districtAssessmentDescription;
	}
	public void setDistrictAssessmentDescription(
			String districtAssessmentDescription) {
		this.districtAssessmentDescription = districtAssessmentDescription;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public Integer getQuestionSessionTime() {
		return questionSessionTime;
	}
	public void setQuestionSessionTime(Integer questionSessionTime) {
		this.questionSessionTime = questionSessionTime;
	}
	public Integer getAssessmentSessionTime() {
		return assessmentSessionTime;
	}
	public void setAssessmentSessionTime(Integer assessmentSessionTime) {
		this.assessmentSessionTime = assessmentSessionTime;
	}
	public Boolean getQuestionRandomization() {
		return questionRandomization;
	}
	public void setQuestionRandomization(Boolean questionRandomization) {
		this.questionRandomization = questionRandomization;
	}
	public Boolean getOptionRandomization() {
		return optionRandomization;
	}
	public void setOptionRandomization(Boolean optionRandomization) {
		this.optionRandomization = optionRandomization;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public java.util.Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(java.util.Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	
	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}
	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public List<DistrictAssessmentJobRelation> getDistrictAssessmentJobRelations() {
		return districtAssessmentJobRelations;
	}
	public void setDistrictAssessmentJobRelations(
			List<DistrictAssessmentJobRelation> districtAssessmentJobRelations) {
		this.districtAssessmentJobRelations = districtAssessmentJobRelations;
	}
	
	@Override
	public boolean equals(Object object) {
		if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof DistrictAssessmentDetail))return false;
	    DistrictAssessmentDetail districtAssessmentDetail = (DistrictAssessmentDetail)object;
	  
	    if(this.districtAssessmentId.equals(districtAssessmentDetail.getDistrictAssessmentId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	@Override
	public int hashCode() {
		return new Integer(""+districtAssessmentId);
	}
}
