package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.TimeZoneMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="centermaster")
public class CenterMaster implements Serializable 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -524806450088289076L;

	/**
	 * 
	 */

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer centerId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private String centerName;
	
	private Integer noOfSeatsAvailable;
	
	@ManyToOne
	@JoinColumn(name="timeZoneId",referencedColumnName="timeZoneId")
	private TimeZoneMaster timeZoneMaster;
	
	private String status;
	
	private Integer centerCreatedBy;
	
	private Date centerCreatedDateTime;
	
	public Integer getNoOfSeatsAvailable() {
		return noOfSeatsAvailable;
	}
	public void setNoOfSeatsAvailable(Integer noOfSeatsAvailable) {
		this.noOfSeatsAvailable = noOfSeatsAvailable;
	}	
	public TimeZoneMaster getTimeZoneMaster() {
		return timeZoneMaster;
	}
	public void setTimeZoneMaster(TimeZoneMaster timeZoneMaster) {
		this.timeZoneMaster = timeZoneMaster;
	}
	public Integer getCenterId() {
		return centerId;
	}
	public void setCenterId(Integer centerId) {
		this.centerId = centerId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getCenterName() {
		return centerName;
	}
	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getCenterCreatedBy() {
		return centerCreatedBy;
	}
	public void setCenterCreatedBy(Integer centerCreatedBy) {
		this.centerCreatedBy = centerCreatedBy;
	}
	public Date getCenterCreatedDateTime() {
		return centerCreatedDateTime;
	}
	public void setCenterCreatedDateTime(Date centerCreatedDateTime) {
		this.centerCreatedDateTime = centerCreatedDateTime;
	}


	
	


}
