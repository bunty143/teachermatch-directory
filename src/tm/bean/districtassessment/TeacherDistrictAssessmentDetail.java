package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teacherdistrictassessmentdetail")
public class TeacherDistrictAssessmentDetail implements Serializable 
{

	private static final long serialVersionUID = -3409156060296230322L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherDistrictAssessmentId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="districtAssessmentId",referencedColumnName="districtAssessmentId")
	private DistrictAssessmentDetail districtAssessmentDetail;
	private String districtAssessmentName; 
	private String districtAssessmentDescription; 
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster; 
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	private Integer questionSessionTime; 
	private Integer assessmentSessionTime;
	private Boolean questionRandomization; 
	private Boolean optionRandomization; 
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	private String status; 
	private Boolean isDone;
	private Date createdDateTime; 
	private String ipaddress;
	
	public Integer getTeacherDistrictAssessmentId() {
		return teacherDistrictAssessmentId;
	}
	public void setTeacherDistrictAssessmentId(Integer teacherDistrictAssessmentId) {
		this.teacherDistrictAssessmentId = teacherDistrictAssessmentId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public DistrictAssessmentDetail getDistrictAssessmentDetail() {
		return districtAssessmentDetail;
	}
	public void setDistrictAssessmentDetail(
			DistrictAssessmentDetail districtAssessmentDetail) {
		this.districtAssessmentDetail = districtAssessmentDetail;
	}
	public String getDistrictAssessmentName() {
		return districtAssessmentName;
	}
	public void setDistrictAssessmentName(String districtAssessmentName) {
		this.districtAssessmentName = districtAssessmentName;
	}
	public String getDistrictAssessmentDescription() {
		return districtAssessmentDescription;
	}
	public void setDistrictAssessmentDescription(
			String districtAssessmentDescription) {
		this.districtAssessmentDescription = districtAssessmentDescription;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public Integer getQuestionSessionTime() {
		return questionSessionTime;
	}
	public void setQuestionSessionTime(Integer questionSessionTime) {
		this.questionSessionTime = questionSessionTime;
	}
	public Integer getAssessmentSessionTime() {
		return assessmentSessionTime;
	}
	public void setAssessmentSessionTime(Integer assessmentSessionTime) {
		this.assessmentSessionTime = assessmentSessionTime;
	}
	public Boolean getQuestionRandomization() {
		return questionRandomization;
	}
	public void setQuestionRandomization(Boolean questionRandomization) {
		this.questionRandomization = questionRandomization;
	}
	public Boolean getOptionRandomization() {
		return optionRandomization;
	}
	public void setOptionRandomization(Boolean optionRandomization) {
		this.optionRandomization = optionRandomization;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getIsDone() {
		return isDone;
	}
	public void setIsDone(Boolean isDone) {
		this.isDone = isDone;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	
}
