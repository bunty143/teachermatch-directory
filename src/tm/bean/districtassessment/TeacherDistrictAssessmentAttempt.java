package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;

@Entity
@Table(name="teacherdistrictassessmentattempt")
public class TeacherDistrictAssessmentAttempt implements Serializable 
{

	private static final long serialVersionUID = 6532170185704184065L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer attemptId;

	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="districtAssessmentId",referencedColumnName="districtAssessmentId")
	private DistrictAssessmentDetail districtAssessmentDetail;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	private Date districtAssessmentStartTime;
	private Date districtAssessmentEndTime;
	private Integer districtAssessmentSessionTime;
	private Boolean isForced;
	private String ipAddress;
	public Integer getAttemptId() {
		return attemptId;
	}
	public void setAttemptId(Integer attemptId) {
		this.attemptId = attemptId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public DistrictAssessmentDetail getDistrictAssessmentDetail() {
		return districtAssessmentDetail;
	}
	public void setDistrictAssessmentDetail(
			DistrictAssessmentDetail districtAssessmentDetail) {
		this.districtAssessmentDetail = districtAssessmentDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public Date getDistrictAssessmentStartTime() {
		return districtAssessmentStartTime;
	}
	public void setDistrictAssessmentStartTime(Date districtAssessmentStartTime) {
		this.districtAssessmentStartTime = districtAssessmentStartTime;
	}
	public Date getDistrictAssessmentEndTime() {
		return districtAssessmentEndTime;
	}
	public void setDistrictAssessmentEndTime(Date districtAssessmentEndTime) {
		this.districtAssessmentEndTime = districtAssessmentEndTime;
	}
	public Integer getDistrictAssessmentSessionTime() {
		return districtAssessmentSessionTime;
	}
	public void setDistrictAssessmentSessionTime(
			Integer districtAssessmentSessionTime) {
		this.districtAssessmentSessionTime = districtAssessmentSessionTime;
	}
	public Boolean getIsForced() {
		return isForced;
	}
	public void setIsForced(Boolean isForced) {
		this.isForced = isForced;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	
}
