package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherDetail;
import tm.bean.master.QuestionTypeMaster;

@Entity
@Table(name="teacherdistrictassessmentanswerdetail")
public class TeacherDistrictAssessmentAnswerDetail implements Serializable 
{

	private static final long serialVersionUID = 3399512640899186925L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer answerId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	private Double questionWeightage;
	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster questionTypeMaster;
	@ManyToOne
	@JoinColumn(name="districtAssessmentId",referencedColumnName="districtAssessmentId")
	private DistrictAssessmentDetail districtAssessmentDetail;
	@ManyToOne
	@JoinColumn(name="teacherDistrictAssessmentId",referencedColumnName="teacherDistrictAssessmentId")
	private TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail;
	@ManyToOne
	@JoinColumn(name="teacherDistrictAssessmentQuestionId",referencedColumnName="teacherDistrictAssessmentQuestionId")
	private TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQuestion;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	private String selectedOptions;
	private String insertedText;
	private String insertedRanks;
	private String optionScore;
	private Double totalScore;
	private Double maxMarks;
	private Date createdDateTime;
	public Integer getAnswerId() {
		return answerId;
	}
	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public Double getQuestionWeightage() {
		return questionWeightage;
	}
	public void setQuestionWeightage(Double questionWeightage) {
		this.questionWeightage = questionWeightage;
	}
	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}
	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}
	public DistrictAssessmentDetail getDistrictAssessmentDetail() {
		return districtAssessmentDetail;
	}
	public void setDistrictAssessmentDetail(
			DistrictAssessmentDetail districtAssessmentDetail) {
		this.districtAssessmentDetail = districtAssessmentDetail;
	}
	public TeacherDistrictAssessmentDetail getTeacherDistrictAssessmentDetail() {
		return teacherDistrictAssessmentDetail;
	}
	public void setTeacherDistrictAssessmentDetail(
			TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail) {
		this.teacherDistrictAssessmentDetail = teacherDistrictAssessmentDetail;
	}
	public TeacherDistrictAssessmentQuestion getTeacherDistrictAssessmentQuestion() {
		return teacherDistrictAssessmentQuestion;
	}
	public void setTeacherDistrictAssessmentQuestion(
			TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQuestion) {
		this.teacherDistrictAssessmentQuestion = teacherDistrictAssessmentQuestion;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public String getSelectedOptions() {
		return selectedOptions;
	}
	public void setSelectedOptions(String selectedOptions) {
		this.selectedOptions = selectedOptions;
	}
	public String getInsertedText() {
		return insertedText;
	}
	public void setInsertedText(String insertedText) {
		this.insertedText = insertedText;
	}
	public String getInsertedRanks() {
		return insertedRanks;
	}
	public void setInsertedRanks(String insertedRanks) {
		this.insertedRanks = insertedRanks;
	}
	public String getOptionScore() {
		return optionScore;
	}
	public void setOptionScore(String optionScore) {
		this.optionScore = optionScore;
	}
	public Double getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(Double totalScore) {
		this.totalScore = totalScore;
	}
	public Double getMaxMarks() {
		return maxMarks;
	}
	public void setMaxMarks(Double maxMarks) {
		this.maxMarks = maxMarks;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	
}
