package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.master.DistrictMaster;

@Entity
@Table(name="priorityassessmentdays")
public class PriorityAssessmentDays implements Serializable 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8037371370866897616L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer priorityAssessmentId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
/*	
	@ManyToOne
	@JoinColumn(name="districtAssessmentId",referencedColumnName="districtAssessmentId")
	private DistrictAssessmentDetail districtAssessmentDetail;
*/
	private Date preferredDates;
	
	private String availableSlots;
	
		
	@ManyToOne
	@JoinColumn(name="priority",referencedColumnName="priorityId")
	private AssessmentsPriorityMaster priority;

	
	//private String status;
	
	/*@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;*/

	private Date createdDateTime;

	public Integer getPriorityAssessmentId() {
		return priorityAssessmentId;
	}

	public void setPriorityAssessmentId(Integer priorityAssessmentId) {
		this.priorityAssessmentId = priorityAssessmentId;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	/*public DistrictAssessmentDetail getDistrictAssessmentDetail() {
		return districtAssessmentDetail;
	}

	public void setDistrictAssessmentDetail(
			DistrictAssessmentDetail districtAssessmentDetail) {
		this.districtAssessmentDetail = districtAssessmentDetail;
	}*/

	public Date getPreferredDates() {
		return preferredDates;
	}

	public void setPreferredDates(Date preferredDates) {
		this.preferredDates = preferredDates;
	}

	public String getAvailableSlots() {
		return availableSlots;
	}

	public void setAvailableSlots(String availableSlots) {
		this.availableSlots = availableSlots;
	}

	public AssessmentsPriorityMaster getPriority() {
		return priority;
	}

	public void setPriority(AssessmentsPriorityMaster priority) {
		this.priority = priority;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
}
