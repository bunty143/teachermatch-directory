package tm.bean.districtassessment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import tm.bean.TeacherAssessmentOption;
import tm.bean.TeacherDetail;
import tm.bean.master.QuestionTypeMaster;

@Entity
@Table(name="teacherdistrictassessmentquestions")
public class TeacherDistrictAssessmentQuestion implements Serializable 
{

	private static final long serialVersionUID = -1549749079418799959L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long teacherDistrictAssessmentQuestionId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;   
	@ManyToOne
	@JoinColumn(name="teacherDistrictAssessmentId",referencedColumnName="teacherDistrictAssessmentId")
	private TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail;
	@ManyToOne
	@JoinColumn(name="teacherDistrictSectionId",referencedColumnName="teacherDistrictSectionId")
	private TeacherDistrictSectionDetail teacherDistrictSectionDetail;
	
	private String question; 
	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster questionTypeMaster; 
	private Double questionWeightage;
	private Double maxMarks;
	private String questionInstruction;
	private Date createdDateTime;
	private Boolean isAttempted;
	private String questionImage;
	@OneToMany()
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(name="teacherDistrictAssessmentQuestionId",referencedColumnName="teacherDistrictAssessmentQuestionId",insertable=false,updatable=false)
	private List<TeacherDistrictAssessmentOption> TeacherDistrictAssessmentOptions;
	
	public Long getTeacherDistrictAssessmentQuestionId() {
		return teacherDistrictAssessmentQuestionId;
	}
	public void setTeacherDistrictAssessmentQuestionId(
			Long teacherDistrictAssessmentQuestionId) {
		this.teacherDistrictAssessmentQuestionId = teacherDistrictAssessmentQuestionId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public TeacherDistrictAssessmentDetail getTeacherDistrictAssessmentDetail() {
		return teacherDistrictAssessmentDetail;
	}
	public void setTeacherDistrictAssessmentDetail(
			TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail) {
		this.teacherDistrictAssessmentDetail = teacherDistrictAssessmentDetail;
	}
	public TeacherDistrictSectionDetail getTeacherDistrictSectionDetail() {
		return teacherDistrictSectionDetail;
	}
	public void setTeacherDistrictSectionDetail(
			TeacherDistrictSectionDetail teacherDistrictSectionDetail) {
		this.teacherDistrictSectionDetail = teacherDistrictSectionDetail;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}
	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}
	public Double getQuestionWeightage() {
		return questionWeightage;
	}
	public void setQuestionWeightage(Double questionWeightage) {
		this.questionWeightage = questionWeightage;
	}
	public Double getMaxMarks() {
		return maxMarks;
	}
	public void setMaxMarks(Double maxMarks) {
		this.maxMarks = maxMarks;
	}
	public String getQuestionInstruction() {
		return questionInstruction;
	}
	public void setQuestionInstruction(String questionInstruction) {
		this.questionInstruction = questionInstruction;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Boolean getIsAttempted() {
		return isAttempted;
	}
	public void setIsAttempted(Boolean isAttempted) {
		this.isAttempted = isAttempted;
	}
	public List<TeacherDistrictAssessmentOption> getTeacherDistrictAssessmentOptions() {
		return TeacherDistrictAssessmentOptions;
	}
	public void setTeacherDistrictAssessmentOptions(
			List<TeacherDistrictAssessmentOption> teacherDistrictAssessmentOptions) {
		TeacherDistrictAssessmentOptions = teacherDistrictAssessmentOptions;
	}
	public String getQuestionImage() {
		return questionImage;
	}
	public void setQuestionImage(String questionImage) {
		this.questionImage = questionImage;
	}
	
}
