package tm.bean.districtassessment;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="teacherdistrictassessmentspriority")
public class TeacherDistrictAssessmentsPriority implements Serializable 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9032094321537429004L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer  assessmentPriorityId;
	
	@ManyToOne
	@JoinColumn(name="districtAssessmentId",referencedColumnName="districtAssessmentId")
	private DistrictAssessmentDetail districtAssessmentDetail;

	@ManyToOne
	@JoinColumn(name="priority",referencedColumnName="priorityId")
	private AssessmentsPriorityMaster priority;


	public Integer getAssessmentPriorityId() {
		return assessmentPriorityId;
	}

	public void setAssessmentPriorityId(Integer assessmentPriorityId) {
		this.assessmentPriorityId = assessmentPriorityId;
	}

	public DistrictAssessmentDetail getDistrictAssessmentDetail() {
		return districtAssessmentDetail;
	}

	public void setDistrictAssessmentDetail(
			DistrictAssessmentDetail districtAssessmentDetail) {
		this.districtAssessmentDetail = districtAssessmentDetail;
	}

	public AssessmentsPriorityMaster getPriority() {
		return priority;
	}

	public void setPriority(AssessmentsPriorityMaster priority) {
		this.priority = priority;
	}

}
