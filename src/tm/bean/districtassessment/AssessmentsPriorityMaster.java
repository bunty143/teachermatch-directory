package tm.bean.districtassessment;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="assessmentprioritymaster")
public class AssessmentsPriorityMaster implements Serializable 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6242688849278585519L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer  priorityId;
	
	private String priorityName;
	
	//private int priorityValue;
	
	public Integer getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(Integer priorityId) {
		this.priorityId = priorityId;
	}

	public String getPriorityName() {
		return priorityName;
	}

	public void setPriorityName(String priorityName) {
		this.priorityName = priorityName;
	}

	/*public int getPriorityValue() {
		return priorityValue;
	}

	public void setPriorityValue(int priorityValue) {
		this.priorityValue = priorityValue;
	}	*/
}
