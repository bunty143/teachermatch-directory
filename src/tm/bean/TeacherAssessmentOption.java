package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="teacherassessmentoptions")
public class TeacherAssessmentOption implements Serializable{

	private static final long serialVersionUID = -8934909527936577708L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long teacherAssessmentOptionId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="teacherAssessmentQuestionId",referencedColumnName="teacherAssessmentQuestionId")
	private TeacherAssessmentQuestion teacherAssessmentQuestion;   
	private String questionOption;                    
	private Integer rank;                      
	private Double score;     
	private Date createdDateTime;
	private Integer questionOptionId;
	private Integer questionOptionTag;

	public Long getTeacherAssessmentOptionId() {
		return teacherAssessmentOptionId;
	}
	public void setTeacherAssessmentOptionId(Long teacherAssessmentOptionId) {
		this.teacherAssessmentOptionId = teacherAssessmentOptionId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public TeacherAssessmentQuestion getTeacherAssessmentQuestion() {
		return teacherAssessmentQuestion;
	}
	public void setTeacherAssessmentQuestion(
			TeacherAssessmentQuestion teacherAssessmentQuestion) {
		this.teacherAssessmentQuestion = teacherAssessmentQuestion;
	}
	public String getQuestionOption() {
		return questionOption;
	}
	public void setQuestionOption(String questionOption) {
		this.questionOption = questionOption;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		this.score = score;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Integer getQuestionOptionId() {
		return questionOptionId;
	}
	public void setQuestionOptionId(Integer questionOptionId) {
		this.questionOptionId = questionOptionId;
	}
	public Integer getQuestionOptionTag() {
		return questionOptionTag;
	}
	public void setQuestionOptionTag(Integer questionOptionTag) {
		this.questionOptionTag = questionOptionTag;
	}   
}
