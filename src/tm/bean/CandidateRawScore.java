package tm.bean;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.DomainMaster;

@Entity
@Table(name="candidaterawscore")
public class CandidateRawScore implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6665882705365457493L;
	/*
	 * `rawId` int(16) NOT NULL auto_increment,
`teacherId` int(10) NOT NULL COMMENT 'foreign key to teacherdetail',
`createdDateTime` datetime NOT NULL,
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)	
	private Integer rawId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	private Date createdDateTime;
	private Double q1;
	private Double q2;
	private Double q3;
	private Double q4;
	private Double q5;
	private Double q6;
	private Double q7;
	private Double q8;
	private Double q9;
	private Double q10;
	private Double q11;
	private Double q12;
	private Double q13;
	private Double q14;
	private Double q15;
	private Double q16;
	private Double q17;
	private Double q18;
	private Double q19;
	private Double q20;
	private Double q21;
	private Double q22;
	private Double q23;
	private Double q24;
	private Double q25;
	private Double q26;
	private Double q27;
	private Double q28;
	private Double q29;
	private Double q30;
	private Double q31;
	private Double q32;
	private Double q33;
	private Double q34;
	private Double q35;
	private Double q36;
	private Double q37;
	private Double q38;
	private Double q39;
	private Double q40;
	private Double q41;
	private Double q42;
	private Double q43;
	private Double q44;
	private Double q45;
	private Double q46;
	private Double q47;
	private Double q48;
	private Double q49;
	private Double q50;
	private Double q51;
	private Double q52;
	private Double q53;
	private Double q54;
	private Double q55;
	private Double q56;
	private Double q57;
	private Double q58;
	private Double q59;
	private Double q60;
	private Double q61;
	private Double q62;
	private Double q63;
	private Double q64;
	private Double q65;
	private Double q66;
	private Double q67;
	private Double q68;
	private Double q69;
	private Double q70;
	private Double q71;
	private Double q72;
	private Double q73;
	private Double q74;
	private Double q75;
	private Double q76;
	private Double q77;
	private Double q78;
	private Double q79;
	private Double q80;
	private Double q81;
	private Double q82;
	private Double q83;
	private Double q84;
	private Double q85;
	private Double q86;
	private Double q87;
	private Double q88;
	private Double q89;
	private Double q90;
	private Double q91;
	private Double q92;
	private Double q93;
	private Double q94;
	private Double q95;
	private Double q96;
	private Double q97;
	private Double q98;
	private Double q99;
	private Double q100;
	private Double q101;
	private Double q102;
	private Double q103;
	private Double q104;
	private Double q105;
	private Double q106;
	private Double q107;
	private Double q108;
	private Double q109;
	private Double q110;
	private Double q111;
	private Double q112;
	private Double q113;
	private Double q114;
	private Double q115;
	private Double q116;
	private Double q117;
	private Double q118;
	private Double q119;
	private Double q120;
	private Double q121;
	private Double q122;
	private Double q123;
	private Double q124;
	private Double q125;
	private Double q126;
	private Double q127;
	private Double q128;
	private Double q129;
	private Double q130;
	private Double q131;
	private Double q132;
	private Double q133;
	private Double q134;
	private Double q135;
	private Double q136;
	private Double q137;
	private Double q138;
	private Double q139;
	private Double q140;
	private Double q141;
	private Double q142;
	private Double q143;
	private Double q144;
	private Double q145;
	private Double q146;
	private Double q147;
	private Double q148;
	private Double q149;
	private Double q150;
	private Double q151;
	private Double q152;
	private Double q153;
	private Double q154;
	private Double q155;
	private Double q156;
	private Double q157;
	private Double q158;
	private Double q159;
	private Double q160;
	private Double q161;
	private Double q162;
	private Double q163;
	private Double q164;
	private Double q165;
	private Double q166;
	private Double q167;
	private Double q168;
	private Double q169;
	private Double q170;
	private Double q171;
	private Double q172;
	private Double q173;
	private Double q174;
	private Double q175;
	private Double q176;
	private Double q177;
	private Double q178;
	private Double q179;
	private Double q180;
	private Double q181;
	private Double q182;
	private Double q183;
	private Double q184;
	private Double q185;
	private Double q186;
	private Double q187;
	private Double q188;
	private Double q189;
	private Double q190;
	private Double q191;
	private Double q192;
	private Double q193;
	private Double q194;
	private Double q195;
	private Double q196;
	private Double q197;
	private Double q198;
	private Double q199;
	private Double q200;
	private Double q201;
	private Double q202;
	private Double q203;
	private Double q204;
	private Double q205;
	private Double q206;
	private Double q207;
	private Double q208;
	private Double q209;
	private Double q210;
	private Double q211;
	private Double q212;
	private Double q213;
	private Double q214;
	private Double q215;
	private Double q216;
	private Double q217;
	private Double q218;
	private Double q219;
	private Double q220;
	private Double q221;
	private Double q222;
	private Double q223;
	private Double q224;
	private Double q225;
	private Double q226;
	private Double q227;
	private Double q228;
	private Double q229;
	private Double q230;
	private Double q231;
	private Double q232;
	private Double q233;
	private Double q234;
	private Double q235;
	private Double q236;
	private Double q237;
	private Double q238;
	private Double q239;
	private Double q240;
	private Double q241;
	private Double q242;
	private Double q243;
	private Double q244;
	private Double q245;
	private Double q246;
	private Double q247;
	private Double q248;
	private Double q249;
	private Double q250;
	private Double q251;
	private Double q252;
	private Double q253;
	private Double q254;
	private Double q255;
	private Double q256;
	private Double q257;
	private Double q258;
	private Double q259;
	private Double q260;
	private Double q261;
	private Double q262;
	private Double q263;
	private Double q264;
	private Double q265;
	private Double q266;
	private Double q267;
	private Double q268;
	private Double q269;
	private Double q270;
	private Double q271;
	private Double q272;
	private Double q273;
	private Double q274;
	private Double q275;
	private Double q276;
	private Double q277;
	private Double q278;
	private Double q279;
	private Double q280;
	private Double q281;
	private Double q282;
	private Double q283;
	private Double q284;
	private Double q285;
	private Double q286;
	private Double q287;
	private Double q288;
	private Double q289;
	private Double q290;
	private Double q291;
	private Double q292;
	private Double q293;
	private Double q294;
	private Double q295;
	private Double q296;
	private Double q297;
	private Double q298;
	private Double q299;
	private Double q300;
	private Double q301;
	private Double q302;
	private Double q303;
	private Double q304;
	private Double q305;
	private Double q306;
	private Double q307;
	private Double q308;
	private Double q309;
	private Double q310;
	private Double q311;
	private Double q312;
	private Double q313;
	private Double q314;
	private Double q315;
	private Double q316;
	private Double q317;
	private Double q318;
	private Double q319;
	private Double q320;
	private Double q321;
	private Double q322;
	private Double q323;
	private Double q324;
	private Double q325;
	private Double q326;
	private Double q327;
	private Double q328;
	private Double q329;
	private Double q330;
	private Double q331;
	private Double q332;
	private Double q333;
	private Double q334;
	private Double q335;
	private Double q336;
	private Double q337;
	private Double q338;
	private Double q339;
	private Double q340;
	private Double q341;
	private Double q342;
	private Double q343;
	private Double q344;
	private Double q345;
	private Double q346;
	private Double q347;
	private Double q348;
	private Double q349;
	private Double q350;
	private Double q351;
	private Double q352;
	private Double q353;
	private Double q354;
	private Double q355;
	private Double q356;
	private Double q357;
	private Double q358;
	private Double q359;
	private Double q360;
	private Double q361;
	private Double q362;
	private Double q363;
	private Double q364;
	private Double q365;
	private Double q366;
	private Double q367;
	private Double q368;
	private Double q369;
	private Double q370;
	private Double q371;
	private Double q372;
	private Double q373;
	private Double q374;
	private Double q375;
	private Double q376;
	private Double q377;
	private Double q378;
	private Double q379;
	private Double q380;
	private Double q381;
	private Double q382;
	private Double q383;
	private Double q384;
	private Double q385;
	private Double q386;
	private Double q387;
	private Double q388;
	private Double q389;
	private Double q390;
	private Double q391;
	private Double q392;
	private Double q393;
	private Double q394;
	private Double q395;
	private Double q396;
	private Double q397;
	private Double q398;
	private Double q399;
	private Double q400;
	private Double q401;
	private Double q402;
	private Double q403;
	private Double q404;
	private Double q405;
	private Double q406;
	private Double q407;
	private Double q408;
	private Double q409;
	private Double q410;
	private Double q411;
	private Double q412;
	private Double q413;
	private Double q414;
	private Double q415;
	private Double q416;
	private Double q417;
	private Double q418;
	private Double q419;
	private Double q420;
	private Double q421;
	private Double q422;
	private Double q423;
	private Double q424;
	private Double q425;
	private Double q426;
	private Double q427;
	private Double q428;
	private Double q429;
	private Double q430;
	private Double q431;
	private Double q432;
	private Double q433;
	private Double q434;
	private Double q435;
	private Double q436;
	private Double q437;
	private Double q438;
	private Double q439;
	private Double q440;
	private Double q441;
	private Double q442;
	private Double q443;
	private Double q444;
	private Double q445;
	private Double q446;
	private Double q447;
	private Double q448;
	private Double q449;
	private Double q450;
	private Double q451;
	private Double q452;
	private Double q453;
	private Double q454;
	private Double q455;
	private Double q456;
	private Double q457;
	private Double q458;
	private Double q459;
	private Double q460;
	private Double q461;
	private Double q462;
	private Double q463;
	private Double q464;
	private Double q465;
	private Double q466;
	private Double q467;
	private Double q468;
	private Double q469;
	private Double q470;
	private Double q471;
	private Double q472;
	private Double q473;
	private Double q474;
	private Double q475;
	private Double q476;
	private Double q477;
	private Double q478;
	private Double q479;
	private Double q480;
	private Double q481;
	private Double q482;
	private Double q483;
	private Double q484;
	private Double q485;
	private Double q486;
	private Double q487;
	private Double q488;
	private Double q489;
	private Double q490;
	private Double q491;
	private Double q492;
	private Double q493;
	private Double q494;
	private Double q495;
	private Double q496;
	private Double q497;
	private Double q498;
	private Double q499;
	private Double q500;
	public Integer getRawId() {
		return rawId;
	}
	public void setRawId(Integer rawId) {
		this.rawId = rawId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Double getQ1() {
		return q1;
	}
	public void setQ1(Double q1) {
		this.q1 = q1;
	}
	public Double getQ2() {
		return q2;
	}
	public void setQ2(Double q2) {
		this.q2 = q2;
	}
	public Double getQ3() {
		return q3;
	}
	public void setQ3(Double q3) {
		this.q3 = q3;
	}
	public Double getQ4() {
		return q4;
	}
	public void setQ4(Double q4) {
		this.q4 = q4;
	}
	public Double getQ5() {
		return q5;
	}
	public void setQ5(Double q5) {
		this.q5 = q5;
	}
	public Double getQ6() {
		return q6;
	}
	public void setQ6(Double q6) {
		this.q6 = q6;
	}
	public Double getQ7() {
		return q7;
	}
	public void setQ7(Double q7) {
		this.q7 = q7;
	}
	public Double getQ8() {
		return q8;
	}
	public void setQ8(Double q8) {
		this.q8 = q8;
	}
	public Double getQ9() {
		return q9;
	}
	public void setQ9(Double q9) {
		this.q9 = q9;
	}
	public Double getQ10() {
		return q10;
	}
	public void setQ10(Double q10) {
		this.q10 = q10;
	}
	public Double getQ11() {
		return q11;
	}
	public void setQ11(Double q11) {
		this.q11 = q11;
	}
	public Double getQ12() {
		return q12;
	}
	public void setQ12(Double q12) {
		this.q12 = q12;
	}
	public Double getQ13() {
		return q13;
	}
	public void setQ13(Double q13) {
		this.q13 = q13;
	}
	public Double getQ14() {
		return q14;
	}
	public void setQ14(Double q14) {
		this.q14 = q14;
	}
	public Double getQ15() {
		return q15;
	}
	public void setQ15(Double q15) {
		this.q15 = q15;
	}
	public Double getQ16() {
		return q16;
	}
	public void setQ16(Double q16) {
		this.q16 = q16;
	}
	public Double getQ17() {
		return q17;
	}
	public void setQ17(Double q17) {
		this.q17 = q17;
	}
	public Double getQ18() {
		return q18;
	}
	public void setQ18(Double q18) {
		this.q18 = q18;
	}
	public Double getQ19() {
		return q19;
	}
	public void setQ19(Double q19) {
		this.q19 = q19;
	}
	public Double getQ20() {
		return q20;
	}
	public void setQ20(Double q20) {
		this.q20 = q20;
	}
	public Double getQ21() {
		return q21;
	}
	public void setQ21(Double q21) {
		this.q21 = q21;
	}
	public Double getQ22() {
		return q22;
	}
	public void setQ22(Double q22) {
		this.q22 = q22;
	}
	public Double getQ23() {
		return q23;
	}
	public void setQ23(Double q23) {
		this.q23 = q23;
	}
	public Double getQ24() {
		return q24;
	}
	public void setQ24(Double q24) {
		this.q24 = q24;
	}
	public Double getQ25() {
		return q25;
	}
	public void setQ25(Double q25) {
		this.q25 = q25;
	}
	public Double getQ26() {
		return q26;
	}
	public void setQ26(Double q26) {
		this.q26 = q26;
	}
	public Double getQ27() {
		return q27;
	}
	public void setQ27(Double q27) {
		this.q27 = q27;
	}
	public Double getQ28() {
		return q28;
	}
	public void setQ28(Double q28) {
		this.q28 = q28;
	}
	public Double getQ29() {
		return q29;
	}
	public void setQ29(Double q29) {
		this.q29 = q29;
	}
	public Double getQ30() {
		return q30;
	}
	public void setQ30(Double q30) {
		this.q30 = q30;
	}
	public Double getQ31() {
		return q31;
	}
	public void setQ31(Double q31) {
		this.q31 = q31;
	}
	public Double getQ32() {
		return q32;
	}
	public void setQ32(Double q32) {
		this.q32 = q32;
	}
	public Double getQ33() {
		return q33;
	}
	public void setQ33(Double q33) {
		this.q33 = q33;
	}
	public Double getQ34() {
		return q34;
	}
	public void setQ34(Double q34) {
		this.q34 = q34;
	}
	public Double getQ35() {
		return q35;
	}
	public void setQ35(Double q35) {
		this.q35 = q35;
	}
	public Double getQ36() {
		return q36;
	}
	public void setQ36(Double q36) {
		this.q36 = q36;
	}
	public Double getQ37() {
		return q37;
	}
	public void setQ37(Double q37) {
		this.q37 = q37;
	}
	public Double getQ38() {
		return q38;
	}
	public void setQ38(Double q38) {
		this.q38 = q38;
	}
	public Double getQ39() {
		return q39;
	}
	public void setQ39(Double q39) {
		this.q39 = q39;
	}
	public Double getQ40() {
		return q40;
	}
	public void setQ40(Double q40) {
		this.q40 = q40;
	}
	public Double getQ41() {
		return q41;
	}
	public void setQ41(Double q41) {
		this.q41 = q41;
	}
	public Double getQ42() {
		return q42;
	}
	public void setQ42(Double q42) {
		this.q42 = q42;
	}
	public Double getQ43() {
		return q43;
	}
	public void setQ43(Double q43) {
		this.q43 = q43;
	}
	public Double getQ44() {
		return q44;
	}
	public void setQ44(Double q44) {
		this.q44 = q44;
	}
	public Double getQ45() {
		return q45;
	}
	public void setQ45(Double q45) {
		this.q45 = q45;
	}
	public Double getQ46() {
		return q46;
	}
	public void setQ46(Double q46) {
		this.q46 = q46;
	}
	public Double getQ47() {
		return q47;
	}
	public void setQ47(Double q47) {
		this.q47 = q47;
	}
	public Double getQ48() {
		return q48;
	}
	public void setQ48(Double q48) {
		this.q48 = q48;
	}
	public Double getQ49() {
		return q49;
	}
	public void setQ49(Double q49) {
		this.q49 = q49;
	}
	public Double getQ50() {
		return q50;
	}
	public void setQ50(Double q50) {
		this.q50 = q50;
	}
	public Double getQ51() {
		return q51;
	}
	public void setQ51(Double q51) {
		this.q51 = q51;
	}
	public Double getQ52() {
		return q52;
	}
	public void setQ52(Double q52) {
		this.q52 = q52;
	}
	public Double getQ53() {
		return q53;
	}
	public void setQ53(Double q53) {
		this.q53 = q53;
	}
	public Double getQ54() {
		return q54;
	}
	public void setQ54(Double q54) {
		this.q54 = q54;
	}
	public Double getQ55() {
		return q55;
	}
	public void setQ55(Double q55) {
		this.q55 = q55;
	}
	public Double getQ56() {
		return q56;
	}
	public void setQ56(Double q56) {
		this.q56 = q56;
	}
	public Double getQ57() {
		return q57;
	}
	public void setQ57(Double q57) {
		this.q57 = q57;
	}
	public Double getQ58() {
		return q58;
	}
	public void setQ58(Double q58) {
		this.q58 = q58;
	}
	public Double getQ59() {
		return q59;
	}
	public void setQ59(Double q59) {
		this.q59 = q59;
	}
	public Double getQ60() {
		return q60;
	}
	public void setQ60(Double q60) {
		this.q60 = q60;
	}
	public Double getQ61() {
		return q61;
	}
	public void setQ61(Double q61) {
		this.q61 = q61;
	}
	public Double getQ62() {
		return q62;
	}
	public void setQ62(Double q62) {
		this.q62 = q62;
	}
	public Double getQ63() {
		return q63;
	}
	public void setQ63(Double q63) {
		this.q63 = q63;
	}
	public Double getQ64() {
		return q64;
	}
	public void setQ64(Double q64) {
		this.q64 = q64;
	}
	public Double getQ65() {
		return q65;
	}
	public void setQ65(Double q65) {
		this.q65 = q65;
	}
	public Double getQ66() {
		return q66;
	}
	public void setQ66(Double q66) {
		this.q66 = q66;
	}
	public Double getQ67() {
		return q67;
	}
	public void setQ67(Double q67) {
		this.q67 = q67;
	}
	public Double getQ68() {
		return q68;
	}
	public void setQ68(Double q68) {
		this.q68 = q68;
	}
	public Double getQ69() {
		return q69;
	}
	public void setQ69(Double q69) {
		this.q69 = q69;
	}
	public Double getQ70() {
		return q70;
	}
	public void setQ70(Double q70) {
		this.q70 = q70;
	}
	public Double getQ71() {
		return q71;
	}
	public void setQ71(Double q71) {
		this.q71 = q71;
	}
	public Double getQ72() {
		return q72;
	}
	public void setQ72(Double q72) {
		this.q72 = q72;
	}
	public Double getQ73() {
		return q73;
	}
	public void setQ73(Double q73) {
		this.q73 = q73;
	}
	public Double getQ74() {
		return q74;
	}
	public void setQ74(Double q74) {
		this.q74 = q74;
	}
	public Double getQ75() {
		return q75;
	}
	public void setQ75(Double q75) {
		this.q75 = q75;
	}
	public Double getQ76() {
		return q76;
	}
	public void setQ76(Double q76) {
		this.q76 = q76;
	}
	public Double getQ77() {
		return q77;
	}
	public void setQ77(Double q77) {
		this.q77 = q77;
	}
	public Double getQ78() {
		return q78;
	}
	public void setQ78(Double q78) {
		this.q78 = q78;
	}
	public Double getQ79() {
		return q79;
	}
	public void setQ79(Double q79) {
		this.q79 = q79;
	}
	public Double getQ80() {
		return q80;
	}
	public void setQ80(Double q80) {
		this.q80 = q80;
	}
	public Double getQ81() {
		return q81;
	}
	public void setQ81(Double q81) {
		this.q81 = q81;
	}
	public Double getQ82() {
		return q82;
	}
	public void setQ82(Double q82) {
		this.q82 = q82;
	}
	public Double getQ83() {
		return q83;
	}
	public void setQ83(Double q83) {
		this.q83 = q83;
	}
	public Double getQ84() {
		return q84;
	}
	public void setQ84(Double q84) {
		this.q84 = q84;
	}
	public Double getQ85() {
		return q85;
	}
	public void setQ85(Double q85) {
		this.q85 = q85;
	}
	public Double getQ86() {
		return q86;
	}
	public void setQ86(Double q86) {
		this.q86 = q86;
	}
	public Double getQ87() {
		return q87;
	}
	public void setQ87(Double q87) {
		this.q87 = q87;
	}
	public Double getQ88() {
		return q88;
	}
	public void setQ88(Double q88) {
		this.q88 = q88;
	}
	public Double getQ89() {
		return q89;
	}
	public void setQ89(Double q89) {
		this.q89 = q89;
	}
	public Double getQ90() {
		return q90;
	}
	public void setQ90(Double q90) {
		this.q90 = q90;
	}
	public Double getQ91() {
		return q91;
	}
	public void setQ91(Double q91) {
		this.q91 = q91;
	}
	public Double getQ92() {
		return q92;
	}
	public void setQ92(Double q92) {
		this.q92 = q92;
	}
	public Double getQ93() {
		return q93;
	}
	public void setQ93(Double q93) {
		this.q93 = q93;
	}
	public Double getQ94() {
		return q94;
	}
	public void setQ94(Double q94) {
		this.q94 = q94;
	}
	public Double getQ95() {
		return q95;
	}
	public void setQ95(Double q95) {
		this.q95 = q95;
	}
	public Double getQ96() {
		return q96;
	}
	public void setQ96(Double q96) {
		this.q96 = q96;
	}
	public Double getQ97() {
		return q97;
	}
	public void setQ97(Double q97) {
		this.q97 = q97;
	}
	public Double getQ98() {
		return q98;
	}
	public void setQ98(Double q98) {
		this.q98 = q98;
	}
	public Double getQ99() {
		return q99;
	}
	public void setQ99(Double q99) {
		this.q99 = q99;
	}
	public Double getQ100() {
		return q100;
	}
	public void setQ100(Double q100) {
		this.q100 = q100;
	}
	public Double getQ101() {
		return q101;
	}
	public void setQ101(Double q101) {
		this.q101 = q101;
	}
	public Double getQ102() {
		return q102;
	}
	public void setQ102(Double q102) {
		this.q102 = q102;
	}
	public Double getQ103() {
		return q103;
	}
	public void setQ103(Double q103) {
		this.q103 = q103;
	}
	public Double getQ104() {
		return q104;
	}
	public void setQ104(Double q104) {
		this.q104 = q104;
	}
	public Double getQ105() {
		return q105;
	}
	public void setQ105(Double q105) {
		this.q105 = q105;
	}
	public Double getQ106() {
		return q106;
	}
	public void setQ106(Double q106) {
		this.q106 = q106;
	}
	public Double getQ107() {
		return q107;
	}
	public void setQ107(Double q107) {
		this.q107 = q107;
	}
	public Double getQ108() {
		return q108;
	}
	public void setQ108(Double q108) {
		this.q108 = q108;
	}
	public Double getQ109() {
		return q109;
	}
	public void setQ109(Double q109) {
		this.q109 = q109;
	}
	public Double getQ110() {
		return q110;
	}
	public void setQ110(Double q110) {
		this.q110 = q110;
	}
	public Double getQ111() {
		return q111;
	}
	public void setQ111(Double q111) {
		this.q111 = q111;
	}
	public Double getQ112() {
		return q112;
	}
	public void setQ112(Double q112) {
		this.q112 = q112;
	}
	public Double getQ113() {
		return q113;
	}
	public void setQ113(Double q113) {
		this.q113 = q113;
	}
	public Double getQ114() {
		return q114;
	}
	public void setQ114(Double q114) {
		this.q114 = q114;
	}
	public Double getQ115() {
		return q115;
	}
	public void setQ115(Double q115) {
		this.q115 = q115;
	}
	public Double getQ116() {
		return q116;
	}
	public void setQ116(Double q116) {
		this.q116 = q116;
	}
	public Double getQ117() {
		return q117;
	}
	public void setQ117(Double q117) {
		this.q117 = q117;
	}
	public Double getQ118() {
		return q118;
	}
	public void setQ118(Double q118) {
		this.q118 = q118;
	}
	public Double getQ119() {
		return q119;
	}
	public void setQ119(Double q119) {
		this.q119 = q119;
	}
	public Double getQ120() {
		return q120;
	}
	public void setQ120(Double q120) {
		this.q120 = q120;
	}
	public Double getQ121() {
		return q121;
	}
	public void setQ121(Double q121) {
		this.q121 = q121;
	}
	public Double getQ122() {
		return q122;
	}
	public void setQ122(Double q122) {
		this.q122 = q122;
	}
	public Double getQ123() {
		return q123;
	}
	public void setQ123(Double q123) {
		this.q123 = q123;
	}
	public Double getQ124() {
		return q124;
	}
	public void setQ124(Double q124) {
		this.q124 = q124;
	}
	public Double getQ125() {
		return q125;
	}
	public void setQ125(Double q125) {
		this.q125 = q125;
	}
	public Double getQ126() {
		return q126;
	}
	public void setQ126(Double q126) {
		this.q126 = q126;
	}
	public Double getQ127() {
		return q127;
	}
	public void setQ127(Double q127) {
		this.q127 = q127;
	}
	public Double getQ128() {
		return q128;
	}
	public void setQ128(Double q128) {
		this.q128 = q128;
	}
	public Double getQ129() {
		return q129;
	}
	public void setQ129(Double q129) {
		this.q129 = q129;
	}
	public Double getQ130() {
		return q130;
	}
	public void setQ130(Double q130) {
		this.q130 = q130;
	}
	public Double getQ131() {
		return q131;
	}
	public void setQ131(Double q131) {
		this.q131 = q131;
	}
	public Double getQ132() {
		return q132;
	}
	public void setQ132(Double q132) {
		this.q132 = q132;
	}
	public Double getQ133() {
		return q133;
	}
	public void setQ133(Double q133) {
		this.q133 = q133;
	}
	public Double getQ134() {
		return q134;
	}
	public void setQ134(Double q134) {
		this.q134 = q134;
	}
	public Double getQ135() {
		return q135;
	}
	public void setQ135(Double q135) {
		this.q135 = q135;
	}
	public Double getQ136() {
		return q136;
	}
	public void setQ136(Double q136) {
		this.q136 = q136;
	}
	public Double getQ137() {
		return q137;
	}
	public void setQ137(Double q137) {
		this.q137 = q137;
	}
	public Double getQ138() {
		return q138;
	}
	public void setQ138(Double q138) {
		this.q138 = q138;
	}
	public Double getQ139() {
		return q139;
	}
	public void setQ139(Double q139) {
		this.q139 = q139;
	}
	public Double getQ140() {
		return q140;
	}
	public void setQ140(Double q140) {
		this.q140 = q140;
	}
	public Double getQ141() {
		return q141;
	}
	public void setQ141(Double q141) {
		this.q141 = q141;
	}
	public Double getQ142() {
		return q142;
	}
	public void setQ142(Double q142) {
		this.q142 = q142;
	}
	public Double getQ143() {
		return q143;
	}
	public void setQ143(Double q143) {
		this.q143 = q143;
	}
	public Double getQ144() {
		return q144;
	}
	public void setQ144(Double q144) {
		this.q144 = q144;
	}
	public Double getQ145() {
		return q145;
	}
	public void setQ145(Double q145) {
		this.q145 = q145;
	}
	public Double getQ146() {
		return q146;
	}
	public void setQ146(Double q146) {
		this.q146 = q146;
	}
	public Double getQ147() {
		return q147;
	}
	public void setQ147(Double q147) {
		this.q147 = q147;
	}
	public Double getQ148() {
		return q148;
	}
	public void setQ148(Double q148) {
		this.q148 = q148;
	}
	public Double getQ149() {
		return q149;
	}
	public void setQ149(Double q149) {
		this.q149 = q149;
	}
	public Double getQ150() {
		return q150;
	}
	public void setQ150(Double q150) {
		this.q150 = q150;
	}
	public Double getQ151() {
		return q151;
	}
	public void setQ151(Double q151) {
		this.q151 = q151;
	}
	public Double getQ152() {
		return q152;
	}
	public void setQ152(Double q152) {
		this.q152 = q152;
	}
	public Double getQ153() {
		return q153;
	}
	public void setQ153(Double q153) {
		this.q153 = q153;
	}
	public Double getQ154() {
		return q154;
	}
	public void setQ154(Double q154) {
		this.q154 = q154;
	}
	public Double getQ155() {
		return q155;
	}
	public void setQ155(Double q155) {
		this.q155 = q155;
	}
	public Double getQ156() {
		return q156;
	}
	public void setQ156(Double q156) {
		this.q156 = q156;
	}
	public Double getQ157() {
		return q157;
	}
	public void setQ157(Double q157) {
		this.q157 = q157;
	}
	public Double getQ158() {
		return q158;
	}
	public void setQ158(Double q158) {
		this.q158 = q158;
	}
	public Double getQ159() {
		return q159;
	}
	public void setQ159(Double q159) {
		this.q159 = q159;
	}
	public Double getQ160() {
		return q160;
	}
	public void setQ160(Double q160) {
		this.q160 = q160;
	}
	public Double getQ161() {
		return q161;
	}
	public void setQ161(Double q161) {
		this.q161 = q161;
	}
	public Double getQ162() {
		return q162;
	}
	public void setQ162(Double q162) {
		this.q162 = q162;
	}
	public Double getQ163() {
		return q163;
	}
	public void setQ163(Double q163) {
		this.q163 = q163;
	}
	public Double getQ164() {
		return q164;
	}
	public void setQ164(Double q164) {
		this.q164 = q164;
	}
	public Double getQ165() {
		return q165;
	}
	public void setQ165(Double q165) {
		this.q165 = q165;
	}
	public Double getQ166() {
		return q166;
	}
	public void setQ166(Double q166) {
		this.q166 = q166;
	}
	public Double getQ167() {
		return q167;
	}
	public void setQ167(Double q167) {
		this.q167 = q167;
	}
	public Double getQ168() {
		return q168;
	}
	public void setQ168(Double q168) {
		this.q168 = q168;
	}
	public Double getQ169() {
		return q169;
	}
	public void setQ169(Double q169) {
		this.q169 = q169;
	}
	public Double getQ170() {
		return q170;
	}
	public void setQ170(Double q170) {
		this.q170 = q170;
	}
	public Double getQ171() {
		return q171;
	}
	public void setQ171(Double q171) {
		this.q171 = q171;
	}
	public Double getQ172() {
		return q172;
	}
	public void setQ172(Double q172) {
		this.q172 = q172;
	}
	public Double getQ173() {
		return q173;
	}
	public void setQ173(Double q173) {
		this.q173 = q173;
	}
	public Double getQ174() {
		return q174;
	}
	public void setQ174(Double q174) {
		this.q174 = q174;
	}
	public Double getQ175() {
		return q175;
	}
	public void setQ175(Double q175) {
		this.q175 = q175;
	}
	public Double getQ176() {
		return q176;
	}
	public void setQ176(Double q176) {
		this.q176 = q176;
	}
	public Double getQ177() {
		return q177;
	}
	public void setQ177(Double q177) {
		this.q177 = q177;
	}
	public Double getQ178() {
		return q178;
	}
	public void setQ178(Double q178) {
		this.q178 = q178;
	}
	public Double getQ179() {
		return q179;
	}
	public void setQ179(Double q179) {
		this.q179 = q179;
	}
	public Double getQ180() {
		return q180;
	}
	public void setQ180(Double q180) {
		this.q180 = q180;
	}
	public Double getQ181() {
		return q181;
	}
	public void setQ181(Double q181) {
		this.q181 = q181;
	}
	public Double getQ182() {
		return q182;
	}
	public void setQ182(Double q182) {
		this.q182 = q182;
	}
	public Double getQ183() {
		return q183;
	}
	public void setQ183(Double q183) {
		this.q183 = q183;
	}
	public Double getQ184() {
		return q184;
	}
	public void setQ184(Double q184) {
		this.q184 = q184;
	}
	public Double getQ185() {
		return q185;
	}
	public void setQ185(Double q185) {
		this.q185 = q185;
	}
	public Double getQ186() {
		return q186;
	}
	public void setQ186(Double q186) {
		this.q186 = q186;
	}
	public Double getQ187() {
		return q187;
	}
	public void setQ187(Double q187) {
		this.q187 = q187;
	}
	public Double getQ188() {
		return q188;
	}
	public void setQ188(Double q188) {
		this.q188 = q188;
	}
	public Double getQ189() {
		return q189;
	}
	public void setQ189(Double q189) {
		this.q189 = q189;
	}
	public Double getQ190() {
		return q190;
	}
	public void setQ190(Double q190) {
		this.q190 = q190;
	}
	public Double getQ191() {
		return q191;
	}
	public void setQ191(Double q191) {
		this.q191 = q191;
	}
	public Double getQ192() {
		return q192;
	}
	public void setQ192(Double q192) {
		this.q192 = q192;
	}
	public Double getQ193() {
		return q193;
	}
	public void setQ193(Double q193) {
		this.q193 = q193;
	}
	public Double getQ194() {
		return q194;
	}
	public void setQ194(Double q194) {
		this.q194 = q194;
	}
	public Double getQ195() {
		return q195;
	}
	public void setQ195(Double q195) {
		this.q195 = q195;
	}
	public Double getQ196() {
		return q196;
	}
	public void setQ196(Double q196) {
		this.q196 = q196;
	}
	public Double getQ197() {
		return q197;
	}
	public void setQ197(Double q197) {
		this.q197 = q197;
	}
	public Double getQ198() {
		return q198;
	}
	public void setQ198(Double q198) {
		this.q198 = q198;
	}
	public Double getQ199() {
		return q199;
	}
	public void setQ199(Double q199) {
		this.q199 = q199;
	}
	public Double getQ200() {
		return q200;
	}
	public void setQ200(Double q200) {
		this.q200 = q200;
	}
	public Double getQ201() {
		return q201;
	}
	public void setQ201(Double q201) {
		this.q201 = q201;
	}
	public Double getQ202() {
		return q202;
	}
	public void setQ202(Double q202) {
		this.q202 = q202;
	}
	public Double getQ203() {
		return q203;
	}
	public void setQ203(Double q203) {
		this.q203 = q203;
	}
	public Double getQ204() {
		return q204;
	}
	public void setQ204(Double q204) {
		this.q204 = q204;
	}
	public Double getQ205() {
		return q205;
	}
	public void setQ205(Double q205) {
		this.q205 = q205;
	}
	public Double getQ206() {
		return q206;
	}
	public void setQ206(Double q206) {
		this.q206 = q206;
	}
	public Double getQ207() {
		return q207;
	}
	public void setQ207(Double q207) {
		this.q207 = q207;
	}
	public Double getQ208() {
		return q208;
	}
	public void setQ208(Double q208) {
		this.q208 = q208;
	}
	public Double getQ209() {
		return q209;
	}
	public void setQ209(Double q209) {
		this.q209 = q209;
	}
	public Double getQ210() {
		return q210;
	}
	public void setQ210(Double q210) {
		this.q210 = q210;
	}
	public Double getQ211() {
		return q211;
	}
	public void setQ211(Double q211) {
		this.q211 = q211;
	}
	public Double getQ212() {
		return q212;
	}
	public void setQ212(Double q212) {
		this.q212 = q212;
	}
	public Double getQ213() {
		return q213;
	}
	public void setQ213(Double q213) {
		this.q213 = q213;
	}
	public Double getQ214() {
		return q214;
	}
	public void setQ214(Double q214) {
		this.q214 = q214;
	}
	public Double getQ215() {
		return q215;
	}
	public void setQ215(Double q215) {
		this.q215 = q215;
	}
	public Double getQ216() {
		return q216;
	}
	public void setQ216(Double q216) {
		this.q216 = q216;
	}
	public Double getQ217() {
		return q217;
	}
	public void setQ217(Double q217) {
		this.q217 = q217;
	}
	public Double getQ218() {
		return q218;
	}
	public void setQ218(Double q218) {
		this.q218 = q218;
	}
	public Double getQ219() {
		return q219;
	}
	public void setQ219(Double q219) {
		this.q219 = q219;
	}
	public Double getQ220() {
		return q220;
	}
	public void setQ220(Double q220) {
		this.q220 = q220;
	}
	public Double getQ221() {
		return q221;
	}
	public void setQ221(Double q221) {
		this.q221 = q221;
	}
	public Double getQ222() {
		return q222;
	}
	public void setQ222(Double q222) {
		this.q222 = q222;
	}
	public Double getQ223() {
		return q223;
	}
	public void setQ223(Double q223) {
		this.q223 = q223;
	}
	public Double getQ224() {
		return q224;
	}
	public void setQ224(Double q224) {
		this.q224 = q224;
	}
	public Double getQ225() {
		return q225;
	}
	public void setQ225(Double q225) {
		this.q225 = q225;
	}
	public Double getQ226() {
		return q226;
	}
	public void setQ226(Double q226) {
		this.q226 = q226;
	}
	public Double getQ227() {
		return q227;
	}
	public void setQ227(Double q227) {
		this.q227 = q227;
	}
	public Double getQ228() {
		return q228;
	}
	public void setQ228(Double q228) {
		this.q228 = q228;
	}
	public Double getQ229() {
		return q229;
	}
	public void setQ229(Double q229) {
		this.q229 = q229;
	}
	public Double getQ230() {
		return q230;
	}
	public void setQ230(Double q230) {
		this.q230 = q230;
	}
	public Double getQ231() {
		return q231;
	}
	public void setQ231(Double q231) {
		this.q231 = q231;
	}
	public Double getQ232() {
		return q232;
	}
	public void setQ232(Double q232) {
		this.q232 = q232;
	}
	public Double getQ233() {
		return q233;
	}
	public void setQ233(Double q233) {
		this.q233 = q233;
	}
	public Double getQ234() {
		return q234;
	}
	public void setQ234(Double q234) {
		this.q234 = q234;
	}
	public Double getQ235() {
		return q235;
	}
	public void setQ235(Double q235) {
		this.q235 = q235;
	}
	public Double getQ236() {
		return q236;
	}
	public void setQ236(Double q236) {
		this.q236 = q236;
	}
	public Double getQ237() {
		return q237;
	}
	public void setQ237(Double q237) {
		this.q237 = q237;
	}
	public Double getQ238() {
		return q238;
	}
	public void setQ238(Double q238) {
		this.q238 = q238;
	}
	public Double getQ239() {
		return q239;
	}
	public void setQ239(Double q239) {
		this.q239 = q239;
	}
	public Double getQ240() {
		return q240;
	}
	public void setQ240(Double q240) {
		this.q240 = q240;
	}
	public Double getQ241() {
		return q241;
	}
	public void setQ241(Double q241) {
		this.q241 = q241;
	}
	public Double getQ242() {
		return q242;
	}
	public void setQ242(Double q242) {
		this.q242 = q242;
	}
	public Double getQ243() {
		return q243;
	}
	public void setQ243(Double q243) {
		this.q243 = q243;
	}
	public Double getQ244() {
		return q244;
	}
	public void setQ244(Double q244) {
		this.q244 = q244;
	}
	public Double getQ245() {
		return q245;
	}
	public void setQ245(Double q245) {
		this.q245 = q245;
	}
	public Double getQ246() {
		return q246;
	}
	public void setQ246(Double q246) {
		this.q246 = q246;
	}
	public Double getQ247() {
		return q247;
	}
	public void setQ247(Double q247) {
		this.q247 = q247;
	}
	public Double getQ248() {
		return q248;
	}
	public void setQ248(Double q248) {
		this.q248 = q248;
	}
	public Double getQ249() {
		return q249;
	}
	public void setQ249(Double q249) {
		this.q249 = q249;
	}
	public Double getQ250() {
		return q250;
	}
	public void setQ250(Double q250) {
		this.q250 = q250;
	}
	public Double getQ251() {
		return q251;
	}
	public void setQ251(Double q251) {
		this.q251 = q251;
	}
	public Double getQ252() {
		return q252;
	}
	public void setQ252(Double q252) {
		this.q252 = q252;
	}
	public Double getQ253() {
		return q253;
	}
	public void setQ253(Double q253) {
		this.q253 = q253;
	}
	public Double getQ254() {
		return q254;
	}
	public void setQ254(Double q254) {
		this.q254 = q254;
	}
	public Double getQ255() {
		return q255;
	}
	public void setQ255(Double q255) {
		this.q255 = q255;
	}
	public Double getQ256() {
		return q256;
	}
	public void setQ256(Double q256) {
		this.q256 = q256;
	}
	public Double getQ257() {
		return q257;
	}
	public void setQ257(Double q257) {
		this.q257 = q257;
	}
	public Double getQ258() {
		return q258;
	}
	public void setQ258(Double q258) {
		this.q258 = q258;
	}
	public Double getQ259() {
		return q259;
	}
	public void setQ259(Double q259) {
		this.q259 = q259;
	}
	public Double getQ260() {
		return q260;
	}
	public void setQ260(Double q260) {
		this.q260 = q260;
	}
	public Double getQ261() {
		return q261;
	}
	public void setQ261(Double q261) {
		this.q261 = q261;
	}
	public Double getQ262() {
		return q262;
	}
	public void setQ262(Double q262) {
		this.q262 = q262;
	}
	public Double getQ263() {
		return q263;
	}
	public void setQ263(Double q263) {
		this.q263 = q263;
	}
	public Double getQ264() {
		return q264;
	}
	public void setQ264(Double q264) {
		this.q264 = q264;
	}
	public Double getQ265() {
		return q265;
	}
	public void setQ265(Double q265) {
		this.q265 = q265;
	}
	public Double getQ266() {
		return q266;
	}
	public void setQ266(Double q266) {
		this.q266 = q266;
	}
	public Double getQ267() {
		return q267;
	}
	public void setQ267(Double q267) {
		this.q267 = q267;
	}
	public Double getQ268() {
		return q268;
	}
	public void setQ268(Double q268) {
		this.q268 = q268;
	}
	public Double getQ269() {
		return q269;
	}
	public void setQ269(Double q269) {
		this.q269 = q269;
	}
	public Double getQ270() {
		return q270;
	}
	public void setQ270(Double q270) {
		this.q270 = q270;
	}
	public Double getQ271() {
		return q271;
	}
	public void setQ271(Double q271) {
		this.q271 = q271;
	}
	public Double getQ272() {
		return q272;
	}
	public void setQ272(Double q272) {
		this.q272 = q272;
	}
	public Double getQ273() {
		return q273;
	}
	public void setQ273(Double q273) {
		this.q273 = q273;
	}
	public Double getQ274() {
		return q274;
	}
	public void setQ274(Double q274) {
		this.q274 = q274;
	}
	public Double getQ275() {
		return q275;
	}
	public void setQ275(Double q275) {
		this.q275 = q275;
	}
	public Double getQ276() {
		return q276;
	}
	public void setQ276(Double q276) {
		this.q276 = q276;
	}
	public Double getQ277() {
		return q277;
	}
	public void setQ277(Double q277) {
		this.q277 = q277;
	}
	public Double getQ278() {
		return q278;
	}
	public void setQ278(Double q278) {
		this.q278 = q278;
	}
	public Double getQ279() {
		return q279;
	}
	public void setQ279(Double q279) {
		this.q279 = q279;
	}
	public Double getQ280() {
		return q280;
	}
	public void setQ280(Double q280) {
		this.q280 = q280;
	}
	public Double getQ281() {
		return q281;
	}
	public void setQ281(Double q281) {
		this.q281 = q281;
	}
	public Double getQ282() {
		return q282;
	}
	public void setQ282(Double q282) {
		this.q282 = q282;
	}
	public Double getQ283() {
		return q283;
	}
	public void setQ283(Double q283) {
		this.q283 = q283;
	}
	public Double getQ284() {
		return q284;
	}
	public void setQ284(Double q284) {
		this.q284 = q284;
	}
	public Double getQ285() {
		return q285;
	}
	public void setQ285(Double q285) {
		this.q285 = q285;
	}
	public Double getQ286() {
		return q286;
	}
	public void setQ286(Double q286) {
		this.q286 = q286;
	}
	public Double getQ287() {
		return q287;
	}
	public void setQ287(Double q287) {
		this.q287 = q287;
	}
	public Double getQ288() {
		return q288;
	}
	public void setQ288(Double q288) {
		this.q288 = q288;
	}
	public Double getQ289() {
		return q289;
	}
	public void setQ289(Double q289) {
		this.q289 = q289;
	}
	public Double getQ290() {
		return q290;
	}
	public void setQ290(Double q290) {
		this.q290 = q290;
	}
	public Double getQ291() {
		return q291;
	}
	public void setQ291(Double q291) {
		this.q291 = q291;
	}
	public Double getQ292() {
		return q292;
	}
	public void setQ292(Double q292) {
		this.q292 = q292;
	}
	public Double getQ293() {
		return q293;
	}
	public void setQ293(Double q293) {
		this.q293 = q293;
	}
	public Double getQ294() {
		return q294;
	}
	public void setQ294(Double q294) {
		this.q294 = q294;
	}
	public Double getQ295() {
		return q295;
	}
	public void setQ295(Double q295) {
		this.q295 = q295;
	}
	public Double getQ296() {
		return q296;
	}
	public void setQ296(Double q296) {
		this.q296 = q296;
	}
	public Double getQ297() {
		return q297;
	}
	public void setQ297(Double q297) {
		this.q297 = q297;
	}
	public Double getQ298() {
		return q298;
	}
	public void setQ298(Double q298) {
		this.q298 = q298;
	}
	public Double getQ299() {
		return q299;
	}
	public void setQ299(Double q299) {
		this.q299 = q299;
	}
	public Double getQ300() {
		return q300;
	}
	public void setQ300(Double q300) {
		this.q300 = q300;
	}
	public Double getQ301() {
		return q301;
	}
	public void setQ301(Double q301) {
		this.q301 = q301;
	}
	public Double getQ302() {
		return q302;
	}
	public void setQ302(Double q302) {
		this.q302 = q302;
	}
	public Double getQ303() {
		return q303;
	}
	public void setQ303(Double q303) {
		this.q303 = q303;
	}
	public Double getQ304() {
		return q304;
	}
	public void setQ304(Double q304) {
		this.q304 = q304;
	}
	public Double getQ305() {
		return q305;
	}
	public void setQ305(Double q305) {
		this.q305 = q305;
	}
	public Double getQ306() {
		return q306;
	}
	public void setQ306(Double q306) {
		this.q306 = q306;
	}
	public Double getQ307() {
		return q307;
	}
	public void setQ307(Double q307) {
		this.q307 = q307;
	}
	public Double getQ308() {
		return q308;
	}
	public void setQ308(Double q308) {
		this.q308 = q308;
	}
	public Double getQ309() {
		return q309;
	}
	public void setQ309(Double q309) {
		this.q309 = q309;
	}
	public Double getQ310() {
		return q310;
	}
	public void setQ310(Double q310) {
		this.q310 = q310;
	}
	public Double getQ311() {
		return q311;
	}
	public void setQ311(Double q311) {
		this.q311 = q311;
	}
	public Double getQ312() {
		return q312;
	}
	public void setQ312(Double q312) {
		this.q312 = q312;
	}
	public Double getQ313() {
		return q313;
	}
	public void setQ313(Double q313) {
		this.q313 = q313;
	}
	public Double getQ314() {
		return q314;
	}
	public void setQ314(Double q314) {
		this.q314 = q314;
	}
	public Double getQ315() {
		return q315;
	}
	public void setQ315(Double q315) {
		this.q315 = q315;
	}
	public Double getQ316() {
		return q316;
	}
	public void setQ316(Double q316) {
		this.q316 = q316;
	}
	public Double getQ317() {
		return q317;
	}
	public void setQ317(Double q317) {
		this.q317 = q317;
	}
	public Double getQ318() {
		return q318;
	}
	public void setQ318(Double q318) {
		this.q318 = q318;
	}
	public Double getQ319() {
		return q319;
	}
	public void setQ319(Double q319) {
		this.q319 = q319;
	}
	public Double getQ320() {
		return q320;
	}
	public void setQ320(Double q320) {
		this.q320 = q320;
	}
	public Double getQ321() {
		return q321;
	}
	public void setQ321(Double q321) {
		this.q321 = q321;
	}
	public Double getQ322() {
		return q322;
	}
	public void setQ322(Double q322) {
		this.q322 = q322;
	}
	public Double getQ323() {
		return q323;
	}
	public void setQ323(Double q323) {
		this.q323 = q323;
	}
	public Double getQ324() {
		return q324;
	}
	public void setQ324(Double q324) {
		this.q324 = q324;
	}
	public Double getQ325() {
		return q325;
	}
	public void setQ325(Double q325) {
		this.q325 = q325;
	}
	public Double getQ326() {
		return q326;
	}
	public void setQ326(Double q326) {
		this.q326 = q326;
	}
	public Double getQ327() {
		return q327;
	}
	public void setQ327(Double q327) {
		this.q327 = q327;
	}
	public Double getQ328() {
		return q328;
	}
	public void setQ328(Double q328) {
		this.q328 = q328;
	}
	public Double getQ329() {
		return q329;
	}
	public void setQ329(Double q329) {
		this.q329 = q329;
	}
	public Double getQ330() {
		return q330;
	}
	public void setQ330(Double q330) {
		this.q330 = q330;
	}
	public Double getQ331() {
		return q331;
	}
	public void setQ331(Double q331) {
		this.q331 = q331;
	}
	public Double getQ332() {
		return q332;
	}
	public void setQ332(Double q332) {
		this.q332 = q332;
	}
	public Double getQ333() {
		return q333;
	}
	public void setQ333(Double q333) {
		this.q333 = q333;
	}
	public Double getQ334() {
		return q334;
	}
	public void setQ334(Double q334) {
		this.q334 = q334;
	}
	public Double getQ335() {
		return q335;
	}
	public void setQ335(Double q335) {
		this.q335 = q335;
	}
	public Double getQ336() {
		return q336;
	}
	public void setQ336(Double q336) {
		this.q336 = q336;
	}
	public Double getQ337() {
		return q337;
	}
	public void setQ337(Double q337) {
		this.q337 = q337;
	}
	public Double getQ338() {
		return q338;
	}
	public void setQ338(Double q338) {
		this.q338 = q338;
	}
	public Double getQ339() {
		return q339;
	}
	public void setQ339(Double q339) {
		this.q339 = q339;
	}
	public Double getQ340() {
		return q340;
	}
	public void setQ340(Double q340) {
		this.q340 = q340;
	}
	public Double getQ341() {
		return q341;
	}
	public void setQ341(Double q341) {
		this.q341 = q341;
	}
	public Double getQ342() {
		return q342;
	}
	public void setQ342(Double q342) {
		this.q342 = q342;
	}
	public Double getQ343() {
		return q343;
	}
	public void setQ343(Double q343) {
		this.q343 = q343;
	}
	public Double getQ344() {
		return q344;
	}
	public void setQ344(Double q344) {
		this.q344 = q344;
	}
	public Double getQ345() {
		return q345;
	}
	public void setQ345(Double q345) {
		this.q345 = q345;
	}
	public Double getQ346() {
		return q346;
	}
	public void setQ346(Double q346) {
		this.q346 = q346;
	}
	public Double getQ347() {
		return q347;
	}
	public void setQ347(Double q347) {
		this.q347 = q347;
	}
	public Double getQ348() {
		return q348;
	}
	public void setQ348(Double q348) {
		this.q348 = q348;
	}
	public Double getQ349() {
		return q349;
	}
	public void setQ349(Double q349) {
		this.q349 = q349;
	}
	public Double getQ350() {
		return q350;
	}
	public void setQ350(Double q350) {
		this.q350 = q350;
	}
	public Double getQ351() {
		return q351;
	}
	public void setQ351(Double q351) {
		this.q351 = q351;
	}
	public Double getQ352() {
		return q352;
	}
	public void setQ352(Double q352) {
		this.q352 = q352;
	}
	public Double getQ353() {
		return q353;
	}
	public void setQ353(Double q353) {
		this.q353 = q353;
	}
	public Double getQ354() {
		return q354;
	}
	public void setQ354(Double q354) {
		this.q354 = q354;
	}
	public Double getQ355() {
		return q355;
	}
	public void setQ355(Double q355) {
		this.q355 = q355;
	}
	public Double getQ356() {
		return q356;
	}
	public void setQ356(Double q356) {
		this.q356 = q356;
	}
	public Double getQ357() {
		return q357;
	}
	public void setQ357(Double q357) {
		this.q357 = q357;
	}
	public Double getQ358() {
		return q358;
	}
	public void setQ358(Double q358) {
		this.q358 = q358;
	}
	public Double getQ359() {
		return q359;
	}
	public void setQ359(Double q359) {
		this.q359 = q359;
	}
	public Double getQ360() {
		return q360;
	}
	public void setQ360(Double q360) {
		this.q360 = q360;
	}
	public Double getQ361() {
		return q361;
	}
	public void setQ361(Double q361) {
		this.q361 = q361;
	}
	public Double getQ362() {
		return q362;
	}
	public void setQ362(Double q362) {
		this.q362 = q362;
	}
	public Double getQ363() {
		return q363;
	}
	public void setQ363(Double q363) {
		this.q363 = q363;
	}
	public Double getQ364() {
		return q364;
	}
	public void setQ364(Double q364) {
		this.q364 = q364;
	}
	public Double getQ365() {
		return q365;
	}
	public void setQ365(Double q365) {
		this.q365 = q365;
	}
	public Double getQ366() {
		return q366;
	}
	public void setQ366(Double q366) {
		this.q366 = q366;
	}
	public Double getQ367() {
		return q367;
	}
	public void setQ367(Double q367) {
		this.q367 = q367;
	}
	public Double getQ368() {
		return q368;
	}
	public void setQ368(Double q368) {
		this.q368 = q368;
	}
	public Double getQ369() {
		return q369;
	}
	public void setQ369(Double q369) {
		this.q369 = q369;
	}
	public Double getQ370() {
		return q370;
	}
	public void setQ370(Double q370) {
		this.q370 = q370;
	}
	public Double getQ371() {
		return q371;
	}
	public void setQ371(Double q371) {
		this.q371 = q371;
	}
	public Double getQ372() {
		return q372;
	}
	public void setQ372(Double q372) {
		this.q372 = q372;
	}
	public Double getQ373() {
		return q373;
	}
	public void setQ373(Double q373) {
		this.q373 = q373;
	}
	public Double getQ374() {
		return q374;
	}
	public void setQ374(Double q374) {
		this.q374 = q374;
	}
	public Double getQ375() {
		return q375;
	}
	public void setQ375(Double q375) {
		this.q375 = q375;
	}
	public Double getQ376() {
		return q376;
	}
	public void setQ376(Double q376) {
		this.q376 = q376;
	}
	public Double getQ377() {
		return q377;
	}
	public void setQ377(Double q377) {
		this.q377 = q377;
	}
	public Double getQ378() {
		return q378;
	}
	public void setQ378(Double q378) {
		this.q378 = q378;
	}
	public Double getQ379() {
		return q379;
	}
	public void setQ379(Double q379) {
		this.q379 = q379;
	}
	public Double getQ380() {
		return q380;
	}
	public void setQ380(Double q380) {
		this.q380 = q380;
	}
	public Double getQ381() {
		return q381;
	}
	public void setQ381(Double q381) {
		this.q381 = q381;
	}
	public Double getQ382() {
		return q382;
	}
	public void setQ382(Double q382) {
		this.q382 = q382;
	}
	public Double getQ383() {
		return q383;
	}
	public void setQ383(Double q383) {
		this.q383 = q383;
	}
	public Double getQ384() {
		return q384;
	}
	public void setQ384(Double q384) {
		this.q384 = q384;
	}
	public Double getQ385() {
		return q385;
	}
	public void setQ385(Double q385) {
		this.q385 = q385;
	}
	public Double getQ386() {
		return q386;
	}
	public void setQ386(Double q386) {
		this.q386 = q386;
	}
	public Double getQ387() {
		return q387;
	}
	public void setQ387(Double q387) {
		this.q387 = q387;
	}
	public Double getQ388() {
		return q388;
	}
	public void setQ388(Double q388) {
		this.q388 = q388;
	}
	public Double getQ389() {
		return q389;
	}
	public void setQ389(Double q389) {
		this.q389 = q389;
	}
	public Double getQ390() {
		return q390;
	}
	public void setQ390(Double q390) {
		this.q390 = q390;
	}
	public Double getQ391() {
		return q391;
	}
	public void setQ391(Double q391) {
		this.q391 = q391;
	}
	public Double getQ392() {
		return q392;
	}
	public void setQ392(Double q392) {
		this.q392 = q392;
	}
	public Double getQ393() {
		return q393;
	}
	public void setQ393(Double q393) {
		this.q393 = q393;
	}
	public Double getQ394() {
		return q394;
	}
	public void setQ394(Double q394) {
		this.q394 = q394;
	}
	public Double getQ395() {
		return q395;
	}
	public void setQ395(Double q395) {
		this.q395 = q395;
	}
	public Double getQ396() {
		return q396;
	}
	public void setQ396(Double q396) {
		this.q396 = q396;
	}
	public Double getQ397() {
		return q397;
	}
	public void setQ397(Double q397) {
		this.q397 = q397;
	}
	public Double getQ398() {
		return q398;
	}
	public void setQ398(Double q398) {
		this.q398 = q398;
	}
	public Double getQ399() {
		return q399;
	}
	public void setQ399(Double q399) {
		this.q399 = q399;
	}
	public Double getQ400() {
		return q400;
	}
	public void setQ400(Double q400) {
		this.q400 = q400;
	}
	public Double getQ401() {
		return q401;
	}
	public void setQ401(Double q401) {
		this.q401 = q401;
	}
	public Double getQ402() {
		return q402;
	}
	public void setQ402(Double q402) {
		this.q402 = q402;
	}
	public Double getQ403() {
		return q403;
	}
	public void setQ403(Double q403) {
		this.q403 = q403;
	}
	public Double getQ404() {
		return q404;
	}
	public void setQ404(Double q404) {
		this.q404 = q404;
	}
	public Double getQ405() {
		return q405;
	}
	public void setQ405(Double q405) {
		this.q405 = q405;
	}
	public Double getQ406() {
		return q406;
	}
	public void setQ406(Double q406) {
		this.q406 = q406;
	}
	public Double getQ407() {
		return q407;
	}
	public void setQ407(Double q407) {
		this.q407 = q407;
	}
	public Double getQ408() {
		return q408;
	}
	public void setQ408(Double q408) {
		this.q408 = q408;
	}
	public Double getQ409() {
		return q409;
	}
	public void setQ409(Double q409) {
		this.q409 = q409;
	}
	public Double getQ410() {
		return q410;
	}
	public void setQ410(Double q410) {
		this.q410 = q410;
	}
	public Double getQ411() {
		return q411;
	}
	public void setQ411(Double q411) {
		this.q411 = q411;
	}
	public Double getQ412() {
		return q412;
	}
	public void setQ412(Double q412) {
		this.q412 = q412;
	}
	public Double getQ413() {
		return q413;
	}
	public void setQ413(Double q413) {
		this.q413 = q413;
	}
	public Double getQ414() {
		return q414;
	}
	public void setQ414(Double q414) {
		this.q414 = q414;
	}
	public Double getQ415() {
		return q415;
	}
	public void setQ415(Double q415) {
		this.q415 = q415;
	}
	public Double getQ416() {
		return q416;
	}
	public void setQ416(Double q416) {
		this.q416 = q416;
	}
	public Double getQ417() {
		return q417;
	}
	public void setQ417(Double q417) {
		this.q417 = q417;
	}
	public Double getQ418() {
		return q418;
	}
	public void setQ418(Double q418) {
		this.q418 = q418;
	}
	public Double getQ419() {
		return q419;
	}
	public void setQ419(Double q419) {
		this.q419 = q419;
	}
	public Double getQ420() {
		return q420;
	}
	public void setQ420(Double q420) {
		this.q420 = q420;
	}
	public Double getQ421() {
		return q421;
	}
	public void setQ421(Double q421) {
		this.q421 = q421;
	}
	public Double getQ422() {
		return q422;
	}
	public void setQ422(Double q422) {
		this.q422 = q422;
	}
	public Double getQ423() {
		return q423;
	}
	public void setQ423(Double q423) {
		this.q423 = q423;
	}
	public Double getQ424() {
		return q424;
	}
	public void setQ424(Double q424) {
		this.q424 = q424;
	}
	public Double getQ425() {
		return q425;
	}
	public void setQ425(Double q425) {
		this.q425 = q425;
	}
	public Double getQ426() {
		return q426;
	}
	public void setQ426(Double q426) {
		this.q426 = q426;
	}
	public Double getQ427() {
		return q427;
	}
	public void setQ427(Double q427) {
		this.q427 = q427;
	}
	public Double getQ428() {
		return q428;
	}
	public void setQ428(Double q428) {
		this.q428 = q428;
	}
	public Double getQ429() {
		return q429;
	}
	public void setQ429(Double q429) {
		this.q429 = q429;
	}
	public Double getQ430() {
		return q430;
	}
	public void setQ430(Double q430) {
		this.q430 = q430;
	}
	public Double getQ431() {
		return q431;
	}
	public void setQ431(Double q431) {
		this.q431 = q431;
	}
	public Double getQ432() {
		return q432;
	}
	public void setQ432(Double q432) {
		this.q432 = q432;
	}
	public Double getQ433() {
		return q433;
	}
	public void setQ433(Double q433) {
		this.q433 = q433;
	}
	public Double getQ434() {
		return q434;
	}
	public void setQ434(Double q434) {
		this.q434 = q434;
	}
	public Double getQ435() {
		return q435;
	}
	public void setQ435(Double q435) {
		this.q435 = q435;
	}
	public Double getQ436() {
		return q436;
	}
	public void setQ436(Double q436) {
		this.q436 = q436;
	}
	public Double getQ437() {
		return q437;
	}
	public void setQ437(Double q437) {
		this.q437 = q437;
	}
	public Double getQ438() {
		return q438;
	}
	public void setQ438(Double q438) {
		this.q438 = q438;
	}
	public Double getQ439() {
		return q439;
	}
	public void setQ439(Double q439) {
		this.q439 = q439;
	}
	public Double getQ440() {
		return q440;
	}
	public void setQ440(Double q440) {
		this.q440 = q440;
	}
	public Double getQ441() {
		return q441;
	}
	public void setQ441(Double q441) {
		this.q441 = q441;
	}
	public Double getQ442() {
		return q442;
	}
	public void setQ442(Double q442) {
		this.q442 = q442;
	}
	public Double getQ443() {
		return q443;
	}
	public void setQ443(Double q443) {
		this.q443 = q443;
	}
	public Double getQ444() {
		return q444;
	}
	public void setQ444(Double q444) {
		this.q444 = q444;
	}
	public Double getQ445() {
		return q445;
	}
	public void setQ445(Double q445) {
		this.q445 = q445;
	}
	public Double getQ446() {
		return q446;
	}
	public void setQ446(Double q446) {
		this.q446 = q446;
	}
	public Double getQ447() {
		return q447;
	}
	public void setQ447(Double q447) {
		this.q447 = q447;
	}
	public Double getQ448() {
		return q448;
	}
	public void setQ448(Double q448) {
		this.q448 = q448;
	}
	public Double getQ449() {
		return q449;
	}
	public void setQ449(Double q449) {
		this.q449 = q449;
	}
	public Double getQ450() {
		return q450;
	}
	public void setQ450(Double q450) {
		this.q450 = q450;
	}
	public Double getQ451() {
		return q451;
	}
	public void setQ451(Double q451) {
		this.q451 = q451;
	}
	public Double getQ452() {
		return q452;
	}
	public void setQ452(Double q452) {
		this.q452 = q452;
	}
	public Double getQ453() {
		return q453;
	}
	public void setQ453(Double q453) {
		this.q453 = q453;
	}
	public Double getQ454() {
		return q454;
	}
	public void setQ454(Double q454) {
		this.q454 = q454;
	}
	public Double getQ455() {
		return q455;
	}
	public void setQ455(Double q455) {
		this.q455 = q455;
	}
	public Double getQ456() {
		return q456;
	}
	public void setQ456(Double q456) {
		this.q456 = q456;
	}
	public Double getQ457() {
		return q457;
	}
	public void setQ457(Double q457) {
		this.q457 = q457;
	}
	public Double getQ458() {
		return q458;
	}
	public void setQ458(Double q458) {
		this.q458 = q458;
	}
	public Double getQ459() {
		return q459;
	}
	public void setQ459(Double q459) {
		this.q459 = q459;
	}
	public Double getQ460() {
		return q460;
	}
	public void setQ460(Double q460) {
		this.q460 = q460;
	}
	public Double getQ461() {
		return q461;
	}
	public void setQ461(Double q461) {
		this.q461 = q461;
	}
	public Double getQ462() {
		return q462;
	}
	public void setQ462(Double q462) {
		this.q462 = q462;
	}
	public Double getQ463() {
		return q463;
	}
	public void setQ463(Double q463) {
		this.q463 = q463;
	}
	public Double getQ464() {
		return q464;
	}
	public void setQ464(Double q464) {
		this.q464 = q464;
	}
	public Double getQ465() {
		return q465;
	}
	public void setQ465(Double q465) {
		this.q465 = q465;
	}
	public Double getQ466() {
		return q466;
	}
	public void setQ466(Double q466) {
		this.q466 = q466;
	}
	public Double getQ467() {
		return q467;
	}
	public void setQ467(Double q467) {
		this.q467 = q467;
	}
	public Double getQ468() {
		return q468;
	}
	public void setQ468(Double q468) {
		this.q468 = q468;
	}
	public Double getQ469() {
		return q469;
	}
	public void setQ469(Double q469) {
		this.q469 = q469;
	}
	public Double getQ470() {
		return q470;
	}
	public void setQ470(Double q470) {
		this.q470 = q470;
	}
	public Double getQ471() {
		return q471;
	}
	public void setQ471(Double q471) {
		this.q471 = q471;
	}
	public Double getQ472() {
		return q472;
	}
	public void setQ472(Double q472) {
		this.q472 = q472;
	}
	public Double getQ473() {
		return q473;
	}
	public void setQ473(Double q473) {
		this.q473 = q473;
	}
	public Double getQ474() {
		return q474;
	}
	public void setQ474(Double q474) {
		this.q474 = q474;
	}
	public Double getQ475() {
		return q475;
	}
	public void setQ475(Double q475) {
		this.q475 = q475;
	}
	public Double getQ476() {
		return q476;
	}
	public void setQ476(Double q476) {
		this.q476 = q476;
	}
	public Double getQ477() {
		return q477;
	}
	public void setQ477(Double q477) {
		this.q477 = q477;
	}
	public Double getQ478() {
		return q478;
	}
	public void setQ478(Double q478) {
		this.q478 = q478;
	}
	public Double getQ479() {
		return q479;
	}
	public void setQ479(Double q479) {
		this.q479 = q479;
	}
	public Double getQ480() {
		return q480;
	}
	public void setQ480(Double q480) {
		this.q480 = q480;
	}
	public Double getQ481() {
		return q481;
	}
	public void setQ481(Double q481) {
		this.q481 = q481;
	}
	public Double getQ482() {
		return q482;
	}
	public void setQ482(Double q482) {
		this.q482 = q482;
	}
	public Double getQ483() {
		return q483;
	}
	public void setQ483(Double q483) {
		this.q483 = q483;
	}
	public Double getQ484() {
		return q484;
	}
	public void setQ484(Double q484) {
		this.q484 = q484;
	}
	public Double getQ485() {
		return q485;
	}
	public void setQ485(Double q485) {
		this.q485 = q485;
	}
	public Double getQ486() {
		return q486;
	}
	public void setQ486(Double q486) {
		this.q486 = q486;
	}
	public Double getQ487() {
		return q487;
	}
	public void setQ487(Double q487) {
		this.q487 = q487;
	}
	public Double getQ488() {
		return q488;
	}
	public void setQ488(Double q488) {
		this.q488 = q488;
	}
	public Double getQ489() {
		return q489;
	}
	public void setQ489(Double q489) {
		this.q489 = q489;
	}
	public Double getQ490() {
		return q490;
	}
	public void setQ490(Double q490) {
		this.q490 = q490;
	}
	public Double getQ491() {
		return q491;
	}
	public void setQ491(Double q491) {
		this.q491 = q491;
	}
	public Double getQ492() {
		return q492;
	}
	public void setQ492(Double q492) {
		this.q492 = q492;
	}
	public Double getQ493() {
		return q493;
	}
	public void setQ493(Double q493) {
		this.q493 = q493;
	}
	public Double getQ494() {
		return q494;
	}
	public void setQ494(Double q494) {
		this.q494 = q494;
	}
	public Double getQ495() {
		return q495;
	}
	public void setQ495(Double q495) {
		this.q495 = q495;
	}
	public Double getQ496() {
		return q496;
	}
	public void setQ496(Double q496) {
		this.q496 = q496;
	}
	public Double getQ497() {
		return q497;
	}
	public void setQ497(Double q497) {
		this.q497 = q497;
	}
	public Double getQ498() {
		return q498;
	}
	public void setQ498(Double q498) {
		this.q498 = q498;
	}
	public Double getQ499() {
		return q499;
	}
	public void setQ499(Double q499) {
		this.q499 = q499;
	}
	public Double getQ500() {
		return q500;
	}
	public void setQ500(Double q500) {
		this.q500 = q500;
	}		
	
	
}
