package tm.bean;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.i4.I4QuestionSets;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;

@Entity
@Table(name="cgstatuseventtemp")
public class CGStatusEventTemp implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3771770320250365754L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer cgStatusEventTempId;  

	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;

	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	private String sessionId;
	
	private Date date;
	
	public Integer getCgStatusEventTempId() {
		return cgStatusEventTempId;
	}

	public void setCgStatusEventTempId(Integer cgStatusEventTempId) {
		this.cgStatusEventTempId = cgStatusEventTempId;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof CGStatusEventTemp))return false;
	    CGStatusEventTemp cgStatusEventTemp = (CGStatusEventTemp)object;
	  
	    if(this.cgStatusEventTempId.equals(cgStatusEventTemp.getCgStatusEventTempId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+cgStatusEventTempId);
	}

	@Override
	public String toString() 
	{
		return "["+cgStatusEventTempId+"]";
	}
	
	
}
