package tm.bean.user;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="personalemailtrack")
public class PersonalEmailTrack implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long personalID;
	
    private	String personalEmailAddress;
    private String personalEmailPassword;
    private String personalStatus;
    private String mailHost;
    private String mailPort;
    private Date lastmailreadtime;
    private Integer districtId;
  ///////////shriram 17 sep 2015 this field is add in the personalEmailTrack table.
  private Integer userId;
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	///////////
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public Date getLastmailreadtime() {
		return lastmailreadtime;
	}
	public void setLastmailreadtime(Date lastmailreadtime) {
		this.lastmailreadtime = lastmailreadtime;
	}
	public String getMailHost() {
		return mailHost;
	}
	public void setMailHost(String mailHost) {
		this.mailHost = mailHost;
	}
	public String getMailPort() {
		return mailPort;
	}
	public void setMailPort(String mailPort) {
		this.mailPort = mailPort;
	}
	public Long getPersonalID() {
		return personalID;
	}
	public void setPersonalID(Long personalID) {
		this.personalID = personalID;
	}
	public String getPersonalEmailAddress() {
		return personalEmailAddress;
	}
	public void setPersonalEmailAddress(String personalEmailAddress) {
		this.personalEmailAddress = personalEmailAddress;
	}
	public String getPersonalEmailPassword() {
		return personalEmailPassword;
	}
	public void setPersonalEmailPassword(String personalEmailPassword) {
		this.personalEmailPassword = personalEmailPassword;
	}
	public String getPersonalStatus() {
		return personalStatus;
	}
	public void setPersonalStatus(String personalStatus) {
		this.personalStatus = personalStatus;
	}
    
	

	
	
	
	}
