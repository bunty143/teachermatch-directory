package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="jobforteacherhistory")
public class JobForTeacherHistory implements Serializable
{
	private static final long serialVersionUID = 2691836501063586924L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long jobForTeacherId;     
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherId;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobId;     
	private String redirectedFromURL;
	
	@ManyToOne
	@JoinColumn(name="status",referencedColumnName="statusId")
	private StatusMaster status; 
	
	private Boolean cgUpdated;	
	private String coverLetter;
	private Integer isAffilated;
	private String staffType;
	private Boolean flagForDistrictSpecificQuestions;

	@ManyToOne
	@JoinColumn(name="updatedBy",referencedColumnName="userId")
	private UserMaster updatedBy; 
	private Integer updatedByEntity; 
	private Date updatedDate; 
	private Date createdDateTime;
	private String lastActivity;
	private Date lastActivityDate;
	private String noteForDistrictSpecificQuestions;
	private Boolean isDistrictSpecificNoteFinalize;
	
	
	@ManyToOne
	@JoinColumn(name="lastActivityDoneBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	@ManyToOne
	@JoinColumn(name="displayStatusId",referencedColumnName="statusId")
	private StatusMaster statusMaster;
	
	@ManyToOne
	@JoinColumn(name="displaySecondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus;
	
	@ManyToOne
	@JoinColumn(name="internalStatus",referencedColumnName="statusId")
	private StatusMaster internalStatus; 
	
	@ManyToOne
	@JoinColumn(name="internalSecondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus internalSecondaryStatus;
	
	@ManyToOne
	@JoinColumn(name="hiredBySchool",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	private String  requisitionNumber;
	
	private Boolean offerReady;
	
	private Date offerMadeDate;
	
	private Boolean offerAccepted;
	
	private Date offerAcceptedDate;
	
	private Boolean noResponseEmail;
	
	private Boolean candidateConsideration;

	private String notes;
	
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getJobForTeacherId() {
		return jobForTeacherId;
	}

	public void setJobForTeacherId(Long jobForTeacherId) {
		this.jobForTeacherId = jobForTeacherId;
	}

	public TeacherDetail getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}

	public JobOrder getJobId() {
		return jobId;
	}

	public void setJobId(JobOrder jobId) {
		this.jobId = jobId;
	}

	public String getRedirectedFromURL() {
		return redirectedFromURL;
	}

	public void setRedirectedFromURL(String redirectedFromURL) {
		this.redirectedFromURL = redirectedFromURL;
	}

	public StatusMaster getStatus() {
		return status;
	}

	public void setStatus(StatusMaster status) {
		this.status = status;
	}

	public Boolean getCgUpdated() {
		return cgUpdated;
	}

	public void setCgUpdated(Boolean cgUpdated) {
		this.cgUpdated = cgUpdated;
	}

	public String getCoverLetter() {
		return coverLetter;
	}

	public void setCoverLetter(String coverLetter) {
		this.coverLetter = coverLetter;
	}

	public Integer getIsAffilated() {
		return isAffilated;
	}

	public void setIsAffilated(Integer isAffilated) {
		this.isAffilated = isAffilated;
	}

	public String getStaffType() {
		return staffType;
	}

	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}

	public Boolean getFlagForDistrictSpecificQuestions() {
		return flagForDistrictSpecificQuestions;
	}

	public void setFlagForDistrictSpecificQuestions(
			Boolean flagForDistrictSpecificQuestions) {
		this.flagForDistrictSpecificQuestions = flagForDistrictSpecificQuestions;
	}

	public UserMaster getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(UserMaster updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getUpdatedByEntity() {
		return updatedByEntity;
	}

	public void setUpdatedByEntity(Integer updatedByEntity) {
		this.updatedByEntity = updatedByEntity;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getLastActivity() {
		return lastActivity;
	}

	public void setLastActivity(String lastActivity) {
		this.lastActivity = lastActivity;
	}

	public Date getLastActivityDate() {
		return lastActivityDate;
	}

	public void setLastActivityDate(Date lastActivityDate) {
		this.lastActivityDate = lastActivityDate;
	}

	public String getNoteForDistrictSpecificQuestions() {
		return noteForDistrictSpecificQuestions;
	}

	public void setNoteForDistrictSpecificQuestions(
			String noteForDistrictSpecificQuestions) {
		this.noteForDistrictSpecificQuestions = noteForDistrictSpecificQuestions;
	}

	public Boolean getIsDistrictSpecificNoteFinalize() {
		return isDistrictSpecificNoteFinalize;
	}

	public void setIsDistrictSpecificNoteFinalize(
			Boolean isDistrictSpecificNoteFinalize) {
		this.isDistrictSpecificNoteFinalize = isDistrictSpecificNoteFinalize;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public StatusMaster getStatusMaster() {
		return statusMaster;
	}

	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}

	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}

	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}

	public StatusMaster getInternalStatus() {
		return internalStatus;
	}

	public void setInternalStatus(StatusMaster internalStatus) {
		this.internalStatus = internalStatus;
	}

	public SecondaryStatus getInternalSecondaryStatus() {
		return internalSecondaryStatus;
	}

	public void setInternalSecondaryStatus(SecondaryStatus internalSecondaryStatus) {
		this.internalSecondaryStatus = internalSecondaryStatus;
	}

	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}

	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}

	public String getRequisitionNumber() {
		return requisitionNumber;
	}

	public void setRequisitionNumber(String requisitionNumber) {
		this.requisitionNumber = requisitionNumber;
	}

	public Boolean getOfferReady() {
		return offerReady;
	}

	public void setOfferReady(Boolean offerReady) {
		this.offerReady = offerReady;
	}

	public Date getOfferMadeDate() {
		return offerMadeDate;
	}

	public void setOfferMadeDate(Date offerMadeDate) {
		this.offerMadeDate = offerMadeDate;
	}

	public Boolean getOfferAccepted() {
		return offerAccepted;
	}

	public void setOfferAccepted(Boolean offerAccepted) {
		this.offerAccepted = offerAccepted;
	}

	public Date getOfferAcceptedDate() {
		return offerAcceptedDate;
	}

	public void setOfferAcceptedDate(Date offerAcceptedDate) {
		this.offerAcceptedDate = offerAcceptedDate;
	}

	public Boolean getNoResponseEmail() {
		return noResponseEmail;
	}

	public void setNoResponseEmail(Boolean noResponseEmail) {
		this.noResponseEmail = noResponseEmail;
	}

	public Boolean getCandidateConsideration() {
		return candidateConsideration;
	}

	public void setCandidateConsideration(Boolean candidateConsideration) {
		this.candidateConsideration = candidateConsideration;
	}
	
		
}
