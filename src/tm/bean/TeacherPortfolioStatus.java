package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="teacherportfoliostatus")
public class TeacherPortfolioStatus implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1643191351116586321L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherId;
	private Boolean isPersonalInfoCompleted;
	private Boolean isAcademicsCompleted;
	private Boolean isCertificationsCompleted;
	private Boolean isExperiencesCompleted;
	private Boolean isAffidavitCompleted;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public TeacherDetail getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}
	public Boolean getIsPersonalInfoCompleted() {
		return isPersonalInfoCompleted;
	}
	public void setIsPersonalInfoCompleted(Boolean isPersonalInfoCompleted) {
		this.isPersonalInfoCompleted = isPersonalInfoCompleted;
	}
	public Boolean getIsAcademicsCompleted() {
		return isAcademicsCompleted;
	}
	public void setIsAcademicsCompleted(Boolean isAcademicsCompleted) {
		this.isAcademicsCompleted = isAcademicsCompleted;
	}
	public Boolean getIsCertificationsCompleted() {
		return isCertificationsCompleted;
	}
	public void setIsCertificationsCompleted(Boolean isCertificationsCompleted) {
		this.isCertificationsCompleted = isCertificationsCompleted;
	}
	public Boolean getIsExperiencesCompleted() {
		return isExperiencesCompleted;
	}
	public void setIsExperiencesCompleted(Boolean isExperiencesCompleted) {
		this.isExperiencesCompleted = isExperiencesCompleted;
	}
	public Boolean getIsAffidavitCompleted() {
		return isAffidavitCompleted;
	}
	public void setIsAffidavitCompleted(Boolean isAffidavitCompleted) {
		this.isAffidavitCompleted = isAffidavitCompleted;
	}
	
}
