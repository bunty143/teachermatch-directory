package tm.bean.menu;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="accessoptionmaster")
public class AccessOptionMaster implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2441216517868979318L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer accessOptionId;
	private String optionName;
	private String status;

	public Integer getAccessOptionId() {
		return accessOptionId;
	}
	public void setAccessOptionId(Integer accessOptionId) {
		this.accessOptionId = accessOptionId;
	}
	public String getOptionName() {
		return optionName;
	}
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
