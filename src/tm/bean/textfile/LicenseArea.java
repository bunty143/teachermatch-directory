package tm.bean.textfile;

import java.util.Date;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.user.UserMaster;
/* 
 * @Author: Sandeep Yadav
 */
@Entity
@Table(name="applicantlicensearea")
public class LicenseArea {

	
	private static final long serialVersionUID = 2183204264206577556L;
		
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Integer licenseAreaId; 
		private String SSN;
		private String licenseArea;
		private String licenseClassLevelCode;
		private String licenseProgramStatusCode;
		private String licenseProgramBasisCode;
		private String yearsOfExperience;
		private String licenseAreaHQCode;
		private Date  licenseAreaEffectiveDate;
		private String status;
		private Date  createdDate;
		
		@ManyToOne
		@JoinColumn(name="createdBy",referencedColumnName="userId")
		private UserMaster userMaster;

		public Integer getLicenseAreaId() {
			return licenseAreaId;
		}

		public void setLicenseAreaId(Integer licenseAreaId) {
			this.licenseAreaId = licenseAreaId;
		}

		public String getSSN() {
			return SSN;
		}

		public void setSSN(String sSN) {
			SSN = sSN;
		}

		public String getLicenseArea() {
			return licenseArea;
		}

		public void setLicenseArea(String licenseArea) {
			this.licenseArea = licenseArea;
		}

		public String getLicenseClassLevelCode() {
			return licenseClassLevelCode;
		}

		public void setLicenseClassLevelCode(String licenseClassLevelCode) {
			this.licenseClassLevelCode = licenseClassLevelCode;
		}

		public String getLicenseProgramStatusCode() {
			return licenseProgramStatusCode;
		}

		public void setLicenseProgramStatusCode(String licenseProgramStatusCode) {
			this.licenseProgramStatusCode = licenseProgramStatusCode;
		}

		public String getLicenseProgramBasisCode() {
			return licenseProgramBasisCode;
		}

		public void setLicenseProgramBasisCode(String licenseProgramBasisCode) {
			this.licenseProgramBasisCode = licenseProgramBasisCode;
		}

		public String getYearsOfExperience() {
			return yearsOfExperience;
		}

		public void setYearsOfExperience(String yearsOfExperience) {
			this.yearsOfExperience = yearsOfExperience;
		}

		public String getLicenseAreaHQCode() {
			return licenseAreaHQCode;
		}

		public void setLicenseAreaHQCode(String licenseAreaHQCode) {
			this.licenseAreaHQCode = licenseAreaHQCode;
		}

		public Date getLicenseAreaEffectiveDate() {
			return licenseAreaEffectiveDate;
		}

		public void setLicenseAreaEffectiveDate(Date licenseAreaEffectiveDate) {
			this.licenseAreaEffectiveDate = licenseAreaEffectiveDate;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Date getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}

		public UserMaster getUserMaster() {
			return userMaster;
		}

		public void setUserMaster(UserMaster userMaster) {
			this.userMaster = userMaster;
		}
		
		
		
		
		
		
}
