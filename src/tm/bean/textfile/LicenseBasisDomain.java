package tm.bean.textfile;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;
/* 
 * @Author: Sandeep Yadav
 */


@Entity
@Table(name="applicantlicensebasisdomain")
public class LicenseBasisDomain implements Serializable{
	
	private static final long serialVersionUID = 2183204264206577556L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer licenseBasisDomainId; 
	private String programBasisCode;
	private String programBasisDescription;
	private String status;
	private Date  createdDate;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;

	public Integer getLicenseBasisDomainId() {
		return licenseBasisDomainId;
	}

	public void setLicenseBasisDomainId(Integer licenseBasisDomainId) {
		this.licenseBasisDomainId = licenseBasisDomainId;
	}

	public String getProgramBasisCode() {
		return programBasisCode;
	}

	public void setProgramBasisCode(String programBasisCode) {
		this.programBasisCode = programBasisCode;
	}

	public String getProgramBasisDescription() {
		return programBasisDescription;
	}

	public void setProgramBasisDescription(String programBasisDescription) {
		this.programBasisDescription = programBasisDescription;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	
	

}
