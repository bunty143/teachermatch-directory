package tm.bean.textfile;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;
/* 
 * @Author: Sandeep Yadav
 */


@Entity
@Table(name="applicantlicensedetails")
public class License implements Serializable{
	
	private static final long serialVersionUID = 2183204264206577556L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer licenseId; 
	private String SSN;
	private Date licenseExpireDate;
	private Date licenseLastIssueDate;
	private Date licenseEffectiveDate;
	private Date licenseRevokeRescindDate;
	private String stateCode;
	private String status;
	private Date  createdDate;
	private String revocationStatus;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;

	
	
	public Integer getLicenseId() {
		return licenseId;
	}

	public void setLicenseId(Integer licenseId) {
		this.licenseId = licenseId;
	}

	public String getSSN() {
		return SSN;
	}

	public void setSSN(String sSN) {
		SSN = sSN;
	}

	public Date getLicenseExpireDate() {
		return licenseExpireDate;
	}

	public void setLicenseExpireDate(Date licenseExpireDate) {
		this.licenseExpireDate = licenseExpireDate;
	}

	public Date getLicenseLastIssueDate() {
		return licenseLastIssueDate;
	}

	public void setLicenseLastIssueDate(Date licenseLastIssueDate) {
		this.licenseLastIssueDate = licenseLastIssueDate;
	}

	public Date getLicenseEffectiveDate() {
		return licenseEffectiveDate;
	}

	public void setLicenseEffectiveDate(Date licenseEffectiveDate) {
		this.licenseEffectiveDate = licenseEffectiveDate;
	}

	public Date getLicenseRevokeRescindDate() {
		return licenseRevokeRescindDate;
	}

	public void setLicenseRevokeRescindDate(Date licenseRevokeRescindDate) {
		this.licenseRevokeRescindDate = licenseRevokeRescindDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getRevocationStatus() {
		return revocationStatus;
	}

	public void setRevocationStatus(String revocationStatus) {
		this.revocationStatus = revocationStatus;
	}
	
	
	
	

}
