package tm.bean.textfile;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;
/* 
 * @Author: Sandeep Yadav
 */


@Entity
@Table(name="licenseclassleveldomain")
public class LicenseClassLevelDomain implements Serializable{
	
	private static final long serialVersionUID = 2183204264206577556L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer classLevelDomainId; 
	private String classCode;	
	private String classCodeDescription;
	public Integer getClassLevelDomainId() {
		return classLevelDomainId;
	}
	public void setClassLevelDomainId(Integer classLevelDomainId) {
		this.classLevelDomainId = classLevelDomainId;
	}
	public String getClassCode() {
		return classCode;
	}
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}
	public String getClassCodeDescription() {
		return classCodeDescription;
	}
	public void setClassCodeDescription(String classCodeDescription) {
		this.classCodeDescription = classCodeDescription;
	}
	
	
	
	
	
	

}
