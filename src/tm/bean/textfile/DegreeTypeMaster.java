package tm.bean.textfile;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="degreetypemaster")
public class DegreeTypeMaster {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer degreeTypeId; 
	
	String degreeTypeCode;
	String degreeName;
	String status;
	Date createdDateTime;
	public Integer getDegreeTypeId() {
		return degreeTypeId;
	}
	public void setDegreeTypeId(Integer degreeTypeId) {
		this.degreeTypeId = degreeTypeId;
	}
	public String getDegreeTypeCode() {
		return degreeTypeCode;
	}
	public void setDegreeTypeCode(String degreeTypeCode) {
		this.degreeTypeCode = degreeTypeCode;
	}
	public String getDegreeName() {
		return degreeName;
	}
	public void setDegreeName(String degreeName) {
		this.degreeName = degreeName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
	
	
	

}
