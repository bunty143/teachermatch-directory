package tm.bean.textfile;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="licensureeducation")
public class LicensureEducation {


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer licensureeducationId; 
	
	private String SSN;
	
	private String DegreeType;
	
	private String IHECode;
	
	private Date GraduationDate; 
	
	private String Major;
	
	private String status;
	
	private Boolean educationFlag;
	
	@Column(name = "createdDate", columnDefinition="")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	private int createdBy;
	
	
	
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLicensureeducationId() {
		return licensureeducationId;
	}

	public void setLicensureeducationId(Integer licensureeducationId) {
		this.licensureeducationId = licensureeducationId;
	}

	public String getSSN() {
		return SSN;
	}

	public void setSSN(String sSN) {
		SSN = sSN;
	}

	public String getDegreeType() {
		return DegreeType;
	}

	public void setDegreeType(String degreeType) {
		DegreeType = degreeType;
	}

	 
	public Date getGraduationDate() {
		return GraduationDate;
	}

	public void setGraduationDate(Date graduationDate) {
		GraduationDate = graduationDate;
	}

	 
	public String getIHECode() {
		return IHECode;
	}

	public void setIHECode(String iHECode) {
		IHECode = iHECode;
	}

	public String getMajor() {
		return Major;
	}

	public void setMajor(String major) {
		Major = major;
	}

	public Boolean getEducationFlag() {
		return educationFlag;
	}

	public void setEducationFlag(Boolean educationFlag) {
		this.educationFlag = educationFlag;
	}
	
	
	
	
	
}
