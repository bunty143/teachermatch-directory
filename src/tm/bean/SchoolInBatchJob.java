package tm.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;

@Entity
@Table(name="schoolinbatchjob")
public class SchoolInBatchJob implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5153713773361659061L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int batchJobSchoolId;  
	
	@ManyToOne
	@JoinColumn(name="batchJobId",referencedColumnName="batchJobId")
	private BatchJobOrder  batchJobId;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster  schoolId;
	
	private Integer noOfSchoolExpHires;
	private boolean jobAccepted;
	private boolean acceptStatus;
	private Integer correspondingSchoolJobOrder;	
	private Integer acceptedBy;
	private Date acceptedDateTime;  
	


	public Integer getCorrespondingSchoolJobOrder() {
		return correspondingSchoolJobOrder;
	}
	public void setCorrespondingSchoolJobOrder(Integer correspondingSchoolJobOrder) {
		this.correspondingSchoolJobOrder = correspondingSchoolJobOrder;
	}
	public boolean isAcceptStatus() {
		return acceptStatus;
	}
	public void setAcceptStatus(boolean acceptStatus) {
		this.acceptStatus = acceptStatus;
	}
	public boolean isJobAccepted() {
		return jobAccepted;
	}
	public void setJobAccepted(boolean jobAccepted) {
		this.jobAccepted = jobAccepted;
	}
	public Integer getNoOfSchoolExpHires() {
		return noOfSchoolExpHires;
	}
	public void setNoOfSchoolExpHires(Integer noOfSchoolExpHires) {
		this.noOfSchoolExpHires = noOfSchoolExpHires;
	}
	public int getBatchJobSchoolId() {
		return batchJobSchoolId;
	}
	public void setBatchJobSchoolId(int batchJobSchoolId) {
		this.batchJobSchoolId = batchJobSchoolId;
	}
	public BatchJobOrder getBatchJobId() {
		return batchJobId;
	}
	public void setBatchJobId(BatchJobOrder batchJobId) {
		this.batchJobId = batchJobId;
	}
	public SchoolMaster getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(SchoolMaster schoolId) {
		this.schoolId = schoolId;
	}

	public Integer getAcceptedBy() {
		return acceptedBy;
	}
	public void setAcceptedBy(Integer acceptedBy) {
		this.acceptedBy = acceptedBy;
	}
	public Date getAcceptedDateTime() {
		return acceptedDateTime;
	}
	public void setAcceptedDateTime(Date acceptedDateTime) {
		this.acceptedDateTime = acceptedDateTime;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
