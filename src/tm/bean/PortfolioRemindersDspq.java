package tm.bean;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioQuestions;

@Entity
@Table(name="portfolioremindersdspq")
public class PortfolioRemindersDspq implements Serializable
{
   	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1088242234431392909L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer portfolioremindersdspqId;
	
	@ManyToOne
	@JoinColumn(name="questionId",referencedColumnName="questionId")
	private DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions;
	
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private Date createdDateTime;
	
	public Integer getPortfolioremindersdspqId() {
		return portfolioremindersdspqId;
	}

	public void setPortfolioremindersdspqId(Integer portfolioremindersdspqId) {
		this.portfolioremindersdspqId = portfolioremindersdspqId;
	}

	public DistrictSpecificPortfolioQuestions getDistrictSpecificPortfolioQuestions() {
		return districtSpecificPortfolioQuestions;
	}

	public void setDistrictSpecificPortfolioQuestions(
			DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions) {
		this.districtSpecificPortfolioQuestions = districtSpecificPortfolioQuestions;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
}
