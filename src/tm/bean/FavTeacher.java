package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.utility.Utility;

@Entity
@Table(name="favteacher")
public class FavTeacher implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1177634134668990926L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
 	private Integer favTeacherId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherId;
	
	private String favTeacherName;
	private String favTeacherStory;
	
	
	public Integer getFavTeacherId() {
		return favTeacherId;
	}
	public void setFavTeacherId(Integer favTeacherId) {
		this.favTeacherId = favTeacherId;
	}
	public TeacherDetail getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}
	public String getFavTeacherName() {
		return favTeacherName;
	}
	public void setFavTeacherName(String favTeacherName) {
		this.favTeacherName = Utility.trim(favTeacherName);
	}
	public String getFavTeacherStory() {
		return favTeacherStory;
	}
	public void setFavTeacherStory(String favTeacherStory) {
		this.favTeacherStory = Utility.trim(favTeacherStory);
	}
}
