package tm.bean;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import tm.bean.master.*;

@Entity
@Table(name="teachersubjectareaexam")
public class TeacherSubjectAreaExam implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5334874205909539913L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherSubjectAreaExamId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	private String examStatus;
	
	private Date examDate;
	
	@ManyToOne
	@JoinColumn(name="subjectAreaExamId",referencedColumnName="subjectAreaExamId")
	private SubjectAreaExamMaster subjectAreaExamMaster;
	
	private String scoreReport;
	
	private String examNote;
	private Date createdDateTime;
	

	public Integer getTeacherSubjectAreaExamId() {
		return teacherSubjectAreaExamId;
	}

	public void setTeacherSubjectAreaExamId(Integer teacherSubjectAreaExamId) {
		this.teacherSubjectAreaExamId = teacherSubjectAreaExamId;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public String getExamStatus() {
		return examStatus;
	}

	public void setExamStatus(String examStatus) {
		this.examStatus = examStatus;
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

	public SubjectAreaExamMaster getSubjectAreaExamMaster() {
		return subjectAreaExamMaster;
	}

	public void setSubjectAreaExamMaster(SubjectAreaExamMaster subjectAreaExamMaster) {
		this.subjectAreaExamMaster = subjectAreaExamMaster;
	}

	public String getScoreReport() {
		return scoreReport;
	}

	public void setScoreReport(String scoreReport) {
		this.scoreReport = scoreReport;
	}
	
	public String getExamNote() {
		return examNote;
	}

	public void setExamNote(String examNote) {
		this.examNote = examNote;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	

	
}
