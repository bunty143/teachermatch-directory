package tm.bean;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class TempdocsData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6914476098434366206L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer tempdocsDataId;
	private Integer docsId;
	private String title;
	private String type;
	private String description;
	private String sessionid;
	private String status;
	
	public Integer getTempdocsDataId() {
		return tempdocsDataId;
	}
	public void setTempdocsDataId(Integer tempdocsDataId) {
		this.tempdocsDataId = tempdocsDataId;
	}
	public Integer getDocsId() {
		return docsId;
	}
	public void setDocsId(Integer docsId) {
		this.docsId = docsId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSessionid() {
		return sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

}
