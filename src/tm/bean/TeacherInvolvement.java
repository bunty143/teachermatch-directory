package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.OrgTypeMaster;
import tm.bean.master.PeopleRangeMaster;
import tm.utility.Utility;

@Entity
@Table(name="teacherinvolvement")
public class TeacherInvolvement implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4682887060985346056L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer involvementId;  
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;     
	
	private String organization;
	
	@ManyToOne
	@JoinColumn(name="orgTypeId",referencedColumnName="orgTypeId")
	private OrgTypeMaster orgTypeMaster;
	
	@ManyToOne
	@JoinColumn(name="totalNoOfPeople",referencedColumnName="rangeId")
	private PeopleRangeMaster peopleRangeMaster;
	
	private Integer leadNoOfPeople;
	public Integer getInvolvementId() {
		return involvementId;
	}
	public void setInvolvementId(Integer involvementId) {
		this.involvementId = involvementId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = Utility.trim(organization);
	}
	public OrgTypeMaster getOrgTypeMaster() {
		return orgTypeMaster;
	}
	public void setOrgTypeMaster(OrgTypeMaster orgTypeMaster) {
		this.orgTypeMaster = orgTypeMaster;
	}
	public PeopleRangeMaster getPeopleRangeMaster() {
		return peopleRangeMaster;
	}
	public void setPeopleRangeMaster(PeopleRangeMaster peopleRangeMaster) {
		this.peopleRangeMaster = peopleRangeMaster;
	}
	public Integer getLeadNoOfPeople() {
		return leadNoOfPeople;
	}
	public void setLeadNoOfPeople(Integer leadNoOfPeople) {
		this.leadNoOfPeople = leadNoOfPeople;
	}
}
