package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.StateMaster;


@Entity
@Table(name="batchjobcertification")
public class BatchJobCertification implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9121207989105655792L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int batchJobCertificationId;     
	@ManyToOne	
	@JoinColumn(name="batchJobId",referencedColumnName="batchJobId")
	private BatchJobOrder batchJobId;    
	private String certType;
	
	@ManyToOne
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private StateMaster stateMaster;
	
	@ManyToOne
	@JoinColumn(name="certTypeId",referencedColumnName="certTypeId")
	private CertificateTypeMaster certificateTypeMaster;
	
	public int getBatchJobCertificationId() {
		return batchJobCertificationId;
	}
	public void setBatchJobCertificationId(int batchJobCertificationId) {
		this.batchJobCertificationId = batchJobCertificationId;
	}
	public BatchJobOrder getBatchJobId() {
		return batchJobId;
	}
	public void setBatchJobId(BatchJobOrder batchJobId) {
		this.batchJobId = batchJobId;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = certType;
	}
	public CertificateTypeMaster getCertificateTypeMaster() {
		return certificateTypeMaster;
	}
	public void setCertificateTypeMaster(CertificateTypeMaster certificateTypeMaster) {
		this.certificateTypeMaster = certificateTypeMaster;
	}
	
	public StateMaster getStateMaster() {
		return stateMaster;
	}
	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
