package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
@Entity
@Table(name="statusspecificemailtemplates")
public class StatusSpecificEmailTemplates implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5535313482398918897L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int templateId;
	
	@ManyToOne
	@JoinColumn(name="districtId", referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	private String statusShortName;
	
	private String subjectLine;
	private String templateBody;
	
	private String status;

	public int getTemplateId() {
		return templateId;
	}

	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public String getStatusShortName() {
		return statusShortName;
	}

	public void setStatusShortName(String statusShortName) {
		this.statusShortName = statusShortName;
	}

	public String getSubjectLine() {
		return subjectLine;
	}

	public void setSubjectLine(String subjectLine) {
		this.subjectLine = subjectLine;
	}

	public String getTemplateBody() {
		return templateBody;
	}

	public void setTemplateBody(String templateBody) {
		this.templateBody = templateBody;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
