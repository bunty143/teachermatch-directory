package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="jobcategorytransaction")
public class JobCategoryTransaction implements Serializable{
	
	private static final long serialVersionUID = 2464512256477305545L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer jobCategoryTransactionId; 
	private Integer jobCategoryId;
	private String candidateType;
	private Integer portfolio;
	private String portfolioStatus;
	private Integer prescreen;
	private String prescreenStatus;
	private Integer epiGroup;
	private String epiGroupStatus;
	private Integer customQuestion;
	private String customQuestionStatus;
	private Integer customQuestionRequired;
	private Integer OnBoarding;
	private String OnBoardingStatus;
	private Integer vvi;
	private String vviStatus;
	private String onlineAssesment;
	private String onlineAssStatus;
	private Integer eReference;
	private String eReferenceStatus;
	private Date createdDate;
	private Integer createdByUser;
	private String ipAddress;
	private String status;
	
	public Integer getJobCategoryTransactionId() {
		return jobCategoryTransactionId;
	}

	public void setJobCategoryTransactionId(Integer jobCategoryTransactionId) {
		this.jobCategoryTransactionId = jobCategoryTransactionId;
	}

	public Integer getJobCategoryId() {
		return jobCategoryId;
	}

	public void setJobCategoryId(Integer jobCategoryId) {
		this.jobCategoryId = jobCategoryId;
	}

	public String getCandidateType() {
		return candidateType;
	}

	public void setCandidateType(String candidateType) {
		this.candidateType = candidateType;
	}

	public Integer getPortfolio() {
		return portfolio;
	}

	public void setPortfolio(Integer portfolio) {
		this.portfolio = portfolio;
	}

	public String getPortfolioStatus() {
		return portfolioStatus;
	}

	public void setPortfolioStatus(String portfolioStatus) {
		this.portfolioStatus = portfolioStatus;
	}

	public Integer getPrescreen() {
		return prescreen;
	}

	public void setPrescreen(Integer prescreen) {
		this.prescreen = prescreen;
	}

	public String getPrescreenStatus() {
		return prescreenStatus;
	}

	public void setPrescreenStatus(String prescreenStatus) {
		this.prescreenStatus = prescreenStatus;
	}

	public Integer getEpiGroup() {
		return epiGroup;
	}

	public void setEpiGroup(Integer epiGroup) {
		this.epiGroup = epiGroup;
	}

	public String getEpiGroupStatus() {
		return epiGroupStatus;
	}

	public void setEpiGroupStatus(String epiGroupStatus) {
		this.epiGroupStatus = epiGroupStatus;
	}

	public Integer getCustomQuestion() {
		return customQuestion;
	}

	public void setCustomQuestion(Integer customQuestion) {
		this.customQuestion = customQuestion;
	}

	public String getCustomQuestionStatus() {
		return customQuestionStatus;
	}

	public void setCustomQuestionStatus(String customQuestionStatus) {
		this.customQuestionStatus = customQuestionStatus;
	}

	public Integer getCustomQuestionRequired() {
		return customQuestionRequired;
	}

	public void setCustomQuestionRequired(Integer customQuestionRequired) {
		this.customQuestionRequired = customQuestionRequired;
	}

	public Integer getOnBoarding() {
		return OnBoarding;
	}

	public void setOnBoarding(Integer onBoarding) {
		OnBoarding = onBoarding;
	}

	public String getOnBoardingStatus() {
		return OnBoardingStatus;
	}

	public void setOnBoardingStatus(String onBoardingStatus) {
		OnBoardingStatus = onBoardingStatus;
	}

	public Integer getVvi() {
		return vvi;
	}

	public void setVvi(Integer vvi) {
		this.vvi = vvi;
	}

	public String getVviStatus() {
		return vviStatus;
	}

	public void setVviStatus(String vviStatus) {
		this.vviStatus = vviStatus;
	}

	public String getOnlineAssesment() {
		return onlineAssesment;
	}

	public void setOnlineAssesment(String onlineAssesment) {
		this.onlineAssesment = onlineAssesment;
	}

	public String getOnlineAssStatus() {
		return onlineAssStatus;
	}

	public void setOnlineAssStatus(String onlineAssStatus) {
		this.onlineAssStatus = onlineAssStatus;
	}

	public Integer geteReference() {
		return eReference;
	}

	public void seteReference(Integer eReference) {
		this.eReference = eReference;
	}

	public String geteReferenceStatus() {
		return eReferenceStatus;
	}

	public void seteReferenceStatus(String eReferenceStatus) {
		this.eReferenceStatus = eReferenceStatus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getCreatedByUser() {
		return createdByUser;
	}

	public void setCreatedByUser(Integer createdByUser) {
		this.createdByUser = createdByUser;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


}
