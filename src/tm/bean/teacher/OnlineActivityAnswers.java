package tm.bean.teacher;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.QuestionTypeMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="onlineactivityanswers")
public class OnlineActivityAnswers implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3914007272760928430L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer answerId; 	
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	/*@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;*/
	
	private Integer districtId;
	
	/*@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster  jobCategoryMaster;*/
	
	private Integer jobCategoryId;
	
	/*@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	*/
	private Integer jobId;
	
	
	/*@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;*/
	
	private Long schoolId;
	
	@ManyToOne
	@JoinColumn(name="questionId",referencedColumnName="questionId")
	private OnlineActivityQuestions onlineActivityQuestions;
	
	private String question;
	@ManyToOne
	@JoinColumn(name="questionTypeId",referencedColumnName="questionTypeId")
	private QuestionTypeMaster  questionTypeMaster;
	
	@ManyToOne
	@JoinColumn(name="questionSetId",referencedColumnName="questionSetId")
	private OnlineActivityQuestionSet onlineActivityQuestionSet;
	
	private String questionType;
	private String selectedOptions;
	private String questionOption;
	private String insertedText;
	
	
	private Boolean isActive; 	
	private String insertedRanks;
	private String optionScore;
	private Double totalScore;
	private Integer Score;
	@ManyToOne
	@JoinColumn(name="scoreBy",referencedColumnName="userId")
	private UserMaster scoreBy;	
	private Date scoreDateTime;
	@ManyToOne
	@JoinColumn(name="updatedBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private Date updatedDateTime;
	
	private Date createdDateTime;
	
	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getJobCategoryId() {
		return jobCategoryId;
	}

	public void setJobCategoryId(Integer jobCategoryId) {
		this.jobCategoryId = jobCategoryId;
	}

	public Integer getJobId() {
		return jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}

	public OnlineActivityQuestionSet getOnlineActivityQuestionSet() {
		return onlineActivityQuestionSet;
	}

	public void setOnlineActivityQuestionSet(
			OnlineActivityQuestionSet onlineActivityQuestionSet) {
		this.onlineActivityQuestionSet = onlineActivityQuestionSet;
	}

	public Integer getScore() {
		return Score;
	}

	public void setScore(Integer score) {
		Score = score;
	}

	public OnlineActivityQuestions getOnlineActivityQuestions() {
		return onlineActivityQuestions;
	}

	public void setOnlineActivityQuestions(
			OnlineActivityQuestions onlineActivityQuestions) {
		this.onlineActivityQuestions = onlineActivityQuestions;
	}

	@Transient
	private Long schoolIdTemp;

	public String getInsertedRanks() {
		return insertedRanks;
	}

	public void setInsertedRanks(String insertedRanks) {
		this.insertedRanks = insertedRanks;
	}

	public String getOptionScore() {
		return optionScore;
	}

	public void setOptionScore(String optionScore) {
		this.optionScore = optionScore;
	}

	public Double getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(Double totalScore) {
		this.totalScore = totalScore;
	}

	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getAnswerId() {
		return answerId;
	}

	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	/*public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}

	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}*/

	/*public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}

	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}*/

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public QuestionTypeMaster getQuestionTypeMaster() {
		return questionTypeMaster;
	}

	public void setQuestionTypeMaster(QuestionTypeMaster questionTypeMaster) {
		this.questionTypeMaster = questionTypeMaster;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getSelectedOptions() {
		return selectedOptions;
	}

	public void setSelectedOptions(String selectedOptions) {
		this.selectedOptions = selectedOptions;
	}

	public String getQuestionOption() {
		return questionOption;
	}

	public void setQuestionOption(String questionOption) {
		this.questionOption = questionOption;
	}

	public String getInsertedText() {
		return insertedText;
	}

	public void setInsertedText(String insertedText) {
		this.insertedText = insertedText;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public UserMaster getScoreBy() {
		return scoreBy;
	}

	public void setScoreBy(UserMaster scoreBy) {
		this.scoreBy = scoreBy;
	}

	public Date getScoreDateTime() {
		return scoreDateTime;
	}

	public void setScoreDateTime(Date scoreDateTime) {
		this.scoreDateTime = scoreDateTime;
	}

	public Long getSchoolIdTemp() {
		return schoolIdTemp;
	}

	public void setSchoolIdTemp(Long schoolIdTemp) {
		this.schoolIdTemp = schoolIdTemp;
	}
	
	
}
