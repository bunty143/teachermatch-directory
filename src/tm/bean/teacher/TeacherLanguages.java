package tm.bean.teacher;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherDetail;
import tm.bean.master.SpokenLanguageMaster;

@Entity
@Table(name="teacherlanguages")
public class TeacherLanguages implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6543718889171329129L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherLanguageId;
	@ManyToOne
	@JoinColumn(name="teacherId", referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	private String language;
	
	@ManyToOne
	@JoinColumn(name="spokenLanguageMasterId", referencedColumnName="spokenlanguagemasterid")
	private SpokenLanguageMaster spokenLanguageMaster;
	
	private Integer oralSkills;
	private Integer writtenSkills;
	private Date createdDateTime;
	public Integer getTeacherLanguageId() {
		return teacherLanguageId;
	}
	public void setTeacherLanguageId(Integer teacherLanguageId) {
		this.teacherLanguageId = teacherLanguageId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public Integer getOralSkills() {
		return oralSkills;
	}
	public void setOralSkills(Integer oralSkills) {
		this.oralSkills = oralSkills;
	}
	public Integer getWrittenSkills() {
		return writtenSkills;
	}
	public void setWrittenSkills(Integer writtenSkills) {
		this.writtenSkills = writtenSkills;
	}	
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	public SpokenLanguageMaster getSpokenLanguageMaster() {
		return spokenLanguageMaster;
	}
	public void setSpokenLanguageMaster(SpokenLanguageMaster spokenLanguageMaster) {
		this.spokenLanguageMaster = spokenLanguageMaster;
	}
	
	
	
}
