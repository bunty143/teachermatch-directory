package tm.bean.teacher;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="onlineactivityquestionset")
public class OnlineActivityQuestionSet implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4107444008505321877L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer questionSetId; 
	
	private String questionSetName;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;

	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster  jobCategoryMaster;
	
	private Integer questionTime;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	
	private String status;
	
	private Date createdDateTime;

	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}

	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}

	public Integer getQuestionTime() {
		return questionTime;
	}

	public void setQuestionTime(Integer questionTime) {
		this.questionTime = questionTime;
	}

	public Integer getQuestionSetId() {
		return questionSetId;
	}

	public void setQuestionSetId(Integer questionSetId) {
		this.questionSetId = questionSetId;
	}

	public String getQuestionSetName() {
		return questionSetName;
	}

	public void setQuestionSetName(String questionSetName) {
		this.questionSetName = questionSetName;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}

	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
}
