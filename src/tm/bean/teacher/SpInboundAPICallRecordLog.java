package tm.bean.teacher;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherDetail;

@Entity
@Table(name="spinboundapicallrecordlog")
public class SpInboundAPICallRecordLog implements Serializable{

	private static final long serialVersionUID = 2818909322238645878L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer spInboundAPICallRecordId;
	
	@ManyToOne
	@JoinColumn(name="teacherId", referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	private Integer currentLessonNo;
	private Integer totalLessons;
	private String lpm;
	private Integer unit03currentLessonNo;
	private Integer unit03TotalLessons;
	private String unit03lpm;
	private Integer unit04currentLessonNo;
	private Integer unit04TotalLessons;
	private String unit04lpm;
	private String IPAddress;
	private Date createdDateTime;
	
	public Integer getSpInboundAPICallRecordId() {
		return spInboundAPICallRecordId;
	}
	public void setSpInboundAPICallRecordId(Integer spInboundAPICallRecordId) {
		this.spInboundAPICallRecordId = spInboundAPICallRecordId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public Integer getCurrentLessonNo() {
		return currentLessonNo;
	}
	public void setCurrentLessonNo(Integer currentLessonNo) {
		this.currentLessonNo = currentLessonNo;
	}
	public Integer getTotalLessons() {
		return totalLessons;
	}
	public void setTotalLessons(Integer totalLessons) {
		this.totalLessons = totalLessons;
	}
	public String getLpm() {
		return lpm;
	}
	public void setLpm(String lpm) {
		this.lpm = lpm;
	}
	public String getIPAddress() {
		return IPAddress;
	}
	public void setIPAddress(String iPAddress) {
		IPAddress = iPAddress;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Integer getUnit03currentLessonNo() {
		return unit03currentLessonNo;
	}
	public void setUnit03currentLessonNo(Integer unit03currentLessonNo) {
		this.unit03currentLessonNo = unit03currentLessonNo;
	}
	public Integer getUnit03TotalLessons() {
		return unit03TotalLessons;
	}
	public void setUnit03TotalLessons(Integer unit03TotalLessons) {
		this.unit03TotalLessons = unit03TotalLessons;
	}
	public String getUnit03lpm() {
		return unit03lpm;
	}
	public void setUnit03lpm(String unit03lpm) {
		this.unit03lpm = unit03lpm;
	}
	public Integer getUnit04currentLessonNo() {
		return unit04currentLessonNo;
	}
	public void setUnit04currentLessonNo(Integer unit04currentLessonNo) {
		this.unit04currentLessonNo = unit04currentLessonNo;
	}
	public Integer getUnit04TotalLessons() {
		return unit04TotalLessons;
	}
	public void setUnit04TotalLessons(Integer unit04TotalLessons) {
		this.unit04TotalLessons = unit04TotalLessons;
	}
	public String getUnit04lpm() {
		return unit04lpm;
	}
	public void setUnit04lpm(String unit04lpm) {
		this.unit04lpm = unit04lpm;
	}
}
