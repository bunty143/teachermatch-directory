package tm.bean.teacher;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="onlineactivitystatus")
public class OnlineActivityStatus implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8658080318677463556L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer onlineactivityId; 	
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;

	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster  jobCategoryMaster;
	
	@ManyToOne
	@JoinColumn(name="questionSetId",referencedColumnName="questionSetId")
	private OnlineActivityQuestionSet onlineActivityQuestionSet;
	
	private String status;
		
	private Date activityStartTime;
	private Date activityEndTime;
	
	private Integer activityTime; 
	
	@ManyToOne
	@JoinColumn(name="updatedBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	private Date activityDate;

	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}

	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}

	public Integer getOnlineactivityId() {
		return onlineactivityId;
	}

	public void setOnlineactivityId(Integer onlineactivityId) {
		this.onlineactivityId = onlineactivityId;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}

	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}

	public OnlineActivityQuestionSet getOnlineActivityQuestionSet() {
		return onlineActivityQuestionSet;
	}

	public void setOnlineActivityQuestionSet(
			OnlineActivityQuestionSet onlineActivityQuestionSet) {
		this.onlineActivityQuestionSet = onlineActivityQuestionSet;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getActivityStartTime() {
		return activityStartTime;
	}

	public void setActivityStartTime(Date activityStartTime) {
		this.activityStartTime = activityStartTime;
	}

	public Date getActivityEndTime() {
		return activityEndTime;
	}

	public void setActivityEndTime(Date activityEndTime) {
		this.activityEndTime = activityEndTime;
	}

	public Integer getActivityTime() {
		return activityTime;
	}

	public void setActivityTime(Integer activityTime) {
		this.activityTime = activityTime;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}
	
}
