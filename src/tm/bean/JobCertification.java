package tm.bean;


import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.CertificateNameMaster;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.StateMaster;


@Entity
@Table(name="jobcertification")
public class JobCertification implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8954759564726269023L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int jobCertificationId;     
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobId;    
	private String certType;
	
	@ManyToOne	
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private StateMaster stateMaster;
	
	@ManyToOne	
	@JoinColumn(name="certTypeId",referencedColumnName="certTypeId")
	private CertificateTypeMaster certificateTypeMaster;
	
	public int getJobCertificationId() {
		return jobCertificationId;
	}
	public void setJobCertificationId(int jobCertificationId) {
		this.jobCertificationId = jobCertificationId;
	}
	public JobOrder getJobId() {
		return jobId;
	}
	public void setJobId(JobOrder jobId) {
		this.jobId = jobId;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = certType;
	}
	public CertificateTypeMaster getCertificateTypeMaster() {
		return certificateTypeMaster;
	}
	public void setCertificateTypeMaster(CertificateTypeMaster certificateTypeMaster) {
		this.certificateTypeMaster = certificateTypeMaster;
	}
	public StateMaster getStateMaster() {
		return stateMaster;
	}
	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
