package tm.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4InterviewInvites;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
 
@Entity
@Table(name="eventparticipantslist")
public class EventParticipantsList implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9160532117683377206L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer eventParticipantId;
	
	@ManyToOne
	@JoinColumn(name="eventId",referencedColumnName="eventId")
	private EventDetails eventDetails;
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;

	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	private String participantFirstName;
	private String participantLastName;
	private String participantEmailAddress;
	private Integer normScore;
	private boolean invitationEmailSent;
	private String status;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBY;
	private Date createdDateTime;
	
	
	@OneToOne
	@JoinColumn(name="interviewInviteId",referencedColumnName="id")
	private I4InterviewInvites i4InterviewInvites;
	
	
	private Integer teacherId;
	
	public Integer getEventParticipantId() {
		return eventParticipantId;
	}
	public void setEventParticipantId(Integer eventParticipantId) {
		this.eventParticipantId = eventParticipantId;
	}
	public EventDetails getEventDetails() {
		return eventDetails;
	}
	public void setEventDetails(EventDetails eventDetails) {
		this.eventDetails = eventDetails;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getParticipantFirstName() {
		return participantFirstName;
	}
	public void setParticipantFirstName(String participantFirstName) {
		this.participantFirstName = participantFirstName;
	}
	public String getParticipantLastName() {
		return participantLastName;
	}
	public void setParticipantLastName(String participantLastName) {
		this.participantLastName = participantLastName;
	}
	public String getParticipantEmailAddress() {
		return participantEmailAddress;
	}
	public void setParticipantEmailAddress(String participantEmailAddress) {
		this.participantEmailAddress = participantEmailAddress;
	}
	public Integer getNormScore() {
		return normScore;
	}
	public void setNormScore(Integer normScore) {
		this.normScore = normScore;
	}
	public boolean isInvitationEmailSent() {
		return invitationEmailSent;
	}
	public void setInvitationEmailSent(boolean invitationEmailSent) {
		this.invitationEmailSent = invitationEmailSent;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public UserMaster getCreatedBY() {
		return createdBY;
	}
	public void setCreatedBY(UserMaster createdBY) {
		this.createdBY = createdBY;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Integer getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}
	public I4InterviewInvites getI4InterviewInvites() {
		return i4InterviewInvites;
	}
	public void setI4InterviewInvites(I4InterviewInvites i4InterviewInvites) {
		this.i4InterviewInvites = i4InterviewInvites;
	}
	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}
	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}
	public BranchMaster getBranchMaster() {
		return branchMaster;
	}
	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}
	
}
