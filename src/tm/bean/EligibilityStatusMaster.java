package tm.bean;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.master.DistrictMaster;
import tm.bean.master.EligibilityMaster;


@Entity
@Table(name="eligibilitystatusmaster")
public class EligibilityStatusMaster implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3063759600551600269L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int eligibilityStatusId;
	private String statusName;
	private String statusCode;
	private String statusColorCode;
	private String status;
	private String validateStatus;
	private String formCode;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtId;
	
	@ManyToOne
	@JoinColumn(name="eligibilityId",referencedColumnName="eligibilityId")
	private EligibilityMaster eligibilityMaster;

	public int getEligibilityStatusId() {
		return eligibilityStatusId;
	}

	public void setEligibilityStatusId(int eligibilityStatusId) {
		this.eligibilityStatusId = eligibilityStatusId;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusColorCode() {
		return statusColorCode;
	}

	public void setStatusColorCode(String statusColorCode) {
		this.statusColorCode = statusColorCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public DistrictMaster getDistrictId() {
		return districtId;
	}

	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}

	public String getValidateStatus() {
		return validateStatus;
	}

	public void setValidateStatus(String validateStatus) {
		this.validateStatus = validateStatus;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public EligibilityMaster getEligibilityMaster() {
		return eligibilityMaster;
	}

	public void setEligibilityMaster(EligibilityMaster eligibilityMaster) {
		this.eligibilityMaster = eligibilityMaster;
	}
	
	

}
