package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;

@Entity
@Table(name="districtaccountingcode")
public class DistrictAccountingCode implements Serializable 
{

	private static final long serialVersionUID = 4878544745878754545L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer accountingCodeId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
    private DistrictMaster districtMaster;
	private String accountingCode;
	private String name;
	private String status;
	private String ipAddress;
	
	public Integer getAccountingCodeId() {
		return accountingCodeId;
	}
	public void setAccountingCodeId(Integer accountingCodeId) {
		this.accountingCodeId = accountingCodeId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public String getAccountingCode() {
		return accountingCode;
	}
	public void setAccountingCode(String accountingCode) {
		this.accountingCode = accountingCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	
	
}
