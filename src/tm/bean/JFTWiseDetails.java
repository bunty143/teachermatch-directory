package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="jftwisedetails")
public class JFTWiseDetails implements Serializable{
	/**
	 * Ram Nath
	 * 
	 * CREATE TABLE `jftwisedetails` (
		  `jobForTeacherId` int(10) NOT NULL,
		  `affidavit` tinyint(1) default NULL,
		  `complete` tinyint(1) default NULL,
		  `affidavitAcceptedDate` datetime default NULL,
		  PRIMARY KEY  (`jobForTeacherId`)
		) ENGINE=InnoDB;
		alter table jftwisedetails add column applyJobWithSignUp tinyint(1) default null Comment 'this flag true if job apply with sign up' after affidavitAcceptedDate ;
alter table jftwisedetails add column applyJobWithSignUpFirstTime tinyint(1) default null Comment 'this flag true if job apply successfully with sign up' after applyJobWithSignUp ;
alter table jftwisedetails add column internalCandidateComplete tinyint(1) default null Comment 'this flag true if job apply successfully with sign up' after applyJobWithSignUpFirstTime ;
	 */
	private static final long serialVersionUID = 4300838054490845333L;
	@Id
	private Long jobForTeacherId;
	private Boolean affidavit;
	private Boolean complete;
	private Date affidavitAcceptedDate;
	private Boolean applyJobWithSignUp;
	private Boolean applyJobWithSignUpFirstTime;
	private Boolean internalCandidateComplete;
	public Boolean getApplyJobWithSignUp() {
		return applyJobWithSignUp;
	}
	public void setApplyJobWithSignUp(Boolean applyJobWithSignUp) {
		this.applyJobWithSignUp = applyJobWithSignUp;
	}
	
	public Boolean getApplyJobWithSignUpFirstTime() {
		return applyJobWithSignUpFirstTime;
	}
	public void setApplyJobWithSignUpFirstTime(Boolean applyJobWithSignUpFirstTime) {
		this.applyJobWithSignUpFirstTime = applyJobWithSignUpFirstTime;
	}
	public Long getJobForTeacherId() {
		return jobForTeacherId;
	}
	public void setJobForTeacherId(Long jobForTeacherId) {
		this.jobForTeacherId = jobForTeacherId;
	}
	public Boolean getAffidavit() {
		return affidavit;
	}
	public void setAffidavit(Boolean affidavit) {
		this.affidavit = affidavit;
	}
	public Boolean getComplete() {
		return complete;
	}
	public void setComplete(Boolean complete) {
		this.complete = complete;
	}
	public Date getAffidavitAcceptedDate() {
		return affidavitAcceptedDate;
	}
	public void setAffidavitAcceptedDate(Date affidavitAcceptedDate) {
		this.affidavitAcceptedDate = affidavitAcceptedDate;
	}
	public Boolean getInternalCandidateComplete() {
		return internalCandidateComplete;
	}
	public void setInternalCandidateComplete(Boolean internalCandidateComplete) {
		this.internalCandidateComplete = internalCandidateComplete;
	}
	
}
