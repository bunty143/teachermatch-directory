package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentSections;
import tm.bean.user.UserMaster;

@Entity
@Table(name="teachersectiondetails")
public class TeacherSectionDetail implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1469887238962037479L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherSectionId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail; 
	
	@ManyToOne
	@JoinColumn(name="teacherAssessmentId",referencedColumnName="teacherAssessmentId")
	private TeacherAssessmentdetail teacherAssessmentdetail; 
	
	private String sectionName;
	private String sectionDescription;
	private String sectionInstructions;
	private String sectionVideoUrl;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;
	private String status;
	private Date createdDateTime; 
	private String ipAddress;
	
	public Integer getTeacherSectionId() {
		return teacherSectionId;
	}
	public void setTeacherSectionId(Integer teacherSectionId) {
		this.teacherSectionId = teacherSectionId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	
	
	public TeacherAssessmentdetail getTeacherAssessmentdetail() {
		return teacherAssessmentdetail;
	}
	public void setTeacherAssessmentdetail(
			TeacherAssessmentdetail teacherAssessmentdetail) {
		this.teacherAssessmentdetail = teacherAssessmentdetail;
	}

	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getSectionDescription() {
		return sectionDescription;
	}
	public void setSectionDescription(String sectionDescription) {
		this.sectionDescription = sectionDescription;
	}
	public String getSectionInstructions() {
		return sectionInstructions;
	}
	public void setSectionInstructions(String sectionInstructions) {
		this.sectionInstructions = sectionInstructions;
	}
	public String getSectionVideoUrl() {
		return sectionVideoUrl;
	}
	public void setSectionVideoUrl(String sectionVideoUrl) {
		this.sectionVideoUrl = sectionVideoUrl;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	
}
