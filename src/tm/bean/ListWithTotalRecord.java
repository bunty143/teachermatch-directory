package tm.bean;

import java.io.Serializable;


public class ListWithTotalRecord implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3416864175725765317L;
	private Long totalRecords;
	private JobOrder jobOrder;
	
	public Long getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(Long totalRecords) {
		this.totalRecords = totalRecords;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	

	
		
}
