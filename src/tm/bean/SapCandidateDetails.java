package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity(name="sapcandidatedetails")
public class SapCandidateDetails implements Serializable {

	private static final long serialVersionUID = 8084569006589796531L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer sapCandidateId; 
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	private String hireDate;
	private String employeeNumber;
	private String position;
	private String jobTitle;
	private String candidateFirstName;
	private String candidateMiddleName;
	private String candidateLastName;
	private String candidateSecondName;
	private String candidateSirNamePrefix;
	private String candidateInitials;
	private String candidateSSN;
	private String candidateGender;
	private String candidateDOB;
	private String candidateCorrespondLanguage;
	private String candidateStreet;
	private String candidateSecondAddressLine;
	private String candidateCity;
	private String candidateZip;
	private String candidateCountry;
	private String candidateState;
	private String candidateTelephoneNumber;
	private String candidateTelephoneNumber2;
	private String candidateEthnicOrigin;
	private String candidateMilitaryStatus;
	private String candidateDisability;
	private String candidateVeteranStatus;
	private String candidateEthnicity;
	private String candidateRac01;
	private String candidateRac02;
	private String candidateRac03;
	private String candidateRac04;
	private String candidateRac05;
	private String candidateRac06;
	private String candidateRac07;
	private String candidateRac08;
	private String candidateRac09;
	private String candidateRac10;
	private String sapStatus;
	private String sapError;
	private Integer noOfTries;
	private Integer districtId;
	@ManyToOne
	@JoinColumn(name="lastUpdatedBy",referencedColumnName="userId")
	private UserMaster userMaster;
	
	@ManyToOne
	@JoinColumn(name="hiredBySchool",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	
	@ManyToOne
	@JoinColumn(name="jobForTeacherId",referencedColumnName="jobForTeacherId")
	private JobForTeacher jobForTeacher;
	
	private Date lastUpdateDateTime;
	private Date createdDateTime;
	
	
	
	
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public JobForTeacher getJobForTeacher() {
		return jobForTeacher;
	}
	public void setJobForTeacher(JobForTeacher jobForTeacher) {
		this.jobForTeacher = jobForTeacher;
	}
	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}
	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}
	public Integer getSapCandidateId() {
		return sapCandidateId;
	}
	public void setSapCandidateId(Integer sapCandidateId) {
		this.sapCandidateId = sapCandidateId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public String getHireDate() {
		return hireDate;
	}
	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getCandidateFirstName() {
		return candidateFirstName;
	}
	public void setCandidateFirstName(String candidateFirstName) {
		this.candidateFirstName = candidateFirstName;
	}
	public String getCandidateMiddleName() {
		return candidateMiddleName;
	}
	public void setCandidateMiddleName(String candidateMiddleName) {
		this.candidateMiddleName = candidateMiddleName;
	}
	public String getCandidateLastName() {
		return candidateLastName;
	}
	public void setCandidateLastName(String candidateLastName) {
		this.candidateLastName = candidateLastName;
	}
	public String getCandidateSecondName() {
		return candidateSecondName;
	}
	public void setCandidateSecondName(String candidateSecondName) {
		this.candidateSecondName = candidateSecondName;
	}
	public String getCandidateSirNamePrefix() {
		return candidateSirNamePrefix;
	}
	public void setCandidateSirNamePrefix(String candidateSirNamePrefix) {
		this.candidateSirNamePrefix = candidateSirNamePrefix;
	}
	public String getCandidateInitials() {
		return candidateInitials;
	}
	public void setCandidateInitials(String candidateInitials) {
		this.candidateInitials = candidateInitials;
	}
	public String getCandidateSSN() {
		return candidateSSN;
	}
	public void setCandidateSSN(String candidateSSN) {
		this.candidateSSN = candidateSSN;
	}
	public String getCandidateGender() {
		return candidateGender;
	}
	public void setCandidateGender(String candidateGender) {
		this.candidateGender = candidateGender;
	}
	public String getCandidateDOB() {
		return candidateDOB;
	}
	public void setCandidateDOB(String candidateDOB) {
		this.candidateDOB = candidateDOB;
	}
	public String getCandidateCorrespondLanguage() {
		return candidateCorrespondLanguage;
	}
	public void setCandidateCorrespondLanguage(String candidateCorrespondLanguage) {
		this.candidateCorrespondLanguage = candidateCorrespondLanguage;
	}
	public String getCandidateStreet() {
		return candidateStreet;
	}
	public void setCandidateStreet(String candidateStreet) {
		this.candidateStreet = candidateStreet;
	}
	public String getCandidateSecondAddressLine() {
		return candidateSecondAddressLine;
	}
	public void setCandidateSecondAddressLine(String candidateSecondAddressLine) {
		this.candidateSecondAddressLine = candidateSecondAddressLine;
	}
	public String getCandidateCity() {
		return candidateCity;
	}
	public void setCandidateCity(String candidateCity) {
		this.candidateCity = candidateCity;
	}
	public String getCandidateZip() {
		return candidateZip;
	}
	public void setCandidateZip(String candidateZip) {
		this.candidateZip = candidateZip;
	}
	public String getCandidateCountry() {
		return candidateCountry;
	}
	public void setCandidateCountry(String candidateCountry) {
		this.candidateCountry = candidateCountry;
	}
	public String getCandidateState() {
		return candidateState;
	}
	public void setCandidateState(String candidateState) {
		this.candidateState = candidateState;
	}
	public String getCandidateTelephoneNumber() {
		return candidateTelephoneNumber;
	}
	public void setCandidateTelephoneNumber(String candidateTelephoneNumber) {
		this.candidateTelephoneNumber = candidateTelephoneNumber;
	}
	public String getCandidateTelephoneNumber2() {
		return candidateTelephoneNumber2;
	}
	public void setCandidateTelephoneNumber2(String candidateTelephoneNumber2) {
		this.candidateTelephoneNumber2 = candidateTelephoneNumber2;
	}
	public String getCandidateEthnicOrigin() {
		return candidateEthnicOrigin;
	}
	public void setCandidateEthnicOrigin(String candidateEthnicOrigin) {
		this.candidateEthnicOrigin = candidateEthnicOrigin;
	}
	public String getCandidateMilitaryStatus() {
		return candidateMilitaryStatus;
	}
	public void setCandidateMilitaryStatus(String candidateMilitaryStatus) {
		this.candidateMilitaryStatus = candidateMilitaryStatus;
	}
	public String getCandidateDisability() {
		return candidateDisability;
	}
	public void setCandidateDisability(String candidateDisability) {
		this.candidateDisability = candidateDisability;
	}
	public String getCandidateVeteranStatus() {
		return candidateVeteranStatus;
	}
	public void setCandidateVeteranStatus(String candidateVeteranStatus) {
		this.candidateVeteranStatus = candidateVeteranStatus;
	}
	public String getCandidateEthnicity() {
		return candidateEthnicity;
	}
	public void setCandidateEthnicity(String candidateEthnicity) {
		this.candidateEthnicity = candidateEthnicity;
	}
	public String getCandidateRac01() {
		return candidateRac01;
	}
	public void setCandidateRac01(String candidateRac01) {
		this.candidateRac01 = candidateRac01;
	}
	public String getCandidateRac02() {
		return candidateRac02;
	}
	public void setCandidateRac02(String candidateRac02) {
		this.candidateRac02 = candidateRac02;
	}
	public String getCandidateRac03() {
		return candidateRac03;
	}
	public void setCandidateRac03(String candidateRac03) {
		this.candidateRac03 = candidateRac03;
	}
	public String getCandidateRac04() {
		return candidateRac04;
	}
	public void setCandidateRac04(String candidateRac04) {
		this.candidateRac04 = candidateRac04;
	}
	public String getCandidateRac05() {
		return candidateRac05;
	}
	public void setCandidateRac05(String candidateRac05) {
		this.candidateRac05 = candidateRac05;
	}
	public String getCandidateRac06() {
		return candidateRac06;
	}
	public void setCandidateRac06(String candidateRac06) {
		this.candidateRac06 = candidateRac06;
	}
	public String getCandidateRac07() {
		return candidateRac07;
	}
	public void setCandidateRac07(String candidateRac07) {
		this.candidateRac07 = candidateRac07;
	}
	public String getCandidateRac08() {
		return candidateRac08;
	}
	public void setCandidateRac08(String candidateRac08) {
		this.candidateRac08 = candidateRac08;
	}
	public String getCandidateRac09() {
		return candidateRac09;
	}
	public void setCandidateRac09(String candidateRac09) {
		this.candidateRac09 = candidateRac09;
	}
	public String getCandidateRac10() {
		return candidateRac10;
	}
	public void setCandidateRac10(String candidateRac10) {
		this.candidateRac10 = candidateRac10;
	}
	public String getSapStatus() {
		return sapStatus;
	}
	public void setSapStatus(String sapStatus) {
		this.sapStatus = sapStatus;
	}
	public String getSapError() {
		return sapError;
	}
	public void setSapError(String sapError) {
		this.sapError = sapError;
	}
	public Integer getNoOfTries() {
		return noOfTries;
	}
	public void setNoOfTries(Integer noOfTries) {
		this.noOfTries = noOfTries;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public Date getLastUpdateDateTime() {
		return lastUpdateDateTime;
	}
	public void setLastUpdateDateTime(Date lastUpdateDateTime) {
		this.lastUpdateDateTime = lastUpdateDateTime;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
}
