package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.CurrencyMaster;
import tm.bean.master.EmpRoleTypeMaster;
import tm.bean.master.FieldMaster;
import tm.bean.master.FieldOfStudyMaster;
import tm.utility.Utility;

@Entity
@Table(name="teacherrole")
public class TeacherRole implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4908187860681695574L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer roleId;     
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;	
	private String role;
	private Integer position;
	@ManyToOne
	@JoinColumn(name="fieldId",referencedColumnName="fieldId")
	private FieldMaster fieldMaster;
	private Integer roleStartMonth;     
	private String roleStartYear;     
	private Boolean currentlyWorking;     
	private Integer roleEndMonth;     
	private String roleEndYear;	
	private String organization;
	@ManyToOne
	@JoinColumn(name="currencyId",referencedColumnName="currencyId")
	private CurrencyMaster currencyMaster;
	private Integer amount; 
	
	@ManyToOne
	@JoinColumn(name="empRoleTypeId",referencedColumnName="empRoleTypeId")
	private EmpRoleTypeMaster empRoleTypeMaster;   
	private String primaryResp;     
	private String mostSignCont;
	private String reasonForLeaving;
	private Date CreatedDateTime;
	//shadab
	private String city;
	private String state;
	
	private Boolean noWorkExp;
	//shriram
	private String compSupervisor;
	public String getCompSupervisor() {
		return compSupervisor;
	}
	public void setCompSupervisor(String compSupervisor) {
		this.compSupervisor = compSupervisor;
	}
	public String getCompTelephone() {
		return compTelephone;
	}
	public void setCompTelephone(String compTelephone) {
		this.compTelephone = compTelephone;
	}
	private String compTelephone;
	
	
	public CurrencyMaster getCurrencyMaster() {
		return currencyMaster;
	}
	public void setCurrencyMaster(CurrencyMaster currencyMaster) {
		this.currencyMaster = currencyMaster;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = Utility.trim(role);
	}
	public FieldMaster getFieldMaster() {
		return fieldMaster;
	}
	public void setFieldMaster(FieldMaster fieldMaster) {
		this.fieldMaster = fieldMaster;
	}
	public Integer getRoleStartMonth() {
		return roleStartMonth;
	}
	public void setRoleStartMonth(Integer roleStartMonth) {
		this.roleStartMonth = roleStartMonth;
	}
	public String getRoleStartYear() {
		return roleStartYear;
	}
	public void setRoleStartYear(String roleStartYear) {
		this.roleStartYear = roleStartYear;
	}
	public Boolean getCurrentlyWorking() {
		return currentlyWorking;
	}
	public void setCurrentlyWorking(Boolean currentlyWorking) {
		this.currentlyWorking = currentlyWorking;
	}
	public Integer getRoleEndMonth() {
		return roleEndMonth;
	}
	public void setRoleEndMonth(Integer roleEndMonth) {
		this.roleEndMonth = roleEndMonth;
	}
	public String getRoleEndYear() {
		return roleEndYear;
	}
	public void setRoleEndYear(String roleEndYear) {
		this.roleEndYear = roleEndYear;
	}
	public EmpRoleTypeMaster getEmpRoleTypeMaster() {
		return empRoleTypeMaster;
	}
	public void setEmpRoleTypeMaster(EmpRoleTypeMaster empRoleTypeMaster) {
		this.empRoleTypeMaster = empRoleTypeMaster;
	}
	public String getPrimaryResp() {
		return primaryResp;
	}
	public void setPrimaryResp(String primaryResp) {
		this.primaryResp = Utility.trim(primaryResp);
	}
	public String getMostSignCont() {
		return mostSignCont;
	}
	public void setMostSignCont(String mostSignCont) {
		this.mostSignCont = Utility.trim(mostSignCont);
	}
	
	public String getReasonForLeaving() {
		return reasonForLeaving;
	}
	public void setReasonForLeaving(String reasonForLeaving) {
		this.reasonForLeaving = reasonForLeaving;
	}
	public Date getCreatedDateTime() {
		return CreatedDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		CreatedDateTime = createdDateTime;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Boolean getNoWorkExp() {
		return noWorkExp;
	}
	public void setNoWorkExp(Boolean noWorkExp) {
		this.noWorkExp = noWorkExp;
	}
	
	
	
}
