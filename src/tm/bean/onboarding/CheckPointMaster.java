package tm.bean.onboarding;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="checkpointmaster")
public class CheckPointMaster {
	private static final long serialVersionUID = 6631718743665590819L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer checkPointId;
	String checkPointName;
	String status;
	Date createdDate;
	public Integer getCheckPointId() {
		return checkPointId;
	}
	public void setCheckPointId(Integer checkPointId) {
		this.checkPointId = checkPointId;
	}
	public String getCheckPointName() {
		return checkPointName;
	}
	public void setCheckPointName(String checkPointName) {
		this.checkPointName = checkPointName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
