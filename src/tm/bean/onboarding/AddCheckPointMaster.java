package tm.bean.onboarding;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="checkpointmaster")
public class AddCheckPointMaster {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int checkPointId;
	private String checkpointname;
	private String status;
	private Date createdDate;
	public int getCheckPointId() {
		return checkPointId;
	}
	public void setCheckPointId(int checkPointId) {
		this.checkPointId = checkPointId;
	}
	public String getCheckpointname() {
		return checkpointname;
	}
	public void setCheckpointname(String checkpointname) {
		this.checkpointname = checkpointname;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	

}
