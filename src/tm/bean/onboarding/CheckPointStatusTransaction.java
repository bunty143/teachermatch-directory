package tm.bean.onboarding;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="checkpointstatustransaction")
public class CheckPointStatusTransaction {
	private static final long serialVersionUID = 6631718743665590819L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer cpStatusTransactionId; 
	Integer cpStatusMasterId;
	Integer onboardingId;
	Integer checkPointId;
	String  candidateType;
	Integer districtId;
	String cpTransactionStatusName;
	Integer cpDecisionPoint;
	String cpColor;
	String cpTransactionStatus;
	Date createdDate;
	String createdByUser;
	String ipAddress;
	String modifyByUser;
	Date modifiedDate;
	String status;
	public Integer getCpStatusTransactionId() {
		return cpStatusTransactionId;
	}
	public void setCpStatusTransactionId(Integer cpStatusTransactionId) {
		this.cpStatusTransactionId = cpStatusTransactionId;
	}
	public Integer getCpStatusMasterId() {
		return cpStatusMasterId;
	}
	public void setCpStatusMasterId(Integer cpStatusMasterId) {
		this.cpStatusMasterId = cpStatusMasterId;
	}
	public Integer getOnboardingId() {
		return onboardingId;
	}
	public void setOnboardingId(Integer onboardingId) {
		this.onboardingId = onboardingId;
	}
	public Integer getCheckPointId() {
		return checkPointId;
	}
	public void setCheckPointId(Integer checkPointId) {
		this.checkPointId = checkPointId;
	}
	public String getCandidateType() {
		return candidateType;
	}
	public void setCandidateType(String candidateType) {
		this.candidateType = candidateType;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public String getCpTransactionStatusName() {
		return cpTransactionStatusName;
	}
	public void setCpTransactionStatusName(String cpTransactionStatusName) {
		this.cpTransactionStatusName = cpTransactionStatusName;
	}
	public Integer getCpDecisionPoint() {
		return cpDecisionPoint;
	}
	public void setCpDecisionPoint(Integer cpDecisionPoint) {
		this.cpDecisionPoint = cpDecisionPoint;
	}
	public String getCpColor() {
		return cpColor;
	}
	public void setCpColor(String cpColor) {
		this.cpColor = cpColor;
	}
	public String getCpTransactionStatus() {
		return cpTransactionStatus;
	}
	public void setCpTransactionStatus(String cpTransactionStatus) {
		this.cpTransactionStatus = cpTransactionStatus;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedByUser() {
		return createdByUser;
	}
	public void setCreatedByUser(String createdByUser) {
		this.createdByUser = createdByUser;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getModifyByUser() {
		return modifyByUser;
	}
	public void setModifyByUser(String modifyByUser) {
		this.modifyByUser = modifyByUser;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
