package tm.bean.onboarding;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="checkpointaction")
public class CheckPointAction {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer actionId;
	private Integer districtId;
	private Integer onBoardingId;
	private Integer checkPointId;
	private Integer action;
	private Integer checkPointStatus;
	private String candidateType;
	private Date createdDate;
	private Integer createdBy;
	private Date modifiedDate;
	private Integer modifiedBy;
	private String ipAddress;
	private String status;
	public Integer getActionId() {
		return actionId;
	}
	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public Integer getOnBoardingId() {
		return onBoardingId;
	}
	public void setOnBoardingId(Integer onBoardingId) {
		this.onBoardingId = onBoardingId;
	}
	public Integer getCheckPointId() {
		return checkPointId;
	}
	public void setCheckPointId(Integer checkPointId) {
		this.checkPointId = checkPointId;
	}
	public Integer getAction() {
		return action;
	}
	public void setAction(Integer action) {
		this.action = action;
	}
	public Integer getCheckPointStatus() {
		return checkPointStatus;
	}
	public void setCheckPointStatus(Integer checkPointStatus) {
		this.checkPointStatus = checkPointStatus;
	}
	public String getCandidateType() {
		return candidateType;
	}
	public void setCandidateType(String candidateType) {
		this.candidateType = candidateType;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
