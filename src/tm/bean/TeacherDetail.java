package tm.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import sun.security.util.BigInt;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.utility.Utility;

@Entity
@Table(name="teacherdetail")
public class TeacherDetail implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3819263934535900034L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherId;
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String password;
	private String userType;
	private Boolean fbUser;
	private Integer exclusivePeriod;
	private String authenticationCode;
	private String verificationCode;
	private Integer verificationStatus;
	private Integer noOfLogin;
	private String status;
	private Boolean sendOpportunity;
	private Date fgtPwdDateTime;	
	private Date createdDateTime;
	private String identityVerificationPicture;
	private Boolean isPortfolioNeeded;
	private Integer forgetCounter;
	private Boolean isResearchTeacher;
	private String ipAddress;
	private Integer questCandidate;
	
	private Integer drugtestRejectStatus;
	private Date drugtestRejectDate;
	
	private Integer smartPracticesCandidate ;
	
	private BigInteger KSNID;
	private String referralURL;
	
	@ManyToOne
	@JoinColumn(name="associatedBranchId",referencedColumnName="branchId")
	private BranchMaster associatedBranchId;
	
	public Date getDrugtestRejectDate() {
		return drugtestRejectDate;
	}
	public void setDrugtestRejectDate(Date drugtestRejectDate) {
		this.drugtestRejectDate = drugtestRejectDate;
	}
	public Integer getDrugtestRejectStatus() {
		return drugtestRejectStatus;
	}
	public void setDrugtestRejectStatus(Integer drugtestRejectStatus) {
		this.drugtestRejectStatus = drugtestRejectStatus;
	}

	public Integer getQuestCandidate() {
		return questCandidate;
	}
	public void setQuestCandidate(Integer questCandidate) {
		this.questCandidate = questCandidate;
	}

	public Integer getSmartPracticesCandidate() {
		return smartPracticesCandidate;
	}
	public void setSmartPracticesCandidate(Integer smartPracticesCandidate) {
		this.smartPracticesCandidate = smartPracticesCandidate;
	}
	public BranchMaster getAssociatedBranchId() {
		return associatedBranchId;
	}
	public void setAssociatedBranchId(BranchMaster associatedBranchId) {
		this.associatedBranchId = associatedBranchId;
	}

	private Boolean internalTransferCandidate;
	private String phoneNumber;
	
	@Transient
	private Integer normScore = -0;
	@Transient
	private Integer aScore;
	
	@Transient
	private Integer lRScore;
	
	@Transient
	private String tStatus;

	@Transient
	private String tLastName;
	
	@Transient
	private Integer NFScore;
	@Transient
	private Integer MAScore;	
	@Transient
	private Integer spStatus;
	
	public String gettStatus() {
		return tStatus;
	}
	public void settStatus(String tStatus) {
		this.tStatus = tStatus;
	}
	
	public String gettLastName() {
		return tLastName;
	}
	public void settLastName(String tLastName) {
		this.tLastName = tLastName;
	}
	
	public Boolean getInternalTransferCandidate() {
		return internalTransferCandidate;
	}
	public void setInternalTransferCandidate(Boolean internalTransferCandidate) {
		this.internalTransferCandidate = internalTransferCandidate;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Boolean getIsPortfolioNeeded() {
		return isPortfolioNeeded;
	}
	public void setIsPortfolioNeeded(Boolean isPortfolioNeeded) {
		this.isPortfolioNeeded = isPortfolioNeeded;
	}
	public void setIsResearchTeacher(Boolean isResearchTeacher) {
		this.isResearchTeacher = isResearchTeacher;
	}
	public Boolean getIsResearchTeacher() {
		return isResearchTeacher;
	}
	public Integer getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = Utility.trim(firstName);
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = Utility.trim(lastName);
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = Utility.trim(emailAddress);
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public Boolean getFbUser() {
		return fbUser;
	}
	public void setFbUser(Boolean fbUser) {
		this.fbUser = fbUser;
	}
	public Integer getExclusivePeriod() {
		return exclusivePeriod;
	}
	public void setExclusivePeriod(Integer exclusivePeriod) {
		this.exclusivePeriod = exclusivePeriod;
	}
	public String getAuthenticationCode() {
		return authenticationCode;
	}
	public void setAuthenticationCode(String authenticationCode) {
		this.authenticationCode = authenticationCode;
	}
	public String getVerificationCode() {
		return verificationCode;
	}
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
	public Integer getVerificationStatus() {
		return verificationStatus;
	}
	public void setVerificationStatus(Integer verificationStatus) {
		this.verificationStatus = verificationStatus;
	}
	public Integer getNoOfLogin() {
		return noOfLogin;
	}
	public void setNoOfLogin(Integer noOfLogin) {
		this.noOfLogin = noOfLogin;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getSendOpportunity() {
		return sendOpportunity;
	}
	public void setSendOpportunity(Boolean sendOpportunity) {
		this.sendOpportunity = sendOpportunity;
	}
	public String getIdentityVerificationPicture() {
		return identityVerificationPicture;
	}
	public void setIdentityVerificationPicture(String identityVerificationPicture) {
		this.identityVerificationPicture = identityVerificationPicture;
	}
	public Date getFgtPwdDateTime() {
		return fgtPwdDateTime;
	}
	public void setFgtPwdDateTime(Date fgtPwdDateTime) {
		this.fgtPwdDateTime = fgtPwdDateTime;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Integer getForgetCounter() {
		return forgetCounter;
	}
	public void setForgetCounter(Integer forgetCounter) {
		this.forgetCounter = forgetCounter;
	}
	
	public Integer getNormScore() {
		return normScore;
	}
	public void setNormScore(Integer normScore) {
		this.normScore = normScore;
	}
	public Integer getaScore() {
		return aScore;
	}
	public void setaScore(Integer aScore) {
		this.aScore = aScore;
	}
	public Integer getlRScore() {
		return lRScore;
	}
	public void setlRScore(Integer lRScore) {
		this.lRScore = lRScore;
	}
	
	public Integer getNFScore() {
		return NFScore;
	}
	public void setNFScore(Integer nFScore) {
		NFScore = nFScore;
	}
	public Integer getMAScore() {
		return MAScore;
	}
	public void setMAScore(Integer mAScore) {
		MAScore = mAScore;
	}
	public Integer getSpStatus() {
		return spStatus;
	}
	public void setSpStatus(Integer spStatus) {
		this.spStatus = spStatus;
	}
	
	public BigInteger getKSNID() {
		return KSNID;
	}
	public void setKSNID(BigInteger kSNID) {
		KSNID = kSNID;
	}
	public String getReferralURL() {
		return referralURL;
	}
	public void setReferralURL(String referralURL) {
		this.referralURL = referralURL;
	}
	
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof TeacherDetail))return false;
	    TeacherDetail teacherDetail = (TeacherDetail)object;
	  
	    if(this.teacherId.equals(teacherDetail.getTeacherId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+teacherId);
	}
	
	public static Comparator<TeacherDetail> teacherDetailComparatorNormScore = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail td1, TeacherDetail td2) {
			Integer cScore1 = td1.getNormScore();
			Integer cScore2 = td2.getNormScore();		
			int c = cScore1.compareTo(cScore2);
			return c;
		}		
	};
	
	public static Comparator<TeacherDetail> teacherDetailComparatorNormScoreDesc  = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail td1, TeacherDetail td2) {			
			Integer cScore1 = td1.getNormScore();
			Integer cScore2 = td2.getNormScore();		
			int c = cScore2.compareTo(cScore1);
			return c;
		}		
	};
	
	public static Comparator<TeacherDetail> TeacherDetailComparatorAScore = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail td1, TeacherDetail td2) {
			Integer aScore1 = td1.getaScore();
			Integer aScore2=td2.getaScore();		
			int c =aScore1.compareTo(aScore2);
			return c;
		}		
	};
	
	public static Comparator<TeacherDetail> TeacherDetailComparatorAScoreDesc  = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail td1, TeacherDetail td2) {			
			Integer aScore1 = td1.getaScore();
			Integer aScore2=td2.getaScore();		
			int c =aScore2.compareTo(aScore1);
			return c;
		}		
	};
	
	public static Comparator<TeacherDetail> TeacherDetailComparatorLRScore = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail td1, TeacherDetail td2) {
			Integer lRScore1 = td1.getlRScore();
			Integer lRScore2=td2.getlRScore();		
			int c =lRScore1.compareTo(lRScore2);
			return c;
		}		
	};
	
	public static Comparator<TeacherDetail> TeacherDetailComparatorLRScoreDesc  = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail td1, TeacherDetail td2) {			
			Integer lRScore1 = td1.getlRScore();
			Integer lRScore2=td2.getlRScore();		
			int c =lRScore2.compareTo(lRScore1);
			return c;
		}		
	};
	
	public static Comparator<TeacherDetail> TeacherDetailComparatorNFScore = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail td1, TeacherDetail td2) {
			Integer NFScore1 = td1.getNFScore();
			Integer NFScore2=td2.getNFScore();		
			int c =NFScore1.compareTo(NFScore2);
			return c;
		}		
	};
	
	public static Comparator<TeacherDetail> TeacherDetailComparatorNFScoreDesc  = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail td1, TeacherDetail td2) {			
			Integer NFScore1 = td1.getNFScore();
			Integer NFScore2=td2.getNFScore();		
			int c =NFScore2.compareTo(NFScore1);
			return c;
		}		
	};
	public static Comparator<TeacherDetail> TeacherDetailComparatorMAScore = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail td1, TeacherDetail td2) {
			Integer MAScore1 = td1.getMAScore();
			Integer MAScore2=td2.getMAScore();		
			int c =MAScore1.compareTo(MAScore2);
			return c;
		}		
	};
	
	public static Comparator<TeacherDetail> TeacherDetailComparatorMAScoreDesc  = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail td1, TeacherDetail td2) {			
			Integer MAScore1 = td1.getMAScore();
			Integer MAScore2=td2.getMAScore();		
			int c =MAScore2.compareTo(MAScore1);
			return c;
		}		
	};
	
	
	public static Comparator<TeacherDetail> TeacherDetailComparatorSpStatus = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail t1, TeacherDetail t2) {
			Integer spStatus1 = t1.getSpStatus();
			Integer spStatus2 =	t2.getSpStatus();		
			int c =spStatus1.compareTo(spStatus2);
			return c;
		}		
	};
	public static Comparator<TeacherDetail> TeacherDetailComparatorSpStatusDesc  = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail t1, TeacherDetail t2) {			
			Integer spStatus1 = t1.getSpStatus();
			Integer spStatus2 =	t2.getSpStatus();	
			int c =spStatus2.compareTo(spStatus1);
			return c;
		}		
	};
	
	
	public static Comparator<TeacherDetail> TeacherDetailComparatorLastName = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail t1, TeacherDetail t2) {
			String spStatus1 = t1.getLastName();
			String spStatus2 =	t2.getLastName();		
			int c =spStatus1.compareTo(spStatus2);
			return c;
		}		
	};
	public static Comparator<TeacherDetail> TeacherDetailComparatorLastNameDesc  = new Comparator<TeacherDetail>(){
		public int compare(TeacherDetail t1, TeacherDetail t2) {			
			String spStatus1 = t1.getLastName();
			String spStatus2 =	t2.getLastName();	
			int c =spStatus2.compareTo(spStatus1);
			return c;
		}		
	};
	
	
	@Override
	public String toString() {
		return String.valueOf(teacherId);
	}
}
