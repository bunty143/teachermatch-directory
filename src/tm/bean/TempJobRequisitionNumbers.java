package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="tempjobrequisitionnumbers")
public class TempJobRequisitionNumbers implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3864736701896256150L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer tempjobRequisitionId;
	
	@ManyToOne
	@JoinColumn(name="districtRequisitionId",referencedColumnName="districtRequisitionId")
	private DistrictRequisitionNumbers districtRequisitionNumbers;
	
	@ManyToOne
	@JoinColumn(name="schoolId",referencedColumnName="schoolId")
	private SchoolMaster schoolMaster;
	
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster userMaster;

	private Integer noOfSchoolExpHires;
	private String jobAuthKey; 
	private Date createdDateTime;

	public Integer getTempjobRequisitionId() {
		return tempjobRequisitionId;
	}

	public void setTempjobRequisitionId(Integer tempjobRequisitionId) {
		this.tempjobRequisitionId = tempjobRequisitionId;
	}


	public DistrictRequisitionNumbers getDistrictRequisitionNumbers() {
		return districtRequisitionNumbers;
	}

	public void setDistrictRequisitionNumbers(
			DistrictRequisitionNumbers districtRequisitionNumbers) {
		this.districtRequisitionNumbers = districtRequisitionNumbers;
	}

	public SchoolMaster getSchoolMaster() {
		return schoolMaster;
	}

	public void setSchoolMaster(SchoolMaster schoolMaster) {
		this.schoolMaster = schoolMaster;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public Integer getNoOfSchoolExpHires() {
		return noOfSchoolExpHires;
	}

	public void setNoOfSchoolExpHires(Integer noOfSchoolExpHires) {
		this.noOfSchoolExpHires = noOfSchoolExpHires;
	}

	public String getJobAuthKey() {
		return jobAuthKey;
	}

	public void setJobAuthKey(String jobAuthKey) {
		this.jobAuthKey = jobAuthKey;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
}
