package tm.bean.mq;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.WorkFlowStatus;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;



@Entity
@Table(name="mqevent")
public class MQEvent implements Serializable{

	private static final long serialVersionUID = -5300567914703061309L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer eventId;
	private String eventType;
	private String ksnTransId;
	private String tmTransId;
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherdetail;
	
	private String status;
	private Date createdDateTime;
	
	@Column(name="updatedDateTime")
	private Date updateDateTime;
	
	private Integer createdBy;
	private String ipAddress;
	
	@ManyToOne
	@JoinColumn(name="workFlowStatusId",referencedColumnName="workFlowStatusId")
	private WorkFlowStatus workFlowStatusId;
	
	private String ackStatus;
	private String msgStatus;
	private String eRegURL;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;     

	@ManyToOne
	@JoinColumn(name="statusId",referencedColumnName="statusId")
	private StatusMaster statusMaster; 
	
	@ManyToOne
	@JoinColumn(name="secondaryStatusId",referencedColumnName="secondaryStatusId")
	private SecondaryStatus secondaryStatus;
	
	private String ksnActionType;
	private Boolean checkEmailAPI;
	private String eRegStatus;
	private Boolean tmInitiate;
	
	public WorkFlowStatus getWorkFlowStatusId() {
		return workFlowStatusId;
	}
	public void setWorkFlowStatusId(WorkFlowStatus workFlowStatusId) {
		this.workFlowStatusId = workFlowStatusId;
	}
	public String getKsnActionType() {
		return ksnActionType;
	}
	public void setKsnActionType(String ksnActionType) {
		this.ksnActionType = ksnActionType;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getKsnTransId() {
		return ksnTransId;
	}
	public void setKsnTransId(String ksnTransId) {
		this.ksnTransId = ksnTransId;
	}
	public String getTmTransId() {
		return tmTransId;
	}
	public void setTmTransId(String tmTransId) {
		this.tmTransId = tmTransId;
	}
	public TeacherDetail getTeacherdetail() {
		return teacherdetail;
	}
	public void setTeacherdetail(TeacherDetail teacherdetail) {
		this.teacherdetail = teacherdetail;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public Date getUpdateDateTime() {
		return updateDateTime;
	}
	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public String getAckStatus() {
		return ackStatus;
	}
	public void setAckStatus(String ackStatus) {
		this.ackStatus = ackStatus;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public StatusMaster getStatusMaster() {
		return statusMaster;
	}
	public void setStatusMaster(StatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}
	public SecondaryStatus getSecondaryStatus() {
		return secondaryStatus;
	}
	public void setSecondaryStatus(SecondaryStatus secondaryStatus) {
		this.secondaryStatus = secondaryStatus;
	}
	public String getMsgStatus() {
		return msgStatus;
	}
	public void setMsgStatus(String msgStatus) {
		this.msgStatus = msgStatus;
	}
	public String geteRegURL() {
		return eRegURL;
	}
	public void seteRegURL(String eRegURL) {
		this.eRegURL = eRegURL;
	}
	public Boolean getCheckEmailAPI() {
		return checkEmailAPI;
	}
	public void setCheckEmailAPI(Boolean checkEmailAPI) {
		this.checkEmailAPI = checkEmailAPI;
	}
	public String geteRegStatus() {
		return eRegStatus;
	}
	public void seteRegStatus(String eRegStatus) {
		this.eRegStatus = eRegStatus;
	}
	public Boolean getTmInitiate() {
		return tmInitiate;
	}
	public void setTmInitiate(Boolean tmInitiate) {
		this.tmInitiate = tmInitiate;
	}
}
