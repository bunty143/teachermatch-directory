package tm.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.user.UserMaster;
/*
CREATE TABLE `jobapprovalnotes` (
 `jobApprovalNotesId` int(10) NOT NULL auto_increment,
 `userBy` int(5) NOT NULL COMMENT 'Foreign key of usermaster',
 `jobId` int(6) NOT NULL COMMENT 'Foreign key of joborder',
 `ipAddress` varchar(200) default NULL,
 `type` int(1) default NULL,
 `note` text default NULL,
 `createdDateTime` datetime NOT NULL,
 PRIMARY KEY  (`jobApprovalNotesId`)
);
*/
@Entity
@Table(name="jobapprovalnotes")
public class JobApprovalNotes {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer jobApprovalNotesId;
	@ManyToOne
	@JoinColumn(name="userBy",referencedColumnName="userId")
	private UserMaster userMaster;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	private String ipAddress;
	@Column(name="type" ,columnDefinition="1 is used to deny job")
	private Integer type;
	private String note;
	private Date createdDateTime;
	public Integer getJobApprovalNotesId() {
		return jobApprovalNotesId;
	}
	public void setJobApprovalNotesId(Integer jobApprovalNotesId) {
		this.jobApprovalNotesId = jobApprovalNotesId;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	} 
	

}
