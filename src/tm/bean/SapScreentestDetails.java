package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="sapscreentestdetails")
public class SapScreentestDetails implements Serializable 
{

	private static final long serialVersionUID = -7932006250700735410L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer sapScreenTestId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	private String subType;
	private String result;
	private String examinationDate;
	private String examinationArea1;
	private String proficiency;
	private String examinationArea2;
	private String clearDate;
	private Date createdDateTime;
	
	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public Integer getSapScreenTestId() {
		return sapScreenTestId;
	}
	public void setSapScreenTestId(Integer sapScreenTestId) {
		this.sapScreenTestId = sapScreenTestId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getExaminationDate() {
		return examinationDate;
	}
	public void setExaminationDate(String examinationDate) {
		this.examinationDate = examinationDate;
	}
	public String getExaminationArea1() {
		return examinationArea1;
	}
	public void setExaminationArea1(String examinationArea1) {
		this.examinationArea1 = examinationArea1;
	}
	public String getProficiency() {
		return proficiency;
	}
	public void setProficiency(String proficiency) {
		this.proficiency = proficiency;
	}
	public String getExaminationArea2() {
		return examinationArea2;
	}
	public void setExaminationArea2(String examinationArea2) {
		this.examinationArea2 = examinationArea2;
	}
	public String getClearDate() {
		return clearDate;
	}
	public void setClearDate(String clearDate) {
		this.clearDate = clearDate;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
}
