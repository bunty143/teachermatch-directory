package tm.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import tm.bean.user.UserMaster;
@Entity(name="jobwiseapprovalprocess")
public class JobWiseApprovalProcess {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int jobWiseApprovalProcessId;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobId;
	@ManyToOne
	@JoinColumn(name="jobApprovalProcessId",referencedColumnName="jobApprovalProcessId")
	private JobApprovalProcess jobApprovalProcessId;
	private Date createdDateTime;
	@ManyToOne
	@JoinColumn(name="createdBy",referencedColumnName="userId")
	private UserMaster createdBy;
	private int status;
	public int getJobWiseApprovalProcessId() {
		return jobWiseApprovalProcessId;
	}
	public void setJobWiseApprovalProcessId(int jobWiseApprovalProcessId) {
		this.jobWiseApprovalProcessId = jobWiseApprovalProcessId;
	}
	public JobOrder getJobId() {
		return jobId;
	}
	public void setJobId(JobOrder jobId) {
		this.jobId = jobId;
	}
	public JobApprovalProcess getJobApprovalProcessId() {
		return jobApprovalProcessId;
	}
	public void setJobApprovalProcessId(JobApprovalProcess jobApprovalProcessId) {
		this.jobApprovalProcessId = jobApprovalProcessId;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public UserMaster getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(UserMaster createdBy) {
		this.createdBy = createdBy;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	

}
