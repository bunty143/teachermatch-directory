package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="teacherresidencydetails")
public class TeacherResidencyDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5403251287277386662L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer teacherResidencyId; 
	
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherId;  
	
	private String residencyStreetAddress;
	private String residencyCity;
	private String residencyState;
	private String residencyZip;
	private String residencyCountry;
	private String residencyFromDate;
	private String residencyToDate;
	private Date createdDateTime;
	public Integer getTeacherResidencyId() {
		return teacherResidencyId;
	}
	public void setTeacherResidencyId(Integer teacherResidencyId) {
		this.teacherResidencyId = teacherResidencyId;
	}
	public TeacherDetail getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(TeacherDetail teacherId) {
		this.teacherId = teacherId;
	}
	public String getResidencyStreetAddress() {
		return residencyStreetAddress;
	}
	public void setResidencyStreetAddress(String residencyStreetAddress) {
		this.residencyStreetAddress = residencyStreetAddress;
	}
	public String getResidencyCity() {
		return residencyCity;
	}
	public void setResidencyCity(String residencyCity) {
		this.residencyCity = residencyCity;
	}
	public String getResidencyState() {
		return residencyState;
	}
	public void setResidencyState(String residencyState) {
		this.residencyState = residencyState;
	}
	public String getResidencyZip() {
		return residencyZip;
	}
	public void setResidencyZip(String residencyZip) {
		this.residencyZip = residencyZip;
	}
	public String getResidencyCountry() {
		return residencyCountry;
	}
	public void setResidencyCountry(String residencyCountry) {
		this.residencyCountry = residencyCountry;
	}
	public String getResidencyFromDate() {
		return residencyFromDate;
	}
	public void setResidencyFromDate(String residencyFromDate) {
		this.residencyFromDate = residencyFromDate;
	}
	public String getResidencyToDate() {
		return residencyToDate;
	}
	public void setResidencyToDate(String residencyToDate) {
		this.residencyToDate = residencyToDate;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

}
