package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="sapacademicdetails")
public class SapAcademicDetails implements Serializable 
{

	private static final long serialVersionUID = -1101126620196356437L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer sapAcademicId;
	@ManyToOne
	@JoinColumn(name="teacherId",referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder;
	
	private String subType;
	private String recordSequence;
	private String startDateAttended;
	private String endDateAttended;
	private String educationalEstablishment;
	private String collegeCode;
	private String certificate;
	private String branchOfStudy1;
	private String branchOfStudy2;
	private Date createdDateTime;

	public JobOrder getJobOrder() {
		return jobOrder;
	}
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public Integer getSapAcademicId() {
		return sapAcademicId;
	}
	public void setSapAcademicId(Integer sapAcademicId) {
		this.sapAcademicId = sapAcademicId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public String getRecordSequence() {
		return recordSequence;
	}
	public void setRecordSequence(String recordSequence) {
		this.recordSequence = recordSequence;
	}
	public String getStartDateAttended() {
		return startDateAttended;
	}
	public void setStartDateAttended(String startDateAttended) {
		this.startDateAttended = startDateAttended;
	}
	public String getEndDateAttended() {
		return endDateAttended;
	}
	public void setEndDateAttended(String endDateAttended) {
		this.endDateAttended = endDateAttended;
	}
	public String getEducationalEstablishment() {
		return educationalEstablishment;
	}
	public void setEducationalEstablishment(String educationalEstablishment) {
		this.educationalEstablishment = educationalEstablishment;
	}
	public String getCollegeCode() {
		return collegeCode;
	}
	public void setCollegeCode(String collegeCode) {
		this.collegeCode = collegeCode;
	}
	public String getCertificate() {
		return certificate;
	}
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	public String getBranchOfStudy1() {
		return branchOfStudy1;
	}
	public void setBranchOfStudy1(String branchOfStudy1) {
		this.branchOfStudy1 = branchOfStudy1;
	}
	public String getBranchOfStudy2() {
		return branchOfStudy2;
	}
	public void setBranchOfStudy2(String branchOfStudy2) {
		this.branchOfStudy2 = branchOfStudy2;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}



}
