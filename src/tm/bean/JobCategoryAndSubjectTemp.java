package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.QuestionOptions;
import tm.bean.assessment.QuestionsPool;

public class JobCategoryAndSubjectTemp implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4116148367631903910L;
	private Integer internaljobCategoryId;
	private Integer categoryId;
	private String  categoryName;
	private String  subjectIds;
	private String  subjectNames;
	
	public Integer getInternaljobCategoryId() {
		return internaljobCategoryId;
	}
	public void setInternaljobCategoryId(Integer internaljobCategoryId) {
		this.internaljobCategoryId = internaljobCategoryId;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getSubjectIds() {
		return subjectIds;
	}
	public void setSubjectIds(String subjectIds) {
		this.subjectIds = subjectIds;
	}
	public String getSubjectNames() {
		return subjectNames;
	}
	public void setSubjectNames(String subjectNames) {
		this.subjectNames = subjectNames;
	}
	


}
