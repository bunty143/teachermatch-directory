package tm.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.utility.Utility;

@Entity
@Table(name=" teacherhonor")
public class TeacherHonor implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6535318412214970855L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer honorId; 
	
	@ManyToOne
	@JoinColumn(name="teacherId", referencedColumnName="teacherId")
	private TeacherDetail teacherDetail;
	private String honor;
	private Integer honorYear;
	
	public Integer getHonorId() {
		return honorId;
	}
	public void setHonorId(Integer honorId) {
		this.honorId = honorId;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public String getHonor() {
		return honor;
	}
	public void setHonor(String honor) {
		this.honor = Utility.trim(honor);
	}
	public Integer getHonorYear() {
		return honorYear;
	}
	public void setHonorYear(Integer honorYear) {
		this.honorYear = honorYear;
	}
}
