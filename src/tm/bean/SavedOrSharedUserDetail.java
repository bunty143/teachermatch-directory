package tm.bean;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;


public class SavedOrSharedUserDetail implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1212091178687014938L;

	private TeacherDetail teacherDetail;
	
	private Integer savedTeacherId;
	private String teacherFirstName;
	private String teacherLastName;
	private String emailAddress;
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	private String userFirstName;
	private String userLastName;
	private Date createdDateTime; 
	private Integer normScore;
	
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public Integer getNormScore() {
		return normScore;
	}
	public void setNormScore(Integer normScore) {
		this.normScore = normScore;
	}
	public String getTeacherFirstName() {
		return teacherFirstName;
	}
	public void setTeacherFirstName(String teacherFirstName) {
		this.teacherFirstName = teacherFirstName;
	}
	public String getTeacherLastName() {
		return teacherLastName;
	}
	public void setTeacherLastName(String teacherLastName) {
		this.teacherLastName = teacherLastName;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	public Integer getSavedTeacherId() {
		return savedTeacherId;
	}
	public void setSavedTeacherId(Integer savedTeacherId) {
		this.savedTeacherId = savedTeacherId;
	}
	public static Comparator<SavedOrSharedUserDetail> teacherName  = new Comparator<SavedOrSharedUserDetail>(){
		public int compare(SavedOrSharedUserDetail jft1, SavedOrSharedUserDetail jft2) {			
			return jft1.getTeacherLastName().toUpperCase().compareTo(jft2.getTeacherLastName().toUpperCase());
		}		
	};
	
	public static Comparator<SavedOrSharedUserDetail> teacherNameDesc  = new Comparator<SavedOrSharedUserDetail>(){
		public int compare(SavedOrSharedUserDetail jft1, SavedOrSharedUserDetail jft2) {
			int c = jft2.getTeacherLastName().toUpperCase().compareTo(jft1.getTeacherLastName().toUpperCase());
			return c;
		}			
	};
	public static Comparator<SavedOrSharedUserDetail> normScoreSort  = new Comparator<SavedOrSharedUserDetail>(){
		public int compare(SavedOrSharedUserDetail jft1, SavedOrSharedUserDetail jft2) {			
			Integer cScore1 = Math.round(jft1.getNormScore());
			Integer cScore2 = Math.round(jft2.getNormScore());		
			int c = cScore1.compareTo(cScore2);
			return c;
		}			
	};
	
	public static Comparator<SavedOrSharedUserDetail> normScoreSortDesc  = new Comparator<SavedOrSharedUserDetail>(){
		public int compare(SavedOrSharedUserDetail jft1, SavedOrSharedUserDetail jft2) {			
			Integer cScore1 = Math.round(jft1.getNormScore());
			Integer cScore2 = Math.round(jft2.getNormScore());		
			int c = cScore2.compareTo(cScore1);
			return c;
		}				
	};
	public static Comparator<SavedOrSharedUserDetail> userNameDesc= new Comparator<SavedOrSharedUserDetail>(){
		public int compare(SavedOrSharedUserDetail jft1, SavedOrSharedUserDetail jft2) {
			int c = jft2.getUserLastName().toUpperCase().compareTo(jft1.getUserLastName().toUpperCase());
			return c;
		}			
	};
	
	public static Comparator<SavedOrSharedUserDetail> userName  = new Comparator<SavedOrSharedUserDetail>(){
		public int compare(SavedOrSharedUserDetail jft1, SavedOrSharedUserDetail jft2) {			
			return jft1.getUserLastName().toUpperCase().compareTo(jft2.getUserLastName().toUpperCase());
		}				
	};
	public static Comparator<SavedOrSharedUserDetail> savedDateTime= new Comparator<SavedOrSharedUserDetail>(){
		public int compare(SavedOrSharedUserDetail jft1, SavedOrSharedUserDetail jft2) {
			int c = jft2.getUserLastName().toUpperCase().compareTo(jft1.getUserLastName().toUpperCase());
			return c;
		}			
	};
	
	public static Comparator<SavedOrSharedUserDetail> savedDateTimeDesc  = new Comparator<SavedOrSharedUserDetail>(){
		 public int compare(SavedOrSharedUserDetail m1, SavedOrSharedUserDetail m2) {
		        return m2.getCreatedDateTime().compareTo(m1.getCreatedDateTime());
		  }			
	};
}
