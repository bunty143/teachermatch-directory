package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;

@Entity
@Table(name="districtapprovalgroups")
public class DistrictApprovalGroups implements Serializable
{
	private static final long serialVersionUID = -9121207989105655792L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int districtApprovalGroupsId;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtId;
	
	@ManyToOne
	@JoinColumn(name="jobCategoryId",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryId;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobId;
	
	private String groupName;
	private String groupMembers;
	private Integer noOfApprovals;
	private Date approvalGroupCreatedDateTime;
	private String status;
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public int getDistrictApprovalGroupsId() {
		return districtApprovalGroupsId;
	}


	public void setDistrictApprovalGroupsId(int districtApprovalGroupsId) {
		this.districtApprovalGroupsId = districtApprovalGroupsId;
	}


	public DistrictMaster getDistrictId() {
		return districtId;
	}


	public void setDistrictId(DistrictMaster districtId) {
		this.districtId = districtId;
	}


	public JobCategoryMaster getJobCategoryId() {
		return jobCategoryId;
	}


	public void setJobCategoryId(JobCategoryMaster jobCategoryId) {
		this.jobCategoryId = jobCategoryId;
	}


	public JobOrder getJobId() {
		return jobId;
	}


	public void setJobId(JobOrder jobId) {
		this.jobId = jobId;
	}


	public String getGroupName() {
		return groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public String getGroupMembers() {
		return groupMembers;
	}


	public void setGroupMembers(String groupMembers) {
		this.groupMembers = groupMembers;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	public Integer getNoOfApprovals() {
		return noOfApprovals;
	}


	public void setNoOfApprovals(Integer noOfApprovals) {
		this.noOfApprovals = noOfApprovals;
	}

	
	public Date getApprovalGroupCreatedDateTime() {
		return approvalGroupCreatedDateTime;
	}


	public void setApprovalGroupCreatedDateTime(Date approvalGroupCreatedDateTime) {
		this.approvalGroupCreatedDateTime = approvalGroupCreatedDateTime;
	}


	@Override
	public String toString() {
		return "{"+groupName+", ["+groupMembers+"] }";
	}
}