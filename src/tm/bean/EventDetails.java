package tm.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4QuestionSets;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name = "eventdetails")
public class EventDetails implements Serializable 
{
	private static final long serialVersionUID = -1700587597555819912L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer eventId;
	private String eventName;
	@ManyToOne
	@JoinColumn(name = "districtId", referencedColumnName = "districtId")
	private DistrictMaster districtMaster;
	
	@ManyToOne
	@JoinColumn(name="headQuarterId",referencedColumnName="headQuarterId")
	private HeadQuarterMaster headQuarterMaster;

	@ManyToOne
	@JoinColumn(name="branchId",referencedColumnName="branchId")
	private BranchMaster branchMaster;
	
	@ManyToOne
	@JoinColumn(name = "eventTypeId", referencedColumnName = "eventTypeId")
	private CmsEventTypeMaster eventTypeId;

	@ManyToOne
	@JoinColumn(name = "eventSchedulelId", referencedColumnName = "eventScheduleId")
	private CmsEventScheduleMaster eventSchedulelId;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="jobId")
	private JobOrder jobOrder; 
	
	
	@ManyToOne
	@JoinColumn(name = "vviQuestionSet", referencedColumnName = "ID")
	private I4QuestionSets i4QuestionSets;
	
	private Long schoolId;     

	@ManyToOne
	@JoinColumn(name = "createdBy", referencedColumnName = "userId")
	private UserMaster createdBY;
	
	private Date createdDateTime;
	
	private String status, description, logo, eventURL;

	private String msgtoparticipants;

	private String msgtofacilitators;

	private String subjectforFacilator;

	private String subjectforParticipants;
	
	@Transient
	private Integer noOfCandidate;
	
	@Transient
	private Date eventStartDate;
	
	@Transient
	private Date eventEndDate;
	
	@Transient
	private String districtOrSchoolName;
	
	public I4QuestionSets getI4QuestionSets() {
		return i4QuestionSets;
	}

	public void setI4QuestionSets(I4QuestionSets i4QuestionSets) {
		this.i4QuestionSets = i4QuestionSets;
	}

	public String getEventURL() {
		return eventURL;
	}

	public void setEventURL(String eventURL) {
		this.eventURL = eventURL;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public CmsEventTypeMaster getEventTypeId() {
		return eventTypeId;
	}

	public void setEventTypeId(CmsEventTypeMaster eventTypeId) {
		this.eventTypeId = eventTypeId;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}

	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}

	

	public CmsEventScheduleMaster getEventSchedulelId() {
		return eventSchedulelId;
	}

	public void setEventSchedulelId(CmsEventScheduleMaster eventSchedulelId) {
		this.eventSchedulelId = eventSchedulelId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public UserMaster getCreatedBY() {
		return createdBY;
	}

	public void setCreatedBY(UserMaster createdBY) {
		this.createdBY = createdBY;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getMsgtoparticipants() {
		return msgtoparticipants;
	}

	public void setMsgtoparticipants(String msgtoparticipants) {
		this.msgtoparticipants = msgtoparticipants;
	}

	public String getMsgtofacilitators() {
		return msgtofacilitators;
	}

	public void setMsgtofacilitators(String msgtofacilitators) {
		this.msgtofacilitators = msgtofacilitators;
	}

	public String getSubjectforFacilator() {
		return subjectforFacilator;
	}

	public void setSubjectforFacilator(String subjectforFacilator) {
		this.subjectforFacilator = subjectforFacilator;
	}

	public String getSubjectforParticipants() {
		return subjectforParticipants;
	}

	public void setSubjectforParticipants(String subjectforParticipants) {
		this.subjectforParticipants = subjectforParticipants;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	public HeadQuarterMaster getHeadQuarterMaster() {
		return headQuarterMaster;
	}

	public void setHeadQuarterMaster(HeadQuarterMaster headQuarterMaster) {
		this.headQuarterMaster = headQuarterMaster;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Long getSchoolId() {
		return schoolId;
	}
	
	public Integer getNoOfCandidate() {
		return noOfCandidate;
	}

	public void setNoOfCandidate(Integer noOfCandidate) {
		this.noOfCandidate = noOfCandidate;
	}
	
	public void setEventStartDate(Date eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	public Date getEventStartDate() {
		return eventStartDate;
	}

	public void setEventEndDate(Date eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	public Date getEventEndDate() {
		return eventEndDate;
	}
	
	public String getDistrictOrSchoolName() {
		return districtOrSchoolName;
	}

	public void setDistrictOrSchoolName(String districtOrSchoolName) {
		this.districtOrSchoolName = districtOrSchoolName;
	}

	//New Added by Mukesh
	@Override	
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof EventDetails))return false;
	    EventDetails eventDetails = (EventDetails)object;
	  
	    if(this.eventId.equals(eventDetails.getEventId()))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	@Override	
	public int hashCode() 
	{	
		return new Integer(""+eventId);
	}
}
