package tm.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

@Entity
@Table(name="jobdocumentsbydistrict")
public class JobDocumentsByDistrict implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -2345073720731423335L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer jobDocsId;

	@ManyToOne
	@JoinColumns({
					@JoinColumn(name="uploadedBy",referencedColumnName="userId"), 
					@JoinColumn(name="entityType",referencedColumnName="entityType")
				})
	private UserMaster userMaster;

	@ManyToOne
	@JoinColumn(name="jobCategory",referencedColumnName="jobCategoryId")
	private JobCategoryMaster jobCategoryMaster;
	
	@ManyToOne
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private DistrictMaster districtMaster;


	//private Integer entityType;		
	private String docsName;
	private Date createdDateTime;
	private String status;
	private String uploadedFileName;
	public Integer getJobDocsId() {
		return jobDocsId;
	}
	public void setJobDocsId(Integer jobDocsId) {
		this.jobDocsId = jobDocsId;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public UserMaster getUserMaster() {
		return userMaster;
	}
	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	public JobCategoryMaster getJobCategoryMaster() {
		return jobCategoryMaster;
	}
	public void setJobCategoryMaster(JobCategoryMaster jobCategoryMaster) {
		this.jobCategoryMaster = jobCategoryMaster;
	}
	public String getDocsName() {
		return docsName;
	}
	public void setDocsName(String docsName) {
		this.docsName = docsName;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUploadedFileName() {
		return uploadedFileName;
	}
	public void setUploadedFileName(String uploadedFileName) {
		this.uploadedFileName = uploadedFileName;
	}


}
