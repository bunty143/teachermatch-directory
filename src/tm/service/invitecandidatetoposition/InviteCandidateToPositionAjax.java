package tm.service.invitecandidatetoposition;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.invitecandidatetoposition.InviteCandidateToPositions;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.inviteCandidateToPositionDAO.InviteCandidateDAO;
import tm.utility.IPAddressUtility;
import tm.utility.TMCommonUtil;
import tm.utility.Utility;

public class InviteCandidateToPositionAjax {

	String locale = Utility.getValueOfPropByKey("locale");
	String msgYrSesstionExp =Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);

	@Autowired
	private InviteCandidateDAO inviteCandidateDAO;
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;


	/* public String storeInvitecandidateData(String positionId,long jobForTeacherId){

		 	WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();

			HttpSession session = request.getSession(false);

			UserMaster userMaster=null;
			DistrictMaster districtMaster=null;

			TeacherDetail teacherDetail = null;
			JobOrder jobOrder = null;

			JobForTeacher jobForTeacher = null;

			 System.out.println("Hello 1");

			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			else{

				userMaster=	(UserMaster) session.getAttribute("userMaster");

				jobForTeacher = jobForTeacherDAO.findById(jobForTeacherId, false, false);


				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				if(jobForTeacher.getJobId()!=null){
					jobOrder=jobForTeacher.getJobId();
				}

				if(jobForTeacher.getTeacherId()!=null){
					teacherDetail =jobForTeacher.getTeacherId();
				}

				 System.out.println("Hello 3");
			}

		 try{
			 InviteCandidateToPositions invitecandidate = new InviteCandidateToPositions();

			 System.out.println("Hello");

			 Date date = new Date();
			 SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSSSSS");

			 String positionArray[]= positionId.split(",");

			 for(String posId : positionArray)
			 {
				 invitecandidate.setCreatedDateTime(dateFormat.parse(dateFormat.format(date)));
				 invitecandidate.setTeacherId(teacherDetail);
				 invitecandidate.setDistrictId(districtMaster);
				 invitecandidate.setJobId(jobOrder);
				 invitecandidate.setPositionId(Integer.parseInt(posId));
				 invitecandidate.setCreatedBy(userMaster);
				 invitecandidate.setStatus("A");
				 invitecandidate.setServiceProvider("HireVue");
				 invitecandidate.setIpAddress(IPAddressUtility.getIpAddress(request));

				 invitecandidatetoposition.makePersistent(invitecandidate);
			 }



			 System.out.println("Hello 33");
		 }
		 catch (Exception e) {
		e.printStackTrace();
		}

		 return "";
	 }
	 */
	public String storeInvitecandidateData(String positionId,long jobForTeacherId){
		String posSuccessName = "";
		String posErrorName = "";
		WebContext context;
		context = WebContextFactory.get();
		String csrftoken = "";
		String sessionId = "";
		String firstName = "" , lastName = "", email = "";
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		csrftoken = (String)session.getAttribute("csrftoken");
		sessionId = (String)session.getAttribute("hireVueSessionId");

		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;

		TeacherDetail teacherDetail = null;
		JobOrder jobOrder = null;

		JobForTeacher jobForTeacher = null;

		System.out.println("Hello 1");

		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else{

			userMaster= (UserMaster) session.getAttribute("userMaster");

			jobForTeacher = jobForTeacherDAO.findById(jobForTeacherId, false, false);


			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			if(jobForTeacher.getJobId()!=null){
				jobOrder=jobForTeacher.getJobId();
			}

			if(jobForTeacher.getTeacherId()!=null){
				teacherDetail =jobForTeacher.getTeacherId();
			}

			System.out.println("Hello 3");
		}

		try{

			firstName = jobForTeacher.getTeacherId().getFirstName();
			lastName = jobForTeacher.getTeacherId().getLastName();
			email = jobForTeacher.getTeacherId().getEmailAddress();


			InviteCandidateToPositions invitecandidate;


			String HvApisId = "";
			String HvApisCode = "";
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSSSSS");

			String positionArray[]= positionId.split(",");

			for(String posIds : positionArray)
			{
				if(posIds.trim().toString()!="" && !posIds.trim().toString().equals(""))
				{
					String posIdsArr[] = posIds.split("__");
					String posId = posIdsArr[0];
					
					String interviewStatus = TMCommonUtil.addACandidateToAPosition(csrftoken, sessionId, posId, firstName, lastName, email);
					if(interviewStatus.contains("HTTP/1.1 201 CREATED"))
					{
						

						String strarr[] = interviewStatus.split(",");
						for(int i=0; i<=strarr.length-1; i++)
						{
							//System.out.println(strarr[i]);
							if(strarr[i].contains("X-HvApi-Id:"))
							{
								String HvApis[] =  strarr[i].split(":");
								HvApisId = HvApis[1].trim();
							}
							if(strarr[i].contains("X-HvApi-Interview-Code:"))
							{
								String HvApintrvs[] =  strarr[i].split(":");
								HvApisCode = HvApintrvs[1].trim();
							}
						}
						
						invitecandidate = new InviteCandidateToPositions();
						invitecandidate.setCreatedDateTime(dateFormat.parse(dateFormat.format(date)));
						invitecandidate.setTeacherId(teacherDetail);
						invitecandidate.setDistrictId(districtMaster);
						invitecandidate.setJobId(jobOrder);
						invitecandidate.setPositionId(Integer.parseInt(posId));
						invitecandidate.setCreatedBy(userMaster);
						invitecandidate.setStatus("A");
						invitecandidate.setServiceProvider("HireVue");
						invitecandidate.setIpAddress(IPAddressUtility.getIpAddress(request));
						invitecandidate.setInterviewId(HvApisId);
						invitecandidate.setInterviewCode(HvApisCode);
						inviteCandidateDAO.makePersistent(invitecandidate);
						posSuccessName += "Candidate is successfully applied to the position: "+posIdsArr[1]+"<br>";
					}
					else
						posErrorName += "<font color='red'>Candidate is not applied to the posion: "+posIdsArr[1]+"</font><br>";
				}
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return posSuccessName+posErrorName;
	}


	public Integer[] checkAppliedPositionOfInviteCandidate(String positionId,long jobForTeacherId){

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		List<InviteCandidateToPositions> inviteCandidateToPositions = null;

		String positionArray[]= positionId.split(",");
		Integer[] intArray = new Integer[positionArray.length];
		Integer[] checkPositionIdtArray =null;

		for (int i = 0; i < positionArray.length; i++) {
			String numberAsString = positionArray[i];
			intArray[i] = Integer.parseInt(numberAsString);
		}

		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		else
		{

			try{
				if(intArray.length>0)
				{
					inviteCandidateToPositions = inviteCandidateDAO.findByCriteria(Restrictions.in("positionId", intArray));


				}

				checkPositionIdtArray = new Integer[inviteCandidateToPositions.size()];

				int c=0;


				for(InviteCandidateToPositions checkedpositionId :inviteCandidateToPositions)
				{
					System.out.println("C : "+c);
					checkPositionIdtArray[c] = checkedpositionId.getPositionId();

					c++;
				}

				System.out.println(checkPositionIdtArray.length+"******");
			}

			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return checkPositionIdtArray;
	}

	public void setInvitecandidatetoposition(InviteCandidateDAO invitecandidatetoposition) {
		this.inviteCandidateDAO = invitecandidatetoposition;
	}

	public InviteCandidateDAO getInvitecandidatetoposition() {
		return inviteCandidateDAO;
	}

}
