package tm.service.jobapplication;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.api.controller.JeffcoAPIController;
import tm.bean.DistrictSpecificTeacherTfaOptions;
import tm.bean.EmployeeMaster;
import tm.bean.JobCategoryTransaction;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.PraxisMaster;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAdditionalDocuments;
import tm.bean.TeacherAnswerDetail;
import tm.bean.TeacherAssessmentOption;
import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherExperience;
import tm.bean.TeacherGeneralKnowledgeExam;
import tm.bean.TeacherHonor;
import tm.bean.TeacherInvolvement;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherRole;
import tm.bean.TeacherSectionDetail;
import tm.bean.TeacherSubjectAreaExam;
import tm.bean.TeacherVideoLink;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.master.CertificationStatusMaster;
import tm.bean.master.CertificationTypeMaster;
import tm.bean.master.CityMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.CurrencyMaster;
import tm.bean.master.DegreeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioAnswers;
import tm.bean.master.DistrictSpecificPortfolioOptions;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.DspqFieldMaster;
import tm.bean.master.DspqGroupMaster;
import tm.bean.master.DspqJobWiseStatus;
import tm.bean.master.DspqPortfolioName;
import tm.bean.master.DspqRouter;
import tm.bean.master.DspqSectionMaster;
import tm.bean.master.EmpRoleTypeMaster;
import tm.bean.master.EthinicityMaster;
import tm.bean.master.EthnicOriginMaster;
import tm.bean.master.FieldMaster;
import tm.bean.master.FieldOfStudyMaster;
import tm.bean.master.GenderMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.OptionsForDistrictSpecificQuestions;
import tm.bean.master.OrgTypeMaster;
import tm.bean.master.PeopleRangeMaster;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.QqQuestionsetQuestions;
import tm.bean.master.RaceMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SpokenLanguageMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectAreaExamMaster;
import tm.bean.master.TFAAffiliateMaster;
import tm.bean.master.TFARegionMaster;
import tm.bean.master.TeacherAnswerDetailsForDistrictSpecificQuestions;
import tm.bean.master.UniversityMaster;
import tm.bean.teacher.TeacherLanguages;
import tm.bean.user.UserMaster;
import tm.dao.CandidateRawScoreDAO;
import tm.dao.DistrictSpecificTeacherTfaOptionsDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.JobCategoryTransactionDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAdditionalDocumentsDAO;
import tm.dao.TeacherAnswerDetailDAO;
import tm.dao.TeacherAssessmentAttemptDAO;
import tm.dao.TeacherAssessmentDetailDAO;
import tm.dao.TeacherAssessmentQuestionDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherGeneralKnowledgeExamDAO;
import tm.dao.TeacherHonorDAO;
import tm.dao.TeacherInvolvementDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherRoleDAO;
import tm.dao.TeacherSubjectAreaExamDAO;
import tm.dao.TeacherVideoLinksDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.master.CertificationStatusMasterDAO;
import tm.dao.master.CertificationTypeMasterDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.CurrencyMasterDAO;
import tm.dao.master.DegreeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificPortfolioAnswersDAO;
import tm.dao.master.DistrictSpecificPortfolioQuestionsDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.DspqFieldMasterDAO;
import tm.dao.master.DspqGroupMasterDAO;
import tm.dao.master.DspqJobWiseStatusDAO;
import tm.dao.master.DspqPortfolioNameDAO;
import tm.dao.master.DspqRouterDAO;
import tm.dao.master.DspqSectionMasterDAO;
import tm.dao.master.EmpRoleTypeMasterDAO;
import tm.dao.master.EthinicityMasterDAO;
import tm.dao.master.EthnicOriginMasterDAO;
import tm.dao.master.FieldMasterDAO;
import tm.dao.master.FieldOfStudyMasterDAO;
import tm.dao.master.GenderMasterDAO;
import tm.dao.master.OrgTypeMasterDAO;
import tm.dao.master.PeopleRangeMasterDAO;
import tm.dao.master.QqQuestionsetQuestionsDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.SpokenLanguageMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectAreaExamMasterDAO;
import tm.dao.master.TFAAffiliateMasterDAO;
import tm.dao.master.TFARegionMasterDAO;
import tm.dao.master.TeacherAnswerDetailsForDistrictSpecificQuestionsDAO;
import tm.dao.master.UniversityMasterDAO;
import tm.dao.master.UserEmailNotificationsDAO;
import tm.dao.teacher.TeacherLanguagesDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.AssessmentCampaignAjax;
import tm.services.CadidateRawScoreThread;
import tm.services.CommonService;
import tm.services.DemoScheduleMailThread;
import tm.services.DistrictAjax;
import tm.services.EmailerService;
import tm.services.MailSendToTeacher;
import tm.services.MailText;
import tm.services.PaginationAndSorting;
import tm.services.StateAjax;
import tm.services.district.PrintOnConsole;
import tm.services.report.CGReportService;
import tm.services.teacher.DistrictPortfolioConfigAjax;
import tm.services.teacher.PFAcademics;
import tm.services.teacher.PFCertifications;
import tm.services.teacher.PFExperiences;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class JobApplicationServiceAjax {
	
	
	 String locale = Utility.getValueOfPropByKey("locale");
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lnkAddCerti_Lice=Utility.getLocaleValuePropByKey("lnkAddCerti/Lice", locale);
	 
	 String optSchool=Utility.getLocaleValuePropByKey("tableheaderSchool", locale);
	 String lblDatAtt=Utility.getLocaleValuePropByKey("lblDatAtt", locale);
	 String lblDgr=Utility.getLocaleValuePropByKey("lblDgr", locale);
	 String lblGPA=Utility.getLocaleValuePropByKey("lblGPA", locale);
	 String lblFildOfStudy=Utility.getLocaleValuePropByKey("lblFildOfStudy", locale);
	 String headTranscript=Utility.getLocaleValuePropByKey("headTranscript", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	 String lnkDlt=Utility.getLocaleValuePropByKey("lnkDlt", locale);
	 String msgNorecordfound=Utility.getLocaleValuePropByKey("msgNorecordfound", locale);
	 
	 String editToolTip=Utility.getLocaleValuePropByKey("DSPQ_EditToolTip", locale);
	 String deleteToolTip=Utility.getLocaleValuePropByKey("DSPQ_DeleteToolTip", locale);
	 String grdseparator=Utility.getLocaleValuePropByKey("DSPQ_Grdseparator", locale);
	 String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	 String msgViewScoreReport=Utility.getLocaleValuePropByKey("msgViewScoreReport", locale);
	 
	 
	 @Autowired
	 private PeopleRangeMasterDAO peopleRangeMasterDAO;
	 
	 @Autowired
	 private OrgTypeMasterDAO orgTypeMasterDAO;
	 
	 @Autowired
	 private EmpRoleTypeMasterDAO empRoleTypeMasterDAO;
		
	 
	@Autowired
	private FieldMasterDAO fieldMasterDAO;
	 
    @Autowired
	private TFARegionMasterDAO tfaRegionMasterDAO;
    
	@Autowired
	private TFAAffiliateMasterDAO tfaAffiliateMasterDAO;
		
	@Autowired
	private DspqPortfolioNameDAO dspqPortfolioNameDAO;
	
	@Autowired
	private DspqRouterDAO dspqRouterDAO;
	
	@Autowired
	private DspqFieldMasterDAO dspqFieldMasterDAO;
	
	@Autowired
	private DspqSectionMasterDAO dspqSectionMasterDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private DspqGroupMasterDAO dspqGroupMasterDAO;
	
	@Autowired
	private CountryMasterDAO countryMasterDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	
	@Autowired
	private EthinicityMasterDAO ethinicityMasterDAO;
	
	@Autowired
	private EthnicOriginMasterDAO ethnicOriginMasterDAO;
	
	@Autowired
	private GenderMasterDAO genderMasterDAO;
	
	@Autowired
	private PFCertifications pfCertifications;
	
	@Autowired
	private PFExperiences pFExperiences;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private CertificationStatusMasterDAO certificationStatusMasterDAO;
	
	@Autowired
	private CertificationTypeMasterDAO certificationTypeMasterDAO;
	
	@Autowired
	private PFAcademics pFAcademics;
	
	@Autowired
	private DspqJobWiseStatusDAO dspqJobWiseStatusDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private AssessmentCampaignAjax assessmentCampaignAjax;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private StateAjax stateAjax;
	
	@Autowired
	private DistrictSpecificPortfolioQuestionsDAO districtSpecificPortfolioQuestionsDAO;
	
	@Autowired
	private DegreeMasterDAO degreeMasterDAO;
	
	@Autowired
	private UniversityMasterDAO universityMasterDAO;
	
	@Autowired
	private FieldOfStudyMasterDAO fieldOfStudyMasterDAO;
	
	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	
	@Autowired
	private DistrictSpecificPortfolioAnswersDAO districtSpecificPortfolioAnswersDAO;
	
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	
	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	
	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;
	
	@Autowired
	private TeacherVideoLinksDAO teacherVideoLinksDAO;
	
	@Autowired
	private TeacherAdditionalDocumentsDAO teacherAdditionalDocumentsDAO;
	
	@Autowired
	private CurrencyMasterDAO currencyMasterDAO;
	
	@Autowired
	private TeacherRoleDAO teacherRoleDAO;
	
	@Autowired
	private TeacherInvolvementDAO teacherInvolvementDAO;
	
	@Autowired
	private TeacherHonorDAO teacherHonorDAO;
	
	@Autowired
	private DistrictAjax districtAjax;
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	
	@Autowired
	private QqQuestionsetQuestionsDAO qqQuestionsetQuestionsDAO;
	
	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	
	@Autowired
	private TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	
	@Autowired
	private DistrictSpecificTeacherTfaOptionsDAO districtSpecificTeacherTfaOptionsDAO;
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private TeacherAssessmentDetailDAO teacherAssessmentDetailDAO;

	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;

	@Autowired
	private TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO;

	@Autowired
	private TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO;

	@Autowired
	private TeacherAnswerDetailDAO teacherAnswerDetailDAO;
	
	@Autowired
	private CGReportService reportService;
	
	@Autowired
	private CandidateRawScoreDAO candidateRawScoreDAO;
	
	@Autowired
	private UserEmailNotificationsDAO userEmailNotificationsDAO;
	
	@Autowired
	private TeacherLanguagesDAO teacherLanguagesDAO;
	
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	
	@Autowired
	private DistrictPortfolioConfigAjax districtPortfolioConfigAjax;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private SpokenLanguageMasterDAO spokenLanguageMasterDAO;
	
	@Autowired
	private JobCategoryTransactionDAO jobCategoryTransactionDAO;
	
	@Autowired
	private TeacherGeneralKnowledgeExamDAO teacherGeneralKnowledgeExamDAO;
	
	@Autowired
	private TeacherSubjectAreaExamDAO teacherSubjectAreaExamDAO;
	
	@Autowired
	private SubjectAreaExamMasterDAO subjectAreaExamMasterDAO;
	
	@Transactional(readOnly=false)
	public String getPortfolioHeader(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getPortfolioHeader");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		String locale = Utility.getValueOfPropByKey("locale");
		StringBuffer sb=new StringBuffer();
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		JobOrder jobOrder=null;
		JobCategoryMaster jobCategoryMaster=null;
		int districtId=0;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			if(jobOrder!=null && jobOrder.getDistrictMaster()!=null)
				districtId=jobOrder.getDistrictMaster().getDistrictId();
			
			if(jobOrder!=null )
				jobCategoryMaster=jobOrder.getJobCategoryMaster();
		}
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		sb.append("<input type='hidden' id='dspqJobId' value='"+iJobId+"'>");
		sb.append("<input type='hidden' id='dspqct' value='"+candidateType+"'>");
		sb.append("<input type='hidden' id='dspqjobflowdistrictId' value='"+districtId+"'>");
		
		DspqPortfolioName dspqPortfolioNameNew =getDSPQByJobOrder(jobOrder,sCandidateTypeDec);
		
		boolean isCoverLetterG1=false;
		boolean isPersonalInformationG1=false;
		boolean isAffiliateG4=false;
		boolean isProfessionalG4=false;
		boolean isQQRequired=false;
		boolean isEPIRequired=false;
		boolean isJSIRequired=false;
		boolean isCompleted=true;
		
		Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
		
		if(dspqPortfolioNameNew!=null)
		{
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, sCandidateTypeDec);
			sb.append("<input type='hidden' id='dspqPortfolioNameId' value='"+dspqPortfolioNameNew.getDspqPortfolioNameId()+"'>");
			
			Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
			isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
			isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
			isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
			isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
		}
		
		
		
		Map<Integer, String> mapNAMENavi=new HashMap<Integer, String>();
		Map<Integer, String> mapShortNAMENavi=new HashMap<Integer, String>();
		Map<Integer, String> mapURLNavi=new HashMap<Integer, String>();
		
		isQQRequired=getPreSreen(jobCategoryMaster, sCandidateTypeDec);
		isEPIRequired=getEPI(jobCategoryMaster, sCandidateTypeDec);
		isJSIRequired=getCustomQuestion(jobOrder,jobCategoryMaster, sCandidateTypeDec,teacherDetail);
		
		DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
		if(dspqJobWiseStatus==null)
		{
			dspqJobWiseStatus=new DspqJobWiseStatus();
			dspqJobWiseStatus.setTeacherDetail(teacherDetail);
			dspqJobWiseStatus.setJobOrder(jobOrder);
			dspqJobWiseStatus.setiPAddress(IPAddressUtility.getIpAddress(request));
			dspqJobWiseStatus.setCreatedDataTime(new Date());
			
			dspqJobWiseStatus.setIsCoverLetterRequired(isCoverLetterG1);
			dspqJobWiseStatus.setIsPersonaInfoRequired(isPersonalInformationG1);
			
			if(mapGroup.get(2)!=null )
				dspqJobWiseStatus.setIsAcademicRequired(true);
			else
				dspqJobWiseStatus.setIsAcademicRequired(false);
			
			if(mapGroup.get(3)!=null )
				dspqJobWiseStatus.setIsCredentialsRequired(true);
			else
				dspqJobWiseStatus.setIsCredentialsRequired(false);
			
			dspqJobWiseStatus.setIsProfessionalRequired(isProfessionalG4);
			dspqJobWiseStatus.setIsAffidaviteRequired(isAffiliateG4);
			dspqJobWiseStatus.setIsPreScreenRequired(isQQRequired);
			dspqJobWiseStatus.setIsEPIRequired(isEPIRequired);
			dspqJobWiseStatus.setIsJSIRequired(isJSIRequired);
			
			if(isEPIRequired)
			{
				boolean isEPICompleteStatus=isEPICompleteStatus(teacherDetail);
				if(isEPICompleteStatus)
					dspqJobWiseStatus.setEPI(true);
				else
					dspqJobWiseStatus.setEPI(false);
			}
			
			if(isJSIRequired)
			{
				boolean isJSICompleteStatus=isJSICompleteStatus(teacherDetail,jobOrder);
				if(isJSICompleteStatus)
					dspqJobWiseStatus.setJSI(true);
				else
					dspqJobWiseStatus.setJSI(false);
			}
			
			dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
		}
		else
		{
			dspqJobWiseStatus.setIsCoverLetterRequired(isCoverLetterG1);
			dspqJobWiseStatus.setIsPersonaInfoRequired(isPersonalInformationG1);
			
			if(mapGroup.get(2)!=null )
				dspqJobWiseStatus.setIsAcademicRequired(true);
			else
				dspqJobWiseStatus.setIsAcademicRequired(false);
			
			if(mapGroup.get(3)!=null )
				dspqJobWiseStatus.setIsCredentialsRequired(true);
			else
				dspqJobWiseStatus.setIsCredentialsRequired(false);
			
			dspqJobWiseStatus.setIsProfessionalRequired(isProfessionalG4);
			dspqJobWiseStatus.setIsAffidaviteRequired(isAffiliateG4);
			dspqJobWiseStatus.setIsPreScreenRequired(isQQRequired);
			dspqJobWiseStatus.setIsEPIRequired(isEPIRequired);
			dspqJobWiseStatus.setIsJSIRequired(isJSIRequired);
			
			if(isEPIRequired && ( dspqJobWiseStatus.getEPI()==null || !dspqJobWiseStatus.getEPI() ))
			{
				boolean isEPICompleteStatus=isEPICompleteStatus(teacherDetail);
				if(isEPICompleteStatus)
					dspqJobWiseStatus.setEPI(true);
				else
					dspqJobWiseStatus.setEPI(false);
			}
			
			if(isJSIRequired && (dspqJobWiseStatus.getJSI()==null || !dspqJobWiseStatus.getJSI()))
			{
				boolean isJSICompleteStatus=isJSICompleteStatus(teacherDetail,jobOrder);
				if(isJSICompleteStatus)
					dspqJobWiseStatus.setJSI(true);
				else
					dspqJobWiseStatus.setJSI(false);
			}
			
			dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
			
		}
		
		int iAffliate=0;
		if(sCandidateTypeDec!=null && !sCandidateTypeDec.equals("") && ( sCandidateTypeDec.equalsIgnoreCase("I") || sCandidateTypeDec.equalsIgnoreCase("T") ) )
			iAffliate=1;
		updateJFT(dspqJobWiseStatus, teacherDetail, jobOrder, iAffliate);
		
		int iNav=0;
		if(isCoverLetterG1)
		{
			iNav++;
			mapNAMENavi.put(iNav, "DSPQlblCoverLetter");
			mapShortNAMENavi.put(iNav, "CL");
			mapURLNavi.put(iNav, "managejobapplicationflow.do?jobId="+iJobId+"&ct="+candidateType+"");
		}
		if(isPersonalInformationG1)
		{
			iNav++;
			mapNAMENavi.put(iNav, "DSPQlblPersonal");
			mapShortNAMENavi.put(iNav, "PI");
			mapURLNavi.put(iNav, "jobapplicationflowpersonal.do?jobId="+iJobId+"&ct="+candidateType+"");
		}
		if(mapGroup.get(2)!=null )
		{
			iNav++;
			mapNAMENavi.put(iNav, "DSPQlblAcademic");
			mapShortNAMENavi.put(iNav, "AC");
			mapURLNavi.put(iNav, "jobapplicationflowacademic.do?jobId="+iJobId+"&ct="+candidateType+"");
		}
		if(mapGroup.get(3)!=null )
		{
			iNav++;
			mapNAMENavi.put(iNav, "DSPQlblCredentials");
			mapShortNAMENavi.put(iNav, "CR");
			mapURLNavi.put(iNav, "jobapplicationflowcredentials.do?jobId="+iJobId+"&ct="+candidateType+"");
		}
		if(isProfessionalG4)
		{
			iNav++;
			mapNAMENavi.put(iNav, "DSPQlblProfessional");
			mapShortNAMENavi.put(iNav, "PR");
			mapURLNavi.put(iNav, "jobapplicationflowprofessional.do?jobId="+iJobId+"&ct="+candidateType+"");
		}
		if(isAffiliateG4)
		{
			iNav++;
			mapNAMENavi.put(iNav, "DSPQlblAffidavit");
			mapShortNAMENavi.put(iNav, "AF");
			mapURLNavi.put(iNav, "jobapplicationflowAffidavit.do?jobId="+iJobId+"&ct="+candidateType+"");
		}
		if(isQQRequired)
		{
			iNav++;
			mapNAMENavi.put(iNav, "DSPQlblPreScreen");
			mapShortNAMENavi.put(iNav, "QQ");
			mapURLNavi.put(iNav, "jobapplicationflowprescreen.do?jobId="+iJobId+"&ct="+candidateType+"");
		}
		if(isEPIRequired)
		{
			iNav++;
			mapNAMENavi.put(iNav, "DSPQlblEPI");
			mapShortNAMENavi.put(iNav, "EPI");
			mapURLNavi.put(iNav, "jobapplicationflowepi.do?jobId="+iJobId+"&ct="+candidateType+"");
		}
		if(isJSIRequired)
		{
			iNav++;
			mapNAMENavi.put(iNav, "DSPQlblQuestions");
			mapShortNAMENavi.put(iNav, "JSI");
			mapURLNavi.put(iNav, "jobapplicationflowcustomquestion.do?jobId="+iJobId+"&ct="+candidateType+"");
		}
		if(isCompleted)
		{
			iNav++;
			mapNAMENavi.put(iNav, "DSPQlblCompleted");
			mapShortNAMENavi.put(iNav, "Comp");
			mapURLNavi.put(iNav, "jobapplicationflowcompleted.do?jobId="+iJobId+"&ct="+candidateType+"");
		}
			
		String margin="";
		String margin_left="'margin-left:0px ;'";
		
		sb.append("<div class='stepDSPQ1 row' style='margin-left:0px;'>");
		printdata("mapShortNAMENavi size ---------------------------- "+mapShortNAMENavi.size());
		sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;padding-right:0px;height:40px;'>");
		
		String sSubDivFL="";
		float divWidth=0;
		if(iNav == 1)
		{
			divWidth=100;
		}
		else if(iNav >1)
		{
			divWidth=(float) (100.0/(2*(iNav-1)));
			sb.append("<style>.subDivFL{float:left;width:"+divWidth+"%;}</style>");
			sSubDivFL="subDivFL";
			sb.append("<style>.subDiv{float:left;width:"+2*divWidth+"%;}</style>");
		}
		
		if(mapShortNAMENavi.size()==1)
		{
			
		}
		
		for(int i=1; i<=iNav; i++)
		{
			String sShortName=mapShortNAMENavi.get(i);
			String sURL=mapURLNavi.get(i);
			String sLableName=mapNAMENavi.get(i);
			
			printdata("sShortName "+sShortName);
			printdata("sURL "+sURL);
			printdata("sLableName "+sLableName);
			
			if(i==1)
			{
				margin = "float:left;width:"+divWidth+"%";
				margin_left="width:63%;margin-left: 0px;";
				
				if(iNav==1)
					margin_left="width:36%;margin-left: 0px;";
				else if(iNav==2)
					margin_left="width:7%;margin-left: 0px;";
				else if(iNav==3)
					margin_left="width:14%;margin-left: 0px;";
				else if(iNav==4)
					margin_left="width:21%;margin-left: 0px;";
				else if(iNav==5)
					margin_left="width:28%;margin-left: 0px;";
				else if(iNav==6)
					margin_left="width:36%;margin-left: 0px;";
				else if(iNav==7)
					margin_left="width:43%;margin-left: 0px;";
				else if(iNav==8)
					margin_left="width:50%;margin-left: 0px;";
				else if(iNav==9)
					margin_left="width:58%;margin-left: 0px;";
				else if(iNav==10)
					margin_left="width:65%;margin-left: 0px;";
				
				if(sShortName.equalsIgnoreCase("CL"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getCoverLetter()!=null && dspqJobWiseStatus.getCoverLetter())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','CL');\"></div> <span style='width: 70px;'>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','CL');\"></div> <span style='width: 70px;'>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("PI"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getPersonaInfo()!=null && dspqJobWiseStatus.getPersonaInfo())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','PI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','PI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("AC"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getAcademic()!=null && dspqJobWiseStatus.getAcademic())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','AC');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','AC');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("CR"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getCredentials()!=null && dspqJobWiseStatus.getCredentials())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"',,'CR');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"',,'CR');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("PR"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getProfessional()!=null && dspqJobWiseStatus.getProfessional())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','PR');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','PR');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("AF"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getAffidavite()!=null && dspqJobWiseStatus.getAffidavite())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','AF');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','AF');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("QQ"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getPreScreen()!=null && dspqJobWiseStatus.getPreScreen())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','QQ');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','QQ');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("EPI"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getEPI()!=null && dspqJobWiseStatus.getEPI())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','EPI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','EPI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("JSI"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getJSI()!=null && dspqJobWiseStatus.getJSI())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','JSI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','JSI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("Comp"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getJobComplete()!=null && dspqJobWiseStatus.getJobComplete())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','Comp');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align:left;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','Comp');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
			}
			else if(i==iNav)
			{
				margin = "float:left;width:"+divWidth+"%";
				margin_left="margin-left: 19px;";
				
				if(iNav==1)
					margin_left="margin-left: 62px;";
				else if(iNav==2)
					margin_left="margin-left: 438px;";
				else if(iNav==3)
					margin_left="margin-left: 203px;";
				else if(iNav==4)
					margin_left="margin-left: 124px;";
				else if(iNav==5)
					margin_left="margin-left: 86px;";
				else if(iNav==6)
					margin_left="margin-left: 62px;";
				else if(iNav==7)
					margin_left="margin-left: 46px;";
				else if(iNav==8)
					margin_left="margin-left: 35px;";
				else if(iNav==9)
					margin_left="margin-left: 27px;";
				else if(iNav==10)
					margin_left="margin-left: 21px;";
				
				
				if(sShortName.equalsIgnoreCase("CL"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getCoverLetter()!=null && dspqJobWiseStatus.getCoverLetter())
						sb.append("<div  class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','CL');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div  class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','CL');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("PI"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getPersonaInfo()!=null && dspqJobWiseStatus.getPersonaInfo())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','PI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','PI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("AC"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getAcademic()!=null && dspqJobWiseStatus.getAcademic())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','AC');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','AC');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("CR"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getCredentials()!=null && dspqJobWiseStatus.getCredentials())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','CR');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','CR');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("PR"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getProfessional()!=null && dspqJobWiseStatus.getProfessional())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','PR');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','PR');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("AF"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getAffidavite()!=null && dspqJobWiseStatus.getAffidavite())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','AF');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','AF');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("QQ"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getPreScreen()!=null && dspqJobWiseStatus.getPreScreen())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','QQ');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','QQ');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("EPI"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getEPI()!=null && dspqJobWiseStatus.getEPI())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','EPI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','EPI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("JSI"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getJSI()!=null && dspqJobWiseStatus.getJSI())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','JSI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','JSI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("Comp"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getJobComplete()!=null && dspqJobWiseStatus.getJobComplete())
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','Comp');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='"+sSubDivFL+" stepline' style='text-align: right;'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','Comp');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
			}
			else
			{
				String JSICss="";
				if(iNav>=9)
				{
					JSICss="style='margin-left:-10px;width: 110px;'";
				}
				
				
				margin = "float:left;width:"+divWidth+"%";
				margin_left="margin-right: -3px;";
				
				if(sShortName.equalsIgnoreCase("CL"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getCoverLetter()!=null && dspqJobWiseStatus.getCoverLetter())
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','CL');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','CL');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("PI"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getPersonaInfo()!=null && dspqJobWiseStatus.getPersonaInfo())
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','PI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','PI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("AC"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getAcademic()!=null && dspqJobWiseStatus.getAcademic())
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','AC');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','AC');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("CR"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getCredentials()!=null && dspqJobWiseStatus.getCredentials())
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','CR');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','CR');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("PR"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getProfessional()!=null && dspqJobWiseStatus.getProfessional())
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','PR');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','PR');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("AF"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getAffidavite()!=null && dspqJobWiseStatus.getAffidavite())
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','AF');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','AF');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("QQ"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getPreScreen()!=null && dspqJobWiseStatus.getPreScreen())
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','QQ');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','QQ');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("EPI"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getEPI()!=null && dspqJobWiseStatus.getEPI())
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','EPI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','EPI');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("JSI"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getJSI()!=null && dspqJobWiseStatus.getJSI())
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','JSI');\"'></div> <span "+JSICss+">"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','JSI');\"'></div> <span "+JSICss+">"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
				else if(sShortName.equalsIgnoreCase("Comp"))
				{
					if(dspqJobWiseStatus!=null && dspqJobWiseStatus.getJobComplete()!=null && dspqJobWiseStatus.getJobComplete())
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepactive' onclick=\"return getCurrentURL('"+sURL+"','Comp');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
					else
						sb.append("<div class='subDiv stepline'><a title='"+Utility.getLocaleValuePropByKey(sLableName, locale)+"' href='"+sURL+"'><div style='cursor:pointer;"+margin_left+"' class='stepinactive' onclick=\"return getCurrentURL('"+sURL+"','Comp');\"'></div> <span>"+Utility.getLocaleValuePropByKey(sLableName, locale)+"</span></a></div>");
				}
			}
		}
		sb.append("</div></div>");

		sb.append("</div>");
		return sb.toString();
	}
	
	@Transactional(readOnly=false)
	public String callNewSelfServiceApplicationFlow(String iJobId,String candidateType)
	{
		printdata("Calling DSPQServiceAjax => callNewSelfServiceApplicationFlow");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		boolean bSelfServiceStatus=false;
		JobOrder jobOrder=null;
		JobCategoryMaster jobCategoryMaster=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
			if(jobOrder!=null )
				jobCategoryMaster=jobOrder.getJobCategoryMaster();
			if(jobOrder!=null && jobOrder.getDistrictMaster()!=null)
				bSelfServiceStatus=getSelfServicePortfolioStatus(jobOrder.getDistrictMaster());
		}
		
		
		
		String sReturnURL="";
		
		boolean isDSPQRequired=false;
		boolean isQQRequired=false;
		
		boolean isInternalTransferCandidate=false;
		if(session.getAttribute("teacherDetail")!=null) 
		{
			String isImCandidate=(String)session.getAttribute("isImCandidate");
			if(isImCandidate!=null && isImCandidate.equalsIgnoreCase("true"))
				isInternalTransferCandidate=true;
		}
		
		boolean isInternalCandidate=false;
		DspqPortfolioName dspqPortfolioNameNew=null;
		List<JobForTeacher> lstJobForTeacher=null;
		
		if(bSelfServiceStatus)
		{
			lstJobForTeacher=jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail, jobOrder);
			if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
			{
				if(lstJobForTeacher.get(0).getIsAffilated()!=null && lstJobForTeacher.get(0).getIsAffilated()==1)
					isInternalCandidate=true;
			}
			candidateType=getCandidateType(isInternalTransferCandidate, isInternalCandidate);
			dspqPortfolioNameNew =getDSPQByJobOrder(jobOrder,candidateType);
		}
		
		if(bSelfServiceStatus && dspqPortfolioNameNew!=null && dspqPortfolioNameNew.getStatus()!=null && dspqPortfolioNameNew.getStatus().equalsIgnoreCase("A"))
		{
			isDSPQRequired=true;
			if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
			{
				if(lstJobForTeacher.get(0).getIsAffilated()!=null && lstJobForTeacher.get(0).getIsAffilated()==1)
					isInternalCandidate=true;
				
					int iUnDoneCounter=0;
					DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
					if(dspqJobWiseStatus!=null)
					{
						if(dspqJobWiseStatus.getIsCoverLetterRequired()!=null && dspqJobWiseStatus.getIsCoverLetterRequired() && (dspqJobWiseStatus.getCoverLetter()==null || !dspqJobWiseStatus.getCoverLetter()) && iUnDoneCounter==0)
							iUnDoneCounter++;
						else if(dspqJobWiseStatus.getIsPersonaInfoRequired()!=null && dspqJobWiseStatus.getIsPersonaInfoRequired() && (dspqJobWiseStatus.getPersonaInfo()==null || !dspqJobWiseStatus.getPersonaInfo()) && iUnDoneCounter==0)
							iUnDoneCounter++;
						else if(dspqJobWiseStatus.getIsAcademicRequired()!=null && dspqJobWiseStatus.getIsAcademicRequired() && (dspqJobWiseStatus.getAcademic()==null || !dspqJobWiseStatus.getAcademic()) && iUnDoneCounter==0)
							iUnDoneCounter++;
						else if(dspqJobWiseStatus.getIsCredentialsRequired()!=null && dspqJobWiseStatus.getIsCredentialsRequired() && (dspqJobWiseStatus.getCredentials()==null || !dspqJobWiseStatus.getCredentials()) && iUnDoneCounter==0)
							iUnDoneCounter++;
						else if(dspqJobWiseStatus.getIsProfessionalRequired()!=null && dspqJobWiseStatus.getIsProfessionalRequired() && (dspqJobWiseStatus.getProfessional()==null || !dspqJobWiseStatus.getProfessional()) && iUnDoneCounter==0)
							iUnDoneCounter++;
						else if(dspqJobWiseStatus.getIsAffidaviteRequired()!=null && dspqJobWiseStatus.getIsAffidaviteRequired() && (dspqJobWiseStatus.getAffidavite()==null || !dspqJobWiseStatus.getAffidavite()) && iUnDoneCounter==0)
							iUnDoneCounter++;
						else if(dspqJobWiseStatus.getIsPreScreenRequired()!=null && dspqJobWiseStatus.getIsPreScreenRequired() && (dspqJobWiseStatus.getPreScreen()==null || !dspqJobWiseStatus.getPreScreen()) && iUnDoneCounter==0)
							iUnDoneCounter++;
						else if(dspqJobWiseStatus.getIsEPIRequired()!=null && dspqJobWiseStatus.getIsEPIRequired() && (dspqJobWiseStatus.getEPI()==null || !dspqJobWiseStatus.getEPI()) && iUnDoneCounter==0)
							iUnDoneCounter++;
						else if(dspqJobWiseStatus.getIsJSIRequired()!=null && dspqJobWiseStatus.getIsJSIRequired() && (dspqJobWiseStatus.getJSI()==null || !dspqJobWiseStatus.getJSI()) && iUnDoneCounter==0)
							iUnDoneCounter++;
						else if((dspqJobWiseStatus.getJobComplete()==null || !dspqJobWiseStatus.getJobComplete()) && iUnDoneCounter==0)
							iUnDoneCounter++;
						
						if(iUnDoneCounter == 0)
						{
							sReturnURL="AlreadyCompleted";
							return sReturnURL;
						}
						
					}

			}
			
			isQQRequired=getPreSreen(jobCategoryMaster, candidateType);
			
			String sEnc_candidateType=Utility.encodeInBase64(candidateType);
			boolean isCoverLetterG1=false;
			boolean isPersonalInformationG1=false;
			boolean isAffiliateG4=false;
			boolean isProfessionalG4=false;
			Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
			Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
			
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, candidateType);
			
			Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
			isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
			isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
			isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
			isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
					
			sReturnURL=getControllerReturnURL(mapGroup, iJobId, sEnc_candidateType, isDSPQRequired,isCoverLetterG1, isPersonalInformationG1, isAffiliateG4, isProfessionalG4, isQQRequired);	
			
			printdata("********************************************************************************");
			printdata(" candidateType "+candidateType+" isDSPQRequired "+isDSPQRequired +" sReturnURL "+sReturnURL);
			printdata("********************************************************************************");
		}
		else if(bSelfServiceStatus)
		{
			sReturnURL="DSPQNC";
		}
		else
		{
			sReturnURL="OldProcess";
			return sReturnURL;
		}
		return sReturnURL;
	}
	
	@Transactional(readOnly=false)
	public String getGroup01Section01(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId,Integer sectionId)
	{
		printdata("Calling DSPQServiceAjax => getGroup01Section01");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		StringBuffer sb=new StringBuffer();
		
		//TeacherPersonalInfo teacherpersonalinfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		printdata("sCandidateTypeDec "+sCandidateTypeDec);
		
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		DspqSectionMaster dspqSectionMaster=dspqSectionMasterDAO.findById(sectionId, false, false);
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		sectionMasters.add(dspqSectionMaster);
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		
		List<DspqRouter> dspqRouterList=new ArrayList<DspqRouter>();
		if(dspqFieldList!=null && dspqFieldList.size()>0)
			dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		
		//printdata("dspqRouterList for Cover Letter "+dspqRouterList.size());
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		JobOrder jobOrder=null;
		int districtId=0;
		
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			if(jobOrder!=null && jobOrder.getDistrictMaster()!=null)
				districtId=jobOrder.getDistrictMaster().getDistrictId();
			
		}
		
		sb.append("<input type='hidden' id='dspqJobId' value='"+iJobId+"'>");
		sb.append("<input type='hidden' id='dspqct' value='"+candidateType+"'>");
		sb.append("<input type='hidden' id='dspqjobflowdistrictId' value='"+districtId+"'>");
		
		int iOtherIdonotWant=0;
		
		if(mapDspqRouterByFID!=null)
		{
			DistrictMaster dspqPotfolioDM=null;
			if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
				dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
			
		DspqRouter dspqRouterSection1=mapDspqRouterBySID.get(1);
		if(dspqRouterSection1!=null)
		{
			String sSectionName="tooltipSection01";
			
			sb.append("<div class='row'><div class='col-sm-12 col-md-12'><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection1, listToolTips)+"</div></div></div>");
			sb.append(getInstructionForSection(dspqRouterSection1));
			
			sb.append("<div class='row'>");//Start :: New Row 01
			
			
			if(mapDspqRouterByFID!=null)
			{
				if(mapDspqRouterByFID.get(1)!=null)
				{
					sb.append("<div class='col-sm-12 col-md-12' id='noCoverLetter'>");//Start :: #1#
					DspqRouter dspqRouter=mapDspqRouterByFID.get(1);
					if(dspqRouter!=null)
					{
						String sInputControlId="CLTypeL1_1";
						String sInputControlName="CLTypeL1";
						
						sb.append("<label class='radio'>"+
						"<input type='radio' value='1' id='"+sInputControlId+"' name='"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter)+" onclick='CL_IdonotWant()'>"+
						"<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong>"+
						"</label>");
					}
					sb.append("</div>");//End :: #1#
				}
				
				if(mapDspqRouterByFID.get(137)!=null)
				{
					sb.append("<div class='col-sm-12 col-md-12' id='addCoverLetter'>");//Start :: #2#
					DspqRouter dspqRouter=mapDspqRouterByFID.get(137);
					if(dspqRouter!=null)
					{
						String sInputControlId="CLTypeL1_2";
						String sInputControlName="CLTypeL1";
						sb.append("<label class='radio'>"+
						"<input type='radio' value='2' id='"+sInputControlId+"' name='"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter)+" onclick='showCLTypOptions()'>"+
						"<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong>"+
						"</label>");
						iOtherIdonotWant=1;
					}
					sb.append("</div>");//End :: #2#
				}
				
				String sInputTypeCtrlIdForFocus="";
				sb.append("<span style='display:none;' id='rdCLTypeOptions'>");// Start Cover letter input show/hide
				if(mapDspqRouterByFID.get(2)!=null)
				{
					sb.append("<div class='col-sm-12 col-md-12' id='rdCLTypeOptions1'>");//Start :: #3#
					DspqRouter dspqRouter=mapDspqRouterByFID.get(2);
					if(dspqRouter!=null)
					{
						String sInputControlId="CLTypeL1_3";
						String sInputControlName="CLTypeL1";
						
						if(sInputTypeCtrlIdForFocus.equals(""))
							sInputTypeCtrlIdForFocus="CLTypeL1_3";
						
						sb.append("<label class='radio'><input type='radio' value='3' id='"+sInputControlId+"' name='"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter)+" onclick='showTypeCLEditor()'> <strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"<div id='divTypeCoverLetter'>"+
						"	<label class='redtextmsg'>"+Utility.getLocaleValuePropByKey("msgHowToCopyPastCutDouc", locale)+"</label>"+
						"	<textarea id='txtAReaCoverLetter' name='txtAReaCoverLetter' rows='7' cols='183' "+getInputCustomeAttribute(dspqRouter)+"></textarea>"+
						"</div>");
					}
					sb.append("</div>");//End :: #3#
				}
				
				
				if(mapDspqRouterByFID.get(3)!=null)
				{
					sb.append("<div class='col-sm-12 col-md-12' id='rdCLTypeOptions2'>");//Start :: #4#
					DspqRouter dspqRouter=mapDspqRouterByFID.get(3);
					if(dspqRouter!=null)
					{
						String sInputControlId="CLTypeL1_4";
						String sInputControlName="CLTypeL1";
						
						if(sInputTypeCtrlIdForFocus.equals(""))
							sInputTypeCtrlIdForFocus="CLTypeL1_4";
						
						sb.append("<label class='radio'>"+
						"<input type='radio' value='4' id='"+sInputControlId+"' name='"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter)+" onclick='setLatestCoverLetter()'>"+
						"<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong>"+
						"</label>");
					}
					sb.append("</div>");//End :: #4#
				}
				
				if(mapDspqRouterByFID.get(138)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(138);
					if(dspqRouter!=null)
					{
						sb.append("<div class='col-sm-12 col-md-12' id='rdCLTypeOptions3'>");//Start :: #5#
						String sInputControlId="CLTypeL1_5";
						String sInputControlName="CLTypeL1";
						
						if(sInputTypeCtrlIdForFocus.equals(""))
							sInputTypeCtrlIdForFocus="CLTypeL1_5";
						
						sb.append("<label class='radio'>"+
						"<input type='radio' value='5' id='"+sInputControlId+"' name='"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter)+" onclick='CLFileUpload()'>"+
						"<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong>"+
						"</label>");
						sb.append("</div>");//End :: #5#
						
						
						//***************** Start ... Cover Letter Upload *******************
						String sCLFileName="";
						List<JobForTeacher> lstJobForTeacher=jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail, jobOrder);
						if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
						{
							if(lstJobForTeacher.get(0).getCoverLetterFileName()!=null && !lstJobForTeacher.get(0).getCoverLetterFileName().equals(""));
								sCLFileName=lstJobForTeacher.get(0).getCoverLetterFileName();
						}
						
						String sInputControlIdCLUpload="clupload";
						sb.append("<span class='hide' id='spncluploadId'><div class='col-sm-12 col-md-12'><div class='divErrorMsg' id='errordiv_clupload' style='display: block;'></div></div>"+
								"<div class='col-sm-3 col-md-3' style='margin-left: 20px;'> <input type='hidden' id='hdnclupload' value='' />");
						
								if(sCLFileName!=null && !sCLFileName.equals("") && !sCLFileName.equals("0"))
									sb.append("	<input type='hidden' id='db_"+sInputControlId+"' name='db_"+sInputControlId+"' value='"+sCLFileName+"'/>");
								else
									sb.append("	<input type='hidden' id='db_"+sInputControlId+"' name='db_"+sInputControlId+"' value='0'/>");
								
								sb.append("<iframe name='uploadCLFrameID' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");
								sb.append("<form id='jafCLUploadfrm' enctype='multipart/form-data' method='post' target='uploadCLFrameID' action='jafCLUploadFileServlet.do'>");
								
								sb.append("<input name='clJobId' id='clJobId' type='hidden' value='"+iJobId+"'>");
								
								sb.append("<input name='"+sInputControlIdCLUpload+"' id='"+sInputControlIdCLUpload+"' "+getInputCustomeAttribute(dspqRouter)+" type='file' onchange='jafUploadCLFile(this.value);'>");
								sb.append("</form>");
								
								sb.append("</div>"+
								"<div class='col-sm-5 col-md-5'> "+
								"<label id='lblclupload'>"+Utility.getLocaleValuePropByKey("DSPQ_lblRecentResOnCL", locale));
								
								if(sCLFileName!=null && !sCLFileName.equals(""))
								{
									String DSPQ_CLUploadtoolView=Utility.getLocaleValuePropByKey("DSPQ_CLUploadtoolView", locale);
									String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
									sb.append("&nbsp;<a href='javascript:void(0)' rel='tooltip' data-original-title='"+DSPQ_CLUploadtoolView+"'  id='hrefToolTioCLUploadFileId' onclick=\"downloadCLUploadFile('hrefToolTioCLUploadFileId');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
								}
								else
								{
									sb.append("&nbsp;None");
								}
								
								sb.append("</label></div></form></span>");
								
						
						//***************** End ... Cover Letter Upload *******************
						
					}
					
				}
				sb.append("</span>");// End Cover letter input show/hide
				
				sb.append("</div>");//End :: New Row 01
				
				sb.append("<input type='hidden' id='txtInputTypeCtrlIdForFocus' value='"+sInputTypeCtrlIdForFocus+"'>");
				
				sb.append("<div class='row' style='margin-top:30px;' id='jeffcoCLDivId'>");//Start :: Jeffco
				sb.append("<div class='col-sm-12 col-md-12'>");
				
				if(mapDspqRouterByFID.get(139)!=null || mapDspqRouterByFID.get(140)!=null || mapDspqRouterByFID.get(141)!=null)
				{
						sb.append("<div class='row'>");
						DspqRouter dspqRouterTmp=null;
						if(mapDspqRouterByFID.get(139)!=null)
							dspqRouterTmp=mapDspqRouterByFID.get(139);
						
						Boolean isRequired=false;
						if(dspqRouterTmp!=null && dspqRouterTmp.getIsRequired()!=null)
							isRequired=dspqRouterTmp.getIsRequired();
						
						DspqRouter dspqRouter139=new DspqRouter();
						dspqRouter139.setIsRequired(isRequired);
						dspqRouter139.setTooltip(Utility.getLocaleValuePropByKey("DSPQ_CL_DOB_ToolTips", locale));
						dspqRouter139.setDisplayName(Utility.getLocaleValuePropByKey("DSPQ_CL_DOB_Lbl", locale));
						
						String sInputControlIdCLDOB="lblCLDObJeffco";
						sb.append("<div class='col-sm-12 col-md-12' >"+
						"<div><label style='width: 200px;'><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlIdCLDOB,dspqRouter139,listToolTips)+"</strong></label></div>"+						
						"</div>");
						sb.append("</div>");
						
						sb.append("<div class='row'>");
						Date dDOB=null;
						int iYear =0;
						int iMonth =0;
						int iDay =0;
						    
						if(teacherPersonalInfo!=null && teacherPersonalInfo.getDob()!=null)
						{
							dDOB=teacherPersonalInfo.getDob();
							Calendar cal = Calendar.getInstance();
							cal.setTime(dDOB);
							iYear = cal.get(Calendar.YEAR);	
							iMonth = cal.get(Calendar.MONTH)+1;
							iDay = cal.get(Calendar.DAY_OF_MONTH);
						}
						//Month
						if(mapDspqRouterByFID.get(139)!=null)
						{
							DspqRouter dspqRouter=mapDspqRouterByFID.get(139);
							if(dspqRouter!=null)
							{
								
								String sInputControlId="dobMonth";
								sb.append("<div class='col-sm-2 col-md-2'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<select class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"' onchange='getDayByMonthNo(this.value);'>"+
								"	<option value='0'>"+Utility.getLocaleValuePropByKey("optStrMonth", locale)+"</option>");
									
								List<String> listMonth=Utility.getMonthList();
								int i=0;
								for(String strMonthName:listMonth)
								{
									i++;
									if(iMonth==i)
										sb.append("<option value='"+i+"' selected>"+strMonthName+"</option>");
									else
										sb.append("<option value='"+i+"'>"+strMonthName+"</option>");
								}
								sb.append("</select></div>");
								
							}
						}
						
						//Day
						if(mapDspqRouterByFID.get(140)!=null)
						{
							DspqRouter dspqRouter=mapDspqRouterByFID.get(140);
							if(dspqRouter!=null)
							{
								String sInputControlId="dobDay";
								sb.append("<div class='col-sm-2 col-md-2'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<select class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"'>"+
								"	<option value='0'>"+Utility.getLocaleValuePropByKey("lblSelectDay", locale)+"</option>");
									
								List<String> listDay=Utility.getDays();
								int i=0;
								for(String strday:listDay)
								{
									i++;
									if(iDay==i)
										sb.append("<option value='"+i+"' selected>"+strday+"</option>");
									else
										sb.append("<option value='"+i+"'>"+strday+"</option>");
								}
								sb.append("</select></div>");
							}
						}
						
						//Year
						if(mapDspqRouterByFID.get(141)!=null)
						{
							DspqRouter dspqRouter=mapDspqRouterByFID.get(141);
							if(dspqRouter!=null)
							{
								String sInputControlId="dobYear";
								
								sb.append("<div class='col-sm-2 col-md-2'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' id='"+sInputControlId+"' value="+iYear+" "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"' class='form-control' maxlength='4' onkeypress='return checkForInt(event);' onblur='return validateDOBYearForDSPQ();' />");
								sb.append("</div>");
								
								sb.append("<input type='hidden' id='dob' name='dob'/>");
							}
						}
						sb.append("</div>");
						
				}
				
				//SSN
				if(mapDspqRouterByFID.get(133)!=null)
				{
					sb.append("<div class='row'>");
					DspqRouter dspqRouter=mapDspqRouterByFID.get(133);
					if(dspqRouter!=null)
					{
						
						String sSSN="";
						if(teacherPersonalInfo!=null && teacherPersonalInfo.getSSN()!=null && !teacherPersonalInfo.getSSN().equals(""))
							sSSN=teacherPersonalInfo.getSSN();
						if(sSSN!=null && !sSSN.equals(""))
						{
							try {
								sSSN=Utility.decodeBase64(sSSN);
								if(sSSN.length()>4)
									sSSN=sSSN.substring(sSSN.length()-4);
								
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						
						String sInputControlId="CL_SSNLast4";
						sb.append("<div class='col-sm-2 col-md-2' >"+
						"<div>"+
							"<label style='width: 200px;'><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
							"<input type='text' id='"+sInputControlId+"' value='"+sSSN+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' maxlength='4' onkeypress='return checkForInt(event);' onclick='chkDistrict();'/>"+
						"</div>"+						
						"</div>");
					}
					sb.append("</div>");
				}
				
				
				if(districtId==804800)
				{
					sb.append("<input type='hidden' id='txtHdnFld_Jeffco_staffType' value=''>");
					sb.append("<input type='hidden' id='txtHdnFld_Jeffco_isAffilated' value='0'>");
					sb.append("<input type='hidden' id='txtHdnFld_Jeffco_empNumCoverLtr' value=''>");
				}
				
				sb.append("</div>");
				sb.append("</div>");//End :: Jeffco
				
				sb.append("<div class='row' style='margin-top:30px;'>");//Start :: New Row 02
				
				if(mapDspqRouterByFID.get(4)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(4);
					if(dspqRouter!=null)
					{
						sb.append("<div class='col-sm-12 col-md-12'>");//Start :: #6.1#
						String sInputControlId="isAffilated";
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						sb.append("<label class='checkbox'>");
						sb.append("<input type='checkbox' value='1' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" onclick='validateIsAffilated()'>");
						
						sb.append("<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong>"+
						"</label>");
						sb.append("</div>");//End :: #6.1#
					}
					
					if(mapDspqRouterByFID.get(135)!=null || mapDspqRouterByFID.get(136)!=null)
					{
						sb.append("<div class='row' style='margin-left:20px;'>");
						//Employee Number
						if(mapDspqRouterByFID.get(136)!=null)
						{
							DspqRouter dspqRouter136=mapDspqRouterByFID.get(136);
							if(dspqRouter136!=null)
							{
								
								
								String empNumCoverLtrValue="";
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeNumber()!=null && !teacherPersonalInfo.getEmployeeNumber().equals(""))
									empNumCoverLtrValue=teacherPersonalInfo.getEmployeeNumber();
								
								String sInputControlId="empNumCoverLtr";
								sb.append("<div class='col-sm-3 col-md-3' id='divEmpNumCoverLtr' style='display:none'>"+//Start :: #6.2.1#
										"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter136,listToolTips)+"</strong></label>"+
										"<input type='text' autocomplete='off' id='"+sInputControlId+"' class='form-control' value='"+empNumCoverLtrValue+"' "+getInputCustomeAttribute(dspqRouter136)+" maxlength='50' />"+
								"</div>");//End :: #6.2.1#
							}
						}
						
						//Zip Code
						if(mapDspqRouterByFID.get(135)!=null)
						{
							DspqRouter dspqRouter135=mapDspqRouterByFID.get(135);
							if(dspqRouter135!=null)
							{
								String zipCodeCoverLtrValue="";
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getZipCode()!=null && !teacherPersonalInfo.getZipCode().equals(""))
									zipCodeCoverLtrValue=teacherPersonalInfo.getZipCode();
								
								String sInputControlId="zipCodeCoverLtr";
								sb.append("<div class='col-sm-3 col-md-3' id='divZipCodeCoverLtr' style='display:none'>"+//Start :: #6.2.2#
										"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter135,listToolTips)+"</strong></label>"+
										"<input type='text' autocomplete='off' id='"+sInputControlId+"' class='form-control' value='"+zipCodeCoverLtrValue+"' "+getInputCustomeAttribute(dspqRouter135)+" maxlength='10' />"+
								"</div>");//End :: #6.2.2#
							}
						}
						sb.append("</div>");
					}
					
					if(mapDspqRouterByFID.get(111)!=null || mapDspqRouterByFID.get(112)!=null || mapDspqRouterByFID.get(113)!=null)
					{
						String sInputControlName="staffType";
						boolean bStaffTypehdn=false;
						if(mapDspqRouterByFID.get(111)!=null)
						{
							DspqRouter dspqRouter111=mapDspqRouterByFID.get(111);
							if(dspqRouter111!=null)
							{
								
								sb.append("<div class='col-sm-12 col-md-12' style='margin-left:20px;'>");//Start :: #6.3#
								String sInputControlId="staffType_I";
								if(!bStaffTypehdn)
								{
									sb.append("<input type='hidden' id='hdn_"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter111)+" >");
									bStaffTypehdn=true;
								}
								
								sb.append("<label class='radio' id='inst' style='display:none'>"+
								"<input type='radio' value='I' id='"+sInputControlId+"' name='"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter)+">"+
								"<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter111,listToolTips)+"</strong>"+
								"</label>");
								sb.append("</div>");//End :: #6.3#
							}
						}
							
						if(mapDspqRouterByFID.get(112)!=null)
						{
							DspqRouter dspqRouter112=mapDspqRouterByFID.get(112);
							if(dspqRouter112!=null)
							{
								if(!bStaffTypehdn)
								{
									sb.append("<input type='hidden' id='hdn_"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter112)+" >");
									bStaffTypehdn=true;
								}
								
								sb.append("<div class='col-sm-12 col-md-12' style='margin-left:20px;'>");//Start :: #6.4#
								String sInputControlId="staffType_PI";
								//String sInputControlName="staffType";
								sb.append("<label class='radio' id='partinst' style='display:none'>"+
								"<input type='radio' value='PI' id='"+sInputControlId+"' name='"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter)+">"+
								"<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter112,listToolTips)+"</strong>"+
								"</label>");
								sb.append("</div>");//End :: #6.4#
							}
						}
							
						if(mapDspqRouterByFID.get(113)!=null)
						{
							DspqRouter dspqRouter113=mapDspqRouterByFID.get(113);
							if(dspqRouter113!=null)
							{
								if(!bStaffTypehdn)
								{
									sb.append("<input type='hidden' id='hdn_"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter113)+" >");
									bStaffTypehdn=true;
								}
								
								sb.append("<div class='col-sm-12 col-md-12' style='margin-left:20px;'>");//Start :: #6.5#
								String sInputControlId="staffTyp_Ne";
								//String sInputControlName="staffType";
								sb.append("<label class='radio' id='noninst' style='display:none'>"+
								"<input type='radio' value='N' id='"+sInputControlId+"' name='"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter)+">"+
								"<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter113,listToolTips)+"</strong>"+
								"</label>");
								sb.append("</div>");//End :: #6.5#
							}
						}
					}
				}
				sb.append("</div>");//End :: New Row 02
			}
			sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection1.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
		}
	}
		
		sb.append("<input type='hidden' id='txtOtherIdonotWant' value='"+iOtherIdonotWant+"'>");
		
		sb.append("<div class='row'>" +
				"<div class='col-sm-9 col-md-9'><div class='mt10'><button class='flatbtn' style='background:#F89507;width:20%'  type='button' onclick='return validateDSPQPersonalGroup01Section01(1);'>"+Utility.getLocaleValuePropByKey("btnSaveExit", locale)+" <i class='fa fa-check-circle'></i></button>" +
				"&nbsp;&nbsp;<button class='flatbtn' style='background: #da4f49;width:16%;'  type='button' onclick='return cancelApplyJob();'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+" <i class='fa fa-times-circle'></i></button>" +
				"&nbsp;&nbsp;<button class='flatbtn' style='width:27%;'  type='button' onclick='return oneBtnJobApply();'>"+Utility.getLocaleValuePropByKey("DSPQ_btnOneBtnJobApply", locale)+" <i class='fa fa-plane'></i></button>" +
				"</div></div>" +
				"<div class='col-sm-3 col-md-3'><div class='mt10' style='text-align:right;'><button class='flatbtn' style='width:90%;'  type='button' onclick='return validateDSPQPersonalGroup01Section01(0);'>"+Utility.getLocaleValuePropByKey("btnSaveAndNextDSPQ", locale)+" <i class='icon'></i></button></div></div>" +
				"</div>");
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
		
	}
	
	@Transactional(readOnly=false)
	public String getGroup01(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup01");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		StringBuffer sb=new StringBuffer();
		
		TeacherPersonalInfo teacherpersonalinfo = teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
		
		JobOrder jobOrder=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
		}
		
		List<StateMaster> listStateMaster = null;
		List<CityMaster> listCityMasters=null;
		if(teacherpersonalinfo!=null)
		{
			if(teacherpersonalinfo.getCountryId()!=null)
				listStateMaster=stateMasterDAO.findActiveStateByCountryId(teacherpersonalinfo.getCountryId());
			if(teacherpersonalinfo.getStateId()!=null)
				listCityMasters = cityMasterDAO.findCityByState(teacherpersonalinfo.getStateId());
		}
			
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		List<RaceMaster> listRaceMasters = raceMasterDAO.findAllRaceByOrder();
		List<EthinicityMaster> lstEthinicityMasters=ethinicityMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
		List<EthnicOriginMaster> lstethnicOriginMasters=ethnicOriginMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
		List<GenderMaster> lstGenderMasters  = genderMasterDAO.findAllGenderByOrder();
		
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		List<DspqSectionMaster> sectionMasters=dspqSectionMasterDAO.getDspqSectionListByGroup(dspqGroupMaster);
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		if(mapDspqRouterByFID!=null)
		{
			
			DistrictMaster dspqPotfolioDM=null;
			if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
				dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
			
			DspqRouter dspqRouterSection2=mapDspqRouterBySID.get(2);
			if(dspqRouterSection2!=null)
			{
				
				String sSectionName="tooltipSection02";
				sb.append("<div class='row'><div class='col-sm-12 col-md-12'><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection2, listToolTips)+"</div></div></div>");
				sb.append(getInstructionForSection(dspqRouterSection2));
				sb.append("<div class='row'>");
				
				if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(804800))
				{
					if(teacherpersonalinfo!=null)
					{
						if(teacherpersonalinfo.getEmployeeNumber()!=null && !teacherpersonalinfo.getEmployeeNumber().equals(""))
						{
							sb.append("<input type='hidden' id='jeffcoEmpNo' value='"+teacherpersonalinfo.getEmployeeNumber()+"'>");
						}
						else
						{
							sb.append("<input type='hidden' id='jeffcoEmpNo' value=''>");
						}
						
						if(teacherpersonalinfo.getZipCode()!=null && !teacherpersonalinfo.getZipCode().equals(""))
						{
							sb.append("<input type='hidden' id='jeffcoZipCode' value='"+teacherpersonalinfo.getZipCode()+"'>");
						}
						else
						{
							sb.append("<input type='hidden' id='jeffcoZipCode' value=''>");
						}						
					}
					
					sb.append("<div class='col-sm-12 col-md-12' id='jeFFcoEmployeeInf' style='padding-left:0px;margin-left: -10px;'></div>");
					
					sb.append("<div class='col-sm-12 col-md-12'>&nbsp;</div>");
					
				}
				
				//Solutation
				if(mapDspqRouterByFID.get(5)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(5);
					if(dspqRouter!=null)
					{
						Integer iSalutation=0;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getSalutation()!=null)
							iSalutation=teacherpersonalinfo.getSalutation();
					
						String sInputControlId="salutation_pi";
						sb.append("<div class='col-sm-2 col-md-2'>"+
							"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+					
							"<select id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' >"+
							"<option value='0'></option>");
						
							if(iSalutation==4)
								sb.append("<option selected value='4'>"+Utility.getLocaleValuePropByKey("optDr", locale)+"</option>");
							else
								sb.append("<option value='4'>"+Utility.getLocaleValuePropByKey("optDr", locale)+"</option>");
							
							if(iSalutation==3)
								sb.append("<option selected value='3'>"+Utility.getLocaleValuePropByKey("optMiss", locale)+"</option>");
							else
								sb.append("<option value='3'>"+Utility.getLocaleValuePropByKey("optMiss", locale)+"</option>");
							
							if(iSalutation==2)
								sb.append("<option selected value='2'>"+Utility.getLocaleValuePropByKey("optMr", locale)+"</option>");
							else
								sb.append("<option value='2'>"+Utility.getLocaleValuePropByKey("optMr", locale)+"</option>");
							
							if(iSalutation==1)
								sb.append("<option selected value='1'>"+Utility.getLocaleValuePropByKey("optMrs", locale)+"</option>");
							else
								sb.append("<option value='1'>"+Utility.getLocaleValuePropByKey("optMrs", locale)+"</option>");
							
							if(iSalutation==5)
								sb.append("<option selected value='5'>"+Utility.getLocaleValuePropByKey("optMs", locale)+"</option>");
							else
								sb.append("<option value='5'>"+Utility.getLocaleValuePropByKey("optMs", locale)+"</option>");
							
							sb.append("</select>"+
						"</div>");
					}
				}
				
				//First Name
				if(mapDspqRouterByFID.get(6)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(6);
					if(dspqRouter!=null)
					{
						
						String sFirstName="";
						if(teacherDetail!=null && teacherDetail.getFirstName()!=null)
							sFirstName=teacherDetail.getFirstName();
							
						String sInputControlId="firstName_pi";
						sb.append("<div class='col-sm-2 col-md-2'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' id='"+sInputControlId+"' class='form-control' value='"+sFirstName+"' "+getInputCustomeAttribute(dspqRouter)+" maxlength='50'/>"+
						"</div>");
					}
				}
				
				//Midle Name
				if(mapDspqRouterByFID.get(79)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(79);
					if(dspqRouter!=null)
					{
						String sMiddleName="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getMiddleName()!=null)
							sMiddleName=teacherpersonalinfo.getMiddleName();
						
						String sInputControlId="middleName_pi";
						sb.append("<div class='col-sm-2 col-md-2'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' id='"+sInputControlId+"' value='"+sMiddleName+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' maxlength='50'/>"+
						"</div>");
					}
				}
				
				//Last Name
				if(mapDspqRouterByFID.get(7)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(7);
					if(dspqRouter!=null)
					{
						String sLastName="";
						if(teacherDetail!=null && teacherDetail.getLastName()!=null)
							sLastName=teacherDetail.getLastName();
						
						String sInputControlId="lastName_pi";
						sb.append("<div class='col-sm-2 col-md-2'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' id='"+sInputControlId+"' value='"+sLastName+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' maxlength='50'/>"+
						"</div>");
					}
				}
				
				//Another Name
				if(mapDspqRouterByFID.get(80)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(80);
					if(dspqRouter!=null)
					{
						String sAnotherName="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getAnotherName()!=null)
							sAnotherName=teacherpersonalinfo.getAnotherName();
						
						String sInputControlId="anotherName";
						sb.append("<div class='col-sm-4 col-md-4'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' id='"+sInputControlId+"' value='"+sAnotherName+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control'/>"+
						"</div>");
					}
				}
				
				//SSN
				if(mapDspqRouterByFID.get(81)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(81);
					if(dspqRouter!=null)
					{
						
						String sSSN="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getSSN()!=null && !teacherpersonalinfo.getSSN().equals(""))
							sSSN=teacherpersonalinfo.getSSN();
						if(sSSN!=null && !sSSN.equals(""))
						{
							try {
								sSSN=Utility.decodeBase64(sSSN);
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						
						String sInputControlId="ssn_pi";
						sb.append("<div class='col-sm-2 col-md-2' >"+
						"<div>"+
							"<label style='width: 200px;'><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
							"<input type='text' id='"+sInputControlId+"' value='"+sSSN+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' maxlength='9' onkeypress='return checkForInt(event);' onclick='chkDistrict();'/>"+
						"</div>"+						
						"</div>");
					}
				}
				
				//expectedSalary
				if(mapDspqRouterByFID.get(19)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(19);
					if(dspqRouter!=null)
					{
						Integer iExpectedSalary=0;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getExpectedSalary()!=null)
							iExpectedSalary=teacherpersonalinfo.getExpectedSalary();
						
						
						String sInputControlId="expectedSalary";
						sb.append("<div class='col-sm-2 col-md-2'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon("iconpophoverexpectedSalary",dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' name='"+sInputControlId+"' value="+iExpectedSalary+" id='"+sInputControlId+"' onkeypress='return checkForInt(event);' class='form-control' maxlength='20'/>"+
						"</div>");
					}
				}
				
				
				if(mapDspqRouterByFID.get(96)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(96);
					if(dspqRouter!=null)
					{
						
						String sPhoneNumber="";
						String sPhoneNumber1="";
						String sPhoneNumber2="";
						String sPhoneNumber3="";
						
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getPhoneNumber()!=null)
						{
							sPhoneNumber=teacherpersonalinfo.getPhoneNumber();
							if(sPhoneNumber!=null && !sPhoneNumber.equals(""))
							{
								if(sPhoneNumber.length()==10 || sPhoneNumber.length()==12)
								{
									String arrPh[]=sPhoneNumber.split("-");
									if(arrPh.length==3)
									{
										sPhoneNumber1=arrPh[0];
										sPhoneNumber2=arrPh[1];
										sPhoneNumber3=arrPh[2];
									}
									else
									{
										sPhoneNumber1=sPhoneNumber.substring(0, 3);
										sPhoneNumber2=sPhoneNumber.substring(3, 6);
										sPhoneNumber3=sPhoneNumber.substring(6, 10);
									}
								}
								
							}
						}
						
						String sInputControlId="phoneNumber";
						String sInputControlId1="phoneNumber1";
						String sInputControlId2="phoneNumber2";
						String sInputControlId3="phoneNumber3";
						
						sb.append("<div class='col-sm-3 col-md-3'>"+				
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<div>"+
									"<div style='float: left;'><input type='text' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId1+"' id='"+sInputControlId1+"' class='form-control' maxlength='3'  value='"+sPhoneNumber1+"' onkeypress='return checkForInt(event);' style='width:56px; padding-left:10px;'/></div><div style='float: left;color: #cccccc;margin-left: 4px;margin-top: 6px'>-</div>"+
									"<div style='float: left;margin-left: 5px;'><input type='text' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId2+"' id='"+sInputControlId2+"' class='form-control' maxlength='3'  value='"+sPhoneNumber2+"' onkeypress='return checkForInt(event);' style='width:56px; padding-left:10px;'/></div><div style='float: left;color: #cccccc;margin-left: 4px;margin-top: 6px'>-</div>"+
									"<div style='float: left;margin-left: 5px;'><input type='text' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId3+"' id='"+sInputControlId3+"' class='form-control' maxlength='4'  value='"+sPhoneNumber3+"' style='width:66px; padding-left:10px;'' onkeypress='return checkForInt(event);'/></div>"+
								"</div>"+
							"</div>");
					}
				}
				
				
				if(mapDspqRouterByFID.get(18)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(18);
					if(dspqRouter!=null)
					{
						String sMobileNumber="";
						String sMobileNumber1="";
						String sMobileNumber2="";
						String sMobileNumber3="";
						
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getMobileNumber()!=null)
						{
							sMobileNumber=teacherpersonalinfo.getMobileNumber();
							if(sMobileNumber!=null && !sMobileNumber.equals(""))
							{
								if(sMobileNumber.length()==10 || sMobileNumber.length()==12)
								{
									String arrMobile[]=sMobileNumber.split("-");
									if(arrMobile.length==3)
									{
										sMobileNumber1=arrMobile[0];
										sMobileNumber2=arrMobile[1];
										sMobileNumber3=arrMobile[2];
									}
									else
									{
										sMobileNumber1=sMobileNumber.substring(0, 3);
										sMobileNumber2=sMobileNumber.substring(3, 6);
										sMobileNumber3=sMobileNumber.substring(6, 10);
									}
								}
								
							}
						}
						
						String sInputControlId="mobileNumber";
						String sInputControlId1="mobileNumber1";
						String sInputControlId2="mobileNumber2";
						String sInputControlId3="mobileNumber3";
						
						sb.append("<div class='col-sm-3 col-md-3'>"+				
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<div>"+
									"<div style='float: left;'><input type='text' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId1+"' id='"+sInputControlId1+"' class='form-control' maxlength='3'  value='"+sMobileNumber1+"' onkeypress='return checkForInt(event);' style='width:56px; padding-left:10px;'/></div><div style='float: left;color: #cccccc;margin-left: 4px;margin-top: 6px'>-</div>"+
									"<div style='float: left;margin-left: 5px;'><input type='text' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId2+"' id='"+sInputControlId2+"' class='form-control' maxlength='3'  value='"+sMobileNumber2+"' onkeypress='return checkForInt(event);' style='width:56px; padding-left:10px;'/></div><div style='float: left;color: #cccccc;margin-left: 4px;margin-top: 6px'>-</div>"+
									"<div style='float: left;margin-left: 5px;'><input type='text' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId3+"' id='"+sInputControlId3+"' class='form-control' maxlength='4'  value='"+sMobileNumber3+"' style='width:66px; padding-left:10px;'' onkeypress='return checkForInt(event);'/></div>"+
								"</div>"+
							"</div>");
					}
				}
				
				sb.append("</div>");
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection2.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
			
			DspqRouter dspqRouterSection16=mapDspqRouterBySID.get(16);
			if(dspqRouterSection16!=null)
			{
				String sSectionName="tooltipSection15";
				sb.append("<div class='row'>");
				sb.append("<div class='col-sm-12 col-md-12' ><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection16, listToolTips)+"</div></div>");
				sb.append("</div>");
				
				sb.append(getInstructionForSection(dspqRouterSection16));
				
				sb.append("<div class='row'>");
				Date dDOB=null;
				int iYear =0;
				int iMonth =0;
				int iDay =0;
				    
				if(teacherpersonalinfo!=null && teacherpersonalinfo.getDob()!=null)
				{
					dDOB=teacherpersonalinfo.getDob();
					Calendar cal = Calendar.getInstance();
					cal.setTime(dDOB);
					iYear = cal.get(Calendar.YEAR);	
					iMonth = cal.get(Calendar.MONTH)+1;
					iDay = cal.get(Calendar.DAY_OF_MONTH);
				}
				//Month
				if(mapDspqRouterByFID.get(84)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(84);
					if(dspqRouter!=null)
					{
						
						String sInputControlId="dobMonth";
						sb.append("<div class='col-sm-2 col-md-2'>"+
						"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"<select class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"'>"+
						"	<option value='0'>"+Utility.getLocaleValuePropByKey("optStrMonth", locale)+"</option>");
							
						List<String> listMonth=Utility.getMonthList();
						int i=0;
						for(String strMonthName:listMonth)
						{
							i++;
							if(iMonth==i)
								sb.append("<option value='"+i+"' selected>"+strMonthName+"</option>");
							else
								sb.append("<option value='"+i+"'>"+strMonthName+"</option>");
						}
						sb.append("</select></div>");
					}
				}
				
				//Day
				if(mapDspqRouterByFID.get(85)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(85);
					if(dspqRouter!=null)
					{
						String sInputControlId="dobDay";
						sb.append("<div class='col-sm-2 col-md-2'>"+
						"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"<select class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"'>"+
						"	<option value='0'>"+Utility.getLocaleValuePropByKey("lblSelectDay", locale)+"</option>");
							
						List<String> listDay=Utility.getDays();
						int i=0;
						for(String strday:listDay)
						{
							i++;
							if(iDay==i)
								sb.append("<option value='"+i+"' selected>"+strday+"</option>");
							else
								sb.append("<option value='"+i+"'>"+strday+"</option>");
						}
						sb.append("</select></div>");
					}
				}
				
				//Year
				if(mapDspqRouterByFID.get(86)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(86);
					if(dspqRouter!=null)
					{
						String sInputControlId="dobYear";
						
						sb.append("<div class='col-sm-2 col-md-2'>"+
						"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"<input type='text' id='"+sInputControlId+"' value="+iYear+" "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"' class='form-control' maxlength='4' onkeypress='return checkForInt(event);' onblur='return validateDOBYearForDSPQ();' />");
						sb.append("</div>");
						
						sb.append("<input type='hidden' id='dob' name='dob'/>");
					}
				}
				sb.append("</div>");
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM,sCandidateTypeDec, dspqGroupMaster, dspqRouterSection16.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
			
			DspqRouter dspqRouterSection4=mapDspqRouterBySID.get(4);
			if(dspqRouterSection4!=null)
			{
				String sSectionName="tooltipSection4";
				sb.append("<div class='row'>");
				sb.append("<div class='col-sm-12 col-md-12'><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection4, listToolTips)+"</div></div>");
				sb.append("</div>");
				
				sb.append(getInstructionForSection(dspqRouterSection4));
				
				sb.append("<div class='row'>");
				//AddressLine1
				if(mapDspqRouterByFID.get(12)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(12);
					if(dspqRouter!=null)
					{
						
						String sAddressLine1="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getAddressLine1()!=null)
							sAddressLine1=teacherpersonalinfo.getAddressLine1();
						
						String sInputControlId="addressLine1";
						sb.append("<div class='col-sm-6 col-md-6'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' id='"+sInputControlId+"' value='"+sAddressLine1+"' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"' maxlength='50' class='form-control'/>"+
						"</div>");
					}
				}
				
				//AddressLine2
				if(mapDspqRouterByFID.get(13)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(13);
					if(dspqRouter!=null)
					{
						
						String sAddressLine2="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getAddressLine2()!=null)
							sAddressLine2=teacherpersonalinfo.getAddressLine2();
						
						String sInputControlId="addressLine2";
						sb.append("<div class='col-sm-6 col-md-6'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' id='"+sInputControlId+"' value='"+sAddressLine2+"' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"' maxlength='50' class='form-control'/>"+
						"</div>");
					}
				}
				
				//country
				if(mapDspqRouterByFID.get(14)!=null)
				{
					List<CountryMaster> countryMasters=countryMasterDAO.findAllActiveCountry();
					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(14);
					if(dspqRouter!=null)
					{
						Integer iCountryId=0;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getCountryId()!=null)
							iCountryId=teacherpersonalinfo.getCountryId().getCountryId();
						
						String sInputControlId="countryId";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<select id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' onchange=\"getStateByCountryForDspq('dspq');\">"+
								"	<option value='0'>"+Utility.getLocaleValuePropByKey("lblSelectCountry", locale)+"</option>");
								for(CountryMaster countryMaster:countryMasters)
								{
									if(iCountryId==countryMaster.getCountryId())
										sb.append("<option selected value='"+countryMaster.getCountryId()+"'>"+countryMaster.getName()+"</option>");
									else
										sb.append("<option value='"+countryMaster.getCountryId()+"'>"+countryMaster.getName()+"</option>");
								}
								sb.append("	</select></div>");
					}
				}
				
				//zipCode
				if(mapDspqRouterByFID.get(15)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(15);
					if(dspqRouter!=null)
					{
						String sZipCode="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getZipCode()!=null)
							sZipCode=teacherpersonalinfo.getZipCode();
						
						String sInputControlId="zipCode";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' value='"+sZipCode+"' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"' maxlength='10' class='form-control' onblur='changeCityStateByZipForDSPQ()' />"+
						"</div>");
					}
				}
				
				//State
				if(mapDspqRouterByFID.get(16)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(16);
					if(dspqRouter!=null)
					{
						
						Long lStateId=null;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getStateId()!=null && teacherpersonalinfo.getStateId().getStateId()!=null)
							lStateId=teacherpersonalinfo.getStateId().getStateId();
						
						String sInputControlId="stateIdForDSPQ";
						sb.append("<div class='col-sm-3 col-md-3' >"+
								"<label>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</label>"+
								"<select  class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' onchange='getCityListByStateForDSPQ()'>"+   
								"	<option value='0' id='slst'>"+Utility.getLocaleValuePropByKey("optSltSt", locale)+"</option>");
								if(listStateMaster!=null && listStateMaster.size() >0)
									for(StateMaster master :listStateMaster)
									{
										if(lStateId!=null && lStateId==master.getStateId())
										{
											sb.append("<option selected id='st'"+master.getStateId()+" shortname='"+master.getStateShortName()+"' value='"+master.getStateId()+"'>"+master.getStateName()+"</option>");
										}
										else
											sb.append("<option id='st'"+master.getStateId()+" shortname='"+master.getStateShortName()+"' value='"+master.getStateId()+"'>"+master.getStateName()+"</option>");
									}
								sb.append("</select></div>");
					}
				}
				
				//city
				if(mapDspqRouterByFID.get(17)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(17);
					if(dspqRouter!=null)
					{
						Long lCityId=null;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getCityId()!=null)
							lCityId=teacherpersonalinfo.getCityId().getCityId();
						
						String sInputControlId="cityIdForDSPQ";
						sb.append("<div class='col-sm-3 col-md-3' >"+
								"<label>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</label>"+
								"<select  class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"'>"+   
								"	<option value='0' id='slcty'>"+Utility.getLocaleValuePropByKey("lblSelectCity", locale)+"</option>");
								if(listCityMasters!=null && listCityMasters.size() >0)
									for(CityMaster cityMaster :listCityMasters)
									{
										if(lCityId!=null && lCityId==cityMaster.getCityId())
											sb.append("<option selected id='ct'"+cityMaster.getCityId()+" value='"+cityMaster.getCityId()+"'>"+cityMaster.getCityName()+"</option>");
										else
											sb.append("<option id='ct'"+cityMaster.getCityId()+" value='"+cityMaster.getCityId()+"'>"+cityMaster.getCityName()+"</option>");
									}
								sb.append("</select></div>");
					}
				}
				
				//otherState
				if(mapDspqRouterByFID.get(83)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(83);
					if(dspqRouter!=null)
					{
						String sOtherState="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getOtherState()!=null)
							sOtherState=teacherpersonalinfo.getOtherState();
						
						String sInputControlId="otherState";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' value='"+sOtherState+"' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"' maxlength='50' class='form-control' />"+
						"</div>");
					}
				}
			
				//otherCity
				if(mapDspqRouterByFID.get(82)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(82);
					if(dspqRouter!=null)
					{
						String sOtherCity="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getOtherCity()!=null)
							sOtherCity=teacherpersonalinfo.getOtherCity();
						
						String sInputControlId="otherCity";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' value='"+sOtherCity+"' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"' maxlength='50' class='form-control' />"+
						"</div>");
					}
				}
				
				sb.append("</div>");
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection4.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
				
			}
			
			
			DspqRouter dspqRouterSection17=mapDspqRouterBySID.get(17);
			if(dspqRouterSection17!=null)
			{
				
				String sSectionName="tooltipSection17";
				sb.append("<div class='row'>");
				sb.append("<div class='col-sm-12 col-md-12' ><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection17, listToolTips)+"</div></div>");
				sb.append("</div>");
				
				sb.append(getInstructionForSection(dspqRouterSection17));
				
				sb.append("<div class='row'>");
				
				printdata("AddressLine1");
				//AddressLine1
				if(mapDspqRouterByFID.get(87)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(87);
					if(dspqRouter!=null)
					{
						
						String sAddressLinePr1="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getPresentAddressLine1()!=null)
							sAddressLinePr1=teacherpersonalinfo.getPresentAddressLine1();
						
						String sInputControlId="addressLinePr";
						sb.append("<div class='col-sm-6 col-md-6'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' value='"+sAddressLinePr1+"' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"' maxlength='50' class='form-control'/>"+
						"</div>");
					}
				}
				printdata("AddressLine2");
				//AddressLine2
				if(mapDspqRouterByFID.get(88)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(88);
					if(dspqRouter!=null)
					{
						String sAddressLinePr2="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getPresentAddressLine2()!=null)
							sAddressLinePr2=teacherpersonalinfo.getPresentAddressLine2();
						
						String sInputControlId="addressLine2Pr";
						sb.append("<div class='col-sm-6 col-md-6'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' value='"+sAddressLinePr2+"' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"' maxlength='50' class='form-control'/>"+
						"</div>");
					}
				}
				printdata("country");
				//country
				if(mapDspqRouterByFID.get(89)!=null)
				{
					List<CountryMaster> countryMasters=countryMasterDAO.findAllActiveCountry();
					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(89);
					if(dspqRouter!=null)
					{
						Integer iPresentCountryId=0;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getPresentCountryId()!=null)
							iPresentCountryId=teacherpersonalinfo.getPresentCountryId().getCountryId();
								
								String sInputControlId="countryIdPr";
								sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<select id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' onchange=\"getStateByCountryForDspqPr('dspq');\">"+
								"	<option value='0'>"+Utility.getLocaleValuePropByKey("lblSelectCountry", locale)+"</option>");
								for(CountryMaster countryMaster:countryMasters)
								{
									if(iPresentCountryId==countryMaster.getCountryId())
										sb.append("<option selected id='ctryPr"+countryMaster.getCountryId()+"' value='"+countryMaster.getCountryId()+"'>"+countryMaster.getName()+"</option>");
									else
										sb.append("<option id='ctryPr"+countryMaster.getCountryId()+"' value='"+countryMaster.getCountryId()+"'>"+countryMaster.getName()+"</option>");
								}
								sb.append("	</select></div>");
					}
				}
				printdata("zipCode");
				//zipCode
				if(mapDspqRouterByFID.get(90)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(90);
					if(dspqRouter!=null)
					{
						String sPresentZipCode="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getPresentZipCode()!=null)
							sPresentZipCode=teacherpersonalinfo.getPresentZipCode();
						
						String sInputControlId="zipCodePr";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' value='"+sPresentZipCode+"' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"' maxlength='10' class='form-control' onblur='changeCityStateByZipForDSPQ()' />"+
						"</div>");
					}
				}
				printdata("State");
				//State
				if(mapDspqRouterByFID.get(91)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(91);
					if(dspqRouter!=null)
					{
							Long lPresentStateId=null;
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getPresentStateId()!=null)
								lPresentStateId=teacherpersonalinfo.getPresentStateId().getStateId();
							
									String sInputControlId="stateIdForDSPQPr";
									sb.append("<div class='col-sm-3 col-md-3' >"+
									"<label>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</label>"+
									"<select  class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' onchange='getCityListByStateForDSPQPr()'>"+   
									"	<option value='0' id='slstPr'>"+Utility.getLocaleValuePropByKey("optSltSt", locale)+"</option>");
									if(listStateMaster!=null && listStateMaster.size()>0)
										for(StateMaster master :listStateMaster)
										{
											if(lPresentStateId!=null && lPresentStateId==master.getStateId())
												sb.append("<option selected id='stPr'"+master.getStateId()+" shortname='"+master.getStateShortName()+"' value='"+master.getStateId()+"'>"+master.getStateName()+"</option>");
											else
												sb.append("<option id='stPr'"+master.getStateId()+" shortname='"+master.getStateShortName()+"' value='"+master.getStateId()+"'>"+master.getStateName()+"</option>");
										}
									sb.append("</select></div>");
						
					}
				}
				printdata("city");
				//city
				if(mapDspqRouterByFID.get(92)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(92);
					if(dspqRouter!=null)
					{
						
						Long lPresentCityId=null;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getPresentCityId()!=null)
							lPresentCityId=teacherpersonalinfo.getPresentCityId().getCityId();
						
								String sInputControlId="cityIdForDSPQPr";
								sb.append("<div class='col-sm-3 col-md-3' >"+
								"<label>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</label>"+
								"<select  class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"'>"+   
								"	<option value='0' id='slctyPr'>"+Utility.getLocaleValuePropByKey("lblSelectCity", locale)+"</option>");
								if(listCityMasters!=null && listCityMasters.size() >0)
									for(CityMaster cityMaster :listCityMasters)
									{
										if(lPresentCityId!=null && lPresentCityId==cityMaster.getCityId())
											sb.append("<option selected id='ctPr'"+cityMaster.getCityId()+" value='"+cityMaster.getCityId()+"'>"+cityMaster.getCityName()+"</option>");
										else
											sb.append("<option id='ctPr'"+cityMaster.getCityId()+" value='"+cityMaster.getCityId()+"'>"+cityMaster.getCityName()+"</option>");
									}
								sb.append("</select></div>");
					}
				}
				printdata("otherState");
				//otherState
				if(mapDspqRouterByFID.get(94)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(94);
					if(dspqRouter!=null)
					{
						String sPersentOtherState="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getPersentOtherState()!=null)
							sPersentOtherState=teacherpersonalinfo.getPersentOtherState();
						
							String sInputControlId="otherStatePr";
							sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' value='"+sPersentOtherState+"' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"' maxlength='50' class='form-control' />"+
						"</div>");
					}
				}
				printdata("otherCity");
				//otherCity
				if(mapDspqRouterByFID.get(93)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(93);
					if(dspqRouter!=null)
					{
						String sPersentOtherCity="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getPersentOtherCity()!=null)
							sPersentOtherCity=teacherpersonalinfo.getPersentOtherCity();
						
							String sInputControlId="otherCityPr";
							sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' value='"+sPersentOtherCity+"' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"' maxlength='50' class='form-control' />"+
						"</div>");
					}
				}
				
				sb.append("</div>");
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM,sCandidateTypeDec, dspqGroupMaster, dspqRouterSection17.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
			
			
			DspqRouter dspqRouterSection3=mapDspqRouterBySID.get(3);
			if(dspqRouterSection3!=null)
			{
				String sSectionName="tooltipSection3";
				sb.append("<div class='row'>");
				sb.append("<div class='col-sm-12 col-md-12' ><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection3, listToolTips)+"</div></div>");
				sb.append("</div>");
				
				sb.append(getInstructionForSection(dspqRouterSection3));
				
				if(mapDspqRouterByFID.get(8)!=null)
				{
					sb.append("<div class='row'>");//Start :: #5#
					DspqRouter dspqRouter=mapDspqRouterByFID.get(8);
					if(dspqRouter!=null)
					{
						Integer ethnicOriginId=null;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getEthnicOriginId()!=null)
							ethnicOriginId=teacherpersonalinfo.getEthnicOriginId().getEthnicOriginId();
						
						String sInputControlId="ethnicOriginId";
						sb.append("<div class='col-sm-12 col-md-12'><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div></div>");
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						for(EthnicOriginMaster ethnicOriginMaster :lstethnicOriginMasters)
						{
							sb.append("<div class='col-sm-4 col-md-4'>");
							sb.append("<label class='radio' style='margin-top: 2px;margin-bottom: 2px;'>");
							if(ethnicOriginId!=null && ethnicOriginId.equals(ethnicOriginMaster.getEthnicOriginId()))
								sb.append("<input type='radio' checked='checked' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"' id='"+sInputControlId+"_"+ethnicOriginMaster.getEthnicOriginId()+"' value='"+ethnicOriginMaster.getEthnicOriginId()+"'>"+ethnicOriginMaster.getEthnicOriginName());
							else
								sb.append("<input type='radio' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"' id='"+sInputControlId+"_"+ethnicOriginMaster.getEthnicOriginId()+"' value='"+ethnicOriginMaster.getEthnicOriginId()+"'>"+ethnicOriginMaster.getEthnicOriginName());
							sb.append("</label></div>");
						}
					}
				sb.append("</div>");//End :: #5#
				}
				
				
				if(mapDspqRouterByFID.get(9)!=null)
				{
					sb.append("<div class='row'>");//Start :: #6#
					DspqRouter dspqRouter=mapDspqRouterByFID.get(9);
					if(dspqRouter!=null)
					{
						
						Integer ethinicityId=null;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getEthnicityId()!=null)
							ethinicityId=teacherpersonalinfo.getEthnicityId().getEthnicityId();
						
						String sInputControlId="ethinicityId";
						sb.append("<div class='col-sm-12 col-md-12'><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div></div>");
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						for(EthinicityMaster ethinicityMaster :lstEthinicityMasters)
						{
							sb.append("<div class='col-sm-4 col-md-4'>");
							sb.append("<label class='radio' style='margin-top: 2px;margin-bottom: 2px;'>");
							if(ethinicityId!=null && ethinicityId.equals(ethinicityMaster.getEthnicityId()))
								sb.append("<input type='radio' checked='checked' name='"+sInputControlId+"' id='"+sInputControlId+"_"+ethinicityMaster.getEthnicityId()+"' value='"+ethinicityMaster.getEthnicityId()+"'>"+ethinicityMaster.getEthnicityName());
							else
								sb.append("<input type='radio' name='"+sInputControlId+"' id='"+sInputControlId+"_"+ethinicityMaster.getEthnicityId()+"' value='"+ethinicityMaster.getEthnicityId()+"'>"+ethinicityMaster.getEthnicityName());
							sb.append("</label></div>");
						}
					}
				sb.append("</div>");//End :: #6#
				}
				
				if(mapDspqRouterByFID.get(10)!=null)
				{
					sb.append("<div class='row'>");//Start :: #7#
					DspqRouter dspqRouter=mapDspqRouterByFID.get(10);
					if(dspqRouter!=null)
					{
						Map<Integer, Boolean> mapRaceId=new HashMap<Integer, Boolean>();
						String raceIds=null;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getRaceId()!=null)
						{
							raceIds=teacherpersonalinfo.getRaceId();
							if(raceIds!=null && !raceIds.equals(""))
							{
								String sRaceIdArr[]=raceIds.split(",");
								if(sRaceIdArr!=null && sRaceIdArr.length>0)
								{
									for(String sRaceIsTemp:sRaceIdArr)
									{
										int IracetTemp=Utility.getIntValue(sRaceIsTemp);
										mapRaceId.put(IracetTemp, true);
									}
								}
							}
							
						}
						
						String sInputControlId="raceId";
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						sb.append("<div class='col-sm-12 col-md-12'><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div></div>");
						for(RaceMaster rm:listRaceMasters)
						{
							sb.append("<div class='col-sm-4 col-md-4'>");
							sb.append("<label class='checkbox inline' style='margin-top: 2px;margin-bottom: 2px;'>");
							
							if(mapRaceId!=null && mapRaceId.get(rm.getRaceId())!=null)
								sb.append("<input type='checkbox' checked='checked' name='"+sInputControlId+"' id='"+sInputControlId+"_"+rm.getRaceId()+"' value='"+rm.getRaceId()+"'>"+rm.getRaceName()+"</br>");
							else
								sb.append("<input type='checkbox' name='"+sInputControlId+"' id='"+sInputControlId+"_"+rm.getRaceId()+"' value='"+rm.getRaceId()+"'>"+rm.getRaceName()+"</br>");
							
							sb.append("</label>");
							sb.append("</div>");
						}
					}
				sb.append("</div>");//End :: #7#
				}
				
				if(mapDspqRouterByFID.get(11)!=null)
				{
					sb.append("<div class='row'>");//Start :: #8#
					DspqRouter dspqRouter=mapDspqRouterByFID.get(11);
					if(dspqRouter!=null)
					{
						
						Integer genderId=null;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getGenderId()!=null)
							genderId=teacherpersonalinfo.getGenderId().getGenderId();
						
						String sInputControlId="genderId";
						sb.append("<div class='col-sm-12 col-md-12'><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div></div>");
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						for(GenderMaster genderMaster:lstGenderMasters)
						{
							sb.append("<div class='col-sm-4 col-md-4'>");
							sb.append("<label class='radio' style='margin-top: 2px;margin-bottom: 2px;'>");
							if(genderId!=null && genderId.equals(genderMaster.getGenderId()))
								sb.append("<input type='radio' checked='checked' name='"+sInputControlId+"' id='"+sInputControlId+"+"+genderMaster.getGenderId()+"' value='"+genderMaster.getGenderId()+"'>"+genderMaster.getGenderName());
							else
								sb.append("<input type='radio' name='"+sInputControlId+"' id='"+sInputControlId+"_"+genderMaster.getGenderId()+"' value='"+genderMaster.getGenderId()+"'>"+genderMaster.getGenderName());
							sb.append("</label>");
							sb.append("</div>");
						}
					}
				sb.append("</div>");
				}
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM,sCandidateTypeDec, dspqGroupMaster, dspqRouterSection3.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
			
			
			DspqRouter dspqRouterSection20=mapDspqRouterBySID.get(20);
			if(dspqRouterSection20!=null)
			{
				String sSectionName="tooltipSection20";
				sb.append("<div class='row'>");
				sb.append("<div class='col-sm-12 col-md-12' ><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection20, listToolTips)+"</div></div>");
				sb.append("</div>");
				
				sb.append(getInstructionForSection(dspqRouterSection20));
				
				sb.append("<div class='row'>");
				if(mapDspqRouterByFID.get(97)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(97);
					if(dspqRouter!=null)
					{
						
						Integer isVateran=null;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getIsVateran()!=null)
							isVateran=teacherpersonalinfo.getIsVateran();
						
						String sInputControlId="veteran";
						sb.append("<div class='col-sm-12 col-md-12'><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div></div>");
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						sb.append("<div class='col-sm-11 col-md-11'>");
						sb.append("<label class='radio' style='margin-top: 2px;margin-bottom: 2px;'>");
						
						if(isVateran!=null && isVateran==1)
							sb.append("<input type='radio' id='vt1' value='1' checked name='"+sInputControlId+"' onclick='showdistrictspeciFicVeteran();'>"+Utility.getLocaleValuePropByKey("lblYes", locale));
						else
							sb.append("<input type='radio' id='vt1' value='1' name='"+sInputControlId+"' onclick='showdistrictspeciFicVeteran();'>"+Utility.getLocaleValuePropByKey("lblYes", locale));
						
						sb.append("</label>");
						sb.append("</div>");
						
						sb.append("<div class='col-sm-11 col-md-11'>");
						sb.append("<label class='radio' style='margin-top: 2px;margin-bottom: 2px;'>");
						
						if(isVateran!=null && isVateran==0)
							sb.append("<input type='radio' id='vt2' checked value='0' name='"+sInputControlId+"' onclick='showdistrictspeciFicVeteran();'>"+Utility.getLocaleValuePropByKey("lblNo", locale));
						else
							sb.append("<input type='radio' id='vt2' value='0' name='"+sInputControlId+"' onclick='showdistrictspeciFicVeteran();'>"+Utility.getLocaleValuePropByKey("lblNo", locale));
						
						sb.append("</label>");
						sb.append("</div>");
						
						sb.append("<div class='col-sm-11 col-md-11'>");
						sb.append("<div class='col-sm-12 col-md-12 top5 hide' id='vetranOptionDiv' class='hide'>");					
						sb.append("<table>");
						sb.append("<tbody>");
						sb.append("<tr>");
						sb.append("<td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;'><input type='radio' id='veteranOptS' name='veteranOptS' value='1'></div></td>");
						sb.append("<td style='border:0px;background-color: transparent;padding-left:5px;'><font size='2' color='red'>"+Utility.getLocaleValuePropByKey("lblDp_commonsA", locale)+"</font> "+Utility.getLocaleValuePropByKey("msgDp_commons5", locale)+"</td>");
						sb.append("</tr>");
						sb.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;'><input type='radio' id='veteranOptS' name='veteranOptS' value='2'></div></td>");
						sb.append("<td style='border:0px;background-color: transparent;padding-left:5px;' ><font size='2' color='red'>  "+Utility.getLocaleValuePropByKey("lblDp_commonsB", locale)+"</font>"+Utility.getLocaleValuePropByKey("msgDp_commons6", locale)+"</td>");
						sb.append("</tr>");
						sb.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;'><input type='radio' id='veteranOptS' name='veteranOptS' value='3'></div></td>");
						sb.append("<td style='border:0px;background-color: transparent;padding-left:5px;' ><font size='2' color='red'> "+Utility.getLocaleValuePropByKey("lblDp_commonsC", locale)+"</font>"+Utility.getLocaleValuePropByKey("msgDp_commons7", locale)+" </td>");
						sb.append("</tr>");
						sb.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' id='veteranOptS' name='veteranOptS' value='4'></div></td>");
						sb.append("<td style='border:0px;background-color: transparent;padding-left:5px;' ><font size='2' color='red'> "+Utility.getLocaleValuePropByKey("lblDp_commonsD", locale)+"</font>"+Utility.getLocaleValuePropByKey("msgDp_commons50", locale)+" </td>");
						sb.append("</tr>");
						sb.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;'><input type='radio' id='veteranOptS' name='veteranOptS' value='5'></div></td>");
						sb.append("<td style='border:0px;background-color: transparent;padding-left:5px;' ><font size='2' color='red'> "+Utility.getLocaleValuePropByKey("lblDp_commonsE", locale)+"</font>"+Utility.getLocaleValuePropByKey("msgDp_commons8", locale)+" </td>");
						sb.append("</tr>");
						sb.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' id='veteranOptS' name='veteranOptS' value='6'></div></td>");
						sb.append("<td style='border:0px;background-color: transparent;padding-left:5px;' ><font size='2' color='red'>"+Utility.getLocaleValuePropByKey("lblDp_commonsF", locale)+"</font>"+Utility.getLocaleValuePropByKey("msgDp_commons9", locale)+"</td>");
						sb.append("</tr>");
						sb.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' id='veteranOptS' name='veteranOptS' value='7'></div></td>");
						sb.append("<td style='border:0px;background-color: transparent;padding-left:5px;' ><font size='2' color='red'>"+Utility.getLocaleValuePropByKey("lblDp_commonsG", locale)+"</font>"+Utility.getLocaleValuePropByKey("msgDp_commons10", locale)+"</td>");
						sb.append("</tr>");
						sb.append("</tbody>");
						sb.append("</table>");
						sb.append("<div class='top10'>"+Utility.getLocaleValuePropByKey("msgDp_commons11", locale)+"</div>");
						sb.append("<table>");
						sb.append("<tbody>");
						sb.append("<tr>");
						sb.append("<td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' id='veteranPreference1' name='veteranPreference1' value='1'></div> </td>");
						sb.append("<td style='border:0px;background-color: transparent;padding-left:5px;' >"+Utility.getLocaleValuePropByKey("lblYes", locale)+"</td></tr>");
						sb.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' id='veteranPreference1' name='veteranPreference1' value='2'></div> ");
						sb.append("</td><td style='border:0px;background-color: transparent;padding-left:5px;'>No </td>");
						sb.append("</tr>");
						sb.append("</tbody>");
						sb.append("</table>");
						sb.append("<div class='top10'>"+Utility.getLocaleValuePropByKey("msgDp_commons12", locale)+"</div>");
						sb.append("<table>");
						sb.append("<tbody>");
						sb.append("<tr>");
						sb.append("<td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' id='veteranPreference2' name='veteranPreference2' value='1'></div> </td>");
						sb.append("<td style='border:0px;background-color: transparent;padding-left:5px;' >"+Utility.getLocaleValuePropByKey("lblYes", locale)+" </td>");
						sb.append("</tr>");
						sb.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' id='veteranPreference2' name='veteranPreference2' value='2'></div> </td>");
						sb.append("<td style='border:0px;background-color: transparent;padding-left:5px;'>"+Utility.getLocaleValuePropByKey("lblNo", locale)+"</td>");
						sb.append("</tr>");
						sb.append("</tbody>");
						sb.append("</table>");
						sb.append("</div>");
						sb.append("</div>");
						
					}
				}
				sb.append("</div>");
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM,sCandidateTypeDec, dspqGroupMaster, dspqRouterSection20.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
			
			DspqRouter dspqRouterSection22=mapDspqRouterBySID.get(22);
			if(dspqRouterSection22!=null)
			{
				String sSectionName="tooltipSection22";
				sb.append("<div class='row'>");
				sb.append("<div class='col-sm-12 col-md-12' ><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection22, listToolTips)+"</div></div>");
				sb.append("</div>");
				
				sb.append(getInstructionForSection(dspqRouterSection22));
				
				sb.append("<div class='row'>");
				
				if(mapDspqRouterByFID.get(100)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(100);
					if(dspqRouter!=null)
					{
						String sDrivingLicNum="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getDrivingLicNum()!=null)
							sDrivingLicNum=teacherpersonalinfo.getDrivingLicNum();
						
							String sInputControlId="drivingLicNum";
							sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' value='"+sDrivingLicNum+"' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"' maxlength='50' class='form-control' />"+
						"</div>");
					}
				}
				
				if(mapDspqRouterByFID.get(99)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(99);
					if(dspqRouter!=null)
					{
							String sDrivingLicState=null;
							int iDrivingLicState=0;
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getDrivingLicState()!=null && !teacherpersonalinfo.getDrivingLicState().equals(""))
							{
								sDrivingLicState=teacherpersonalinfo.getDrivingLicState();
								if(!sDrivingLicState.equals(""))
								{
									iDrivingLicState=Utility.getIntValue(sDrivingLicState);
								}
							}
							
									String sInputControlId="drivingLicState";
									sb.append("<div class='col-sm-3 col-md-3' >"+
									"<label>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</label>"+
									"<select  class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"'>"+   
									"	<option value='0' id='slstPr'>"+Utility.getLocaleValuePropByKey("optSltSt", locale)+"</option>");
									if(listStateMaster!=null && listStateMaster.size()>0)
										for(StateMaster master :listStateMaster)
										{
											if(iDrivingLicState!=0 && iDrivingLicState==master.getStateId())
												sb.append("<option selected id='stDL'"+master.getStateId()+" shortname='"+master.getStateShortName()+"' value='"+master.getStateId()+"'>"+master.getStateName()+"</option>");
											else
												sb.append("<option id='stDL'"+master.getStateId()+" shortname='"+master.getStateShortName()+"' value='"+master.getStateId()+"'>"+master.getStateName()+"</option>");
										}
									sb.append("</select></div>");
						
					}
				}
				
				
				sb.append("</div>");
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM,sCandidateTypeDec, dspqGroupMaster, dspqRouterSection22.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
			
			DspqRouter dspqRouterSection18=mapDspqRouterBySID.get(18);
			if(dspqRouterSection18!=null)
			{
				boolean bKnowOtherLanguages=false;
				TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
				if(teacherExperience!=null && teacherExperience.getKnowOtherLanguages()!=null && teacherExperience.getKnowOtherLanguages())
				{
					bKnowOtherLanguages=true;
				}
				String sSubSection=getAddSubSectionText(dspqRouterSection18);
				
				String sSectionName="tooltipSection18";
				sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;'>			"+
				"				<div style='float: left' class='portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection18, listToolTips)+"</div>"+
				"				<div style='clear: both;'></div>"+
				"		    </div>");
				
				sb.append(getInstructionForSection(dspqRouterSection18));
				
				sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;'>"+
							"<label>"+Utility.getLocaleValuePropByKey("qnDoYouknwOtherLang", locale)+"</label>");
				if(bKnowOtherLanguages)
				{
					sb.append("<br><INPUT TYPE='radio' checked class='custlstner' NAME='knowLanguage' id='knowLanguage1'  value='1' onclick='showLanguageGrid();'> &nbsp;&nbsp;&nbsp;"+Utility.getLocaleValuePropByKey("btnYes", locale));
					sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE='radio' class='custlstner' id='knowLanguage2' NAME='knowLanguage' value='0' onclick='showLanguageGrid();'> &nbsp;&nbsp;&nbsp;"+Utility.getLocaleValuePropByKey("btnNo", locale));
				}
				else
				{
					sb.append("<br><INPUT TYPE='radio' NAME='knowLanguage' class='custlstner' id='knowLanguage1'  value='1' onclick='showLanguageGrid();'> &nbsp;&nbsp;&nbsp;"+Utility.getLocaleValuePropByKey("btnYes", locale));
					sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE='radio' checked class='custlstner' id='knowLanguage2' NAME='knowLanguage' value='0' onclick='showLanguageGrid();'> &nbsp;&nbsp;&nbsp;"+Utility.getLocaleValuePropByKey("btnNo", locale));
				}
				
				sb.append("</div>");
				
				sb.append("<div class='col-sm-12 col-md-12'  style='padding-left:0px;padding-right:0px;' id='lanAddDiv'>			"+
						"				<div style='float: left' class='portfolio_Subheading'>&nbsp;</div>"+
						"					<div style='float: right' class='addPortfolio'>"+
						"						<a href='javascript:void(0);' onclick='return showLanguageForm();'>"+sSubSection+"</a>"+
						"					</div>"+
						"				<div style='clear: both;'></div>"+
						"		    </div>");
				
				sb.append("<input type='hidden' id='gridNameFlag' name='language'/>");
				sb.append("<div class='row' onmouseover=\"setGridNameFlag('language')\">"+
				"	    <div class='col-sm-12 col-md-12'>"+
				"			<div id='divDataLangTchrGrid'></div>"+
				"		</div>"+
				"	</div>");
				
				
				sb.append("<div class='portfolio_Section_ImputFormGap' id='divLanguageForm' style='display: none;'>"+
				"			<input type='hidden' id='teacherLanguageId' name='teacherLanguageId' />");
				
				//--------- Input Field start
				sb.append("<div class='row'>");
				
			
				
				if(mapDspqRouterByFID.get(95)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(95);
					if(dspqRouter!=null)
					{
						// Part #1
						List<SpokenLanguageMaster> lstSpokenLanguageMasters=spokenLanguageMasterDAO.findAllActiveSpokenLanguageMasterByOrder();
						String sInputControlIdSpknLan="spokenLanguage";
						sb.append("<div class='col-sm-3 col-md-3' >"+
						"<label>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlIdSpknLan,dspqRouter,listToolTips)+"</label>"+
						"<select  class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlIdSpknLan+"' onchange='hideShowSpknLng()'>"+
						"<option value='0'>"+Utility.getLocaleValuePropByKey("optSlt", locale)+"</option>");
						
						for(SpokenLanguageMaster slm:lstSpokenLanguageMasters)
							sb.append("<option value='"+slm.getSpokenlanguagemasterid()+"' id='spknLan"+slm.getSpokenlanguagemasterid()+"'>"+slm.getLanguageName()+"</option>");
						sb.append("<option value='-1' id='spknLan"+Utility.getLocaleValuePropByKey("lblOthr", locale)+"'>"+Utility.getLocaleValuePropByKey("lblOthr", locale)+"</option>");
						sb.append("</select></div>");
						
						// Part #2
						String sOtherText=Utility.getLocaleValuePropByKey("lblOthr", locale)+"&nbsp;";
						String sInputControlId="languageText";
						sb.append("<div class='col-sm-3 col-md-3 hide' id='divlanguageText'>"+
							"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips,sOtherText)+"</strong></label>"+
							"<input type='text' value='' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"' maxlength='50' class='form-control' />"+
						"</div>");
					}
				}
				
				if(mapDspqRouterByFID.get(117)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(117);
					if(dspqRouter!=null)
					{
						String sInputControlId="oralSkills";
						sb.append("<div class='col-sm-3 col-md-3' >"+
						"<label>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</label>"+
						"<select  class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"'>"+
						"<option value='0'>"+Utility.getLocaleValuePropByKey("optSlt", locale)+"</option>"+
						"<option value='1'>"+Utility.getLocaleValuePropByKey("lblPolite", locale)+"</option>"+
						"<option value='2'>"+Utility.getLocaleValuePropByKey("lblLiterate", locale)+"</option>"+
						"<option value='3'>"+Utility.getLocaleValuePropByKey("lblFluent", locale)+"</option>");
						sb.append("</select></div>");
					}
				}
				
				if(mapDspqRouterByFID.get(118)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(118);
					if(dspqRouter!=null)
					{
						String sInputControlId="writtenSkills";
						sb.append("<div class='col-sm-3 col-md-3' >"+
						"<label>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</label>"+
						"<select  class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"'>"+
						"<option value='0'>"+Utility.getLocaleValuePropByKey("optSlt", locale)+"</option>"+
						"<option value='1'>"+Utility.getLocaleValuePropByKey("lblPolite", locale)+"</option>"+
						"<option value='2'>"+Utility.getLocaleValuePropByKey("lblLiterate", locale)+"</option>"+
						"<option value='3'>"+Utility.getLocaleValuePropByKey("lblFluent", locale)+"</option>");
						sb.append("</select></div>");
					}
				}
				
				sb.append("</div>");
				
				sb.append(
						" 		<div class='row'>								"+
						"	    <div class='col-sm-11 col-md-11 divDone'>"+
						"			<a id='hrefDone' class='idone' href='#' style='cursor: pointer; margin-left: 0px; text-decoration:none;' onclick='return insertOrUpdateTchrLang();' >"+Utility.getLocaleValuePropByKey("lnkImD", locale)+"</a>&nbsp;&nbsp;"+
						"			<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;'	onclick='return hideTchrLang()'>"+
										Utility.getLocaleValuePropByKey("lnkCancel", locale)+
						"			</a>"+
						"		</div>"+
						"		</div>"+
						"	</div>");
				
				sb.append("</div>");
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM,sCandidateTypeDec, dspqGroupMaster, dspqRouterSection18.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			
				
			}
			
		}
		

		sb.append("<div class='row'>" +
				"<div class='col-sm-9 col-md-9'><div class='mt30'><button class='flatbtn' id='btnprev' type='button' style='width:20%' onclick='return getPreviousPage();'> <i class='backicon'></i> "+Utility.getLocaleValuePropByKey("btnPrevDSPQ", locale)+" </button>&nbsp;&nbsp;<button class='flatbtn' style='background:#F89507;width:20%'  type='button' onclick='return validateDSPQPersonalGroup(1);'>"+Utility.getLocaleValuePropByKey("btnSaveExit", locale)+" <i class='fa fa-check-circle'></i></button>&nbsp;&nbsp;<button onclick='cancelApplyJob();' class='flatbtn' style='background: #da4f49;width:20%' id='cancel'><strong>"+Utility.getLocaleValuePropByKey("btnClr", locale)+" <i class='fa fa-times-circle'></i></strong> </button>&nbsp;&nbsp;</div></div>" +
				"<div class='col-sm-3 col-md-3'><div class='mt30' style='text-align:right;'><button class='flatbtn' id='btnnext' style='width: 90%;' type='button' onclick='return validateDSPQPersonalGroup(0);'>"+Utility.getLocaleValuePropByKey("btnSaveAndNextDSPQ", locale)+" <i class='icon'></i></button></div></div>" +
				"</div>");
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}
	
	@Transactional(readOnly=false)
	public String getGroup02(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup02");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		StringBuffer sb=new StringBuffer();
		
		sb.append("<div class='row'>" +
				"<div class='col-sm-9 col-md-9'><div class='mt10'><button class='flatbtn' id='btnprev' type='button' style='width:16%' onclick='return getPreviousPage();'><i class='backicon'></i> "+Utility.getLocaleValuePropByKey("btnPrevDSPQ", locale)+"</button>&nbsp;&nbsp;<button class='flatbtn' style='background:#F89507;width:20%'  type='button' onclick='return validateDSPQAcademicGroup(1);'>"+Utility.getLocaleValuePropByKey("btnSaveExit", locale)+" <i class='fa fa-check-circle'></i></button>&nbsp;&nbsp;<button class='flatbtn' style='background: #da4f49;width:16%'  type='button' onclick='return cancelApplyJob();'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+" <i class='fa fa-times-circle'></i></button></div></div>" +
				"<div class='col-sm-3 col-md-3'><div class='mt10' style='text-align:right;'><button class='flatbtn' style='width:90%;' id='btnnext' type='button' onclick='return validateDSPQAcademicGroup(0);'>"+Utility.getLocaleValuePropByKey("btnSaveAndNextDSPQ", locale)+" <i class='icon'></i></button></div></div>" +
				"</div>");

		String sReturnToolTips="";
		return sb.toString()+"|||"+sReturnToolTips;
	}
	
	
	@Transactional(readOnly=false)
	public String getGroup02_2(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup02_2");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		//TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		JobOrder jobOrder=null;
		JobCategoryMaster dspqPotfolioJC=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
			if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null)
				dspqPotfolioJC=jobOrder.getJobCategoryMaster();
		}
		
		StringBuffer sb=new StringBuffer();
		
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(6);
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		sectionMasters.add(dspqSectionMaster);
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		if(true)
		{
			DspqRouter dspqRouterSection6=mapDspqRouterBySID.get(6);
			
			DistrictMaster dspqPotfolioDM=null;
			if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
				dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
			
			if(dspqRouterSection6!=null)
			{
				
				String sSubSection=getAddSubSectionText(dspqRouterSection6);
				
				String sSectionName="tooltipSection06";
				sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;padding-right:0px;'>			"+
				"				<div style='float: left' class='portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection6, listToolTips)+"</div>"+
				"					<div style='float: right' class='addPortfolio'>"+
				"						<a href='javascript:void(0);' id='addSchoolIdDSPQ' onclick='return showUniversityForm();'>"+sSubSection+"</a>"+
				"					</div>"+
				"				<div style='clear: both;'></div>"+
				"		    </div>");
				
				sb.append(getInstructionForSection(dspqRouterSection6));
				
				sb.append("<input type='hidden' id='gridNameFlag' name='gridNameFlag'/>");
				sb.append("<div class='row' onmouseover=\"setGridNameFlag('academics')\">"+
				"	    <div class='col-sm-12 col-md-12'>"+
				"			<div id='divDataGridAcademin'></div>"+
				"		</div>"+
				"	</div>");
				
				
				sb.append("<div class='portfolio_Section_ImputFormGap' id='divAcademicRow' style='display: none;' onkeypress='chkForEnter_Academic(event)'>"+
				"		<iframe id='uploadFrameAcademicID' name='uploadFrameAcademic' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>"+
				"		<form id='frmAcadamin' name='frmAcadamin' enctype='multipart/form-data' method='post' target='uploadFrameAcademic' action='fileuploadservlet.do' >"+
				"			<input type='hidden' id='insertOrUpdateLink' name='insertOrUpdateLink' value=''/>"+
				"			<input type='hidden' id='academicId' name='academicId'/>");
				
				//--------- Input Field start
				sb.append("<div class='row'>");
				printdata("degreeName");
				//degreeName
				if(mapDspqRouterByFID.get(20)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(20);
					if(dspqRouter!=null)
					{
						sb.append(" <input  type='hidden' id='degreeId' "+getInputCustomeAttribute(dspqRouter)+" value=''><input  type='hidden' id='degreeType' value=''> ");
						
						String sInputControlId="degreeName";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+
								"	<a href='#' onclick=\"document.getElementById('degreeName').value='Others';document.getElementById('degreeName').focus();return false;\" style='margin-left: 18px;'>"+Utility.getLocaleValuePropByKey("msgDp_commons30", locale)+"</a>"+
								"</strong></label>"+
								"<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' autocomplete='off' class='form-control input-small' value='' maxlength='50' onfocus=\"getDegreeMasterAutoComp(this, event, 'divTxtShowData', 'degreeName','degreeId','');\" onkeyup=\"getDegreeMasterAutoComp(this, event, 'divTxtShowData', 'degreeName','degreeId','');\" onblur=\"hideDegreeMasterDiv(this,'degreeId','divTxtShowData');\" />"+
						"");
						
						sb.append("<div id='divTxtShowData' style=' display:none;' class='result' onmouseover=\"mouseOverChk('divTxtShowData','degreeName');\"></div></div>");
						
					}
				}
				
				printdata("universityId");
				//universityId
				if(mapDspqRouterByFID.get(21)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(21);
					if(dspqRouter!=null)
					{
						sb.append(" <input  type='hidden' id='universityId' value='' "+getInputCustomeAttribute(dspqRouter)+">");
						
						String sInputControlId="universityName";
						sb.append("<div class='col-sm-4 col-md-4'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+
								"	<a href='#' onclick=\"document.getElementById('universityName').value='Other';document.getElementById('universityName').focus();return false;\" style='margin-left: 106px;'>"+Utility.getLocaleValuePropByKey("lnkMySchNotLi", locale)+"</a>"+
								"</strong></label>"+
								"<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' autocomplete='off' class='form-control' value=''  maxlength='50' onfocus=\"getUniversityAutoComp(this, event, 'divTxtUniversityData', 'universityName','universityId','');\" onkeyup=\"getUniversityAutoComp(this, event, 'divTxtUniversityData', 'universityName','universityId','');\" onblur=\"hideUniversityDiv(this,'universityId','divTxtUniversityData');\" />");
						
						sb.append("<div id='divTxtUniversityData' style=' display:none;' class='result' onmouseover=\"mouseOverChk('divTxtUniversityData','universityName')\"></div></div>");
						
					}
				}
				
				printdata("fieldName");
				//fieldName
				if(mapDspqRouterByFID.get(22)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(22);
					if(dspqRouter!=null)
					{
						String sInputControlId="fieldName";
						
						sb.append("<div class='col-sm-5 col-md-5'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+
								"<a  href='#' onclick=\"document.getElementById('fieldName').value='Other'; document.getElementById('fieldName').focus();return false;\" style='margin-left: 160px;'>"+Utility.getLocaleValuePropByKey("lnkMyFldNotLi", locale)+"</a>"+
								"	</strong></label>"+
								"	<input  type='hidden' id='fieldId' value='' "+getInputCustomeAttribute(dspqRouter)+">"+
								"	<input  type='text' class='form-control' maxlength='50' autocomplete='off' id='fieldName' value='' name='fieldName' onfocus=\"getFieldOfStudyAutoComp(this, event, 'divTxtFieldOfStudyData', 'fieldName','fieldId','');\" onkeyup=\"getFieldOfStudyAutoComp(this, event, 'divTxtFieldOfStudyData', 'fieldName','fieldId','');\"  onblur=\"hideFeildOfStudyDiv(this,'fieldId','divTxtFieldOfStudyData');\"/>"+
								"	"+
								"	<div id='divTxtFieldOfStudyData' style=' display:none;position: absolute; z-index: 5000;' onmouseover=\"mouseOverChk('divTxtFieldOfStudyData','fieldName')\"  class='result' ></div>"+
								"</div>");
					}
				}
				
				printdata("Dates Attended");
				//Dates Attended
				if(mapDspqRouterByFID.get(23)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(23);
					if(dspqRouter!=null)
					{
						List<String>  lstLastYear= Utility.getLasterYearByYear(1955);
						String sInputControlId="attendedInYear";
						sb.append("<div class='col-sm-11 col-md-11'>"+
								  "  <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								  " </div>");
						
						sb.append("<div class='col-sm-2 col-md-2'>"+
								"   <select class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"'>"+
								"	 <option id='attendedInYearSelect' value='0'>"+Utility.getLocaleValuePropByKey("optSlt", locale)+"</option>");
									for(String year:lstLastYear)
									{
										sb.append("<option id='"+sInputControlId+""+year+"' value='"+year+"'>"+year+"</option>");
									}
						sb.append("</select></div>");
						
						sb.append("<div class='fl to' style='margin-left:-7px;'>"+
								   Utility.getLocaleValuePropByKey("lblTo", locale)+
								"</div>");
						
						String sInputControlId2="leftInYear";
						sb.append("<div class='col-sm-2 col-md-2' style='margin-left:-7px;'>"+
								"	<select class='span1 form-control' id='"+sInputControlId2+"' name='"+sInputControlId2+"'>"+
								"	   <option id='attendedInYearSelect' value='0'>"+Utility.getLocaleValuePropByKey("optSlt", locale)+"</option>");
										for(String year:lstLastYear)
										{
											sb.append("<option id='"+sInputControlId2+""+year+"' value='"+year+"'>"+year+"</option>");
										}		
						sb.append("</select></div>");
						
					}
				}
				
				printdata("Transcript/Diploma/Certificate ");
				//Transcript/Diploma/Certificate 
				if(mapDspqRouterByFID.get(24)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(24);
					if(dspqRouter!=null)
					{
						String sInputControlId="pathOfTranscript";
						sb.append("<div  id='transcriptDiv' class='nobleCssHide ${transUDis}'>"+
								"	<div  class='col-sm-3 col-md-3' style='margin-top: -23px;'>"+
								"	   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"		<input id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"' type='file' width='20px;'>"+
								"		<input type='hidden' value='0' id='aca_file'>"+
								"		<a href='javascript:void(0)' onclick='clearTranscript()'>"+Utility.getLocaleValuePropByKey("lnkClear", locale)+"</a>"+
								"	</div>"+
								"	<span class='col-sm-3 col-md-3' id='divResumeSection' name='divResumeSection' style='display: none;'>"+
								"		<label>"+
								"			&nbsp;&nbsp;"+
								"		</label>"+
								"		<span id='divResumerName'></span>"+
								"		&nbsp;&nbsp;<a href='javascript:void(0)' onclick='removeTranscript()' class='left5'>"+Utility.getLocaleValuePropByKey("lnkRmovS", locale)+"</a>"+
								"	</span>					  "+
								"</div>");
					}
					
				}
				sb.append("</div>");
				//GPA/Freshman/Sophomore/Junior...etc start.
				sb.append("<div id='divGPARow' style='display: none;>");
				sb.append("<form  id='frmGPA'>");
				sb.append("<div class='row'>"+
						"	<div class='col-sm-2 col-md-2' id='GPAAll'>"+
								Utility.getLocaleValuePropByKey("lblGPA", locale)+
						"		<a href='#' id='iconpophover2' rel='tooltip' data-original-title='Grade Point Average'><img src='images/qua-icon.png' width='15' height='15' alt=''></a>"+
						"	</div>"+
						"</div>");
				
				sb.append("<div class='row'>");
				//Input Field Start
				
				//Freshman  
				if(mapDspqRouterByFID.get(25)!=null)
				{
					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(25);
					if(dspqRouter!=null)
					{
						String sInputControlId="gpaFreshmanYear";
						sb.append("<div class='col-sm-2 col-md-2'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"'  name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" onkeypress='return checkForDecimalTwo(event)' class='input-small form-control' id='gpaFreshman' maxlength='4'>"+
								"</div>");
					}
				}
				
				printdata("Sophomore");
				//Sophomore   
				if(mapDspqRouterByFID.get(26)!=null)
				{
					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(26);
					if(dspqRouter!=null)
					{
						String sInputControlId="gpaSophomoreYear";
						sb.append("<div class='col-sm-2 col-md-2'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"'  name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" onkeypress='return checkForDecimalTwo(event)'  class='input-small form-control' id='gpaSophomore' maxlength='4'>"+
								"</div>");
					}
				}
				//Junior
				printdata("Junior");
				if(mapDspqRouterByFID.get(27)!=null)
				{
					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(27);
					if(dspqRouter!=null)
					{
						String sInputControlId="gpaJuniorYear";
						sb.append("<div class='col-sm-2 col-md-2'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"'  name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" onkeypress='return checkForDecimalTwo(event)'  class='input-small form-control' id='gpaJunior' maxlength='4'>"+
								"</div>");
					}
				}
				
				//Senior 
				printdata("Senior");
				if(mapDspqRouterByFID.get(28)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(28);
					if(dspqRouter!=null)
					{
						String sInputControlId="gpaSeniorYear";
						sb.append("<div class='col-sm-2 col-md-2'>"+
								"  <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"'  name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" onkeypress='return checkForDecimalTwo(event)'  class='input-small form-control' id='gpaSenior' maxlength='4'>"+
								"</div>");
					}
				}
				
				//gpaCumulative 
				printdata("gpaCumulative");
				if(mapDspqRouterByFID.get(29)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(29);
					if(dspqRouter!=null)
					{
						String sInputControlId="gpaCumulative";
						sb.append("<div class='col-sm-2 col-md-2'>"+
								"  <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"'  name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" onkeypress='return checkForDecimalTwo(event)'  class='input-small form-control' id='gpaCumulative' maxlength='4'>"+
								"</div>");
					}
				}
				
				sb.append("</div>");
				sb.append("</form>");
				sb.append("</div>");
				//GPA End.
				
				
				//gpaCumulative 
				printdata("gpaCumulative");
				if(mapDspqRouterByFID.get(29)!=null)
				{
					// GPARowOther.
					sb.append("<div id='divGPARowOther' style='display: none;'>");
					sb.append("<form id='frmGPA1'>");
					sb.append("<div class='row'>"+
							"	<div class='col-sm-2 col-md-2'>"+
									Utility.getLocaleValuePropByKey("lblGPA", locale)+
							"		<a href='#' id='iconpophover2' rel='tooltip' data-original-title='Grade Point Average'><img src='images/qua-icon.png' width='15' height='15' alt=''></a>"+
							"	</div>"+
							"</div>");
					sb.append("<div class='row'>");
					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(29);
					if(dspqRouter!=null)
					{
						String sInputControlId="gpaCumulative1";
						sb.append("<div class='col-sm-2 col-md-2'>"+
								"  <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"'  name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" onkeypress='return checkForDecimalTwo(event)'  class='input-small form-control' id='gpaCumulative' maxlength='4'>"+
								"</div>");
					}
					
					sb.append("</div>");
					sb.append("</form>");
					sb.append("</div>");
					
				}
				
				//--------- Input Field end
																												//onblur=\"hideFeildOfStudyDiv(this,'fieldId','divTxtFieldOfStudyData');\"
				sb.append("</form>"+
				" 		<div class='row'>								"+
				"	    <div class='col-sm-11 col-md-11'>"+
				"			<a id='hrefDone' class='idone' href='#' style='cursor: pointer; margin-left: 0px; text-decoration:none;' onclick=\"return insertOrUpdate_Academics();\" >"+Utility.getLocaleValuePropByKey("lnkImD", locale)+"</a>&nbsp;&nbsp;"+
				"			<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;'	onclick='return resetUniversityForm()'>"+
								Utility.getLocaleValuePropByKey("lnkCancel", locale)+
				"			</a>"+
				"		</div>"+
				"		</div>"+
				"	</div>");
				
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM,sCandidateTypeDec, dspqGroupMaster, dspqRouterSection6.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
		}
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}
	
	@Transactional(readOnly=false)
	public String getGroup03(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup03");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		StringBuffer sb=new StringBuffer();
		
		sb.append("<div class='row'>" +
				"<div class='col-sm-9 col-md-9'><div class='mt10'><button class='flatbtn' id='btnprev' type='button' style='width:16%' onclick='return getPreviousPage();'> <i class='backicon'></i> "+Utility.getLocaleValuePropByKey("btnPrevDSPQ", locale)+"</button>&nbsp;&nbsp;<button class='flatbtn' style='background:#F89507;width:20%'  type='button' onclick='return validateDSPQCredentialsGroup(1);'>"+Utility.getLocaleValuePropByKey("btnSaveExit", locale)+" <i class='fa fa-check-circle'></i></button>&nbsp;&nbsp;<button class='flatbtn' style='background: #da4f49;width:16%'  type='button' onclick='return cancelApplyJob();'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+" <i class='fa fa-times-circle'></i></button></div></div>" +
				"<div class='col-sm-3 col-md-3'><div class='mt10' style='text-align:right;'><button class='flatbtn' style='width:90%;' id='btnnext' type='button' onclick='return validateDSPQCredentialsGroup(0);'>"+Utility.getLocaleValuePropByKey("btnSaveAndNextDSPQ", locale)+" <i class='icon'></i></button></div></div>" +
				"</div>");
		
		String sReturnToolTips="";
		return sb.toString()+"|||"+sReturnToolTips;
	}
	
	@Transactional(readOnly=false)
	public String getGroup03_8(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup03_8");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		JobOrder jobOrder=null;
		JobCategoryMaster dspqPotfolioJC=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
			if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null)
				dspqPotfolioJC=jobOrder.getJobCategoryMaster();
		}
		
		StringBuffer sb=new StringBuffer();
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(8);
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		sectionMasters.add(dspqSectionMaster);
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		if(mapDspqRouterBySID!=null)
		{
			DistrictMaster dspqPotfolioDM=null;
			if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
				dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
			
			DspqRouter dspqRouterSection8=mapDspqRouterBySID.get(8);
			if(dspqRouterSection8!=null)
			{
				String sSubSection=getAddSubSectionText(dspqRouterSection8);
				
				String sSectionName="tooltipSection8";
				sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;padding-right:0px;'>			"+
				"				<div style='float: left' class='portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection8, listToolTips)+
				"				</div>"+
				"					<div style='float: right' class='addPortfolio'>"+
				"					<a href='javascript:void(0);' id='addCertificationIdDSPQ' onclick='showForm_Certification();' >"+sSubSection+"</a>"+
				"				</div>"+
				"				<div  class='addPortfolio show' id='addLicenseCerti'>"+
				"					<a href='javascript:void(0);' onclick='showForm_Licence();' ><spring:message code='licenseAddButton' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>"+
				"				</div>"+
				"				<div style='clear: both;'></div>	"+
				"		    </div>");
				
				sb.append(getInstructionForSection(dspqRouterSection8));
				
				sb.append("<div class='row' onmouseover=\"setGridNameFlag('certification')\">"+
				"	    <div class='col-sm-12 col-md-12'>"+
				"			<div id='divDataGridCertifications'></div>"+
				"		</div>"+
				"	</div>");
				
				
				sb.append("<div class='portfolio_Section_ImputFormGap' id='divMainForm' style='display: none;' onkeypress='chkForEnter_Certifications(event)'>"+
				"		<iframe id='uploadFrameCertificationsID' name='uploadFrameCertifications' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>"+
				"		<form id='frmCertificate' name='frmCertificate' enctype='multipart/form-data' method='post' target='uploadFrameCertifications' action='fileuploadservletforcertification.do' >"+
				"			<input type='hidden' id='certId' name='certId'/>");
				
				//--------- Input Field start
				
				printdata("certificationStatusMaster");
				//certificationStatusMaster
				if(mapDspqRouterByFID.get(32)!=null)
				{
					List<CertificationStatusMaster> lstCertificationStatusMaster=certificationStatusMasterDAO.findAllCertificationStatusMasterByOrder();
					DspqRouter dspqRouter=mapDspqRouterByFID.get(32);
					if(dspqRouter!=null)
					{
						String sInputControlId="certificationStatusMaster";
						
						sb.append("<div class='col-sm-4 col-md-4' style='padding-left:0px;'>"+
						"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"<select class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"' >"+
						"<option value='0'>"+Utility.getLocaleValuePropByKey("lblSltCerti/LuceStatus", locale)+"</option>");
						for(CertificationStatusMaster certificationStatusMaster:lstCertificationStatusMaster)
						{
							sb.append("<option id='csml"+certificationStatusMaster.getCertificationStatusId()+"' value='"+certificationStatusMaster.getCertificationStatusId()+"'>"+certificationStatusMaster.getCertificationStatusName()+"</option>");
						}
						sb.append("	</select></div>");
					}
				}
				
				printdata("certificationtypeMaster");
				//certificationtypeMaster
				if(mapDspqRouterByFID.get(33)!=null)
				{
					Criterion certIficatCri = Restrictions.eq("status", "A");
					List<CertificationTypeMaster> listCertificationTypeMasters=certificationTypeMasterDAO.findByCriteria(certIficatCri);
					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(33);
					if(dspqRouter!=null)
					{
						String sInputControlId="certificationtypeMaster";
						
						sb.append("<div class='col-sm-3 col-md-3'>"+
						"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"<select id='certificationtypeMaster' class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"'>"+
						"<option value='0'>"+Utility.getLocaleValuePropByKey("optSlt", locale)+"</option>");
						for(CertificationTypeMaster certificationTypeMaster:listCertificationTypeMasters)
						{
							sb.append("<option id='"+certificationTypeMaster.getCertificationTypeMasterId()+"' value='"+certificationTypeMaster.getCertificationTypeMasterId()+"'>"+certificationTypeMaster.getCertificationType()+"</option>");
						}
						sb.append("	</select></div>");
					}
				}
				
				printdata("Cert State");
				//Cert State
				if(mapDspqRouterByFID.get(34)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(34);
					if(dspqRouter!=null)
					{
						List<StateMaster> listStateMaster = stateMasterDAO.findAllStateByOrder();
						
						String sInputControlId="stateMaster";
						sb.append("<div class='col-sm-4 col-md-4' >"+
						"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"<select  class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' onchange=\"getPraxis(),clearCertType();\">"+   
						"<option value='0'>"+Utility.getLocaleValuePropByKey("optSltSt", locale)+"</option>");
						if(listStateMaster!=null && listStateMaster.size() >0)
							for(StateMaster master :listStateMaster)
								sb.append("<option id='certst'"+master.getStateId()+" value='"+master.getStateId()+"'>"+master.getStateName()+"</option>");
						
						sb.append("</select></div>");
					}
				}
				
				
				printdata("yearReceived");
				//yearReceived
				if(mapDspqRouterByFID.get(35)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(35);
					if(dspqRouter!=null)
					{
						List<String> lstYearReceived=Utility.getLasterYearByYear(1955);
						
						String sInputControlId="yearReceived";
						sb.append("<div class='col-sm-2 col-md-2' style='padding-left:0px;' >"+
						"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"<select  class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' >"+   
						"<option id='yearReceivedSelect' value='0'>"+Utility.getLocaleValuePropByKey("optSlt", locale)+"</option>");
						if(lstYearReceived!=null && lstYearReceived.size() >0)
							for(String yearReceived :lstYearReceived)
								sb.append("<option id='yearReceived'"+yearReceived+" value='"+yearReceived+"'>"+yearReceived+"</option>");
						
						sb.append("</select></div>");
					}
				}
				
				printdata("yearexpires");
				//yearexpires
				if(mapDspqRouterByFID.get(36)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(36);
					if(dspqRouter!=null)
					{
						List<String> lstYearReceived=Utility.getComingYearsByYear();
						
						String sInputControlId="yearexpires";
						sb.append("<div class='col-sm-2 col-md-2' style='width:19.5%;'>"+
						"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"<select  class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' >"+   
						"<option value='0'>"+Utility.getLocaleValuePropByKey("optDoesNotExpire", locale)+"</option>");
						if(lstYearReceived!=null && lstYearReceived.size() >0)
							for(String yearReceived :lstYearReceived)
								sb.append("<option id='yearExpires'"+yearReceived+" value='"+yearReceived+"'>"+yearReceived+"</option>");
						
						sb.append("</select></div>");
					}
				}
				
				printdata("Certification/Licensure Name");
				////Certification/Licensure Name
				if(mapDspqRouterByFID.get(37)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(37);
					if(dspqRouter!=null)
					{
						String sInputControlIdHidden="certificateTypeMaster";
						String sInputControlId="certType";
						sb.append("<div class='col-sm-7 col-md-7 certClass'>"+
								"		<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"		<a href='#' style='margin-left: 130px;' onclick='showcertiDiv()'>"+Utility.getLocaleValuePropByKey("lnkCerti/LiceNotLi", locale)+"</a>"+
								"	 </label>"+
								"	<input  type='hidden' id='"+sInputControlIdHidden+"' "+getInputCustomeAttribute(dspqRouter)+" value=''>"+
								"	<input  type='text' class='form-control' maxlength='500'"+
								"		id='"+sInputControlId+"' "+
								"		value=''"+
								"		name='"+sInputControlId+"'"+
								"		onfocus=\"getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', '"+sInputControlId+"','"+sInputControlIdHidden+"','');\""+
								"		onkeyup=\"getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', '"+sInputControlId+"','"+sInputControlIdHidden+"','');\" "+
								"		onblur=\"hideCertificateType(this,'"+sInputControlIdHidden+"','divTxtCertTypeData');\"/>"+
								"	<div id='divTxtCertTypeData' style=' display:none;' class='result' onmouseover=\"mouseOverChk('divTxtCertTypeData','"+sInputControlId+"')\"></div>"+
								"</div>");
					}
				}
				printdata("DOE Number");
				//DOE Number
				if(mapDspqRouterByFID.get(38)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(38);
					if(dspqRouter!=null)
					{
						String sInputControlId="doenumber";
						sb.append("<div class='col-sm-2 col-md-2 certClass dOENumberDiv' style='padding-left:0px;'>"+
						     "<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"	<input type='text' class='form-control' name='"+sInputControlId+"' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" maxlength='25' value='' />"+
						"</div>");
					}
				}
				
				printdata("Certification/Licensure Url");
				//Certification/Licensure Url
				if(mapDspqRouterByFID.get(39)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(39);
					if(dspqRouter!=null)
					{
						String sInputControlId="certUrl";
						sb.append("<div class='col-sm-9 col-md-9 certClass cluDiv'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"	<input  type='text' class='form-control' maxlength='500' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' value='' name='"+sInputControlId+"' />"+
						"</div>");
					}
				}
				if(mapDspqRouterByFID.get(114)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(114);
					if(dspqRouter!=null)
					{
						String sInputControlId="certExp";
						sb.append("<div class='col-sm-11 col-md-11 certClass' style='padding-left:0px;'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"	<input  type='text' class='form-control' maxlength='500' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' value='' name='"+sInputControlId+"' />"+
						"</div>");
					}
				}
				//Grade Level(s)
				printdata("Grade Level(s)");
				if(mapDspqRouterByFID.get(40)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(40);
					if(dspqRouter!=null)
					{
						String sInputControlId="GradeLevel";
						sb.append("	<input  type='hidden' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" value=''>");
						sb.append("<div class='col-sm-11 col-md-11' style='padding-left:0px;'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label></div>"+
								"<div class='col-sm-11 col-md-11' style='padding-left:0px;'>" +
								"<div class='divwidth'>"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='pkOffered' name='pkOffered' >"+
										Utility.getLocaleValuePropByKey("lblPK", locale) +
								"		</label>"+
								"	</div>	"+
								"	<div class='divwidth'>"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='kgOffered' name='kgOffered' >"+
										Utility.getLocaleValuePropByKey("lblKG", locale) +
								"		</label>"+
								"	</div>	"+
								"	<div class='divwidth'>	"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='g01Offered' name='g01Offered' >"+
								"		1 "+
								"		</label>"+
								"	</div>	"+
								"	<div class='divwidth'>"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='g02Offered' name='g02Offered' >"+
								"		2 "+
								"		</label>"+
								"	</div>	"+
								"	<div class='divwidth'>					"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='g03Offered' name='g03Offered' >"+
								"		3 "+
								"		</label>"+
								"	</div>	"+
								"	<div class='divwidth'>"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='g04Offered' name='g04Offered' >"+
								"		4 "+
								"		</label>"+
								"	</div>	"+
								"	<div class='divwidth'>"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='g05Offered' name='g05Offered' >"+
								"		5 "+
								"		</label>"+
								"	</div>	"+
								"	<div class='divwidth'>"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='g06Offered' name='g06Offered' >"+
								"		6 "+
								"		</label>"+
								"	</div>	"+
								"	<div class='divwidth'>"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='g07Offered' name='g07Offered' >"+
								"		7 "+
								"		</label>"+
								"	</div>	"+
								"	<div class='divwidth'>"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='g08Offered' name='g08Offered' >"+
								"		8 "+
								"		</label>"+
								"	</div>	"+
								"	<div class='divwidth'>"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='g09Offered' name='g09Offered' >"+
								"		9 "+
								"		</label>"+
								"	</div>	"+
								"	<div class='divwidth'>"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='g10Offered' name='g10Offered' >"+
								"		10 "+
								"		</label>"+
								"	</div>	"+
								"	<div class='divwidth'>"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='g11Offered' name='g11Offered' >"+
								"		11 "+
								"		</label>"+
								"	</div>	"+
								"	<div class='divwidth'>"+
								"		<label class='checkbox inline'>"+
								"		<input type='checkbox' value='1' id='g12Offered' name='g12Offered' >"+
								"		12 "+
								"		</label>"+
								"	</div>"+
								"</div>");
						
						
					}
				}
				printdata("raxis I Reading 41 42 43 ");
				
				if(mapDspqRouterByFID.get(41)!=null ||mapDspqRouterByFID.get(42)!=null || mapDspqRouterByFID.get(43)!=null )
				{
					DspqRouter dspqRouter41=mapDspqRouterByFID.get(41);
					DspqRouter dspqRouter42=mapDspqRouterByFID.get(42);
					DspqRouter dspqRouter43=mapDspqRouterByFID.get(43);
					
					sb.append("<div id='praxisArea' class='certClass' >"+
							"    <div class='col-sm-2 col-md-2' style='padding-left:0px;'>"+
							"	    <label ><strong>"+Utility.getLocaleValuePropByKey("lblReqPrxITests", locale) +"</strong></label>"+
							"     </div>");
							
					if(dspqRouter41!=null)
					{
						String sInputControlId41="readingQualifyingScore";
						sb.append("<div class='col-sm-3 col-md-3' >"+
							    "	  <label ><strong id='reading'></strong></label>"+
							    "	   <div class='row'>" +
								"    		<div class='col-sm-8 col-md-8'>"+
								"      			<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId41,dspqRouter41,listToolTips)+"</strong></label><input type='text' maxlength='3' id='"+sInputControlId41+"' "+getInputCustomeAttribute(dspqRouter41)+" class='form-control' onkeypress='return checkForInt(event)'></strong></label>"+
								"			</div>"+
								"	   </div>"+
								"</div>");
					}	
					
					if(dspqRouter42!=null)
					{		
						String sInputControlId42="writingQualifyingScore";
					    sb.append("<div class='col-sm-3 col-md-3'>"+
							"	   <label ><strong id='writing'></strong></label>"+
							"	     <div class='row'>"+
							"		  <div class='col-sm-8 col-md-8'>"+
							"             <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId42,dspqRouter42,listToolTips)+"</strong></label><input type='text' maxlength='3' id='"+sInputControlId42+"' "+getInputCustomeAttribute(dspqRouter42)+" class='form-control' onkeypress='return checkForInt(event)'></strong></label>"+
							"		  </div>"+
							"       </div>"+
							"	 </div>");
					}	
					if(dspqRouter43!=null)
					{
						String sInputControlId43="mathematicsQualifyingScore";
						sb.append("<div class='col-sm-3 col-md-3' >"+
								"	    <label ><strong id='maths'></strong></label>"+
								"	      <div class='row'>"+
								"		    <div class='col-sm-8 col-md-8'>"+
								"		      <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId43,dspqRouter43,listToolTips)+"</strong></label><input type='text' maxlength='3' id='"+sInputControlId43+"' "+getInputCustomeAttribute(dspqRouter43)+" class='form-control' onkeypress='return checkForInt(event)'></strong></label>"+
								"		   </div>"+
								"	   </div>"+
								"    </div>");
					}
					sb.append("</div>"); 
				}
				
				
				printdata("Certification/Licensure Letter");
				// 	Certification/Licensure Letter
				if(mapDspqRouterByFID.get(44)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(44);
					if(dspqRouter!=null)
					{
						String sInputControlId="pathOfCertificationFile"; 
						String sInputControlIdHidden="pathOfCertification"; 
						sb.append("<div class='col-sm-4 col-md-4 top5' style='padding-left:0px;'>"+
								"	   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"' type='file'>"+
								"	<a href='javascript:void(0)' id='clearLink' onclick='clearCertification()'>Clear</a>"+
								"</div>"+
								"<input type=hidden id='"+sInputControlIdHidden+"' value='0'/><input type='hidden' name='sbtsource_cert' value='0'/>"+
								"<div class='col-sm-5 col-md-5'>"+
								"<span  id='removeCert' name='removeCert' style='display: none;'>"+
								"	<label>"+
								"		&nbsp;&nbsp;"+
								"	</label>"+
								"	<span id='divCertName'></span>"+
								"	<a href='javascript:void(0)' onclick='removeCertificationFile()'><spring:message code='lnkRemo' /></a>&nbsp;"+
								"</span>"+
								"</div>");
						
					}
				}
				
				sb.append("</form>"+
						" 		<div class='row'>								"+
						"	    <div class='col-sm-11 col-md-11'>"+
						"			<a id='hrefDone' class='idone' href='#' style='cursor: pointer; margin-left: 0px; text-decoration:none;' onclick=\"return insertOrUpdate_Certification();\" >"+Utility.getLocaleValuePropByKey("lnkImD", locale)+"</a>&nbsp;&nbsp;"+
						"			<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;'	onclick='return hideForm_Certification()'>"+
										Utility.getLocaleValuePropByKey("lnkCancel", locale)+
						"			</a>"+
						"		</div>"+
						"		</div>"+
						"	</div>");
				
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM,sCandidateTypeDec, dspqGroupMaster, dspqRouterSection8.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
				
			}
			
		}
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}
	
	@Transactional(readOnly=false)
	public String getGroup03_9(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup03_9");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		JobOrder jobOrder=null;
		JobCategoryMaster dspqPotfolioJC=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
			if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null)
				dspqPotfolioJC=jobOrder.getJobCategoryMaster();
		}
		
		StringBuffer sb=new StringBuffer();
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(9);
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		sectionMasters.add(dspqSectionMaster);
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		if(mapDspqRouterBySID!=null)
		{
			DistrictMaster dspqPotfolioDM=null;
			if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
				dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
			
			
			DspqRouter dspqRouterSection9=mapDspqRouterBySID.get(9);
			if(dspqRouterSection9!=null)
			{
				String sSectionName="tooltipSection09";
				String sSubSection=getAddSubSectionText(dspqRouterSection9);
				sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;padding-right:0px;'>"+
				"				<div style='float: left' class='portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection9, listToolTips)+
				"				</div>"+
				"					<div style='float: right' class='addPortfolio'>"+
				"					<a href='javascript:void(0);' id='addReferIdDSPQ' onclick='showElectronicReferencesForm()' >"+sSubSection+"</a>"+
				"				</div>"+
				"				<div style='clear: both;'></div>	"+
				"		    </div>");
				
				sb.append(getInstructionForSection(dspqRouterSection9));
				
				sb.append("<div class='row'>"+
				"		        <div class='col-sm-12 col-md-12'>"+
				"		        <div class='divErrorMsg' id='errordivElectronicReferences' style='display: block;'></div>"+
				"		        </div>"+
				"	        </div>");
				
				sb.append("<div class='row' onmouseover=\"setGridNameFlag('reference')\">"+
				"	    <div class='col-sm-12 col-md-12'>"+
				"			<div id='divDataElectronicReferences'></div>"+
				"		</div>"+
				"	</div>");
				
				
				sb.append("<div id='divMainForm_ref' style='display: none;'>"+
				"		<iframe id='uploadFrameReferencesID' name='uploadFrameReferences' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>"+
				"		<form id='frmElectronicReferences' name='frmElectronicReferences' enctype='multipart/form-data' method='post' target='uploadFrameReferences' action='fileuploadservletforreferences.do' >");
				//--------- Input Field start
				
				//Salutation
				printdata("Salutation");
				if(mapDspqRouterByFID.get(45)!=null)
				{
					
					sb.append("<input type='hidden' id='elerefAutoId'>");
					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(45);
					if(dspqRouter!=null)
					{
						String sInputControlId="salutation";
						sb.append("<div class='col-sm-3 col-md-3' style='padding-left:0px;'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<select class='form-control' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >"+
								"		<option value='0'></option> "+
								"		<option value='4'>"+Utility.getLocaleValuePropByKey("optDr", locale)+"</option>"+
								"		<option value='3'>"+Utility.getLocaleValuePropByKey("optMiss", locale)+"</option>"+
								"		<option value='2'>"+Utility.getLocaleValuePropByKey("optMr", locale)+"</option>"+
								"		<option value='1'>"+Utility.getLocaleValuePropByKey("optMrs", locale)+"</option>"+
								"		<option value='5'>"+Utility.getLocaleValuePropByKey("optMs", locale)+"</option>													"+
								"	</select>"+
								"</div>");
					}
					
				}
				
				printdata("First Name");
				if(mapDspqRouterByFID.get(46)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(46);
					if(dspqRouter!=null)
					{
						String sInputControlId="firstName";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='hidden' id='elerefAutoId'>"+
								"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' placeholder='' maxlength='50'>"+
								"</div>");
					}
				}
				//Last Name
				if(mapDspqRouterByFID.get(47)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(47);
					if(dspqRouter!=null)
					{
						String sInputControlId="lastName";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' placeholder='' maxlength='50'>"+
								"</div>");
					}
				}
				//Tital/Designation
				if(mapDspqRouterByFID.get(48)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(48);
					if(dspqRouter!=null)
					{
						String sInputControlId="designation";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' placeholder='' maxlength='50'>"+
								"</div>");
					}
					
				}
				
				//Organization/Emp
				if(mapDspqRouterByFID.get(49)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(49);
					if(dspqRouter!=null)
					{
						String sInputControlId="organization";
						sb.append("<div class='col-sm-6 col-md-6' style='padding-left:0px;'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' placeholder='' maxlength='50'>"+
								"</div>");
					}
					
				}
				
				//Contact Number
				if(mapDspqRouterByFID.get(50)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(50);
					if(dspqRouter!=null)
					{
						String sInputControlId="contactnumber";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' placeholder='' maxlength='50'>"+
								"</div>");
					}
					
				}
				
				//Email 
				if(mapDspqRouterByFID.get(51)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(51);
					if(dspqRouter!=null)
					{
						String sInputControlId="email";
						sb.append("<div class='col-sm-6 col-md-6' style='padding-left:0px;'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' placeholder='' maxlength='50'>"+
								"</div>");
					}
					
				}
				//Recommendation Letter
				if(mapDspqRouterByFID.get(52)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(52);
					if(dspqRouter!=null)
					{
						String sInputControlId="pathOfReferenceFile";
						String sInputControlIdHidden="pathOfReference";
						sb.append("<div class='row top15'>"+
								"	<div class='col-sm-4 col-md-4'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='hidden' name='sbtsource_ref' id='sbtsource_ref' value='0'/>"+
								"		<input id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" type='file' width='20px;'>"+
								"<input type=hidden id='"+sInputControlIdHidden+"' value='0'/>"+
								"	    <a href='javascript:void(0)' onclick='clearReferences()'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+"</a>"+
								"    </div>"+
								"	<div class='col-sm-3 col-md-3' id='removeref' name='removeref' style='display: none;'>"+
								"		<label>"+
								"			&nbsp;&nbsp;"+
								"		</label>"+
								"		<span id='divRefName'>"+
								"		</span>"+
								"		<a href='javascript:void(0)' onclick='removeReferences()'>"+Utility.getLocaleValuePropByKey("lnkRemo", locale)+"</a>&nbsp;&nbsp;&nbsp;"+
								"	</div>"+
								"</div>");
					}
				}
				
				// Can this person be directly contacted  
				if(mapDspqRouterByFID.get(53)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(53);
					if(dspqRouter!=null)
					{
						String sInputControlId="rdcontacted";
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						sb.append("<div class='nobleCssHide'>"+
								"    <div class='col-sm-5 col-md-5' style='padding-left:0px;'>"+
								"       <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	    <div class='' id='' style='height: 40px;'>"+
								"		   <div class='radio inline col-sm-1 col-md-1'>"+
								"			 <input type='radio' checked='checked' id='rdcontacted1' value='1'  name='"+sInputControlId+"' onchange='showAndHideCanContOnOffer(1)'> "+Utility.getLocaleValuePropByKey("lblYes", locale)+
								"		   </div>"+
								"		    </br>"+
								"		    <div class='radio inline col-sm-1 col-md-1' style='margin-left:20px;margin-top:-10px;'>"+
								"				<input type='radio' id='rdcontacted0' value='0' name='"+sInputControlId+"' onchange='showAndHideCanContOnOffer(1)'> No"+
								"		    </div>"+
								"	    </div>"+
								"    </div>"+
								"</div>");
					}
					
				}
				//How Long Do u Know
				if(mapDspqRouterByFID.get(115)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(115);
					if(dspqRouter!=null)
					{
						String sInputControlId="longHaveYouKnow";
						sb.append(" <div class='col-sm-5 col-md-5'>"+
								"       <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"		<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' placeholder=''>"+
								"	</div>");
					}
				}
				
				//New Field is not added
				if(mapDspqRouterByFID.get(116)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(116);
					if(dspqRouter!=null)
					{
						String sInputControlId="referenceDetailText";
					   sb.append("<div class='row'>"+
							"    <div class='col-sm-11 col-md-11'>"+
							"		<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
							"	    <div id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+"><textarea rows='8' style='width:100%;' id='txtArea_"+sInputControlId+"'></textarea></div>"+
							"    </div>"+
							"</div>");
					}
				}
				
				//***************************End*************************************
				//--------- Input Field end
				
				sb.append("</form>"+
				" 		<div class='row'>								"+
				"	    <div class='col-sm-11 col-md-11 top10 idone'>"+
				"			<a id='hrefDone' href='#' style='cursor: pointer; margin-left: 0px; text-	;' onclick='return insertOrUpdateElectronicReferences();' >"+Utility.getLocaleValuePropByKey("lnkImD", locale)+"</a>&nbsp;&nbsp;"+
				"			<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;'	onclick='return hideElectronicReferencesForm()'>"+
								Utility.getLocaleValuePropByKey("lnkCancel", locale)+
				"			</a>"+
				"		</div>"+
				"		</div>"+
				"	</div>");
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM,sCandidateTypeDec, dspqGroupMaster, dspqRouterSection9.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
		}
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}
	
	@Transactional(readOnly=false)
	public String getGroup03_10(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup03_10");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		JobOrder jobOrder=null;
		JobCategoryMaster dspqPotfolioJC=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
			if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null)
				dspqPotfolioJC=jobOrder.getJobCategoryMaster();
		}
		
		StringBuffer sb=new StringBuffer();
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(10);
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		sectionMasters.add(dspqSectionMaster);
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		if(mapDspqRouterBySID!=null)
		{
			DistrictMaster dspqPotfolioDM=null;
			if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
				dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
			
			DspqRouter dspqRouterSection10=mapDspqRouterBySID.get(10);
			if(dspqRouterSection10!=null)
			{
				String sSubSection=getAddSubSectionText(dspqRouterSection10);
				String sSectionName="tooltipSection10";
				sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;padding-right:0px;'>"+
				"				<div style='float: left' class='portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection10, listToolTips)+
				"				 </div>"+
				"					<div style='float: right' class='addPortfolio'>"+
				"					   <a href='javascript:void(0);' id='addVideoIdDSPQ' onclick='showVideoLinksForm()' >"+sSubSection+"</a>"+
				"				    </div>"+
				"				<div style='clear: both;'></div>	"+
				"		  </div>");
				
				sb.append(getInstructionForSection(dspqRouterSection10));
				
				sb.append("<div class='row' onmouseover=\"setGridNameFlag('videolinks')\">"+
				"	        <div class='col-sm-12 col-md-12'>"+
				"			  <div id='divDataVideoLinks'></div>"+
				"		    </div>"+
				"	      </div>");
				
				
				sb.append("<div class='portfolio_Section_ImputFormGap' id='divvideoLinks' style='display: none;'>"+
				"			<iframe id='uploadFrameVideoID' name='uploadFrameVideo' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>"+
				"			<div class='divErrorMsg' id='errordivvideoLinks' style='display: block;'></div>"+
				"			<form id='frmvideoLinks' name='frmvideoLinks' enctype='multipart/form-data' method='post' target='uploadFrameVideo' action='certificationVideoUploadServlet.do' >");
				
				sb.append("<input type='hidden' id='sbtsource_videoLink' name='sbtsource_videoLink' value='0'/>");
				//--------- Input Field start
				
				//Video Link
				printdata("Video Link");
				if(mapDspqRouterByFID.get(54)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(54);
					if(dspqRouter!=null)
					{
						String sInputControlId="videourl";
						sb.append("<div class='row'>"+
								"	<div class='col-sm-12 col-md-12' style='padding-right:0px;'>"+
								"		<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"		<input type='hidden' id='videolinkAutoId' name='videolinkAutoId'>"+
								"		<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' placeholder='' maxlength='200'>"+
								"	</div>"+
								"</div>");
					}
				}
				//Video
				printdata("videofile");
				if(mapDspqRouterByFID.get(55)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(55);
					if(dspqRouter!=null)
					{
						String sInputControlId="videofile"; 
						String sInputControlIdHidden="dbvideofile"; 
						
						sb.append("<div class=''>"+
								"	<div class='col-sm-6 col-md-6' style='padding-left:0px;'>" +
								"		<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"		<input type='file' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"'>												"+
								"		<input type=hidden id='"+sInputControlIdHidden+"' value='0'/>"+
								"		<span style='display: none; visibility: hidden;' id='video'></span>"+
								"	</div>"+
								" </div>");
					}
				}
				
				
				//--------- Input Field end
				
				sb.append("</form>"+
				" 		<div class='row'>"+
				"	    <div class='col-sm-11 col-md-11 top10 idone'>"+
				"			<a id='hrefDone' href='#' style='cursor: pointer; margin-left: 0px; text-	;' onclick='return insertOrUpdatevideoLinks();' >"+Utility.getLocaleValuePropByKey("lnkImD", locale)+"</a>&nbsp;&nbsp;"+
				"			<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;'	onclick='return hideVideoLinksForm()'>"+
								Utility.getLocaleValuePropByKey("lnkCancel", locale)+
				"			</a>"+
				"		</div>"+
				"		</div>"+
				"	</div>");
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection10.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
		}
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}
	
	@Transactional(readOnly=false)
	public String getGroup03_11(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup03_11");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		JobOrder jobOrder=null;
		JobCategoryMaster dspqPotfolioJC=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
			if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null)
				dspqPotfolioJC=jobOrder.getJobCategoryMaster();
		}
		
		StringBuffer sb=new StringBuffer();
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(11);
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		sectionMasters.add(dspqSectionMaster);
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		if(mapDspqRouterBySID!=null)
		{
			DistrictMaster dspqPotfolioDM=null;
			if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
				dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
			
			DspqRouter dspqRouterSection11=mapDspqRouterBySID.get(11);
			if(dspqRouterSection11!=null)
			{
				String sSubSection=getAddSubSectionText(dspqRouterSection11);
				String sSectionName="tooltipSection11";
				sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;padding-right:0px;'>"+
				"				<div style='float: left' class='portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection11, listToolTips)+
				"				 </div>"+
				"					<div style='float: right' class='addPortfolio'>"+
				"					   <a href='javascript:void(0);' id='addAdditionalDocsdDSPQ' onclick='showAdditionalDocumentsForm()' >"+sSubSection+"</a>"+
				"				    </div>"+
				"				<div style='clear: both;'></div>	"+
				"		  </div>");
				
				sb.append(getInstructionForSection(dspqRouterSection11));
				
				sb.append("<div class='row' onmouseover=\"setGridNameFlag('additionalDocuments')\">"+
				"	        <div class='col-sm-12 col-md-12'>"+
				"			  <div id='divGridAdditionalDocuments'></div>"+
				"		    </div>"+
				"	      </div>");
				
				
				sb.append("<div class='portfolio_Section_ImputFormGap' id='divAdditionalDocumentsRow' style='display: none;'>"+
				"			<iframe id='uploadFrameAdditionalDocumentsID' name='uploadFrameAdditionalDocuments' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>"+
				"			<div class='divErrorMsg' id='errAdditionalDocuments' style='display: block;'></div>"+
				"			<form id='additionalDocumentsForm' name='multifileuploadformTarget2' enctype='multipart/form-data' method='post' target='uploadFrameAdditionalDocuments' action='additionalDocumentsUploadServlet.do' >");
				
				//--------- Input Field start
				//documentName
				printdata("documentName");
				if(mapDspqRouterByFID.get(56)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(56);
					if(dspqRouter!=null)
					{
						String sInputControlId="documentName";
						sb.append("<div class='col-sm-4 col-md-4' style='padding-left:0px;'>"+
									"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
									"	<input type='hidden' id='additionDocumentId' name='additionDocumentId' value=''/>"+
									"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' placeholder='' maxlength='250'>"+
									"</div>");
					}
				}
				
				//documentName
				printdata("uploadedDocument");
				if(mapDspqRouterByFID.get(57)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(57);
					if(dspqRouter!=null)
					{
						String sInputControlId="uploadedDocument";
						sb.append("<div class='col-sm-4 col-md-4'>"+
								"	<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='hidden' id='db_"+sInputControlId+"' name='db_"+sInputControlId+"' value='0'/>"+
								"	<input type='hidden' id='sbtsource_aadDoc' name='sbtsource_aadDoc' value='0'/>"+
								"	<input id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" type='file' width='20px;'>"+
								"	<a href='javascript:void(0)' onclick='clearUploadedDocument()'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+"</a>"+
								"</div>"+
								"<div class='col-sm-3 col-md-3' style='margin-top: 25px;' id='removeUploadedDocumentSpan' name='removeUploadedDocumentSpan' style='display: none;'>"+
								"	<label>"+
								"		&nbsp;&nbsp;"+
								"	</label>"+
								"	<div id='divUploadedDocument'>"+
								"	</div>"+
								"</div>");
					}
				}
				
				//--------- Input Field end
				
				sb.append("</form>"+
				" 		<div class='row'>"+
				"	    <div class='col-sm-11 col-md-11 top10 idone'>"+
				"			<a id='hrefDone' href='#' style='cursor: pointer; margin-left: 0px; text-	;' onclick='return insertOrUpdate_AdditionalDocuments();' >"+Utility.getLocaleValuePropByKey("lnkImD", locale)+"</a>&nbsp;&nbsp;"+
				"			<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;'	onclick='return resetAdditionalDocumentsForm()'>"+
								Utility.getLocaleValuePropByKey("lnkCancel", locale)+
				"			</a>"+
				"		</div>"+
				"		</div>"+
				"	</div>");
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection11.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
		}
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}
	
	@Transactional(readOnly=false)
	public String getGroup03_21(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup03_21");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		JobOrder jobOrder=null;
		JobCategoryMaster dspqPotfolioJC=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
			if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null)
				dspqPotfolioJC=jobOrder.getJobCategoryMaster();
		}
		
		StringBuffer sb=new StringBuffer();
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		
		//for Cert
		dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(7);
		sectionMasters.add(dspqSectionMaster);
		
		//for Substitute Teacher 
		dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(21);
		sectionMasters.add(dspqSectionMaster);

		//for TFA
		dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(23);
		sectionMasters.add(dspqSectionMaster);
		
		//for Certified Teacher
		dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(29);
		sectionMasters.add(dspqSectionMaster);
		
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		DistrictMaster dspqPotfolioDM=null;
		if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
			dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
		
		if(mapDspqRouterBySID!=null)
		{
			DspqRouter dspqRouterSection29=mapDspqRouterBySID.get(29);
			if(dspqRouterSection29!=null)
			{
				String sSectionName="tooltipSection29";
				sb.append("<div class='row'>");
				sb.append("<div class='col-sm-12 col-md-12' ><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection29, listToolTips)+"</div></div>");
				sb.append("</div>");
				
				sb.append(getInstructionForSection(dspqRouterSection29));
				
				boolean isNonTeacher=false;
				if(mapDspqRouterByFID.get(142)!=null)
				{
					sb.append("<div class='row'>");
					DspqRouter dspqRouter=mapDspqRouterByFID.get(142);
					if(dspqRouter!=null)
					{
						isNonTeacher=true;
						Boolean bIsNonTeacher=null;
						if(teacherExperience!=null && teacherExperience.getIsNonTeacher()!=null)
							bIsNonTeacher=teacherExperience.getIsNonTeacher();
						
						String sInputControlId="isNonTeacher";
						sb.append("<div class='col-sm-12 col-md-12'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>");
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						sb.append("<div class='row'>");
						sb.append("<div class='col-sm-12 col-md-12'>");
						sb.append("<label class='radio' style='margin-top: 2px;margin-bottom: 2px;'>");
						if(bIsNonTeacher!=null && bIsNonTeacher.equals(true))
							sb.append("<input type='radio' checked value='1' id='isNonTeacher1' name='"+sInputControlId+"' onclick=\"JAFchkNonTeacher('1')\">"+Utility.getLocaleValuePropByKey("lblYes", locale));
						else 
							sb.append("<input type='radio' value='1' id='isNonTeacher1' name='"+sInputControlId+"' onclick=\"JAFchkNonTeacher('1')\">"+Utility.getLocaleValuePropByKey("lblYes", locale));
						sb.append("</label>");
						sb.append("</div>");
						sb.append("<div class='col-sm-12 col-md-12'>");
						sb.append("<label class='radio' style='margin-top: 2px;margin-bottom: 2px;'>");
						if(bIsNonTeacher!=null && bIsNonTeacher.equals(false))
							sb.append("<input type='radio' checked value='0' id='isNonTeacher0' name='"+sInputControlId+"' onclick=\"JAFchkNonTeacher('0')\">"+Utility.getLocaleValuePropByKey("lblNo", locale));
						else
							sb.append("<input type='radio' value='0' id='isNonTeacher0' name='"+sInputControlId+"' onclick=\"JAFchkNonTeacher('0')\">"+Utility.getLocaleValuePropByKey("lblNo", locale));
						sb.append("</label>");
						sb.append("</div>");
						sb.append("</div>");
						sb.append("</div>");
					
					}
					sb.append("</div>");
				}
				
				
				if(mapDspqRouterByFID.get(143)!=null  && isNonTeacher)
				{
					sb.append("<div class='row hide' id='expCertTeacherTrainingDivId'>");
					DspqRouter dspqRouter=mapDspqRouterByFID.get(143);
					if(dspqRouter!=null)
					{
						Double dExpCertTeacherTraining=0.0;
						if(teacherExperience!=null && teacherExperience.getExpCertTeacherTraining()!=null)
							dExpCertTeacherTraining=teacherExperience.getExpCertTeacherTraining();
							
						String sInputControlId="expCertTeacherTraining";
						sb.append("<div class='col-sm-11 col-md-11'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' id='"+sInputControlId+"' class='form-control' value='"+dExpCertTeacherTraining+"' "+getInputCustomeAttribute(dspqRouter)+" onkeypress='return checkForDecimalTwo(event)' maxlength='5' style='width: 150px;' />"+
						"</div>");
					}
					sb.append("</div>");
				}
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection29.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
			//**************************************
			
			DspqRouter dspqRouterSection7=mapDspqRouterBySID.get(7);
			if(dspqRouterSection7!=null)
			{
				String sSectionName="tooltipSection7";
				sb.append("<div class='row'>");
				sb.append("<div class='col-sm-12 col-md-12' ><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection7, listToolTips)+"</div></div>");
				sb.append("</div>");
				
				sb.append(getInstructionForSection(dspqRouterSection7));
				
				boolean isDispalyNBCY=false;
				if(mapDspqRouterByFID.get(31)!=null)
				{
					sb.append("<div class='row'>");
					DspqRouter dspqRouter=mapDspqRouterByFID.get(31);
					if(dspqRouter!=null)
					{
						isDispalyNBCY=true;
						Boolean bNationalBoardCert=null;
						if(teacherExperience!=null && teacherExperience.getNationalBoardCert()!=null)
							bNationalBoardCert=teacherExperience.getNationalBoardCert();
						
						String sInputControlId="nationalBoardCert";
						sb.append("<div class='col-sm-12 col-md-12'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>");
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						sb.append("<div class='row'>");
						sb.append("<div class='col-sm-12 col-md-12'>");
						sb.append("<label class='radio' style='margin-top: 2px;margin-bottom: 2px;'>");
						if(bNationalBoardCert!=null && bNationalBoardCert.equals(true))
							sb.append("<input type='radio' checked value='1' id='nbc1' name='"+sInputControlId+"' onclick=\"chkNBCert('1')\">"+Utility.getLocaleValuePropByKey("lblYes", locale));
						else 
							sb.append("<input type='radio' value='1' id='nbc1' name='"+sInputControlId+"' onclick=\"chkNBCert('1')\">"+Utility.getLocaleValuePropByKey("lblYes", locale));
						sb.append("</label>");
						sb.append("</div>");
						sb.append("<div class='col-sm-12 col-md-12'>");
						sb.append("<label class='radio' style='margin-top: 2px;margin-bottom: 2px;'>");
						if(bNationalBoardCert!=null && bNationalBoardCert.equals(false))
							sb.append("<input type='radio' checked value='0' id='nbc0' name='"+sInputControlId+"' onclick=\"chkNBCert('0')\">"+Utility.getLocaleValuePropByKey("lblNo", locale));
						else
							sb.append("<input type='radio' value='0' id='nbc0' name='"+sInputControlId+"' onclick=\"chkNBCert('0')\">"+Utility.getLocaleValuePropByKey("lblNo", locale));
						sb.append("</label>");
						sb.append("</div>");
						sb.append("</div>");
						sb.append("</div>");
					
					}
					sb.append("</div>");
				}
				
				if(mapDspqRouterByFID.get(30)!=null && isDispalyNBCY)
				{
					List<String> lstNationalBoardCertYear=Utility.getLasterYeartillAdvanceYear(1990);
					DspqRouter dspqRouter=mapDspqRouterByFID.get(30);
					if(dspqRouter!=null)
					{
						sb.append("<div class='row'>");
						Integer NationalBoardCertYear=null;
						if(teacherExperience!=null && teacherExperience.getNationalBoardCertYear()!=null)
							NationalBoardCertYear=teacherExperience.getNationalBoardCertYear();
						
						String sInputControlId="nationalBoardCertYear";
						sb.append("<div class='col-sm-4 col-md-4' id='divNBCY'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
									"	<select  class='form-control' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" > "+
									"		<option value='0' id='psl'>"+Utility.getLocaleValuePropByKey("optSlt", locale)+"</option>");
											for(String nbsy : lstNationalBoardCertYear)
											{
												int iCorps=Utility.getIntValue(nbsy);
												if(NationalBoardCertYear!=null && NationalBoardCertYear==iCorps)
													sb.append("<option id='nationalBoardCertYear"+nbsy+"' selected value='"+nbsy+"'>"+nbsy+"</option>");
												else
													sb.append("<option id='nationalBoardCertYear"+nbsy+"' value='"+nbsy+"'>"+nbsy+"</option>");
											}			
						sb.append("</select></div>");
						
						sb.append("</div>");
					}		
				}
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection7.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
			
			//Can Sub
			DspqRouter dspqRouterSection21=mapDspqRouterBySID.get(21);
			if(dspqRouterSection21!=null)
			{
				String sSectionName="tooltipSection21";
				sb.append("<div class='row'>");
				sb.append("<div class='col-sm-12 col-md-12' ><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection21, listToolTips)+"</div></div>");
				sb.append("</div>");
				
				sb.append(getInstructionForSection(dspqRouterSection21));
				
				sb.append("<div class='row'>");
				if(mapDspqRouterByFID.get(98)!=null)
				{
					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(98);
					if(dspqRouter!=null)
					{
						Integer canServeAsSubTeacher=null;
						if(teacherExperience!=null && teacherExperience.getCanServeAsSubTeacher()!=null)
							canServeAsSubTeacher=teacherExperience.getCanServeAsSubTeacher();
						
						String sInputControlId="Substitute_Teacher";
						sb.append("<div class='col-sm-12 col-md-12'><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div></div>");
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						sb.append("<div class='col-sm-12 col-md-12'>");
						sb.append("<label class='radio' style='margin-top: 2px;margin-bottom: 2px;'>");
						
						if(canServeAsSubTeacher!=null && canServeAsSubTeacher==1)
							sb.append("<input type='radio' id='canServeAsSubTeacher1' value='1' checked name='"+sInputControlId+"' >"+Utility.getLocaleValuePropByKey("lblYes", locale));
						else 
							sb.append("<input type='radio' id='canServeAsSubTeacher1' value='1' name='"+sInputControlId+"' >"+Utility.getLocaleValuePropByKey("lblYes", locale));
						
						sb.append("</label>");
						sb.append("</div>");
						
						sb.append("<div class='col-sm-12 col-md-12'>");
						sb.append("<label class='radio' style='margin-top: 2px;margin-bottom: 2px;'>");
						
						if(canServeAsSubTeacher!=null && canServeAsSubTeacher==0)
							sb.append("<input type='radio' id='canServeAsSubTeacher0' checked value='0' name='"+sInputControlId+"' >"+Utility.getLocaleValuePropByKey("lblNo", locale));
						else
							sb.append("<input type='radio' id='canServeAsSubTeacher0' value='0' name='"+sInputControlId+"' >"+Utility.getLocaleValuePropByKey("lblNo", locale));
						
						sb.append("</label>");
						sb.append("</div>");
						
					}
				}
				sb.append("</div>");
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection21.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
			
			//TFA
		
			DspqRouter dspqRouterSection23=mapDspqRouterBySID.get(23);
			if(dspqRouterSection23!=null)
			{
				String sSectionName="tooltipSection23";
				sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;padding-right:0px;'>"+
				"				<div style='float: left' class='portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection23, listToolTips)+
				"				 </div>"+
				"				<div style='clear: both;'></div>	"+
				"		  </div>");
				
				
				sb.append("<div class='divErrorMsg' id='errordiv_bottomPart_TFA' style='display: block;'></div>");
				
				sb.append(getInstructionForSection(dspqRouterSection23));
				
				//--------- Input Field start
				sb.append("<div class='row'>");
				//Substitute Teacher
				printdata("TFA");
				if(mapDspqRouterByFID.get(101)!=null)
				{
					List<TFAAffiliateMaster> lstTFAAffiliateMaster	=	tfaAffiliateMasterDAO.findByCriteria(Order.asc("tfaAffiliateName"));
					DspqRouter dspqRouter=mapDspqRouterByFID.get(101);
					if(dspqRouter!=null)
					{
						Integer tfaAffiliate=null;
						if(teacherExperience!=null && teacherExperience.getTfaAffiliateMaster()!=null && teacherExperience.getTfaAffiliateMaster().getTfaAffiliateId()!=null)
							tfaAffiliate=teacherExperience.getTfaAffiliateMaster().getTfaAffiliateId();
						
						String sInputControlId="tfaAffiliate";
						sb.append("<div class='col-sm-4 col-md-4'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
									"	<select  class='form-control' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" onchange='hideTFAFieldsNJAF()'>   "+
									"		<option value='0'>"+Utility.getLocaleValuePropByKey("lblSelectTFA", locale)+"</option>");
												for(TFAAffiliateMaster tfaMAster : lstTFAAffiliateMaster)
												{
													if(tfaAffiliate!=null && tfaAffiliate==tfaMAster.getTfaAffiliateId())
														sb.append("<option id='"+tfaMAster.getTfaAffiliateId()+"' selected value='"+tfaMAster.getTfaAffiliateId()+"'>"+tfaMAster.getTfaAffiliateName()+"</option>");
													else
														sb.append("<option id='"+tfaMAster.getTfaAffiliateId()+"' value='"+tfaMAster.getTfaAffiliateId()+"'>"+tfaMAster.getTfaAffiliateName()+"</option>");
												}			
						sb.append("</select></div>");
					}		
				}
				
				sb.append("<div id='tfaFieldsDiv'>");
				printdata("corps");
				if(mapDspqRouterByFID.get(102)!=null)
				{
					List<String> lstCorpsYear=Utility.getLasterYeartillAdvanceYear(1990);
					DspqRouter dspqRouter=mapDspqRouterByFID.get(102);
					if(dspqRouter!=null)
					{
						Integer corpsYear=null;
						if(teacherExperience!=null && teacherExperience.getCorpsYear()!=null)
							corpsYear=teacherExperience.getCorpsYear();
						
						String sInputControlId="corpsYear";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
									"	<select  class='form-control' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" > "+
									"		<option value='0'>"+Utility.getLocaleValuePropByKey("lblSltCorpsYear", locale)+"</option>");
											for(String corps : lstCorpsYear)
											{
												int iCorps=Utility.getIntValue(corps);
												if(corpsYear!=null && corpsYear==iCorps)
													sb.append("<option id='corps"+corps+"' selected value='"+corps+"'>"+corps+"</option>");
												else
													sb.append("<option id='corps"+corps+"' value='"+corps+"'>"+corps+"</option>");
											}			
						sb.append("</select></div>");
					}		
				}
				
				printdata("tfaRegion");
				if(mapDspqRouterByFID.get(103)!=null)
				{
					List<TFARegionMaster> lstTFARegionMaster		=	tfaRegionMasterDAO.findByCriteria(Order.asc("tfaRegionName"));
					DspqRouter dspqRouter=mapDspqRouterByFID.get(103);
					if(dspqRouter!=null)
					{
						Integer iTFARegion=null;
						if(teacherExperience!=null && teacherExperience.getTfaRegionMaster()!=null && teacherExperience.getTfaRegionMaster().getTfaRegionId()!=null)
							iTFARegion=teacherExperience.getTfaRegionMaster().getTfaRegionId();
						
						String sInputControlId="tfaRegion";
						sb.append("<div class='col-sm-5 col-md-5'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
									"	<select  class='form-control' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+">   "+
									"		<option value='0'>"+Utility.getLocaleValuePropByKey("lblSelectTFARegion", locale)+"</option>");
												for(TFARegionMaster tfaRegion : lstTFARegionMaster)
												{
													if(iTFARegion!=null && tfaRegion.getTfaRegionId()==iTFARegion)
														sb.append("<option id='"+tfaRegion.getTfaRegionId()+"' selected value='"+tfaRegion.getTfaRegionId()+"'>"+tfaRegion.getTfaRegionName()+"</option>");
													else
														sb.append("<option id='"+tfaRegion.getTfaRegionId()+"' value='"+tfaRegion.getTfaRegionId()+"'>"+tfaRegion.getTfaRegionName()+"</option>");
												}				
						sb.append("</select></div>");
					}		
				}
				sb.append("</div>");
				sb.append("</div>");
				//--------- Input Field end
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection23.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
		}
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}
	
	
	@Transactional(readOnly=false)
	public String getGroup03_27(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup03_27");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		JobOrder jobOrder=null;
		JobCategoryMaster dspqPotfolioJC=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
			if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null)
				dspqPotfolioJC=jobOrder.getJobCategoryMaster();
		}
		
		StringBuffer sb=new StringBuffer();
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		
		//for Pass/Fail General Knowledge Exam 
		dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(27);
		sectionMasters.add(dspqSectionMaster);

		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		DistrictMaster dspqPotfolioDM=null;
		if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
			dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		//TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
		
		if(mapDspqRouterBySID!=null)
		{
			DspqRouter dspqRouterSection27=mapDspqRouterBySID.get(27);
			if(dspqRouterSection27!=null)
			{
				String sSectionName="tooltipSection27";
				sb.append("<div class='row'>");
				sb.append("<div class='col-sm-12 col-md-12' ><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection27, listToolTips)+"</div></div>");
				sb.append("</div>");
				
				sb.append(getInstructionForSection(dspqRouterSection27));
				
				if(mapDspqRouterByFID.get(145)!=null || mapDspqRouterByFID.get(146)!=null || mapDspqRouterByFID.get(147)!=null)
				{
					
					TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam = teacherGeneralKnowledgeExamDAO.findTeacherGeneralKnowledgeExam(teacherDetail);
					
					sb.append("<iframe id='multifileuploadformiframe' name='ifrmjafFrmMultifileuploadform' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");
					
					sb.append("<form id='jafFrmMultifileuploadform' enctype='multipart/form-data' method='post' target='ifrmjafFrmMultifileuploadform' action='teacherGeneralKnowledgeServlet.do'>");
					sb.append("<input type='hidden' id='uploadFileNumber' name='uploadFileNumber' value='1'/>");
					
					sb.append("<div class='row'>");
					if(mapDspqRouterByFID.get(145)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(145);
						if(dspqRouter!=null)
						{
							String sGeneralKnowledgeExamStatusValue="";
							if(teacherGeneralKnowledgeExam!=null && teacherGeneralKnowledgeExam.getGeneralKnowledgeExamStatus()!=null && !teacherGeneralKnowledgeExam.getGeneralKnowledgeExamStatus().equals(""))
								sGeneralKnowledgeExamStatusValue=teacherGeneralKnowledgeExam.getGeneralKnowledgeExamStatus();
							
							String sInputControlId="generalKnowledgeExamStatus";
							sb.append("<div class='col-sm-3 col-md-3'>"+
									"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
										"	<select  class='form-control' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+">   "+
										"		<option value='0'>"+Utility.getLocaleValuePropByKey("sltStatis", locale)+"</option>");
										if(sGeneralKnowledgeExamStatusValue.equalsIgnoreCase("P"))
										{
											sb.append("<option id='gkstatus_' selected value='P'>"+Utility.getLocaleValuePropByKey("optPass", locale)+"</option>");
										}
										else
										{
											sb.append("<option id='gkstatus_' value='P'>"+Utility.getLocaleValuePropByKey("optPass", locale)+"</option>");
										}
										
										if(sGeneralKnowledgeExamStatusValue.equalsIgnoreCase("F"))
										{
											sb.append("<option id='gkstatus_' selected value='F'>"+Utility.getLocaleValuePropByKey("optFail", locale)+"</option>");
										}
										else
										{
											sb.append("<option id='gkstatus_' value='F'>"+Utility.getLocaleValuePropByKey("optFail", locale)+"</option>");
										}
																	
							sb.append("</select></div>");
						}
					}
					
					if(mapDspqRouterByFID.get(146)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(146);
						if(dspqRouter!=null)
						{
							String sGeneralKnowledgeExamStatusValue=null;
							if(teacherGeneralKnowledgeExam!=null && teacherGeneralKnowledgeExam.getGeneralKnowledgeExamDate()!=null && !teacherGeneralKnowledgeExam.getGeneralKnowledgeExamDate().equals(""))
								sGeneralKnowledgeExamStatusValue=Utility.convertDateAndTimeToDatabaseformatOnlyDate(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamDate());
								
							String sInputControlId="generalKnowledgeExamDate";
							sb.append("<div class='col-sm-3 col-md-3'>"+
									"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
									"<input type='text' id='"+sInputControlId+"' class='form-control' value='"+sGeneralKnowledgeExamStatusValue+"' "+getInputCustomeAttribute(dspqRouter)+" />"+
							"</div>");
							
							listCalDated.add(sInputControlId);
						}
					}
					
					if(mapDspqRouterByFID.get(147)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(147);
						if(dspqRouter!=null)
						{
							String db_generalKnowledgeScoreReport="";
							if(teacherGeneralKnowledgeExam!=null && teacherGeneralKnowledgeExam.getGeneralKnowledgeScoreReport()!=null && !teacherGeneralKnowledgeExam.getGeneralKnowledgeScoreReport().equals(""))
								db_generalKnowledgeScoreReport=teacherGeneralKnowledgeExam.getGeneralKnowledgeScoreReport();
							
							String sInputControlId="generalKnowledgeScoreReport";
							sb.append("<div class='col-sm-3 col-md-3'>"+
							"<input type='hidden' id='db_"+sInputControlId+"' value='"+db_generalKnowledgeScoreReport+"'>"+
							"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
							"<input id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"' type='file' width='20px;'>"+
							"<a href='javascript:void(0)' onclick='clearGeneralKnowledgeScoreReport()'>"+Utility.getLocaleValuePropByKey("lnkClear", locale)+"</a>"+
							"</div>");
							
							sb.append("<div class='col-sm-3 col-md-3'>");
							if(db_generalKnowledgeScoreReport!=null && !db_generalKnowledgeScoreReport.equals(""))
							{
								sb.append("<span>Recent score report on file:</BR><a href='javascript:void(0)' id='hrefGKnw' onclick=\"jafDownloadGeneralKnowledge('"+teacherGeneralKnowledgeExam.getGeneralKnowledgeExamId()+"','hrefGKnw');if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;\">Click Here</a></span>");
							}
							else
							{
								sb.append("None");
							}
							
							sb.append("</div>");		
						}
					}
					
					if(mapDspqRouterByFID.get(153)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(153);
						if(dspqRouter!=null)
						{
							String generalExamNote="";
							if(teacherGeneralKnowledgeExam!=null && teacherGeneralKnowledgeExam.getGeneralExamNote()!=null && !teacherGeneralKnowledgeExam.getGeneralExamNote().equals(""))
								generalExamNote=teacherGeneralKnowledgeExam.getGeneralExamNote();
							
							String sInputControlId="generalExamNote";
							sb.append("<div class='col-sm-11 col-md-11'>"+
							"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
							"<div id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+"><textarea rows='8' style='width:100%;' id='txtArea_"+sInputControlId+"'>"+generalExamNote+"</textarea></div>"+
							"</div>");
						}
					}
					
					sb.append("</div>");
					sb.append("</form>");
				}
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection27.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
		}
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}
	
	@Transactional(readOnly=false)
	public String getGroup03_28(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup03_28");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		JobOrder jobOrder=null;
		JobCategoryMaster dspqPotfolioJC=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);		
			
		}
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		StringBuffer sb=new StringBuffer();
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		
		//for Pass/Fail General Knowledge Exam 
		dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(28);
		sectionMasters.add(dspqSectionMaster);

		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		DistrictMaster dspqPotfolioDM=null;
		if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
			dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
		List<SubjectAreaExamMaster> subjectAreaExamMastersList=null;
		subjectAreaExamMastersList=subjectAreaExamMasterDAO.findActiveSubjectExamByDistrict(dspqPotfolioDM);
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
		
		if(mapDspqRouterBySID!=null)
		{
			DspqRouter dspqRouterSection28=mapDspqRouterBySID.get(28);
			if(dspqRouterSection28!=null)
			{
				String sSubSection=getAddSubSectionText(dspqRouterSection28);
				String sSectionName="tooltipSection28";
				sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;padding-right:0px;'>"+
				"				<div style='float: left' class='portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection28, listToolTips)+
				"				 </div>"+
				"					<div style='float: right' class='addPortfolio'>"+
				"					   <a href='javascript:void(0);' id='addSubjectAreaIdDSPQ' onclick='showSubjectAreaForm()' >"+sSubSection+"</a>"+
				"				    </div>"+
				"				<div style='clear: both;'></div>	"+
				"		  </div>");
				
				sb.append(getInstructionForSection(dspqRouterSection28));
				
				sb.append("<div class='row' onmouseover=\"setGridNameFlag('subjectAreas')\">"+
				"	        <div class='col-sm-12 col-md-12'>"+
				"			  <div id='divGridSubjectAreas'></div>"+
				"		    </div>"+
				"	      </div>");
				
				
				sb.append("<div class='portfolio_Section_ImputFormGap' id='divSubjectArea' style='display: none;'>"+
				"			<iframe id='uploadFrameSubjectArea' name='uploadFrameSubjectArea' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>"+
				"			<div class='divErrorMsg' id='errSubjectArea' style='display: block;'></div>"+
				"			<form id='frmSubjectArea' name='frmSubjectArea' enctype='multipart/form-data' method='post' target='uploadFrameSubjectArea' action='teacherSubjectAreaServlet.do' >");
				
				sb.append("<input type='hidden' id='sbtsource_subArea' name='sbtsource_subArea' value='0'/>");
				sb.append("<input type='hidden' id='teacherSubjectAreaExamId'>");
				//--------- Input Field start
			
				sb.append(getInstructionForSection(dspqRouterSection28));
				//Status
				sb.append("<div class='row'>");
				if(mapDspqRouterByFID.get(148)!=null)
				{
					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(148);
					if(dspqRouter!=null)
					{
						String sInputControlId="examStatus";
						sb.append("<div class='col-sm-3 col-md-3' >"+
								"<label>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</label>"+
								"<select  class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' >"+   
								"	<option value='0'>"+Utility.getLocaleValuePropByKey("sltStatis", locale)+"</option>"+
								"	<option value='P'>"+Utility.getLocaleValuePropByKey("optPass", locale)+"</option>"+
								"	<option value='F'>"+Utility.getLocaleValuePropByKey("optFail", locale)+"</option>");															
								sb.append("</select>");
						sb.append("</div>");					
					}
					
				}
				
				//Exam Date
				if(mapDspqRouterByFID.get(149)!=null)
				{					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(149);
					if(dspqRouter!=null)
					{						
						String sInputControlId="examDate";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' id='"+sInputControlId+"' class='form-control'  "+getInputCustomeAttribute(dspqRouter)+" />"+
						"</div>");
						
						listCalDated.add(sInputControlId);
					}
					
				}
				//Subject
				if(mapDspqRouterByFID.get(150)!=null)
				{					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(150);
					if(dspqRouter!=null)
					{						
						String sInputControlId="subjectIdforDSPQ";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<select  class='form-control' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+">   "+
								"		<option value='0'>"+Utility.getLocaleValuePropByKey("optStlSub", locale)+"</option>");
								for(SubjectAreaExamMaster subjectArea : subjectAreaExamMastersList){
										if(subjectArea!=null && subjectArea.getSubjectAreaExamName()!=null)
											sb.append("<option id='"+subjectArea.getSubjectAreaExamId()+"'  value='"+subjectArea.getSubjectAreaExamId()+"'>"+subjectArea.getSubjectAreaExamName()+"</option>");
									}				
								sb.append("</select></div>");
					}
					
				}
				/// Score Report
				if(mapDspqRouterByFID.get(151)!=null)
				{					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(151);
					if(dspqRouter!=null)
					{
						String sInputControlIdHidden="scoreReportHidden"; 
						String sInputControlId="scoreReport";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"<input type='hidden' id="+sInputControlIdHidden+" value='0'/>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input id="+sInputControlId+" name="+sInputControlId+" type='file'  width='20px;'  "+getInputCustomeAttribute(dspqRouter)+" />"+
						"</div>");
					}					
				}				
				
				/// Notes
				if(mapDspqRouterByFID.get(162)!=null)
				{			
					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(162);
					if(dspqRouter!=null)
					{
						String sInputControlId="subjectExamTextarea";
							 sb.append("<div class='col-sm-11 col-md-11'>"+								
								"		<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	    <div id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+"><textarea rows='8' style='width:100%;' id='txtArea_"+sInputControlId+"'></textarea></div>"+
								
								"</div>");
					}
					
				}
				sb.append("</div>");
				sb.append("</form>"+
						" 		<div class='row'>"+
						"	    <div class='col-sm-11 col-md-11 top10 idone'>"+
						"			<a id='hrefDone' href='javascript:void(0)' style='cursor: pointer; margin-left: 0px; text-	;' onclick='return insertOrUpdateSubjectArea();' >"+Utility.getLocaleValuePropByKey("lnkImD", locale)+"</a>&nbsp;&nbsp;"+
						"			<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;'	onclick='return hideSubjectAreaForm()'>"+
										Utility.getLocaleValuePropByKey("lnkCancel", locale)+
						"			</a>"+
						"		</div>"+
						"		</div>"+
						"	</div>");
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection28.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
			
		}
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}	
	
	@Transactional(readOnly=false)
	public String getGroup04(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup04");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		String locale = Utility.getValueOfPropByKey("locale");
		StringBuffer sb=new StringBuffer();
		sb.append("<div class='row'>" +
			"<div class='col-sm-9 col-md-9'><div class='mt10'><button class='flatbtn' id='btnprev' type='button' style='width:16%' onclick='return getPreviousPage();'> <i class='backicon'></i> "+Utility.getLocaleValuePropByKey("btnPrevDSPQ", locale)+"</button>&nbsp;&nbsp;<button class='flatbtn' style='background:#F89507;width:20%'  type='button' onclick='return validateDSPQProfessionalGroup(1);'>"+Utility.getLocaleValuePropByKey("btnSaveExit", locale)+" <i class='fa fa-check-circle'></i></button>&nbsp;&nbsp;<button class='flatbtn' style='background: #da4f49;width:16%'  type='button' onclick='return cancelApplyJob();'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+" <i class='fa fa-times-circle'></i></button></div></div>" +
			"<div class='col-sm-3 col-md-3'><div class='mt10' style='text-align:right;'><button class='flatbtn' style='width:90%;' id='btnnext' type='button' onclick='return validateDSPQProfessionalGroup(0);'>"+Utility.getLocaleValuePropByKey("btnSaveAndNextDSPQ", locale)+" <i class='icon'></i></button></div></div>" +
			"</div>");
		
		String sReturnToolTips="";
		return sb.toString()+"|||"+sReturnToolTips;
	}
	
	@Transactional(readOnly=false)
	public String getGroup04Section25Name(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup04Section25Name");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		//TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		String locale = Utility.getValueOfPropByKey("locale");
		StringBuffer sb=new StringBuffer();
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(25);
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		sectionMasters.add(dspqSectionMaster);
		
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		
		if(mapDspqRouterBySID!=null)
		{
			DspqRouter dspqRouterSection25=mapDspqRouterBySID.get(25);
			if(dspqRouterSection25!=null)
			{
				String sSectionName="tooltipSection25";
				sb.append("<div class='row'>"+
						"	<div class='col-sm-11 col-md-11'>"+
						"		<div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection25, listToolTips)+"</div>"+
						"	</div>"+
						"</div>");
				sb.append(getInstructionForSection(dspqRouterSection25));
			}
		}
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		return sb.toString()+"|||"+sReturnToolTips;
	}
	
	
	@Transactional(readOnly=false)
	public String getGroup04Section25(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup04Section25");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		StringBuffer sb=new StringBuffer();
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		
		dspqSectionMaster.setSectionId(25);
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		sectionMasters.add(dspqSectionMaster);
		
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		
		//DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		
		if(mapDspqRouterBySID!=null)
		{
			
			
			DspqRouter dspqRouterSection25=mapDspqRouterBySID.get(25);
			if(dspqRouterSection25!=null)
			{
				
				
				TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				boolean isAffidavitCompleted=false;
				if(teacherPortfolioStatus!=null && teacherPortfolioStatus.getIsAffidavitCompleted()!=null && teacherPortfolioStatus.getIsAffidavitCompleted())
				{
					isAffidavitCompleted=teacherPortfolioStatus.getIsAffidavitCompleted();
				}
				
				if(mapDspqRouterByFID.get(120)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(120);
					if(dspqRouter!=null)
					{
						
						
						String sInputControlId="affidavit";
						sb.append("<div class='row'>");
						sb.append("<div class='col-sm-11 col-md-11'><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div></div>");
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						sb.append("<div class='col-sm-11 col-md-11 hide'><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div></div>");
						
						sb.append("<div class='col-sm-11 col-md-11'>");
						sb.append("<label class='checkbox inline' style='margin-top: 2px;margin-bottom: 2px;'>");
						
						if(isAffidavitCompleted)
							sb.append("<input type='checkbox' checked name='"+sInputControlId+"' id='"+sInputControlId+"' value='1' >"+Utility.getLocaleValuePropByKey("pConfirm", locale)+"</br>");
						else
							sb.append("<input type='checkbox' name='"+sInputControlId+"' id='"+sInputControlId+"' value='1'>"+Utility.getLocaleValuePropByKey("pConfirm", locale)+"</br>");
						
						sb.append("</label>");
						sb.append("</div>");
						sb.append("</div>");
					}
				}
			}
		}
		
		sb.append("<div class='row'>" +
				"<div class='col-sm-9 col-md-9'><div class='mt10'><button class='flatbtn' id='btnprev' type='button' style='width:16%' onclick='return getPreviousPage();'> <i class='backicon'></i> "+Utility.getLocaleValuePropByKey("btnPrevDSPQ", locale)+"</button>&nbsp;&nbsp;<button class='flatbtn' style='background:#F89507;width:20%'  type='button' onclick='return validateDSPQG4Section25(1);'>"+Utility.getLocaleValuePropByKey("btnSaveExit", locale)+" <i class='fa fa-check-circle'></i></button>&nbsp;&nbsp;<button class='flatbtn' style='background: #da4f49;width:16%'  type='button' onclick='return cancelApplyJob();'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+" <i class='fa fa-times-circle'></i></button></div></div>" +
				"<div class='col-sm-3 col-md-3'><div class='mt10' style='text-align:right;'><button class='flatbtn' style='width:90%;' id='btnnext' type='button' onclick='return validateDSPQG4Section25(0);'>"+Utility.getLocaleValuePropByKey("btnSaveAndNextDSPQ", locale)+" <i class='icon'></i></button></div></div>" +
				"</div>");
		
		String sReturnToolTips="";
		return sb.toString()+"|||"+sReturnToolTips;
	}
	
	//Open area on Experience
	@Transactional(readOnly=false)
	public String getGroup04_12(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup04_12");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		JobOrder jobOrder=null;
		JobCategoryMaster dspqPotfolioJC=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
			if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null)
				dspqPotfolioJC=jobOrder.getJobCategoryMaster();
		}
		
		StringBuffer sb=new StringBuffer();
		
		TeacherPersonalInfo teacherpersonalinfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		
		dspqSectionMaster.setSectionId(12);
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		sectionMasters.add(dspqSectionMaster);
		
		dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(24);
		sectionMasters.add(dspqSectionMaster);
		
		dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(26);
		sectionMasters.add(dspqSectionMaster);
		
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		if(mapDspqRouterBySID!=null)
		{
			DistrictMaster dspqPotfolioDM=null;
			if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
				dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
			
			DspqRouter dspqRouterSection12=mapDspqRouterBySID.get(12);
			if(dspqRouterSection12!=null)
			{
				
				TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
				String sResumeName="";
				if(teacherExperience!=null && teacherExperience.getResume()!=null && !teacherExperience.getResume().equalsIgnoreCase(""))
					sResumeName=teacherExperience.getResume();
				
				String sSectionName="tooltipSection12";
				
				sb.append("<div class='row'>"+
						"	<div class='col-sm-12 col-md-12'>"+
						"		<div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection12, listToolTips)+"</div>"+
						"	</div>"+
						"</div>");
				
				sb.append("<div id='resumeDiv' class='col-sm-12 col-md-12' style='padding-left:0px;'>"+
						"<iframe id='uploadFrameResumeID' name='uploadFrameResume' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");//+
						//"<form id='frmExpResumeUpload' enctype='multipart/form-data' method='post' target='uploadFrameResume' action='selfServiceResumeUploadServlet.do?f="+sResumeName+"' class='form-inline'>	");
				//--------- Input Field start
				
				//Salutation
				printdata("Resume");
				if(mapDspqRouterByFID.get(58)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(58);
					if(dspqRouter!=null)
					{
						String sInputControlId="resume";
						sb.append(getInstructionForSection(dspqRouterSection12));
						sb.append("<div class='row'>"+								
								"		<div class='col-sm-12 col-md-12'>"+
								"			 <div class='divErrorMsg' id='errordiv_bottomPart_resume' style='display: block;'></div>"+
								"		</div>	"+
								"		<div class='col-sm-12 col-md-12'> "+
								"		<div class='row'> "+
								"		<div class='col-sm-3 col-md-3'> "+
								"			<input type='hidden' id='hdnResume' value='' /> "+
								"				<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>");
						
								if(sResumeName!=null && !sResumeName.equals("") && !sResumeName.equals("0"))
									sb.append("	<input type='hidden' id='db_"+sInputControlId+"' name='db_"+sInputControlId+"' value='"+sResumeName+"'/>");
								else
									sb.append("	<input type='hidden' id='db_"+sInputControlId+"' name='db_"+sInputControlId+"' value='0'/>");
								
								sb.append("			<input name='"+sInputControlId+"' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" type='file'>"+
								"			</div>"+
								"		<div class='col-sm-5 col-md-5' style='height:43px;'> "+
								"			<div id='divResumeTxt' style='margin-top:21px;'>"+
								"				 <label id='lblResume'>"+
													Utility.getLocaleValuePropByKey("lblRecentResOnFi", locale));
						
								if(sResumeName!=null && !sResumeName.equals(""))
								{
									sb.append("					<a href='javascript:void(0)' id='hrefResume' onclick=\"jafDownloadResume();if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;\">"+sResumeName+"</a>");
								}
								else
								{
									sb.append("None");
								}
								
								sb.append("</label>"+
								"			</div>"+
								"		</div>	"+
								"</div>"+
								"</div>"+
								//"</form>"+
								"</div>");
				}
			 }
				
				
			sb.append("</form>"+
			"	</div>");
				
			sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection12.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
			
			DspqRouter dspqRouterSection24=mapDspqRouterBySID.get(24);
			if(dspqRouterSection24!=null)
			{
				
				String sSectionName="tooltipSection24";
				sb.append("<div class='row'>");
				sb.append("<div class='col-sm-12 col-md-12'><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection24, listToolTips)+"</div></div>");
				sb.append("</div>");
				
				sb.append(getInstructionForSection(dspqRouterSection24));
				
				
				if(mapDspqRouterByFID.get(110)!=null)
				{
					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(110);
					if(dspqRouter!=null)
					{
						sb.append("<div class='row'>");
						String sInputControlId="isretired";
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						sb.append("<div class='col-sm-11 col-md-11'><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div></div>");
						
						sb.append("<div class='col-sm-11 col-md-11'>");
						sb.append("<label class='radio inline' style='margin-top: 2px;margin-bottom: 2px;'>");
						sb.append("<input type='radio' name='"+sInputControlId+"' id='"+sInputControlId+"_1' value='1' onclick='chkRetired()'>"+Utility.getLocaleValuePropByKey("lblYes", locale));
						sb.append("</label>");
						sb.append("</div>");
						
						sb.append("<div class='col-sm-11 col-md-11'>");
						sb.append("<label class='radio inline' style='margin-top: 2px;margin-bottom: 2px;'>");
						sb.append("<input type='radio' name='"+sInputControlId+"' id='"+sInputControlId+"_0' value='0' onclick='chkRetired()'>"+Utility.getLocaleValuePropByKey("lblNo", locale));
						sb.append("</label>");
						sb.append("</div>");
						
						sb.append("</div>");
						
					}
					
				}
				sb.append("<div class='row' id='isRetiredDiv'>");
				if(mapDspqRouterByFID.get(106)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(106);
					if(dspqRouter!=null)
					{
						int retireddistrictId=0;
						String retireddistrictName="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getDistrictmaster()!=null)
						{
							if(teacherpersonalinfo.getDistrictmaster().getDistrictId()!=null)
								retireddistrictId=teacherpersonalinfo.getDistrictmaster().getDistrictId();
							
							if(teacherpersonalinfo.getDistrictmaster().getDistrictName()!=null)
								retireddistrictName=teacherpersonalinfo.getDistrictmaster().getDistrictName();
						}
						
						
						String sInputControlId="retireddistrictName";
						String sInputControlId1="retireddistrictId";
						
						sb.append("<div class='col-sm-6 col-md-6'>"+
						"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"	<input  type='hidden' id='"+sInputControlId1+"' value='"+retireddistrictId+"' "+getInputCustomeAttribute(dspqRouter)+">"+
						"	<input  type='text' class='form-control' maxlength='50' autocomplete='off' id='"+sInputControlId+"' value='"+retireddistrictName+"' name='"+sInputControlId+"' onfocus=\"getDistrictMasterAutoComp(this, event, 'divTxtShowData1', '"+sInputControlId+"','"+sInputControlId1+"','');\" onkeyup=\"getDistrictMasterAutoComp(this, event, 'divTxtShowData1', '"+sInputControlId+"','"+sInputControlId1+"','');\"  onblur=\"hideDistrictMasterDiv(this,'"+sInputControlId1+"','divTxtShowData1');\"/>"+
						"	"+
						"	<div id='divTxtShowData1' style='display:none;position:absolute;z-index:5000;' onmouseover=\"mouseOverChk('divTxtShowData1','"+sInputControlId+"')\"  class='result' ></div>"+
						"</div>");
						
						
					}
				}
				
				if(mapDspqRouterByFID.get(105)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(105);
					if(dspqRouter!=null)
					{
						List<StateMaster> listStateMaster = stateMasterDAO.findAllStateByOrder();
						Long lstMForretire=null;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getStateMaster()!=null && teacherpersonalinfo.getStateMaster().getStateId()!=null)
							lstMForretire=teacherpersonalinfo.getStateMaster().getStateId();
						
						String sInputControlId="stMForretire";
						sb.append("<div class='col-sm-6 col-md-6' >"+
								"<label>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</label>"+
								"<select  class='form-control' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' >"+   
								"	<option value='0' id='slst'>"+Utility.getLocaleValuePropByKey("optSltSt", locale)+"</option>");
								if(listStateMaster!=null && listStateMaster.size() >0)
									for(StateMaster master :listStateMaster)
									{
										if(lstMForretire!=null && lstMForretire==master.getStateId())
										{
											sb.append("<option selected id='st'"+master.getStateId()+" shortname='"+master.getStateShortName()+"' value='"+master.getStateId()+"'>"+master.getStateName()+"</option>");
										}
										else
											sb.append("<option id='st'"+master.getStateId()+" shortname='"+master.getStateShortName()+"' value='"+master.getStateId()+"'>"+master.getStateName()+"</option>");
									}
								sb.append("</select></div>");
								
					
					}
				}
				
				if(mapDspqRouterByFID.get(104)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(104);
					if(dspqRouter!=null)
					{
						String sRetirementnumber="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getRetirementnumber()!=null)
							sRetirementnumber=teacherpersonalinfo.getRetirementnumber();
						
						String sInputControlId="retireNo";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"<input type='text' value='"+sRetirementnumber+"' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='"+sInputControlId+"' maxlength='10' class='form-control' />"+
						"</div>");
					}
				}
				sb.append("</div>");
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection24.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
						
			}
			
			
			DspqRouter dspqRouterSection26=mapDspqRouterBySID.get(26);
			if(dspqRouterSection26!=null)
			{
				String sSectionName="tooltipSection26";
				sb.append("<div class='row'>");
				sb.append("<div class='col-sm-12 col-md-12'><div class='span4 portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection26, listToolTips)+"</div></div>");
				sb.append("</div>");
				
				sb.append(getInstructionForSection(dspqRouterSection26));
				
				if(mapDspqRouterByFID.get(121)!=null || mapDspqRouterByFID.get(127)!=null || mapDspqRouterByFID.get(132)!=null)
				{
					int iEmployeeType=-1;
					if(teacherpersonalinfo!=null && teacherpersonalinfo.getEmployeeType()!=null)
						iEmployeeType=teacherpersonalinfo.getEmployeeType();
					
					boolean bRdHidden=false;
					String sInputControlName="employmentStatus";
					
					if(mapDspqRouterByFID.get(121)!=null) //I am a former employee of 
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(121);
						if(dspqRouter!=null)
						{
							String sInputControlId="employmentStatus_1";
							bRdHidden=true;
							sb.append("<input type='hidden' id='hdn_"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter)+" >");
							sb.append("<div class='row'>"); // Start new Row #1
							sb.append("<div class='col-sm-12 col-md-12'>");
							sb.append("<label class='radio inline' style='margin-top: 2px;margin-bottom: 2px;'>");
							
							String sShowHideByEmpStCssClass="hide";
							if(iEmployeeType==0)
							{
								sb.append("<input type='radio' name='"+sInputControlName+"' id='"+sInputControlId+"' checked value='0' onclick='jafShowEmpNoDiv(0);' ><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div>");
								sShowHideByEmpStCssClass="";
							}
							else
								sb.append("<input type='radio' name='"+sInputControlName+"' id='"+sInputControlId+"' value='0' onclick='jafShowEmpNoDiv(0);' ><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div>");
							
							sb.append("</label>");
							sb.append("</div>");
							
							sb.append("<div id='empDivId1' class='"+sShowHideByEmpStCssClass+"'>"); // Start show hide main div #1
							if(mapDspqRouterByFID.get(122)!=null)
							{
								DspqRouter dspqRouter122=mapDspqRouterByFID.get(122);
								if(dspqRouter122!=null)
								{
									String sEmployeeNumber="";
									if(teacherpersonalinfo!=null && teacherpersonalinfo.getEmployeeNumber()!=null)
										sEmployeeNumber=teacherpersonalinfo.getEmployeeNumber().trim();
									
									String sInputControlId122="employeeNumberOpt1";
									sb.append("<div class='col-sm-12 col-md-12' style='margin-left:20px;'>"+
											"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId122,dspqRouter122,listToolTips)+"</strong></label>"+
											"<input type='text' id='"+sInputControlId122+"' class='form-control' value='"+sEmployeeNumber+"' "+getInputCustomeAttribute(dspqRouter122)+" maxlength='50' onkeypress='return jafCheckForIntEmpNum(event);' style='width:200px;' />"+
									"</div>");
								}
							}
							
							if(mapDspqRouterByFID.get(123)!=null || mapDspqRouterByFID.get(124)!=null)
							{
								String chkChecked="";
								String sRetirementDate124=null;
								if(teacherpersonalinfo!=null && teacherpersonalinfo.getRetirementdate()!=null)
								{
									sRetirementDate124=Utility.convertDateAndTimeToDatabaseformatOnlyDate(teacherpersonalinfo.getRetirementdate());
									chkChecked="checked";
								}
								
								if(mapDspqRouterByFID.get(123)!=null)
								{
									DspqRouter dspqRouter123=mapDspqRouterByFID.get(123);
									sb.append("<div class='col-sm-12 col-md-12' style='margin-left:20px;'>");
									String sInputControlId123="chkRetiredFrom";
									
									sb.append("<input type='hidden' id='hdn_"+sInputControlId123+"' "+getInputCustomeAttribute(dspqRouter123)+" >");
									sb.append("<label class='checkbox'>");
									sb.append("<input type='checkbox' "+chkChecked+" value='1' id='"+sInputControlId123+"' name='"+sInputControlId123+"' "+getInputCustomeAttribute(dspqRouter123)+" onclick='jafShowDivForRetirementDate();'>");
									
									sb.append("<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId123,dspqRouter123,listToolTips)+"</strong>"+
									"</label>");
									sb.append("</div>");
								}
								
								if(mapDspqRouterByFID.get(124)!=null)
								{
									DspqRouter dspqRouter124=mapDspqRouterByFID.get(124);
									if(dspqRouter124!=null)
									{
										
										String sInputControlId124="RetirementDate";
										
										sb.append("<div class='col-sm-12 col-md-12 hide' style='margin-left:40px;' id='retirementDateDivId'>"+
												"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId124,dspqRouter124,listToolTips)+"</strong></label>");
										
										if(sRetirementDate124!=null && !sRetirementDate124.equals(""))		
											sb.append("<input type='text' id='"+sInputControlId124+"' class='form-control' value='"+sRetirementDate124+"' "+getInputCustomeAttribute(dspqRouter124)+" maxlength='50' style='width:200px;' />");
										else
											sb.append("<input type='text' id='"+sInputControlId124+"' class='form-control' value='' "+getInputCustomeAttribute(dspqRouter124)+" maxlength='50' style='width:200px;' />");
										
										sb.append("</div>");
										
										listCalDated.add(sInputControlId124);
									}
								}
							}
							
							
							if(mapDspqRouterByFID.get(125)!=null || mapDspqRouterByFID.get(126)!=null)
							{
								
								String chkChecked="";
								String sRetirementDate126=null;
								if(teacherpersonalinfo!=null && teacherpersonalinfo.getMoneywithdrawaldate()!=null)
								{
									sRetirementDate126=Utility.convertDateAndTimeToDatabaseformatOnlyDate(teacherpersonalinfo.getMoneywithdrawaldate());
									chkChecked="checked";
								}
								
								if(mapDspqRouterByFID.get(125)!=null)
								{
									DspqRouter dspqRouter125=mapDspqRouterByFID.get(125);
									
									sb.append("<div class='col-sm-12 col-md-12' style='margin-left:20px;'>");
									String sInputControlId125="memberWithdrawnFunds";
									
									sb.append("<input type='hidden' id='hdn_"+sInputControlId125+"' "+getInputCustomeAttribute(dspqRouter125)+" >");
									sb.append("<label class='checkbox'>");
									sb.append("<input type='checkbox' "+chkChecked+" value='1' id='"+sInputControlId125+"' name='"+sInputControlId125+"' "+getInputCustomeAttribute(dspqRouter125)+" onclick='jafDateOfWithdrawnDivId();'>");
									
									sb.append("<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId125,dspqRouter125,listToolTips)+"</strong>"+
									"</label>");
									sb.append("</div>");
								}
								
								if(mapDspqRouterByFID.get(126)!=null)
								{
									DspqRouter dspqRouter126=mapDspqRouterByFID.get(126);
									if(dspqRouter126!=null)
									{
										String sInputControlId126="dateOfWithdrawn";
										sb.append("<div class='col-sm-12 col-md-12 hide' style='margin-left:40px;' id='dateOfWithdrawnDivId'>"+
												"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId126,dspqRouter126,listToolTips)+"</strong></label>");
										
										if(sRetirementDate126!=null && !sRetirementDate126.equals(""))
											sb.append("<input type='text' id='"+sInputControlId126+"' class='form-control' value='"+sRetirementDate126+"' "+getInputCustomeAttribute(dspqRouter126)+" maxlength='50' style='width:200px;' />");
										else
											sb.append("<input type='text' id='"+sInputControlId126+"' class='form-control' value='' "+getInputCustomeAttribute(dspqRouter126)+" maxlength='50' style='width:200px;' />");
												
										sb.append("</div>");
										
										listCalDated.add(sInputControlId126);
									}
								}
								///
								
								if(mapDspqRouterByFID.get(152)!=null)//Why are you no longer employed with us?
								{
									DspqRouter dspqRouter152=mapDspqRouterByFID.get(152);
									if(dspqRouter152!=null)
									{
										String sNoLongerEmployed="";
										if(teacherpersonalinfo!=null && teacherpersonalinfo.getNoLongerEmployed()!=null && !teacherpersonalinfo.getNoLongerEmployed().equals(""))
											sNoLongerEmployed=teacherpersonalinfo.getNoLongerEmployed();
										
										String sInputControlId152="noLongerEmployed";
										sb.append("<div class='row' style='margin-left:0px;'>"+
											"    <div class='col-sm-11 col-md-11' style='margin-left:20px; margin-bottom:10px;'>"+
											"		<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId152,dspqRouter152,listToolTips)+"</strong></label>"+
											"	    <div id='"+sInputControlId152+"' "+getInputCustomeAttribute(dspqRouter152)+"><textarea rows='3' style='width:100%;' id='txtArea_"+sInputControlId152+"'>"+sNoLongerEmployed+"</textarea></div>"+
											"    </div>"+
											"</div>");
									}
									
								}
								
								///
								
								
							}
							
							
							sb.append("</div>"); // End show hide main div #2
							
							sb.append("</div>"); // End Row #1
						}
						
					}
					
					if(mapDspqRouterByFID.get(127)!=null) //I am a current employee of
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(127);
						if(dspqRouter!=null)
						{
							String sInputControlId="employmentStatus_2";
							if(bRdHidden)
								sb.append("<input type='hidden' id='hdn_"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter)+" >");
							
							sb.append("<div class='row'>");//Start Row #2
							
							sb.append("<div class='col-sm-12 col-md-12'>");
							sb.append("<label class='radio inline' style='margin-top: 2px;margin-bottom: 2px;'>");
							
							String sShowHideByEmpStCssClass="hide";
							if(iEmployeeType==1)
							{
								sb.append("<input type='radio' name='"+sInputControlName+"' id='"+sInputControlId+"' checked value='1' onclick='jafShowEmpNoDiv(1);' ><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div>");
								sShowHideByEmpStCssClass="";
							}
							else
								sb.append("<input type='radio' name='"+sInputControlName+"' id='"+sInputControlId+"' value='1' onclick='jafShowEmpNoDiv(1);' ><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div>");
							
							sb.append("</label>");
							sb.append("</div>");
							
							sb.append("<div id='empDivId2' class='"+sShowHideByEmpStCssClass+"'>"); // Start show hide main div #2
							if(mapDspqRouterByFID.get(128)!=null)
							{
								DspqRouter dspqRouter128=mapDspqRouterByFID.get(128);
								if(dspqRouter128!=null)
								{
									String sEmployeeNumber="";
									if(teacherpersonalinfo!=null && teacherpersonalinfo.getEmployeeNumber()!=null)
										sEmployeeNumber=teacherpersonalinfo.getEmployeeNumber().trim();
									
									String sInputControlId128="employeeNumberOpt2";
									sb.append("<div class='col-sm-12 col-md-12' style='margin-left:20px;'>"+
											"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId128,dspqRouter128,listToolTips)+"</strong></label>"+
											"<input type='text' id='"+sInputControlId128+"' class='form-control' value='"+sEmployeeNumber+"' "+getInputCustomeAttribute(dspqRouter128)+" maxlength='50' onkeypress='return jafCheckForIntEmpNum(event);' style='width:200px;' />"+
									"</div>");
								}
							}
							
							
							if(mapDspqRouterByFID.get(129)!=null || mapDspqRouterByFID.get(130)!=null || mapDspqRouterByFID.get(131)!=null)
							{
								int iIsCurrentFullTimeTeacher=-1;
								if(teacherpersonalinfo!=null && teacherpersonalinfo.getIsCurrentFullTimeTeacher()!=null)
									iIsCurrentFullTimeTeacher=teacherpersonalinfo.getIsCurrentFullTimeTeacher();
								
								String sInputControlName129="staffTypeOnExperiences";
								if(mapDspqRouterByFID.get(129)!=null)
								{
									DspqRouter dspqRouter129=mapDspqRouterByFID.get(129);
									if(dspqRouter129!=null)
									{
										sb.append("<div class='col-sm-12 col-md-12' style='margin-left:20px;'>");//Start :: #6.3#
										String sInputControlId129="staffType_I";
										
										String sRdChecked="";
										if(iIsCurrentFullTimeTeacher==1)
											sRdChecked="checked";
										
										sb.append("<label class='radio' id='inst'>"+
										"<input type='radio' value='I' "+sRdChecked+" id='"+sInputControlId129+"' name='"+sInputControlName129+"' "+getInputCustomeAttribute(dspqRouter129)+">"+
										"<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId129,dspqRouter129,listToolTips)+"</strong>"+
										"</label>");
										sb.append("</div>");//End :: #6.3#
									}
								}
									
								if(mapDspqRouterByFID.get(130)!=null)
								{
									DspqRouter dspqRouter130=mapDspqRouterByFID.get(130);
									if(dspqRouter130!=null)
									{
										sb.append("<div class='col-sm-12 col-md-12' style='margin-left:20px;'>");//Start :: #6.4#
										String sInputControlId130="staffType_PI";
										
										String sRdChecked="";
										if(iIsCurrentFullTimeTeacher==2)
											sRdChecked="checked";
										
										sb.append("<label class='radio' id='partinst'>"+
										"<input type='radio' value='PI' "+sRdChecked+" id='"+sInputControlId130+"' name='"+sInputControlName129+"' "+getInputCustomeAttribute(dspqRouter130)+">"+
										"<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId130,dspqRouter130,listToolTips)+"</strong>"+
										"</label>");
										sb.append("</div>");//End :: #6.4#
									}
								}
									
								if(mapDspqRouterByFID.get(131)!=null)
								{
									DspqRouter dspqRouter131=mapDspqRouterByFID.get(131);
									if(dspqRouter131!=null)
									{
										sb.append("<div class='col-sm-12 col-md-12' style='margin-left:20px;'>");//Start :: #6.5#
										String sInputControlId131="staffTyp_Ne";
										
										String sRdChecked="";
										if(iIsCurrentFullTimeTeacher==0)
											sRdChecked="checked";
										
										sb.append("<label class='radio' id='noninst'>"+
										"<input type='radio' value='N' "+sRdChecked+" id='"+sInputControlId131+"' name='"+sInputControlName129+"' "+getInputCustomeAttribute(dspqRouter131)+">"+
										"<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId131,dspqRouter131,listToolTips)+"</strong>"+
										"</label>");
										sb.append("</div>");//End :: #6.5#
									}
								}
							}
							sb.append("</div>"); // Start show hide main div #2
							
							sb.append("</div>");//End Row #2
						}
					}
					
					
					if(mapDspqRouterByFID.get(132)!=null) //I have never been employed by
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(132);
						if(dspqRouter!=null)
						{
							String sInputControlId="employmentStatus_3";
							
							if(bRdHidden)
								sb.append("<input type='hidden' id='hdn_"+sInputControlName+"' "+getInputCustomeAttribute(dspqRouter)+" >");
							
							sb.append("<div class='row'>");//Start Row #3
							sb.append("<div class='col-sm-11 col-md-11'>");
							sb.append("<label class='radio inline' style='margin-top: 2px;margin-bottom: 2px;'>");
							
							if(iEmployeeType==2)
								sb.append("<input type='radio' name='"+sInputControlName+"' id='"+sInputControlId+"_3' checked value='2' onclick='jafShowEmpNoDiv(2);' ><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div>");
							else
								sb.append("<input type='radio' name='"+sInputControlName+"' id='"+sInputControlId+"_3' value='2' onclick='jafShowEmpNoDiv(2);' ><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div>");
							
							sb.append("</label>");
							sb.append("</div>");
							sb.append("</div>");//End Row #3
						}
					}
					
				}
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection26.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
						
			}
		}
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}
	
	//Employment History
	@Transactional(readOnly=false)
	public String getGroup04_13(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup04_13");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		JobOrder jobOrder=null;
		JobCategoryMaster dspqPotfolioJC=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
			if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null)
				dspqPotfolioJC=jobOrder.getJobCategoryMaster();
		}
		
		StringBuffer sb=new StringBuffer();
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(13);
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		sectionMasters.add(dspqSectionMaster);
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		if(mapDspqRouterBySID!=null)
		{
			DistrictMaster dspqPotfolioDM=null;
			if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
				dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
			
			DspqRouter dspqRouterSection13=mapDspqRouterBySID.get(13);
			if(dspqRouterSection13!=null)
			{
				String sSubSection=getAddSubSectionText(dspqRouterSection13);
				String sSectionName="tooltipSection13";
				sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;padding-right:0px;'>"+
				"				<div style='float: left' class='portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection13, listToolTips)+
				"				</div>"+
				"					<div style='float: right' class='addPortfolio'>"+
				"					<input type='hidden' id='sbtsource_emp' name='sbtsource_emp' value='0'/>"+
				"					<a href='javascript:void(0);' id='addEmploymentIdDSPQ' onclick='showEmploymentForm()' >"+sSubSection+"</a>"+
				"				</div>"+
				"				<div style='clear: both;'></div>	"+
				"		    </div>");
				
				sb.append(getInstructionForSection(dspqRouterSection13));
				
				sb.append("<div class='row' onmouseover=\"setGridNameFlag('workExperience')\">"+
				"	    <div class='col-sm-12 col-md-12'>"+
				"			<div id='divDataGridEmployment'></div>"+
				"		</div>"+
				"	</div>");
				
				
				sb.append("<div class='portfolio_Section_ImputFormGap' id='divEmployment' style='display: none;'>"+
				"		<form id='frmEmployment' name='frmEmployment' onsubmit='return false;'>");
				
				sb.append("<div class='row'>					"+
						"	<div class='col-sm-12 col-md-12'>"+
						"	<div class='divErrorMsg' id='errordivEmployment' style='display: block;'></div>"+
						"	</div>			"+
						"</div>");
				
				//--------- Input Field start
				sb.append("<input type='hidden' id='roleId' name='roleId'>");
				
				if(mapDspqRouterByFID.get(144)!=null)
				{
					sb.append("<div class='row'>");
					DspqRouter dspqRouter144=mapDspqRouterByFID.get(144);
					
					String schkChecked="chkChecked";
					sb.append("<div class='col-sm-11 col-md-11'>");
					String sInputControlId144="idonothaveworkexp";
					
					sb.append("<input type='hidden' id='hdn_"+sInputControlId144+"' "+getInputCustomeAttribute(dspqRouter144)+" >");
					sb.append("<label class='checkbox inline' style='margin-top: 2px;margin-bottom: 2px;'>");
					sb.append("<input type='checkbox' "+schkChecked+" value='1' id='"+sInputControlId144+"' name='"+sInputControlId144+"' "+getInputCustomeAttribute(dspqRouter144)+" onclick='jafhideworkexp();'>");
					
					sb.append("<strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId144,dspqRouter144,listToolTips)+"</strong></label>");
					sb.append("</div>");
					sb.append("</div>");
				}
				
				//Position
				printdata("Position");
				if(mapDspqRouterByFID.get(59)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(59);
					if(dspqRouter!=null)
					{
						String sInputControlId="empPosition";
						sb.append("<div class='col-sm-3 col-md-3' style='padding-left:0px;' id='empHisPos'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	 <select class='form-control jafHideWorkExp' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"'>"+
								"		<option value='0'>"+Utility.getLocaleValuePropByKey("optSlt", locale)+"</option>"+
								"		<option value='1'>"+Utility.getLocaleValuePropByKey("lblStudentTeaching", locale)+"</option>"+
								"		<option value='2'>"+Utility.getLocaleValuePropByKey("lblFullTimeTeaching", locale)+"</option>"+
								"		<option value='3'>"+Utility.getLocaleValuePropByKey("lblSubstituteTeaching", locale)+"</option>"+
								"		<option value='4'>"+Utility.getLocaleValuePropByKey("lblOtherWorkExperience", locale)+"</option>"+
								"	</select>"+
								"</div>");
					}
					
				}
				
				//Role 
				printdata("Role");
				if(mapDspqRouterByFID.get(60)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(60);
					if(dspqRouter!=null)
					{
						String sInputControlId="role";
						sb.append("<div class='col-sm-4 col-md-4'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control jafHideWorkExp' placeholder='' maxlength='50'>"+
								"</div>");
					}
				}
				
				//Field 
				printdata("Field");
				if(mapDspqRouterByFID.get(61)!=null)
				{
					List<FieldMaster> lstFieldMaster = fieldMasterDAO.findByCriteria(Order.asc("fieldName"),Restrictions.eq("status", "A"));
					DspqRouter dspqRouter=mapDspqRouterByFID.get(61);
					if(dspqRouter!=null)
					{
						String sInputControlId="fieldId2";
						sb.append("<div class='col-sm-5 col-md-5' id='ndustry_FieldDspq'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<select class='form-control jafHideWorkExp' "+getInputCustomeAttribute(dspqRouter)+" id='"+sInputControlId+"' name='fieldId'>"+
								"		<option value='0'>"+Utility.getLocaleValuePropByKey("optSlt", locale)+"</option>");
										for(FieldMaster fieldMaster : lstFieldMaster){
											sb.append("<option id='"+fieldMaster.getFieldId()+"' value='"+fieldMaster.getFieldId()+"'>"+fieldMaster.getFieldName()+"</option>");
										}
					    sb.append("</select></div>");
					}
				}
				
				//Name of Organization/Emp. 
				printdata("Name of Organization/Emp.");
				if(mapDspqRouterByFID.get(62)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(62);
					if(dspqRouter!=null)
					{
						String sInputControlId="empOrg";
						sb.append("<div class='col-sm-3 col-md-3' style='padding-left:0px;'>" +
								"  <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	 <input type='text' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control jafHideWorkExp' name='"+sInputControlId+"'/>"+
								"</div>");
					}
				}
				
				//cityEmp 
				printdata("cityEmp");
				if(mapDspqRouterByFID.get(64)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(64);
					if(dspqRouter!=null)
					{
						String sInputControlId="cityEmp";
						sb.append("<div class='col-sm-4 col-md-4'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control jafHideWorkExp' name='"+sInputControlId+"'/>"+
								"</div>");
					}
				}
				
				//stateOfOrg 
				printdata("stateOfOrg");
				if(mapDspqRouterByFID.get(65)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(65);
					if(dspqRouter!=null)
					{
						String sInputControlId="stateOfOrg";
						sb.append("<div class='col-sm-5 col-md-5'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control jafHideWorkExp' name='"+sInputControlId+"'/>"+
								"</div>");
					}
				}
				
				//Duration 
				printdata("Duration");
				
				if(mapDspqRouterByFID.get(63)!=null)
				{
					sb.append("<div class='row'>");
					DspqRouter dspqRouter=mapDspqRouterByFID.get(63);
					if(dspqRouter!=null)
					{
						String sInputControlId="currentlyWorking";
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						sb.append("<div class='col-sm-11 col-md-11'><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div></div>");
						
						sb.append("<div class='col-sm-4 col-md-4'>");
						sb.append("<label class='checkbox inline' style='margin-top: 2px;margin-bottom: 2px;'>");
						sb.append("<input type='checkbox' class='jafHideWorkExp' name='"+sInputControlId+"' id='"+sInputControlId+"' onclick='hideToExp()'>"+Utility.getLocaleValuePropByKey("lblICurrWrkHr", locale)+"</br>");
						sb.append("</label>");
						sb.append("</div>");
						
					}
				sb.append("</div>");//End :: #7#
				}
				
				//From 
				printdata("From ");
				if(mapDspqRouterByFID.get(66)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(66);
					if(dspqRouter!=null)
					{
						String sInputControlId1="roleStartMonth";
						String sInputControlId2="roleStartYear";
						
						int count=1;
						List<String> lstMonth= Utility.getMonthList();
						List<String> lstYear=Utility.getLasterYearByYear(1955);
						sb.append("<div class='col-sm-2 col-md-2' style='padding-left:0px;'>" +
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId1,dspqRouter,listToolTips)+"</strong></label>"+
								"	<select id='"+sInputControlId1+"' name='"+sInputControlId1+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control jafHideWorkExp'>"+
								"		<option value='0'>"+Utility.getLocaleValuePropByKey("optStrMonth", locale)+"</option>");
									for(String month : lstMonth){
										sb.append("<option value='"+(count++)+"'>"+month+"</option>");
									}		
						sb.append("</select></div>");
						sb.append("<div class='col-sm-2 col-md-2'>"+
								"	<label>&nbsp;</label>"+
								"	<select id='"+sInputControlId2+"' name='"+sInputControlId2+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control jafHideWorkExp'>"+
								"		<option value='0' >"+Utility.getLocaleValuePropByKey("optYear", locale)+"</option>");
										for(String year : lstYear){
											sb.append("<option value="+year+">"+year+"</option>");
										}
						sb.append("</select></div>");
					}
				}
				
				//To 
				printdata("To ");
				if(mapDspqRouterByFID.get(67)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(67);
					if(dspqRouter!=null)
					{
						int count=1;
						List<String> lstMonth= Utility.getMonthList();
						List<String> lstYear=Utility.getLasterYearByYear(1955);
						
						String sInputControlId1="roleEndMonth";
						String sInputControlId2="roleEndYear";
						
						sb.append("<div id='divToMonth' class='col-sm-2 col-md-2 '>" +
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId1,dspqRouter,listToolTips)+"</strong></label>"+
								  "	<select class='form-control jafHideWorkExp' id='"+sInputControlId1+"' "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId1+"'>"+
								  "		<option value='0'>"+Utility.getLocaleValuePropByKey("optStrMonth", locale)+"</option>");
											for(String month : lstMonth){
												sb.append("<option value='"+(count++)+"'>"+month+"</option>");
											}	
						sb.append("</select></div>");
						
						sb.append("<div id='divToYear' class='col-sm-2 col-md-2'>"+
								  "	<label>&nbsp;</label>"+
								  "	<select id='"+sInputControlId2+"' name='"+sInputControlId2+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control jafHideWorkExp'>"+
								  "		<option value='0'>"+Utility.getLocaleValuePropByKey("optYear", locale)+"</option>");
											for(String year : lstYear){
												sb.append("<option value="+year+">"+year+"</option>");
											}
						sb.append("</select></div>");
					}
				}
				//Annual Salary Amount (in $)
				printdata("amount");
				if(mapDspqRouterByFID.get(68)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(68);
					if(dspqRouter!=null)
					{
						String sInputControlId="amount";
						sb.append("<div id='empHisAnnSal' class='col-sm-4 col-md-4' style='padding-left:0px;'>" +
								  "   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								  "   <input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control jafHideWorkExp'   onkeypress='return checkForInt(event);' maxlength='8'>"+
								  "</div>");
					}
				}
				
				// Type of Role  
				printdata("Type of Role ");
				if(mapDspqRouterByFID.get(69)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(69);
					if(dspqRouter!=null)
					{
						List<EmpRoleTypeMaster> lstEmpRoleTypeMaster = empRoleTypeMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
						String sInputControlId="empRoleTypeId";
						sb.append("<div class='col-sm-11 col-md-11 empHisTypRol' style='padding-left:0px;'>"+
								"  <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"</div>"+
								"<div class='col-sm-11 col-md-11 empHisTypRol' style='padding-left:0px;' id='empRoleDspq'>");
								sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
									for(EmpRoleTypeMaster empRoleType : lstEmpRoleTypeMaster)
									{
										sb.append("<div class='row'>");
										sb.append("<div class='col-sm-12 col-md-12'>"+
												  "     <label class='radio' style='margin-top: 0px;'>"+
												  "			<input type='radio' class='jafHideWorkExp' value='"+empRoleType.getEmpRoleTypeId()+"'  name='"+sInputControlId+"' id='"+sInputControlId+"_"+empRoleType.getEmpRoleTypeId()+"'>"+empRoleType.getEmpRoleTypeName()+
												  "	    </label>"+
												  "</div>");
										sb.append("</div>");
									}
								sb.append("</div>");
					}
				}	
				
				//Primary Responsibilities in this Role  
				printdata("Primary Responsibilities in this Role");
				if(mapDspqRouterByFID.get(70)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(70);
					if(dspqRouter!=null)
					{
						String sInputControlId="primaryResp";
						   sb.append("<div class='row'>"+
								"    <div class='col-sm-11 col-md-11' >"+
								"		<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	    <div id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+"><textarea rows='8' style='width:100%;' id='txtArea_"+sInputControlId+"'></textarea></div>"+
								"    </div>"+
								"</div>");
						   
					}
				}	
				//Most Significant Contributions in this Role  
				printdata("Most Significant Contributions in this Role");
				if(mapDspqRouterByFID.get(71)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(71);
					if(dspqRouter!=null)
					{
						String sInputControlId="mostSignCont";
						   sb.append("<div class='row'>"+
								"    <div class='col-sm-11 col-md-11 mt10'>"+
								"		<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	    <div id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+"><textarea rows='8' style='width:100%;' id='txtArea_"+sInputControlId+"'></textarea></div>"+
								"    </div>"+
								"</div>");
					}
				}
				
				
				//Reason for leaving 
				printdata("reasonForLea");
				if(mapDspqRouterByFID.get(72)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(72);
					if(dspqRouter!=null)
					{
						String sInputControlId="reasonForLea";
						   sb.append("<div class='row'>"+
								"    <div class='col-sm-11 col-md-11 mt10' >"+
								"		<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	    <div id='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+"><textarea rows='8' style='width:100%;' id='txtArea_"+sInputControlId+"'></textarea></div>"+
								"    </div>"+
								"</div>");
						   
					}
				}
				
				//--------- Input Field end
				
				sb.append("</form>"+
				" 		<div class='row'>								"+
				"	    <div class='col-sm-11 col-md-11 top10 idone'>"+
				"			<a id='hrefDone' href='#' style='cursor: pointer; margin-left: 0px; text-	;' onclick='return insertOrUpdateEmployment();' >"+Utility.getLocaleValuePropByKey("lnkImD", locale)+"</a>&nbsp;&nbsp;"+
				"			<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;'	onclick='return hideEmploymentForm()'>"+
								Utility.getLocaleValuePropByKey("lnkCancel", locale)+
				"			</a>"+
				"		</div>"+
				"		</div>"+
				"	</div>");
				
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection13.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
		}
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}
	
	//Involvement/Volunteer Work/Student Teaching
	@Transactional(readOnly=false)
	public String getGroup04_14(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup04_14");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		JobOrder jobOrder=null;
		JobCategoryMaster dspqPotfolioJC=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
			if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null)
				dspqPotfolioJC=jobOrder.getJobCategoryMaster();
		}
		
		StringBuffer sb=new StringBuffer();
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(14);
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		sectionMasters.add(dspqSectionMaster);
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		if(mapDspqRouterBySID!=null)
		{
			DistrictMaster dspqPotfolioDM=null;
			if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
				dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
			
			DspqRouter dspqRouterSection14=mapDspqRouterBySID.get(14);
			if(dspqRouterSection14!=null)
			{
				String sSectionName="tooltipSection14";
				String sSubSection=getAddSubSectionText(dspqRouterSection14);
				sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;padding-right:0px;'>"+
				"				<div style='float: left' class='portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection14, listToolTips)+
				"				</div>"+
				"					<div style='float: right' class='addPortfolio'>"+
				"					<a href='javascript:void(0);' id='addInvolvemetIdDSPQ' onclick='showInvolvementForm()' >"+sSubSection+"</a>"+
				"				</div>"+
				"				<div style='clear: both;'></div>	"+
				"		    </div>");
				
				sb.append(getInstructionForSection(dspqRouterSection14));
				
				sb.append("<div class='row' onmouseover=\"setGridNameFlag('involvement')\">"+
				"	    <div class='col-sm-12 col-md-12'>"+
				"			<div id='divDataInvolvement'></div>"+
				"		</div>"+
				"	</div>");
				
				
				sb.append("<div class='portfolio_Section_ImputFormGap' id='divInvolvement' style='display: none;'>"+
				"		<form id='frmInvolvement' name='frmInvolvement'><input type='hidden' id='involvementId' name='involvementId'/>");
				
				//--------- Input Field start
				sb.append("<div class='row'>");
				//organizationInv
				printdata("organizationInv");
				if(mapDspqRouterByFID.get(73)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(73);
					if(dspqRouter!=null)
					{
						String sInputControlId="organizationInv";
						sb.append("<div class='col-sm-3 col-md-3'>" +
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"'  "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"' maxlength='50' class='form-control' >"+
								"</div>");
					}
				}
				
				//Type of Organization
				printdata("Type of Organization");
				if(mapDspqRouterByFID.get(74)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(74);
					if(dspqRouter!=null)
					{
						List<OrgTypeMaster>  lstOrgTypeMasters = orgTypeMasterDAO.findByCriteria(Order.asc("orgType"),Restrictions.eq("status", "A"));
						String sInputControlId="orgTypeId";
						
						sb.append("<div class='col-sm-3 col-md-3' id='orgTypDivInv'>" +
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<select class='form-control ' id='"+sInputControlId+"'  "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"'>"+
								"	  <option value='0'>"+Utility.getLocaleValuePropByKey("optSlt", locale)+"</option>");
									   for(OrgTypeMaster orgType : lstOrgTypeMasters){
										 sb.append("<option value='"+orgType.getOrgTypeId()+"'>"+orgType.getOrgType()+"</option>");
									   }
						sb.append("</select></div>");
					}
				}
				
				//Number of people in this Organization
				printdata("Type of Organization");
				if(mapDspqRouterByFID.get(75)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(75);
					if(dspqRouter!=null)
					{
						List<PeopleRangeMaster> lstPeopleRangeMasters = peopleRangeMasterDAO.findAll();
						
						String sInputControlId="rangeId";
						sb.append("<div class='col-sm-4 col-md-4' id='rangeIdspq'>" +
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<select class='form-control ' id='"+sInputControlId+"'  "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"'>"+
								"		<option value='0'>Select</option>");
						
								for(PeopleRangeMaster peopleRange : lstPeopleRangeMasters)
								{
									sb.append("<option value='"+peopleRange.getRangeId()+"'>"+peopleRange.getRange()+"</option>");
								}
								
						sb.append("</select></div>");
					}
				}
				//Did you lead people in this organization? 
				printdata("Did you lead people in this organization? ");
				if(mapDspqRouterByFID.get(76)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(76);
					if(dspqRouter!=null)
					{
						String sInputControlId="optionsRadios";
						
						sb.append("<div class='col-sm-11 col-md-11 mt10' ><div class='span4'>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId, dspqRouter, listToolTips)+"</div></div>");
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" >");
						
						sb.append("<div class='col-sm-11 col-md-11'>");
						sb.append("<label class='radio' style='margin-top: 2px;margin-bottom: 2px;'>");
						sb.append("<input type='radio'  name='"+sInputControlId+"' id='"+sInputControlId+"_1' value='1' onclick=\"showAndHideLeadNoOfPeople('1')\"> "+Utility.getLocaleValuePropByKey("lblYes", locale));
						sb.append("</label></div>");
						
						sb.append("<div class='col-sm-11 col-md-11'>");
						sb.append("<label class='radio' style='margin-top: 2px;margin-bottom: 2px;'>");
						sb.append("<input type='radio' name='"+sInputControlId+"' id='"+sInputControlId+"_0' checked='checked' value='0' onclick=\"showAndHideLeadNoOfPeople('0')\">"+Utility.getLocaleValuePropByKey("lblNo", locale));
						sb.append("</label></div>");
						
					}
				}
				
				//How many people ?
				printdata("How many people ?");
				if(mapDspqRouterByFID.get(77)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(77);
					String sInputControlId="leadNoOfPeople";
					if(dspqRouter!=null)
					{
						sb.append("<div class='col-sm-3 col-md-3' id='divLeadNoOfPeople' name='divLeadNoOfPeople'>" +
								"	<label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"'  "+getInputCustomeAttribute(dspqRouter)+" name='"+sInputControlId+"' class='form-control' onkeypress='return checkForInt(event);' maxlength='4'/>"+
								"</div>");
					}
				}
				
				
				sb.append("</div>");
				
				//--------- Input Field end
				
				sb.append("</form>"+
				" 		<div class='row'>								"+
				"	    <div class='col-sm-11 col-md-11 top10 idone'>"+
				"			<a id='hrefDone' href='#' style='cursor: pointer; margin-left: 0px; text-	;' onclick='return saveOrUpdateInvolvement();' >"+Utility.getLocaleValuePropByKey("lnkImD", locale)+"</a>&nbsp;&nbsp;"+
				"			<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;'	onclick='return hideInvolvement()'>"+
								Utility.getLocaleValuePropByKey("lnkCancel", locale)+
				"			</a>"+
				"		</div>"+
				"		</div>"+
				"	</div>");
				
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection14.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
		}
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}
	
	//Honors
	@Transactional(readOnly=false)
	public String getGroup04_15(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getGroup04_15");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		JobOrder jobOrder=null;
		JobCategoryMaster dspqPotfolioJC=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
			if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null)
				dspqPotfolioJC=jobOrder.getJobCategoryMaster();
		}
		
		StringBuffer sb=new StringBuffer();
		
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(15);
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		sectionMasters.add(dspqSectionMaster);
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		if(mapDspqRouterBySID!=null)
		{
			/*JobCategoryMaster dspqPotfolioJC=null;
			if(dspqPortfolioName!=null && dspqPortfolioName.getJobCategoryMaster()!=null)
				dspqPotfolioJC=dspqPortfolioName.getJobCategoryMaster();*/
			DistrictMaster dspqPotfolioDM=null;
			if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
				dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
			
			DspqRouter dspqRouterSection15=mapDspqRouterBySID.get(15);
			if(dspqRouterSection15!=null)
			{
				String sSubSection=getAddSubSectionText(dspqRouterSection15);
				String sSectionName="tooltipSection15";
				sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;padding-right:0px;'>"+
				"				<div style='float: left' class='portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection15, listToolTips)+
				"				</div>"+
				"					<div style='float: right' class='addPortfolio'>"+
				"					<a href='javascript:void(0);' id='addHonorsIdDSPQ' onclick='showHonorForm()' >"+sSubSection+"</a>"+
				"				</div>"+
				"				<div style='clear: both;'></div>	"+
				"		    </div>");
				
				sb.append(getInstructionForSection(dspqRouterSection15));
				
				sb.append("<div class='row'>"+
				"	    <div class='col-sm-12 col-md-12'>"+
				"			<div id='divDataHoner'></div>"+
				"		</div>"+
				"	</div>");
				
				
				sb.append("<div class='portfolio_Section_ImputFormGap' id='divHonor' style='display: none;'>"+
				"		<form id='frmHonor' name='frmHonor' onsubmit='return false;'>");
				
				sb.append("<div class='row'>					"+
						"	<div class='col-sm-12 col-md-12'>"+
						"	   <div class='divErrorMsg' id='errordivHonor' style='display: block;'></div>"+
						"	</div>"+
						"</div>");
				
				
				//--------- Input Field start
				sb.append("<div class='row'>");
				//honor
				printdata("honor");
				if(mapDspqRouterByFID.get(78)!=null)
				{
					sb.append("<input type='hidden' id='honorId' name='honorId'/>");
					DspqRouter dspqRouter=mapDspqRouterByFID.get(78);
					if(dspqRouter!=null)
					{
						String sInputControlId="honor";
						sb.append("<div class='col-sm-4 col-md-4 rowmargin1'>" +
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"     <input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttribute(dspqRouter)+" class='form-control' placeholder='' maxlength='50'>"+
								"</div>");	
						
					}
				}
				
				if(mapDspqRouterByFID.get(119)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(119);
					if(dspqRouter!=null)
					{
						List<String> lstYear=Utility.getLasterYearByYear(1955);
						String sInputControlId="honorYear";
						sb.append("<div class='col-sm-2 col-md-2'>"+
						"   <label><strong>"+getLabelOrToolTipTagOrRequiredIcon(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
						"	<select id='"+sInputControlId+"' name='"+sInputControlId+"' class='form-control' "+getInputCustomeAttribute(dspqRouter)+" >"+
						"		<option value='0' >"+Utility.getLocaleValuePropByKey("optYear", locale)+"</option>");
						for(String year : lstYear){
							sb.append("<option value="+year+">"+year+"</option>");
						}
						sb.append("</select></div>");
					}
				}
				
				sb.append("</div>");
				
				//--------- Input Field end
				
				sb.append("</form>"+
				" 		<div class='row'>								"+
				"	    <div class='col-sm-11 col-md-11 top10 idone'>"+
				"			<a id='hrefDone' href='#' style='cursor: pointer; margin-left: 0px; text-	;' onclick='return saveOrUpdateHonors();' >"+Utility.getLocaleValuePropByKey("lnkImD", locale)+"</a>&nbsp;&nbsp;"+
				"			<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;'	onclick='return hideHonor()'>"+
								Utility.getLocaleValuePropByKey("lnkCancel", locale)+
				"			</a>"+
				"		</div>"+
				"		</div>"+
				"	</div>");
			
				sb.append(getCustomFieldBySectionId(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection15.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,jobOrder));
			}
		}
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}
	
	public String getLabelOrToolTipTagOrRequiredIcon(String tooltipId, DspqRouter dspqRouter,List<String> listToolTips)
	{
		return getLabelOrToolTipTagOrRequiredIcon(tooltipId, dspqRouter, listToolTips,"");
	}
	
	public String getLabelOrToolTipTagOrRequiredIcon(String tooltipId, DspqRouter dspqRouter,List<String> listToolTips,String sPreFixText)
	{
		String sReturnValue="";
		
		int iNumberRequired=0;
		try {
			if(dspqRouter.getNumberRequired()!=null && dspqRouter.getNumberRequired()>0)
			{
				iNumberRequired=dspqRouter.getNumberRequired();
			}
			//icon-warning required
		} catch (Exception e) {

		}
		
		try {
			if(dspqRouter.getDisplayName()!=null && !dspqRouter.getDisplayName().equals(""))
			{
				if(dspqRouter.getDspqFieldMaster()!=null && dspqRouter.getDspqSectionMaster()==null)
				{
					if(sPreFixText!=null && !sPreFixText.equals(""))
						sReturnValue="<span id='lblFieldId_"+tooltipId+"' numberRequired='"+iNumberRequired+"'>"+sPreFixText+""+dspqRouter.getDisplayName()+"</span><span id='lblFieldIdWarningBeforeSpace_"+tooltipId+"'></span><span id='lblFieldIdWarning_"+tooltipId+"'></span><span id='lblFieldIdWarningAfterSpace_"+tooltipId+"'></span>";
					else
						sReturnValue="<span id='lblFieldId_"+tooltipId+"' numberRequired='"+iNumberRequired+"'>"+dspqRouter.getDisplayName()+"</span><span id='lblFieldIdWarningBeforeSpace_"+tooltipId+"'></span><span id='lblFieldIdWarning_"+tooltipId+"'></span><span id='lblFieldIdWarningAfterSpace_"+tooltipId+"'></span>";
				}
				else
					sReturnValue="<span id='lblSectionId_"+tooltipId+"' numberRequired='"+iNumberRequired+"'>"+dspqRouter.getDisplayName()+"</span><span id='lblFieldIdWarningBeforeSpace_"+tooltipId+"'></span><span id='lblSectionIdWarning_"+tooltipId+"'></span><span id='lblFieldIdWarningAfterSpace_"+tooltipId+"'></span>";
			}
			//icon-warning required
		} catch (Exception e) {

		}
		
		try {
			if(dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired())
				sReturnValue=sReturnValue+"<span class='required'>*</span>";
		} catch (Exception e) {

		}
		
		try {
			if(dspqRouter.getTooltip()!=null && !dspqRouter.getTooltip().equals(""))
			{
				sReturnValue=sReturnValue+"&nbsp;<a href='#' id='tooltip_"+tooltipId+"' rel='tooltip' data-original-title=\""+dspqRouter.getTooltip()+"\"><img src='images/qua-icon.png' width='15' height='15' alt=''></a>";
				listToolTips.add("tooltip_"+tooltipId);
			}
		} catch (Exception e) {

		}
		
		return sReturnValue;
	}
	
	
	public String getToolTipTagOrRequiredIcon(String tooltipId, DspqRouter dspqRouter,List<String> listToolTips)
	{
		String sReturnValue="";
		
		try {
			if(dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired())
				sReturnValue="<span class='required'>*</span>";
		} catch (Exception e) {

		}
		
		try {
			if(dspqRouter.getTooltip()!=null && !dspqRouter.getTooltip().equals(""))
			{
				sReturnValue=sReturnValue+"&nbsp;<a href='#' id='tooltip_"+tooltipId+"' rel='tooltip' data-original-title=\""+dspqRouter.getTooltip()+"\"><img src='images/qua-icon.png' width='15' height='15' alt=''></a>";
				listToolTips.add("tooltip_"+tooltipId);
			}
		} catch (Exception e) {

		}
		
		return sReturnValue;
	}
	
	
	public String getInputCustomeAttribute(DspqRouter dspqRouter)
	{
		String sReturnValue="";
		
		int iIsRequired=0;
		if(dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired())
			iIsRequired=1;
		
		String sDisplayName="";
		if(dspqRouter.getDisplayName()!=null && !dspqRouter.getDisplayName().equals(""))
			sDisplayName=dspqRouter.getDisplayName();
		
		try {
			if(dspqRouter!=null)
			{
				sReturnValue=" dspqRequired='"+iIsRequired+"' dspqLabelName='"+sDisplayName+"' ";
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return sReturnValue;
	}
	
	
	@Transactional
	public String getPFCertificationsGridDspqNoble(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		return pfCertifications.getPFCertificationsGridDspqNoble(noOfRow, pageNo, sortOrder, sortOrderType);
	}
	
	public String getPFCertificationsGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer teacherId,Integer portfolioId,String candidateType,Integer sectionId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		TeacherDetail teacherDetail =null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		if(teacherId!=0){
			teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
		}else{
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		}
		StringBuffer sb = new StringBuffer();
		//***************************************************************************
		String applicantType="";
		
		if(candidateType.length()!=1){
			applicantType=validateCTAndDec(candidateType);
		}else{
			if(candidateType.equalsIgnoreCase("")){
			}else{
				applicantType=validateApplicantType(candidateType);
			}
		}
		System.out.println("ApplicantType :::::::::: "+applicantType);
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		mapDspqRouterByFID=getDSPQRouterMapById(portfolioId, applicantType,sectionId);
		//**************************************************************************
		
		
		String lblNm=getGridHeaderName(mapDspqRouterByFID,37);
		String lblSt= getGridHeaderName(mapDspqRouterByFID,34);
		String lblYearRece=getGridHeaderName(mapDspqRouterByFID,35);
		String lblStatus=getGridHeaderName(mapDspqRouterByFID,32);
		String lblSource=getGridHeaderName(mapDspqRouterByFID,39);
		String toolViewScurec=Utility.getLocaleValuePropByKey("toolViewScurec", locale);
		
		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"certType";
			String sortOrderNoField		=	"certType";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("state")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("state")){
					 sortOrderNoField="state";
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
						
			List<TeacherCertificate> listTeacherCertificates = null;
			listTeacherCertificates = teacherCertificateDAO.findSortedCertificateByTeacher(sortOrderStrVal,teacherDetail);
			
			List<TeacherCertificate> sortedlistTeacherCertificates		=	new ArrayList<TeacherCertificate>();
			
			SortedMap<String,TeacherCertificate>	sortedMap = new TreeMap<String,TeacherCertificate>();
			if(sortOrderNoField.equals("state"))
			{
				sortOrderFieldName	=	"state";
			}
			int mapFlag=2;
			for (TeacherCertificate trCertificate : listTeacherCertificates){
				String orderFieldName=trCertificate.getCertType();
				if(sortOrderFieldName.equals("state")){
					String stateName="";
					if(trCertificate.getStateMaster()==null){
						stateName = " ";
					}else{
						stateName = trCertificate.getStateMaster().getStateName();
					}
						
					orderFieldName=stateName+"||"+trCertificate.getCertId();
					sortedMap.put(orderFieldName+"||",trCertificate);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherCertificates=listTeacherCertificates;
			}
			
			totalRecord =sortedlistTeacherCertificates.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherCertificate> listsortedTeacherCertificates		=	sortedlistTeacherCertificates.subList(start,end);
			
			sb.append("<table border='0'  id='tblGridCertifications'  width='100%' class='table  table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");
			
			String responseText="";
			
				responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblNm,sortOrderFieldName,"certType",sortOrderTypeVal,pgNo);
				sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblSt,sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
				sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblYearRece,sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
				sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblStatus,sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
				sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblSource,sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
				sb.append("<th width='20%' valign='top' id='certSRCHeader'>"+responseText+"</th>");
			if(teacherId==0){
				sb.append("<th width='10%' valign='top'>");
				sb.append(lblAct);
			}
			sb.append("</tr>");
			
			sb.append("</thead>");
			if(teacherCertificateDAO!=null)
			for(TeacherCertificate tCertificate:listsortedTeacherCertificates)
			{
				
				
				sb.append("<tr>");
				
				sb.append("<td>");
				if(tCertificate.getCertificateTypeMaster()!=null)
				sb.append(tCertificate.getCertificateTypeMaster().getCertType());
				sb.append("</td>");
				
				sb.append("<td>");
				if(tCertificate.getStateMaster()!=null)
				sb.append(tCertificate.getStateMaster().getStateName());
				sb.append("</td>");
				
				sb.append("<td>");
				String yearReceived = "";
				if(tCertificate.getYearReceived()!=null)
					yearReceived=tCertificate.getYearReceived().toString();
				sb.append(yearReceived);
				sb.append("</td>");
				
				sb.append("<td>");
				if(tCertificate.getCertificationStatusMaster()!=null)
					sb.append(tCertificate.getCertificationStatusMaster().getCertificationStatusName());
				else
					sb.append("");
				sb.append("</td>");
				
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				
				sb.append("<td class='certSRCRec'>");
				
				if(tCertificate.getPathOfCertification()!=null)
				{
					sb.append(tCertificate.getPathOfCertification()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='"+toolViewScurec+"' id='cert"+tCertificate.getCertId()+"' onclick=\"downloadCertification('"+tCertificate.getCertId()+"','cert"+tCertificate.getCertId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue' style='padding-left:20px;'></span></a><br>");
					sb.append("<script>$('#cert"+tCertificate.getCertId()+"').tooltip();</script>");
				}
				
				if(tCertificate.getCertUrl()!=null && !tCertificate.getCertUrl().equals(""))
				{
					if(tCertificate.getCertUrl().length()>5)
					{
						String url=tCertificate.getCertUrl().substring(0,5);
						url=url+"....";
						
						sb.append("<a href='"+tCertificate.getCertUrl()+"' rel='tooltip' data-original-title='"+tCertificate.getCertUrl()+"'  id='cert1"+tCertificate.getCertId()+"'  target='_blank'"+windowFunc+">"+url+"</a>");
						sb.append("<script>$('#cert1"+tCertificate.getCertId()+"').tooltip();</script>");
					}
				}
				sb.append("</td>");
				if(teacherId==0){
				
				sb.append("<td>");
				
				sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+editToolTip+"' id='editcert"+tCertificate.getCertId()+"'  onclick=\"return showEditForm('"+tCertificate.getCertId()+"')\" ><i class='fa fa-pencil-square-o fa-lg'></i></a>"+grdseparator);
				sb.append("<script>$('#editcert"+tCertificate.getCertId()+"').tooltip();</script>");

				sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+deleteToolTip+"' id='delcert"+tCertificate.getCertId()+"'  onclick=\"return deleteRecord_Certificate('"+tCertificate.getCertId()+"')\" ><i class='fa fa-trash-o'></i></a>");
				sb.append("<script>$('#delcert"+tCertificate.getCertId()+"').tooltip();</script>");
				
				sb.append("</td>");
				}
				sb.append("</tr>");
			}
			
			if(listTeacherCertificates==null || listTeacherCertificates.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
						
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow, pageNo));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	
		
	}
	
	public String getPFAcademinGridForDspqWOT(String noOfRow, String pageNo,String sortOrder,String sortOrderType,boolean isMiami,int transcriptFlag)
	{
		return pFAcademics.getPFAcademinGridForDspqWOT(noOfRow, pageNo, sortOrder, sortOrderType, isMiami, transcriptFlag);
	}
	
	public String getPFAcademinGridForDspq(String noOfRow, String pageNo,String sortOrder,String sortOrderType,boolean isMiami,int transcriptFlag,Integer teacherId,Integer portfolioId,String candidateType,Integer sectionId)
	{
		printdata("getPFAcademinGridForDspq isMiami "+isMiami +" transcriptFlag "+transcriptFlag);
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		TeacherDetail teacherDetail = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer sb = new StringBuffer();		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"attendedInYear";
			String sortOrderNoField		=	"attendedInYear";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("degree") && !sortOrder.equals("fieldOfStudy") && !sortOrder.equals("university")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("degree")){
					 sortOrderNoField="degree";
				 }
				 if(sortOrder.equals("fieldOfStudy")){
					 sortOrderNoField="fieldOfStudy";
				 }
				 if(sortOrder.equals("university")){
					 sortOrderNoField="university";
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			if(teacherId!=0){
				teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			}else{
				teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			}
			//***************************************************************************
			String applicantType="";
			
			if(candidateType.length()!=1){
				applicantType=validateCTAndDec(candidateType);
			}else{
				if(candidateType.equalsIgnoreCase("")){
				}else{
					applicantType=validateApplicantType(candidateType);
				}
			}
			Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
			mapDspqRouterByFID=getDSPQRouterMapById(portfolioId, applicantType,sectionId);
			//**************************************************************************
			String optSchool=getGridHeaderName(mapDspqRouterByFID,21);
			String lblDatAtt=getGridHeaderName(mapDspqRouterByFID,23);
			String lblDgr=getGridHeaderName(mapDspqRouterByFID,20);
			String lblFildOfStudy=getGridHeaderName(mapDspqRouterByFID,22);
			String headTranscript=getGridHeaderName(mapDspqRouterByFID,24).replaceAll("/", "/ ");
			String lblGPA=getGridHeaderName(mapDspqRouterByFID,29);
			List<TeacherAcademics> listTeacherAcademics = null;
			listTeacherAcademics = teacherAcademicsDAO.findSortedAcadamicDetailByTeacher(sortOrderStrVal,teacherDetail);
			
			
			
			List<TeacherAcademics> sortedlistTeacherAcademics		=	new ArrayList<TeacherAcademics>();
			
			SortedMap<String,TeacherAcademics>	sortedMap = new TreeMap<String,TeacherAcademics>();
			if(sortOrderNoField.equals("degree"))
			{
				sortOrderFieldName	=	"degree";
			}
			if(sortOrderNoField.equals("fieldOfStudy"))
			{
				sortOrderFieldName	=	"fieldOfStudy";
			}
			if(sortOrderNoField.equals("university"))
			{
				sortOrderFieldName	=	"university";
			}
			int mapFlag=2;
			for (TeacherAcademics trAcademics : listTeacherAcademics){
				String orderFieldName=trAcademics.getAttendedInYear()+"";
				if(sortOrderFieldName.equals("degree")){
					orderFieldName=trAcademics.getDegreeId().getDegreeName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("fieldOfStudy")){
					orderFieldName=trAcademics.getFieldId().getFieldName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("university")){
					orderFieldName=trAcademics.getUniversityId().getUniversityName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherAcademics=listTeacherAcademics;
			}
			
			totalRecord =sortedlistTeacherAcademics.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherAcademics> listsortedTeacherCertificates		=	sortedlistTeacherAcademics.subList(start,end);
			
			sb.append("<table border='0' id='academicGrid'  width='100%' class='table table-striped'  >");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLinkSSPF(optSchool,sortOrderFieldName,"university",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblDatAtt,sortOrderFieldName,"attendedInYear",sortOrderTypeVal,pgNo);
			sb.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblDgr,sortOrderFieldName,"degree",sortOrderTypeVal,pgNo);
			sb.append("<th width='15%' valign='top'>"+responseText+"</th>");
		
			sb.append("<th width='15%' class='net-header-text' valign='top'>");
			sb.append(lblGPA);
			sb.append("</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblFildOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
			sb.append("<th width='12%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkSSPF(headTranscript,sortOrderFieldName,"pathOfTranscript",sortOrderTypeVal,pgNo);
			sb.append("<th width='14%' valign='top'>"+responseText+"</th>");
			if(teacherId==0){
				sb.append("<th width='10%' class='net-header-text'>");
				sb.append(lblAct);
				sb.append("</th>");	
			}
			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherAcademics!=null)
			for(TeacherAcademics ta:listsortedTeacherCertificates)
			{
				sb.append("<tr class='cntAcad'>");
				
				sb.append("<td>");
				if(ta.getUniversityId()!=null)
				sb.append(ta.getUniversityId().getUniversityName());	
				sb.append("</td>");
				
				sb.append("<td>");
				
				if(ta.getAttendedInYear()!=null && !ta.getAttendedInYear().equals(""))
					sb.append(ta.getAttendedInYear());
				if(ta.getAttendedInYear()!=null && !ta.getAttendedInYear().equals("") && ta.getLeftInYear()!=null && !ta.getLeftInYear().equals(""))
					sb.append(" to ");
					if(ta.getLeftInYear()!=null && !ta.getLeftInYear().equals(""))
					sb.append(ta.getLeftInYear());
					sb.append("</td>");
				
					sb.append("<td  class='degreeTypeVal' degreeTypeVal='"+ta.getDegreeId().getDegreeType()+"'>");
				sb.append(ta.getDegreeId().getDegreeName());;
				sb.append("</td>");
				
				String chkGpa="gpaNo";
				if(ta.getGpaCumulative()!=null){
					chkGpa="gpaYes";
				}
				sb.append("<td class='"+chkGpa+"'>");
				if(ta.getDegreeId().getDegreeType().trim().equalsIgnoreCase("b"))
				{
					sb.append("Freshman: "+(ta.getGpaFreshmanYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaFreshmanYear()))+"<br> " +
						" Sophomore: "+(ta.getGpaSophomoreYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaSophomoreYear()))+"<br>" +
						" Junior: "+(ta.getGpaJuniorYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaJuniorYear()))+"<br>" +
						" Senior: "+(ta.getGpaSeniorYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaSeniorYear()))+"<br>" +
						" Cumulative: "+(ta.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaCumulative())));
				}else{
					sb.append("Cumulative:"+(ta.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaCumulative())));
				}
				sb.append("</td>");
				
				sb.append("<td>");
				if(ta.getFieldId()!=null)
				sb.append(ta.getFieldId().getFieldName());	
				sb.append("</td>");
				
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				sb.append("<td style='text-align:center'>");								
				sb.append(ta.getPathOfTranscript()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view Transcript !'  id='trans"+ta.getAcademicId()+"' onclick=\"downloadTranscript('"+ta.getAcademicId()+"','trans"+ta.getAcademicId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
				sb.append("<script>$('#trans"+ta.getAcademicId()+"').tooltip();</script>");
				sb.append("</td>");
				if(teacherId==0){
					sb.append("<td>");
					if(isMiami)
					{
						if(ta.getStatus()==null || !ta.getStatus().equalsIgnoreCase("V"))
						{
							sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+editToolTip+"' id='editac"+ta.getAcademicId()+"'  onclick=\"return showRecordToForm('"+ta.getAcademicId()+"','"+ta.getStatus()+"')\" ><i class='fa fa-pencil-square-o fa-lg'></i></a>"+grdseparator);
							sb.append("<script>$('#editac"+ta.getAcademicId()+"').tooltip();</script>");
							
							sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+deleteToolTip+"' id='delac"+ta.getAcademicId()+"'  onclick=\"return delRow_academic('"+ta.getAcademicId()+"','"+ta.getStatus()+"')\" ><i class='fa fa-trash-o'></i></a>");
							sb.append("<script>$('#delac"+ta.getAcademicId()+"').tooltip();</script>");
							
						}
					}
					else
					{
						if(ta.getStatus()!=null && ta.getStatus().equalsIgnoreCase("V"))
						{
							if(transcriptFlag==1 && ta.getPathOfTranscript()==null)
							{
								sb.append("	<a href='javascript:void(0)' rel='tooltip' data-original-title='"+editToolTip+"' id='editac"+ta.getAcademicId()+"'  onclick=\"return showRecordToForm('"+ta.getAcademicId()+"','"+ta.getStatus()+"')\" ><i class='fa fa-pencil-square-o fa-lg'></i></a>");
								sb.append("<script>$('#editac"+ta.getAcademicId()+"').tooltip();</script>");
							}
						}
						else
						{
							sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+editToolTip+"' id='editac"+ta.getAcademicId()+"'  onclick=\"return showRecordToForm('"+ta.getAcademicId()+"','"+ta.getStatus()+"')\" ><i class='fa fa-pencil-square-o fa-lg'></i></a>"+grdseparator);
							sb.append("<script>$('#editac"+ta.getAcademicId()+"').tooltip();</script>");
							
							sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+deleteToolTip+"' id='delac"+ta.getAcademicId()+"'  onclick=\"return delRow_academic('"+ta.getAcademicId()+"','"+ta.getStatus()+"')\" ><i class='fa fa-trash-o'></i></a>");
							sb.append("<script>$('#delac"+ta.getAcademicId()+"').tooltip();</script>");
						}
					}
					sb.append("</td>");
				}
				sb.append("</tr>");
			}
			
			if(listTeacherAcademics==null || listTeacherAcademics.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	
	}
	
	public String validateCTAndDec(String sCandidateType)
	{
		String sDec_candidateType="E";
		try {
			sDec_candidateType=Utility.decodeBase64(sCandidateType);
		} catch (Exception e) { e.printStackTrace(); }
		if(sDec_candidateType==null && sDec_candidateType.equals(""))
		{
			sCandidateType="E";
		}
		else
		{
			sCandidateType=sDec_candidateType;
		}
		return sCandidateType;
	}
	
	
	@Transactional(readOnly=false)
	public String getPrevAndNext(List<DspqRouter> dspqRouterList,String sEnc_candidateType, String iJobId,String sGroupOrSec)
	{
		Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
		
		for(DspqRouter dspqRouter :dspqRouterList)
		{
			mapGroup.put(dspqRouter.getDspqSectionMaster().getDspqGroupMaster().getGroupId(), true);
			mapSection.put(dspqRouter.getDspqSectionMaster().getSectionId(), true);
		}
			
		return "";
	}
	
	public String getElectronicReferencesGridNoble(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		return pfCertifications.getElectronicReferencesGridNoble(noOfRow, pageNo, sortOrder, sortOrderType);
	}
	
	public String getElectronicReferencesGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer teacherId,Integer portfolioId,String candidateType,Integer sectionId)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		TeacherDetail teacherDetail =null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb = new StringBuffer();
		//***************************************************************************
		String applicantType="";
		if(candidateType.length()!=1){
			applicantType=validateCTAndDec(candidateType);
		}else{
			if(candidateType.equalsIgnoreCase("")){
			}else{
				applicantType=validateApplicantType(candidateType);
			}
		}
		System.out.println("ApplicantType :::::::::: "+applicantType);
		//******************************************************************************
		if(teacherId!=0){
			teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
		}else{
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		}
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		mapDspqRouterByFID=getDSPQRouterMapById(portfolioId, applicantType,sectionId);
		//**************************************************************************
		
		String lblTitle=getGridHeaderName(mapDspqRouterByFID,48);
		String lblOrganizationEmp=getGridHeaderName(mapDspqRouterByFID,49);
		String lblEmail=getGridHeaderName(mapDspqRouterByFID,51);
		String lblRecLetter=getGridHeaderName(mapDspqRouterByFID,52).replaceAll("&nbsp;", " ");
		String lblContactNo=getGridHeaderName(mapDspqRouterByFID,50);
		String lblRefName=Utility.getLocaleValuePropByKey("lblRefName", locale);
		String lblCanContact=Utility.getLocaleValuePropByKey("lblCanContact", locale);
		String toolViewRecommendationLetter=Utility.getLocaleValuePropByKey("toolViewRecommendationLetter", locale);
		
		String activateToolTip=Utility.getLocaleValuePropByKey("DSPQ_ActivateToolTip", locale);
		String deActivateToolTip=Utility.getLocaleValuePropByKey("DSPQ_DeActivateToolTip", locale);
		
		try 
		{
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------
			List<TeacherElectronicReferences> listTeacherElectronicReferences= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"lastName";
			String sortOrderNoField		=	"lastName";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.isNull("districtMaster");
			listTeacherElectronicReferences = teacherElectronicReferencesDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2);		

			List<TeacherElectronicReferences> sortedTeacherRole		=	new ArrayList<TeacherElectronicReferences>();

			SortedMap<String,TeacherElectronicReferences>	sortedMap = new TreeMap<String,TeacherElectronicReferences>();
			int mapFlag=2;
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherRole.add((TeacherElectronicReferences) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherRole.add((TeacherElectronicReferences) sortedMap.get(key));
				}
			}else{
				sortedTeacherRole=listTeacherElectronicReferences;
			}

			totalRecord =listTeacherElectronicReferences.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherElectronicReferences> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);

			String responseText="";
			sb.append("<table border='0' id='eleReferencesGrid' width='100%' class='table table-striped'>");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblRefName,sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblTitle,sortOrderFieldName,"designation",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblOrganizationEmp,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblEmail,sortOrderFieldName,"email",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblRecLetter,sortOrderFieldName,"pathOfReference",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top' class='recomTit'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblContactNo,sortOrderFieldName,"contactnumber",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblCanContact,sortOrderFieldName,"rdcontacted",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			if(teacherId==0){
				sb.append("<th width='20%' valign='top'>");
				sb.append(lblAct);
				sb.append("</th>");	
			}
			sb.append("</tr>");
			sb.append("</thead>");
			if(listsortedTeacherRole!=null)
				
				
				for(TeacherElectronicReferences tRole:listsortedTeacherRole)
				{
					String Salutation="";
					if(tRole.getSalutation()==0)
						Salutation="";
					else if(tRole.getSalutation()==1)
						Salutation="Mrs.";
					else if(tRole.getSalutation()==2)
						Salutation="Mr.";
					else if(tRole.getSalutation()==3)
						Salutation="Miss";
					else if(tRole.getSalutation()==4)
						Salutation="Dr.";
					else if(tRole.getSalutation()==5)
						Salutation="Ms.";
							
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(Salutation+" "+tRole.getFirstName()+" "+tRole.getLastName());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(tRole.getDesignation());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(tRole.getOrganization());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(tRole.getEmail());
					sb.append("</td>");
					
					
					String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
					sb.append("<td class='recomTit'>");
					sb.append(tRole.getPathOfReference()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='"+toolViewRecommendationLetter+"'  id='trans"+tRole.getElerefAutoId()+"' onclick=\"downloadReference('"+tRole.getElerefAutoId()+"','trans"+tRole.getElerefAutoId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
					sb.append("<script>$('#trans"+tRole.getElerefAutoId()+"').tooltip();</script>");
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(tRole.getContactnumber());
					sb.append("</td>");
					
					sb.append("<td>");
					if(tRole.getRdcontacted())
						sb.append(Utility.getLocaleValuePropByKey("lblY", locale));
					else if(!tRole.getRdcontacted())
						sb.append(Utility.getLocaleValuePropByKey("lblN", locale));
					sb.append("</td>");
					
					if(teacherId==0){
						sb.append("<td>");
						
						sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+editToolTip+"' id='editRef"+tRole.getElerefAutoId()+"'  onclick=\"return editFormElectronicReferences('"+tRole.getElerefAutoId()+"')\" ><i class='fa fa-pencil-square-o fa-lg'></i></a>"+grdseparator);
						sb.append("<script>$('#editRef"+tRole.getElerefAutoId()+"').tooltip();</script>");
						
						if(tRole.getStatus()==0)
						{
							sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+activateToolTip+"' id='activateRef"+tRole.getElerefAutoId()+"'  onclick=\"return changeStatusElectronicReferences('"+tRole.getElerefAutoId()+"','1')\" ><i class='fa fa-check fa-lg'></i></a>");
							sb.append("<script>$('#activateRef"+tRole.getElerefAutoId()+"').tooltip();</script>");
						}
						else if(tRole.getStatus()==1)
						{
							sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+deActivateToolTip+"' id='deactivateRef"+tRole.getElerefAutoId()+"'  onclick=\"return changeStatusElectronicReferences('"+tRole.getElerefAutoId()+"','0')\" ><i class='fa fa-times fa-lg'></i></a>");
							sb.append("<script>$('#deactivateRef"+tRole.getElerefAutoId()+"').tooltip();</script>");
						}
						
						sb.append("</td>");
					}
					sb.append("</tr>");
				}

			if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='8'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationDoubleStringSSPF(request,totalRecord,noOfRow, pageNo));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	
		
	}
	
	public String getVideoLinksGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer teacherId,Integer portfolioId,String candidateType,Integer sectionId)
	{	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		TeacherDetail teacherDetail = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb = new StringBuffer();
				 
		try 
		{
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------
			//***************************************************************************
			String applicantType="";
			if(candidateType.length()!=1){
				applicantType=validateCTAndDec(candidateType);
			}else{
				applicantType=validateApplicantType(candidateType);
			}
			System.out.println("ApplicantType :::::::::: "+applicantType);
			//******************************************************************************
			if(teacherId!=0){
				teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			}else{
				teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			}
			Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
			mapDspqRouterByFID=getDSPQRouterMapById(portfolioId, applicantType,sectionId);
			//**************************************************************************
			String lnkViLnk=getGridHeaderName(mapDspqRouterByFID,54);
			String lblVid=getGridHeaderName(mapDspqRouterByFID,55);
			String lblcretedDate=Utility.getLocaleValuePropByKey("lblcretedDate", locale);
			
			List<TeacherVideoLink> listTeacherVideoLink= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDate";
			String sortOrderNoField		=	"createdDate";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.isNull("districtMaster");
			listTeacherVideoLink = teacherVideoLinksDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2);		

			List<TeacherVideoLink> sortedTeacherRole		=	new ArrayList<TeacherVideoLink>();

			SortedMap<String,TeacherVideoLink>	sortedMap = new TreeMap<String,TeacherVideoLink>();
			int mapFlag=2;
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherRole.add((TeacherVideoLink) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherRole.add((TeacherVideoLink) sortedMap.get(key));
				}
			}else{
				sortedTeacherRole=listTeacherVideoLink;
			}

			totalRecord =listTeacherVideoLink.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherVideoLink> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);
			String video="";
			String responseText="";
			sb.append("<table border='0' id='videoLinktblGrid' class='table table-striped'>");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lnkViLnk,sortOrderFieldName,"videourl",sortOrderTypeVal,pgNo);
			sb.append("<th width='50%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblVid,sortOrderFieldName,"video",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblcretedDate,sortOrderFieldName,"createdDate",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			if(teacherId==0){
				sb.append("<th  width='10%' valign='top'>");
				sb.append(lblAct);
				sb.append("</th>");	
			}
			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherVideoLink!=null)
				
				for(TeacherVideoLink pojo:listsortedTeacherRole)
				{
					sb.append("<tr>");
					sb.append("<td width='50%'>");
					String httpStr="";
					String httpsStr="";
					
					if(pojo.getVideourl().length()>"http://".length()||pojo.getVideourl().length()>"https://".length()){
						httpStr=pojo.getVideourl().substring(0,"http://".length());
						httpsStr=pojo.getVideourl().substring(0,"https://".length());
					}
					if(httpStr.equals("http://"))
						sb.append("<a  href='"+pojo.getVideourl()+"' target='blank'>"+pojo.getVideourl()+"</a>");
					else if(httpsStr.equals("https://"))
						sb.append("<a  href='"+pojo.getVideourl()+"' target='blank'>"+pojo.getVideourl()+"</a>");
					else
						sb.append("<a  href='http://"+pojo.getVideourl()+"' target='blank'>"+pojo.getVideourl()+"</a>");
					sb.append("</td>");
					
					sb.append("<td>");
					
					if(pojo.getVideo()==null || (pojo.getVideo()!=null && pojo.getVideo().equals("")))					
						sb.append("&nbsp;");						
					else	
					{
						sb.append("<a onclick='return getVideoPlay("+pojo.getVideolinkAutoId()+")'  href='javascript:void(0)'>"+pojo.getVideo()+"</a>");
					}
					//"+filepath+"
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(pojo.getCreatedDate()));
					sb.append("</td>");
					if(teacherId==0){
						sb.append("<td>");
						
						sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+editToolTip+"' id='editvideolnk"+pojo.getVideolinkAutoId()+"'  onclick=\"return editFormVideoLink('"+pojo.getVideolinkAutoId()+"')\" ><i class='fa fa-pencil-square-o fa-lg'></i></a>"+grdseparator);
						sb.append("<script>$('#editvideolnk"+pojo.getVideolinkAutoId()+"').tooltip();</script>");
	
						sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+deleteToolTip+"' id='delvideolnk"+pojo.getVideolinkAutoId()+"'  onclick=\"return delVideoLink('"+pojo.getVideolinkAutoId()+"')\" ><i class='fa fa-trash-o'></i></a>");
						sb.append("<script>$('#delvideolnk"+pojo.getVideolinkAutoId()+"').tooltip();</script>");
						
						sb.append("</td>");
					}
					sb.append("</tr>");
				}

			if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='3'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationTrippleGridSSPF(request,totalRecord,noOfRow, pageNo));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	
	}
	
	public String getAdditionalDocumentsGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer teacherId,Integer portfolioId,String candidateType,Integer sectionId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		TeacherDetail teacherDetail=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer sb = new StringBuffer();	
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null)){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			
			//***************************************************************************
			String applicantType="";
			if(candidateType.length()!=1){
				applicantType=validateCTAndDec(candidateType);
			}else{
				if(candidateType.equalsIgnoreCase("")){
				}else{
					applicantType=validateApplicantType(candidateType);
				}
			}
			//******************************************************************************
			if(teacherId!=0){
				teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			}else{
				teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			}
			Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
			mapDspqRouterByFID=getDSPQRouterMapById(portfolioId, applicantType,sectionId);
			//**************************************************************************
			String lbllDocName=getGridHeaderName(mapDspqRouterByFID,56);
			String lblDoc=getGridHeaderName(mapDspqRouterByFID,57);
			String lnkClickHereToViewDoc=Utility.getLocaleValuePropByKey("lnkClickHereToViewDoc", locale);
			
			List<TeacherAdditionalDocuments> lstTeacherAdditionalDocuments=teacherAdditionalDocumentsDAO.findSortedAdditionalDocumentsByTeacher(sortOrderStrVal,teacherDetail);
			
			totalRecord =lstTeacherAdditionalDocuments.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherAdditionalDocuments> teacherAdditionalDocumentsList		=	lstTeacherAdditionalDocuments.subList(start,end);
			
			
			sb.append("<table border='0' id='additionalDocumentsGrid'  width='100%' class='table table-striped mt30'  >");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLinkSSPF(lbllDocName,sortOrderFieldName,"documentName",sortOrderTypeVal,pgNo);
			sb.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblDoc,sortOrderFieldName,"uploadedDocument",sortOrderTypeVal,pgNo);
			sb.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
	
			if(teacherId==0){
				sb.append("<th width='20%' class='net-header-text'>");
				sb.append(lblAct);
				sb.append("</th>");
			}
			sb.append("</tr>");
			sb.append("</thead>");
			
			if(teacherAdditionalDocumentsList!=null && teacherAdditionalDocumentsList.size()>0)
			for(TeacherAdditionalDocuments objTAD:teacherAdditionalDocumentsList)
			{
				sb.append("<tr>");
				
				sb.append("<td>");
				sb.append(objTAD.getDocumentName());	
				sb.append("</td>");
				
				
				
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				sb.append("<td style='text-align:left'>");								
				sb.append(objTAD.getUploadedDocument()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='"+lnkClickHereToViewDoc+"'  id='addDoc"+objTAD.getAdditionDocumentId()+"' onclick=\"downloadUploadedDocument('"+objTAD.getAdditionDocumentId()+"','addDoc"+objTAD.getAdditionDocumentId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
				sb.append("<script>$('#addDoc"+objTAD.getAdditionDocumentId()+"').tooltip();</script>");
				sb.append("</td>");
				if(teacherId==0){
					sb.append("<td>");
					
					sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+editToolTip+"' id='editAdditionalDoc"+objTAD.getAdditionDocumentId()+"'  onclick=\"return showEditTeacherAdditionalDocuments('"+objTAD.getAdditionDocumentId()+"')\" ><i class='fa fa-pencil-square-o fa-lg'></i></a>"+grdseparator);
					sb.append("<script>$('#editAdditionalDoc"+objTAD.getAdditionDocumentId()+"').tooltip();</script>");
	
					sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+deleteToolTip+"' id='delAdditionalDoc"+objTAD.getAdditionDocumentId()+"'  onclick=\"return delRow_Document('"+objTAD.getAdditionDocumentId()+"')\" ><i class='fa fa-trash-o'></i></a>");
					sb.append("<script>$('#delAdditionalDoc"+objTAD.getAdditionDocumentId()+"').tooltip();</script>");
					
					sb.append("</td>");
				}
				
				sb.append("</tr>");
			}
			
			if(teacherAdditionalDocumentsList==null || teacherAdditionalDocumentsList.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	
	}
	
	@Transactional(readOnly=false)
	public JobForTeacher getLatestCoverLetter()
	{
		return null;
	}
	
	@Transactional(readOnly=false)
	public String saveGroup01Section01CoverLetter(String iJobId,Integer isAffilated,String CLText,String staffType,String empNumCoverLtr,String filename)
	{
		printdata("Calling DSPQServiceAjax => saveGroup01Section01CoverLetter");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		String sReturnValue="";
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String sPrevious="",sNext="";
		
		JobOrder jobOrder=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
		}
		
		TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
		if(teacherPersonalInfo==null)
		{
			teacherPersonalInfo=new TeacherPersonalInfo();
			teacherPersonalInfo.setTeacherId(teacherDetail.getTeacherId());
			teacherPersonalInfo.setFirstName(teacherDetail.getFirstName());
			teacherPersonalInfo.setLastName(teacherDetail.getLastName());
		}
		else
		{
			if(empNumCoverLtr!=null && !empNumCoverLtr.equals(""))
			{
				if(empNumCoverLtr!=null && !empNumCoverLtr.equals(""))
					teacherPersonalInfo.setEmployeeNumber(empNumCoverLtr);
				
				teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
			}
		}
		
		JobForTeacher forTeacher=null;
		List<JobForTeacher> lstJobForTeacher=jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail, jobOrder);
		if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
		{
			forTeacher=lstJobForTeacher.get(0);
			if(forTeacher!=null)
			{
				forTeacher.setIsAffilated(isAffilated);
				forTeacher.setCoverLetter(CLText);
				forTeacher.setStaffType(staffType);
				
				if(filename!=null && !filename.equals("") || !filename.equals("0"))
					forTeacher.setCoverLetterFileName(filename);
				
				jobForTeacherDAO.makePersistent(forTeacher);
				sReturnValue="OK";
			}
		}
		else
		{
			forTeacher=new JobForTeacher();
			forTeacher.setTeacherId(teacherDetail);
			forTeacher.setJobId(jobOrder);
			forTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
			forTeacher.setIsAffilated(isAffilated);
			forTeacher.setCoverLetter(CLText);
			forTeacher.setCreatedDateTime(new Date());
			forTeacher.setStaffType(staffType);
			
			if(filename!=null && !filename.equals("") || !filename.equals("0"))
				forTeacher.setCoverLetterFileName(filename);
			
			jobForTeacherDAO.makePersistent(forTeacher);
			sReturnValue="OK";
		}
		
		if(sReturnValue=="OK")
		{
			DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
			if(dspqJobWiseStatus==null)
			{
				dspqJobWiseStatus=new DspqJobWiseStatus();
				dspqJobWiseStatus.setTeacherDetail(teacherDetail);
				dspqJobWiseStatus.setJobOrder(jobOrder);
				dspqJobWiseStatus.setiPAddress(IPAddressUtility.getIpAddress(request));
				dspqJobWiseStatus.setCreatedDataTime(new Date());
				dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
			}
			else
			{
				dspqJobWiseStatus.setCoverLetter(true);
				dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
			}
		}
		
		String sCandidateType="";
		String sEnc_candidateType="";
		
		if(isAffilated==null || isAffilated==0)
		{
			sEnc_candidateType=Utility.encodeInBase64("E");
			sCandidateType="E";
		}
		else if(isAffilated!=null && isAffilated==1)
		{
			String isImCandidate=(String)session.getAttribute("isImCandidate");
			if(isImCandidate!=null && isImCandidate.equalsIgnoreCase("true"))
			{
				sEnc_candidateType=Utility.encodeInBase64("T");
				sCandidateType="T";
			}
			else
			{
				sEnc_candidateType=Utility.encodeInBase64("I");
				sCandidateType="I";
			}
		}
		
		if(sEnc_candidateType!=null && !sEnc_candidateType.equals(""))
		{
			boolean isDspqRequired=false;
			
			boolean isCoverLetterG1=false;
			boolean isPersonalInformationG1=false;
			boolean isAffiliateG4=false;
			boolean isProfessionalG4=false;
			
			Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
			Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
			
			DspqPortfolioName dspqPortfolioNameNew =getDSPQByJobOrder(jobOrder,sCandidateType);
			
			if(dspqPortfolioNameNew!=null)
			{
				isDspqRequired=true;
				
				List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, sCandidateType);
				Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
				isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
				isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
				isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
				isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
				
			}
			JobCategoryMaster jobCategoryMaster=null;
			if(jobOrder!=null )
				jobCategoryMaster=jobOrder.getJobCategoryMaster();
			boolean isQQRequired=getPreSreen(jobCategoryMaster, sCandidateType);
			boolean isEPI=getEPI(jobCategoryMaster, sCandidateType);
			boolean isJSI=getCustomQuestion(jobOrder, jobCategoryMaster, sCandidateType, teacherDetail);
			printdata("sCandidateType "+sCandidateType +" isQQRequired "+isQQRequired);
			
			Map<Integer, String> mapNextAndPreURL=new HashMap<Integer, String>();
			Map<String,Integer> mapNextAndPreNavigater=new HashMap<String,Integer>();
			
			setPrevAndNextNavigation(mapGroup, mapNextAndPreURL, mapNextAndPreNavigater, iJobId, sEnc_candidateType, isDspqRequired, isCoverLetterG1, isPersonalInformationG1, isProfessionalG4, isAffiliateG4, isQQRequired,isEPI,isJSI);
			sNext=getNextNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "CL");
			sPrevious=getPrevNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "CL");
			
			
		}

		return sReturnValue+"||"+sEnc_candidateType+"||"+sNext;
		
	}
	
	@Transactional(readOnly=false)
	public JobForTeacher getCLData(String iJobId)
	{
		printdata("Calling DSPQServiceAjax => getCLData");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		JobOrder jobOrder=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
		}
		
		JobForTeacher forTeacher=null;
		List<JobForTeacher> lstJobForTeacher=jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail, jobOrder);
		if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
			forTeacher=lstJobForTeacher.get(0);
		else
		{
			String isImCandidate=(String)session.getAttribute("isImCandidate");
			if(isImCandidate!=null && isImCandidate.equalsIgnoreCase("true"))
			{
				forTeacher=new JobForTeacher();
				forTeacher.setIsAffilated(1);
				forTeacher.setJobForTeacherId(0l);
			}
		}
		return forTeacher;
		
	}
	
	public String getInstructionForSection(DspqRouter dspqRouter)
	{
		String sReyurnValue="";
		if(dspqRouter!=null && dspqRouter.getInstructions()!=null && !dspqRouter.getInstructions().equals(""))
		{
			sReyurnValue="<div class='row'><div class='col-sm-12 col-md-12 top5' id='instructionId_'"+dspqRouter.getDspqSectionMaster().getSectionId()+"><label><strong>"+dspqRouter.getInstructions()+"</strong></label></div></div>";
		}
		return sReyurnValue;
	}
	
	@Transactional(readOnly=false)
	public String savePersonalInfoData(String jobId,String dspqct,Integer salutation_pi,String firstName_pi,String middleName_pi,String lastName_pi,String anotherName,String ssn_pi,Integer expectedSalary,String phoneNumber,String dob,String addressLine1,String addressLine2,String countryId,String zipCode,String stateIdForDSPQ,String cityIdForDSPQ,String otherState,String otherCity,String addressLine1Pr,String addressLine2Pr,String countryIdPr,String zipCodePr,String stateIdForDSPQPr,String cityIdForDSPQPr,String otherStatePr,String otherCityPr,String ethnicOriginId,String ethinicityId,String raceId,String genderId,String veteran,String mobileNumber,String drivingLicNum,String drivingLicState,Integer knowLanguageChkValue)
	{
		printdata("Calling DSPQServiceAjax => savePersonalInfoData");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		String sReturnValue="";
		
		JobOrder jobOrder=null;
		if(jobId!=null && !jobId.equals(""))
		{
			int iJobId=Utility.getIntValue(jobId);
			jobOrder=jobOrderDAO.findById(iJobId, false, false);
		}
		
		try {
			
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
			if(teacherPersonalInfo==null)
			{
				teacherPersonalInfo=new TeacherPersonalInfo();
			}
			
				if(firstName_pi!=null && !firstName_pi.equalsIgnoreCase(""))
				{
					teacherPersonalInfo.setFirstName(firstName_pi);
					teacherDetail.setFirstName(firstName_pi);
				}
				else
				{	
					if(teacherDetail.getFirstName()!=null)
						teacherPersonalInfo.setFirstName(teacherDetail.getFirstName());
					else
						teacherPersonalInfo.setFirstName("");
				}

				if(middleName_pi!=null && !middleName_pi.equalsIgnoreCase(""))
				{
					teacherPersonalInfo.setMiddleName(middleName_pi);
				}
				else
				{
					teacherPersonalInfo.setMiddleName("");
				}

				if(lastName_pi!=null && !lastName_pi.equalsIgnoreCase(""))
				{
					teacherPersonalInfo.setLastName(lastName_pi);
					teacherDetail.setLastName(lastName_pi);
				}
				else
				{
					if(teacherDetail.getLastName()!=null)
					{
						teacherPersonalInfo.setLastName(teacherDetail.getLastName());
					}
					else
					{
						teacherPersonalInfo.setLastName("");
					}
				}
				
				teacherPersonalInfo.setAnotherName(anotherName);
				Date dob_converted=null;
				if(dob!=null && !dob.equalsIgnoreCase(""))
					dob_converted=Utility.getCurrentDateFormart(dob);
				
				if(dob_converted!=null)
					teacherPersonalInfo.setDob(dob_converted);
				
				teacherPersonalInfo.setSalutation(salutation_pi);
				teacherPersonalInfo.setExpectedSalary(expectedSalary);

				if(phoneNumber!=null && !phoneNumber.equalsIgnoreCase(""))
					teacherPersonalInfo.setPhoneNumber(phoneNumber);
				
				if(mobileNumber!=null && !mobileNumber.equalsIgnoreCase(""))
					teacherPersonalInfo.setMobileNumber(mobileNumber);
				
				ssn_pi=ssn_pi==null?"":ssn_pi;
				boolean validSSN=false;
				try {
					Integer.parseInt(ssn_pi);
					validSSN=true;
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				if(ssn_pi!=null && !ssn_pi.equalsIgnoreCase("") && validSSN && ssn_pi.trim().length()==9)
					teacherPersonalInfo.setSSN(Utility.encodeInBase64(ssn_pi));
				
				if(addressLine1!=null && !addressLine1.equals(""))
					teacherPersonalInfo.setAddressLine1(addressLine1);

				if(addressLine2!=null && !addressLine2.equals(""))
					teacherPersonalInfo.setAddressLine2(addressLine2);

				if(zipCode!=null && !zipCode.equals(""))
					teacherPersonalInfo.setZipCode(zipCode);
				
				if(countryId!=null && !countryId.equals("0"))
				{
					int iCountryId=Utility.getIntValue(countryId);
					if(iCountryId >0)
					{
						CountryMaster countryMaster = countryMasterDAO.findById(iCountryId, false, false);
						if(countryMaster!=null)
							teacherPersonalInfo.setCountryId(countryMaster);
					}
					
					
					if(stateIdForDSPQ!=null && !stateIdForDSPQ.equals("0") && (cityIdForDSPQ!=null && !cityIdForDSPQ.equals("")))
					{
						StateMaster stateMaster = stateMasterDAO.findById(Long.parseLong(stateIdForDSPQ), false, false);
						
						CityMaster cityMaster = cityMasterDAO.findById(Long.parseLong(cityIdForDSPQ), false, false);
						
						if(stateMaster!=null)
							teacherPersonalInfo.setStateId(stateMaster);
						
						if(cityMaster!=null)
							teacherPersonalInfo.setCityId(cityMaster);
						
						if(otherState!=null && !otherState.equals(""))
							teacherPersonalInfo.setOtherState(otherState);
						
						if(otherCity!=null && !otherCity.equals(""))
							teacherPersonalInfo.setOtherCity(otherState);
					}
				}

				
				if(addressLine1Pr!=null && !addressLine1Pr.equals(""))
					teacherPersonalInfo.setPresentAddressLine1(addressLine1Pr);

				if(addressLine2Pr!=null && !addressLine2Pr.equals(""))
					teacherPersonalInfo.setPresentAddressLine2(addressLine2Pr);

				if(zipCodePr!=null && !zipCodePr.equals(""))
					teacherPersonalInfo.setPresentZipCode(zipCodePr);
				
				if(countryIdPr!=null && !countryIdPr.equals(""))
				{
					int iCountryId=Utility.getIntValue(countryIdPr);
					if(iCountryId >0)
					{
						CountryMaster countryMaster = countryMasterDAO.findById(iCountryId, false, false);
						if(countryMaster!=null)
							teacherPersonalInfo.setPresentCountryId(countryMaster);
					}
					
					
					StateMaster stateMaster = new StateMaster();
					CityMaster cityMaster = new CityMaster();

					if(stateIdForDSPQPr!=null && !stateIdForDSPQPr.equals("") && (cityIdForDSPQPr!=null && !cityIdForDSPQPr.equals("0")))
					{
						stateMaster = stateMasterDAO.findById(Long.parseLong(stateIdForDSPQPr), false, false);
						cityMaster = cityMasterDAO.findById(Long.parseLong(cityIdForDSPQPr), false, false);

						teacherPersonalInfo.setPresentStateId(stateMaster);
						teacherPersonalInfo.setPresentCityId(cityMaster);
						
						if(otherStatePr!=null && !otherStatePr.equals(""))
							teacherPersonalInfo.setPersentOtherState(otherStatePr);
						
						if(otherCityPr!=null && !otherCityPr.equals(""))
							teacherPersonalInfo.setPersentOtherCity(otherStatePr);
					}
				}
				
			
				if(veteran!=null && !veteran.equals(""))
					teacherPersonalInfo.setIsVateran(Utility.getIntValue(veteran));
				
				if(teacherPersonalInfo.getTeacherId()==null)
				{
					teacherPersonalInfo.setTeacherId(teacherDetail.getTeacherId());
					teacherPersonalInfo.setCreatedDateTime(new Date());
					teacherPersonalInfo.setIsDone(false);
				}
				
				
				if(raceId!=null && !raceId.equals(""))
					teacherPersonalInfo.setRaceId(raceId);


				if(genderId!=null && !genderId.equals("-1"))
				{
					GenderMaster genderMaster = genderMasterDAO.findById(Utility.getIntValue(genderId), false, false);
					teacherPersonalInfo.setGenderId(genderMaster);
				}

				if(ethnicOriginId!=null && !ethnicOriginId.equals("-1"))
				{
					EthnicOriginMaster ethnicOriginMaster = ethnicOriginMasterDAO.findById(Utility.getIntValue(ethnicOriginId), false, false);
					teacherPersonalInfo.setEthnicOriginId(ethnicOriginMaster);
				}

				if(ethinicityId!=null && !ethinicityId.equals("-1"))
				{
					EthinicityMaster ethinicityMaster = ethinicityMasterDAO.findById(Utility.getIntValue(ethinicityId), false, false);
					teacherPersonalInfo.setEthnicityId(ethinicityMaster);
				}
				
				if(drivingLicNum!=null && !drivingLicNum.equals(""))
					teacherPersonalInfo.setDrivingLicNum(drivingLicNum);
				
				if(drivingLicState!=null && !drivingLicState.equals(""))
					teacherPersonalInfo.setDrivingLicState(drivingLicState);
				
				teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
				teacherDetailDAO.makePersistent(teacherDetail);
				
				try {
					if(knowLanguageChkValue!=null && knowLanguageChkValue!=-1)
					{
						TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
						if(teacherExperience!=null)
						{
							if(knowLanguageChkValue==1)
								teacherExperience.setKnowOtherLanguages(true);
							else
								teacherExperience.setKnowOtherLanguages(false);
						}
						else
						{
							teacherExperience=new TeacherExperience();
							teacherExperience.setTeacherId(teacherDetail);
							teacherExperience.setCanServeAsSubTeacher(2);
							teacherExperience.setCreatedDateTime(new Date());
							
						}
						teacherExperienceDAO.makePersistent(teacherExperience);
						
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				
				DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
				if(dspqJobWiseStatus==null)
				{
					dspqJobWiseStatus=new DspqJobWiseStatus();
					dspqJobWiseStatus.setTeacherDetail(teacherDetail);
					dspqJobWiseStatus.setJobOrder(jobOrder);
					dspqJobWiseStatus.setiPAddress(IPAddressUtility.getIpAddress(request));
					dspqJobWiseStatus.setCreatedDataTime(new Date());
				}
				dspqJobWiseStatus.setPersonaInfo(true);
				dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
			
				int iIsAffilated=0;
				String sDecIsAffilated=validateCTAndDec(dspqct);
				if(sDecIsAffilated!=null && (sDecIsAffilated.equalsIgnoreCase("I") || sDecIsAffilated.equalsIgnoreCase("T")))
					iIsAffilated=1;
				updateJFT(dspqJobWiseStatus,teacherDetail, jobOrder, iIsAffilated);
				
				TeacherPortfolioStatus portfolioStatus=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				if(portfolioStatus!=null)
				{
					portfolioStatus.setIsPersonalInfoCompleted(true);
				}
				else
				{
					portfolioStatus=new TeacherPortfolioStatus();
					portfolioStatus.setTeacherId(teacherDetail);
					portfolioStatus.setIsPersonalInfoCompleted(true);
					
					portfolioStatus.setIsAcademicsCompleted(false);
					portfolioStatus.setIsCertificationsCompleted(false);
					portfolioStatus.setIsExperiencesCompleted(false);
					portfolioStatus.setIsAffidavitCompleted(false);
				}

				try {
					teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			sReturnValue="OK";
			
			
		} catch (Exception e) {
			e.printStackTrace();
			sReturnValue="";
		}
		
		
		
		return sReturnValue;
		
	}
	
	
	
	public String[] getStateByCountry(Integer countryId,String stateModuleFlag)
	{
		return stateAjax.getStateByCountry(countryId, stateModuleFlag);
	}
	
	public String[] getStateByCountryPresent(Integer countryId,String stateModuleFlag)
	{
		return stateAjax.getStateByCountryPresent(countryId, stateModuleFlag);
	}

	public String getPFEmploymentGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer teacherId,Integer portfolioId,String candidateType,Integer sectionId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		TeacherDetail teacherDetail = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		StringBuffer sb = new StringBuffer();
		
		try 
		{
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------
			//***************************************************************************
			String applicantType="";
			if(candidateType.length()!=1){
				applicantType=validateCTAndDec(candidateType);
			}else{
				if(candidateType.equalsIgnoreCase("")){
				}else{
					applicantType=validateApplicantType(candidateType);
				}
			}
			//******************************************************************************
			if(teacherId!=0){
				teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			}else{
				teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			}
			Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
			mapDspqRouterByFID=getDSPQRouterMapById(portfolioId, applicantType,sectionId);
			//**************************************************************************
			String lblRole=getGridHeaderName(mapDspqRouterByFID,60);
			String lblOrganizationEmp=getGridHeaderName(mapDspqRouterByFID,62);
			String lblDuration=getGridHeaderName(mapDspqRouterByFID,63);
			String lblTypOfRole=getGridHeaderName(mapDspqRouterByFID,69);
			String lblTpe=getGridHeaderName(mapDspqRouterByFID,61);
			
			List<TeacherRole> listTeacherRole= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"roleStartYear";
			String sortOrderNoField		=	"roleStartYear";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("type") && !sortOrder.equals("empRoleTypeMaster")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("type")){
					sortOrderNoField="type";
				}
				if(sortOrder.equals("empRoleTypeMaster")){
					sortOrderNoField="empRoleTypeMaster";
				}

			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
			/**End ------------------------------------**/

			listTeacherRole = teacherRoleDAO.findSortedTeacherRoleByTeacher(sortOrderStrVal,teacherDetail);

			List<TeacherRole> sortedTeacherRole		=	new ArrayList<TeacherRole>();

			SortedMap<String,TeacherRole>	sortedMap = new TreeMap<String,TeacherRole>();
			SortedMap<Long,TeacherRole>	sortedIntMap = new TreeMap<Long,TeacherRole>();
			if(sortOrderNoField.equals("type"))
			{
				sortOrderFieldName	=	"type";
			}
			if(sortOrderNoField.equals("empRoleTypeMaster"))
			{
				sortOrderFieldName	=	"empRoleTypeMaster";
			}
			int mapFlag=2;
			int sortingDynaVal=99999;
			for (TeacherRole trRole : listTeacherRole){
				String orderFieldName=trRole.getRole();
				if(sortOrderFieldName.equals("type")){
					orderFieldName=trRole.getFieldMaster().getFieldName()+"||"+trRole.getRoleId();
					sortedMap.put(orderFieldName+"||",trRole);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("empRoleTypeMaster")){
					orderFieldName=trRole.getEmpRoleTypeMaster().getEmpRoleTypeName()+"||"+trRole.getRoleId();
					sortedMap.put(orderFieldName+"||",trRole);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("roleStartYear")){
					sortingDynaVal--;
					
					String sYearMonth="";
					if(trRole.getRoleStartYear()!=null && trRole.getRoleStartMonth()!=null)
						sYearMonth=trRole.getRoleStartYear()+trRole.getRoleStartMonth().toString()+sortingDynaVal;
					else if(trRole.getRoleStartYear()!=null && trRole.getRoleStartMonth()==null)
						sYearMonth=trRole.getRoleStartYear().toString()+sortingDynaVal;
					else if(trRole.getRoleStartYear()==null && trRole.getRoleStartMonth()!=null)
						sYearMonth=trRole.getRoleStartMonth().toString()+sortingDynaVal;
					
					Long lYearMonth=Utility.getLongValue(sYearMonth);
					
					sortedIntMap.put(lYearMonth,trRole);

					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
			}

			if(sortOrderFieldName.equals("roleStartYear")){
				if(mapFlag==1){
					NavigableSet<String> navig = ((TreeMap)sortedIntMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						sortedTeacherRole.add((TeacherRole) sortedIntMap.get(key));
					} 
				}else if(mapFlag==0){
					Iterator iterator = sortedIntMap.keySet().iterator();
					while (iterator.hasNext()) {
						Object key = iterator.next();
						sortedTeacherRole.add((TeacherRole) sortedIntMap.get(key));
					}
				}else{
					sortedTeacherRole=listTeacherRole;
				}
			}
			else{
				if(mapFlag==1){
					NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						sortedTeacherRole.add((TeacherRole) sortedMap.get(key));
					} 
				}else if(mapFlag==0){
					Iterator iterator = sortedMap.keySet().iterator();
					while (iterator.hasNext()) {
						Object key = iterator.next();
						sortedTeacherRole.add((TeacherRole) sortedMap.get(key));
					}
				}else{
					sortedTeacherRole=listTeacherRole;
				}
			}

			totalRecord =listTeacherRole.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherRole> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);

			String responseText="";
			sb.append("<table border='0' id='employeementGrid' class='table table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblRole,sortOrderFieldName,"role",sortOrderTypeVal,pgNo);
			sb.append("<th width='200px;' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblOrganizationEmp,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
			sb.append("<th width='100px;' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblTpe,sortOrderFieldName,"type",sortOrderTypeVal,pgNo);
			sb.append("<th width='150px;' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblDuration,sortOrderFieldName,"roleStartYear",sortOrderTypeVal,pgNo);
			sb.append("<th width='150px;' valign='top' id='empDurationHeader'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblTypOfRole,sortOrderFieldName,"empRoleTypeMaster",sortOrderTypeVal,pgNo);
			sb.append("<th width='200px;' valign='top'>"+responseText+"</th>");
			if(teacherId==0){
				sb.append("<th width='100px;' class='net-header-text'>");
				sb.append(lblAct);
				sb.append("</th>");	
			}
			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherRole!=null)
				for(TeacherRole tRole:listsortedTeacherRole)
				{
					if(tRole.getNoWorkExp()!=null && tRole.getNoWorkExp()==true)
					{
						sb.append("<tr>");
						sb.append("<td colspan='5'>");
						sb.append("I do not have any work experience.");
						sb.append("</td>");
						if(teacherId==0){
							sb.append("<td>");
	
							sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+editToolTip+"' id='editEmploymentHistory"+tRole.getRoleId()+"'  onclick=\"return showEditFormEmployment('"+tRole.getRoleId()+"')\" ><i class='fa fa-pencil-square-o fa-lg'></i></a>"+grdseparator);
							sb.append("<script>$('#editEmploymentHistory"+tRole.getRoleId()+"').tooltip();</script>");
	
							sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+deleteToolTip+"' id='delEmploymentHistory"+tRole.getRoleId()+"'  onclick=\"return deleteEmployment('"+tRole.getRoleId()+"')\" ><i class='fa fa-trash-o'></i></a>");
							sb.append("<script>$('#delEmploymentHistory"+tRole.getRoleId()+"').tooltip();</script>");
							
							sb.append("</td>");
						}
						sb.append("</tr>");
					}
					else
					{
						sb.append("<tr>");
						sb.append("<td>");
						sb.append(tRole.getRole());
						sb.append("</td>");
						
						sb.append("<td>");
						if(tRole.getOrganization()!=null)
						sb.append(tRole.getOrganization());
						sb.append("</td>");

						sb.append("<td>");
						sb.append(tRole.getFieldMaster().getFieldName());
						sb.append("</td>");

						sb.append("<td class='empDurationRec'>");

						if(tRole.getCurrentlyWorking())
						{
							sb.append(tRole.getRoleStartYear()+" to Present");
						}	
						else
						{
							sb.append(tRole.getRoleStartYear()+" to "+tRole.getRoleEndYear());
						}
						sb.append("</td>");
						String currency="";
						String amount="";
						if(tRole.getCurrencyMaster()!=null){
							currency=tRole.getCurrencyMaster().getCurrencyShortName();
						}
						if(tRole.getAmount()!=null){
							amount=tRole.getAmount()+"";
						}

						sb.append("<td class='empRoleTypegrd' id='empRoleTypegrd_"+tRole.getRoleId()+"'>");
						if(tRole.getEmpRoleTypeMaster()!=null)
						{
							sb.append(tRole.getEmpRoleTypeMaster().getEmpRoleTypeName());
						}
						sb.append("</td>");	
						if(teacherId==0){
							sb.append("<td>");
	
							sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+editToolTip+"' id='editEmploymentHistory"+tRole.getRoleId()+"'  onclick=\"return showEditFormEmployment('"+tRole.getRoleId()+"')\" ><i class='fa fa-pencil-square-o fa-lg'></i></a>"+grdseparator);
							sb.append("<script>$('#editEmploymentHistory"+tRole.getRoleId()+"').tooltip();</script>");
	
							sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+deleteToolTip+"' id='delEmploymentHistory"+tRole.getRoleId()+"'  onclick=\"return deleteEmployment('"+tRole.getRoleId()+"')\" ><i class='fa fa-trash-o'></i></a>");
							sb.append("<script>$('#delEmploymentHistory"+tRole.getRoleId()+"').tooltip();</script>");
							
							sb.append("</td>");
						}
						sb.append("</tr>");
					}
					
				}

			if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	
		
	}
	
	public String getInvolvementGridDistrictSpecific(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer districtId,String jobcategoryName)
	{
		return pFExperiences.getInvolvementGridDistrictSpecific(noOfRow, pageNo, sortOrder, sortOrderType, districtId, jobcategoryName);
	}
	
	public String getInvolvementGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer teacherId,Integer portfolioId,String candidateType,Integer sectionId)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		TeacherDetail teacherDetail =null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		StringBuffer sb = new StringBuffer();
		try 
		{
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------
			//***************************************************************************
			String applicantType="";
			if(candidateType.length()!=1){
				applicantType=validateCTAndDec(candidateType);
			}else{
				if(candidateType.equalsIgnoreCase("")){
				}else{
					applicantType=validateApplicantType(candidateType);
				}
			}
			//******************************************************************************
			if(teacherId!=0){
				teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			}else{
				teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			}
			Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
			mapDspqRouterByFID=getDSPQRouterMapById(portfolioId, applicantType,sectionId);
			//**************************************************************************
			String lblOrga=getGridHeaderName(mapDspqRouterByFID,73);
			String lblTpe=getGridHeaderName(mapDspqRouterByFID,74);
			String lblNumberofPeople=Utility.getLocaleValuePropByKey("lblNumberofPeople", locale);
			String lblNumPeLed=Utility.getLocaleValuePropByKey("lblNumPeLed", locale);
			
			List<TeacherInvolvement> listTeacherInvolvements = null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"organization";
			String sortOrderNoField		=	"organization";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("orgtype")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("orgtype")){
					sortOrderNoField="orgtype";
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/


			listTeacherInvolvements = teacherInvolvementDAO.findSortedTeacherInvolvementByTeacher(sortOrderStrVal,teacherDetail);


			List<TeacherInvolvement> sortedlistTeacherInvolvement		=	new ArrayList<TeacherInvolvement>();

			SortedMap<String,TeacherInvolvement>	sortedMap = new TreeMap<String,TeacherInvolvement>();
			if(sortOrderNoField.equals("orgtype"))
			{
				sortOrderFieldName	=	"orgtype";
			}
			int mapFlag=2;
			for (TeacherInvolvement trInvolvement : listTeacherInvolvements){
				String orderFieldName=trInvolvement.getOrganization();
				if(sortOrderFieldName.equals("orgtype")){
					orderFieldName=trInvolvement.getOrgTypeMaster().getOrgType()+"||"+trInvolvement.getInvolvementId();
					sortedMap.put(orderFieldName+"||",trInvolvement);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}

			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherInvolvement.add((TeacherInvolvement) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherInvolvement.add((TeacherInvolvement) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherInvolvement=listTeacherInvolvements;
			}

			totalRecord =sortedlistTeacherInvolvement.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherInvolvement> listsortedTeacherCertificates		=	sortedlistTeacherInvolvement.subList(start,end);
			String responseText="";

			sb.append("<table border='0' id='involvementGrid' class='table table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblOrga,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblTpe,sortOrderFieldName,"orgtype",sortOrderTypeVal,pgNo);
			sb.append("<th width='15%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblNumberofPeople,sortOrderFieldName,"peopleRangeMaster",sortOrderTypeVal,pgNo);
			sb.append("<th width='25%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblNumPeLed,sortOrderFieldName,"leadNoOfPeople",sortOrderTypeVal,pgNo);
			sb.append("<th width='25%' valign='top'>"+responseText+"</th>");
			if(teacherId==0){
				sb.append("<th width='15%' class='net-header-text'>");
				sb.append(lblAct);
				sb.append("</th>");	
			}
			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherInvolvements!=null)
				for(TeacherInvolvement tInv:listsortedTeacherCertificates)
				{
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(tInv.getOrganization());
					sb.append("</td>");

					sb.append("<td>");
					if(tInv.getOrgTypeMaster()!=null)
						sb.append(tInv.getOrgTypeMaster().getOrgType());
					sb.append("</td>");

					sb.append("<td>");
					if(tInv.getPeopleRangeMaster()!=null)
					sb.append(tInv.getPeopleRangeMaster().getRange());	
					sb.append("</td>");

					sb.append("<td>");
					sb.append(tInv.getLeadNoOfPeople()==null?"":tInv.getLeadNoOfPeople());	
					sb.append("</td>");	
					if(teacherId==0){
						sb.append("<td>");				
						sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+editToolTip+"' id='editInvolvement"+tInv.getInvolvementId()+"'  onclick=\"return showEditFormInvolvement('"+tInv.getInvolvementId()+"')\" ><i class='fa fa-pencil-square-o fa-lg'></i></a>"+grdseparator);
						sb.append("<script>$('#editInvolvement"+tInv.getInvolvementId()+"').tooltip();</script>");
	
						sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+deleteToolTip+"' id='delInvolvement"+tInv.getInvolvementId()+"'  onclick=\"return deleteRecordInvolvment('"+tInv.getInvolvementId()+"')\" ><i class='fa fa-trash-o'></i></a>");
						sb.append("<script>$('#delInvolvement"+tInv.getInvolvementId()+"').tooltip();</script>");
						
						sb.append("</td>");
					}
					sb.append("</tr>");
				}

			if(listTeacherInvolvements==null || listTeacherInvolvements.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	
		
	}
	public String getHonorsGrid(Integer teacherId,Integer portfolioId,String candidateType,Integer sectionId)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		TeacherDetail teacherDetail =null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		StringBuffer sb = new StringBuffer();
		try 
		{
			//***************************************************************************
			String applicantType="";
			if(candidateType.length()!=1){
				applicantType=validateCTAndDec(candidateType);
			}else{
				if(candidateType.equalsIgnoreCase("")){
				}else{
					applicantType=validateApplicantType(candidateType);
				}
			}
			//******************************************************************************
			if(teacherId!=0){
				teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			}else{
				teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			}
			Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
			mapDspqRouterByFID=getDSPQRouterMapById(portfolioId, applicantType,sectionId);
			//**************************************************************************
			String lblAward=getGridHeaderName(mapDspqRouterByFID,78);
			String lblYear=getGridHeaderName(mapDspqRouterByFID,119);

			List<TeacherHonor> listTeacherHonors = null;
			listTeacherHonors = teacherHonorDAO.findTeacherHonersByTeacher(teacherDetail);

			String responseText="";

			sb.append("<table border='0' id='honorsGrid' class='table table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			sb.append("<th width='20%' valign='top'>"+lblAward+"</th>");

			sb.append("<th width='20%' valign='top'>"+lblYear+"</th>");
			if(teacherId==0){
			 sb.append("<th width='15%' valign='top'>"+lblAct+"</th>");
			}
			sb.append("</tr>");
			sb.append("</thead>");

			if(listTeacherHonors!=null)
				for(TeacherHonor tHon:listTeacherHonors)
				{
					sb.append("<tr>");
					sb.append("<td>"); 
						sb.append(tHon.getHonor());
					sb.append("</td>");
					sb.append("<td>"); 
					sb.append(tHon.getHonorYear());
				sb.append("</td>");
					if(teacherId==0){
						sb.append("<td>");
						sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+editToolTip+"' id='editHonor"+tHon.getHonorId()+"'  onclick=\"return showEditHonors('"+tHon.getHonorId()+"')\" ><i class='fa fa-pencil-square-o fa-lg'></i></a>"+grdseparator);
						sb.append("<script>$('#editHonor"+tHon.getHonorId()+"').tooltip();</script>");
	
						sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+deleteToolTip+"' id='delHonor"+tHon.getHonorId()+"'  onclick=\"return deleteRecordHonors('"+tHon.getHonorId()+"')\" ><i class='fa fa-trash-o'></i></a>");
						sb.append("<script>$('#delHonor"+tHon.getHonorId()+"').tooltip();</script>");
						
						sb.append("</td>");
					}
					sb.append("</tr>");
				}

			if(listTeacherHonors==null || listTeacherHonors.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
				
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	
	}
	
	
	@Transactional(readOnly=false)
	public String getCustomFieldBySectionId(DistrictMaster dspqPotfolioDM,String candidateType,DspqGroupMaster dspqGroupMaster,DspqSectionMaster dspqSectionMaster,List<String> listToolTips,String sSectionNameCustFld,List<String> listDateCals,DspqPortfolioName dspqPortfolioName,JobOrder jobOrder)
	{
		printdata("Calling DSPQServiceAjax => getCustomFieldBySectionId "+dspqSectionMaster.getSectionId() +" "+dspqSectionMaster.getSectionName());
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		StringBuffer sb=new StringBuffer();
		
		///
		List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsList=new ArrayList<DistrictSpecificPortfolioQuestions>();
		boolean bDisaplayNewQuestion=true;
		boolean SelfServicePortfolioStatus=false;
		if(dspqPotfolioDM!=null && dspqPotfolioDM.getSelfServicePortfolioStatus()!=null && dspqPotfolioDM.getSelfServicePortfolioStatus())
		{
			SelfServicePortfolioStatus=getSelfServicePortfolioStatus(dspqPotfolioDM);
		}
		if(SelfServicePortfolioStatus)
		{
			boolean isAlsoSA=false;
			if(session!=null && session.getAttribute("isAlsoSA")!=null && session.getAttribute("isAlsoSA").equals(true)) 
			{
				isAlsoSA=true;
			}
			Map<Integer,List<DistrictSpecificPortfolioQuestions>> dSPQ=districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestion(jobOrder,isAlsoSA);
			
			int dspqType=0;
			try{
				for(int i=0;i<5;i++)
				{
					if(dSPQ.get(i)!=null)
					{
						districtSpecificPortfolioQuestionsList=dSPQ.get(i);
						dspqType=i;
						break;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(districtSpecificPortfolioQuestionsList.size()>0)
			{
				
				List<DistrictSpecificPortfolioAnswers>	lastList = districtSpecificPortfolioAnswersDAO.findSpecificPortfolioAnswersTP(teacherDetail,jobOrder,dspqType);
				if(lastList!=null && lastList.size()>0)
				{
					bDisaplayNewQuestion=false;
				}
			}
		}
		
		
		///
		boolean bDisplayFinalView=true;
		List<DistrictSpecificPortfolioQuestions> lstDistrictSpecificPortfolioQuestions=null;
		if(bDisaplayNewQuestion)
		{
			lstDistrictSpecificPortfolioQuestions=districtSpecificPortfolioQuestionsDAO.getCustumFieldBySection(dspqPotfolioDM, dspqGroupMaster, dspqSectionMaster,candidateType,dspqPortfolioName);
			bDisplayFinalView=true;
		}
		else
		{
			lstDistrictSpecificPortfolioQuestions=districtSpecificPortfolioQuestionsList;
			
			List<Object> lstDistSpecQuestions= new ArrayList<Object>();
			lstDistSpecQuestions=districtSpecificPortfolioQuestionsDAO.getSectionForNewSS(candidateType, dspqPortfolioName);
			
			System.out.println("lstDistSpecQuestions "+lstDistSpecQuestions.size());
			
			if(lstDistSpecQuestions!=null && lstDistSpecQuestions.size()>0)
			{
				if(lstDistSpecQuestions.size()==1)
				{
					Object object=lstDistSpecQuestions.get(0);
					Object[] objArr1 = (Object[]) object;
					int iNewQuestionSectionId=Utility.getIntValue(objArr1[0]+"");

					if(iNewQuestionSectionId==dspqSectionMaster.getSectionId())
					{
						bDisplayFinalView=true;
					}
					else
					{
						bDisplayFinalView=false;
					}
				}
				
			}
			
		}
		
		Map<Integer, DistrictSpecificPortfolioAnswers> mapQA=new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
		List<DistrictSpecificPortfolioAnswers> lstDistrictSpecificPortfolioAnswers = new ArrayList<DistrictSpecificPortfolioAnswers>();
		if(lstDistrictSpecificPortfolioQuestions!=null && lstDistrictSpecificPortfolioQuestions.size()>0 && bDisplayFinalView)
		{
			lstDistrictSpecificPortfolioAnswers=districtSpecificPortfolioAnswersDAO.getDSPAByTID(teacherDetail, lstDistrictSpecificPortfolioQuestions);
		
			for(DistrictSpecificPortfolioAnswers obj:lstDistrictSpecificPortfolioAnswers)
			{
				mapQA.put(obj.getDistrictSpecificPortfolioQuestions().getQuestionId(), obj);
			}
		}
		if(lstDistrictSpecificPortfolioQuestions!=null && lstDistrictSpecificPortfolioQuestions.size()>0 && bDisplayFinalView)
		{
			
			int iTotalCountNo_CustomField=lstDistrictSpecificPortfolioQuestions.size();
			sb.append("<input type='hidden' id='customFld_"+sSectionNameCustFld+"' customFldTotalCount='"+iTotalCountNo_CustomField+"' value="+iTotalCountNo_CustomField+">");
			int iCustFldCounter=0;
			
			for(DistrictSpecificPortfolioQuestions dspq:lstDistrictSpecificPortfolioQuestions)
			{
				sb.append("<div class='row'>");
				
				iCustFldCounter++;
				List<DistrictSpecificPortfolioOptions> lstDistrictSpecificPortfolioOptions=dspq.getQuestionOptions();
				String sShortName=dspq.getQuestionTypeMaster().getQuestionTypeShortName();
				
				if(dspq!=null)
				{
					String sOptionId="_"+dspq.getQuestionId()+"_"+iCustFldCounter;
					String sCustomFieldInstructio=getCustomFieldInstruction(dspq);
					if(sCustomFieldInstructio!=null && !sCustomFieldInstructio.equals(""))
					{
						sCustomFieldInstructio=sCustomFieldInstructio.replaceAll("&amp;", "");
						sCustomFieldInstructio=sCustomFieldInstructio.replaceAll("nbsp;", " ");
						sCustomFieldInstructio.trim();
						sb.append(sCustomFieldInstructio);
					}
					
					String sTextAReaValue="";
					DistrictSpecificPortfolioAnswers Qans=null;
					if(mapQA!=null && mapQA.get(dspq.getQuestionId())!=null)
					{
						Qans=mapQA.get(dspq.getQuestionId());
						if(Qans!=null)
						{
							if(Qans.getInsertedText()!=null && !Qans.getInsertedText().equals(""))
							{
								sTextAReaValue=Qans.getInsertedText();
							}
						}
					}
					if(sCustomFieldInstructio==null || sCustomFieldInstructio.equals(""))
						sb.append("<div class='col-sm-12 col-md-12 mt10'>");
					else
						sb.append("<div class='col-sm-12 col-md-12'>");
					
					sb.append("<label><strong>"+getLabelAndCustomFieldAttributes(dspq, listToolTips,iCustFldCounter,sSectionNameCustFld,Qans)+"</strong></label>");
					if(sShortName.equalsIgnoreCase("ml") || sShortName.equalsIgnoreCase("et"))
					{
						sb.append("<div id='divOpt"+sOptionId+"'>");
						sb.append("<textarea name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' rows='8' style='width:100%;' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+sTextAReaValue+"</textarea><br/>");
						sb.append("</div>");
					}
					else if(sShortName.equalsIgnoreCase("mlsel"))
					{
						Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
						String sListValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
						{
							sListValue=Qans.getSelectedOptions();
							String arrOp[]=sListValue.split("\\|");
							if(arrOp.length >0)
							{
								for(String sOpt :arrOp)
								{
									int iOpt=Utility.getIntValue(sOpt);
									mapOption.put(iOpt, true);
								}
							}
						}
						
						for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions) 
						{
							sb.append("<label class='checkbox inline' style='margin-top: 0px;margin-bottom: 2px;'>");
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
								sb.append("<input type='checkbox' checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							else
								sb.append("<input type='checkbox' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							sb.append("</label>");
						}
					}
					else if(sShortName.equalsIgnoreCase("mloet"))
					{
						Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
						String sListValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
						{
							sListValue=Qans.getSelectedOptions();
							String arrOp[]=sListValue.split("\\|");
							if(arrOp.length >0)
							{
								for(String sOpt :arrOp)
								{
									int iOpt=Utility.getIntValue(sOpt);
									mapOption.put(iOpt, true);
								}
							}
						}
						for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
						{
							sb.append("<label class='checkbox inline' style='margin-top: 0px;margin-bottom: 2px;'>");
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
								sb.append("<input type='checkbox' checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							else
								sb.append("<input type='checkbox' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							
							sb.append("</label>");
						}
						sb.append("<div id='divOpt"+sOptionId+"'>");
						sb.append("&nbsp;&nbsp;<textarea name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' rows='8' style='width:100%;' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+sTextAReaValue+"</textarea><br/>");
						sb.append("</div>");
					}
					else if(sShortName.equalsIgnoreCase("sloet"))
					{
						Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
						String sListValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
						{
							sListValue=Qans.getSelectedOptions();
							String arrOp[]=sListValue.split("\\|");
							if(arrOp.length >0)
							{
								for(String sOpt :arrOp)
								{
									int iOpt=Utility.getIntValue(sOpt);
									mapOption.put(iOpt, true);
								}
							}
						}
						
						for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
						{
							sb.append("<label class='radio' style='margin-top: 0px;margin-bottom: 2px;'>");
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
								sb.append("<input checked type='radio' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							else
								sb.append("<input type='radio' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							sb.append("</label>");
						}
						sb.append("<div id='divOpt"+sOptionId+"'>");
						sb.append("&nbsp;&nbsp;<textarea name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' rows='8' style='width:100%;' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+sTextAReaValue+"</textarea><br/>");
						sb.append("</div>");
					}
					else if(sShortName.equalsIgnoreCase("tf") || sShortName.equalsIgnoreCase("slsel"))
					{
						Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
						String sListValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
						{
							sListValue=Qans.getSelectedOptions();
							String arrOp[]=sListValue.split("\\|");
							if(arrOp.length >0)
							{
								for(String sOpt :arrOp)
								{
									int iOpt=Utility.getIntValue(sOpt);
									mapOption.put(iOpt, true);
								}
							}
						}
						
						for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
						{
							sb.append("<label class='radio' style='margin-top: 0px;margin-bottom: 2px;'>");
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
								sb.append("<input type='radio' checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							else
								sb.append("<input type='radio' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							sb.append("</label>");
						}
					}
					else if(sShortName.equalsIgnoreCase("OSONP"))
					{
						Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
						String sListValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
						{
							sListValue=Qans.getSelectedOptions();							
							String arrOp[]=sListValue.split("\\|");
							if(arrOp.length >0)
							{
								for(String sOpt :arrOp)
								{
									int iOpt=Utility.getIntValue(sOpt);
									mapOption.put(iOpt, true);
								}
							}							
						}
						
						for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
						{
							System.out.println("sonu  dqo "+dqo.getOptionId() +" mapOption.get(dqo.getOptionId()" +mapOption.get(dqo.getOptionId()));
							sb.append("<label class='radio' style='margin-top: 0px;margin-bottom: 2px;'>");
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
								sb.append("<input type='radio' checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							else
								sb.append("<input type='radio' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							sb.append("</label>");
						}
					}
					else if(sShortName.equalsIgnoreCase("drsls") || sShortName.equalsIgnoreCase("DD"))
					{
						if(lstDistrictSpecificPortfolioOptions!=null)
						{
							String sListValue="";
							if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
								sListValue=Qans.getSelectedOptions();
							
							sb.append("<select name='opt"+sOptionId+"' id='opt"+sOptionId+"' class='form-control' style='width: 410px;'>");
							sb.append("<option value='0'>Select "+dspq.getQuestion()+"</option>");
							for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
							{
								
								if(sListValue!=null && !sListValue.equals("") && sListValue.equals(dqo.getOptionId()+""))
									sb.append("<option selected value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</option>");
								else
									sb.append("<option value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</option>");
							}
							sb.append("</select>");
						}
					}
					else if(sShortName.equalsIgnoreCase("dt"))
					{
						String sDateValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
							sDateValue=Qans.getSelectedOptions();
						sb.append("<input type='text' value='"+sDateValue+"' id='opt"+sOptionId+"' name='opt"+sOptionId+"' maxlength='0' class='form-control' style='width: 120px;'> ");
						listDateCals.add("opt"+sOptionId);
					}
					else if(sShortName.equalsIgnoreCase("sl"))
					{
						String sTextValue="";
						//if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
						if(Qans!=null && Qans.getInsertedText()!=null && !Qans.getInsertedText().equals(""))
							sTextValue=Qans.getInsertedText();
						sb.append("<input type='text' value='"+sTextValue+"' id='opt"+sOptionId+"' name='opt"+sOptionId+"' class='form-control' style='width: 450px;'> ");
					}
					else if(sShortName.equalsIgnoreCase("UAT") || sShortName.equalsIgnoreCase("UATEF"))
					{
						String lnkViewAnswer=Utility.getLocaleValuePropByKey("lnkViewAnswer", locale);
						String lnkV=Utility.getLocaleValuePropByKey("lnkV", locale);
						
						String fileName="";
						if(Qans!=null && Qans.getFileName()!=null && !Qans.getFileName().equalsIgnoreCase(""))
						{
							fileName=Qans.getFileName();
						}
						
						sb.append("<iframe name='ssiFrmName"+sOptionId+"' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");
						sb.append("<form id='frmSSCFldID"+sOptionId+"' enctype='multipart/form-data' method='post' target='ssiFrmName"+sOptionId+"' action='selfServiceCustomFieldFileUploadServlet.do' class='form-inline'>");
						sb.append("<div class='col-sm-6 col-md-6'>");
						sb.append("<input name='opt"+sOptionId+"DBFileName' id='opt"+sOptionId+"DBFileName' type='hidden' value='"+fileName+"' />");
						sb.append("<input name='opt"+sOptionId+"File' id='opt"+sOptionId+"File' type='file'/>");
						sb.append("</div>");
						
						sb.append("<div class='col-sm-6 col-md-6 top5' id='divAnswerTxt'>");
						String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
						if(Qans!=null && Qans.getFileName()!=null && !Qans.getFileName().equalsIgnoreCase(""))
						{
							sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+lnkViewAnswer+"' id='jafHREFAnswerFile"+Qans.getAnswerId()+"' onclick=\"downloadCustomFileUpload('"+Qans.getAnswerId()+"');"+windowFunc+"\">"+lnkV+"</a>");
							listToolTips.add("jafHREFAnswerFile"+Qans.getAnswerId());
						}
						sb.append("</div>");
					
						sb.append("</form>");
						
						if(sShortName.equalsIgnoreCase("UATEF"))
						{
							sb.append("<div id='divOpt"+sOptionId+"'>");
							sb.append("&nbsp;&nbsp;<textarea name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' rows='8' style='width:100%;' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+sTextAReaValue+"</textarea><br/>");
							sb.append("</div>");
						}
						
					}
					else if(sShortName.equalsIgnoreCase("sswc"))
					{
						Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
						String sListValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
						{
							sListValue=Qans.getSelectedOptions();
							String arrOp[]=sListValue.split("\\|");
							if(arrOp.length >0)
							{
								for(String sOpt :arrOp)
								{
									int iOpt=Utility.getIntValue(sOpt);
									mapOption.put(iOpt, true);
								}
							}
						}
						
						for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
						{
							sb.append("<label class='radio' style='margin-top: 0px;margin-bottom: 2px;'>");
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
								sb.append("<input type='radio' checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							else
								sb.append("<input type='radio' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							sb.append("</label>");
						}
						sb.append("<div class='row left10 top5'>");
						String sDateValue="";
						if(Qans!=null && Qans.getInsertedText()!=null && !Qans.getInsertedText().equals(""))
							sDateValue=Qans.getInsertedText();
						sb.append("<input type='text' value='"+sDateValue+"' id='opt"+sOptionId+"' name='opt"+sOptionId+"' maxlength='0' class='form-control' style='width: 120px;'> ");
						listDateCals.add("opt"+sOptionId);
						sb.append("</div>");
					}
					sb.append("</div></div>");
				}
			}
		}
		return sb.toString();
	}
	
	
	public String getLabelAndCustomFieldAttributes(DistrictSpecificPortfolioQuestions dspq,List<String> listToolTips,int iCustomFieldIdCounter,String sSectionNameCustFld,DistrictSpecificPortfolioAnswers Qans)
	{
		String sReturnValue="";
		int iOptionCount=0;
		if(dspq!=null && dspq.getQuestionOptions()!=null)
			iOptionCount=dspq.getQuestionOptions().size();
		
		int iAnswerId=0;
		if(Qans!=null && Qans.getAnswerId()!=null)
			iAnswerId=Qans.getAnswerId();
		
		try {
			if(dspq.getQuestion()!=null && !dspq.getQuestion().equals(""))
				sReturnValue="<span id='lblCustomFieldId_"+sSectionNameCustFld+"_"+iCustomFieldIdCounter+"' customFieldQNo='"+dspq.getQuestionId()+"' customFieldShortCode='"+dspq.getQuestionTypeMaster().getQuestionTypeShortName()+"' customFieldRequired='"+dspq.getIsRequired()+"' customFieldOtionCount='"+iOptionCount+"' customFieldQuestionTypeId='"+dspq.getQuestionTypeMaster().getQuestionTypeId()+"' customFieldLabelAnswer='"+dspq.getQuestion()+"' customFieldAnswerID='"+iAnswerId+"'>"+dspq.getQuestion()+"</span><span id='lblCustomFieldIdWarningBeforeSpace_"+dspq.getQuestionId()+"'></span><span id='lblCustomFieldIdWarning_"+dspq.getQuestionId()+"'></span><span id='lblCustomFieldIdWarningAfterSpace_"+dspq.getQuestionId()+"'></span>";
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			if(dspq.getIsRequired()==1)
				sReturnValue=sReturnValue+"<span class='required'>*</span>";
		} catch (Exception e) {

		}
		
		try {
			if(dspq.getTooltip()!=null && !dspq.getTooltip().equals(""))
			{
				sReturnValue=sReturnValue+"&nbsp;<a href='#' id='CustomFieldId_tooltip_"+dspq.getQuestionId()+"' rel='tooltip' data-original-title='"+dspq.getTooltip()+"'><img src='images/qua-icon.png' width='15' height='15' alt=''></a>";
				listToolTips.add("CustomFieldId_tooltip_"+dspq.getQuestionId());
			}
		} catch (Exception e) {}
		
		
		return sReturnValue;
	}
	
	public String getCustomFieldInstruction(DistrictSpecificPortfolioQuestions dspq)
	{
		StringBuffer sb=new StringBuffer();
		try {
			if(dspq.getQuestionInstructions()!=null && !dspq.getQuestionInstructions().equals(""))
			{
				
				sb.append("<div class='col-sm-12 col-md-12 mt10'>");
				sb.append("<span>"+dspq.getQuestionInstructions()+"</span>");
				sb.append("</div>");
				
			}
		} catch (Exception e) {	}
		
		return sb.toString();
	}
	
	//save Academics controls
	public String saveOrUpdateAcademics(String academicId, String degreeId, String universityId, String fieldId, 
			String attendedInYear, String leftInYear, String gpaCumulative, String gpaFreshmanYear,String gpaSophomoreYear,
			String gpaJuniorYear,  String gpaSeniorYear,  String degreeType, String fileName/*,int international*/)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			
			TeacherAcademics teacherAcademics = null;
			UniversityMaster universityMaster  = null;
			FieldOfStudyMaster fieldOfStudyMaster=null;
			DegreeMaster degreeMaster = degreeMasterDAO.findById(new Long(degreeId.trim()), false, false);
			
			
			if(universityId!=null && !universityId.trim().equals("") && !universityId.trim().equals(0)){
				universityMaster = universityMasterDAO.findById(new Integer(universityId.trim()), false, false);
			}
			
			if(fieldId!=null && !fieldId.trim().equals("") && !fieldId.trim().equals(0))
			fieldOfStudyMaster = fieldOfStudyMasterDAO.findById(new Long(fieldId.trim()), false, false);
			
			if(academicId==null || academicId.equals("") || academicId.equals("0"))
			{
				teacherAcademics = new TeacherAcademics();				
			}
			else
			{
				teacherAcademics = teacherAcademicsDAO.findById(new Long(academicId.trim()), false, false);
			}
			
			teacherAcademics.setTeacherId(teacherDetail);
			teacherAcademics.setDegreeId(degreeMaster);
			teacherAcademics.setUniversityId(universityMaster);
			teacherAcademics.setFieldId(fieldOfStudyMaster);
			if(attendedInYear!=null && !attendedInYear.equals(""))
			teacherAcademics.setAttendedInYear(new Long(attendedInYear));
			
			if(leftInYear!=null && !leftInYear.equals(""))
				teacherAcademics.setLeftInYear(new Long(leftInYear));
			
			teacherAcademics.setCreatedDateTime(new Date());
			
			if(fileName!=null && !fileName.equals(""))
			{
				String filePath =Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+teacherAcademics.getPathOfTranscript();
				File file=new File(filePath);
				file.delete();
				teacherAcademics.setPathOfTranscript(fileName);
			}
			
			if(degreeType.trim().equalsIgnoreCase("B"))
			{
				if (gpaFreshmanYear!=null && !gpaFreshmanYear.trim().equals(""))
					teacherAcademics.setGpaFreshmanYear(new Double(gpaFreshmanYear));
				else
					teacherAcademics.setGpaFreshmanYear(null);
				
				if (gpaJuniorYear!=null && !gpaJuniorYear.trim().equals(""))
					teacherAcademics.setGpaJuniorYear(new Double(gpaJuniorYear));
				else
					teacherAcademics.setGpaJuniorYear(null);
						
				if (gpaSophomoreYear!=null && !gpaSophomoreYear.trim().equals(""))
					teacherAcademics.setGpaSophomoreYear(new Double(gpaSophomoreYear));
				else
					teacherAcademics.setGpaSophomoreYear(null);
							
				if (gpaSeniorYear!=null && !gpaSeniorYear.trim().equals(""))
					teacherAcademics.setGpaSeniorYear(new Double(gpaSeniorYear));
				else
					teacherAcademics.setGpaSeniorYear(null);
								
				if (gpaCumulative!=null && !gpaCumulative.trim().equals(""))
					teacherAcademics.setGpaCumulative(new Double(gpaCumulative));
				else
					teacherAcademics.setGpaCumulative(null);
			}
			else
			{
				if (gpaCumulative!=null && !gpaCumulative.trim().equals(""))
					teacherAcademics.setGpaCumulative(new Double(gpaCumulative));
				else
					teacherAcademics.setGpaCumulative(null);
			}
			teacherAcademicsDAO.makePersistent(teacherAcademics);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "OK";
	}
	
	public boolean deletePFAcademinGrid(String id)
	{
		return pFAcademics.deletePFAcademinGrid(id);
	}
	public TeacherAcademics showEditAcademics(String academicId)
	{
		return pFAcademics.showEditAcademics(academicId);
	}
	
	@Transactional(readOnly=false)
	public String saveCustomFieldAnswer(DistrictSpecificPortfolioAnswers[] dspas)
	{
		printdata("Calling DSPQServiceAjax => saveCustomFieldAnswer");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		String sReturnValue="OK";
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		List<DistrictSpecificPortfolioQuestions> lstDistrictSpecificPortfolioQuestions=new ArrayList<DistrictSpecificPortfolioQuestions>();
		for (DistrictSpecificPortfolioAnswers dspa : dspas) 
			lstDistrictSpecificPortfolioQuestions.add(dspa.getDistrictSpecificPortfolioQuestions());

		Map<Integer, DistrictSpecificPortfolioAnswers> mapDSPQTemp=new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
		List<DistrictSpecificPortfolioAnswers> lstDistrictSpecificPortfolioAnswers=districtSpecificPortfolioAnswersDAO.getDSPAByTID(teacherDetail, lstDistrictSpecificPortfolioQuestions);
		if(lstDistrictSpecificPortfolioAnswers!=null && lstDistrictSpecificPortfolioAnswers.size()>0)
		{
			for(DistrictSpecificPortfolioAnswers dspaTemp:lstDistrictSpecificPortfolioAnswers)
			{
				mapDSPQTemp.put(dspaTemp.getDistrictSpecificPortfolioQuestions().getQuestionId(), dspaTemp);
				printdata("dspaTemp.getDistrictSpecificPortfolioQuestions().getQuestionId() "+dspaTemp.getDistrictSpecificPortfolioQuestions().getQuestionId());
			}
		}
		DistrictMaster districtMaster=null;
		boolean bIsNew=true;
		for (DistrictSpecificPortfolioAnswers dspa : dspas) 
		{
			bIsNew=true;
			
			if(mapDSPQTemp!=null && mapDSPQTemp.get(dspa.getDistrictSpecificPortfolioQuestions().getQuestionId())!=null)
			{
				bIsNew=false;
				DistrictSpecificPortfolioAnswers answerIdTemp=mapDSPQTemp.get(dspa.getDistrictSpecificPortfolioQuestions().getQuestionId());
				if(dspa.getSelectedOptions()!=null && !dspa.getSelectedOptions().equals("") && dspa.getSelectedOptions().contains(","))
					answerIdTemp.setSelectedOptions(dspa.getSelectedOptions().replace(",", "|"));
				else if(dspa.getSelectedOptions()!=null && !dspa.getSelectedOptions().equals(""))
					answerIdTemp.setSelectedOptions(dspa.getSelectedOptions());
				else
					answerIdTemp.setSelectedOptions(null);
				
				answerIdTemp.setInsertedText(dspa.getInsertedText());
				answerIdTemp.setFileName(dspa.getFileName());
				
				if(answerIdTemp.getJobOrder()!=null && answerIdTemp.getJobOrder().getDistrictMaster()!=null)
					answerIdTemp.setDistrictMaster(answerIdTemp.getJobOrder().getDistrictMaster());
				else if (answerIdTemp.getDistrictMaster()!=null)
					answerIdTemp.setDistrictMaster(answerIdTemp.getDistrictMaster());
				try {
					districtSpecificPortfolioAnswersDAO.makePersistent(answerIdTemp);
				} catch (Exception e) {
					e.printStackTrace();
					sReturnValue="error";
				}
			}
			
			if(bIsNew)
			{
				if(districtMaster==null)
				{
					 JobOrder jobOrder= jobOrderDAO.findById(dspa.getJobOrder().getJobId(), false, false);
					 if(jobOrder!=null)
						 districtMaster=jobOrder.getDistrictMaster();
				}
				
				if(dspa.getSelectedOptions()!=null && !dspa.getSelectedOptions().equals("") && dspa.getSelectedOptions().contains(","))
					dspa.setSelectedOptions(dspa.getSelectedOptions().replace(",", "|"));
				else if(dspa.getSelectedOptions()!=null && !dspa.getSelectedOptions().equals(""))
					dspa.setSelectedOptions(dspa.getSelectedOptions());
				else
					dspa.setSelectedOptions(null);
				
				dspa.setAnswerId(null);
				dspa.setTeacherDetail(teacherDetail);
				dspa.setIsActive(true);
				dspa.setCreatedDateTime(new Date());
				dspa.setDistrictMaster(districtMaster);
				try {
					districtSpecificPortfolioAnswersDAO.makePersistent(dspa);
				} catch (Exception e) {
					e.printStackTrace();
					sReturnValue="error";
				}
			}
		}
		
		return sReturnValue;
	}
	
	public String displayTeacherLanguage(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer teacherId,Integer portfolioId,String candidateType,Integer sectionId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		TeacherDetail teacherDetail = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb = new StringBuffer();
		try 
		{
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------
			//printdata(start+" start "+end);
			//***************************************************************************
			String applicantType="";
			if(candidateType.length()!=1){
				applicantType=validateCTAndDec(candidateType);
			}else{
				if(candidateType.equalsIgnoreCase("")){
				}else{
					applicantType=validateApplicantType(candidateType);
				}
			}
			//******************************************************************************
			if(teacherId!=0){
				teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			}else{
				teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			}
			Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
			mapDspqRouterByFID=getDSPQRouterMapById(portfolioId, applicantType,sectionId);
			//**************************************************************************
			String lblLanguage=getGridHeaderName(mapDspqRouterByFID,95);
			String lblOralSkill=getGridHeaderName(mapDspqRouterByFID,117);
			String lblWrittenSkill=getGridHeaderName(mapDspqRouterByFID,118);
			 
			List<TeacherLanguages> listTeacherExp= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			
			listTeacherExp = teacherLanguagesDAO.findByCriteria(sortOrderStrVal,criterion1);		

			List<TeacherLanguages> sortedTeacherRole		=	new ArrayList<TeacherLanguages>();

			SortedMap<String,TeacherLanguages>	sortedMap = new TreeMap<String,TeacherLanguages>();
			int mapFlag=2;
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherRole.add((TeacherLanguages) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherRole.add((TeacherLanguages) sortedMap.get(key));
				}
			}else{
				sortedTeacherRole=listTeacherExp;
			}

			totalRecord =listTeacherExp.size();
			if(totalRecord<end)
				end=totalRecord;
			List<TeacherLanguages> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);

			String responseText="";
			sb.append("<table border='0' id='teacherLanguageGrid' class='table table-striped'>");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			sb.append("<th width='20%' valign='top'>"+lblLanguage+"</th>");

			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblOralSkill,sortOrderFieldName,"oralSkills",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			sb.append("<th width='20%' valign='top'>"+lblWrittenSkill+"</th>");
			if(teacherId==0){
				sb.append("<th  width='20%' valign='top'>");
				sb.append(lblAct);
				sb.append("</th>");	
			}
			sb.append("</tr>");
			sb.append("</thead>");
			Map<Integer,String> optionMap = new HashMap<Integer, String>();
			optionMap.put(1, "Polite");
			optionMap.put(2, "Literate");
			optionMap.put(3, "Fluent");
			if(listTeacherExp!=null)
				
				for(TeacherLanguages pojo:listsortedTeacherRole)
				{
					sb.append("<tr>");
					sb.append("<td>");
					
					if(pojo.getLanguage()!=null && !pojo.getLanguage().equals(""))
						sb.append(pojo.getLanguage());
					else if(pojo.getLanguage()==null && pojo.getSpokenLanguageMaster()!=null)
						sb.append(pojo.getSpokenLanguageMaster().getLanguageName());
					
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(optionMap.get(pojo.getOralSkills()));
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(optionMap.get(pojo.getWrittenSkills()));
					sb.append("</td>");
					
					if(teacherId==0){
						sb.append("<td>");
						
						sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+editToolTip+"' id='editac"+pojo.getTeacherLanguageId()+"'  onclick=\"return editTeacherLanguage('"+pojo.getTeacherLanguageId()+"')\" ><i class='fa fa-pencil-square-o fa-lg'></i></a>"+grdseparator);
						sb.append("<script>$('#editac"+pojo.getTeacherLanguageId()+"').tooltip();</script>");
	
						sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+deleteToolTip+"' id='delac"+pojo.getTeacherLanguageId()+"'  onclick=\"return delTeacherLanguage('"+pojo.getTeacherLanguageId()+"')\" ><i class='fa fa-trash-o'></i></a>");
						sb.append("<script>$('#delac"+pojo.getTeacherLanguageId()+"').tooltip();</script>");
						
						sb.append("</td>");
					}
					sb.append("</tr>");
				}

			if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='3'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationTrippleGrid(request,totalRecord,noOfRow, pageNo));
			sb.append("<input type='hidden' id='ttlTeacherLanguage' value='"+listsortedTeacherRole.size()+"'>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	
	}
	
	public Integer insertOrUpdateTchrLang(String language,Integer oralSkill,Integer writtenSkill,Integer teacherLanguageId,Integer spokenLanguage)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		try
		{
			TeacherLanguages teacherLanguage = new TeacherLanguages();
			printdata(" Spoken Language Master  "+language+"  "+oralSkill+"  "+writtenSkill+"  "+teacherLanguageId +" "+spokenLanguage);
			if(teacherLanguageId!=null && teacherLanguageId!=0)
				teacherLanguage= teacherLanguagesDAO.findById(teacherLanguageId, false, false);
					
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");		
			teacherLanguage.setTeacherDetail(teacherDetail);
			if(spokenLanguage!=null && spokenLanguage > 0)
			{
				SpokenLanguageMaster spokenLanguageMaster=new SpokenLanguageMaster();
				spokenLanguageMaster.setSpokenlanguagemasterid(spokenLanguage);
				teacherLanguage.setSpokenLanguageMaster(spokenLanguageMaster);
				teacherLanguage.setLanguage(null);
			}
			
			if(spokenLanguage!=null && spokenLanguage==-1)
			{
				teacherLanguage.setLanguage(language);
				teacherLanguage.setSpokenLanguageMaster(null);
			}
			
			teacherLanguage.setOralSkills(oralSkill);
			teacherLanguage.setWrittenSkills(writtenSkill);
			teacherLanguage.setCreatedDateTime(new Date());
			teacherLanguagesDAO.makePersistent(teacherLanguage);
			return 1;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return 0;
	}
	
	public TeacherLanguages showEditLang(String id)
	{
		return pFExperiences.showEditLang(id);
	}
	
	public Boolean delTeacherLanguage(String id)
	{
		return pFExperiences.delTeacherLanguage(id);
	}
	
	public String downloadTranscript(String academicId)
	{
		return pFAcademics.downloadTranscript(academicId);
	}
	
	
	@Transactional(readOnly=false)
	public String updatedspqJobWiseStatusAcademic(Integer dspqPortfolioNameId,String dspqct,int jobId)
	{
		printdata("Calling DSPQServiceAjax => updatedspqJobWiseStatusAcademic");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		String sReturnValue="OK";
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		boolean bDspqJobWiseStatus=true;
		String sGroupMsg="";
		boolean bSection06Academics=false;
		
		int iDegreeType=0;
		String sDegreeTypeError="";
		boolean bMatchDegreeType=false;
		
		String sDegreeHierarchy="";
		String sDegreeHierarchyError="";
		boolean bNoDegreeHierarchy=false;
		
		String sCandidateTypeDec=validateCTAndDec(dspqct);
		DspqPortfolioName dspqPortfolioName=dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		if(dspqPortfolioName!=null)
		{
			DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
			dspqSectionMaster.setSectionId(6);
			
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCTAndSec(dspqPortfolioName, sCandidateTypeDec, dspqSectionMaster);
			if(dspqRouterList!=null && dspqRouterList.size()>0)
			{
				if(dspqRouterList.get(0).getStatus()!=null && dspqRouterList.get(0).getStatus().equalsIgnoreCase("A"))
				{
					if(dspqRouterList.get(0).getIsRequired()!=null)
						bSection06Academics=dspqRouterList.get(0).getIsRequired();
					
					if(dspqRouterList.get(0).getNumberRequired()!=null)
						iDegreeType=dspqRouterList.get(0).getNumberRequired();
					
					if(dspqRouterList.get(0).getOthersAttribute()!=null)
						sDegreeHierarchy=dspqRouterList.get(0).getOthersAttribute();
				}
			}
		}
		
		
		
		if(bSection06Academics || iDegreeType > 0 || sDegreeHierarchy.equals("1"))
		{
			List<TeacherAcademics> lstTeacherAcademics=new ArrayList<TeacherAcademics>();
			lstTeacherAcademics=teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
			
			// ### 1 Required atleast one degree ###
			if(bSection06Academics)
			{
				if(lstTeacherAcademics==null || lstTeacherAcademics.size()==0)
				{
					bDspqJobWiseStatus=false;
					sGroupMsg="6";
					sReturnValue="Error";
				}
			}
			
			// ### 2 Degree Type ###
			if(iDegreeType > 0)
			{
				//1- High School(H), 2- Associate Degree(H), 3-Bachelor Degree(H), 4-Master Degree(H), 5 -Phd(H) 
				
				String sDegreeType="";
				String sDegreeTypeName="";
				
				if(iDegreeType==1)
				{
					sDegreeType="H";
					sDegreeTypeName="a High School";
				}
				else if(iDegreeType==2)
				{
					sDegreeType="A";
					sDegreeTypeName="an Associate Degree";
				}
				else if(iDegreeType==3)
				{
					sDegreeType="B";
					sDegreeTypeName="a Bachelor Degree";
				}
				else if(iDegreeType==4)
				{
					sDegreeType="M";
					sDegreeTypeName="a Master Degree";
				}
				else if(iDegreeType==5)
				{
					sDegreeType="D";
					sDegreeTypeName="a Phd";
				}
				
				if(lstTeacherAcademics==null || lstTeacherAcademics.size()==0)
				{
					bMatchDegreeType=true;
				}
				else if(!bMatchDegreeType)
				{
					for(TeacherAcademics academics :lstTeacherAcademics)
					{
						if(academics!=null && academics.getDegreeId()!=null && academics.getDegreeId().getDegreeType()!=null && academics.getDegreeId().getDegreeType().equalsIgnoreCase(sDegreeType) && !bMatchDegreeType)
						{
							bMatchDegreeType=true;
							break;
						}
					}
				}
				
				if(!bMatchDegreeType)
				{
					bDspqJobWiseStatus=false;
					sReturnValue="Error";
					
					sDegreeTypeError="This job requires "+sDegreeTypeName+".";
				}
			}
			
			// ### 3 Degree Hierarchy ###
			if(sDegreeHierarchy.equals("1"))
			{
				TreeMap<Integer,Boolean> tmDegreeHierarchy = new TreeMap<Integer, Boolean>();
				
				for(TeacherAcademics academics :lstTeacherAcademics)
				{
					if(academics!=null && academics.getDegreeId()!=null && academics.getDegreeId().getDegreeType()!=null)
					{
						 int iDegreeTypeTemp=getNumericDegreeType(academics.getDegreeId().getDegreeType());
						 tmDegreeHierarchy.put(iDegreeTypeTemp, true);
					}
				}
				
				if(tmDegreeHierarchy==null || tmDegreeHierarchy.size()==0)
				{
					bNoDegreeHierarchy=true;
				}
				else
				{
					List<Integer> lstMissingDegreeType=new ArrayList<Integer>();
					
					int iMaxDegreeType=0;
					
					int iCandidateProvicedMaxDegreeType=tmDegreeHierarchy.lastKey();
					
					if(iDegreeType >0 && iDegreeType > iCandidateProvicedMaxDegreeType)
						iMaxDegreeType=iDegreeType;
					else
						iMaxDegreeType=iCandidateProvicedMaxDegreeType;
					
					printdata("iCandidateProvicedMaxDegreeType "+iCandidateProvicedMaxDegreeType +" iDegreeType "+iDegreeType +" == > iMaxDegreeType "+iMaxDegreeType);
					
					if(iMaxDegreeType==5)
					{
						iMaxDegreeType=4;
					}
					
					for(int i=1;i <=iMaxDegreeType; i++)
					{
						Boolean bTempDegreeHierarchy=tmDegreeHierarchy.get(i);
						if(bTempDegreeHierarchy==null)
							lstMissingDegreeType.add(i);
					}
					
					String sTempError="";
					if(lstMissingDegreeType!=null && lstMissingDegreeType.size() >0)
					{
						sReturnValue="Error";
						int iTemoCounter=0;
						for(int iDTCode:lstMissingDegreeType)
						{
							iTemoCounter++;
							String sTempReturnValue=getDegreeTypeNameByNumericCode(iDTCode);
							if(sTempError.equals(""))
								sTempError=sTempReturnValue;
							else if(iTemoCounter==lstMissingDegreeType.size())
								sTempError=sTempError+" and "+sTempReturnValue;
							else
								sTempError=sTempError+", "+sTempReturnValue;
							
						}
						sDegreeHierarchyError="Please add "+sTempError +".";
						if(sDegreeHierarchyError!=null && !sDegreeHierarchyError.equals(""))
							sDegreeHierarchyError="This job requires multiple degrees. "+sDegreeHierarchyError;
					}
					
				}
			}
			
		}
		
		JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
		if(bDspqJobWiseStatus)
		{
			
			if(dspqJobWiseStatus==null)
			{
				dspqJobWiseStatus=new DspqJobWiseStatus();
				dspqJobWiseStatus.setTeacherDetail(teacherDetail);
				dspqJobWiseStatus.setJobOrder(jobOrder);
				dspqJobWiseStatus.setiPAddress(IPAddressUtility.getIpAddress(request));
				dspqJobWiseStatus.setCreatedDataTime(new Date());
			}
			dspqJobWiseStatus.setAcademic(true);
			
			try 
			{
				dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
			} 
			catch (Exception e) 
			{
				sReturnValue="Error";
			}
		}
		
		int iIsAffilated=0;
		String sDecIsAffilated=validateCTAndDec(dspqct);
		if(sDecIsAffilated!=null && (sDecIsAffilated.equalsIgnoreCase("I") || sDecIsAffilated.equalsIgnoreCase("T")))
			iIsAffilated=1;
		updateJFT(dspqJobWiseStatus, teacherDetail, jobOrder, iIsAffilated);
		
		TeacherPortfolioStatus portfolioStatus=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
		if(portfolioStatus!=null)
		{
			portfolioStatus.setIsAcademicsCompleted(true);
		}
		else
		{
			portfolioStatus=new TeacherPortfolioStatus();
			portfolioStatus.setTeacherId(teacherDetail);
			portfolioStatus.setIsAcademicsCompleted(true);
			
			portfolioStatus.setIsPersonalInfoCompleted(false);
			portfolioStatus.setIsCertificationsCompleted(false);
			portfolioStatus.setIsExperiencesCompleted(false);
			portfolioStatus.setIsAffidavitCompleted(false);
		}

		try {
			teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return sReturnValue+"||"+sGroupMsg+"||"+sDegreeTypeError+"||"+sDegreeHierarchyError;
	}
	
	public Boolean removeTranscript(String academicId)
	{
		return pFAcademics.removeTranscript(academicId);
	}
	
	
	@Transactional(readOnly=false)
	public String updateCredentialsGroup(Integer dspqPortfolioNameId,String dspqct,Integer jobId,Integer Substitute_Teacher,Integer tfaAffiliate,Integer corpsYear,Integer tfaRegion,Integer nationalBoardCert,Integer nationalBoardCertYear,Integer isNonTeacher,Double expCertTeacherTraining)
	{
		printdata("Calling DSPQServiceAjax => updateCredentialsGroup");
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		String sReturnValue="OK";
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String sGroupMsg="";
		boolean bDspqJobWiseStatus=true;
		boolean bSection08CertificationLicensure=false;
		boolean bSection09References=false;
		boolean bSection10VideoLinks=false;
		boolean bSection11AdditionalDocuments=false;
		
		int iSection09NumberRequired=0;
		
		String sCandidateTypeDec=validateCTAndDec(dspqct);
		DspqPortfolioName dspqPortfolioName=dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		if(dspqPortfolioName!=null)
		{
			List<DspqSectionMaster> lstDspqSectionMasters=new ArrayList<DspqSectionMaster>();
			DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
			
			dspqSectionMaster.setSectionId(8);
			lstDspqSectionMasters.add(dspqSectionMaster);
			
			dspqSectionMaster=new DspqSectionMaster();
			dspqSectionMaster.setSectionId(9);
			lstDspqSectionMasters.add(dspqSectionMaster);
			
			dspqSectionMaster=new DspqSectionMaster();
			dspqSectionMaster.setSectionId(10);
			lstDspqSectionMasters.add(dspqSectionMaster);
			
			dspqSectionMaster=new DspqSectionMaster();
			dspqSectionMaster.setSectionId(11);
			lstDspqSectionMasters.add(dspqSectionMaster);
			
			if(lstDspqSectionMasters.size() >0)
			{
				List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCTAndLstSec(dspqPortfolioName, sCandidateTypeDec, lstDspqSectionMasters);
				if(dspqRouterList!=null && dspqRouterList.size()>0)
				{
					for(DspqRouter dspqRouter :dspqRouterList)
					{
						if(dspqRouter.getDspqSectionMaster()!=null && dspqRouter.getDspqSectionMaster().getSectionId()==9 && dspqRouter.getNumberRequired()!=null && dspqRouter.getStatus()!=null && dspqRouter.getStatus().equalsIgnoreCase("A"))
							iSection09NumberRequired=dspqRouter.getNumberRequired();
						else if(dspqRouter.getDspqSectionMaster()!=null && dspqRouter.getDspqSectionMaster().getSectionId()==8 && dspqRouter.getIsRequired()!=null && dspqRouter.getStatus()!=null && dspqRouter.getStatus().equalsIgnoreCase("A"))
							bSection08CertificationLicensure=dspqRouter.getIsRequired();
						else if(dspqRouter.getDspqSectionMaster()!=null && dspqRouter.getDspqSectionMaster().getSectionId()==9 && dspqRouter.getIsRequired()!=null && dspqRouter.getStatus()!=null && dspqRouter.getStatus().equalsIgnoreCase("A"))
							bSection09References=dspqRouter.getIsRequired();
						else if(dspqRouter.getDspqSectionMaster()!=null && dspqRouter.getDspqSectionMaster().getSectionId()==10 && dspqRouter.getIsRequired()!=null && dspqRouter.getStatus()!=null && dspqRouter.getStatus().equalsIgnoreCase("A"))
							bSection10VideoLinks=dspqRouter.getIsRequired();
						else if(dspqRouter.getDspqSectionMaster()!=null && dspqRouter.getDspqSectionMaster().getSectionId()==11 && dspqRouter.getIsRequired()!=null && dspqRouter.getStatus()!=null && dspqRouter.getStatus().equalsIgnoreCase("A"))
							bSection11AdditionalDocuments=dspqRouter.getIsRequired();
					}
				}
			}
			
		}
		
		if(bSection08CertificationLicensure)
		{
			List<TeacherCertificate> lstTeacherCertificate=new ArrayList<TeacherCertificate>();
			lstTeacherCertificate=teacherCertificateDAO.findCertificateAscByTeacher(teacherDetail);
			if(lstTeacherCertificate==null || lstTeacherCertificate.size()==0)
			{
				bDspqJobWiseStatus=false;
				sReturnValue="Error";
				if(sGroupMsg.equals(""))
					sGroupMsg="8";
				else
					sGroupMsg=sGroupMsg+",8";
			}
		}
		
		if(bSection10VideoLinks)
		{
			List<TeacherVideoLink> lstTeacherVideoLink=new ArrayList<TeacherVideoLink>();
			lstTeacherVideoLink=teacherVideoLinksDAO.findTeachByVideoLink(teacherDetail);
			if(lstTeacherVideoLink==null || lstTeacherVideoLink.size()==0)
			{
				bDspqJobWiseStatus=false;
				sReturnValue="Error";
				if(sGroupMsg.equals(""))
					sGroupMsg="10";
				else
					sGroupMsg=sGroupMsg+",10";
			}
		}
		
		
		if(bSection11AdditionalDocuments)
		{
			List<TeacherAdditionalDocuments> lstTeacherAdditionalDocuments=new ArrayList<TeacherAdditionalDocuments>();
			lstTeacherAdditionalDocuments=teacherAdditionalDocumentsDAO.findTeacherAdditionalDoc(teacherDetail);
			if(lstTeacherAdditionalDocuments==null || lstTeacherAdditionalDocuments.size()==0)
			{
				bDspqJobWiseStatus=false;
				sReturnValue="Error";
				if(sGroupMsg.equals(""))
					sGroupMsg="11";
				else
					sGroupMsg=sGroupMsg+",11";
			}
		}
		
		
		JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		
		TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
		if(teacherExperience!=null)
		{
			if(Substitute_Teacher!=null && Substitute_Teacher>0)
				teacherExperience.setCanServeAsSubTeacher(Substitute_Teacher);
			
			if(isNonTeacher!=null && isNonTeacher==1)
			{
				teacherExperience.setIsNonTeacher(true);
				if(expCertTeacherTraining!=null && expCertTeacherTraining > 0)
					teacherExperience.setExpCertTeacherTraining(expCertTeacherTraining);
				else
					teacherExperience.setExpCertTeacherTraining(0.0);
			}
			else
			{
				teacherExperience.setIsNonTeacher(false);
			}
			
			if(nationalBoardCert!=null && nationalBoardCert==1)
			{
				teacherExperience.setNationalBoardCert(true);
				if(nationalBoardCertYear!=null && nationalBoardCertYear>0)
					teacherExperience.setNationalBoardCertYear(nationalBoardCertYear);
			}
			else if(nationalBoardCert!=null && nationalBoardCert==0)
			{
				teacherExperience.setNationalBoardCert(false);
			}
			
			teacherExperience.setCanServeAsSubTeacher(Substitute_Teacher);
			
			if(tfaAffiliate!=null)
			{
				TFAAffiliateMaster affiliateMaster=new TFAAffiliateMaster();
				affiliateMaster.setTfaAffiliateId(tfaAffiliate);
				teacherExperience.setTfaAffiliateMaster(affiliateMaster);
			}
			
			if(corpsYear!=null && corpsYear>0)
			{
				teacherExperience.setCorpsYear(corpsYear);
			}
			
			if(tfaRegion!=null && tfaRegion>0)
			{
				TFARegionMaster regionMaster=new TFARegionMaster();
				regionMaster.setTfaRegionId(tfaRegion);
				teacherExperience.setTfaRegionMaster(regionMaster);
			}
			teacherExperience.setPotentialQuality(0);	
			
		}
		else
		{
			teacherExperience=new TeacherExperience();
			teacherExperience.setTeacherId(teacherDetail);
			teacherExperience.setCanServeAsSubTeacher(2);
			teacherExperience.setCreatedDateTime(new Date());
			teacherExperience.setPotentialQuality(0);
			if(nationalBoardCert!=null && nationalBoardCert==1)
			{
				teacherExperience.setNationalBoardCert(true);
				if(nationalBoardCertYear!=null && nationalBoardCertYear>0)
					teacherExperience.setNationalBoardCertYear(nationalBoardCertYear);
			}
			else if(nationalBoardCert!=null && nationalBoardCert==0)
			{
				teacherExperience.setNationalBoardCert(false);
			}
			
		}
		teacherExperienceDAO.makePersistent(teacherExperience);
		if(iSection09NumberRequired>0)
		{
			int iSection09NumberRequired_Ref=teacherElectronicReferencesDAO.findTotalReference(teacherDetail);
			if(iSection09NumberRequired_Ref < iSection09NumberRequired)
			{
				bDspqJobWiseStatus=false;
				sReturnValue="Error";
				if(sGroupMsg.equals(""))
					sGroupMsg="9";
				else
					sGroupMsg=sGroupMsg+",9";
				
			}
		}
		
		DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
		if(bDspqJobWiseStatus)
		{
			if(dspqJobWiseStatus==null)
			{
				dspqJobWiseStatus=new DspqJobWiseStatus();
				dspqJobWiseStatus.setTeacherDetail(teacherDetail);
				dspqJobWiseStatus.setJobOrder(jobOrder);
				dspqJobWiseStatus.setiPAddress(IPAddressUtility.getIpAddress(request));
				dspqJobWiseStatus.setCreatedDataTime(new Date());
			}
			dspqJobWiseStatus.setCredentials(true);
			
			try 
			{
				dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
			} 
			catch (Exception e) 
			{
				sReturnValue="Error";
			}
		}
		
		int iIsAffilated=0;
		String sDecIsAffilated=validateCTAndDec(dspqct);
		if(sDecIsAffilated!=null && (sDecIsAffilated.equalsIgnoreCase("I") || sDecIsAffilated.equalsIgnoreCase("T")))
			iIsAffilated=1;
		updateJFT(dspqJobWiseStatus,teacherDetail, jobOrder, iIsAffilated);
		
		TeacherPortfolioStatus portfolioStatus=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
		if(portfolioStatus!=null)
		{
			portfolioStatus.setIsCertificationsCompleted(true);
		}
		else
		{
			portfolioStatus=new TeacherPortfolioStatus();
			portfolioStatus.setTeacherId(teacherDetail);
			portfolioStatus.setIsCertificationsCompleted(true);
			
			portfolioStatus.setIsPersonalInfoCompleted(false);
			portfolioStatus.setIsAcademicsCompleted(false);
			portfolioStatus.setIsExperiencesCompleted(false);
			portfolioStatus.setIsAffidavitCompleted(false);
		}

		try {
			teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return sReturnValue+"||"+sGroupMsg;
	}
	
	public PraxisMaster getPraxis(StateMaster stateMaster)
	{
		return pfCertifications.getPraxis(stateMaster);
	}
	
	@Transactional(readOnly=false)
	public int saveOrUpdateCert(TeacherCertificate teacherCertificate,String sUploadedFileName)
	{
		printdata("Calling DSPQServiceAjax => saveOrUpdateCert");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		try 
		{	
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			if(teacherCertificate!=null)
			{
				teacherCertificate.setCreatedDateTime(new Date());
				teacherCertificate.setTeacherDetail(teacherDetail);
				if(sUploadedFileName!=null && !sUploadedFileName.equals("") && !sUploadedFileName.equals("0"))
				{
					teacherCertificate.setPathOfCertification(sUploadedFileName);
				}
				else
				{
					printdata("is null");
				}
				
				teacherCertificateDAO.makePersistent(teacherCertificate);
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return 2;
	}
	
	public boolean deleteRecordCert(String id)
	{
		return pfCertifications.deleteRecord(id);
	}
	
	public TeacherCertificate showEditFormCert(String id)
	{
		return pfCertifications.showEditForm(id);
	}
	
	public String downloadCertification(Integer referenceId)
	{
		return pfCertifications.downloadCertification(referenceId);
	}
	
	public String saveOrUpdateElectronicReferences(TeacherElectronicReferences teacherElectronicReferences)
	{
		printdata("Calling DSPQServiceAjax => saveOrUpdateElectronicReferences");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		int iStatus=1;
		String sFileName=null;
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		TeacherElectronicReferences dbTER=null;
		try 
		{
			if(teacherElectronicReferences.getElerefAutoId()!=null && teacherElectronicReferences.getElerefAutoId() >0)
			{
				dbTER = teacherElectronicReferencesDAO.findById(teacherElectronicReferences.getElerefAutoId(), false, false);
				if(dbTER.getStatus()!=null)
					iStatus=dbTER.getStatus();
				if(dbTER.getPathOfReference()!=null)
					sFileName=dbTER.getPathOfReference();
			}
			teacherElectronicReferences.setStatus(iStatus);
			teacherElectronicReferences.setTeacherDetail(teacherDetail);
			
			if(sFileName!=null && (teacherElectronicReferences.getPathOfReference()==null || teacherElectronicReferences.getPathOfReference().equals("")))
				teacherElectronicReferences.setPathOfReference(sFileName);
			
			teacherElectronicReferencesDAO.makePersistent(teacherElectronicReferences);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "OK";
	}
	
	public String downloadReference(Integer referenceId)
	{
		return pfCertifications.downloadReference(referenceId);
	}
	
	public TeacherElectronicReferences editElectronicReferences(String id)
	{
		return pfCertifications.editElectronicReferences(id);
	}
	
	public Boolean changeStatusElectronicReferences(String id,Integer status)
	{
		return pfCertifications.changeStatusElectronicReferences(id, status);
	}
	
	public Boolean removeReferences(Integer referenceId)
	{
		return pfCertifications.removeReferences(referenceId);
	}
	
	public String saveOrUpdateVideoLinks(TeacherVideoLink teacherVideoLink,Integer videolinkAutoId)
	{
		printdata("Calling DSPQServiceAjax => saveOrUpdateVideoLinks");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		teacherVideoLink.setTeacherDetail(teacherDetail);
		
		try
		{
			String sFileName="";
			TeacherVideoLink tvlTemp=null;
			if(videolinkAutoId!=null)
			{
				tvlTemp=teacherVideoLinksDAO.findById(teacherVideoLink.getVideolinkAutoId(), false, false);
				if(tvlTemp!=null && tvlTemp.getVideo()!=null && !tvlTemp.getVideo().equals(""))
					sFileName=tvlTemp.getVideo();
			}
			
			if(sFileName!=null && (teacherVideoLink.getVideo()==null || teacherVideoLink.getVideo().equals("")))
				teacherVideoLink.setVideo(sFileName);
			
			if(teacherVideoLink.getCreatedDate()==null)
				teacherVideoLink.setCreatedDate(new Date());
			
			if(teacherVideoLink!=null && ( teacherVideoLink.getVideo()==null || teacherVideoLink.getVideo().equals("") ) || ( teacherVideoLink.getVideourl()==null || teacherVideoLink.getVideourl().equals("") ))
			{
				
			}
			else
			{
				if(teacherVideoLink.getVideo().equals("0"))
					teacherVideoLink.setVideo("");
				
				teacherVideoLinksDAO.makePersistent(teacherVideoLink);
			}
					
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "";
		
	}
	
	public String getVideoPlayDiv(Integer videoLinkId)
	{
		return pfCertifications.getVideoPlayDiv(videoLinkId);
	}
	
	public Boolean deleteVideoLink(String id)
	{
		return pfCertifications.deleteVIdeoLink(id);
	}
	
	public TeacherVideoLink editVideoLink(String id)
	{
		return pfCertifications.editVideoLink(id);
	}
	
	public String saveOrUpdateAdditionalDocuments(Integer additionDocumentId, String documentName, String uploadedDocument)
	{
		printdata("Calling DSPQServiceAjax => saveOrUpdateAdditionalDocuments ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			
			TeacherAdditionalDocuments teacherAdditionalDocuments = null;
			if(additionDocumentId==null)
			{
				teacherAdditionalDocuments = new TeacherAdditionalDocuments();
				teacherAdditionalDocuments.setCreatedDateTime(new Date());
				teacherAdditionalDocuments.setTeacherDetail(teacherDetail);
			}
			else
			{
				teacherAdditionalDocuments = teacherAdditionalDocumentsDAO.findById(additionDocumentId, false, false);
			}
			
			
			if(documentName!=null)
				teacherAdditionalDocuments.setDocumentName(documentName);
			
			if(uploadedDocument!=null && !uploadedDocument.equals(""))
				teacherAdditionalDocuments.setUploadedDocument(uploadedDocument);
			
			if((documentName==null || documentName.equals("")) && (uploadedDocument==null || uploadedDocument.equals("")))
			{
				
			}
			else
			{
				teacherAdditionalDocumentsDAO.makePersistent(teacherAdditionalDocuments);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "OK";
	}
	
	public boolean removeUploadedDocument(String additionDocumentId)
	{
		printdata("Calling DSPQServiceAjax => removeUploadedDocument ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		boolean bReturnValue=false;
		try 
		{	
			//TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			int iadditionDocumentId=Utility.getIntValue(additionDocumentId);
			TeacherAdditionalDocuments	teacherAdditionalDocuments = teacherAdditionalDocumentsDAO.findById(iadditionDocumentId, false,false);
			if(teacherAdditionalDocuments!=null)
			{
				teacherAdditionalDocumentsDAO.makeTransient(teacherAdditionalDocuments);
				bReturnValue=true;
			}

		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return bReturnValue;
	
	}
	
	public TeacherAdditionalDocuments showEditTeacherAdditionalDocuments(String additionDocumentId)
	{
		return pfCertifications.showEditTeacherAdditionalDocuments(additionDocumentId);
	}
	
	public String downloadUploadedDocument(Integer additionDocumentId)
	{
		return pfCertifications.downloadUploadedDocument(additionDocumentId);
	}
	
	
	public String UpdateResume(String UploadedFileName)
	{
		printdata("Calling DSPQServiceAjax => UpdateResume ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		String sReturnValue="OK";
		try 
		{	
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			printdata("UploadedFileName "+UploadedFileName);
			if(teacherExperience!=null && UploadedFileName!=null && !UploadedFileName.equals(""))
			{
				teacherExperience.setResume(UploadedFileName);
				teacherExperienceDAO.makePersistent(teacherExperience);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sReturnValue;
	
	}
	
	public String downloadResume()
	{
		return pFExperiences.downloadResume();
	}
	
	public String saveOrUpdateEmployment(TeacherRole teacherRole)
	{
		printdata("Calling DSPQServiceAjax => saveOrUpdateEmployment ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			teacherRole.setTeacherDetail(teacherDetail);
			teacherRole.setCreatedDateTime(new Date());
			CurrencyMaster currencyMaster = currencyMasterDAO.findById(1, false, false);
			if(teacherRole.getAmount()!=null)
			{
				if(teacherRole.getAmount()!=0)
				{
					teacherRole.setCurrencyMaster(currencyMaster);
				}
				else
				{
					teacherRole.setCurrencyMaster(null);
					teacherRole.setAmount(null);
				}
			}
			else
			{
				teacherRole.setCurrencyMaster(null);
				teacherRole.setAmount(null);
			}
			teacherRoleDAO.makePersistent(teacherRole);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public Boolean deleteEmployment(String id)
	{
		return pFExperiences.deleteEmployment(id);
	}
	
	public TeacherRole showEditFormEmployment(String id)
	{
		return pFExperiences.showEditFormEmployment(id);
	}
	
	public TeacherInvolvement showEditFormInvolvement(String id)
	{
		return pFExperiences.showEditFormInvolvement(id);
	}
	
	public Boolean deleteRecordInvolvment(String id)
	{
		return pFExperiences.deleteRecordInvolvment(id);
	}
	
	public String saveOrUpdateInvolvement(TeacherInvolvement teacherInvolvement)
	{
		printdata("Calling DSPQServiceAjax => saveOrUpdateInvolvement ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		try 
		{						
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			if(teacherInvolvement.getPeopleRangeMaster()==null)
				teacherInvolvement.setPeopleRangeMaster(null);

			teacherInvolvement.setTeacherDetail(teacherDetail);			
			teacherInvolvementDAO.makePersistent(teacherInvolvement);

		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
		}
		return null;
	}
	
	public TeacherHonor showEditHonors(String id)
	{
		return pFExperiences.showEditHonors(id);
	}
	
	public Boolean deleteRecordHonors(String id)
	{
		return pFExperiences.deleteRecordHonors(id);
	}
	
	public String saveOrUpdateHonors(TeacherHonor teacherHonor )
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			teacherHonor.setTeacherDetail(teacherDetail);
			if((teacherHonor.getHonor()==null || teacherHonor.getHonor().equals("")) && (teacherHonor.getHonorYear()==null || teacherHonor.getHonorYear()==0) )
			{
				
			}
			else
			{
				teacherHonorDAO.makePersistent(teacherHonor);
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;

	}
	
	
	@Transactional(readOnly=false)
	public String updateProfessionalGroup(Integer dspqPortfolioNameId,String dspqct,Integer jobId,Integer isretired,Integer retireddistrictId,Long stMForretire,String retireNo,Integer employeeType,Integer IsCurrentFullTimeTeacher,String employeeNumber,String retirementDate,String moneywithdrawaldate,String noLongerEmployed)
	{
		printdata("Calling DSPQServiceAjax => updateProfessionalGroup");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		String sReturnValue="OK";
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		TeacherPersonalInfo teacherpersonalinfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
		if(teacherpersonalinfo!=null)
		{
			if(isretired!=null)
			{
				if(isretired==1)
				{
					if(retireddistrictId!=null && retireddistrictId >0)
					{
						DistrictMaster districtMaster=new DistrictMaster();
						districtMaster.setDistrictId(retireddistrictId);
						teacherpersonalinfo.setDistrictmaster(districtMaster);
					}
					if(stMForretire!=null && stMForretire >0)
					{
						StateMaster stateMaster=new StateMaster();
						stateMaster.setStateId(stMForretire);
						teacherpersonalinfo.setStateMaster(stateMaster);
					}
					if(retireNo!=null && !retireNo.trim().equals(""))
					{
						teacherpersonalinfo.setRetirementnumber(retireNo);
					}
				}
				else if(isretired==0)
				{
					teacherpersonalinfo.setDistrictmaster(null);
					teacherpersonalinfo.setStateMaster(null);
					teacherpersonalinfo.setRetirementnumber(null);
				}
			}
			
			if(employeeType!=null)
				teacherpersonalinfo.setEmployeeType(employeeType);
			
			if(employeeNumber!=null && !employeeNumber.equals(""))
				teacherpersonalinfo.setEmployeeNumber(employeeNumber);
			
			if(IsCurrentFullTimeTeacher!=null)
				teacherpersonalinfo.setIsCurrentFullTimeTeacher(IsCurrentFullTimeTeacher);
			
			Date retirementDate_converted=null;
			if(retirementDate!=null && !retirementDate.equals(""))
			{
				try {
					retirementDate_converted=Utility.getCurrentDateFormart(retirementDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				teacherpersonalinfo.setRetirementdate(retirementDate_converted);
			}
			
			Date moneyWithdrawalDate_converted=null;
			if(moneywithdrawaldate!=null && !moneywithdrawaldate.equals(""))
			{
				try {
					moneyWithdrawalDate_converted=Utility.getCurrentDateFormart(moneywithdrawaldate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				teacherpersonalinfo.setMoneywithdrawaldate(moneyWithdrawalDate_converted);
			}
			
			if(noLongerEmployed!=null && !noLongerEmployed.equals(""))
			{
				teacherpersonalinfo.setNoLongerEmployed(noLongerEmployed);
			}
			
			
			try { teacherPersonalInfoDAO.makePersistent(teacherpersonalinfo); } catch (Exception e) {}
			
		}
		
		JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		
		String sGroupMsg="";
		boolean bDspqJobWiseStatus=true;
		boolean bSection13EmploymentHistory=false;
		boolean bSection14Involvement=false;
		boolean bSection15Honors=false;
		
		String sCandidateTypeDec=validateCTAndDec(dspqct);
		DspqPortfolioName dspqPortfolioName=dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		if(dspqPortfolioName!=null)
		{
			List<DspqSectionMaster> lstDspqSectionMasters=new ArrayList<DspqSectionMaster>();
			DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
			
			dspqSectionMaster.setSectionId(13);
			lstDspqSectionMasters.add(dspqSectionMaster);
			
			dspqSectionMaster=new DspqSectionMaster();
			dspqSectionMaster.setSectionId(14);
			lstDspqSectionMasters.add(dspqSectionMaster);
			
			dspqSectionMaster=new DspqSectionMaster();
			dspqSectionMaster.setSectionId(15);
			lstDspqSectionMasters.add(dspqSectionMaster);
			
			if(lstDspqSectionMasters.size() >0)
			{
				List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCTAndLstSec(dspqPortfolioName, sCandidateTypeDec, lstDspqSectionMasters);
				if(dspqRouterList!=null && dspqRouterList.size()>0)
				{
					for(DspqRouter dspqRouter :dspqRouterList)
					{
						if(dspqRouter.getDspqSectionMaster()!=null && dspqRouter.getDspqSectionMaster().getSectionId()==13 && dspqRouter.getIsRequired()!=null && dspqRouter.getStatus()!=null && dspqRouter.getStatus().equalsIgnoreCase("A"))
							bSection13EmploymentHistory=dspqRouter.getIsRequired();
						else if(dspqRouter.getDspqSectionMaster()!=null && dspqRouter.getDspqSectionMaster().getSectionId()==14 && dspqRouter.getIsRequired()!=null && dspqRouter.getStatus()!=null && dspqRouter.getStatus().equalsIgnoreCase("A"))
							bSection14Involvement=dspqRouter.getIsRequired();
						else if(dspqRouter.getDspqSectionMaster()!=null && dspqRouter.getDspqSectionMaster().getSectionId()==15 && dspqRouter.getIsRequired()!=null && dspqRouter.getStatus()!=null && dspqRouter.getStatus().equalsIgnoreCase("A"))
							bSection15Honors=dspqRouter.getIsRequired();
						
					}
				}
			}
			
		}
		
		if(bSection13EmploymentHistory)
		{
			List<TeacherRole> lstTeacherRole=new ArrayList<TeacherRole>();
			lstTeacherRole=teacherRoleDAO.findTeacherRoleByTeacher(teacherDetail);
			if(lstTeacherRole==null || lstTeacherRole.size()==0)
			{
				bDspqJobWiseStatus=false;
				sReturnValue="Error";
				if(sGroupMsg.equals(""))
					sGroupMsg="13";
				else
					sGroupMsg=sGroupMsg+",13";
			}
		}
		
		if(bSection14Involvement)
		{
			List<TeacherInvolvement> lstTeacherInvolvement=new ArrayList<TeacherInvolvement>();
			lstTeacherInvolvement=teacherInvolvementDAO.findTeacherInvolvementByTeacher(teacherDetail);
			if(lstTeacherInvolvement==null || lstTeacherInvolvement.size()==0)
			{
				bDspqJobWiseStatus=false;
				sReturnValue="Error";
				if(sGroupMsg.equals(""))
					sGroupMsg="14";
				else
					sGroupMsg=sGroupMsg+",14";
			}
		}
		
		if(bSection15Honors)
		{
			List<TeacherHonor> lstTeacherHonor=new ArrayList<TeacherHonor>();
			lstTeacherHonor=teacherHonorDAO.findTeacherHonersByTeacher(teacherDetail);
			if(lstTeacherHonor==null || lstTeacherHonor.size()==0)
			{
				bDspqJobWiseStatus=false;
				sReturnValue="Error";
				if(sGroupMsg.equals(""))
					sGroupMsg="15";
				else
					sGroupMsg=sGroupMsg+",15";
			}
		}
		
		DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
		if(bDspqJobWiseStatus)
		{
			if(dspqJobWiseStatus==null)
			{
				dspqJobWiseStatus=new DspqJobWiseStatus();
				dspqJobWiseStatus.setTeacherDetail(teacherDetail);
				dspqJobWiseStatus.setJobOrder(jobOrder);
				dspqJobWiseStatus.setiPAddress(IPAddressUtility.getIpAddress(request));
				dspqJobWiseStatus.setCreatedDataTime(new Date());
			}
			dspqJobWiseStatus.setProfessional(true);
			try 
			{
				dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
			} 
			catch (Exception e) 
			{
				sReturnValue="Error";
			}
		}
		
		
		int iIsAffilated=0;
		String sDecIsAffilated=validateCTAndDec(dspqct);
		if(sDecIsAffilated!=null && (sDecIsAffilated.equalsIgnoreCase("I") || sDecIsAffilated.equalsIgnoreCase("T")))
			iIsAffilated=1;
		updateJFT(dspqJobWiseStatus,teacherDetail, jobOrder, iIsAffilated);
		
		TeacherPortfolioStatus portfolioStatus=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
		if(portfolioStatus!=null)
		{
			portfolioStatus.setIsExperiencesCompleted(true);
		}
		else
		{
			portfolioStatus=new TeacherPortfolioStatus();
			portfolioStatus.setTeacherId(teacherDetail);
			portfolioStatus.setIsExperiencesCompleted(true);
			
			portfolioStatus.setIsPersonalInfoCompleted(false);
			portfolioStatus.setIsAcademicsCompleted(false);
			portfolioStatus.setIsCertificationsCompleted(false);
			portfolioStatus.setIsAffidavitCompleted(false);
		}

		try {
			teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return sReturnValue+"||"+sGroupMsg;
	}
	
	
	@Transactional(readOnly=false)
	public String updateGroup04Section25(Integer dspqPortfolioNameId,String dspqct,Integer jobId,Integer affidavit)
	{
		printdata("Calling DSPQServiceAjax => updateGroup04Section25");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		String sReturnValue="OK";
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		//TeacherPersonalInfo teacherpersonalinfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
		JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		
		TeacherPortfolioStatus portfolioStatus=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
		if(portfolioStatus!=null)
		{
			if(affidavit!=null && affidavit==1)
				portfolioStatus.setIsAffidavitCompleted(true);
			else
				portfolioStatus.setIsAffidavitCompleted(false);
		}
		else
		{
			portfolioStatus=new TeacherPortfolioStatus();
			portfolioStatus.setTeacherId(teacherDetail);
			portfolioStatus.setIsAffidavitCompleted(true);
			
			portfolioStatus.setIsPersonalInfoCompleted(false);
			portfolioStatus.setIsAcademicsCompleted(false);
			portfolioStatus.setIsCertificationsCompleted(false);
			portfolioStatus.setIsExperiencesCompleted(false);
		}
		
		try {
			teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		String sGroupMsg="";
		boolean bDspqJobWiseStatus=true;
			
		DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
		if(bDspqJobWiseStatus)
		{
			if(dspqJobWiseStatus==null)
			{
				dspqJobWiseStatus=new DspqJobWiseStatus();
				dspqJobWiseStatus.setTeacherDetail(teacherDetail);
				dspqJobWiseStatus.setJobOrder(jobOrder);
				dspqJobWiseStatus.setiPAddress(IPAddressUtility.getIpAddress(request));
				dspqJobWiseStatus.setCreatedDataTime(new Date());
			}
			dspqJobWiseStatus.setAffidavite(true);
			try 
			{
				dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
			} 
			catch (Exception e) 
			{
				sReturnValue="Error";
			}
		}
		
		int iIsAffilated=0;
		String sDecIsAffilated=validateCTAndDec(dspqct);
		if(sDecIsAffilated!=null && (sDecIsAffilated.equalsIgnoreCase("I") || sDecIsAffilated.equalsIgnoreCase("T")))
			iIsAffilated=1;
		updateJFT(dspqJobWiseStatus,teacherDetail, jobOrder, iIsAffilated);
		
		return sReturnValue+"||"+sGroupMsg;
	}
	
	public List<DistrictMaster> getFieldOfDistrictList(String DistrictName)
	{
		return districtAjax.getFieldOfDistrictList(DistrictName);
	}
	
	
	@Transactional(readOnly=false)
	public String getGroup01_SectionName01(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{

		printdata("Calling DSPQServiceAjax => getGroup01_SectionName01");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		StringBuffer sb=new StringBuffer();
		String sCandidateTypeDec=validateCTAndDec(candidateType);
		
		DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
		dspqSectionMaster.setSectionId(1);
		List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
		sectionMasters.add(dspqSectionMaster);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCTAndLstSec(dspqPortfolioName, sCandidateTypeDec, sectionMasters);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMap(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		if(mapDspqRouterBySID!=null)
		{
			DspqRouter dspqRouterSection1=mapDspqRouterBySID.get(1);
			
			if(dspqRouterSection1!=null)
			{

				String sSectionName="tooltipSection01";
				sb.append("<div class='col-sm-12 col-md-12' style='padding-left:0px;'>			"+
				"				<div style='float: left' class='portfolio_Subheading'>"+getLabelOrToolTipTagOrRequiredIcon(sSectionName, dspqRouterSection1, listToolTips)+"</div>"+
				"				<div style='clear: both;'></div>"+
				"		    </div>");
				
				sb.append(getInstructionForSection(dspqRouterSection1));
				
			}
		}
		
		String sReturnToolTips="";
		for(String ttips:listToolTips)
			sReturnToolTips=sReturnToolTips+","+ttips;
		
		String sReturnCalDateds="";
		for(String sCalDated:listCalDated)
			sReturnCalDateds=sReturnCalDateds+","+sCalDated;
		
		return sb.toString()+"|||"+sReturnToolTips+"|||"+sReturnCalDateds;
	}
	
	
	@Transactional(readOnly=false)
	public String completedspqJobWiseStatus(int jobId)
	{
		printdata("Calling DSPQServiceAjax => completedspqJobWiseStatus");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		String sReturnValue="OK";
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
		if(dspqJobWiseStatus==null)
		{
			dspqJobWiseStatus=new DspqJobWiseStatus();
			dspqJobWiseStatus.setTeacherDetail(teacherDetail);
			dspqJobWiseStatus.setJobOrder(jobOrder);
			dspqJobWiseStatus.setiPAddress(IPAddressUtility.getIpAddress(request));
			dspqJobWiseStatus.setCreatedDataTime(new Date());
		}
		dspqJobWiseStatus.setPortfolioComplete(true);
		
		try 
		{
			dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
		} 
		catch (Exception e) 
		{
			sReturnValue="Error";
		}
		
		return sReturnValue;
	}
	
	
	@Transactional(readOnly=false)
	public String getPreScreenFooter(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getPreScreenFooter");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		StringBuffer sb=new StringBuffer();
		
		sb.append("<div class='row'>" +
				"<div class='col-sm-9 col-md-9'><div class='mt10'><button class='flatbtn' id='btnprev' type='button' style='width:16%' onclick='return getPreviousPage();'><i class='backicon'></i> "+Utility.getLocaleValuePropByKey("btnPrevDSPQ", locale)+"</button>&nbsp;&nbsp;<button class='flatbtn' style='background:#F89507;width:20%'  type='button' onclick='return validatePreScreen(1);'>"+Utility.getLocaleValuePropByKey("btnSaveExit", locale)+" <i class='fa fa-check-circle'></i></button>&nbsp;&nbsp;<button class='flatbtn' style='background: #da4f49;width:16%'  type='button' onclick='return cancelApplyJob();'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+" <i class='fa fa-times-circle'></i></button></div></div>" +
				"<div class='col-sm-3 col-md-3'><div class='mt10' style='text-align:right;'><button class='flatbtn' style='width:90%;' id='btnnext' type='button' onclick='return validatePreScreen(0);'>"+Utility.getLocaleValuePropByKey("btnSaveAndNextDSPQ", locale)+" <i class='icon'></i></button></div></div>" +
				"</div>");
		return sb.toString();
	}
	
	@Transactional(readOnly=false)
	public String getEPIFooter(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getPreScreenFooter");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		StringBuffer sb=new StringBuffer();
		
		sb.append("<div class='row rowGapEPIFooter'>" +
				"<div class='col-sm-9 col-md-9'><div class='mt10'><button class='flatbtn' id='btnprev' type='button' style='width:16%' onclick='return getPreviousPage();'><i class='backicon'></i> "+Utility.getLocaleValuePropByKey("btnPrevDSPQ", locale)+"</button>&nbsp;&nbsp;<button class='flatbtn' style='background:#F89507;width:20%'  type='button' onclick='return validateEPI(1);'>"+Utility.getLocaleValuePropByKey("btnExitDSPQ", locale)+" <i class='fa fa-check-circle'></i></button>&nbsp;&nbsp;<button class='flatbtn' style='background: #da4f49;width:16%'  type='button' onclick='return cancelApplyJob();'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+" <i class='fa fa-times-circle'></i></button></div></div>" +
				"<div class='col-sm-3 col-md-3'><div class='mt10' style='text-align:right;'><button class='flatbtn' style='width:60%;' id='btnnext' type='button' onclick='return validateEPI(0);'>"+Utility.getLocaleValuePropByKey("btnContinueDSPQ", locale)+" <i class='icon'></i></button></div></div>" +
				"</div>");
		return sb.toString();
	}
	
	@Transactional(readOnly=false)
	public String getJSIFooter(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getPreScreenFooter");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		StringBuffer sb=new StringBuffer();
		
		sb.append("<div class='row rowGapEPIFooter'>" +
				"<div class='col-sm-9 col-md-9'><div class='mt10'><button class='flatbtn' id='btnprev' type='button' style='width:16%' onclick='return getPreviousPage();'><i class='backicon'></i> "+Utility.getLocaleValuePropByKey("btnPrevDSPQ", locale)+"</button>&nbsp;&nbsp;<button class='flatbtn' style='background:#F89507;width:20%'  type='button' onclick='return validateJSI(1);'>"+Utility.getLocaleValuePropByKey("btnExitDSPQ", locale)+" <i class='fa fa-check-circle'></i></button>&nbsp;&nbsp;<button class='flatbtn' style='background: #da4f49;width:16%'  type='button' onclick='return cancelApplyJob();'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+" <i class='fa fa-times-circle'></i></button></div></div>" +
				"<div class='col-sm-3 col-md-3'><div class='mt10' style='text-align:right;'><button class='flatbtn' style='width:60%;' id='btnnext' type='button' onclick='return validateJSI(0);'>"+Utility.getLocaleValuePropByKey("btnContinueDSPQ", locale)+" <i class='icon'></i></button></div></div>" +
				"</div>");
		return sb.toString();
	}
	
	@Transactional(readOnly=false)
	public String getJSIFooterL2(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId)
	{
		printdata("Calling DSPQServiceAjax => getJSIFooterL2");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		String locale = Utility.getValueOfPropByKey("locale");
		
		StringBuffer sb=new StringBuffer();
		
		sb.append("<div class='row'>"+
				"<div class='col-sm-9 col-md-9'>" +
					"<div class='mt10'><button class='flatbtn' id='btnprev' type='button' style='width:16%' onclick='return getPreviousPage();'><i class='backicon'></i> "+Utility.getLocaleValuePropByKey("btnPrevDSPQ", locale)+"</button>" +
						"&nbsp;&nbsp;<button class='flatbtn' style='background: #da4f49;width:16%'  type='button' onclick='return cancelApplyJob();'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+" <i class='fa fa-times-circle'></i></button>" +
					"</div>" +
				"</div>" +
				"<div class='col-sm-3 col-md-3'>"+
					"<div class='mt10' style='text-align:right;'>"+
						"<button class='flatbtn' style='width:100px;' id='btnnext' type='button' onclick='return startJSI();'>"+Utility.getLocaleValuePropByKey("btnContinueDSPQ", locale)+" <i class='icon'></i></button>"+
					"</div>"+
				"</div>"+
			"</div>");
		
		return sb.toString();
	}
	
	public String getCandidateType(boolean isInternalTransferCandidate,boolean isInternalCandidate)
	{
		String candidateType="E";
		if(isInternalTransferCandidate)
		{
			candidateType="T";
		}
		else if(isInternalCandidate)
		{
			candidateType="I";
		}
		return candidateType;
	}
	
	public boolean getPreSreen(JobCategoryMaster jobCategoryMaster,String sCandidateType)
	{
		boolean isQQRequired=false;
		if(sCandidateType!=null && !sCandidateType.equals("") && jobCategoryMaster!=null)
		{
			if(sCandidateType.equalsIgnoreCase("E") && jobCategoryMaster.getQuestionSets()!=null)
				isQQRequired=true;
			else if(sCandidateType.equalsIgnoreCase("I") && jobCategoryMaster.getOfferQualificationItems()!=null && jobCategoryMaster.getOfferQualificationItems())
				isQQRequired=true;
			else if(sCandidateType.equalsIgnoreCase("T") && jobCategoryMaster.getQualificationItemsForIMCandidates()!=null && jobCategoryMaster.getQualificationItemsForIMCandidates())
				isQQRequired=true;
		}
		return isQQRequired;
	}
	
	
	public boolean getDSPQPortfolioRequired(JobCategoryMaster jobCategoryMaster,String sCandidateType)
	{
		boolean isDSPQRequired=false;
		if(sCandidateType!=null && !sCandidateType.equals("") && sCandidateType.equalsIgnoreCase("E"))
			isDSPQRequired=true;
		else if(sCandidateType!=null && !sCandidateType.equals("") && sCandidateType.equalsIgnoreCase("I") && jobCategoryMaster!=null && jobCategoryMaster.getOfferDistrictSpecificItems()!=null && jobCategoryMaster.getOfferDistrictSpecificItems())
				isDSPQRequired=true;
		else if(sCandidateType!=null && !sCandidateType.equals("") && sCandidateType.equalsIgnoreCase("T") && jobCategoryMaster!=null && jobCategoryMaster.getDistrictSpecificItemsForIMCandidates()!=null && jobCategoryMaster.getDistrictSpecificItemsForIMCandidates())
				isDSPQRequired=true;
		
		return isDSPQRequired;
	}
	
	public Map<String,Boolean> getPortfolioG1G4(Map<Integer, Boolean> mapGroup,Map<Integer, Boolean> mapSection,List<DspqRouter> dspqRouterList)
	{
		Map<String,Boolean> mapPortfolioG1G4=new HashMap<String, Boolean>();
		
		for(DspqRouter dspqRouter :dspqRouterList)
		{
			mapGroup.put(dspqRouter.getDspqSectionMaster().getDspqGroupMaster().getGroupId(), true);
			mapSection.put(dspqRouter.getDspqSectionMaster().getSectionId(), true);
			
			if(dspqRouter.getDspqSectionMaster().getDspqGroupMaster().getGroupId()==1 && dspqRouter.getDspqSectionMaster().getSectionId()==1)
				mapPortfolioG1G4.put(JobApplicationFlowConfig.CoverLetterGroup01, true);
			else if(dspqRouter.getDspqSectionMaster().getDspqGroupMaster().getGroupId()==1 && dspqRouter.getDspqSectionMaster().getSectionId()!=1)
				mapPortfolioG1G4.put(JobApplicationFlowConfig.PersonalInformationGroup01, true);
			
			if(dspqRouter.getDspqSectionMaster().getDspqGroupMaster().getGroupId()==4 && dspqRouter.getDspqSectionMaster().getSectionId()==25)
				mapPortfolioG1G4.put(JobApplicationFlowConfig.AffidavitGroup04, true);
			else if(dspqRouter.getDspqSectionMaster().getDspqGroupMaster().getGroupId()==4 && dspqRouter.getDspqSectionMaster().getSectionId()!=25)
				mapPortfolioG1G4.put(JobApplicationFlowConfig.ProfessionalGroup04, true);
			
		}
		return mapPortfolioG1G4;
	}
	
	public boolean getPortfolioG1G4Required(Map<String,Boolean> mapPortfolioG1G4,String sInputName)
	{
		boolean bReturnValue=false;
		if(mapPortfolioG1G4!=null && mapPortfolioG1G4.size() >0 && sInputName!=null && !sInputName.equals(""))
		{
			if(mapPortfolioG1G4.get(sInputName)!=null)
				bReturnValue=true;
		}
		return bReturnValue;
	}
	
	public String getControllerReturnURL(Map<Integer, Boolean> mapGroup,String iJobId,String sEnc_candidateType,boolean isDSPQRequired,boolean isCoverLetterG1,boolean isPersonalInformationG1,boolean isAffiliateG4,boolean isProfessionalG4,boolean isQQRequired)
	{
		String sReturnURL="";
		if(isDSPQRequired)
		{
			if(isCoverLetterG1)
			{
				sReturnURL="managejobapplicationflow.do?jobId="+iJobId+"&ct="+sEnc_candidateType;
			}
			else if(isPersonalInformationG1)
			{
				sReturnURL="jobapplicationflowpersonal.do?jobId="+iJobId+"&ct="+sEnc_candidateType;
			}
			else if(mapGroup!=null && mapGroup.get(2)!=null)
			{
				sReturnURL="jobapplicationflowacademic.do?jobId="+iJobId+"&ct="+sEnc_candidateType;
			}
			else if(mapGroup!=null && mapGroup.get(3)!=null)
			{
				sReturnURL="jobapplicationflowcredentials.do?jobId="+iJobId+"&ct="+sEnc_candidateType;
			}
			else if(isAffiliateG4)
			{
				sReturnURL="jobapplicationflowAffidavit.do?jobId="+iJobId+"&ct="+sEnc_candidateType;
			}
			else if(isProfessionalG4)
			{
				sReturnURL="jobapplicationflowprofessional.do?jobId="+iJobId+"&ct="+sEnc_candidateType;
			}
			else
				sReturnURL="DSPQNC"; // DSPQNC : DSPQ is not configure
		}
		else if(isQQRequired)
		{
			sReturnURL="jobapplicationflowprescreen.do?jobId="+iJobId+"&ct="+sEnc_candidateType;
		}
		else
		{
			sReturnURL="jobapplicationflowcompleted.do?jobId="+iJobId+"&ct="+sEnc_candidateType;
		}
		return sReturnURL;
	}
	
	
	public void setSIDAndFIDInMap(Map<Integer, DspqRouter> mapDspqRouterByFID,Map<Integer, DspqRouter> mapDspqRouterBySID,List<DspqRouter> dspqRouterList)
	{
		if(dspqRouterList!=null && dspqRouterList.size()>0)
		{
			for(DspqRouter dspqRouter :dspqRouterList)
			{
				if(dspqRouter.getDspqFieldMaster()!=null)
					mapDspqRouterByFID.put(dspqRouter.getDspqFieldMaster().getDspqFieldId(), dspqRouter);
				else if(dspqRouter.getDspqSectionMaster()!=null)
					mapDspqRouterBySID.put(dspqRouter.getDspqSectionMaster().getSectionId(), dspqRouter);
			}
		}
	}
	
	public String getNextNavigation(Map<String,Integer> mapNextAndPreNavigater,Map<Integer, String> mapNextAndPreURL,String sInputShortCode)
	{
		String sNext="";
		//CL - Cover Letter, PI - Personal Information, AC - Academic, CR - Credentials, PR - Professional  AF , QQ
		if(mapNextAndPreNavigater!=null && mapNextAndPreNavigater.get(sInputShortCode)!=null)
		{
			int iCurrentPosition=mapNextAndPreNavigater.get(sInputShortCode);
			if(mapNextAndPreURL!=null && mapNextAndPreURL.get(iCurrentPosition+1)!=null)
				sNext=mapNextAndPreURL.get(iCurrentPosition+1);
			else
				sNext="";
		}
		
		return sNext;
	}
	
	public String getPrevNavigation(Map<String,Integer> mapNextAndPreNavigater,Map<Integer, String> mapNextAndPreURL,String sInputShortCode)
	{
		String sPrevious="";
		//CL - Cover Letter, PI - Personal Information, AC - Academic, CR - Credentials, PR - Professional  AF , QQ
		if(mapNextAndPreNavigater!=null && mapNextAndPreNavigater.get(sInputShortCode)!=null)
		{
			int iCurrentPosition=mapNextAndPreNavigater.get(sInputShortCode);
			if(mapNextAndPreURL!=null && mapNextAndPreURL.get(iCurrentPosition-1)!=null)
				sPrevious=mapNextAndPreURL.get(iCurrentPosition-1);
			else
				sPrevious="";
		}
		
		return sPrevious;
	}
	
	
	public boolean setPrevAndNextNavigation(Map<Integer, Boolean> mapGroup, Map<Integer, String> mapNextAndPreURL, Map<String,Integer> mapNextAndPreNavigater, String sJobId, String candidateTypeDec,boolean isDspqRequired,boolean isCoverLetterG1,boolean isPersonalInformationG1,boolean isProfessionalG4,boolean isAffiliateG4,boolean isQQRequired,boolean isEPI,boolean isJSI)
	{
		int iCounter=1;
		String sParameters="?jobId="+sJobId+"&ct="+candidateTypeDec;
		if(mapGroup!=null && isDspqRequired)
		{
			if(mapGroup.get(1)!=null && isCoverLetterG1)
			{
				iCounter++;
				mapNextAndPreURL.put(iCounter, "managejobapplicationflow.do"+sParameters);
				mapNextAndPreNavigater.put("CL", iCounter);
			}
			if(mapGroup.get(1)!=null && isPersonalInformationG1)
			{
				iCounter++;
				mapNextAndPreURL.put(iCounter, "jobapplicationflowpersonal.do"+sParameters);
				mapNextAndPreNavigater.put("PI", iCounter);
			}
			if(mapGroup.get(2)!=null)
			{
				iCounter++;
				mapNextAndPreURL.put(iCounter, "jobapplicationflowacademic.do"+sParameters);
				mapNextAndPreNavigater.put("AC", iCounter);
			}
			if(mapGroup.get(3)!=null)
			{
				iCounter++;
				mapNextAndPreURL.put(iCounter, "jobapplicationflowcredentials.do"+sParameters);
				mapNextAndPreNavigater.put("CR", iCounter);
			}
			if(mapGroup.get(4)!=null && isProfessionalG4)
			{
				iCounter++;
				mapNextAndPreURL.put(iCounter, "jobapplicationflowprofessional.do"+sParameters);
				mapNextAndPreNavigater.put("PR", iCounter);
			}
			
			if(mapGroup.get(4)!=null && isAffiliateG4)
			{
				iCounter++;
				mapNextAndPreURL.put(iCounter, "jobapplicationflowAffidavit.do"+sParameters);
				mapNextAndPreNavigater.put("AF", iCounter);
			}
		}
		
		if(isQQRequired)
		{
			iCounter++;
			mapNextAndPreURL.put(iCounter, "jobapplicationflowprescreen.do"+sParameters);
			mapNextAndPreNavigater.put("QQ", iCounter);
		}
		
		if(isEPI)
		{
			iCounter++;
			mapNextAndPreURL.put(iCounter, "jobapplicationflowepi.do"+sParameters);
			mapNextAndPreNavigater.put("EPI", iCounter);
		}
		
		if(isJSI)
		{
			iCounter++;
			mapNextAndPreURL.put(iCounter, "jobapplicationflowcustomquestion.do"+sParameters);
			mapNextAndPreNavigater.put("JSI", iCounter);
		}
		
		boolean isCompleted=true;
		if(isCompleted)
		{
			iCounter++;
			mapNextAndPreURL.put(iCounter, "jobapplicationflowcompleted.do"+sParameters);
			mapNextAndPreNavigater.put("Comp", iCounter);
		}
		return true;
	}
	
	
	@Transactional(readOnly=false)
	public String getDistrictSpecificQuestion(Integer jobId,String isAffilated)
	{
		printdata("getDistrictSpecificQuestion");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		StringBuffer sb =new StringBuffer();
		try
		{
			List<DistrictSpecificQuestions> districtSpecificQuestionList	= new ArrayList<DistrictSpecificQuestions>(); 
			int totalQuestions =0;
			
			TeacherDetail teacherDetail = null;

			if (session == null || session.getAttribute("teacherDetail") == null) 
			{
				//return "false";
			}
			else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			if(jobId!=null && jobId>0)
			{
				JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
				JobCategoryMaster jobCategoryMaster = null;
				if(jobOrder!=null)
					jobCategoryMaster=jobOrder.getJobCategoryMaster();
				
				Integer iqqQuestionSets=null;
				QqQuestionSets qqQuestionSets = new QqQuestionSets();
				List<QqQuestionsetQuestions> qqQuestionsetQuestions=null;
				if(jobCategoryMaster!=null && jobCategoryMaster.getQuestionSets()!=null)
				{
					qqQuestionSets = jobCategoryMaster.getQuestionSets();
					qqQuestionsetQuestions = qqQuestionsetQuestionsDAO.findByQuestionSet(qqQuestionSets); 
					iqqQuestionSets=qqQuestionSets.getID();
				}
				
				List<Integer> tempDistSpecQues = new ArrayList<Integer>();
				if(qqQuestionsetQuestions!=null && qqQuestionsetQuestions.size()>0)
				{
					for (QqQuestionsetQuestions qqQuestionsetQuestions2 : qqQuestionsetQuestions) 
					{
						tempDistSpecQues.add(qqQuestionsetQuestions2.getDistrictSpecificQuestions().getQuestionId());
					}
				}
				
				if(tempDistSpecQues!=null && tempDistSpecQues.size()>0)
				{
					districtSpecificQuestionList=districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYQues(tempDistSpecQues);
					totalQuestions=districtSpecificQuestionList.size();
				}
				List<JobOrder> jobOrders = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findLastTeacherAnswersByDistrictAndJob(teacherDetail,jobOrder);

				List<TeacherAnswerDetailsForDistrictSpecificQuestions> lastList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
				if(jobOrders!=null && jobOrders.size()>0)
					lastList = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersBySetQues(teacherDetail,qqQuestionSets);

				Map<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions> mapAnswer = new HashMap<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions>();

				if(lastList!=null)
				for(TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion : lastList)
					mapAnswer.put(teacherAnswerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId(), teacherAnswerDetailsForDistrictSpecificQuestion);

				// change 
				TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion = null;
				List<OptionsForDistrictSpecificQuestions> questionOptionsList = null;
				String questionInstruction = null;
				String shortName = "";
				int iCounter=0;
				
				//boolean focus = false;
				sb.append("<input type='hidden' id='preScreenTotalQNo' preScreenSetId='"+iqqQuestionSets+"' value="+totalQuestions+">");
				if(totalQuestions>0)
				{	
					sb.append("<div class='row'><div class='col-sm-12 col-md-12'>&nbsp;</div></div>");
					for (DistrictSpecificQuestions districtSpecificQuestion : districtSpecificQuestionList) 
					{
						++iCounter;	
						shortName = districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeShortName();
						
						sb.append("<div class='row'>");
						
						if(iCounter==1)
							sb.append("<div class='col-sm-12 col-md-12'>");
						else
							sb.append("<div class='col-sm-12 col-md-12' style='margin-top: 15px;'>");
						
						sb.append("<span><b>Question "+iCounter+" of "+totalQuestions+"</b></span>");
						sb.append("</div>");
						
						questionInstruction = districtSpecificQuestion.getQuestionInstructions()==null?"":districtSpecificQuestion.getQuestionInstructions();
						if(questionInstruction!=null && !questionInstruction.equals(""))
						{
							sb.append("<div class='col-sm-12 col-md-12'>");
							sb.append("<span>"+Utility.getUTFToHTML(questionInstruction)+"</span>");
							sb.append("</div>");
						}
						
						if(districtSpecificQuestion.getQuestion()!=null && !districtSpecificQuestion.getQuestion().equals(""))
						{
							String sQuestionText=districtSpecificQuestion.getQuestion();
							sQuestionText=sQuestionText.replaceAll("<br>", "");
							//Utility.getUTFToHTML(districtSpecificQuestion.getQuestion())
							
							sb.append("<div class='col-sm-12 col-md-12'>");
							sb.append("<label><strong><span id='preScreenSNo_"+iCounter+"' preScreenQId='"+districtSpecificQuestion.getQuestionId()+"' preScreenShortCode='"+districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeShortName()+"' preScreenQRequired='1' preScreenQuestionTypeId='"+districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeId()+"'>"+sQuestionText+"</span><span id='lblpreScreenWarningBeforeSpace_"+iCounter+"'></span><span id='lblpreScreenWarning_"+iCounter+"'></span><span id='lblpreScreenWarningAfterSpace_"+iCounter+"'></span><span class='required'>*</span></strong></label>");
							sb.append("</div>");
						}
								
						
						boolean bValidAnswer=false;
						String checked = "";
						Integer optId = 0;
						String insertedText = "";
						teacherAnswerDetailsForDistrictSpecificQuestion = mapAnswer.get(districtSpecificQuestion.getQuestionId());
						if(teacherAnswerDetailsForDistrictSpecificQuestion!=null && shortName.equalsIgnoreCase(teacherAnswerDetailsForDistrictSpecificQuestion.getQuestionType()))
						{
							if(teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions()!=null)
								optId = Utility.getIntValue(teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions());
								
							insertedText = teacherAnswerDetailsForDistrictSpecificQuestion.getInsertedText();
						}
						
						questionOptionsList = districtSpecificQuestion.getQuestionOptions();
						String validAnswer="";
						sb.append("<div class='col-sm-12 col-md-12'>");
						if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel"))
						{
							for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList)
							{
								if(optId.equals(questionOptions.getOptionId()))
									checked = "checked";
								else
									checked = "";
								
								sb.append("<label class='radio' style='margin-top: 0px;margin-bottom: 2px;'>");
								sb.append("<input type='radio' "+checked+" name='preScreenOpt"+iCounter+"' value='"+questionOptions.getOptionId()+"' questionoption='"+questionOptions.getQuestionOption()+"' >"+questionOptions.getQuestionOption()+"</br>");
								sb.append("</label>");
								
								if(questionOptions!=null && questionOptions.getValidOption()!=null && questionOptions.getValidOption().equals(true)){
									if(validAnswer.equalsIgnoreCase(""))
										validAnswer=questionOptions.getOptionId().toString();
									else
										validAnswer=validAnswer+","+questionOptions.getOptionId().toString();
								}
							}
							sb.append("<input type='hidden' id='bValidOption_"+iCounter+"' value='"+validAnswer+"'>");
						}
						else if(shortName.equalsIgnoreCase("et"))
						{
							String hideShowDiv="hide";
							for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) 
							{
								if(optId.equals(questionOptions.getOptionId())){
									checked = "checked";									
								}
								else
									checked = "";
								String hideShow="";
								if(questionOptions.getRequiredExplanation().equals(true)){
									hideShow = "onclick='showHidePreScreenExplanationField("+iCounter+",1);'";
									if(checked=="checked")
										hideShowDiv="show";
								}
								else
									hideShow = "onclick='showHidePreScreenExplanationField("+iCounter+",0);'";
								 

								sb.append("<label class='radio' style='margin-top: 0px;margin-bottom: 2px;'>");
								sb.append("<input type='radio' "+checked+" name='preScreenOpt"+iCounter+"' "+hideShow+" value='"+questionOptions.getOptionId()+"' questionoption='"+questionOptions.getQuestionOption()+"' >"+questionOptions.getQuestionOption()+"</br>");
								sb.append("</label>");
								
								if(questionOptions!=null && questionOptions.getValidOption()!=null && questionOptions.getValidOption().equals(true)){
									if(validAnswer.equalsIgnoreCase(""))
										validAnswer=questionOptions.getOptionId().toString();
									else
										validAnswer=validAnswer+","+questionOptions.getOptionId().toString();
									
								}
							}
							sb.append("<input type='hidden' id='bValidOption_"+iCounter+"' value='"+validAnswer+"' >");
							
							sb.append("<div class='"+hideShowDiv+"' id='divOpt_"+iCounter+"'>");
							if(districtSpecificQuestion.getQuestionExplanation()!=null)
								sb.append("<div>"+Utility.getUTFToHTML(districtSpecificQuestion.getQuestionExplanation())+"</div>");

							sb.append("<textarea name='txtAreaOpt_"+iCounter+"' id='txtAreaOpt_"+iCounter+"' class='form-control' maxlength='5000'  rows='8' style='width:100%;' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+insertedText+"</textarea>");
							sb.append("</div>");
						}
						else if(shortName.equalsIgnoreCase("ml"))
						{
							sb.append("<div id='divOpt_"+iCounter+"'>");
							sb.append("<textarea name='txtAreaOpt_"+iCounter+"' id='txtAreaOpt_"+iCounter+"' class='form-control' maxlength='5000'  rows='8' style='width:100%;' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+insertedText+"</textarea>");
							sb.append("</div>");
						}
						else if(shortName.equalsIgnoreCase("mlsel"))
						{
							Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
							String sListValue="";
							if(teacherAnswerDetailsForDistrictSpecificQuestion!=null && teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedoptionmlsel()!=null && !teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedoptionmlsel().equals(""))
							{
								sListValue=teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedoptionmlsel();
								String arrOp[]=sListValue.split(",");
								if(arrOp.length >0)
								{
									for(String sOpt :arrOp)
									{
										int iOpt=Utility.getIntValue(sOpt);
										mapOption.put(iOpt, true);
									}
								}
							}
							
							for (OptionsForDistrictSpecificQuestions dqo : questionOptionsList) 
							{
								sb.append("<label class='checkbox inline' style='margin-top: 0px;margin-bottom: 2px;'>");
								if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
									sb.append("<input type='checkbox' checked name='preScreenOpt"+iCounter+"' value='"+dqo.getOptionId()+"' textValue='"+dqo.getQuestionOption()+"'>"+dqo.getQuestionOption()+"</br>");
								else
									sb.append("<input type='checkbox' name='preScreenOpt"+iCounter+"' value='"+dqo.getOptionId()+"' textValue='"+dqo.getQuestionOption()+"'>"+dqo.getQuestionOption()+"</br>");
								sb.append("</label>");
								
								if(dqo!=null && dqo.getValidOption()!=null && dqo.getValidOption().equals(true)){
									if(validAnswer.equalsIgnoreCase(""))
										validAnswer=dqo.getOptionId().toString();
									else
										validAnswer=validAnswer+","+dqo.getOptionId().toString();
									
								}
							}
							sb.append("<input type='hidden' id='bValidOption_"+iCounter+"' value='"+validAnswer+"' >");
						}

						sb.append("</div>");
						sb.append("</div>");
					}
			}
					
			}
	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	@Transactional(readOnly=false)
	public String savePreScreenAnswerBySet(String jobId,String dspqct,TeacherAnswerDetailsForDistrictSpecificQuestions[] answerDetailsForDistrictSpecificQuestions,QqQuestionSets qqQuestionSets)
	{
		printdata("Calling DSPQServiceAjax => savePreScreenAnswerBySet");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		TeacherDetail teacherDetail = null;

		if (session == null || session.getAttribute("teacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

		try {

			int inValidCount = 0;
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			JobOrder jobOrder = answerDetailsForDistrictSpecificQuestions[0].getJobOrder();
			jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);
			
			Map<Integer,Integer> map = new HashMap<Integer, Integer>();

			try {
				map = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersForQQByDistrictSet(teacherDetail,qqQuestionSets);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			List<Integer> answers = new ArrayList<Integer>();
			Integer ansId = null;
			
			for (TeacherAnswerDetailsForDistrictSpecificQuestions answerDetailsForDistrictSpecificQuestion : answerDetailsForDistrictSpecificQuestions) 
			{
				answerDetailsForDistrictSpecificQuestion.setTeacherDetail(teacherDetail);
				answerDetailsForDistrictSpecificQuestion.setDistrictMaster(jobOrder.getDistrictMaster());
				answerDetailsForDistrictSpecificQuestion.setCreatedDateTime(new Date());

				ansId = map.get(answerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId());
				
				if(ansId!=null)
					answerDetailsForDistrictSpecificQuestion.setAnswerId(ansId);
				
				if(!answerDetailsForDistrictSpecificQuestion.getQuestionType().equalsIgnoreCase("ml"))
				{
					if(answerDetailsForDistrictSpecificQuestion.getIsValidAnswer()==false)
					{
						inValidCount++;
						wrongAnswerDetailsForDistrictSpecificQuestionList.add(answerDetailsForDistrictSpecificQuestion);
					}
				}

				answerDetailsForDistrictSpecificQuestion.setIsActive(true);
			
				teacherAnswerDetailsForDistrictSpecificQuestionsDAO.getSession().merge(answerDetailsForDistrictSpecificQuestion);
				answers.add(answerDetailsForDistrictSpecificQuestion.getAnswerId());
			}

			if(answers.size()>0)
				teacherAnswerDetailsForDistrictSpecificQuestionsDAO.updateOtherQQ(teacherDetail,jobOrder.getDistrictMaster(),answers);
			
			//Start Ph-II
			
			Boolean QQFlag=false;
			
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnsListInFinalizedQQ = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			List<JobForTeacher> lstJobForTeacher1 = jobForTeacherDAO.findByTeacherAndQuestionSet(teacherDetail,jobOrder.getJobCategoryMaster().getQuestionSets());
            
            try
            {
	            if(lstJobForTeacher1.size()>0)
	            {
	            	if(lstJobForTeacher1.get(0).getIsDistrictSpecificNoteFinalize()!=null && lstJobForTeacher1.get(0).getIsDistrictSpecificNoteFinalize()==true)
	            	{
	            		List<TeacherAnswerDetailsForDistrictSpecificQuestions> list=new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();        				
	    				list=teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersByDistrictTeacherAndQuestionSet(teacherDetail,jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster().getQuestionSets());
	    				if(list.size()>0)
	    				{
	    					for(TeacherAnswerDetailsForDistrictSpecificQuestions tadfdsq : list)
	    					{
	    						if(tadfdsq.getIsValidAnswer()!=null && tadfdsq.getIsValidAnswer()==false)
	    						{
	    							wrongAnsListInFinalizedQQ.add(tadfdsq);
	    							QQFlag=true;
	    						}
	    					}
	    				}
	            	}
	            }
            }
            catch(Exception e)
            {
            	e.printStackTrace();
            }
 			
			List<JobForTeacher> lstJobForTeacher= jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail,jobOrder);
			JobForTeacher jobForTeacher = null;
			boolean validFlag = false;

			if(inValidCount>0)
				validFlag=false;
			else
				validFlag=true;

			if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
			{
				//session.setAttribute("jobValid"+jobOrder.getJobId(),validFlag);
				PrintOnConsole.debugPrintln("setDistrictQuestions ... Not save in JFT");
			}
			else
			{
				PrintOnConsole.debugPrintln("setDistrictQuestions ... already saved in JFT");

				jobForTeacher  = lstJobForTeacher.get(0);
				jobForTeacher.setFlagForDistrictSpecificQuestions(validFlag);
				////////////////////Checking status to be set///////////////////////
				StatusMaster statusMaster = null;
				boolean completeFlag=false;
				int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
				statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
				try{
					String jftStatus=jobForTeacher.getStatus().getStatusShortName();
					if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
						completeFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(jobForTeacher.getInternalSecondaryStatus()!=null){
					jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
					jobForTeacher.setStatusMaster(null);
					jobForTeacher.setSecondaryStatus(jobForTeacher.getInternalSecondaryStatus());
					if(jobForTeacher.getInternalStatus()!=null){
						jobForTeacher.setApplicationStatus(jobForTeacher.getInternalStatus().getStatusId());
					}
				}else if(jobForTeacher.getInternalStatus()!=null){
					jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
					jobForTeacher.setStatusMaster(jobForTeacher.getInternalStatus());
					if(jobForTeacher.getInternalStatus()!=null){
						jobForTeacher.setApplicationStatus(jobForTeacher.getInternalStatus().getStatusId());
					}
				}else{
					try{
						if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
							if(statusMaster!=null){
								jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
							}
							jobForTeacher.setStatus(statusMaster);
							if(jobForTeacher.getSecondaryStatus()!=null){
								jobForTeacher.setStatusMaster(null);
							}else{
								jobForTeacher.setStatusMaster(statusMaster);
							}
						}
					}catch(Exception e){}
				}

				PrintOnConsole.debugPrintln("try to set setIsAffilated flag in setDistrictQuestions");
				if (session != null && session.getAttribute("isCurrentEmploymentNeeded") != null) 
				{
					boolean isCurrentEmploymentNeeded=false;
					isCurrentEmploymentNeeded=(Boolean)session.getAttribute("isCurrentEmploymentNeeded");
					PrintOnConsole.debugPrintln("setDistrictQuestions isCurrentEmploymentNeeded "+isCurrentEmploymentNeeded);
					if(isCurrentEmploymentNeeded)
					{
						TeacherPersonalInfo teacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
						if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType()!=null && teacherPersonalInfo.getEmployeeType()==1)
							jobForTeacher.setIsAffilated(1);
						else
							jobForTeacher.setIsAffilated(0);
					}
					session.removeAttribute("isCurrentEmploymentNeeded");
				}

				jobForTeacherDAO.makePersistent(jobForTeacher);
				////////////////////////////////////
				try{
					if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
						commonService.futureJobStatus(jobOrder, teacherDetail,jobForTeacher);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
            //ravindra qq update
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnswerDetailsForDistrictSpecificQuestionList1 = null; 
			wrongAnswerDetailsForDistrictSpecificQuestionList1 = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();				
			if(wrongAnswerDetailsForDistrictSpecificQuestionList.size()>0 && wrongAnsListInFinalizedQQ.size()>0){
				wrongAnswerDetailsForDistrictSpecificQuestionList1.addAll(wrongAnswerDetailsForDistrictSpecificQuestionList);
				printdata("wrongAnswerDetailsForDistrictSpecificQuestionList1 after addAll-->"+wrongAnswerDetailsForDistrictSpecificQuestionList1.size());
				wrongAnswerDetailsForDistrictSpecificQuestionList1.removeAll(wrongAnsListInFinalizedQQ);
			}
			printdata("wrongAnswerDetailsForDistrictSpecificQuestionList1 after Remove-->"+wrongAnswerDetailsForDistrictSpecificQuestionList1.size());
			
			if(lstJobForTeacher1.size()>0)
			{			
					for(JobForTeacher jft:lstJobForTeacher1)
					{
						if(jft.getIsDistrictSpecificNoteFinalize()!=null && jft.getIsDistrictSpecificNoteFinalize()){

							if(validFlag==true)
							{
								jft.setFlagForDistrictSpecificQuestions(validFlag);
								jft.setIsDistrictSpecificNoteFinalize(false);
							}
							else
							{																

								if(QQFlag==true && wrongAnswerDetailsForDistrictSpecificQuestionList1.size()==0)
								{
									jft.setFlagForDistrictSpecificQuestions(true);	
								}
								else
								{
									jft.setFlagForDistrictSpecificQuestions(validFlag);
									jft.setIsDistrictSpecificNoteFinalize(validFlag);
								}

							}
						}
						else
						{
							jft.setFlagForDistrictSpecificQuestions(validFlag);								
						}
						jobForTeacherDAO.makePersistent(jft);
					}				
			}
			
			printdata("-----------------------------------Valid Flag:- "+validFlag+" and inValidCount:- "+inValidCount+"-------------------------------------------------------");
			if(!validFlag || inValidCount>0)
				sendMailOnNegativeQQ(jobOrder, wrongAnswerDetailsForDistrictSpecificQuestionList);
			
			
			DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
			if(dspqJobWiseStatus==null)
			{
				dspqJobWiseStatus=new DspqJobWiseStatus();
				dspqJobWiseStatus.setTeacherDetail(teacherDetail);
				dspqJobWiseStatus.setJobOrder(jobOrder);
				dspqJobWiseStatus.setiPAddress(IPAddressUtility.getIpAddress(request));
				dspqJobWiseStatus.setCreatedDataTime(new Date());
			}
			dspqJobWiseStatus.setPreScreen(true);
			
			try 
			{
				dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
			} 
			catch (Exception e) 
			{
				return "Error";
			}
			
			int iIsAffilated=0;
			String sDecIsAffilated=validateCTAndDec(dspqct);
			if(sDecIsAffilated!=null && (sDecIsAffilated.equalsIgnoreCase("I") || sDecIsAffilated.equalsIgnoreCase("T")))
				iIsAffilated=1;
			updateJFT(dspqJobWiseStatus,teacherDetail, jobOrder, iIsAffilated);
			
			return "OK";

		} catch (Exception e) {
			e.printStackTrace();
			return "Error";
		}
		
	}
	
	public void sendMailOnNegativeQQ(JobOrder jobOrder, List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnswerDetailsForDistrictSpecificQuestionList)
	{
		JobOrder jb = jobOrderDAO.findById(jobOrder.getJobId(), false, false);
		DistrictMaster districtMaster = jb.getDistrictMaster();
		List<UserMaster> userMasterList=null;
		
		List<Integer> userIds = new ArrayList<Integer>();
		if(districtMaster!=null && districtMaster.getSendNotificationOnNegativeQQ()!=null)
			for(String id : districtMaster.getSendNotificationOnNegativeQQ().split("#"))
				if(!id.equals(""))
				{
					int iId=Utility.getIntValue(id);
					userIds.add(iId);
				}
		if(userIds.size()>0)
			userMasterList = userMasterDAO.getActiveDAUserByUserIds(userIds);
		
		
		//printdata("districtMaster.getSendNotificationOnNegativeQQ():- "+districtMaster.getSendNotificationOnNegativeQQ()+",districtId:- "+districtMaster.getDistrictId()+", userMasterList:- "+userMasterList);
		if(userMasterList!=null && userMasterList.size()>0)
		{
			for(UserMaster userMaster : userMasterList)
			{
				try
				{
					printdata("Start Negative Question Qualification Response");
					
					String sEmailBodyText = MailText.getNegativeQQMailText(userMaster,jobOrder,districtMaster,wrongAnswerDetailsForDistrictSpecificQuestionList);
					String sEmailSubject="Negative Question Qualification Response";
					
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("shadab.ansari@netsutra.com");
					dsmt.setMailto(userMaster.getEmailAddress());
					dsmt.setMailsubject(sEmailSubject);
					dsmt.setMailcontent(sEmailBodyText);
					
					printdata("sEmailSubject:- "+sEmailSubject);
					printdata("sEmailBodyText:- "+sEmailBodyText);
					
					try {
						dsmt.start();	
					} catch (Exception e) {}
					
		     	 	printdata("End Negative Question Qualification Response");
		     	 	emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(),"Negative Question Qualification Response", sEmailBodyText);
				}
				catch(Exception e){e.printStackTrace();}
			}
		}
	}
	
	
	public boolean updateJFT(DspqJobWiseStatus dspqJobWiseStatus,TeacherDetail teacherDetail,JobOrder jobOrder,Integer isAffilated)
	{
		JobForTeacher forTeacher=null;
		List<JobForTeacher> lstJobForTeacher=jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail, jobOrder);
		
		int iStatusId=setSLCStatusForNewJAF("", teacherDetail, jobOrder, dspqJobWiseStatus);
		
		if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
		{
			forTeacher=new JobForTeacher();
			forTeacher.setTeacherId(teacherDetail);
			forTeacher.setJobId(jobOrder);
			forTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
			forTeacher.setIsAffilated(isAffilated);
			forTeacher.setCreatedDateTime(new Date());
			
		}
		else
		{
			forTeacher=lstJobForTeacher.get(0);
		}
		
		if(iStatusId > 0)
		{
			StatusMaster statusMaster=new StatusMaster();
			statusMaster.setStatusId(iStatusId);
			forTeacher.setStatus(statusMaster);
			forTeacher.setStatusMaster(statusMaster);
			jobForTeacherDAO.makePersistent(forTeacher);
		}
		return true;
	}
	
	public boolean getEPI(JobCategoryMaster jobCategoryMaster,String sCandidateType)
	{
		boolean isEPIRequired=false;
		if(sCandidateType!=null && !sCandidateType.equals("") && jobCategoryMaster!=null)
		{
			if(sCandidateType.equalsIgnoreCase("E") && jobCategoryMaster.getBaseStatus()!=null && jobCategoryMaster.getBaseStatus())
				isEPIRequired=true;
			else if(sCandidateType.equalsIgnoreCase("I") && jobCategoryMaster.getEpiForFullTimeTeachers()!=null && jobCategoryMaster.getEpiForFullTimeTeachers())
				isEPIRequired=true;
			else if(sCandidateType.equalsIgnoreCase("T") && jobCategoryMaster.getEpiForIMCandidates()!=null && jobCategoryMaster.getEpiForIMCandidates())
				isEPIRequired=true;
		}
		return isEPIRequired;
	}
	
	public boolean getCustomQuestion(JobOrder jobOrder,JobCategoryMaster jobCategoryMaster,String sCandidateType,TeacherDetail teacherDetail)
	{
		boolean isCustomQuestionRequired=false;
		try {
			if(sCandidateType!=null && !sCandidateType.equals("") && jobCategoryMaster!=null)
			{
				DistrictMaster districtMaster=jobCategoryMaster.getDistrictMaster();
				if(districtMaster==null)
					districtMaster=jobOrder.getDistrictMaster();
				if(sCandidateType.equalsIgnoreCase("E") && jobOrder!=null && jobOrder.getIsJobAssessment()!=null && jobOrder.getIsJobAssessment())
					isCustomQuestionRequired=true;
				else if(sCandidateType.equalsIgnoreCase("T") && districtMaster!=null && districtMaster.getOfferJSI()!=null && districtMaster.getOfferJSI())
				{
					if(jobCategoryMaster.getOfferJSI()!=null && jobCategoryMaster.getJsiForIMCandidates()==1)
						isCustomQuestionRequired=true;
				}
				else if(sCandidateType.equalsIgnoreCase("I") && jobCategoryMaster.getOfferJSI()!=null && jobCategoryMaster.getOfferJSI()==1)
				{
					isCustomQuestionRequired=true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isCustomQuestionRequired;
	}
	
	@Transactional(readOnly=false)
	public String getEPIServiceDataL1(Integer jobId,String isAffilated)
	{
		printdata("getEPIServiceDataL1");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		StringBuffer sb =new StringBuffer();
		try
		{
			TeacherDetail teacherDetail = null;

			if (session == null || session.getAttribute("teacherDetail") == null) 
			{
				//return "false";
			}
			else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
			
			String baseStatus="";
			
			TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster()!=null)
				baseStatus = teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName();
			sb.append("<input type='hidden' id='txtEPIStatus' value='"+baseStatus+"'>");
			
			boolean isKellyJobApply = false;
			isKellyJobApply = jobForTeacherDAO.getJFTByTeachers(teacherDetail);
			boolean isKelly = false;
			if(request.getServerName().contains("kelly") || request.getServerName().toLowerCase().contains("smartpractice") || request.getRequestURL().toString().contains("smartpractice")){
				isKelly = true;
			}
			
			String msgForProfessionalInventoryTm=Utility.getLocaleValuePropByKey("msgForProfessionalInventoryTm", locale);
			String baseMsg=msgForProfessionalInventoryTm;
			if(isKellyJobApply || isKelly)
				baseMsg="Kelly Educational Staffing� (KES�) and TeacherMatch� share a common philosophy and believe high-quality substitute teachers are the most critical factor in fostering student achievement. We are proud to offer you � a valued KES substitute teacher candidate � exclusive access to the TeacherMatch Educator's Professional Inventory EPI�.   While developed specifically for full-time teaching candidates, taking the EPI with KES offers you a great opportunity to gain valuable insights into the essential teaching skill sets that are critical to success in the classroom.  The EPI is NOT used as a hiring assessment tool by KES, and your performance will NEVER be used as a qualifier or filter for you to be able to view or accept substitute teaching positions with KES school districts.  The EPI is for KES substitute teacher talent only.";
			
			String lblCompleted=Utility.getLocaleValuePropByKey("lblCompleted", locale);
			String optTOut=Utility.getLocaleValuePropByKey("optTOut", locale);
			String optIncomlete=Utility.getLocaleValuePropByKey("optIncomlete", locale);
			
			boolean bBottomBtn=false;
			sb.append("<div class='row'>");
			sb.append("<div class='col-sm-3 col-md-3 mt10'>");
			sb.append(Utility.getLocaleValuePropByKey("DSPQlblEPIInner", locale)+"&nbsp;<a href='javascript:void(0);' id='iconpophoverBase' rel='tooltip' data-original-title=\""+baseMsg+"\"><img src='images/qua-icon.png' width='15' height='15' alt=''></a>");
			sb.append("</div><div class='col-sm-2 col-md-2 mt10'>");
			if(baseStatus==null || baseStatus.equals(""))
			{
				sb.append("<strong class='text-error'>"+optIncomlete+"</strong>");
				bBottomBtn=true;
			}
			else if(baseStatus.equalsIgnoreCase("comp"))
			{
				sb.append("<strong class='text-success'>"+lblCompleted+"</strong>");
			}
			else if(baseStatus.equalsIgnoreCase("vlt"))
			{
				sb.append("<strong class='text-error'>"+optTOut+"</strong>");
			}
			else if(baseStatus.equalsIgnoreCase("icomp"))
			{
				sb.append("<strong class='text-error'>"+optIncomlete+"</strong>");
				bBottomBtn=true;
			}
			sb.append("</div></div>");
			
			
			if(bBottomBtn)
			{
				sb.append("<div class='row'>");
				sb.append("<div class='col-sm-12 col-md-12 mt30'>");
				if(isKelly)
				{
					sb.append("<span>Kelly Educational Staffing&reg; (KES&reg;) and TeacherMatch&reg; share a common philosophy and believe high-quality substitute teachers are the most critical factor in fostering student achievement. While developed specifically for full-time teaching candidates, taking the EPI with KES offers you a great opportunity to gain valuable insights into the essential teaching skill sets that are critical to success in the classroom.  The EPI is NOT used as a hiring assessment tool by KES, and your performance will NEVER be used as a qualifier or filter for you to be able to view or accept substitute teaching positions with KES school districts.  The EPI is for KES substitute teacher talent only.</span>");
				}
				else
				{
					sb.append(Utility.getLocaleValuePropByKey("msgEPIIsUsedBySomeDistToScrAply", locale));
				}
				sb.append("</div></div>");
					
				sb.append("<div class='row'>"+
					"<div class='col-sm-9 col-md-9'>" +
						"<div class='mt10'><button class='flatbtn' id='btnprev' type='button' style='width:16%' onclick='return getPreviousPage();'><i class='backicon'></i> "+Utility.getLocaleValuePropByKey("btnPrevDSPQ", locale)+"</button>" +
							"&nbsp;&nbsp;<button class='flatbtn' style='background: #da4f49;width:16%'  type='button' onclick='return cancelApplyJob();'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+" <i class='fa fa-times-circle'></i></button>" +
						"</div>" +
					"</div>" +
					"<div class='col-sm-3 col-md-3'>"+
						"<div class='mt10' style='text-align:right;'>"+
							"<button class='flatbtn' style='width:100px;' id='btnnext' type='button' onclick='return startEPI(0);'>"+Utility.getLocaleValuePropByKey("btnContinueDSPQ", locale)+" <i class='icon'></i></button>"+
						"</div>"+
					"</div>"+
				"</div>");
				
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		printdata(sb.toString());
		
		return sb.toString();
	}
	
	@Transactional(readOnly=false)
	public String getJSIServiceDataL1(Integer jobId,String isAffilated)
	{
		printdata("getJSIServiceDataL1");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		StringBuffer sb =new StringBuffer();
		try
		{
			TeacherDetail teacherDetail = null;

			if (session == null || session.getAttribute("teacherDetail") == null) 
			{
				//return "false";
			}
			else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
			
			JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
			AssessmentDetail assessmentDetail = null;
			TeacherAssessmentStatus teacherBaseAssessmentStatus=null;
			List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
			if(lstAssessmentJobRelation.size()>0)
			{
				AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
				assessmentDetail = assessmentJobRelation.getAssessmentId();
				List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail,assessmentDetail);
				if(lstTeacherAssessmentStatus!=null && lstTeacherAssessmentStatus.size()==1)
				{
					teacherBaseAssessmentStatus=lstTeacherAssessmentStatus.get(0);
				}
			}
			
			String jsiStatus="";
			
			if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster()!=null)
				jsiStatus = teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName();
			sb.append("<input type='hidden' id='txtEPIStatus' value='"+jsiStatus+"'>");
			
			String lblCompleted=Utility.getLocaleValuePropByKey("lblCompleted", locale);
			String optTOut=Utility.getLocaleValuePropByKey("optTOut", locale);
			String optIncomlete=Utility.getLocaleValuePropByKey("optIncomlete", locale);
			
			boolean bBottomBtn=false;
			sb.append("<div class='row'>");
			sb.append("<div class='col-sm-2 col-md-2 mt10'>");
			sb.append(Utility.getLocaleValuePropByKey("DSPQlblJSIInner", locale));
			sb.append("</div><div class='col-sm-2 col-md-2 mt10'>");
			if(jsiStatus==null || jsiStatus.equals(""))
			{
				sb.append("<strong class='text-error'>"+optIncomlete+"</strong>");
				bBottomBtn=true;
			}
			else if(jsiStatus.equalsIgnoreCase("comp"))
			{
				sb.append("<strong class='text-success'>"+lblCompleted+"</strong>");
			}
			else if(jsiStatus.equalsIgnoreCase("vlt"))
			{
				sb.append("<strong class='text-error'>"+optTOut+"</strong>");
			}
			else if(jsiStatus.equalsIgnoreCase("icomp"))
			{
				sb.append("<strong class='text-error'>"+optIncomlete+"</strong>");
				bBottomBtn=true;
			}
			sb.append("</div></div>");
			
			sb.append("<div class='row' id='jsimsg'>");
			sb.append("<div class='col-sm-12 col-md-12 mt10'>");
			sb.append(
					"<table width='100%'>"+
					"<tr>"+
					"<td width='5%' valign='top' style='padding-top: 0px;'>"+
					"	<div id='jafwarningImg'></div>"+
					"</td>"+
					"<td width='85%'>"+
					"	<div id='jafmessage2showConfirm'></div>"+
					"</td>"+
					"</tr>"+
					"</table>"+
					"");
			sb.append("</div></div>");
			
			sb.append("<div class='row'><div class='col-sm-12 col-md-12' style='padding-top:200px;'></div></div>");
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	
	public boolean isEPICompleteStatus(TeacherDetail teacherDetail)
	{
		printdata("isEPIComplete");
		boolean baseInvStatus=false;
		try
		{
			TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster()!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName()!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
				baseInvStatus=true;
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster()!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName()!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
				baseInvStatus=true;
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return baseInvStatus;
	}
	
	
	public boolean isJSICompleteStatus(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		printdata("isJSICompleteStatus");
		boolean baseInvStatus=false;
		AssessmentDetail assessmentDetail = null;
		TeacherAssessmentStatus teacherBaseAssessmentStatus=null;
		List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
		if(lstAssessmentJobRelation.size()>0)
		{
			AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
			assessmentDetail = assessmentJobRelation.getAssessmentId();
			List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail,assessmentDetail);
			if(lstTeacherAssessmentStatus!=null && lstTeacherAssessmentStatus.size()==1)
			{
				teacherBaseAssessmentStatus=lstTeacherAssessmentStatus.get(0);
				if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster()!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
					baseInvStatus=true;
				else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster()!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName()!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
					baseInvStatus=true;
			}
		}
		return baseInvStatus;
	}
	
	public AssessmentDetail jafCheckInventory(JobOrder jobOrder,String epiJobId)
	{
		int iJobId=Utility.getIntValue(epiJobId);
		return assessmentCampaignAjax.checkInventory(jobOrder,iJobId);
	}
	
	@Transactional(readOnly=false)
	public String jafLoadAssessmentQuestions(AssessmentDetail assessmentDetail,JobOrder jobOrder,String epiJobId)
	{
		int iJobId=Utility.getIntValue(epiJobId);
		return assessmentCampaignAjax.loadAssessmentQuestions(assessmentDetail,jobOrder,iJobId);
	}
	
	@Transactional(readOnly=false)
	public int jafinsertTeacherAssessmentAttempt(AssessmentDetail assessmentDetail,JobOrder jobOrder)
	{
		return assessmentCampaignAjax.insertTeacherAssessmentAttempt(assessmentDetail, jobOrder);
		
	}
	
	public String jafinsertLastAttempt(AssessmentDetail assessmentDetail, Integer assessmentTakenCount, JobOrder jobOrder)
	{
		return assessmentCampaignAjax.insertLastAttempt(assessmentDetail, assessmentTakenCount, jobOrder);
	}
	
	@Transactional(readOnly=false)
	public String jafgetAssessmentSectionCampaignQuestions(AssessmentDetail assessmentDetail,TeacherAssessmentdetail teacherAssessmentdetail,TeacherAssessmentQuestion teacherAssessmentQues,TeacherSectionDetail teacherSectionDetail,TeacherAnswerDetail[] teacherAnswerDetails,int attemptId,int newJobId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		int assessmentType=1;
		Boolean epiStandalone = session.getAttribute("epiStandalone")==null?false:(Boolean)session.getAttribute("epiStandalone");
		StringBuffer sb =new StringBuffer();
		JobOrder jobOrderForUpdate=null;
		try{

			try{
				if(newJobId>0)
					jobOrderForUpdate = jobOrderDAO.findById(newJobId, false, false);
			}catch(Exception e){
				e.printStackTrace();
			}

			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
			try{
				System.out.print("New JAF Assesment:"+new Date()+", T:"+teacherDetail.getTeacherId() +", E:"+teacherDetail.getEmailAddress());
			}catch(Exception e){
				e.printStackTrace();
			}

			if(attemptId!=0)
			{
				teacherAssessmentAttemptDAO.updateAssessmentAttemptSessionTime(attemptId,false);
			}

			if(teacherAssessmentQues!=null && teacherAssessmentQues.getTeacherAssessmentQuestionId()!=null)
			{

				TeacherAnswerDetail teacherAnswerDetail = teacherAnswerDetailDAO.findTADetailbyQuestion(teacherAssessmentQues);
                if(teacherAnswerDetails!=null && teacherAnswerDetails.length>0)
                {
                    if(teacherAnswerDetail==null)
                        teacherAnswerDetail = teacherAnswerDetails[0];
					if(teacherAnswerDetail.getJobOrder().getJobId()==null)
						teacherAnswerDetail.setJobOrder(null);
					
					teacherAnswerDetail.setAssessmentDetail(assessmentDetail);
					teacherAnswerDetail.setTeacherAssessmentdetail(teacherAssessmentdetail);
					teacherAnswerDetail.setTeacherAssessmentQuestion(teacherAssessmentQues);
					teacherAnswerDetail.setCreatedDateTime(new Date());
					teacherAnswerDetail.setTeacherDetail(teacherDetail);
					teacherAnswerDetailDAO.makePersistent(teacherAnswerDetail);
				}
				/////////////////////////////////////////////////////////////////////////////////////////////
				teacherAssessmentQues=teacherAssessmentQuestionDAO.findById(teacherAssessmentQues.getTeacherAssessmentQuestionId(), false, false);
				teacherAssessmentQues.setIsAttempted(true);
				teacherAssessmentQuestionDAO.makePersistent(teacherAssessmentQues);
			}
			int teacherSectionId = 0;
			if(teacherSectionDetail!=null && teacherSectionDetail.getTeacherSectionId()!=null)
			{
				teacherSectionId = teacherSectionDetail.getTeacherSectionId();
			}
			List<TeacherAssessmentQuestion> teacherAssessmentQuestions =null;
			Criterion criterion = Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("isAttempted", false);
			Criterion criterion3 = Restrictions.eq("isAttempted", true);
			int totalQuestions = teacherAssessmentQuestionDAO.getRowCount(criterion,criterion1);
			int totalQuestionsAttempted = teacherAssessmentQuestionDAO.getRowCount(criterion,criterion1,criterion3);
			teacherAssessmentQuestions = teacherAssessmentQuestionDAO.findWithLimit(null,0,1,criterion,criterion1,criterion2);
			int i=0;
			List<TeacherAssessmentOption> teacherQuestionOptionsList = null;
			String questionInstruction = null;
			int teacherSectionIDD = 0;
			String shortName = "";
			String sectionInstructions="";
			for (TeacherAssessmentQuestion teacherAssessmentQuestion : teacherAssessmentQuestions) 
			{
				teacherAssessmentdetail = teacherAssessmentQuestion.getTeacherAssessmentdetail();

				teacherSectionIDD=teacherAssessmentQuestion.getTeacherSectionDetail().getTeacherSectionId();
				if(teacherSectionId==0 || (teacherSectionIDD!=teacherSectionId))
				{
					sb.append("<div class='col-sm-12 col-md-12'><b>"+Utility.getLocaleValuePropByKey("epiInstructionssection", locale)+"</b></div>");

					sectionInstructions = teacherAssessmentQuestion.getTeacherSectionDetail().getSectionInstructions();

					sb.append("<div class='col-sm-12 col-md-12'>"+Utility.getUTFToHTML(sectionInstructions)+"</div>");
					
					String sectionVideoUrl = teacherAssessmentQuestion.getTeacherSectionDetail().getSectionVideoUrl();

					if(sectionVideoUrl!=null && !sectionVideoUrl.equals(""))
					{
						sb.append("<div class='col-sm-12 col-md-12'>"+Utility.getYouTubeCode(sectionVideoUrl,450,350)+"</div>");
					}
					if(teacherSectionId!=0)
						sb.append("<input type='hidden' id='teacherAssessmentQuestionId' value=''/>" );
					
					sb.append("<input type='hidden' id='teacherSectionId' value='"+teacherSectionIDD+"'/>" );
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("document.getElementById('sectionName').innerHTML='<b>Section: "+teacherAssessmentQuestion.getTeacherSectionDetail().getSectionName()+"</b>';");
					sb.append("document.getElementById('sectionName').style.display='block';");
					sb.append("document.getElementById('questionTimer').style.display='none';");
					sb.append("</script>");
					return sb.toString();

				}

				sb.append("<div class='col-sm-12 col-md-12 mt10'><b>"+Utility.getLocaleValuePropByKey("headQues", locale)+" "+(++totalQuestionsAttempted)+" "+Utility.getLocaleValuePropByKey("epiof", locale)+ " "+totalQuestions+"</b></div>");
				try{
					assessmentType=teacherAssessmentdetail.getAssessmentType();
					printdata(", Type:"+teacherAssessmentdetail.getAssessmentType()+", Q:"+(totalQuestionsAttempted)+" of "+totalQuestions);
				}catch(Exception e){}
				questionInstruction = teacherAssessmentQuestion.getQuestionInstruction()==null?"":teacherAssessmentQuestion.getQuestionInstruction();
				if(!questionInstruction.equals(""))
					sb.append(""+Utility.getUTFToHTML(questionInstruction)+"<br/>");

				sb.append("<div class='col-sm-12 col-md-12'><label><strong><span id='jafinventoryQuestion'>"+Utility.getUTFToHTML(teacherAssessmentQuestion.getQuestion())+"</span><span id='lbljafInvenWarningBeforeSpace'></span><span id='lbljafInvenWarning'></span><span id='lbljafInvenWarningAfterSpace'></span><span class='required'>*</span></strong></label></div>");
				sb.append("<input type='hidden' name='o_maxMarks' value='"+(teacherAssessmentQuestion.getMaxMarks()==null?0:teacherAssessmentQuestion.getMaxMarks())+"'  />");

				teacherQuestionOptionsList = teacherAssessmentQuestion.getTeacherAssessmentOptions();
				shortName=teacherAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName();

				if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel") || shortName.equalsIgnoreCase("lkts"))
				{
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) 
					{	
						sb.append("<div class='col-sm-12 col-md-12'>");
						sb.append("<label class='radio' style='margin-top: 0px;margin-bottom: 2px;'>");
						sb.append("<input type='radio' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"'>"+teacherQuestionOption.getQuestionOption()+"</br>");
						sb.append("</label>");
						sb.append("</div>");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'  /></td></tr>");
					}
				}
				if(shortName.equalsIgnoreCase("it"))
				{
					sb.append("<table >");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherQuestionOption.getQuestionOption()+"'> ");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'  /></td></tr>");
					}
					sb.append("</table>");
				}
				else if(shortName.equalsIgnoreCase("rt"))
				{
					int rank=1;
					sb.append("<table>");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' id='rnk"+rank+"' name='rank' class='span1' maxlength='1' onkeypress='return checkForIntUpto6(event)' onblur='checkUniqueRank(this);'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"' /><input type='hidden' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' />");
						sb.append("<input type='hidden' name='o_rank' value='"+(teacherQuestionOption.getRank()==null?0:teacherQuestionOption.getRank())+"' />");
						sb.append("<script type=\"text/javascript\" language=\"javascript\">");
						sb.append("document.getElementById('rnk1').focus();");
						sb.append("</script></td></tr>");
						rank++;
					}
					sb.append("</table >");
				}else if(shortName.equalsIgnoreCase("sl"))
				{
					sb.append("&nbsp;&nbsp;<input type='text' name='opt' id='opt' class='span15' maxlength='75'/><br/>");
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("document.getElementById('opt').focus();");
					sb.append("</script>");
				}
				else if(shortName.equalsIgnoreCase("ml"))
				{
					sb.append("<div class='col-sm-12 col-md-12'>");
					sb.append("<textarea name='opt' id='opt' class='form-control' maxlength='5000' rows='15' style='width:98%;margin:5px;' /></textarea>");
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("document.getElementById('opt').focus();");
					sb.append("</script>");
					sb.append("</div>");
				}
				else if(shortName.equalsIgnoreCase("mlsel"))
				{
					sb.append("<table>");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;padding: 5px 3px;' >"+teacherQuestionOption.getQuestionOption()+" ");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'  /></td></tr>");
					}
					sb.append("</table>");
				}
				else if(shortName.equalsIgnoreCase("mloet"))
				{
					sb.append("<table>");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;padding: 5px 3px;' >"+teacherQuestionOption.getQuestionOption()+" ");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'  /></td></tr>");
					}
					sb.append("</table>");
					
					sb.append("<table>");
					sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><textarea name='insertedText' id='insertedText' class='form-control' maxlength='5000' rows='15' cols='100' style='width:98%;margin:5px;' /></textarea></td></tr>");
					sb.append("</table>");
					
					
				}
				else if(shortName.equalsIgnoreCase("sloet"))
				{
					sb.append("<table>");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;padding: 5px 3px;' >"+teacherQuestionOption.getQuestionOption()+" ");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'  /></td></tr>");
					}
					sb.append("</table>");
					if(shortName.equalsIgnoreCase("sloet"))
					{
						sb.append("<table >");
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><textarea name='insertedText' id='insertedText' class='form-control' maxlength='5000' rows='15' cols='100' style='width:98%;margin:5px;' /></textarea></td></tr>");
						sb.append("</table>");
					}
					
				}
				sb.append("<input type='hidden' id='teacherAssessmentQuestionId' value='"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"'/>" );
				sb.append("<input type='hidden' id='teacherSectionId' value='"+teacherSectionIDD+"'/>" );
				sb.append("<input type='hidden' id='questionWeightage' value='"+(teacherAssessmentQuestion.getQuestionWeightage()==null?0:teacherAssessmentQuestion.getQuestionWeightage())+"'/>" );
				sb.append("<input type='hidden' id='questionTypeId' value='"+teacherAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
				sb.append("<input type='hidden' id='questionTypeShortName' value='"+teacherAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName()+"'/>" );

				if(teacherAssessmentdetail.getQuestionSessionTime()!=null && teacherAssessmentdetail.getQuestionSessionTime()!=0)
				{
					sb.append("<script type=\"text/javascript\" language=\"javascript\">tmTimer("+teacherAssessmentdetail.getQuestionSessionTime()+",3);" );
					sb.append("</script>");
				}
				sb.append("<input type='hidden' id='questionSessionTime' value='"+(teacherAssessmentdetail.getQuestionSessionTime()==null?0:teacherAssessmentdetail.getQuestionSessionTime())+"'/>" );

			}
			
			if(teacherAssessmentQuestions.size()==0)
			{
				if(totalQuestions!=0)
				{
					JobOrder jobOrder = new JobOrder();

					List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
					int jId=0;
					lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentdetail);

					TeacherAssessmentStatus teacherAssessmentStatus=lstTeacherAssessmentStatus.get(0);
					
					if(lstTeacherAssessmentStatus.size()!=0)
						if(teacherAssessmentStatus!=null)
						{
							StatusMaster statusMasterIcomp = WorkThreadServlet.statusMap.get("icomp");
							
							teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();

							StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
							List<JobOrder> jobs = new ArrayList<JobOrder>();
							for(TeacherAssessmentStatus tas: lstTeacherAssessmentStatus)
							{
								jobs.add(tas.getJobOrder());

								tas.setStatusMaster(statusMaster);
								
								if(statusMaster.getStatusShortName().equalsIgnoreCase("comp"))
									tas.setAssessmentCompletedDateTime(new Date());
								
								teacherAssessmentStatusDAO.makePersistent(tas);
							}

							//						printdata("done.......................");
							boolean onDashboard = false;
							List<JobForTeacher> lstJobForTeacherIcomp = new ArrayList<JobForTeacher>();
							List<JobOrder> lstJBOrder = new ArrayList<JobOrder>();
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							sb.append("window.onbeforeunload = function() {};");
							if(teacherAssessmentdetail.getAssessmentType()==1)
							{
								///////////////////////// candidate data coping ///////////////////
								String status = teacherAssessmentStatus.getStatusMaster().getStatusShortName();
								if(status.equalsIgnoreCase("comp"))
								{
									CadidateRawScoreThread crt = new CadidateRawScoreThread();
									crt.setCandidateRawScoreDAO(candidateRawScoreDAO);
									crt.setTeacherAnswerDetailDAO(teacherAnswerDetailDAO);
									crt.setTeacherAssessmentQuestionDAO(teacherAssessmentQuestionDAO);
									crt.setTeacherDetail(teacherDetail);
									crt.setReportService(reportService);
									crt.setTeacherAssessmentdetail(teacherAssessmentdetail);
									crt.setTeacherAssessmentStatus(teacherAssessmentStatus);
									crt.start();
								}
								///////////////////////////////////////////////////////////////////

								///////////////////////// for external user /////////////////////////
								List<JobForTeacher> lstJobForTeacher = null;
								JobForTeacher jobForTeacherObj=null;

								List<StatusMaster> statusList =  Utility.getStaticMasters(new String[]{"icomp","vlt"});
								Map<Integer,Boolean> qqRequiredMap = new HashMap<Integer, Boolean>();
								//for completed 
								lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail,statusList);
								List<DistrictMaster> districts = new ArrayList<DistrictMaster>();
								for(JobForTeacher jft: lstJobForTeacher){
									districts.add(jft.getJobId().getDistrictMaster());
								}
								if(districts.size()>0)
								{
									districts = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistricts(districts);
									
									for (DistrictMaster districtMaster : districts) {
										qqRequiredMap.put(districtMaster.getDistrictId(), false);
									}
									
									if(districts.size()>0)
									{
										districts = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findByQQTakenByTeacher(districts,teacherDetail);
										for (DistrictMaster districtMaster : districts) {
											qqRequiredMap.put(districtMaster.getDistrictId(), true);
										}
									}
								}
								
								for(JobForTeacher jft: lstJobForTeacher){
								
									Integer districtId = null;
									if(jft.getJobId().getDistrictMaster()!=null)
										districtId	 = jft.getJobId().getDistrictMaster().getDistrictId();
								Boolean teacherQQTaken	= qqRequiredMap.get(districtId)==null?true:qqRequiredMap.get(districtId);
								if(jft.getJobId().getJobCategoryMaster().getOfferQualificationItems()==false)
									teacherQQTaken=true;
								else if(jft.getJobId().getDistrictMaster()==null)
									teacherQQTaken=true;
								JobOrder jO = jft.getJobId();
								if(jO.getIsJobAssessment()==false){
										boolean completeFlag=false;
										try{
											if(!jft.getStatus().getStatusShortName().equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
												completeFlag=true;
												jobForTeacherObj=jft;
											}
										}catch(Exception e){
											e.printStackTrace();
										}
										if(jft.getInternalSecondaryStatus()!=null){
											jft.setStatus(jft.getInternalStatus());
											jft.setStatusMaster(null);
											jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
										}else if(jft.getInternalStatus()!=null){
											jft.setStatus(jft.getInternalStatus());
											jft.setStatusMaster(jft.getInternalStatus());
										}else{
											String jftStatus = jft.getStatus().getStatusShortName();
											if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
												if(teacherQQTaken)
												{
													jft.setStatus(statusMaster);
													try{
														if(jft.getSecondaryStatus()!=null){
															jft.setStatusMaster(null);
														}else{
															jft.setStatusMaster(statusMaster);
														}
													}catch(Exception e){
														jft.setStatusMaster(statusMaster);
														e.printStackTrace();
													}
												}else
												{
													jft.setStatus(statusMasterIcomp);
													try{
														if(jft.getSecondaryStatus()!=null){
															jft.setStatusMaster(null);
														}else{
															jft.setStatusMaster(statusMasterIcomp);
														}
													}catch(Exception e){
														jft.setStatusMaster(statusMasterIcomp);
														e.printStackTrace();
													}
												}
											}
										}
										
										jobForTeacherDAO.makePersistent(jft);
										
										if(teacherQQTaken)
										{
											try{
												if(completeFlag && jobForTeacherObj!=null && jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
													printdata("::::::::::::::Update Status 23-::::::::::::::::");
													commonService.futureJobStatus(jobForTeacherObj.getJobId(), jobForTeacherObj.getTeacherId(),jobForTeacherObj);
												}
											}catch(Exception e){
												e.printStackTrace();
											}
										}
										
									}else if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){//**##**
										lstJobForTeacherIcomp.add(jft);
										lstJBOrder.add(jft.getJobId());
									}
								}
								TeacherAssessmentStatus tAStatus = null;
								if(lstJobForTeacherIcomp.size()>0){
									List<TeacherAssessmentStatus> lstTAStatus = teacherAssessmentStatusDAO.findByTeacherAndListJob(teacherDetail, lstJBOrder);
									Map<Integer, TeacherAssessmentStatus> mapAssessStatus = new HashMap<Integer, TeacherAssessmentStatus>();
									for(TeacherAssessmentStatus tas: lstTAStatus){
										mapAssessStatus.put(tas.getJobOrder().getJobId(), tas);
									}
									districts = new ArrayList<DistrictMaster>();
									for(JobForTeacher jft: lstJobForTeacherIcomp){
										districts.add(jft.getJobId().getDistrictMaster());
									}
									if(districts.size()>0)
									{
										districts = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistricts(districts);
										
										for (DistrictMaster districtMaster : districts) {
											qqRequiredMap.put(districtMaster.getDistrictId(), false);
										}
										
										if(districts.size()>0)
										{
											districts = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findByQQTakenByTeacher(districts,teacherDetail);
											for (DistrictMaster districtMaster : districts) {
												qqRequiredMap.put(districtMaster.getDistrictId(), true);
											}
										}
									}
									
									for(JobForTeacher jft: lstJobForTeacherIcomp){
										
										Integer districtId = jft.getJobId().getDistrictMaster().getDistrictId();
										Boolean teacherQQTaken	= qqRequiredMap.get(districtId)==null?true:qqRequiredMap.get(districtId);
										if(jft.getJobId().getJobCategoryMaster().getOfferQualificationItems()==false)
											teacherQQTaken=true;
										
										boolean completeFlag=false;
										tAStatus = mapAssessStatus.get(jft.getJobId().getJobId());
										if(tAStatus==null && jft.getJobId().getJobCategoryMaster().getOfferJSI()==2)
										{
											tAStatus = new TeacherAssessmentStatus();
											tAStatus.setStatusMaster(statusMaster);
										}
										try{
											if(!jft.getStatus().getStatusShortName().equalsIgnoreCase("comp") && tAStatus!=null &&  tAStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
												completeFlag=true;
												jobForTeacherObj=jft;
											}
										}catch(Exception e){
											e.printStackTrace();
										}
										if(tAStatus!=null){
											if(jft.getInternalSecondaryStatus()!=null){
												jft.setStatus(jft.getInternalStatus());
												jft.setStatusMaster(null);
												jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
											}else if(jft.getInternalStatus()!=null){
												jft.setStatus(jft.getInternalStatus());
												jft.setStatusMaster(jft.getInternalStatus());
											}else{
												String jftStatus = jft.getStatus().getStatusShortName();
												if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
													if(teacherQQTaken)
													{
														jft.setStatus(tAStatus.getStatusMaster());
														try{
															if(jft.getSecondaryStatus()!=null){
																jft.setStatusMaster(null);
															}else{
																jft.setStatusMaster(tAStatus.getStatusMaster());
															}
														}catch(Exception e){
															jft.setStatusMaster(tAStatus.getStatusMaster());
															e.printStackTrace();
														}
													}else
													{
														jft.setStatus(statusMasterIcomp);
														try{
															if(jft.getSecondaryStatus()!=null){
																jft.setStatusMaster(null);
															}else{
																jft.setStatusMaster(statusMasterIcomp);
															}
														}catch(Exception e){
															jft.setStatusMaster(statusMasterIcomp);
															e.printStackTrace();
														}
													}
													
												}
											}
											jobForTeacherDAO.makePersistent(jft);
											if(teacherQQTaken)
											{
												try{
													if(completeFlag && jobForTeacherObj!=null && jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
														commonService.futureJobStatus(jobForTeacherObj.getJobId(), jobForTeacherObj.getTeacherId(),jobForTeacherObj);
													}
												}catch(Exception e){
													e.printStackTrace();
												}
											}
										}else
										{
											jft.setStatus(statusMasterIcomp);
											try{
												if(jft.getSecondaryStatus()!=null){
													jft.setStatusMaster(null);
												}else{
													jft.setStatusMaster(statusMasterIcomp);
												}
											}catch(Exception e){
												jft.setStatusMaster(statusMasterIcomp);
												e.printStackTrace();
											}
											jobForTeacherDAO.makePersistent(jft);
										}
									}
								}
								/////////////////////////////////////////////////////////////////////
								onDashboard = true; 
							}
							else if(teacherAssessmentdetail.getAssessmentType()==2)
							{
								jobOrder=teacherAssessmentStatus.getJobOrder();
								try{
									if(jobOrderForUpdate==null){
										jobOrderForUpdate=jobOrder;
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								printdata("jobOrderForUpdate::::::"+jobOrderForUpdate);
								jId=jobOrder.getJobId();
								List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByAssessment(teacherAssessmentStatus.getAssessmentDetail());

								for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations1) {
									jobs.add(assessmentJobRelation.getJobId());
								}

								List<JobForTeacher> jobForTeachers = jobForTeacherDAO.findJobByTeacherAndSatusFormJobIds(teacherDetail, jobs);
								String jftStatus = "";
								boolean completeFlag=false;
								JobForTeacher jobForTeacherObj=null;
								
								//==================
								Map<Integer,Boolean> qqRequiredMap = new HashMap<Integer, Boolean>();
								
								List<DistrictMaster> districts = new ArrayList<DistrictMaster>();
								for(JobForTeacher jft: jobForTeachers){
									districts.add(jft.getJobId().getDistrictMaster());
								}
								if(districts.size()>0)
								{
									districts = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistricts(districts);
									
									for (DistrictMaster districtMaster : districts) {
										qqRequiredMap.put(districtMaster.getDistrictId(), false);
									}
									
									if(districts.size()>0)
									{
										districts = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findByQQTakenByTeacher(districts,teacherDetail);
										for (DistrictMaster districtMaster : districts) {
											qqRequiredMap.put(districtMaster.getDistrictId(), true);
										}
									}
								}
								//==========================================================
								
								for(JobForTeacher jft: jobForTeachers)
								{
									jftStatus = jft.getStatus().getStatusShortName();
									Integer districtId = jft.getJobId().getDistrictMaster().getDistrictId();
									Boolean teacherQQTaken	= qqRequiredMap.get(districtId)==null?true:qqRequiredMap.get(districtId);
									if(jft.getJobId().getJobCategoryMaster().getOfferQualificationItems()==false)
										teacherQQTaken=true;
									
									try{
										if(jobOrderForUpdate!=null && jobOrderForUpdate.getJobId().equals(jft.getJobId().getJobId())){
											if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
												completeFlag=true;
												jobForTeacherObj=jft;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									if(!(jftStatus.equalsIgnoreCase("hird") || jftStatus.equalsIgnoreCase("widrw") || jftStatus.equalsIgnoreCase("hide")))
									{
										if(jft.getInternalSecondaryStatus()!=null){
											jft.setStatus(jft.getInternalStatus());
											jft.setStatusMaster(null);
											jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
										}else if(jft.getInternalStatus()!=null){
											jft.setStatus(jft.getInternalStatus());
											jft.setStatusMaster(jft.getInternalStatus());
										}else{
											if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
												
												if(teacherQQTaken)
												{
													jft.setStatus(statusMaster);
													try{
														if(jft.getSecondaryStatus()!=null){
															jft.setStatusMaster(null);
														}else{
															jft.setStatusMaster(statusMaster);
														}
													}catch(Exception e){
														jft.setStatusMaster(statusMaster);
														e.printStackTrace();
													}
												}else
												{
													jft.setStatus(statusMasterIcomp);
													try{
														if(jft.getSecondaryStatus()!=null){
															jft.setStatusMaster(null);
														}else{
															jft.setStatusMaster(statusMasterIcomp);
														}
													}catch(Exception e){
														jft.setStatusMaster(statusMasterIcomp);
														e.printStackTrace();
													}
												}
											}
										}
										// New Check JSI optional
										if(jft.getJobId().getJobAssessmentStatus()==1)
											jobForTeacherDAO.makePersistent(jft);
									}
								}
								try{
									if(completeFlag && jobForTeacherObj!=null && jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
										commonService.futureJobStatus(jobForTeacherObj.getJobId(), jobForTeacherObj.getTeacherId(),jobForTeacherObj);
									}
								}catch(Exception e){
									e.printStackTrace();
								}


							}

								String redirectURL = "jobapplicationflowepi.do";
								printdata("jId "+jId);
								if(jId>0)
								{
									printdata("Hum JSI per ja rahe hai");
									redirectURL = "jobapplicationflowcustomquestion.do"; // JSI ... Custome Question
								}
								else
								{
									printdata("Hum EPI per ja rahe hai");
								}
								sb.append("jafRedirectInventory(0,'"+redirectURL+"',"+assessmentType+");");
							//}
							sb.append("</script>");
						}

					teacherAssessmentdetail.setIsDone(true);
					teacherAssessmentDetailDAO.makePersistent(teacherAssessmentdetail);

				}
				else
				{
					sb.append("No Questions found");
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("window.onbeforeunload = function() {};");
					sb.append("document.getElementById('mainDiv').style.display='none';");
					if(epiStandalone)
						sb.append("window.location.href='epiboard.do'");
					else
						sb.append("window.location.href='userdashboard.do'");

					sb.append("</script>");
				}

			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return sb.toString();
	
	}
	
	@Transactional(readOnly=false)
	public String jafFinishAssessment(TeacherAssessmentdetail teacherAssessmentdetail,JobOrder jobOrder,int newJobId,int sessionCheck)
	{
		printdata("::::::::::::::: jafFinishAssessment :::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			Boolean epiStandalone = session.getAttribute("epiStandalone")==null?false:(Boolean)session.getAttribute("epiStandalone");

			String ipAddress = request.getRemoteAddr();
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");


			////////////////////////////////////////////////////////////////////////////////
			StringBuffer sb =new StringBuffer();
			int jId=0;
			List<TeacherAssessmentQuestion> teacherAssessmentQuestions =null;
			Criterion criterion = Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("isAttempted", false);
			teacherAssessmentQuestions = teacherAssessmentQuestionDAO.findByCriteria(criterion,criterion1,criterion2);
			int unattemptedQuestions = teacherAssessmentQuestions.size();

			List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;

			lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentdetail);
			TeacherAssessmentStatus teacherAssessmentStatus=lstTeacherAssessmentStatus.get(0);

			int assessmentType=1;
			if(teacherAssessmentStatus!=null){
				if(teacherAssessmentStatus.getAssessmentType()!=null){
					assessmentType=teacherAssessmentStatus.getAssessmentType();
				}
			}

			if(lstTeacherAssessmentStatus.size()!=0)
				if(teacherAssessmentStatus!=null)
				{
					StatusMaster statusMaster = null;
					String appendMsg="not ";
					String status="";
					if(unattemptedQuestions==0)
					{
						status="comp";
						appendMsg="";
					}
					else
						status="vlt";

					statusMaster = WorkThreadServlet.statusMap.get(status);

					List<JobOrder> jobs = new ArrayList<JobOrder>();
					for(TeacherAssessmentStatus tas: lstTeacherAssessmentStatus)
					{
						jobs.add(tas.getJobOrder());

						tas.setStatusMaster(statusMaster);
						if(statusMaster.getStatusShortName().equalsIgnoreCase("comp"))
							tas.setAssessmentCompletedDateTime(new Date());
						teacherAssessmentStatusDAO.makePersistent(tas);
					}

					boolean onDashboard = false;
					List<JobForTeacher> lstJobForTeacherIcomp = new ArrayList<JobForTeacher>();
					List<JobOrder> lstJBOrder = new ArrayList<JobOrder>();
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("window.onbeforeunload = function() {};");

					if(teacherAssessmentdetail.getAssessmentType()==1)
					{
						///////////////////////// candidate data coping ///////////////////
						if(status.equalsIgnoreCase("comp"))
						{
							CadidateRawScoreThread crt = new CadidateRawScoreThread();
							crt.setCandidateRawScoreDAO(candidateRawScoreDAO);
							crt.setTeacherAnswerDetailDAO(teacherAnswerDetailDAO);
							crt.setTeacherAssessmentQuestionDAO(teacherAssessmentQuestionDAO);
							crt.setTeacherDetail(teacherDetail);
							crt.setReportService(reportService);
							crt.setTeacherAssessmentdetail(teacherAssessmentdetail);
							crt.setTeacherAssessmentStatus(teacherAssessmentStatus);
							crt.start();
						}
						///////////////////////////////////////////////////////////////////



						///////////////////////// for external user /////////////////////////
						List<JobForTeacher> lstJobForTeacher= null;
						lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail,WorkThreadServlet.statusMap.get("icomp"));
						for(JobForTeacher jft: lstJobForTeacher)
						{
							if(jft.getJobId().getIsJobAssessment()==false)
							{
								if(jft.getInternalSecondaryStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(null);
									jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
								}else if(jft.getInternalStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(jft.getInternalStatus());
								}else{
									String jftStatus = jft.getStatus().getStatusShortName();
									if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
										jft.setStatus(statusMaster);
										try{
											if(jft.getSecondaryStatus()!=null){
												jft.setStatusMaster(null);
											}else{
												jft.setStatusMaster(statusMaster);
											}
										}catch(Exception e){
											jft.setStatusMaster(statusMaster);
											e.printStackTrace();
										}
									}
								}

								boolean isInviteFlag = false;
								if(jft.getJobId().getIsInviteOnly()!=null && jft.getJobId().getIsInviteOnly())
								{
									isInviteFlag=true;
								}
								if(isInviteFlag==false)	{
									jobForTeacherDAO.makePersistent(jft);
								}
							}
							else if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){ //**##**
								lstJobForTeacherIcomp.add(jft);
								lstJBOrder.add(jft.getJobId());
							}
						}

						TeacherAssessmentStatus tAStatus = null;
						if(lstJobForTeacherIcomp.size()>0){
							List<TeacherAssessmentStatus> lstTAStatus = teacherAssessmentStatusDAO.findByTeacherAndListJob(teacherDetail, lstJBOrder);
							Map<Integer, TeacherAssessmentStatus> mapAssessStatus = new HashMap<Integer, TeacherAssessmentStatus>();
							for(TeacherAssessmentStatus tas: lstTAStatus){
								mapAssessStatus.put(tas.getJobOrder().getJobId(), tas);
							}

							for(JobForTeacher jft: lstJobForTeacherIcomp){
								tAStatus = mapAssessStatus.get(jft.getJobId().getJobId());
								if(tAStatus!=null){
									if(jft.getInternalSecondaryStatus()!=null){
										jft.setStatus(jft.getInternalStatus());
										jft.setStatusMaster(null);
										jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
									}else if(jft.getInternalStatus()!=null){
										jft.setStatus(jft.getInternalStatus());
										jft.setStatusMaster(jft.getInternalStatus());
									}else{
										String jftStatus = jft.getStatus().getStatusShortName();
										if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
											jft.setStatus(tAStatus.getStatusMaster());
											try{
												if(jft.getSecondaryStatus()!=null){
													jft.setStatusMaster(null);
												}else{
													jft.setStatusMaster(tAStatus.getStatusMaster());
												}
											}catch(Exception e){
												jft.setStatusMaster(tAStatus.getStatusMaster());
												e.printStackTrace();
											}	
										}
									}
									boolean isInviteFlag = false;
									if(jft.getJobId().getIsInviteOnly()!=null && jft.getJobId().getIsInviteOnly())
									{
										isInviteFlag=true;
									}
									if(isInviteFlag==false)	{
										// New Check JSI optional
										if(jft.getJobId().getJobAssessmentStatus()==1)
											jobForTeacherDAO.makePersistent(jft);
									}
									
								}
							}							
						}


						/////////////////////////////////////////////////////////////////////
						onDashboard = true; 
					}
					else if(teacherAssessmentdetail.getAssessmentType()==2)
					{
						jobOrder=teacherAssessmentStatus.getJobOrder();
						List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByAssessment(teacherAssessmentStatus.getAssessmentDetail());

						for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations1) {
							jobs.add(assessmentJobRelation.getJobId());
						}

						List<JobForTeacher> jobForTeachers = jobForTeacherDAO.findJobByTeacherAndSatusFormJobIds(teacherDetail, jobs);

						String jftStatus = "";
						for(JobForTeacher jft: jobForTeachers)
						{
							jftStatus = jft.getStatus().getStatusShortName();
							if(!(jftStatus.equalsIgnoreCase("hird") || jftStatus.equalsIgnoreCase("widrw") || jftStatus.equalsIgnoreCase("hide")))
							{
								if(jft.getInternalSecondaryStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(null);
									jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
								}else if(jft.getInternalStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(jft.getInternalStatus());
								}else{
									if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
										jft.setStatus(statusMaster);
										try{
											if(jft.getSecondaryStatus()!=null){
												jft.setStatusMaster(null);
											}else{
												jft.setStatusMaster(statusMaster);
											}
										}catch(Exception e){
											jft.setStatusMaster(statusMaster);
											e.printStackTrace();
										}
									}
								}
								// New Check JSI optional
								if(jft.getJobId().getJobAssessmentStatus()==1)
									jobForTeacherDAO.makePersistent(jft);
							}
						}


					}

					if(onDashboard)
					{}
					
					String redirectURL = "jobapplicationflowepi.do";
					if(assessmentType==2)
					{
						redirectURL = "jobapplicationflowcustomquestion.do"; // JSI ... Custome Question
					}
					sb.append("jafRedirectInventory(0,'"+redirectURL+"',"+assessmentType+");");
					
					sb.append("</script>");

					teacherAssessmentdetail = teacherAssessmentDetailDAO.findById(teacherAssessmentdetail.getTeacherAssessmentId(), false, false);
					teacherAssessmentdetail.setIsDone(true);
					teacherAssessmentDetailDAO.makePersistent(teacherAssessmentdetail);
				}

			////////////////////////////////////////////////////////////////////////////////

			//printdata(sb.toString());
			
			return sb.toString();
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	
		
	}
	
	@Transactional(readOnly=false)
	public int jafCheckAssessmentDone(TeacherAssessmentdetail teacherAssessmentdetail)
	{
		return assessmentCampaignAjax.checkAssessmentDone(teacherAssessmentdetail);
	}
	
	@Transactional(readOnly=false)
	public int jafSaveStrike(TeacherAssessmentdetail teacherAssessmentdetail,TeacherAssessmentQuestion teacherAssessmentQuestion)
	{
		return assessmentCampaignAjax.saveStrike(teacherAssessmentdetail, teacherAssessmentQuestion);
	}
	
	@Transactional(readOnly=false)
	public String jafValidationInventoryEPI(int jobId,String dspqct,String txtEPIStatus)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		return "OK";
	
		
	
	}
	
	@Transactional(readOnly=false)
	public String jafValidationInventoryJSI(int jobId,String dspqct,String txtEPIStatus)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		return "OK";
	}
	
	
	@Transactional(readOnly=false)
	public String getJAFCompleteProcess(int jobId,int dspqPortfolioNameId,String dspqct,int groupId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		
		
		boolean isDACustomRedirectionURL=false;
		String sDACustomRedirectionURL="";
		
		boolean isDACustomMsg=false;
		String sDACustomMsg="";
		
		int iMsgType=0;
		
		if(jobOrder!=null)
		{
			if(jobOrder.getFlagForURL()!=null && jobOrder.getFlagForURL()==1 && jobOrder.getExitURL()!=null && !jobOrder.getExitURL().equals(""))
			{
				isDACustomRedirectionURL=true;
				sDACustomRedirectionURL=jobOrder.getExitURL();
				iMsgType=1;
			}
			
			if(jobOrder.getFlagForMessage()!=null && jobOrder.getFlagForMessage()==1 && jobOrder.getExitMessage()!=null && !jobOrder.getExitMessage().equals(""))
			{
				isDACustomMsg=true;
				sDACustomMsg=jobOrder.getExitMessage();
				iMsgType=2;
			}
		}
		
		int iUnDoneCounter=0;
		String sUnDoneShortCode="";
		String sUnDoneMessage="";
		String sUnDoneURL="";
		
		DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
		if(dspqJobWiseStatus!=null)
		{
			int iIsAffilated=0;
			String sDecIsAffilated=validateCTAndDec(dspqct);
			if(sDecIsAffilated!=null && (sDecIsAffilated.equalsIgnoreCase("I") || sDecIsAffilated.equalsIgnoreCase("T")))
				iIsAffilated=1;
			updateJFT(dspqJobWiseStatus,teacherDetail, jobOrder, iIsAffilated);
		}
		
		
		if(dspqJobWiseStatus!=null)
		{
			if(dspqJobWiseStatus.getIsCoverLetterRequired()!=null && dspqJobWiseStatus.getIsCoverLetterRequired())
				if(dspqJobWiseStatus.getCoverLetter()==null || !dspqJobWiseStatus.getCoverLetter())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="CL";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblCoverLetter", locale)+"<BR>";
						sUnDoneURL="managejobapplicationflow.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||CL";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblCoverLetter", locale)+"<BR>";
					}
					
				}
			
			if(dspqJobWiseStatus.getIsPersonaInfoRequired()!=null && dspqJobWiseStatus.getIsPersonaInfoRequired())
				if(dspqJobWiseStatus.getPersonaInfo()==null || !dspqJobWiseStatus.getPersonaInfo())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="PI";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblPersonal", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowpersonal.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||PI";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblPersonal", locale)+"<BR>";
					}
				}
			
			if(dspqJobWiseStatus.getIsAcademicRequired()!=null && dspqJobWiseStatus.getIsAcademicRequired())
				if(dspqJobWiseStatus.getAcademic()==null || !dspqJobWiseStatus.getAcademic())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="AC";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblAcademic", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowacademic.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||AC";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblAcademic", locale)+"<BR>";
					}
				}
			
			if(dspqJobWiseStatus.getIsCredentialsRequired()!=null && dspqJobWiseStatus.getIsCredentialsRequired())
				if(dspqJobWiseStatus.getCredentials()==null || !dspqJobWiseStatus.getCredentials())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="CR";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblCredentials", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowcredentials.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||CR";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblCredentials", locale)+"<BR>";
						
					}
				}
			
			if(dspqJobWiseStatus.getIsProfessionalRequired()!=null && dspqJobWiseStatus.getIsProfessionalRequired())
				if(dspqJobWiseStatus.getProfessional()==null || !dspqJobWiseStatus.getProfessional())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="PR";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblProfessional", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowprofessional.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||PR";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblProfessional", locale)+"<BR>";
					}
				}
			
			if(dspqJobWiseStatus.getIsAffidaviteRequired()!=null && dspqJobWiseStatus.getIsAffidaviteRequired())
				if(dspqJobWiseStatus.getAffidavite()==null || !dspqJobWiseStatus.getAffidavite())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="AF";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblAffidavit", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowAffidavit.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||AF";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblAffidavit", locale)+"<BR>";
					}
				}
			
			if(dspqJobWiseStatus.getIsPreScreenRequired()!=null && dspqJobWiseStatus.getIsPreScreenRequired())
				if(dspqJobWiseStatus.getPreScreen()==null || !dspqJobWiseStatus.getPreScreen())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="QQ";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblPreScreen", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowprescreen.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||QQ";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblPreScreen", locale)+"<BR>";
					}
				}
			
			if(dspqJobWiseStatus.getIsEPIRequired()!=null && dspqJobWiseStatus.getIsEPIRequired())
				if(dspqJobWiseStatus.getEPI()==null || !dspqJobWiseStatus.getEPI())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="EPI";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblEPI", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowepi.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||EPI";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblEPI", locale)+"<BR>";
					}
				}
			
			if(dspqJobWiseStatus.getIsJSIRequired()!=null && dspqJobWiseStatus.getIsJSIRequired())
				if(dspqJobWiseStatus.getJSI()==null || !dspqJobWiseStatus.getJSI())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="JSI";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblQuestions", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowcustomquestion.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||JSI";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblQuestions", locale)+"<BR>";
					}
				}
			
			if(iUnDoneCounter > 0)
			{
				sUnDoneMessage="There are following incomplete section(s):-<BR>"+sUnDoneMessage;
			}
		}
		
		StringBuffer sb=new StringBuffer("");
		sb.append("<input type='hidden' id='frdMsgType' value='"+iMsgType+"'>");
		
		sb.append("<div class='row DSPQRequired12'><div class='col-sm-12 col-md-12 mt10' id='completedErrorDivId'></div></div>");
		
		if(iUnDoneCounter == 0)
		{
			sb.append("<div class='row'><div class='col-sm-12 col-md-12 mt10' id='completedDivId' style='height: 250px;'>");
			
			if(isDACustomRedirectionURL)
			{
				sb.append("<input type='hidden' id='jafDAFarwardURL' value='"+sDACustomRedirectionURL+"'>");
				sb.append(Utility.getLocaleValuePropByKey("jafDAFRedirectionMsg", locale));
			}
			else if(isDACustomMsg)
			{
				sb.append(sDACustomMsg);
			}
			else
			{
				sb.append(Utility.getLocaleValuePropByKey("jafTMStandardMsg", locale));
			}
			sb.append("</div></div>");
		}
		
		
		sb.append("<div class='row'>" +
				"<div class='col-sm-9 col-md-9'></div>" +
				"<div class='col-sm-3 col-md-3'><div class='mt10' style='text-align:right;'><button class='flatbtn' style='width:60%;'  type='button' onclick='validateJAFCompleteProcess();'>"+Utility.getLocaleValuePropByKey("btnContinueDSPQ", locale)+" <i class='icon'></i></button></div></div>" +
				"</div>");
		
		return iUnDoneCounter+"||"+sUnDoneURL+"||"+sUnDoneMessage+"||"+sb.toString();
	}
	
	@Transactional(readOnly=false)
	public DspqPortfolioName getDSPQByJobOrder(JobOrder jobOrder,String sCandidateType)
	{
		
		List<JobCategoryTransaction> lstJobCategoryTrans= new ArrayList<JobCategoryTransaction>();
		JobCategoryTransaction jobCategoryTransaction=null;
		Integer iPortfolioId=null;
		
		if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null && sCandidateType!=null)
		{
			lstJobCategoryTrans=jobCategoryTransactionDAO.getJobCatTransList(jobOrder.getJobCategoryMaster().getJobCategoryId(), sCandidateType);
			if(lstJobCategoryTrans.size()==1)
			{
				jobCategoryTransaction=lstJobCategoryTrans.get(0);
				if(jobCategoryTransaction!=null && jobCategoryTransaction.getPortfolioStatus()!=null && jobCategoryTransaction.getPortfolioStatus().equalsIgnoreCase("A"))
					iPortfolioId=jobCategoryTransaction.getPortfolio();
			}
		}
		
		DspqPortfolioName dspqPortfolioName=null;
		if(iPortfolioId!=null)
		{
			dspqPortfolioName=dspqPortfolioNameDAO.findById(iPortfolioId, false, false);
			printdata("Have Portfolio");
		}
		else
			printdata("No Portfolio");
		return dspqPortfolioName;
	}
	
	@Transactional(readOnly=false)
	public String validateJAFCompleteProcess(int jobId,int dspqPortfolioNameId,String dspqct)
	{
		printdata("validateJAFCompleteProcess");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		
		JobForTeacher jobForTeacher=null;
		List<JobForTeacher> lstJobForTeacher=jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail, jobOrder);
		if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
		{
			jobForTeacher=lstJobForTeacher.get(0);
		}
		String sDecIsAffilated=validateCTAndDec(dspqct);
		
		int iMessageType=0;
		boolean isSendEmail=false;
		int iUnDoneCounter=0;
		String sUnDoneShortCode="";
		String sUnDoneMessage="";
		String sUnDoneURL="";
		DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
		
		if(dspqJobWiseStatus!=null)
		{

			if(dspqJobWiseStatus.getIsCoverLetterRequired()!=null && dspqJobWiseStatus.getIsCoverLetterRequired())
				if(dspqJobWiseStatus.getCoverLetter()==null || !dspqJobWiseStatus.getCoverLetter())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="CL";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblCoverLetter", locale)+"<BR>";
						sUnDoneURL="managejobapplicationflow.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||CL";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblCoverLetter", locale)+"<BR>";
					}
					
				}
			
			if(dspqJobWiseStatus.getIsPersonaInfoRequired()!=null && dspqJobWiseStatus.getIsPersonaInfoRequired())
				if(dspqJobWiseStatus.getPersonaInfo()==null || !dspqJobWiseStatus.getPersonaInfo())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="PI";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblPersonal", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowpersonal.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||PI";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblPersonal", locale)+"<BR>";
					}
				}
			
			if(dspqJobWiseStatus.getIsAcademicRequired()!=null && dspqJobWiseStatus.getIsAcademicRequired())
				if(dspqJobWiseStatus.getAcademic()==null || !dspqJobWiseStatus.getAcademic())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="AC";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblAcademic", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowacademic.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||AC";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblAcademic", locale)+"<BR>";
					}
				}
			
			if(dspqJobWiseStatus.getIsCredentialsRequired()!=null && dspqJobWiseStatus.getIsCredentialsRequired())
				if(dspqJobWiseStatus.getCredentials()==null || !dspqJobWiseStatus.getCredentials())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="CR";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblCredentials", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowcredentials.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||CR";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblCredentials", locale)+"<BR>";
						
					}
				}
			
			if(dspqJobWiseStatus.getIsProfessionalRequired()!=null && dspqJobWiseStatus.getIsProfessionalRequired())
				if(dspqJobWiseStatus.getProfessional()==null || !dspqJobWiseStatus.getProfessional())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="PR";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblProfessional", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowprofessional.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||PR";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblProfessional", locale)+"<BR>";
					}
				}
			
			if(dspqJobWiseStatus.getIsAffidaviteRequired()!=null && dspqJobWiseStatus.getIsAffidaviteRequired())
				if(dspqJobWiseStatus.getAffidavite()==null || !dspqJobWiseStatus.getAffidavite())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="AF";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblAffidavit", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowAffidavit.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||AF";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblAffidavit", locale)+"<BR>";
					}
				}
			
			if(dspqJobWiseStatus.getIsPreScreenRequired()!=null && dspqJobWiseStatus.getIsPreScreenRequired())
				if(dspqJobWiseStatus.getPreScreen()==null || !dspqJobWiseStatus.getPreScreen())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="QQ";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblPreScreen", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowprescreen.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||QQ";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblPreScreen", locale)+"<BR>";
					}
				}
			
			if(dspqJobWiseStatus.getIsEPIRequired()!=null && dspqJobWiseStatus.getIsEPIRequired())
				if(dspqJobWiseStatus.getEPI()==null || !dspqJobWiseStatus.getEPI())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="EPI";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblEPI", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowepi.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||EPI";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblEPI", locale)+"<BR>";
					}
				}
			
			if(dspqJobWiseStatus.getIsJSIRequired()!=null && dspqJobWiseStatus.getIsJSIRequired())
				if(dspqJobWiseStatus.getJSI()==null || !dspqJobWiseStatus.getJSI())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="JSI";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblQuestions", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowcustomquestion.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||JSI";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblQuestions", locale)+"<BR>";
					}
				}
			
			if(iUnDoneCounter==0)
			{
				isSendEmail=true;
				if(dspqJobWiseStatus.getJobComplete()==null || !dspqJobWiseStatus.getJobComplete())
				{
					dspqJobWiseStatus.setJobComplete(true);
					dspqJobWiseStatus.setJobCompleteDate(new Date());
					dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
				}
				
				int iStatusId=setSLCStatusForNewJAF(sDecIsAffilated, teacherDetail, jobOrder, dspqJobWiseStatus);
				if(jobForTeacher.getStatus().getStatusId()!=iStatusId)
				{
					StatusMaster status=new StatusMaster();
					status.setStatusId(iStatusId);
					jobForTeacher.setStatus(status);
					jobForTeacher.setStatusMaster(status);
					if(status!=null){
						jobForTeacher.setApplicationStatus(status.getStatusId());
					}
					jobForTeacherDAO.makePersistent(jobForTeacher);
				}
				else
				{
					printdata("JFT Status and Current Status are same... No need to update JFT status");
				}
			}
			else if(iUnDoneCounter > 0)
			{
				isSendEmail=false;
				iMessageType=1;
				sUnDoneMessage="There are following incomplete section(s):-<BR>"+sUnDoneMessage;
			}
		}
		
		if(isSendEmail)
		{
			
			// ************************ Job apply :: email *********************
			
			List<UserMaster> lstusermaster	=	new ArrayList<UserMaster>();
			
			try{
				StatusMaster statusJobApply = WorkThreadServlet.statusMap.get("apl");
				String statusShortName = statusJobApply.getStatusShortName();
				boolean isSchool=false;
				if(jobOrder.getSelectedSchoolsInDistrict()!=null)
				{
					if(jobOrder.getSelectedSchoolsInDistrict()==1)
					{
						isSchool=true;
					}
				}
				if(jobOrder.getCreatedForEntity()==2 && isSchool==false)
				{
					lstusermaster=userEmailNotificationsDAO.getUserByDistrict(jobOrder.getDistrictMaster(),statusShortName);
				}
				else if(jobOrder.getCreatedForEntity()==2 && isSchool)
				{
					List<SchoolMaster>  lstschoolMaster= new ArrayList<SchoolMaster>();
					if(jobOrder.getSchool()!=null)
					{
						if(jobOrder.getSchool().size()!=0)
						{
							for(SchoolMaster sMaster : jobOrder.getSchool())
							{
								lstschoolMaster.add(sMaster);
							}
						}
					}
					lstusermaster=userEmailNotificationsDAO.getUserByDistrictAndSchoolList(jobOrder.getDistrictMaster(),lstschoolMaster,jobOrder,statusShortName);
				}
				else if(jobOrder.getCreatedForEntity()==3)
				{
					lstusermaster=userEmailNotificationsDAO.getUserByOnlySchool(jobOrder.getSchool().get(0),statusShortName);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			TeacherAssessmentStatus _teacherAssessmentStatus =null;
			StatusMaster _statusMaster =	null;
			
			if(jobForTeacher.getJobId().getJobCategoryMaster().getBaseStatus())
			{
				List<TeacherAssessmentStatus> _teacherAssessmentStatusList = null;
				JobOrder _jOrder = new JobOrder();
				_jOrder.setJobId(0);
				_teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(),_jOrder);
				if(_teacherAssessmentStatusList.size()>0){
					_teacherAssessmentStatus = _teacherAssessmentStatusList.get(0);
					_statusMaster = _teacherAssessmentStatus.getStatusMaster();
				}else{
					_statusMaster = WorkThreadServlet.statusMap.get("icomp");
				}
			}
			
			
			String[] arrHrDetail = commonService.getHrDetailToTeacher(request,jobForTeacher.getJobId());
			try{
				arrHrDetail[10]	=	Utility.getBaseURL(request)+"signin.do";
				arrHrDetail[11] =	Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jobForTeacher.getJobId().getJobId()+"&JobOrderType="+jobForTeacher.getJobId().getCreatedForEntity();
				if(jobForTeacher.getJobId().getDistrictMaster()!=null)
					arrHrDetail[12] =	Utility.getShortURL(Utility.getBaseURL(request)+"jobsboard.do?districtId="+Utility.encryptNo(jobForTeacher.getJobId().getDistrictMaster().getDistrictId()));
				else
					arrHrDetail[12] = "";
				arrHrDetail[13] =	Utility.getValueOfPropByKey("basePath")+"/forgotpassword.do";
				try 
				{
					if(_statusMaster.getStatusShortName()!=null)
						arrHrDetail[14] =	_statusMaster.getStatusShortName();
				} catch (NullPointerException e) 
				{
					//e.printStackTrace();
				}
				String flagforEmail	="applyJob";
				
				MailSendToTeacher mst =new MailSendToTeacher(teacherDetail,jobOrder,request,arrHrDetail,flagforEmail,lstusermaster,emailerService,jobForTeacher);
				Thread currentThread = new Thread(mst);
				currentThread.start(); 
			}catch(Exception e){
				e.printStackTrace();
			}
			
			// ****************************************************************
			
		}
		
		return iMessageType+","+sUnDoneShortCode+","+sUnDoneMessage;
	}
	
	
	public StatusMaster getEPIStatus(TeacherDetail teacherDetail)
	{
		StatusMaster statusMaster=null;
		try
		{
			TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster()!=null)
				statusMaster=teacherBaseAssessmentStatus.getStatusMaster();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return statusMaster;
	}
	
	
	public StatusMaster getCustomQuestionStatus(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		StatusMaster statusMaster=null;
		AssessmentDetail assessmentDetail = null;
		TeacherAssessmentStatus teacherBaseAssessmentStatus=null;
		List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
		if(lstAssessmentJobRelation.size()>0)
		{
			AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
			assessmentDetail = assessmentJobRelation.getAssessmentId();
			List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail,assessmentDetail);
			if(lstTeacherAssessmentStatus!=null && lstTeacherAssessmentStatus.size()==1)
			{
				teacherBaseAssessmentStatus=lstTeacherAssessmentStatus.get(0);
				if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster()!=null)
					statusMaster=teacherBaseAssessmentStatus.getStatusMaster();
			}
		}
		return statusMaster;
	}
	
	@Transactional
	public int setSLCStatusForNewJAF(String candidateType,TeacherDetail teacherDetail,JobOrder jobOrder,DspqJobWiseStatus dspqJobWiseStatus)
	{
		printdata("Calling DSPQServiceAjax => setSLCStatusForNewJAF");
		
		boolean bNextCheck=true;
		StatusMaster statusMaster=null;
		int iStatusId=0;
		StatusMaster statusMasterCQ=null;

		if(dspqJobWiseStatus!=null)
		{
			// EPI
			if(dspqJobWiseStatus.getIsEPIRequired()!=null && dspqJobWiseStatus.getIsEPIRequired())
			{
				statusMaster=getEPIStatus(teacherDetail);
				if(statusMaster==null)
				{
					bNextCheck=true;
					iStatusId=3;
				}
				else if(statusMaster!=null)
				{
					iStatusId=statusMaster.getStatusId();
					if(statusMaster.getStatusShortName().equalsIgnoreCase("comp"))
					{
						bNextCheck=true;
					}
					else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp"))
					{
						bNextCheck=true;
					}
					else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt"))
					{
						bNextCheck=false;
					}
					
				}
				printdata("EPI iStatusId "+iStatusId);
			}
			else
			{
				printdata("EPI is not required");
			}
			
			// Custom Questions (Formally JSI)
			if(bNextCheck)
			{
				if(dspqJobWiseStatus.getIsJSIRequired()!=null && dspqJobWiseStatus.getIsJSIRequired())
				{
					statusMasterCQ=getCustomQuestionStatus(teacherDetail, jobOrder);
					if(statusMasterCQ==null)
					{
						bNextCheck=true;
						iStatusId=3;
					}
					else if(statusMasterCQ!=null)
					{
						if(statusMasterCQ.getStatusShortName().equalsIgnoreCase("comp"))
						{
							if(statusMaster!=null && statusMaster.getStatusShortName().equalsIgnoreCase("comp"))
							{
								iStatusId=4;
								bNextCheck=true;
							}
							else if(statusMaster!=null && statusMaster.getStatusShortName().equalsIgnoreCase("icomp"))
							{
								iStatusId=3;
								bNextCheck=false;
							}
							else
							{
								bNextCheck=true; // will never come here
							}
						}
						else if(statusMasterCQ.getStatusShortName().equalsIgnoreCase("icomp"))
						{
							bNextCheck=false;
							iStatusId=3;
							
						}
						else if(statusMasterCQ.getStatusShortName().equalsIgnoreCase("vlt"))
						{
							bNextCheck=false;
							iStatusId=9;
						}
						
					}
					printdata("JSI iStatusId "+iStatusId);
				}
				else
				{
					printdata("JSI is not required");
				}
			}
			else
			{
				printdata("No need to check JSI");
			}
			
			// Pre Screen (QQ)
			if(bNextCheck)
			{
				if(dspqJobWiseStatus.getIsPreScreenRequired()!=null && dspqJobWiseStatus.getIsPreScreenRequired())
				{
					if(dspqJobWiseStatus.getPreScreen()==null || (dspqJobWiseStatus.getPreScreen()!=null && !dspqJobWiseStatus.getPreScreen()))
					{
						bNextCheck=false;
						if(iStatusId==0 || iStatusId==4)
						{
							iStatusId=3;
						}
						
					}
					printdata("QQ iStatusId "+iStatusId);
				}
				else
				{
					printdata("QQ is not required");
				}
			}
			else
			{
				printdata("No need to check QQ");
			}
			// ************ Portfolio Start **********************
			//Cover Letter
			if(bNextCheck)
			{
				if(dspqJobWiseStatus.getIsCoverLetterRequired()!=null && dspqJobWiseStatus.getIsCoverLetterRequired())
				{
					if(dspqJobWiseStatus.getCoverLetter()==null || (dspqJobWiseStatus.getCoverLetter()!=null && !dspqJobWiseStatus.getCoverLetter()))
					{
						bNextCheck=false;
						if(iStatusId==0 || iStatusId==4)
						{
							iStatusId=3;
						}
					}
					printdata("CL iStatusId "+iStatusId);
				}
				else
				{
					printdata("CL is not required");
				}
			}
			else
			{
				printdata("No need to check CL");
			}
			//Persona Info
			if(bNextCheck)
			{
				if(dspqJobWiseStatus.getIsPersonaInfoRequired()!=null && dspqJobWiseStatus.getIsPersonaInfoRequired())
				{
					if(dspqJobWiseStatus.getPersonaInfo()==null || (dspqJobWiseStatus.getPersonaInfo()!=null && !dspqJobWiseStatus.getPersonaInfo()))
					{
						bNextCheck=false;
						if(iStatusId==0 || iStatusId==4)
						{
							iStatusId=3;
						}
					}
					printdata("PI iStatusId "+iStatusId);
				}
				else
				{
					printdata("PI is not required");
				}
			}
			else
			{
				printdata("No need to check PI");
			}
			//Academic
			if(bNextCheck)
			{
				if(dspqJobWiseStatus.getIsAcademicRequired()!=null && dspqJobWiseStatus.getIsAcademicRequired())
				{
					if(dspqJobWiseStatus.getAcademic()==null || (dspqJobWiseStatus.getAcademic()!=null && !dspqJobWiseStatus.getAcademic()))
					{
						bNextCheck=false;
						if(iStatusId==0 || iStatusId==4)
						{
							iStatusId=3;
						}
					}
					printdata("AC iStatusId "+iStatusId);
				}
				else
				{
					printdata("AC is not required");
				}
			}
			else
			{
				printdata("No need to check AC");
			}
			//Credentials
			if(bNextCheck)
			{
				if(dspqJobWiseStatus.getIsCredentialsRequired()!=null && dspqJobWiseStatus.getIsCredentialsRequired())
				{
					if(dspqJobWiseStatus.getCredentials()==null || (dspqJobWiseStatus.getCredentials()!=null && !dspqJobWiseStatus.getCredentials()))
					{
						bNextCheck=false;
						if(iStatusId==0 || iStatusId==4)
						{
							iStatusId=3;
						}
					}
					printdata("CR iStatusId "+iStatusId);
				}
				else
				{
					printdata("CR is not required");
				}
			}
			else
			{
				printdata("No need to check CR");
			}
			//Professional
			if(bNextCheck)
			{
				if(dspqJobWiseStatus.getIsProfessionalRequired()!=null && dspqJobWiseStatus.getIsProfessionalRequired())
				{
					if(dspqJobWiseStatus.getProfessional()==null || (dspqJobWiseStatus.getProfessional()!=null && !dspqJobWiseStatus.getProfessional()))
					{
						bNextCheck=false;
						if(iStatusId==0 || iStatusId==4)
						{
							iStatusId=3;
						}
					}
					printdata("PR iStatusId "+iStatusId);
				}
				else
				{
					printdata("PR is not required");
				}
			}
			else
			{
				printdata("No need to check PR");
			}
			//Affidavite
			if(bNextCheck)
			{
				if(dspqJobWiseStatus.getIsAffidaviteRequired()!=null && dspqJobWiseStatus.getIsAffidaviteRequired())
				{
					if(dspqJobWiseStatus.getAffidavite()==null || (dspqJobWiseStatus.getAffidavite()!=null && !dspqJobWiseStatus.getAffidavite()))
					{
						bNextCheck=false;
						if(iStatusId==0 || iStatusId==4)
						{
							iStatusId=3;
						}
					}
					printdata("AF iStatusId "+iStatusId);
				}
				else
				{
					printdata("AF is not required");
				}
			}
			else
			{
				printdata("No need to check AF");
			}
			
		}
		
		printdata("************** Status Id ************** "+iStatusId);
		if(iStatusId==0)
		{
			iStatusId=4;
		}
		
		return iStatusId;
		
	}
	
	public String getAddSubSectionText(DspqRouter dspqRouter)
	{
		String sSubSection=dspqRouter.getDisplayName();
		sSubSection=sSubSection.replaceAll("s ", " ");
		sSubSection=sSubSection.replaceAll("s/", "/");
		
		if(sSubSection.endsWith("S") || sSubSection.endsWith("s"))
			sSubSection=sSubSection.substring(0, sSubSection.length()-1);
			
		sSubSection=Utility.getLocaleValuePropByKey("DSPQ_lnkAddPrefix", locale)+" "+sSubSection;
		
		return sSubSection;
	}
	
	public String downloadCLUploadFile(String sJobId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }

		String path="";
		
		try 
		{
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			int iJobId=Utility.getIntValue(sJobId);
			JobOrder jobOrder=jobOrderDAO.findById(iJobId, false, false);
			String sCLFileName="";
			List<JobForTeacher> lstJobForTeacher=jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail, jobOrder);
			if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
			{
				if(lstJobForTeacher.get(0).getCoverLetterFileName()!=null && !lstJobForTeacher.get(0).getCoverLetterFileName().equals(""));
					sCLFileName=lstJobForTeacher.get(0).getCoverLetterFileName();
			}
			
			String source = Utility.getValueOfPropByKey("teacherJobCLRootPath")+""+teacherDetail.getTeacherId()+"_"+sJobId+"/"+sCLFileName;
	        String target = context.getServletContext().getRealPath("/")+"/"+"/jftCLteacher/"+teacherDetail.getTeacherId()+"/";
	        
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        if(sourceFile.exists()){
	        	FileUtils.copyFile(sourceFile, targetFile);
	         path = Utility.getValueOfPropByKey("contextBasePath")+"/jftCLteacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
	        }
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return path;
	
	}
	
	public String[] jafCheckEmployeeDetail(Integer jobId)
	{
		TeacherDetail teacherDetail = null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		String arr[] = new String[3];
		try
		{
			printdata("teacherDetail.getTeacherId(): "+teacherDetail.getTeacherId());
			JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
			
			if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Aspiring Assistant Principal"))
				arr[2]="1";
			else
				arr[2]="0";
			
			printdata("KKKKKKKKKKKKKKKKKKKKKKKKKKKK: "+jobId);
			if(!(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1200390)))
			{
				arr[0]="1";
				arr[1]="A";
				return arr;
			}
			printdata("jobId: "+jobOrder.getJobId());
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
			String firstName = "";
			String lastName = "";
			String ssn = "";
			String employeeCode="";
			if(teacherPersonalInfo!=null)
			{
				ssn=teacherPersonalInfo.getSSN();
				employeeCode= teacherPersonalInfo.getEmployeeNumber();
				firstName = teacherPersonalInfo.getFirstName();
				lastName = teacherPersonalInfo.getLastName();

				if(ssn!=null)
				{
					ssn = ssn.trim();
					ssn = Utility.decodeBase64(ssn);
					if(ssn.length()>4)
						ssn = ssn.substring(ssn.length() - 4, ssn.length());
				}
				
			}
			else
			{
				ssn="";
				employeeCode="";
			}
			printdata("teacherPersonalInfo: "+teacherPersonalInfo);
			List<EmployeeMaster> employeeMasters =	employeeMasterDAO.checkEmployee(ssn, employeeCode,firstName,lastName,jobOrder.getDistrictMaster());

			String FECode=null; //Former Employee
			String RRCode=null; //Part Time
			String OCCode=null; //Full Time
			String PCCode=null; //Do not apply
			String staffType=null; // N - Non-Instructional Internal Employee, I - Instructional Internal Employee
			String jobType=jobOrder.getJobType()==null?"":jobOrder.getJobType().trim();
			printdata("jobType: "+jobOrder.getJobType());
			printdata("employeeMaster: "+employeeMasters.size());
			
			if(employeeMasters.size()>0)
			{
				//printdata("employeeMaster: "+employeeMasters.size());
				EmployeeMaster employeeMaster = employeeMasters.get(0);
				FECode=employeeMaster.getFECode()==null?"":employeeMaster.getFECode().trim();
				RRCode=employeeMaster.getRRCode()==null?"":employeeMaster.getRRCode().trim();
				OCCode=employeeMaster.getOCCode()==null?"":employeeMaster.getOCCode().trim();
				PCCode=employeeMaster.getPCCode()==null?"":employeeMaster.getPCCode().trim();
				staffType=employeeMaster.getStaffType()==null?"":employeeMaster.getStaffType().trim();
				Integer isCurrentFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?0:teacherPersonalInfo.getIsCurrentFullTimeTeacher();
				// printdata("Pccod:::: "+PCCode);
				JobForTeacher jobForTeacher = null;
				jobForTeacher = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
				printdata("jobForTeacher:::: "+jobForTeacher);
				String jobCategoryName =jobOrder.getJobCategoryMaster().getJobCategoryName();
				if(PCCode.equalsIgnoreCase("y"))
				{
					arr[0]="0";
					arr[1]="R";
					if(jobForTeacher!=null)
						jobForTeacherDAO.makeTransient(jobForTeacher);
					if(session.getAttribute("referer")!=null)
						session.removeAttribute("referer");
					return arr;
				}else if(!FECode.equalsIgnoreCase("0"))
				{
					printdata("Current Employee ============================FECode>>>"+FECode);
					if(staffType.equalsIgnoreCase("I") && isCurrentFullTime==1)
					{
						arr[0]="0";
						arr[1]="RD";
						if(jobForTeacher!=null && !jobCategoryName.equalsIgnoreCase("Aspiring Assistant Principal"))
							jobForTeacherDAO.makeTransient(jobForTeacher);
						if(session.getAttribute("referer")!=null && !jobCategoryName.equalsIgnoreCase("Aspiring Assistant Principal"))
							session.removeAttribute("referer");
					}else
					{
						printdata("Current Employee Non Institutional ================isCurrentFullTime:"+isCurrentFullTime);
						/*arr[0]="1";
						arr[1]="A";*/
						/////////For current employee/////////////////////////////////
						if(jobType.equalsIgnoreCase("p") || jobType.equalsIgnoreCase("f"))
						{
							if(RRCode.equals("") && OCCode.equals(""))
							{
								arr[0]="1";
								arr[1]="A";
								return arr;
							}else if(jobType.equalsIgnoreCase("p"))
							{
								if(OCCode.equalsIgnoreCase("y"))
								{
									arr[0]="0";
									arr[1]="P";
									if(jobForTeacher!=null)
										jobForTeacherDAO.makeTransient(jobForTeacher);

									if(session.getAttribute("referer")!=null)
										session.removeAttribute("referer");
								}else
								{
									arr[0]="1";
									arr[1]="A";
								}
								return arr;
							}else if(jobType.equalsIgnoreCase("f"))
							{
								if(OCCode.equalsIgnoreCase("y"))
								{
									arr[0]="1";
									arr[1]="A";
								}
								else
								{
									// RR allowed
									if(RRCode.equalsIgnoreCase("y"))
									{
										arr[0]="1";
										arr[1]="A";
									}else 
									{
										arr[0]="1";
										arr[1]="A";
									}
								}
								return arr;
							}
						}
					   ///For curenent Employee end//////////////////////////////////////////////////////
						
						
					}
					return arr;
				}
				else if(RRCode.equals("") && OCCode.equals(""))
				{
					arr[0]="1";
					arr[1]="A";
					return arr;
				}					 
				else if(jobType.equalsIgnoreCase("p") || jobType.equalsIgnoreCase("f"))
				{

					if(jobType.equalsIgnoreCase("p"))
					{
						if(OCCode.equalsIgnoreCase("y"))
						{
							arr[0]="0";
							arr[1]="P";
							if(jobForTeacher!=null)
								jobForTeacherDAO.makeTransient(jobForTeacher);

							if(session.getAttribute("referer")!=null)
								session.removeAttribute("referer");
						}else
						{
							arr[0]="1";
							arr[1]="A";
						}
						return arr;
					}else if(jobType.equalsIgnoreCase("f"))
					{
						if(OCCode.equalsIgnoreCase("y"))
						{
							arr[0]="1";
							arr[1]="A";
						}
						else
						{
							// RR allowed
							if(RRCode.equalsIgnoreCase("y"))
							{
								arr[0]="1";
								arr[1]="A";
							}else 
							{
								arr[0]="1";
								arr[1]="A";
							}
						}
						return arr;
					}
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}

		return arr;
	}
	
	public String jafNotifyUsers(Integer jobId,String retVal)
	{
		return districtPortfolioConfigAjax.notifyUsers(jobId, retVal);
	}
	
	public String doNotHiredForJeffco(String empDOB,String empSSN)throws Exception
	{
		return districtPortfolioConfigAjax.doNotHiredForJeffco(empDOB, empSSN);
	}
	
	public String getDoNotHireStatusByEmpoloyeeId(String employeeCode, Integer districtId,String empDOB,String empSSN)
	{
		return districtPortfolioConfigAjax.getDoNotHireStatusByEmpoloyeeId(employeeCode, districtId, empDOB, empSSN);
	}
	
	public int[] jafMiamiValidation(Integer districtIdForDSPQ,String jobId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		
		int retArray[] = new int[2];
		retArray[0]=1;
		retArray[1]=1;
		
		TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
		
		boolean onlySSNcheck = false;
		if(districtIdForDSPQ!=null && districtIdForDSPQ.equals(4218990))// Philadaphia Check
			onlySSNcheck=true;
		
		if(districtIdForDSPQ!=null && districtIdForDSPQ.equals(1200390) || onlySSNcheck)// Miami Check
		{
			String ssn = teacherPersonalInfo.getSSN()==null?"":teacherPersonalInfo.getSSN();
			String sCandidateFirstName=teacherPersonalInfo.getFirstName();
			String sCandidateLastName=teacherPersonalInfo.getLastName();
			Integer iEmployeeType=teacherPersonalInfo.getEmployeeType();
			
			if(ssn.equals(""))
			{
				retArray[0]=1;
				retArray[1]=1;
				
			}
			else
			{
				if(!ssn.equals(""))
				{
					ssn = ssn.trim();
					try { ssn = Utility.decodeBase64(ssn); 	} catch (IOException e) {	e.printStackTrace(); }
					
					if(ssn.length()>4 && districtIdForDSPQ.equals(1200390))
						ssn = ssn.substring(ssn.length() - 4, ssn.length());
				}
				
				List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
				
				DistrictMaster dm = new DistrictMaster();
				dm.setDistrictId(districtIdForDSPQ);
				if(onlySSNcheck)//Philedaphia
				{
					employeeMasters =	employeeMasterDAO.checkEmployeeByFullSSN(ssn,dm);
					
				}
				else
				{
					//Miami Only
					if(iEmployeeType!=null && iEmployeeType.equals(2))// Not an employee
					{
						try {
							employeeMasters =	employeeMasterDAO.checkEmployeeBySSN(ssn,sCandidateFirstName,sCandidateLastName,dm);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					else 
					{	// Miami requested to remove empNo check
						try {
							employeeMasters =	employeeMasterDAO.checkEmployeeBySSN(ssn,sCandidateFirstName,sCandidateLastName,dm);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				
				if(employeeMasters.size()>0 && onlySSNcheck)
				{
					EmployeeMaster employeeMaster = employeeMasters.get(0);
					String PCCode=employeeMaster.getPCCode()==null?"":employeeMaster.getPCCode().trim();
					if(PCCode.equalsIgnoreCase("y"))
					{
							retArray[0]=0;
							retArray[1]=3;// restricted
					}

				}
				else if(districtIdForDSPQ.equals(1200390)) //Miami
				{
					JobOrder jobOrder1= null;
					String jobCategoryName = "";
					if(jobId!=null && !jobId.equalsIgnoreCase(""))
					{
						
						int iJobId=0;
						try { iJobId=Utility.getIntValue(jobId); } catch (Exception e) {}
						try { jobOrder1=jobOrderDAO.findById(iJobId, false, false); } catch (Exception e) {}
						if(jobOrder1!=null)
						jobCategoryName = jobOrder1.getJobCategoryMaster().getJobCategoryName();
					}
					
					if(employeeMasters.size()>0)
					{
						EmployeeMaster employeeMaster = employeeMasters.get(0);

						Date grugTestFailDate = employeeMaster.getDrugTestFailDate();
						
						boolean isJobNotAllowed = false;
						if(grugTestFailDate!=null)
						{
							DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForDSPQ, false, false);
							if(districtMaster.getNoOfDaysRestricted()!=null && districtMaster.getNoOfDaysRestricted()!=0)
							{
								Calendar c = Calendar.getInstance();
								c.setTime(grugTestFailDate);
								c.add(Calendar.DATE, districtMaster.getNoOfDaysRestricted());
								Date restrictedDate = c.getTime();
								Date currTime = Utility.getDateWithoutTime();
								isJobNotAllowed = Utility.between(currTime,grugTestFailDate, restrictedDate);
							}
						}

						String FECode=employeeMaster.getFECode()==null?"":employeeMaster.getFECode().trim();
						if(FECode.equalsIgnoreCase("3"))
							FECode="1";

						if(isJobNotAllowed) //drug failed
						{
							retArray[0]=0;
							retArray[1]=2;// restricted
						}
						else if(iEmployeeType!=null && iEmployeeType.equals(1))// current employee
						{
							String isFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?"N":((teacherPersonalInfo.getIsCurrentFullTimeTeacher()==1 || teacherPersonalInfo.getIsCurrentFullTimeTeacher()==2)?"I":"N");
							String staffType=employeeMaster.getStaffType()==null?"":employeeMaster.getStaffType().trim();
							
							if(iEmployeeType!=null && iEmployeeType.toString().equals(FECode) && isFullTime.equalsIgnoreCase(staffType))
							{
								//printdata("------------------------equal-----------1");
							}
							else
							{
								retArray[0]=0;
								retArray[1]=1;
							}

						}
						else if(iEmployeeType!=null && (iEmployeeType.equals(0) || iEmployeeType.equals(2)))
						{
							if(iEmployeeType.toString().equals(FECode))
							{
								printdata("---FE--------------------equal-----------2");
							}
							else
							{
								retArray[0]=0;
								retArray[1]=1;
							}
						}
					}
					else
					{
						if(iEmployeeType!=null && iEmployeeType.equals(1))
						{
							String isFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?"N":((teacherPersonalInfo.getIsCurrentFullTimeTeacher()==1 || teacherPersonalInfo.getIsCurrentFullTimeTeacher()==2)?"I":"N");
							Integer isCurrentFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?0:teacherPersonalInfo.getIsCurrentFullTimeTeacher();
							if(isFullTime.equalsIgnoreCase("I") && isCurrentFullTime==1)
							{
								if(!jobCategoryName.equals("Aspiring Assistant Principal")) // new change
								{
									retArray[0]=0;
									retArray[1]=0;
								}
							}
						}
					}
				}
			}
		}
		
		return retArray;
	}

	
	public String jeffcoGetEmployeeNumberInfo(String empId)
	{
		StringBuffer sb=new StringBuffer();
		try {
			sb.append(JeffcoAPIController.getEmployeeInfoByEmployeeNumber(empId));
		} catch (Exception e) {
			e.printStackTrace();
			sb.append("1####Information could not be found. Please try again.");
		}
		return sb.toString();
	}
	
	public int getNumericDegreeType(String sDegreeType)
	{
		int iDegreeType=0;
		if(sDegreeType!=null && !sDegreeType.equals(""))
		{
			if(sDegreeType.equalsIgnoreCase("H"))
				iDegreeType=1;
			else if(sDegreeType.equalsIgnoreCase("A"))
				iDegreeType=2;
			else if(sDegreeType.equalsIgnoreCase("B"))
				iDegreeType=3;
			else if(sDegreeType.equalsIgnoreCase("M"))
				iDegreeType=4;
			else if(sDegreeType.equalsIgnoreCase("D"))
				iDegreeType=5;
		}
		
		return iDegreeType;
	}
	
	public String getDegreeTypeNameByNumericCode(int iDegreeType)
	{
		/*sDegreeType="H";
		sDegreeTypeName="High School";

		sDegreeType="A";
		sDegreeTypeName="Associate Degree";

		sDegreeType="B";
		sDegreeTypeName="Bachelor Degree";

		sDegreeType="M";
		sDegreeTypeName="Master Degree";

		sDegreeType="D";
		sDegreeTypeName="Phd";*/
		
		String sDegreeType="";
		
		if(iDegreeType==1)
			sDegreeType="High School";
		else if(iDegreeType==2)
			sDegreeType="Associate Degree";
		if(iDegreeType==3)
			sDegreeType="Bachelor Degree";
		if(iDegreeType==4)
			sDegreeType="Master Degree";
		if(iDegreeType==5)
			sDegreeType="Phd";
		
		return sDegreeType;
	}
	
	public void printdata(String sInputValue)
	{
		if(sInputValue!=null && !sInputValue.equals(""))
			System.out.println(new Date()+" => "+sInputValue);
	}
	
	public String openEmployeeNumberInfo(String empId)
	{
		System.out.println("empId "+empId);
		StringBuffer sb=new StringBuffer();
		try 
		{
			sb.append(JeffcoAPIController.getEmployeeInfoByEmployeeNumber(empId));
		} catch (Exception e) {
			e.printStackTrace();
			sb.append("1####Information could not be found. Please try again.");
		}
		return sb.toString();
	}
	
	//************** Quick Job Apply process ***********************
	@Transactional(readOnly=false)
	public String oneBtnJobApply(int jobId,int dspqPortfolioNameId,String dspqct)
	{	
		printdata("********************** Start oneBtnJobApply ****************************");
		printdata("Calling DSPQServiceAjax => oneBtnJobApply");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		
		int iUnDoneCounter=0;
		String sUnDoneShortCode="";
		String sUnDoneMessage="";
		String sUnDoneURL="";
		
		String sDecIsAffilated=validateCTAndDec(dspqct);
		
		DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
		if(dspqJobWiseStatus!=null)
		{
			int iIsAffilated=0;
			if(sDecIsAffilated!=null && (sDecIsAffilated.equalsIgnoreCase("I") || sDecIsAffilated.equalsIgnoreCase("T")))
				iIsAffilated=1;
			updateJFT(dspqJobWiseStatus,teacherDetail, jobOrder, iIsAffilated);
		}
		
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqGroupMaster> dspqGroupMasterList=dspqGroupMasterDAO.getActiveDspqGroupMaster();
		List<DspqSectionMaster> sectionMasters=dspqSectionMasterDAO.getDspqSectionListByGroupList(dspqGroupMasterList);
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sDecIsAffilated, dspqFieldList,sectionMasters);
		
		TeacherPersonalInfo teacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
		TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
		
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		Map<Integer, Map<Integer, DspqRouter>> mapRouterBySection=new HashMap<Integer, Map<Integer, DspqRouter>>();
		
		setSIDAndFIDInMapQJA(mapDspqRouterBySID,mapRouterBySection, dspqRouterList);
		
		System.out.println("************************************************");
		System.out.println("mapDspqRouterBySID "+mapDspqRouterBySID.size() +" mapRouterBySection "+mapRouterBySection.size());
		System.out.println("************************************************");
		
		// Start ... Custom Field one time call
		Map<Integer, DistrictSpecificPortfolioAnswers> mapCustomQuestionAnswers=new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
		Map<Integer, List<DistrictSpecificPortfolioQuestions>> mapCustomFieldlstBySectionId=new TreeMap<Integer, List<DistrictSpecificPortfolioQuestions>>();
		getCustomFieldBySectionId_QuickJobApplyL1(dspqPortfolioName.getDistrictMaster(), sDecIsAffilated, dspqPortfolioName, teacherDetail, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
		// End ... Custom Field one time call
		
		if(dspqJobWiseStatus!=null)
		{
			if(dspqJobWiseStatus.getIsCoverLetterRequired()!=null && dspqJobWiseStatus.getIsCoverLetterRequired())
			{
				
				if(dspqJobWiseStatus.getCoverLetter()==null || !dspqJobWiseStatus.getCoverLetter())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="CL";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblCoverLetter", locale)+"<BR>";
						sUnDoneURL="managejobapplicationflow.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||CL";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblCoverLetter", locale)+"<BR>";
					}
					
				}
			}
				
			int iSectionId=0;
			Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
			if(dspqJobWiseStatus.getIsPersonaInfoRequired()!=null && dspqJobWiseStatus.getIsPersonaInfoRequired())
			{
				//Start ... ************** New Process *********************
				boolean bPersonaInfo_Incomplete=false;
				if(dspqJobWiseStatus.getPersonaInfo()==null || !dspqJobWiseStatus.getPersonaInfo())
				{
					iSectionId=2;// Personal Information
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bPersonaInfo_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("PI mapDspqRouterByFID "+mapDspqRouterByFID.size());
							
							if(mapDspqRouterByFID.get(5)!=null && !bPersonaInfo_Incomplete)//Salutation
							{
								
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getSalutation()!=null && teacherPersonalInfo.getSalutation()>0)
								{
									
								}
								else
								{
									quickPrintData("Salutation");
									bPersonaInfo_Incomplete=true;
								}
								
							}
							
							if(mapDspqRouterByFID.get(6)!=null && !bPersonaInfo_Incomplete)//First Name
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getFirstName()!=null && !teacherPersonalInfo.getFirstName().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("FirstName");
									bPersonaInfo_Incomplete=true;
								}
								
							}
							
							if(mapDspqRouterByFID.get(7)!=null && !bPersonaInfo_Incomplete)//Last Name
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getLastName()!=null && !teacherPersonalInfo.getLastName().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("LastName");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(79)!=null && !bPersonaInfo_Incomplete)//Middle Name
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getMiddleName()!=null && !teacherPersonalInfo.getMiddleName().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("MiddleName");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							
							if(mapDspqRouterByFID.get(80)!=null && !bPersonaInfo_Incomplete)//Another Name
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getAnotherName()!=null && !teacherPersonalInfo.getAnotherName().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("AnotherName");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(18)!=null && !bPersonaInfo_Incomplete)//Mobile Phone Number
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getMobileNumber()!=null && !teacherPersonalInfo.getMobileNumber().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("MobileNumber");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(81)!=null && !bPersonaInfo_Incomplete)//Social Security Number
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getSSN()!=null && !teacherPersonalInfo.getSSN().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("SSN");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(96)!=null && !bPersonaInfo_Incomplete)//Phone
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getPhoneNumber()!=null && !teacherPersonalInfo.getPhoneNumber().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("PhoneNumber");
									bPersonaInfo_Incomplete=true;
								}
							}
						}
					}
					
					
					iSectionId=16;// Date of Birth (DOB)
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bPersonaInfo_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("Date of Birth (DOB) mapDspqRouterByFID "+mapDspqRouterByFID.size());
							if(mapDspqRouterByFID.get(84)!=null && mapDspqRouterByFID.get(85)!=null && mapDspqRouterByFID.get(86)!=null && !bPersonaInfo_Incomplete)//DOB
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getDob()!=null)
								{
									
								}
								else
								{
									quickPrintData("Dob");
									bPersonaInfo_Incomplete=true;
								}
							}
						}
					}
					
					iSectionId=4;// Permanent Address
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bPersonaInfo_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("Permanent Address mapDspqRouterByFID "+mapDspqRouterByFID.size());
							if(mapDspqRouterByFID.get(12)!=null && !bPersonaInfo_Incomplete)//Address Line 1
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getAddressLine1()!=null && !teacherPersonalInfo.getAddressLine1().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("AddressLine1");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(13)!=null && !bPersonaInfo_Incomplete)//Address Line 2
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getAddressLine2()!=null && !teacherPersonalInfo.getAddressLine2().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("AddressLine2");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(14)!=null && !bPersonaInfo_Incomplete)//Country
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getCountryId()!=null && teacherPersonalInfo.getCountryId().getCountryId()>0)
								{
									
								}
								else
								{
									quickPrintData("Country");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(15)!=null && !bPersonaInfo_Incomplete)//Zip Code
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getZipCode()!=null && !teacherPersonalInfo.getZipCode().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("Zip Code");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(16)!=null && !bPersonaInfo_Incomplete)//State
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getStateId()!=null && teacherPersonalInfo.getStateId().getStateId()>0)
								{
									
								}
								else
								{
									quickPrintData("State");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(17)!=null && !bPersonaInfo_Incomplete)//City
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getCityId()!=null && teacherPersonalInfo.getCityId().getCityId()>0 )
								{
									
								}
								else
								{
									quickPrintData("City");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(82)!=null && !bPersonaInfo_Incomplete)//Other City
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getOtherCity()!=null && !teacherPersonalInfo.getOtherCity().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("Other City");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(83)!=null && !bPersonaInfo_Incomplete)//Other State
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getOtherState()!=null && !teacherPersonalInfo.getOtherState().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("Other State");
									bPersonaInfo_Incomplete=true;
								}
							}
						}
					}
					
					
					iSectionId=17;// Present Address
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bPersonaInfo_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("Present Address mapDspqRouterByFID "+mapDspqRouterByFID.size());
							if(mapDspqRouterByFID.get(87)!=null && !bPersonaInfo_Incomplete)//Address Line 1
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getPresentAddressLine1()!=null && !teacherPersonalInfo.getPresentAddressLine1().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("PresentAddressLine1");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(88)!=null && !bPersonaInfo_Incomplete)//Address Line 2
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getPresentAddressLine2()!=null && !teacherPersonalInfo.getPresentAddressLine2().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("PresentAddressLine2");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(89)!=null && !bPersonaInfo_Incomplete)//Country
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getPresentCountryId()!=null && teacherPersonalInfo.getPresentCountryId().getCountryId()>0)
								{
									
								}
								else
								{
									quickPrintData("PresentCountry");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(90)!=null && !bPersonaInfo_Incomplete)//Zip Code
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getPresentZipCode()!=null && !teacherPersonalInfo.getPresentZipCode().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("PresentZip Code");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(91)!=null && !bPersonaInfo_Incomplete)//State
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getPresentStateId()!=null && teacherPersonalInfo.getPresentStateId().getStateId()>0)
								{
									
								}
								else
								{
									quickPrintData("PresentState");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(92)!=null && !bPersonaInfo_Incomplete)//City
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getPresentCityId()!=null && teacherPersonalInfo.getPresentCityId().getCityId()>0 )
								{
									
								}
								else
								{
									quickPrintData("PresentCity");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(93)!=null && !bPersonaInfo_Incomplete)//Other City
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getPersentOtherCity()!=null && !teacherPersonalInfo.getPersentOtherCity().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("getPersentOtherCity");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(94)!=null && !bPersonaInfo_Incomplete)//Other State
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getPersentOtherState()!=null && !teacherPersonalInfo.getPersentOtherState().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("PersentOtherState");
									bPersonaInfo_Incomplete=true;
								}
							}
						}
					}
					
					
					iSectionId=3;// EEOC
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bPersonaInfo_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("EEOC mapDspqRouterByFID "+mapDspqRouterByFID.size());
							if(mapDspqRouterByFID.get(8)!=null && !bPersonaInfo_Incomplete)//Ethnic Origin
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getEthnicOriginId()!=null && teacherPersonalInfo.getEthnicOriginId().getEthnicOriginId()>0)
								{
									
								}
								else
								{
									quickPrintData("Ethnic Origin");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(9)!=null && !bPersonaInfo_Incomplete)//Ethnicity
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getEthnicityId()!=null && teacherPersonalInfo.getEthnicityId().getEthnicityId()>0)
								{
									
								}
								else
								{
									quickPrintData("Ethnicity");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(10)!=null && !bPersonaInfo_Incomplete)//Race/Ethnicity
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getRaceId()!=null && !teacherPersonalInfo.getRaceId().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("Race/Ethnicity");
									bPersonaInfo_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(11)!=null && !bPersonaInfo_Incomplete)//Gender
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getGenderId()!=null && teacherPersonalInfo.getGenderId().getGenderId() > 0)
								{
									
								}
								else
								{
									quickPrintData("Gender");
									bPersonaInfo_Incomplete=true;
								}
							}
						}
						
						
					}
					
					iSectionId=20;// Veteran Check
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bPersonaInfo_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("Veteran Check mapDspqRouterByFID "+mapDspqRouterByFID.size());
							if(mapDspqRouterByFID.get(97)!=null && !bPersonaInfo_Incomplete)
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getIsVateran()!=null)
								{
									
								}
								else
								{
									quickPrintData("Veteran Check");
									bPersonaInfo_Incomplete=true;
								}
							}
						}
					}
					
					iSectionId=22;// Drivers License
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bPersonaInfo_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("Drivers License mapDspqRouterByFID "+mapDspqRouterByFID.size());
							if(mapDspqRouterByFID.get(99)!=null && !bPersonaInfo_Incomplete)//Driving Licence State
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getDrivingLicState()!=null && !teacherPersonalInfo.getDrivingLicState().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("Driving Licence State");
									bPersonaInfo_Incomplete=true;
								}
							}
							if(mapDspqRouterByFID.get(100)!=null && !bPersonaInfo_Incomplete)//Driving Licence Number
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getDrivingLicNum()!=null && !teacherPersonalInfo.getDrivingLicNum().trim().equals(""))
								{
									
								}
								else
								{
									quickPrintData("Driving Licence Number");
									bPersonaInfo_Incomplete=true;
								}
							}
						}
					}
					
					iSectionId=18;// Languages (Spoken)
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bPersonaInfo_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bPersonaInfo_Incomplete)
					{
						DspqRouter dspqRouter=mapDspqRouterBySID.get(iSectionId);
						if(dspqRouter!=null && dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired())
						{
							if(teacherExperience!=null && teacherExperience.getKnowOtherLanguages()!=null && teacherExperience.getKnowOtherLanguages())
							{
								
							}
						}
					}
					
					
					
					//------------------------
					
					if(!bPersonaInfo_Incomplete)
					{
						dspqJobWiseStatus.setPersonaInfo(true);
						dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
					}
					
					
				}
				// ENd ... ************** New Process *********************
				
				if(dspqJobWiseStatus.getPersonaInfo()==null || !dspqJobWiseStatus.getPersonaInfo())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="PI";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblPersonal", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowpersonal.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||PI";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblPersonal", locale)+"<BR>";
					}
				}
			}
				
			
			if(dspqJobWiseStatus.getIsAcademicRequired()!=null && dspqJobWiseStatus.getIsAcademicRequired())
			{
				//Start ... ************** New Process *********************
				boolean bAcademics_Incomplete=false;
				int iDegreeType=0;
				
				if(dspqJobWiseStatus.getAcademic()==null || !dspqJobWiseStatus.getAcademic())
				{
					iSectionId=6; //Academic
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bAcademics_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bAcademics_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bAcademics_Incomplete)
					{
						DspqRouter dspqRouter=mapDspqRouterBySID.get(iSectionId);
						
						List<TeacherAcademics> lstTeacherAcademics=new ArrayList<TeacherAcademics>();
						lstTeacherAcademics=teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
						
						if(dspqRouter!=null && dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired() && !bAcademics_Incomplete)// Count check
						{
							if(lstTeacherAcademics==null || lstTeacherAcademics.size()==0)
							{
								quickPrintData("Academic List");
								bAcademics_Incomplete=true;	
							}
						}
						
						if(dspqRouter!=null && dspqRouter.getNumberRequired()!=null && dspqRouter.getNumberRequired()>0 && !bAcademics_Incomplete)//Required Degree Type
						{
							boolean bMatchDegreeType=false;
							iDegreeType=dspqRouter.getNumberRequired();
							if(iDegreeType > 0)
							{
								//1- High School(H), 2- Associate Degree(H), 3-Bachelor Degree(H), 4-Master Degree(H), 5 -Phd(H) 
								
								String sDegreeType="";
								String sDegreeTypeName="";
								
								if(iDegreeType==1)
								{
									sDegreeType="H";
									sDegreeTypeName="High School";
								}
								else if(iDegreeType==2)
								{
									sDegreeType="A";
									sDegreeTypeName="Associate Degree";
								}
								else if(iDegreeType==3)
								{
									sDegreeType="B";
									sDegreeTypeName="Bachelor Degree";
								}
								else if(iDegreeType==4)
								{
									sDegreeType="M";
									sDegreeTypeName="Master Degree";
								}
								else if(iDegreeType==5)
								{
									sDegreeType="D";
									sDegreeTypeName="Phd";
								}
								
								if(lstTeacherAcademics==null || lstTeacherAcademics.size()==0)
								{
									bMatchDegreeType=true;
								}
								else if(!bMatchDegreeType)
								{
									for(TeacherAcademics academics :lstTeacherAcademics)
									{
										if(academics!=null && academics.getDegreeId()!=null && academics.getDegreeId().getDegreeType()!=null && academics.getDegreeId().getDegreeType().equalsIgnoreCase(sDegreeType) && !bMatchDegreeType)
										{
											bMatchDegreeType=true;
											break;
										}
									}
								}
								
								if(!bMatchDegreeType)
								{
									quickPrintData("Academic Required Degree Type");
									bAcademics_Incomplete=true;	
								}
							}
						}
						
						if(dspqRouter!=null && dspqRouter.getOthersAttribute()!=null && !dspqRouter.getOthersAttribute().trim().equals("") && !bAcademics_Incomplete)// Do you want Degree hierarchy
						{
							String sDegreeHierarchy=dspqRouter.getOthersAttribute();
							boolean bNoDegreeHierarchy=false;
							
							// ### 3 Degree Hierarchy ###
							if(sDegreeHierarchy.equals("1"))
							{
								TreeMap<Integer,Boolean> tmDegreeHierarchy = new TreeMap<Integer, Boolean>();
								
								for(TeacherAcademics academics :lstTeacherAcademics)
								{
									if(academics!=null && academics.getDegreeId()!=null && academics.getDegreeId().getDegreeType()!=null)
									{
										 int iDegreeTypeTemp=getNumericDegreeType(academics.getDegreeId().getDegreeType());
										 tmDegreeHierarchy.put(iDegreeTypeTemp, true);
									}
								}
								
								if(tmDegreeHierarchy==null || tmDegreeHierarchy.size()==0)
								{
									bNoDegreeHierarchy=true;
								}
								else
								{
									List<Integer> lstMissingDegreeType=new ArrayList<Integer>();
									
									int iMaxDegreeType=0;
									
									int iCandidateProvicedMaxDegreeType=tmDegreeHierarchy.lastKey();
									
									if(iDegreeType >0 && iDegreeType > iCandidateProvicedMaxDegreeType)
										iMaxDegreeType=iDegreeType;
									else
										iMaxDegreeType=iCandidateProvicedMaxDegreeType;
									
									printdata("iCandidateProvicedMaxDegreeType "+iCandidateProvicedMaxDegreeType +" iDegreeType "+iDegreeType +" == > iMaxDegreeType "+iMaxDegreeType);
									
									if(iMaxDegreeType==5)
									{
										iMaxDegreeType=4;
									}
									
									for(int i=1;i <=iMaxDegreeType; i++)
									{
										Boolean bTempDegreeHierarchy=tmDegreeHierarchy.get(i);
										if(bTempDegreeHierarchy==null)
											lstMissingDegreeType.add(i);
									}
									
									String sTempError="";
									if(lstMissingDegreeType!=null && lstMissingDegreeType.size() >0)
									{
										quickPrintData("Academic ... Do you want Degree hierarchy");
										bAcademics_Incomplete=true;
									}
								}
							}
						}
					}
					
					//--------------------------------------
					if(!bAcademics_Incomplete)
					{
						dspqJobWiseStatus.setAcademic(true);
						dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
					}
					
				}
				// End ... ************** New Process *********************
				
				if(dspqJobWiseStatus.getAcademic()==null || !dspqJobWiseStatus.getAcademic())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="AC";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblAcademic", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowacademic.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||AC";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblAcademic", locale)+"<BR>";
					}
				}
			}
				
			
			if(dspqJobWiseStatus.getIsCredentialsRequired()!=null && dspqJobWiseStatus.getIsCredentialsRequired())
			{
				// Start ... ************** New Process *********************
				if(dspqJobWiseStatus.getCredentials()==null || !dspqJobWiseStatus.getCredentials())
				{
					// 8-Certifications/Licensure, 9-References, 10-Video Links, 11-Additional Documents (All Grid) 
					// 21-Substitute Teacher,23-Teach For America,29-Certified Teacher,7-Credentials (Open Sections)
					
					boolean bCredentials_Incomplete=false;
					
					iSectionId=8; //Certifications/Licensure
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bCredentials_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						DspqRouter dspqRouter=mapDspqRouterBySID.get(iSectionId);
						if(dspqRouter!=null && dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired() && !bCredentials_Incomplete)// Count check
						{
							List<Integer> teacherCertificates=teacherCertificateDAO.getCertificateCountByTeacherJAF(teacherDetail);
							if(teacherCertificates!=null && teacherCertificates.size()>0)
							{
								System.out.println("teacherCertificates(0) "+teacherCertificates.get(0));
								if(teacherCertificates.get(0)==0)
								{
									quickPrintData("Certifications/Licensure Grid");
									bCredentials_Incomplete=true;
								}
							}
						}
					}
					
					iSectionId=9; //References
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bCredentials_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						DspqRouter dspqRouter=mapDspqRouterBySID.get(iSectionId);
						if(dspqRouter!=null && dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired() && !bCredentials_Incomplete)// Count check
						{
							List<Integer> teacherReferences=teacherElectronicReferencesDAO.getReferencesCountByTeacherJAF(teacherDetail);
							if(teacherReferences!=null && teacherReferences.size()>0)
							{
								System.out.println("teacherReferences(0) "+teacherReferences.get(0));
								if(teacherReferences.get(0)==0)
								{
									quickPrintData("teacherReferences Grid");
									bCredentials_Incomplete=true;
								}
							}
						}
					}
					
					
					iSectionId=10; //Video Links
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bCredentials_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						DspqRouter dspqRouter=mapDspqRouterBySID.get(iSectionId);
						if(dspqRouter!=null && dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired() && !bCredentials_Incomplete)// Count check
						{
							List<Integer> teacherVideoLink=teacherVideoLinksDAO.getTeacherVideoLinkCountByTeacherJAF(teacherDetail);
							if(teacherVideoLink!=null && teacherVideoLink.size()>0)
							{
								System.out.println("teacherVideoLink(0) "+teacherVideoLink.get(0));
								if(teacherVideoLink.get(0)==0)
								{
									quickPrintData("VideoLinks Grid");
									bCredentials_Incomplete=true;
								}
							}
						}
					}
					
					iSectionId=11; //Additional Documents
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bCredentials_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						DspqRouter dspqRouter=mapDspqRouterBySID.get(iSectionId);
						if(dspqRouter!=null && dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired() && !bCredentials_Incomplete)// Count check
						{
							List<Integer> teacherAdditionalDocuments=teacherAdditionalDocumentsDAO.getTeacherAdditionalDocumentsCountByTeacherJAF(teacherDetail);
							if(teacherAdditionalDocuments!=null && teacherAdditionalDocuments.size()>0)
							{
								System.out.println("teacherAdditionalDocuments(0) "+teacherAdditionalDocuments.get(0));
								if(teacherAdditionalDocuments.get(0)==0)
								{
									quickPrintData("teacherAdditionalDocuments Grid");
									bCredentials_Incomplete=true;
								}
							}
						}
					}
					
					// 21-Substitute Teacher,23-Teach For America,29-Certified Teacher,7-Credentials (Open Sections)
					
					iSectionId=21; //Substitute Teacher
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bCredentials_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("Substitute Teacher mapDspqRouterByFID "+mapDspqRouterByFID.size());
							if(mapDspqRouterByFID.get(98)!=null && !bCredentials_Incomplete)
							{
								if(teacherExperience!=null && teacherExperience.getCanServeAsSubTeacher()!=null)
								{
									
								}
								else
								{
									quickPrintData("Substitute Teacher");
									bCredentials_Incomplete=true;
								}
							}
						}
					}

					iSectionId=23; //Teach For America
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bCredentials_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("Teach For America mapDspqRouterByFID "+mapDspqRouterByFID.size());
							
							if(mapDspqRouterByFID.get(101)!=null && !bCredentials_Incomplete)
							{
								if(teacherExperience!=null && teacherExperience.getTfaAffiliateMaster()!=null && teacherExperience.getTfaAffiliateMaster().getTfaAffiliateId()!=null)
								{
									if(teacherExperience.getTfaAffiliateMaster().getTfaAffiliateId()==1 || teacherExperience.getTfaAffiliateMaster().getTfaAffiliateId()==2)
									{
										if(mapDspqRouterByFID.get(102)!=null && !bCredentials_Incomplete)
										{
											if(teacherExperience!=null && teacherExperience.getCorpsYear()!=null)
											{
												
											}
											else
											{
												quickPrintData("CorpsYear");
												bCredentials_Incomplete=true;
											}
										}
										
										if(mapDspqRouterByFID.get(103)!=null && !bCredentials_Incomplete)
										{
											if(teacherExperience!=null && teacherExperience.getTfaRegionMaster()!=null && teacherExperience.getTfaRegionMaster().getTfaRegionId()!=null)
											{
												
											}
											else
											{
												quickPrintData("TfaRegionMaster");
												bCredentials_Incomplete=true;
											}
										}
										
									}
								}
								else
								{
									quickPrintData("TFA");
									bCredentials_Incomplete=true;
								}
							}
						}
					}
					
					
					iSectionId=29; //Certified Teacher
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bCredentials_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("Certified Teacher mapDspqRouterByFID "+mapDspqRouterByFID.size());
							
							if(mapDspqRouterByFID.get(142)!=null && !bCredentials_Incomplete)
							{
								if(teacherExperience!=null && teacherExperience.getIsNonTeacher()!=null)
								{
									if(teacherExperience.getIsNonTeacher())
									{
										if(mapDspqRouterByFID.get(143)!=null && !bCredentials_Incomplete)
										{
											if(teacherExperience!=null && teacherExperience.getExpCertTeacherTraining()!=null)
											{
												
											}
											else
											{
												quickPrintData("Years of Certified Teaching Experience");
												bCredentials_Incomplete=true;
											}
										}
										
									}
								}
								else
								{
									quickPrintData("Are you a certified teacher");
									bCredentials_Incomplete=true;
								}
							}
						}
					}
					
					
					iSectionId=7; //Credentials
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bCredentials_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null && !bCredentials_Incomplete)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("Credentials mapDspqRouterByFID "+mapDspqRouterByFID.size());
							
							if(mapDspqRouterByFID.get(31)!=null && !bCredentials_Incomplete)
							{
								if(teacherExperience!=null && teacherExperience.getNationalBoardCert()!=null)
								{
									if(teacherExperience.getNationalBoardCert())
									{
										if(mapDspqRouterByFID.get(30)!=null && !bCredentials_Incomplete)
										{
											if(teacherExperience!=null && teacherExperience.getNationalBoardCertYear()!=null)
											{
												
											}
											else
											{
												quickPrintData("NationalBoardCertYear");
												bCredentials_Incomplete=true;
											}
										}
										
									}
								}
								else
								{
									quickPrintData("NationalBoardCert");
									bCredentials_Incomplete=true;
								}
							}
						}
					}
					
					
					
					//--------------------------------------
					if(!bCredentials_Incomplete)
					{
						dspqJobWiseStatus.setCredentials(true);
						dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
					}
					
				}
				// End ... ************** New Process *********************
				
				if(dspqJobWiseStatus.getCredentials()==null || !dspqJobWiseStatus.getCredentials())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="CR";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblCredentials", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowcredentials.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||CR";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblCredentials", locale)+"<BR>";
						
					}
				}
			}
				
			
			if(dspqJobWiseStatus.getIsProfessionalRequired()!=null && dspqJobWiseStatus.getIsProfessionalRequired())
			{
				// Start ... ************** New Process *********************
				if(dspqJobWiseStatus.getProfessional()==null || !dspqJobWiseStatus.getProfessional())
				{
					//13 - Employment History,14-Involvement,15-Honors (All Grid)
					//24-Teacher Retirement,26-Current Employment,12 - Resume (PDF preferred) (Open Section)
					boolean bProfessional_Incomplete=false;
					
					iSectionId=13; //Employment History
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bProfessional_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bProfessional_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bProfessional_Incomplete)
					{
						DspqRouter dspqRouter=mapDspqRouterBySID.get(iSectionId);
						if(dspqRouter!=null && dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired() && !bProfessional_Incomplete)// Count check
						{
							List<Integer> lstTeacherRole=teacherRoleDAO.getTeacherRoleCountByTeacherJAF(teacherDetail);
							if(lstTeacherRole!=null && lstTeacherRole.size()>0)
							{
								System.out.println("teacherAdditionalDocuments(0) "+lstTeacherRole.get(0));
								if(lstTeacherRole.get(0)==0)
								{
									quickPrintData("EmploymentHistory/TeacherRole Grid");
									bProfessional_Incomplete=true;
								}
							}
						}
					}
					
					iSectionId=14; //Involvement
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bProfessional_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bProfessional_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bProfessional_Incomplete)
					{
						DspqRouter dspqRouter=mapDspqRouterBySID.get(iSectionId);
						if(dspqRouter!=null && dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired() && !bProfessional_Incomplete)// Count check
						{
							List<Integer> lstteacherInvolvement=teacherInvolvementDAO.getTeacherInvolvementCountByTeacherJAF(teacherDetail);
							if(lstteacherInvolvement!=null && lstteacherInvolvement.size()>0)
							{
								System.out.println("lstteacherInvolvement(0) "+lstteacherInvolvement.get(0));
								if(lstteacherInvolvement.get(0)==0)
								{
									quickPrintData("lstteacherInvolvement Grid");
									bProfessional_Incomplete=true;
								}
							}
						}
					}
					
					
					iSectionId=15; //Honors
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bProfessional_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bProfessional_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bProfessional_Incomplete)
					{
						DspqRouter dspqRouter=mapDspqRouterBySID.get(iSectionId);
						if(dspqRouter!=null && dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired() && !bProfessional_Incomplete)// Count check
						{
							List<Integer> lstteacherHonor=teacherHonorDAO.getTeacherHonorCountByTeacherJAF(teacherDetail);
							if(lstteacherHonor!=null && lstteacherHonor.size()>0)
							{
								System.out.println("lstteacherHonor(0) "+lstteacherHonor.get(0));
								if(lstteacherHonor.get(0)==0)
								{
									quickPrintData("lstteacherHonor Grid");
									bProfessional_Incomplete=true;
								}
							}
						}
					}
					
					//24-Teacher Retirement,26-Current Employment,12 - Resume (PDF preferred) (Open Section)
					iSectionId=24; //Teacher Retirement
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bProfessional_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bProfessional_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null && !bProfessional_Incomplete)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("TeacherRetirement mapDspqRouterByFID "+mapDspqRouterByFID.size());
							
							if(mapDspqRouterByFID.get(105)!=null && !bProfessional_Incomplete)//Retired From State
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getStateMaster()!=null && teacherPersonalInfo.getStateMaster().getStateId()!=null)
								{
									
								}
								else
								{
									quickPrintData("Retired From State");
									bProfessional_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(106)!=null && !bProfessional_Incomplete)
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getDistrictmaster()!=null)//Retired From District
								{
									
								}
								else
								{
									quickPrintData("Retired From District");
									bProfessional_Incomplete=true;
								}
							}
							
							if(mapDspqRouterByFID.get(104)!=null && !bProfessional_Incomplete)
							{
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getRetirementnumber()!=null)//Teacher Retirement Number
								{
									
								}
								else
								{
									quickPrintData("Teacher Retirement Number");
									bProfessional_Incomplete=true;
								}
							}
							
						}
					}
					
					
					iSectionId=26; //Current Employment
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bProfessional_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bProfessional_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null && !bProfessional_Incomplete)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("CurrentEmployment mapDspqRouterByFID "+mapDspqRouterByFID.size());
							
							if((mapDspqRouterByFID.get(121)!=null || mapDspqRouterByFID.get(127)!=null || mapDspqRouterByFID.get(132)!=null) && !bProfessional_Incomplete)//Retired From State
							{
								//int iEmployeeType=-1;
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType()!=null)
								{
									
								}
								else
								{
									quickPrintData("CurrentEmployment");
									bProfessional_Incomplete=true;
								}
							}
							
						}
					}
					
					iSectionId=12; //Resume (PDF preferred)
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bProfessional_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bProfessional_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && mapRouterBySection.get(iSectionId)!=null && !bProfessional_Incomplete)
					{
						mapDspqRouterByFID=mapRouterBySection.get(iSectionId);
						if(mapDspqRouterByFID!=null)
						{
							System.out.println("Resume (PDF preferred) mapDspqRouterByFID "+mapDspqRouterByFID.size());
							
							if(mapDspqRouterByFID.get(58)!=null && !bProfessional_Incomplete)//Retired From State
							{
								if(teacherExperience!=null && teacherExperience.getResume()!=null && !teacherExperience.getResume().equalsIgnoreCase(""))
								{
									
								}
								else
								{
									quickPrintData("Resume");
									bProfessional_Incomplete=true;
								}
							}
							
						}
					}
					
					//--------------------------------------
					if(!bProfessional_Incomplete)
					{
						dspqJobWiseStatus.setProfessional(true);
						dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
					}
					
				}
				// End ... ************** New Process *********************
				
				if(dspqJobWiseStatus.getProfessional()==null || !dspqJobWiseStatus.getProfessional())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="PR";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblProfessional", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowprofessional.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||PR";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblProfessional", locale)+"<BR>";
					}
				}
			}
				
			
			if(dspqJobWiseStatus.getIsAffidaviteRequired()!=null && dspqJobWiseStatus.getIsAffidaviteRequired())
			{
				// Start ... ************** New Process *********************
				if(dspqJobWiseStatus.getAffidavite()==null || !dspqJobWiseStatus.getAffidavite())
				{
					boolean bAffidavite_Incomplete=false;
					iSectionId=25; //Affidavit
					
					//Start .... Custom Field checking
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bAffidavite_Incomplete)
					{
						boolean bCustomFieldIncomplteSTatusTemp=getCustomFieldBySectionId_QuickJobApplyL2(dspqPortfolioName.getDistrictMaster(), teacherDetail, iSectionId, mapCustomFieldlstBySectionId, mapCustomQuestionAnswers);
						if(bCustomFieldIncomplteSTatusTemp)
						{
							quickPrintData("Custom Field incomplete for section Id "+iSectionId);
							bAffidavite_Incomplete=true;
						}
					}
					//End .... Custom Field checking
					
					if(mapDspqRouterBySID.get(iSectionId)!=null && !bAffidavite_Incomplete)
					{
						DspqRouter dspqRouter=mapDspqRouterBySID.get(iSectionId);
						if(dspqRouter!=null && dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired() && !bAffidavite_Incomplete)// Count check
						{
							TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
							if(teacherPortfolioStatus!=null && teacherPortfolioStatus.getIsAffidavitCompleted()!=null && teacherPortfolioStatus.getIsAffidavitCompleted())
							{
								
							}
							else
							{
								quickPrintData("Affidavit Fld");
								bAffidavite_Incomplete=true;
							}
						}
					}
					
					//--------------------------------------
					if(!bAffidavite_Incomplete)
					{
						dspqJobWiseStatus.setAffidavite(true);
						dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
					}
					
				}
				// End ... ************** New Process *********************
				
				if(dspqJobWiseStatus.getAffidavite()==null || !dspqJobWiseStatus.getAffidavite())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="AF";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblAffidavit", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowAffidavit.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||AF";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblAffidavit", locale)+"<BR>";
					}
				}
			}
				
			
			if(dspqJobWiseStatus.getIsPreScreenRequired()!=null && dspqJobWiseStatus.getIsPreScreenRequired())
			{
				// Start ... ************** New Process *********************
				if(dspqJobWiseStatus.getPreScreen()==null || !dspqJobWiseStatus.getPreScreen())
				{
					boolean bPreScreen_Incomplete=false;
					
					//Check QQ Status
					try
					{
						List<DistrictSpecificQuestions> districtSpecificQuestionList	= new ArrayList<DistrictSpecificQuestions>(); 
						int totalQuestions =0;

						if(jobOrder!=null && jobOrder.getJobId()>0)
						{
							JobCategoryMaster jobCategoryMaster = null;
							if(jobOrder!=null)
								jobCategoryMaster=jobOrder.getJobCategoryMaster();
							
							QqQuestionSets qqQuestionSets = new QqQuestionSets();
							List<QqQuestionsetQuestions> qqQuestionsetQuestions=null;
							if(jobCategoryMaster!=null && jobCategoryMaster.getQuestionSets()!=null)
							{
								qqQuestionSets = jobCategoryMaster.getQuestionSets();
								qqQuestionsetQuestions = qqQuestionsetQuestionsDAO.findByQuestionSet(qqQuestionSets); 
							}
							
							List<Integer> tempDistSpecQues = new ArrayList<Integer>();
							if(qqQuestionsetQuestions!=null && qqQuestionsetQuestions.size()>0)
							{
								for (QqQuestionsetQuestions qqQuestionsetQuestions2 : qqQuestionsetQuestions) 
								{
									tempDistSpecQues.add(qqQuestionsetQuestions2.getDistrictSpecificQuestions().getQuestionId());
								}
							}
							
							if(tempDistSpecQues!=null && tempDistSpecQues.size()>0)
							{
								districtSpecificQuestionList=districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYQues(tempDistSpecQues);
								totalQuestions=districtSpecificQuestionList.size();
							}
							List<JobOrder> jobOrders = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findLastTeacherAnswersByDistrictAndJob(teacherDetail,jobOrder);

							List<TeacherAnswerDetailsForDistrictSpecificQuestions> lastList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
							if(jobOrders!=null && jobOrders.size()>0)
								lastList = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersBySetQues(teacherDetail,qqQuestionSets);

							Map<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions> mapAnswer = new HashMap<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions>();

							if(lastList!=null)
							for(TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion : lastList)
								mapAnswer.put(teacherAnswerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId(), teacherAnswerDetailsForDistrictSpecificQuestion);

							// change 
							TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion = null;
							String shortName = "";

							if(totalQuestions>0)
							{	
								for (DistrictSpecificQuestions districtSpecificQuestion : districtSpecificQuestionList) 
								{

									shortName = districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeShortName();
									
									teacherAnswerDetailsForDistrictSpecificQuestion = mapAnswer.get(districtSpecificQuestion.getQuestionId());
									if(teacherAnswerDetailsForDistrictSpecificQuestion!=null && shortName.equalsIgnoreCase(teacherAnswerDetailsForDistrictSpecificQuestion.getQuestionType()))
									{
										
									}
									else
									{
										quickPrintData("QQ");
										bPreScreen_Incomplete=true;
									}
								}
							}
								
						}
				
					}catch (Exception e) {
						e.printStackTrace();
					}
					
					
					if(!bPreScreen_Incomplete)
					{
						dspqJobWiseStatus.setPreScreen(true);
						dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
					}
					
				}
				// End ... ************** New Process *********************
				
				if(dspqJobWiseStatus.getPreScreen()==null || !dspqJobWiseStatus.getPreScreen())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="QQ";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblPreScreen", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowprescreen.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||QQ";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblPreScreen", locale)+"<BR>";
					}
				}
			}
				
			
			if(dspqJobWiseStatus.getIsEPIRequired()!=null && dspqJobWiseStatus.getIsEPIRequired())
			{
				// Start ... ************** New Process *********************
				if(dspqJobWiseStatus.getEPI()==null || !dspqJobWiseStatus.getEPI())
				{
					boolean isEPICompleteStatus=isEPICompleteStatus(teacherDetail);
					if(isEPICompleteStatus)
					{
						dspqJobWiseStatus.setEPI(true);
						dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
					}
					
				}
				// End ... ************** New Process *********************
				
				if(dspqJobWiseStatus.getEPI()==null || !dspqJobWiseStatus.getEPI())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="EPI";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblEPI", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowepi.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||EPI";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblEPI", locale)+"<BR>";
					}
				}
			}
				
			
			if(dspqJobWiseStatus.getIsJSIRequired()!=null && dspqJobWiseStatus.getIsJSIRequired())
			{
				// Start ... ************** New Process *********************
				if(dspqJobWiseStatus.getJSI()==null || !dspqJobWiseStatus.getJSI())
				{
					boolean isJSICompleteStatus=isJSICompleteStatus(teacherDetail,jobOrder);
					if(isJSICompleteStatus)
					{
						dspqJobWiseStatus.setJSI(true);
						dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
					}
					
				}
				// End ... ************** New Process *********************
				
				if(dspqJobWiseStatus.getJSI()==null || !dspqJobWiseStatus.getJSI())
				{
					iUnDoneCounter++;
					if(sUnDoneShortCode.equals(""))
					{
						sUnDoneShortCode="JSI";
						sUnDoneMessage=iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblQuestions", locale)+"<BR>";
						sUnDoneURL="jobapplicationflowcustomquestion.do?jobId="+jobId+"&ct="+dspqct;
					}
					else
					{
						sUnDoneShortCode=sUnDoneShortCode+"||JSI";
						sUnDoneMessage=sUnDoneMessage+iUnDoneCounter+". "+Utility.getLocaleValuePropByKey("DSPQlblQuestions", locale)+"<BR>";
					}
				}
			}
				
			if(iUnDoneCounter > 0)
			{
				sUnDoneMessage="There are following incomplete section(s):-<BR>"+sUnDoneMessage;
			}
		}
		
		printdata("********************** End oneBtnJobApply ****************************");
		return iUnDoneCounter+"||"+sUnDoneURL+"||"+sUnDoneMessage;
	}
	
	public void setSIDAndFIDInMapQJA(Map<Integer, DspqRouter> mapDspqRouterBySID,Map<Integer, Map<Integer, DspqRouter>> mapRouterBySection,List<DspqRouter> dspqRouterList)
	{
		
		Map<Integer, Boolean> mapGridList=new TreeMap<Integer, Boolean>();
		mapGridList.put(6, true);//Academics
		mapGridList.put(8, true);//Certifications/Licensure
		mapGridList.put(9, true);//References
		mapGridList.put(10, true);//Video Links
		mapGridList.put(11, true);//Additional Documents
		mapGridList.put(13, true);//Employment History
		mapGridList.put(14, true);//Involvement
		mapGridList.put(15, true);//Honors
		
		
		if(dspqRouterList!=null && dspqRouterList.size()>0)
		{
			for(DspqRouter dspqRouter :dspqRouterList)
			{
				if(dspqRouter.getDspqSectionMaster()!=null)
					mapDspqRouterBySID.put(dspqRouter.getDspqSectionMaster().getSectionId(), dspqRouter);
			}
			
			for(DspqRouter dspqRouter :dspqRouterList)
			{
				if(dspqRouter.getDspqFieldMaster()!=null && ( dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired()) && mapGridList.get(dspqRouter.getDspqFieldMaster().getDspqSectionMaster().getSectionId())==null)
				{
					if(dspqRouter.getDspqFieldMaster()!=null && dspqRouter.getDspqFieldMaster().getDspqSectionMaster()!=null)
					{
						Map<Integer, DspqRouter> mapDspqRouter=new TreeMap<Integer, DspqRouter>();
						Integer iSectionId=dspqRouter.getDspqFieldMaster().getDspqSectionMaster().getSectionId();
						if(iSectionId!=null)
						{
							if(mapRouterBySection!=null && mapRouterBySection.get(iSectionId)!=null)
							{
								mapDspqRouter=new TreeMap<Integer, DspqRouter>();
								mapDspqRouter=mapRouterBySection.get(iSectionId);
								mapDspqRouter.put(dspqRouter.getDspqFieldMaster().getDspqFieldId(), dspqRouter);
								mapRouterBySection.put(iSectionId, mapDspqRouter);
							}
							else
							{
								mapDspqRouter=new TreeMap<Integer, DspqRouter>();
								mapDspqRouter.put(dspqRouter.getDspqFieldMaster().getDspqFieldId(), dspqRouter);
								
								if(mapRouterBySection==null)
									mapRouterBySection=new HashMap<Integer, Map<Integer, DspqRouter>>();
								
								mapRouterBySection.put(iSectionId, mapDspqRouter);
							}
						}
						
					}
					
				}
					
				
			}
			
		}
	}
	
	
	public void quickPrintData(String sInputValue)
	{
		if(sInputValue!=null && !sInputValue.equals(""))
			System.out.println("QJA Field => "+sInputValue);
	}
	
	
	@Transactional(readOnly=false)
	public boolean getCustomFieldBySectionId_QuickJobApplyL1(DistrictMaster districtMaster,String candidateType,DspqPortfolioName dspqPortfolioName,TeacherDetail teacherDetail,Map<Integer, List<DistrictSpecificPortfolioQuestions>> mapCustomFieldlstBySectionId,Map<Integer, DistrictSpecificPortfolioAnswers> mapCustomQuestionAnswers)
	{
		List<DistrictSpecificPortfolioQuestions> lstDistrictSpecificPortfolioQuestions=districtSpecificPortfolioQuestionsDAO.getCustumFieldByQuockJobApply(districtMaster, dspqPortfolioName, candidateType);
		List<DistrictSpecificPortfolioAnswers> lstDistrictSpecificPortfolioAnswers = new ArrayList<DistrictSpecificPortfolioAnswers>();
		if(lstDistrictSpecificPortfolioQuestions!=null && lstDistrictSpecificPortfolioQuestions.size()>0)
		{
			lstDistrictSpecificPortfolioAnswers=districtSpecificPortfolioAnswersDAO.getDSPAByTID(teacherDetail, lstDistrictSpecificPortfolioQuestions);
			for(DistrictSpecificPortfolioAnswers obj:lstDistrictSpecificPortfolioAnswers)
				mapCustomQuestionAnswers.put(obj.getDistrictSpecificPortfolioQuestions().getQuestionId(), obj);
		}
		
		if(lstDistrictSpecificPortfolioQuestions!=null && lstDistrictSpecificPortfolioQuestions.size()>0)
		{
			List<DistrictSpecificPortfolioQuestions> lstTempDistrictSpecificPortfolioQuestions=null;
			for(DistrictSpecificPortfolioQuestions dspq:lstDistrictSpecificPortfolioQuestions)
			{
				if(dspq!=null && dspq.getIsRequired()==1)
				{
					if(dspq.getDspqSectionMaster()!=null && dspq.getDspqSectionMaster().getSectionId()!=null && mapCustomFieldlstBySectionId.get(dspq.getDspqSectionMaster().getSectionId())!=null)
					{
						lstTempDistrictSpecificPortfolioQuestions=mapCustomFieldlstBySectionId.get(dspq.getDspqSectionMaster().getSectionId());
						lstTempDistrictSpecificPortfolioQuestions.add(dspq);
						mapCustomFieldlstBySectionId.put(dspq.getDspqSectionMaster().getSectionId(), lstTempDistrictSpecificPortfolioQuestions);
					}
					else if(dspq.getDspqSectionMaster()!=null && dspq.getDspqSectionMaster().getSectionId()!=null)
					{
						lstTempDistrictSpecificPortfolioQuestions=new ArrayList<DistrictSpecificPortfolioQuestions>();
						lstTempDistrictSpecificPortfolioQuestions.add(dspq);
						mapCustomFieldlstBySectionId.put(dspq.getDspqSectionMaster().getSectionId(), lstTempDistrictSpecificPortfolioQuestions);
					}
				}
			}
		}
		
		return true;
	}
	
	
	@Transactional(readOnly=false)
	public boolean getCustomFieldBySectionId_QuickJobApplyL2(DistrictMaster districtMaster,TeacherDetail teacherDetail,Integer iSectionId,Map<Integer, List<DistrictSpecificPortfolioQuestions>> mapCustomFieldlstBySectionId,Map<Integer, DistrictSpecificPortfolioAnswers> mapCustomQuestionAnswers)
	{
		boolean bSection_Incomplete=false;
		List<DistrictSpecificPortfolioQuestions> lstDistrictSpecificPortfolioQuestions=null;
		if(mapCustomFieldlstBySectionId!=null && mapCustomFieldlstBySectionId.get(iSectionId)!=null)
			lstDistrictSpecificPortfolioQuestions= mapCustomFieldlstBySectionId.get(iSectionId);
		
		if(lstDistrictSpecificPortfolioQuestions!=null && lstDistrictSpecificPortfolioQuestions.size()>0)
		{
			for(DistrictSpecificPortfolioQuestions dspq:lstDistrictSpecificPortfolioQuestions)
			{
				if(dspq!=null && dspq.getIsRequired()==1 && !bSection_Incomplete)
				{
					if(mapCustomQuestionAnswers!=null && mapCustomQuestionAnswers.get(dspq.getQuestionId())!=null)
					{
						
					}
					else
					{
						bSection_Incomplete=true;
						break;
					}
				}
			}
		}
		return bSection_Incomplete;
	}
	
	
	@Transactional(readOnly=false)
	public String callSelfServiceApplicationFlowOuter(String iJobId,String candidateType)
	{
		printdata("Calling DSPQServiceAjax => callSelfServiceApplicationFlowOuter");
		
		JobOrder jobOrder=null;
		if(iJobId!=null && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
		}

		String sReturnURL="";
		DspqPortfolioName dspqPortfolioNameNew =getDSPQByJobOrder(jobOrder,candidateType);
		
		boolean bSelfServiceStatus=false;
		if(jobOrder!=null && jobOrder.getDistrictMaster()!=null)
			bSelfServiceStatus=getSelfServicePortfolioStatus(jobOrder.getDistrictMaster());
		
		if(bSelfServiceStatus && dspqPortfolioNameNew!=null)
		{
			sReturnURL="NewSelfService";
		}
		
		return sReturnURL;
	}
@Transactional(readOnly=false)
	public String getTeacherVeteran(DistrictMaster districtId)
    {
          System.out.println("Calling getDistrictSpecificVeteranValues Ajax Method   "+districtId);
          WebContext context;
          context = WebContextFactory.get();
          HttpServletRequest request = context.getHttpServletRequest();
          HttpSession session = request.getSession(false);
          if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
          {
               throw new IllegalStateException(msgYrSesstionExp);
          }

          TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");          
          List<DistrictSpecificTeacherTfaOptions> districtSpecificTeacherTfaOptions=null;
          
          String selectedOptions = "";
          try{
               Criterion criterion = Restrictions.eq("districtMaster", districtId);
               Criterion criterion2 =Restrictions.eq("teacherDetail", teacherDetail);
               districtSpecificTeacherTfaOptions = districtSpecificTeacherTfaOptionsDAO.findByCriteria(criterion,criterion2);

               if(districtSpecificTeacherTfaOptions!=null && districtSpecificTeacherTfaOptions.size()>0 && districtSpecificTeacherTfaOptions.get(0).getTfaOptions()!=null){
                    selectedOptions = districtSpecificTeacherTfaOptions.get(0).getTfaOptions()==null?"":districtSpecificTeacherTfaOptions.get(0).getTfaOptions();
                    System.out.println("selectedOptions    "+selectedOptions);
               }
          } 
          catch (Exception e)  {
               e.printStackTrace();
               return selectedOptions;
          }    
          return selectedOptions;
    }
	public int setDistrictSpecificVeteranValues(DistrictMaster districtMaster,String options,String othText)
	{
		System.out.println("Calling getDistrictSpecificTFAValues Ajax Method");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		List<DistrictSpecificTeacherTfaOptions> districtSpecificTeacherTfaOptions=null;
		try {
			Criterion criterion = Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2 =Restrictions.eq("teacherDetail", teacherDetail);			

			districtSpecificTeacherTfaOptions = districtSpecificTeacherTfaOptionsDAO.findByCriteria(criterion,criterion2);

			DistrictSpecificTeacherTfaOptions dSpecificTeacherTfaOptions = new DistrictSpecificTeacherTfaOptions();
			if(districtSpecificTeacherTfaOptions!=null && districtSpecificTeacherTfaOptions.size()>0){
				dSpecificTeacherTfaOptions = districtSpecificTeacherTfaOptions.get(0);
			}

			dSpecificTeacherTfaOptions.setDistrictMaster(districtMaster);
			dSpecificTeacherTfaOptions.setOthersText(othText);
			dSpecificTeacherTfaOptions.setTeacherDetail(teacherDetail);
			dSpecificTeacherTfaOptions.setTfaOptions(options);

			districtSpecificTeacherTfaOptionsDAO.makePersistent(dSpecificTeacherTfaOptions);
		} catch (Exception e) {
			// TODO: handle exception
		}

		return 1;
	}
	public String getGridHeaderName(Map<Integer, DspqRouter> mapDspqRouterBySID,Integer fieldId)
	{
		String sDisplayName="";
		if(fieldId!=null && fieldId>0)
		{
			DspqRouter dspqFieldObj=null;
			if(mapDspqRouterBySID.get(fieldId)!=null)
			{
				dspqFieldObj=mapDspqRouterBySID.get(fieldId);
				if(dspqFieldObj!=null)
					sDisplayName=dspqFieldObj.getDisplayName();
				
				if(sDisplayName!=null && !sDisplayName.equals("") && sDisplayName.contains(" "))
				{
					sDisplayName=sDisplayName.replaceAll(" ", "&nbsp;");
				}
			}
		}
		return sDisplayName;
	}
	public String validateApplicantType(String candidateType){
		String defCandidateType="E";
		if(candidateType.equals("I")){
			defCandidateType="I";
		}else if(candidateType.equals("T")){
			defCandidateType="T";
		}else{
			defCandidateType="E";
		}
		return defCandidateType;
	}
  public Map<Integer,DspqRouter> getDSPQRouterMapById(Integer portfolioId,String applicantType,Integer sectionId){
		Map<Integer, DspqRouter> mapDspqRouterByID=new HashMap<Integer, DspqRouter>();
		try{
			DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
			dspqSectionMaster.setSectionId(sectionId);
			List<DspqFieldMaster> objhList=dspqFieldMasterDAO.getDspqAllFieldListBySectionsID(dspqSectionMaster);
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getDspqSectionListByTypeAndId(portfolioId, applicantType,objhList);
			//System.out.println("sectionId  ::: "+sectionId+" portfolioId ::::::: "+portfolioId+" applicantType ::::: "+applicantType);
				for(DspqRouter dspqRouter :dspqRouterList)
				{
					if(dspqRouter.getDspqFieldMaster()!=null){
						mapDspqRouterByID.put(dspqRouter.getDspqFieldMaster().getDspqFieldId(), dspqRouter);
						//System.out.println("Field Id :::::"+dspqRouter.getDspqFieldMaster().getDspqFieldId());
					}
				}
		}catch (Exception e) {
			e.printStackTrace();
	 }
		return mapDspqRouterByID;
 }
  
 /**
  * 
  * @param generalKnowledgeExamStatus
  * @param generalKnowledgeExamDate
  * @param UploadedFileName
  * @param generalExamNote
  * @return
  * @author Ramesh Kumar Bhartiya
  * Date Feb 2, 2016
  */
  public String saveGeneralKnw(String generalKnowledgeExamStatus,String generalKnowledgeExamDate,String UploadedFileName,String generalExamNote)
	{
		printdata("Calling DSPQServiceAjax => saveGeneralKnw ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		String sReturnValue="OK";
		try 
		{	
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherGeneralKnowledgeExam teacherGKE = teacherGeneralKnowledgeExamDAO.findTeacherGeneralKnowledgeExam(teacherDetail);
			if(teacherGKE==null)
			{
				teacherGKE=new TeacherGeneralKnowledgeExam();
				teacherGKE.setCreatedDateTime(new Date());
			}
			
			teacherGKE.setTeacherDetail(teacherDetail);
			try{
				if(generalKnowledgeExamStatus!=null && !generalKnowledgeExamStatus.equals(""))
					teacherGKE.setGeneralKnowledgeExamStatus(generalKnowledgeExamStatus);
				
				if(generalKnowledgeExamDate!=null && !generalKnowledgeExamDate.equals(""))
					teacherGKE.setGeneralKnowledgeExamDate(Utility.getCurrentDateFormart(generalKnowledgeExamDate));
				
				if(UploadedFileName!=null && !UploadedFileName.equals(""))
					teacherGKE.setGeneralKnowledgeScoreReport(UploadedFileName);
				
				if(generalExamNote!=null && !generalExamNote.equals(""))
					teacherGKE.setGeneralExamNote(generalExamNote);
				
			}catch(Exception e){
				//e.printStackTrace();
			}
			
			try {
				teacherGeneralKnowledgeExamDAO.makePersistent(teacherGKE);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sReturnValue;
	
	}
  
  public String jafDownloadGeneralKnowledge(Integer generalKnowledgeExamId)
  {
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp);
	}
	String path="";
	
	try 
	{
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam = teacherGeneralKnowledgeExamDAO.findTeacherGeneralKnowledgeExam(teacherDetail);
		String fileName= teacherGeneralKnowledgeExam.getGeneralKnowledgeScoreReport();
	    String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+fileName;
	    String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
	    File sourceFile = new File(source);
	    File targetDir = new File(target);
	    if(!targetDir.exists())
	    	targetDir.mkdirs();
	        
	    File targetFile = new File(targetDir+"/"+sourceFile.getName());
	    if(sourceFile.exists()){
	      	FileUtils.copyFile(sourceFile, targetFile);
	       	path =  Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
	       }	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;
	
	}
  
  
  public String downloadCustomFileUpload(Integer answerId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String path="";

		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			List<DistrictSpecificPortfolioAnswers> districtSpecificPortfolioAnswersList = districtSpecificPortfolioAnswersDAO.findByCriteria(Restrictions.eq("answerId", answerId));
			if(districtSpecificPortfolioAnswersList!=null && districtSpecificPortfolioAnswersList.size()>0)
			{	teacherDetail=districtSpecificPortfolioAnswersList.get(0).getTeacherDetail();
				if(districtSpecificPortfolioAnswersList.get(0).getFileName()!=null && !districtSpecificPortfolioAnswersList.get(0).getFileName().equalsIgnoreCase(""))
				{
					 String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+districtSpecificPortfolioAnswersList.get(0).getFileName();
				     String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
				     
				     File sourceFile = new File(source);
				     File targetDir = new File(target);
				     if(!targetDir.exists())
				    	 targetDir.mkdirs();
				     
				     File targetFile = new File(targetDir+"/"+sourceFile.getName());
				     
				     if(sourceFile.exists()){
				       	FileUtils.copyFile(sourceFile, targetFile);
				       	path =  Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
				     }
				}
			}
		}
		catch (Exception e) 
		{
			System.out.println("Error in Answer");
			e.printStackTrace();
		}
		finally
		{
			return path;
		}
	}
  
  
  	public boolean getSelfServicePortfolioStatus(DistrictMaster districtMaster)
  	{
  		boolean bSelfServiceStatus=false;
  		if(districtMaster!=null && districtMaster.getSelfServicePortfolioStatus()!=null && districtMaster.getSelfServicePortfolioStatus().equals(true))
			bSelfServiceStatus=true;
  		return bSelfServiceStatus;
  	}
  	public String getSubjectAreasGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer sb = new StringBuffer();		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("subject")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 
				 if(sortOrder.equals("subject")){
					 sortOrderNoField="subject";
				 }
				 
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			
			
			
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			List<TeacherSubjectAreaExam> lstTeacherSubjectAreaExams=teacherSubjectAreaExamDAO.getTeacherSubjectAreaExamsByTeacher(sortOrderStrVal,teacherDetail);
			
			List<TeacherSubjectAreaExam> sortedlistTeacherSubjectAreaExams = new ArrayList<TeacherSubjectAreaExam>();
			
			SortedMap<String,TeacherSubjectAreaExam>	sortedMap = new TreeMap<String,TeacherSubjectAreaExam>();
			if(sortOrderNoField.equals("subject"))
			{
				sortOrderFieldName	=	"subject";
			}
			
			int mapFlag=2;
			for (TeacherSubjectAreaExam trSubArea : lstTeacherSubjectAreaExams){
				String orderFieldName=trSubArea.getTeacherSubjectAreaExamId()+"";
				if(sortOrderFieldName.equals("subject")){
					orderFieldName=trSubArea.getSubjectAreaExamMaster().getSubjectAreaExamName()+"||"+trSubArea.getTeacherSubjectAreaExamId();
					sortedMap.put(orderFieldName+"||",trSubArea);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
			}
			
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherSubjectAreaExams.add((TeacherSubjectAreaExam) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherSubjectAreaExams.add((TeacherSubjectAreaExam) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherSubjectAreaExams=lstTeacherSubjectAreaExams;
			}
			
			totalRecord =sortedlistTeacherSubjectAreaExams.size();

			if(totalRecord<end)
				end=totalRecord;
			
			List<TeacherSubjectAreaExam> teacherSubjectAreaExamsList =	sortedlistTeacherSubjectAreaExams.subList(start,end);
			
			sb.append("<table border='0' id='subjectAreasGrid'  width='100%' class='table table-striped mt30'  >");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLinkSSPF(lblStatus,sortOrderFieldName,"examStatus",sortOrderTypeVal,pgNo);
			sb.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkSSPF(Utility.getLocaleValuePropByKey("lblExamDate", locale),sortOrderFieldName,"examDate",sortOrderTypeVal,pgNo);
			sb.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkSSPF(Utility.getLocaleValuePropByKey("lblSub", locale),sortOrderFieldName,"subject",sortOrderTypeVal,pgNo);
			sb.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			sb.append("<th width='40%' valign='top'>"+Utility.getLocaleValuePropByKey("lblScoreReport", locale)+"</th>");
			
			sb.append("<th width='20%' class='net-header-text'>");
			sb.append(lblAct);
			sb.append("</th>");			
			sb.append("</tr>");
			sb.append("</thead>");
			
			String sExamStatus="";
			if(teacherSubjectAreaExamsList!=null && teacherSubjectAreaExamsList.size()>0)
			for(TeacherSubjectAreaExam objTAD:teacherSubjectAreaExamsList)
			{
				if(objTAD.getExamStatus()!=null)
				{
					if(objTAD.getExamStatus().equalsIgnoreCase("P"))
						sExamStatus="Pass";
					else if(objTAD.getExamStatus().equalsIgnoreCase("F"))
						sExamStatus="Fail";
				}
				
				sb.append("<tr>");
				
				sb.append("<td>");
				sb.append(sExamStatus);	
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(objTAD.getExamDate()));
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append(objTAD.getSubjectAreaExamMaster().getSubjectAreaExamName());
				sb.append("</td>");
				
				
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				
				sb.append("<td>");
				
				if(objTAD.getScoreReport()!=null)
				{
					sb.append(objTAD.getScoreReport()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='"+msgViewScoreReport+"' id='cert"+objTAD.getTeacherSubjectAreaExamId()+"' onclick=\"downloadSubjectAreaExam('"+objTAD.getTeacherSubjectAreaExamId()+"','cert"+objTAD.getTeacherSubjectAreaExamId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue' style='padding-left:20px;'></span></a><br>");
					sb.append("<script>$('#cert"+objTAD.getTeacherSubjectAreaExamId()+"').tooltip();</script>");
				}
				
				sb.append("</td>");
				
				sb.append("<td>");
				
				sb.append("	<a href='javascript:void(0)' rel='tooltip' data-original-title='"+editToolTip+"' id='subj"+objTAD.getTeacherSubjectAreaExamId()+"' onclick=\"return showEditSubject('"+objTAD.getTeacherSubjectAreaExamId()+"')\" ><i class='fa fa-pencil-square-o fa-lg'></i></a>");
				sb.append("<script>$('#subj"+objTAD.getTeacherSubjectAreaExamId()+"').tooltip();</script>");
				sb.append("&nbsp;|&nbsp;<a href='javascript:void(0)' rel='tooltip' data-original-title='"+deleteToolTip+"' id='subj1"+objTAD.getTeacherSubjectAreaExamId()+"' onclick=\"return delRow_subjectArea('"+objTAD.getTeacherSubjectAreaExamId()+"')\"><i class='fa fa-trash-o'></i></a>");
				sb.append("<script>$('#subj1"+objTAD.getTeacherSubjectAreaExamId()+"').tooltip();</script>");
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			if(teacherSubjectAreaExamsList==null || teacherSubjectAreaExamsList.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='5'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
  	public String deleteSubjectArea(int id)
	{
		String sReturnValue="1";
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{
			TeacherSubjectAreaExam teacherSubjectAreaExam=teacherSubjectAreaExamDAO.findById(id, false, false);
			if(teacherSubjectAreaExam!=null)
			{
				teacherSubjectAreaExamDAO.makeTransient(teacherSubjectAreaExam);
			}
			else
			{
				sReturnValue="0";
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			sReturnValue="0";
		}
		return sReturnValue;
	}
  	@Transactional(readOnly=false)
	public int saveOrUpdateSubjectAreaExam(TeacherSubjectAreaExam teacherSubjectAreaExam,String sUploadedFileName)
	{
		printdata("Calling DSPQServiceAjax => saveOrUpdateSubjectAreaExam");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		try 
		{	
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			if(teacherSubjectAreaExam!=null)
			{
				teacherSubjectAreaExam.setCreatedDateTime(new Date());
				teacherSubjectAreaExam.setTeacherDetail(teacherDetail);
				if(sUploadedFileName!=null && !sUploadedFileName.equals("") && !sUploadedFileName.equals("0"))
				{
					teacherSubjectAreaExam.setScoreReport(sUploadedFileName);
				}
				else
				{
					printdata("is null");
				}
				
				teacherSubjectAreaExamDAO.makePersistent(teacherSubjectAreaExam);
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return 2;
	}
  	@SuppressWarnings("finally")	
	public TeacherSubjectAreaExam showEditSubject(int id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		TeacherSubjectAreaExam teacherSubjectAreaExam=null;
		try 
		{
			teacherSubjectAreaExam=teacherSubjectAreaExamDAO.findById(id, false, false);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			return teacherSubjectAreaExam;
		}
	}
  	public String downloadSubjectAreaExam(Integer subjectAreaExamId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		String path="";
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			
			TeacherSubjectAreaExam teacherSubjectAreaExam = teacherSubjectAreaExamDAO.findById(subjectAreaExamId, false, false);
			
			
			String fileName= teacherSubjectAreaExam.getScoreReport();
	        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+fileName;
	        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        if(sourceFile.exists()){
	        	FileUtils.copyFile(sourceFile, targetFile);
	        	path =  Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
	        }	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;
	
	}
  
}
