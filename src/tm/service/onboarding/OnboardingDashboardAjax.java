package tm.service.onboarding;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.DistrictMaster;
import tm.bean.master.OnBoardingCommunication;
import tm.bean.master.OnboardingDashboard;
import tm.bean.master.QuestionTypeMaster;
import tm.bean.master.ReadOnlyCheckPointMaster;
import tm.bean.master.ReadOnlyCheckPointTransaction;
import tm.bean.onboarding.AddCheckPointTransaction;
import tm.bean.onboarding.CheckPointAction;
import tm.bean.onboarding.CheckPointMaster;
import tm.bean.onboarding.CheckPointPermissionMaster;
import tm.bean.onboarding.CheckPointPermissionTransaction;
import tm.bean.onboarding.CheckPointStatusMaster;
import tm.bean.onboarding.CheckPointStatusTransaction;
import tm.bean.onboarding.CheckPointTransaction;
import tm.bean.onboarding.EditOnboardDashboard;
import tm.bean.onboarding.OnBoardingCheckPointOptions;
import tm.bean.onboarding.OnboardCheckPointQuestion;
import tm.bean.user.UserMaster;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.QuestionTypeMasterDAO;
import tm.dao.onboardingcheckpoint.AddQuestionDAO;
import tm.dao.onboardingcheckpoint.CheckPointActionDAO;
import tm.dao.onboardingcheckpoint.CheckPointMasterDAO;
import tm.dao.onboardingcheckpoint.CheckPointPermissionMasterDAO;
import tm.dao.onboardingcheckpoint.CheckPointPermissionTransactionDAO;
import tm.dao.onboardingcheckpoint.CheckPointStatusMasterDAO;
import tm.dao.onboardingcheckpoint.CheckPointStatusTransactionDAO;
import tm.dao.onboardingcheckpoint.CheckPointTransactionDAO;
import tm.dao.onboardingcheckpoint.EditOnboardingDashboardDAO;
import tm.dao.onboardingcheckpoint.OnBoardingCommunicationDAO;
import tm.dao.onboardingcheckpoint.OnboardCheckPointOptionsDAO;
import tm.dao.onboardingcheckpoint.OnboardingAddCheckPointDAO;
import tm.dao.onboardingcheckpoint.OnboardingDashboardDAO;
import tm.dao.onboardingcheckpoint.ReadOnlyCheckPointTransactionDAO;
import tm.dao.onboardingcheckpoint.ReadonlyCheckPointMasterDAO;
import tm.services.PaginationAndSorting;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class OnboardingDashboardAjax {
	
	String locale = Utility.getValueOfPropByKey("locale");
	String 	msgActiveUser=Utility.getLocaleValuePropByKey("msgActiveUser", locale);
	String 	msgInactiveUser=Utility.getLocaleValuePropByKey("msgInactiveUser", locale);
	
	@Autowired
	DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	AddQuestionDAO addQuestionDAO;
	
	@Autowired
	OnboardCheckPointOptionsDAO OnboardCheckPointOptionsDAO;
	
	@Autowired
	OnboardingAddCheckPointDAO onboardingAddCheckPointDAO;
	
	@Autowired
	OnboardingDashboardDAO onboardingDashboardDAO;
	
	@Autowired
	CheckPointStatusMasterDAO checkPointStatusMasterDAO;
	@Autowired
	EditOnboardingDashboardDAO editOnboardingDashboardDAO; 
	@Autowired
	CheckPointStatusTransactionDAO checkPointStatusTransactionDAO;
	@Autowired
	CheckPointMasterDAO checkPointMasterDAO;
	@Autowired
	OnBoardingCommunicationDAO onBoardingCommunicationDAO;
	@Autowired
	CheckPointPermissionMasterDAO checkPointPermissionMasterDAO;
	@Autowired
	CheckPointPermissionTransactionDAO checkPointPermissionTransactionDAO;
	@Autowired
	ReadonlyCheckPointMasterDAO readonlyCheckPointMasterDAO;
	@Autowired
	ReadOnlyCheckPointTransactionDAO readOnlyCheckPointTransactionDAO;
	@Autowired
	CheckPointTransactionDAO checkPointTransactionDAO;
	@Autowired
	QuestionTypeMasterDAO questionTypeMasterDAO;
	@Autowired
	CheckPointActionDAO checkPointActionDAO;
	

	public String displayOnBoardingRecord(Integer districtId, Integer noOfRow, Integer pageNo,Integer sessionFlag)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			
			int noOfRowInPage 	= 	noOfRow;
			int pgNo 			= 	pageNo;
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			
			UserMaster userMaster=null;
			userMaster=(UserMaster)session.getAttribute("userMaster");
			List<OnboardingDashboard> OnboardingDashboardAll =null;
			List<OnboardingDashboard> OnboardingDashboardList =null;
			DistrictMaster districtMaster=userMaster.getDistrictId();
			Order  sortOrderStrVal		=	null;
			sortOrderStrVal=Order.asc("onboardingName");
			Criterion criterion=null;
			if(userMaster.getEntityType()==1)
			{
				if(districtId!=null && districtId!=0)
				{
					districtMaster=null;
					districtMaster=districtMasterDAO.findById(districtId, false, false);
					if(sessionFlag==1){
						session.setAttribute("districtMasterId", districtMaster.getDistrictId());
						session.setAttribute("districtMasterName", districtMaster.getDistrictName());
					}
					criterion=Restrictions.eq("districtMaster", districtMaster);
					OnboardingDashboardList=onboardingDashboardDAO.findOnBoardingList(sortOrderStrVal, start, noOfRowInPage,criterion);
					OnboardingDashboardAll=onboardingDashboardDAO.findOnBoardingNameByDistrict(districtMaster);
				}
				else	
				{
					if(sessionFlag==1){
						session.setAttribute("districtMasterId",0);
						session.setAttribute("districtMasterName", "");
					}
					criterion=Restrictions.isNotNull("status");				
					OnboardingDashboardList=onboardingDashboardDAO.findOnBoardingList(sortOrderStrVal, start, noOfRowInPage,criterion);
					OnboardingDashboardAll=onboardingDashboardDAO.findAll();
				}
			}
			else
			{
				
				criterion=Restrictions.eq("districtMaster", districtMaster);
				OnboardingDashboardList=onboardingDashboardDAO.findOnBoardingList(sortOrderStrVal, start, noOfRowInPage,criterion);
				OnboardingDashboardAll=onboardingDashboardDAO.findOnBoardingNameByDistrict(districtMaster);
			}
			totalRecord=OnboardingDashboardAll.size();
	
		if(OnboardingDashboardList.size()==0)
		{
			dmRecords.append("<div class='row'>");
			dmRecords.append("<div class='col-sm-8 col-md-8 left5'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</div>");
			dmRecords.append("</div>");
		}else{
			int colorCounter=0,count=0;
			String colorName="";
		for (OnboardingDashboard onboardingDashboard : OnboardingDashboardList) 
		{
			if(colorCounter==0){
				colorName="white";
				colorCounter=1;
			}else{
				colorName="#dff0d8";
				colorCounter=0;
			}
			dmRecords.append("<div id='divId"+(count++)+"' class='row' style='width:101.1%;background-color:"+colorName+"; padding:2px 5px 2px 5px;margin-left:-5px;' onmouseover='onHoverHighLight(this);' onmouseout='onOutHighLight(this);'>");				
				dmRecords.append("<div class='col-sm-4 col-md-4'>"+onboardingDashboard.getOnboardingName()+"</div>");
				dmRecords.append("<div class='col-sm-2 col-md-2'></div>");

				dmRecords.append("<div class='col-sm-3 col-md-3'>"+Utility.convertDateAndTimeToUSformatOnlyDate(onboardingDashboard.getActivationDate())+"</div>");
			
			dmRecords.append("<div class='col-sm-2 col-md-2'>");
			if(onboardingDashboard.getStatus().equalsIgnoreCase("A"))
				dmRecords.append("Active");
			else
				dmRecords.append("Inactive");
			dmRecords.append("</div>");

			dmRecords.append("<div class='col-sm-1 col-md-1' style='text-align:left; padding:0px;'>");
			
			
				dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Edit' onclick=\"return editOnBoarding("+onboardingDashboard.getOnboardingId()+",2,"+onboardingDashboard.getDistrictMaster().getDistrictId()+",'"+onboardingDashboard.getDistrictMaster().getDistrictName().replace("'", "&#")+"');\"><i class='fa fa-pencil-square-o fa-lg'></i></a>&nbsp;&nbsp;");
			
				if(onboardingDashboard.getStatus().equalsIgnoreCase("A"))
					dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+msgActiveUser+"' onclick=\"return activateDeactivateOnBoarding("+onboardingDashboard.getOnboardingId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>&nbsp;&nbsp;");
				else
					dmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+msgInactiveUser+"' onclick=\"return activateDeactivateOnBoarding("+onboardingDashboard.getOnboardingId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>&nbsp;&nbsp;");
			
				dmRecords.append("<a href='javascript:void(0);'  data-toggle='tooltip' class='tempToolTip' data-placement='top' title='Clone' onclick=\"return editOnBoarding("+onboardingDashboard.getOnboardingId()+",1,"+onboardingDashboard.getDistrictMaster().getDistrictId()+",'"+onboardingDashboard.getDistrictMaster().getDistrictName().replace("'", "&#")+"');\"><i class='fa fa-files-o fa-lg'></i></a>");
				dmRecords.append("</div>");

			dmRecords.append("</div>");
			dmRecords.append("<div class='row' style='width:100%;margin-left:1px;'>");
	          dmRecords.append("<div class='col-sm-12 col-md-12' style='margin-left:6px;'></div>");
	          dmRecords.append("</div>");
		}
		dmRecords.append(PaginationAndSorting.getPaginationStringForManageDSPQAjaxUsingDiv(request,totalRecord,noOfRow.toString(), pageNo.toString()));
		}
	}catch (Exception e) 
	{
		e.printStackTrace();
	}
	return dmRecords.toString();
	}
	
	
	@Transactional(readOnly=false)
	public String saveOnBoarding(Integer districtId,String appliedAt,String onboardingName)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster 			= 	null;
		DistrictMaster districtMaster 	= null;	
		Integer onBoardId = null;
		Date currentDate	=	new Date();
		String ipAddress = IPAddressUtility.getIpAddress(request);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }else {
			userMaster = (UserMaster)session.getAttribute("userMaster");
		}
		try{	
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}	
			
			districtMaster = districtMasterDAO.findById(districtId, false, false);		
			OnboardingDashboard onboardingDashboardDuplicate=onboardingDashboardDAO.findByDistrictAndOnboardingName(districtMaster, onboardingName);
			if(onboardingDashboardDuplicate!=null && onboardingDashboardDuplicate.getOnboardingName().equalsIgnoreCase(onboardingName))
			{
				return "true";
			}
			else
			{
				OnboardingDashboard onboardingDashboard=new OnboardingDashboard();	
				onboardingDashboard.setDistrictMaster(districtMaster);
				onboardingDashboard.setBranchMaster(null);
				onboardingDashboard.setHeadQuarterMaster(null);
				onboardingDashboard.setOnboardingName(onboardingName);
				onboardingDashboard.setAppliedAt(appliedAt);
				onboardingDashboard.setActivationDate(null);
				onboardingDashboard.setCreatedDate(currentDate);
				onboardingDashboard.setCreatedByUser(userMaster.getEntityType());
				onboardingDashboard.setIpAddress(ipAddress);
				onboardingDashboard.setStatus("I");
				onboardingDashboardDAO.makePersistent(onboardingDashboard);
				onBoardId=onboardingDashboard.getOnboardingId();
			}
			
		}catch(Exception e){
			e.printStackTrace();}
	return (onBoardId.toString());
	}
	
	@Transactional(readOnly=false)
	public String updateOnBoarding(Integer districtId,Integer cloneId,Integer onboardID,String appliedAt,String onboardingName)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster 			= 	null;
		DistrictMaster districtMaster 	= null;	
		Integer onBoardId = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }else {
			userMaster = (UserMaster)session.getAttribute("userMaster");
		}
		try{	
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}	
			//System.out.println("Clone Id :"+cloneId+" District Id ::::: "+districtId+"  onBoarding Id :::: "+onboardID);
			OnboardingDashboard onboardingDashboardDuplicate=null;
			if(cloneId==1){
				districtMaster = districtMasterDAO.findById(districtId, false, false);	
				onboardingDashboardDuplicate=onboardingDashboardDAO.findByDistrictAndOnboardingName(districtMaster, onboardingName);
			}
			if(onboardingDashboardDuplicate!=null && onboardingDashboardDuplicate.getOnboardingName().equalsIgnoreCase(onboardingName))
			{
				return "true";
			}
			else
			{
			OnboardingDashboard onboardingDashboard=onboardingDashboardDAO.findById(onboardID, false, false);
				if(cloneId==1)
				{
					OnboardingDashboard onboardingDashboardClone = new OnboardingDashboard();
					BeanUtils.copyProperties(onboardingDashboard, onboardingDashboardClone);
					onboardingDashboardClone.setOnboardingId(null);
					onboardingDashboardClone.setOnboardingName(onboardingName);
					onboardingDashboardClone.setAppliedAt(appliedAt);					
					onboardingDashboardDAO.makePersistent(onboardingDashboardClone);
					onBoardId=onboardingDashboardClone.getOnboardingId();
				}
				else{
					onboardingDashboard.setOnboardingName(onboardingName);
					onboardingDashboard.setAppliedAt(appliedAt);
				    onboardingDashboardDAO.makePersistent(onboardingDashboard);
				    onBoardId=onboardID;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			}
	return (onBoardId.toString());
	}
	@Transactional(readOnly=false)
	public String editOnboarding(Integer onBoardingId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		String onboardArr= null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		OnboardingDashboard onboardingDashboard=null;
		if(onBoardingId!=0 && onBoardingId!=null)
		{			
			try{
				onboardingDashboard=onboardingDashboardDAO.findById(onBoardingId, false, false);
				if(onboardingDashboard!=null){
					onboardArr=onboardingDashboard.getOnboardingName()+"^"+onboardingDashboard.getAppliedAt();
				}
			}catch (Exception e) 
			{
				e.printStackTrace();				
			}
		}
		return onboardArr;
	}
	public boolean activateDeactivateOnBoarding(Integer onboardingId,String status)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		Date activateDate	=	new Date();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		try{
			OnboardingDashboard OnboardingDashboardName=onboardingDashboardDAO.findById(onboardingId, false, false);
			OnboardingDashboardName.setStatus(status);	
			if(status.equalsIgnoreCase("A"))
				OnboardingDashboardName.setActivationDate(activateDate);
			onboardingDashboardDAO.makePersistent(OnboardingDashboardName);
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	@Transactional(readOnly=false)
	public String checkPointStatusDefault(Integer districtId,Integer onBoardingId,Integer checkPointId,String candidateType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer CPSRecords =	new StringBuffer();
		UserMaster userMaster=null;
		String checkStatusName="";
		int count=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }else {
			userMaster = (UserMaster)session.getAttribute("userMaster");
		}
		try{	
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}	
		List<CheckPointStatusMaster> checkPointStatusMasterList=null;
		List<CheckPointStatusTransaction> checkPointStatusTransactionList=null;
		checkPointStatusTransactionList=checkPointStatusTransactionDAO.getCheckPointStatusRecord(districtId,onBoardingId,checkPointId,candidateType);
		//int counter =checkPointStatusTransactionDAO.countRowByCriterion(Restrictions.eq("districtId", districtId));
		if(checkPointStatusTransactionList.size()==0){
		checkPointStatusMasterList=checkPointStatusMasterDAO.getcheckpointstatus();
		if(checkPointStatusMasterList.size()>0){
			checkStatusName="";
			CPSRecords.append("<div class='row mb0 left5'>");
			CPSRecords.append("<div class='row'><div class='col-sm-2 col-md-2'> </div><div class='col-sm-1 col-md-1 showhideedit left15'><label>Edit</label></div>");
	        CPSRecords.append("<div class='col-sm-3 col-md-3 managewidth'></div><div class='col-sm-3 col-md-3'><label style='padding-left:10px;'>Properties</label></div></div>");
			for(CheckPointStatusMaster re: checkPointStatusMasterList){
				
				CPSRecords.append("<div class='row mb5 left5'>");
				CPSRecords.append("<div class='col-sm-2 col-md-2 pleftright' ><span id='name_"+count+"'>"+re.getCheckPointStatusName()+"</span>");
				CPSRecords.append(" <span class='tempToolTip' data-toggle='tooltip' data-placement='top' data-original-title='"+re.getCheckPointStatusName()+"' title='"+re.getCheckPointStatusName()+"'><img src='images/qua-icon.png'></span> </div>");
				CPSRecords.append("<div class='col-sm-1 col-md-1 showhideedit top5'><a class='tempToolTip' data-toggle='tooltip' data-placement='top' data-original-title='Edit' href='javascript:void(0);' onclick='editCheckPointStatus(1,"+re.getCheckPointStatusId()+","+count+");'><i class='fa fa-pencil-square-o fa-lg'></i></a></div>");
				CPSRecords.append("<div class='col-sm-3 col-md-3 managewidth'></div>");
				CPSRecords.append("<div class='col-sm-3 col-md-3 txtalign'>");
				CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+count+"dId' name='"+count+"DId' value='0'/>");
				CPSRecords.append(" <label class='circlegrey tempToolTip'  id='"+count+"D' data-toggle='tooltip' data-placement='top' data-original-title='Not selected as Decision Point'>D</label>");
				CPSRecords.append("</div>");
				
				CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+count+"CId' name='"+count+"CId' value='#808080'/>");
				CPSRecords.append(" <label class='circlegrey tempToolTip'  id='"+count+"C' data-toggle='tooltip' data-placement='top' data-original-title='No Color defined'>C</label>");
				CPSRecords.append("</div>");
				
				/*CPSRecords.append("<div class='lfloat'><input type='hidden' id='1"+count+count+"1' name='1"+count+count+"1' value=''/><input type='hidden' id='1"+count+count+"1id' name='1"+count+count+"1id' value='0'/>");
				CPSRecords.append(" <label class='circlegrey tempToolTip' onclick='clickStatusView(1,"+count+",2,1);' id='1"+count+"1' data-toggle='tooltip' data-placement='top' data-original-title='No Color defined'>C</label>");
				CPSRecords.append("</div>");*/
				
//				CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+count+"_"+count+"_1' value='A'/>");
//				CPSRecords.append("<label class='circlegrey green tempToolTip' id='"+count+"_1T' title='Active' onclick='deactivateStatusSection("+count+",1);' data-toggle='tooltip' data-placement='top' data-original-title='Active'>A</label></div> ");
//				CPSRecords.append("</div></div>");
				CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+count+"_"+count+"_1edit' value='A'/>");
				CPSRecords.append("<label class='circlegrey green tempToolTip' id='"+count+"_1T' title='Active' data-toggle='tooltip' data-placement='top' data-original-title='Active'>A</label></div> ");
				CPSRecords.append("</div></div>");
				count++;
				if(checkStatusName.equals(""))
					checkStatusName=re.getCheckPointStatusName();
				else
					checkStatusName+="#"+re.getCheckPointStatusName();
			}
			CPSRecords.append("<input type='hidden' id='checkStatsNa' value='"+checkStatusName+"'/>");
			CPSRecords.append("</div>");
			CPSRecords.append("<input type='hidden' id='checkStatsCounter' value='"+count+"'/>");
		 }
		}else{
			checkStatusName="";
			CPSRecords.append("<div class='row left5'>");
			CPSRecords.append("<div class='row'><div class='col-sm-2 col-md-2'> </div><div class='col-sm-1 col-md-1 showhideedit left15'><label>Edit</label></div>");
	        CPSRecords.append("<div class='col-sm-3 col-md-3 managewidth'></div><div class='col-sm-3 col-md-3'><label style='padding-left:10px;'>Properties</label></div></div>");
			for(CheckPointStatusTransaction re: checkPointStatusTransactionList){
				CPSRecords.append("<div class='row mb5 left5'>");
				CPSRecords.append("<div class='col-sm-2 col-md-2 pleftright'><span id='name_"+count+"'>"+re.getCpTransactionStatusName()+"</span>");
				CPSRecords.append(" <span class='tempToolTip' data-toggle='tooltip' data-placement='top' data-original-title='"+re.getCpTransactionStatusName()+"' title='"+re.getCpTransactionStatusName()+"'><img src='images/qua-icon.png'></span> </div>");
				CPSRecords.append("<div class='col-sm-1 col-md-1 showhideedit top5'><a class='tempToolTip' data-toggle='tooltip' data-placement='top' data-original-title='Edit' href='javascript:void(0);' onclick='editCheckPointStatus(1,"+re.getCpStatusTransactionId()+","+count+");'><i class='fa fa-pencil-square-o fa-lg'></i></a></div>");
				CPSRecords.append("<div class='col-sm-3 col-md-3 managewidth'></div>");
				CPSRecords.append("<div class='col-sm-3 col-md-3 txtalign'>");
				//CPSRecords.append("<div class='lfloat'><input type='hidden' id='1"+count+count+"1' name='1"+count+count+"1' value=''/><input type='hidden' id='1"+count+count+"1id' name='1"+count+count+"1id' value='0'/>");
				if(re.getCpDecisionPoint()==1){
					CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+count+"dId' name='"+count+"DId' value='1'/>");
					CPSRecords.append(" <label class='circlegrey green tempToolTip'  id='"+count+"D' data-toggle='tooltip' data-placement='top' data-original-title='Selected as Decision Point'>D</label>");
				}else{
					CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+count+"dId' name='"+count+"DId' value='0'/>");
					CPSRecords.append(" <label class='circlegrey tempToolTip'  id='"+count+"D' data-toggle='tooltip' data-placement='top' data-original-title='Not selected as Decision Point'>D</label>");
				}
				CPSRecords.append("</div>");
				
				CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+count+"CId' name='"+count+"CId' value='"+re.getCpColor()+"'/>");
				if(re.getCpColor()!=null && !re.getCpColor().equalsIgnoreCase("#D3D3D3"))
					CPSRecords.append(" <label class='circlegrey tempToolTip' style='background-color:"+re.getCpColor()+";' id='"+count+"C' data-toggle='tooltip' data-placement='top' data-original-title='Color defined'>C</label>");
				else
					CPSRecords.append(" <label class='circlegrey tempToolTip' id='"+count+"C' data-toggle='tooltip' data-placement='top' data-original-title='No Color defined'>C</label>");
				
				CPSRecords.append("</div>");
				if(re.getCpTransactionStatus().equalsIgnoreCase("A")){
					CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+count+"_"+count+"_1edit' value='A'/>");
				    CPSRecords.append("<label class='circlegrey green tempToolTip' id='"+count+"_1T' title='Active' data-toggle='tooltip' data-placement='top' data-original-title='Active'>A</label></div> ");
				}else{
					CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+count+"_"+count+"_1edit' value='I'/>");
					 CPSRecords.append("<label class='circlegrey tempToolTip' id='"+count+"_1T' title='Inactive' data-toggle='tooltip' data-placement='top' data-original-title='Inactive'>X</label></div> ");
				}
				CPSRecords.append("</div></div>");
				if(checkStatusName.equals(""))
					checkStatusName=re.getCpTransactionStatusName();
				else
					checkStatusName+="#"+re.getCpTransactionStatusName();
				count++;
			}
			CPSRecords.append("<input type='hidden' id='checkStatsNa' value='"+checkStatusName+"'/>");
			CPSRecords.append("</div>");
			CPSRecords.append("<input type='hidden' id='checkStatsCounter' value='"+count+"'/>");
			CPSRecords.append("<input type='hidden' id='editInitialCounter' value='"+count+"'/>");
		 }
	
	}catch(Exception e){
		e.printStackTrace();
		 return "false";
	}
	 return CPSRecords.toString();
	}

@Transactional(readOnly=false)
public Map<String,Object> getCheckPrintStatus(Integer id,Integer districtId,Integer onBoardingId,Integer checkPointId,String candidateType)
{
	Map<String,Object>  mapList=new HashMap<String, Object>();
	List<CheckPointStatusTransaction>  checkPointStatusTransactionList=null;
	try{
	checkPointStatusTransactionList=checkPointStatusTransactionDAO.getCheckPointStatusRecord(districtId,onBoardingId,checkPointId,candidateType);
	//int count =checkPointStatusTransactionDAO.countRowByCriterion(Restrictions.eq("districtId", districtId),Restrictions.eq("onboardingId", onBoardingId));
	if(checkPointStatusTransactionList.size()==0){
	CheckPointStatusMaster checkPointStatusMaster=new CheckPointStatusMaster();
	
		checkPointStatusMaster=checkPointStatusMasterDAO.findById(id, false, false);
		mapList.put("checkPointStatusId", checkPointStatusMaster.getCheckPointStatusId());
		mapList.put("checkPointStatusName",checkPointStatusMaster.getCheckPointStatusName());
		mapList.put("decisionPoint",checkPointStatusMaster.getDecisionPoint());
		mapList.put("color", checkPointStatusMaster.getColor());
		mapList.put("status", checkPointStatusMaster.getStatus());
	}else{
		CheckPointStatusTransaction checkPointStatusTransaction=new CheckPointStatusTransaction();
		checkPointStatusTransaction=checkPointStatusTransactionDAO.findById(id, false, false);
		mapList.put("checkPointStatusId", checkPointStatusTransaction.getCpStatusTransactionId());
		mapList.put("checkPointStatusName",checkPointStatusTransaction.getCpTransactionStatusName());
		mapList.put("decisionPoint",checkPointStatusTransaction.getCpDecisionPoint());
		mapList.put("color", checkPointStatusTransaction.getCpColor());
		mapList.put("status", checkPointStatusTransaction.getCpTransactionStatus());
	}
	}catch (Exception e) {
		e.printStackTrace();
	}
	return mapList;
}

public String saveCheckPrintStatus(Integer checkPointId,String candidateType,Integer statusId,Integer districtId,Integer onboardingId,String StatusName,Integer decisionpoint,String colorCode,String status,Integer actionType,Integer checkPointRecordStatusFlag)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	CheckPointStatusTransaction checkPointStatusTransaction=new CheckPointStatusTransaction();
	UserMaster userMaster 			= 	null;
	Date currentDate	=	new Date();
	String checkStatusId = null;
	String ipAddress = IPAddressUtility.getIpAddress(request);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException("Your session has expired!");
    }else {
		userMaster = (UserMaster)session.getAttribute("userMaster");
	}
	try{	
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}	
		List<CheckPointStatusTransaction> checkPointStatusTransactionList=null;
		//int count =checkPointStatusTransactionDAO.countRowByCriterion(Restrictions.eq("districtId", districtId));
		
		checkPointStatusTransactionList=checkPointStatusTransactionDAO.getCheckPointStatusRecord(districtId,onboardingId,checkPointId,candidateType);
		if(checkPointRecordStatusFlag==0){
			Integer noOfRecords = checkPointTransactionDAO.getNoOfRecords();
			saveCheckPoint(districtId,onboardingId,null,0,null);
		
		checkPointId=noOfRecords+checkPointId;
		}
		
		if(checkPointStatusTransactionList.size()>0){
			if(actionType==1){
				checkPointStatusTransaction.setCpTransactionStatusName(StatusName);
				checkPointStatusTransaction.setCpStatusMasterId(null);
				checkPointStatusTransaction.setOnboardingId(onboardingId);
				checkPointStatusTransaction.setCheckPointId(checkPointId);
				checkPointStatusTransaction.setCandidateType(candidateType);
				checkPointStatusTransaction.setCpColor(colorCode);
				checkPointStatusTransaction.setCpDecisionPoint(decisionpoint);
				checkPointStatusTransaction.setCpTransactionStatus(status);
				checkPointStatusTransaction.setStatus("A");
				checkPointStatusTransaction.setCreatedDate(currentDate);
				checkPointStatusTransaction.setIpAddress(ipAddress);
				checkPointStatusTransaction.setDistrictId(districtId);
				checkPointStatusTransaction.setCreatedByUser(userMaster.getEntityType().toString());
				checkPointStatusTransactionDAO.makePersistent(checkPointStatusTransaction);
				checkStatusId=checkPointStatusTransaction.getCpStatusTransactionId().toString();
			}else{
				checkPointStatusTransaction=checkPointStatusTransactionDAO.findById(statusId, false, false);
				checkPointStatusTransaction.setCpStatusTransactionId(statusId);
				checkPointStatusTransaction.setCpTransactionStatusName(StatusName);
				checkPointStatusTransaction.setCheckPointId(checkPointId);
				checkPointStatusTransaction.setCandidateType(candidateType);
				checkPointStatusTransaction.setCpColor(colorCode);
				checkPointStatusTransaction.setCpDecisionPoint(decisionpoint);
				checkPointStatusTransaction.setCpTransactionStatus(status);
				checkPointStatusTransaction.setStatus("A");
				checkPointStatusTransaction.setCreatedDate(currentDate);
				checkPointStatusTransaction.setIpAddress(ipAddress);
				checkPointStatusTransaction.setDistrictId(districtId);
				checkPointStatusTransaction.setCreatedByUser(userMaster.getEntityType().toString());
				checkPointStatusTransactionDAO.makePersistent(checkPointStatusTransaction);
				checkStatusId=checkPointStatusTransaction.getCpStatusTransactionId().toString();
			}
		}else{
			int counter=0;
			SessionFactory factory = checkPointStatusTransactionDAO.getSessionFactory();
		    StatelessSession statelessSession=factory.openStatelessSession();
		    Transaction transaction = statelessSession.beginTransaction();
			List<CheckPointStatusMaster> checkPointStatusMasterList=null;
			checkPointStatusMasterList=checkPointStatusMasterDAO.getcheckpointstatus();
			for(CheckPointStatusMaster re:checkPointStatusMasterList){
				if(re.getCheckPointStatusId().equals(statusId)&& actionType!=1){
					//System.out.println(StatusName+"dfasssssss"+re.getCheckPointStatusName().toString()+"dsfaa");
					if(StatusName.equalsIgnoreCase(re.getCheckPointStatusName())){
					checkPointStatusTransaction.setCpStatusMasterId(re.getCheckPointStatusId());
					}
					checkPointStatusTransaction.setDistrictId(districtId);
					checkPointStatusTransaction.setOnboardingId(onboardingId);
					checkPointStatusTransaction.setCheckPointId(checkPointId);
					checkPointStatusTransaction.setCandidateType(candidateType);
					checkPointStatusTransaction.setCpTransactionStatusName(StatusName);
					checkPointStatusTransaction.setCpColor(colorCode);
					checkPointStatusTransaction.setCpDecisionPoint(decisionpoint);
					checkPointStatusTransaction.setCpTransactionStatus(status);
					checkPointStatusTransaction.setStatus("A");
					checkPointStatusTransaction.setCreatedDate(currentDate);
					checkPointStatusTransaction.setCreatedByUser(userMaster.getUserId().toString());
					checkPointStatusTransaction.setIpAddress(ipAddress);
				}else{
					//System.out.println("fdsfd"+re.getCheckPointStatusId());
					checkPointStatusTransaction.setCpStatusMasterId(re.getCheckPointStatusId());
					checkPointStatusTransaction.setDistrictId(districtId);
					checkPointStatusTransaction.setOnboardingId(onboardingId);
					checkPointStatusTransaction.setCheckPointId(checkPointId);
					checkPointStatusTransaction.setCandidateType(candidateType);
					checkPointStatusTransaction.setCpTransactionStatusName(re.getCheckPointStatusName());
					checkPointStatusTransaction.setCpColor(re.getColor());
					checkPointStatusTransaction.setCpDecisionPoint(Integer.parseInt(re.getDecisionPoint()));
					checkPointStatusTransaction.setCpTransactionStatus(re.getStatus());
					checkPointStatusTransaction.setStatus("A");
					checkPointStatusTransaction.setCreatedDate(re.getCreatedDate());
					checkPointStatusTransaction.setCreatedByUser(userMaster.getUserId().toString());
					checkPointStatusTransaction.setIpAddress(ipAddress);	
				}
				counter++;
				statelessSession.insert(checkPointStatusTransaction);
			}
			if(actionType==1){
				checkPointStatusTransaction.setCpStatusMasterId(null);
				checkPointStatusTransaction.setDistrictId(districtId);
				checkPointStatusTransaction.setOnboardingId(onboardingId);
				checkPointStatusTransaction.setCheckPointId(checkPointId);
				checkPointStatusTransaction.setCandidateType(candidateType);
				checkPointStatusTransaction.setCpTransactionStatusName(StatusName);
				checkPointStatusTransaction.setCpColor(colorCode);
				checkPointStatusTransaction.setCpDecisionPoint(decisionpoint);
				checkPointStatusTransaction.setCpTransactionStatus(status);
				checkPointStatusTransaction.setStatus("A");
				checkPointStatusTransaction.setCreatedDate(currentDate);
				checkPointStatusTransaction.setCreatedByUser(userMaster.getUserId().toString());
				checkPointStatusTransaction.setIpAddress(ipAddress);
				statelessSession.insert(checkPointStatusTransaction);
			}
			transaction.commit();
			statelessSession.close();	
		}
	}catch (Exception e) {
		e.printStackTrace();
		return "false";
	}
	return checkStatusId;
}
@Transactional(readOnly=false)
public String checkPointDefault(Integer districtId,Integer onBoardingId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	StringBuffer CPSRecords =	new StringBuffer();
	int count=1,candidateCount=0;
	int recordCount=0;
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException("Your session has expired!");
    }
	try{	
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		List<CheckPointMaster> checkPointMasterList=null;
		List<CheckPointTransaction> checkPointTransactionList=null;
		recordCount=checkPointTransactionDAO.getCheckPointTransactionCount(districtId, onBoardingId);
		String checkPointName="";
		String checkPointids="";
		int counter=0;
		if(recordCount<=0){
			int checkPointId=0;
			 checkPointId=checkPointTransactionDAO.getNoOfRecords();
			 checkPointId+=1;
			Map<String,CheckPointStatusTransaction> mapValue=new HashMap<String, CheckPointStatusTransaction>();
		checkPointMasterList=checkPointMasterDAO.getcheckpoints();
		if(checkPointMasterList.size()>0){
			CPSRecords.append("<input type='hidden' id='checkPointRecordFlag' name='checkPointRecordFlag' value='0'>");
			
			for(CheckPointMaster re: checkPointMasterList){
				if(counter==0){
					checkPointName=re.getCheckPointName();
					checkPointids=checkPointId+"";
					counter=1;
				}else{
					checkPointName+="####"+re.getCheckPointName();
					checkPointids+="##"+checkPointId+"";
				}
			CPSRecords.append("<div class='row mb5'>");
			CPSRecords.append("<div class='col-sm-3 col-md-3 pleftright' style='padding-top: 1px;'><span >"+re.getCheckPointName()+"</span>");
			CPSRecords.append(" <span class='tempToolTip' data-toggle='tooltip' data-placement='top' data-original-title='"+re.getCheckPointName()+"'><img src='images/qua-icon.png'></span></div>");
			CPSRecords.append("<div class='col-sm-1 col-md-1 showhideedit top10'><a href='javascript:void(0);' title='Edit' onclick=\"addEditCheckPoint("+checkPointId+",'"+re.getCheckPointName().replace("'", "&#")+"');\"><i class='fa fa-pencil-square-o fa-lg'></i></a></div>");
			CPSRecords.append("<div class='col-sm-2 col-md-2' id='extraWidth"+(candidateCount++)+"'></div>");
			count=1;
			CPSRecords.append("<div class='col-sm-2 col-md-2 txtalign externalDiv'>");
			CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+count+"EE1' name='"+checkPointId+"EE1'/><input type='hidden' id='"+checkPointId+count+"EEid1' name='"+checkPointId+"EEid1' value='0'/>");
			CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"clickView("+checkPointId+",1,"+count+",'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+checkPointId+(count++)+"E1' data-toggle='tooltip' data-placement='top' data-original-title='Checkpoint defined'>S</label></div>");
			CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+count+"EE2' name='"+checkPointId+"EE2'/><input type='hidden' id='"+checkPointId+count+"EEid2' name='"+checkPointId+"EEid2' value='0'/>");
			CPSRecords.append("<label class='circlegrey grey tempToolTip' onclick=\"clickView("+checkPointId+",1,"+count+",'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+checkPointId+(count++)+"E2' data-toggle='tooltip' data-placement='top' data-original-title='No Communication defined'>C</label></div>");
			CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+count+"EE3' name='"+checkPointId+"EE3' value='0'/><input type='hidden' id='"+checkPointId+count+"EEid3' name='"+checkPointId+"EEid3' value='0'/>");
			CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"actionClickView('',"+checkPointId+",1,"+count+",'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+checkPointId+(count++)+"E3' data-toggle='tooltip' data-placement='top' data-original-title='Action defined'>N</label></div>");
			CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+count+"EE4' name='"+checkPointId+"EE4'/><input type='hidden' id='"+checkPointId+count+"EEid4' name='"+checkPointId+"EEid4' value='0'/>");
		    CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"clickView("+checkPointId+",1,"+count+",'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+checkPointId+(count++)+"E4' data-toggle='tooltip' data-placement='top' data-original-title='Default permissions set'>P</label></div>");
			CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+"_"+checkPointId+"_1' value='1'/>");
			CPSRecords.append("<label class='circlegrey green tempToolTip' id='"+checkPointId+"_1' title='Active' onclick='deactivateSection("+checkPointId+",1);' data-toggle='tooltip' data-placement='top' data-original-title='Active'>A</label></div></div>");
			count=1;
			CPSRecords.append("<div class='col-sm-2 col-md-2 txtalign internalDiv'>");
			CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+count+"II1' name='"+checkPointId+"II1'/><input type='hidden' id='"+checkPointId+count+"IIid1' name='"+checkPointId+"IIid1' value='0'/>");
			CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+checkPointId+(count++)+"I1' data-toggle='tooltip' data-placement='top' data-original-title='Checkpoint disabled'>S</label></div>");
		    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+count+"II2' name='"+checkPointId+"II2'/><input type='hidden' id='"+checkPointId+count+"IIid2' name='"+checkPointId+"IIid2' value='0'/>");
		    CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+checkPointId+(count++)+"I2' data-toggle='tooltip' data-placement='top' data-original-title='Communication disabled'>C</label></div>");
		    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+count+"II3' name='"+checkPointId+"II3' value='0'/><input type='hidden' id='"+checkPointId+count+"IIid3' name='"+checkPointId+"IIid3' value='0'/>");
		    CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+checkPointId+(count++)+"I3' data-toggle='tooltip' data-placement='top' data-original-title='Action disabled'>N</label></div>");
		    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+count+"II4' name='"+checkPointId+"II4'/><input type='hidden' id='"+checkPointId+count+"IIid4' name='"+checkPointId+"IIid4' value='0'/>");
		    CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+checkPointId+(count++)+"I4' data-toggle='tooltip' data-placement='top' data-original-title='permissions disabled'>P</label></div>");
		    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+"_"+checkPointId+"_2' value='1'/>");
		    CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+checkPointId+"_2' title='Inactive' data-toggle='tooltip' data-placement='top' data-original-title='Inactive'>I</label></div></div>");
		    count=1;
		    CPSRecords.append("<div class='col-sm-2 col-md-2 txtalign internalTDiv'>");
		    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+count+"TT1' name='"+checkPointId+"TT1'/><input type='hidden' id='"+checkPointId+count+"TTid1' name='"+checkPointId+count+"TTid1' value='0'/>");
			CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+checkPointId+(count++)+"T1' data-toggle='tooltip' data-placement='top' data-original-title='Checkpoint disabled'>S</label></div>");
		    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+count+"TT2' name='"+checkPointId+"TT2'/><input type='hidden' id='"+checkPointId+count+"TTid2' name='"+checkPointId+count+"TTid2' value='0'/>");
		    CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+checkPointId+(count++)+"T2' data-toggle='tooltip' data-placement='top' data-original-title='Communication disabled'>C</label></div>");
		    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+count+"TT3' name='"+checkPointId+"TT3' value='0'/><input type='hidden' id='"+checkPointId+count+"TTid3' name='"+checkPointId+count+"TTid3' value='0'/>");
		    CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+checkPointId+(count++)+"T3' data-toggle='tooltip' data-placement='top' data-original-title='Action disabled'>N</label></div>");
		    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+count+"TT4' name='"+checkPointId+"TT4'/><input type='hidden' id='"+checkPointId+count+"TTid4' name='"+checkPointId+count+"TTid4' value='0'/>");
		    CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+checkPointId+(count++)+"I4' data-toggle='tooltip' data-placement='top' data-original-title='permissions disabled'>P</label></div>");
		    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+checkPointId+"_"+checkPointId+"_3' value='1'/>");
		    CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+checkPointId+"_3' title='Inactive' data-toggle='tooltip' data-placement='top' data-original-title='Inactive'>I</label></div>");        
		    CPSRecords.append("</div></div>");
		    checkPointId++;
		    }
			 CPSRecords.append("<input type='hidden' id='checkPointIds' value='"+checkPointids+"'/>");
			 CPSRecords.append("<input type='hidden' id='checkPointName' value='"+checkPointName+"'/>");
			 CPSRecords.append("<input type='hidden' id='checkPointRecordCount' value='"+candidateCount+"'/>");
		}
		}else{
			Map<String,String> checkPointMap=new HashMap<String, String>();
			Map<String,String> checkPointCofigureMap=new HashMap<String, String>();
			Map<String,Integer> checkPointActionMap=new HashMap<String, Integer>();
			Map<String,Integer> checkPointStatusMap=new HashMap<String, Integer>();
			Map<String,Integer> checkPointActionIdMap=new HashMap<String, Integer>();
			Map<String,String> checkPointCommunicationMap=new HashMap<String, String>();
			Map<String,String> checkPointCommunicationStatusMap=new HashMap<String, String>();
			Map<String,String> checkPointPermissionMap=new HashMap<String, String>();
			
			List<CheckPointStatusTransaction> checkPointStatusList=checkPointStatusTransactionDAO.getCheckPointStatusRecordCount(districtId, onBoardingId);
			for(CheckPointStatusTransaction rec: checkPointStatusList){
				checkPointMap.put(rec.getCheckPointId()+"#"+rec.getCandidateType(), rec.getCandidateType());
				checkPointCofigureMap.put(rec.getCheckPointId()+"#"+rec.getCandidateType(), rec.getStatus());
			}
			
			List<CheckPointAction> checkPointActionList=checkPointActionDAO.getCheckPointActionRecordCount(districtId, onBoardingId);
			for(CheckPointAction rec: checkPointActionList){
				checkPointActionMap.put(rec.getCheckPointId()+"#"+rec.getCandidateType(), rec.getAction());
				checkPointStatusMap.put(rec.getCheckPointId()+"#"+rec.getCandidateType(), rec.getCheckPointStatus());
				checkPointActionIdMap.put(rec.getCheckPointId()+"#"+rec.getCandidateType(), rec.getActionId());
				
				
			}
			
			List<OnBoardingCommunication> onBoardingCommunicationList=onBoardingCommunicationDAO.findCommunicationByDistrictAndOnboardingId(districtId, onBoardingId);
			for(OnBoardingCommunication rec: onBoardingCommunicationList){
				checkPointCommunicationMap.put(rec.getCheckPointId()+"#"+rec.getCandidateType(), rec.getCandidateType());
				checkPointCommunicationStatusMap.put(rec.getCheckPointId()+"#"+rec.getCandidateType(), rec.getStatus());
			}
			
			List<CheckPointPermissionTransaction> checkPointPermissionList=checkPointPermissionTransactionDAO.findcheckPointPermissionByDistrictAndOnboardingId(districtId, onBoardingId);
			for(CheckPointPermissionTransaction rec: checkPointPermissionList){
				checkPointPermissionMap.put(rec.getCheckPointId()+"#"+rec.getCandidateType(), rec.getCandidateType());
			}
			
			checkPointTransactionList=checkPointTransactionDAO.getCheckPointRecord(districtId, onBoardingId);
			CPSRecords.append("<input type='hidden' id='checkPointRecordFlag' name='checkPointRecordFlag' value='1'>");
			for(CheckPointTransaction re: checkPointTransactionList){
				if(counter==0){
					checkPointName=re.getCheckPointName();
					checkPointids=re.getCheckPointTransactionId().toString();
					counter=1;
				}else{
					checkPointName+="####"+re.getCheckPointName();
					checkPointids+="##"+re.getCheckPointTransactionId().toString();
				}
				CPSRecords.append("<div class='row mb5'>");
				CPSRecords.append("<div class='col-sm-3 col-md-3 pleftright' style='padding-top: 1px;'><span >"+re.getCheckPointName()+"</span>");
				CPSRecords.append(" <span class='tempToolTip' data-toggle='tooltip' data-placement='top' data-original-title='"+re.getCheckPointName()+"'><img src='images/qua-icon.png'></span></div>");
				CPSRecords.append("<div class='col-sm-1 col-md-1 showhideedit top10'><a href='javascript:void(0);' title='Edit' onclick=\"addEditCheckPoint("+re.getCheckPointTransactionId()+",'"+re.getCheckPointName().replace("'", "&#")+"');\"><i class='fa fa-pencil-square-o fa-lg'></i></a></div>");
				CPSRecords.append("<div class='col-sm-2 col-md-2' id='extraWidth"+(candidateCount++)+"'></div>");
				count=1;
				CPSRecords.append("<div class='col-sm-2 col-md-2 txtalign externalDiv'>");
				
				CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"EE1' name='"+re.getCheckPointTransactionId()+"EE1'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"EEid1' name='"+re.getCheckPointTransactionId()+"EEid1' value='1'/>");
				CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",1,"+count+",'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+(count++)+"E1' data-toggle='tooltip' data-placement='top' data-original-title='Checkpoint defined'>S</label></div>");
				if(checkPointCommunicationMap.get(re.getCheckPointTransactionId()+"#E")!=null){
					if(checkPointCommunicationStatusMap.get(re.getCheckPointTransactionId()+"#E").equals("A")){
						CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"EE2' name='"+re.getCheckPointTransactionId()+"EE2'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"EEid2' name='"+re.getCheckPointTransactionId()+"EEid2' value='1'/>");
						CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",1,'2','"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+(count++)+"E2' data-toggle='tooltip' data-placement='top' data-original-title='Communication defined'>C</label></div>");
					}else{
						CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"EE2' name='"+re.getCheckPointTransactionId()+"EE2'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"EEid2' name='"+re.getCheckPointTransactionId()+"EEid2' value='0'/>");
						CPSRecords.append("<label class='circlegrey grey tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",1,'2','"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+(count++)+"E2' data-toggle='tooltip' data-placement='top' data-original-title='No Communication defined'>C</label></div>");
					}
				}else{
						CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"EE2' name='"+re.getCheckPointTransactionId()+"EE2'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"EEid2' name='"+re.getCheckPointTransactionId()+"EEid2' value='0'/>");
						CPSRecords.append("<label class='circlegrey grey tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",1,'2','"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+(count++)+"E2' data-toggle='tooltip' data-placement='top' data-original-title='No Communication defined'>C</label></div>");
					}
				
				if(checkPointActionMap.get(re.getCheckPointTransactionId()+"#E")!=null){
						CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"3EE3' name='"+re.getCheckPointTransactionId()+"EE3' value='"+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#E")+"'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"EEid3' name='"+re.getCheckPointTransactionId()+"EEid3' value='1'/>");
						CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"actionClickView("+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#E")+","+re.getCheckPointTransactionId()+",1,3,'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+"3E3' data-toggle='tooltip' data-placement='top' data-original-title='Action required'>N</label></div>");
				}else{
						CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"3EE3' name='"+re.getCheckPointTransactionId()+"EE3' value='0'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"EEid3' name='"+re.getCheckPointTransactionId()+"EEid3' value='1'/>");
						CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"actionClickView('',"+re.getCheckPointTransactionId()+",1,3,'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+"3E3' data-toggle='tooltip' data-placement='top' data-original-title='Action set'>N</label></div>");
				}
				
				CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"EE4' name='"+re.getCheckPointTransactionId()+"EE4'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"EEid4' name='"+re.getCheckPointTransactionId()+"EEid4' value='1'/>");
			    CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",1,"+count+",'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+(count++)+"E4' data-toggle='tooltip' data-placement='top' data-original-title='Permissions set'>P</label></div>");
			   
			    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"_"+re.getCheckPointTransactionId()+"_1' value='1'/>");
				CPSRecords.append("<label class='circlegrey green tempToolTip' id='"+re.getCheckPointTransactionId()+"_1' title='Active' onclick='deactivateSection("+re.getCheckPointTransactionId()+",1);' data-toggle='tooltip' data-placement='top' data-original-title='Active'>A</label></div></div>");
				
				count=1;
				CPSRecords.append("<div class='col-sm-2 col-md-2 txtalign internalDiv'>");
				if(checkPointMap.get(re.getCheckPointTransactionId()+"#I")!=null){
					if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#I").equals("A")){
						CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"II1' name='"+re.getCheckPointTransactionId()+"II1'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"IIid1' name='"+re.getCheckPointTransactionId()+"IIid1' value='1'/>");
					    CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",2,"+count+",'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+(count++)+"I1' data-toggle='tooltip' data-placement='top' data-original-title='Checkpoint defined'>S</label></div>");
					}else{
						CPSRecords.append("<div class='lfloat'><label class='circleNotAllowed grey tempToolTip' >S</label></div>");
					}
				}else{
						CPSRecords.append("<div class='lfloat'><label class='circlegrey tempToolTip circleNotAllowed' data-toggle='tooltip' data-placement='top' data-original-title='Checkpoint disabled'>S</label></div>");
				}
				if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#I")!=null && checkPointCommunicationStatusMap.get(re.getCheckPointTransactionId()+"#I")!=null){
					if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#I").equalsIgnoreCase("A")){
					    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"II2' name='"+re.getCheckPointTransactionId()+"II2'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"IIid2' name='"+re.getCheckPointTransactionId()+"IIid2' value='1'/>");
					    CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",2,'2','"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+(count++)+"I2' data-toggle='tooltip' data-placement='top' data-original-title='Communication defined'>C</label></div>");
					}else{
						CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"II2' name='"+re.getCheckPointTransactionId()+"II2'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"IIid2' name='"+re.getCheckPointTransactionId()+"IIid2' value='0'/>");
					    CPSRecords.append("<label class='circlegrey tempToolTip' id='"+re.getCheckPointTransactionId()+(count++)+"I2' data-toggle='tooltip' data-placement='top' data-original-title='No Communication defined'>C</label></div>");
					}
				}else{
					if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#I")!=null){
						CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"II2' name='"+re.getCheckPointTransactionId()+"II2'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"IIid2' name='"+re.getCheckPointTransactionId()+"IIid2' value='0'/>");
					    CPSRecords.append("<label class='circlegrey tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",2,'2','"+re.getCheckPointName().replace("'", "&#")+"');\" data-toggle='tooltip' data-placement='top' data-original-title='No Communication set'>C</label></div>");
					}else{
						CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"II2' name='"+re.getCheckPointTransactionId()+"II2'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"IIid2' name='"+re.getCheckPointTransactionId()+"IIid2' value='0'/>");
					    CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+re.getCheckPointTransactionId()+(count++)+"I2' data-toggle='tooltip' data-placement='top' data-original-title='Communication disabled'>C</label></div>");
					}
				}
				if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#I")!=null & checkPointActionMap.get(re.getCheckPointTransactionId()+"#I")!=null){
					if(checkPointActionMap.get(re.getCheckPointTransactionId()+"#I")==1){
						CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"3II3' name='"+re.getCheckPointTransactionId()+"II3' value='"+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#I")+"'/><input type='hidden' id='"+re.getCheckPointTransactionId()+"3IIid3' name='"+re.getCheckPointTransactionId()+"IIid3' value='1'/>");
				    	CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"actionClickView("+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#I")+","+re.getCheckPointTransactionId()+",2,3,'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+"3I3' data-toggle='tooltip' data-placement='top' data-original-title='Action required'>N</label></div>");
					}
					else if(checkPointActionMap.get(re.getCheckPointTransactionId()+"#I")==0){
						CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"3II3' name='"+re.getCheckPointTransactionId()+"II3' value='"+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#I")+"'/><input type='hidden' id='"+re.getCheckPointTransactionId()+"3IIid3' name='"+re.getCheckPointTransactionId()+"IIid3' value='0'/>");
				    	CPSRecords.append("<label class='circlegrey tempToolTip' onclick=\"actionClickView("+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#I")+","+re.getCheckPointTransactionId()+",2,3,'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+"3I3' data-toggle='tooltip' data-placement='top' data-original-title='No Action set'>N</label></div>");
					}
			    }else{
			    	if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#I")!=null){
			    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"3II3' name='"+re.getCheckPointTransactionId()+"II3' value='"+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#I")+"'/><input type='hidden' id='"+re.getCheckPointTransactionId()+"3IIid3' name='"+re.getCheckPointTransactionId()+"IIid3' value='0'/>");
					    CPSRecords.append("<label class='circlegrey tempToolTip' onclick=\"actionClickView('',"+re.getCheckPointTransactionId()+",2,3,'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+"3I3' data-toggle='tooltip' data-placement='top' data-original-title='No action set'>N</label></div>");
			    	}else{
			    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"3II3' name='"+re.getCheckPointTransactionId()+"II3' value='"+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#I")+"'/><input type='hidden' id='"+re.getCheckPointTransactionId()+"3IIid3' name='"+re.getCheckPointTransactionId()+"IIid3' value='0'/>");
				    	CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed'  id='"+re.getCheckPointTransactionId()+"3I3' data-toggle='tooltip' data-placement='top' data-original-title='Action disabled'>N</label></div>");
			    	}
			    }
			    
				if(checkPointPermissionMap.get(re.getCheckPointTransactionId()+"#I")!=null){
					    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+++"II4' name='"+re.getCheckPointTransactionId()+"II4'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+++"IIid4' name='"+re.getCheckPointTransactionId()+"IIid4' value='1'/>");
					    CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",2,4,'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+"4I4' data-toggle='tooltip' data-placement='top' data-original-title='Permissions set'>P</label></div>");
			    }else{
				    	CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"II4' name='"+re.getCheckPointTransactionId()+"II4'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"IIid4' name='"+re.getCheckPointTransactionId()+"IIid4' value='0'/>");
					    CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' onclick=\"clickView("+re.getCheckPointTransactionId()+",2,0,'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+"4I4' data-toggle='tooltip' data-placement='top' data-original-title='Permissions disabled'>P</label></div>");
			    }
				
				 if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#I")!=null & checkPointStatusMap.get(re.getCheckPointTransactionId()+"#I")!=null){
				    	if(checkPointStatusMap.get(re.getCheckPointTransactionId()+"#I")==1){
				    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"_"+re.getCheckPointTransactionId()+"_2' value='1'/>");
					    	CPSRecords.append("<label class='circlegrey green tempToolTip' id='"+re.getCheckPointTransactionId()+"_2' title='Active' onclick='deactivateSection("+re.getCheckPointTransactionId()+",2);' data-toggle='tooltip' data-placement='top' data-original-title='Active'>A</label></div></div>");
				    	}
				    	else if(checkPointStatusMap.get(re.getCheckPointTransactionId()+"#I")==0){
				    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"_"+re.getCheckPointTransactionId()+"_2' value='0'/>");
					    	CPSRecords.append("<label class='circlegrey  tempToolTip' id='"+re.getCheckPointTransactionId()+"_2' title='Inactive' onclick='deactivateSection("+re.getCheckPointTransactionId()+",2);' data-toggle='tooltip' data-placement='top' data-original-title='Inactive'>I</label></div></div>");
				    	}
				    }else{
				    	if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#I")!=null){
					    	CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"_"+re.getCheckPointTransactionId()+"_2' value='0'/>");
					    	CPSRecords.append("<label class='circlegrey tempToolTip' id='"+re.getCheckPointTransactionId()+"_2' onclick='deactivateSection("+re.getCheckPointTransactionId()+",2);' title='Inactive' data-toggle='tooltip' data-placement='top' data-original-title='Inactive'>I</label></div></div>");
				    	}else{
				    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"_"+re.getCheckPointTransactionId()+"_2' value='0'/>");
					    	CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+re.getCheckPointTransactionId()+"_2' title='Inactive' data-toggle='tooltip' data-placement='top' data-original-title='Inactive'>I</label></div></div>");
				    	}
				    }
			    count=1;
			    CPSRecords.append("<div class='col-sm-2 col-md-2 txtalign internalTDiv'>");
			    if(checkPointMap.get(re.getCheckPointTransactionId()+"#T")!=null){
			    	if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#T").equals("A")){
					    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"TT1' name='"+re.getCheckPointTransactionId()+"TT1'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"TTid1' name='"+re.getCheckPointTransactionId()+count+"TTid1' value='1'/>");
					    CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",3,"+count+",'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+(count++)+"T1' data-toggle='tooltip' data-placement='top' data-original-title='Checkpoint defined'>S</label></div>");
			    	}else{
			    		CPSRecords.append("<div class='lfloat'><label class='circleNotAllowed grey tempToolTip' >S</label></div>");
			    	}
			    }else{
			    		CPSRecords.append("<div class='lfloat'><label class='circlegrey tempToolTip circleNotAllowed' data-toggle='tooltip' data-placement='top' data-original-title='Checkpoint disabled' >S</label></div>");
			    }
			    
			    if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#T")!=null & checkPointCommunicationStatusMap.get(re.getCheckPointTransactionId()+"#T")!=null){
			    	if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#T").equalsIgnoreCase("A")){
				    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"TT2' name='"+re.getCheckPointTransactionId()+"TT2'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"TTid2' name='"+re.getCheckPointTransactionId()+count+"TTid2' value='1'/>");
				    CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",3,'2','"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+(count++)+"T2' data-toggle='tooltip' data-placement='top' data-original-title='Communication defined'>C</label></div>");
			    	}else{
			    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"TT2' name='"+re.getCheckPointTransactionId()+"TT2'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"TTid2' name='"+re.getCheckPointTransactionId()+count+"TTid2' value='0'/>");
					    CPSRecords.append("<label class='circlegrey tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",3,'2','"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+(count++)+"T2' data-toggle='tooltip' data-placement='top' data-original-title='No Communication defined'>C</label></div>");
			    	}
			    }else{
			    	if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#T")!=null){
				    	CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"TT2' name='"+re.getCheckPointTransactionId()+"TT2'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"TTid2' name='"+re.getCheckPointTransactionId()+count+"TTid2' value='0'/>");
					    CPSRecords.append("<label class='circlegrey tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",3,'2','"+re.getCheckPointName().replace("'", "&#")+"');\" data-toggle='tooltip' data-placement='top' data-original-title='No Communication set'>C</label></div>");
			    	}else{
			    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"TT2' name='"+re.getCheckPointTransactionId()+"TT2'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"TTid2' name='"+re.getCheckPointTransactionId()+count+"TTid2' value='0'/>");
					    CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' data-toggle='tooltip' data-placement='top' data-original-title='Communication disabled'>C</label></div>");
			    	}
			    }
			    
			    if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#T")!=null & checkPointActionMap.get(re.getCheckPointTransactionId()+"#T")!=null){
			    	if(checkPointActionMap.get(re.getCheckPointTransactionId()+"#T")==1){
			    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"3TT3' name='"+re.getCheckPointTransactionId()+"TT3' value='"+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#T")+"'/><input type='hidden' id='"+re.getCheckPointTransactionId()+"3TTid3' name='"+re.getCheckPointTransactionId()+count+"TTid3' value='1'/>");
				    	CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"actionClickView("+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#T")+","+re.getCheckPointTransactionId()+",3,3,'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+"3T3' data-toggle='tooltip' data-placement='top' data-original-title='Action required'>N</label></div>");
			    	}
			    	else if(checkPointActionMap.get(re.getCheckPointTransactionId()+"#T")==0){
			    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"3TT3' name='"+re.getCheckPointTransactionId()+"TT3' value='"+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#T")+"'/><input type='hidden' id='"+re.getCheckPointTransactionId()+"3TTid3' name='"+re.getCheckPointTransactionId()+count+"TTid3' value='0'/>");
					    CPSRecords.append("<label class='circlegrey tempToolTip' onclick=\"actionClickView("+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#T")+","+re.getCheckPointTransactionId()+",3,3,'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+"3T3' data-toggle='tooltip' data-placement='top' data-original-title='No action set'>N</label></div>");
			    	}
			    	
			    }else{
			    	if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#T")!=null){
			    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"3TT3' name='"+re.getCheckPointTransactionId()+"TT3' value='"+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#T")+"'/><input type='hidden' id='"+re.getCheckPointTransactionId()+"3TTid3' name='"+re.getCheckPointTransactionId()+count+"TTid3' value='0'/>");
					    CPSRecords.append("<label class='circlegrey tempToolTip' onclick=\"actionClickView("+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#T")+","+re.getCheckPointTransactionId()+",3,3,'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+"3T3' data-toggle='tooltip' data-placement='top' data-original-title='No action set'>N</label></div>");
			    	}else{
			    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"3TT3' name='"+re.getCheckPointTransactionId()+"TT3' value='"+checkPointActionIdMap.get(re.getCheckPointTransactionId()+"#T")+"'/><input type='hidden' id='"+re.getCheckPointTransactionId()+"3TTid3' name='"+re.getCheckPointTransactionId()+count+"TTid3' value='0'/>");
					    CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed'  id='"+re.getCheckPointTransactionId()+"3T3' data-toggle='tooltip' data-placement='top' data-original-title='Action disabled'>N</label></div>");
			    	}
			    }
			    
			  
			    
			    if(checkPointPermissionMap.get(re.getCheckPointTransactionId()+"#T")!=null){
				    CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"2TT4' name='"+re.getCheckPointTransactionId()+"TT4'/><input type='hidden' id='"+re.getCheckPointTransactionId()+"3TTid4' name='"+re.getCheckPointTransactionId()+count+++"TTid4' value='1'/>");
				    CPSRecords.append("<label class='circlegrey green tempToolTip' onclick=\"clickView("+re.getCheckPointTransactionId()+",3,"+count+",'"+re.getCheckPointName().replace("'", "&#")+"');\" id='"+re.getCheckPointTransactionId()+"4I4' data-toggle='tooltip' data-placement='top' data-original-title='Permissions set'>P</label></div>");
			    }else{
			    	CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"TT4' name='"+re.getCheckPointTransactionId()+"TT4'/><input type='hidden' id='"+re.getCheckPointTransactionId()+count+"TTid4' name='"+re.getCheckPointTransactionId()+count+"TTid4' value='0'/>");
				    CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+re.getCheckPointTransactionId()+(count++)+"I4' data-toggle='tooltip' data-placement='top' data-original-title='Permissions disabled'>P</label></div>");
			    }
			   
			    if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#T")!=null & checkPointStatusMap.get(re.getCheckPointTransactionId()+"#T")!=null){
			    	if(checkPointStatusMap.get(re.getCheckPointTransactionId()+"#T")==1){
			    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"_"+re.getCheckPointTransactionId()+"_3' value='1'/>");
			    		CPSRecords.append("<label class='circlegrey green tempToolTip' id='"+re.getCheckPointTransactionId()+"_3' title='Active' onclick='deactivateSection("+re.getCheckPointTransactionId()+",3);' data-toggle='tooltip' data-placement='top' data-original-title='Active'>A</label></div>");
			    	}else{
			    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"_"+re.getCheckPointTransactionId()+"_3' value='0'/>");
				    	CPSRecords.append("<label class='circlegrey tempToolTip' onclick='deactivateSection("+re.getCheckPointTransactionId()+",3);' id='"+re.getCheckPointTransactionId()+"_3' title='Inactive' data-toggle='tooltip' data-placement='top' data-original-title='Inactive'>I</label></div>");
			    	}
			    }else{
			    	if(checkPointCofigureMap.get(re.getCheckPointTransactionId()+"#T")!=null){
			    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"_"+re.getCheckPointTransactionId()+"_3' value='0'/>");
				    	CPSRecords.append("<label class='circlegrey tempToolTip' id='"+re.getCheckPointTransactionId()+"_3' title='Inactive' onclick='deactivateSection("+re.getCheckPointTransactionId()+",3);' data-toggle='tooltip' data-placement='top' data-original-title='Inactive'>I</label></div>");
			    	}else{
			    		CPSRecords.append("<div class='lfloat'><input type='hidden' id='"+re.getCheckPointTransactionId()+"_"+re.getCheckPointTransactionId()+"_3' value='0'/>");
				    	CPSRecords.append("<label class='circlegrey tempToolTip circleNotAllowed' id='"+re.getCheckPointTransactionId()+"_3' title='Inactive' data-toggle='tooltip' data-placement='top' data-original-title='Inactive'>I</label></div>");
			    	}
			    }
			    
			    
			    CPSRecords.append("</div></div>");
			    }
			CPSRecords.append("<input type='hidden' id='checkPointIds' value='"+checkPointids+"'/>");
			CPSRecords.append("<input type='hidden' id='checkPointName' value='"+checkPointName+"'/>");
			CPSRecords.append("<input type='hidden' id='checkPointRecordCount' value='"+candidateCount+"'/>");
		}
		
	}catch (Exception e) {
		e.printStackTrace();
	}
	return CPSRecords.toString();
}

public String displayCommunicationRecord(Integer districtId, String noOfRow, String pageNo,Integer onboardingId,String candidateType,Integer checkPointId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException("Your session has expired!");
    }
	StringBuffer dmRecords =	new StringBuffer();
	try{
		
		int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
		int pgNo 			= 	Integer.parseInt(pageNo);
		int start 			= 	((pgNo-1)*noOfRowInPage);
		int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord		= 	0;
		
		UserMaster userMaster=null;
		userMaster=(UserMaster)session.getAttribute("userMaster");
		List<OnBoardingCommunication> OnBoardingCommunicationList =null;
		List<OnBoardingCommunication> OnboardingCommunicationAll =null;
		Order  sortOrderStrVal		=	null;
		sortOrderStrVal=Order.asc("templeteName");
		Criterion criterion,criterion1,criterion2,criterion3;
		if(userMaster.getEntityType()==1)
		{
			if(districtId!=null && districtId!=0)
			{
				OnboardingCommunicationAll=onBoardingCommunicationDAO.findAll();
				
				if((OnboardingCommunicationAll!=null) || (OnboardingCommunicationAll.size()!=0)){
					criterion=Restrictions.eq("districtId",districtId);
					criterion1=Restrictions.eq("onBoardingId",onboardingId);
					criterion2=Restrictions.eq("candidateType",candidateType);
					criterion3=Restrictions.eq("checkPointId",checkPointId);
					OnBoardingCommunicationList=onBoardingCommunicationDAO.findOnBoardingCommunicationList(sortOrderStrVal, start, noOfRowInPage,criterion,criterion1,criterion2,criterion3);
				}
			}
		}
		else
		{
			OnboardingCommunicationAll=onBoardingCommunicationDAO.findAll();
			
			if((OnboardingCommunicationAll!=null) || (OnboardingCommunicationAll.size()!=0)){
				criterion=Restrictions.eq("districtId",districtId);
				criterion1=Restrictions.eq("onBoardingId",onboardingId);
				criterion2=Restrictions.eq("candidateType",candidateType);
				criterion3=Restrictions.eq("checkPointId",checkPointId);
				OnBoardingCommunicationList=onBoardingCommunicationDAO.findOnBoardingCommunicationList(sortOrderStrVal, start, noOfRowInPage,criterion,criterion1,criterion2,criterion3);
			}
		}
		//totalRecord=OnBoardingCommunicationList.size();

	if(OnBoardingCommunicationList==null || OnBoardingCommunicationList.size()==0)
	{
		dmRecords.append("<div class='row'>");
		dmRecords.append("<div class='col-sm-8 col-md-8 left5'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</div>");
		dmRecords.append("</div>");
	}else{
	for(OnBoardingCommunication onBoardingCommunication : OnBoardingCommunicationList) 
	{
		dmRecords.append("<div class='row' style='width:100%; padding:2px 5px 2px 5px;margin-left:0%;'>");				
			dmRecords.append("<div class='col-sm-3 col-md-3'>"+onBoardingCommunication.getTempleteName()+"</div>");
			if(onBoardingCommunication.getAppliedTo()!=null)
				dmRecords.append("<div class='col-sm-1 col-md-1'>"+onBoardingCommunication.getAppliedTo()+"</div>");
			else
				dmRecords.append("<div class='col-sm-1 col-md-1'></div>");

			dmRecords.append("<div class='col-sm-3 col-md-3'>"+onBoardingCommunication.getSubjectLine()+"</div>");
		
			dmRecords.append("<div class='col-sm-3 col-md-3'>"+onBoardingCommunication.getEmailText()+"</div>");
			
		dmRecords.append("<div class='col-sm-1 col-md-1'>");
		if(onBoardingCommunication.getcStatus().equalsIgnoreCase("A"))
			dmRecords.append("Active");
		else
			dmRecords.append("Inactive");
		dmRecords.append("</div>");

		dmRecords.append("<div class='col-sm-1 col-md-1' style='text-align:center; padding:0px 0px 0px 7px;'>");
		
		
			dmRecords.append("<a href='javascript:void(0);' title='Edit' onclick=\"return editCommunication("+onBoardingCommunication.getCommunicationId()+",2,"+onBoardingCommunication.getDistrictId()+");\"><i class='fa fa-pencil-square-o fa-lg'></i></a>&nbsp;&nbsp;");
		
			if(onBoardingCommunication.getcStatus().equalsIgnoreCase("A"))
				dmRecords.append("<a href='javascript:void(0);' title='"+msgActiveUser+"' onclick=\"return activateDeactivateCommunication("+onBoardingCommunication.getCommunicationId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>");
			else
				dmRecords.append("<a href='javascript:void(0);' title='"+msgInactiveUser+"' onclick=\"return activateDeactivateCommunication("+onBoardingCommunication.getCommunicationId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>");
		
			dmRecords.append("</div>");

	    	dmRecords.append("</div>");
		  dmRecords.append("<div class='row' style='width:100%;margin-left:1px;'>");
          dmRecords.append("<div class='col-sm-12 col-md-12' style='margin-left:6px;'></div>");
          dmRecords.append("</div>");
	}
	dmRecords.append(PaginationAndSorting.getPaginationStringForManageDSPQAjax(request,totalRecord,noOfRow, pageNo));
	}
}catch (Exception e) 
{
	e.printStackTrace();
}
return dmRecords.toString();
}

@Transactional(readOnly=false)
public String checkPointPermissionDefault(Integer districtId,Integer onBoardingId,Integer checkPointId,String candidateType)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	StringBuffer CPSRecords =	new StringBuffer();
	UserMaster userMaster=null;
	int count=0;
	int flag;
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException("Your session has expired!");
    }else {
		userMaster = (UserMaster)session.getAttribute("userMaster");
	}
	try{	
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}	
	List<CheckPointPermissionMaster> checkPointPermissionMasterList=null;
	List<CheckPointPermissionTransaction> checkPointPermissionTransactionList=null;
	checkPointPermissionTransactionList=checkPointPermissionTransactionDAO.getCheckPointPermissionRecord(districtId,onBoardingId,checkPointId,candidateType);
	//int counter =checkPointStatusTransactionDAO.countRowByCriterion(Restrictions.eq("districtId", districtId));
	if(checkPointPermissionTransactionList.size()==0){
		checkPointPermissionMasterList=checkPointPermissionMasterDAO.getPermissionRecord();
		
	if(checkPointPermissionMasterList.size()>0){
		CPSRecords.append("<input type='hidden' id='checkPointPermissionFlag' value='0'>");
		CPSRecords.append("<div class='row mb10 left5'>");
		CPSRecords.append("<div class='row'><div class='col-sm-1 col-md-1'> </div><div class='col-sm-2 col-md-2'> </div><div class='col-sm-1 col-md-1' style='padding-right:0px;'><label>Read Only</label></div>");
        CPSRecords.append("<div class='col-sm-1 col-md-1'></div><div class='col-sm-1 col-md-1 pleft0'><label>Read/Write</label></div>");
        CPSRecords.append("<div class='col-sm-1 col-md-1'></div><div class='col-sm-2 col-md-2 pleft0'><label style='margin-left:-15px;'>No Permission</label></div></div>");
		for(CheckPointPermissionMaster re: checkPointPermissionMasterList){
			flag=1;
			CPSRecords.append("<div class='row mb5 left5'>");
			CPSRecords.append("<div class='col-sm-1 col-md-1'> <span> "+re.getCpPermissionName()+"</span><input type='hidden' id='"+flag+count+"MId' name='"+flag+count+"MId' value='"+re.getCpPermissionId()+"'/><input type='hidden' id='"+flag+count+"Name' name='"+flag+count+"Name' value='"+re.getCpPermissionName()+"'/>");
			CPSRecords.append(" <span class='tempToolTip' data-toggle='tooltip' data-placement='top' data-original-title='"+re.getCpPermissionName()+"' title='"+re.getCpPermissionName()+"'><img src='images/qua-icon.png'></span> </div>");
			CPSRecords.append("<div class='col-sm-2 col-md-2'> </div>");
			CPSRecords.append("<div class='col-sm-1 col-md-1 pleft0'>");
			CPSRecords.append("<div class='lfloat'>");
			if(re.getCpReadonly()==1)
				CPSRecords.append("<input type='hidden' id='cp"+flag+count+"' name='"+flag+count+"' value='1'/><input type='hidden' id='"+flag+count+"id' name='"+flag+count+"id' value='"+re.getCpPermissionId()+"'/> <label class='circlegrey green tempToolTip' onclick='clickPermissionView("+flag+","+count+");' id='"+flag+count+"P' data-toggle='tooltip' data-placement='top' data-original-title='Configured'>C</label>");
			else
				CPSRecords.append("<input type='hidden' id='cp"+flag+count+"' name='"+flag+count+"' value='0'/><input type='hidden' id='"+flag+count+"id' name='"+flag+count+"id' value='"+re.getCpPermissionId()+"'/> <label class='circlegrey tempToolTip' onclick='clickPermissionView("+flag+","+count+");' id='"+flag+count+"P' data-toggle='tooltip' data-placement='top' data-original-title='No Configured'>N</label>");
			CPSRecords.append("</div></div>");
			flag++;
	        CPSRecords.append("<div class='col-sm-1 col-md-1'></div><div class='col-sm-1 col-md-1 pleft0'>");
	        CPSRecords.append("<div class='lfloat'>");
	        if(re.getCpReadWrite()==1)
	        	CPSRecords.append("<input type='hidden' id='cp"+flag+count+"' name='"+flag+count+"' value='1'/><input type='hidden' id='"+flag+count+"id' name='"+flag+count+"id' value='"+re.getCpPermissionId()+"'/> <label class='circlegrey green tempToolTip' onclick='clickPermissionView("+flag+","+count+");' id='"+flag+count+"P' data-toggle='tooltip' data-placement='top' data-original-title='Configured'>C</label>");
	        else
	        	CPSRecords.append("<input type='hidden' id='cp"+flag+count+"' name='"+flag+count+"' value='0'/><input type='hidden' id='"+flag+count+"id' name='"+flag+count+"id' value='"+re.getCpPermissionId()+"'/> <label class='circlegrey tempToolTip' onclick='clickPermissionView("+flag+","+count+");' id='"+flag+count+"P' data-toggle='tooltip' data-placement='top' data-original-title='No Configured'>N</label>");
			CPSRecords.append("</div></div>");
			flag++;
	        CPSRecords.append("<div class='col-sm-1 col-md-1'></div><div class='col-sm-2 col-md-2 pleft0'>");
	        CPSRecords.append("<div class='lfloat'>");
	        if(re.getCpNoPermission()==1)
	        	CPSRecords.append("<input type='hidden' id='cp"+flag+count+"' name='"+flag+count+"' value='1'/><input type='hidden' id='"+flag+count+"id' name='"+flag+count+"id' value='"+re.getCpPermissionId()+"'/> <label class='circlegrey green tempToolTip' onclick='clickPermissionView("+flag+","+count+");' id='"+flag+count+"P' data-toggle='tooltip' data-placement='top' data-original-title='Configured'>C</label>");
	        else
	        	CPSRecords.append("<input type='hidden' id='cp"+flag+count+"' name='"+flag+count+"' value='0'/><input type='hidden' id='"+flag+count+"id' name='"+flag+count+"id' value='"+re.getCpPermissionId()+"'/> <label class='circlegrey tempToolTip' onclick='clickPermissionView("+flag+","+count+");' id='"+flag+count+"P' data-toggle='tooltip' data-placement='top' data-original-title='No Configured'>N</label>");
			
	        CPSRecords.append("</div></div></div>");
			count++;
		}
		CPSRecords.append("<input type='hidden' id='noOfPermission' name='noOfPermission' value='"+count+"'/>");
		CPSRecords.append("</div>");
	 }
	}else{
		
		CPSRecords.append("<div class='row mb10 left5'>");
		CPSRecords.append("<div class='row'><div class='col-sm-1 col-md-1'> </div><div class='col-sm-2 col-md-2'> </div><div class='col-sm-1 col-md-1' style='padding-right:0px;'><label>Read Only</label></div>");
        CPSRecords.append("<div class='col-sm-1 col-md-1'></div><div class='col-sm-1 col-md-1 pleft0'><label>Read/Write</label></div>");
        CPSRecords.append("<div class='col-sm-1 col-md-1'></div><div class='col-sm-2 col-md-2 pleft0'><label style='margin-left:-15px;'>No Permission</label></div></div>");
        CPSRecords.append("<input type='hidden' id='checkPointPermissionFlag' value='1'>");
		for(CheckPointPermissionTransaction re: checkPointPermissionTransactionList){
			flag=1;
			CPSRecords.append("<div class='row mb5 left5'>");
			CPSRecords.append("<div class='col-sm-1 col-md-1'> <span> "+re.getPermissionName()+"</span><input type='hidden' id='"+flag+count+"MId' name='"+flag+count+"MId' value=''/><input type='hidden' id='"+flag+count+"Name' name='"+flag+count+"Name' value='"+re.getPermissionName()+"'/>");
			CPSRecords.append(" <span class='tempToolTip' data-toggle='tooltip' data-placement='top' data-original-title='"+re.getPermissionName()+"' title='"+re.getPermissionName()+"'><img src='images/qua-icon.png'></span> </div>");
			CPSRecords.append("<div class='col-sm-2 col-md-2'> </div>");
			CPSRecords.append("<div class='col-sm-1 col-md-1 pleft0'>");
			CPSRecords.append("<div class='lfloat'>");
			if(re.getReadOnly()==1)
				CPSRecords.append("<input type='hidden' id='cp"+flag+count+"' name='"+flag+count+"' value='1'/><input type='hidden' id='"+flag+count+"id' name='"+flag+count+"id' value='"+re.getPermissionTransactionId()+"'/> <label class='circlegrey green tempToolTip' onclick='clickPermissionView("+flag+","+count+");' id='"+flag+count+"P' data-toggle='tooltip' data-placement='top' data-original-title='Configured'>C</label>");
			else
				CPSRecords.append("<input type='hidden' id='cp"+flag+count+"' name='"+flag+count+"' value='0'/><input type='hidden' id='"+flag+count+"id' name='"+flag+count+"id' value='"+re.getPermissionTransactionId()+"'/> <label class='circlegrey tempToolTip' onclick='clickPermissionView("+flag+","+count+");' id='"+flag+count+"P' data-toggle='tooltip' data-placement='top' data-original-title='No Configured'>N</label>");
			CPSRecords.append("</div></div>");
			flag++;
	        CPSRecords.append("<div class='col-sm-1 col-md-1'></div><div class='col-sm-1 col-md-1 pleft0'>");
	        CPSRecords.append("<div class='lfloat'>");
	        if(re.getReadWrite()==1)
	        	CPSRecords.append("<input type='hidden' id='cp"+flag+count+"' name='"+flag+count+"' value='1'/><input type='hidden' id='"+flag+count+"id' name='"+flag+count+"id' value='"+re.getPermissionTransactionId()+"'/> <label class='circlegrey green tempToolTip' onclick='clickPermissionView("+flag+","+count+");' id='"+flag+count+"P' data-toggle='tooltip' data-placement='top' data-original-title='Configured'>C</label>");
	        else
	        	CPSRecords.append("<input type='hidden' id='cp"+flag+count+"' name='"+flag+count+"' value='0'/><input type='hidden' id='"+flag+count+"id' name='"+flag+count+"id' value='"+re.getPermissionTransactionId()+"'/> <label class='circlegrey tempToolTip' onclick='clickPermissionView("+flag+","+count+");' id='"+flag+count+"P' data-toggle='tooltip' data-placement='top' data-original-title='No Configured'>N</label>");
			CPSRecords.append("</div></div>");
			flag++;
	        CPSRecords.append("<div class='col-sm-1 col-md-1'></div><div class='col-sm-2 col-md-2 pleft0'>");
	        CPSRecords.append("<div class='lfloat'>");
	        if(re.getNoPermission()==1)
	        	CPSRecords.append("<input type='hidden' id='cp"+flag+count+"' name='"+flag+count+"' value='1'/><input type='hidden' id='"+flag+count+"id' name='"+flag+count+"id' value='"+re.getPermissionTransactionId()+"'/> <label class='circlegrey green tempToolTip' onclick='clickPermissionView("+flag+","+count+");' id='"+flag+count+"P' data-toggle='tooltip' data-placement='top' data-original-title='Configured'>C</label>");
	        else
	        	CPSRecords.append("<input type='hidden' id='cp"+flag+count+"' name='"+flag+count+"' value='0'/><input type='hidden' id='"+flag+count+"id' name='"+flag+count+"id' value='"+re.getPermissionTransactionId()+"'/> <label class='circlegrey tempToolTip' onclick='clickPermissionView("+flag+","+count+");' id='"+flag+count+"P' data-toggle='tooltip' data-placement='top' data-original-title='No Configured'>N</label>");
			CPSRecords.append("</div></div></div>");
			count++;
		}
		CPSRecords.append("<input type='hidden' id='noOfPermission' name='noOfPermission' value='"+count+"'/>");
		CPSRecords.append("</div>");
	 }

}catch(Exception e){
	e.printStackTrace();
	 return "false";
}
 return CPSRecords.toString();
}

@Transactional(readOnly=false)
public String saveCommunication(Integer communicationId,Integer checkPointId,Integer districtId,Integer onBoardingId,String []dataObjectArray,Integer actionType,String candidateType,Integer checkPointFlag)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	OnBoardingCommunication onBoardingCommunication=new OnBoardingCommunication();
	UserMaster userMaster 			= 	null;
	String dataArray[];
	Date currentDate	=	new Date();
	String ipAddress = IPAddressUtility.getIpAddress(request);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException("Your session has expired!");
    }else {
		userMaster = (UserMaster)session.getAttribute("userMaster");
	}
	try{	
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}	
		
		if(checkPointFlag==0){
			Integer noOfRecords = checkPointTransactionDAO.getNoOfRecords();
			saveCheckPoint(districtId,onBoardingId,null,0,null);
		checkPointId=noOfRecords+checkPointId;
		}
	    if(actionType==1 && communicationId==0){
			SessionFactory factory = checkPointStatusTransactionDAO.getSessionFactory();
		    StatelessSession statelessSession=factory.openStatelessSession();
		    Transaction transaction = statelessSession.beginTransaction();
				for(int i=0;i<dataObjectArray.length;i++){
					
					dataArray=dataObjectArray[i].split("###");
					onBoardingCommunication.setDistrictId(districtId);
					onBoardingCommunication.setOnBoardingId(onBoardingId);
					onBoardingCommunication.setCheckPointId(checkPointId);
					onBoardingCommunication.setCandidateType(candidateType);
					onBoardingCommunication.setcUserType(dataArray[0]);
					onBoardingCommunication.setcEntity(dataArray[1]);
					onBoardingCommunication.setTempleteName(dataArray[2]);
					onBoardingCommunication.setSubjectLine(dataArray[3]);
					if(Integer.parseInt(dataArray[4])==1)
						onBoardingCommunication.setcStatus("A");
					else
						onBoardingCommunication.setcStatus("I");
					onBoardingCommunication.setSetDefault(Integer.parseInt(dataArray[5]));
					onBoardingCommunication.setEmailText(dataArray[6]);
					onBoardingCommunication.setCreatedByUser(userMaster.getEntityType());
					onBoardingCommunication.setCreatedDate(currentDate);
					onBoardingCommunication.setIpAddress(ipAddress);
					onBoardingCommunication.setStatus("A");
					statelessSession.insert(onBoardingCommunication);
				}
				transaction.commit();
				statelessSession.close();	
	    }else{
	    	onBoardingCommunication=onBoardingCommunicationDAO.findById(communicationId, false, false);
	    	dataArray=dataObjectArray[0].split("###");
			onBoardingCommunication.setcUserType(dataArray[0]);
			onBoardingCommunication.setcEntity(dataArray[1]);
			onBoardingCommunication.setTempleteName(dataArray[2]);
			onBoardingCommunication.setSubjectLine(dataArray[3]);
			if(Integer.parseInt(dataArray[4])==1)
				onBoardingCommunication.setcStatus("A");
			else
				onBoardingCommunication.setcStatus("I");
			onBoardingCommunication.setSetDefault(Integer.parseInt(dataArray[5]));
			onBoardingCommunication.setEmailText(dataArray[6]);
			onBoardingCommunication.setModifiedByUser(userMaster.getEntityType());
			onBoardingCommunication.setModifiedDate(currentDate);
			onBoardingCommunication.setIpAddress(ipAddress);
			onBoardingCommunicationDAO.makePersistent(onBoardingCommunication);
	    }
	}catch(Exception e){
		e.printStackTrace();
		}
	return "true";
}
public boolean activateDeactivateCommunication(Integer communicationId,String status){
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	Date activateDate	=	new Date();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException("Your session has expired!");
	}
	try{
		OnBoardingCommunication onBoardingCommunication=onBoardingCommunicationDAO.findById(communicationId, false, false);
		onBoardingCommunication.setcStatus(status);	
		if(status.equalsIgnoreCase("A"))
			onBoardingCommunication.setModifiedDate(activateDate);
		
		onBoardingCommunicationDAO.makePersistent(onBoardingCommunication);
	}catch (Exception e) 
	{
		e.printStackTrace();
		return false;
	}
	return true;	
}
@Transactional(readOnly=false)
public Map<String,Object> editCommunication(Integer communicationId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	Map<String,Object>  mapList=new HashMap<String, Object>();
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException("Your session has expired!");
	}
	OnBoardingCommunication onBoardingCommunication=null;
	
	if(communicationId!=0 && communicationId!=null)
	{			
		try{
			onBoardingCommunication=onBoardingCommunicationDAO.findById(communicationId, false, false);
			if(onBoardingCommunication!=null){
				mapList.put("userType", onBoardingCommunication.getcUserType());
				mapList.put("districtId", onBoardingCommunication.getDistrictId());
				mapList.put("onBoardingId", onBoardingCommunication.getOnBoardingId());
				mapList.put("checkPointId", onBoardingCommunication.getCheckPointId());
				mapList.put("candidateType", onBoardingCommunication.getCandidateType());
				mapList.put("entityType",onBoardingCommunication.getcEntity());
				mapList.put("templeteName",onBoardingCommunication.getTempleteName());
				mapList.put("subjectline",onBoardingCommunication.getSubjectLine());
				mapList.put("emailText",onBoardingCommunication.getEmailText());
				mapList.put("status",onBoardingCommunication.getcStatus());
				mapList.put("setDefault",onBoardingCommunication.getSetDefault());
			}
		}catch (Exception e) 
		{
			e.printStackTrace();				
		}
	}
	return mapList;
  }
@Transactional(readOnly=false)
public String savePermission(Integer checkPointId,Integer districtId,Integer onBoardingId,String []dataObjectArray,String candidateType,Integer checkPointPermissionFlag,String editCandidateType)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	CheckPointPermissionTransaction checkPointPermissionTransaction=new CheckPointPermissionTransaction();
	 List<CheckPointPermissionTransaction> checkPointPermissionTransactionList=null;
	 
	UserMaster userMaster 			= 	null;
	String dataArray[];
	Date currentDate	=	new Date();
	Integer recordCount,counter;
	String ipAddress = IPAddressUtility.getIpAddress(request);
	String multicandidateType[]=null;
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException("Your session has expired!");
    }else {
		userMaster = (UserMaster)session.getAttribute("userMaster");
	}
	try{	
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}	int d=0;
		List<String> lstCandidateType=null;lstCandidateType=new ArrayList<String>();
		if(candidateType.length()==1){
			multicandidateType=new String[1];
			multicandidateType[0]=candidateType;
			lstCandidateType.add(candidateType);
		}else{
			multicandidateType=candidateType.split("-");
			
			for(String lst:multicandidateType){
				lstCandidateType.add(lst);
			}
		}
		  recordCount=checkPointPermissionTransactionDAO.getCheckPointPermissionCount(districtId,onBoardingId,checkPointId,lstCandidateType);
		  if(checkPointPermissionFlag==0){
			  Integer noOfRecords = checkPointTransactionDAO.getNoOfRecords();
			  saveCheckPoint(districtId,onBoardingId,null,0,null);
		  checkPointId=noOfRecords+checkPointId;
		  }
		  
		  if(recordCount==0){
			SessionFactory factory = checkPointPermissionTransactionDAO.getSessionFactory();
		    StatelessSession statelessSession=factory.openStatelessSession();
		    Transaction transaction = statelessSession.beginTransaction();
		    while(d<lstCandidateType.size()){
				for(int i=0;i<dataObjectArray.length;i++){
					dataArray=dataObjectArray[i].split("#");
					checkPointPermissionTransaction.setDistrictId(districtId);
					checkPointPermissionTransaction.setOnBoardingId(onBoardingId);
					checkPointPermissionTransaction.setCheckPointId(checkPointId);
					if(multicandidateType.length!=1)
						checkPointPermissionTransaction.setCandidateType(lstCandidateType.get(d));
					else
						checkPointPermissionTransaction.setCandidateType(multicandidateType[0]);
					checkPointPermissionTransaction.setPermissionMasterId(Utility.getIntValue(dataArray[1]));
					checkPointPermissionTransaction.setPermissionName(dataArray[2]);
					checkPointPermissionTransaction.setReadOnly(Utility.getIntValue(dataArray[3]));
					checkPointPermissionTransaction.setReadWrite(Utility.getIntValue(dataArray[4]));
					checkPointPermissionTransaction.setNoPermission(Utility.getIntValue(dataArray[5]));
					checkPointPermissionTransaction.setCreatedByUser(userMaster.getEntityType().toString());
					checkPointPermissionTransaction.setCreatedDate(currentDate);
					checkPointPermissionTransaction.setIpAddress(ipAddress);
					checkPointPermissionTransaction.setStatus("A");
					statelessSession.insert(checkPointPermissionTransaction);
				}
				d++;
		    }
				transaction.commit();
				statelessSession.close();
		  }else{
			  String[] allCandidateTypes=null;
			  allCandidateTypes=new String[]{"E","I","T"};
			  lstCandidateType.clear();
			  for(String s:allCandidateTypes){
				  lstCandidateType.add(s);
			  }
			  
			  String[] splitEdit=null; 
			  if(candidateType.contains("-")){
				   splitEdit= candidateType.split("-");
			  }else{
				  splitEdit=new String[]{candidateType};
			  }
			  List editlist=null;editlist=new ArrayList();
			  for(String lst:splitEdit){
					editlist.add(lst);
				}
			  SessionFactory factory = checkPointPermissionTransactionDAO.getSessionFactory();
			  StatelessSession statelessSession=factory.openStatelessSession();
			  Transaction transaction = statelessSession.beginTransaction();
			//  checkPointPermissionTransactionList=checkPointPermissionTransactionDAO.getCheckPointPermissionRecord(districtId,onBoardingId,checkPointId,candidateType);
			   //List<CheckPointPermissionTransaction> editCheckPointDetails = checkPointPermissionTransactionDAO.getEditCheckPointDetails(onBoardingId, checkPointId, districtId,lstCandidateType);
			  counter=0;
			  CheckPointPermissionTransaction upRecord=null;
			  upRecord=new CheckPointPermissionTransaction();
			  
				  
				  
				  for(int a=0;a<lstCandidateType.size();a++){
					  counter=0;
					  if(editlist.contains(lstCandidateType.get(a))==true){
						  checkPointPermissionTransactionList=checkPointPermissionTransactionDAO.getCheckPointPermissionRecord(districtId,onBoardingId,checkPointId,lstCandidateType.get(a).toString());
						  	if(checkPointPermissionTransactionList.size()!=0){						  
						  for(CheckPointPermissionTransaction record: checkPointPermissionTransactionList){
							  dataArray=dataObjectArray[counter].split("#");
							  upRecord.setPermissionTransactionId(record.getPermissionTransactionId());
							  upRecord.setPermissionMasterId(record.getPermissionMasterId());
							  upRecord.setOnBoardingId(record.getOnBoardingId());
							  upRecord.setCheckPointId(record.getCheckPointId());
							  upRecord.setDistrictId(record.getDistrictId());
							  upRecord.setCandidateType(record.getCandidateType());
							  upRecord.setPermissionName(record.getPermissionName());
							  upRecord.setReadOnly(Utility.getIntValue(dataArray[3]));
							  upRecord.setReadWrite(Utility.getIntValue(dataArray[4]));
							  upRecord.setNoPermission(Utility.getIntValue(dataArray[5]));
							  upRecord.setModifiedByUser(userMaster.getEntityType().toString());
							  upRecord.setModifiedDate(currentDate);
							  upRecord.setIpAddress(ipAddress);
							  upRecord.setStatus("A");
							  statelessSession.update(upRecord);
							  counter++;
							  
						  }
						  	}else{
						  		int matserid=1;
								for(int i=0;i<dataObjectArray.length;i++){
									dataArray=dataObjectArray[i].split("#");
									checkPointPermissionTransaction.setPermissionMasterId(matserid);
									checkPointPermissionTransaction.setDistrictId(districtId);
									checkPointPermissionTransaction.setOnBoardingId(onBoardingId);
									checkPointPermissionTransaction.setCheckPointId(checkPointId);
									checkPointPermissionTransaction.setCandidateType(lstCandidateType.get(a).toString());
									
									checkPointPermissionTransaction.setPermissionName(dataArray[2]);
									checkPointPermissionTransaction.setReadOnly(Utility.getIntValue(dataArray[3]));
									checkPointPermissionTransaction.setReadWrite(Utility.getIntValue(dataArray[4]));
									checkPointPermissionTransaction.setNoPermission(Utility.getIntValue(dataArray[5]));
									checkPointPermissionTransaction.setCreatedByUser(userMaster.getEntityType().toString());
									checkPointPermissionTransaction.setCreatedDate(currentDate);
									checkPointPermissionTransaction.setIpAddress(ipAddress);
									checkPointPermissionTransaction.setStatus("A");
									statelessSession.insert(checkPointPermissionTransaction);
									matserid++;
								}
						  	}
					  }else{
						  checkPointPermissionTransactionList=checkPointPermissionTransactionDAO.getCheckPointPermissionRecord(districtId,onBoardingId,checkPointId,lstCandidateType.get(a).toString());
						  if(checkPointPermissionTransactionList.size()>0){
							  for(CheckPointPermissionTransaction result:checkPointPermissionTransactionList){
									checkPointPermissionTransaction.setPermissionTransactionId(result.getPermissionTransactionId());
									checkPointPermissionTransaction.setDistrictId(districtId);
									checkPointPermissionTransaction.setOnBoardingId(onBoardingId);
									checkPointPermissionTransaction.setCheckPointId(checkPointId);
										checkPointPermissionTransaction.setCandidateType(lstCandidateType.get(a).toString());
									checkPointPermissionTransaction.setPermissionMasterId(result.getPermissionMasterId());
									checkPointPermissionTransaction.setPermissionName(result.getPermissionName());
									checkPointPermissionTransaction.setReadOnly(0);
									checkPointPermissionTransaction.setReadWrite(0);
									checkPointPermissionTransaction.setNoPermission(0);
									checkPointPermissionTransaction.setCreatedByUser(userMaster.getEntityType().toString());
									checkPointPermissionTransaction.setCreatedDate(currentDate);
									checkPointPermissionTransaction.setIpAddress(ipAddress);
									checkPointPermissionTransaction.setStatus("I");
									statelessSession.update(checkPointPermissionTransaction);
								} 
						  }
					  }
				  }
				  
			  transaction.commit();
			  statelessSession.close();
		  }
		  
	}catch(Exception e){
		e.printStackTrace();
		}
	return "true";
}
@Transactional(readOnly=false)
public String readOnlyCheckPointDefault(Integer districtId,Integer onBoardingId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	StringBuffer CPSRecords =	new StringBuffer();
	UserMaster userMaster=null;
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException("Your session has expired!");
    }else {
		userMaster = (UserMaster)session.getAttribute("userMaster");
	}
	try{	
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}	
	List<ReadOnlyCheckPointMaster> readOnlyCheckPointMasterList=null;
	List<ReadOnlyCheckPointTransaction> readOnlyCheckPointTransactionList=null;
	readOnlyCheckPointTransactionList=readOnlyCheckPointTransactionDAO.getReadOnlyCheckPointRecord(districtId,onBoardingId);
	String readOnlyCheckPointName="";
	if(readOnlyCheckPointTransactionList!=null && readOnlyCheckPointTransactionList.size()==0){
		readOnlyCheckPointMasterList=readonlyCheckPointMasterDAO.getReadOnlyCheckPointRecord();
		CPSRecords.append("<input type='hidden' id='mastercount' value='"+readOnlyCheckPointMasterList.size()+"'/>");
	if(readOnlyCheckPointMasterList.size()>0){
		readOnlyCheckPointName="";
		for(ReadOnlyCheckPointMaster re: readOnlyCheckPointMasterList){
			CPSRecords.append("<div class='row mb5'>");
			CPSRecords.append("<div class='col-sm-3 col-md-3 pleftright'><span id='"+re.getCheckPointId()+"editReadOnlyCheckPointName' >"+re.getCheckPointName()+"</span>");
			CPSRecords.append(" <span class='tempToolTip' data-toggle='tooltip' data-placement='top' data-original-title='"+re.getCheckPointName()+"'><img src='images/qua-icon.png'></span></div>");
			if(readOnlyCheckPointName.equals(""))
				readOnlyCheckPointName=re.getCheckPointName();
			else
				readOnlyCheckPointName=readOnlyCheckPointName+("##"+re.getCheckPointName());
			CPSRecords.append("<div class='col-sm-1 col-md-1 showhideedit top5' ><a href='javascript:void(0);' title='Edit' onclick=\"editReadOnlyCheckPoint(1,"+re.getCheckPointId()+",'"+re.getCheckPointName()+"','"+re.getStatus()+"');\" ><i class='fa fa-pencil-square-o fa-lg'></i></a></div>");
			CPSRecords.append("<div class='col-sm-2 col-md-2 '>	  </div>");
			CPSRecords.append("<div class='col-sm-2 col-md-2 txtalign'><input type='hidden' id='ck"+re.getCheckPointId()+"' value='A'/>");
			CPSRecords.append("<label class='circlegrey green tempToolTip' id='Tool"+re.getCheckPointId()+"' title='Active' onclick=\"activeInactiveReadOnlyCheckPoint("+re.getCheckPointId()+",'A');\" data-toggle='tooltip' data-placement='top' data-original-title='Active'>A</label></div>");
			CPSRecords.append("</div>");	
		}
	 }
	}else{
		readOnlyCheckPointName="";
		for(ReadOnlyCheckPointTransaction re: readOnlyCheckPointTransactionList){
			CPSRecords.append("<input type='hidden' id='mastercount' value='0'/>");
			CPSRecords.append("<input type='hidden' id='lasttransid' value='"+re.getCheckPointTransactionId()+"'/>");
			CPSRecords.append("<div class='row mb5'>");
			CPSRecords.append("<input type='hidden' id='"+re.getCheckPointTransactionId()+"transid' value="+re.getCheckPointTransactionId()+"/>");
			CPSRecords.append("<div class='col-sm-3 col-md-3 pleftright'><span id='"+re.getCheckPointTransactionId()+"editReadOnlyCheckPointName' >"+re.getCheckPointName()+"</span>");
			if(readOnlyCheckPointName.equals(""))
				readOnlyCheckPointName=re.getCheckPointName();
			else
				readOnlyCheckPointName=readOnlyCheckPointName+("##"+re.getCheckPointName());
			
			CPSRecords.append(" <span class='tempToolTip' data-toggle='tooltip' data-placement='top' data-original-title='"+re.getCheckPointName()+"'><img src='images/qua-icon.png'></span></div>");
			CPSRecords.append("<div class='col-sm-1 col-md-1 showhideedit top5' ><a href='javascript:void(0);' title='Edit' onclick=\"editReadOnlyCheckPoint(2,"+re.getCheckPointTransactionId()+",'"+re.getCheckPointName()+"','"+re.getStatus()+"');\"><i class='fa fa-pencil-square-o fa-lg'></i></a></div>");
			CPSRecords.append("<div class='col-sm-2 col-md-2 '>	  </div>");
			if(re.getStatus().equalsIgnoreCase("A")){
				CPSRecords.append("<div class='col-sm-2 col-md-2 txtalign'><input type='hidden' id='ck"+re.getCheckPointTransactionId()+"' value='A'/>");
				CPSRecords.append("<label class='circlegrey green tempToolTip' id='Tool"+re.getCheckPointTransactionId()+"' title='Active' onclick=\"activeInactiveReadOnlyCheckPoint("+re.getCheckPointTransactionId()+",'A');\" data-toggle='tooltip' data-placement='top' data-original-title='Active'>A</label></div>");
			}else{
				CPSRecords.append("<div class='col-sm-2 col-md-2 txtalign'><input type='hidden' id='ck"+re.getCheckPointTransactionId()+"' value='I'/>");
				CPSRecords.append("<label class='circlegrey tempToolTip' id='Tool"+re.getCheckPointTransactionId()+"' title='Inactive' onclick=\"activeInactiveReadOnlyCheckPoint("+re.getCheckPointTransactionId()+",'I');\" data-toggle='tooltip' data-placement='top' data-original-title='Inactive'>X</label></div>");
			}
			CPSRecords.append("</div>");
		}
	 }
	//System.out.println("readOnlyCheckPointName :::::: "+readOnlyCheckPointName);
	CPSRecords.append("<input type='hidden' id='readOnlyCheckPointData' value='"+readOnlyCheckPointName+"'/>");
}catch(Exception e){
	e.printStackTrace();
	 return "false";
}
 return CPSRecords.toString();
}
public String saveCheckPoint(Integer districtId,Integer onBoardingId,String checkPointName,Integer actionNeeded,String checkPointStatus)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	CheckPointTransaction checkPointTransaction=new CheckPointTransaction();
	UserMaster userMaster 			= 	null;
	Date currentDate	=	new Date();
	Integer count=0;
	String ipAddress = IPAddressUtility.getIpAddress(request);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException("Your session has expired!");
    }else {
		userMaster = (UserMaster)session.getAttribute("userMaster");
	}
	try{	
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}	
		List<CheckPointTransaction> checkPointTransactionList=null;
		checkPointTransactionList=checkPointTransactionDAO.getCheckPointRecord(districtId,onBoardingId);
		if(checkPointTransactionList.size()>0){
			/*checkPointTransaction.setCheckPointMasterId(null);
			checkPointTransaction.setDistrictId(districtId);
			checkPointTransaction.setOnBoardingId(onBoardingId);
			checkPointTransaction.setCheckPointName(checkPointName);
			checkPointTransaction.setActionNeeded(actionNeeded);
			checkPointTransaction.setCheckPointStatus(checkPointStatus);
			checkPointTransaction.setCreatedByUser(userMaster.getUserId());
			checkPointTransaction.setCreatedDate(currentDate);
			checkPointTransaction.setIpAddress(ipAddress);
			checkPointTransaction.setStatus("A");
			checkPointTransactionDAO.makePersistent(checkPointTransaction);
			count=1;*/
		}else{
			SessionFactory factory = checkPointTransactionDAO.getSessionFactory();
		    StatelessSession statelessSession=factory.openStatelessSession();
		    Transaction transaction = statelessSession.beginTransaction();
			List<CheckPointMaster> checkPointMasterList=null;
			checkPointMasterList=checkPointMasterDAO.getcheckpoints();
			for(CheckPointMaster re:checkPointMasterList){
				checkPointTransaction.setCheckPointMasterId(re.getCheckPointId());
				checkPointTransaction.setDistrictId(districtId);
				checkPointTransaction.setOnBoardingId(onBoardingId);
				checkPointTransaction.setCheckPointName(re.getCheckPointName());
				checkPointTransaction.setActionNeeded(0);
				checkPointTransaction.setCheckPointStatus("A");
				checkPointTransaction.setCreatedByUser(userMaster.getUserId());
				checkPointTransaction.setCreatedDate(currentDate);
				checkPointTransaction.setIpAddress(ipAddress);
				checkPointTransaction.setStatus("A");
				statelessSession.insert(checkPointTransaction);
				count=1;
			}
			if(count==0){
				checkPointTransaction.setCheckPointMasterId(null);
				checkPointTransaction.setDistrictId(districtId);
				checkPointTransaction.setOnBoardingId(onBoardingId);
				checkPointTransaction.setCheckPointName(checkPointName);
				checkPointTransaction.setActionNeeded(actionNeeded);
				checkPointTransaction.setCheckPointStatus(checkPointStatus);
				checkPointTransaction.setCreatedByUser(userMaster.getUserId());
				checkPointTransaction.setCreatedDate(currentDate);
				checkPointTransaction.setIpAddress(ipAddress);
				checkPointTransaction.setStatus("A");
				statelessSession.insert(checkPointTransaction);
			}
			transaction.commit();
			statelessSession.close();	
		}
	}catch (Exception e) {
		e.printStackTrace();
		return "false";
	}
	return "true";
}
public String saveReadOnlyCheckPoint(Integer districtId,Integer onBoardingId,Integer type,Integer checkPointId,String checkPointName,String checkPointStatus)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	ReadOnlyCheckPointTransaction readOnlyCheckPointTransaction=new ReadOnlyCheckPointTransaction();
	UserMaster userMaster 			= 	null;
	Date currentDate	=	new Date();
	String ipAddress = IPAddressUtility.getIpAddress(request);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
		throw new IllegalStateException("Your session has expired!");
    }else {
		userMaster = (UserMaster)session.getAttribute("userMaster");
	}
	try{	
		if (session == null || session.getAttribute("userMaster") == null){
			return "redirect:index.jsp";
		}	
		if(type==2){
			readOnlyCheckPointTransaction=readOnlyCheckPointTransactionDAO.findById(checkPointId,false,false);
			readOnlyCheckPointTransaction.setDistrictId(districtId);
			readOnlyCheckPointTransaction.setOnBoardingId(onBoardingId);
			readOnlyCheckPointTransaction.setCheckPointName(checkPointName);
			readOnlyCheckPointTransaction.setModifiedByUser(userMaster.getUserId().toString());
			readOnlyCheckPointTransaction.setModifiedDate(currentDate);
			readOnlyCheckPointTransaction.setIpAddress(ipAddress);
			readOnlyCheckPointTransaction.setStatus(checkPointStatus);
			readOnlyCheckPointTransactionDAO.makePersistent(readOnlyCheckPointTransaction);
		}else{
			SessionFactory factory = checkPointTransactionDAO.getSessionFactory();
		    StatelessSession statelessSession=factory.openStatelessSession();
		    Transaction transaction = statelessSession.beginTransaction();
		    List<ReadOnlyCheckPointMaster> readOnlyCheckPointMasterList=null;
		    readOnlyCheckPointMasterList=readonlyCheckPointMasterDAO.findAll();
			for(ReadOnlyCheckPointMaster re:readOnlyCheckPointMasterList){
				if(checkPointId.equals(re.getCheckPointId())){
					readOnlyCheckPointTransaction.setCheckPointMasterId(re.getCheckPointId());
					readOnlyCheckPointTransaction.setDistrictId(districtId);
					readOnlyCheckPointTransaction.setOnBoardingId(onBoardingId);
					readOnlyCheckPointTransaction.setCheckPointName(checkPointName);
					readOnlyCheckPointTransaction.setCreatedByUser(userMaster.getUserId().toString());
					readOnlyCheckPointTransaction.setCreatedDate(currentDate);
					readOnlyCheckPointTransaction.setIpAddress(ipAddress);
					readOnlyCheckPointTransaction.setStatus(checkPointStatus);
					statelessSession.insert(readOnlyCheckPointTransaction);
				}else{
					readOnlyCheckPointTransaction.setCheckPointMasterId(re.getCheckPointId());
					readOnlyCheckPointTransaction.setDistrictId(districtId);
					readOnlyCheckPointTransaction.setOnBoardingId(onBoardingId);
					readOnlyCheckPointTransaction.setCheckPointName(re.getCheckPointName());
					readOnlyCheckPointTransaction.setCreatedByUser(userMaster.getUserId().toString());
					readOnlyCheckPointTransaction.setCreatedDate(currentDate);
					readOnlyCheckPointTransaction.setIpAddress(ipAddress);
					readOnlyCheckPointTransaction.setStatus("A");
					statelessSession.insert(readOnlyCheckPointTransaction);
				}
			}
			transaction.commit();
			statelessSession.close();	
		}
	}catch (Exception e) {
		return "false";
	}
	return "true";
}
public String checkPointType(){
	List<QuestionTypeMaster> questionTypeMastersList		= 	null;
	StringBuffer sb=new StringBuffer();
	try{
	questionTypeMastersList = questionTypeMasterDAO.getActiveQuestionTypesByShortName(new String[]{"dl","tf","ml","sl","dt","cb"});
					sb.append("<select id='avlbList' name='avlbList' onchange='checkPointTypeChange();' class='form-control'>");
					sb.append("<option value='' selected='true'>Select Question Type</option>");
					if(questionTypeMastersList!=null && questionTypeMastersList.size() > 0)
						for(QuestionTypeMaster questionTypeLst : questionTypeMastersList){
							if(questionTypeLst!=null){
									sb.append("<option value='"+questionTypeLst.getQuestionTypeId()+"'>"+questionTypeLst.getQuestionType()+"</option>");
								}
							}
				  sb.append("</select>");
	}catch (Exception e) {
		// TODO: handle exception
	}
	return sb.toString();
}
/**
 * Add CheckPoint records from master table plus newly added check point record in checkpointtransaction  table
 * if master records  already exists in checkpointtransaction  against district and onboardid,it will insert only newly added record
 *  
 * */
public String addCheckpoint(int districtId,int onBoardingId,String checkPointName,int communicationRequired,String actionPerformedBy,String status,int editCheckPointID,Integer transactionId){
	AddCheckPointTransaction addCheckPointTransaction=null;
	addCheckPointTransaction=new AddCheckPointTransaction();
	try{
		WebContext context;
		UserMaster userMaster =null;
		context = WebContextFactory.get();
		boolean checkPointNameExists=false;
		HttpServletRequest request = context.getHttpServletRequest();
		String ipAddress = IPAddressUtility.getIpAddress(request);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String format = sdf.format(new Date());
		Date parseDate = sdf.parse(format);
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException("Your session has expired!");
	    }else {
			userMaster = (UserMaster)session.getAttribute("userMaster");
			
		}
		if(editCheckPointID==0){
		List<CheckPointTransaction> checkPointStatusRecord = null;
		checkPointStatusRecord=checkPointTransactionDAO.getCheckPointRecord(districtId, onBoardingId);;
		List<CheckPointMaster> getcheckpoints = checkPointMasterDAO.getcheckpoints();
		
		if(checkPointStatusRecord.size()==0){
			SessionFactory factory = checkPointMasterDAO.getSessionFactory();
			StatelessSession statelessSession=factory.openStatelessSession();
			Transaction transaction = statelessSession.beginTransaction();
				for(CheckPointMaster re: getcheckpoints)
				{
					if(re.getCheckPointName().equals(checkPointName))
						checkPointNameExists=true;
						addCheckPointTransaction.setCheckPointMasterId(re.getCheckPointId());
						addCheckPointTransaction.setOnBoardingId(onBoardingId);
						addCheckPointTransaction.setDistrictId(districtId);
						addCheckPointTransaction.setCheckPointName(re.getCheckPointName());
						addCheckPointTransaction.setConmmunicationRequired(0);
						addCheckPointTransaction.setActionPerformedBy("0,0");
						addCheckPointTransaction.setCheckPointStatus(status);
						addCheckPointTransaction.setCreatedByUser(userMaster.getUserId());
						addCheckPointTransaction.setCreatedDate(parseDate);
						addCheckPointTransaction.setModifiedByUser(null);
						addCheckPointTransaction.setModifiedDate(parseDate);
						addCheckPointTransaction.setIpAddress(ipAddress);
						addCheckPointTransaction.setStatus("A");
						statelessSession.insert(addCheckPointTransaction);
				}
			transaction.commit();
			statelessSession.close();
	}
		
		if(checkPointNameExists==false){
			SessionFactory factory = checkPointMasterDAO.getSessionFactory();
			StatelessSession statelessSession=factory.openStatelessSession();
			Transaction transaction = statelessSession.beginTransaction();
	    	addCheckPointTransaction.setCheckPointMasterId(null);
	    	addCheckPointTransaction.setOnBoardingId(onBoardingId);
	    	addCheckPointTransaction.setDistrictId(districtId);
	    	addCheckPointTransaction.setCheckPointName(checkPointName);
	    	addCheckPointTransaction.setConmmunicationRequired(communicationRequired);
	    	addCheckPointTransaction.setActionPerformedBy(actionPerformedBy);
	    	addCheckPointTransaction.setCheckPointStatus(status);
	    	addCheckPointTransaction.setCreatedByUser(userMaster.getUserId());
	    	addCheckPointTransaction.setCreatedDate(parseDate);
	    	addCheckPointTransaction.setModifiedByUser(userMaster.getUserId());
	    	addCheckPointTransaction.setModifiedDate(parseDate);
	    	addCheckPointTransaction.setIpAddress(ipAddress);
	    	addCheckPointTransaction.setStatus("A");
	    	statelessSession.insert(addCheckPointTransaction);
	    	transaction.commit();
	    	statelessSession.close();
		}else{
			addCheckPointTransaction.setCheckPointTransactionId(transactionId);
		}
	    	
		}else{
			SessionFactory factory = onboardingAddCheckPointDAO.getSessionFactory();
			StatelessSession statelessSession=factory.openStatelessSession();
			Transaction transaction = statelessSession.beginTransaction();
			addCheckPointTransaction.setCheckPointTransactionId(editCheckPointID);
			addCheckPointTransaction.setCheckPointMasterId(null);
	    	addCheckPointTransaction.setOnBoardingId(onBoardingId);
	    	addCheckPointTransaction.setDistrictId(districtId);
	    	addCheckPointTransaction.setCheckPointName(checkPointName);
	    	addCheckPointTransaction.setConmmunicationRequired(communicationRequired);
	    	addCheckPointTransaction.setActionPerformedBy(actionPerformedBy);
	    	addCheckPointTransaction.setCheckPointStatus(status);
	    	addCheckPointTransaction.setCreatedByUser(userMaster.getUserId());
	    	addCheckPointTransaction.setCreatedDate(parseDate);
	    	addCheckPointTransaction.setModifiedByUser(userMaster.getUserId());
	    	addCheckPointTransaction.setModifiedDate(parseDate);
	    	addCheckPointTransaction.setIpAddress(ipAddress);
	    	addCheckPointTransaction.setStatus("A");
	    	statelessSession.update(addCheckPointTransaction);
	    	transaction.commit();
	    	statelessSession.close();
		}
		
	
}catch(Exception e)
{
	e.printStackTrace();
}
	return addCheckPointTransaction.getCheckPointTransactionId().toString();
}
/**
 * Add Question in onboardingcheckpointquestion table respective to checkpoint name and checkpointtransactionid addeded in checkpointtransaction
 * 
 * */
public String addCheckpointQuestion(Integer onBoardID,int districtId,int checkPointTypeID,String question,String candidateType,int checkPointId,int editCheckPointQuestionId,String filesUploaded){
	WebContext context;
	OnboardCheckPointQuestion onboardCheckPointQuestion=new OnboardCheckPointQuestion();
	try{
		UserMaster userMaster =null;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException("Your session has expired!");
	    }else {
			userMaster = (UserMaster)session.getAttribute("userMaster");
		}
		String ipAddress = IPAddressUtility.getIpAddress(request);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String format = sdf.format(new Date());
		Date parseDate = sdf.parse(format);
		SessionFactory factory = addQuestionDAO.getSessionFactory();
		StatelessSession statelessSession=factory.openStatelessSession();
		Transaction transaction = statelessSession.beginTransaction();
	
		if(editCheckPointQuestionId==0){
				onboardCheckPointQuestion.setOnBoardingId(onBoardID);
				onboardCheckPointQuestion.setDistrictId(districtId);
				onboardCheckPointQuestion.setCheckPointId(checkPointId);
				onboardCheckPointQuestion.setCheckPointTypeId(checkPointTypeID);
				onboardCheckPointQuestion.setQuestion(question);
				onboardCheckPointQuestion.setDocumentFileName(filesUploaded);
				onboardCheckPointQuestion.setCandidateType(candidateType);
				onboardCheckPointQuestion.setCreatedDate(parseDate);
				onboardCheckPointQuestion.setCreatedBy(userMaster.getUserId());
				onboardCheckPointQuestion.setModifiedDate(parseDate);
				onboardCheckPointQuestion.setModifiedBy(userMaster.getUserId());
				onboardCheckPointQuestion.setIpAddress(ipAddress);
				onboardCheckPointQuestion.setStatus('A');
				statelessSession.insert(onboardCheckPointQuestion);
	}else{
		onboardCheckPointQuestion.setCheckPointQuestionId(editCheckPointQuestionId);
		onboardCheckPointQuestion.setOnBoardingId(onBoardID);
		onboardCheckPointQuestion.setDistrictId(districtId);
		onboardCheckPointQuestion.setCheckPointId(checkPointId);
		onboardCheckPointQuestion.setCheckPointTypeId(checkPointTypeID);
		onboardCheckPointQuestion.setQuestion(question);
		onboardCheckPointQuestion.setDocumentFileName(filesUploaded);
		onboardCheckPointQuestion.setCandidateType(candidateType);
		onboardCheckPointQuestion.setCreatedDate(parseDate);
		onboardCheckPointQuestion.setCreatedBy(userMaster.getUserId());
		onboardCheckPointQuestion.setModifiedDate(parseDate);
		onboardCheckPointQuestion.setModifiedBy(userMaster.getUserId());
		onboardCheckPointQuestion.setIpAddress(ipAddress);
		onboardCheckPointQuestion.setStatus('A');
		statelessSession.update(onboardCheckPointQuestion);
	}
		transaction.commit();
		statelessSession.close();
	}catch(Exception e){e.printStackTrace();}
	return onboardCheckPointQuestion.getCheckPointQuestionId()+"";
	
}
public void addCheckPointOptions(int checkPointQuestionId,String data,int checkPointOptionId){
	UserMaster userMaster =null;
	String[] split = data.split(",");
	WebContext context;
	
	try{
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
		throw new IllegalStateException("Your session has expired!");
    }else {
		userMaster = (UserMaster)session.getAttribute("userMaster");
	}
		String ipAddress = IPAddressUtility.getIpAddress(request);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String format = sdf.format(new Date());
		Date parseDate = sdf.parse(format);
		SessionFactory factory = OnboardCheckPointOptionsDAO.getSessionFactory();
		StatelessSession statelessSession=factory.openStatelessSession();
		Transaction transaction = statelessSession.beginTransaction();
		OnBoardingCheckPointOptions onBoardingCheckPointOptions=new OnBoardingCheckPointOptions();
	if(checkPointOptionId==0){
		onBoardingCheckPointOptions.setCheckPointQuestionId(checkPointQuestionId);
		onBoardingCheckPointOptions.setValidOption(split[1].substring(0,split[1].length()-1));
		onBoardingCheckPointOptions.setOptions(split[0].substring(0,split[0].length()-1));
		onBoardingCheckPointOptions.setCreatedDate(parseDate);
		onBoardingCheckPointOptions.setCreatedBy(userMaster.getUserId());
		onBoardingCheckPointOptions.setModifiedDate(parseDate);
		onBoardingCheckPointOptions.setModifiedBy(userMaster.getUserId());
		onBoardingCheckPointOptions.setIpAddress(ipAddress);
		onBoardingCheckPointOptions.setStatus('A');
		statelessSession.insert(onBoardingCheckPointOptions);
	}else{
		onBoardingCheckPointOptions.setCheckPointOptionsId(checkPointOptionId);
		onBoardingCheckPointOptions.setCheckPointQuestionId(checkPointQuestionId);
		onBoardingCheckPointOptions.setValidOption(split[1].substring(0,split[1].length()-1));
		onBoardingCheckPointOptions.setOptions(split[0].substring(0,split[0].length()-1));
		onBoardingCheckPointOptions.setCreatedDate(parseDate);
		onBoardingCheckPointOptions.setCreatedBy(userMaster.getUserId());
		onBoardingCheckPointOptions.setModifiedDate(parseDate);
		onBoardingCheckPointOptions.setModifiedBy(userMaster.getUserId());
		onBoardingCheckPointOptions.setIpAddress(ipAddress);
		onBoardingCheckPointOptions.setStatus('A');
		statelessSession.update(onBoardingCheckPointOptions);
	
	}
		transaction.commit();
		statelessSession.close();	


	}catch(Exception e)
	{
		e.printStackTrace();
	}
}

@SuppressWarnings("null")
public void addCheckPointStatus(int onboardID,int checkPointId,String candidateTypecheck,int districtId,String[] checkPointTransactionData,int counter,String editStatus,Integer initialCounter){
	WebContext context;UserMaster userMaster 			= 	null;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
		throw new IllegalStateException("Your session has expired!");
    }else {
		userMaster = (UserMaster)session.getAttribute("userMaster");
	}
	CheckPointStatusTransaction checkPointStatusTransaction=new CheckPointStatusTransaction();
	
	Date currentDate	=	new Date();
	String ipAddress = IPAddressUtility.getIpAddress(request);
	
	String[] splitName=null;
	splitName=new String[checkPointTransactionData.length];
	String[] splitDecision=null;
	splitDecision=new String[checkPointTransactionData.length];
	String[] splitColorCode =null;
	splitColorCode=new String[checkPointTransactionData.length];
	String[] splitStatus =null;
	splitStatus=new String[checkPointTransactionData.length];
	
	for(int i=0;i<checkPointTransactionData.length;i++){
		String[] splitSingle = checkPointTransactionData[i].split("-");
		splitName[i]=splitSingle[0];
		splitDecision[i]=splitSingle[1];
		splitColorCode[i]=splitSingle[2];
		splitStatus[i]=splitSingle[3];
		
	}
	
	SessionFactory factory = checkPointStatusTransactionDAO.getSessionFactory();
    StatelessSession statelessSession=factory.openStatelessSession();
    Transaction transaction = statelessSession.beginTransaction();
    
    
    try{
    	String multicandidateType[]=null;
    	List<String> lstCandidateType=null;lstCandidateType=new ArrayList<String>();
		if(candidateTypecheck.length()==1){
			multicandidateType=new String[1];
			multicandidateType[0]=candidateTypecheck;
			lstCandidateType.add(candidateTypecheck);
		}else{
			multicandidateType=candidateTypecheck.split("-");
			for(String lst:multicandidateType){
				lstCandidateType.add(lst);
			}
		}
    	
    	
    List<CheckPointStatusTransaction> checkPointStatusTransactionList=null;
    if(editStatus.equals("0")){
    checkPointStatusTransactionList=checkPointStatusTransactionDAO.getCheckPointStatusRecord(districtId,onboardID,checkPointId,lstCandidateType);
    if(checkPointStatusTransactionList.size()==0){
    	
		List<CheckPointStatusMaster> checkPointStatusMasterList=null;
		checkPointStatusMasterList=checkPointStatusMasterDAO.getcheckpointstatus();
		int i=0;
		
    	for(int candidateTypeLoop=0;candidateTypeLoop<lstCandidateType.size();candidateTypeLoop++){    		
    		 i=0;
    		for(CheckPointStatusMaster re:checkPointStatusMasterList){
    			checkPointStatusTransaction.setCpStatusMasterId(re.getCheckPointStatusId());
    			checkPointStatusTransaction.setOnboardingId(onboardID);
    			checkPointStatusTransaction.setCheckPointId(checkPointId);
    			checkPointStatusTransaction.setCandidateType(lstCandidateType.get(candidateTypeLoop));
    			checkPointStatusTransaction.setDistrictId(districtId);
    			checkPointStatusTransaction.setCpTransactionStatusName(splitName[i]);
				checkPointStatusTransaction.setCpColor(splitColorCode[i]);
				checkPointStatusTransaction.setCpDecisionPoint(Integer.parseInt(splitDecision[i]));
				checkPointStatusTransaction.setCpTransactionStatus(splitStatus[i]);
    			checkPointStatusTransaction.setStatus("A");
    			checkPointStatusTransaction.setCreatedDate(currentDate);
    			checkPointStatusTransaction.setCreatedByUser(userMaster.getUserId().toString());
    			checkPointStatusTransaction.setIpAddress(ipAddress);
    			statelessSession.insert(checkPointStatusTransaction);
    			i++;
    }
    }
		int aditionalCheckPointStatus=5;
    	while(aditionalCheckPointStatus<counter){
    		checkPointStatusTransaction.setCpStatusMasterId(null);
			checkPointStatusTransaction.setOnboardingId(onboardID);
			checkPointStatusTransaction.setCheckPointId(checkPointId);
			checkPointStatusTransaction.setCandidateType(multicandidateType[0]);
			checkPointStatusTransaction.setDistrictId(districtId);
			checkPointStatusTransaction.setCpTransactionStatusName(splitName[aditionalCheckPointStatus]);
			checkPointStatusTransaction.setCpColor(splitColorCode[aditionalCheckPointStatus]);
			checkPointStatusTransaction.setCpDecisionPoint(Integer.parseInt(splitDecision[aditionalCheckPointStatus]));
			checkPointStatusTransaction.setCpTransactionStatus(splitStatus[aditionalCheckPointStatus]);
			checkPointStatusTransaction.setStatus("A");
			checkPointStatusTransaction.setCreatedDate(currentDate);
			checkPointStatusTransaction.setCreatedByUser(userMaster.getUserId().toString());
			checkPointStatusTransaction.setIpAddress(ipAddress);
			statelessSession.insert(checkPointStatusTransaction);
			aditionalCheckPointStatus++;
    	}
    
    }
    }else{
    		String[] allCandidateTypes=null;
    		allCandidateTypes=new String[]{"E","I","T"};
    		lstCandidateType.clear();
    		for(String s:allCandidateTypes){
    			lstCandidateType.add(s);
    		}
    		checkPointStatusTransactionList=checkPointStatusTransactionDAO.getCheckPointStatusRecord(districtId,onboardID,checkPointId,lstCandidateType);
    		String[] splitCandidate=null;
    		if(candidateTypecheck.contains("-")==true){
    			 splitCandidate = candidateTypecheck.split("-");
    		}else{
    			splitCandidate=new String[]{candidateTypecheck} ;
    		}
    		List<String> editCandidateTypeList=null;
    		editCandidateTypeList=new ArrayList<String>();
    		for(String r:splitCandidate){
    			editCandidateTypeList.add(r);
    		}
    		for(int allCandidateTypesLoop=0;allCandidateTypesLoop<lstCandidateType.size();allCandidateTypesLoop++){
    			if(editCandidateTypeList.contains(lstCandidateType.get(allCandidateTypesLoop))==true){
    				int masterID=0;
    						checkPointStatusTransactionList=checkPointStatusTransactionDAO.getCheckPointStatusRecord(districtId,onboardID,checkPointId,lstCandidateType.get(allCandidateTypesLoop).toString());
    						if(checkPointStatusTransactionList.size()==0){
    							
    							for(int inc=0;inc<splitName.length;inc++){
    								if(masterID>=5)
    									checkPointStatusTransaction.setCpStatusMasterId(null);
    								else
    		    					checkPointStatusTransaction.setCpStatusMasterId(masterID+1);
    		    					checkPointStatusTransaction.setOnboardingId(onboardID);
    		    					checkPointStatusTransaction.setCheckPointId(checkPointId);
    		    					checkPointStatusTransaction.setCandidateType(lstCandidateType.get(allCandidateTypesLoop).toString());
    		    					checkPointStatusTransaction.setDistrictId(districtId);
    		    					checkPointStatusTransaction.setCpTransactionStatusName(splitName[inc]);
    		    					checkPointStatusTransaction.setCpColor(splitColorCode[inc]);
    		    					checkPointStatusTransaction.setCpDecisionPoint(Integer.parseInt(splitDecision[inc]));
    		    					checkPointStatusTransaction.setCpTransactionStatus(splitStatus[inc]);
    		    					checkPointStatusTransaction.setStatus("A");
    		    					checkPointStatusTransaction.setCreatedDate(currentDate);
    		    					checkPointStatusTransaction.setCreatedByUser(userMaster.getUserId().toString());
    		    					checkPointStatusTransaction.setIpAddress(ipAddress);
    		    					statelessSession.insert(checkPointStatusTransaction);
    		    					masterID++;
    		    					}
    						}
    						else{
    							masterID=0;
    							for(CheckPointStatusTransaction r:checkPointStatusTransactionList){
			    					checkPointStatusTransaction.setCpStatusTransactionId(r.getCpStatusTransactionId());
			    					checkPointStatusTransaction.setCpStatusMasterId(r.getCpStatusMasterId());
			    					checkPointStatusTransaction.setOnboardingId(onboardID);
			    					checkPointStatusTransaction.setCheckPointId(checkPointId);
			    					checkPointStatusTransaction.setCandidateType(r.getCandidateType());
			    					checkPointStatusTransaction.setDistrictId(districtId);
			    					checkPointStatusTransaction.setCpTransactionStatusName(splitName[masterID]);
			    					checkPointStatusTransaction.setCpColor(splitColorCode[masterID]);
			    					checkPointStatusTransaction.setCpDecisionPoint(Integer.parseInt(splitDecision[masterID]));
			    					checkPointStatusTransaction.setCpTransactionStatus(splitStatus[masterID]);
			    					checkPointStatusTransaction.setStatus("A");
			    					checkPointStatusTransaction.setCreatedDate(currentDate);
			    					checkPointStatusTransaction.setCreatedByUser(userMaster.getUserId().toString());
			    					checkPointStatusTransaction.setIpAddress(ipAddress);
			    					statelessSession.update(checkPointStatusTransaction);
			    					masterID++;
    							}
    						}
    			}
    			else{
    				checkPointStatusTransactionList=checkPointStatusTransactionDAO.getCheckPointStatusRecord(districtId,onboardID,checkPointId,lstCandidateType.get(allCandidateTypesLoop).toString());
    				if(checkPointStatusTransactionList.size()>0){
    					for(CheckPointStatusTransaction result:checkPointStatusTransactionList){
        					int id=0;
        					if(result.getCandidateType().equals(lstCandidateType.get(allCandidateTypesLoop))){
        					checkPointStatusTransaction.setCpStatusTransactionId(result.getCpStatusTransactionId());
        					checkPointStatusTransaction.setCpStatusMasterId(result.getCpStatusMasterId());
        					checkPointStatusTransaction.setOnboardingId(onboardID);
        					checkPointStatusTransaction.setCheckPointId(checkPointId);
        					checkPointStatusTransaction.setCandidateType(result.getCandidateType());
        					checkPointStatusTransaction.setDistrictId(districtId);
        					checkPointStatusTransaction.setCpTransactionStatusName(result.getCpTransactionStatusName());
        					checkPointStatusTransaction.setCpColor("#808080");
        					checkPointStatusTransaction.setCpDecisionPoint(0);
        					checkPointStatusTransaction.setCpTransactionStatus("A");
        					checkPointStatusTransaction.setStatus("I");
        					checkPointStatusTransaction.setCreatedDate(currentDate);
        					checkPointStatusTransaction.setCreatedByUser(userMaster.getUserId().toString());
        					checkPointStatusTransaction.setIpAddress(ipAddress);
        					statelessSession.update(checkPointStatusTransaction);
        					id++;
        					}
        				
    					}
    				}
    			}	
    	}
    }
	transaction.commit();
	statelessSession.close();
    }catch(Exception e){e.printStackTrace();}
	
}
@SuppressWarnings("unchecked")
public String getCandidateDetails(int checkPointId,String checkPointName,Integer districtId,Integer onBoardingId){
	String question="";
    int checkPointTypeId=0; 
    int conmmunicationRequireds=0;
    String actionPerformedBy="";
    String status ="";
    String singleText="";
    String options ="";
    String validOption ="";
    String candisate="0";
    String user="0";
    int checkPointQuestionId=0;
    int chkpointid=0;
    int chkoptid=0;
    String cpCheckStatusTransId="";
    String documentFileName="";
    String candidatetype="E-";
	try{
		 List<EditOnboardDashboard> editDashboardRecords = editOnboardingDashboardDAO.getEditDashboardRecords(districtId, onBoardingId, checkPointId);
		 for(EditOnboardDashboard result:editDashboardRecords){
			 if(result.getConmmunicationRequired()!=null)
					conmmunicationRequireds = result.getConmmunicationRequired();
					chkpointid= result.getCheckPointTransactionId();
					question=result.getCheckPointName();
					if(result.getActionPerformedBy()!=null){
					  actionPerformedBy = result.getActionPerformedBy();
					  String[] split = actionPerformedBy.split(",");
						
							candisate=split[0];
							user=split[1];
					}
							status = result.getCheckPointStatus();
					if(result.getCheckPointQuestionId()!=null)
					checkPointQuestionId=result.getCheckPointQuestionId();
					if(result.getCheckPointTypeId()!=null)
					{
					checkPointTypeId = result.getCheckPointTypeId();
					documentFileName = result.getDocumentFileName();
					singleText= result.getQuestion();
					candidatetype=result.getCandidateType();
					}
					if(result.getCheckPointOptionsId()!=null){
							chkoptid=result.getCheckPointOptionsId();
							options =result.getOptions();
							validOption = result.getValidOption();
					}
							
			 
		 }
		
	}catch(Exception e){e.printStackTrace();}
	return question+"^"+checkPointTypeId+"^"+singleText+"^"+conmmunicationRequireds+"^"+candisate+"^"+user+"^"+status+"^"+options+"^"+validOption+"^"+chkpointid+"^"+checkPointQuestionId+"^"+chkoptid+"^"+cpCheckStatusTransId+"^"+documentFileName+"^"+candidatetype;
}
public void updateReadOnlyCheckPoint(int masterCount,int checkPointId,String checkPointName,String status,int districtId,int onBoardId,int counter){
	
	try {
		WebContext context;UserMaster userMaster 			= 	null;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException("Your session has expired!");
	    }else {
			userMaster = (UserMaster)session.getAttribute("userMaster");
		}
		String ipAddress = IPAddressUtility.getIpAddress(request);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String format = sdf.format(new Date());
		Date parseDate = sdf.parse(format);
		String[] chkPointNameSplit = checkPointName.split("-");
		String[] splitStatus = status.split("-");
		
		//List<ReadOnlyCheckPointTransaction> readOnlyCheckPointTrans = readOnlyCheckPointTransactionDAO.getReadOnlyCheckPointRecord(districtId,onBoardId);
		ReadOnlyCheckPointTransaction readOnlyCheckPoint=null;readOnlyCheckPoint=new ReadOnlyCheckPointTransaction();
		SessionFactory sessions = readonlyCheckPointMasterDAO.getSessionFactory();
		StatelessSession StatelessSession = sessions.openStatelessSession();
		Transaction transaction = StatelessSession.beginTransaction();
		if(masterCount!=0){
			//if(readOnlyCheckPointTrans.size()==0){
			
			int i=0;
			
			List<ReadOnlyCheckPointMaster> readOnlyCheckPointRecord = readonlyCheckPointMasterDAO.getReadOnlyCheckPointRecord();
			for(ReadOnlyCheckPointMaster re: readOnlyCheckPointRecord){
				readOnlyCheckPoint.setCheckPointMasterId(re.getCheckPointId());
				readOnlyCheckPoint.setOnBoardingId(onBoardId);
				readOnlyCheckPoint.setDistrictId(districtId);
				readOnlyCheckPoint.setCheckPointName(chkPointNameSplit[i]);
				readOnlyCheckPoint.setCreatedDate(parseDate);
				readOnlyCheckPoint.setCreatedByUser(userMaster.getUserId().toString());
				readOnlyCheckPoint.setModifiedDate(parseDate);
				readOnlyCheckPoint.setModifiedByUser(userMaster.getUserId().toString());
				readOnlyCheckPoint.setIpAddress(ipAddress);
				readOnlyCheckPoint.setStatus(splitStatus[i]);
				StatelessSession.insert(readOnlyCheckPoint);
				i++;
			}
			transaction.commit();
			StatelessSession.close();
			//}
		}else{
		SessionFactory sessionFactory = readOnlyCheckPointTransactionDAO.getSessionFactory();
		StatelessSession openStatelessSession = sessionFactory.openStatelessSession();
		Transaction beginTransaction = openStatelessSession.beginTransaction();
		
		int toCounter=counter+5;
		int masterId=0;
		for(int i=counter;i<=toCounter;i++){
		readOnlyCheckPoint.setCheckPointTransactionId(i);
		readOnlyCheckPoint.setCheckPointMasterId(masterId+1);
		readOnlyCheckPoint.setOnBoardingId(onBoardId);
		readOnlyCheckPoint.setDistrictId(districtId);
		readOnlyCheckPoint.setCheckPointName(chkPointNameSplit[masterId]);
		readOnlyCheckPoint.setCreatedDate(parseDate);
		readOnlyCheckPoint.setCreatedByUser(userMaster.getUserId().toString());
		readOnlyCheckPoint.setModifiedDate(parseDate);
		readOnlyCheckPoint.setModifiedByUser(userMaster.getUserId().toString());
		readOnlyCheckPoint.setIpAddress(ipAddress);
		readOnlyCheckPoint.setStatus(splitStatus[masterId]);
		openStatelessSession.update(readOnlyCheckPoint);
		
		masterId++;
		}
		beginTransaction.commit();
		openStatelessSession.close();
		}
		
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
}
public void updateCheckPointTransaction(int districtID,int onboardID ){
	try{
	AddCheckPointTransaction checkPointTransaction=new AddCheckPointTransaction();
	WebContext context;UserMaster userMaster 			= 	null;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
		throw new IllegalStateException("Your session has expired!");
    }else {
		userMaster = (UserMaster)session.getAttribute("userMaster");
	}
	String ipAddress = IPAddressUtility.getIpAddress(request);
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	String format = sdf.format(new Date());
	Date parseDate = sdf.parse(format);
	
	List<CheckPointTransaction> checkPointStatusRecord = checkPointTransactionDAO.getCheckPointRecord(districtID, onboardID);
	
	SessionFactory sessions = readonlyCheckPointMasterDAO.getSessionFactory();
	StatelessSession StatelessSession = sessions.openStatelessSession();
	Transaction transaction = StatelessSession.beginTransaction();
	
	if(checkPointStatusRecord.size()==0){
		List<CheckPointMaster> getcheckpoints = checkPointMasterDAO.getcheckpoints();
		for(CheckPointMaster re:getcheckpoints){
			checkPointTransaction.setCheckPointMasterId(re.getCheckPointId());
			checkPointTransaction.setOnBoardingId(onboardID);
			checkPointTransaction.setDistrictId(districtID);
			checkPointTransaction.setCheckPointName(re.getCheckPointName());
			checkPointTransaction.setConmmunicationRequired(0);
			checkPointTransaction.setActionPerformedBy(null);
			checkPointTransaction.setCheckPointStatus("0");
			checkPointTransaction.setCreatedByUser(userMaster.getUserId());
			checkPointTransaction.setCreatedDate(parseDate);
			checkPointTransaction.setModifiedByUser(null);
			checkPointTransaction.setModifiedDate(null);
			checkPointTransaction.setIpAddress(ipAddress);
			checkPointTransaction.setActionPerformedBy("0,0");
			checkPointTransaction.setStatus("A");
			StatelessSession.insert(checkPointTransaction);
		}
		transaction.commit();
		StatelessSession.close();
	}
	
	}catch(Exception e){e.printStackTrace();}
}
public String validateApplicantType(String candidateType){
	String defCandidateType="E";
	if(candidateType.equals("I")){
		defCandidateType="I";
	}else if(candidateType.equals("T")){
		defCandidateType="E";
	}else{
		defCandidateType="E";
	}
	return defCandidateType;
}
public void saveCheckPointAction(Integer districtId,Integer onBoardingId, String[] actionWithCheckPointId,String []candidateType,Integer checkPointFlag)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	UserMaster userMaster 			= 	null;
	try{
		
		Date currentDate	=	new Date();
		HttpSession session = request.getSession(false);
		String ipAddress = IPAddressUtility.getIpAddress(request);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }else {
			userMaster = (UserMaster)session.getAttribute("userMaster");
		}
		SessionFactory sessions = checkPointActionDAO.getSessionFactory();
		StatelessSession statelessSession = sessions.openStatelessSession();
		Transaction transaction = statelessSession.beginTransaction();
		CheckPointAction checkPointAction=new CheckPointAction();
		
		 List<CheckPointAction> checkPointActionRecordCount = checkPointActionDAO.getCheckPointActionRecordCount(districtId, onBoardingId);
		
		Integer checkPointId=0;
		for(int j=0;j<actionWithCheckPointId.length;j++){
			String actiondata[]=actionWithCheckPointId[j].split("###");
			checkPointId=Utility.getIntValue(actiondata[0]);
			String actionValue[]=actiondata[1].split("::");
			String statusValue[]=actiondata[2].split("::");
			String actionIdValue[]=actiondata[3].split("::");

			for(int i=0;i<candidateType.length;i++){
				checkPointAction.setDistrictId(districtId);
				checkPointAction.setOnBoardingId(onBoardingId);
				checkPointAction.setCheckPointId(checkPointId);				
				checkPointAction.setAction(Utility.getIntValue(actionValue[i]));
				checkPointAction.setCheckPointStatus(Utility.getIntValue(statusValue[i]));
				checkPointAction.setCandidateType(candidateType[i]);
				checkPointAction.setCreatedDate(currentDate);
				checkPointAction.setCreatedBy(userMaster.getEntityType());
				checkPointAction.setModifiedDate(null);
				checkPointAction.setModifiedBy(null);
				checkPointAction.setIpAddress(ipAddress);
				checkPointAction.setStatus("A");
				if(actionIdValue[i].equalsIgnoreCase("null") || actionIdValue[i].equalsIgnoreCase("0") || checkPointActionRecordCount.size()==0){
					checkPointAction.setActionId(null);
					statelessSession.insert(checkPointAction);
				}
				else if(checkPointActionRecordCount.size()>0){
					checkPointAction.setActionId(Utility.getIntValue(actionIdValue[i]));
					statelessSession.update(checkPointAction);
				
				}
			}
		}
		transaction.commit();
		statelessSession.close();
	}catch (Exception e) {
		e.printStackTrace();
	}
}
public boolean saveCheckPointStatusClone(Integer districtId,Integer onboardingId,Integer oldOnBoardingId,String []checkPointIds){
	List<CheckPointStatusTransaction> checkPointStatuslst=null;
	Boolean insertionFlag=false;
	try{
		Date currentDate	=	new Date();
		checkPointStatuslst=checkPointStatusTransactionDAO.getCheckPointStatusRecordCount(districtId, oldOnBoardingId);
		if(checkPointStatuslst.size()>0){
			int i=0;
			SessionFactory sessions = checkPointStatusTransactionDAO.getSessionFactory();
			StatelessSession statelessSession = sessions.openStatelessSession();
			Transaction transaction = statelessSession.beginTransaction();
			CheckPointStatusTransaction checkPointStatusTransaction=null;
			System.out.println("CheckPointid Size ::: "+checkPointIds.length+" List Size;"+checkPointStatuslst.size());
			for(CheckPointStatusTransaction rec:checkPointStatuslst){
				checkPointStatusTransaction=new CheckPointStatusTransaction();
				checkPointStatusTransaction=rec;
				//BeanUtils.copyProperties(checkPointStatusTransaction, rec);
				
				checkPointStatusTransaction.setCreatedDate(currentDate);
				checkPointStatusTransaction.setCpColor(rec.getCpColor());
				//checkPointStatusTransaction.setCheckPointId(Utility.getIntValue(checkPointIds[i++]));
				checkPointStatusTransaction.setOnboardingId(onboardingId);
				statelessSession.insert(checkPointStatusTransaction);
			}
			transaction.commit();
			statelessSession.close();
			insertionFlag=true;
		}else{
			insertionFlag=false;
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
	return insertionFlag;
}
public boolean saveCheckPointCommunicationClone(Integer districtId,Integer onboardingId,Integer oldOnBoardingId,String []checkPointIds){
	List<OnBoardingCommunication> OnBoardingCommunicationlst=null;
	Boolean insertionFlag=false;
	try{
		Date currentDate	=	new Date();
		OnBoardingCommunicationlst=onBoardingCommunicationDAO.findCommunicationByDistrictAndOnboardingId(districtId, oldOnBoardingId);
		SessionFactory sessions = onBoardingCommunicationDAO.getSessionFactory();
		StatelessSession statelessSession = sessions.openStatelessSession();
		Transaction transaction = statelessSession.beginTransaction();
		OnBoardingCommunication onBoardingCommunication=null;
		int i=0;
		if(OnBoardingCommunicationlst.size()>0){
			for(OnBoardingCommunication rec:OnBoardingCommunicationlst){
				onBoardingCommunication=new OnBoardingCommunication();
				onBoardingCommunication=rec;
				//BeanUtils.copyProperties(onBoardingCommunication, rec);
				
				onBoardingCommunication.setCommunicationId(null);
				onBoardingCommunication.setCreatedDate(currentDate);
				//onBoardingCommunication.setCheckPointId(Utility.getIntValue(checkPointIds[i++]));
				onBoardingCommunication.setOnBoardingId(onboardingId);
				statelessSession.insert(onBoardingCommunication);
			}
			transaction.commit();
			statelessSession.close();
			insertionFlag=true;
		}else{
			insertionFlag=false;
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
	return insertionFlag;
}
public boolean saveCheckPointPermissionClone(Integer districtId,Integer onboardingId,Integer oldOnBoardingId,String []checkPointIds){
	List<CheckPointPermissionTransaction> CheckPointPermissionTransactionlst=null;
	Boolean insertionFlag=false;
	try{
		Date currentDate	=	new Date();
		CheckPointPermissionTransactionlst=checkPointPermissionTransactionDAO.findcheckPointPermissionByDistrictAndOnboardingId(districtId, oldOnBoardingId);
		if(CheckPointPermissionTransactionlst.size()>0){
			SessionFactory sessions = checkPointPermissionTransactionDAO.getSessionFactory();
			StatelessSession statelessSession = sessions.openStatelessSession();
			Transaction transaction = statelessSession.beginTransaction();
			CheckPointPermissionTransaction checkPointPermissionTransaction=null;
			int i=0;
		for(CheckPointPermissionTransaction rec:CheckPointPermissionTransactionlst){
			checkPointPermissionTransaction=new CheckPointPermissionTransaction();
			checkPointPermissionTransaction=rec;
			//BeanUtils.copyProperties(checkPointPermissionTransaction, rec);
			
			checkPointPermissionTransaction.setCreatedDate(currentDate);
			checkPointPermissionTransaction.setPermissionTransactionId(null);
			//checkPointPermissionTransaction.setCheckPointId(Utility.getIntValue(checkPointIds[i++]));
			checkPointPermissionTransaction.setOnBoardingId(onboardingId);
			statelessSession.insert(checkPointPermissionTransaction);
		}
		transaction.commit();
		statelessSession.close();
		insertionFlag=true;
		}else{
			insertionFlag=false;
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
	return insertionFlag;
}
}