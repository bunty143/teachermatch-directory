package tm.propertyeditor;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.master.CityMaster;
import tm.dao.master.CityMasterDAO;

public class CityPropertyEditor extends PropertyEditorSupport
{
	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO) 
	{
		this.cityMasterDAO = cityMasterDAO;
	}
	
	@Override
	public void setAsText(String text) throws IllegalArgumentException 
	{

		//Category category=null;
		CityMaster cityMaster = null;
		try
		{	
			if(text!=null && text.trim().length()>0)
			{
				
				if(text!=null)
				{
					try
					{
						cityMaster=cityMasterDAO.findById(Long.parseLong(text), false, true);
						setValue(cityMaster);
					}
					catch(Exception ex)
					{	
						
						cityMaster=new CityMaster();
						cityMaster.setCityName(text);
						setValue(cityMaster);	
					}		
				}						
			}
			else
			{
				setValue(null);
			}
		}
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}	
	
	}
	

}
