package tm.propertyeditor;
import java.beans.PropertyEditorSupport;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.master.StateMaster;
import tm.dao.master.StateMasterDAO;

public class StatePropertyEditor   extends PropertyEditorSupport
{
	@Autowired
	StateMasterDAO stateMasterDao;
	public void setStateMasterDao(StateMasterDAO stateMasterDao) {
		this.stateMasterDao = stateMasterDao;
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException 
	{
		//Category category=null;
		StateMaster stateMaster=null;
		try
		{	
			if(text!=null && text.trim().length()>0)
			{
				
				if(text!=null)
				{
					try
					{
						stateMaster=stateMasterDao.findById(Long.parseLong(text), false, true);
						setValue(stateMaster);
					}
					catch(Exception ex)
					{	
						
						stateMaster=new StateMaster();
						stateMaster.setStateShortName(text);
						setValue(stateMaster);	
					}		
				}						
			}
			else
			{
				setValue(null);
			}
		}
		catch (Exception e) 
		{	
			e.printStackTrace();
		}	
	}
}
