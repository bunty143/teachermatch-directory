package tm.propertyeditor;
import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.master.CountryMaster;
import tm.dao.master.CountryMasterDAO;

public class CountryPropertyEditor   extends PropertyEditorSupport
{
	@Autowired
	CountryMasterDAO countryMasterDAO;
	public void setCountryMasterDAO(CountryMasterDAO countryMasterDAO) {
		this.countryMasterDAO = countryMasterDAO;
	}
	
	@Override
	public void setAsText(String text) throws IllegalArgumentException 
	{
		//Category category=null;
		CountryMaster countryMaster=null;
		try
		{	
			if(text!=null && text.trim().length()>0)
			{
				
				if(text!=null)
				{
					try
					{
						Integer iCountryId=0;
						try {
							iCountryId=Integer.parseInt(text);
						} catch (Exception e) {
							// TODO: handle exception
						}
						countryMaster=countryMasterDAO.findById(iCountryId, false, false);
						setValue(countryMaster);
					}
					catch(Exception ex)
					{	
						ex.printStackTrace();
						/*countryMaster=new CountryMaster();
						setValue(countryMaster);*/	
					}		
				}						
			}
			else
			{
				setValue(null);
			}
		}
		catch (Exception e) 
		{	
			e.printStackTrace();
		}	
	}
}
