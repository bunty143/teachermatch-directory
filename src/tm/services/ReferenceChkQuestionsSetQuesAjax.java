package tm.services;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificRefChkQuestions;
import tm.bean.master.ReferenceQuestionSetQuestions;
import tm.bean.master.ReferenceQuestionSets;
import tm.bean.user.UserMaster;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.i4.I4QuestionPoolDAO;
import tm.dao.i4.I4QuestionSetQuestionsDAO;
import tm.dao.i4.I4QuestionSetsDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificRefChkQuestionsDAO;
import tm.dao.master.ReferenceQuestionSetQuestionsDAO;
import tm.dao.master.ReferenceQuestionSetsDAO;
import tm.utility.Utility;

public class ReferenceChkQuestionsSetQuesAjax {

	
	String locale = Utility.getValueOfPropByKey("locale");
	@Autowired
	private I4QuestionPoolDAO i4QuestionPoolDAO;
	
	@Autowired
	private I4QuestionSetsDAO i4QuestionSetsDAO;
	
	@Autowired
	private I4QuestionSetQuestionsDAO i4QuestionSetQuestionsDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private DistrictSpecificRefChkQuestionsDAO districtSpecificRefChkQuestionsDAO;
	
	@Autowired
	private ReferenceQuestionSetsDAO referenceQuestionSetsDAO;
	
	@Autowired
	private ReferenceQuestionSetQuestionsDAO referenceQuestionSetQuestionsDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	// DIsplay Questions from question pool
	public String displayQuestions(String noOfRow, String pageNo,String sortOrder,String sortOrderType,String quesSetSearchText,String quesSetId,String districtId , String headQuarterId)
	{
		System.out.println(":::::::::::::::::::: displayQuestions :::::::::::::::::::::::");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		
		try{
			
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			boolean allData	   	= false;
			boolean quesTextflag= false;
			
			List<DistrictSpecificRefChkQuestions> districtSpecificRefChkQuestions	=	new ArrayList<DistrictSpecificRefChkQuestions>();
			List<DistrictSpecificRefChkQuestions> filterData						=	new ArrayList<DistrictSpecificRefChkQuestions>();
			List<DistrictSpecificRefChkQuestions> finalDatalist						=	new ArrayList<DistrictSpecificRefChkQuestions>();
			List<DistrictSpecificRefChkQuestions> i4QuesForQues						=	new ArrayList<DistrictSpecificRefChkQuestions>();
			List<DistrictSpecificRefChkQuestions> repeatedQues						=   new ArrayList<DistrictSpecificRefChkQuestions>();
			
			List<ReferenceQuestionSetQuestions> 	quesofQuesSetList	=   new ArrayList<ReferenceQuestionSetQuestions>();
			
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"QuestionText";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			
			ReferenceQuestionSets referenceQuestionSets = referenceQuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
			
			DistrictMaster districtMaster = null;
			HeadQuarterMaster headQuarterMaster = null;
			if(districtId!=null && !districtId.equals(""))
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			
			if(headQuarterId!=null && !headQuarterId.equals(""))
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);
			
			
			districtSpecificRefChkQuestions = districtSpecificRefChkQuestionsDAO.getDistrictRefChkQuestionBYDistrict(districtMaster,headQuarterMaster);
			
			quesofQuesSetList	= referenceQuestionSetQuestionsDAO.findByQuesSetId(referenceQuestionSets,districtMaster,headQuarterMaster);
			
			for(ReferenceQuestionSetQuestions i4QSQ:quesofQuesSetList)
			{
				repeatedQues.add(i4QSQ.getDistrictSpecificRefChkQuestions());
			}
			if(quesofQuesSetList.size()>0)
			{
				districtSpecificRefChkQuestions.removeAll(repeatedQues);
			}
			
			if(districtSpecificRefChkQuestions.size()>0)
			{
				filterData.addAll(districtSpecificRefChkQuestions);
				allData = true;
			}
			
			if(quesSetSearchText!=null && !quesSetSearchText.equalsIgnoreCase(""))
			{
				i4QuesForQues = districtSpecificRefChkQuestionsDAO.findi4QuesByQues(quesSetSearchText);
				quesTextflag = true;
			}
			System.out.println(quesTextflag);
			if(quesTextflag==true)
				filterData.retainAll(i4QuesForQues);
			else
				filterData.addAll(i4QuesForQues);
			
			totalRecord = filterData.size();
			
			if(totalRecord<end)
				end=totalRecord;
			
			finalDatalist	=	filterData.subList(start,end);
			System.out.println("::::::::::::: finalDatalist :::::::::::::"+finalDatalist.size());
			dmRecords.append("<table  id='refChkQuesTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");

			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblQues", locale),sortOrderFieldName,"QuestionText",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='55%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblcretedDate", locale),sortOrderFieldName,"DateCreated",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			dmRecords.append("<th width='15%'><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</span></th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(finalDatalist.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoQuestionfound1", locale)+"</td></tr>" );
			System.out.println(Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+districtSpecificRefChkQuestions.size());

			for (DistrictSpecificRefChkQuestions i4qp: finalDatalist) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+i4qp.getQuestion()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(i4qp.getCreatedDateTime())+"</td>");
				dmRecords.append("<td><a href='javascript:void(0);' onclick='addQuesFromQPtoQS("+i4qp.getQuestionId()+")'>"+Utility.getLocaleValuePropByKey("lnkAddQues", locale)+"</a>" +
                		" | <a href='referencecheckspecificquestions.do?districtId="+(districtMaster!=null?districtMaster.getDistrictId():"")+"&headQuarterId="+(headQuarterMaster!=null?headQuarterMaster.getHeadQuarterId():"")+"&questionId="+i4qp.getQuestionId()+"&quesSetId="+quesSetId+"' >Edit</a>" +
                		"</td>");
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	
	// Display Questions in Question Set
	public String displayQuesSetQues(String quesSetId,String districtId ,String headQuarterId)
	{
		System.out.println(" =========== displayQuesSetQues ========="+quesSetId);
		
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			
			List<ReferenceQuestionSetQuestions> referenceQuestionSetQuestions = new ArrayList<ReferenceQuestionSetQuestions>(); 
			
			ReferenceQuestionSets referenceQuestionSets = referenceQuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
			
			DistrictMaster districtMaster = null;
			HeadQuarterMaster headQuarterMaster =	null;
			if(districtId!=null && !districtId.equals(""))
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			
			if(headQuarterId!=null && !headQuarterId.equals(""))
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);
			
			
			referenceQuestionSetQuestions = referenceQuestionSetQuestionsDAO.findByQuesSetId(referenceQuestionSets,districtMaster,headQuarterMaster);

			dmRecords.append("<table  id='refChkQuesSetQuesTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");

			dmRecords.append("<th width='55%' valign='top'><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblQues", locale)+"</span></th>");

			dmRecords.append("<th width='15%'><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</span></th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");

			/*================= Checking If Record Not Found ======================*/
			if(referenceQuestionSetQuestions.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoQuestionfound1", locale)+"</td></tr>" );
			System.out.println(Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+referenceQuestionSetQuestions.size());

			for (int i=0; i<referenceQuestionSetQuestions.size();i++ )
			{
				String upButton ="";
				String downButton ="";
				
				System.out.println(" ");
				
				if(i==0)
					upButton = Utility.getLocaleValuePropByKey("msgUp3", locale);
				else
					upButton = "<a href='javascript:void(0);' disabled='disabled' onclick='moveUpQues("+referenceQuestionSetQuestions.get(i).getID()+");'>"+Utility.getLocaleValuePropByKey("msgUp3", locale)+"</a>";
				
				if(i==referenceQuestionSetQuestions.size()-1)
					downButton = Utility.getLocaleValuePropByKey("msgDown3", locale);
				else
					downButton = "<a href='javascript:void(0);' onclick='moveDownQues("+referenceQuestionSetQuestions.get(i).getID()+");'>"+Utility.getLocaleValuePropByKey("msgDown3", locale)+"</a>";
				
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+referenceQuestionSetQuestions.get(i).getDistrictSpecificRefChkQuestions().getQuestion()+"</td>");
				dmRecords.append("<td>");
				
				dmRecords.append(upButton+" | "+downButton+" | <a href='javascript:void(0);' onclick='deleteQuesFromQuesSet("+referenceQuestionSetQuestions.get(i).getID()+");'>"+Utility.getLocaleValuePropByKey("lnkRemo", locale)+"</a>");
				dmRecords.append("</td>");
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	public int addQuesFromQPtoQS(String quesId,String quesSetId,String districtId, String headQuarterId)
	{
		System.out.println(" addQuesFromQPtoQS ");
		
		System.out.println("************addQuesFromQPtoQS**************** HeadQuarterId = "+headQuarterId);
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try
		{
			DistrictSpecificRefChkQuestions districtSpecificRefChkQuestions = districtSpecificRefChkQuestionsDAO.findById(Integer.parseInt(quesId), false, false);
			ReferenceQuestionSets referenceQuestionSets = referenceQuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
			DistrictMaster districtMaster = null;						// @ Anurag
			HeadQuarterMaster headQuarterMaster = null;
			if(districtId!=null && !districtId.equals(""))
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);

			if(headQuarterId!=null && !headQuarterId.equals(""))
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);

			ReferenceQuestionSetQuestions referenceQuestionSetQuestions = new ReferenceQuestionSetQuestions();
			int quesSequence =0;
			try
			{
				List<ReferenceQuestionSetQuestions> existQuesList = new ArrayList<ReferenceQuestionSetQuestions>();
				existQuesList = referenceQuestionSetQuestionsDAO.findByQuesSetId(referenceQuestionSets,districtMaster,headQuarterMaster);
				
				if(existQuesList.size()==0)
				{
					quesSequence=1;
				}
				else
				{
					if(existQuesList.size()>0)
					{
						quesSequence = existQuesList.get(existQuesList.size()-1).getQuestionSequence()+1; 
					}
				}
				
				referenceQuestionSetQuestions.setQuestionSequence(quesSequence);
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			referenceQuestionSetQuestions.setDistrictSpecificRefChkQuestions(districtSpecificRefChkQuestions);
			referenceQuestionSetQuestions.setQuestionSets(referenceQuestionSets);
			
			referenceQuestionSetQuestionsDAO.makePersistent(referenceQuestionSetQuestions);
		
			return 1;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	//Delete Question from Question Set And display in Question Pool
	public int deleteQuesFromQuesSet(String quesSetQuesID)
	{
		 /*========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try
		{
			ReferenceQuestionSetQuestions referenceQuestionSetQuestions = new ReferenceQuestionSetQuestions();

			referenceQuestionSetQuestions = referenceQuestionSetQuestionsDAO.findById(Integer.parseInt(quesSetQuesID), false, false);
			
			referenceQuestionSetQuestionsDAO.makeTransient(referenceQuestionSetQuestions);
			
			return 1;
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return 0;
	}
	
	// Question Move Up in question set
	public int moveUpQues(String quesSetQuesID,String districtId,String headQuarterId)
	{
		 /*========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try
		{
			ReferenceQuestionSetQuestions forMoveQues = new ReferenceQuestionSetQuestions();
			
			if(quesSetQuesID!=null && !quesSetQuesID.equals(""))
				forMoveQues = referenceQuestionSetQuestionsDAO.findById(Integer.parseInt(quesSetQuesID), false, false);
					
			ReferenceQuestionSets referenceQuestionSets = referenceQuestionSetsDAO.findById(forMoveQues.getQuestionSets().getID(), false, false);
			
			DistrictMaster districtMaster = null;		
			HeadQuarterMaster headQuarterMaster=null;		// @ Anurag
			if(districtId!=null && !districtId.equals(""))
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);

			if(headQuarterId!=null && !headQuarterId.equals(""))
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);

			
			List<ReferenceQuestionSetQuestions> existQuesList = new ArrayList<ReferenceQuestionSetQuestions>();
			List<ReferenceQuestionSetQuestions> updatedObjList = new ArrayList<ReferenceQuestionSetQuestions>();
			
			existQuesList = referenceQuestionSetQuestionsDAO.findByQuesSetId(referenceQuestionSets,districtMaster,headQuarterMaster);
			
			ReferenceQuestionSetQuestions first = new ReferenceQuestionSetQuestions();
			ReferenceQuestionSetQuestions second = new ReferenceQuestionSetQuestions();
			
			int first_QS = 0;
			int second_QS = 0;
			
			
			for(int i=existQuesList.size()-1; i>0;i--)
			{
				System.out.println(existQuesList.get(i).getID()+" -----"+referenceQuestionSets.getID()+" ====================== >>>>>>>>>>>>>>> "+existQuesList.get(i).getID().equals(referenceQuestionSets.getID()));
				
				if(existQuesList.get(i).getID().equals(forMoveQues.getID()))
				{
					first = existQuesList.get(i);
					second = existQuesList.get(i-1);
					break;
				}
			}
			first_QS = first.getQuestionSequence();
			second_QS = second.getQuestionSequence();
			
			//System.out.println("Before ::::::: first :: "+first.getQuestionSequence()+" second :: "+second.getQuestionSequence());
			
			first.setQuestionSequence(second_QS);
			second.setQuestionSequence(first_QS);
			
			//System.out.println("After ::::::: first :: "+first.getQuestionSequence()+" second :: "+second.getQuestionSequence());
			
			updatedObjList.add(first);
			updatedObjList.add(second);
			
			for(ReferenceQuestionSetQuestions I4QSQ:updatedObjList)
			{
				referenceQuestionSetQuestionsDAO.makePersistent(I4QSQ);
			}
			
			return 1;
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return 0;
	}
	
	// Question Move down in question set 
	public int moveDownQues(String quesSetQuesID, String districtId  ,String headQuarterId)
	{
		
		 /*========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try
		{
			ReferenceQuestionSetQuestions forMoveQues = new ReferenceQuestionSetQuestions();
			
			if(quesSetQuesID!=null && !quesSetQuesID.equals(""))
				forMoveQues = referenceQuestionSetQuestionsDAO.findById(Integer.parseInt(quesSetQuesID), false, false);
					
			ReferenceQuestionSets referenceQuestionSets = referenceQuestionSetsDAO.findById(forMoveQues.getQuestionSets().getID(), false, false);
			
			DistrictMaster districtMaster = null;
			HeadQuarterMaster headQuarterMaster = null ; 	// @ Anurag
			if(districtId!=null && !districtId.equals(""))
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			
			if(headQuarterId!=null && !headQuarterId.equals(""))
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);
			
			List<ReferenceQuestionSetQuestions> existQuesList = new ArrayList<ReferenceQuestionSetQuestions>();
			List<ReferenceQuestionSetQuestions> updatedObjList = new ArrayList<ReferenceQuestionSetQuestions>();
			
			existQuesList = referenceQuestionSetQuestionsDAO.findByQuesSetId(referenceQuestionSets,districtMaster,headQuarterMaster);
			
			ReferenceQuestionSetQuestions first = new ReferenceQuestionSetQuestions();
			ReferenceQuestionSetQuestions second = new ReferenceQuestionSetQuestions();
			
			int first_QS = 0;
			int second_QS = 0;
			
			for(int i=0;i<existQuesList.size()-1;i++)
			{
				if(existQuesList.get(i).getID().equals(forMoveQues.getID()))
				{
					first = existQuesList.get(i);
					second = existQuesList.get(i+1);
					break;
				}
			}
			first_QS = first.getQuestionSequence();
			second_QS = second.getQuestionSequence();
			
			System.out.println("Before ::::::: first :: "+first.getQuestionSequence()+" second :: "+second.getQuestionSequence());
			
			first.setQuestionSequence(second_QS);
			second.setQuestionSequence(first_QS);
			
			System.out.println("After ::::::: first :: "+first.getQuestionSequence()+" second :: "+second.getQuestionSequence());
			
			updatedObjList.add(first);
			updatedObjList.add(second);
			
			for(ReferenceQuestionSetQuestions I4QSQ:updatedObjList)
			{
				referenceQuestionSetQuestionsDAO.makePersistent(I4QSQ);
			}
			
			return 1;
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return 0;
	}
	

}
