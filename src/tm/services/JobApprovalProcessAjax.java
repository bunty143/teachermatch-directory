package tm.services;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictApprovalGroups;
import tm.bean.DistrictWiseApprovalGroup;
import tm.bean.JobApprovalProcess;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictApprovalGroupsDAO;
import tm.dao.DistrictWiseApprovalGroupDAO;
import tm.dao.JobApprovalProcessDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.utility.Utility;

public class JobApprovalProcessAjax {
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private DistrictApprovalGroupsDAO districtApprovalGroupsDAO;
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	
	@Autowired
	private JobApprovalProcessDAO jobApprovalProcessDAO;
	
	@Autowired
	private DistrictWiseApprovalGroupDAO districtWiseApprovalGroupDAO;
	
	static String locale = Utility.getValueOfPropByKey("locale");
	
	public String displayApprocalProcess(String districtName,Integer districtId,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		System.out.println("::::::::::::::::::::::::::::::::: displayGroups :::::::::::::::::::::::::::::::::");
		System.out.println("districtId : "+districtId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			throw new IllegalStateException("Your session has expired!");
		StringBuffer tmRecords = new StringBuffer();
		int sortingcheck=1;
		try
		{
			UserMaster userMaster = null;
			Integer entityID = null;
			DistrictMaster districtMaster = null;
			
			if (session==null || session.getAttribute("userMaster")==null)
				return "false";
			else
			{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getDistrictId()!=null)
					districtMaster=userMaster.getDistrictId();
				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
				
			/** set default sorting fieldName **/
			String sortOrderFieldName="groupName";
			String sortOrderNoField="groupName";
			String locale = Utility.getValueOfPropByKey("locale");
			String lblDeactivate = Utility.getLocaleValuePropByKey("lblDeactivate", locale);
			String lblActivate = Utility.getLocaleValuePropByKey("lblActivate", locale);
			
			if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("groupName"))
				sortingcheck=1;
				
			//System.out.println("Sort order :: "+sortingcheck);
			/**Start set dynamic sorting fieldName **/
				
			Order sortOrderStrVal=null;

			if(sortOrder!=null)
			{
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}				
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
			
		    if(totalRecord<end)
		    	end=totalRecord;
		    
			String responseText="";
			
			tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			//responseText=PaginationAndSorting.responseSortingLink("Group Name",sortOrderNoField,"groupName",sortOrderTypeVal,pgNo);
			//tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			tmRecords.append("<th  valign='top'>Job Approval Process</th>");
			tmRecords.append("<th  valign='top'>Approval Required</th>");
			tmRecords.append("<th  valign='top'>Group Name</th>");
			//responseText=PaginationAndSorting.responseSortingLink("Members",sortOrderNoField,"",sortOrderTypeVal,pgNo);
			//tmRecords.append("<th valign='top'>"+responseText+"</th>");			
			tmRecords.append("<th  valign='top'>Actions</th>");
					
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(districtId!=null){
				try {
					if(districtMaster==null)
						districtMaster = districtMasterDAO.findById(districtId, false, false);
					
				//	List<JobApprovalProcess> jobApprovalProcessList = jobApprovalProcessDAO.findAllJobApprovalProcessByDistrict(districtMaster);
					List<JobApprovalProcess> jobApprovalProcessList = jobApprovalProcessDAO.findActiveInactiveJobApprovalProcessByDistrict(districtMaster);
					
					if(jobApprovalProcessList!=null && jobApprovalProcessList.size()==0)
						tmRecords.append("<tr><td colspan='5' align='center' class='net-widget-content'>No Record found</td></tr>" );
					
					if(jobApprovalProcessList!=null && jobApprovalProcessList.size()>0)
					{
						Set<Integer> groupsId = new TreeSet<Integer>();
						for(JobApprovalProcess jobApprovalProcess :jobApprovalProcessList)
						{
							String[] groupName = (jobApprovalProcess.getJobApprovalGroups()!=null)? jobApprovalProcess.getJobApprovalGroups().split("\\|") : null;
							if(groupName!=null && groupName.length >0)
								for(String member : groupName)
									if(!member.equals(""))
										groupsId.add(Integer.parseInt(member));
						}
						
						Map<String, DistrictApprovalGroups> groupMap = new HashMap<String, DistrictApprovalGroups>();
						if(groupsId!=null && groupsId.size()>0)
						{
							List<DistrictApprovalGroups> groupList = districtApprovalGroupsDAO.findByCriteria(Restrictions.in("districtApprovalGroupsId", groupsId));
							for(DistrictApprovalGroups group : groupList)
								groupMap.put(""+group.getDistrictApprovalGroupsId(), group);
						}
						
						groupsId=null;
						for(JobApprovalProcess jobApprovalProcess :jobApprovalProcessList)
						{						
							String[] groupIds = (jobApprovalProcess.getJobApprovalGroups()!=null)? jobApprovalProcess.getJobApprovalGroups().split("\\|") : null;
							String grpIds = "";
							tmRecords.append("<tr>");
							tmRecords.append("<td>"+jobApprovalProcess.getJobApprovalProcess()+"</td>");
							tmRecords.append("<td>"+jobApprovalProcess.getApprovalRequired()+"</td>");
				            if(groupIds!=null && groupIds.length >0){
				            	tmRecords.append("<td>");
								for(String groupId : groupIds){
									if(!groupId.equals("")){
										if(groupMap.get(groupId)!=null){	
											tmRecords.append(groupMap.get(groupId).getGroupName()+"&nbsp<a href='javascript:void(0);' data-toggle='tooltip' title='Remove Member' onclick='removeGroupConfirmation("+jobApprovalProcess.getJobApprovalProcessId()+","+groupMap.get(groupId).getDistrictApprovalGroupsId()+")' ><img width='15' height='15' class='can' src='images/can-icon.png'></a>&nbsp;&nbsp;");
											grpIds +=groupMap.get(groupId).getDistrictApprovalGroupsId()+",";
											}
									}
								}
								tmRecords.append("</td>");
				            }
				            else
				            	tmRecords.append("<td></td>");
				            tmRecords.append("<td><a href='javascript:void(0);' data-toggle='tooltip' title='Edit Group' onclick=\"editGroup("+jobApprovalProcess.getJobApprovalProcessId()+",'"+(!grpIds.trim().equals("")?grpIds.substring(0, grpIds.length()-1):"")+"',"+jobApprovalProcess.getApprovalRequired()+");\"><i class='icon-edit'></i></a> | " );
				            //		"<a href='javascript:void(0);' data-toggle='tooltip' title='Remove Group' onclick='removeJobApprovalProcessConfirmation("+jobApprovalProcess.getJobApprovalProcessId()+")' ><i class='icon-trash'></i></a></td>");
				            if(jobApprovalProcess.getStatus().equals(1)){
								tmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' title='Deactvate' onclick=deactivateAndActivateJobApprovalProcessConfirmation("
									+ jobApprovalProcess
											.getJobApprovalProcessId()
									+ ",'I')>");
								tmRecords.append(lblDeactivate+"</a>&nbsp;&nbsp;");
								} else{
									tmRecords.append("<a href='javascript:void(0);' data-toggle='tooltip' title='Activate' onclick=deactivateAndActivateJobApprovalProcessConfirmation("
											+ jobApprovalProcess
													.getJobApprovalProcessId()
											+ ",'A') >");
									tmRecords.append(lblActivate+"</a>&nbsp;&nbsp;");
								}
				            tmRecords.append("</tr>");			
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
			{
				tmRecords.append("<tr><td colspan='5' align='center' class='net-widget-content'>No Record found</td></tr>" );
			}
			tmRecords.append("</tbody>");
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return tmRecords.toString();
	}
	public JSONObject getApprovalGroupsList(Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		JSONObject approvalGroupAndMemberList = new JSONObject();
		JSONObject  groupList = new JSONObject();
				
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		if(districtId!=null)
		{
			try
			{
				DistrictMaster districtMaster=null;
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null)
					if(userMaster.getDistrictId().getDistrictId().equals(districtId))
						districtMaster = userMaster.getDistrictId();
				
				if(districtMaster==null)
					districtMaster = districtMasterDAO.findById(districtId, false, false);
					
				List<DistrictApprovalGroups> districtApprovalGroupList = districtApprovalGroupsDAO.findAllDistrictApprovalGroupsByDistrict(districtMaster);
				//Adding the group name into groupList
				if(districtApprovalGroupList!=null && districtApprovalGroupList.size()>0)
				{
					for(DistrictApprovalGroups districtApprovalGroup :districtApprovalGroupList)
					{
						groupList.put(districtApprovalGroup.getDistrictApprovalGroupsId(), districtApprovalGroup.getGroupName());						
					}
				}
				approvalGroupAndMemberList.put("groups", groupList);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return approvalGroupAndMemberList;
	}
	public JSONObject getApprovalProcessList(Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		JSONObject approvalGroupAndMemberList = new JSONObject();
		JSONObject  approvalList = new JSONObject();
				
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		if(districtId!=null)
		{
			try
			{
				DistrictMaster districtMaster=null;
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null)
					if(userMaster.getDistrictId().getDistrictId().equals(districtId))
						districtMaster = userMaster.getDistrictId();
				
				if(districtMaster==null)
					districtMaster = districtMasterDAO.findById(districtId, false, false);
					
				List<JobApprovalProcess> JobApprovalProcessList = jobApprovalProcessDAO.findAllDistrictJobApproval(districtMaster);
				
				
				Set<String> membersExist = new TreeSet<String>();
				
				//Adding the group name into groupList
				if(JobApprovalProcessList!=null && JobApprovalProcessList.size()>0)
				{
					for(JobApprovalProcess jobApprovalProcess :JobApprovalProcessList)
					{
						approvalList.put(jobApprovalProcess.getJobApprovalProcessId(), jobApprovalProcess.getJobApprovalProcess());
					}
				}
				approvalGroupAndMemberList.put("approvalProcess", approvalList);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return approvalGroupAndMemberList;
	}
	public String addApprovalProcess(Integer districtId, String approvalName)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
						
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		if(districtId!=null && approvalName!=null && !approvalName.equals(""))
		{
			try
			{
				DistrictMaster districtMaster=null;
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null)
					if(userMaster.getDistrictId().getDistrictId().equals(districtId))
						districtMaster = userMaster.getDistrictId();
				
				if(districtMaster==null)
					districtMaster = districtMasterDAO.findById(districtId, false, false);				
				
					boolean isGroupExist = jobApprovalProcessDAO.isProcessExistByDistrict(districtId, approvalName);
					if(!isGroupExist)
					{
						String groupMembers=null;
						JobApprovalProcess jobApprovalProcess = new JobApprovalProcess();					
						
						jobApprovalProcess.setDistrictId(districtMaster);
						jobApprovalProcess.setJobApprovalProcess(approvalName);
						jobApprovalProcess.setCreatedDateTime(new Date());
						jobApprovalProcess.setStatus(1);						
						jobApprovalProcessDAO.makePersistent(jobApprovalProcess);
						System.out.println("JobApprovalProcessId()" +jobApprovalProcess.getJobApprovalProcessId());
						return jobApprovalProcess.getJobApprovalProcessId().toString();
					}
					else
						return "ProcessExist";
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return "";
	}
	/*public String addApprovalGroup(Integer districtId, String groupName, Integer noOfApproval, String keyContactIds, boolean isRestrict,Integer jobApprovalProcessId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
						
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		if(districtId!=null && groupName!=null && !groupName.equals(""))
		{
			try
			{
				DistrictMaster districtMaster=null;
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null)
					if(userMaster.getDistrictId().getDistrictId().equals(districtId))
						districtMaster = userMaster.getDistrictId();
				
				if(districtMaster==null)
					districtMaster = districtMasterDAO.findById(districtId, false, false);
				
				if(isRestrict)
				{
					List<DistrictWiseApprovalGroup> districtWiseApprovalGroupES = districtWiseApprovalGroupDAO.findGroupsByDistrictAndShortName(districtMaster,"ES");
					
					if(districtWiseApprovalGroupES!=null && districtWiseApprovalGroupES.size()>0)
						if(districtWiseApprovalGroupES.get(0).getGroupName()!=null)
							if(districtWiseApprovalGroupES.get(0).getGroupName().equalsIgnoreCase(groupName))
								return "EsExist";
					
					List<DistrictWiseApprovalGroup> districtWiseApprovalGroupSA = districtWiseApprovalGroupDAO.findGroupsByDistrictAndShortName(districtMaster,"SA");
					
					if(districtWiseApprovalGroupSA!=null && districtWiseApprovalGroupSA.size()>0){
						if(districtWiseApprovalGroupSA.get(0).getGroupName()!=null){
							if(districtWiseApprovalGroupSA.get(0).getGroupName().equalsIgnoreCase(groupName)){
								
								String groupMembers=null;
								if(keyContactIds!=null && !keyContactIds.equals(""))
									groupMembers = keyContactIds;
								
								List<DistrictApprovalGroups> districtApprovalGroupsList = districtApprovalGroupsDAO.findGroupsByDistrictAndName(districtMaster, groupName);
								
								if(districtApprovalGroupsList!=null && districtApprovalGroupsList.size()>0){
									DistrictApprovalGroups districtApprovalGroups = districtApprovalGroupsList.get(0);
									districtApprovalGroups.setGroupMembers(groupMembers);
									districtApprovalGroups.setStatus("A");
								
									districtApprovalGroupsDAO.makePersistent(districtApprovalGroups);
									
									return "";
								}
							}
						}
					}
					
					boolean isGroupExist = districtApprovalGroupsDAO.isGroupExistByDistrict(districtId, groupName);
					if(!isGroupExist)
					{
						String groupMembers=null;
						if(keyContactIds!=null && !keyContactIds.equals(""))
							groupMembers = keyContactIds;
											
						DistrictApprovalGroups districtApprovalGroups = new DistrictApprovalGroups();
						districtApprovalGroups.setApprovalGroupCreatedDateTime(new Date());
						districtApprovalGroups.setGroupName(groupName);
						districtApprovalGroups.setDistrictId(districtMaster);
						districtApprovalGroups.setGroupMembers(groupMembers);
						districtApprovalGroups.setJobCategoryId(null);
						districtApprovalGroups.setJobId(null);
						districtApprovalGroups.setStatus("A");
						districtApprovalGroups.setNoOfApprovals(noOfApproval);
						
						districtApprovalGroupsDAO.makePersistent(districtApprovalGroups);
						
						return "";
					}
					else
						return "GroupExist";
				}
				else
				{
					boolean isGroupExist = districtApprovalGroupsDAO.isGroupExistByDistrict(districtId, groupName);
					if(!isGroupExist)
					{
						String groupMembers=null;
						if(keyContactIds!=null && !keyContactIds.equals(""))
							groupMembers = keyContactIds;
											
						DistrictApprovalGroups districtApprovalGroups = new DistrictApprovalGroups();
						districtApprovalGroups.setApprovalGroupCreatedDateTime(new Date());
						districtApprovalGroups.setGroupName(groupName);
						districtApprovalGroups.setDistrictId(districtMaster);
						districtApprovalGroups.setGroupMembers(groupMembers);
						districtApprovalGroups.setJobCategoryId(null);
						districtApprovalGroups.setJobId(null);
						districtApprovalGroups.setStatus("A");
						districtApprovalGroups.setNoOfApprovals(noOfApproval);
						
						districtApprovalGroupsDAO.makePersistent(districtApprovalGroups);
						
						return "";
					}
					else
						return "GroupExist";
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return "";
	}*/
	public String addJobApprovalProcess(Integer districtId, String approvalProcessName, Integer noOfApproval, String groupIds)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
						
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		if(districtId!=null && approvalProcessName!=null && !approvalProcessName.equals(""))
		{
			try
			{
				DistrictMaster districtMaster=null;
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null)
					if(userMaster.getDistrictId().getDistrictId().equals(districtId))
						districtMaster = userMaster.getDistrictId();
				
				if(districtMaster==null)
					districtMaster = districtMasterDAO.findById(districtId, false, false);				
				
					boolean isGroupExist = districtApprovalGroupsDAO.isGroupExistByDistrict(districtId, approvalProcessName);
					if(!isGroupExist)
					{
						String processGroup=null;
						if(groupIds!=null && !groupIds.equals(""))
							processGroup = groupIds;
											
						JobApprovalProcess jobApprovalProcess = new JobApprovalProcess();
						
						jobApprovalProcess.setCreatedDateTime(new Date());
						jobApprovalProcess.setJobApprovalProcess(approvalProcessName);
						jobApprovalProcess.setDistrictId(districtMaster);
						jobApprovalProcess.setJobApprovalGroups(processGroup);
						jobApprovalProcess.setApprovalRequired(noOfApproval);
						jobApprovalProcess.setStatus(1);
						
						jobApprovalProcessDAO.makePersistent(jobApprovalProcess);
						
						return "";
					}
					else
						return "GroupExist";
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return "";
	}
	public JSONObject getApprovalProcessAndGroupsList(Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		JSONObject approvalGroupAndMemberList = new JSONObject();
		JSONObject  approvalList = new JSONObject();
		JSONObject  groupList = new JSONObject();
	
				
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		if(districtId!=null)
		{
			try
			{
				DistrictMaster districtMaster=null;
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null)
					if(userMaster.getDistrictId().getDistrictId().equals(districtId))
						districtMaster = userMaster.getDistrictId();
				
				if(districtMaster==null)
					districtMaster = districtMasterDAO.findById(districtId, false, false);
					
				List<JobApprovalProcess> jobApprovalProcessList = jobApprovalProcessDAO.findAllDistrictJobApproval(districtMaster);
				List<DistrictApprovalGroups> districtApprovalGroupList = districtApprovalGroupsDAO.findAllDistrictApprovalGroupsByDistrict(districtMaster);
				
				Set<String> groupsExist = new TreeSet<String>();
				
				//Adding the group name into groupList
				if(jobApprovalProcessList!=null && jobApprovalProcessList.size()>0)
				{
					for(JobApprovalProcess jobApprovalProcess :jobApprovalProcessList)
					{
						approvalList.put(jobApprovalProcess.getJobApprovalProcessId(), jobApprovalProcess.getJobApprovalProcess());
						if(jobApprovalProcess.getJobApprovalGroups()!=null)
							for(String group : jobApprovalProcess.getJobApprovalGroups().split("\\|"))
								groupsExist.add(group);
					}
				}
				
				//Adding the members name into membersList
				if(districtApprovalGroupList!=null && districtApprovalGroupList.size()>0)
				{
					for(DistrictApprovalGroups districtApprovalGroups :districtApprovalGroupList)
					{
						//if(!membersExist.contains(""+districtKeyContact.getKeyContactId()))
						groupList.put(districtApprovalGroups.getDistrictApprovalGroupsId(), districtApprovalGroups.getGroupName());
					}
				}
				
				approvalGroupAndMemberList.put("approval", approvalList);
				approvalGroupAndMemberList.put("groups", groupList);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return approvalGroupAndMemberList;
	}
	public String displayApprovalProcessAndGroup(Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb = new StringBuffer();
				
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		if(districtId!=null)
		{
			try
			{
				DistrictMaster districtMaster=null;
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null)
					if(userMaster.getDistrictId().getDistrictId().equals(districtId))
						districtMaster = userMaster.getDistrictId();
				
				if(districtMaster==null)
					districtMaster = districtMasterDAO.findById(districtId, false, false);
					
				List<JobApprovalProcess> jobApprovalProcessList = jobApprovalProcessDAO.findAllJobApprovalProcessByDistrict(districtMaster);
				if(jobApprovalProcessList!=null && jobApprovalProcessList.size()>0)
				{
					
					Set<Integer> groupsId = new TreeSet<Integer>();
					for(JobApprovalProcess jobApprovalProcess :jobApprovalProcessList)
					{
						String[] groupsName = (jobApprovalProcess.getJobApprovalGroups()!=null)? jobApprovalProcess.getJobApprovalGroups().split("\\|") : null;
						if(groupsName!=null && groupsName.length >0)
							for(String group : groupsName)
								if(!group.equals(""))
									groupsId.add(Integer.parseInt(group));
					}
					
					Map<String, DistrictApprovalGroups> groupsMap = new HashMap<String, DistrictApprovalGroups>();
					if(groupsId!=null && groupsId.size()>0)
					{
						List<DistrictApprovalGroups> groupsList = districtApprovalGroupsDAO.findByCriteria(Restrictions.in("districtApprovalGroupsId", groupsId));
						for(DistrictApprovalGroups group : groupsList)
							groupsMap.put(""+group.getDistrictApprovalGroupsId(), group);
					}
					
					groupsId=null;
					for(JobApprovalProcess jobApprovalProcess :jobApprovalProcessList)
					{
						String approvalProcessName = jobApprovalProcess.getJobApprovalProcess();
						String[] groupsIds = (jobApprovalProcess.getJobApprovalGroups()!=null)? jobApprovalProcess.getJobApprovalGroups().split("\\|") : null;
						
						sb.append("<div class='row mt10'>");
						sb.append("<label> "+approvalProcessName+"&nbsp<a href='javascript:void(0);' data-toggle='tooltip' title='"+Utility.getLocaleValuePropByKey("msgRemoveGroup", locale)+"' onclick='removeGroupConfirmation("+jobApprovalProcess.getJobApprovalProcessId()+")' ><img width='15' height='15' class='can' src='images/can-icon.png'></a> </label>");
						
						sb.append("<div style='margin-top: -6px;'>");
						
						int i=0;
						if(groupsIds!=null && groupsIds.length >0)
							for(String groupId : groupsIds)
								if(!groupId.equals(""))
									sb.append("<div class='col-sm-3 col-md-3'>"+groupsMap.get(groupId).getGroupName()+"&nbsp<a href='javascript:void(0);' data-toggle='tooltip' title='"+Utility.getLocaleValuePropByKey("msgRemoveMember", locale)+"' onclick='removeMemberConfirmation("+jobApprovalProcess.getJobApprovalProcessId()+","+groupsMap.get(groupId).getDistrictApprovalGroupsId()+")' ><img width='15' height='15' class='can' src='images/can-icon.png'></a></div>");
									
						sb.append("</div>");
						sb.append("</div>");
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return sb.toString();
	}
	
	public String addGroupIntoJobApprovalProcess(Integer approvalProcessId, String groupIds,Integer noOfApproval)
	{
		System.out.println("approvalProcessId "+approvalProcessId);
		System.out.println("groupIds "+groupIds);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		String str="";
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		if(approvalProcessId!=null && groupIds!=null && !groupIds.equals(""))
		{
			try
			{
				JobApprovalProcess jobApprovalProcess = jobApprovalProcessDAO.findById(approvalProcessId, false, false);
				
				if(jobApprovalProcess!=null)
				{
					String approvalGroups = (jobApprovalProcess.getJobApprovalGroups()==null || jobApprovalProcess.getJobApprovalGroups().equals(""))? "|" : jobApprovalProcess.getJobApprovalGroups();
					System.out.println("approvalGroups:- "+approvalGroups);
					
					/*if(!groupIds.contains("Remove"))
					{
						String members[] = groupIds.split("|");
						groupIds="";
						for(String mem : members)
						{
							System.out.println("Mem:- "+mem+", and conatins:- "+(approvalGroups.contains("|"+mem+"|") ));
							if(!mem.equals("") && !approvalGroups.contains("|"+mem+"|"))
							{
								groupIds = groupIds +mem+"|";
								System.out.println("keyContactIds:- "+groupIds);
							}
						}
						if(!groupIds.equals(""))
							approvalGroups = approvalGroups+groupIds;
					}
					else
					{
						System.out.println("inside Remove ::::::::::");
						approvalGroups=groupIds.replace("Remove", "");
						if(!approvalGroups.startsWith("|"))
							approvalGroups = "|"+approvalGroups;
						if(!approvalGroups.endsWith("|"))
							approvalGroups = approvalGroups+"|";
					}*/
					
					System.out.println("keyContactIds:- "+groupIds);
					//System.out.println("groupMembers:- "+approvalGroups);
					jobApprovalProcess.setJobApprovalGroups(groupIds);
					jobApprovalProcess.setApprovalRequired(noOfApproval);
					jobApprovalProcessDAO.makePersistent(jobApprovalProcess);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return str;
	}
	@Transactional(readOnly=false)
	public boolean removeJobProcessByProcessId(Integer jobProcessId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
						
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		
		if(jobProcessId!=null)
		{
			JobApprovalProcess jobApprovalProcess = jobApprovalProcessDAO.findById(jobProcessId, false, false);
			/*if(jobApprovalProcess.getDistrictId()!=null){
				List<DistrictWiseApprovalGroup> districtWiseApprovalGroupSA = districtWiseApprovalGroupDAO.findGroupsByDistrictAndShortName(districtApprovalGroups.getDistrictId(),"SA");
				System.out.println("districtWiseApprovalGroupSA.size() : "+districtWiseApprovalGroupSA.size());
				if(districtWiseApprovalGroupSA!=null && districtWiseApprovalGroupSA.size()>0){
					if(districtWiseApprovalGroupSA.get(0).getGroupName()!=null){
						if(districtWiseApprovalGroupSA.get(0).getGroupName().equalsIgnoreCase(districtApprovalGroups.getGroupName())){
							districtApprovalGroups.setGroupMembers("");
							districtApprovalGroups.setStatus("I");
							districtApprovalGroupsDAO.makePersistent(districtApprovalGroups);
							return true;
						}
					}	
				}
				List<DistrictWiseApprovalGroup> districtWiseApprovalGroupES = districtWiseApprovalGroupDAO.findGroupsByDistrictAndShortName(districtApprovalGroups.getDistrictId(),"ES");
				System.out.println("districtWiseApprovalGroupES.size() : "+districtWiseApprovalGroupES.size());
				if(districtWiseApprovalGroupES!=null && districtWiseApprovalGroupES.size()>0){
					if(districtWiseApprovalGroupES.get(0).getGroupName()!=null){
						if(districtWiseApprovalGroupES.get(0).getGroupName().equalsIgnoreCase(districtApprovalGroups.getGroupName())){
							districtApprovalGroups.setGroupMembers("");
							districtApprovalGroups.setStatus("I");
							districtApprovalGroupsDAO.makePersistent(districtApprovalGroups);
							return true;
						}
					}	
				}
			}*/
			
				jobApprovalProcessDAO.makeTransient(jobApprovalProcess);
					return true;
		}
		return false;
	}
	public String removeGroupFromApprovalProcess(Integer approvalProcessId, Integer groupId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
						
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		if(approvalProcessId!=null && groupId!=null)
		{
			try
			{
				JobApprovalProcess jobApprovalProcess = jobApprovalProcessDAO.findById(approvalProcessId, false, false);
				
				if(jobApprovalProcess!=null)
				{
					if(jobApprovalProcess.getJobApprovalGroups()!=null || jobApprovalProcess.getJobApprovalGroups().contains("|"+groupId+"|") )
					{
						String groupMembers = jobApprovalProcess.getJobApprovalGroups();
						groupMembers = groupMembers.replace( "|"+groupId+"|", "|");
						
						jobApprovalProcess.setJobApprovalGroups(groupMembers);
						
						SessionFactory sessionFactory = jobApprovalProcessDAO.getSessionFactory();
						StatelessSession statelesSsession 	= sessionFactory.openStatelessSession();
						Transaction txOpen =statelesSsession.beginTransaction();
						statelesSsession.update(jobApprovalProcess);
						txOpen.commit();
				       	statelesSsession.close();
				    }
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return "";
	}
	public JSONObject groupNameByapprovalId(Integer approvalId,Integer districtId)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
	//	JSONObject approvalGroupAndMemberList = new JSONObject();
		JSONObject  groupList = new JSONObject();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		Map<String ,DistrictApprovalGroups> groupMap=new LinkedHashMap<String, DistrictApprovalGroups>();
			try
			{				
				DistrictMaster districtMaster=null;
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null)
					if(userMaster.getDistrictId().getDistrictId().equals(districtId))
						districtMaster = userMaster.getDistrictId();
				
				if(districtMaster==null)
					districtMaster = districtMasterDAO.findById(districtId, false, false);
				
				JobApprovalProcess jobApprovalProcess = jobApprovalProcessDAO.findById(approvalId, false, false);
				if(jobApprovalProcess!=null && jobApprovalProcess.getJobApprovalGroups()!=null)
				{
					List<DistrictApprovalGroups> districtApprovalGroupList = districtApprovalGroupsDAO.findAllDistrictApprovalGroupsByDistrict(districtMaster);
					if(districtApprovalGroupList!=null && districtApprovalGroupList.size()>0)
					{
						//for(String group:groupsExist)
						for(DistrictApprovalGroups districtApprovalGroups :districtApprovalGroupList)
						{
							groupMap.put(""+districtApprovalGroups.getDistrictApprovalGroupsId(), districtApprovalGroups);
							//if(groupsExist.contains(""+districtApprovalGroups.getDistrictApprovalGroupsId()))
								//groupList.put(districtApprovalGroups.getDistrictApprovalGroupsId(), districtApprovalGroups.getGroupName());
						}
					}
				//	Set<String> groupsExist = new TreeSet<String>();
							
					if(jobApprovalProcess.getJobApprovalGroups()!=null)
						for(String group : jobApprovalProcess.getJobApprovalGroups().split("\\|"))
						{
							if(group!=null && !group.equals("")){
								DistrictApprovalGroups districtApprovalGroups=	groupMap.get(group);
								if(districtApprovalGroups!=null)
								groupList.put("key:"+districtApprovalGroups.getDistrictApprovalGroupsId(), districtApprovalGroups.getGroupName());
								//groupList.put(districtApprovalGroups.getDistrictApprovalGroupsId(), districtApprovalGroups.getGroupName());
						//	System.out.println("sssssssc " +districtApprovalGroups.getDistrictApprovalGroupsId());
							}
							//groupsExist.add(group);
						}
							
					
					
					//System.out.println("groupList "+groupsExist.size());
					//approvalGroupAndMemberList.put("groups", groupList);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			//System.out.println("approvalGroupAndMemberList"+approvalGroupAndMemberList.size());
		//return approvalGroupAndMemberList;
		return groupList;
	}
	@Transactional(readOnly=false)
	public boolean activateAndDeactivateApprovalProcess(Integer approvalProcessId, String status)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
						
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		if(approvalProcessId!=null && status!=null)
		{
			try{
				
				JobApprovalProcess jobApprovalProcess = jobApprovalProcessDAO.findById(approvalProcessId, false, false);
				if(jobApprovalProcess!=null){
					if(status.equals("I")){
						jobApprovalProcess.setStatus(0);
					}
					if(status.equals("A")){
						jobApprovalProcess.setStatus(1);
					}
					jobApprovalProcessDAO.updatePersistent(jobApprovalProcess);
					return true;
				}
				
			}catch (Exception e) {
					e.printStackTrace();
					}
		}
		return false;
	}
}
