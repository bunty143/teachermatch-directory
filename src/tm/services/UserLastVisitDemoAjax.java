package tm.services;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.UserLastVisitToDemoSchedule;
import tm.bean.user.UserMaster;
import tm.dao.master.UserLastVisitToDemoScheduleDAO;
import tm.utility.IPAddressUtility;

public class UserLastVisitDemoAjax {
	@Autowired
	private UserLastVisitToDemoScheduleDAO userLastVisitToDemoScheduleDAO;
	public void setUserLastVisitToDemoScheduleDAO(
			UserLastVisitToDemoScheduleDAO userLastVisitToDemoScheduleDAO) {
		this.userLastVisitToDemoScheduleDAO = userLastVisitToDemoScheduleDAO;
	}
	public void updateUserLastVisitDemo(){
		try{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			
			UserMaster userMaster		=	(UserMaster) session.getAttribute("userMaster");
			DistrictMaster districtMaster=userMaster.getDistrictId();
			SchoolMaster schoolMaster=userMaster.getSchoolId();
			
			System.out.println("inside user last visit demo");
			
			UserLastVisitToDemoSchedule userLastVisitToDemoSchedule=userLastVisitToDemoScheduleDAO.getuserVisitID(districtMaster, schoolMaster, userMaster);
			if(userLastVisitToDemoSchedule==null){
				userLastVisitToDemoSchedule=new UserLastVisitToDemoSchedule();
				userLastVisitToDemoSchedule.setDistrictMaster(districtMaster);
				userLastVisitToDemoSchedule.setSchoolMaster(schoolMaster);
				userLastVisitToDemoSchedule.setUserMaster(userMaster);
				userLastVisitToDemoSchedule.setIpAddress(IPAddressUtility.getIpAddress(request));
				userLastVisitToDemoSchedule.setVisitedDateTime(new Date());
			}else{
				userLastVisitToDemoSchedule.setVisitedDateTime(new Date());
				userLastVisitToDemoSchedule.setIpAddress(IPAddressUtility.getIpAddress(request));
			}
			userLastVisitToDemoScheduleDAO.makePersistent(userLastVisitToDemoSchedule);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
