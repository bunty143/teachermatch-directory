package tm.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import tm.api.PaginationAndSortingAPI;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherProfileVisitHistory;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.user.PersonalEmailTrack;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherProfileVisitHistoryDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.user.PersonalEmailTrackDAO;
import tm.utility.Utility;
public class PersonalEmailTrackAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;

	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;
	
	@Autowired
	private PersonalEmailTrackDAO personalEmailTrackDAO;

	
	//public PersonalEmailTrack getPErsonalEmailId(String userId)
	@ResponseBody
	public String getPErsonalEmailId(String userId)
	{	
		
		PersonalEmailTrack personalEmailTrack=null;;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userMaster = null;
		int distId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			
			if(userMaster.getDistrictId()!=null)
			{
				distId=userMaster.getDistrictId().getDistrictId();
				
			}
		}
		 List<PersonalEmailTrack> lstEmailTracks=null;
		 Map<String, PersonalEmailTrack> emailMap=new HashMap<String, PersonalEmailTrack>();
		 try
		 {
		 lstEmailTracks=personalEmailTrackDAO.findAll();
		 System.out.println("lstEmailTracks:::"+lstEmailTracks.size());
		 }catch (Exception e) {
			e.printStackTrace();
		}
		 if(lstEmailTracks!=null && lstEmailTracks.size()>0)
		 {
			 for(PersonalEmailTrack pet:lstEmailTracks)
			 {
				 emailMap.put(pet.getDistrictId()+"_"+pet.getUserId() , pet);
			 } 		 
		 }
		
		try
		{
			Integer uid=Integer.parseInt(userId);
			
			personalEmailTrack=null;
			if(emailMap.containsKey(distId+"_"+uid))
			{
				personalEmailTrack=emailMap.get(distId+"_"+userId);
			}
			
		}
			catch(Exception e)
			{
				System.out.println(e);
			}
			Gson gson=new Gson();
		return	gson.toJson(personalEmailTrack);
	}
	public String saveEmailRecords(String userId,String email,String password,String mailHost,String mailPort)
	{
		System.out.println("inside saveeeeeee ajax");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userMaster = null;
		int distId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			
			if(userMaster.getDistrictId()!=null)
			{
				distId=userMaster.getDistrictId().getDistrictId();
				
			}
		}
		SessionFactory sessionFactory = personalEmailTrackDAO.getSessionFactory();
		 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
		 Transaction txOpen =statelesSsession.beginTransaction();
		 
		 List<PersonalEmailTrack> lstEmailTracks=null;
		 Map<String, PersonalEmailTrack> emailMap=new HashMap<String, PersonalEmailTrack>();
		 try
		 {
		 lstEmailTracks=personalEmailTrackDAO.findAll();  
		 System.out.println("lstEmailTracks:::"+lstEmailTracks.size());
		 }catch (Exception e) {
			e.printStackTrace();
		}
		 if(lstEmailTracks!=null && lstEmailTracks.size()>0)
		 {
			 for(PersonalEmailTrack pet:lstEmailTracks)
			 {
				 emailMap.put(pet.getDistrictId()+"_"+pet.getUserId() , pet);
			 } 		 
		 }
		
		try
		{
			Integer uid=Integer.parseInt(userId);
			PersonalEmailTrack personalEmailTrack=null;
			if(emailMap.containsKey(distId+"_"+uid))
			{
				personalEmailTrack=emailMap.get(distId+"_"+userId);
			}
			if(personalEmailTrack==null)
			{
				personalEmailTrack=new PersonalEmailTrack();
				System.out.println("inside insert");
				personalEmailTrack.setUserId(uid);
			personalEmailTrack.setDistrictId(distId);
			personalEmailTrack.setPersonalEmailAddress(email);
			personalEmailTrack.setPersonalEmailPassword(password);
			personalEmailTrack.setMailHost(mailHost);
			personalEmailTrack.setMailPort(mailPort);
			personalEmailTrack.setPersonalStatus("A");
			statelesSsession.insert(personalEmailTrack);
			}
			else
			{
				System.out.println("inside updateeeeeeee");
				personalEmailTrack.setUserId(uid);
				personalEmailTrack.setPersonalID(personalEmailTrack.getPersonalID());
				personalEmailTrack.setDistrictId(distId);
				personalEmailTrack.setPersonalEmailAddress(email);
				personalEmailTrack.setPersonalEmailPassword(password);
				personalEmailTrack.setMailHost(mailHost);
				personalEmailTrack.setMailPort(mailPort);
				personalEmailTrack.setPersonalStatus("A");
				statelesSsession.update(personalEmailTrack);
				
			}
			
			
			
		}
		catch(Exception e){e.printStackTrace();}
		txOpen.commit();
		statelesSsession.close();
		
		return "";
	}
	
	
}


