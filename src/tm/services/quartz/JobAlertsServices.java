package tm.services.quartz;


import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;

import com.mysql.jdbc.Util;

import tm.bean.JobAlerts;
import tm.bean.JobOrder;
import tm.bean.master.SchoolMaster;
import tm.dao.JobAlertsDAO;
import tm.dao.JobOrderDAO;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.utility.Utility;


import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class JobAlertsServices implements Job {
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService){
		this.emailerService = emailerService;
	}
	
	@Autowired
	private JobAlertsDAO jobAlertsDAO;
	public void setJobAlertsDAO(JobAlertsDAO jobAlertsDAO) {
		this.jobAlertsDAO = jobAlertsDAO;
	}
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	
	public void execute(JobExecutionContext arg0) throws JobExecutionException{
		System.out.println("############################");
		//new NewJobAlertServices().doJobAlert();
		
		//Utility.sendRequestToURL(Utility.getValueOfPropByKey("basePath")+"/jobalerts.do");
		Utility.sendRequestToURL(Utility.getValueOfPropByKey("contextBasePath")+"/jobalerts.do");
	}
	
	
	
	public String doJobAlert()
	{
		try {
			
		
			String emailAddress = "";
			
			List<JobAlerts> userListJobAlerts = new ArrayList<JobAlerts>();
			JobAlerts userJobAlerts = null;
			
			
			System.out.println(jobAlertsDAO);
			List<JobAlerts> lstJobAlerts = jobAlertsDAO.findActiveJobAlert();
			//System.out.println("lstJobAlerts.size()>"+lstJobAlerts.size());
			List<JobOrder> lstJobOrders = jobOrderDAO.getTodayAciveJob();
			//System.out.println("lstJobOrders.size()>"+lstJobOrders.size());
			
			
			if(lstJobAlerts.size()!=0){
				emailAddress = lstJobAlerts.get(0).getSenderEmail();
				userJobAlerts = lstJobAlerts.get(1);
			}
			for(JobAlerts jobAlerts: lstJobAlerts){
				if(emailAddress.equals(jobAlerts.getSenderEmail())){
					userListJobAlerts.add(jobAlerts);
				}
				else{
					sendJobAlertMail(lstJobOrders,userListJobAlerts);
					userListJobAlerts = new ArrayList<JobAlerts>();
					userListJobAlerts.add(jobAlerts);
					emailAddress = jobAlerts.getSenderEmail();
					userJobAlerts = jobAlerts;
				}
			}
			if(lstJobAlerts.size()>0){
				sendJobAlertMail(lstJobOrders,userListJobAlerts);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public void sendJobAlertMail(List<JobOrder> lstJobOrders, List<JobAlerts> lstJobAlerts ){
		System.out.println("\nMail Send to:");
		System.out.println("lstJobAlerts.size()"+lstJobAlerts.size());
		System.out.println("lstJobOrders.size()"+lstJobOrders.size());
		System.out.println(""+lstJobAlerts.get(0).getSenderEmail()+"");
		
		
		List<JobOrder> userListJobOrders = new ArrayList<JobOrder>();
		
		topLoop:
		for(JobAlerts jobAlerts : lstJobAlerts){
			/*System.out.println("\njobAlerts.getDistrictMaster()"+jobAlerts.getDistrictMaster());
			System.out.println("jobAlerts.getSchoolMaster()"+jobAlerts.getSchoolMaster());
			System.out.println("jobAlerts.getZipCode()"+jobAlerts.getZipCode());
			System.out.println("jobAlerts.getJobCategoryMaster()"+jobAlerts.getJobCategoryMaster());*/
			
			for(JobOrder jobOrder:lstJobOrders){
				//System.out.println(">>>"+((jobAlerts.getDistrictMaster()==null) && (jobAlerts.getSchoolMaster()==null) && (jobAlerts.getJobCategoryMaster()==null) && (jobAlerts.getZipCode() ==null || jobAlerts.getZipCode().trim().equals(""))));
				if((jobAlerts.getDistrictMaster()==null) && (jobAlerts.getSchoolMaster()==null) && (jobAlerts.getJobCategoryMaster()==null) && (jobAlerts.getZipCode() ==null || jobAlerts.getZipCode().trim().equals(""))){
					System.out.println("Block 1 All");
					userListJobOrders.addAll(lstJobOrders);
					break topLoop;
				}
				if(jobAlerts.getJobCategoryMaster()==null || jobOrder.getJobCategoryMaster().equals(jobAlerts.getJobCategoryMaster())){
					if((jobAlerts.getSchoolMaster()!=null)){
						System.out.println("Block 2 School");
						if(jobOrder.getSchool()!=null)
						for(SchoolMaster schoolMaster: jobOrder.getSchool()){
							if(jobAlerts.getSchoolMaster().equals(schoolMaster))
								userListJobOrders.add(jobOrder);
						}
					}
					else if((jobAlerts.getZipCode()!=null && (!jobAlerts.getZipCode().trim().equals("")))){
						System.out.println("Block 3 Zipcode");
						
						if(jobAlerts.getDistrictMaster()==null || jobAlerts.getDistrictMaster().equals(jobOrder.getDistrictMaster())){
							System.out.println("District equals");							
							if( jobOrder.getDistrictMaster().getZipCode().equals(""+jobAlerts.getZipCode())){
								userListJobOrders.add(jobOrder);
							}
								
							System.out.println(jobOrder.getSchool().size());
							if(jobOrder.getSchool()!=null)
							for(SchoolMaster schoolMaster: jobOrder.getSchool()){
								System.out.println(">>>"+schoolMaster.getZip()+"  >> "+jobAlerts.getZipCode());
								if(jobAlerts.getZipCode().trim().equals(""+schoolMaster.getZip())){
									System.out.println("OK");
									userListJobOrders.add(jobOrder);
								}
									
							}
						}
					}
					else if((jobAlerts.getDistrictMaster()!=null)){
						System.out.println("Block 4 District");
						if(jobAlerts.getDistrictMaster().equals(jobOrder.getDistrictMaster()))
							userListJobOrders.add(jobOrder);										
					}
				}				
			}
		}
		
		
		System.out.println("userListJobOrders.size()"+userListJobOrders.size());
		
		Set<JobOrder> setJobOrders= new LinkedHashSet<JobOrder>(userListJobOrders);
		userListJobOrders = new ArrayList<JobOrder>(new LinkedHashSet<JobOrder>(setJobOrders));
		
		System.out.println("after removing duplicates userListJobOrders.size()"+userListJobOrders.size());
		
		JobAlerts jobAlert = lstJobAlerts.get(0);
		Collections.sort(userListJobOrders,JobOrder.jobOrderComparator);
		//emailerService.sendMailAsHTMLText(jobAlert.getSenderEmail(), "Welcome to TeacherMatch, please confirm your account",MailText.getJobAlertText(jobAlert,userListJobOrders));
		
		
		
	}
}