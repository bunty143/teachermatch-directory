package tm.services.quartz;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.PanelAttendees;
import tm.bean.master.PanelSchedule;
import tm.bean.master.SecondaryStatus;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobWisePanelStatusDAO;
import tm.dao.master.PanelAttendeesDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.services.district.PrintOnConsole;
import tm.utility.Utility;

public class NoResponseCandidate implements Job{

	String locale = Utility.getValueOfPropByKey("locale");
	
	public void execute(JobExecutionContext arg0) throws JobExecutionException{
		
		PrintOnConsole.debugPrintln("NoResponseCandidate", "Calling NoResponseCandidate execute method");
		
		ApplicationContext springContext=WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		
		JobForTeacherDAO jobForTeacherDAO = null;
		DistrictMasterDAO districtMasterDAO = null;
		JobOrderDAO jobOrderDAO = null;
		SecondaryStatusDAO secondaryStatusDAO = null;
		JobWisePanelStatusDAO jobWisePanelStatusDAO = null;
		PanelScheduleDAO panelScheduleDAO = null;
		PanelAttendeesDAO panelAttendeesDAO = null;
		DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO=null;
		JobRequisitionNumbersDAO jobRequisitionNumbersDAO=null;
		
		EmailerService emailerService = null;
		try {
			jobForTeacherDAO = (JobForTeacherDAO) springContext.getBean("jobForTeacherDAO");
			districtMasterDAO = (DistrictMasterDAO) springContext.getBean("districtMasterDAO");
			jobOrderDAO = (JobOrderDAO) springContext.getBean("jobOrderDAO");
			secondaryStatusDAO = (SecondaryStatusDAO) springContext.getBean("secondaryStatusDAO");
			jobWisePanelStatusDAO = (JobWisePanelStatusDAO) springContext.getBean("jobWisePanelStatusDAO");
			panelScheduleDAO = (PanelScheduleDAO) springContext.getBean("panelScheduleDAO");
			panelAttendeesDAO = (PanelAttendeesDAO) springContext.getBean("panelAttendeesDAO");
			districtRequisitionNumbersDAO = (DistrictRequisitionNumbersDAO) springContext.getBean("districtRequisitionNumbersDAO");
			jobRequisitionNumbersDAO = (JobRequisitionNumbersDAO) springContext.getBean("jobRequisitionNumbersDAO");
			emailerService = (EmailerService) springContext.getBean("myMailSender");
			
			DistrictMaster districtMaster=districtMasterDAO.findById(1200390, false,false);
			if(districtMaster!=null)
			{
				List<JobOrder> jobOrders=jobOrderDAO.findJobByDistrict(districtMaster);
				
				PrintOnConsole.debugPrintln("jobOrders.size "+jobOrders.size());
				
				if(jobOrders!=null && jobOrders.size() >0)
				{
					List<JobForTeacher> jobForTeachers = new ArrayList<JobForTeacher>();
					jobForTeachers=jobForTeacherDAO.getOfferReadyDate(jobOrders);
					
					if(jobForTeachers!=null && jobForTeachers.size() > 0)
					{
						PrintOnConsole.debugPrintln("jobForTeachers Size "+jobForTeachers.size());
						
						List<JobWisePanelStatus> jobWiseList=new ArrayList<JobWisePanelStatus>();
						SecondaryStatus  secondaryStatusObj =secondaryStatusDAO.findSSByDID(districtMaster,"Offer Ready");
						if(secondaryStatusObj!=null)
							jobWiseList=jobWisePanelStatusDAO.getPanelForJobs(jobOrders,secondaryStatusObj);
						
						List<PanelSchedule> panelSList= new ArrayList<PanelSchedule>();
						if(jobWiseList!=null && jobWiseList.size() > 0)
						{
							
							Map<String, Integer> mapPanelSchedule=new HashMap<String, Integer>();
							Map<Integer, List<UserMaster>> mapPanelAttendee=new HashMap<Integer, List<UserMaster>>();
							
							panelSList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusList(jobWiseList);
							if(panelSList!=null && panelSList.size() > 0)
							{
								for(PanelSchedule schedule:panelSList)
								{
									if(schedule!=null)
									{
										String sJID_TID=schedule.getJobOrder().getJobId()+"_"+schedule.getTeacherDetail().getTeacherId();
										if(mapPanelSchedule.get(sJID_TID)==null)
										{
											mapPanelSchedule.put(sJID_TID, schedule.getPanelId());
										}
									}
								}
								
								List<PanelAttendees> panelAttendees=new ArrayList<PanelAttendees>();
								panelAttendees=panelAttendeesDAO.getPanelAttendeeList(panelSList);
								if(panelAttendees!=null && panelAttendees.size() > 0)
								{
									for(PanelAttendees attendees:panelAttendees)
									{
										if(attendees!=null)
										{
											List<UserMaster> lstTemp=new ArrayList<UserMaster>();
											if(mapPanelAttendee.get(attendees.getPanelSchedule().getPanelId())==null)
											{
												lstTemp=new ArrayList<UserMaster>();
												lstTemp.add(attendees.getPanelInviteeId());
												mapPanelAttendee.put(attendees.getPanelSchedule().getPanelId(), lstTemp);
											}
											else
											{
												lstTemp=new ArrayList<UserMaster>();
												lstTemp=mapPanelAttendee.get(attendees.getPanelSchedule().getPanelId());
												lstTemp.add(attendees.getPanelInviteeId());
												mapPanelAttendee.put(attendees.getPanelSchedule().getPanelId(), lstTemp);
											}
										}
									}
									
									
									///////////////
									Map<String, String> mapSchoolName=new HashMap<String, String>();
									List<String> lstReqNo=new ArrayList<String>();
									for (JobForTeacher jobForTeacher : jobForTeachers) 
									{
										if(jobForTeacher.getRequisitionNumber()!=null && !jobForTeacher.getRequisitionNumber().equals(""))
										lstReqNo.add(jobForTeacher.getRequisitionNumber());
									}
									List<DistrictRequisitionNumbers> lstDstReqNo=districtRequisitionNumbersDAO.getDistrictRequisitionNumbersBYReqNo(lstReqNo,districtMaster);
									if(lstDstReqNo!=null && lstDstReqNo.size() >0)
									{
										List<JobRequisitionNumbers> lstRequisitionNumbers =jobRequisitionNumbersDAO.findSchoolByJobsAndDRN(jobOrders, lstDstReqNo);
										if(lstRequisitionNumbers!=null && lstRequisitionNumbers.size() > 0)
										{
											for(JobRequisitionNumbers jobRequisitionNumbers:lstRequisitionNumbers)
											{
												if(jobRequisitionNumbers!=null)
												{
													if(mapSchoolName.get(jobRequisitionNumbers.getDistrictRequisitionNumbers())==null)
													{
														mapSchoolName.put(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber(), jobRequisitionNumbers.getSchoolMaster()==null?"":jobRequisitionNumbers.getSchoolMaster().getSchoolName());
													}
												}
											}
										}
									}
									
									int iTemp=0;
									for (JobForTeacher jobForTeacher : jobForTeachers) 
									{
										PrintOnConsole.debugPrintln(++iTemp +". Job Id "+jobForTeacher.getJobId().getJobId() +" Teacher Id "+jobForTeacher.getTeacherId().getTeacherId()+" Job For TID "+jobForTeacher.getJobForTeacherId());
										try{
											jobForTeacher.setNoResponseEmail(true);
											jobForTeacherDAO.updatePersistent(jobForTeacher);
										}catch(Exception e){
											
										}
										//location
										
										String sReqNo=jobForTeacher.getRequisitionNumber();
										
											String sJID_TID=jobForTeacher.getJobId().getJobId()+"_"+jobForTeacher.getTeacherId().getTeacherId();
											if(mapPanelSchedule.get(sJID_TID)!=null)
											{
												Integer iPanelId=mapPanelSchedule.get(sJID_TID);
												if(iPanelId!=null)
												{
													if(mapPanelAttendee.get(iPanelId)!=null)
													{
														List<UserMaster> lstEmailsUserMaster=new ArrayList<UserMaster>();
														lstEmailsUserMaster=mapPanelAttendee.get(iPanelId);
														if(lstEmailsUserMaster!=null && lstEmailsUserMaster.size() > 0)
														{
															for(UserMaster userMaster:lstEmailsUserMaster)
															{
																if(userMaster!=null)
																{
																	String sEmail=userMaster.getEmailAddress().trim();
																	DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
																	String sSchoolName=null;
																	if(mapSchoolName.get(sReqNo)!=null)
																		sSchoolName=mapSchoolName.get(sReqNo);
																	
																	String mailContent = MailText.getFifthNoResponseAcceptOrDecline(jobForTeacher.getTeacherId(), jobForTeacher.getJobId(), sSchoolName, jobForTeacher.getOfferMadeDate(),userMaster.getUserId());
																	dsmt.setEmailerService(emailerService);
																	dsmt.setMailfrom("noreply@netsutra.com");
																	dsmt.setMailto(sEmail);
																	dsmt.setMailsubject(" "+Utility.getLocaleValuePropByKey("msgNoReceivedCandidate", locale)+"");
																	dsmt.setMailcontent(mailContent);
																	
																	PrintOnConsole.debugPrintln("NoResponseCandidate","Try to send email for sJID_TID "+sJID_TID +" Email "+sEmail+" ,ET "+userMaster.getEntityType() +" UID "+userMaster.getUserId());
																	PrintOnConsole.debugPrintln("NoResponseCandidate","Email Subject :: No Response Received From Candidate");
																	PrintOnConsole.debugPrintln("NoResponseCandidate","Email Content :: "+mailContent);
																	
																	dsmt.start();
																}
																else
																{
																	PrintOnConsole.debugPrintln("UserMaster is null for Job Id "+jobForTeacher.getJobId().getJobId() +" Teacher Id "+jobForTeacher.getTeacherId().getTeacherId()+" Job For TID "+jobForTeacher.getJobForTeacherId());
																}
															}
														}
														else
														{
															PrintOnConsole.debugPrintln("No UserMaster records for Job Id "+jobForTeacher.getJobId().getJobId() +" Teacher Id "+jobForTeacher.getTeacherId().getTeacherId()+" Job For TID "+jobForTeacher.getJobForTeacherId());
														}
													}
													else
													{
														PrintOnConsole.debugPrintln("No PanelAttendee for Job Id "+jobForTeacher.getJobId().getJobId() +" Teacher Id "+jobForTeacher.getTeacherId().getTeacherId()+" Job For TID "+jobForTeacher.getJobForTeacherId());
													}
												}
												else
												{
													PrintOnConsole.debugPrintln("No PanelAttendee for Job Id "+jobForTeacher.getJobId().getJobId() +" Teacher Id "+jobForTeacher.getTeacherId().getTeacherId()+" Job For TID "+jobForTeacher.getJobForTeacherId());
												}
											}
											else
											{
												PrintOnConsole.debugPrintln("No PanelSchedule for Job Id "+jobForTeacher.getJobId().getJobId() +" Teacher Id "+jobForTeacher.getTeacherId().getTeacherId()+" Job For TID "+jobForTeacher.getJobForTeacherId());
											}
									}
									
									///////////////
								}
								else
								{
									PrintOnConsole.debugPrintln("No panelAttendees at top level(Mass)");
								}
							}
							else
							{
								PrintOnConsole.debugPrintln("No PanelSchedule at top level(Mass)");
							}
						}
						else
						{
							PrintOnConsole.debugPrintln("No JobWisePanelStatus at top level(Mass)");
						}
					}
					else
					{
						PrintOnConsole.debugPrintln("No Job For Teacher Found at top level(Mass)");
					}
					
					}
					else
					{
						PrintOnConsole.debugPrintln("No Job Oreder Found");
					}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		PrintOnConsole.debugPrintln("End NoResponseCandidate");
	}
	
}

