package tm.services.quartz;
import java.util.Date;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import tm.services.es.ElasticSearchService;

public class ElasticSearchIndexUpdateService implements Job
{
	public void execute(JobExecutionContext arg0) throws JobExecutionException
	{
		System.out.println("************************************************************************");
		System.out.println("Calling ElasticSearchIndexUpdateService execute "+new Date());
		System.out.println("************************************************************************");
		ElasticSearchService es=new ElasticSearchService();
		//MatchData matchData=new MatchData();
		Map<Integer, String> elasticMap=null;
		try {
				elasticMap = es.searchByDocument("");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(elasticMap.size());
		Map<Integer, String> databaseMap=null;
		try {
				databaseMap = es.getDatabaseData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(databaseMap.size());
		//System.out.println(matchData.indexMap.size());
		try {
				es.compareData(databaseMap, elasticMap);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("************************************************************************");
		System.out.println("End ... Calling ElasticSearchIndexUpdateService execute "+new Date());
		System.out.println("************************************************************************");
	}
}

