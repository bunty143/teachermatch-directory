package tm.services.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import tm.services.report.CGReportService;
import tm.utility.Utility;

public class ResourceUpdateService implements Job{
	@Autowired
	CGReportService cGReportService = null;
	public void setcGReportService(CGReportService cGReportService) {
		this.cGReportService = cGReportService;
	}
	
	public void execute(JobExecutionContext arg0) throws JobExecutionException{
		System.out.println("ResourceConfigServlet Update");
		Utility.sendRequestToURL(Utility.getValueOfPropByKey("basePath")+"/ResourceConfigServlet");
		System.out.println("ResourceConfigServlet Update");
	}
}
