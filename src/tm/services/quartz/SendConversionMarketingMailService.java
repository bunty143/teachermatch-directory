package tm.services.quartz;


import java.util.Date;

import javax.servlet.ServletContext;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.services.AdminDashboardAjax;


public class SendConversionMarketingMailService implements Job{

	
	public void execute(JobExecutionContext arg0) throws JobExecutionException{
		
		System.out.println("============================================");
		System.out.println("SendConversionMarketingMailService Start: "+ new Date());
		
		ApplicationContext springContext = 
		    WebApplicationContextUtils.getWebApplicationContext(
		        ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		
		AdminDashboardAjax adminDashboardAjax = null;
		
		try {
		    
			ServletContext ctx = ContextLoaderListener.getCurrentWebApplicationContext().getServletContext();
			System.out.println("ctx: "+ctx);
			
			adminDashboardAjax = (AdminDashboardAjax) springContext.getBean("adminDashboardAjax");
			System.out.println("adminDashboardAjax: "+adminDashboardAjax);
			
			adminDashboardAjax.runScheduler();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		System.out.println("============================================");
		System.out.println("SendConversionMarketingMailService End: "+ new Date());
		System.out.println("============================================");
	}
	
}

