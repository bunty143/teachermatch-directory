package tm.services.quartz;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.UserUploadFolderAccess;
import tm.bean.master.DistrictMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.utility.Utility;

public class ApplicantHire implements Job {
	
	public static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

	
	ApplicationContext springContext=null;
    ServletContext servletContext = null;
    JobOrderDAO jobOrderDAO = null;
	UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	JobForTeacherDAO jobForTeacherDAO;
	Integer ndDistrictID  = null; //6300021;   // for North carolina district....  it will be change for headquarterId
	Integer hqid = 2; 
	DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO = null;
	JobRequisitionNumbersDAO jobRequisitionNumbersDAO = null;
	 
	 DistrictMasterDAO districtMasterDAO = null;
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		
	try{
 
		 springContext = WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		 
		// System.out.println("********************************SprintContext#####################################"+springContext);
		 if(springContext!=null){
		 jobOrderDAO = (JobOrderDAO) springContext.getBean("jobOrderDAO"); 
		 userUploadFolderAccessDAO =  (UserUploadFolderAccessDAO)springContext.getBean("userUploadFolderAccessDAO");
		 teacherStatusHistoryForJobDAO = (TeacherStatusHistoryForJobDAO)springContext.getBean("teacherStatusHistoryForJobDAO");
		 teacherPersonalInfoDAO = (TeacherPersonalInfoDAO)springContext.getBean("teacherPersonalInfoDAO");
		 jobForTeacherDAO = (JobForTeacherDAO)springContext.getBean("jobForTeacherDAO");
		 districtRequisitionNumbersDAO =  (DistrictRequisitionNumbersDAO)springContext.getBean("districtRequisitionNumbersDAO");
		 jobRequisitionNumbersDAO =  (JobRequisitionNumbersDAO)springContext.getBean("jobRequisitionNumbersDAO");
		 
		 districtMasterDAO = (DistrictMasterDAO)springContext.getBean("districtMasterDAO");
		 
		 servletContext = ContextLoaderListener.getCurrentWebApplicationContext().getServletContext();

				Criterion drt= Restrictions.eq("functionality", "lastHireCronJobTime");
				  List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
				  uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(drt);
				 if(uploadFolderAccessesrec.size()>0){
					 this.ndDistrictID=uploadFolderAccessesrec.get(0).getDistrictId().getDistrictId();
				 }
				 else{
					
					 DistrictMaster districtMaster=new DistrictMaster();
					  districtMaster.setDistrictId(3702340);
					  UserUploadFolderAccess usrf= new UserUploadFolderAccess();
					  usrf.setDistrictId(districtMaster);
					  usrf.setLastUploadDateTime(new Date());
					  usrf.setFunctionality("lastHireCronJobTime");
					  usrf.setFolderPath("");
					  usrf.setStatus("A");
					  userUploadFolderAccessDAO.makePersistent(usrf);   // update last updated time
					 
					  
					  this.ndDistrictID=3702340;
					 
				 }
		 }
			}
			catch (Exception e) {
//				System.out.println("Please insrt 1 row in table useruploadfolderaccessdetails functionality name  = lastHireCronJobTime  and district id  =  6300021");
				e.printStackTrace();
			}
		 
		 
		 
		 
		Date lastUpdate=setLastApllicantHireCronJobTime();		
		    List<TeacherStatusHistoryForJob> hiredteachers = teacherStatusHistoryForJobDAO.hiredteachers(lastUpdate, 6, ndDistrictID);   // for district
		
		 if(hiredteachers.size()>0){
			writeToTextFile(hiredteachers);
					
		System.out.println(" ApplicantHire Cron Job Fire for Exacting  Data in txt file ( TMI1-CCYYMMDDHHMMSS-00-2261-HIRE--HireData.txt )  "+new Date());
		
		 }
		 else{
			 System.out.println("ApplicantHire Cron Job has no Data    "+new Date());
			}	
		 
		 
		 System.out.println("==============District Id ============"+ndDistrictID);
		 
			}
	
	@Transactional
	public  void writeToTextFile(List<TeacherStatusHistoryForJob> hiredteachers) {

		 
		// get Folder loction from UserUploadFolderAccess table 
		    Criterion functionality = Restrictions.eq("functionality", "applicantTxtExport");
		    Criterion active = Restrictions.eq("status", "A");
		    List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
		    uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
		    
		      String fileName = "TMI1-"+DATE_TIME_FORMAT.format(new Date())+"-00-2261-HIRE--HireData.txt";
		      String folderPath = null;
		     
		      // get foler location from useruploadFolderAccess table 
		      if(uploadFolderAccessesrec.size()!=0){
		      folderPath= uploadFolderAccessesrec.get(0).getFolderPath();
		     
		
		      if(! new File(folderPath).exists())
		    	  new File(folderPath).mkdir();
		      
		File f = new File(folderPath+fileName);	
		BufferedWriter bwriter=null;
		try {

			// if file not Exist
			if (!f.exists()) {
				f.createNewFile();
			}

			FileWriter fwriter = new FileWriter(f);
			 bwriter = new BufferedWriter(fwriter);
			//bwriter.write("LEA|SSN|First Name|Last Name|Street address|Address 2|City|State|Zip Code|Home Phone|Cell Phone|Personal Email|Source System|Position Number|Internal Employee");
			//bwriter.write(System.getProperty("line.separator"));
		
			List<TeacherPersonalInfo> teachers =null;	
			List<JobForTeacher> jobForTeacher = new ArrayList<JobForTeacher>();
			List<Integer> teacherids= new ArrayList<Integer>();
			List<JobOrder> jobids= new ArrayList<JobOrder>();
			List<TeacherDetail> teacheridsObjs= new ArrayList<TeacherDetail>();
			List<DistrictMaster> listDistrictMasters=new ArrayList<DistrictMaster>();
			
			for(TeacherStatusHistoryForJob  data : hiredteachers){
				teacherids.add(data.getTeacherDetail().getTeacherId());
				jobids.add(data.getJobOrder());
				teacheridsObjs.add(data.getTeacherDetail());
			}			
			HashMap<Integer, TeacherPersonalInfo> allteacher = new HashMap<Integer, TeacherPersonalInfo>();
						
			Criterion crt= Restrictions.in("teacherId", teacherids);
			teachers=teacherPersonalInfoDAO.findByCriteria(crt);
			
			for(TeacherPersonalInfo teacherdata : teachers){
				allteacher.put(teacherdata.getTeacherId(), teacherdata);   //set to HashMap				
			}
			Criterion crtTeacherObj= Restrictions.in("teacherId", teacheridsObjs);
			Criterion crtJobsObj= Restrictions.in("jobId", jobids);
			jobForTeacher = jobForTeacherDAO.findByCriteria(crtTeacherObj,crtJobsObj);
			TeacherPersonalInfo teacher=null;			
			
			List<String> requistions = new ArrayList<String>();
			Map<String, JobForTeacher> jftmap= new HashMap<String, JobForTeacher>();			
			if(jobForTeacher!=null && jobForTeacher.size()>0){
				for (JobForTeacher jftobj : jobForTeacher) {
					jftmap.put(jftobj.getJobId().getJobId()+"##"+jftobj.getTeacherId().getTeacherId(), jftobj);
					}
			}
			
			for(TeacherStatusHistoryForJob  dataForDistrict : hiredteachers){
				if(dataForDistrict.getJobOrder()!=null && dataForDistrict.getJobOrder().getDistrictMaster()!=null)
				{
				listDistrictMasters.add(dataForDistrict.getJobOrder().getDistrictMaster());
				}
			}			
			Map<Integer,JobRequisitionNumbers> jrnmap = new HashMap<Integer, JobRequisitionNumbers>();
			List<JobRequisitionNumbers> requisitionNumbers = jobRequisitionNumbersDAO.getHRStatusByJobId(jobids);
			
			if(requisitionNumbers!=null && requisitionNumbers.size()>0)
			for(JobRequisitionNumbers jobReqNumber : requisitionNumbers)
			{				
				jrnmap.put(jobReqNumber.getJobOrder().getJobId(), jobReqNumber);				
			}
			System.out.println("hiredteachers==========================================="+hiredteachers.size());
			System.out.println("jobids===================================================="+jobids.size());
			System.out.println("requisitionNumbers========================================="+requisitionNumbers.size());			
			System.out.println("jrn map size---------=================================="+jrnmap.size());
					
		
		for(TeacherStatusHistoryForJob  data : hiredteachers){
			 
			String requisition="";
			String candidateFlag="";
			String schoolLocationCode = "";
			String positionDescription = "";
			try {				
				if(jrnmap.containsKey(data.getJobOrder().getJobId())){										
					if(jftmap!=null && jftmap.get(data.getJobOrder().getJobId()+"##"+data.getTeacherDetail().getTeacherId()).getIsAffilated()!=null)
						candidateFlag=jftmap.get(data.getJobOrder().getJobId()+"##"+data.getTeacherDetail().getTeacherId()).getIsAffilated()+"";
					
					requisition=jrnmap.get(data.getJobOrder().getJobId()).getDistrictRequisitionNumbers().getRequisitionNumber();
					System.out.println("M Req: "+requisition);
										
					try {
						if(jrnmap.get(data.getJobOrder().getJobId()).getSchoolMaster()!=null && jrnmap.get(data.getJobOrder().getJobId()).getSchoolMaster().getLocationCode()!=null )
						schoolLocationCode = jrnmap.get(data.getJobOrder().getJobId()).getSchoolMaster().getLocationCode();							
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {						
						System.out.println(data.getJobOrder().getJobDescription());
						if(data.getJobOrder().getJobDescription()!=null)
						positionDescription = data.getJobOrder().getJobDescription().replaceAll("\\<.*?\\>", "");						
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try{
				
		    teacher=allteacher.get(data.getTeacherDetail().getTeacherId());		    
		    if(teacher!=null){
			bwriter.write(""+(data.getJobOrder().getDistrictMaster()==null?"":(data.getJobOrder().getDistrictMaster().getLocationCode()==null?"":data.getJobOrder().getDistrictMaster().getLocationCode()))+"");
			bwriter.write("|"+(teacher.getSSN()==null ? "" : Utility.decodeBase64(teacher.getSSN()))+"");
			bwriter.write("|"+teacher.getFirstName()+"");
			bwriter.write("|"+ (teacher.getLastName()==null ? "":teacher.getLastName())+"");
			bwriter.write("|"+ (teacher.getAddressLine1()==null ? "" : teacher.getAddressLine1())+"");
			bwriter.write("|"+ (teacher.getAddressLine2()==null ? "" : teacher.getAddressLine2())+"");
			bwriter.write("|"+(teacher.getCityId()==null ? "" : (teacher.getCityId().getCityName()==null?"" : teacher.getCityId().getCityName()  ) )+"");
			bwriter.write("|"+(teacher.getStateId()==null ? "":(teacher.getStateId().getStateName()==null? "" : teacher.getStateId().getStateShortName())+""));
			bwriter.write("|"+(teacher.getZipCode()==null ? "" : teacher.getZipCode())+"");
			bwriter.write("|"+(teacher.getPhoneNumber() == null ? "" : teacher.getPhoneNumber())+"");
			bwriter.write("|"+(teacher.getMobileNumber()==null ? "" :teacher.getMobileNumber())+"");
			bwriter.write("|"+(data.getTeacherDetail()==null?"":data.getTeacherDetail().getEmailAddress().toString())+"");
			bwriter.write("|TMI");
			bwriter.write("|"+(requisition==null?"":requisition)+"");
			bwriter.write("|"+(candidateFlag==null?"":candidateFlag)+"");
			bwriter.write("|"+(schoolLocationCode==null?"":schoolLocationCode)+"");
			bwriter.write("|"+(positionDescription==null?"":positionDescription)+"");
			bwriter.write(System.getProperty("line.separator"));
			 }
		    }
			catch (Exception e) {
				System.out.println(" Exception for For Data :- "+ data.getJobOrder().getJobId());
			e.printStackTrace();
			}			
			
		}

		bwriter.close();
		FTPConnectAndLogin.startFTP(folderPath,fileName);
		
		} catch (Exception e) {
//			f.deleteOnExit();
			e.printStackTrace();
		}
		
		      }
		      else{
		    	  System.out.println();
		    	  
		      }
		
		  
		
		
	}


	// for update LastApllicantHireCronJobTime in table( useruploadfolderaccessdetails ) - functionality (lastHireCronJobTime )
	public Date setLastApllicantHireCronJobTime() {
	 
		Date lastUpdate=null;
		try{
		
			 
			
			
			Criterion drt= Restrictions.eq("functionality", "lastHireCronJobTime");
			  List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
			  uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(drt);
			 if(uploadFolderAccessesrec.size()>0){
			  
			  UserUploadFolderAccess usrf= uploadFolderAccessesrec.get(0);
			  lastUpdate=usrf.getLastUploadDateTime();
			  usrf.setLastUploadDateTime(new Date());
			  userUploadFolderAccessDAO.makePersistent(usrf);   // update last updated time
			  
			  
			 }
			 else{
				 
				System.out.println("Please insrt 1 row in table useruploadfolderaccessdetails functionality name  = lastHireCronJobTime  and district id  =  6300021");
			
			 
			 }
			
		}
		catch (Exception e) {
		e.printStackTrace();
		}
		return lastUpdate;
		
		
	}
	
	
//	// get value of a specific key at run time from resources.properties
//	 public String getValueFromResource(String key) throws IOException{
//		 
//		 String result = "";
//			Properties prop = new Properties();
//			String propFileName = "resource.properties";
//
//			InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
//
//			if (inputStream != null) {
//				prop.load(inputStream);
//			} else {
//				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
//			}
//		  result = prop.getProperty(key);
//		 System.out.println("----------------"+result+"-------------------");
//		 return result;
//	 }

}

