package tm.services.quartz;


import java.util.Date;

import javax.servlet.ServletContext;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.services.PortfolioReminderAjax;

public class ApplicantManagementReminderService implements Job{

	
	public void execute(JobExecutionContext arg0) throws JobExecutionException{
		
		System.out.println("============================================");
		System.out.println("ApplicantManagementReminderService Start: "+ new Date());
		
		ApplicationContext springContext = 
		    WebApplicationContextUtils.getWebApplicationContext(
		        ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		
		PortfolioReminderAjax portfolioReminderAjax = null;
		
		try {
		    
			ServletContext ctx = ContextLoaderListener.getCurrentWebApplicationContext().getServletContext();
			System.out.println("ctx: "+ctx);
			
			portfolioReminderAjax = (PortfolioReminderAjax) springContext.getBean("portfolioReminderAjax");
			System.out.println("portfolioReminderAjax: "+portfolioReminderAjax);
			
			portfolioReminderAjax.startApplicantManagementReminder();
			portfolioReminderAjax.startApplicantManagementNoResponse();
			
			//commonDashboardAjax.startReminderForHQ();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//////////////////////////////////////
		System.out.println("============================================");
		System.out.println("ApplicantManagementReminderService  End: "+ new Date());
		System.out.println("============================================");
	}
}

