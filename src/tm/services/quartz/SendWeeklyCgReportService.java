package tm.services.quartz;


import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.CgEmailSentLog;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolKeyContact;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolKeyContactDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.services.report.CandidateGridAjax;
import tm.utility.Utility;


public class SendWeeklyCgReportService implements Job{

	String locale = Utility.getValueOfPropByKey("locale");
	
	public void execute(JobExecutionContext arg0) throws JobExecutionException{

		System.out.println("============================================");
		System.out.println("SendWeeklyCgReportService Start: "+ new Date());

		String basepath = Utility.getValueOfPropByKey("basePath");

		ApplicationContext springContext = 
			WebApplicationContextUtils.getWebApplicationContext(
					ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());

		JobOrderDAO jobOrderDAO = null;
		DistrictMasterDAO districtMasterDAO = null;
		SchoolMasterDAO schoolMasterDAO = null;
		DistrictKeyContactDAO districtKeyContactDAO = null;
		SchoolKeyContactDAO schoolKeyContactDAO = null;
		SchoolInJobOrderDAO schoolInJobOrderDAO = null;
		CandidateGridAjax candidateGridAjax = null;
		
		boolean cgFlag = false;
		try {

			ServletContext ctx = ContextLoaderListener.getCurrentWebApplicationContext().getServletContext();
			System.out.println("ctx: "+ctx);

			jobOrderDAO = (JobOrderDAO) springContext.getBean("jobOrderDAO");
			//System.out.println("jobOrder: "+jobOrderDAO);
			districtMasterDAO = (DistrictMasterDAO) springContext.getBean("districtMasterDAO");
			schoolMasterDAO = (SchoolMasterDAO) springContext.getBean("schoolMasterDAO");
			districtKeyContactDAO = (DistrictKeyContactDAO) springContext.getBean("districtKeyContactDAO");
			schoolKeyContactDAO = (SchoolKeyContactDAO) springContext.getBean("schoolKeyContactDAO");
			schoolInJobOrderDAO = (SchoolInJobOrderDAO) springContext.getBean("schoolInJobOrderDAO");
			candidateGridAjax = (CandidateGridAjax) springContext.getBean("candidateAjax");
			EmailerService emailerService = (EmailerService)springContext.getBean("myMailSender");
			StatusMasterDAO statusMasterDAO = (StatusMasterDAO)springContext.getBean("statusMasterDAO");

			//String[] statuss = {"comp","icomp","hird","rem","vlt"};
			String[] statuss = {"comp","icomp","vlt","widrw","hird","scomp","ecomp","vcomp","dcln","rem"};
			List<StatusMaster> lstStatusMasters = null;
			try{
				lstStatusMasters = Utility.getStaticMasters(statuss);
			}catch (Exception e) {
				e.printStackTrace();
			}
			Map<Integer,String> cgfileMap = new HashMap<Integer, String>();

			List<DistrictMaster> lstDistrictMaster = null;
			List<SchoolMaster> lstSchoolMaster = null;
			List<DistrictKeyContact> districtKeyContactList = null;
			List<SchoolKeyContact> schoolKeyContactList = null;
			String basePath = ctx.getRealPath("/")+"/cguser/";
			
			File file1 = new File(basePath);
			if(file1.exists())
				Utility.deleteAllFileFromDir(basePath);
			else
				file1.mkdirs();
			
			String realPath = ctx.getRealPath("/");
			
			UserMaster usermaster = new UserMaster();
			usermaster.setUserId(1);
			usermaster.setEntityType(2);
			RoleMaster roleMaster = new RoleMaster();
			roleMaster.setEntityType(2);
			roleMaster.setRoleId(1);
			usermaster.setRoleId(roleMaster);
			
			String emailsTo = null;
			String districtName = null;
			String schoolName = null;
			String cgfile = null;
			
			SessionFactory sessionFactory =  districtMasterDAO.getSessionFactory();
			StatelessSession session = sessionFactory.openStatelessSession();
			
			//for district
			 
			lstDistrictMaster = districtMasterDAO.findDistrictsWeeklyCgReportRequired(1);
		
			for (DistrictMaster districtMaster : lstDistrictMaster) {
				
				districtName = districtMaster.getDistrictName();
				
				districtKeyContactList = districtKeyContactDAO.findDistrictsWeeklyCgReportRequired(districtMaster);
				System.out.println("districtKeyContactList.size(): "+districtKeyContactList.size());
				int size = districtKeyContactList.size();

				if(size>0)
				{
					List<String> tos = new ArrayList<String>();
					for (DistrictKeyContact districtKeyContact : districtKeyContactList) {
						tos.add(districtKeyContact.getKeyContactEmailAddress());
					}
					
					emailsTo = tos.toString().substring(1);
					emailsTo = emailsTo.substring(0, emailsTo.length()-1);
					
					List<JobOrder> jobOrderList = jobOrderDAO.findAllJobOrderbyDistrict(districtMaster);
					System.out.println("jobOrderList: "+jobOrderList.size());
					
					List<JobOrder> jobs = new ArrayList<JobOrder>(); 
					
					if(jobOrderList.size()>0)
					{
						for (JobOrder jobOrder : jobOrderList) 
						{
							String time = String.valueOf(System.currentTimeMillis()).substring(6);
							String fileName =time+"Report.pdf";

							try{
								cgFlag = candidateGridAjax.generateCandidateReportData(usermaster, jobOrder, basePath+"/"+fileName,realPath,lstStatusMasters);
								System.out.println(jobOrder.getJobId()+" cgFlag: "+cgFlag);
							}catch (Exception e) {
								e.printStackTrace();
							}
							
							//filePaths.add(realPath+"/cguser/"+fileName);
							if(cgFlag)
							{
								jobs.add(jobOrder);
								cgfileMap.put(jobOrder.getJobId(), realPath+"/cguser/"+fileName);
							}
						}
						
						//////////////////////// One Mail logic ////////////////////////////////////////////////////
						if(jobs.size()>0)
						{
							emailerService.sendMailsToMultipleContacts(tos, ""+Utility.getLocaleValuePropByKey("msgCandidateGridJobOrders", locale)+"","vishwanath@netsutra.com",(MailText.getCGMailText(jobs)));
							//inserting of mail sent
							try {
								CgEmailSentLog cgEmailSentLog = new CgEmailSentLog();
								cgEmailSentLog.setCreatedDateTime(new Date());
								cgEmailSentLog.setSentTos(emailsTo);
								cgEmailSentLog.setDistrictName(districtName);
								cgEmailSentLog.setSchoolName(schoolName);
								cgEmailSentLog.setJobTitle("");
								cgEmailSentLog.setJobType(2);
								session.insert(cgEmailSentLog);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						
					}
				}
			}

			////////////////////// school ///////////////////////////////////
			districtName = null;
			
			lstSchoolMaster = schoolMasterDAO.findDistrictsWeeklyCgReportRequired(1);

			JobOrder jo = null;
			for (SchoolMaster schoolMaster : lstSchoolMaster) {

				schoolKeyContactList = schoolKeyContactDAO.findDistrictsWeeklyCgReportRequired(schoolMaster);
				System.out.println("schoolKeyContactList.size(): "+schoolKeyContactList.size());
				int size = schoolKeyContactList.size();
				
				List<JobOrder> jobs = new ArrayList<JobOrder>(); 
				
				if(size>0)
				{
					schoolName = schoolMaster.getSchoolName();
					districtName = schoolMaster.getDistrictName();
					
					List<String> tos = new ArrayList<String>();
					for (SchoolKeyContact schoolKeyContact : schoolKeyContactList) {
						tos.add(schoolKeyContact.getKeyContactEmailAddress());
					}

					emailsTo = tos.toString().substring(1);
					emailsTo = emailsTo.substring(0, emailsTo.length()-1);
					
					List<SchoolInJobOrder> schoolInJobOrderList =  schoolInJobOrderDAO.findJobBySchoolForCG(schoolMaster);
					if(schoolInJobOrderList.size()>0)
					{
						for (SchoolInJobOrder schoolInJobOrder : schoolInJobOrderList) {
							jo = schoolInJobOrder.getJobId();
							cgfile = cgfileMap.get(jo.getJobId());
							
							if(cgfile==null)
							{
								String time = String.valueOf(System.currentTimeMillis()).substring(6);
								String fileName =time+"Report.pdf";
								try{
									cgFlag = candidateGridAjax.generateCandidateReportData(usermaster, jo, basePath+"/"+fileName,realPath,lstStatusMasters);
									System.out.println(jo.getJobId()+" cgFlag: "+cgFlag);
								}catch(Exception e)
								{
									e.printStackTrace();
								}

								cgfile = realPath+"/cguser/"+fileName;
							}else
								cgFlag=true;
							
							if(cgFlag)
							{
								jobs.add(jo);
							}
						}
						
						if(jobs.size()>0)
						{
							emailerService.sendMailsToMultipleContacts(tos,""+Utility.getLocaleValuePropByKey("msgCandidateGridJobOrders", locale)+"","vishwanath@netsutra.com",MailText.getCGMailText(jobs));
							
							//insertion of mail sent
							try {
								CgEmailSentLog cgEmailSentLog = new CgEmailSentLog();
								cgEmailSentLog.setCreatedDateTime(new Date());
								cgEmailSentLog.setSentTos(emailsTo);
								cgEmailSentLog.setDistrictName(districtName);
								cgEmailSentLog.setSchoolName(schoolName);
								cgEmailSentLog.setJobTitle("");
								cgEmailSentLog.setJobType(3);
								session.insert(cgEmailSentLog);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}

			File file0 = new File(ctx.getRealPath("/")+"/candidatereports/");
			if(file0.exists())
				FileUtils.deleteDirectory(file0);

		} catch (Exception e) {
			e.printStackTrace();
		}


		System.out.println("============================================");
		System.out.println("SendWeeklyCgReportService End: "+ new Date());
		System.out.println("============================================");
	}
	
	/*public void execute(JobExecutionContext arg0) throws JobExecutionException{

		System.out.println("============================================");
		System.out.println("SendWeeklyCgReportService Start: "+ new Date());

		String basepath = Utility.getValueOfPropByKey("basePath");

		ApplicationContext springContext = 
			WebApplicationContextUtils.getWebApplicationContext(
					ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());

		JobOrderDAO jobOrderDAO = null;
		DistrictMasterDAO districtMasterDAO = null;
		SchoolMasterDAO schoolMasterDAO = null;
		DistrictKeyContactDAO districtKeyContactDAO = null;
		SchoolKeyContactDAO schoolKeyContactDAO = null;
		SchoolInJobOrderDAO schoolInJobOrderDAO = null;
		CandidateGridAjax candidateGridAjax = null;
		
		boolean cgFlag = false;
		try {

			ServletContext ctx = ContextLoaderListener.getCurrentWebApplicationContext().getServletContext();
			System.out.println("ctx: "+ctx);

			jobOrderDAO = (JobOrderDAO) springContext.getBean("jobOrderDAO");
			//System.out.println("jobOrder: "+jobOrderDAO);
			districtMasterDAO = (DistrictMasterDAO) springContext.getBean("districtMasterDAO");
			schoolMasterDAO = (SchoolMasterDAO) springContext.getBean("schoolMasterDAO");
			districtKeyContactDAO = (DistrictKeyContactDAO) springContext.getBean("districtKeyContactDAO");
			schoolKeyContactDAO = (SchoolKeyContactDAO) springContext.getBean("schoolKeyContactDAO");
			schoolInJobOrderDAO = (SchoolInJobOrderDAO) springContext.getBean("schoolInJobOrderDAO");
			candidateGridAjax = (CandidateGridAjax) springContext.getBean("candidateAjax");
			EmailerService emailerService = (EmailerService)springContext.getBean("myMailSender");

			Map<Integer,String> cgfileMap = new HashMap<Integer, String>();

			List<DistrictMaster> lstDistrictMaster = null;
			List<SchoolMaster> lstSchoolMaster = null;
			List<DistrictKeyContact> districtKeyContactList = null;
			List<SchoolKeyContact> schoolKeyContactList = null;
			String basePath = ctx.getRealPath("/")+"/cguser/";
			
			File file1 = new File(basePath);
			if(file1.exists())
				Utility.deleteAllFileFromDir(basePath);
			else
				file1.mkdirs();
			
			String realPath = ctx.getRealPath("/");
			
			UserMaster usermaster = new UserMaster();
			usermaster.setUserId(1);
			usermaster.setEntityType(2);
			RoleMaster roleMaster = new RoleMaster();
			roleMaster.setEntityType(2);
			roleMaster.setRoleId(1);
			usermaster.setRoleId(roleMaster);
			
			String emailsTo = null;
			String districtName = null;
			String schoolName = null;
			String cgfile = null;
			
			SessionFactory sessionFactory =  districtMasterDAO.getSessionFactory();
			StatelessSession session = sessionFactory.openStatelessSession();
			
			//for district
			 
			lstDistrictMaster = districtMasterDAO.findDistrictsWeeklyCgReportRequired(1);
		
			for (DistrictMaster districtMaster : lstDistrictMaster) {
				
				districtName = districtMaster.getDistrictName();
				
				districtKeyContactList = districtKeyContactDAO.findDistrictsWeeklyCgReportRequired(districtMaster);
				System.out.println("districtKeyContactList.size(): "+districtKeyContactList.size());
				int size = districtKeyContactList.size();

				if(size>0)
				{
					List<String> tos = new ArrayList<String>();
					for (DistrictKeyContact districtKeyContact : districtKeyContactList) {
						tos.add(districtKeyContact.getKeyContactEmailAddress());
					}
					
					emailsTo = tos.toString().substring(1);
					emailsTo = emailsTo.substring(0, emailsTo.length()-1);
					
					List<JobOrder> jobOrderList = jobOrderDAO.findAllJobOrderbyDistrict(districtMaster);
					System.out.println("jobOrderList: "+jobOrderList.size());
					if(jobOrderList.size()>0)
					{
						for (JobOrder jobOrder : jobOrderList) {
							
							String time = String.valueOf(System.currentTimeMillis()).substring(6);
							String fileName =time+"Report.pdf";

							try{
								cgFlag = candidateGridAjax.generateCandidateReport(usermaster, jobOrder, basePath+"/"+fileName,realPath);
								System.out.println(jobOrder.getJobId()+" cgFlag: "+cgFlag);
							}catch (Exception e) {
								e.printStackTrace();
							}
							
							//filePaths.add(realPath+"/cguser/"+fileName);
							if(cgFlag)
							{
								System.out.println("url: "+basepath+"/cguser/"+fileName);
								cgfileMap.put(jobOrder.getJobId(), realPath+"/cguser/"+fileName);
								emailerService.sendMailsWithMultipleAttachments(tos, "Candidate Grid Report for \""+jobOrder.getJobTitle()+"\"","vishwanath@netsutra.com",Utility.getUTFToHTML(MailText.getCGMailText(jobOrder,districtMaster.getDistrictName())),realPath+"/cguser/"+fileName);
								
								//inserting of mail sent
								try {
									CgEmailSentLog cgEmailSentLog = new CgEmailSentLog();
									cgEmailSentLog.setCreatedDateTime(new Date());
									cgEmailSentLog.setSentTos(emailsTo);
									cgEmailSentLog.setDistrictName(districtName);
									cgEmailSentLog.setSchoolName(schoolName);
									cgEmailSentLog.setJobTitle(jobOrder.getJobTitle());
									cgEmailSentLog.setJobType(jobOrder.getCreatedForEntity());
									session.insert(cgEmailSentLog);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			}

			////////////////////// school ///////////////////////////////////
			districtName = null;
			
			lstSchoolMaster = schoolMasterDAO.findDistrictsWeeklyCgReportRequired(1);

			JobOrder jo = null;
			for (SchoolMaster schoolMaster : lstSchoolMaster) {

				schoolKeyContactList = schoolKeyContactDAO.findDistrictsWeeklyCgReportRequired(schoolMaster);
				System.out.println("schoolKeyContactList.size(): "+schoolKeyContactList.size());
				int size = schoolKeyContactList.size();

				if(size>0)
				{
					schoolName = schoolMaster.getSchoolName();
					districtName = schoolMaster.getDistrictName();
					
					List<String> tos = new ArrayList<String>();
					for (SchoolKeyContact schoolKeyContact : schoolKeyContactList) {
						tos.add(schoolKeyContact.getKeyContactEmailAddress());
					}

					emailsTo = tos.toString().substring(1);
					emailsTo = emailsTo.substring(0, emailsTo.length()-1);
					
					List<SchoolInJobOrder> schoolInJobOrderList =  schoolInJobOrderDAO.findJobBySchoolForCG(schoolMaster);
					if(schoolInJobOrderList.size()>0)
					{
						for (SchoolInJobOrder schoolInJobOrder : schoolInJobOrderList) {
							jo = schoolInJobOrder.getJobId();
							cgfile = cgfileMap.get(jo.getJobId());
							
							if(cgfile==null)
							{
								String time = String.valueOf(System.currentTimeMillis()).substring(6);
								String fileName =time+"Report.pdf";
								try{
									cgFlag = candidateGridAjax.generateCandidateReport(usermaster, jo, basePath+"/"+fileName,realPath);
									System.out.println(jo.getJobId()+" cgFlag: "+cgFlag);
								}catch(Exception e)
								{
									e.printStackTrace();
								}
								System.out.println("url: "+basepath+"/cguser/"+fileName);
								cgfile = realPath+"/cguser/"+fileName;
							}else
								cgFlag=true;
							
							if(cgFlag)
							{
								emailerService.sendMailsWithMultipleAttachments(tos,"Candidate Grid Report for \""+jo.getJobTitle()+"\"","vishwanath@netsutra.com",Utility.getUTFToHTML(MailText.getCGMailText(jo,schoolInJobOrder.getSchoolId().getSchoolName())),cgfile);
								
								//insertion of mail sent
								try {
									CgEmailSentLog cgEmailSentLog = new CgEmailSentLog();
									cgEmailSentLog.setCreatedDateTime(new Date());
									cgEmailSentLog.setSentTos(emailsTo);
									cgEmailSentLog.setDistrictName(districtName);
									cgEmailSentLog.setSchoolName(schoolName);
									cgEmailSentLog.setJobTitle(jo.getJobTitle());
									cgEmailSentLog.setJobType(jo.getCreatedForEntity());
									session.insert(cgEmailSentLog);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			}

			File file0 = new File(ctx.getRealPath("/")+"/candidatereports/");
			if(file0.exists())
				FileUtils.deleteDirectory(file0);

		} catch (Exception e) {
			e.printStackTrace();
		}


		System.out.println("============================================");
		System.out.println("SendWeeklyCgReportService End: "+ new Date());
		System.out.println("============================================");
	}*/

}

