package tm.services.quartz;

import java.util.Map;

import javax.servlet.ServletContext;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.dao.JobOrderDAO;
import tm.services.es.ElasticSearchService;


public class ElasticSearchJobsOfInterestNotCandidateUpdateService  implements Job
{
	JobOrderDAO jobOrderDAO = null;
	ApplicationContext springContext=null;
	ServletContext servletContext = null;
	
	@Transactional
	public void execute(JobExecutionContext arg0) throws JobExecutionException
	{
		
		//not for candidate
		springContext = WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		jobOrderDAO = (JobOrderDAO) springContext.getBean("jobOrderDAO");
		ElasticSearchService es=new ElasticSearchService();
		System.out.println("schedular for updateESForJobsOfInterestNotCandidate.do start");
		Map<Integer, String> dataBaseMap=es.getDBDataForJobsOfInterest(jobOrderDAO);
		Map<Integer,String> elasticMap=es.esForJobsOfInterestNotCandidateAll();
		es.updateESForJobsOfInterestNotCandidate(dataBaseMap, elasticMap);
			
		System.out.println("schedular for updateESForJobsOfInterestNotCandidate.do end");
		
		//for candidate
		/*ElasticSearchService es1=new ElasticSearchService();
		System.out.println("schedular for updateESForJobsOfInterestForCandidate.do start");
		Map<Integer, String> dataBaseMap1=es1.getDBDataForJobsOfInterestForCandidate(jobOrderDAO);
		Map<Integer,String> elasticMap1=es1.esForJobsOfInterestForCandidateAll();
		es1.updateESForJobsOfInterestForCandidate(dataBaseMap1, elasticMap1);*/
			
		//System.out.println("schedular for updateESForJobsOfInterestForCandidate.do end");
		
		
	}

}
