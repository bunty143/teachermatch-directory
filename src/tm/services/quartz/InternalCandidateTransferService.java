package tm.services.quartz;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.JobCategoryForInternalCandidate;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.dao.JobCategoryForInternalCandidateDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.services.AdminDashboardAjax;
import tm.services.CommonDashboardAjax;
import tm.services.CommonService;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.utility.Utility;


public class InternalCandidateTransferService implements Job{

	
	public void execute(JobExecutionContext arg0) throws JobExecutionException{
		
		System.out.println("============================================");
		System.out.println("InternalCandidateTransferService Start: "+ new Date());
		
		ApplicationContext springContext = 
		    WebApplicationContextUtils.getWebApplicationContext(
		        ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		
		JobOrderDAO jobOrderDAO = null;
		SchoolInJobOrderDAO schoolInJobOrderDAO = null;
		JobCategoryForInternalCandidateDAO jobCategoryForInternalCandidateDAO = null;
		CommonService commonService =null; 
		EmailerService emailerService = null;
		try {
		    
			ServletContext ctx = ContextLoaderListener.getCurrentWebApplicationContext().getServletContext();
			System.out.println("ctx: "+ctx);
			jobOrderDAO = (JobOrderDAO) springContext.getBean("jobOrderDAO");
			schoolInJobOrderDAO = (SchoolInJobOrderDAO) springContext.getBean("schoolInJobOrderDAO");
			jobCategoryForInternalCandidateDAO = (JobCategoryForInternalCandidateDAO) springContext.getBean("jobCategoryForInternalCandidateDAO");
			commonService = (CommonService) springContext.getBean("commonService");
			emailerService = (EmailerService) springContext.getBean("myMailSender");
			//List<DistrictMaster> districtMasters = new ArrayList<DistrictMaster>();
			/*districtMasters = jobOrderDAO.getUniqueDistrictsByJobOrderStartDate();*/
			List<JobOrder> jobOrders = new ArrayList<JobOrder>();
			jobOrders = jobOrderDAO.getJobOrdersByStartDate();
			System.out.println("jobOrders.size(): "+jobOrders.size());
			//Map<Integer,String> map = new HashMap<Integer, String>(); 
			
			/*if(jobOrders.size()>0)
			{
				List<SchoolInJobOrder> schoolInJobOrders = schoolInJobOrderDAO.getSIJO(jobOrders);
				System.out.println("=============================");
				for (SchoolInJobOrder schoolInJobOrder : schoolInJobOrders) {
					String schoolName = map.get(schoolInJobOrder.getJobId().getJobId());
					if(schoolName==null)
						map.put(schoolInJobOrder.getJobId().getJobId(),schoolInJobOrder.getSchoolId().getSchoolName());
					else
						map.put(schoolInJobOrder.getJobId().getJobId(),schoolName+", "+schoolInJobOrder.getSchoolId().getSchoolName());
				}
			}*/
			String basePath = Utility.getValueOfPropByKey("basePath");
			Map<Integer,List<SchoolMaster>> schoolMap = new HashMap<Integer, List<SchoolMaster>>(); 
			if(jobOrders.size()>0)
			{
				List<SchoolInJobOrder> schoolInJobOrders = schoolInJobOrderDAO.getSIJO(jobOrders);
				System.out.println("=============================");
				for (SchoolInJobOrder schoolInJobOrder : schoolInJobOrders) {
					List<SchoolMaster> sms = schoolMap.get(schoolInJobOrder.getJobId().getJobId());
					if(sms==null)
						sms = new ArrayList<SchoolMaster>();

					sms.add(schoolInJobOrder.getSchoolId());
					schoolMap.put(schoolInJobOrder.getJobId().getJobId(),sms);
				}
			}
			System.out.println("schoolMap------------------> "+schoolMap.size());
	
			for (JobOrder jobOrder : jobOrders) {
				//districtMasters.add(jobOrder.getDistrictMaster());
				DistrictMaster districtMaster = jobOrder.getDistrictMaster();
				String[] arrHrDetail = new String[20]; 
				
				List<JobCategoryForInternalCandidate> jfiList = jobCategoryForInternalCandidateDAO.getTeachers(jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster(),jobOrder.getSubjectMaster());
				if(jfiList.size()>0)
				{
					arrHrDetail = commonService.getHrDetailToTeacherNow(null, jobOrder,schoolMap);
				}
				
				for (JobCategoryForInternalCandidate jobCategoryForInternalCandidate : jfiList) {
					
					List<SchoolMaster> schools = schoolMap.get(jobOrder.getJobId());
					if(schools==null)
						schools = new ArrayList<SchoolMaster>();
					//System.out.println("::::::::::::: "+map.get(jobOrder.getJobId()));
					//System.out.println("::::::::::::: "+jobCategoryForInternalCandidate.getTeacherDetail().getTeacherId());
					TeacherDetail teacherDetail = jobCategoryForInternalCandidate.getTeacherDetail();
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
						
					//String displayName = districtMaster.getDisplayName()==null?districtMaster.getDistrictName():districtMaster.getDisplayName();
					String displayName = (districtMaster.getDisplayName()==null || districtMaster.getDisplayName().trim().length()==0)?districtMaster.getDistrictName():districtMaster.getDisplayName();
					String mailContent = MailText.internalCandidateTransferMailText(basePath,teacherDetail,jobOrder,schools,arrHrDetail);
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("vishwanath@netsutra.com");
					dsmt.setMailto(teacherDetail.getEmailAddress());
					dsmt.setMailsubject("Recent opportunity at "+displayName);
					dsmt.setMailcontent(mailContent);
					dsmt.start();
				}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		//////////////////////////////////////
		System.out.println("============================================");
		System.out.println("InternalCandidateTransferService End: "+ new Date());
		System.out.println("============================================");
	}
	
}

