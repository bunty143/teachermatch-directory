package tm.services.quartz;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.Result;
import javax.xml.transform.Source;

import org.apache.commons.io.FileUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.dao.JobOrderDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.utility.Utility;

public class ZipRecruiterXmlFeed implements Job{
	    
	JobOrderDAO jobOrderDAO = null;
	SchoolMasterDAO schoolMasterDAO = null; 
	ApplicationContext springContext=null;
	ServletContext servletContext = null;
	String fileName="feed.xml";
	
	public void execute(JobExecutionContext context) throws JobExecutionException{
		System.out.println("==================execute==========================: ");
	
		springContext = WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		jobOrderDAO = (JobOrderDAO) springContext.getBean("jobOrderDAO");
		
		System.out.println("joborder:*********** "+jobOrderDAO);
		
		getXMLFeedForZiprecruiterByPOST(jobOrderDAO);
	}
	public void getXMLFeedForZiprecruiterByPOST(JobOrderDAO jobOrderDAO){
		try{
			System.out.println("**************Enter in getXMLFeedForZiprecruiter*********************");
			FileOutputStream outStream = null;
			servletContext = ContextLoaderListener.getCurrentWebApplicationContext().getServletContext();
			System.out.println("servletContext : "+servletContext);
			
	    /***************************creating directory inside  ***rootPath*******************************************/		
			    String jobFeedDir = Utility.getValueOfPropByKey("rootPath"); 
				//System.out.println("jobFeedDir : "+jobFeedDir);	
				
				String sourceDirName = jobFeedDir+"//"+"jobfeed"; 
				String targetFileName = sourceDirName+"/"+fileName; ;
				
				File fileFeedDir = new File(sourceDirName);
				
				File sourceFile= new File(targetFileName);
				//System.out.println("ileFeedDir.exists() before : "+fileFeedDir.exists());
				
				if(!fileFeedDir.exists())
				{
					fileFeedDir.mkdir();
					outStream = new FileOutputStream(sourceFile);
					//System.out.println("directory created succesffully");
				}
				if(sourceFile.exists())
				{
					outStream = new FileOutputStream(targetFileName);
					//System.out.println("sourceFile.exists()writing feed.xml : "+sourceFile.exists());
				}
				
		/********************************************************************/	
			
			List<DistrictMaster> lstDistrictMaster = null;
			List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
			
			
			String country = "USA";				
			String url = "www.teachermatch.org";
			String jobPostingDate = null;
			
			Timestamp currentTime = new Timestamp(new Date().getTime());
			jobPostingDate = currentTime.toString();
			
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = null;
			Document document = null;
			Element root = null;
			
			docBuilder = builderFactory.newDocumentBuilder();
			document = docBuilder.newDocument();
		
			root = document.createElement("source");
			document.appendChild(root);	
			
				if(lstDistrictMaster!=null){
				
				System.out.println("inside districtMaster: ");
				}else{
					    lstJobOrder = jobOrderDAO.findAllActivJobOrder();
						System.out.println("lstJobOrder : "+lstJobOrder);
						System.out.println("");
					}
				
			if(lstJobOrder!=null && lstJobOrder.size()>0){
				for(JobOrder jobOrder : lstJobOrder)
				{
					
					String jobTitle = "";
					Integer referenceNumber = null;
					String company = "";
					String city = "";
					String state = "";
					String postalCode = "";
					String description = "";
					String category = "";
					
					
					jobTitle = jobOrder.getJobTitle();
					if(jobTitle==null || jobTitle.equalsIgnoreCase(""))
					{
						jobTitle = "N/A";
					}
					
					referenceNumber = jobOrder.getJobId();
					String jobId = referenceNumber.toString();
					if(jobId==null ||jobId.equalsIgnoreCase(""))
					{
						jobId = "N/A";
					}
					//System.out.println("jobID: "+jobId);
					
					
					
					if(jobOrder!=null && jobOrder.getDistrictMaster()!=null)
					{
						company = jobOrder.getDistrictMaster().getDistrictName();
						city = jobOrder.getDistrictMaster().getCityName();
						state = jobOrder.getDistrictMaster().getStateId().getStateShortName();
						postalCode = jobOrder.getDistrictMaster().getZipCode();
					}
					else if(jobOrder.getBranchMaster()!=null)
					{
						company = jobOrder.getBranchMaster().getBranchName();
						city = jobOrder.getBranchMaster().getCityName();
						state = jobOrder.getBranchMaster().getStateMaster().getStateShortName();
						postalCode = jobOrder.getBranchMaster().getZipCode();
					}
					else if(jobOrder.getHeadQuarterMaster()!=null)
					{
						company = jobOrder.getHeadQuarterMaster().getHeadQuarterName();
						city = jobOrder.getHeadQuarterMaster().getCityName();
						state = jobOrder.getHeadQuarterMaster().getStateId().getStateShortName();
						postalCode = jobOrder.getHeadQuarterMaster().getZipCode();
					}
					
					if(company==null || company.equalsIgnoreCase(""))
					{
						company = "N/A";
					}
					
					if(city==null || city.equalsIgnoreCase(""))
					{
						city = "N/A";
					}
					
					if(state==null ||state.equalsIgnoreCase(""))
					{
						state = "N/A";
					}
					
					if(postalCode==null || postalCode.equalsIgnoreCase(""))
					{
						postalCode = "N/A";
					}
					
					description = jobOrder.getJobDescription();
					if(description==null || description.equalsIgnoreCase(""))
					{	
						description = "N/A";
					
					}
				
					//System.out.println("jobTitle : "+jobTitle);
					//System.out.println("referenceNumber : "+jobId);
					//System.out.println("description : "+description);
					//System.out.println("category : "+category);
					//System.out.println("company : "+company);
					//System.out.println("city : "+city);
					//System.out.println("state : "+state);
					//System.out.println("postalCode : "+postalCode);
					
					Node jobNode = createJobNode(document,jobTitle,jobPostingDate,jobId,company,city,state,postalCode,country,description,category,url);
				    root.appendChild(jobNode);
				  }
			}	
			
		    try {
		        document.setXmlStandalone(true);
		        Source xmlSource = new DOMSource(document);
		      
		        Result result = new StreamResult(outStream);
		        TransformerFactory transformerFactory = TransformerFactory.newInstance();
		        Transformer transformer = transformerFactory.newTransformer();
		        transformer.setOutputProperty("indent", "no");
		        transformer.transform(xmlSource, result);
		        
		        writeFeedXMLONServer();
		      }
		    catch(TransformerFactoryConfigurationError factoryError) {
		          System.err.println("Error creating " + "TransformerFactory");
		          factoryError.printStackTrace();
		        }catch (TransformerException transformerError) {
		          System.err.println("Error transforming document");
		          transformerError.printStackTrace();
		        }
		}catch(Exception e){e.printStackTrace();}
	}

  public static Node createJobNode(Document document,String title,String date,String jobId,String jobCompany,String jobCity,String jobState, String pstlCode,String jobCountry,String jobDescription,String jobCategory, String jobUrl) {

		   // System.out.println("*********inside createJobNode***********");
		    Element jobTitle = document.createElement("title");
		    Element jobPostingDate = document.createElement("date");
		    
		    Element jobReferenceNumber = document.createElement("referencenumber");
		    Element company = document.createElement("company");
		    
		    Element city = document.createElement("city");
		    Element state = document.createElement("state");
		    
		    Element postalCode = document.createElement("postalcode");
		    Element country = document.createElement("country");
		    
		    Element description = document.createElement("description");
		    Element category = document.createElement("category");
		    Element url = document.createElement("url");
		    
		    jobTitle.appendChild(document.createCDATASection(title));
		    jobPostingDate.appendChild(document.createCDATASection(date));
		    
		    jobReferenceNumber.appendChild(document.createCDATASection(jobId));
		    company.appendChild(document.createCDATASection(jobCompany));
		    
		    city.appendChild(document.createCDATASection(jobCity));
		    state.appendChild(document.createCDATASection(jobState));
		    
		    postalCode.appendChild(document.createCDATASection(pstlCode));
		    country.appendChild(document.createCDATASection(jobCountry));
		    
		    description.appendChild(document.createCDATASection(jobDescription));
		    category.appendChild(document.createCDATASection(jobCategory));
		    url.appendChild(document.createCDATASection(jobUrl));
		    
		    Element job = document.createElement("job");

		    job.appendChild(jobTitle);
		    job.appendChild(jobPostingDate);
		    
		    job.appendChild(jobReferenceNumber);
		    job.appendChild(company);
		    
		    job.appendChild(city);
		    job.appendChild(state);
		    
		    
		    job.appendChild(postalCode);
		    job.appendChild(country);
		    
		    job.appendChild(description);
		    job.appendChild(category);
		    job.appendChild(url);

		  // System.out.println("*********end createJobNode***********"+job);
		    return job;
		  }

   public void writeFeedXMLONServer(){
	  System.out.println("************writing  Feed.xml on server inside writeFeedXMLONServer block*******************");
	    
	     String path="";
	   
	     String source = Utility.getValueOfPropByKey("rootPath")+"jobfeed/feed.xml";
		 System.out.println("source Path "+source);

		 String target = servletContext.getRealPath("/")+"/"+"/jobfeed/";
		 System.out.println("Real target Path "+target);

		File sourceFile = new File(source);
		File targetDir = new File(target);
		
		if(!targetDir.exists())
			targetDir.mkdirs();

		File targetFile = new File(targetDir+"/"+sourceFile.getName());

		try {
			FileUtils.copyFile(sourceFile, targetFile);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		path = Utility.getValueOfPropByKey("contextBasePath")+"/jobfeed/feed.xml";
		System.out.println("feed.xml contextBasePath "+path);
	  
	}
}
