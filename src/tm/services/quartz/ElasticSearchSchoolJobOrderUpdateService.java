package tm.services.quartz;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.services.es.ElasticSearchService;

public class ElasticSearchSchoolJobOrderUpdateService implements Job
{
	JobOrderDAO jobOrderDAO = null;
	JobRequisitionNumbersDAO jobRequisitionNumbersDAO=null;
	ApplicationContext springContext=null;
	ServletContext servletContext = null;
	private static int count=0;
	private static int size=0;
	
	@Transactional
	public void execute(JobExecutionContext arg0) throws JobExecutionException
	{
		long l=new Date().getTime();
		
		
		if(count==0)
		{
			
			try
			{
				count=1;
				springContext = WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
				jobOrderDAO = (JobOrderDAO) springContext.getBean("jobOrderDAO");
				jobRequisitionNumbersDAO=(JobRequisitionNumbersDAO)springContext.getBean("jobRequisitionNumbersDAO");
				ElasticSearchService es=new ElasticSearchService();
				//System.out.println("schedular for ElasticSearchSchoolJobOrderUpdateService start size="+size);
				Map<Integer, String> dataBaseMap=es.getDBDataForSchoolJobOrder(jobOrderDAO,jobRequisitionNumbersDAO,size);
				if(dataBaseMap==null || dataBaseMap.isEmpty())
				{
					size=0;
				}
				else
				{
					//Map<Integer,String> elasticMap=es.esForSchoolJobOrder();
					Set<Integer> jobId=dataBaseMap.keySet();
					Map<Integer,String> elasticMap=es.esForSchoolJobOrderNew(StringUtils.join(jobId, " "));
					if(dataBaseMap!=null && dataBaseMap.size()>0)
						es.updateESForSchoolJobOrder(dataBaseMap, elasticMap);
					size=size+1000;
				}
				count=0;
				//System.out.println("schedular for ElasticSearchSchoolJobOrderUpdateService end");
				//System.out.println("/esearch/updateESForSchoolJobOrder.do end");
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				//System.out.println("*****************IN FINALLY***********************");
				count=0;
			}
			
		}
		else
		{
			System.out.println("**********************OTHEHR PROCESS IS RUNNING************************");
		}
		
		
		
		
		
		
		//for candidate
		/*ElasticSearchService es1=new ElasticSearchService();
		System.out.println("schedular for updateESForJobsOfInterestForCandidate.do start");
		Map<Integer, String> dataBaseMap1=es1.getDBDataForJobsOfInterestForCandidate(jobOrderDAO);
		Map<Integer,String> elasticMap1=es1.esForJobsOfInterestForCandidateAll();
		es1.updateESForJobsOfInterestForCandidate(dataBaseMap1, elasticMap1);*/
			
		//System.out.println("schedular for updateESForJobsOfInterestForCandidate.do end");
		
		
	}


}
