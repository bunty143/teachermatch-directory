package tm.services;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.utility.Utility;


public class CommonUtilityAjax {

	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	@Autowired 
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	
	
	public String getJobs(DistrictMaster districtMaster,SchoolMaster schoolMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer sb = new StringBuffer();
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			//DistrictMaster districtMaster=null;
			//SchoolMaster schoolMaster=null;
			SubjectMaster subjectMaster	=	null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			List<JobOrder> jobOrders = new ArrayList<JobOrder>();
			if(entityID==3)
			{
				jobOrders = schoolInJobOrderDAO.findAllJobBySchool(schoolMaster);
			}else if(entityID==2)
				jobOrders = jobOrderDAO.findAllJobByDistrict(districtMaster,null);
			else
				jobOrders = jobOrderDAO.findAll();
			
			sb.append("<table id='jobTableCommon' width='100%' border='0' class='table table-striped'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th width='10px' valign='top'>"+Utility.getLocaleValuePropByKey("msgJobIdJobCode", locale)+"</th>");
			sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblJoTil", locale)+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			for (JobOrder jobOrder : jobOrders) {
				sb.append("<tr>");
				sb.append("<td style='text-align: left; vertical-align: middle;width:8px;'>"+jobOrder.getJobId()+"</td>");
				sb.append("<td style='text-align: left; vertical-align: middle;'>"+jobOrder.getJobTitle()+"</td>");
				sb.append("</tr>");
			}
			if(jobOrders.size()==0)
			{
				sb.append("<tr><td colspan='13' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;line-height:30px;'>");
				sb.append(Utility.getLocaleValuePropByKey("lblNoRecord", locale));
				sb.append("</td></tr>");
			}
			sb.append("</table>");
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}
