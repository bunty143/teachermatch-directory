package tm.services;


import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.utility.Utility;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class WeCanExportAjax 
{
	  String locale = Utility.getValueOfPropByKey("locale");
	 
	
	@Autowired
	private RawDataForDomainDAO rawDataForDomainDAO;
	
	public String displayWeCanExportData(String pageNo, String noOfRow,String sortOrder,String sortOrderType,String startDate,String endDate,Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		DistrictMaster districtMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		if(session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		
		String responseText="";
		StringBuilder tmRecords = new StringBuilder();
		int sortingcheck=0;
		int searchDistrictId = 0;
		
		
		try
		{
			if(userMaster.getDistrictId()!=null)
				searchDistrictId = userMaster.getDistrictId().getDistrictId();
			else
			{
				if(districtId!=null)
					searchDistrictId = (int)districtId;
			}
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			int totalRecords=0;
			//------------------------------------
			
			String  sortOrderStrVal		=	null;
			String sortOrderFieldName	=	"lastName";
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
			}
			
							
							List lstRawData = new ArrayList();
							
							try
							{
																					
								lstRawData = rawDataForDomainDAO.findTeacherEpiNormScoreForReport(start, noOfRowInPage, sortOrderStrVal, true,sortOrderFieldName,startDate,endDate,searchDistrictId);
								totalRecord = rawDataForDomainDAO.findTeacherEpiNormScoreForReportCount(startDate,endDate,searchDistrictId);
								
								System.out.println(" >>>>>>>> lstRawData :: "+lstRawData.size());
								
								if(lstRawData.size()>0)
								{
									for(Object oo:lstRawData)
									{
										Object obj[] = (Object[])oo;
										String s1= (String) obj[0];
										String s2= (String) obj[1];
										String s3= (String) obj[2];
										String s4= (String) obj[3];
										//Long s5 = (Long) obj[4];
										int s5 = Integer.parseInt(String.valueOf(obj[4]));
										System.out.println(s1+"--"+s2+"--"+s3+"--"+s4+"--"+s5);
									}
								}
							}catch(Exception e)
							{
								e.printStackTrace();
							}
							
						
						tmRecords.append("<table  id='tblGridEEC' width='100%' border='0'>");
						tmRecords.append("<thead class='bg'>");
						tmRecords.append("<tr>");
						
						responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCandidateLastName", locale),sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
						tmRecords.append("<th  valign='top'>"+responseText+"</th>");
		        
						responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCandidateFirstName", locale),sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo);
						tmRecords.append("<th valign='top'>"+responseText+"</th>");
						
						responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgTEACHEREMAIL", locale),sortOrderFieldName,"emailAddress",sortOrderTypeVal,pgNo);
						tmRecords.append("<th  valign='top'>"+responseText+"</th>");
						
										
						responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgEPIDATE", locale),sortOrderFieldName,"epiDate",sortOrderTypeVal,pgNo);
						tmRecords.append("<th  valign='top'>"+responseText+"</th>");
						
						responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgEPINORMSCORE", locale),sortOrderFieldName,"normScore",sortOrderTypeVal,pgNo);
						tmRecords.append("<th valign='top'>"+responseText+"</th>");
						tmRecords.append("</tr>");
						tmRecords.append("</thead>");
						tmRecords.append("<tbody>");
						
						if(totalRecord<end)
							end=totalRecord;
						
						if(lstRawData.size()==0)
						{
							tmRecords.append("<tr>");
							tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td>");
							tmRecords.append("</tr>");
						}
						
						if(lstRawData.size()>0)
						{
							for(Object oo:lstRawData)
							{
								Object obj[] = (Object[])oo;
								String lastName = "";
								String firstname = "";
								String email = "";
								String epiDate = "";
								int normScore = 0;
								
								if(obj[0]!=null)
									lastName = (String) obj[0];
								
								if(obj[1]!=null)
									firstname = (String) obj[1];;
									
								if(obj[2]!=null)
									email = (String) obj[2];
										
								if(obj[3]!=null)
									epiDate = (String) obj[3];
											
								if(obj[4]!=null)
									normScore= Integer.parseInt(String.valueOf(obj[4]));
								
								tmRecords.append("<tr>");
								tmRecords.append("<td>"+lastName+"</td>");
								tmRecords.append("<td>"+firstname+"</td>");
								tmRecords.append("<td>"+email+"</td>");
								if(obj[3]!=null)
								tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(new Date(epiDate))+"</td>");
								else
								tmRecords.append("<td>"+"NA"+"</td>");	
								tmRecords.append("<td>"+normScore+"</td>");
								tmRecords.append("</tr>");
							}
						}
						tmRecords.append("</tbody>");
						tmRecords.append("</table>");
						tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
						
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return tmRecords.toString();
	}
	
	
	
	public String displayWeCanExportDataEXL(String pageNo, String noOfRow,String sortOrder,String sortOrderType,String startDate,String endDate,Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		DistrictMaster districtMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		if(session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		
		String responseText="";
		StringBuilder tmRecords = new StringBuilder();
		int sortingcheck=0;
		String fileName=null;
		int searchDistrictId = 0;
		
		try
		{
			if(userMaster.getDistrictId()!=null)
				searchDistrictId = userMaster.getDistrictId().getDistrictId();
			else
			{
				if(districtId!=null)
					searchDistrictId = (int)districtId;
			}
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			int totalRecords=0;
			//------------------------------------
			
			String  sortOrderStrVal		=	null;
			String sortOrderFieldName	=	"lastName";
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
			}
			
							
							List lstRawData = new ArrayList();
							
							lstRawData = rawDataForDomainDAO.findTeacherEpiNormScoreForReport(start, noOfRowInPage, sortOrderStrVal, false,sortOrderFieldName,startDate,endDate,searchDistrictId);
							totalRecord = rawDataForDomainDAO.findTeacherEpiNormScoreForReportCount(startDate,endDate,searchDistrictId);
							
							System.out.println(" >>>>>>>> lstRawData :: "+lstRawData.size());
							
							///////////////////////////////////////////////////////////////////////////////////////////////////////
						//  Excel   Exporting	
							
							String time = String.valueOf(System.currentTimeMillis()).substring(6);
							//String basePath = request.getRealPath("/")+"/candidate";
							String basePath = request.getSession().getServletContext().getRealPath ("/")+"/wecanexport";
							System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
							fileName ="wecanexport"+time+".xls";

							File file = new File(basePath);
							if(!file.exists())
								file.mkdirs();

							Utility.deleteAllFileFromDir(basePath);

							file = new File(basePath+"/"+fileName);
							WorkbookSettings wbSettings = new WorkbookSettings();

							wbSettings.setLocale(new Locale("en", "EN"));
							WritableCellFormat timesBoldUnderline;
							WritableCellFormat header;
							WritableCellFormat headerBold;
							WritableCellFormat times;
							WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
							workbook.createSheet(" wecanexport", 0);
							WritableSheet excelSheet = workbook.getSheet(0);

							WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
							// Define the cell format
							times = new WritableCellFormat(times10pt);
							// Lets automatically wrap the cells
							times.setWrap(true);
							// Create create a bold font with unterlines
							WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
							WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);
							timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
							timesBoldUnderline.setAlignment(Alignment.CENTRE);
							// Lets automatically wrap the cells
							timesBoldUnderline.setWrap(true);

							header = new WritableCellFormat(times10ptBoldUnderline);
							headerBold = new WritableCellFormat(times10ptBoldUnderline);
							CellView cv = new CellView();
							cv.setFormat(times);
							cv.setFormat(timesBoldUnderline);
							cv.setAutosize(true);

							header.setBackground(Colour.GRAY_25);
							// Write a few headers
							//excelSheet.mergeCells(0, 0, 4, 1);
							Label label;
							
						//	label = new Label(0, 0, " WECAN Export ", timesBoldUnderline);
						//	excelSheet.addCell(label);
							
						//	excelSheet.mergeCells(0, 3,15, 3);
						//	label = new Label(0, 3, "");
						//	excelSheet.addCell(label);
							
							excelSheet.getSettings().setDefaultColumnWidth(18);
							
							int k=0;
							int col=1;
							label = new Label(0, k, Utility.getLocaleValuePropByKey("lblCandidateLastName", locale),header); 
							excelSheet.addCell(label);
							label = new Label(1, k, Utility.getLocaleValuePropByKey("lblCandidateFirstName", locale),header); 
							excelSheet.addCell(label);
							label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgTEACHEREMAIL", locale),header); 
							excelSheet.addCell(label);
							
							label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgEPIDATE", locale),header); 
							excelSheet.addCell(label);
							label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgEPINORMSCORE", locale),header); 
							excelSheet.addCell(label);
							
							k=k+1;
							if(lstRawData.size()==0)
							{	
								excelSheet.mergeCells(0, k, 4, k);
								label = new Label(0, k, Utility.getLocaleValuePropByKey("lblNoRecord", locale)); 
								excelSheet.addCell(label);
							}
							HashMap<String, WritableCellFormat> mapColors= new HashMap<String, WritableCellFormat>();
							
							
							/*if (lstRawData.size()>0) {
								WritableCellFormat cellFormat = new WritableCellFormat();
								Color color = Color.decode("0xFF0000");
								int red = color.getRed();
								int blue = color.getBlue();
								int green = color.getGreen();
								workbook.setColourRGB(Colour.AQUA, red, green, blue);
								cellFormat.setBackground(Colour.AQUA);
								mapColors.put("FF0000", cellFormat);
								WritableCellFormat cellFormat1 = new WritableCellFormat();
								Color color1 = Color.decode("0xFF6666");
								workbook.setColourRGB(Colour.BLUE, color1.getRed(), color1
										.getGreen(), color1.getBlue());
								cellFormat1.setBackground(Colour.BLUE);
								mapColors.put("FF6666", cellFormat1);
								WritableCellFormat cellFormat2 = new WritableCellFormat();
								Color color2 = Color.decode("0xFFCCCC");
								workbook.setColourRGB(Colour.BROWN, color2.getRed(), color2
										.getGreen(), color2.getBlue());
								cellFormat2.setBackground(Colour.BROWN);
								mapColors.put("FFCCCC", cellFormat2);
								WritableCellFormat cellFormat3 = new WritableCellFormat();
								Color color3 = Color.decode("0xFF9933");
								workbook.setColourRGB(Colour.CORAL, color3.getRed(), color3
										.getGreen(), color3.getBlue());
								cellFormat3.setBackground(Colour.CORAL);
								mapColors.put("FF9933", cellFormat3);
								WritableCellFormat cellFormat4 = new WritableCellFormat();
								Color color4 = Color.decode("0xFFFFCC");
								workbook.setColourRGB(Colour.GREEN, color4.getRed(), color4
										.getGreen(), color4.getBlue());
								cellFormat4.setBackground(Colour.GREEN);
								mapColors.put("FFFFCC", cellFormat4);
								WritableCellFormat cellFormat5 = new WritableCellFormat();
								Color color5 = Color.decode("0xFFFF00");
								workbook.setColourRGB(Colour.INDIGO, color5.getRed(), color5
										.getGreen(), color5.getBlue());
								cellFormat5.setBackground(Colour.INDIGO);
								mapColors.put("FFFF00", cellFormat5);
								WritableCellFormat cellFormat6 = new WritableCellFormat();
								Color color6 = Color.decode("0x66CC66");
								workbook.setColourRGB(Colour.LAVENDER, color6.getRed(), color6
										.getGreen(), color6.getBlue());
								cellFormat6.setBackground(Colour.LAVENDER);
								mapColors.put("66CC66", cellFormat6);
								WritableCellFormat cellFormat7 = new WritableCellFormat();
								Color color7 = Color.decode("0x00FF00");
								workbook.setColourRGB(Colour.LIME, color7.getRed(), color7
										.getGreen(), color7.getBlue());
								cellFormat7.setBackground(Colour.LIME);
								mapColors.put("00FF00", cellFormat7);
								WritableCellFormat cellFormat8 = new WritableCellFormat();
								Color color8 = Color.decode("0x006633");
								workbook.setColourRGB(Colour.ORANGE, color8.getRed(), color8
										.getGreen(), color8.getBlue());
								cellFormat8.setBackground(Colour.ORANGE);
								mapColors.put("006633", cellFormat8);
							}*/
							
							if(lstRawData.size()==0)
							{
								tmRecords.append("<tr>");
								tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td>");
								tmRecords.append("</tr>");
							}
							
							
							if(lstRawData.size()>0){
							 for(Object oo:lstRawData) 
							 {
								 	Object obj[] = (Object[])oo;
									String lastName = "";
									String firstname = "";
									String email = "";
									String epiDate = "";
									int normScore = 0;
									
									if(obj[0]!=null)
										lastName = (String) obj[0];
									
									if(obj[1]!=null)
										firstname = (String) obj[1];;
										
									if(obj[2]!=null)
										email = (String) obj[2];
											
									if(obj[3]!=null)
										epiDate = (String) obj[3];
												
									if(obj[4]!=null)
										normScore= Integer.parseInt(String.valueOf(obj[4]));
								 
								 col=1;
								// String name=jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"("+jft.getTeacherId().getEmailAddress()+")";
								 label = new Label(0, k, lastName); 
								 excelSheet.addCell(label);
								 
								 label = new Label(1, k, firstname); 
								 excelSheet.addCell(label);
								 
								 label = new Label(++col, k, email); 
								 excelSheet.addCell(label);
								 if(obj[3]!=null)
								 label = new Label(++col, k, Utility.convertDateAndTimeToUSformatOnlyDate(new Date(epiDate)));//+",12:01 AM");
								 else
								 label = new Label(++col, k, "NA");
								 excelSheet.addCell(label);
								
								 label = new Label(++col, k, normScore+""); 
								 excelSheet.addCell(label);
							  ++k;
							 }
							}
						workbook.write();
					workbook.close();
				 return fileName;
							////////////////////////////////////////////////////////////////////////////////////////////////////////
						
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return tmRecords.toString();
	}
	

	public String displayWeCanExportDataPDF(String pageNo, String noOfRow,String sortOrder,String sortOrderType,String startDate,String endDate,Integer districtId)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}


		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport(pageNo,noOfRow,sortOrder,sortOrderType,basePath+"/"+fileName,context.getServletContext().getRealPath("/"),startDate,endDate,districtId);
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}
	
	public boolean generateCandidatePDReport(String pageNo, String noOfRow,String sortOrder,String sortOrderType,String path,String realPath,String startDate,String endDate,Integer districtId)
	{
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		Font font8 = null;
		Font font8Green = null;
		Font font8bold = null;
		Font font9 = null;
		Font font9bold = null;
		Font font10 = null;
		Font font10_10 = null;
		Font font10bold = null;
		Font font11 = null;
		Font font11bold = null;
		Font font20bold = null;
		Font font11b   =null;
		Color bluecolor =null;
		Font font8bold_new = null;
		
		//System.out.println(sortOrderType+" ::@@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		DistrictMaster districtMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		
		if (session == null || session.getAttribute("userMaster") == null) {
			return false;
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			
			int searchDistrictId = 0;	
			
		try
		{
			if(userMaster.getDistrictId()!=null)
				searchDistrictId = userMaster.getDistrictId().getDistrictId();
			else
			{
				if(districtId!=null)
					searchDistrictId = (int)districtId;
			}
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			int totalRecords=0;
			//------------------------------------
			
			String  sortOrderStrVal		=	null;
			String sortOrderFieldName	=	"lastName";
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
			}
			
							
							List lstRawData = new ArrayList();
							
							lstRawData = rawDataForDomainDAO.findTeacherEpiNormScoreForReport(start, noOfRowInPage, sortOrderStrVal, false,sortOrderFieldName,startDate,endDate,searchDistrictId);
							totalRecord = rawDataForDomainDAO.findTeacherEpiNormScoreForReportCount(startDate,endDate,searchDistrictId);
			
			
					//#########################################################################################################
					  String fontPath = realPath;
						 	try {
								
								BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
								BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
								font8 = new Font(tahoma, 8);
		
								font8Green = new Font(tahoma, 8);
								font8Green.setColor(Color.BLUE);
								font8bold = new Font(tahoma, 8, Font.NORMAL);
								
								font8bold_new = new Font(tahoma, 7, Font.NORMAL);
								
		
								font9 = new Font(tahoma, 9);
								font9bold = new Font(tahoma, 9, Font.BOLD);
								font10 = new Font(tahoma, 10);
								
								font10_10 = new Font(tahoma, 8);
								font10_10.setColor(Color.white);
								//font10.setColor(Color.white);
								font10bold = new Font(tahoma, 10, Font.BOLD);
								//font10bold.setColor(Color.white);
								font11 = new Font(tahoma, 11);
								font11bold = new Font(tahoma, 11,Font.BOLD);
								
								
								 bluecolor =  new Color(0,122,180); 
								
								font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
								//font20bold.setColor(Color.BLUE);
								font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);
		
		
							} 
							catch (DocumentException e1) 
							{
								e1.printStackTrace();
							} 
							catch (IOException e1) 
							{
								e1.printStackTrace();
							}
							
							document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
							document.addAuthor("TeacherMatch");
							document.addCreator("TeacherMatch Inc.");
							document.addSubject("WECAN Export ");
							document.addCreationDate();
							document.addTitle("WECAN Export ");
		
							fos = new FileOutputStream(path);
							PdfWriter.getInstance(document, fos);
							
							document.open();
							
							PdfPTable mainTable = new PdfPTable(1);
							mainTable.setWidthPercentage(100);
		
							Paragraph [] para = null;
							PdfPCell [] cell = null;
		
		
							para = new Paragraph[3];
							cell = new PdfPCell[3];
							
							//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
							Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
							logo.scalePercent(75);
							
							document.add(new Phrase("\n"));
							//para[0] = new Paragraph(" ",font20bold);
							cell[0]= new PdfPCell(logo);
							cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
							cell[0].setBorder(0);
							mainTable.addCell(cell[0]);
							
							
		
							//para[1] = new Paragraph(" ",font20bold);
							cell[1]= new PdfPCell(para[1]);
							cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
							cell[1].setBorder(0);
							mainTable.addCell(cell[1]);
		
							document.add(new Phrase("\n"));
							document.add(new Phrase("\n"));
							document.add(new Phrase("\n"));
							
							
		
							document.add(mainTable);
							
							document.add(new Phrase("\n"));
							//document.add(new Phrase("\n"));
							
		
							float[] tblwidthz={.15f};
							
							mainTable = new PdfPTable(tblwidthz);
							mainTable.setWidthPercentage(100);
							para = new Paragraph[1];
							cell = new PdfPCell[1];
		
							
							para[0] = new Paragraph("WECAN Export ",font20bold);
							cell[0]= new PdfPCell(para[0]);
							cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
							cell[0].setBorder(0);
							mainTable.addCell(cell[0]);
							document.add(mainTable);
		
							document.add(new Phrase("\n"));
							
							
					        float[] tblwidths={.15f,.20f};
							
							mainTable = new PdfPTable(tblwidths);
							mainTable.setWidthPercentage(100);
							para = new Paragraph[2];
							cell = new PdfPCell[2];
		
							String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
							
							para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
							cell[0]= new PdfPCell(para[0]);
							cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
							cell[0].setBorder(0);
							mainTable.addCell(cell[0]);
							
							para[1] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
							cell[1]= new PdfPCell(para[1]);
							cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell[1].setBorder(0);
							mainTable.addCell(cell[1]);
							
							document.add(mainTable);
							
							
							
							//document.add(new Phrase("\n"));
							//document.add(new Phrase("\n"));
							//document.add(new Phrase("\n")); 	
							document.add(Chunk.NEWLINE);
							
							float[] tblwidth={.60f,.60f,1.0f,.30f,.30f};
							
							mainTable = new PdfPTable(tblwidth);
							mainTable.setWidthPercentage(100);
							para = new Paragraph[16];
							cell = new PdfPCell[16];
					// header
							para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblCandidateLastName", locale),font10_10);
							cell[0]= new PdfPCell(para[0]);
							cell[0].setBackgroundColor(bluecolor);
							cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
							//cell[0].setBorder(1);
							mainTable.addCell(cell[0]);
							
							para[1] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCandidateFirstName", locale),font10_10);
							cell[1]= new PdfPCell(para[1]);
							cell[1].setBackgroundColor(bluecolor);
							cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
							//cell[1].setBorder(1);
							mainTable.addCell(cell[1]);
							
							para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgTEACHEREMAIL", locale),font10_10);
							cell[2]= new PdfPCell(para[2]);
							cell[2].setBackgroundColor(bluecolor);
							cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
							//cell[1].setBorder(1);
							mainTable.addCell(cell[2]);
							
							para[3] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgEPIDATE", locale),font10_10);
							cell[3]= new PdfPCell(para[3]);
							cell[3].setBackgroundColor(bluecolor);
							cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
							//cell[1].setBorder(1);
							mainTable.addCell(cell[3]);
							
							para[4] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgEPINORMSCORE", locale),font10_10);
							cell[4]= new PdfPCell(para[4]);
							cell[4].setBackgroundColor(bluecolor);
							cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
							//cell[1].setBorder(1);
							mainTable.addCell(cell[4]);
							
							
							
						
							
							document.add(mainTable);
						
							if(lstRawData.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblNoRecord", locale),font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 //cell[index].setBorder(1);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
							
							
						 	 
					if(lstRawData.size()>0){
					 for(Object oo : lstRawData){
						 Object obj[] = (Object[])oo;
						 String lastName = "";
						 String firstname = "";
						 String email = "";
						 String epiDate = "";
						 int normScore = 0;

						 if(obj[0]!=null)
						 	lastName = (String) obj[0];

						 if(obj[1]!=null)
						 	firstname = (String) obj[1];;
						 	
						 if(obj[2]!=null)
						 	email = (String) obj[2];
						 		
						 if(obj[3]!=null)
						 	epiDate = (String) obj[3];
						 			
						 if(obj[4]!=null)
						 	normScore= Integer.parseInt(String.valueOf(obj[4]));
						 
						 
						 int index=0;
						 float[] tblwidth1={.60f,.60f,1.0f,.30f,.30f};
							
						mainTable = new PdfPTable(tblwidth1);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[16];
						cell = new PdfPCell[16];
						
						 //Last Name
						 para[index] = new Paragraph(lastName,font8bold_new);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
				
						//First Name
						 para[index] = new Paragraph(firstname,font8bold_new);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
						 
						 //email Address
						 para[index] = new Paragraph(email,font8bold_new);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
						 if(obj[3]!=null){
						 para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(new Date(epiDate)),font8bold_new);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
						 }else
						 {
							 para[index] = new Paragraph("NA",font8bold_new);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
						 }

						 para[index] = new Paragraph(""+normScore,font8bold_new);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
						 
						 document.add(mainTable);
						 
						
					}
					}
				
				}catch(Exception e){e.printStackTrace();}
				finally
				{
					if(document != null && document.isOpen())
						document.close();
					if(writer!=null){
						writer.flush();
						writer.close();
					}
				}
		}
				return true;
		/*}catch(Exception e)
		{
			e.printStackTrace();
		}*/
	}
	
	
	public String displayWeCanExportDataPrintPreview(String pageNo, String noOfRow,String sortOrder,String sortOrderType,String startDate,String endDate,Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		DistrictMaster districtMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		if(session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		
		String responseText="";
		StringBuilder tmRecords = new StringBuilder();
		int sortingcheck=0;
		int searchDistrictId = 0;
		
		try
		{
			if(userMaster.getDistrictId()!=null)
				searchDistrictId = userMaster.getDistrictId().getDistrictId();
			else
			{
				if(districtId!=null)
					searchDistrictId = (int)districtId;
			}
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			int totalRecords=0;
			//------------------------------------
			
			String  sortOrderStrVal		=	null;
			String sortOrderFieldName	=	"lastName";
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
			}
			
							
							List lstRawData = new ArrayList();
							
							try
							{
								
								lstRawData = rawDataForDomainDAO.findTeacherEpiNormScoreForReport(start, noOfRowInPage, sortOrderStrVal, false,sortOrderFieldName,startDate,endDate,searchDistrictId);
								totalRecord = rawDataForDomainDAO.findTeacherEpiNormScoreForReportCount(startDate,endDate,searchDistrictId);
								
								System.out.println(" >>>>>>>> lstRawData :: "+lstRawData.size());
								if(lstRawData.size()>0)
								{
									for(Object oo:lstRawData)
									{
										Object obj[] = (Object[])oo;
										String s1= (String) obj[0];
										String s2= (String) obj[1];
										String s3= (String) obj[2];
										String s4= (String) obj[3];
										//Long s5 = (Long) obj[4];
										int s5 = Integer.parseInt(String.valueOf(obj[4]));
										System.out.println(s1+"--"+s2+"--"+s3+"--"+s4+"--"+s5);
									}
								}
							}catch(Exception e)
							{
								e.printStackTrace();
							}
							
						tmRecords.append("<div style='text-align: center; font-size: 25px; text-decoration: underline; font-weight:bold;'>"+Utility.getLocaleValuePropByKey("msgWECANExport", locale)+"</div><br/>");
						
						tmRecords.append("<div style='width:100%'>");
						tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
						tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
						tmRecords.append("<br/><br/>");
						tmRecords.append("</div>");
							
							
						tmRecords.append("<table  id='tblGridEECPrint' width='100%' border='0'>");
						tmRecords.append("<thead class='bg'>");
						tmRecords.append("<tr>");
						
					//	responseText=PaginationAndSorting.responseSortingLink("Candidate Last Name",sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
						tmRecords.append("<th  style='text-align:left;font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblCandidateLastName", locale)+"</th>");
		        
					//	responseText=PaginationAndSorting.responseSortingLink("Candidate First Name",sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo);
						tmRecords.append("<th style='text-align:left;font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblCandidateFirstName", locale)+"</th>");
						
					//	responseText=PaginationAndSorting.responseSortingLink("Teacher Email",sortOrderFieldName,"emailAddress",sortOrderTypeVal,pgNo);
						tmRecords.append("<th  style='text-align:left;font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgTEACHEREMAIL", locale)+"</th>");
						
										
					//	responseText=PaginationAndSorting.responseSortingLink("EPI Date",sortOrderFieldName,"epiDate",sortOrderTypeVal,pgNo);
						tmRecords.append("<th  style='text-align:left;font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgEPIDATE", locale)+"</th>");
						
					//	responseText=PaginationAndSorting.responseSortingLink("EPI Norm Score",sortOrderFieldName,"normScore",sortOrderTypeVal,pgNo);
						tmRecords.append("<th style='text-align:left;font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgEPINORMSCORE", locale)+"</th>");
						tmRecords.append("</tr>");
						tmRecords.append("</thead>");
						tmRecords.append("<tbody>");
						
						if(totalRecord<end)
							end=totalRecord;
						
						if(lstRawData.size()==0)
						{
							tmRecords.append("<tr>");
							tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td>");
							tmRecords.append("</tr>");
						}
						
						if(lstRawData.size()>0)
						{
							for(Object oo:lstRawData)
							{
								Object obj[] = (Object[])oo;
								String lastName = "";
								String firstname = "";
								String email = "";
								String epiDate = "";
								int normScore = 0;
								
								if(obj[0]!=null)
									lastName = (String) obj[0];
								
								if(obj[1]!=null)
									firstname = (String) obj[1];;
									
								if(obj[2]!=null)
									email = (String) obj[2];
										
								if(obj[3]!=null)
									epiDate = (String) obj[3];
											
								if(obj[4]!=null)
									normScore= Integer.parseInt(String.valueOf(obj[4]));
								
								tmRecords.append("<tr>");
								tmRecords.append("<td>"+lastName+"</td>");
								tmRecords.append("<td>"+firstname+"</td>");
								tmRecords.append("<td>"+email+"</td>");
								if(obj[3]!=null)
								tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(new Date(epiDate))+"</td>");
								else
									tmRecords.append("<td>"+"NA"+"</td>");	
								tmRecords.append("<td>"+normScore+"</td>");
								tmRecords.append("</tr>");
							}
						}
						tmRecords.append("</tbody>");
						tmRecords.append("</table>");
						//tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
						
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return tmRecords.toString();
	}
	
	/*public void getCSVData(String pageNo, String noOfRow,String sortOrder,String sortOrderType,String startDate,String endDate,Integer districtId)
	{
		System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>> getCSVData <<<<<<<<<<<<<<<<<<<<<<<");
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		DistrictMaster districtMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		if(session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		
//		String responseText="";
//		StringBuilder tmRecords = new StringBuilder();
//		int sortingcheck=0;
		int searchDistrictId = 0;
		String fileName=null;
		
		
		
		try
		{
			if(userMaster.getDistrictId()!=null)
				searchDistrictId = userMaster.getDistrictId().getDistrictId();
			else
			{
				if(districtId!=null)
					searchDistrictId = (int)districtId;
			}
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			int totalRecords=0;
			//------------------------------------
			
			String  sortOrderStrVal		=	null;
			String sortOrderFieldName	=	"lastName";
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
			}
			
							
			List lstRawData = new ArrayList();
			
			lstRawData = rawDataForDomainDAO.findTeacherEpiNormScoreForReport(start, noOfRowInPage, sortOrderStrVal, false,sortOrderFieldName,startDate,endDate,searchDistrictId);
			totalRecord = rawDataForDomainDAO.findTeacherEpiNormScoreForReportCount(startDate,endDate,searchDistrictId);
			
			System.out.println(" >>>>>>>> lstRawData :: "+lstRawData.size());
			
			///////////////////////////////////////////////////////////////////////////////////////////////////////
		//  Excel   Exporting	
			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			//String basePath = request.getRealPath("/")+"/candidate";
			String basePath = request.getSession().getServletContext().getRealPath ("/")+"/wecanexport";
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
			fileName ="wecanexport"+time+".csv";
			
			
			System.out.println(" basePath :: "+basePath);
			
			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			
			file = new File(basePath+"/"+fileName);
			 boolean alreadyExists = file.exists();

			 CsvWriter csvOutput = new CsvWriter(new FileWriter(fileName), ',');
			 
		
			 if (!alreadyExists)
             {
                      csvOutput.write("Candidate Last Name");
                      csvOutput.write("Candidate First Name");
                      csvOutput.write("Teacher Email");
                      csvOutput.write("EPI Date");
                      csvOutput.write("EPI Norm Score");
                      csvOutput.endRecord();
                      
                      if(lstRawData!=null && lstRawData.size()>0)
         			 {
         				 for(Object oo:lstRawData) 
         				 {
         					 	Object obj[] = (Object[])oo;
         						String lastName = "";
         						String firstname = "";
         						String email = "";
         						String epiDate = "";
         						Integer normScore = 0;
         						
         						if(obj[0]!=null)
         							lastName = (String) obj[0];
         						
         						if(obj[1]!=null)
         							firstname = (String) obj[1];;
         							
         						if(obj[2]!=null)
         							email = (String) obj[2];
         								
         						if(obj[3]!=null)
         							epiDate = (String) obj[3];
         									
         						if(obj[4]!=null)
         							normScore= Integer.parseInt(String.valueOf(obj[4]));
         						
         						csvOutput.write(lastName);
         						csvOutput.write(firstname);
         						csvOutput.write(email);
         						csvOutput.write(epiDate);
         						csvOutput.write(""+normScore);
         						
         				 }
         			 }
                      csvOutput.close();
             }
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}*/
}


