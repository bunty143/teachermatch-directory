package tm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.ReferenceCheckQuestionSet;
import tm.bean.master.ReferenceQuestionSets;
import tm.bean.user.UserMaster;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.ReferenceCheckQuestionSetDAO;
import tm.dao.master.ReferenceQuestionSetsDAO;
import tm.utility.Utility;

public class ReferenceChkQuestionsSetAjax {
	
	  String locale = Utility.getValueOfPropByKey("locale");
	  String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	  String lblQuesSet=Utility.getLocaleValuePropByKey("lblQuesSet", locale);
	  String lblcretedDate=Utility.getLocaleValuePropByKey("lblcretedDate", locale);
	  String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
		 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
		 String lblNoQuestionSetfound1=Utility.getLocaleValuePropByKey("lblNoQuestionSetfound1", locale);
		 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
		 String optAct=Utility.getLocaleValuePropByKey("optAct", locale);
		 String optInActiv=Utility.getLocaleValuePropByKey("optInActiv", locale);
		 String lblManageQuestions=Utility.getLocaleValuePropByKey("lblManageQuestions", locale);
		 
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private ReferenceQuestionSetsDAO referenceQuestionSetsDAO;

	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private ReferenceCheckQuestionSetDAO referenceCheckQuestionSetDAO;
	
	// DIsplay Questions from question pool
	public String displayQuestionSet(String noOfRow, String pageNo,String sortOrder,String sortOrderType,String i4QuesSetStatus,String quesSetSearchText,String districtIdFilter , String hqIdFilter)
	{
		
		
		System.out.println("********** HeadQuarter filter ********"+hqIdFilter);
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			boolean Statusflag 	= false;
			boolean districtFlag 	= false;
			boolean allData	   	= false;
			boolean quesTextflag= false;
			boolean hqFlag = false;
			

			DistrictMaster districtMaster = new DistrictMaster();
			HeadQuarterMaster headQuarterMaster=new HeadQuarterMaster();
			
			List<ReferenceQuestionSets> refChkQuestionSets  	=	new ArrayList<ReferenceQuestionSets>();
			List<ReferenceQuestionSets> i4QuesForStatus			=	new ArrayList<ReferenceQuestionSets>();
			List<ReferenceQuestionSets> i4QuesForQues			=	new ArrayList<ReferenceQuestionSets>();
			List<ReferenceQuestionSets> filterData				=	new ArrayList<ReferenceQuestionSets>();
			List<ReferenceQuestionSets> finalDatalist			=	new ArrayList<ReferenceQuestionSets>();
			List<ReferenceQuestionSets> filterByDistrict		=	new ArrayList<ReferenceQuestionSets>();
			List<ReferenceQuestionSets> filterByHQ		=	new ArrayList<ReferenceQuestionSets>();
			
			
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"QuestionSetText";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			
			refChkQuestionSets = referenceQuestionSetsDAO.findByCriteria(sortOrderStrVal) ;
			
			if(refChkQuestionSets.size()>0)
			{
				filterData.addAll(refChkQuestionSets);
				allData = true;
			}
			
			if(quesSetSearchText!=null && !quesSetSearchText.equalsIgnoreCase(""))
			{
				i4QuesForQues = referenceQuestionSetsDAO.findi4QuesSetByQues(quesSetSearchText);
				quesTextflag = true;
			}
			
			if(quesTextflag==true)
				filterData.retainAll(i4QuesForQues);
			else
				filterData.addAll(i4QuesForQues);
			
			if(i4QuesSetStatus!=null && !i4QuesSetStatus.equalsIgnoreCase("") && !i4QuesSetStatus.equalsIgnoreCase("0"))
			{
				i4QuesForStatus = referenceQuestionSetsDAO.findi4QuesSetByStatus(i4QuesSetStatus);
				Statusflag = true;
			}
			
			if(districtIdFilter!=null && !districtIdFilter.equalsIgnoreCase("") && !districtIdFilter.equalsIgnoreCase("0"))
			{
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtIdFilter), false, false);	
				filterByDistrict = referenceQuestionSetsDAO.findByDistrict(districtMaster);
				
				districtFlag = true;
			}
			
			
			if(hqIdFilter!=null && !hqIdFilter.equalsIgnoreCase("") && !hqIdFilter.equalsIgnoreCase("0"))
			{
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(hqIdFilter), false, false);	
				filterByHQ = referenceQuestionSetsDAO.findbyHeadQuarter(headQuarterMaster);
				
				hqFlag = true;
			}
			
			if(filterByHQ!=null){
			if(hqFlag)
				filterData.retainAll(filterByHQ);
			else
				
				filterData.addAll(filterByHQ);
			}
			
			if(districtFlag)
				filterData.retainAll(filterByDistrict);
			else
				filterData.addAll(filterByDistrict);
			
			if(Statusflag==true)
				filterData.retainAll(i4QuesForStatus);
			else
				filterData.addAll(i4QuesForStatus);
				
			
			totalRecord = filterData.size();
			if(totalRecord<end)
				end=totalRecord;
			
			System.out.println(" start :: "+start+" end "+end);
			
			finalDatalist	=	filterData.subList(start,end);
			
			dmRecords.append("<table  id='refChkQuesTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			//dmRecords.append("<th width='55%'>Domain Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblQuesSet,sortOrderFieldName,"QuestionSetText",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='55%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblcretedDate,sortOrderFieldName,"DateCreated",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"Status",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			dmRecords.append("<th width='15%'><span style='color:#ffffff'>"+lblAct+"</span></th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(finalDatalist.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+lblNoQuestionSetfound1+"</td></tr>" );
			System.out.println("No of Records "+finalDatalist.size());

			for (ReferenceQuestionSets i4qp: finalDatalist) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+i4qp.getQuestionSetText()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(i4qp.getDateCreated())+"</td>");
				dmRecords.append("<td>");
				if(i4qp.getStatus().equalsIgnoreCase("A"))
					dmRecords.append(optAct);
				else
					dmRecords.append(optInActiv);
				dmRecords.append("</td>");
				
				if(i4qp.getStatus().equalsIgnoreCase("A"))
					dmRecords.append("<td><a href='javascript:void(0);' onclick='editI4Question("+i4qp.getID()+")'>"+lblEdit+"</a> | <a href='javascript:void(0);' onclick=\"activateDeactivateQues("+i4qp.getID()+",'I')\">Deactivate</a> | <a href='refChkquestionSetQuestion.do?quesSetId="+i4qp.getID()+"&districtId="+((i4qp.getDistrictMaster()!=null)?(i4qp.getDistrictMaster().getDistrictId()):(""))+"&headQuarterId="+((i4qp.getHeadQuarterId()!=null)?(i4qp.getHeadQuarterId()):(""))+"'>"+lblManageQuestions+"</a></td>");
				else if(i4qp.getStatus().equalsIgnoreCase("I"))
					dmRecords.append("<td><a href='javascript:void(0);' onclick='editI4Question("+i4qp.getID()+")'>"+lblEdit+"</a> | <a href='javascript:void(0);' onclick=\"activateDeactivateQues("+i4qp.getID()+",'A')\">Activate</a> | <a href='refChkquestionSetQuestion.do?quesSetId="+i4qp.getID()+"&districtId="+((i4qp.getDistrictMaster()!=null)?(i4qp.getDistrictMaster().getDistrictId()):(""))+"&headQuarterId="+((i4qp.getHeadQuarterId()!=null)?(i4qp.getHeadQuarterId()):(""))+"'>"+lblManageQuestions+"</a></td>");
			
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	public int saveRefChkQuestionSet(Integer quesSetId,String quesSetName,String quesSetStatus,String districtId, String headQuarterId)
	{
		System.out.println(" ==========saveI4Question ========");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		int res = 0;
		boolean chkDup = false;
		
		try
		{
			UserMaster userMaster = new UserMaster();
			
			if (session.getAttribute("userMaster") != null) 
			{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
			}
			
			DistrictMaster districtMaster = null;
			HeadQuarterMaster headQuarterMaster=null;
			
			if (session.getAttribute("userMaster") != null) 
				userMaster=	(UserMaster) session.getAttribute("userMaster");
			
			if(userMaster.getEntityType()==1)
			{
				if(districtId!=null && !districtId.equals(""))
					districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			}
			else {
				if(userMaster.getEntityType()==2)
			{
				districtMaster = userMaster.getDistrictId();
			}
				else{
					if(userMaster.getEntityType()==5){
						headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);
					}
				}
			}
			
			System.out.println("-----------------"+headQuarterId);
			
			if(quesSetId==null){
				if(districtId!="" && districtId !=null){
				if(checkDuplicateQuesSet(quesSetName, districtMaster))
					chkDup = true;
				}
				
				if(headQuarterId!="" && headQuarterId !=null){
					if(checkDuplicateQuesSetByHQ(quesSetName, headQuarterMaster))
						chkDup = true;
					}
					
				
			}
			
			
			if(chkDup==false)
			{
				ReferenceQuestionSets referenceQuestionSets = new ReferenceQuestionSets();
				Date date = new Date();
				
				referenceQuestionSets.setQuestionSetText(quesSetName);
				referenceQuestionSets.setStatus(quesSetStatus);
				referenceQuestionSets.setDistrictMaster(districtMaster);
				referenceQuestionSets.setDateCreated(date); 
				if(headQuarterMaster!=null)
				referenceQuestionSets.setHeadQuarterId(headQuarterMaster.getHeadQuarterId());
				if(quesSetId!=null)
				{
					referenceQuestionSets.setID(quesSetId);
				}
				referenceQuestionSetsDAO.makePersistent(referenceQuestionSets);
			}
			else
			{
				System.out.println(" Question set is duplicate.");
				res = 1;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return res;
	}
	
	public ReferenceQuestionSets editRefChkQuestionSet(String quesId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		ReferenceQuestionSets refChkQuestionSets = null;
		
		try
		{
			refChkQuestionSets = referenceQuestionSetsDAO.findById(Integer.parseInt(quesId), false, false);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return refChkQuestionSets;
	}
	
	public boolean activateDeactivateQuestionSet(String quesSetId,String status)
	{
		/* ========  For Session time Out Error =========*/
				WebContext context;
				context = WebContextFactory.get();
				HttpServletRequest request = context.getHttpServletRequest();
				HttpSession session = request.getSession(false);
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				{
					throw new IllegalStateException(msgYrSesstionExp);
			    }
				
				try{
					ReferenceQuestionSets referenceQuestionSets = referenceQuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
					referenceQuestionSets.setStatus(status);
					referenceQuestionSetsDAO.makePersistent(referenceQuestionSets);
					
				}catch (Exception e) 
				{
					e.printStackTrace();
					return false;
				}
				return true;
	}	
	
	public boolean checkDuplicateQuesSet(String quesTxt,DistrictMaster districtMaster)
	{
		boolean chkDup = false;
		List<ReferenceQuestionSets> existQuesList = new ArrayList<ReferenceQuestionSets>();
		
		try
		{
			existQuesList = referenceQuestionSetsDAO.findExistQuestionSet(quesTxt,districtMaster);
			
			if(existQuesList!=null && existQuesList.size()>0)
				chkDup = true;
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return chkDup;
	}
	
	
	// @ Anurag
	
	public boolean checkDuplicateQuesSetByHQ(String quesTxt,HeadQuarterMaster headQuarterMaster)
	{
		boolean chkDup = false;
		List<ReferenceQuestionSets> existQuesList = new ArrayList<ReferenceQuestionSets>();
		
		try
		{
			existQuesList = referenceQuestionSetsDAO.findExistQuestionSetByHQ(quesTxt,headQuarterMaster);
			
			if(existQuesList!=null && existQuesList.size()>0)
				chkDup = true;
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return chkDup;
	}
	
	// @ Anurag 
	public ReferenceCheckQuestionSet getReferenceCheckQuestionSetByHQandJobCategory(HeadQuarterMaster headQuarterMaster , JobCategoryMaster jobCategoryMaster){
		ReferenceCheckQuestionSet referenceCheckQuestionSet = null;
		List<ReferenceCheckQuestionSet>  referenceCheckQuestionSetList = new ArrayList<ReferenceCheckQuestionSet>();
		try{
			referenceCheckQuestionSetList =  referenceCheckQuestionSetDAO.findByQuestionAndJobCategoryByHQ(headQuarterMaster, jobCategoryMaster);
			if(referenceCheckQuestionSetList.size()>0){
				referenceCheckQuestionSet = referenceCheckQuestionSetList.get(0);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return referenceCheckQuestionSet;
	}
	

}
