package tm.services;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.cgreport.UserMailSend;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.bean.user.UserMasterTemp;
import tm.dao.TeacherDetailDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.dao.user.UserMasterTempDAO;
import tm.utility.Utility;


public class UserUploadTempAjax 
{
	@Autowired
	private UserMasterTempDAO userMasterTempDAO;
	
	@Autowired
	private UserMasterDAO usermasterdao;
	public void setUsermasterdao(UserMasterDAO usermasterdao) {
		this.usermasterdao = usermasterdao;
	}
	@Autowired
	private  TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) {
		this.teacherDetailDAO = teacherDetailDAO;
	}
	@Autowired
	private  RoleMasterDAO roleMasterDAO;
	public void setRoleMasterDAO(RoleMasterDAO roleMasterDAO) {
		this.roleMasterDAO = roleMasterDAO;
	}

	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}

	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	private Vector vectorDataExcelXLSX = new Vector();
	public String saveUserTemp(String fileName,String sessionId,Integer districtId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		System.out.println("::::::::saveTeacherTemp:::::::::");
		session.setAttribute("Did",districtId);
		String returnVal="";
        try{
        	 String root = request.getRealPath("/")+"/uploads/";
        	 String filePath=root+fileName;
        	 System.out.println("filePath :"+filePath);
        	 if(fileName.contains(".xlsx")){
        		 vectorDataExcelXLSX = readDataExcelXLSX(filePath);
        	 }else{
        		 vectorDataExcelXLSX = readDataExcelXLS(filePath);
        	 }
        	 System.out.println("::::::::get value of XLSX and XLS:::::::::");
			 int location_count=11;
			 int role_count=11;
			 int user_first_name_count=11;
			 int user_last_name_count=11;
			 int user_email_count=11;
			 DistrictMaster districtMaster =districtMasterDAO.findById(districtId, false, false);
			 
			 Map<String,JobOrder> jobOrderList = new HashMap<String, JobOrder>();
			 List<String> jobApiJobIdList = new ArrayList<String>();
			 List<UserMaster> userMasterlst = usermasterdao.findByAllUserMaster();
			 Map<String,Boolean> userMap = new HashMap<String,Boolean>();
			 for (UserMaster userMaster : userMasterlst) {
				 userMap.put(userMaster.getEmailAddress(),true);
			 }
			 List emails = new ArrayList();
			 int user_email_count1=11;
			 int location_count1=11;
			 
			 SessionFactory sessionFactory=usermasterdao.getSessionFactory();
			 StatelessSession sessionHiber = sessionFactory.openStatelessSession();
			 List<String> locationCodes = new ArrayList<String>();
        	 Transaction txOpen =sessionHiber.beginTransaction();
			 for(int em=0; em<vectorDataExcelXLSX.size(); em++) {
	            Vector vectorCellEachRowDataGetEmail = (Vector) vectorDataExcelXLSX.get(em);
	            String email="",location=null;
	            for(int e=0; e<vectorCellEachRowDataGetEmail.size(); e++) {
	            	if(vectorCellEachRowDataGetEmail.get(e).toString().equalsIgnoreCase("user_email")){
	            		user_email_count1=e;
	            	}
	            	if(user_email_count1==e)
	            		email=vectorCellEachRowDataGetEmail.get(e).toString().trim();
	            	
	            	if(vectorCellEachRowDataGetEmail.get(e).toString().equalsIgnoreCase("location")){
	            		location_count1=e;
	            	}
	            	if(location_count1==e)
	            		location=vectorCellEachRowDataGetEmail.get(e).toString().trim();
	            }
	            if(em!=0){
	            	 emails.add(email);
	            	 if(location!=null && !location.equals("")){
	            		 System.out.println("location:::::>>>"+location);
	            		 try{
	            			 locationCodes.add(location);
	            		 }catch(Exception e){
	            			 e.printStackTrace();
	            		 }
	            	 }
	            }
			 }
			 Map<String,SchoolMaster> schoolMasterMap=new HashMap<String, SchoolMaster>();
        	 System.out.println("locationCodes:::::::::::"+locationCodes.size());
        	 if(locationCodes.size()>0){
	        	 List<SchoolMaster> schoolMasterList = schoolMasterDAO.findSchoolMastersByDistrictAndLocation(locationCodes,districtMaster);
	        	 System.out.println("schoolMasterList::::::::::::"+schoolMasterList.size());
	        	 for (SchoolMaster schoolMasterObj : schoolMasterList) {
	        		 schoolMasterMap.put(schoolMasterObj.getLocationCode(),schoolMasterObj);
				 }
        	 }
			 List <TeacherDetail> teacherUploadTempList=teacherDetailDAO.findAllTeacherDetails(emails);
			 Map<String,TeacherDetail> teacherMap=new HashMap<String, TeacherDetail>();
			 for (TeacherDetail teacherDetail : teacherUploadTempList) {
				 teacherMap.put(teacherDetail.getEmailAddress(),teacherDetail);
			}
			 System.out.println("::::::::Start Inserting:::::::::");
			 UserMasterTemp userMasterTemp= new UserMasterTemp();
			 for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
	             Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
	             String location="";
				 String role="";
				 String user_first_name="";
				 String user_last_name="";
				 String user_email="";
				 
	            for(int j=0; j<vectorCellEachRowData.size(); j++) {
	            	
	            	try{
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("location")){
		            		location_count=j;
		            	}
		            	if(location_count==j){
		            		location=vectorCellEachRowData.get(j).toString().trim();
		            	}
		            	
	            		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("role")){
		            		role_count=j;
		            	}
		            	if(role_count==j)
		            		role=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("user_first_name")){
		            		user_first_name_count=j;
		            	}
		            	if(user_first_name_count==j)
		            		 user_first_name=vectorCellEachRowData.get(j).toString().trim();
		            		
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("user_last_name")){
		            		user_last_name_count=j;
		            	}
		            	if(user_last_name_count==j)
		            		user_last_name=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("user_email")){
		            		user_email_count=j;
		            	}
		            	if(user_email_count==j)
		            		user_email=vectorCellEachRowData.get(j).toString().trim();
	            	}catch(Exception e){}
	            	
	            }
	            if(i!=0){
	            	boolean jobFlag=false;
		             if(returnVal.equals("1") && (!role.equalsIgnoreCase("") || !user_first_name.equalsIgnoreCase("") || !user_last_name.equalsIgnoreCase("") || !user_email.equalsIgnoreCase(""))){
		            	 userMasterTemp= new UserMasterTemp();
	            	 	String errorText=""; boolean errorFlag=false;
	            	 	
	            	 	int entityType=0;
	            	 	if(role.equalsIgnoreCase("DA")){
	            	 		entityType=2;
	            	 	}else if(role.equalsIgnoreCase("SA")){
	            	 		entityType=3;
	            	 	}else if(role.equalsIgnoreCase("TM Analyst")){
	            	 		entityType=1;
	            	 	}else if(role.equalsIgnoreCase("District Analyst")){
	            	 		entityType=2;
	            	 	}else if(role.equalsIgnoreCase("School Analyst")){
	            	 		entityType=3;
	            	 	}else if(role.equalsIgnoreCase("Admin")){
	            	 		entityType=1;
	            	 	}
	            	 	if(role.equalsIgnoreCase("")||role.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="Role is not available.";
	            	 		errorFlag=true;
	            	 	}else if(entityType==0){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="Role is not valid.";
	            	 		errorFlag=true;
	            	 	}
	            	 	if(user_first_name.equalsIgnoreCase("")||user_first_name.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="User First Name is not available.";
	            	 		errorFlag=true;
	            	 	}
	            	 	if(user_last_name.equalsIgnoreCase("")||user_last_name.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br> ";	
	            	 		}
	            	 		errorText+="User Last Name is not available.";
	            	 		errorFlag=true;
	            	 	}
	            	 	if(user_email.equalsIgnoreCase("")||user_email.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="User Email is not available.";
	            	 		errorFlag=true;
	            	 	}else if(!Utility.validEmail(user_email)){
            	 			if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="User Email is not valid.";
	            	 		errorFlag=true;
	            	 	}else if(userMap.get(user_email)!=null){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="User Email has already registered.";
	            	 		errorFlag=true;
	            	 	}else if(teacherMap.get(user_email)!=null){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="User Email has already registered.";
	            	 		errorFlag=true;
	            	 	}else{
	            	 		userMap.put(user_email,true);
	            	 	}
	            	 	
            	 		userMasterTemp.setEntityType(entityType);
            	 		userMasterTemp.setRole(role);
	            	 	userMasterTemp.setFirstName(user_first_name);
	            	 	userMasterTemp.setLastName(user_last_name);
	            	 	userMasterTemp.setEmailAddress(user_email);
	            	 	userMasterTemp.setSessionid(sessionId);
	            	 	userMasterTemp.setDistrictId(districtId);
	            	 	if(location!=null && !location.equals("")){
	            	 		userMasterTemp.setLocation(location+"");
	            	 		try{
	            	 			System.out.println("location::::::::"+location);
	        					if(schoolMasterMap.get(location)==null){
	        						if(errorFlag){
	    	            	 			errorText+=" </br>";	
	    	            	 		}
	    	            	 		errorText+="location is not valid.";
	        					}
	            	 		}catch(Exception e){
	            	 			if(errorFlag){
    	            	 			errorText+=" </br>";	
    	            	 		}
    	            	 		errorText+="location is not valid.";
	            	 			e.printStackTrace();
	            	 		}
	            	 	}else if(entityType==3){
            	 			if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="location is not available.";
	            	 	}
	            	 	if(errorText!=null)
	            	 		userMasterTemp.setErrortext(errorText);
	            	 	sessionHiber.insert(userMasterTemp);
		             }
	            }else{
	            	 if(role_count!=11 && user_first_name_count!=11 && user_last_name_count!=11 && user_email_count!=11){
	            		 returnVal="1"; 
	            		 userMasterTempDAO.deleteUserTemp(sessionId);
	            	 }else{
	            		 returnVal="";
	            		 boolean fieldVal=false;
		            	 if(role_count==11){
		            		 returnVal+=" role";
		            		 fieldVal=true;
		            	 }
		            	 if(user_first_name_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" user_first_name";
		            	 }
		            	 if(user_last_name_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" user_last_name";
		            	 }
		            	 if(user_email_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" user_email";
		            	 }
	            	 }
	            }
		     }
			 txOpen.commit();
     	 	 sessionHiber.close();
     	 	System.out.println("Data has been inserted successfully.");
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return returnVal;
	}
	public static Vector readDataExcelXLS(String fileName) throws IOException {

		System.out.println(":::::::::::::::::readDataExcel XLS::::::::::::");
		Vector vectorData = new Vector();
		
		try{
			InputStream ExcelFileToRead = new FileInputStream(fileName);
			HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);
	 
			HSSFSheet sheet=wb.getSheetAt(0);
			HSSFRow row; 
			HSSFCell cell;
	 
			Iterator rows = sheet.rowIterator();
			int location_count=11;
	        int role_count=11;
	        int user_first_name_count=11;
	        int user_last_name_count=11;
	        int user_email_count=11;
	        String indexCheck="";
			 
			while (rows.hasNext()){
				row=(HSSFRow) rows.next();
				Iterator cells = row.cellIterator();
				Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false,locationFlag=false;
				String indexPrev="";
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
				while (cells.hasNext())
				{
					cell=(HSSFCell) cells.next();
					cIndex=cell.getColumnIndex();
					if(cell.toString().equalsIgnoreCase("location")){
						location_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(location_count==cIndex){
						cellFlag=true;
						locationFlag=true;
					}	
					if(locationFlag){
						cell.setCellType(1);
	            	}  
					if(cell.toString().equalsIgnoreCase("role")){
						role_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(role_count==cIndex){
						cellFlag=true;
					}
					
					if(cell.toString().equalsIgnoreCase("user_first_name")){
						user_first_name_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(user_first_name_count==cIndex){
						cellFlag=true;
					}
					
					
					if(cell.toString().equalsIgnoreCase("user_last_name")){
						user_last_name_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(user_last_name_count==cIndex){
						cellFlag=true;
					}
					
					
					if(cell.toString().equalsIgnoreCase("user_email")){
						user_email_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(user_email_count==cIndex){
						cellFlag=true;
					}
					if(cellFlag){
						if(!locationFlag){
	            			try{
	            				mapCell.put(Integer.valueOf(cIndex),cell.getStringCellValue()+"");
	            			}catch(Exception e){
	            				mapCell.put(Integer.valueOf(cIndex),new Double(cell.getNumericCellValue()).longValue()+"");
	            			}
	            		}else{
	            			mapCell.put(Integer.valueOf(cIndex),cell+"");
	            		}
					}
					locationFlag=false;
		        	cellFlag=false;
					
				}
			    vectorCellEachRowData=cellValuePopulate(mapCell);
				vectorData.addElement(vectorCellEachRowData);
			}
		}catch(Exception e){}
		return vectorData;
    }
	public static Vector readDataExcelXLSX(String fileName) {
		System.out.println(":::::::::::::::::readDataExcel XLSX ::::::::::::");
        Vector vectorData = new Vector();
        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            XSSFWorkbook xssfWorkBook = new XSSFWorkbook(fileInputStream);
            // Read data at sheet 0
            XSSFSheet xssfSheet = xssfWorkBook.getSheetAt(0);
            Iterator rowIteration = xssfSheet.rowIterator();
            int location_count=11;
            int role_count=11;
            int user_first_name_count=11;
            int user_last_name_count=11;
            int user_email_count=11;
            // Looping every row at sheet 0
            String indexCheck="";
            while (rowIteration.hasNext()) {
                XSSFRow xssfRow = (XSSFRow) rowIteration.next();
                Iterator cellIteration = xssfRow.cellIterator();
                Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false,locationFlag=false;
				String indexPrev="";
                // Looping every cell in each row at sheet 0
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
                while (cellIteration.hasNext()){
                    XSSFCell xssfCell = (XSSFCell) cellIteration.next();
                    cIndex=xssfCell.getColumnIndex();
                    
                    if(xssfCell.toString().equalsIgnoreCase("location")){
                    	location_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(location_count==cIndex){
	            		cellFlag=true;
	            		locationFlag=true;
	            	}	
	            	if(locationFlag){
	            		xssfCell.setCellType(1);
	            	}
	            	if(xssfCell.toString().equalsIgnoreCase("role")){
	            		role_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(role_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("user_first_name")){
	            		user_first_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(user_first_name_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("user_last_name")){
	            		user_last_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(user_last_name_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("user_email")){
	            		user_email_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(user_email_count==cIndex){
	            		cellFlag=true;
	            	}
	            	if(cellFlag){
	            		if(!locationFlag){
	            			try{
	            				mapCell.put(Integer.valueOf(cIndex),xssfCell.getStringCellValue()+"");
	            			}catch(Exception e){
	            				mapCell.put(Integer.valueOf(cIndex),xssfCell.getRawValue()+"");
	            			}
	            		}else{
	            			mapCell.put(Integer.valueOf(cIndex),xssfCell+"");
	            		}
	            	}
	            	locationFlag=false;
	            	cellFlag=false;
                }
                vectorCellEachRowData=cellValuePopulate(mapCell);
                vectorData.addElement(vectorCellEachRowData);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
         
        return vectorData;
    }
	public static Vector cellValuePopulate(Map<Integer,String> mapCell){
		 Vector vectorCellEachRowData = new Vector();
		 Map<Integer,String> mapCellTemp = new TreeMap<Integer, String>();
		 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false;
		 for(Map.Entry<Integer, String> entry : mapCell.entrySet()){
			int key=entry.getKey();
			String cellValue=null;
			if(entry.getValue()!=null)
				cellValue=entry.getValue().trim();
			
			if(key==0){
				mapCellTemp.put(key, cellValue);
				flag0=true;
			}
			if(key==1){
				flag1=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==2){
				flag2=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==3){
				flag3=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==4){
				flag4=true;
				mapCellTemp.put(key, cellValue);
			}
		 }
		 if(flag0==false){
			 mapCellTemp.put(0, "");
		 }
		 if(flag1==false){
			 mapCellTemp.put(1, "");
		 }
		 if(flag2==false){
			 mapCellTemp.put(2, "");
		 }
		 if(flag3==false){
			 mapCellTemp.put(3, "");
		 }
		 if(flag4==false){
			 mapCellTemp.put(4, "");
		 }
		 for(Map.Entry<Integer, String> entry : mapCellTemp.entrySet()){
			 vectorCellEachRowData.addElement(entry.getValue());
		 }
		 return vectorCellEachRowData;
	}
	public String displayTempUserRecords(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			System.out.println("calling displayTempUserRecords ");
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			System.out.println("start"+start);
			System.out.println("end"+end);
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			/***** Sorting by  Stating ( Sekhar ) ******/
			List<UserMasterTemp> userMasterTempList	  =	new ArrayList<UserMasterTemp>();
			
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"tempuserId";
	
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				 {
					 sortOrderFieldName		=	sortOrder;
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion = Restrictions.eq("sessionid",session.getId());
			totalRecord = userMasterTempDAO.getRowCountTempUser(criterion);
			userMasterTempList = userMasterTempDAO.findByTempUser(sortOrderStrVal,start,noOfRowInPage,criterion);
			
			
			/***** Sorting by Temp Teacher  End ******/
	
			dmRecords.append("<table  id='tempUserTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLink("User First Name",sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("User Last Name",sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("User Email",sortOrderFieldName,"emailAddress",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Role",sortOrderFieldName,"role",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Location",sortOrderFieldName,"location",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Errors",sortOrderFieldName,"errortext",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(userMasterTempList.size()==0)
				dmRecords.append("<tr><td colspan='6' align='center'>No User found</td></tr>" );
			System.out.println("No of Records "+userMasterTempList.size());

			for (UserMasterTemp userMasterTemp : userMasterTempList){
				String rowCss="";
				
				if(!userMasterTemp.getErrortext().equalsIgnoreCase("")){
					rowCss="style=\"background-color:#FF0000;\"";
				}
				String location ="";
				if(userMasterTemp.getLocation()!=null){
					location=userMasterTemp.getLocation()+"";
				}
				dmRecords.append("<tr >" );
				dmRecords.append("<td "+rowCss+">"+userMasterTemp.getFirstName()+"</td>");
				dmRecords.append("<td "+rowCss+">"+userMasterTemp.getLastName()+"</td>");
				dmRecords.append("<td "+rowCss+">"+userMasterTemp.getEmailAddress()+"</td>");
				dmRecords.append("<td "+rowCss+">"+userMasterTemp.getRole()+"</td>");
				dmRecords.append("<td "+rowCss+">"+location+"</td>");
				dmRecords.append("<td "+rowCss+">"+userMasterTemp.getErrortext()+"</td>");
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	public int deleteTempUser(String sessionId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		System.out.println(":::::::::::::::::deleteTempUser:::::::::::::::::"+sessionId);
		try{
			userMasterTempDAO.deleteUserTemp(sessionId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	public int saveUser()
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession=null;
		HeadQuarterMaster headQuarterMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }else{
	    	userSession=(UserMaster)session.getAttribute("userMaster");
		}
		System.out.println("::::::::saveUser now:::::::::");
		int returnVal=0;
        try{
        	Criterion criterion1 = Restrictions.eq("sessionid",session.getId());
        	Criterion criterion2 = Restrictions.eq("errortext","");
        	List <UserMasterTemp> userMasterTemplst=userMasterTempDAO.findByCriteria(criterion1,criterion2);
        	 List emails = new ArrayList();
        	 List<Integer> roleIds = new ArrayList<Integer>();
        	 List<String> locationCodes = new ArrayList<String>();
        	 int districtId=0;
        	 for(int e=0; e<userMasterTemplst.size(); e++) {
	            		emails.add(userMasterTemplst.get(e).getEmailAddress());
	            		String roleText=userMasterTemplst.get(e).getRole();
	            		int roleId=1;
	            	 	if(roleText.equalsIgnoreCase("DA")){
	            	 		roleId=2;
	            	 	}else if(roleText.equalsIgnoreCase("SA")){
	            	 		roleId=3;
	            	 	}else if(roleText.equalsIgnoreCase("TM Analyst")){
	            	 		roleId=4;
	            	 	}else if(roleText.equalsIgnoreCase("District Analyst")){
	            	 		roleId=5;
	            	 	}else if(roleText.equalsIgnoreCase("School Analyst")){
	            	 		roleId=6;
	            	 	}
	            		roleIds.add(roleId);
	            		if(userMasterTemplst.get(e).getLocation()!=null){
	            			locationCodes.add(userMasterTemplst.get(e).getLocation());
	            		}
	            		if(e==0){
	            			districtId=userMasterTemplst.get(e).getDistrictId();
	            		}
			 }
        	 List<RoleMaster> roleMasters = roleMasterDAO.getRoleByEntityType(roleIds);
        	 Map<Integer,RoleMaster> roleMasterMap=new HashMap<Integer, RoleMaster>();
        	 for (RoleMaster roleMaster2 : roleMasters) {
        		 roleMasterMap.put(roleMaster2.getRoleId(),roleMaster2);
			}
        	 Map<String,SchoolMaster> schoolMasterMap=new HashMap<String, SchoolMaster>();
        	 System.out.println("locationCodes:::::::::::"+locationCodes.size());
        	 DistrictMaster  districtMaster =	districtMasterDAO.findById(districtId, false, false);
        	 if(locationCodes.size()>0){
	        	 List<SchoolMaster> schoolMasterList = schoolMasterDAO.findSchoolMastersByDistrictAndLocation(locationCodes,districtMaster); 
	        	 System.out.println("schoolMasterList::::::::::::"+schoolMasterList.size());
	        	 for (SchoolMaster schoolMasterObj : schoolMasterList) {
	        		 schoolMasterMap.put(schoolMasterObj.getLocationCode(),schoolMasterObj);
				 }
        	 }
        	 List <UserMaster> userMasterList=usermasterdao.findAllUserMaster(emails);
        	 Map<String,Boolean> userExistMap=new HashMap<String, Boolean>();
        	 for (UserMaster userMasterObj : userMasterList) {
        		 userExistMap.put(userMasterObj.getEmailAddress(), true);
			 }
        	 Map<String,Boolean> userInsertMap=new HashMap<String, Boolean>();
        	 /**** Session dunamic ****/
        	 SessionFactory sessionFactory=teacherDetailDAO.getSessionFactory();
			 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
        	 Transaction txOpen =statelesSsession.beginTransaction();
        	 
        	List<UserMailSend> userMailSendList =new ArrayList<UserMailSend>();   
        	System.out.println("userMasterTemplst:::::::::::::::::"+userMasterTemplst.size());
        	List<String> emailList=new ArrayList<String>();
        	for(UserMasterTemp userMasterTemp : userMasterTemplst){
        		System.out.println(userInsertMap.get(userMasterTemp.getEmailAddress())+" | "+userExistMap.get(userMasterTemp.getEmailAddress()));
    			if(userInsertMap.get(userMasterTemp.getEmailAddress())== null && userExistMap.get(userMasterTemp.getEmailAddress())==null){
    				int verificationCode=(int) Math.round(Math.random() * 2000000);
    				UserMaster userMaster =	new UserMaster();
    				userMaster.setEntityType(userMasterTemp.getEntityType());
    				Utility utility	=	new Utility();
    				String authenticationCode	=	utility.getUniqueCodebyId((1));
    				userMaster.setPassword(MD5Encryption.toMD5("teachermatch"));
    				userMaster.setAuthenticationCode(authenticationCode);		
    				if(userMasterTemp.getEntityType()!=1){
    					userMaster.setDistrictId(districtMaster);
    				}
    				if(districtMaster!=null && districtMaster.getHeadQuarterMaster()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
    				{
    					headQuarterMaster=headQuarterMasterDAO.findById(2, false, false);
    					if(headQuarterMaster!=null)
    					userMaster.setHeadQuarterMaster(headQuarterMaster);
    				}	
    				userMaster.setFirstName(userMasterTemp.getFirstName());
    				userMaster.setLastName(userMasterTemp.getLastName());
    				userMaster.setEmailAddress(userMasterTemp.getEmailAddress());
    				int roleId=1;
    				String roleText=userMasterTemp.getRole();
            	 	if(roleText.equalsIgnoreCase("DA")){
            	 		roleId=2;
            	 	}else if(roleText.equalsIgnoreCase("SA")){
            	 		roleId=3;
            	 	}else if(roleText.equalsIgnoreCase("TM Analyst")){
            	 		roleId=4;
            	 	}else if(roleText.equalsIgnoreCase("District Analyst")){
            	 		roleId=5;
            	 	}else if(roleText.equalsIgnoreCase("School Analyst")){
            	 		roleId=6;
            	 	}
    				userMaster.setRoleId(roleMasterMap.get(roleId));
    				userMaster.setStatus("A");
    				userMaster.setVerificationCode(""+verificationCode);
    				userMaster.setForgetCounter(0);
    				userMaster.setIsQuestCandidate(false);
    				userMaster.setCreatedDateTime(new Date());
    				
    				if(userMasterTemp.getLocation()!=null){
    					if(schoolMasterMap.get(userMasterTemp.getLocation())!=null){
    						userMaster.setSchoolId(schoolMasterMap.get(userMasterTemp.getLocation()));
    					}
    				}
    				statelesSsession.insert(userMaster);
    				userInsertMap.put(userMasterTemp.getEmailAddress(),true);
    				emailList.add(userMasterTemp.getEmailAddress());
    			}
        	}
	       	txOpen.commit();
	       	statelesSsession.close();
	       	List <UserMaster> userMasterUploadList=usermasterdao.findAllUserMaster(emailList);
	       	for (UserMaster userMaster2 : userMasterUploadList) {
	       		UserMailSend userMailSend= new UserMailSend();
				userMailSend.setUserMaster(userMaster2);
				userMailSend.setUserMasterForOther(userSession);
				userMailSend.setEmail(userMaster2.getEmailAddress());
				userMailSendList.add(userMailSend);
			}
	       	try{
		       	mailSendByThread(userMailSendList);
	        	deleteXlsAndXlsxFile(request,session.getId());
	        	userMasterTempDAO.deleteAllUserTemp(session.getId());
	       	}catch(Exception e){
	       		e.printStackTrace();
	       	}
		}catch (Exception e){
			System.out.println("Error >"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			return returnVal;
		}
	}
	public void mailSendByThread(List<UserMailSend> userMailSendList){
		MailSend mst =new MailSend(userMailSendList);
		Thread currentThread = new Thread(mst);
		currentThread.start(); 
	}
	public class MailSend  extends Thread{
		
		List<UserMailSend> userMailSendListTrdVal=new ArrayList<UserMailSend>();
		public MailSend(List<UserMailSend> userMailSendList){
			userMailSendListTrdVal=userMailSendList;
		 }
		 public void run() {
				try {
					Thread.sleep(1000);
					System.out.println("::Trd size::"+userMailSendListTrdVal.size());
					for(UserMailSend userMailSend :userMailSendListTrdVal){
						//emailerService.sendMailAsHTMLText(userMailSend.getEmail(), "I Have Added You As a User",MailText.createUserForUpload(userMailSend.getUserMasterForOther(),userMailSend.getUserMaster()));
						String mailContent = "";
						if(userMailSend.getUserMaster()!=null && userMailSend.getUserMaster().getDistrictId()!=null && userMailSend.getUserMaster().getDistrictId().getDistrictId()==806900){
							//mailContent = MailText.createUserPwdMailToOtUser(userMailSend.getRequest(),userMailSend.getUserMaster());
							mailContent = MailText.createUserPwdMailToOtUser(userMailSend.getUserMaster());
						}else{
							mailContent = MailText.createUserForUpload(userMailSend.getUserMasterForOther(),userMailSend.getUserMaster());
						}
						emailerService.sendMailAsHTMLText(userMailSend.getEmail(), "I Have Added You As a User",mailContent);
					}
				}catch (NullPointerException en) {
					en.printStackTrace();
				}catch (InterruptedException e) {
					e.printStackTrace();
				}
		  }
	}
	
	public void deleteXlsAndXlsxFile(HttpServletRequest request,String sessionId){
		List<UserMasterTemp> teacherUploadTemplst= userMasterTempDAO.findByAllTempUser();
		String root = request.getRealPath("/")+"/uploads/";
		for(UserMasterTemp userMasterTemp : teacherUploadTemplst){
			String filePath="";
			File file=null;
			try{
				filePath=root+userMasterTemp.getSessionid()+".xlsx";
				file = new File(filePath);
				file.delete();
			}catch(Exception e){}
			try{
				filePath=root+userMasterTemp.getSessionid()+".xls";
				file = new File(filePath);
				file.delete();
			}catch(Exception e){}
		}
		
		String filePath="";
		File file=null;
		try{
			filePath=root+sessionId+".xlsx";
			file = new File(filePath);
			file.delete();
		}catch(Exception e){}
		try{
			filePath=root+sessionId+".xls";
			file = new File(filePath);
			file.delete();
		}catch(Exception e){}
	}
}


