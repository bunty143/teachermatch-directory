package tm.services;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.master.AffidavitMaster;
import tm.dao.master.AffidavitMasterDAO;
import tm.services.district.GlobalServices;
import tm.services.quartz.StatusSchedule;
import tm.servlet.ListFilesUtil;

public class TMContextListener implements ServletContextListener{

	private StatusSchedule ss;
	
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("<<<<<############################################");
		System.out.println(""+sce.getServletContext());		
		ServletContext context =  sce.getServletContext();
		try{
			ListFilesUtil listFilesUtil = new ListFilesUtil();
			Map<String,Long> resouceMap = listFilesUtil.getUpdatedFilesMap(context);
		    context.setAttribute("resouceMap",resouceMap);   
		    
        	ss = new StatusSchedule();
        	cgViewTemplate(sce);
		}catch(Exception e){
			e.printStackTrace();
		}
	}	
	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("############################################>>>");		
	}
	public void cgViewTemplate(ServletContextEvent sce){
		System.out.println("Bijan CG VIEW Testing");
		List<AffidavitMaster> affidavitMaster=null;
		
		try{
			ApplicationContext APPLICATIONCONTEXT = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
			AffidavitMasterDAO affidavitMasterDAO = (AffidavitMasterDAO)APPLICATIONCONTEXT.getBean("affidavitMasterDAO");
			affidavitMaster = affidavitMasterDAO.getAllActiveAffidavit();
			for(AffidavitMaster obj:affidavitMaster){
				GlobalServices.AFFIDAVIT_MASTER_HEADING.put(obj.getHeadQuarterMaster().getHeadQuarterId(), obj.getAffidavitheading());
				GlobalServices.AFFIDAVIT_MASTER.put(obj.getHeadQuarterMaster().getHeadQuarterId(),obj.getContent());
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
