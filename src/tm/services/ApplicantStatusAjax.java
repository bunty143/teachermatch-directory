package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
public class ApplicantStatusAjax 
{
       String locale = Utility.getValueOfPropByKey("locale");
	
	/*start by Ram nath*/
	@Autowired
	private JobOrderDAO jobOrderDAO;	
	/*end by Ram nath*/
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;	
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;	
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;
	
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;

	
	/*Start by Ram nath*/
	public String getJobIdOrJobCode(int districtId){
		/* ========  For Session time Out Error =========*/
		System.out.println("*******getJobIdOrJobCode***********District Id=="+districtId);
		StringBuffer sb=new StringBuffer("");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
		List<String[]> lstJobOrder=jobOrderDAO.findActiveAndInactiveJobOrderByDistrict(districtId);
		if(lstJobOrder.size()>0){
			sb.append("<option value='0'>"+Utility.getLocaleValuePropByKey("msgAllJobIdJobCode", locale)+"</option>");
		for (Iterator it = lstJobOrder.iterator(); it.hasNext();) {
            String jobId= it.next().toString();
            sb.append("<option value='"+jobId+"'>"+jobId+"</option>");
          //  System.out.println("next-->");
        }
		}
		}catch (Throwable e) {
			e.printStackTrace();
		}
		
		
		return sb.toString();
	}
	/*end By Ram nath*/
	
	 public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	

	public String displayRecordsByEntityType(String endfromDate,String endtoDate,int jobOrderId,int districtOrSchoolId,int schoolId,String jobStatus,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	 {
			System.out.println(jobStatus+":: jobStatus ::"+schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			String lblOfferMade = Utility.getLocaleValuePropByKey("lblOfferMade", locale);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int colCount = 0;
			int sortingcheck=0;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				/*add By Ram Nath*/
				SimpleDateFormat sdf=new SimpleDateFormat("MM-dd-yyyy");
				Date endStartDate=null,endToDate=null;
				if(endfromDate.equals("") && endtoDate.equals("") && districtOrSchoolId!=0){
					List<String[]> minEndStartDateFromJobOrder=jobOrderDAO.findMinJobEndDateFromJobOrderByDistrict(districtOrSchoolId,jobOrderId);
					for (Iterator it = minEndStartDateFromJobOrder.iterator(); it.hasNext();){
						 Object[] row = (Object[]) it.next();
						endStartDate = (Date) row[1];
						endToDate 	= (Date) row[0];
					}
					String endStartDate1 = endStartDate.toString();
					String[] parts 	= 	endStartDate1.split("-");
					
					String year		= 	parts[0];
					String month 	= 	parts[1];
					String[] day1	=	parts[2].split(" ");
					String day		=	day1[0];
					endStartDate1	= month+"-"+day+"-"+year;	
					endStartDate=sdf.parse(endStartDate1);

				}else if(districtOrSchoolId!=0){
					endStartDate=sdf.parse(endfromDate);
				}
				/*if(endtoDate.equals("") && districtOrSchoolId!=0){
					endToDate=new Date();
				}else */
				/*if(!endtoDate.equals("") && districtOrSchoolId!=0){
					System.out.println(":::::::::::::::::::::::::::1111111");
					endToDate=sdf.parse(endtoDate);
				}*/
				
				/*end By Ram Nath*/
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="jobTitle";
				String sortOrderNoField="jobTitle";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("jobTitle") )
					sortingcheck=1;
				
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			
			  //@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				
				List list = new ArrayList();
				Set<JobOrder> lstjoborders = new HashSet<JobOrder>();
				Map<Integer, Map<String,Integer>> mapsscountwithJob= new HashMap<Integer, Map<String,Integer>>();
				Map<Integer,Integer> mapTotalCount = new HashMap<Integer, Integer>(); 
				Map<Integer,String> mapSpecificStatus = new HashMap<Integer, String>();
				List<SecondaryStatus> secondaryStatusList = new ArrayList<SecondaryStatus>();
				List<JobOrder> listjobJobOrders = new ArrayList<JobOrder>();
				
				
				//System.out.println("endfromDate=="+endStartDate+" endtoDate=="+endToDate+" jobOrderId=="+jobOrderId);
				
				List<JobOrder> lstwithFilterJOrder=jobOrderDAO.findJobOrderForReportByDistrict(districtOrSchoolId,jobOrderId,endStartDate,endToDate,schoolId);
				System.out.println("lstwithFilterJOrder size()=="+lstwithFilterJOrder.size());
				//System.out.println("entityID=="+entityID);
				if(entityID==2)
				{
					
					
					List<JobOrder> lstJobOrders =new ArrayList<JobOrder>();
					if(schoolId!=0){
						SchoolMaster  sclMaster=null;
						sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
					}
					
					/*by Ram nath*/
					lstwithFilterJOrder.containsAll(lstJobOrders);
					if(jobOrderId==0 && lstwithFilterJOrder.size()==0)
						lstwithFilterJOrder=null;	
					/*end by Ram nath*/
					list=jobForTeacherDAO.fintApplicntByStatus(districtMaster,jobStatus,sortingcheck,sortOrderStrVal,lstwithFilterJOrder);
				}
				else if(entityID==1)
				{
					if(districtOrSchoolId!=0){
						districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
						List<JobOrder> lstJobOrders =new ArrayList<JobOrder>();
						if(schoolId!=0){
							SchoolMaster  sclMaster=null;
							sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
							lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
						 }
						
						/*by Ram nath*/		
						//System.out.println("lstJobOrders controller==="+lstJobOrders);
						lstwithFilterJOrder.containsAll(lstJobOrders);
						//System.out.println("lstwithFilterJOrder controller==="+lstwithFilterJOrder);
						if(jobOrderId==0 && lstwithFilterJOrder.size()==0)
							lstwithFilterJOrder=null;
						/*end by Ram nath*/
						list=jobForTeacherDAO.fintApplicntByStatus(districtMaster,jobStatus,sortingcheck,sortOrderStrVal,lstwithFilterJOrder);  
					 }
					}
					System.out.println("list  ss=="+list.size());
				String []statusArray={"scomp" , "ecomp" , "vcomp"};
			    List<StatusMaster> lstStatusMasters = statusMasterDAO.findStatusByStatusByShortNames(statusArray);
			    secondaryStatusList= secondaryStatusDAO.findSecondaryStatusforSpecificStatusList(districtMaster,lstStatusMasters);
			    for(SecondaryStatus ss : secondaryStatusList)
			     {
			    	mapSpecificStatus.put(ss.getStatusMaster().getStatusId(), ss.getSecondaryStatusName());
			     }
			    
			    Iterator itr = list.iterator();
				if(list.size()>0){
					while(itr.hasNext()){
						Object[] obj = (Object[]) itr.next();
						JobOrder jobOrder=(JobOrder)obj[0];
						lstjoborders.add(jobOrder);
						
						
						if(!listjobJobOrders.contains(jobOrder)){
							listjobJobOrders.add(jobOrder);
						}
						
						SecondaryStatus secondaryStatus= null;
						StatusMaster statusMaster= null;
						if(obj[1]!=null)
							secondaryStatus=(SecondaryStatus) obj[1];
						
						statusMaster=(StatusMaster) obj[2];
						Integer secCount= (Integer) obj[4];
						Integer statusCount = (Integer) obj[5];
						int count=0;
						if(secCount!=0 && statusCount!=0){
							count=statusCount;
						}else{
							count=secCount+statusCount;
						}
						if(mapTotalCount.get(jobOrder.getJobId())!=null){
							mapTotalCount.put(jobOrder.getJobId(),mapTotalCount.get(jobOrder.getJobId())+count);
						}else{
							mapTotalCount.put(jobOrder.getJobId(), count);
						}
						
						if(secondaryStatus!=null && statusMaster==null){
							Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
							if(countValue==null){
								Map<String,Integer> tempcountVal = new HashMap<String, Integer>();
								tempcountVal.put(secondaryStatus.getSecondaryStatusName(), secCount);
								mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
							}else{
								countValue.put(secondaryStatus.getSecondaryStatusName(), secCount);
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								
							}
						}
						
						
						if(secondaryStatus==null && statusMaster!=null){
							String sDidplayStatusName="";
							if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
								sDidplayStatusName=Utility.getLocaleValuePropByKey("optAvble", locale); 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
								sDidplayStatusName=Utility.getLocaleValuePropByKey("optTOut", locale); 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln")){
								sDidplayStatusName=Utility.getLocaleValuePropByKey("lblDeclined", locale); 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw")){
								sDidplayStatusName=Utility.getLocaleValuePropByKey("lblWithdrew", locale); 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird")){
								sDidplayStatusName=Utility.getLocaleValuePropByKey("lblHired", locale);
							
							}else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
						    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						    }
								 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
								sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							}
							  
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
								 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
								 
							 }
							
							
							Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
							if(countValue==null){
								Map<String,Integer> tempcountVal = new HashMap<String, Integer>();
								tempcountVal.put(sDidplayStatusName, statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
							}else{
								if(countValue.get(sDidplayStatusName)!=null){
									countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}else{
									countValue.put(sDidplayStatusName,statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}
							}
						}
					
						
						if(secondaryStatus!=null && statusMaster!=null){
							String sDidplayStatusName="";
							if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
								sDidplayStatusName=Utility.getLocaleValuePropByKey("optAvble", locale); 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
								sDidplayStatusName=Utility.getLocaleValuePropByKey("optTOut", locale); 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
								sDidplayStatusName=Utility.getLocaleValuePropByKey("lblDeclined", locale); 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
								sDidplayStatusName=Utility.getLocaleValuePropByKey("lblWithdrew", locale); 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
								sDidplayStatusName=Utility.getLocaleValuePropByKey("lblHired", locale);
							else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
						    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						    }
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
								sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							}
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
								 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							}
							Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
							if(countValue==null){
								Map<String,Integer> tempcountVal = new HashMap<String, Integer>();
								tempcountVal.put(sDidplayStatusName, statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
							}else{
								if(countValue.get(sDidplayStatusName)!=null){
									countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}else{
									countValue.put(sDidplayStatusName,statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}
							}
						}
					}
				}
				 if(sortOrder.equals("totalCandidate")){    
				     for(JobOrder jobOrder:listjobJobOrders) 
					 {
				    	 jobOrder.setFitScore(mapTotalCount.get(jobOrder.getJobId()));
					 }
				     
				     if(sortOrderTypeVal.equalsIgnoreCase("1"))
							Collections.sort(listjobJobOrders,JobOrder.jobOrderComparatorFitScoreDesc );
						else if(sortOrderTypeVal.equalsIgnoreCase("0"))
							Collections.sort(listjobJobOrders,JobOrder.jobOrderComparatorFitScoreAsc );
			      } 
			System.out.println(">>>>>>>>>>>>>>> listjobJobOrders.size() :: "+listjobJobOrders.size());
		    List<JobOrder> finalJobOrders = new ArrayList<JobOrder>();
			
		    totalRecord=listjobJobOrders.size();
			if(totalRecord<end)
				end=totalRecord;
			
			finalJobOrders = listjobJobOrders.subList(start,end);
			List<String> lstsecoundryStatusNames = new ArrayList<String>();
			List<SecondaryStatus> lstSecondaryStatus=null;
			if(districtMaster!=null)
			     lstSecondaryStatus = secondaryStatusDAO.findSecondryStatusByDistrict(districtMaster);
			if(lstSecondaryStatus!=null && lstSecondaryStatus.size()>0){
				for(SecondaryStatus ss: lstSecondaryStatus)
				{
					if(lstsecoundryStatusNames.size()==0){
						lstsecoundryStatusNames.add(ss.getSecondaryStatusName());
					}else if(!lstsecoundryStatusNames.contains(ss.getSecondaryStatusName())){
						lstsecoundryStatusNames.add(ss.getSecondaryStatusName());
				    }
				}
				if(lstsecoundryStatusNames!=null && lstsecoundryStatusNames.contains(lblOfferMade)){
					lstsecoundryStatusNames.remove(lblOfferMade);
					lstsecoundryStatusNames.add(lblOfferMade);
				}
				lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("lblHired", locale));
				lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("optIncomlete", locale));
				lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("btnRjt", locale));
				lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("optTOut", locale));
				lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("lblWithdrew", locale));
				lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("lblDeclined", locale));	
			}
			
			
			String responseText="";
			if(districtOrSchoolId!=0)
			{
				tmRecords.append("<table  id='tblGridApplicatStatus' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobId", locale),sortOrderNoField,"jobId",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderNoField,"jobTitle",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink( Utility.getLocaleValuePropByKey("msgTotalApplicants", locale),sortOrderNoField,"totalCandidate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
					for(String str : lstsecoundryStatusNames)
					{
					  tmRecords.append("<th valign='top'>"+str+"</th>");
					}
				System.out.println("lstsecoundryStatusNames.size()"+lstsecoundryStatusNames.size());
				colCount = lstsecoundryStatusNames.size();
				
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				tmRecords.append("<tbody>");
				
				if(finalJobOrders.size()==0){
					 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
					}
				if(finalJobOrders.size()>0){
				 for(JobOrder jobOrder:finalJobOrders) 
				 {
				  tmRecords.append("<tr>");	
				   tmRecords.append("<td>"+jobOrder.getJobId()+"</td>");
					tmRecords.append("<td>"+jobOrder.getJobTitle()+"</td>");
					 tmRecords.append("<td><a href='javascript:(void);' onclick='showDetails("+jobOrder.getJobId()+")'>"+mapTotalCount.get(jobOrder.getJobId())+"</a></td>");
					
					    Map<String, Integer> mapsecoundryStatus = mapsscountwithJob.get(jobOrder.getJobId());
						for(String str : lstsecoundryStatusNames){
							 if(mapsecoundryStatus==null){
								 tmRecords.append("<td>0</td>");
							 }else{
								 if(mapsecoundryStatus.get(str)!=null && mapsecoundryStatus.get(str)!=0){	
								    tmRecords.append("<td><a href='javascript:(void);' onclick='showDetailsByStatus(\""+jobOrder.getJobId()+"\",\""+str+"\")'>"+mapsecoundryStatus.get(str)+"</a></td>");
								   }   
								 else{
								    tmRecords.append("<td>0</td>");
								 }
							 }
						 }
				  tmRecords.append("</tr>");
				 }
				}
			tmRecords.append("</tbody>");
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			}
			}catch(Exception e){
				e.printStackTrace();
			}
			colCount = colCount+1;
		 return tmRecords.toString()+"###&&###"+colCount;
	  }
	
		
	@SuppressWarnings("unchecked")
	public String displayRecordsByEntityTypeEXL(String endfromDate,String endtoDate,int jobOrderId,int districtOrSchoolId,int schoolId,String jobStatus,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		String lblOfferMade = Utility.getLocaleValuePropByKey("lblOfferMade", locale);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = null;
		int sortingcheck=0;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			/*add By Ram Nath*/
			SimpleDateFormat sdf=new SimpleDateFormat("MM-dd-yyyy");
			Date endStartDate=null,endToDate=null;
			if(endfromDate.equals("") && endtoDate.equals("") && districtOrSchoolId!=0){
				List<String[]> minEndStartDateFromJobOrder=jobOrderDAO.findMinJobEndDateFromJobOrderByDistrict(districtOrSchoolId,jobOrderId);
				for (Iterator it = minEndStartDateFromJobOrder.iterator(); it.hasNext();){
					 Object[] row = (Object[]) it.next();
					endStartDate = (Date) row[1];
					endToDate 	= (Date) row[0];
				}
				String endStartDate1 = endStartDate.toString();
				String[] parts 	= 	endStartDate1.split("-");
				
				String year		= 	parts[0];
				String month 	= 	parts[1];
				String[] day1	=	parts[2].split(" ");
				String day		=	day1[0];
				endStartDate1	= month+"-"+day+"-"+year;	
				endStartDate=sdf.parse(endStartDate1);

			}else if(districtOrSchoolId!=0){
				endStartDate=sdf.parse(endfromDate);
			}
			/*SimpleDateFormat sdf=new SimpleDateFormat("MM-dd-yyyy");
			Date endStartDate=null,endToDate=null;
			try{
				if(endfromDate.equals("") && districtOrSchoolId!=0){
					List<String[]> minEndStartDateFromJobOrder=jobOrderDAO.findMinJobEndDateFromJobOrderByDistrict(districtOrSchoolId,jobOrderId);
					for (Iterator it = minEndStartDateFromJobOrder.iterator(); it.hasNext();){
						 Object[] row = (Object[]) it.next();
							endStartDate = (Date) row[1];
					}
										
				}else if(districtOrSchoolId!=0){
					endStartDate=sdf.parse(endfromDate);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(endtoDate.equals("") && districtOrSchoolId!=0){
				endToDate=new Date();
			}else if(districtOrSchoolId!=0){
				endToDate=sdf.parse(endtoDate);
			}*/				
			/*end By Ram Nath*/
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

			 /** set default sorting fieldName **/
			String sortOrderFieldName="jobTitle";
			String sortOrderNoField="jobTitle";

			if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("jobTitle") )
				sortingcheck=1;
			
			
			/**Start set dynamic sorting fieldName **/
			
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
		
		  //@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
			
			List list = new ArrayList();
			Set<JobOrder> lstjoborders = new HashSet<JobOrder>();
			Map<Integer, Map<String,Integer>> mapsscountwithJob= new HashMap<Integer, Map<String,Integer>>();
			Map<Integer,Integer> mapTotalCount = new HashMap<Integer, Integer>(); 
			Map<Integer,String> mapSpecificStatus = new HashMap<Integer, String>();
			List<SecondaryStatus> secondaryStatusList = new ArrayList<SecondaryStatus>();
			List<JobOrder> listjobJobOrders = new ArrayList<JobOrder>();
			
			//System.out.println("endfromDate=="+endStartDate+" endtoDate=="+endToDate+" jobOrderId=="+jobOrderId);			
			List<JobOrder> lstwithFilterJOrder=jobOrderDAO.findJobOrderForReportByDistrict(districtOrSchoolId,jobOrderId,endStartDate,endToDate,schoolId);
			System.out.println("lstwithFilterJOrder size()=="+lstwithFilterJOrder.size());
			//System.out.println("entityID=="+entityID);
			
			if(entityID==2)
			{
				
				List<JobOrder> lstJobOrders =new ArrayList<JobOrder>();
				if(schoolId!=0){
					SchoolMaster  sclMaster=null;
					sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
					lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
				}
				
				/*by Ram nath*/
				lstwithFilterJOrder.containsAll(lstJobOrders);
				if(jobOrderId==0 && lstwithFilterJOrder.size()==0)
					lstwithFilterJOrder=null;	
				/*end by Ram nath*/
				list=jobForTeacherDAO.fintApplicntByStatus(districtMaster,jobStatus,sortingcheck,sortOrderStrVal,lstwithFilterJOrder);
			}
			else if(entityID==1)
			{
				if(districtOrSchoolId!=0){
					districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					List<JobOrder> lstJobOrders =new ArrayList<JobOrder>();
					if(schoolId!=0){
						SchoolMaster  sclMaster=null;
						sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
						 
					 }
					
					/*by Ram nath*/
					lstwithFilterJOrder.containsAll(lstJobOrders);
					if(jobOrderId==0 && lstwithFilterJOrder.size()==0)
						lstwithFilterJOrder=null;	
					/*end by Ram nath*/
					list=jobForTeacherDAO.fintApplicntByStatus(districtMaster,jobStatus,sortingcheck,sortOrderStrVal,lstwithFilterJOrder);  
				 }
				}
				
			String []statusArray={"scomp" , "ecomp" , "vcomp"};
		    List<StatusMaster> lstStatusMasters = statusMasterDAO.findStatusByStatusByShortNames(statusArray);
		    secondaryStatusList= secondaryStatusDAO.findSecondaryStatusforSpecificStatusList(districtMaster,lstStatusMasters);
		    for(SecondaryStatus ss : secondaryStatusList)
		     {
		    	mapSpecificStatus.put(ss.getStatusMaster().getStatusId(), ss.getSecondaryStatusName());
		     }
		    
		    Iterator itr = list.iterator();
			if(list.size()>0){
				while(itr.hasNext()){
					Object[] obj = (Object[]) itr.next();
					JobOrder jobOrder=(JobOrder)obj[0];
					lstjoborders.add(jobOrder);
					
					
					if(!listjobJobOrders.contains(jobOrder)){
						listjobJobOrders.add(jobOrder);
					}
					
					SecondaryStatus secondaryStatus= null;
					StatusMaster statusMaster= null;
					if(obj[1]!=null)
						secondaryStatus=(SecondaryStatus) obj[1];
					
					statusMaster=(StatusMaster) obj[2];
					Integer secCount= (Integer) obj[4];
					Integer statusCount = (Integer) obj[5];
					
					int count=0;
					if(secCount!=0 && statusCount!=0){
						count=statusCount;
					}else{
						count=secCount+statusCount;
					}
					if(mapTotalCount.get(jobOrder.getJobId())!=null){
						mapTotalCount.put(jobOrder.getJobId(),mapTotalCount.get(jobOrder.getJobId())+count);
					}else{
						mapTotalCount.put(jobOrder.getJobId(), count);
					}
					
					if(secondaryStatus!=null && statusMaster==null){
						Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
						if(countValue==null){
							Map<String,Integer> tempcountVal = new HashMap<String, Integer>();
							tempcountVal.put(secondaryStatus.getSecondaryStatusName(), secCount);
							mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
						}else{
							countValue.put(secondaryStatus.getSecondaryStatusName(), secCount);
							mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							
						}
					}
					
					
					if(secondaryStatus==null && statusMaster!=null){
						String sDidplayStatusName="";
						if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
							sDidplayStatusName=Utility.getLocaleValuePropByKey("optAvble", locale); 
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
							sDidplayStatusName=statusMaster.getStatus();
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
							sDidplayStatusName=statusMaster.getStatus();
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
							sDidplayStatusName=Utility.getLocaleValuePropByKey("optTOut", locale);  
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
							sDidplayStatusName=Utility.getLocaleValuePropByKey("lblDeclined", locale);  
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
							sDidplayStatusName=Utility.getLocaleValuePropByKey("lblWithdrew", locale); 
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
							sDidplayStatusName=Utility.getLocaleValuePropByKey("lblHired", locale);
						
						else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
					    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
					    }
							 
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
							sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						}
						  
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
							 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						 }
						
						
						Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
						if(countValue==null){
							Map<String,Integer> tempcountVal = new HashMap<String, Integer>();
							tempcountVal.put(sDidplayStatusName, statusCount);
							mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
						}else{
							if(countValue.get(sDidplayStatusName)!=null){
								countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							}else{
								countValue.put(sDidplayStatusName,statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							}
						}
					}
					
					if(secondaryStatus!=null && statusMaster!=null){
						String sDidplayStatusName="";
						if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
							sDidplayStatusName=Utility.getLocaleValuePropByKey("optAvble", locale); 
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
							sDidplayStatusName=statusMaster.getStatus();
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
							sDidplayStatusName=statusMaster.getStatus();
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
							sDidplayStatusName=Utility.getLocaleValuePropByKey("optTOut", locale);  
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
							sDidplayStatusName=Utility.getLocaleValuePropByKey("lblDeclined", locale);  
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
							sDidplayStatusName=Utility.getLocaleValuePropByKey("lblWithdrew", locale); 
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
							sDidplayStatusName=Utility.getLocaleValuePropByKey("lblHired", locale);
						else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
					    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
					    }
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
							sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						}
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
							 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						}
						Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
						if(countValue==null){
							Map<String,Integer> tempcountVal = new HashMap<String, Integer>();
							tempcountVal.put(sDidplayStatusName, statusCount);
							mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
						}else{
							if(countValue.get(sDidplayStatusName)!=null){
								countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							}else{
								countValue.put(sDidplayStatusName,statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							}
						}
					}
				}
			}
			 if(sortOrder.equals("totalCandidate")){    
			     for(JobOrder jobOrder:listjobJobOrders) 
				 {
			    	 jobOrder.setFitScore(mapTotalCount.get(jobOrder.getJobId()));
				 }
			     
			     if(sortOrderTypeVal.equalsIgnoreCase("1"))
						Collections.sort(listjobJobOrders,JobOrder.jobOrderComparatorFitScoreDesc );
					else if(sortOrderTypeVal.equalsIgnoreCase("0"))
						Collections.sort(listjobJobOrders,JobOrder.jobOrderComparatorFitScoreAsc );
		      } 
		System.out.println(">>>>>>>>>>>>>>> listjobJobOrders.size() :: "+listjobJobOrders.size());
		
		
		List<String> lstsecoundryStatusNames = new ArrayList<String>();
		List<SecondaryStatus> lstSecondaryStatus=null;
		if(districtMaster!=null)
		     lstSecondaryStatus = secondaryStatusDAO.findSecondryStatusByDistrict(districtMaster);
		if(lstSecondaryStatus!=null && lstSecondaryStatus.size()>0){
			for(SecondaryStatus ss: lstSecondaryStatus)
			{
				if(lstsecoundryStatusNames.size()==0){
					lstsecoundryStatusNames.add(ss.getSecondaryStatusName());
				}else if(!lstsecoundryStatusNames.contains(ss.getSecondaryStatusName())){
					lstsecoundryStatusNames.add(ss.getSecondaryStatusName());
			    }
			}
			if(lstsecoundryStatusNames!=null && lstsecoundryStatusNames.contains(lblOfferMade)){
				lstsecoundryStatusNames.remove(lblOfferMade);
				lstsecoundryStatusNames.add(lblOfferMade);
			}
			lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("lblHired", locale));
			lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("lblIncomplete", locale));
			lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("btnRjt", locale));
			lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("lblTimedOut", locale));
			lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("lblWithdrew", locale));
			lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("lblDeclined", locale));
		}
		
			 
			 //  Excel   Exporting	
					
					String time = String.valueOf(System.currentTimeMillis()).substring(6);
					//String basePath = request.getRealPath("/")+"/candidate";
					String basePath = request.getSession().getServletContext().getRealPath ("/")+"/applicantstatus";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="applicantstatus"+time+".xls";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);

					WorkbookSettings wbSettings = new WorkbookSettings();

					wbSettings.setLocale(new Locale("en", "EN"));

					WritableCellFormat timesBoldUnderline;
					WritableCellFormat header;
					WritableCellFormat headerBold;
					WritableCellFormat times;

					WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
					workbook.createSheet("applicantstatus", 0);
					WritableSheet excelSheet = workbook.getSheet(0);

					WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
					// Define the cell format
					times = new WritableCellFormat(times10pt);
					// Lets automatically wrap the cells
					times.setWrap(true);

					// Create create a bold font with unterlines
					WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
					WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

					timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
					timesBoldUnderline.setAlignment(Alignment.LEFT);
					// Lets automatically wrap the cells
					timesBoldUnderline.setWrap(true);

					header = new WritableCellFormat(times10ptBoldUnderline);
					headerBold = new WritableCellFormat(times10ptBoldUnderline);
					CellView cv = new CellView();
					cv.setFormat(times);
					cv.setFormat(timesBoldUnderline);
					cv.setAutosize(true);

					header.setBackground(Colour.GRAY_25);

					int numberOfRow=2+lstsecoundryStatusNames.size();
					// Write a few headers
					excelSheet.mergeCells(0, 0, numberOfRow, 1);
					Label label;
					label = new Label(0, 0, Utility.getLocaleValuePropByKey("headAppStByJoOdr", locale) , timesBoldUnderline);
					excelSheet.addCell(label);
					excelSheet.mergeCells(0, 3, numberOfRow, 3);
					label = new Label(0, 3, "");
					excelSheet.addCell(label);
					excelSheet.getSettings().setDefaultColumnWidth(18);
					
					int k=4;
					int col=1;
					label = new Label(0, k, Utility.getLocaleValuePropByKey("lblJobId", locale) ,header); 
					excelSheet.addCell(label);
					label = new Label(1, k, Utility.getLocaleValuePropByKey("lblJoTil", locale) ,header); 
					excelSheet.addCell(label);
					label = new Label(2, k, Utility.getLocaleValuePropByKey("msgTotalApplicants", locale) ,header); 
					excelSheet.addCell(label);
					
					for(String str : lstsecoundryStatusNames)
					{
						label = new Label(++col, k, str,header); 
						excelSheet.addCell(label);
					}
					
					k=k+1;
					if(listjobJobOrders.size()==0)
					{	
						excelSheet.mergeCells(0, k, 8, k);
						label = new Label(0, k, Utility.getLocaleValuePropByKey("lblNoRecord", locale)); 
						excelSheet.addCell(label);
					}
					
			if(listjobJobOrders.size()>0){
			 for(JobOrder jobOrder:listjobJobOrders) 
			 {
				col=1;
				label = new Label(0, k, jobOrder.getJobId().toString()); 
				excelSheet.addCell(label);
			
			    label = new Label(1, k, jobOrder.getJobTitle()); 
				excelSheet.addCell(label);
				
				
				label = new Label(2, k, ""+mapTotalCount.get(jobOrder.getJobId())); 
				excelSheet.addCell(label);
				
					
				    Map<String, Integer> mapsecoundryStatus = mapsscountwithJob.get(jobOrder.getJobId());
					for(String str : lstsecoundryStatusNames){
						 if(mapsecoundryStatus==null){
							 label = new Label(++col, k, "0"); 
								excelSheet.addCell(label);
						 }else{
							 if(mapsecoundryStatus.get(str)!=null && mapsecoundryStatus.get(str)!=0){
									 label = new Label(++col, k,mapsecoundryStatus.get(str)+""); 
									 excelSheet.addCell(label);
							 }
							 else{
									 label = new Label(++col, k,"0"); 
									 excelSheet.addCell(label);
							 }
						 }
					 }
				 k++;
			 }
			}
	
			workbook.write();
			workbook.close();
		}catch(Exception e){}
		
	 return fileName;
  }
	
  
	
	public String displayRecordsByEntityTypePDF(String endfromDate,String endtoDate,int jobOrderId,int districtOrSchoolId,int schoolId,String jobStatus,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport(endfromDate,endtoDate,jobOrderId, districtOrSchoolId, schoolId, jobStatus,noOfRow, pageNo, sortOrder, sortOrderType,basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}

	@SuppressWarnings("unchecked")
	public boolean generateCandidatePDReport(String endfromDate,String endtoDate,int jobOrderId,int districtOrSchoolId,int schoolId,String jobStatus,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String path,String realPath)
	{
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			String lblOfferMade = Utility.getLocaleValuePropByKey("lblOfferMade", locale);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			int sortingcheck=0;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				/*add By Ram Nath*/
				/*SimpleDateFormat sdf=new SimpleDateFormat("MM-dd-yyyy");
				Date endStartDate=null,endToDate=null;
				try{
					if(endfromDate.equals("") && districtOrSchoolId!=0){
						List<String[]> minEndStartDateFromJobOrder=jobOrderDAO.findMinJobEndDateFromJobOrderByDistrict(districtOrSchoolId,jobOrderId);
						for (Iterator it = minEndStartDateFromJobOrder.iterator(); it.hasNext();){
							Object[] row = (Object[]) it.next();
							endStartDate = (Date) row[1];
						}
											
					}else if(districtOrSchoolId!=0){
						endStartDate=sdf.parse(endfromDate);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(endtoDate.equals("") && districtOrSchoolId!=0){
					endToDate=new Date();
				}else if(districtOrSchoolId!=0){
					endToDate=sdf.parse(endtoDate);
				}*/
				SimpleDateFormat sdf=new SimpleDateFormat("MM-dd-yyyy");
				Date endStartDate=null,endToDate=null;
				if(endfromDate.equals("") && endtoDate.equals("") && districtOrSchoolId!=0){
					List<String[]> minEndStartDateFromJobOrder=jobOrderDAO.findMinJobEndDateFromJobOrderByDistrict(districtOrSchoolId,jobOrderId);
					for (Iterator it = minEndStartDateFromJobOrder.iterator(); it.hasNext();){
						 Object[] row = (Object[]) it.next();
						endStartDate = (Date) row[1];
						endToDate 	= (Date) row[0];
					}
					String endStartDate1 = endStartDate.toString();
					String[] parts 	= 	endStartDate1.split("-");
					
					String year		= 	parts[0];
					String month 	= 	parts[1];
					String[] day1	=	parts[2].split(" ");
					String day		=	day1[0];
					endStartDate1	= month+"-"+day+"-"+year;	
					endStartDate=sdf.parse(endStartDate1);

				}else if(districtOrSchoolId!=0){
					endStartDate=sdf.parse(endfromDate);
				}
				/*end By Ram Nath*/
				
				 /** set default sorting fieldName **/
				String sortOrderFieldName="jobTitle";
				String sortOrderNoField="jobTitle";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("jobTitle") )
					sortingcheck=1;
				
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			
			  //@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				
				List list = new ArrayList();
				Set<JobOrder> lstjoborders = new HashSet<JobOrder>();
				Map<Integer, Map<String,Integer>> mapsscountwithJob= new HashMap<Integer, Map<String,Integer>>();
				Map<Integer,Integer> mapTotalCount = new HashMap<Integer, Integer>(); 
				Map<Integer,String> mapSpecificStatus = new HashMap<Integer, String>();
				List<SecondaryStatus> secondaryStatusList = new ArrayList<SecondaryStatus>();
				List<JobOrder> listjobJobOrders = new ArrayList<JobOrder>();
				
				//System.out.println("endfromDate=="+endStartDate+" endtoDate=="+endToDate+" jobOrderId=="+jobOrderId);			
				List<JobOrder> lstwithFilterJOrder=jobOrderDAO.findJobOrderForReportByDistrict(districtOrSchoolId,jobOrderId,endStartDate,endToDate,schoolId);
				System.out.println("lstwithFilterJOrder size()=="+lstwithFilterJOrder.size());
				//System.out.println("entityID=="+entityID);
				
				if(entityID==2)
				{
					
					List<JobOrder> lstJobOrders =new ArrayList<JobOrder>();
					if(schoolId!=0){
						SchoolMaster  sclMaster=null;
						sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
					}
					
					/*by Ram nath*/
					lstwithFilterJOrder.containsAll(lstJobOrders);
					if(jobOrderId==0 && lstwithFilterJOrder.size()==0)
						lstwithFilterJOrder=null;	
					/*end by Ram nath*/
					list=jobForTeacherDAO.fintApplicntByStatus(districtMaster,jobStatus,sortingcheck,sortOrderStrVal,lstwithFilterJOrder);
				}
				else if(entityID==1)
				{
					if(districtOrSchoolId!=0){
						districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
						List<JobOrder> lstJobOrders =new ArrayList<JobOrder>();
						if(schoolId!=0){
							SchoolMaster  sclMaster=null;
							sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
							lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
							 
						 }
						
						/*by Ram nath*/
						lstwithFilterJOrder.containsAll(lstJobOrders);
						if(jobOrderId==0 && lstwithFilterJOrder.size()==0)
							lstwithFilterJOrder=null;	
						/*end by Ram nath*/
						list=jobForTeacherDAO.fintApplicntByStatus(districtMaster,jobStatus,sortingcheck,sortOrderStrVal,lstwithFilterJOrder);  
					 }
					}
					
				String []statusArray={"scomp" , "ecomp" , "vcomp"};
			    List<StatusMaster> lstStatusMasters = statusMasterDAO.findStatusByStatusByShortNames(statusArray);
			    secondaryStatusList= secondaryStatusDAO.findSecondaryStatusforSpecificStatusList(districtMaster,lstStatusMasters);
			    for(SecondaryStatus ss : secondaryStatusList)
			     {
			    	mapSpecificStatus.put(ss.getStatusMaster().getStatusId(), ss.getSecondaryStatusName());
			     }
			    
			    Iterator itr = list.iterator();
				if(list.size()>0){
					while(itr.hasNext()){
						Object[] obj = (Object[]) itr.next();
						JobOrder jobOrder=(JobOrder)obj[0];
						lstjoborders.add(jobOrder);
						
						
						if(!listjobJobOrders.contains(jobOrder)){
							listjobJobOrders.add(jobOrder);
						}
						
						SecondaryStatus secondaryStatus= null;
						StatusMaster statusMaster= null;
						if(obj[1]!=null)
							secondaryStatus=(SecondaryStatus) obj[1];
						
						statusMaster=(StatusMaster) obj[2];
						Integer secCount= (Integer) obj[4];
						Integer statusCount = (Integer) obj[5];
						
						int count=0;
						if(secCount!=0 && statusCount!=0){
							count=statusCount;
						}else{
							count=secCount+statusCount;
						}
						if(mapTotalCount.get(jobOrder.getJobId())!=null){
							mapTotalCount.put(jobOrder.getJobId(),mapTotalCount.get(jobOrder.getJobId())+count);
						}else{
							mapTotalCount.put(jobOrder.getJobId(), count);
						}
						
						if(secondaryStatus!=null && statusMaster==null){
							Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
							if(countValue==null){
								Map<String,Integer> tempcountVal = new HashMap<String, Integer>();
								tempcountVal.put(secondaryStatus.getSecondaryStatusName(), secCount);
								mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
							}else{
								countValue.put(secondaryStatus.getSecondaryStatusName(), secCount);
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								
							}
						}
						
						
						if(secondaryStatus==null && statusMaster!=null){
							String sDidplayStatusName="";
							if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
								sDidplayStatusName=Utility.getLocaleValuePropByKey("optAvble", locale); 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
								sDidplayStatusName=Utility.getLocaleValuePropByKey("lblTimedOut", locale); 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
								sDidplayStatusName=Utility.getLocaleValuePropByKey("lblDeclined", locale); 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
								sDidplayStatusName= Utility.getLocaleValuePropByKey("lblWithdrew", locale) ; 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
								sDidplayStatusName= Utility.getLocaleValuePropByKey("lblHired", locale);
							
							else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
						    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						    }
								 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
								sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							}
							  
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
								 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							 }
							
							
							Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
							if(countValue==null){
								Map<String,Integer> tempcountVal = new HashMap<String, Integer>();
								tempcountVal.put(sDidplayStatusName, statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
							}else{
								if(countValue.get(sDidplayStatusName)!=null){
									countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}else{
									countValue.put(sDidplayStatusName,statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}
							}
						}
						
						
						if(secondaryStatus!=null && statusMaster!=null){
							String sDidplayStatusName="";
							if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
								sDidplayStatusName=Utility.getLocaleValuePropByKey("optAvble", locale); 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
								sDidplayStatusName=statusMaster.getStatus();
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
								sDidplayStatusName=Utility.getLocaleValuePropByKey("lblTimedOut", locale); 
							}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
								sDidplayStatusName=Utility.getLocaleValuePropByKey("lblDeclined", locale) ; 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
								sDidplayStatusName=Utility.getLocaleValuePropByKey("lblWithdrew", locale); 
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
								sDidplayStatusName=Utility.getLocaleValuePropByKey("lblHired", locale) ;
							else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
						    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						    }
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
								sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							}
							else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
								 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
							}
							Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
							if(countValue==null){
								Map<String,Integer> tempcountVal = new HashMap<String, Integer>();
								tempcountVal.put(sDidplayStatusName, statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
							}else{
								if(countValue.get(sDidplayStatusName)!=null){
									countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}else{
									countValue.put(sDidplayStatusName,statusCount);
									mapsscountwithJob.put(jobOrder.getJobId(), countValue);
								}
							}
						}
					}
				}
				 if(sortOrder.equals("totalCandidate")){    
				     for(JobOrder jobOrder:listjobJobOrders) 
					 {
				    	 jobOrder.setFitScore(mapTotalCount.get(jobOrder.getJobId()));
					 }
				     
				     if(sortOrderTypeVal.equalsIgnoreCase("1"))
							Collections.sort(listjobJobOrders,JobOrder.jobOrderComparatorFitScoreDesc );
						else if(sortOrderTypeVal.equalsIgnoreCase("0"))
							Collections.sort(listjobJobOrders,JobOrder.jobOrderComparatorFitScoreAsc );
			      } 
			System.out.println(">>>>>>>>>>>>>>> listjobJobOrders.size() :: "+listjobJobOrders.size());
			
			
			List<String> lstsecoundryStatusNames = new ArrayList<String>();
			List<SecondaryStatus> lstSecondaryStatus=null;
			if(districtMaster!=null)
			     lstSecondaryStatus = secondaryStatusDAO.findSecondryStatusByDistrict(districtMaster);
			if(lstSecondaryStatus!=null && lstSecondaryStatus.size()>0){
				for(SecondaryStatus ss: lstSecondaryStatus)
				{
					if(lstsecoundryStatusNames.size()==0){
						lstsecoundryStatusNames.add(ss.getSecondaryStatusName());
					}else if(!lstsecoundryStatusNames.contains(ss.getSecondaryStatusName())){
						lstsecoundryStatusNames.add(ss.getSecondaryStatusName());
				    }
				}
				//Added by Gaurav Kumar
				if(lstsecoundryStatusNames!=null && lstsecoundryStatusNames.contains(lblOfferMade)){
					lstsecoundryStatusNames.remove(lblOfferMade);
					lstsecoundryStatusNames.add(lblOfferMade);
				}
				lstsecoundryStatusNames.add( Utility.getLocaleValuePropByKey("lblHired", locale) );
				lstsecoundryStatusNames.add( Utility.getLocaleValuePropByKey("lblIncomplete", locale) );
				lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("btnRjt", locale) );
				lstsecoundryStatusNames.add( Utility.getLocaleValuePropByKey("lblTimedOut", locale) );
				lstsecoundryStatusNames.add( Utility.getLocaleValuePropByKey("lblWithdrew", locale) );
				lstsecoundryStatusNames.add( Utility.getLocaleValuePropByKey("lblDeclined", locale) );
			}
			
				 
				String fontPath = realPath;
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma,5);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 6);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							//Color bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 15,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject(Utility.getLocaleValuePropByKey("msgApplicantsStatus", locale));
						document.addCreationDate();
						document.addTitle(Utility.getLocaleValuePropByKey("msgApplicantsStatus", locale));

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;


						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						//para[1] = new Paragraph(" ",font20bold);
						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						

						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];

						
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("headAppStByJoOdr", locale),font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						
						
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						
						
						document.add(new Phrase("\n"));

						//lstsecoundryStatusNames
						int totalColumn=2+lstsecoundryStatusNames.size()+1;
						float[] tblwidth =new float[totalColumn];
						 tblwidth[0]=.22f;
						 tblwidth[1]=.10f;
						 for(int i=2;i<totalColumn;i++)
							 tblwidth[i]=.10f;
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[totalColumn];
						cell = new PdfPCell[totalColumn];
				       //header
						para[0] = new Paragraph( Utility.getLocaleValuePropByKey("lblJobId", locale),font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[0].setBorder(1);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph( Utility.getLocaleValuePropByKey("lblJoTil", locale),font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[0].setBorder(1);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph(Utility.getLocaleValuePropByKey("msgTotalApplicants", locale),font10_10);
						cell[2]= new PdfPCell(para[1]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[2]);
						

						int colCount=2;
						for(String str : lstsecoundryStatusNames)
						{
							para[colCount] = new Paragraph(""+str,font10_10);
							cell[colCount]= new PdfPCell(para[colCount]);
							cell[colCount].setBackgroundColor(bluecolor);
							cell[colCount].setHorizontalAlignment(Element.ALIGN_LEFT);
							mainTable.addCell(cell[colCount]);
							colCount++;
						}
						document.add(mainTable);
						
						if(listjobJobOrders.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgNorecordfound", locale),font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 //cell[index].setBorder(1);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
				     
						if(listjobJobOrders.size()>0){
							 for(JobOrder jobOrder :listjobJobOrders) 
							 {
							  int index=0;
							  //float[] tblwidth1={.22f,.22f,.08f,.08f,.21f,.06f};
									
							  mainTable = new PdfPTable(tblwidth);
							  mainTable.setWidthPercentage(100);
							  para = new Paragraph[totalColumn];
							  cell = new PdfPCell[totalColumn];
							  
							 para[index] = new Paragraph(""+jobOrder.getJobId(),font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;

							 para[index] = new Paragraph(""+jobOrder.getJobTitle(),font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
							 
							 para[index] = new Paragraph(""+mapTotalCount.get(jobOrder.getJobId()),font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
							 
							 Map<String, Integer> mapsecoundryStatus = mapsscountwithJob.get(jobOrder.getJobId());
								for(String str : lstsecoundryStatusNames){
									 if(mapsecoundryStatus==null){
										 para[index] = new Paragraph(""+"0",font8bold);
										 cell[index]= new PdfPCell(para[index]);
										 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
										 mainTable.addCell(cell[index]);
										 index++;
										 
									 }else{
										 if(mapsecoundryStatus.get(str)!=null && mapsecoundryStatus.get(str)!=0){
												 para[index] = new Paragraph(""+mapsecoundryStatus.get(str),font8bold);
												 cell[index]= new PdfPCell(para[index]);
												 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
												 mainTable.addCell(cell[index]);
												 index++;
										  }
										 else{
												 para[index] = new Paragraph(""+"0",font8bold);
												 cell[index]= new PdfPCell(para[index]);
												 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
												 mainTable.addCell(cell[index]);
												 index++;
										 }
										 
									 }
								 }
							
						document.add(mainTable);
					  }
				}
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
			return true;
	}
	
	@SuppressWarnings("unchecked")
	public String displayRecordsByEntityTypePrintPreview(String endfromDate,String endtoDate,int jobOrderId,int districtOrSchoolId,int schoolId,String jobStatus,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		System.out.println("****************Print********************");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		String lblOfferMade = Utility.getLocaleValuePropByKey("lblOfferMade", locale);
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=0;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			/*add By Ram Nath*/
			/*SimpleDateFormat sdf=new SimpleDateFormat("MM-dd-yyyy");
			Date endStartDate=null,endToDate=null;
			try{
				if(endfromDate.equals("") && districtOrSchoolId!=0){
					List<String[]> minEndStartDateFromJobOrder=jobOrderDAO.findMinJobEndDateFromJobOrderByDistrict(districtOrSchoolId,jobOrderId);
					for (Iterator it = minEndStartDateFromJobOrder.iterator(); it.hasNext();){
						Object[] row = (Object[]) it.next();
						endStartDate = (Date) row[1];
					}
										
				}else if(districtOrSchoolId!=0){
					endStartDate=sdf.parse(endfromDate);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(endtoDate.equals("") && districtOrSchoolId!=0){
				endToDate=new Date();
			}else if(districtOrSchoolId!=0){
				endToDate=sdf.parse(endtoDate);
			}*/
			SimpleDateFormat sdf=new SimpleDateFormat("MM-dd-yyyy");
			Date endStartDate=null,endToDate=null;
			if(endfromDate.equals("") && endtoDate.equals("") && districtOrSchoolId!=0){
				List<String[]> minEndStartDateFromJobOrder=jobOrderDAO.findMinJobEndDateFromJobOrderByDistrict(districtOrSchoolId,jobOrderId);
				for (Iterator it = minEndStartDateFromJobOrder.iterator(); it.hasNext();){
					 Object[] row = (Object[]) it.next();
					endStartDate = (Date) row[1];
					endToDate 	= (Date) row[0];
				}
				String endStartDate1 = endStartDate.toString();
				String[] parts 	= 	endStartDate1.split("-");
				
				String year		= 	parts[0];
				String month 	= 	parts[1];
				String[] day1	=	parts[2].split(" ");
				String day		=	day1[0];
				endStartDate1	= month+"-"+day+"-"+year;	
				endStartDate=sdf.parse(endStartDate1);

			}else if(districtOrSchoolId!=0){
				endStartDate=sdf.parse(endfromDate);
			}
			/*end By Ram Nath*/
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------
		 /** set default sorting fieldName **/
			String sortOrderFieldName="jobTitle";
			String sortOrderNoField="jobTitle";

			if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("jobTitle") )
				sortingcheck=1;
			
			
			/**Start set dynamic sorting fieldName **/
			
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
		
		  //@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
			
			List list = new ArrayList();
			Set<JobOrder> lstjoborders = new HashSet<JobOrder>();
			Map<Integer, Map<String,Integer>> mapsscountwithJob= new HashMap<Integer, Map<String,Integer>>();
			Map<Integer,Integer> mapTotalCount = new HashMap<Integer, Integer>(); 
			Map<Integer,String> mapSpecificStatus = new HashMap<Integer, String>();
			List<SecondaryStatus> secondaryStatusList = new ArrayList<SecondaryStatus>();
			List<JobOrder> listjobJobOrders = new ArrayList<JobOrder>();
			
			//System.out.println("endfromDate=="+endStartDate+" endtoDate=="+endToDate+" jobOrderId=="+jobOrderId);			
			List<JobOrder> lstwithFilterJOrder=jobOrderDAO.findJobOrderForReportByDistrict(districtOrSchoolId,jobOrderId,endStartDate,endToDate,schoolId);
			System.out.println("lstwithFilterJOrder size()=="+lstwithFilterJOrder.size());
			//System.out.println("entityID=="+entityID);
			
			if(entityID==2)
			{
				
				List<JobOrder> lstJobOrders =new ArrayList<JobOrder>();
				if(schoolId!=0){
					SchoolMaster  sclMaster=null;
					sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
					lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
				}
				
				/*by Ram nath*/
				lstwithFilterJOrder.containsAll(lstJobOrders);
				if(jobOrderId==0 && lstwithFilterJOrder.size()==0)
					lstwithFilterJOrder=null;	
				/*end by Ram nath*/
				list=jobForTeacherDAO.fintApplicntByStatus(districtMaster,jobStatus,sortingcheck,sortOrderStrVal,lstwithFilterJOrder);
			}
			else if(entityID==1)
			{
				if(districtOrSchoolId!=0){
					districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					List<JobOrder> lstJobOrders =new ArrayList<JobOrder>();
					if(schoolId!=0){
						SchoolMaster  sclMaster=null;
						sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
						 
					 }
					/*by Ram nath*/
					lstwithFilterJOrder.containsAll(lstJobOrders);
					if(jobOrderId==0 && lstwithFilterJOrder.size()==0)
						lstwithFilterJOrder=null;	
					/*end by Ram nath*/
					list=jobForTeacherDAO.fintApplicntByStatus(districtMaster,jobStatus,sortingcheck,sortOrderStrVal,lstwithFilterJOrder);  
				 }
				}
				
			String []statusArray={"scomp" , "ecomp" , "vcomp"};
		    List<StatusMaster> lstStatusMasters = statusMasterDAO.findStatusByStatusByShortNames(statusArray);
		    secondaryStatusList= secondaryStatusDAO.findSecondaryStatusforSpecificStatusList(districtMaster,lstStatusMasters);
		    for(SecondaryStatus ss : secondaryStatusList)
		     {
		    	mapSpecificStatus.put(ss.getStatusMaster().getStatusId(), ss.getSecondaryStatusName());
		     }
		    
		    Iterator itr = list.iterator();
			if(list.size()>0){
				while(itr.hasNext()){
					Object[] obj = (Object[]) itr.next();
					JobOrder jobOrder=(JobOrder)obj[0];
					lstjoborders.add(jobOrder);
					
					
					if(!listjobJobOrders.contains(jobOrder)){
						listjobJobOrders.add(jobOrder);
					}
					
					SecondaryStatus secondaryStatus= null;
					StatusMaster statusMaster= null;
					if(obj[1]!=null)
						secondaryStatus=(SecondaryStatus) obj[1];
					
					statusMaster=(StatusMaster) obj[2];
					Integer secCount= (Integer) obj[4];
					Integer statusCount = (Integer) obj[5];
					
					int count=0;
					if(secCount!=0 && statusCount!=0){
						count=statusCount;
					}else{
						count=secCount+statusCount;
					}
					if(mapTotalCount.get(jobOrder.getJobId())!=null){
						mapTotalCount.put(jobOrder.getJobId(),mapTotalCount.get(jobOrder.getJobId())+count);
					}else{
						mapTotalCount.put(jobOrder.getJobId(), count);
					}
					
					if(secondaryStatus!=null && statusMaster==null){
						Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
						if(countValue==null){
							Map<String,Integer> tempcountVal = new HashMap<String, Integer>();
							tempcountVal.put(secondaryStatus.getSecondaryStatusName(), secCount);
							mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
						}else{
							countValue.put(secondaryStatus.getSecondaryStatusName(), secCount);
							mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							
						}
					}
					
					
					if(secondaryStatus==null && statusMaster!=null){
						String sDidplayStatusName="";
						if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
							sDidplayStatusName=Utility.getLocaleValuePropByKey("lblAvailable", locale); 
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
							sDidplayStatusName=statusMaster.getStatus();
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
							sDidplayStatusName=statusMaster.getStatus();
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
							sDidplayStatusName=Utility.getLocaleValuePropByKey("optTOut", locale); 
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
							sDidplayStatusName=Utility.getLocaleValuePropByKey("lblDeclined", locale); 
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
							sDidplayStatusName=Utility.getLocaleValuePropByKey("lblWithdrew", locale); 
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
							sDidplayStatusName=Utility.getLocaleValuePropByKey("lblHired", locale);
						
						else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
					    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
					    }
							 
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
							sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						}
						  
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
							 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						 }
						
						
						Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
						if(countValue==null){
							Map<String,Integer> tempcountVal = new HashMap<String, Integer>();
							tempcountVal.put(sDidplayStatusName, statusCount);
							mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
						}else{
							if(countValue.get(sDidplayStatusName)!=null){
								countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							}else{
								countValue.put(sDidplayStatusName,statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							}
						}
					}
					
					
					if(secondaryStatus!=null && statusMaster!=null){
						String sDidplayStatusName="";
						if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
							sDidplayStatusName=Utility.getLocaleValuePropByKey("lblAvailable", locale); 
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem")){
							sDidplayStatusName=statusMaster.getStatus();
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
							sDidplayStatusName=statusMaster.getStatus();
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
							sDidplayStatusName=Utility.getLocaleValuePropByKey("optTOut", locale); 
						}else if(statusMaster.getStatusShortName().equalsIgnoreCase("dcln"))
							sDidplayStatusName= Utility.getLocaleValuePropByKey("lblDeclined", locale); 
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("widrw"))
							sDidplayStatusName=Utility.getLocaleValuePropByKey("lblWithdrew", locale); 
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
							sDidplayStatusName=Utility.getLocaleValuePropByKey("lblHired", locale);
						else  if(statusMaster.getStatusShortName().equalsIgnoreCase("scomp")){
					    	sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
					    }
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("ecomp")){
							sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						}
						else if(statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")){
							 sDidplayStatusName=mapSpecificStatus.get(statusMaster.getStatusId());
						}
						Map<String,Integer> countValue = mapsscountwithJob.get(jobOrder.getJobId());
						if(countValue==null){
							Map<String,Integer> tempcountVal = new HashMap<String, Integer>();
							tempcountVal.put(sDidplayStatusName, statusCount);
							mapsscountwithJob.put(jobOrder.getJobId(), tempcountVal);
						}else{
							if(countValue.get(sDidplayStatusName)!=null){
								countValue.put(sDidplayStatusName,countValue.get(sDidplayStatusName)+statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							}else{
								countValue.put(sDidplayStatusName,statusCount);
								mapsscountwithJob.put(jobOrder.getJobId(), countValue);
							}
						}
					}
				}
			}
			 if(sortOrder.equals("totalCandidate")){    
			     for(JobOrder jobOrder:listjobJobOrders) 
				 {
			    	 jobOrder.setFitScore(mapTotalCount.get(jobOrder.getJobId()));
				 }
			     
			     if(sortOrderTypeVal.equalsIgnoreCase("1"))
						Collections.sort(listjobJobOrders,JobOrder.jobOrderComparatorFitScoreDesc );
					else if(sortOrderTypeVal.equalsIgnoreCase("0"))
						Collections.sort(listjobJobOrders,JobOrder.jobOrderComparatorFitScoreAsc );
		      } 
		
		
		List<String> lstsecoundryStatusNames = new ArrayList<String>();
		List<SecondaryStatus> lstSecondaryStatus=null;
		if(districtMaster!=null)
		     lstSecondaryStatus = secondaryStatusDAO.findSecondryStatusByDistrict(districtMaster);
		if(lstSecondaryStatus!=null && lstSecondaryStatus.size()>0){
			for(SecondaryStatus ss: lstSecondaryStatus)
			{
				if(lstsecoundryStatusNames.size()==0){
					lstsecoundryStatusNames.add(ss.getSecondaryStatusName());
				}else if(!lstsecoundryStatusNames.contains(ss.getSecondaryStatusName())){
					lstsecoundryStatusNames.add(ss.getSecondaryStatusName());
			    }
			}
			if(lstsecoundryStatusNames!=null && lstsecoundryStatusNames.contains(lblOfferMade)){
				lstsecoundryStatusNames.remove(lblOfferMade);
				lstsecoundryStatusNames.add(lblOfferMade);
			}
			lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("lblHired", locale));
			lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("lblIncomplete", locale));
			lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("btnRjt", locale));
			lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("optTOut", locale));
			lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("lblWithdrew", locale));
			lstsecoundryStatusNames.add(Utility.getLocaleValuePropByKey("lblDeclined", locale));
		}
		
	        tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'> "+Utility.getLocaleValuePropByKey("headAppStByJoOdr", locale)+"</div><br/>");
			
			tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");
			
			
			tmRecords.append("<table  id='tblGridApplicatStatus' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJobId", locale)+"</th>");
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJoTil", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgTotalApplicants", locale)+"</th>");
			for(String str : lstsecoundryStatusNames)
			{
			  tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+str+"</th>");
			}
			
			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(listjobJobOrders.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			}
		
			if(listjobJobOrders.size()>0){
			 for(JobOrder jobOrder:listjobJobOrders) 
			 {
			  tmRecords.append("<tr>");	
			  tmRecords.append("<td style='font-size:11px;'>"+jobOrder.getJobId()+"</td>");
				
				tmRecords.append("<td style='font-size:11px;'>"+jobOrder.getJobTitle()+"</td>");
				
				 tmRecords.append("<td style='font-size:11px;'>"+mapTotalCount.get(jobOrder.getJobId())+"</td>");
				
				 Map<String, Integer> mapsecoundryStatus = mapsscountwithJob.get(jobOrder.getJobId());
					for(String str : lstsecoundryStatusNames){
						 if(mapsecoundryStatus==null){
							 tmRecords.append("<td style='font-size:11px;'>0</td>");
							 
						 }else{
							 if(mapsecoundryStatus.get(str)!=null && mapsecoundryStatus.get(str)!=0){
									 tmRecords.append("<td style='font-size:11px;'>"+mapsecoundryStatus.get(str)+"</td>");
							 }
							 else{
								 tmRecords.append("<td style='font-size:11px;'>0</td>");
							 }
						 }
					 }
				
			  tmRecords.append("</tr>");
			 }
			}
	

		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
  }
	@Transactional(readOnly=true)
	public String getCandidateDetailsByJobId(int jobId){
		System.out.println("/**********************  getCandidateDetailsByJobId ****************/");
		StringBuffer sb=new StringBuffer();
		try{
			Criteria criteria = jobForTeacherDAO.getSession().createCriteria(jobForTeacherDAO.getPersistentClass());
			criteria.add(Restrictions.eq("jobId", jobOrderDAO.findById(jobId, false, false)));
			criteria.add(Restrictions.ne("status", WorkThreadServlet.statusMap.get("hide")));
			Criteria c1=criteria.createCriteria("teacherId");
			c1.addOrder(Order.asc("firstName"));
		List<JobForTeacher> lstjft=criteria.list();
		List<TeacherDetail> lstTD=new ArrayList<TeacherDetail>();
		for(JobForTeacher jft:lstjft){
			if(jft.getTeacherId()!=null)
				lstTD.add(jft.getTeacherId());
		}
	
		List<TeacherNormScore> lstTNS=teacherNormScoreDAO.findByCriteria(Restrictions.in("teacherDetail", lstTD));
		Map<Integer,Integer> normScoreMap=new HashMap<Integer,Integer>();
		for(TeacherNormScore tns:lstTNS){
			try{
				normScoreMap.put(tns.getTeacherDetail().getTeacherId(), tns.getTeacherNormScore());
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		sb.append(" <table  id='canDetails' width='100%' border='0'> ");
		sb.append("<thead class='bg'><tr><!--<th>"+Utility.getLocaleValuePropByKey("msgSrNo", locale)+"</th>--><th>"+Utility.getLocaleValuePropByKey("lblFirstName", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblLastName", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblEmailAddress", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblNormScore", locale)+"</th></tr></thead>");
		if(lstjft.size()>0){
			int sr=0;
			sb.append("<tbody>");
			for(JobForTeacher jft:lstjft){
				String normScore="";
				if(normScoreMap!=null)
					normScore=""+(normScoreMap.get(jft.getTeacherId().getTeacherId())!=null?normScoreMap.get(jft.getTeacherId().getTeacherId()):"");
				else
					normScore="";
					
			sb.append("<tr><!--<td>"+(++sr)+"</td>--><td>"+jft.getTeacherId().getFirstName()+"</td><td>"+jft.getTeacherId().getLastName()+"</td><td>"+jft.getTeacherId().getEmailAddress()+"</td><td>"+normScore+"</td></tr>");	
			}
			sb.append("</tbody>");
		}else{
		sb.append("<tbody><tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr></tbody>");
		}
		sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	@Transactional(readOnly=true)
	public String getCanDetialsByStatusId(int jobId, String searchValue){
		System.out.println("/**********************  getCanDetialsByStatusId ****************/");
		List<JobForTeacher> lstjft=new ArrayList<JobForTeacher>();
		List<String> dPointStatusMaster=new ArrayList<String>();
		dPointStatusMaster.add("vcomp");
		dPointStatusMaster.add("ecomp");
		dPointStatusMaster.add("scomp");
		
		StringBuffer sb=new StringBuffer();
		try{
			boolean statusFlag=false;
			boolean secStatusFlag=false;
	 	JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		 List<SecondaryStatus> lstsecStatus=secondaryStatusDAO.findByCriteria(Restrictions.eq("secondaryStatusName",searchValue),Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()),Restrictions.isNull("statusNodeMaster"),Restrictions.eq("status","A"));
		 if(searchValue.equals(Utility.getLocaleValuePropByKey("optAvble", locale)))
			 searchValue=Utility.getLocaleValuePropByKey("lblCompleted", locale);
		 List<StatusMaster> lstStatus=statusMasterDAO.findByCriteria(Restrictions.eq("status",searchValue));
		
		 if(lstsecStatus!=null && lstsecStatus.size()>0){
			 System.out.println("lstsecStatus size=="+lstsecStatus.size()+" lstStatus=="+lstStatus.size());
			 for(SecondaryStatus ss:lstsecStatus){
				 if(dPointStatusMaster.contains(ss.getStatusMaster().getStatusShortName()))
					 lstStatus.add(ss.getStatusMaster());
			     //System.out.print(ss.getSecondaryStatusId()+",");
			 }
		 }
		
		/* for(StatusMaster s:lstStatus)
			 System.out.println("s=="+s.getStatusId()+" s Name=="+s.getStatus());*/
		 if(lstsecStatus.size()>0 && lstStatus.size()>0){
			 statusFlag=true;
			 secStatusFlag=false;			 
		 }
		 else if(lstsecStatus.isEmpty() && lstStatus.size()>0){
			 statusFlag=true;
			 secStatusFlag=false;
		 } else if(lstsecStatus.size()>0 && lstStatus.isEmpty()){
			 statusFlag=false;
			 secStatusFlag=true;
		 }
		 
		 Criteria criteria = jobForTeacherDAO.getSession().createCriteria(jobForTeacherDAO.getPersistentClass());
		 criteria.add(Restrictions.eq("jobId", jobOrder));
		 criteria.add(Restrictions.ne("status", WorkThreadServlet.statusMap.get("hide")));
		 if(secStatusFlag)
			 criteria.add(Restrictions.in("secondaryStatus", lstsecStatus));
		 if(statusFlag)
			 criteria.add(Restrictions.in("statusMaster", lstStatus));
		 Criteria c1=criteria.createCriteria("teacherId");
			c1.addOrder(Order.asc("firstName"));
		 lstjft=criteria.list();
		 System.out.println("Status size=="+lstjft.size()); 
		 
		 List<TeacherDetail> lstTD=new ArrayList<TeacherDetail>();
			for(JobForTeacher jft:lstjft){
				if(jft.getTeacherId()!=null)
					lstTD.add(jft.getTeacherId());
			}
			List<TeacherNormScore> lstTNS = new ArrayList<TeacherNormScore>();
		    try{
			  lstTNS=teacherNormScoreDAO.findByCriteria(Restrictions.in("teacherDetail", lstTD));
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
			Map<Integer,Integer> normScoreMap=new HashMap<Integer,Integer>();
			for(TeacherNormScore tns:lstTNS){
				try{
					normScoreMap.put(tns.getTeacherDetail().getTeacherId(), tns.getTeacherNormScore());
				}catch(Exception e){
					e.printStackTrace();
				}
			}
	 	sb.append(" <table  id='canDetails' width='100%' border='0'> ");
		sb.append("<thead class='bg'><tr><!--<th>"+Utility.getLocaleValuePropByKey("msgSrNo", locale)+"</th>--><th>"+Utility.getLocaleValuePropByKey("lblFirstName", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblLname", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblEmailAddress", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblNormScore", locale)+"</th></tr></thead>");
		if(lstjft.size()>0){
			int sr=0;
			sb.append("<tbody>");
			for(JobForTeacher jft:lstjft){
				String normScore="";
				if(normScoreMap!=null)
					normScore=""+(normScoreMap.get(jft.getTeacherId().getTeacherId())!=null?normScoreMap.get(jft.getTeacherId().getTeacherId()):"");
				else
					normScore="";
					
			sb.append("<tr><!--<td>"+(++sr)+"</td>--><td>"+jft.getTeacherId().getFirstName()+"</td><td>"+jft.getTeacherId().getLastName()+"</td><td>"+jft.getTeacherId().getEmailAddress()+"</td><td>"+normScore+"</td></tr>");	
			}
			sb.append("</tbody>");
		}else{
		sb.append("<tbody><tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr></tbody>");
		}
		sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
	return sb.toString();
}
	
}


