package tm.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.util.UidGenerator;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.districtassessment.AssessmentNotesHistory;
import tm.bean.districtassessment.AssessmentsPriorityMaster;
import tm.bean.districtassessment.CenterMaster;
import tm.bean.districtassessment.CenterScheduleMaster;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.DistrictAssessmentJobRelation;
import tm.bean.districtassessment.ExaminationCodeDetails;
import tm.bean.districtassessment.PriorityAssessmentDays;
import tm.bean.districtassessment.TeacherDistrictAssessmentAnswerDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentOption;
import tm.bean.districtassessment.TeacherDistrictAssessmentQuestion;
import tm.bean.districtassessment.TeacherDistrictAssessmentStatus;
import tm.bean.districtassessment.TeacherDistrictAssessmentsPriority;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.districtassessment.AssessmentNotesHistoryDAO;
import tm.dao.districtassessment.AssessmentsPriorityMasterDAO;
import tm.dao.districtassessment.CenterMasterDAO;
import tm.dao.districtassessment.CenterScheduleMasterDAO;
import tm.dao.districtassessment.DistrictAssessmentDetailDAO;
import tm.dao.districtassessment.DistrictAssessmentJobRelationDAO;
import tm.dao.districtassessment.ExaminationCodeDetailsDAO;
import tm.dao.districtassessment.PriorityAssessmentDaysDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentAnswerDetailDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentDetailDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentStatusDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentsPriorityDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.services.teacher.AssessmentInviteThread;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class ExamAjax 
{
	
	
	 String locale = Utility.getValueOfPropByKey("locale");
	 String lblAssessmentName=Utility.getLocaleValuePropByKey("lblAssessmentName", locale);
	 String lblDistrictName=Utility.getLocaleValuePropByKey("lblDistrictName", locale);
	 String lblAssDTime=Utility.getLocaleValuePropByKey("lblAssDTime", locale);
	 
	@Autowired
	private  CenterScheduleMasterDAO centerScheduleMasterDAO;
	
	@Autowired
	private CenterMasterDAO centerMasterDAO;
		
	@Autowired
	private TeacherDistrictAssessmentDetailDAO teacherDistrictAssessmentDetailDAO;
	@Autowired
	private TeacherDistrictAssessmentAnswerDetailDAO teacherDistrictAssessmentAnswerDetailDAO;
	
	@Autowired
	private DistrictAssessmentDetailDAO districtAssessmentDetailDAO;
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService){
		this.emailerService = emailerService;
	}
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;

	@Autowired 
	private JobOrderDAO jobOrderDAO;

	@Autowired
	private ExaminationCodeDetailsDAO examinationCodeDetailsDAO;

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO; 

	@Autowired
	private DistrictAssessmentJobRelationDAO districtAssessmentJobRelationDAO;

	@Autowired
	private TeacherDistrictAssessmentStatusDAO teacherDistrictAssessmentStatusDAO;
	
	@Autowired
	private PriorityAssessmentDaysDAO priorityAssessmentDaysDAO;
	//add by Ram Nath
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;	
	//end bY Ram Nath
	
	@Autowired
	private AssessmentNotesHistoryDAO assessmentNotesHistoryDAO;
	
	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;
	
	@Autowired
	private AssessmentsPriorityMasterDAO assessmentsPriorityMasterDAO;
	
	@Autowired
	private TeacherDistrictAssessmentsPriorityDAO teacherDistrictAssessmentsPriorityDAO;
	
	@Transactional(readOnly=false)
	public String validateExamCode(String sCode)
	{
		System.out.println("Call validateExamCode");
		StringBuffer buffer=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<ExaminationCodeDetails> lstExam=new ArrayList<ExaminationCodeDetails>();
		if(sCode!=null && !sCode.equals(""))
		{
			lstExam=examinationCodeDetailsDAO.getExamCode(sCode);
			if(lstExam!=null && lstExam.size()==1)
			{
				boolean approved=lstExam.get(0).getApproved();
				if(!approved)
					return "2";
				else
				{
					TeacherPersonalInfo personalInfo=null;
					TeacherDetail teacherDetail=lstExam.get(0).getTeacherDetail();
					String sName="",sEmail="",sSSN="",sDOB="";;
					if(teacherDetail!=null)
					{
						if(teacherDetail.getFirstName()!=null && !teacherDetail.getFirstName().equals(""))
							sName=teacherDetail.getFirstName();
						if(teacherDetail.getLastName()!=null && !teacherDetail.getLastName().equals(""))
							sName+=" "+teacherDetail.getLastName();

						if(teacherDetail.getEmailAddress()!=null && !teacherDetail.getEmailAddress().equals(""))
							sEmail=teacherDetail.getEmailAddress();

						personalInfo=teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);

						if(personalInfo!=null)
						{
							if(personalInfo.getSSN()!=null && !personalInfo.getSSN().equals(""))
							{
								sSSN=personalInfo.getSSN();
								if(sSSN!=null && !sSSN.equals(""))
								{
									try { sSSN=Utility.decodeBase64(sSSN); } catch (IOException e) { e.printStackTrace(); }
									if(sSSN.length()>=4)
										sSSN=sSSN.substring(sSSN.length()-4, sSSN.length());
								}
								else
									sSSN="N/A";

							}

							if(personalInfo.getDob()!=null && !personalInfo.getDob().equals(""))
								sDOB=Utility.convertDateAndTimeToUSformatOnlyDate(personalInfo.getDob());
							else
								sDOB="N/A";
						}
					}

					String sDistrictName="";

					JobOrder jobOrder=null;
					if(lstExam.get(0).getJobOrder()!=null)
						jobOrder=lstExam.get(0).getJobOrder();
					String sJobTitle="";
					if(jobOrder!=null)
					{
						if(jobOrder!=null && !jobOrder.getJobTitle().equals(""))
							sJobTitle=jobOrder.getJobTitle();
						else
							sJobTitle="N/A";

						if(lstExam.get(0).getJobCategoryMaster().getDistrictMaster()!=null)
							sDistrictName=lstExam.get(0).getJobCategoryMaster().getDistrictMaster().getDistrictName();
					}

					buffer.append("<div class='row'>");
					buffer.append("<div class='col-sm-6'>");
					buffer.append("<div class='heading'>"+Utility.getLocaleValuePropByKey("lnkCandidateDetails", locale)+"</div>");
					buffer.append("</div>");
					buffer.append("</div>");

					buffer.append("<div class='row'>");
					buffer.append("<div class='col-sm-2'>");
					buffer.append(Utility.getLocaleValuePropByKey("lblCandidateName", locale)+" :");
					buffer.append("</div>");
					buffer.append("<div class='col-sm-4'>");
					buffer.append(sName);
					buffer.append("</div>");
					buffer.append("</div>");

					buffer.append("<div class='row'>");
					buffer.append("<div class='col-sm-2'>");
					buffer.append(Utility.getLocaleValuePropByKey("lblEmail1", locale)+ ":");
					buffer.append("</div>");
					buffer.append("<div class='col-sm-4'>");
					buffer.append(sEmail);
					buffer.append("</div>");
					buffer.append("</div>");

					buffer.append("<div class='row'>");
					buffer.append("<div class='col-sm-2'>");
					buffer.append( Utility.getLocaleValuePropByKey("msgExamAjax1", locale)+":");
					buffer.append("</div>");
					buffer.append("<div class='col-sm-4'>");
					buffer.append(sSSN);
					buffer.append("</div>");
					buffer.append("</div>");

					buffer.append("<div class='row'>");
					buffer.append("<div class='col-sm-2'>");
					buffer.append( Utility.getLocaleValuePropByKey("lblDOB", locale)+":");
					buffer.append("</div>");
					buffer.append("<div class='col-sm-4'>");
					buffer.append(sDOB);
					buffer.append("</div>");
					buffer.append("</div>");

					buffer.append("<div class='row'>");
					buffer.append("<div class='col-sm-6'>");
					buffer.append("<div class='heading'>"+Utility.getLocaleValuePropByKey("msgExaminationDetails", locale)+"</div>");
					buffer.append("</div>");
					buffer.append("</div>");

					if(sDistrictName!=null && !sDistrictName.equals(""))
					{
						buffer.append("<div class='row'>");
						buffer.append("<div class='col-sm-2'>");
						buffer.append("+lblDistrictName+"+":");
						buffer.append("</div>");
						buffer.append("<div class='col-sm-4'>");
						buffer.append(sDistrictName);
						buffer.append("</div>");
						buffer.append("</div>");
					}

					buffer.append("<div class='row'>");
					buffer.append("<div class='col-sm-2'>");
					buffer.append(Utility.getLocaleValuePropByKey("lblJoTil", locale)+":");
					buffer.append("</div>");
					buffer.append("<div class='col-sm-4'>");
					buffer.append(sJobTitle);
					buffer.append("</div>");
					buffer.append("</div>");

					String sEacmCode=lstExam.get(0).getExaminationCode();
					buffer.append("<div class='row'>");
					buffer.append("<div class='col-sm-2'>");
					buffer.append("Code:");
					buffer.append("</div>");
					buffer.append("<div class='col-sm-4'>");
					buffer.append(sEacmCode);
					buffer.append("</div>");
					buffer.append("</div>");

					int iTid=lstExam.get(0).getTeacherDetail().getTeacherId();
					int iJid=lstExam.get(0).getJobOrder().getJobId();
					int iAid=0;
					boolean bStartExam=false;

					String sStatus="N/A";
					//List<DistrictAssessmentJobRelation> assessmentJobRelations=new ArrayList<DistrictAssessmentJobRelation>();
					//assessmentJobRelations=districtAssessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
					
					DistrictAssessmentDetail districtAssessmentDetail=lstExam.get(0).getDistrictAssessmentDetail();
					if(districtAssessmentDetail!=null && districtAssessmentDetail.getDistrictAssessmentId()>0)
					{
						//if(assessmentJobRelations.get(0).getDistrictAssessmentDetail()!=null)
						//{
							//districtAssessmentDetail=assessmentJobRelations.get(0).getDistrictAssessmentDetail();
							iAid = districtAssessmentDetail.getDistrictAssessmentId();
							List<TeacherDistrictAssessmentStatus> assessmentStatus=new ArrayList<TeacherDistrictAssessmentStatus>();
							assessmentStatus=teacherDistrictAssessmentStatusDAO.findAssessmentTakenByTeacher(teacherDetail,districtAssessmentDetail);
							if(assessmentStatus!=null && assessmentStatus.size()>0)
							{
								if(assessmentStatus.get(0).getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
									sStatus=assessmentStatus.get(0).getStatusMaster().getStatus();
								else if(assessmentStatus.get(0).getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
									sStatus=assessmentStatus.get(0).getStatusMaster().getStatus();
								else{
									bStartExam=true;
									sStatus=assessmentStatus.get(0).getStatusMaster().getStatus();
								}
							}
							else
							{
								bStartExam=true;
							}
						//}
						/*else
						{
							sStatus="New";
							bStartExam=true;
						}*/
					}
					else
					{
						sStatus="No Assessement is attached";
						bStartExam=false;
					}

					buffer.append("<div class='row'>");
					buffer.append("<div class='col-sm-2'>");
					buffer.append("Status:");
					buffer.append("</div>");
					buffer.append("<div class='col-sm-4'>");
					buffer.append(sStatus);
					buffer.append("</div>");
					buffer.append("</div>");

					if(bStartExam)
					{
						buffer.append("<BR><div class='row'>");
						buffer.append("<div class='col-sm-6'>");
						System.out.println("iAid:: "+iAid);
						//buffer.append("<button class='btn fl btn-primary' style='padding: 6px 17px;'  type='submit' onclick='return startExam("+iTid+","+iJid+")'>Start Exam <i class='icon'></i></button>");
						buffer.append("<button class='btn fl btn-primary' style='padding: 6px 17px;'  type='submit' onclick='return checkDistrictInventory("+iTid+","+iJid+","+iAid+")'>"+Utility.getLocaleValuePropByKey("msgStartExam", locale)+" <i class='icon'></i></button>");
						buffer.append("</div>");
						buffer.append("</div>");
					}
					return buffer.toString();
				}

			}
		}
		System.out.println("lstExam "+lstExam.size());
		return "1";
	}
	public String getDistrictAssessmentDetails(Integer teacherId,Integer jobId)
	{

		System.out.println(":::::::::::::getDistrictAssessmentDetails::::::");

		UserMaster userMaster             =     null;
		DistrictMaster districtMaster     =     null;
		SchoolMaster schoolMaster         =     null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session1 = request.getSession(false);
		if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		if (session1.getAttribute("userMaster") != null) {
			userMaster=(UserMaster)session1.getAttribute("userMaster");
		}

		JobOrder jobOrder = null;
		if(jobId!=null){
			try{
				jobOrder    =    jobOrderDAO.findById(jobId, false, false);
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		
		boolean slotSelectionNeeded = false;
		
		if(districtMaster == null)
		{
			districtMaster = jobOrder.getDistrictMaster();
			if(districtMaster.getDistrictId()==4218990)
				slotSelectionNeeded = true;
		}
		
		
		
		TeacherDetail teacherDetail    =    teacherDetailDAO.findById(teacherId, false, false);   
		
		List<DistrictAssessmentJobRelation> districtAssessmentJobRelationList=districtAssessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
		Map<Integer,DistrictAssessmentJobRelation> mapDAssess=new HashMap<Integer, DistrictAssessmentJobRelation>();
		List<DistrictAssessmentDetail> districtAssessmentDetailList=new ArrayList<DistrictAssessmentDetail>();
		for (DistrictAssessmentJobRelation districtAssessmentJobRelation : districtAssessmentJobRelationList) {
			districtAssessmentDetailList.add(districtAssessmentJobRelation.getDistrictAssessmentDetail());
		}
		
		List<ExaminationCodeDetails> examinationCodeDetailsList = examinationCodeDetailsDAO.getExamCodeByDAssessmentAndTeacher(districtAssessmentDetailList,teacherDetail);
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatus = new ArrayList<TeacherDistrictAssessmentStatus>();
		teacherDistrictAssessmentStatus = teacherDistrictAssessmentStatusDAO.findAll();
		Map<String,Integer> mapAssessmentStatus = new HashMap<String,Integer>();
		if(teacherDistrictAssessmentStatus!=null && teacherDistrictAssessmentStatus.size()>0)
		{
			for(TeacherDistrictAssessmentStatus status : teacherDistrictAssessmentStatus)
			{
				mapAssessmentStatus.put(status.getTeacherDetail().getTeacherId()+"@@@"+status.getDistrictAssessmentDetail().getDistrictAssessmentId(), status.getStatusMaster().getStatusId());
			}
		}
		
		Map<Integer,ExaminationCodeDetails> mapExamCDetails=new HashMap<Integer, ExaminationCodeDetails>();
		if(examinationCodeDetailsList.size()>0){
			for (ExaminationCodeDetails examinationCodeDetails : examinationCodeDetailsList) {
				mapExamCDetails.put(examinationCodeDetails.getDistrictAssessmentDetail().getDistrictAssessmentId(), examinationCodeDetails);
			}
		}
		System.out.println(":::::::::::::::: districtAssessmentJobRelationList :::::::::::::::::::::"+districtAssessmentJobRelationList.size());

		StringBuffer sb = new StringBuffer();
		try{


			sb.append("<tr><td>");
			sb.append("<table border='0' id='assessmentInviteGridCG'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th valign='top'>"+lblAssessmentName+"</th>");
			if(examinationCodeDetailsList!=null && examinationCodeDetailsList.size()>0){
				sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgAssessmentDate", locale)+"</th>");
			}else{
				sb.append("<th valign='top'>&nbsp;</th>");
			}
			sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgInvite", locale)+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			String teacherFullName="";
			
			
			Set<Integer> districtASMTIds = new HashSet<Integer>();
			/////////////////////////////////////////////////////////////////
			if(districtAssessmentJobRelationList!=null && districtAssessmentJobRelationList.size()>0)
			{
				 districtASMTIds = displayAssessmentNameUrl(teacherDetail,districtAssessmentJobRelationList);
			}
			
			
			/////////////////////////////////////////////////////////////////
			
			/*for(DistrictAssessmentJobRelation dajr:districtAssessmentJobRelationList)
			{
				System.out.println(" MMMMMMMM<<<<<<<<<<<<<<<<<<>>>>>> "+dajr.getDistrictAssessmentDetail().getDistrictAssessmentId());
			}*/
			
			
			
			for(DistrictAssessmentJobRelation districtAssessmentJobRelation : districtAssessmentJobRelationList){
				ExaminationCodeDetails examinationCodeDetails = new ExaminationCodeDetails();
				try{
					teacherFullName             =     teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
					teacherFullName             =     teacherFullName.replace("'","\\'");

					examinationCodeDetails     =    mapExamCDetails.get(districtAssessmentJobRelation.getDistrictAssessmentDetail().getDistrictAssessmentId());
				}catch(Exception e){
					e.printStackTrace();
				}
				sb.append("<tr>" );
				
				
				if(districtASMTIds.contains(districtAssessmentJobRelation.getDistrictAssessmentDetail().getDistrictAssessmentId()))
					sb.append("<td width='100%'><a href='#' onclick=\"getAssessmentInviteQuestionAnswers('"+teacherId+"','"+jobId+"','"+districtAssessmentJobRelation.getDistrictAssessmentDetail().getDistrictAssessmentId()+"');\" >"+districtAssessmentJobRelation.getDistrictAssessmentDetail().getDistrictAssessmentName()+"</a></td>");
				else
					sb.append("<td width='100%'>"+districtAssessmentJobRelation.getDistrictAssessmentDetail().getDistrictAssessmentName()+"</td>");
				
				if(examinationCodeDetails!=null)
				{
					
					if(examinationCodeDetails.getExamDate()==null)
					{
						sb.append("<td width='100%'>N/A</td>");						
						sb.append("<td width='100%'><a href='#' onclick=\"sendAssessmentInvite("+teacherId+","+jobId+","+districtAssessmentJobRelation.getDistrictAssessmentDetail().getDistrictAssessmentId()+")\">"+Utility.getLocaleValuePropByKey("msgResendInvite", locale)+"</a></td>");
					}
					else
					{
						String slotDate = "";
						if(slotSelectionNeeded)
							slotDate = examinationCodeDetails.getExamStartTime()+" to "+examinationCodeDetails.getExamEndTime();
						
						if(examinationCodeDetails.getInviteStatus().equals("C"))
						{
							String key = examinationCodeDetails.getTeacherDetail().getTeacherId()+"@@@"+examinationCodeDetails.getDistrictAssessmentDetail().getDistrictAssessmentId();
							if(mapAssessmentStatus.get(key)!=null && mapAssessmentStatus.get(key)==3)
							{
								sb.append("<td width='100%'>"+Utility.convertDateAndTimeToUSformatOnlyDate(examinationCodeDetails.getExamDate())+"&nbsp;&nbsp;<br/>"+slotDate+"</td>");
								sb.append("<td width='100%'>"+Utility.getLocaleValuePropByKey("lblIncomplete", locale)+"</td>");
							}
							else if(mapAssessmentStatus.get(key)!=null && mapAssessmentStatus.get(key)==4)
							{
								sb.append("<td width='100%'>"+Utility.convertDateAndTimeToUSformatOnlyDate(examinationCodeDetails.getExamDate())+"&nbsp;&nbsp;<br/>"+slotDate+"</td>");
								sb.append("<td width='100%'>"+Utility.getLocaleValuePropByKey("lblCompleted", locale)+"</td>");
							}
							else if( mapAssessmentStatus.get(key)!=null && mapAssessmentStatus.get(key)==9)
							{
								sb.append("<td width='100%'>"+Utility.convertDateAndTimeToUSformatOnlyDate(examinationCodeDetails.getExamDate())+"&nbsp;&nbsp;<br/>"+slotDate+"</td>");
								sb.append("<td width='100%'>"+Utility.getLocaleValuePropByKey("lblTimedOut", locale)+"</td>");
							}
							else
							{
								sb.append("<td width='100%'>"+Utility.convertDateAndTimeToUSformatOnlyDate(examinationCodeDetails.getExamDate())+"&nbsp;&nbsp;<br/>"+slotDate+"</td>");
								if(slotSelectionNeeded)
									sb.append("<td width='100%'>"+Utility.getLocaleValuePropByKey("msgSlotConfirmed", locale)+"</td>");
								else
									sb.append("<td width='100%'>Invitation Sent</td>");
							}
						}
					}
				}
				else
				{
					sb.append("<td>&nbsp;</td>");
					sb.append("<td><a href='#' onclick=\"sendAssessmentInvite("+teacherId+","+jobId+","+districtAssessmentJobRelation.getDistrictAssessmentDetail().getDistrictAssessmentId()+")\">"+Utility.getLocaleValuePropByKey("msgSendInvite", locale)+"</a></td>");

				}
				sb.append("</tr>" );
			}
			if(districtAssessmentJobRelationList.size()==0){
				sb.append("<tr><td colspan='5'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			}
			sb.append("</table>");
			sb.append("</table>");

		} catch (Exception e){
			e.printStackTrace();
		}
		return sb.toString();

	}
	public String saveAssessmentInvite(Integer teacherId,Integer jobId,Integer districtAssessmentId,Integer centreScheduleId)
	{
		System.out.println(":::::::::::::::    saveAssessmentInvite-=::    :::::::::::::::::::;; ");
		List<UserMaster> userMasterList 			= 	new ArrayList<UserMaster>();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		String examinationCode = "";
		HttpSession session1 = request.getSession(false);
		CenterScheduleMaster centerScheduleMasterUpdate = null;
		CenterMaster centerMaster = null;
		ExaminationCodeDetails examinationCodeDetails = new ExaminationCodeDetails();
		if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		try {
			TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			DistrictAssessmentDetail districtAssessmentDetail=districtAssessmentDetailDAO.findById(districtAssessmentId, false, false);
			CenterScheduleMaster centerScheduleMaster=centerScheduleMasterDAO.findById(centreScheduleId, false, false);
			List<CenterScheduleMaster> centerScheduleMasterList=new ArrayList<CenterScheduleMaster>();
			System.out.println(":::::::::::::::::::1111111111::::::::::::::::::"+teacherDetail.getTeacherId());
			if(centerScheduleMaster!=null){
				System.out.println("centerScheduleMaster.getDate()::::"+centerScheduleMaster.getCsDate());
				centerMaster  = centerScheduleMaster.getCenterMaster();
				centerScheduleMasterList=centerScheduleMasterDAO.getScheduleByDate(centerScheduleMaster.getDistrictMaster(),centerScheduleMaster.getCsDate());
			}
			System.out.println("centerScheduleMasterList::::"+centerScheduleMasterList.size());
			List<String> timeList=new ArrayList<String>();
			
			for (CenterScheduleMaster centerScheduleMaster2 : centerScheduleMasterList) {
				timeList.add(centerScheduleMaster2.getCsTime());
			}
			int selectSlot=0;
			if(districtAssessmentDetail.getAssessmentSessionTime()!=null){
				try{
					selectSlot=districtAssessmentDetail.getAssessmentSessionTime()/1800;
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			List<String> dateAvailabilityCheck=Utility.getFilterTime(timeList, centerScheduleMaster.getCsTime(),selectSlot);
			
			String endTime="";
			String timeStrCheck="";
			String endNewTime="";
			if(dateAvailabilityCheck!=null){
				for (String dateStr : dateAvailabilityCheck) {
					System.out.println("dateStr::::::::"+dateStr);
					endTime=dateStr;
					timeStrCheck+="||"+dateStr+"||";
				}
			}
			System.out.println("endTime::"+endTime);
			System.out.println("timeStrCheck::"+timeStrCheck);
			System.out.println("timeStrCheck::"+timeStrCheck);
			for (CenterScheduleMaster centerScheduleMaster2 : centerScheduleMasterList) {
				int noOfApplicats=centerScheduleMaster2.getNoOfApplicantsScheduled()+1;
				System.out.println("noOfApplicats:==::"+noOfApplicats);
				try{
					if(timeStrCheck.contains("||"+centerScheduleMaster2.getCsTime()+"||")){
						centerScheduleMasterUpdate=centerScheduleMaster2;
						System.out.println(":::::::::::::::::::::::::::::::::::noOfApplicats:>>>>>>>>:centerScheduleMasterUpdate:::::::::::::"+centerScheduleMaster2.getCentreScheduleId());
						centerScheduleMasterUpdate.setNoOfApplicantsScheduled(noOfApplicats);
						centerScheduleMasterDAO.makePersistent(centerScheduleMasterUpdate);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			List<ExaminationCodeDetails> examinationCodeDetailsList = examinationCodeDetailsDAO.getInviteByDAssessmentAndTeacherAndJob(districtAssessmentDetail,teacherDetail,jobOrder);
			System.out.println("examinationCodeDetailsList:::>>>>>>:::::::>"+examinationCodeDetailsList.size());
			if(examinationCodeDetailsList.size()>0){   
				examinationCodeDetails =examinationCodeDetailsList.get(0);
				examinationCodeDetails.setExamDate(centerScheduleMaster.getCsDate()); // new added
				examinationCodeDetails.setExamStartTime(centerScheduleMaster.getCsTime());
				try {
					endNewTime = Utility.addMinutesToDateNew(centerScheduleMaster.getCsDate(),endTime,30);
				} catch (Exception e) {
					e.printStackTrace();
				}
				examinationCodeDetails.setExamEndTime(endNewTime);
				examinationCodeDetails.setInviteStatus("C");
				examinationCodeDetailsDAO.updatePersistent(examinationCodeDetails);
			}	
			List<DistrictKeyContact> districtKeyContactList= districtKeyContactDAO.findByContactType(jobOrder.getDistrictMaster(),"Center Incharge");			
			if(districtKeyContactList.size()>0){
				for(DistrictKeyContact districtKeyContact : districtKeyContactList){
					userMasterList.add(districtKeyContact.getUserMaster());
				}
			}
			System.out.println("userMasterList:::"+userMasterList.size());
			try{
			System.out.println("examinationCodeDetails:::"+examinationCodeDetails);
			}catch(Exception e){
				e.printStackTrace();
			}
			String IcalFileName=createIcalFile(Utility.getLocaleValuePropByKey("msgInvitecal", locale), "", examinationCodeDetails);			
			
			AssessmentInviteThread dsmt = new AssessmentInviteThread();			
			ExamAjax examAjax=new ExamAjax();
			dsmt.setExamAjax(examAjax);
			dsmt.setTeacherDetail(teacherDetail);
			dsmt.setEmailerService(emailerService);
			dsmt.setJobOrder(jobOrder);
			dsmt.setUserMasterList(userMasterList);
			dsmt.setRequest(request);
			dsmt.setExaminationCodeDetails(examinationCodeDetails);
			dsmt.setIcalFile(IcalFileName);
			dsmt.setCenterMaster(centerMaster);
			try {
				dsmt.start();	
			} catch (Exception e) {}

			return "1";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	/*** Add by Sekhar, This method using for send automatically mail ( Staus Updated )***/
	public int mailToTeacherByThread(JobOrder jobOrder, TeacherDetail teacherDetail,List<UserMaster> userMasterList ,HttpServletRequest request,ExaminationCodeDetails examinationCodeDetails,EmailerService emailerService,String IcalFile,CenterMaster centerMaster)
	{
		System.out.println("========== mailToTeacherByThread  Assessment Invite=||||==========="+userMasterList.size());
			try{
				System.out.println("teacherDetail.getEmailAddress():::"+teacherDetail.getEmailAddress());
				//Mail for Teacher 
				MailText mail=new MailText();				
				SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
				SimpleDateFormat formday=new SimpleDateFormat("EEEE");
				String dayofweek=formday.format(examinationCodeDetails.getExamDate());	
				String timeZone = "";
				if(centerMaster!=null)
				{
					timeZone = centerMaster.getTimeZoneMaster().getTimeZoneShortName();
				}
				else
				{
					timeZone = "CST";
				}
				String teacherContent=mail.mailTextForTeacherExamConfirmed(teacherDetail, examinationCodeDetails,timeZone);
				
				
				System.out.println(" Time Zone :: "+timeZone);
				String slotTime=examinationCodeDetails.getExamStartTime()+" To "+examinationCodeDetails.getExamEndTime()+" "+timeZone;
				String slotDate=dateFormat.format(examinationCodeDetails.getExamDate()).toString();	
				
				String teacherSubject=Utility.getLocaleValuePropByKey("msgExamAjax3", locale)+examinationCodeDetails.getJobOrder().getJobTitle()+": "+slotTime+" on "+dayofweek+", "+slotDate+"";
				String daSubject=Utility.getLocaleValuePropByKey("msgExamAjax4", locale)+" '"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"' "+Utility.getLocaleValuePropByKey("msgExamAjax6", locale)+examinationCodeDetails.getJobOrder().getJobTitle()+": "+slotTime+" on "+slotDate+"";
				
				System.out.println("teacherSubject "+teacherSubject);
				System.out.println("daSubject "+daSubject);
				List<String> lstBcc=new ArrayList<String>();
				List<String> lstDaBcc=new ArrayList<String>();
				try{
					String[] arrBccEmail=Utility.getValueOfSmtpPropByKey("smtphost.assessmentbccemail").split(",");
					for(int i=0; i<arrBccEmail.length;i++){
						if(arrBccEmail[i]!=null && !arrBccEmail[i].equals(""))
							lstBcc.add(arrBccEmail[i]);
							lstDaBcc.add(arrBccEmail[i]);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				emailerService.sendMailWithAttachmentsWithBCC(teacherDetail.getEmailAddress(), teacherSubject, "noreply@teachermatch.net", teacherContent,IcalFile.toString(),lstBcc);
				if(userMasterList.size()>0)
					for(UserMaster userMaster:userMasterList)
					{						
						try{		
							System.out.println();
							String daContent = mail.mailTextForDaExamConfirmed(teacherDetail, examinationCodeDetails,userMaster,timeZone);						
							System.out.println("da content "+daContent);
							System.out.println("teacher content "+teacherContent);
							emailerService.sendMailWithAttachmentsWithBCC(userMaster.getEmailAddress(), daSubject, "noreply@teachermatch.net", daContent,IcalFile.toString(),lstDaBcc);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				
				//Mail for DA 
				//emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),subject,templatetoSend);
				System.out.println("Mail set successfully");
			}catch (Exception e) {
				e.printStackTrace();
			}
		return 0;
	}
	public String getAssessmentInviteQuestionAnswers(Integer teacherId,Integer jobId,Integer districtAssessmentId)
	{
		System.out.println("*****:::::::::::::::||getAssessmentInviteQuestionAnswers||:::::::::::::::::::;; ");
		WebContext context;
		UserMaster userMaster = null;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		StringBuffer sb =new StringBuffer();
		int allScoringQuestion = 0;
		int completeQuestion = 0;
		List<AssessmentNotesHistory> notesHistories = new ArrayList<AssessmentNotesHistory>();
		Map<Integer,List<String>> notesMap = new HashMap<Integer, List<String>>();
		try{
			if(jobId!=0){
				JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
				TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);

				TeacherDistrictAssessmentStatus teacherDistrictAssessmentStatus = null;
				TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail = null;
				DistrictAssessmentDetail districtAssessmentDetail=districtAssessmentDetailDAO.findById(districtAssessmentId, false,false);
				if(districtAssessmentDetail!=null && teacherDetail!=null){
					teacherDistrictAssessmentStatus = teacherDistrictAssessmentStatusDAO.findAssessmentObjByTeacher(teacherDetail,districtAssessmentDetail);
				}
				String status = "";
				if(teacherDistrictAssessmentStatus==null){
					sb.append("<table><tr><td>"+Utility.getLocaleValuePropByKey("msgExamAjax5", locale)+"</td></tr></table>");
					return sb.toString();
				}else{
					status = teacherDistrictAssessmentStatus.getStatusMaster().getStatusShortName();
					teacherDistrictAssessmentDetail=teacherDistrictAssessmentStatus.getTeacherDistrictAssessmentDetail();
					if(status.equalsIgnoreCase("icomp"))
					{
						sb.append("<table><tr><td>"+Utility.getLocaleValuePropByKey("msgIncompleteStatus", locale)+"</td></tr></table>");
						return sb.toString();
					}else if(status.equalsIgnoreCase("vlt"))
					{
						sb.append("<table><tr><td>"+Utility.getLocaleValuePropByKey("lblTimedOut", locale)+"</td></tr></table>");
						return sb.toString();
					}
				}
				if(teacherDistrictAssessmentStatus!=null){
					TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQuestion = null;
					List<TeacherDistrictAssessmentOption> teacherDistrictAssessmentQuestionOptionsList = null;
					if(teacherDistrictAssessmentDetail!=null){
						List<TeacherDistrictAssessmentAnswerDetail> teacherDistrictAssessmentAnswerDetailList =teacherDistrictAssessmentAnswerDetailDAO.getAssessmentAnswerDetails(teacherDistrictAssessmentDetail,teacherDistrictAssessmentDetail.getTeacherDetail(),teacherDistrictAssessmentStatus);
						System.out.println("teacherDistrictAssessmentAnswerDetailList::::"+teacherDistrictAssessmentAnswerDetailList.size());
						String selectedOptions = "";
						long optionId = 0;
						String answerText = "";
						int questionCounter = 0;
						int sliderCounter = 0;
						int counter = 0;
						StringBuffer subsb = new StringBuffer();
						sb.append("<table border=0 width=970>");
						String shortName = "";
						int totalMarks = 0;
						int marksObtained = 0;
						float marksPercentage=0;
						
						int cnt 			= 	1;
						Double topMaxScore 	= 	0.0;
						Integer sliderScore	=	0;
						int interval 		= 	10;
						int dslider 		= 	0;
						int scoreProvided 	= 	0;
						Integer weightage = 0;
						Integer score = 0;
						if(teacherDistrictAssessmentStatus.getTotalMarks()!=null && teacherDistrictAssessmentStatus.getTotalMarks()>0)
							totalMarks=teacherDistrictAssessmentStatus.getTotalMarks().intValue();
						
						if(teacherDistrictAssessmentStatus.getMarksObtained()!=null && teacherDistrictAssessmentStatus.getMarksObtained()>0)
							marksObtained = teacherDistrictAssessmentStatus.getMarksObtained().intValue();
						
						
						if(marksObtained>0 && totalMarks>0)
						{
							marksPercentage = (float)((float)(marksObtained*100)/totalMarks);
						}
						/*notesHistories = assessmentNotesHistoryDAO.findDistrictAssessmentNotesByTeacher(teacherDetail, teacherDistrictAssessmentStatus.getDistrictAssessmentDetail());
						if(notesHistories!=null && notesHistories.size()>0)
						{
							for(AssessmentNotesHistory assessNotes : notesHistories)
							{
								List<String> questionWiseNotes = new ArrayList<String>();
								if(assessNotes.getAssessmentNotes()!=null && notesMap.containsKey(assessNotes.getTeacherDistrictAssessmentQuestion().getTeacherDistrictAssessmentQuestionId().intValue()))
								{
									questionWiseNotes = notesMap.get(assessNotes.getTeacherDistrictAssessmentQuestion().getTeacherDistrictAssessmentQuestionId().intValue());
									questionWiseNotes.add(assessNotes.getAssessmentNotes()+"###"+Utility.convertDateAndTimeFormatForEpiAndJsi(assessNotes.getCreatedDateTime()));
									notesMap.put(assessNotes.getTeacherDistrictAssessmentQuestion().getTeacherDistrictAssessmentQuestionId().intValue(), questionWiseNotes);
								}
								else
								{
									questionWiseNotes.add(assessNotes.getAssessmentNotes()+"###"+Utility.convertDateAndTimeFormatForEpiAndJsi(assessNotes.getCreatedDateTime()));
									notesMap.put(assessNotes.getTeacherDistrictAssessmentQuestion().getTeacherDistrictAssessmentQuestionId().intValue(), questionWiseNotes);
								}
							} 
						}*/
					//	allQuestionList = teacherDistrictAssessmentAnswerDetailList.size();
						for(TeacherDistrictAssessmentAnswerDetail teacherDistrictAssessmentAnswerDetail : teacherDistrictAssessmentAnswerDetailList) 
						{
							++questionCounter;
							counter++;
							String gridColor="style='background-color: #F2FAEF;'";
						
							if(counter%2==0){
								gridColor="style='background-color:white;'";
							}
							if(teacherDistrictAssessmentAnswerDetail.getMaxMarks().intValue()>=0)
							{
								completeQuestion++;
							}
							if(teacherDistrictAssessmentAnswerDetail.getTotalScore()!=null && teacherDistrictAssessmentAnswerDetail.getMaxMarks().intValue()>=0)
							{
								allScoringQuestion++;
							}
							teacherDistrictAssessmentQuestion = teacherDistrictAssessmentAnswerDetail.getTeacherDistrictAssessmentQuestion();
							selectedOptions = teacherDistrictAssessmentAnswerDetail.getSelectedOptions();

							String questionImage = teacherDistrictAssessmentQuestion.getQuestionImage();
							shortName=teacherDistrictAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName();
							
							subsb.append("<tr "+gridColor+">");
							subsb.append("<td width=500> ");
							subsb.append("<div style='padding-left:5px;'><b>"+Utility.getLocaleValuePropByKey("headQues", locale)+" "+questionCounter+":</b></div>");
							subsb.append("</td>");
						//	totalMarks+=teacherDistrictAssessmentAnswerDetail.getMaxMarks().intValue();
						//	marksObtained+=teacherDistrictAssessmentAnswerDetail.getTotalScore().intValue();
							if(teacherDistrictAssessmentAnswerDetail.getTotalScore()!=null)								
							subsb.append("<td width=180><b>"+teacherDistrictAssessmentAnswerDetail.getTotalScore().intValue()+"/"+teacherDistrictAssessmentAnswerDetail.getMaxMarks().intValue()+"</b>");
							else
								subsb.append("<td width=180><b>"+0+"/"+teacherDistrictAssessmentAnswerDetail.getMaxMarks().intValue()+"</b>");
							subsb.append("</td>");

							subsb.append("</tr>");

							subsb.append("<tr "+gridColor+">");
							subsb.append("<td width=100>");
							weightage = teacherDistrictAssessmentAnswerDetail.getTeacherDistrictAssessmentQuestion().getQuestionWeightage()!=null?teacherDistrictAssessmentAnswerDetail.getTeacherDistrictAssessmentQuestion().getQuestionWeightage().intValue():0;
							score = teacherDistrictAssessmentAnswerDetail.getTeacherDistrictAssessmentQuestion().getQuestionWeightage()!=null?teacherDistrictAssessmentAnswerDetail.getTeacherDistrictAssessmentQuestion().getMaxMarks().intValue():0;
							subsb.append("<span>");
							subsb.append("<span>");
							subsb.append("<div style='padding-left:5px;'>"+teacherDistrictAssessmentQuestion.getQuestion()+"</div>");
							subsb.append("</span>");
						
							if(shortName.equalsIgnoreCase("itq"))
							{
								subsb.append("<img src='showImage?image=teacher/"+teacherDetail.getTeacherId()+"/questions/"+questionImage+"'><br>");
							}
							subsb.append("</span>");
							
							subsb.append("</td>");
							subsb.append("<td>");
							subsb.append("</td>");
							subsb.append("</tr>");
							
							subsb.append("<tr "+gridColor+">");
							subsb.append("<td colspan='2'>");
							
							/*if(weightage==0 && score>0)
							{
								
							}*/
							subsb.append("</td>");
							subsb.append("</tr>");
							cnt++;
							teacherDistrictAssessmentQuestionOptionsList = teacherDistrictAssessmentQuestion.getTeacherDistrictAssessmentOptions();
							shortName=teacherDistrictAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName();

							if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel") || shortName.equalsIgnoreCase("lkts") || shortName.equalsIgnoreCase("itq"))
							{
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td> ");
								if(selectedOptions!=null && !selectedOptions.equals(""))
								{ 
									try{optionId = Long.parseLong(selectedOptions);}catch(Exception e){optionId = 0;}
								}
								else
									optionId = 0;
								subsb.append("<table>");
								for (TeacherDistrictAssessmentOption teacherDistrictAssessmentOption : teacherDistrictAssessmentQuestionOptionsList) {
									subsb.append("<tr ><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' disabled name='opt' value='"+teacherDistrictAssessmentOption.getTeacherDistrictAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;padding: 5px 3px;' >"+teacherDistrictAssessmentOption.getQuestionOption()+" ");
									if(optionId==teacherDistrictAssessmentOption.getTeacherDistrictAssessmentOptionId())
									{
										answerText=teacherDistrictAssessmentOption.getQuestionOption();
									}
								}
								subsb.append("</table>"); 
								subsb.append("</td>");
								subsb.append("<td></td>");
								subsb.append("</tr>");
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td width='100%'>");
								subsb.append("<div style='padding-left:5px;'><b>"+Utility.getLocaleValuePropByKey("msgAnswer", locale)+":</b></div>");
								subsb.append("</td>");
								subsb.append("<td></td>");
								subsb.append("</tr>");
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td width='100%'>");
								subsb.append("<div style='padding-bottom:10px;padding-left:5px;'>"+answerText+"</div>");
								subsb.append("</td>");
								subsb.append("<td></td>");
								subsb.append("</tr>");
								answerText="";
							}

							if(shortName.equalsIgnoreCase("it"))
							{
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td> ");
								subsb.append("<table >");
								for (TeacherDistrictAssessmentOption teacherDistrictQuestionOption : teacherDistrictAssessmentQuestionOptionsList) {
									subsb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='opt' value='"+teacherDistrictQuestionOption.getTeacherDistrictAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherDistrictQuestionOption.getQuestionOption()+"'> ");
								}
								subsb.append("</table>");
								subsb.append("</td>");
								subsb.append("<td></td>");
								subsb.append("</tr>");
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td width='100%'>");
								subsb.append("<b>"+Utility.getLocaleValuePropByKey("msgAnswer", locale)+":</b>");
								subsb.append("</td>");
								subsb.append("<td></td>");
								subsb.append("</tr>");
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td width='100%'>");
								subsb.append(answerText);
								subsb.append("</td>");
								subsb.append("<td></td>");
								subsb.append("</tr>");
								answerText="";
							}
							else if(shortName.equalsIgnoreCase("rt"))
							{
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td> ");
								int rank=1;
								subsb.append("<table>");
								for (TeacherDistrictAssessmentOption teacherDistrictQuestionOption : teacherDistrictAssessmentQuestionOptionsList) {
									subsb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' id='rnk"+rank+"' name='rank' class='span1' maxlength='1' onkeypress='return checkForIntUpto6(event)' onblur='checkUniqueRank(this);'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherDistrictQuestionOption.getQuestionOption()+"");
									subsb.append("</td></tr>");
									rank++;
								}
								subsb.append("</table >");
								subsb.append("</td>");
								subsb.append("<td></td>");
								subsb.append("</tr>");
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td width='100%'>");
								subsb.append("<b>"+Utility.getLocaleValuePropByKey("msgAnswer", locale)+":</b>");
								subsb.append("</td>");
								subsb.append("<td></td>");
								subsb.append("</tr>");
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td width='100%'>");
								subsb.append(answerText);
								subsb.append("</td>");
								subsb.append("<td></td>");
								subsb.append("</tr>");
								answerText="";
							}else if(shortName.equalsIgnoreCase("sl"))
							{
								answerText = teacherDistrictAssessmentAnswerDetail.getInsertedText()==null?"":teacherDistrictAssessmentAnswerDetail.getInsertedText();
								subsb.append("<tr "+gridColor+">");
								/*subsb.append("<td> ");
								subsb.append("&nbsp;&nbsp;<input type='text' name='opt' id='opt' class='span15' maxlength='75'/><br/>");
								subsb.append("</td>");*/
								subsb.append("<td></td>");
								subsb.append("</tr>");
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td width='100%'>");
								subsb.append("<b>"+Utility.getLocaleValuePropByKey("msgAnswer", locale)+":</b>");
								subsb.append("</td>");
								subsb.append("<td></td>");
								subsb.append("</tr>");
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td width='100%'>");
								subsb.append(answerText);
								subsb.append("</td>");
								subsb.append("<td></td>");
								subsb.append("</tr>");
								answerText="";
							}
							else if(shortName.equalsIgnoreCase("ml"))
							{
								answerText = teacherDistrictAssessmentAnswerDetail.getInsertedText()==null?"":teacherDistrictAssessmentAnswerDetail.getInsertedText();
								subsb.append("<tr "+gridColor+">");
								/*subsb.append("<td> ");
								subsb.append("&nbsp;&nbsp;<textarea name='opt' id='opt' class='form-control' maxlength='5000' rows='15' style='width:98%;margin:5px;' /></textarea><br/>");
								subsb.append("</td>");
								subsb.append("<td></td>");*/
								subsb.append("</tr>");
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td width='100%'>");
								subsb.append("<b>"+Utility.getLocaleValuePropByKey("msgAnswer", locale)+":</b>");
								subsb.append("</td>");
								subsb.append("<td></td>");
								subsb.append("</tr>");
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td width='100%'>");
								subsb.append(answerText);
								subsb.append("</td>");
								subsb.append("<td></td>");
								subsb.append("</tr>");
								answerText="";
							}
							if(weightage==0 && score>0 )
							{
								topMaxScore = teacherDistrictAssessmentQuestion.getMaxMarks();
								scoreProvided = teacherDistrictAssessmentAnswerDetail.getTotalScore()!=null?teacherDistrictAssessmentAnswerDetail.getTotalScore().intValue():0;
								sliderScore = topMaxScore.intValue();
								dslider 	= 0;

								if(sliderScore > 0)
									dslider = 1;

								if(sliderScore%10==0)
									interval=10;
								else if(sliderScore%5==0)
									interval=5;
								else if(sliderScore%3==0)
									interval=3;
								else if(sliderScore%2==0)
									interval=2;
								subsb.append("<tr "+gridColor+">");
								subsb.append("<td width='100%'>");
								subsb.append("<span>");
								if(teacherDistrictAssessmentAnswerDetail.getTotalScore()==null && teacherDistrictAssessmentAnswerDetail.getMaxMarks().intValue()==0){
								subsb.append("<div style='padding-left:5px;'><table><tr><td><input type='checkbox' id='scoreSet"+sliderCounter+"' name='scoreSet' onchange=\"showSlider('scoreSet"+sliderCounter+"',"+sliderCounter+","+interval+","+topMaxScore+","+scoreProvided+");showSaveScoreButton('scoreSet"+sliderCounter+"',"+sliderCounter+");\"><td/><td style='padding-top:4px;'>"+Utility.getLocaleValuePropByKey("lblWantSco", locale)+" ?</td></table></div>");
								}else{
									subsb.append("<div style='padding-left:5px;display:none;'><table><tr><td><input type='checkbox' id='scoreSet"+sliderCounter+"' name='scoreSet' onchange=\"showSlider('scoreSet"+sliderCounter+"',"+sliderCounter+","+interval+","+topMaxScore+","+scoreProvided+");showSaveScoreButton('scoreSet"+sliderCounter+"',"+sliderCounter+");\"><td/><td style='padding-top:4px;'>"+Utility.getLocaleValuePropByKey("lblWantSco", locale)+" ?</td></table></div>");
								}
								
								subsb.append("<div><input type='hidden' id='Q"+sliderCounter+"' value='"+teacherDistrictAssessmentAnswerDetail.getTeacherDistrictAssessmentQuestion().getTeacherDistrictAssessmentQuestionId()+"'></div>");
								subsb.append("</span>");								
								subsb.append("<table id='DiscScoreDiv"+sliderCounter+"' border=0 style='margin-bottom:15px;'><tr><td style='vertical-align:top;height:33px;padding-left:8px;border:0px;background-color: transparent;'><label><B>Score</B></label></td>");
								subsb.append("<td style='padding:5px;vertical-align:bottom;height:33px;border:0px;background-color: transparent;'>&nbsp;");
								subsb.append("<iframe id='Q"+sliderCounter+"ifrmStatusNote' src='slideract.do?name=statusNoteFrm&amp;tickInterval="+interval+"&amp;max="+topMaxScore+"&amp;swidth=536&amp;dslider="+0+"&amp;svalue="+scoreProvided+"' scrolling='no' frameborder='0' style='border: 0px; padding: 0px; margin:-13px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:580px;'></iframe>");						
								subsb.append("</td>");
								subsb.append("<td valign=top style='padding-top:4px; padding-left: 17px;border:0px;background-color: transparent;'><input type='hidden' name='Q"+sliderCounter+"dsliderchk' id='Q"+sliderCounter+"dsliderchk'  value="+dslider+">");
								subsb.append("</td></tr>");
								subsb.append("</table>");
								subsb.append("</td>");
								subsb.append("<td width='100%'>");
								subsb.append("</td>");
								subsb.append("</tr>");
								
								/*subsb.append("<tr  "+gridColor+">");
								subsb.append("<td width='100%'>");
								subsb.append("<table id='notesDiv"+sliderCounter+"' style='display:none;margin-bottom:15px;padding-left:5px;'><tr><td ><label><B>Note: </B></label></td><td  width='100%'>");
								if(teacherDistrictAssessmentAnswerDetail.getTotalScore()==null)
								{
									subsb.append("<textarea id='notes"+sliderCounter+"' name='notes"+sliderCounter+"' class='form-control' maxlength='5000' rows='5' style='width:61%;'></textarea>");
								}
								subsb.append("</td></td>");
								subsb.append("<td width='100%'>");
								subsb.append("</td>");
								subsb.append("</table>");
								subsb.append("<td width='100%'>");
								subsb.append("</td>");
								subsb.append("</tr>");
								
								subsb.append("<tr  "+gridColor+">");
								subsb.append("<td width='100%'>");
								int quesId = teacherDistrictAssessmentAnswerDetail.getTeacherDistrictAssessmentQuestion().getTeacherDistrictAssessmentQuestionId().intValue();
								if(notesMap.containsKey(quesId))
								{
									subsb.append("<table id='postedNotesDiv"+sliderCounter+"' style='margin-bottom:15px;padding-left:5px;'><tr><td width='5%'><label><B>Notes: </B></label></td>");
									List<String> retrieveNotes = new ArrayList<String>();
									retrieveNotes = notesMap.get(quesId);
									if(retrieveNotes!=null)
									{
										for(String s : retrieveNotes)
										{
											subsb.append("<tr style='border: 1px solid black;'>");
											subsb.append("<td width='50%' style='padding-left:10px;'>"+s.split("###")[0]+"</td>");
											subsb.append("<td width='30%'> - &nbsp;"+teacherDistrictAssessmentAnswerDetail.getTeacherDetail().getFirstName()+" "+teacherDistrictAssessmentAnswerDetail.getTeacherDetail().getLastName()+" ("+s.split("###")[1]+")</td>");
											subsb.append("</tr>");
										}
									}
									subsb.append("</td>");
									subsb.append("</table>");
									retrieveNotes.clear();
								}
								subsb.append("</td>");	
								subsb.append("<td  width='100%'></td>");
								subsb.append("</tr>");
								subsb.append("</tr>");*/
								sliderCounter++;
							}
							if(teacherDistrictAssessmentDetail.getQuestionSessionTime()!=null && teacherDistrictAssessmentDetail.getQuestionSessionTime()!=0)
							{
								//subsb.append("<script type=\"text/javascript\" language=\"javascript\">tmTimer("+teacherDistrictAssessmentDetail.getQuestionSessionTime()+",3);" );
								//subsb.append("document.getElementById('timer').style.display='block';");
								//subsb.append("</script>");
							}
						}
						notesMap.clear();
						subsb.append("</table>");
						System.out.println(" allScoringQuestion :: "+allScoringQuestion+" completeQuestion :: "+completeQuestion);
						if(allScoringQuestion < completeQuestion)
						{
							sb.append("<div style='text-align:right;'><b>"+Utility.getLocaleValuePropByKey("msgMarksObtained", locale)+": "+marksObtained+"/"+totalMarks+"<span class='required'>*</span>&nbsp;&nbsp;&nbsp;("+String.format("%.2f", marksPercentage)+"%)</b><br></div>");	
						}
						else
						{
							sb.append("<div style='text-align:right;'><b>"+Utility.getLocaleValuePropByKey("msgMarksObtained", locale)+": "+marksObtained+"/"+totalMarks+"&nbsp;&nbsp;&nbsp;("+String.format("%.2f", marksPercentage)+"%)</b><br></div>");
						}
						
						sb.append(subsb);
						sb.append("<input type='hidden' id='assessmentId' value='"+teacherDistrictAssessmentDetail.getDistrictAssessmentDetail().getDistrictAssessmentId()+"'/>");
						sb.append("<input type='hidden' id='candidateId' value='"+teacherId+"'/>");
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	public String displayDataOLD(Integer entityType,Integer districtIdFilter,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String date1, String date2, String firstName, String lastName, String emailAddress, String examCode,String teacherIdsInString)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		int permissionlockUnlock=0;
		
		if(session == null ||session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		//add By Ram nath
		else{
			try{
			List<DistrictKeyContact> districtKeyContactList= districtKeyContactDAO.findByContactType(userSession,"Center Incharge");
			if(districtKeyContactList.size()>0){
			permissionlockUnlock=1;
			}
			}catch(Throwable t){
				t.printStackTrace();
			}
		}
		//End By Ram Nath
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------

			UserMaster userMaster=null;
			DistrictMaster districtMaster=null;
			int roleId=0;

			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}

			if(userMaster.getDistrictId().getDistrictId()!=null){
				districtMaster = userMaster.getDistrictId();
			}

			String sortOrderFieldName	=	"examinationCode";
			String sortOrderNoField		=	"examinationCode";

			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("districtMaster")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("districtMaster"))
				{
					sortOrderNoField="districtMaster";
				}
			}

			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					if(sortOrderFieldName.equals("firstName"))
						sortOrderStrVal		=	Order.asc("td.firstName");
					else
						sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					if(sortOrderFieldName.equals("firstName"))
						sortOrderStrVal		=	Order.desc("td.firstName");
					else
						sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				if(sortOrderFieldName.equals("firstName"))
					sortOrderStrVal		=	Order.asc("td.firstName");
				else
					sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			System.out.println("sortOrderFieldName   "+sortOrderFieldName);

			date1 = (date1!=null)? ( (date1.equals("-1"))? null : date1.trim() ) : "";
			date2 = (date2!=null)? ( (date2.equals("-1"))? null : date2.trim() ) : "";
			firstName = (firstName!=null)? firstName.trim() : "";
			lastName = (lastName!=null)? lastName.trim() : "";
			emailAddress = (emailAddress!=null)? emailAddress.trim() : "";
			examCode = (examCode!=null)? examCode.trim() : "";

			List<ExaminationCodeDetails>sortedStsList=new ArrayList<ExaminationCodeDetails>();
			List<Integer> teacherIdList = new ArrayList<Integer>();

			if(teacherIdsInString!=null && !teacherIdsInString.equals(""))
				for(String teacherId : teacherIdsInString.split(","))
					teacherIdList.add(Integer.parseInt(teacherId.trim()));

				
			System.out.println("=========================>>>> teacherIdList "+teacherIdList.size());

			sortedStsList=examinationCodeDetailsDAO.getExaminationCodeDetails(districtMaster,date1,date2,firstName,lastName,emailAddress,examCode,start, noOfRowInPage,sortOrderStrVal,teacherIdList);
			totalRecord = examinationCodeDetailsDAO.getExaminationCodeDetailsCount(districtMaster,date1,date2,firstName,lastName,emailAddress,examCode,teacherIdList).size();

			System.out.println("sortedStsList   "+sortedStsList.size());
			dmRecords.append("<table  id='examCodeTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			//responseText=PaginationAndSorting.responseSortingLink("Teacher Name",sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
			//responseText=PaginationAndSorting.responseSortingLink("Teacher Name",sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='55%' valign='top'>"+responseText+"</th>");

			//responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderFieldName,"districtMaster",sortOrderTypeVal,pgNo);
			responseText=Utility.getLocaleValuePropByKey("lblJoTil", locale);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblExamCode", locale),sortOrderFieldName,"examinationCode",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblExamDate", locale),sortOrderFieldName,"examDate",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			/*responseText=PaginationAndSorting.responseSortingLink("Approved",sortOrderFieldName,"pathOfTagsIcon",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");*/

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgApprovedBy", locale),sortOrderFieldName,"approvedBy",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgApprovedDate", locale),sortOrderFieldName,"approvedDate",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			//responseText=PaginationAndSorting.responseSortingLink("Invite Send By",sortOrderFieldName,"pathOfTagsIcon",sortOrderTypeVal,pgNo);
			responseText=Utility.getLocaleValuePropByKey("msgInviteSentBy", locale);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			/*responseText=PaginationAndSorting.responseSortingLink("Invite Status",sortOrderFieldName,"pathOfTagsIcon",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink("status",sortOrderFieldName,"pathOfTagsIcon",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");*/

			dmRecords.append("<th width='15%' valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");

			if(sortedStsList.size()>0){
				for (ExaminationCodeDetails examinationCodeDetails : sortedStsList) {
					dmRecords.append("<tr>");

					if(examinationCodeDetails.getTeacherDetail()!=null && examinationCodeDetails.getTeacherDetail().getFirstName()!=null && examinationCodeDetails.getTeacherDetail().getLastName()!=null )
						dmRecords.append("<td width='55%'>"+examinationCodeDetails.getTeacherDetail().getFirstName()+" "+examinationCodeDetails.getTeacherDetail().getLastName()+"</td>");
					else
						dmRecords.append("<td width='55%'></td>");

					if(examinationCodeDetails.getJobOrder()!=null && examinationCodeDetails.getJobOrder().getJobTitle()!=null)
						dmRecords.append("<td width='15%'><div style='width:100%; height:25px; overflow:hidden' data-popover='true' data-container='body' data-content='"+examinationCodeDetails.getJobOrder().getJobTitle()+"'>"+examinationCodeDetails.getJobOrder().getJobTitle()+"</div></td>");
					else
						dmRecords.append("<td width='15%'></td>");
					//dmRecords.append("<td width='15%'>"+examinationCodeDetails.getJobCategoryMaster().getJobCategoryName()+"</td>");

					if(examinationCodeDetails.getExaminationCode()!=null)
						dmRecords.append("<td width='15%'>"+examinationCodeDetails.getExaminationCode()+"</td>");
					else
						dmRecords.append("<td width='15%'></td>");

					if(examinationCodeDetails.getExamDate()!=null)
						dmRecords.append("<td width='15%'>"+Utility.convertDateAndTimeToUSformatOnlyDate(examinationCodeDetails.getExamDate())+"</td>");
					else
						dmRecords.append("<td width='15%'></td>");
					//dmRecords.append("<td width='15%'>"+examinationCodeDetails.getApproved()+"</td>");

					if(examinationCodeDetails.getApprovedBy()!=null && examinationCodeDetails.getApprovedBy().getFirstName()!=null && examinationCodeDetails.getApprovedBy().getLastName()!=null)
						dmRecords.append("<td width='15%'>"+examinationCodeDetails.getApprovedBy().getFirstName()+" "+examinationCodeDetails.getApprovedBy().getLastName()+"</td>");
					else
						dmRecords.append("<td width='15%'></td>");

					if(examinationCodeDetails.getApprovedDate()!=null)
						dmRecords.append("<td width='15%'>"+Utility.convertDateAndTimeToUSformatOnlyDate(examinationCodeDetails.getApprovedDate())+"</td>");
					else
						dmRecords.append("<td width='15%'></td>");

					if(examinationCodeDetails.getUserMaster()!=null && examinationCodeDetails.getUserMaster().getFirstName()!=null && examinationCodeDetails.getUserMaster().getLastName()!=null)
						dmRecords.append("<td width='15%'>"+examinationCodeDetails.getUserMaster().getFirstName()+" "+examinationCodeDetails.getUserMaster().getLastName()+"</td>");
					else
						dmRecords.append("<td width='15%'></td>");
					//dmRecords.append("<td width='15%'>"+examinationCodeDetails.getStatus()+"</td>");

					if(examinationCodeDetails.getApproved())
						dmRecords.append("<td width='15%'> <a href='#' id='"+examinationCodeDetails.getCodeId()+"' name='Disapprove' onclick='return showConfirmationpopup("+examinationCodeDetails.getCodeId()+","+permissionlockUnlock+");'>"+Utility.getLocaleValuePropByKey("msgLock", locale)+"</a> </td>"); //Change Approved/disapprove to Lock/Unlock
					else
						dmRecords.append("<td width='15%'> <a href='#' id='"+examinationCodeDetails.getCodeId()+"' name='Approve' onclick='return showConfirmationpopup("+examinationCodeDetails.getCodeId()+","+permissionlockUnlock+");'>"+Utility.getLocaleValuePropByKey("msgUnlock", locale)+"</a> </td>");//Change  Approved/disapprove to Lock/Unlock
					dmRecords.append("</tr>");
				}


			}else{
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgExamAjax2", locale)+"</td></tr>" );
			}

			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	public String displayData(Integer entityType,Integer districtIdFilter,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String date1, String date2, String firstName, String lastName, String emailAddress, String examCode,String teacherIdsInString)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		int permissionlockUnlock=0;
		
		if(session == null ||session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		//add By Ram nath
		else{
			try{
			List<DistrictKeyContact> districtKeyContactList= districtKeyContactDAO.findByContactType(userSession,"Center Incharge");
			if(districtKeyContactList.size()>0){
			permissionlockUnlock=1;
			}
			}catch(Throwable t){
				t.printStackTrace();
			}
		}
		//End By Ram Nath
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------

			UserMaster userMaster=null;
			DistrictMaster districtMaster=null;
			int roleId=0;

			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}

			if(userMaster.getDistrictId().getDistrictId()!=null){
				districtMaster = userMaster.getDistrictId();
			}

			String sortOrderFieldName	=	"examinationCode";
			String sortOrderNoField		=	"examinationCode";

			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("districtMaster")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("districtMaster"))
				{
					sortOrderNoField="districtMaster";
				}
			}

			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					if(sortOrderFieldName.equals("firstName"))
						sortOrderStrVal		=	Order.asc("td.firstName");
					else
						sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					if(sortOrderFieldName.equals("firstName"))
						sortOrderStrVal		=	Order.desc("td.firstName");
					else
						sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				if(sortOrderFieldName.equals("firstName"))
					sortOrderStrVal		=	Order.asc("td.firstName");
				else
					sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			System.out.println("sortOrderFieldName   "+sortOrderFieldName);

			date1 = (date1!=null)? ( (date1.equals("-1"))? null : date1.trim() ) : "";
			date2 = (date2!=null)? ( (date2.equals("-1"))? null : date2.trim() ) : "";
			firstName = (firstName!=null)? firstName.trim() : "";
			lastName = (lastName!=null)? lastName.trim() : "";
			emailAddress = (emailAddress!=null)? emailAddress.trim() : "";
			examCode = (examCode!=null)? examCode.trim() : "";

			List<ExaminationCodeDetails>sortedStsList=new ArrayList<ExaminationCodeDetails>();
			List<Integer> teacherIdList = new ArrayList<Integer>();

			if(teacherIdsInString!=null && !teacherIdsInString.equals(""))
				for(String teacherId : teacherIdsInString.split(","))
					teacherIdList.add(Integer.parseInt(teacherId.trim()));

				
			System.out.println("=========================>>>> teacherIdList "+teacherIdList.size());

			sortedStsList=examinationCodeDetailsDAO.getExaminationCodeDetails(districtMaster,date1,date2,firstName,lastName,emailAddress,examCode,start, noOfRowInPage,sortOrderStrVal,teacherIdList);
			totalRecord = examinationCodeDetailsDAO.getExaminationCodeDetailsCount(districtMaster,date1,date2,firstName,lastName,emailAddress,examCode,teacherIdList).size();

			System.out.println("sortedStsList   "+sortedStsList.size());
			dmRecords.append("<table  id='examCodeTable' align='center' class='tableborder'>");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr style='height:44px;color:white'>");
			String responseText=Utility.getLocaleValuePropByKey("lblApplicantName", locale);
			dmRecords.append("<th width='15%' valign='top' style='padding-top: 1%;padding-left: 1%;'>"+responseText+"</th>");

			responseText=Utility.getLocaleValuePropByKey("lblJoTil", locale);
			dmRecords.append("<th width='19%' valign='top' style='padding-top: 1%;'>"+responseText+"</th>");

			responseText=Utility.getLocaleValuePropByKey("lblAssessmentCode", locale);
			dmRecords.append("<th width='12.5%' valign='top' style='padding-top: 1%;'>"+responseText+"</th>");

			responseText=lblAssDTime;
			dmRecords.append("<th width='17%' valign='top' style='padding-top: 1%;'>"+responseText+"</th>");

			responseText=Utility.getLocaleValuePropByKey("msgApprovedBy", locale);
			dmRecords.append("<th width='10%' valign='top' style='padding-top: 1%;'>"+responseText+"</th>");

			responseText=Utility.getLocaleValuePropByKey("msgApprovedDate", locale);
			dmRecords.append("<th width='8%' valign='top' style='padding-top: 1%;'>"+responseText+"</th>");

			responseText=Utility.getLocaleValuePropByKey("msgInviteSentBy", locale);
			dmRecords.append("<th width='12.5%' valign='top' style='padding-top: 1%;'>"+responseText+"</th>");

			dmRecords.append("<th width='6%' valign='top' style='padding-top: 1%;'>"+Utility.getLocaleValuePropByKey("msgMarksObtained", locale)+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			
			int i=0;
			if(sortedStsList.size()>0){
				for (ExaminationCodeDetails examinationCodeDetails : sortedStsList) {
					
					i++;
					if(i%2!=0)
						dmRecords.append("<tr style='background-color:#F2FAEF; text-align:left;'>");
					else
						dmRecords.append("<tr style='background-color:#FFFFFF; text-align:left;'>");
					

					if(examinationCodeDetails.getTeacherDetail()!=null && examinationCodeDetails.getTeacherDetail().getFirstName()!=null && examinationCodeDetails.getTeacherDetail().getLastName()!=null )
						dmRecords.append("<td style='padding-left: 1%;'>"+examinationCodeDetails.getTeacherDetail().getFirstName()+" "+examinationCodeDetails.getTeacherDetail().getLastName()+"</td>");
					else
						dmRecords.append("<td></td>");

					if(examinationCodeDetails.getJobOrder()!=null && examinationCodeDetails.getJobOrder().getJobTitle()!=null)
						dmRecords.append("<td>"+examinationCodeDetails.getJobOrder().getJobTitle()+"</td>");
					else
						dmRecords.append("<td></td>");
					
					if(examinationCodeDetails.getExaminationCode()!=null)
						dmRecords.append("<td>"+examinationCodeDetails.getExaminationCode()+"</td>");
					else
						dmRecords.append("<td></td>");

					if(examinationCodeDetails.getExamDate()!=null)
						dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(examinationCodeDetails.getExamDate())+"/ "+examinationCodeDetails.getExamStartTime()+"-"+examinationCodeDetails.getExamEndTime()+"</td>");
					else
						dmRecords.append("<td></td>");
					
					if(examinationCodeDetails.getApprovedBy()!=null && examinationCodeDetails.getApprovedBy().getFirstName()!=null && examinationCodeDetails.getApprovedBy().getLastName()!=null)
						dmRecords.append("<td>"+examinationCodeDetails.getApprovedBy().getFirstName()+" "+examinationCodeDetails.getApprovedBy().getLastName()+"</td>");
					else
						dmRecords.append("<td></td>");

					if(examinationCodeDetails.getApprovedDate()!=null)
						dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(examinationCodeDetails.getApprovedDate())+"</td>");
					else
						dmRecords.append("<td></td>");

					if(examinationCodeDetails.getUserMaster()!=null && examinationCodeDetails.getUserMaster().getFirstName()!=null && examinationCodeDetails.getUserMaster().getLastName()!=null)
						dmRecords.append("<td>"+examinationCodeDetails.getUserMaster().getFirstName()+" "+examinationCodeDetails.getUserMaster().getLastName()+"</td>");
					else
						dmRecords.append("<td></td>");
					
					if(examinationCodeDetails.getApproved())
						dmRecords.append("<td> <a href='#' id='"+examinationCodeDetails.getCodeId()+"' name='Disapprove' onclick='return showConfirmationpopup("+examinationCodeDetails.getCodeId()+","+permissionlockUnlock+");'>"+Utility.getLocaleValuePropByKey("msgLock", locale)+"</a> </td>"); //Change Approved/disapprove to Lock/Unlock
					else
						dmRecords.append("<td> <a href='#' id='"+examinationCodeDetails.getCodeId()+"' name='Approve' onclick='return showConfirmationpopup("+examinationCodeDetails.getCodeId()+","+permissionlockUnlock+");'>"+Utility.getLocaleValuePropByKey("msgUnlock", locale)+"</a> </td>");//Change  Approved/disapprove to Lock/Unlock
					dmRecords.append("</tr>");
				}


			}else{
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgExamAjax2", locale)+"</td></tr>" );
			}

			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	public JSONArray[] displayTeacherAssesmentTime(String startDate,String examCode)
	{
		JSONArray jsonArray = new JSONArray();
		JSONArray[] myJSONArray = new JSONArray[2];
		/*JSONArray jsonArray = new JSONArray();
		JSONArray priorityDayArray = new JSONArray();*/
		JSONArray jsonArrayColor = new JSONArray();

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		if(session == null ||session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		try{
			UserMaster userMaster=null;
			DistrictMaster districtMaster=null;
			int roleId=0;

			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}

			if(userMaster.getDistrictId().getDistrictId()!=null){
				districtMaster = userMaster.getDistrictId();
			}

			examCode = examCode==null? "": examCode.trim();

			//date format is:- mm-dd-yy, e.g:- 02-17-2015
			startDate = startDate==null? "" : startDate.trim();
			String endDate="";

			Calendar currentDate = Calendar.getInstance();
			if(startDate.equals(""))
				currentDate.setTime(new Date());	//set current Date.
			else
				currentDate.setTime(Utility.getCurrentDateFormart(startDate));	//set Date.
			
			startDate="";
			endDate="";
			
			currentDate.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);	//go to the first day of week
			startDate = getDate(currentDate);
			currentDate.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);	//go to the first day of week
			endDate = getDate(currentDate);
			
			System.out.println("StartDate:- "+startDate+", endDate:- "+endDate);
			
			List<ExaminationCodeDetails> examinationCodeDetailsList =new ArrayList<ExaminationCodeDetails>();
			examinationCodeDetailsList = examinationCodeDetailsDAO.getExaminationCodeDetails(districtMaster,startDate,endDate,null,null,null,examCode,null,null,null,null);
			
			NavigableMap<Integer, String> timeStamp = new TreeMap<Integer, String>();	//key format:- hr*100+min
			Map<String, String> tableContent = new LinkedHashMap<String, String>();		//key format:- dd-mm-yy_time(hh:min AM);
			
			int timeIntervalInMin=30;

			timeStamp.put(800,"8:00 AM");
			timeStamp.put(830,"8:30 AM");
			timeStamp.put(900,"9:00 AM");
			timeStamp.put(930,"9:30 AM");
			timeStamp.put(1000,"10:00 AM");
			timeStamp.put(1030,"10:30 AM");
			timeStamp.put(1100,"11:00 AM");
			timeStamp.put(1130,"11:30 AM");
			timeStamp.put(1200,"12:00 PM");
			timeStamp.put(1230,"12:30 PM");
			timeStamp.put(1300,"1:00 PM");
			timeStamp.put(1330,"1:30 PM");
			timeStamp.put(1400,"2:00 PM");
			timeStamp.put(1430,"2:30 PM");
			timeStamp.put(1500,"3:00 PM");
			timeStamp.put(1530,"3:30 PM");
			timeStamp.put(1600,"4:00 PM");
			timeStamp.put(1630,"4:30 PM");
			timeStamp.put(1700,"5:00 PM");
			
			
			
			for(ExaminationCodeDetails examinationCodeDetails : examinationCodeDetailsList)
			{
				Date StartTime = getDate(examinationCodeDetails.getExamStartTime());
				Date endTime = getDate(examinationCodeDetails.getExamEndTime());
				
				if(StartTime!=null && endTime!=null)
				{
				
						Calendar examCalendar = Calendar.getInstance();
						examCalendar.setTime(examinationCodeDetails.getExamDate());
						
						Calendar StartTimeCalendar = Calendar.getInstance();
						Calendar endTimeCalendar = Calendar.getInstance();
						
						StartTimeCalendar.setTime(StartTime);
						endTimeCalendar.setTime(endTime);
						
						int examDte =  examCalendar.get(Calendar.DATE);
						int examMonth = examCalendar.get(Calendar.MONTH)+1;
						int examYear = examCalendar.get(Calendar.YEAR);
						
						int hr = StartTimeCalendar.get(Calendar.HOUR_OF_DAY);
						int min = StartTimeCalendar.get(Calendar.MINUTE);
						
						int assessmentTimeInMin=1;
						assessmentTimeInMin = (int)( ( endTimeCalendar.getTimeInMillis() - StartTimeCalendar.getTimeInMillis() )/(1000*60) );
						if(assessmentTimeInMin<1)
							assessmentTimeInMin=1;
						
						//calculating the no of slot
						int numOfSlot = assessmentTimeInMin/timeIntervalInMin; //assessmentTimeInMin/30;
						if(assessmentTimeInMin%timeIntervalInMin!=0)
							numOfSlot = numOfSlot+1;
						
						//add teacher into multiple slot.(if he is present in multiple slot.)
						Integer timeStampKey = hr*100+min;	//0, 30, 100, 130
						while(numOfSlot>0)
						{
							Entry<Integer, String> timeStampEntry = timeStamp.floorEntry(timeStampKey);
							String time = timeStampEntry.getValue();

							String tableContentKey = Utility.getFormatDate(examinationCodeDetails.getExamDate())+"_"+time; //examDte+"-"+examMonth+"-"+examYear+"_"+time;
							String tableContentValue = tableContent.get(tableContentKey);
							if(tableContentValue==null)
								tableContentValue = ""+examinationCodeDetails.getCodeId();
							else
								tableContentValue = tableContentValue+","+examinationCodeDetails.getCodeId();
							tableContent.put(tableContentKey, tableContentValue);
							try{
								timeStampKey = timeStamp.higherEntry(timeStampEntry.getKey()).getKey();	//getting the next time stamp.
							}
							catch(NullPointerException npe)
							{
								timeStampKey = timeStamp.firstKey();	//getting the first time stamp.
								examCalendar.add(Calendar.DATE, 1);	//move to next day and set new date,month,year to examDte,examMonth,examYear
								examDte = examCalendar.get(Calendar.DATE);
								examMonth = examCalendar.get(Calendar.MONTH)+1;
								examYear = examCalendar.get(Calendar.YEAR);
							}
							numOfSlot--;
						}
				}
			}
			
			int id=0;		
			String sDate="";
			String startTime = "";
			String endTime = "";			
			System.out.println("Before centerScheduleMasters startDate:- "+startDate+", endDate:- "+endDate);
			List<CenterScheduleMaster> centerScheduleMasters = centerScheduleMasterDAO.getScheduleByDistrictAndDate(districtMaster, startDate, endDate);
			List<PriorityAssessmentDays> priorityAssessmentDays=priorityAssessmentDaysDAO.getPriorityDaysByDistrictAndWeekly(districtMaster,startDate,endDate);
			Map<String,Integer> mapPriority=new HashMap<String, Integer>();
			for (PriorityAssessmentDays priorityAssessmentDays2 : priorityAssessmentDays) {
				String stDate = Utility.getFormatDate(priorityAssessmentDays2.getPreferredDates())+"##"+priorityAssessmentDays2.getAvailableSlots();
				//System.out.println(priorityAssessmentDays2.getPriority().getPriorityId().intValue()+"::::::::::::::stDate::::::::::::::::::::::::::::::::::"+stDate);
				mapPriority.put(stDate, priorityAssessmentDays2.getPriority().getPriorityId().intValue());
			}
			//List<CenterScheduleMaster> centerScheduleMasters = centerScheduleMasterDAO.getScheduleByDistrict(districtMaster);
			if(centerScheduleMasters!=null && centerScheduleMasters.size()>0)
			{				
				for(CenterScheduleMaster csm : centerScheduleMasters)
				{
					
					String tableContentKey = Utility.getFormatDate(csm.getCsDate())+"_"+csm.getCsTime();
					String tableContentValue = tableContent.get(tableContentKey);
					if(tableContentValue!=null)
					{
						String stDate = Utility.getFormatDate(csm.getCsDate());
						String sTime = stDate+" "+csm.getCsTime();
						String eTime = stDate+" "+csm.getCsTime();
						id++;
						JSONObject datasource = new JSONObject();
						datasource.put("title",tableContentValue.split(",").length+" Candidate(s)");
						datasource.put("tIds",tableContentValue);
						datasource.put("start",sTime);
						datasource.put("end",eTime);
												
						JSONObject jsoncolors = new JSONObject();
						jsoncolors.put("text", "");
						jsoncolors.put("value", csm.getCentreScheduleId());
						jsoncolors.put("color", "green");
						jsonArrayColor.add(jsoncolors);
						jsonArray.add(datasource);
					}
					else
					{
						try
						{
							startDate = Utility.getDateWithoutTime(csm.getCsDate());	
						}
						catch(Exception e){e.printStackTrace();}
						try{
							startTime = Utility.convert12HourrTo24HourWithFormat(csm.getCsTime(),"hh:mm a");
						} catch (Exception e) {
							e.printStackTrace();
						}
						try{
							endTime = Utility.addMinutesToDateNew(csm.getCsDate(),csm.getCsTime(),30);
								//System.out.println(" startTime :: "+startTime+" ||||||||| endTime :: "+endTime);
						} catch (Exception e) {
							e.printStackTrace();
						}
						JSONObject datasource = new JSONObject();
						String startDateTime = startDate+" "+startTime;
						String endDateTime = startDate+" "+endTime;
						//System.out.println(" startDateTime :: "+startDateTime+" "+endDateTime);

						datasource.put("centerId",csm.getCentreScheduleId());
						datasource.put("start",startDateTime);
						datasource.put("end",endDateTime);
						String stDate = Utility.getFormatDate(csm.getCsDate())+"##"+csm.getCsTime();
						
						int priority=0;
						if(mapPriority.get(stDate)!=null){
							priority=mapPriority.get(stDate);
						}
					//	System.out.println(priority+" :::::::stDate:::::::"+stDate);
						if(csm.getAvailability()!=null && csm.getAvailability()==1)
						{
							// datasource.put("color","green");
							datasource.put("title","");
							JSONObject jsoncolors = new JSONObject();
							jsoncolors.put("text", Utility.getLocaleValuePropByKey("msgAvailabilityLocked", locale));
							jsoncolors.put("value", csm.getCentreScheduleId());
							if(priority==2){
								jsoncolors.put("color", "#00CC99");
							}else{
								jsoncolors.put("color", "green");
							}
							jsonArrayColor.add(jsoncolors);
						}
						else if(csm.getAvailability()!=null && csm.getAvailability()==0)
						{
							// datasource.put("color","green");
							datasource.put("title","");
							JSONObject jsoncolors = new JSONObject();
							jsoncolors.put("text", Utility.getLocaleValuePropByKey("msgUnavailabilityLocked", locale));
							jsoncolors.put("value", csm.getCentreScheduleId());
							jsoncolors.put("color", "#ebebeb");
							jsonArrayColor.add(jsoncolors);
						}
						else
						{
							//datasource.put("color","white");
							datasource.put("title","");
							JSONObject jsoncolors = new JSONObject();
							jsoncolors.put("text", Utility.getLocaleValuePropByKey("optAvble", locale));
							jsoncolors.put("value", csm.getCentreScheduleId());
							jsoncolors.put("color", "white");
							jsonArrayColor.add(jsoncolors);
						}
						jsonArray.add(datasource);
					}
				}
			}			
		}
		catch (Exception e)
		{ e.printStackTrace(); }
		
	//	System.out.println("jsonArray:- "+jsonArray);
		myJSONArray[0] = jsonArray;
		myJSONArray[1] = jsonArrayColor;
		return myJSONArray;
	}
	
	public String getDate(Calendar currentDate)
	{
		try
		{
			String startDate="";
			int date = currentDate.get(Calendar.DATE);
			int month = currentDate.get(Calendar.MONTH)+1;
			int year = currentDate.get(Calendar.YEAR);
			
			if(month>=1 && month<=9)
				startDate=startDate+"0"+month+"-";
			else
				startDate=startDate+month+"-";
			
			if(date>=1 && date<=9)
				startDate=startDate+"0"+date+"-";
			else
				startDate=startDate+date+"-";
			startDate=startDate+year;
			
			return startDate;
		}
		catch(Exception e){}
		return null;
	}
	
	public Date getDate(String time)
	{
		if(time==null)
			return null;
		
		try
		{
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
			return simpleDateFormat.parse(time);
		}
		catch(Exception e){e.printStackTrace();}
		
		try
		{
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mma");
			return simpleDateFormat.parse(time);
		}
		catch(Exception e){e.printStackTrace();}
		
		return null;
	}

	public String ApproveAndDisapprove(Integer codeId)
	{
		try
		{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||session.getAttribute("userMaster")==null) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");

			ExaminationCodeDetails examinationCodeDetails=examinationCodeDetailsDAO.findById(codeId, false, false);
			if(examinationCodeDetails!=null)
			{
				examinationCodeDetails.setApproved( !examinationCodeDetails.getApproved() );
				examinationCodeDetails.setApprovedBy(userMaster);
				examinationCodeDetails.setApprovedDate(new Date());

				examinationCodeDetailsDAO.makePersistent(examinationCodeDetails);
			}
		}
		catch(Exception e)
		{e.printStackTrace();}

		return "";
	}

	public String getAssessmentDetails(Integer districtAssessmentId, Integer centreScheduleId)
	{
		StringBuffer gridData =	new StringBuffer();
		try
		{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||session.getAttribute("userMaster")==null) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");
			DistrictAssessmentDetail districtAssessmentDetail =districtAssessmentDetailDAO.findAssessmentDetail(districtAssessmentId);
			gridData.append("Time :"+Utility.timeConversion(districtAssessmentDetail.getAssessmentSessionTime()));
			gridData.append("<input type=\"text\"  name=\"districtAssessmentIdForSlot\" id=\"districtAssessmentIdForSlot\"/ value=\""+districtAssessmentId+"\">");
			gridData.append("<input type=\"text\"  name=\"centreScheduleIdForSlot\" id=\"centreScheduleIdForSlot\"/ value='"+centreScheduleId+"'>");
		}
		catch(Exception e)
		{e.printStackTrace();}
		
		return gridData.toString();
	}
	
	public String sendAssessmentInvite(Integer teacherId,Integer jobId,Integer districtAssessmentId)
	{
		System.out.println(":::::::::::::::::sendAssessmentInvite::::::::::");
		System.out.println(" "+teacherId+" "+jobId+" "+districtAssessmentId);
		StringBuffer gridData =	new StringBuffer();
		String forMated= "";
		List<ExaminationCodeDetails> examinationCodeDetailList = new ArrayList<ExaminationCodeDetails>();
		String baseURL=Utility.getValueOfPropByKey("basePath");
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||session.getAttribute("userMaster")==null) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			DistrictAssessmentDetail districtAssessmentDetail  = districtAssessmentDetailDAO.findById(districtAssessmentId, false, false);
			
			try {
				examinationCodeDetailList = examinationCodeDetailsDAO.getExamCodeByDAssessAndTeacher(districtAssessmentDetail,teacherDetail);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			boolean slotSelectionNeeded = false;
			
			if(districtAssessmentDetail.getDistrictMaster().getDistrictId()==4218990)
				slotSelectionNeeded = true;
			
 			String emailAddress = teacherDetail.getEmailAddress();
			System.out.println(" teacher emailAddress :: "+emailAddress);
			String linkIds = emailAddress+"###"+districtAssessmentId+"###"+jobId+"###"+slotSelectionNeeded;
			
			forMated = Utility.encodeInBase64(linkIds);
			String subject = "click here to login for assessment slot selection";
			forMated = baseURL+"examslotselection.do?id="+forMated;
			if(!slotSelectionNeeded)
			{
				subject = "click here to login for assessment";
			}
			
			System.out.println("formatted url ::  "+forMated);
			String assessmentLink="<a href='"+forMated+"'> "+subject+"</a>";
			ExaminationCodeDetails examinationCodeDetails = null;
			
				try {
					if(examinationCodeDetailList.size()==0)
					{
						String examinationCode ="";						
						JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
						examinationCodeDetails =new ExaminationCodeDetails();
						examinationCodeDetails.setDistrictAssessmentDetail(districtAssessmentDetail);
						examinationCodeDetails.setTeacherDetail(teacherDetail);
						examinationCodeDetails.setJobOrder(jobOrder);
						examinationCodeDetails.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
						examinationCode = Utility.randomString(16);
						examinationCodeDetails.setExaminationCode(examinationCode);
						examinationCodeDetails.setStatus("A");
						examinationCodeDetails.setUserMaster(userMaster);
						
						if(slotSelectionNeeded)
						{
							examinationCodeDetails.setExamDate(null);
							examinationCodeDetails.setInviteStatus("I");
							examinationCodeDetails.setApproved(false); 
						}else
						{
							examinationCodeDetails.setExamDate(new Date());
							examinationCodeDetails.setInviteStatus("C");
							examinationCodeDetails.setApproved(true);
						}
						
						examinationCodeDetails.setCreatedDateTime(new Date());
						//examinationCodeDetails.setIpaddress(IPAddressUtility.getIpAddress(request));
						 examinationCodeDetails.setIpaddress(request.getRemoteAddr());
						examinationCodeDetailsDAO.makePersistent(examinationCodeDetails);
						System.out.println(" Invite Saved :: ");
						gridData.append(teacherDetail.getFirstName()+" "+teacherDetail.getLastName());
					}else{
						examinationCodeDetails=examinationCodeDetailList.get(0);
					}
					try {
						List<String> lstBcc=new ArrayList<String>();
						if(slotSelectionNeeded)
						{
							try{
								String[] arrBccEmail=Utility.getValueOfSmtpPropByKey("smtphost.assessmentbccemail").split(",");
								for(int i=0; i<arrBccEmail.length;i++){
									if(arrBccEmail[i]!=null && !arrBccEmail[i].equals(""))
										lstBcc.add(arrBccEmail[i]);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
						String content = "";
						content = MailText.mailTextForExamSlotSelection(examinationCodeDetails,assessmentLink);						
						DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom(Utility.getValueOfSmtpPropByKey("smtphost.adminusername"));
						dsmt.setLstBcc(lstBcc);
						dsmt.setMailto(teacherDetail.getEmailAddress());						
						dsmt.setMailsubject( Utility.getLocaleValuePropByKey("msgExamAjax8", locale) );
						dsmt.setMailcontent(content);
						System.out.println("content  "+content);
						try {
							dsmt.start();	
						} catch (Exception e) {}
				} catch (Exception e) {
				e.printStackTrace();
			}
		}
			catch(Exception e)
			{e.printStackTrace();}
		
		return gridData.toString();
	}
	public String createIcalFile(String subject,String description,ExaminationCodeDetails  examinationCodeDetails) throws ParseException 
	{
		System.out.println("Ical File");
		net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
		String calFileName = String.valueOf(System.currentTimeMillis()).substring(6);
		File calFile = new File(Utility.getValueOfPropByKey("iclcalendarRootPath")+"/"+calFileName+"cal.ics");
		String email="";
		String fromdate="";
		String fromtime="";
		String enddate="";
		String endtime="";
		String googleStartTime="";
		String googleEndTime="";
		String location="";
		//EventSchedule eventShedule=eventschlist.get(0);
		if(examinationCodeDetails!=null)
		{					
			System.out.println("examinationCodeDetails:::::"+examinationCodeDetails.getCodeId());
			//location=eventShedule.getLocation();		
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			fromdate=dateFormat.format(examinationCodeDetails.getExamDate());
			enddate=dateFormat.format(examinationCodeDetails.getExamDate());
			fromtime=examinationCodeDetails.getExamStartTime();
			endtime=examinationCodeDetails.getExamEndTime();
			
			//convert time AM/PM to 24 hour
			SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
			SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
			Date sdate = parseFormat.parse(fromtime);
			Date edate = parseFormat.parse(endtime);
			String[] convertStartTime =displayFormat.format(sdate).split(":");
			String[] convertEndTime =displayFormat.format(edate).split(":");
			
			/////StartTime
			int Atime=Integer.parseInt(convertStartTime[0]);
			int Btime=Integer.parseInt(convertStartTime[1]);
			
			/////EndTime
			int Ctime=Integer.parseInt(convertEndTime[0]);
			int Dtime=Integer.parseInt(convertEndTime[1]);
			
			String[] convertStartDates =fromdate.split("-");
			String[] convertEndDates =enddate.split("-");
			
			/////////Start Date       
			
			if(Integer.parseInt(convertStartDates[1])==1)
			{			
				convertStartDates[1]="12";
				convertStartDates[0]=String.valueOf((Integer.parseInt(convertStartDates[0])-1));
			}
			else
			{
				convertStartDates[1]=String.valueOf((Integer.parseInt(convertStartDates[1])-1));			
			}
			int A=(Integer.parseInt(convertStartDates[1]));
			int B=(Integer.parseInt(convertStartDates[2]));
			int C=(Integer.parseInt(convertStartDates[0]));			
			/////////////End Date		
			if(Integer.parseInt(convertEndDates[1])==1)
			{			
				convertEndDates[1]="12";
				convertEndDates[0]=String.valueOf((Integer.parseInt(convertEndDates[0])-1));
			}
			else
			{
				convertEndDates[1]=String.valueOf((Integer.parseInt(convertEndDates[1])-1));			
			}
			int D=(Integer.parseInt(convertEndDates[1]));
			int E=(Integer.parseInt(convertEndDates[2]));
			int F=(Integer.parseInt(convertEndDates[0]));		
		
			try {
			
			// Start Date is on:
			java.util.Calendar startDate = new GregorianCalendar();
			//startDate.setTimeZone(timezone);
			startDate.set(java.util.Calendar.MONTH, A);
			startDate.set(java.util.Calendar.DAY_OF_MONTH, B);
			startDate.set(java.util.Calendar.YEAR, C);
			startDate.set(java.util.Calendar.HOUR_OF_DAY, Atime);
			startDate.set(java.util.Calendar.MINUTE, Btime);
			startDate.set(java.util.Calendar.SECOND, 0);
			
			// End Date is on:
			java.util.Calendar endDate = new GregorianCalendar();
			//endDate.setTimeZone(timezone);
			endDate.set(java.util.Calendar.MONTH, D);
			endDate.set(java.util.Calendar.DAY_OF_MONTH, E);
			endDate.set(java.util.Calendar.YEAR, F);
			endDate.set(java.util.Calendar.HOUR_OF_DAY, Ctime);
			endDate.set(java.util.Calendar.MINUTE, Dtime);	
			endDate.set(java.util.Calendar.SECOND, 0);
			
			// Create the event props
			String eventName = subject;
			DateTime start = new DateTime(startDate.getTime());
			DateTime end = new DateTime(endDate.getTime());			
			googleStartTime=start.toString();
			googleEndTime=end.toString();
			// Create the event
			VEvent meeting = new VEvent(start, end, eventName);	
			
			// add timezone to vEvent
			//meeting.getProperties().add(tz.getTimeZoneId());
			if(location!=null && location!=""){
				Location loc = new Location(location);
				meeting.getProperties().add(loc);
			}
			
			Description sum = new Description(description);
			meeting.getProperties().add(sum);
			
			// generate unique identifier and add it to vEvent
			UidGenerator ug;
			ug = new UidGenerator("uidGen");
			Uid uid = ug.generateUid();			
			meeting.getProperties().add(uid);
			
			// assign props to calendar object
			icsCalendar.getProperties().add(new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
			icsCalendar.getProperties().add(CalScale.GREGORIAN);
			
			// Add the event and print
			System.out.println(meeting);
			icsCalendar.getComponents().add(meeting);
			CalendarOutputter outputter = new CalendarOutputter();
			outputter.setValidating(false);
			
			FileOutputStream fout = new FileOutputStream(calFile);
			outputter.output(icsCalendar, fout);
			
			}
			catch (Exception e)
			{
			System.err.println("Error in method icalfile() " + e);		
			}
		}		
		return calFile.toString();
		}	
	public JSONArray[] displayExamCalendar(Integer districtAssessmentId, Integer centreScheduleId,Integer teacherId,Integer jobId, String startDate)	
	{
		System.out.println(":::::::::::::::::displayExamCalendar::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb =	new StringBuffer();
		JSONArray[] myJSONArray = new JSONArray[3];
		JSONArray jsonArray = new JSONArray();
		JSONArray priorityDayArray = new JSONArray();
		JSONArray jsonArrayColor = new JSONArray();
		Boolean priority = false;
		List<String> priorityList = new ArrayList<String>();
		List<PriorityAssessmentDays> priorDaysList = new ArrayList<PriorityAssessmentDays>();
		//List<PriorityAssessmentDays> allPriorDaysList = new ArrayList<PriorityAssessmentDays>();
		//List<String> weekDays = new ArrayList<String>();
		List<String> priorDateTime = new ArrayList<String>();
		try {
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("teacherDetail") ==null) {
				//return "false";
			}else{
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
			}
			
			try {
				//JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
				List<CenterScheduleMaster> centerScheduleMasterList;
				
				///////////////////////////////////////////////////////////////////////////////////////////
				startDate = startDate==null? "" : startDate.trim();
				String endDate="";
				
				Calendar currentDate = Calendar.getInstance();
				if(startDate.equals(""))
					currentDate.setTime(new Date());	//set current Date.
				else
					currentDate.setTime(Utility.getCurrentDateFormart(startDate));	//set Date.
				
				startDate="";
				endDate="";
				
				currentDate.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);	//go to the first day of week
				startDate = getDate(currentDate);
				currentDate.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);	//go to the first day of week
				endDate = getDate(currentDate);
				
				System.out.println("StartDate:- "+startDate+", endDate:- "+endDate);
				
				///////////////////////////////////////////////////////////////////////////////////////////
				DistrictAssessmentDetail districtAssessmentDetail  = districtAssessmentDetailDAO.findById(districtAssessmentId, false, false);
				List<ExaminationCodeDetails> candidateExams = examinationCodeDetailsDAO.getDistrictAssessmentByTeacher(teacherDetail);
				
				centerScheduleMasterList=centerScheduleMasterDAO.getScheduleByDate(districtAssessmentDetail.getDistrictMaster(),startDate, endDate);
				
					
				System.out.println("centerScheduleMasterList :::"+centerScheduleMasterList.size());
				String stDate ="";
				String startTime ="";
				String endTime ="";
			
				// UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");
				
				List<TeacherDistrictAssessmentsPriority> teacherDistrictAssessmentsPriorities = teacherDistrictAssessmentsPriorityDAO.findByDistrictAssessment(districtAssessmentDetail);
				if(teacherDistrictAssessmentsPriorities!=null && teacherDistrictAssessmentsPriorities.size() > 0)
				{
					priorDaysList = priorityAssessmentDaysDAO.getPriorityDaysByPriorityAndDistrict(teacherDistrictAssessmentsPriorities.get(0).getPriority(), districtAssessmentDetail.getDistrictMaster());
				}
				//	Map<Integer,String> priorityDaysMap = new HashMap<Integer, String>();
				PriorityAssessmentDays priorityAssessmentDays = null;
				if(priorDaysList!=null && priorDaysList.size() > 0)
				{					
					priority = true;
					//String[] days = null;
					for(PriorityAssessmentDays prio : priorDaysList)
					{
						priorDateTime.add(Utility.getFormatDate(prio.getPreferredDates())+"@@@"+prio.getAvailableSlots());
						
						priorityAssessmentDays = prio ;
						if(priorityAssessmentDays.getPreferredDates()!=null)
						{
							/* days = priorityAssessmentDays.getPreferredDays().split(",");
							for(String s : days)
							{
								String weekDay = Utility.getWeekDayByStr(s);
								priorityList.add(weekDay);
								JSONObject jsonpriordays = new JSONObject();
								jsonpriordays.put("day", weekDay);
								jsonpriordays.put("color", "white");
								priorityDayArray.add(jsonpriordays);
							}
						}
						else
						{*/
							String weekDay = Utility.getFormatDate(priorityAssessmentDays.getPreferredDates());
							//System.out.println(" weekDay :: "+weekDay);
							JSONObject jsonpriordays = new JSONObject();
							jsonpriordays.put("day", weekDay);
							jsonpriordays.put("color", "white");
							priorityDayArray.add(jsonpriordays);
							priorityList.add(weekDay);
						}
						//priorityDaysMap.put(prio.getDistrictAssessmentDetail().getDistrictAssessmentId(), Utility.getFormatDate(priorityAssessmentDays.getPreferredDates()));
					}
				}
				/*else
				{
					priorDaysList = priorityAssessmentDaysDAO.getPriorityDaysByDistrict(districtAssessmentDetail.getDistrictMaster());
					List<String> otherPriorityDates = new ArrayList<String>();
					if(priorDaysList!=null && priorDaysList.size() > 0)
					{
						priority = true;
						for(PriorityAssessmentDays pad : priorDaysList)
						{
							priorDateTime.add(Utility.getFormatDate(pad.getPreferredDates())+"@@@"+pad.getAvailableSlots());
							if(pad.getPreferredDates()!=null)
							{
								otherPriorityDates.add(Utility.getFormatDate(pad.getPreferredDates()));	
							}							
						}						
						
						if(otherPriorityDates!=null)
						{
							for(CenterScheduleMaster csm : centerScheduleMasterList)
							{
								if(!otherPriorityDates.contains(Utility.getFormatDate(csm.getCsDate())))
								{
									JSONObject jsonpriordays = new JSONObject();
									jsonpriordays.put("day", Utility.getFormatDate(csm.getCsDate()));
									jsonpriordays.put("color", "white");
									priorityDayArray.add(jsonpriordays);
									priorityList.add(Utility.getFormatDate(csm.getCsDate()));
								}
							}
						}
					}*/
					/*
					priority = true;
					weekDays = Utility.getAllWeekDays();
					System.out.println("weekDays list :: "+weekDays.size());
					priorDaysList = priorityAssessmentDaysDAO.getPriorityDaysByDistrict(districtAssessmentDetail.getDistrictMaster());
					System.out.println(" allPriorDaysList ::: "+allPriorDaysList.size());
					 if(priorDaysList!=null && priorDaysList.size()>0)
					 {
						 List<String> tempDays = new ArrayList<String>(); 
						 for(PriorityAssessmentDays  days : priorDaysList)
						 {
							 String[] pDays = days.getPreferredDays().split(",");
							 for(String s : pDays)
							 {
								 tempDays.add(Utility.getWeekDayByStr(s));
							 }
						 }
						 for(String str : weekDays)
						 {
							 if(!tempDays.contains(str))
							 {
								 if(!priorityList.contains(str))
								 {
									 priorityList.add(str); 
									 JSONObject jsonpriordays = new JSONObject();
										jsonpriordays.put("day", str);
										jsonpriordays.put("color", "white");
										priorityDayArray.add(jsonpriordays);
								 }								 
							 }							 
						 }						 
					 }
					 else
					 {
						 for(String str : weekDays)
						 {
							 if(!str.equalsIgnoreCase("Sunday") && !str.equalsIgnoreCase("Saturday"))
							 {
								 priorityList.add(str); 
								 JSONObject jsonpriordays = new JSONObject();
								 jsonpriordays.put("day", str);
								 jsonpriordays.put("color", "white");
								 priorityDayArray.add(jsonpriordays);
							 }
							 
						 }		 
					 }
					
				*///}
				System.out.println("centerScheduleMasterList:>>>>:::"+centerScheduleMasterList.size());
				List<String> timeList=new ArrayList<String>();
				String examDate="";
				String timeStrCheck="";
				for(ExaminationCodeDetails examinationCodeDetails : candidateExams){
					timeList=new ArrayList<String>();
					if(examinationCodeDetails!=null && examinationCodeDetails.getExamDate()!=null){
						for (CenterScheduleMaster centerScheduleMaster2 : centerScheduleMasterList) 
						{
							String csDate = centerScheduleMaster2.getCsDate().toString();
							/*if(priority)
							{
								
							}*/
							
							examDate=examinationCodeDetails.getExamDate().toString();
							if(csDate.equals(examDate)){
								timeList.add(centerScheduleMaster2.getCsTime());
							}
						}
						int selectSlot=0;
						if(examinationCodeDetails.getDistrictAssessmentDetail().getAssessmentSessionTime()!=null){
							try{
								selectSlot=examinationCodeDetails.getDistrictAssessmentDetail().getAssessmentSessionTime()/1800;
							}catch(Exception e){
								e.printStackTrace();
							}
						}
						List<String> dateAvailabilityCheck=Utility.getFilterTime(timeList, examinationCodeDetails.getExamStartTime(),selectSlot);
						if(dateAvailabilityCheck!=null){
							for (String dateStr : dateAvailabilityCheck) {
								endTime=dateStr;
								timeStrCheck+="||"+dateStr+examDate+"||";
							}
							System.out.println(examinationCodeDetails.getDistrictAssessmentDetail().getDistrictAssessmentId()+":::timeStrCheck:::::::::::::::>"+timeStrCheck);
						}
					}
				} 
				System.out.println("timeStrCheck::"+timeStrCheck);
				int noOfSeatsAvailable=0;
				   try{
					   if(centerScheduleMasterList.size()>0){
						   noOfSeatsAvailable=centerScheduleMasterList.get(0).getCenterMaster().getNoOfSeatsAvailable();
					   }
				   }catch(Exception e){
					   e.printStackTrace();
				   }
				   
				System.out.println("noOfSeatsAvailable::::"+noOfSeatsAvailable);
				for(CenterScheduleMaster centerScheduleMaster :centerScheduleMasterList){
					try {
						stDate = Utility.getDateWithoutTime(centerScheduleMaster.getCsDate());
					} catch (Exception e) {
						e.printStackTrace();
					}    	
					try{
						startTime = Utility.convert12HourrTo24HourWithFormat(centerScheduleMaster.getCsTime(),"hh:mm a");
					} catch (Exception e) {
						e.printStackTrace();
					}
					try{
						endTime = Utility.addMinutesToDateNew(centerScheduleMaster.getCsDate(),centerScheduleMaster.getCsTime(),30);
						//System.out.println(" startTime :: "+startTime+" ||||||| "+" endTime :: "+endTime );
					} catch (Exception e) {
						e.printStackTrace();
					}
					String csSLDate=centerScheduleMaster.getCsDate().toString();
					String day = Utility.getDayByDate(centerScheduleMaster.getCsDate());
				    boolean yourBooking=false;
				    try{
					   if(timeStrCheck.contains("||"+centerScheduleMaster.getCsTime()+csSLDate+"||")){
						   yourBooking=true;
					   }
				   }catch(Exception e){
					   e.printStackTrace();
				   }
				   if(yourBooking)
				   System.out.println(centerScheduleMaster.getCentreScheduleId()+":::::::::yourBooking:::::::::"+yourBooking);
				
				   
				   int noOfSeats=0;
				   boolean notAvailable=false;
				   try{
					   if(centerScheduleMaster.getAvailability()!=null && centerScheduleMaster.getAvailability()==0){
						   notAvailable=true;
					   }
				   }catch(Exception e){
					   e.printStackTrace();
				   }
				   
				   
				   if(centerScheduleMaster.getNoOfApplicantsScheduled()!=null){
					   noOfSeats =centerScheduleMaster.getNoOfApplicantsScheduled();
				   }
				   // String checkDay = Utility.getDayByDate(centerScheduleMaster.getCsDate());
				   String checkDay = Utility.getFormatDate(centerScheduleMaster.getCsDate());
				   if(priorityList.contains(checkDay) && priorDateTime.contains(Utility.getFormatDate(centerScheduleMaster.getCsDate())+"@@@"+centerScheduleMaster.getCsTime()) && (!notAvailable))
				   {
				  /* if(!priorDateTime.contains(Utility.getFormatDate(centerScheduleMaster.getCsDate())+"@@@"+centerScheduleMaster.getCsTime()))
				   {*/
					   	   if(((noOfSeats>=noOfSeatsAvailable) || notAvailable)){	
							   if(yourBooking){
								   JSONObject jsoncolors = new JSONObject();
								   jsoncolors.put("text", Utility.getLocaleValuePropByKey("msgExamAjax9", locale));
								   jsoncolors.put("value", centerScheduleMaster.getCentreScheduleId());
								   jsoncolors.put("color", "green");
								   jsonArrayColor.add(jsoncolors);
							   }else{
								   JSONObject jsoncolors = new JSONObject();
								   jsoncolors.put("text", Utility.getLocaleValuePropByKey("lblNotAvailable", locale));
								   jsoncolors.put("value", "");
								   jsoncolors.put("color", "#ebebeb");
								   jsonArrayColor.add(jsoncolors);
							   }
						   }
						   else{
							   if(yourBooking){
								   JSONObject jsoncolors = new JSONObject();
								   jsoncolors.put("text", Utility.getLocaleValuePropByKey("msgExamAjax9", locale));
								   jsoncolors.put("value", centerScheduleMaster.getCentreScheduleId());
								   jsoncolors.put("color", "green");
								   jsonArrayColor.add(jsoncolors);
							   }else{
								   JSONObject jsoncolors = new JSONObject();
								   jsoncolors.put("text", Utility.getLocaleValuePropByKey("optAvble", locale));
								   jsoncolors.put("value", centerScheduleMaster.getCentreScheduleId());
								   jsoncolors.put("color", "white");
								   jsonArrayColor.add(jsoncolors);
							   }
						   }
				   }
				   /*else if((centerScheduleMaster.getAvailability()==null) || (centerScheduleMaster.getAvailability()==1))
				   {
					   if(((noOfSeats>=noOfSeatsAvailable) || notAvailable)){	
						   if(yourBooking){
							   JSONObject jsoncolors = new JSONObject();
							   jsoncolors.put("text", "Your Bookings");
							   jsoncolors.put("value", centerScheduleMaster.getCentreScheduleId());
							   jsoncolors.put("color", "green");
							   jsonArrayColor.add(jsoncolors);
						   }else{
							   JSONObject jsoncolors = new JSONObject();
							   jsoncolors.put("text", "Not Available");
							   jsoncolors.put("value", "");
							   jsoncolors.put("color", "#ebebeb");
							   jsonArrayColor.add(jsoncolors);
						   }
					   }
					   else{
						   if(yourBooking){
							   JSONObject jsoncolors = new JSONObject();
							   jsoncolors.put("text", "Your Bookings");
							   jsoncolors.put("value", centerScheduleMaster.getCentreScheduleId());
							   jsoncolors.put("color", "green");
							   jsonArrayColor.add(jsoncolors);
						   }else{
							   JSONObject jsoncolors = new JSONObject();
							   jsoncolors.put("text", "Available");
							   jsoncolors.put("value", centerScheduleMaster.getCentreScheduleId());
							   jsoncolors.put("color", "white");
							   jsonArrayColor.add(jsoncolors);
						   }
					   }
				   }*/
				   else {
					   if(((noOfSeats>=noOfSeatsAvailable) || notAvailable)){	
						   if(yourBooking){
							   JSONObject jsoncolors = new JSONObject();
							   jsoncolors.put("text", Utility.getLocaleValuePropByKey("msgExamAjax9", locale));
							   jsoncolors.put("value", centerScheduleMaster.getCentreScheduleId());
							  /* if(priorDaysList!=null && priorDaysList.size()>0)
								   jsoncolors.put("color", "#ebebeb");
							   else*/
								   jsoncolors.put("color", "green");
							   jsonArrayColor.add(jsoncolors);
						   }else{
							   
							   JSONObject jsoncolors = new JSONObject();
							   jsoncolors.put("text", Utility.getLocaleValuePropByKey("lblNotAvailable", locale));
							   jsoncolors.put("value", centerScheduleMaster.getCentreScheduleId());
							   if(centerScheduleMaster.getAvailability()==0 && priorDateTime.contains(Utility.getFormatDate(centerScheduleMaster.getCsDate())+"@@@"+centerScheduleMaster.getCsTime()))
							   {
								   jsoncolors.put("color", "white");   
							   }
							   else
							   {
								   jsoncolors.put("color", "#ebebeb");
							   }
							   
							   jsonArrayColor.add(jsoncolors);
						   }
					   }
					   else{
						   if(yourBooking){
							   JSONObject jsoncolors = new JSONObject();
							   jsoncolors.put("text", Utility.getLocaleValuePropByKey("msgExamAjax9", locale));
							   jsoncolors.put("value", centerScheduleMaster.getCentreScheduleId());
							   /*if(priorDaysList!=null && priorDaysList.size()>0)
								   jsoncolors.put("color", "#ebebeb");
							   else*/
								   jsoncolors.put("color", "green");
							   jsonArrayColor.add(jsoncolors);
						   }else{
							   JSONObject jsoncolors = new JSONObject();
							   jsoncolors.put("text", Utility.getLocaleValuePropByKey("optAvble", locale));
							   jsoncolors.put("value", centerScheduleMaster.getCentreScheduleId());
							   if(priorDaysList!=null && priorDaysList.size()>0)
								   jsoncolors.put("color", "#ebebeb");
							   else
								   jsoncolors.put("color", "white");
							   jsonArrayColor.add(jsoncolors);
						   }
					   }
				   }				  
				   
					String startTimedLoop =  stDate+" "+startTime;
					String endTimedLoop =  stDate+" "+endTime;
					JSONObject formDetails = new JSONObject();
					formDetails.put("id",centerScheduleMaster.getCentreScheduleId());
					formDetails.put("start",startTimedLoop);
					formDetails.put("end", endTimedLoop);
					formDetails.put("day", day);
					if(noOfSeats!=0 && yourBooking){
						formDetails.put("title","");
					}else{
						formDetails.put("title","");
					}
					
					jsonArray.add(formDetails);
				}
				myJSONArray[0] = jsonArray;
				myJSONArray[1] = jsonArrayColor;
				myJSONArray[2] = priorityDayArray;
				System.out.println(" priorityDayArray :: "+priorityDayArray.toString());
				//myJSONArray[3] = prioFlag;
 			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return myJSONArray;
	}
	
	public String showAssessmentDetails(Integer teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		System.out.println("================== showAssessmentDetails Grid ==========");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer tmRecords = new StringBuffer();
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		List<TeacherDetail> tList = new ArrayList<TeacherDetail>();
		tList.add(teacherDetail);
		List<ExaminationCodeDetails> examinationCodeDetails = new ArrayList<ExaminationCodeDetails>();
		Map<String,TeacherDistrictAssessmentStatus> assessmentStatus = new HashMap<String, TeacherDistrictAssessmentStatus>();
		List<TeacherDistrictAssessmentStatus> teacherAssessmentStatus = new ArrayList<TeacherDistrictAssessmentStatus>();
		teacherAssessmentStatus = teacherDistrictAssessmentStatusDAO.findAssessmentTakenByTeachers(tList);
		System.out.println("teacherAssessmentStatus::: "+teacherAssessmentStatus.size());
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			for(TeacherDistrictAssessmentStatus tdaStatus : teacherAssessmentStatus)
	 		{
	 			assessmentStatus.put(tdaStatus.getTeacherDetail().getTeacherId()+"###"+tdaStatus.getDistrictAssessmentDetail().getDistrictAssessmentId(), tdaStatus);
	 		}
			String sortOrderFieldName	=	"districtAssessmentName";
			String sortOrderNoField		=	"districtAssessmentName";
			int checkShortOrder=0;
			if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("districtAssessmentName")){
				checkShortOrder=0;
			}
			else if(sortOrder.equalsIgnoreCase("examinationCode")){ 
				checkShortOrder=2;
			}
			else if(sortOrder.equalsIgnoreCase("examDate")){
				checkShortOrder=3;
			}
			System.out.println("noofRow :: "+noOfRow+" pageNo :: "+sortOrder+" sortOrderType :: "+sortOrderType);

			Order  sortOrderStrVal=null;
			Boolean flag = false;
			if(sortOrder!=null){	
				flag = true;
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("districtAssessmentName"))
				{
					sortOrderNoField="districtAssessmentName";
				}
				/*if(sortOrder.equals("districtName"))
				{
					sortOrderNoField="districtName";
				}*/
				/*if(sortOrder.equals("jobTitle"))
				{
					sortOrderNoField="jobTitle";
				}*/
				/*if(sortOrder.equals("examinationCode"))
				{
					sortOrderNoField="examinationCode";
				}*/
				if(sortOrder.equals("examDate"))
				{
					sortOrderNoField="examDate";
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("")){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			System.out.println("rows :: "+noOfRowInPage+" pgNo :: "+pgNo+" start :: "+start+" end :: "+end+" sortOrderStrVal :: "+sortOrderStrVal.toString());	
			if(teacherDetail!=null)
			{						
					System.out.println(" Examination Code Details...");
					examinationCodeDetails = examinationCodeDetailsDAO.getExamCodeByTeacher(teacherDetail,checkShortOrder,sortOrderStrVal,start,end,flag);
					totalRecord = examinationCodeDetailsDAO.getTotalExamCodeByTeacher(teacherDetail,checkShortOrder,sortOrderStrVal,start,end);
			}
			if(totalRecord<end)
				end=totalRecord;

			tmRecords.append("<table id='examDetailsTable' width='100%' class='tablecgtbl'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			String responseText="";

			responseText=PaginationAndSorting.responseSortingLink(lblAssessmentName,sortOrderFieldName,"districtAssessmentName",sortOrderTypeVal,pgNo);		
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			/*responseText=PaginationAndSorting.responseSortingLink("District Name",sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);		
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");*/
			tmRecords.append("<th  valign='top'>"+lblDistrictName+"</th>");
			/*responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");*/
			/*responseText=PaginationAndSorting.responseSortingLink("Assessment Code",sortOrderFieldName,"examinationCode",sortOrderTypeVal,pgNo);
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");*/
			//tmRecords.append("<th   valign='top'>Marks Obtained&nbsp;<a data-original-title='Total Marks Obtained' class='net-header-text' rel='tooltip' id='marksTool' href='javascript:void(0);'><span class='icon-question-sign'></span></a></th>");
			//tmRecords.append("<script>$('#marksTool').tooltip();</script>");
			responseText=PaginationAndSorting.responseSortingLink(lblAssDTime,sortOrderFieldName,"examDate",sortOrderTypeVal,pgNo);
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			int counter=0 ;
			String baseURL=Utility.getValueOfPropByKey("basePath");
			boolean slotSelectionNeeded = false;
			
			if(examinationCodeDetails!=null && examinationCodeDetails.size()>0)
			{
				String startTime="";
				String endTime="";
				
				for(ExaminationCodeDetails examDetails : examinationCodeDetails)
				{
					if(examDetails.getJobOrder().getDistrictMaster().getDistrictId()==4218990)
						slotSelectionNeeded = true;
					
					counter++;
					String gridColor="class='bggrid'";
					if(counter%2==0){
						gridColor="style='background-color:white;'";
					}
					String emailAddress = examDetails.getTeacherDetail().getEmailAddress();
					System.out.println(" teacher emailAddress :: "+emailAddress);
					String linkIds = emailAddress+"###"+examDetails.getDistrictAssessmentDetail().getDistrictAssessmentId()+"###"+examDetails.getJobOrder().getJobId();
					String forMated = Utility.encodeInBase64(linkIds);
					forMated = baseURL+"examslotselection.do?id="+forMated;
					tmRecords.append("<tr "+gridColor+">");
					tmRecords.append("<td>"+examDetails.getDistrictAssessmentDetail().getDistrictAssessmentName()+"</td>");
					tmRecords.append("<td>"+examDetails.getDistrictAssessmentDetail().getDistrictMaster().getDistrictName()+"</td>");
					//tmRecords.append("<td>"+examDetails.getExaminationCode()+"</td>");
					if(examDetails.getInviteStatus().equalsIgnoreCase("C"))
					{
						Integer districtAssessmentId = examDetails.getDistrictAssessmentDetail().getDistrictAssessmentId();
						Integer jobId =examDetails.getJobOrder().getJobId();
						teacherId = examDetails.getTeacherDetail().getTeacherId();
						TeacherDistrictAssessmentStatus status = assessmentStatus.get(teacherId+"###"+districtAssessmentId);
						
						startTime = examDetails.getExamStartTime();
						endTime = examDetails.getExamEndTime();
						String slotDate = "";
						if(slotSelectionNeeded)
							slotDate = startTime+" to "+endTime;
						
						boolean isApproved=false;
						if(examDetails.getApproved()!=null && examDetails.getApproved()){
							isApproved=true;
						}
						if(status!=null)
						{
							int marksObtained = status.getMarksObtained()==null?0:status.getMarksObtained().intValue();
							//tmRecords.append("<td>"+marksObtained+"</td>");
							tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(examDetails.getExamDate())+"<br/>"+slotDate+"</td>");
							if(status.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")){
								if(isApproved){
									tmRecords.append("<td><a href='javascript:void(0);' id='Exam"+counter+"' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgStartAssessment", locale)+"' onclick='checkDistrictInventory("+teacherId+","+jobId+","+districtAssessmentId+");'>"+Utility.getLocaleValuePropByKey("msgStartAssessment", locale)+"</a></td>");
								}else{
									tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("magApprovalPending", locale)+"</td>");
								}
							}else if(status.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
								tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("lblTimedOut", locale)+"</td>");
							else if(status.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
								tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("lblCompleted", locale)+"</td>");
						}
						else
						{
							//tmRecords.append("<td> N/A </td>");
							tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(examDetails.getExamDate())+"<br/>"+slotDate+"</td>");
							if(isApproved){
								tmRecords.append("<td><a href='javascript:void(0);' id='Exam"+counter+"' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgStartAssessment", locale)+"' onclick='checkDistrictInventory("+teacherId+","+jobId+","+districtAssessmentId+");'>"+Utility.getLocaleValuePropByKey("msgStartAssessment", locale)+"</a>");
							}else{
								tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("magApprovalPending", locale)+"</td>");
							}
							
							tmRecords.append("<script>$('#Exam"+counter+"').tooltip();</script></td>");
						}
					}else
					{
						//tmRecords.append("<td> N/A </td>");
						tmRecords.append("<td> N/A </td>");
						tmRecords.append("<td><a href='"+forMated+"' id='Slot"+counter+"' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgSelectSlot", locale)+"'>"+Utility.getLocaleValuePropByKey("msgSelectSlot", locale)+"</a>");
						tmRecords.append("<script>$('#Slot"+counter+"').tooltip();</script></td>");
					}
					tmRecords.append("</tr>");
				}
			}
			else
			{
				tmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgExamAjax10", locale)+"</td></tr>" );
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForTeacherInfoAjax(request,totalRecord,noOfRow, pageNo));
		}
		
		return tmRecords.toString();
	}

	public String getDisabledSlotIds()
	{
		StringBuffer sb = new StringBuffer();
		int count = 0;
		List<CenterScheduleMaster> centerScheduleMasterList = centerScheduleMasterDAO.getSchedule();
		if(centerScheduleMasterList!=null && centerScheduleMasterList.size()>0)
		{
			for(CenterScheduleMaster centerScheduleMaster : centerScheduleMasterList)
			{
				if(centerScheduleMaster.getNoOfApplicantsScheduled() > 20)
				{
					if(count==0)
						sb.append("").append(centerScheduleMaster.getCentreScheduleId());
					else
						sb.append(",").append(centerScheduleMaster.getCentreScheduleId());
					count++;	
				}
			}
		}
		System.out.println(" disabled ids :: "+sb.toString());
		return sb.toString();
	}
	
	public Set<Integer> displayAssessmentNameUrl(TeacherDetail teacherDetail,List<DistrictAssessmentJobRelation> districtAMTJobRelation)
	{
		Map<String,TeacherDistrictAssessmentStatus> mapForAssementUrl = new HashMap<String, TeacherDistrictAssessmentStatus>();
		List<TeacherDistrictAssessmentAnswerDetail> teacherDistrictAssessmentAnswerList = new ArrayList<TeacherDistrictAssessmentAnswerDetail>();
		List<DistrictAssessmentDetail> districtAssesssmentList = new ArrayList<DistrictAssessmentDetail>();
		Set<Integer> districtASMTId = new HashSet<Integer>();
		for(DistrictAssessmentJobRelation dsjr: districtAMTJobRelation)
		{
			districtAssesssmentList.add(dsjr.getDistrictAssessmentDetail());
		}
		teacherDistrictAssessmentAnswerList = teacherDistrictAssessmentAnswerDetailDAO.findDistrictAssessmentStatusByTeacherAndDAMT(teacherDetail,districtAssesssmentList);
		
		for(TeacherDistrictAssessmentAnswerDetail tdaad:teacherDistrictAssessmentAnswerList)
		{
			districtASMTId.add(tdaad.getDistrictAssessmentDetail().getDistrictAssessmentId());
		}

		System.out.println("  ddistrict assessment id in Set size::  "+districtASMTId.size());
		
		return districtASMTId;
	}
	public String getColorAndEvent(Integer districtAssessmentId, Integer centreScheduleId,Integer teacherId,Integer jobId)	
	{
		System.out.println("::::::::::::::String:::displayExamCalendarnrewewewe:>>>>>>>>>>>>>>>>>..:::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb =	new StringBuffer();
		JSONObject responseDetailsJson = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		try {
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("teacherDetail") ==null) {
				//return "false";
			}else{
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
			}
			try {
				CenterMaster centerMaster=centerMasterDAO.findById(1, false, false);
				System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
				//JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			//	CenterScheduleMaster centerScheduleMasters=centerScheduleMasterDAO.findById(5, false, false);
				List<CenterScheduleMaster> centerScheduleMasterList=centerScheduleMasterDAO.getSchedule();
				//SimpleDateFormat formatter =  new SimpleDateFormat("yyyy-mm-dd hh:mm a");
				System.out.println("centerScheduleMasterList :::"+centerScheduleMasterList.size());
				String stDate ="";
				String startTime ="";
				String endTime ="";
			
		    /*	System.out.println("mmmmm  "+"  "+centerScheduleMaster.getTime());
		    	try{
		    	startTime = Utility.convert12HourrTo24HourWithFormat(centerScheduleMaster.getTime(),"hh:mma");
		    	} catch (Exception e) {
					e.printStackTrace();
				}
		    	
		    	System.out.println("aaaaaaa  ");
		    	try{
		    	endTime = Utility.convert12HourrTo24HourWithFormat(centerScheduleMasters.getTime(),"hh:mma");
		    	} catch (Exception e) {
					e.printStackTrace();
				}
		    	System.out.println("bbbbbbb  ");
		    	String startTimed =  stDate+" "+startTime;
		    	String endTimed =  stDate+" "+endTime;
				JSONObject formDetailsJson = new JSONObject();
				formDetailsJson.put("id", 1);
				formDetailsJson.put("start",startTimed);
				formDetailsJson.put("end", endTimed);
				formDetailsJson.put("title","Sekhaer1");
				//formDetailsJson.put("selectable","false");
				jsonArray.add(formDetailsJson);*/
				
				
				for(CenterScheduleMaster centerScheduleMaster :centerScheduleMasterList){
					
				}
				
				for(CenterScheduleMaster centerScheduleMaster :centerScheduleMasterList){/*
					try {
						stDate = Utility.getDateWithoutTime(centerScheduleMaster.getCsDate());
					} catch (Exception e) {
						e.printStackTrace();
					}    	
					try{
				    	startTime = Utility.convert12HourrTo24HourWithFormat(centerScheduleMaster.getCsTime(),"hh:mm a");
				    	} catch (Exception e) {
							e.printStackTrace();
						}
				    	
				    	System.out.println("aaaaaaa  ");
				    	try{
				    	endTime = Utility.convert12HourrTo24HourWithFormat(centerScheduleMaster.getCsTime(),"hh:mm a");
				    	} catch (Exception e) {
							e.printStackTrace();
						}
				    	
				   int noOfSeatsAvailable=centerMaster.getNoOfSeatsAvailable(); 	
				    	
				    	
					String startTimedLoop =  stDate+" "+startTime;
			    	String endTimedLoop =  stDate+" "+startTime;
					JSONObject formDetails = new JSONObject();
					formDetails.put("value",centerScheduleMaster.getCentreScheduleId());
					//formDetails.put("start",startTimedLoop);
					//formDetails.put("end", endTimedLoop);
					formDetails.put("text",centerScheduleMaster.getCentreScheduleId());
					formDetails.put("color","#00a910");
					jsonArray.add(formDetails);
				*/}
			/*	String startTimedLoop =  stDate+" "+startTime;
		    	String endTimedLoop =  stDate+" "+startTime;
				JSONObject formDetails = new JSONObject();
				formDetails.put("value",5);
				//formDetails.put("start",startTimedLoop);
				//formDetails.put("end", endTimedLoop);
				formDetails.put("text",6);
				formDetails.put("color","#00a910");
				jsonArray.add(formDetails);*/
				
				sb.append("[{text:\"Holiday\", value: \"7\", color: \"#00a910\"},{text:\"Holiday\", value: \"8\", color: \"#00a910\"}]");
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		    System.out.println(jsonArray);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		System.out.println("llllllllllllll"+sb.toString());
		return sb.toString();
	}
	
	public String getStartEndDateByAssessment(Integer centreScheduleId,Integer districtAssessmentId,Integer teacherId)
	{

		/*Date restrictDate = null;
		List<CenterScheduleMaster> scheduleMasterList=centerScheduleMasterDAO.getSchedule();
		System.out.println("scheduleMasterList :::"+scheduleMasterList.size());
		Map<String,Boolean> slotCheck = new HashMap<String, Boolean>();
		for(CenterScheduleMaster centerScheduleMaster : scheduleMasterList)
		{
			slotCheck.put(centerScheduleMaster.getCsDate()+"@@@"+centerScheduleMaster.getCsTime(), true);
		}*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		TeacherDetail teacherDetail = null;
		if (session == null || session.getAttribute("teacherDetail") ==null) {
			//return "false";
		}else{
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		}
		
		System.out.println(" districtAssessmentId :: "+districtAssessmentId);
		long dateDiff = 0;
		Boolean assessmentPriority = false;
		DistrictAssessmentDetail districtAssessmentDetail = districtAssessmentDetailDAO.findById(districtAssessmentId, false, false);
		TeacherDetail teacherDetails =teacherDetailDAO.findById(teacherId, false, false);
		List<ExaminationCodeDetails> exam = examinationCodeDetailsDAO.getExamCodeByDAssessAndTeacher(districtAssessmentDetail,teacherDetails);
		List<TeacherDistrictAssessmentsPriority> teacherDistrictAssessmentsPriorities = teacherDistrictAssessmentsPriorityDAO.findByDistrictAssessment(districtAssessmentDetail);
		if(teacherDistrictAssessmentsPriorities!=null && teacherDistrictAssessmentsPriorities.size()>0)
		{
			assessmentPriority = true;
		}
		int selectSlot=0;
		if(districtAssessmentDetail.getAssessmentSessionTime()!=null){
			try{
				selectSlot=districtAssessmentDetail.getAssessmentSessionTime()/1800;
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		String date = "";
		String end ="";
		String selectedStartTime = "";
		String selectedEndTime = "";
		int examDuration = 0;
		Boolean availability = false;
		CenterScheduleMaster centerScheduleMaster = centerScheduleMasterDAO.findById(centreScheduleId, false, false);
		List<CenterScheduleMaster> centerScheduleMasterList=new ArrayList<CenterScheduleMaster>();
		if(centerScheduleMaster!=null){
			System.out.println("centerScheduleMaster.getDate()::::"+centerScheduleMaster.getCsDate().toString());
			try {
				dateDiff = Utility.getDateDifference(new Date(), Utility.addMinutesToDate(centerScheduleMaster.getCsDate(), centerScheduleMaster.getCsTime(),selectSlot));
				System.out.println(" dateDiff :: "+dateDiff);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			if(!(dateDiff < 0))
			{
				SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
				try {
					
					/*restrictDate = Utility.convertDateAndTime(Utility.addMinutesToDate(centerScheduleMaster.getCsDate(), centerScheduleMaster.getCsTime(), 0));
					System.out.println(" restrictDate ::: "+restrictDate);*/
					
					date = Utility.convertDateAndTimeFormatForEpiAndJsi(Utility.addMinutesToDate(centerScheduleMaster.getCsDate(), centerScheduleMaster.getCsTime(), 0));
					selectedStartTime = Utility.convert12HourrTo24Hour(sdf.format(Utility.addMinutesToDate(centerScheduleMaster.getCsDate(), centerScheduleMaster.getCsTime(), 0)));
					System.out.println(" selectedStartTime :: "+selectedStartTime);
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					examDuration = (selectSlot)*30;
					end = Utility.convertDateAndTimeFormatForEpiAndJsi(Utility.addMinutesToDate(centerScheduleMaster.getCsDate(), centerScheduleMaster.getCsTime(),examDuration));
					selectedEndTime = Utility.convert12HourrTo24Hour(sdf.format(Utility.addMinutesToDate(centerScheduleMaster.getCsDate(), centerScheduleMaster.getCsTime(),examDuration-30)));
					System.out.println(" selectedEndTime :: "+selectedEndTime);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			System.out.println(" centerScheduleMaster.getCsDate() :: "+centerScheduleMaster.getCsDate());
			centerScheduleMasterList=centerScheduleMasterDAO.getScheduleByDate(centerScheduleMaster.getDistrictMaster(),centerScheduleMaster.getCsDate());
		}
		System.out.println("centerScheduleMasterList::::"+centerScheduleMasterList.size());
		List<String> timeList=new ArrayList<String>();
		for (CenterScheduleMaster centerScheduleMaster2 : centerScheduleMasterList) {
			timeList.add(centerScheduleMaster2.getCsTime());
		}
		
		List<String> dateAvailabilityCheck = Utility.getFilterTime(timeList, centerScheduleMaster.getCsTime(),selectSlot);
		
		
		List<String> timeSelectSlots = new ArrayList<String>();
		String endTime="";
		String timeStrCheck="";
		if(dateAvailabilityCheck!=null){
			for (String dateStr : dateAvailabilityCheck) {
				timeSelectSlots.add(dateStr);
				System.out.println("dateStr::::::::"+dateStr);
				endTime=dateStr;
				timeStrCheck+="||"+dateStr+"||";
			}
		}
		System.out.println(" timeStrCheck :: "+timeStrCheck);
		String csSLDate=centerScheduleMaster.getCsDate().toString();
		String examDate="";
		if(exam.size()>0){
			if(exam.get(0).getExamDate()!=null){
				examDate=exam.get(0).getExamDate().toString();
			}
		}
		
	    boolean yourBooking=false;
	    try{
		   if(timeStrCheck.contains("||"+centerScheduleMaster.getCsTime()+"||") && csSLDate.equalsIgnoreCase(examDate) ){
			   yourBooking=true;
		   }
		   else
		   {
			   System.out.println(" not booked any slot");
		   }
	   }catch(Exception e){
		   e.printStackTrace();
	   }
		/**
		 * for check DA availability for candidate selected start and end date/time
		 * START
		 */
	   			try {
					List<CenterScheduleMaster> centerScheduleMasters = new ArrayList<CenterScheduleMaster>();
					centerScheduleMasters = centerScheduleMasterDAO.getScheduleByDateAndAvailability(districtAssessmentDetail.getDistrictMaster(),centerScheduleMaster.getCsDate());
					List<String> cScheduleSlots = new ArrayList<String>();
					if(centerScheduleMasters!=null && centerScheduleMasters.size()>0)
					{
						for(CenterScheduleMaster csm : centerScheduleMasters)
						{
							if(csm.getAvailability()!=null && csm.getAvailability()==1)
							{
								cScheduleSlots.add(Utility.convert12HourrTo24Hour(csm.getCsTime()));								
							}
						}		   
					}
					
					if(cScheduleSlots!=null && cScheduleSlots.size()>0)
					{						
						if(timeSelectSlots!=null && timeSelectSlots.size()>0)
						{
							for(String ex : timeSelectSlots)
							{
								if(cScheduleSlots.contains(Utility.convert12HourrTo24Hour(ex)))
								{
									availability = true;
								}
								else
								{
									availability = false;
									break;
								}
							}
						}
						System.out.println(" availability :: "+availability);
						
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   /**
	    * END
	    */
		StringBuffer sb = new StringBuffer();
	if(dateAvailabilityCheck==null)
		{
			System.out.println("<div class='col-sm-10 col-md-10' style='padding:15px;margin-left:20px;'> This slot is not Available </div>");
		}
	else{
		if((dateDiff >= 0) && exam!=null && availability && (assessmentPriority)){
			sb.append("<div class='col-sm-10 col-md-10' style='padding:15px;margin-left:20px;'>");
			sb.append("<div class='row' id='startdiv'>");
			sb.append("<div class='col-sm-4 col-md-4' style='float:left;'>");
			sb.append("<label>"+Utility.getLocaleValuePropByKey("lblStartTime", locale)+"</label><br/>");
			sb.append("<input type='text' name='startTime' id='startTime' class='form-control'  readonly='readonly' value='"+date+"' style='height:20px;width:165px;'/></div>");
			sb.append("</div><br/>");
			sb.append("<div class='row' id='enddiv'>");
			sb.append("<div class='col-sm-4 col-md-4' style='float:left;margin-top:-13px;'>");
			sb.append("<label>"+Utility.getLocaleValuePropByKey("lblEndTime", locale)+"</label><br/>");
			sb.append("<input type='text' name='endTime' id='endTime' class='form-control'  readonly='readonly' value='"+end+"' style='height:20px;width:165px;'/></div>");
			sb.append("</div>");
			sb.append("<div id='actiondiv' class='col-sm-10 col-md-10' style='float:left;margin-top:20px;padding-left:26px;'>");
			if((yourBooking || dateAvailabilityCheck==null) && (exam.get(0).getExamDate()!=null)){
				System.out.println(" exam date not null ");
				sb.append("<button onclick='closePopUp();' class='btn' style='float:right;margin-right:22px;'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+"</i></button></div>");
				sb.append("<script>$('#showpopupKendoWindow_wnd_title').html('"+Utility.getLocaleValuePropByKey("msgAssessmentTime", locale)+"');</script>");
				sb.append("</div>");
			}
			else if(exam!=null && dateAvailabilityCheck!=null && (exam.get(0).getExamDate()==null)) {
				System.out.println(" dateAvailabilityCheck "+dateAvailabilityCheck);
				System.out.println(" exam date null ");
				sb.append("<button onclick='saveSlot();' class='btn btn-primary' style=''>Allot<i class='icon'></i></button>&nbsp;&nbsp;<button onclick='closePopUp();' class='btn' >"+Utility.getLocaleValuePropByKey("btnClr", locale)+"</i></button></div>");
				sb.append("<script>$('#showpopupKendoWindow_wnd_title').html('"+Utility.getLocaleValuePropByKey("msgAssessmentTime", locale)+"');</script>");
				sb.append("</div>");
			}
			else{
				System.out.println(" slot booked ");
				sb.append("</div>");
				sb.append("<script>$('#startdiv').hide();$('#actiondiv').css('margin-top', '0px');</script>");
				sb.append("<script>$('#enddiv').hide();</script>");
				sb.append("<script>$('#showpopupKendoWindow_wnd_title').html('"+Utility.getLocaleValuePropByKey("msgSlotConfirmed", locale)+"');</script>");
				sb.append("<div class='row' style='margin-top:-24px;margin-right:20px;padding:7px;width:100%;'>");
				sb.append("<b>"+Utility.getLocaleValuePropByKey("msgconfSlotFor", locale)+" "+exam.get(0).getDistrictAssessmentDetail().getDistrictAssessmentName()+" <br/> <br/>"+Utility.getLocaleValuePropByKey("msgExamDateTime", locale)+": <br/>"+Utility.convertDateAndTimeToUSformatOnlyDate(exam.get(0).getExamDate())+" "+exam.get(0).getExamStartTime()+" "+Utility.getLocaleValuePropByKey("lblTos", locale)+" "+exam.get(0).getExamEndTime()+"</b>");
				sb.append("</div>");
			}
		}	
		else if(!availability && (dateDiff >= 0) && !yourBooking)
		{
			sb.append("</div>");
			sb.append("<script>$('#startdiv').hide();$('#actiondiv').css('margin-top', '0px');</script>");
			sb.append("<script>$('#enddiv').hide();</script>");
			sb.append("<script>$('#showpopupKendoWindow_wnd_title').html('"+Utility.getLocaleValuePropByKey("msgSlotInfo", locale)+"');</script>");
			sb.append("<div class='row' style='padding-top: 26px; margin-right: 20px;padding-left: 18px;width:100%;text-align:center;'>");
			sb.append( Utility.getLocaleValuePropByKey("msgExamAjax11_1", locale)+" "+(examDuration/60.0f)+" "+Utility.getLocaleValuePropByKey("msgExamAjax11_2", locale)+" "+(examDuration/60.0f)+ Utility.getLocaleValuePropByKey("msgExamAjax11_3", locale) );
			sb.append("</div>");
		}
		
	}
		return sb.toString();
	}
	
	
	public String getMultipleInviteAssessment(String teacherIds)
	{
		System.out.println(" teacherIds :: "+teacherIds);
		UserMaster userMaster             =     null;
		DistrictMaster districtMaster     =     null;
		SchoolMaster schoolMaster         =     null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session1 = request.getSession(false);
		if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		if (session1.getAttribute("userMaster") != null) {
			userMaster=(UserMaster)session1.getAttribute("userMaster");
		}
		
		StringBuffer sb = new StringBuffer();
		districtMaster = userMaster.getDistrictId();
		List<DistrictAssessmentDetail> districtAssessmentDetails = districtAssessmentDetailDAO.findByDistrict(districtMaster);
		sb.append("<div class='row'>");
		sb.append("<div class='col-sm-8 col-md-8' style='float:left;'>");
		sb.append("<label>"+Utility.getLocaleValuePropByKey("msgSelectAssessment", locale)+"</label><span  class='required'>*</span><br/>");
		sb.append("<select id='assessId' name='' class='form-control'>");
		if(districtAssessmentDetails!=null && districtAssessmentDetails.size()>0)
		{				
				sb.append("<option value='-1'>"+Utility.getLocaleValuePropByKey("msgSelectAssessment", locale)+"</option>");
				for(DistrictAssessmentDetail dad : districtAssessmentDetails)
				{
					sb.append("<option value="+dad.getDistrictAssessmentId()+">"+dad.getDistrictAssessmentName()+"</option>");
				}
		}
		else
		{
			sb.append("<option value='-1'>"+Utility.getLocaleValuePropByKey("msgNoAssessment", locale)+"</option>");
		}
		sb.append("</select>");
		sb.append("</div>");
		sb.append("<div class='col-sm-4 col-md-4' style='float:left;margin-top:3px;'><br/>");
		sb.append("<button value='"+Utility.getLocaleValuePropByKey("msgSendInvite", locale)+"' class='btn-primary btn-large' onclick=\"sendMultipleInvite('"+teacherIds+"');\">"+Utility.getLocaleValuePropByKey("msgSendInvite", locale)+"</button>");
		sb.append("</div>");
	
		
		return sb.toString();
	}
	
	public String sendMultipleAssessmentInvite(String teacherIds,Integer jobId,Integer districtAssessmentId)
	{

		System.out.println(":::::::::::::::::sendMultipleAssessmentInvite::::::::::");
		System.out.println(" "+teacherIds+" "+jobId+" "+districtAssessmentId);
		
		StringBuffer gridData =	new StringBuffer();
		String forMated= "";
		List<ExaminationCodeDetails> examinationCodeDetailList = new ArrayList<ExaminationCodeDetails>();
		String baseURL=Utility.getValueOfPropByKey("basePath");
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||session.getAttribute("userMaster")==null) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");
			List<Integer> candidatesIds = new ArrayList<Integer>();
			String[] candidates = teacherIds.split(",");
			if(candidates!=null && candidates.length>0)
			{
				for(String s : candidates)
				{
					candidatesIds.add(Integer.parseInt(s));
				}
			}
			List<TeacherDetail> teacherDetail = teacherDetailDAO.findByTeacherIds(candidatesIds);
			DistrictAssessmentDetail districtAssessmentDetail  = districtAssessmentDetailDAO.findById(districtAssessmentId, false, false);
			JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			Map<Integer,ExaminationCodeDetails> examMap= new HashMap<Integer, ExaminationCodeDetails>();
			try {
				examinationCodeDetailList = examinationCodeDetailsDAO.getExamCodeByTeachers(districtAssessmentDetail,teacherDetail);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			if(examinationCodeDetailList!=null && examinationCodeDetailList.size()>0)
			{
				for(ExaminationCodeDetails examiCodeDetails : examinationCodeDetailList)
				{
					examMap.put(examiCodeDetails.getTeacherDetail().getTeacherId(), examiCodeDetails);
				}
			}
			
			Boolean mailFlag=false;
			System.out.println(" teacherDetails :: "+teacherDetail.size());
			if(teacherDetail!=null && teacherDetail.size() > 0)
			{
				SessionFactory factoryExam = examinationCodeDetailsDAO.getSessionFactory();
				StatelessSession statelessSessionExam=factoryExam.openStatelessSession();

				Transaction transactionExam = statelessSessionExam.beginTransaction();
				ExaminationCodeDetails examinationCodeDetails = null;
				for(TeacherDetail tDetail : teacherDetail)
				{
					/*try {
						examinationCodeDetailList = examinationCodeDetailsDAO.getExamCodeByDAssessAndTeacher(districtAssessmentDetail,tDetail);
					} catch (Exception e1) {
						e1.printStackTrace();
					}*/
					examinationCodeDetails = examMap.get(tDetail.getTeacherId());
					String emailAddress = tDetail.getEmailAddress();
					System.out.println(" teacher emailAddress :: "+emailAddress);
					String linkIds = emailAddress+"###"+districtAssessmentId+"###"+jobId;
					forMated = Utility.encodeInBase64(linkIds);
					forMated = baseURL+"examslotselection.do?id="+forMated;
					System.out.println("formatted url ::  "+forMated);
					String assessmentLink="<a href='"+forMated+"'> "+Utility.getLocaleValuePropByKey("msgExamAjax7", locale)+"</a>";
				

					try {
						if(examinationCodeDetails==null)
						{
							mailFlag = true;
							String examinationCode ="";						
							examinationCodeDetails = new ExaminationCodeDetails();
							examinationCodeDetails.setDistrictAssessmentDetail(districtAssessmentDetail);
							examinationCodeDetails.setTeacherDetail(tDetail);
							examinationCodeDetails.setJobOrder(jobOrder);
							examinationCodeDetails.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
							examinationCode = Utility.randomString(16);
							examinationCodeDetails.setExaminationCode(examinationCode);				
							examinationCodeDetails.setExamDate(null);
							examinationCodeDetails.setUserMaster(userMaster);
							examinationCodeDetails.setInviteStatus("I");
							examinationCodeDetails.setStatus("A");
							examinationCodeDetails.setApproved(false);
							examinationCodeDetails.setCreatedDateTime(new Date());
							examinationCodeDetails.setIpaddress(request.getRemoteAddr());
							statelessSessionExam.insert(examinationCodeDetails);
							System.out.println(" Invite Saved :: ");
							gridData.append(tDetail.getFirstName()+" "+tDetail.getLastName());
						}
						else if(examinationCodeDetails!=null && examinationCodeDetails.getExamDate()==null)
						{
							mailFlag = true;
						}
						else
						{
							mailFlag = false;
						}
						if(mailFlag)
						{
							try {
								List<String> lstBcc=new ArrayList<String>();
								try{
									String[] arrBccEmail=Utility.getValueOfSmtpPropByKey("smtphost.assessmentbccemail").split(",");
									for(int i=0; i<arrBccEmail.length;i++){
										if(arrBccEmail[i]!=null && !arrBccEmail[i].equals(""))
											lstBcc.add(arrBccEmail[i]);
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								System.out.println(" Mail Send to ::::::::: "+tDetail.getEmailAddress());
								String content = "";
								content = MailText.mailTextForExamSlotSelection(examinationCodeDetails,assessmentLink);						
								DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
								dsmt.setEmailerService(emailerService);
								dsmt.setMailfrom(Utility.getValueOfSmtpPropByKey("smtphost.adminusername"));
								dsmt.setLstBcc(lstBcc);
								dsmt.setMailto(tDetail.getEmailAddress());						
								dsmt.setMailsubject(Utility.getLocaleValuePropByKey("msgExamAjax8", locale));
								dsmt.setMailcontent(content);
								System.out.println("content  "+content);
								try {
									dsmt.start();	
								} catch (Exception e) {}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
					catch(Exception e)
					{e.printStackTrace();}
				}
				transactionExam.commit();
			}
		
		return gridData.toString();
	
	}
	
	public String getScoreForQuestions(TeacherDistrictAssessmentAnswerDetail[] teacherDistrictAssessmentAnswerDetails,Integer teacherId, AssessmentNotesHistory[] assessmentNotesHistories)
	{
		System.out.println("::::::::::getScoreForQuestions::::");
		Double totalScore = 0.0;
		Double marksObtained = 0.0;
		WebContext context;
		UserMaster userMaster = null;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		String s = "";
			
		List<TeacherDistrictAssessmentQuestion> answerDetails = new ArrayList<TeacherDistrictAssessmentQuestion>();
		List<AssessmentNotesHistory> notesHistories = new ArrayList<AssessmentNotesHistory>();
		Map<Integer,Double> scoreMap = new HashMap<Integer, Double>();
		Map<String,String> notesMap = new HashMap<String, String>();
		if(teacherDistrictAssessmentAnswerDetails!=null && teacherDistrictAssessmentAnswerDetails.length>0)
		{
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			DistrictAssessmentDetail districtAssessmentDetail = teacherDistrictAssessmentAnswerDetails[0].getDistrictAssessmentDetail();
				for(TeacherDistrictAssessmentAnswerDetail teacherDAAD : teacherDistrictAssessmentAnswerDetails)
				{
					answerDetails.add(teacherDAAD.getTeacherDistrictAssessmentQuestion());
					scoreMap.put(teacherDAAD.getTeacherDistrictAssessmentQuestion().getTeacherDistrictAssessmentQuestionId().intValue(), teacherDAAD.getTotalScore());
				}
				System.out.println(" scoreMap :::>>>> "+scoreMap.size());
				try {
					List<TeacherDistrictAssessmentAnswerDetail> tAssessmentAnswerDetails = teacherDistrictAssessmentAnswerDetailDAO.findTeacherAssessmentQuestionAnswers(teacherDetail,districtAssessmentDetail,answerDetails);
					System.out.println(" tAssessmentAnswerDetails ::: "+tAssessmentAnswerDetails.size());
					if(tAssessmentAnswerDetails!=null && tAssessmentAnswerDetails.size() > 0)
					{
						SessionFactory factoryScore = teacherDistrictAssessmentAnswerDetailDAO.getSessionFactory();
						StatelessSession statelessSessionScore=factoryScore.openStatelessSession();
						Transaction transactionScore = statelessSessionScore.beginTransaction();
						
						SessionFactory factoryNotes = assessmentNotesHistoryDAO.getSessionFactory();
						StatelessSession statelessSessionNotes=factoryNotes.openStatelessSession();
						Transaction transactionNotes = statelessSessionNotes.beginTransaction();
						
						for(TeacherDistrictAssessmentAnswerDetail tdans : tAssessmentAnswerDetails)
						{
							tdans.setTotalScore(scoreMap.get(tdans.getTeacherDistrictAssessmentQuestion().getTeacherDistrictAssessmentQuestionId().intValue()));
							tdans.setMaxMarks(tdans.getTeacherDistrictAssessmentQuestion().getMaxMarks()==null?0:tdans.getTeacherDistrictAssessmentQuestion().getMaxMarks());
							statelessSessionScore.update(tdans);
						}
						transactionScore.commit();
						List<TeacherDistrictAssessmentAnswerDetail> answerScoreList = teacherDistrictAssessmentAnswerDetailDAO.findTeacherAssessmentQuestionAnswers(teacherDetail, districtAssessmentDetail, null);
						if(answerScoreList!=null && answerScoreList.size()>0)
						{
							for(TeacherDistrictAssessmentAnswerDetail tans : answerScoreList)
							{
								if(tans.getTotalScore()!=null)
								{
									marksObtained += tans.getTotalScore();
									 totalScore +=tans.getMaxMarks()==null?0:tans.getMaxMarks();
								}
							}
						}
						try{
							teacherDistrictAssessmentStatusDAO.updatefitScore(teacherDetail, districtAssessmentDetail, marksObtained, totalScore,1);
						}catch(Exception e){
							e.printStackTrace();
						}
												
						/*if(assessmentNotesHistories!=null && assessmentNotesHistories.length>0)
						{
								for(AssessmentNotesHistory assessNotes : assessmentNotesHistories)
								{
									if(assessNotes.getAssessmentNotes().length()>0)
									{
										assessNotes.setAssessmentNotes(assessNotes.getAssessmentNotes());
										assessNotes.setCreatedDateTime(new Date());
										assessNotes.setUserMaster(userMaster);
										statelessSessionNotes.insert(assessNotes);
									}
								}
								transactionNotes.commit();
						}*/
						s = "update";
						scoreMap.clear();
						notesMap.clear();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

		}
		return s;
	}
	
	public String getAvailability(String centerScheduleId)
	{		
		System.out.println(" centerScheduleId :: "+centerScheduleId);
		WebContext context;
		UserMaster userMaster = null;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		Boolean availability = false;
		Boolean priorityLocked = false;
		long dateDiff = 0;
		String selectedSlotTime = "";
		String selectedTime = "";
		StringBuffer sb = new StringBuffer();
		Map<Integer,String> timeMap = new HashMap<Integer, String>();
		timeMap = Utility.getTimeForKendoSlots();
		List<DistrictAssessmentDetail> districtAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
		districtAssessmentDetails = districtAssessmentDetailDAO.findByDistrict(userMaster.getDistrictId());
		List<PriorityAssessmentDays> priorityDate = null;
		List<String> slotsTiming = new ArrayList<String>();
		List<String> examSlots = new ArrayList<String>();
		Date dateFromKendo = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
		//DateFormat df = new SimpleDateFormat("E MMM dd hh:mm:ss Z yyyy");
		//	df.setTimeZone(TimeZone.getTimeZone("CST"));	
		SimpleDateFormat time = new SimpleDateFormat("hh:mm a");
		Calendar cl = Calendar. getInstance();
	    Date dbDate = null;
		try {
			if(centerScheduleId!=null && centerScheduleId.length()>0)
			{
				CenterScheduleMaster csCenterScheduleMaster = centerScheduleMasterDAO.findById(Integer.parseInt(centerScheduleId), false, false);
				if(csCenterScheduleMaster!=null)
				{
					selectedSlotTime = csCenterScheduleMaster.getCsTime();
					dateFromKendo = sdf.parse(sdf.format(Utility.convertDateAndTimeToDbFormat(csCenterScheduleMaster.getCsDate())));	
				}
				
				SimpleDateFormat sdfDb = new SimpleDateFormat("yyyy-MM-dd"); // for database date format in centerschedulemaster
				dbDate = sdfDb.parse(sdfDb.format(dateFromKendo));
				List<CenterScheduleMaster> centerScheduleMasters = centerScheduleMasterDAO.getScheduleByDate(csCenterScheduleMaster.getDistrictMaster(),dbDate);
				String listEndTime ="";
				for(CenterScheduleMaster csScheduleMaster : centerScheduleMasters)
				{
					if(csScheduleMaster.getAvailability()!=null && csScheduleMaster.getAvailability()==1)
					{
						availability = true;
						slotsTiming.add(csScheduleMaster.getCsTime());
						listEndTime = csScheduleMaster.getCsTime();
					}
					else if(csScheduleMaster.getAvailability()!=null && csScheduleMaster.getAvailability()==0)
					{
						availability = true;
						slotsTiming.add(csScheduleMaster.getCsTime());
						listEndTime = csScheduleMaster.getCsTime();
					}
					
				}
				/*if(centerScheduleMasters!=null && centerScheduleMasters.size()>0)
				{
					slotsTiming.add(Utility.addMinutesToDateNew(centerScheduleMasters.get(0).getCsDate(), listEndTime, 30));
				}*/
				cl.setTime(dateFromKendo);
				selectedTime = time.format(cl.getTime());
				
				List<ExaminationCodeDetails> exCodeDetails = examinationCodeDetailsDAO.getExamCodeByDate(dateFromKendo);
				if(exCodeDetails!=null && exCodeDetails.size()>0)
				{
					System.out.println(" exCodeDetails size :: "+exCodeDetails);
					for(ExaminationCodeDetails exam : exCodeDetails)
					{
						if(exam.getDistrictAssessmentDetail().getDistrictMaster().getDistrictId().intValue() == userMaster.getDistrictId().getDistrictId().intValue())
						{
							if(exam.getExamDate()!=null)
							{
								examSlots.add(Utility.getFormatDate(exam.getExamDate()));
							}
						}
					}					
				}				
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		try {
			priorityDate = priorityAssessmentDaysDAO.getPriorityDaysByDistrictAndDate(userMaster.getDistrictId(),dbDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		dateDiff = Utility.getDateDifference(new Date() ,dateFromKendo);
		Boolean candidateExists = false; 
		if(examSlots!=null && examSlots.size()>0)
		{
			System.out.println(" Date from kendo :: "+Utility.getFormatDate(dateFromKendo));
			if(examSlots.contains(Utility.getFormatDate(dateFromKendo)))
			{
				//candidateExists = true;
			}
		}
			System.out.println(" candidateExists :: "+candidateExists);
		if(dateDiff >= 0 && (!candidateExists))
		{
			List<AssessmentsPriorityMaster> assPriorityMasters =  assessmentsPriorityMasterDAO.findAll();
			
			sb.append("<div class='' style=''>");
			sb.append("<div id='errorDiv' style='color:red;display:none;padding:15px;'></div>");
			sb.append("<div class='row'>");
			sb.append("<input type='hidden' name='dateTime' id='dateTime' value='"+dateFromKendo+"'/>");
			sb.append("<div class='col-sm-6 col-md-6' style='padding:15px;margin-left:20px;width:45%;'> <b>Date: "+Utility.convertDateAndTimeToUSformatOnlyDate(dateFromKendo)+"</b></div>");
			sb.append("<div class='col-sm-3 col-md-3' style='padding:0px;'></div>");
			sb.append("</div>");

			//System.out.println("Availability ::: "+availability);
			/*if(availability)
		{*/
			sb.append("<div class='row'>");
			if(availability)
			{
				sb.append("<div class='col-sm-5 col-md-5' style='padding-right:0px;'><label class='radio' ><input type='radio' name='lockUnlock' onclick='setlockUnlockFlag(1);disabledPriorityList(1);' id='lockUnlock' value='1' checked='true' style='margin: 1px 0 0;'/>&nbsp;"+Utility.getLocaleValuePropByKey("msgLockAvailability", locale)+"</label></div>");
				sb.append("<div class='col-sm-5 col-md-5' style='margin-left:10px;padding-left:0px;padding-right:0px;'><label class='radio' ><input type='radio' name='lockUnlock' onclick='setlockUnlockFlag(0);disabledPriorityList(0);' id='lockUnlock1' value='0' style='margin: 1px 0 0;' />&nbsp;"+Utility.getLocaleValuePropByKey("msgUnLockAvailability", locale)+"</label></div>");
			}
			else
			{
				sb.append("<div class='col-sm-5 col-md-5' style='padding-right:0px;'><label class='radio' ><input type='radio' name='lockUnlock' onclick='setlockUnlockFlag(1);' id='lockUnlock' value='1' checked='true' style='margin: 1px 0 0;'/>&nbsp;Lock Availability</label></div>");
				sb.append("<div class='col-sm-5 col-md-5' style='margin-left:10px;padding-left:0px;padding-right:0px;'><label class='radio' ><input type='radio' name='lockUnlock' onclick='setlockUnlockFlag(0);' id='lockUnlock1' value='0' style='margin: 1px 0 0;' disabled/>&nbsp;"+Utility.getLocaleValuePropByKey("msgUnLockAvailability", locale)+"</label></div>");
			}
			sb.append("</div>");
			
			
			sb.append("<div class='row' style='align:center;'>");
			if(priorityDate!=null && priorityDate.size() > 0)
			{
				priorityLocked = true;
				sb.append("<div class='col-sm-5 col-md-5' style='padding-right:0px;margin-left:20px;'><label>"+Utility.getLocaleValuePropByKey("msgSelectPriority", locale)+"</label>");
				sb.append("<select id='priorityIds' name='priorityIds' class='form-control' style='height:18px;width:247px;' onchange=\"setPriorityFlag();forcelyChange('"+Utility.convertDateAndTimeToUSformatOnlyDate(dbDate)+"');\">");
				sb.append("<option value='-1'>"+Utility.getLocaleValuePropByKey("msgSelectPriority", locale)+"</option>");
				if(assPriorityMasters!=null && assPriorityMasters.size()>0)
				{
					for(AssessmentsPriorityMaster apm : assPriorityMasters)
					{
						if(apm.getPriorityId().intValue() == priorityDate.get(0).getPriority().getPriorityId().intValue())
						{
							sb.append("<option value="+apm.getPriorityId().intValue()+" selected>"+apm.getPriorityName()+"</option>");
						}
						else
						{
							sb.append("<option value="+apm.getPriorityId().intValue()+" >"+apm.getPriorityName()+"</option>");
						}
					}
				}
				sb.append("</select></div>");	
				sb.append("</div>");
			}
			else
			{				
				sb.append("<div class='col-sm-4 col-md-4' style='padding-right:0px;margin-left:20px;'><label>"+Utility.getLocaleValuePropByKey("msgSelectPriority", locale)+"</label>");
				sb.append("<select id='priorityIds' name='priorityIds' class='form-control' style='height:18px;width:250px;'>");
				sb.append("<option value='-1'>"+Utility.getLocaleValuePropByKey("msgSelectPriority", locale)+"</option>");
				if(assPriorityMasters!=null && assPriorityMasters.size()>0)
				{
					for(AssessmentsPriorityMaster apm : assPriorityMasters)
					{
						sb.append("<option value="+apm.getPriorityId().intValue()+">"+apm.getPriorityName()+"</option>");
					}
				}
				sb.append("</select></div>");
				sb.append("</div>");
			}

			/*}
		else
		{*/
			/*sb.append("<div class='row'>");
			sb.append("<div class='col-sm-4 col-md-4' style='padding-right:0px;'><label class='radio' ><input type='radio' name='availabilityFlag' onclick='setAvailabilityFlag(1);' id='availabilityFlag' value='0' checked='true' style='margin: 1px 0 0;' >&nbsp;Lock Availability</label></div>");
			sb.append("<div class='col-sm-4 col-md-4' style='margin-left:10px;padding-left:0px;padding-right:0px;'><label class='radio' ><input type='radio' name='availabilityFlag' onclick='setAvailabilityFlag(0);' id='availabilityFlag' value='1' style='margin: 1px 0 0;'>&nbsp;Unlock Availability</label></div>");
			sb.append("</div>");*/
			//	}

			/* sb.append("<div class='row'>");
		sb.append("<div class='col-sm-6 col-md-6' style='margin-left:20px;'><label>Select Assessment</label>");
		sb.append("<select id='assessmentIds' name='assessmentIds' class='form-control' style='height:18px;width:268px;'>");
		sb.append("<option value='-1'>Select Assessment</option>");
		if(districtAssessmentDetails!=null && districtAssessmentDetails.size()>0)
		{
			for(DistrictAssessmentDetail dad : districtAssessmentDetails)
			{
				sb.append("<option value='"+dad.getDistrictAssessmentId()+"'>"+dad.getDistrictAssessmentName()+"</option>");
			}
		}
		sb.append("</select>");
		sb.append("</div>");
		sb.append("</div>"); */
			
			sb.append("<div class='row'>");
			sb.append("<div class='col-sm-3 col-md-3' style='padding:15px;margin-left:20px;width:31%;'><label>"+Utility.getLocaleValuePropByKey("lblStartTime", locale)+"</label>");
			int startTm = 0;
			int endTm = 0;
			/*for(int i = 0;i<timeMap.size();i++)
			{
				sb.append("<option value="+i+">"+slotsTiming.get(i)+"</option>");
			}*/
				
			sb.append("<select id='fromTime' name='fromTime' class='form-control' style='height:18px;'>");
			sb.append("<option value='-1'>"+Utility.getLocaleValuePropByKey("lblSltTime", locale)+"</option>");
			
			for(int i = 0;i<timeMap.size();i++)
			{
				if(selectedSlotTime!=null && selectedSlotTime.length()>0)
				{
					if(selectedSlotTime.equals(timeMap.get(i)))
					{
						sb.append("<option value="+i+" selected>"+timeMap.get(i)+"</option>");
					}
					else
					{
						sb.append("<option value="+i+">"+timeMap.get(i)+"</option>");
					}
				}
				
			}

			sb.append("</select></div>");
			sb.append("<div class='col-sm-3 col-md-3' style='padding:15px;width:31%;'><label>"+Utility.getLocaleValuePropByKey("lblEndTime", locale)+"</label>");
			sb.append("<select id='toTime' name='toTime' class='form-control' style='height:18px;'>");
			sb.append("<option value='-1'>"+Utility.getLocaleValuePropByKey("lblSltTime", locale)+"</option>");

			/*int remTime = 0;
		for(int i=0;i<timeMap.size();i++)
		{
			String str = timeMap.get(i);

			if(str.equals(selectedTime))
			{
				remTime = i+1;
			}
		}

		for(int loop=remTime;loop<timeMap.size();loop++)
		{
			sb.append("<option value='"+loop+"'>"+timeMap.get(loop)+"</option>");
		}*/
			Map<Integer,String> times = Utility.getTimeForKendoSlots();
			
			/*if(availability)
			{
				for(int i=0;i<slotsTiming.size();i++)
				{
					if(i==(slotsTiming.size()-1))
					{
						int index = (Integer) Utility.getKeyFromValue(times,slotsTiming.get(i));
							System.out.println(" index :: "+index);
							System.out.println(times.get(index));
						sb.append("<option value="+i+" selected>"+slotsTiming.get(i)+"</option>");
					}
					else
					{
						sb.append("<option value="+i+">"+slotsTiming.get(i)+"</option>");
					}
				}
			}
			else
			{
				for(int i=0;i<timeMap.size();i++)
				{
					sb.append("<option value="+i+">"+timeMap.get(i)+"</option>");
				}
			}*/
			for(int i = 0;i<timeMap.size();i++)
			{
				sb.append("<option value="+i+">"+timeMap.get(i)+"</option>");
			}

			sb.append("</select>");
			sb.append("</div>");
			sb.append("</div>");

			sb.append("<div class='row'>");
			sb.append("<div class='col-sm-4 col-md-4 top15' style='margin-left:20px;'></div>");
		/*	if(!priorityLocked)
			{
				sb.append("<div class='col-sm-6 col-md-6 top5' style='float:right;'><button id='submitAvail' class='btn btn-primary' onclick='saveDAAvailability();'>Lock</button>&nbsp;&nbsp; <button class='btn' onclick='closeAvailabilityKendo();'>Cancel</button></div>");
			}
			else
			{*/
				sb.append("<div id='lckbtn'  class='col-sm-6 col-md-6' style='float:right;'><button id='submitAvail' class='btn btn-primary' onclick='saveDAAvailability();'>"+Utility.getLocaleValuePropByKey("btnSave", locale)+"</button>&nbsp;&nbsp; <button class='btn' onclick='closeAvailabilityKendo();'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+"</button></div>");
				//sb.append("<div id='cnclbtn'  class='col-sm-3 col-md-3' style='float:right;width:31%;'>&nbsp;&nbsp; <button class='btn' onclick='closeAvailabilityKendo();'>Cancel</button></div>");
			//}
			sb.append("</div>");

			sb.append("</div>");
		}
		return sb.toString();
	}
	
	public String saveDaAvailabilities(String dateTime,int flag,String fromTime,String toTime,String overrideFlag,int lockUnlockVal)
	{
		WebContext context;
		UserMaster userMaster = null;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		System.out.println(" overrideFlag :: "+overrideFlag);
		int startTime = 0;
		int endTime = 0;
		System.out.println(" flag :: "+flag);
		System.out.println(" lockUnlockVal :: "+lockUnlockVal);
		String result ="";
		String availDate ="";
		Boolean priorityExists = false;
		Date selectedDate = null;
		Map<Integer,String> timeMaps = Utility.getTimeForKendoSlots();
		List<String> applyAvailabilitySlot = new ArrayList<String>();
		if((!fromTime.equals("-1")) && (!fromTime.equals("-1")))
		{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			DateFormat df = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
			try {
				Date date = df.parse(dateTime);
				selectedDate = sdf.parse(sdf.format(date));
				availDate = Utility.getFormatDate(selectedDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			startTime = Integer.parseInt(fromTime);
			endTime = Integer.parseInt(toTime);
			Map<Integer,String> timeMap = Utility.getTimeForKendoSlots();
			for(int i=startTime;i<endTime;i++)
			{
				//System.out.println(" time slots for save :: "+timeMap.get(i));
				applyAvailabilitySlot.add(timeMap.get(i));
			}
			
			List<CenterScheduleMaster> centerScheduleMasters = centerScheduleMasterDAO.getScheduleByDistrictAndDate(userMaster.getDistrictId(), selectedDate);
			List<PriorityAssessmentDays> priorityDate = priorityAssessmentDaysDAO.getPriorityDaysByDistrictAndDate(userMaster.getDistrictId(),selectedDate);
			List<String> priorityDates = new ArrayList<String>();
			List<String> candidateSlots = new ArrayList<String>();
			List<ExaminationCodeDetails> candidateExamSlots = examinationCodeDetailsDAO.getExamCodeByDistrictAndDate(userMaster.getDistrictId(),selectedDate);
			if(candidateExamSlots!=null && candidateExamSlots.size()>0)
			{
				String start = "";
				String end = "";
				System.out.println(" candidateExamSlots size :: "+candidateExamSlots.size());
				for(ExaminationCodeDetails exam : candidateExamSlots)
				{
					start = exam.getExamStartTime();
					end = exam.getExamEndTime();
					int from =0;
					int to = 0;
					for(int i=0;i<timeMaps.size();i++)
					{
						if(start.equals(timeMap.get(i)))
						{
							from = i;
							//System.out.println(" from :: "+from);
						}
						if(end.equals(timeMap.get(i)))
						{
							to = i;
							//System.out.println(" to :: "+to);
						}					
					}
					for(int f = from;f<to;f++)
					{
						//System.out.println(" time slots :: "+timeMap.get(f));
						candidateSlots.add(timeMap.get(f));
					}					
				}
				
				//System.out.println(" candidate slots :: "+candidateSlots);
				
			}
			
			
			// ======================================================================== //
				if(priorityDate!=null && priorityDate.size()>0)
				{
					priorityExists = true;
					
					for(PriorityAssessmentDays pr : priorityDate)
					{
						priorityDates.add(pr.getAvailableSlots());
					}
				}
				
				SessionFactory factoryCSM = centerScheduleMasterDAO.getSessionFactory();
				StatelessSession statelessSessionCSM=factoryCSM.openStatelessSession();
				Transaction transactionCSM = statelessSessionCSM.beginTransaction();
				
				SessionFactory factoryPAD = priorityAssessmentDaysDAO.getSessionFactory();
				StatelessSession statelessSessionPAD=factoryPAD.openStatelessSession();
				Transaction transactionPAD = statelessSessionPAD.beginTransaction();
				System.out.println(" overrideFlag :: "+overrideFlag);
				Map<String,PriorityAssessmentDays> priorityMap=new HashMap<String, PriorityAssessmentDays>();
				for(PriorityAssessmentDays pad : priorityDate)
				{
					priorityMap.put(pad.getAvailableSlots(), pad);
				}
				
				if(overrideFlag!=null && overrideFlag.length()>0)
				{
					List<AssessmentsPriorityMaster> assessmentsPriorityMaster = assessmentsPriorityMasterDAO.findByPriorityValue(Integer.parseInt(overrideFlag));
					if(overrideFlag.equals("1"))
					{					
						if(centerScheduleMasters!=null && centerScheduleMasters.size()>0)
						{
							for(CenterScheduleMaster csm : centerScheduleMasters)
							{
								if(availDate.equals(Utility.getFormatDate(csm.getCsDate())))
								{	
									if(!candidateSlots.contains(csm.getCsTime()))
									{
										if(priorityDates.contains(csm.getCsTime()) && applyAvailabilitySlot.contains(csm.getCsTime()))
										{
											if(lockUnlockVal==1)
												csm.setAvailability(lockUnlockVal);
											else
												csm.setAvailability(null);
											statelessSessionCSM.update(csm);

											PriorityAssessmentDays pd = priorityMap.get(csm.getCsTime());
											if(pd!=null)
											{
												if(lockUnlockVal==1)
												{
													if(assessmentsPriorityMaster!=null && assessmentsPriorityMaster.size()>0)
													{
														pd.setPriority(assessmentsPriorityMaster.get(0));
													}
													csm.setAvailability(lockUnlockVal);
													statelessSessionPAD.update(pd);
												}
												else
												{			
													if(lockUnlockVal==1)
														csm.setAvailability(lockUnlockVal);
													else
														csm.setAvailability(null);
													if(pd!=null)
													{
														statelessSessionPAD.delete(pd);
													}
												}
											}
										}
										else if(applyAvailabilitySlot.contains(csm.getCsTime()) && !priorityDates.contains(csm.getCsTime()))
										{											
											if(lockUnlockVal==1)
												csm.setAvailability(lockUnlockVal);
											else
												csm.setAvailability(null);
											statelessSessionCSM.update(csm);
											
											PriorityAssessmentDays	pd=new PriorityAssessmentDays();
											if(assessmentsPriorityMaster!=null && assessmentsPriorityMaster.size()>0)
											{
												pd.setPriority(assessmentsPriorityMaster.get(0));
											}
											pd.setDistrictMaster(csm.getDistrictMaster());
											pd.setPreferredDates(csm.getCsDate());
											pd.setAvailableSlots(csm.getCsTime());
											statelessSessionPAD.insert(pd);
											
											
										}
										else if(!applyAvailabilitySlot.contains(csm.getCsTime()) && priorityDates.contains(csm.getCsTime()))
										{
											if(lockUnlockVal==1)
												csm.setAvailability(lockUnlockVal);
											else
												csm.setAvailability(null);
											
											PriorityAssessmentDays pd = priorityMap.get(csm.getCsTime());
											if(pd!=null)
												if(assessmentsPriorityMaster!=null && assessmentsPriorityMaster.size()>0)
												{
													pd.setPriority(assessmentsPriorityMaster.get(0));
												}
											statelessSessionPAD.update(pd);
										}
									}
								}
							}					
						}
					}
					else if(overrideFlag.equals("2"))
					{										
						if(centerScheduleMasters!=null && centerScheduleMasters.size()>0)
						{
							for(CenterScheduleMaster csm : centerScheduleMasters)
							{
								if(availDate.equals(Utility.getFormatDate(csm.getCsDate())))
								{										
									if(!candidateSlots.contains(csm.getCsTime()))
									{
										if( priorityDates.contains(csm.getCsTime()) && applyAvailabilitySlot.contains(csm.getCsTime()))
										{
											if(lockUnlockVal==1)
												csm.setAvailability(lockUnlockVal);
											else
												csm.setAvailability(null);	
											statelessSessionCSM.update(csm);
											PriorityAssessmentDays pd = priorityMap.get(csm.getCsTime());
											if(pd!=null)
											{
												if(lockUnlockVal==1)
												{
													if(assessmentsPriorityMaster!=null && assessmentsPriorityMaster.size()>0)
													{
														pd.setPriority(assessmentsPriorityMaster.get(0));
													}
													csm.setAvailability(lockUnlockVal);
													statelessSessionPAD.update(pd);
												}
												else
												{									
													if(pd!=null)
													{
														statelessSessionPAD.delete(pd);
													}
												}
											}
										}
										else if(applyAvailabilitySlot.contains(csm.getCsTime()) && !priorityDates.contains(csm.getCsTime()))
										{											
											if(lockUnlockVal==1)
												csm.setAvailability(lockUnlockVal);
											else
												csm.setAvailability(null);
											statelessSessionCSM.update(csm);
											
											PriorityAssessmentDays	pd=new PriorityAssessmentDays();
											if(assessmentsPriorityMaster!=null && assessmentsPriorityMaster.size()>0)
											{
												pd.setPriority(assessmentsPriorityMaster.get(0));
											}
											pd.setDistrictMaster(csm.getDistrictMaster());
											pd.setPreferredDates(csm.getCsDate());
											pd.setAvailableSlots(csm.getCsTime());
											statelessSessionPAD.insert(pd);
											
											
										}
										else if(!applyAvailabilitySlot.contains(csm.getCsTime()) && priorityDates.contains(csm.getCsTime()))
										{
											if(lockUnlockVal==1)
												csm.setAvailability(lockUnlockVal);
											else
												csm.setAvailability(null);
											
											PriorityAssessmentDays pd = priorityMap.get(csm.getCsTime());
											if(pd!=null)
												if(assessmentsPriorityMaster!=null && assessmentsPriorityMaster.size()>0)
												{
													pd.setPriority(assessmentsPriorityMaster.get(0));
												}
											statelessSessionPAD.update(pd);
										}
									}
								}
							}					
						}
					}
				}
				else
				{
					if(centerScheduleMasters!=null && centerScheduleMasters.size()>0)
					{				
						List<AssessmentsPriorityMaster> assessmentsPriorityMaster = assessmentsPriorityMasterDAO.findByPriorityValue(flag);
						for(CenterScheduleMaster csm : centerScheduleMasters)
						{
							if(availDate.equals(Utility.getFormatDate(csm.getCsDate())) && applyAvailabilitySlot.contains(csm.getCsTime()))
							{
								if(!candidateSlots.contains(csm.getCsTime()))
								{
									if(lockUnlockVal==1)
										csm.setAvailability(lockUnlockVal);
									else
										csm.setAvailability(null);
									statelessSessionCSM.update(csm);

									if(!priorityExists)
									{
										PriorityAssessmentDays priorityAssessmentDays = new PriorityAssessmentDays();
										priorityAssessmentDays.setAvailableSlots(csm.getCsTime());
										priorityAssessmentDays.setDistrictMaster(userMaster.getDistrictId());
										priorityAssessmentDays.setPreferredDates(csm.getCsDate());
										if(assessmentsPriorityMaster!=null && assessmentsPriorityMaster.size()>0)
										{
											priorityAssessmentDays.setPriority(assessmentsPriorityMaster.get(0));
										}
										statelessSessionPAD.insert(priorityAssessmentDays);
									}
									else
									{
										PriorityAssessmentDays pd = priorityMap.get(csm.getCsTime());
										if(pd!=null)
										{
											if(lockUnlockVal==1)
											{
												if(assessmentsPriorityMaster!=null && assessmentsPriorityMaster.size()>0)
												{
													pd.setPriority(assessmentsPriorityMaster.get(0));
												}
												//	csm.setAvailability(lockUnlockVal);
												statelessSessionPAD.update(pd);
											}
											else
											{									
												if(pd!=null)
												{
													statelessSessionPAD.delete(pd);
												}
											}
										}
										else
										{
											if(lockUnlockVal==1)
											{
												pd=new PriorityAssessmentDays();
												if(assessmentsPriorityMaster!=null && assessmentsPriorityMaster.size()>0)
												{
													pd.setPriority(assessmentsPriorityMaster.get(0));
												}
												pd.setDistrictMaster(csm.getDistrictMaster());
												pd.setPreferredDates(csm.getCsDate());
												pd.setAvailableSlots(csm.getCsTime());
												statelessSessionPAD.insert(pd);
											}
										}
									}
								}
							}
						}					
					}
				}
				transactionCSM.commit();
				transactionPAD.commit();
				statelessSessionCSM.close();
				statelessSessionPAD.close();				
				result = "success";
				
			}
			else
			{
				System.out.println(" Timing(s) Not Received....");
			}			
					
		return "";
	}	
	
}