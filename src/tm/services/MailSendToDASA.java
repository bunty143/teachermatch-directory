package tm.services;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.cgreport.UserMailSend;
import tm.bean.user.UserMaster;

public class MailSendToDASA  extends Thread{
	EmailerService emailerService = null;
	List<UserMailSend> userListTRD=new ArrayList<UserMailSend>();
	public MailSendToDASA(List<UserMailSend> userList,EmailerService emailerService)
	{
		userListTRD=userList;
		this.emailerService = emailerService;
	}
	public void run() {
		try 
		{
			Thread.sleep(1000);
			System.out.println("MailSendToDASA Size :: "+userListTRD.size());
			int iCount=0;
			int iTotalCount=userListTRD.size();
			for(UserMailSend userMailSend :userListTRD)
			{
				iCount++;
				 String toEmail="";
				 String sEmailSubject="";
				 String sPanelUserName="";
				 String sPanelUserNameTo="";
				 String sTeacheName="";
				 String sJobTitle="";
				 String sStatusName="";
				 String requisitionNumber="";
				 int offerReadyMailFlag=0;
				 int flag=0;
				 String schoolLocation="";
				 UserMaster userMaster=null;
				 
				 String locationCode="";
				    try{
						if(userMailSend.getLocationCode()!=null && !userMailSend.getLocationCode().equals("")){
							locationCode = " ("+userMailSend.getLocationCode()+")";
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				 if(userMailSend.getEmail()!=null){
					 toEmail=userMailSend.getEmail();
				 }
				 
				 if(userMailSend.getSubject()!=null){
					 sEmailSubject=userMailSend.getSubject();
				 }
				 
				 if(userMailSend.getFromUserName()!=null){
					 sPanelUserName=userMailSend.getFromUserName();
				 }
				 
				 if(userMailSend.getToUserName()!=null){
					 sPanelUserNameTo=userMailSend.getToUserName();
				 }
				
				 if(userMailSend.getTeacherName()!=null){
					 sTeacheName=userMailSend.getTeacherName();
				 }
				 
				 if(userMailSend.getJobTitle()!=null){
					 sJobTitle=userMailSend.getJobTitle();
				 }
				 
				 if(userMailSend.getStatusName()!=null){
					 sStatusName=userMailSend.getStatusName();
				 }
				 
				 if(userMailSend.getUserMaster()!=null){
					 userMaster=userMailSend.getUserMaster();
				 }
				 
				 if(userMailSend.getIsUserOrTeacherFlag()!=0){
					 offerReadyMailFlag=userMailSend.getIsUserOrTeacherFlag();
				 }
				 if(userMailSend.getSchoolLocation()!=null){
					 schoolLocation=userMailSend.getSchoolLocation()+locationCode;;
				 }
				 if(userMailSend.getFlag()!=null){
					 flag=userMailSend.getFlag();
				 }
				 if(userMailSend.getRequisitionNumber()!=null){
					 requisitionNumber=userMailSend.getRequisitionNumber();
				 }
				 String emailBodyText="";
				 try{
					 System.out.println("userMailSend.getSubject()::::"+userMailSend.getSubject());
				 }catch(Exception e){
					e.printStackTrace();
				 }
				 try{
                   	System.out.println(":::::::::::::::::::::::::Offer Decline And Accepted ::::::::::::>::::::"+userMailSend.getSubject());
                    emailBodyText=MailText.getFourthAcceptOrDecline(sPanelUserName,userMailSend.getTeacherDetail(),sJobTitle,schoolLocation,flag,userMaster);
                    System.out.println("toEmail:::"+toEmail);
                    emailerService.sendMailAsHTMLTextWithBCC(toEmail, userMailSend.getSubject(),emailBodyText,null,"offer");
				 }catch(Exception e){ e.printStackTrace();}
			}
		}catch (NullPointerException en) {
			en.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
  }
}	
