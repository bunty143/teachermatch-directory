package tm.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobOrderDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.Utility;

public class UserHomeAjax
{	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	@Autowired
	private UserMasterDAO userMasterDAO;	
	public UserMasterDAO getUserMasterDAO() {
		return userMasterDAO;
	}
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public int getJob(String id)
	{	
		int noOfjobs=0;
		try
		{
		List<JobOrder> jobOrders=null;
		DistrictMaster districtMaster=districtMasterDAO.findByDistrictId(id);	
		if(districtMaster!=null && districtMaster!=null)
		{
			jobOrders=jobOrderDAO.findAllJobOrderbyDistrict(districtMaster);
		}
		System.out.println(jobOrders.size());
		noOfjobs=jobOrders.size();
		}
		catch(Exception e)
		{e.printStackTrace();}
		return noOfjobs;
	}	
	public String editDistrict(String fname,String lname,String email,String oldemail,int reqType)
	{
		System.out.println("reqType=="+reqType);
		String i="0";
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession();		
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:quest.do";
		}
		
		try
		{		
		UserMaster master=(UserMaster) session.getAttribute("userMaster");	
		master.setUserId(master.getUserId());
		master.setFirstName(fname);
		master.setLastName(lname);
		master.setEmailAddress(email);
		//master.setPassword(password);
	   // master.setPassword(MD5Encryption.toMD5(password));
		userMasterDAO.updatePersistent(master);	
		DistrictMaster districtMaster=districtMasterDAO.findByDistrictId(master.getDistrictId().getDistrictId()+"");
		if(districtMaster!=null)
		{
			districtMaster.setJobApplicationCriteriaForProspects(reqType);
			districtMasterDAO.updatePersistent(districtMaster);
		}		
		session.setAttribute("userMaster", master);			
		i="1";		
		if(!oldemail.equalsIgnoreCase(email))
		{
			//mail to admin
		}
		}
		catch(Exception e)
		{e.printStackTrace();}
		
		return i;
	}
	
	public String SearchDistrictRecords(int entityID,String DistrictName,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer tmRecords =	new StringBuffer();
		try{
			
			//-- get no of record in grid,
			//-- set start and end position			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord 		=	0;
			//------------------------------------			
			UserMaster userMaster = null;
			int districtId =0;
			int entityIDLogin=0;
			int roleId=0;

			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getDistrictId()!=null){
					
					districtId =userMaster.getDistrictId().getDistrictId();
				}
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				entityIDLogin=userMaster.getEntityType();
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,5,"managedistrict.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(districtId!=0 && entityIDLogin==2){
				entityID=entityIDLogin;
			}
		
			List<DistrictMaster> districtMaster	  =	null;
			
			/** set default sorting fieldName **/
			String sortOrderFieldName="districtName";
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				sortOrderFieldName=sortOrder;
			}
			String sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
			if(sortOrderType!=null)
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("1")){
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}

			/**End ------------------------------------**/
			
			if(entityID==1 || districtId==0){
					if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
						Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
						totaRecord			= districtMasterDAO.getRowCountWithSort(sortOrderStrVal,criterion);
						districtMaster  	= districtMasterDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion);
					}else{
						totaRecord			= districtMasterDAO.getRowCountWithSort(sortOrderStrVal);
						districtMaster  	= districtMasterDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage);
					}
			}else{
				Criterion criterion= Restrictions.eq("districtId", districtId);
				totaRecord			= districtMasterDAO.getRowCountWithSort(sortOrderStrVal,criterion);
				districtMaster  	= districtMasterDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion);
			}
				
			tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDistrictName", locale),sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");		
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblFinalDecisionMaker", locale),sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCreatedOn", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblStatus", locale),sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			tmRecords.append(" <th  valign='top'>"+responseText+"</th>");
			tmRecords.append(" <th valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(districtMaster.size()==0)
				tmRecords.append("<tr><td colspan='8' align='center'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			
			System.out.println(Utility.getLocaleValuePropByKey("msgNoOfRecords", locale)+" "+districtMaster.size());

			for (DistrictMaster districtMasterDetails : districtMaster) 
			{
				tmRecords.append("<tr>");
				tmRecords.append("<td  >"+districtMasterDetails.getDistrictName()+"</td>");	
				tmRecords.append("<td>"+districtMasterDetails.getDmName()+"</td>");
				tmRecords.append("<td nowrap>"+Utility.convertDateAndTimeToUSformatOnlyDate(districtMasterDetails.getCreatedDateTime())+"</td>");
				tmRecords.append("<td>");
				if(districtMasterDetails.getStatus().equalsIgnoreCase("A"))
					tmRecords.append("Active  ");
				else
					tmRecords.append("Inactive");
				tmRecords.append("</td>");
				tmRecords.append("<td nowrap>");
				
					tmRecords.append("<a rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgClickEditRecord", locale)+"' class='editrecord' rel='tooltip'  href='javascript:void(0);' onclick=\"return editDistrict("+districtMasterDetails.getDistrictId()+")\">"+Utility.getLocaleValuePropByKey("lblEdit", locale));
				
					tmRecords.append("<script>$('.editrecord').tooltip();</script>");	
				tmRecords.append("</td>");
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForManageJobOrdersAjax(request,totaRecord,noOfRow, pageNo));
		
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tmRecords.toString();
	}
	public String getDistrictRecords(String districtId)
	{
		String s="";
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession();		
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:quest.do";
		}
		
		try
		{		
		UserMaster master=(UserMaster) session.getAttribute("userMaster");	
		if(master!=null)
		{
			s=master.getDistrictId().getDistrictName()+"::"+master.getEmailAddress()+"::"+master.getFirstName()+"::"+master.getLastName()+"::"+master.getPassword();	
			
			DistrictMaster districtMaster=districtMasterDAO.findByDistrictId(master.getDistrictId().getDistrictId()+"");
			if(districtMaster!=null)
			{
				s=s+"::"+districtMaster.getJobApplicationCriteriaForProspects();
			}
		}
		
		System.out.println("s===="+s);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}	
	public String displaySchoolRecords()
	{
		String s="";
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession();		
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:quest.do";
		}
		
		try
		{		
		UserMaster master=(UserMaster) session.getAttribute("userMaster");	
		
		
		
		
		
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}	
	
}	