package tm.services.certificate;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.utility.Utility;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

	@Controller
	public class PrintCertificateAjax {
	

		@Autowired
		private TeacherDetailDAO teacherDetailDAO;
		public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) {
			this.teacherDetailDAO = teacherDetailDAO;
		}
		
		@Autowired
		private  TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
		public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
			this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
		}
		
		/*@RequestMapping(value="/printcertificate1.do", method=RequestMethod.GET)
		public  void doPrintCertificateGET(ModelMap map,HttpServletRequest request)
		{
			
			//WebContext context;
			//context = WebContextFactory.get();
			//HttpServletRequest httpRequest = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			//String basePath = context.getServletContext().getRealPath("/")+"/candidatereports/";
			String basePath = request.getSession().getServletContext().getRealPath("/")+"/headquarter/";
			String fileName ="PDReport.pdf";
			String path = request.getContextPath();
			String urlRootPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
			generatePDFSkillCertificate( request , basePath , fileName , urlRootPath);
			//return urlRootPath+"candidatereports/"+teacherId+"/"+fileName;
		 }*/
		


		
		
		public static void generatePDFCertCompGiudLine(HttpServletRequest httpRequest ,String reportPath , String fileName , String urlRootPath)
		{


			
			Font font20 = null;
			Font font16 = null;
			Font font16bold = null;
			Font font11bold = null;
			Font font7 = null;
			Font font9bold = null;
			Font font9 = null;
			BaseFont tahoma = null;
			String fontPath = httpRequest.getRealPath("/");
			
			try {

				tahoma = BaseFont.createFont(fontPath+"fonts/42933.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
				font20 = new Font(tahoma, 20,Font.NORMAL);
				font11bold = new Font(tahoma, 11,Font.BOLD);
				font16 = new Font(tahoma, 16 , Font.NORMAL);
				font16bold = new Font(tahoma, 16 , Font.BOLD);
				font7 = new Font(tahoma, 7);
				font9bold = new Font(tahoma, 9,Font.BOLD);
				font9 = new Font(tahoma, 9);
			
			} catch (DocumentException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			Document document=null;
			FileOutputStream fos = null;
			PdfWriter writer = null;
			
			try{

				File file = new File(reportPath+"/temp");
				if(!file.exists())
					file.mkdirs();
				else
				Utility.deleteAllFileFromDir(reportPath+"/temp");
				document=new Document(PageSize.LETTER.rotate(),30f,30f,30f,55f);
				document.addAuthor("TeacherMatch");
				document.addCreator("TeacherMatch Inc.");
				document.addSubject("CERTIFICATION REPORT");
				document.addCreationDate();
				document.addTitle("CERTIFICATION REPORT");

				fos = new FileOutputStream(reportPath+"/"+fileName);
				writer = PdfWriter.getInstance(document, fos);
				writer.setStrictImageSequence(true);
				
				writer.setBoxSize("art", new Rectangle(29, 36, 760, 572));
				document.open();
				
				Rectangle rect= new Rectangle(29, 36, 760, 572);
		        rect.enableBorderSide(1);
		        rect.enableBorderSide(2);
		        rect.enableBorderSide(4);
		        rect.enableBorderSide(8);
		        rect.setBorder(Rectangle.BOX);
		        rect.setBorderWidth(1);
		        rect.setBorderColor(Color.BLACK);
		        document.add(rect);
				
				PdfPTable mainTable = new PdfPTable(1);
				mainTable.setWidthPercentage(100);
				
				/////////////////////////// Start 1st page /////////////////////////////////////////
				Paragraph [] para = null;
				PdfPCell [] cell = null;

				para = new Paragraph[20];
				cell = new PdfPCell[20];
				para[0] = new Paragraph(" ",font20);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[0].setBorder(0);

				
				Image logo = Image.getInstance (fontPath+"/images/certificateCompletionHeader1.png");
				logo.scalePercent(91);
				//float[] widthParticularlogo = {.5f,.5f};
				PdfPTable logotable = new PdfPTable(1);
				logotable.setWidthPercentage(100);
				PdfPCell logocol1 = new PdfPCell(new Paragraph("     ",font7));
				logocol1.setBorder(0);
				PdfPCell logocol2 = new PdfPCell(logo);
				logocol2.setBorder(0);
				
				logotable.addCell(logocol1);
				logotable.addCell(logocol2);
				cell[1]= new PdfPCell(logotable);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				
				para[4] = new Paragraph("This is to certify that",font16);
				cell[4]= new PdfPCell(para[4]);
				cell[4].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[4].setBorder(0);
				
				para[5] = new Paragraph("  Sandeep ",font16bold);
				cell[5]= new PdfPCell(para[5]);
				cell[5].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[5].setBorder(0);
				
				para[6] = new Paragraph("Has Completed the Smart Practices Substitute Teacher",font16);
				cell[6]= new PdfPCell(para[6]);
				cell[6].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[6].setBorder(0);
				
				para[7] = new Paragraph("  Introductory Professional Development Program",font16);
				cell[7]= new PdfPCell(para[7]);
				cell[7].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[7].setBorder(0);
				
                Image logoSignature = Image.getInstance (fontPath+"/images/signCertCompletion.png");
                logoSignature.scalePercent(45);
                float[] widthParticularlogo = {.04f,.2f};
				PdfPTable logotableSig = new PdfPTable(widthParticularlogo);
				logotableSig.setWidthPercentage(100);
				PdfPCell logocolSig1 = new PdfPCell(new Paragraph("    ",font7));
				logocolSig1.setBorder(0);
				PdfPCell logocolSig2 = new PdfPCell(logoSignature);
				logocolSig2.setBorder(0);
				
				logotableSig.addCell(logocolSig1);
				logotableSig.addCell(logocolSig2);
				
				cell[8]= new PdfPCell(logotableSig);
				cell[8].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[8].setBorder(0);
				
				mainTable.addCell(cell[1]);
				mainTable.addCell(cell[0]);
				
				mainTable.addCell(cell[4]);
				mainTable.addCell(cell[0]);
				mainTable.addCell(cell[5]);
				mainTable.addCell(cell[0]);
				mainTable.addCell(cell[6]);
				
				mainTable.addCell(cell[7]);
				mainTable.addCell(cell[0]);
				mainTable.addCell(cell[8]);
				document.add(mainTable);
				document.newPage();
				
				
			    //////////Second Page ///////////////
				
			    rect= new Rectangle(25, 31, 765, 585);
		        rect.enableBorderSide(1);
		        rect.enableBorderSide(2);
		        rect.enableBorderSide(4);
		        rect.enableBorderSide(8);
		        rect.setBorder(Rectangle.BOX);
		        rect.setBorderWidth(1);
		        rect.setBorderColor(Color.BLACK);
		        document.add(rect);
				
		
		        para = new Paragraph[35];
				
		        Image logoSkillStart = Image.getInstance (fontPath+"/images/smartStartHeader1.png");
		        logoSkillStart.scaleAbsolute(731f, 105f);
		        document.add(logoSkillStart);
		        para[2] = new Paragraph(new Chunk("Competency and Objective",font9bold));
				para[2].setSpacingAfter(1);
				para[2].setIndentationLeft(70);
			    document.add(para[2]);
			    
			    
			    para[3] = new Paragraph(new Chunk("1.1              "+new Chunk( "Important Qualities of a Substitute Professional ",font9bold),font9bold));
			//	para[3].setSpacingAfter(1);
				para[3].setIndentationLeft(75);
				para[3].setIndentationRight(75);
			    document.add(para[3]);
			    
			    para[4] = new Paragraph(new Chunk("1.1a            Understand how effective educators engage students.",font9));
			//	para[4].setSpacingAfter(1);
				para[4].setIndentationLeft(75);
				para[4].setIndentationRight(75);
			    document.add(para[4]);
			    
			    para[5] = new Paragraph(new Chunk("1.1b            Understand how reliability, flexibility, and self-control are important qualities for a substitute professional.",font9));
				//para[5].setSpacingAfter(1);
				para[5].setIndentationLeft(75);
				para[5].setIndentationRight(75);
			    document.add(para[5]);
			    
			    para[6] = new Paragraph(new Chunk("1.2             "+new Chunk( " Ethics of Teaching ",font9),font9bold));
				//para[6].setSpacingAfter(1);
				para[6].setIndentationLeft(75);
				para[6].setIndentationRight(75);
			    document.add(para[6]);
			    
			    para[7] = new Paragraph(new Chunk("1.2a            Understand how ethics provide a guidance for behavior and beliefs. ",font9));
			//	para[7].setSpacingAfter(1);
				para[7].setIndentationLeft(75);
				para[7].setIndentationRight(75);
			    document.add(para[7]);
			    
			    para[8] = new Paragraph(new Chunk("1.2b            Understand the seven points of ethical behavior for professional educators.",font9));
			//	para[8].setSpacingAfter(1);
				para[8].setIndentationLeft(75);
				para[8].setIndentationRight(75);
			    document.add(para[8]);
			    
			    para[9] = new Paragraph(new Chunk("1.2c            Understand how ethics balance the protection of students - physically and emotionally - with demands of the job, while",font9));
				//para[9].setSpacingAfter(1);
				para[9].setIndentationLeft(75);
				para[9].setIndentationRight(75);
			    document.add(para[9]);
			    
			    para[31] = new Paragraph(new Chunk("shepherding student development.",font9));
				//para[9].setSpacingAfter(1);
				para[31].setIndentationLeft(125);
				para[31].setIndentationRight(75);
			    document.add(para[31]);
			    
			    para[10] = new Paragraph(new Chunk("1.3             "+new Chunk( " Legal Obligations",font9bold),font9bold));
			//	para[10].setSpacingAfter(1);
				para[10].setIndentationLeft(75);
				para[10].setIndentationRight(75);
			    document.add(para[10]);
			    
			    para[11] = new Paragraph(new Chunk("1.3a            Understand the importance of CAPTA, HIPAA, and FERPA.",font9));
				//para[11].setSpacingAfter(1);
				para[11].setIndentationLeft(75);
				para[11].setIndentationRight(75);
			    document.add(para[11]);
			    
			    para[12] = new Paragraph(new Chunk("1.4             "+new Chunk( "Social Behavior Guidelines",font9bold),font9bold));
			//	para[12].setSpacingAfter(1);
				para[12].setIndentationLeft(75);
				para[12].setIndentationRight(75);
			    document.add(para[12]);
			    
			    para[13] = new Paragraph(new Chunk("1.4a            Learn the social behavior guidelines for working in schools: Social interactions with students, no-touch policy, minimizing",font9));
				//para[13].setSpacingAfter(1);
				para[13].setIndentationLeft(75);
				para[13].setIndentationRight(75);
			    document.add(para[13]);
			    
			    para[32] = new Paragraph(new Chunk("information sharing, and social media guidelines.",font9));
				//para[13].setSpacingAfter(1);
				para[32].setIndentationLeft(125);
				para[32].setIndentationRight(75);
			    document.add(para[32]);
			    
			    para[14] = new Paragraph(new Chunk("1.5             "+new Chunk( " Learning About Technology",font9bold),font9bold));
			//	para[14].setSpacingAfter(1);
				para[14].setIndentationLeft(75);
				para[14].setIndentationRight(75);
			    document.add(para[14]);
			    
			    para[15] = new Paragraph(new Chunk("2.6a            Ensure lesson plans are completed.",font9));
			//	para[15].setSpacingAfter(1);
				para[15].setIndentationLeft(75);
				para[15].setIndentationRight(75);
			    document.add(para[15]);
			    
			    para[16] = new Paragraph(new Chunk("1.5a            Understand why it's important to become familiar with technology frequently used in schools.",font9));
			//	para[16].setSpacingAfter(1);
				para[16].setIndentationLeft(75);
				para[16].setIndentationRight(75);
			    document.add(para[16]);
			    
			    para[17] = new Paragraph(new Chunk("1.5b            Understand appropriate and inappropriate uses for school technology in the classroom.",font9));
			//	para[17].setSpacingAfter(1);
				para[17].setIndentationLeft(75);
				para[17].setIndentationRight(75);
			    document.add(para[17]);
			    
			    para[18] = new Paragraph(new Chunk("1.6             "+new Chunk( " Your First Placement: The Night Before ",font9bold),font9bold));
			//	para[18].setSpacingAfter(1);
				para[18].setIndentationLeft(75);
				para[18].setIndentationRight(75);
			    document.add(para[18]);
			    
			    para[19] = new Paragraph(new Chunk("1.6a            Understand the importance of checking the school website for important information.",font9));
			//	para[19].setSpacingAfter(1);
				para[19].setIndentationLeft(75);
				para[19].setIndentationRight(75);
			    document.add(para[19]);
			    
			    para[20] = new Paragraph(new Chunk("1.6b            Understand appropriate attire for a professional educator.",font9));
				//para[20].setSpacingAfter(1);
				para[20].setIndentationLeft(75);
				para[20].setIndentationRight(75);
			    document.add(para[20]);
			    
			    
			    para[21] = new Paragraph(new Chunk("1.7             "+new Chunk( "  The Day Has Arrived ",font9bold),font9bold));
			//	para[21].setSpacingAfter(1);
				para[21].setIndentationLeft(75);
				para[21].setIndentationRight(75);
			    document.add(para[21]);
			    
			    
			    para[22] = new Paragraph(new Chunk("1.7a            Understand the importance of checking-in with school administrators and staff.",font9));
			//	para[22].setSpacingAfter(1);
				para[22].setIndentationLeft(75);
				para[22].setIndentationRight(75);
			    document.add(para[22]);
			    
			    para[23] = new Paragraph(new Chunk("1.7b            Understand basic classroom rules: Food and drink, transportation, and medical guidelines.",font9));
			//	para[23].setSpacingAfter(1);
				para[23].setIndentationLeft(75);
				para[23].setIndentationRight(75);
			    document.add(para[23]);
			    
			    para[24] = new Paragraph(new Chunk("1.8             "+new Chunk( " You Are in Your Classroom ",font9bold),font9bold));
			//	para[24].setSpacingAfter(1);
				para[24].setIndentationLeft(75);
				para[24].setIndentationRight(75);
			    document.add(para[24]);
			    
			    para[25] = new Paragraph(new Chunk("1.8a            Understand basic student orientation procedures: Checking lesson plans, photocopying, bell schedule, dismissal",font9));
			//	para[25].setSpacingAfter(1);
				para[25].setIndentationLeft(75);
				para[25].setIndentationRight(75);
			    document.add(para[25]);
			    
			    
			    para[33] = new Paragraph(new Chunk("procedures, room arrangement, classroom technology, and classroom neighbors.",font9));
				
				para[33].setIndentationLeft(125);
				para[33].setIndentationRight(75);
			    document.add(para[33]);
		    
			    para[26] = new Paragraph(new Chunk("1.9             "+new Chunk( "  In Case of Emergency ",font9bold),font9bold));
			//	para[26].setSpacingAfter(1);
				para[26].setIndentationLeft(75);
				para[26].setIndentationRight(75);
			    document.add(para[26]);
			    
			    para[27] = new Paragraph(new Chunk("1.9a            Understand how to react to disaster drills: Fire, natural disaster, and lockdown.",font9));
			//	para[27].setSpacingAfter(1);
				para[27].setIndentationLeft(75);
				para[27].setIndentationRight(75);
			    document.add(para[27]);
			    
			    para[28] = new Paragraph(new Chunk("1.9b            Understand how to react to emergencies involving students: Treats, injury, weapons, drugs.",font9));
			//	para[28].setSpacingAfter(1);
				para[28].setIndentationLeft(75);
				para[28].setIndentationRight(75);
			    document.add(para[28]);
			    document.newPage();
				
				
			    //////////THIRD PAGE///////////
			    
			    rect= new Rectangle(25, 35, 765, 578);
		        rect.enableBorderSide(1);
		        rect.enableBorderSide(2);
		        rect.enableBorderSide(4);
		        rect.enableBorderSide(8);
		        rect.setBorder(Rectangle.BOX);
		        rect.setBorderWidth(1);
		        rect.setBorderColor(Color.BLACK);
		        document.add(rect);
			    
			    
                para = new Paragraph[35];
				
		        Image logClassRoom = Image.getInstance (fontPath+"/images/classroomStartHeader1.png");
		        logClassRoom.scaleAbsolute(731f, 105f);
		        document.add(logClassRoom);
		       
		        para[2] = new Paragraph(new Chunk("Competency and Objective",font9bold));
				para[2].setSpacingAfter(1);
				para[2].setIndentationLeft(70);
			    document.add(para[2]);
			    
			    para[3] = new Paragraph(new Chunk("2.1              "+new Chunk( "You Are the Teacher Today",font9bold),font9bold));
				para[3].setSpacingAfter(4);
				para[3].setIndentationLeft(75);
				para[3].setIndentationRight(75);
			    document.add(para[3]);
			    
			    para[4] = new Paragraph(new Chunk("2.1a            Understand how to provide students with a normal academic day.",font9));
				para[4].setSpacingAfter(4);
				para[4].setIndentationLeft(75);
				para[4].setIndentationRight(75);
			    document.add(para[4]);
			    
			    para[5] = new Paragraph(new Chunk("2.2              "+new Chunk( "The First Five Minutes",font9bold),font9bold));
				para[5].setSpacingAfter(4);
				para[5].setIndentationLeft(75);
				para[5].setIndentationRight(75);
			    document.add(para[5]);
			    
			    para[6] = new Paragraph(new Chunk("2.2a            Understand how to set the tone for the day: Greeting students and providing instructions.",font9));
				para[6].setSpacingAfter(4);
				para[6].setIndentationRight(75);
				para[6].setIndentationLeft(75);
			    document.add(para[6]);
			    
			    para[7] = new Paragraph(new Chunk("2.3              "+new Chunk( "Attendance and the First Assignment ",font11bold),font9bold));
				para[7].setSpacingAfter(4);
				para[7].setIndentationRight(75);
				para[7].setIndentationLeft(75);
			    document.add(para[7]);
			    
			    para[8] = new Paragraph(new Chunk("2.3a            Understand the importantance of taking attendance immediately.",font9));
				para[8].setSpacingAfter(4);
				para[8].setIndentationRight(75);
				para[8].setIndentationLeft(75);
			    document.add(para[8]);
			    
			    para[9] = new Paragraph(new Chunk("2.3b            Understand how to engage students with an opening assignment.",font9));
				para[9].setSpacingAfter(4);
				para[9].setIndentationRight(75);
				para[9].setIndentationLeft(75);
			    document.add(para[9]);
			    
			    para[10] = new Paragraph(new Chunk("2.4              "+new Chunk( "Classroom Management",font11bold),font9bold));
				para[10].setSpacingAfter(4);
				para[10].setIndentationRight(75);
				para[10].setIndentationLeft(75);
			    document.add(para[10]);
			    
			    para[11] = new Paragraph(new Chunk("2.4a            Understand important aspects of classroom management: Following a classroom management plan, reacting to and",font9));
				para[11].setSpacingAfter(4);
				para[11].setIndentationRight(75);
				para[11].setIndentationLeft(75);
			    document.add(para[11]);
			    
			    
			    para[31] = new Paragraph(new Chunk("changing innapropriate student behavior, and positive reinforcement techniques.",font9));
				para[31].setSpacingAfter(4);
				para[31].setIndentationRight(75);
				para[31].setIndentationLeft(125);
			    document.add(para[31]);
			    
			    
			    para[12] = new Paragraph(new Chunk("2.5              "+new Chunk( "Managing Bullying and Harassment",font11bold),font9bold));
				para[12].setSpacingAfter(4);
				para[12].setIndentationRight(75);
				para[12].setIndentationLeft(75);
			    document.add(para[12]);
			    
			    para[13] = new Paragraph(new Chunk("2.5a            Understand bullying and harassment and how to identify and react to both.",font9));
				para[13].setSpacingAfter(4);
				para[13].setIndentationRight(75);
				para[13].setIndentationLeft(75);
			    document.add(para[13]);
			    
			    
			    para[14] = new Paragraph(new Chunk("2.6             "+new Chunk( "Dismissal Time ",font11bold),font9bold));
				para[14].setSpacingAfter(4);
				para[14].setIndentationRight(75);
				para[14].setIndentationLeft(75);
			    document.add(para[14]);
			    
			    para[15] = new Paragraph(new Chunk("2.6a            Ensure lesson plans are completed.",font9));
				para[15].setSpacingAfter(4);
				para[15].setIndentationRight(75);
				para[15].setIndentationLeft(75);
			    document.add(para[15]);
			    
			    para[16] = new Paragraph(new Chunk("2.6b            Prepare students for dismissal.",font9));
				para[16].setSpacingAfter(4);
				para[16].setIndentationRight(75);
				para[16].setIndentationLeft(75);
			    document.add(para[16]);
			    
			    para[17] = new Paragraph(new Chunk("2.6c            Know where students go at the end of the day.",font9));
				para[17].setSpacingAfter(4);
				para[17].setIndentationRight(75);
				para[17].setIndentationLeft(75);
			    document.add(para[17]);
			    
			    para[18] = new Paragraph(new Chunk("2.7              "+new Chunk( "Finishing the Day ",font9bold),font9bold));
				para[18].setSpacingAfter(4);
				para[18].setIndentationRight(75);
				para[18].setIndentationLeft(75);
			    document.add(para[18]);
			    
			    para[19] = new Paragraph(new Chunk("2.7a            Understand proper methods for completing the day: Leave detailed notes for the teacher, leave the classsroom neat and",font9));
				para[19].setSpacingAfter(4);
				para[19].setIndentationRight(75);
				para[19].setIndentationLeft(75);
			    document.add(para[19]);
			    
			    
			    para[32] = new Paragraph(new Chunk("organized.",font9));
				para[32].setSpacingAfter(4);
				para[32].setIndentationRight(75);
				para[32].setIndentationLeft(125);
			    document.add(para[32]);
			    
			    para[20] = new Paragraph(new Chunk("2.7b             Know how to check out from the school: Inform administration and/or staff, leave all materials in the classroom.",font9));
				para[20].setSpacingAfter(4);
				para[20].setIndentationRight(75);
				para[20].setIndentationLeft(75);
			    document.add(para[20]);
			    document.newPage();
				
				document.newPage();
				
			}catch (Exception e) {
				e.printStackTrace();
			}finally
			{

				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}

			}
		//	return false;
		}

		
			
		
		public static void generatePDFCertCompletion(HttpServletRequest httpRequest ,String reportPath , String fileName , String urlRootPath)
		{


			
			Font font20 = null;
			Font font16 = null;
			Font font16bold = null;
			Font font11bold = null;
			Font font7 = null;
			Font font9bold = null;
			Font font9 = null;
			BaseFont tahoma = null;
			String fontPath = httpRequest.getRealPath("/");
			
			try {

				tahoma = BaseFont.createFont(fontPath+"fonts/42933.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
				font20 = new Font(tahoma, 20,Font.NORMAL);
				font11bold = new Font(tahoma, 11,Font.BOLD);
				font16 = new Font(tahoma, 16 , Font.NORMAL);
				font16bold = new Font(tahoma, 16 , Font.BOLD);
				font7 = new Font(tahoma, 7);
				font9bold = new Font(tahoma, 9,Font.BOLD);
				font9 = new Font(tahoma, 9);
				
	
			} catch (DocumentException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			Document document=null;
			FileOutputStream fos = null;
			PdfWriter writer = null;
			
			try{

				File file = new File(reportPath+"/temp");
				if(!file.exists())
					file.mkdirs();
				else
				Utility.deleteAllFileFromDir(reportPath+"/temp");
				document=new Document(PageSize.LETTER.rotate(),30f,30f,30f,55f);

				document.addAuthor("TeacherMatch");
				document.addCreator("TeacherMatch Inc.");
				document.addSubject("CERTIFICATION REPORT");
				document.addCreationDate();
				document.addTitle("CERTIFICATION REPORT");

				fos = new FileOutputStream(reportPath+"/"+fileName);
				writer = PdfWriter.getInstance(document, fos);
				writer.setStrictImageSequence(true);
				writer.setBoxSize("art", new Rectangle(29, 36, 760, 572));
				document.open();
				Rectangle rect= new Rectangle(29, 36, 760, 572);
		        rect.enableBorderSide(1);
		        rect.enableBorderSide(2);
		        rect.enableBorderSide(4);
		        rect.enableBorderSide(8);
		        rect.setBorder(Rectangle.BOX);
		        rect.setBorderWidth(1);
		        rect.setBorderColor(Color.BLACK);
		        document.add(rect);

		        PdfPTable mainTable = new PdfPTable(1);
				mainTable.setWidthPercentage(100);
				
				/////////////////////////// Start 1st page /////////////////////////////////////////
				Paragraph [] para = null;
				PdfPCell [] cell = null;

				para = new Paragraph[9];
				cell = new PdfPCell[9];
				para[0] = new Paragraph(" ",font20);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[0].setBorder(0);
				Image logo = Image.getInstance (fontPath+"/images/certificateCompletionHeader.png");
				logo.scalePercent(91);
				//float[] widthParticularlogo = {.5f,.5f};
				PdfPTable logotable = new PdfPTable(1);
				logotable.setWidthPercentage(100);
				PdfPCell logocol1 = new PdfPCell(new Paragraph("     ",font7));
				logocol1.setBorder(0);
				PdfPCell logocol2 = new PdfPCell(logo);
				logocol2.setBorder(0);
				
				logotable.addCell(logocol1);
				logotable.addCell(logocol2);
				cell[1]= new PdfPCell(logotable);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);

				para[2] = new Paragraph("                Smart                             Professional Development Series                    Classroom",font16);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[2].setBorder(0);
				
				para[3] = new Paragraph("                 Start                                      for substitute Teachers                                  Smarts",font16);
				cell[3]= new PdfPCell(para[3]);
				cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[3].setBorder(0);
				
				
				para[4] = new Paragraph("This is to certify that",font16);
				cell[4]= new PdfPCell(para[4]);
				cell[4].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[4].setBorder(0);
				
				para[5] = new Paragraph("   Sandeep ",font16bold);
				cell[5]= new PdfPCell(para[5]);
				cell[5].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[5].setBorder(0);
				
				para[6] = new Paragraph("Has Completed the Smart Practices Substitute Teacher",font16);
				cell[6]= new PdfPCell(para[6]);
				cell[6].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[6].setBorder(0);
				
				para[7] = new Paragraph("  Introductory Professional Development Program",font16);
				cell[7]= new PdfPCell(para[7]);
				cell[7].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[7].setBorder(0);
                Image logoSignature = Image.getInstance (fontPath+"/images/signCertCompletion.png");
				
                logoSignature.scalePercent(45);
                float[] widthParticularlogo = {.04f,.2f};
				PdfPTable logotableSig = new PdfPTable(widthParticularlogo);
				logotableSig.setWidthPercentage(100);
				PdfPCell logocolSig1 = new PdfPCell(new Paragraph("    ",font7));
				logocolSig1.setBorder(0);
				PdfPCell logocolSig2 = new PdfPCell(logoSignature);
				logocolSig2.setBorder(0);
				
				logotableSig.addCell(logocolSig1);
				logotableSig.addCell(logocolSig2);

				cell[8]= new PdfPCell(logotableSig);
				cell[8].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[8].setBorder(0);
				
				mainTable.addCell(cell[1]);
				mainTable.addCell(cell[2]);
				mainTable.addCell(cell[3]);

				mainTable.addCell(cell[0]);
				
				mainTable.addCell(cell[4]);
				mainTable.addCell(cell[0]);
				mainTable.addCell(cell[5]);
				mainTable.addCell(cell[0]);
				mainTable.addCell(cell[6]);
				
				mainTable.addCell(cell[7]);
				mainTable.addCell(cell[0]);
				mainTable.addCell(cell[8]);
				document.add(mainTable);
				document.newPage();
				
				
    			//////////Second Page ///////////////
				
			    rect= new Rectangle(25, 31, 765, 585);
		        rect.enableBorderSide(1);
		        rect.enableBorderSide(2);
		        rect.enableBorderSide(4);
		        rect.enableBorderSide(8);
		        rect.setBorder(Rectangle.BOX);
		        rect.setBorderWidth(1);
		        rect.setBorderColor(Color.BLACK);
		        document.add(rect);
				
		
		        para = new Paragraph[35];
				
		        Image logoSkillStart = Image.getInstance (fontPath+"/images/smartStartHeader.png");
		        logoSkillStart.scaleAbsolute(731f, 105f);
		        document.add(logoSkillStart);
		        para[2] = new Paragraph(new Chunk("Competency and Objective",font9bold));
				para[2].setSpacingAfter(1);
				para[2].setIndentationLeft(70);
			    document.add(para[2]);
			    
			    para[3] = new Paragraph(new Chunk("1.1              "+new Chunk( "Important Qualities of a Substitute Professional ",font9bold),font9bold));
			//	para[3].setSpacingAfter(1);
				para[3].setIndentationLeft(75);
				para[3].setIndentationRight(75);
			    document.add(para[3]);
			    
			    para[4] = new Paragraph(new Chunk("1.1a            Understand how effective educators engage students.",font9));
			//	para[4].setSpacingAfter(1);
				para[4].setIndentationLeft(75);
				para[4].setIndentationRight(75);
			    document.add(para[4]);
			    
			    para[5] = new Paragraph(new Chunk("1.1b            Understand how reliability, flexibility, and self-control are important qualities for a substitute professional.",font9));
				//para[5].setSpacingAfter(1);
				para[5].setIndentationLeft(75);
				para[5].setIndentationRight(75);
			    document.add(para[5]);
			    
			    para[6] = new Paragraph(new Chunk("1.2             "+new Chunk( " Ethics of Teaching ",font9),font9bold));
				//para[6].setSpacingAfter(1);
				para[6].setIndentationLeft(75);
				para[6].setIndentationRight(75);
			    document.add(para[6]);
			    
			    para[7] = new Paragraph(new Chunk("1.2a            Understand how ethics provide a guidance for behavior and beliefs. ",font9));
			//	para[7].setSpacingAfter(1);
				para[7].setIndentationLeft(75);
				para[7].setIndentationRight(75);
			    document.add(para[7]);
			    
			    para[8] = new Paragraph(new Chunk("1.2b            Understand the seven points of ethical behavior for professional educators.",font9));
			//	para[8].setSpacingAfter(1);
				para[8].setIndentationLeft(75);
				para[8].setIndentationRight(75);
			    document.add(para[8]);
			    
			    para[9] = new Paragraph(new Chunk("1.2c            Understand how ethics balance the protection of students - physically and emotionally - with demands of the job, while",font9));
				//para[9].setSpacingAfter(1);
				para[9].setIndentationLeft(75);
				para[9].setIndentationRight(75);
			    document.add(para[9]);
			    
			    para[31] = new Paragraph(new Chunk("shepherding student development.",font9));
				//para[9].setSpacingAfter(1);
				para[31].setIndentationLeft(125);
				para[31].setIndentationRight(75);
			    document.add(para[31]);
			    
			    para[10] = new Paragraph(new Chunk("1.3             "+new Chunk( " Legal Obligations",font9bold),font9bold));
			//	para[10].setSpacingAfter(1);
				para[10].setIndentationLeft(75);
				para[10].setIndentationRight(75);
			    document.add(para[10]);
			    
			    para[11] = new Paragraph(new Chunk("1.3a            Understand the importance of CAPTA, HIPAA, and FERPA.",font9));
				//para[11].setSpacingAfter(1);
				para[11].setIndentationLeft(75);
				para[11].setIndentationRight(75);
			    document.add(para[11]);
			    
			    para[12] = new Paragraph(new Chunk("1.4             "+new Chunk( "Social Behavior Guidelines",font9bold),font9bold));
			//	para[12].setSpacingAfter(1);
				para[12].setIndentationLeft(75);
				para[12].setIndentationRight(75);
			    document.add(para[12]);
			    
			    para[13] = new Paragraph(new Chunk("1.4a            Learn the social behavior guidelines for working in schools: Social interactions with students, no-touch policy, minimizing",font9));
				//para[13].setSpacingAfter(1);
				para[13].setIndentationLeft(75);
				para[13].setIndentationRight(75);
			    document.add(para[13]);
			    
			    para[32] = new Paragraph(new Chunk("information sharing, and social media guidelines.",font9));
				//para[13].setSpacingAfter(1);
				para[32].setIndentationLeft(125);
				para[32].setIndentationRight(75);
			    document.add(para[32]);
			    
			    
			    
			    para[14] = new Paragraph(new Chunk("1.5             "+new Chunk( " Learning About Technology",font9bold),font9bold));
			//	para[14].setSpacingAfter(1);
				para[14].setIndentationLeft(75);
				para[14].setIndentationRight(75);
			    document.add(para[14]);
			    
			    para[15] = new Paragraph(new Chunk("2.6a            Ensure lesson plans are completed.",font9));
			//	para[15].setSpacingAfter(1);
				para[15].setIndentationLeft(75);
				para[15].setIndentationRight(75);
			    document.add(para[15]);
			    
			    para[16] = new Paragraph(new Chunk("1.5a            Understand why it's important to become familiar with technology frequently used in schools.",font9));
			//	para[16].setSpacingAfter(1);
				para[16].setIndentationLeft(75);
				para[16].setIndentationRight(75);
			    document.add(para[16]);
			    
			    para[17] = new Paragraph(new Chunk("1.5b            Understand appropriate and inappropriate uses for school technology in the classroom.",font9));
			//	para[17].setSpacingAfter(1);
				para[17].setIndentationLeft(75);
				para[17].setIndentationRight(75);
			    document.add(para[17]);
			    
			    para[18] = new Paragraph(new Chunk("1.6             "+new Chunk( " Your First Placement: The Night Before ",font9bold),font9bold));
			//	para[18].setSpacingAfter(1);
				para[18].setIndentationLeft(75);
				para[18].setIndentationRight(75);
			    document.add(para[18]);
			    
			    para[19] = new Paragraph(new Chunk("1.6a            Understand the importance of checking the school website for important information.",font9));
			//	para[19].setSpacingAfter(1);
				para[19].setIndentationLeft(75);
				para[19].setIndentationRight(75);
			    document.add(para[19]);
			    
			    para[20] = new Paragraph(new Chunk("1.6b            Understand appropriate attire for a professional educator.",font9));
				//para[20].setSpacingAfter(1);
				para[20].setIndentationLeft(75);
				para[20].setIndentationRight(75);
			    document.add(para[20]);
			    
			    
			    para[21] = new Paragraph(new Chunk("1.7             "+new Chunk( "  The Day Has Arrived ",font9bold),font9bold));
			//	para[21].setSpacingAfter(1);
				para[21].setIndentationLeft(75);
				para[21].setIndentationRight(75);
			    document.add(para[21]);
			    
			    
			    para[22] = new Paragraph(new Chunk("1.7a            Understand the importance of checking-in with school administrators and staff.",font9));
			//	para[22].setSpacingAfter(1);
				para[22].setIndentationLeft(75);
				para[22].setIndentationRight(75);
			    document.add(para[22]);
			    
			    para[23] = new Paragraph(new Chunk("1.7b            Understand basic classroom rules: Food and drink, transportation, and medical guidelines.",font9));
			//	para[23].setSpacingAfter(1);
				para[23].setIndentationLeft(75);
				para[23].setIndentationRight(75);
			    document.add(para[23]);
			    
			    para[24] = new Paragraph(new Chunk("1.8             "+new Chunk( " You Are in Your Classroom ",font9bold),font9bold));
			//	para[24].setSpacingAfter(1);
				para[24].setIndentationLeft(75);
				para[24].setIndentationRight(75);
			    document.add(para[24]);
			    
			    para[25] = new Paragraph(new Chunk("1.8a            Understand basic student orientation procedures: Checking lesson plans, photocopying, bell schedule, dismissal",font9));
			//	para[25].setSpacingAfter(1);
				para[25].setIndentationLeft(75);
				para[25].setIndentationRight(75);
			    document.add(para[25]);
			    
			    
			    para[33] = new Paragraph(new Chunk("procedures, room arrangement, classroom technology, and classroom neighbors.",font9));
				
				para[33].setIndentationLeft(125);
				para[33].setIndentationRight(75);
			    document.add(para[33]);
		    
			    para[26] = new Paragraph(new Chunk("1.9             "+new Chunk( "  In Case of Emergency ",font9bold),font9bold));
			//	para[26].setSpacingAfter(1);
				para[26].setIndentationLeft(75);
				para[26].setIndentationRight(75);
			    document.add(para[26]);
			    
			    para[27] = new Paragraph(new Chunk("1.9a            Understand how to react to disaster drills: Fire, natural disaster, and lockdown.",font9));
			//	para[27].setSpacingAfter(1);
				para[27].setIndentationLeft(75);
				para[27].setIndentationRight(75);
			    document.add(para[27]);
			    
			    para[28] = new Paragraph(new Chunk("1.9b            Understand how to react to emergencies involving students: Treats, injury, weapons, drugs.",font9));
			//	para[28].setSpacingAfter(1);
				para[28].setIndentationLeft(75);
				para[28].setIndentationRight(75);
			    document.add(para[28]);
			    document.newPage();
				
				
			    //////////THIRD PAGE
			    
			    rect= new Rectangle(25, 35, 765, 578);
		        rect.enableBorderSide(1);
		        rect.enableBorderSide(2);
		        rect.enableBorderSide(4);
		        rect.enableBorderSide(8);
		        rect.setBorder(Rectangle.BOX);
		        rect.setBorderWidth(1);
		        rect.setBorderColor(Color.BLACK);
		        document.add(rect);
			    
			    
                para = new Paragraph[35];
				
		        Image logClassRoom = Image.getInstance (fontPath+"/images/classroomStartHeader.png");
		        logClassRoom.scaleAbsolute(731f, 105f);
		        document.add(logClassRoom);
		       
		        para[2] = new Paragraph(new Chunk("Competency and Objective",font9bold));
				para[2].setSpacingAfter(1);
				para[2].setIndentationLeft(70);
			    document.add(para[2]);
			    
			    para[3] = new Paragraph(new Chunk("2.1              "+new Chunk( "You Are the Teacher Today",font9bold),font9bold));
				para[3].setSpacingAfter(4);
				para[3].setIndentationLeft(75);
				para[3].setIndentationRight(75);
			    document.add(para[3]);
			    
			    para[4] = new Paragraph(new Chunk("2.1a            Understand how to provide students with a normal academic day.",font9));
				para[4].setSpacingAfter(4);
				para[4].setIndentationLeft(75);
				para[4].setIndentationRight(75);
			    document.add(para[4]);
			    
			    para[5] = new Paragraph(new Chunk("2.2              "+new Chunk( "The First Five Minutes",font9bold),font9bold));
				para[5].setSpacingAfter(4);
				para[5].setIndentationLeft(75);
				para[5].setIndentationRight(75);
			    document.add(para[5]);
			    
			    para[6] = new Paragraph(new Chunk("2.2a            Understand how to set the tone for the day: Greeting students and providing instructions.",font9));
				para[6].setSpacingAfter(4);
				para[6].setIndentationRight(75);
				para[6].setIndentationLeft(75);
			    document.add(para[6]);
			    
			    para[7] = new Paragraph(new Chunk("2.3              "+new Chunk( "Attendance and the First Assignment ",font11bold),font9bold));
				para[7].setSpacingAfter(4);
				para[7].setIndentationRight(75);
				para[7].setIndentationLeft(75);
			    document.add(para[7]);
			    
			    para[8] = new Paragraph(new Chunk("2.3a            Understand the importantance of taking attendance immediately.",font9));
				para[8].setSpacingAfter(4);
				para[8].setIndentationRight(75);
				para[8].setIndentationLeft(75);
			    document.add(para[8]);
			    
			    para[9] = new Paragraph(new Chunk("2.3b            Understand how to engage students with an opening assignment.",font9));
				para[9].setSpacingAfter(4);
				para[9].setIndentationRight(75);
				para[9].setIndentationLeft(75);
			    document.add(para[9]);
			    
			    para[10] = new Paragraph(new Chunk("2.4              "+new Chunk( "Classroom Management",font11bold),font9bold));
				para[10].setSpacingAfter(4);
				para[10].setIndentationRight(75);
				para[10].setIndentationLeft(75);
			    document.add(para[10]);
			    
			    para[11] = new Paragraph(new Chunk("2.4a            Understand important aspects of classroom management: Following a classroom management plan, reacting to and",font9));
				para[11].setSpacingAfter(4);
				para[11].setIndentationRight(75);
				para[11].setIndentationLeft(75);
			    document.add(para[11]);
			    
			    
			    para[31] = new Paragraph(new Chunk("changing innapropriate student behavior, and positive reinforcement techniques.",font9));
				para[31].setSpacingAfter(4);
				para[31].setIndentationRight(75);
				para[31].setIndentationLeft(125);
			    document.add(para[31]);
			    
			    
			    para[12] = new Paragraph(new Chunk("2.5              "+new Chunk( "Managing Bullying and Harassment",font11bold),font9bold));
				para[12].setSpacingAfter(4);
				para[12].setIndentationRight(75);
				para[12].setIndentationLeft(75);
			    document.add(para[12]);
			    
			    para[13] = new Paragraph(new Chunk("2.5a            Understand bullying and harassment and how to identify and react to both.",font9));
				para[13].setSpacingAfter(4);
				para[13].setIndentationRight(75);
				para[13].setIndentationLeft(75);
			    document.add(para[13]);
			    
			    
			    para[14] = new Paragraph(new Chunk("2.6             "+new Chunk( "Dismissal Time ",font11bold),font9bold));
				para[14].setSpacingAfter(4);
				para[14].setIndentationRight(75);
				para[14].setIndentationLeft(75);
			    document.add(para[14]);
			    
			    para[15] = new Paragraph(new Chunk("2.6a            Ensure lesson plans are completed.",font9));
				para[15].setSpacingAfter(4);
				para[15].setIndentationRight(75);
				para[15].setIndentationLeft(75);
			    document.add(para[15]);
			    
			    para[16] = new Paragraph(new Chunk("2.6b            Prepare students for dismissal.",font9));
				para[16].setSpacingAfter(4);
				para[16].setIndentationRight(75);
				para[16].setIndentationLeft(75);
			    document.add(para[16]);
			    
			    para[17] = new Paragraph(new Chunk("2.6c            Know where students go at the end of the day.",font9));
				para[17].setSpacingAfter(4);
				para[17].setIndentationRight(75);
				para[17].setIndentationLeft(75);
			    document.add(para[17]);
			    
			    para[18] = new Paragraph(new Chunk("2.7              "+new Chunk( "Finishing the Day ",font9bold),font9bold));
				para[18].setSpacingAfter(4);
				para[18].setIndentationRight(75);
				para[18].setIndentationLeft(75);
			    document.add(para[18]);
			    
			    para[19] = new Paragraph(new Chunk("2.7a            Understand proper methods for completing the day: Leave detailed notes for the teacher, leave the classsroom neat and",font9));
				para[19].setSpacingAfter(4);
				para[19].setIndentationRight(75);
				para[19].setIndentationLeft(75);
			    document.add(para[19]);
			    
			    
			    para[32] = new Paragraph(new Chunk("organized.",font9));
				para[32].setSpacingAfter(4);
				para[32].setIndentationRight(75);
				para[32].setIndentationLeft(125);
			    document.add(para[32]);
			    
			    para[20] = new Paragraph(new Chunk("2.7b             Know how to check out from the school: Inform administration and/or staff, leave all materials in the classroom.",font9));
				para[20].setSpacingAfter(4);
				para[20].setIndentationRight(75);
				para[20].setIndentationLeft(75);
			    document.add(para[20]);
			    document.newPage();
				
			
				document.newPage();
				
				
			}catch (Exception e) {
				e.printStackTrace();
			}finally
			{

				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}

			}
		//	return false;
		}
		
		//shadab
		public String generateSkillPDF(int teacher, Integer groupName)
		{

			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			try
			{
				
				String path = request.getContextPath();
				String urlRootPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
				TeacherDetail teacherDetail = null;
				
				teacherDetail = teacherDetailDAO.findById(teacher, false, false);
				int teacherId = teacherDetail.getTeacherId();

				String time = String.valueOf(System.currentTimeMillis()).substring(6);
				String basePath = context.getServletContext().getRealPath("/")+"/candidatereports/"+teacherId;
				
				String fileName =time+"PDReport.pdf";

				File file = new File(basePath);
				if(!file.exists())
					file.mkdirs();

				Utility.deleteAllFileFromDir(basePath);
				//candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);
				if(groupName!=null && groupName==3){
					generatePDFSkillCertificateUnit3( request , basePath , fileName , urlRootPath,teacherDetail);
					System.out.println("Group Name : Unit 3");
				}
				else if(groupName!=null && groupName==4){
					 
					generatePDFSkillCertificateUnit4( request , basePath , fileName , urlRootPath,teacherDetail);
					System.out.println("Group Name : Unit 4");
				}
				else{
					generatePDFSkillCertificate( request , basePath , fileName , urlRootPath,teacherDetail);
					System.out.println("Group Name : Unit 1 & 2");
				}
				return urlRootPath+"candidatereports/"+teacherId+"/"+fileName;
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			return "Not available";
		
		}
		
		public  void generatePDFSkillCertificate(HttpServletRequest httpRequest ,String reportPath , String fileName , String urlRootPath,TeacherDetail teacherDetail)
		{


			
			Font font20 = null;
			Font font16 = null;
			Font font16bold = null;
			Font font11bold = null;
			Font font7 = null;
			Font font9bold = null;
			Font font9 = null;
			BaseFont tahoma = null;
			String fontPath = httpRequest.getRealPath("/");
			
			try {

				tahoma = BaseFont.createFont(fontPath+"fonts/42933.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
				font20 = new Font(tahoma, 20,Font.NORMAL);
				font11bold = new Font(tahoma, 11,Font.BOLD);
				font16 = new Font(tahoma, 16 , Font.NORMAL);
				font16bold = new Font(tahoma, 16 , Font.BOLD);
				font7 = new Font(tahoma, 7);
				font9bold = new Font(tahoma, 9,Font.BOLD);
				font9 = new Font(tahoma, 9 );
				
			} catch (DocumentException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			Document document=null;
			FileOutputStream fos = null;
			PdfWriter writer = null;
			String passDate = "";
			List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
			try{
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacher(teacherDetail);
				if(teacherAssessmentStatus!=null && teacherAssessmentStatus.size()>0){
					for(TeacherAssessmentStatus teachAssessmentStatus : teacherAssessmentStatus){
						if(teachAssessmentStatus.getAssessmentType()==3 && teachAssessmentStatus.getPass()!=null && teachAssessmentStatus.getPass().equalsIgnoreCase("p")){
							passDate = "as of "+Utility.convertToCompleteMonthDate(teachAssessmentStatus.getAssessmentCompletedDateTime());
							break;
						}
					}
				}
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
			
			try{

				File file = new File(reportPath+"/temp");
				if(!file.exists())
					file.mkdirs();
				else
				Utility.deleteAllFileFromDir(reportPath+"/temp");
				document=new Document(PageSize.LETTER.rotate(),30f,30f,30f,55f);

				document.addAuthor("TeacherMatch");
				document.addCreator("TeacherMatch Inc.");
				document.addSubject("CRETIFICATION REPORT");
				document.addCreationDate();
				document.addTitle("CRETIFICATION REPORT");

				fos = new FileOutputStream(reportPath+"/"+fileName);
				writer = PdfWriter.getInstance(document, fos);
				writer.setStrictImageSequence(true);
				
				writer.setBoxSize("art", new Rectangle(29, 36, 760, 572));
				document.open();
				
				Rectangle rect= new Rectangle(29, 36, 760, 572);
		        rect.enableBorderSide(1);
		        rect.enableBorderSide(2);
		        rect.enableBorderSide(4);
		        rect.enableBorderSide(8);
		        rect.setBorder(Rectangle.BOX);
		        rect.setBorderWidth(1);
		        rect.setBorderColor(Color.BLACK);
		        document.add(rect);
				
				PdfPTable mainTable = new PdfPTable(1);
				mainTable.setWidthPercentage(100);
				
				/////////////////////////// Start 1st page /////////////////////////////////////////
				Paragraph [] para = null;
				PdfPCell [] cell = null;

				para = new Paragraph[9];
				cell = new PdfPCell[9];
				para[0] = new Paragraph(" ",font20);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[0].setBorder(0);

				Image logo = Image.getInstance (fontPath+"/images/skillCertificateHeader.png");
				logo.scalePercent((float) 76.2);
				//float[] widthParticularlogo = {.5f,.5f};
				PdfPTable logotable = new PdfPTable(1);
				logotable.setWidthPercentage(100);
				PdfPCell logocol1 = new PdfPCell(new Paragraph("     ",font7));
				logocol1.setBorder(0);
				PdfPCell logocol2 = new PdfPCell(logo);
				logocol2.setBorder(0);
				
				logotable.addCell(logocol1);
				logotable.addCell(logocol2);
				cell[1]= new PdfPCell(logotable);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);

				para[2] = new Paragraph("                Smart                             Professional Development Series                    Classroom",font16);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[2].setBorder(0);
				
				para[3] = new Paragraph("                 Start                                      for substitute teachers                                  Smarts",font16);
				cell[3]= new PdfPCell(para[3]);
				cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[3].setBorder(0);
				
				
				para[4] = new Paragraph("This is to certify that",font16);
				cell[4]= new PdfPCell(para[4]);
				cell[4].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[4].setBorder(0);
				
				para[5] = new Paragraph(" "+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"" ,font16bold);
				cell[5]= new PdfPCell(para[5]);
				cell[5].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[5].setBorder(0);
				
				para[6] = new Paragraph("has Completed the Smart Practices Substitute Educator Introductory Professional Development Program "+passDate+".",font16);
				cell[6]= new PdfPCell(para[6]);
				cell[6].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[6].setBorder(0);
				
//	            Image logoSignature = Image.getInstance (fontPath+"/images/signatureCertificate.png");
//	            logoSignature.scalePercent(40);
//                //float[] widthParticularlogo = {.1f,.2f};
//				PdfPTable logotableSig = new PdfPTable(1);
//				logotableSig.setWidthPercentage(100);
//				PdfPCell logocolSig1 = new PdfPCell(new Paragraph("     ",font7));
//				logocolSig1.setBorder(0);
//				PdfPCell logocolSig2 = new PdfPCell(logoSignature);
//				logocolSig2.setBorder(0);
//				
//				logotableSig.addCell(logocolSig1);
//				logotableSig.addCell(logocolSig2);
//				////////////////////////////////////////////////////////////////
//				cell[7]= new PdfPCell(logotableSig);
//				cell[7].setHorizontalAlignment(Element.ALIGN_CENTER);
//				cell[7].setBorder(0);
				
				Image logoSignature = Image.getInstance (fontPath+"/images/signatureCertificate.png");
				logoSignature.scalePercent((float) 76.2);
				//float[] widthParticularlogo = {.5f,.5f};
				PdfPTable logotableSig = new PdfPTable(1);
				logotableSig.setWidthPercentage(100);
				PdfPCell logocolSig1 = new PdfPCell(new Paragraph("     ",font7));
				logocolSig1.setBorder(0);
				PdfPCell logocolSig2 = new PdfPCell(logoSignature);
				logocolSig2.setBorder(0);
				
				logotableSig.addCell(logocolSig1);
				logotableSig.addCell(logocolSig2);
				cell[7]= new PdfPCell(logotableSig);
				cell[7].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[7].setBorder(0);
				
				mainTable.addCell(cell[1]);
				//mainTable.addCell(cell[2]);
				//mainTable.addCell(cell[3]);
				for(int i=0;i<1;i++)
					mainTable.addCell(cell[0]);
				
				mainTable.addCell(cell[4]);
				//mainTable.addCell(cell[0]);
				mainTable.addCell(cell[5]);
				//mainTable.addCell(cell[0]);
				mainTable.addCell(cell[6]);
				//mainTable.addCell(cell[0]);
				mainTable.addCell(cell[7]);
				document.add(mainTable);
				document.newPage();
				
				
		        //////////Second Page ///////////////
				
			    rect= new Rectangle(25, 31, 765, 585);
		        rect.enableBorderSide(1);
		        rect.enableBorderSide(2);
		        rect.enableBorderSide(4);
		        rect.enableBorderSide(8);
		        rect.setBorder(Rectangle.BOX);
		        rect.setBorderWidth(1);
		        rect.setBorderColor(Color.BLACK);
		        document.add(rect);
				
		
		        para = new Paragraph[45];
				
		        Image logoSkillStart = Image.getInstance (fontPath+"/images/smartStartHeader.png");
		        logoSkillStart.scaleAbsolute(731f, 105f);
		        document.add(logoSkillStart);
		        para[2] = new Paragraph(new Chunk("Competency and Objective",font9bold));
				para[2].setSpacingAfter(1);
				para[2].setIndentationLeft(70);
			    document.add(para[2]);
			    
			    
			    para[3] = new Paragraph(new Chunk("1.1              "+new Chunk( "Important Qualities of a Substitute Professional ",font9bold),font9bold));
			//	para[3].setSpacingAfter(1);
				para[3].setIndentationLeft(75);
				para[3].setIndentationRight(75);
			    document.add(para[3]);
			    
			    para[4] = new Paragraph(new Chunk("1.1a            Understand how effective educators engage students.",font9));
			//	para[4].setSpacingAfter(1);
				para[4].setIndentationLeft(75);
				para[4].setIndentationRight(75);
			    document.add(para[4]);
			    
			    para[5] = new Paragraph(new Chunk("1.1b            Understand how reliability, flexibility, and self-control are important qualities for a substitute professional.",font9));
				//para[5].setSpacingAfter(1);
				para[5].setIndentationLeft(75);
				para[5].setIndentationRight(75);
			    document.add(para[5]);
		
			    para[6] = new Paragraph(new Chunk("1.2             "+new Chunk( " Ethics of Teaching ",font9),font9bold));
				//para[6].setSpacingAfter(1);
				para[6].setIndentationLeft(75);
				
				para[6].setIndentationRight(75);
			    document.add(para[6]);
			    
			    para[7] = new Paragraph(new Chunk("1.2a            Understand how ethics provide a guidance for behavior and beliefs. ",font9));
			//	para[7].setSpacingAfter(1);
				para[7].setIndentationLeft(75);
				para[7].setIndentationRight(75);
			    document.add(para[7]);
			    
			    para[8] = new Paragraph(new Chunk("1.2b            Understand the seven points of ethical behavior for professional educators.",font9));
			//	para[8].setSpacingAfter(1);
				para[8].setIndentationLeft(75);
				para[8].setIndentationRight(75);
			    document.add(para[8]);
			    
			    para[9] = new Paragraph(new Chunk("1.2c            Understand how ethics balance the protection of students - physically and emotionally - with demands of the job, while",font9));
				//para[9].setSpacingAfter(1);
				para[9].setIndentationLeft(75);
				para[9].setIndentationRight(75);
			    document.add(para[9]);
			    
			    para[31] = new Paragraph(new Chunk("shepherding student development.",font9));
				para[31].setIndentationLeft(125);
				para[31].setIndentationRight(75);
				
			    document.add(para[31]);
			    
			    
			    Chunk sigUnderline = new Chunk("   ");
			    sigUnderline.setUnderline(0.05f, 0.02f);
			    sigUnderline.setHorizontalScaling(75f);
			    para[37] = new Paragraph(new Chunk(" ",font9));
			    para[37].add(sigUnderline);
			    para[37].setFirstLineIndent(1f);
			    para[37].setIndentationLeft(75);
			    document.add(para[37]);
			    
			    
			    
			    para[10] = new Paragraph(new Chunk("1.3             "+new Chunk( " Legal Obligations",font9bold),font9bold));
			//	para[10].setSpacingAfter(1);
				para[10].setIndentationLeft(75);
				para[10].setIndentationRight(75);
			    document.add(para[10]);
			    
			    para[11] = new Paragraph(new Chunk("1.3a            Understand the importance of CAPTA, HIPAA, and FERPA.",font9));
				//para[11].setSpacingAfter(1);
				para[11].setIndentationLeft(75);
				para[11].setIndentationRight(75);
			    document.add(para[11]);
			    
			    
			    /*
			    sigUnderline.setUnderline(0.05f, 0.02f);
			    sigUnderline.setHorizontalScaling(37f);
			    para[38] = new Paragraph(new Chunk(" ",font9));
			    para[38].add(sigUnderline);
			    para[38].setIndentationLeft(75);
			    document.add(para[38]);
			    */
			    
			    para[12] = new Paragraph(new Chunk("1.4              "+new Chunk( "Social Behavior Guidelines",font9bold),font9bold));
			//	para[12].setSpacingAfter(1);
				para[12].setIndentationLeft(75);
				para[12].setIndentationRight(75);
			    document.add(para[12]);
			    
			    para[13] = new Paragraph(new Chunk("1.4a            Learn the social behavior guidelines for working in schools: Social interactions with students, no-touch policy, minimizing",font9));
				//para[13].setSpacingAfter(1);
				para[13].setIndentationLeft(75);
				para[13].setIndentationRight(75);
			    document.add(para[13]);
			    
			    para[32] = new Paragraph(new Chunk("information sharing, and social media guidelines.",font9));
				//para[13].setSpacingAfter(1);
				para[32].setIndentationLeft(125);
				para[32].setIndentationRight(75);
			    document.add(para[32]);
			    
			/*    sigUnderline.setUnderline(0.05f, 0.02f);
			    sigUnderline.setHorizontalScaling(37f);
			    para[39] = new Paragraph(new Chunk(" ",font9));
			    para[39].add(sigUnderline);
			    para[39].setIndentationLeft(75);
			    document.add(para[39]);
			  */  
			    
			    para[14] = new Paragraph(new Chunk("1.5             "+new Chunk( " Learning About Technology",font9bold),font9bold));
			//	para[14].setSpacingAfter(1);
				para[14].setIndentationLeft(75);
				para[14].setIndentationRight(75);
			    document.add(para[14]);
			    
			   
			    
			    para[16] = new Paragraph(new Chunk("1.5a            Understand why it's important to become familiar with technology frequently used in schools.",font9));
			//	para[16].setSpacingAfter(1);
				para[16].setIndentationLeft(75);
				para[16].setIndentationRight(75);
			    document.add(para[16]);
			    
			    para[17] = new Paragraph(new Chunk("1.5b            Understand appropriate and inappropriate uses for school technology in the classroom.",font9));
			//	para[17].setSpacingAfter(1);
				para[17].setIndentationLeft(75);
				para[17].setIndentationRight(75);
			    document.add(para[17]);
			    
			    
			/*    sigUnderline.setUnderline(0.05f, 0.02f);
			    sigUnderline.setHorizontalScaling(37f);
			    para[39] = new Paragraph(new Chunk(" ",font9));
			    para[39].add(sigUnderline);
			    para[39].setIndentationLeft(75);
			    document.add(para[39]);
			 */   
			    
			    para[18] = new Paragraph(new Chunk("1.6             "+new Chunk( " Your First Placement: The Night Before ",font9bold),font9bold));
			//	para[18].setSpacingAfter(1);
				para[18].setIndentationLeft(75);
				para[18].setIndentationRight(75);
			    document.add(para[18]);
			    
			    para[19] = new Paragraph(new Chunk("1.6a            Understand the importance of checking the school website for important information.",font9));
			//	para[19].setSpacingAfter(1);
				para[19].setIndentationLeft(75);
				para[19].setIndentationRight(75);
			    document.add(para[19]);
			    
			    para[20] = new Paragraph(new Chunk("1.6b            Understand appropriate attire for a professional educator.",font9));
				//para[20].setSpacingAfter(1);
				para[20].setIndentationLeft(75);
				para[20].setIndentationRight(75);
			    document.add(para[20]);
			    
			    
			/*    sigUnderline.setUnderline(0.05f, 0.02f);
			    sigUnderline.setHorizontalScaling(37f);
			    para[40] = new Paragraph(new Chunk(" ",font9));
			    para[40].add(sigUnderline);
			    para[40].setIndentationLeft(75);
			    document.add(para[40]);
			*/    
			    
			    para[21] = new Paragraph(new Chunk("1.7             "+new Chunk( "  The Day Has Arrived ",font9bold),font9bold));
			//	para[21].setSpacingAfter(1);
				para[21].setIndentationLeft(75);
				para[21].setIndentationRight(75);
			    document.add(para[21]);
			    
			    
			    para[22] = new Paragraph(new Chunk("1.7a            Understand the importance of checking-in with school administrators and staff.",font9));
			//	para[22].setSpacingAfter(1);
				para[22].setIndentationLeft(75);
				para[22].setIndentationRight(75);
			    document.add(para[22]);
			    
			    para[23] = new Paragraph(new Chunk("1.7b            Understand basic classroom rules: Food and drink, transportation, and medical guidelines.",font9));
			//	para[23].setSpacingAfter(1);
				para[23].setIndentationLeft(75);
				para[23].setIndentationRight(75);
			    document.add(para[23]);
			    
			/*    sigUnderline.setUnderline(0.05f, 0.02f);
			    sigUnderline.setHorizontalScaling(37f);
			    para[41] = new Paragraph(new Chunk(" ",font9));
			    para[41].add(sigUnderline);
			    para[41].setIndentationLeft(75);
			    document.add(para[41]);
			*/    
			    para[24] = new Paragraph(new Chunk("1.8             "+new Chunk( " You Are in Your Classroom ",font9bold),font9bold));
			//	para[24].setSpacingAfter(1);
				para[24].setIndentationLeft(75);
				para[24].setIndentationRight(75);
			    document.add(para[24]);
			    
			    para[25] = new Paragraph(new Chunk("1.8a            Understand basic student orientation procedures: Checking lesson plans, photocopying, bell schedule, dismissal",font9));
			//	para[25].setSpacingAfter(1);
				para[25].setIndentationLeft(75);
				para[25].setIndentationRight(75);
			    document.add(para[25]);
			    
			    
			    para[33] = new Paragraph(new Chunk("procedures, room arrangement, classroom technology, and classroom neighbors.",font9));
				
				para[33].setIndentationLeft(125);
				para[33].setIndentationRight(75);
			    document.add(para[33]);
		    
			    
			/*    sigUnderline.setUnderline(0.05f, 0.02f);
			    sigUnderline.setHorizontalScaling(37f);
			    para[42] = new Paragraph(new Chunk(" ",font9));
			    para[42].add(sigUnderline);
			    para[42].setIndentationLeft(75);
			    document.add(para[42]);
			*/    
			    para[26] = new Paragraph(new Chunk("1.9             "+new Chunk( "  In Case of Emergency ",font9bold),font9bold));
			//	para[26].setSpacingAfter(1);
				para[26].setIndentationLeft(75);
				para[26].setIndentationRight(75);
			    document.add(para[26]);
			    
			    para[27] = new Paragraph(new Chunk("1.9a            Understand how to react to disaster drills: Fire, natural disaster, and lockdown.",font9));
			//	para[27].setSpacingAfter(1);
				para[27].setIndentationLeft(75);
				para[27].setIndentationRight(75);
			    document.add(para[27]);
			    
			    para[28] = new Paragraph(new Chunk("1.9b            Understand how to react to emergencies involving students: Treats, injury, weapons, drugs.",font9));
			//	para[28].setSpacingAfter(1);
				para[28].setIndentationLeft(75);
				para[28].setIndentationRight(75);
			    document.add(para[28]);
			    document.newPage();
				
				
			    //////////THIRD PAGE
			    
			    rect= new Rectangle(25, 35, 765, 578);
		        rect.enableBorderSide(1);
		        rect.enableBorderSide(2);
		        rect.enableBorderSide(4);
		        rect.enableBorderSide(8);
		        rect.setBorder(Rectangle.BOX);
		        rect.setBorderWidth(1);
		        rect.setBorderColor(Color.BLACK);
		        document.add(rect);
			    
			    
                para = new Paragraph[35];
				
		        Image logClassRoom = Image.getInstance (fontPath+"/images/classroomStartHeader.png");
		        logClassRoom.scaleAbsolute(731f, 105f);
		        document.add(logClassRoom);
		       
		        para[2] = new Paragraph(new Chunk("Competency and Objective",font9bold));
				para[2].setSpacingAfter(1);
				para[2].setIndentationLeft(70);
			    document.add(para[2]);
			    
			    para[3] = new Paragraph(new Chunk("2.1              "+new Chunk( "You Are the Teacher Today",font9bold),font9bold));
				para[3].setSpacingAfter(4);
				para[3].setIndentationLeft(75);
				para[3].setIndentationRight(75);
			    document.add(para[3]);
			    
			    para[4] = new Paragraph(new Chunk("2.1a            Understand how to provide students with a normal academic day.",font9));
				para[4].setSpacingAfter(4);
				para[4].setIndentationLeft(75);
				para[4].setIndentationRight(75);
			    document.add(para[4]);
			    
			    para[5] = new Paragraph(new Chunk("2.2              "+new Chunk( "The First Five Minutes",font9bold),font9bold));
				para[5].setSpacingAfter(4);
				para[5].setIndentationLeft(75);
				para[5].setIndentationRight(75);
			    document.add(para[5]);
			    
			    para[6] = new Paragraph(new Chunk("2.2a            Understand how to set the tone for the day: Greeting students and providing instructions.",font9));
				para[6].setSpacingAfter(4);
				para[6].setIndentationRight(75);
				para[6].setIndentationLeft(75);
			    document.add(para[6]);
			    
			    para[7] = new Paragraph(new Chunk("2.3              "+new Chunk( "Attendance and the First Assignment ",font11bold),font9bold));
				para[7].setSpacingAfter(4);
				para[7].setIndentationRight(75);
				para[7].setIndentationLeft(75);
			    document.add(para[7]);
			    
			    para[8] = new Paragraph(new Chunk("2.3a            Understand the importantance of taking attendance immediately.",font9));
				para[8].setSpacingAfter(4);
				para[8].setIndentationRight(75);
				para[8].setIndentationLeft(75);
			    document.add(para[8]);
			    
			    para[9] = new Paragraph(new Chunk("2.3b            Understand how to engage students with an opening assignment.",font9));
				para[9].setSpacingAfter(4);
				para[9].setIndentationRight(75);
				para[9].setIndentationLeft(75);
			    document.add(para[9]);
			    
			    para[10] = new Paragraph(new Chunk("2.4              "+new Chunk( "Classroom Management",font11bold),font9bold));
				para[10].setSpacingAfter(4);
				para[10].setIndentationRight(75);
				para[10].setIndentationLeft(75);
			    document.add(para[10]);
			    
			    para[11] = new Paragraph(new Chunk("2.4a            Understand important aspects of classroom management: Following a classroom management plan, reacting to and",font9));
				para[11].setSpacingAfter(4);
				para[11].setIndentationRight(75);
				para[11].setIndentationLeft(75);
			    document.add(para[11]);
			    
			    
			    para[31] = new Paragraph(new Chunk("changing innapropriate student behavior, and positive reinforcement techniques.",font9));
				para[31].setSpacingAfter(4);
				para[31].setIndentationRight(75);
				para[31].setIndentationLeft(125);
			    document.add(para[31]);
			    
			    
			    para[12] = new Paragraph(new Chunk("2.5              "+new Chunk( "Managing Bullying and Harassment",font11bold),font9bold));
				para[12].setSpacingAfter(4);
				para[12].setIndentationRight(75);
				para[12].setIndentationLeft(75);
			    document.add(para[12]);
			    
			    para[13] = new Paragraph(new Chunk("2.5a            Understand bullying and harassment and how to identify and react to both.",font9));
				para[13].setSpacingAfter(4);
				para[13].setIndentationRight(75);
				para[13].setIndentationLeft(75);
			    document.add(para[13]);
			    
			    
			    para[14] = new Paragraph(new Chunk("2.6             "+new Chunk( "Dismissal Time ",font11bold),font9bold));
				para[14].setSpacingAfter(4);
				para[14].setIndentationRight(75);
				para[14].setIndentationLeft(75);
			    document.add(para[14]);
			    
			    para[15] = new Paragraph(new Chunk("2.6a            Ensure lesson plans are completed.",font9));
				para[15].setSpacingAfter(4);
				para[15].setIndentationRight(75);
				para[15].setIndentationLeft(75);
			    document.add(para[15]);
			    
			    para[16] = new Paragraph(new Chunk("2.6b            Prepare students for dismissal.",font9));
				para[16].setSpacingAfter(4);
				para[16].setIndentationRight(75);
				para[16].setIndentationLeft(75);
			    document.add(para[16]);
			    
			    para[17] = new Paragraph(new Chunk("2.6c            Know where students go at the end of the day.",font9));
				para[17].setSpacingAfter(4);
				para[17].setIndentationRight(75);
				para[17].setIndentationLeft(75);
			    document.add(para[17]);
			    
			    para[18] = new Paragraph(new Chunk("2.7              "+new Chunk( "Finishing the Day ",font9bold),font9bold));
				para[18].setSpacingAfter(4);
				para[18].setIndentationRight(75);
				para[18].setIndentationLeft(75);
			    document.add(para[18]);
			    
			    para[19] = new Paragraph(new Chunk("2.7a            Understand proper methods for completing the day: Leave detailed notes for the teacher, leave the classsroom neat and",font9));
				para[19].setSpacingAfter(4);
				para[19].setIndentationRight(75);
				para[19].setIndentationLeft(75);
			    document.add(para[19]);
			    
			    
			    para[32] = new Paragraph(new Chunk("organized.",font9));
				para[32].setSpacingAfter(4);
				para[32].setIndentationRight(75);
				para[32].setIndentationLeft(125);
			    document.add(para[32]);
			    
			    para[20] = new Paragraph(new Chunk("2.7b             Know how to check out from the school: Inform administration and/or staff, leave all materials in the classroom.",font9));
				para[20].setSpacingAfter(4);
				para[20].setIndentationRight(75);
				para[20].setIndentationLeft(75);
			    document.add(para[20]);
			    document.newPage();
				
		        
		        
			
				document.newPage();
				
				
			}catch (Exception e) {
				e.printStackTrace();
			}finally
			{

				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}

			}
		//	return false;
		}
		
	// Indra jeet
	// TPL-5339 Unit3 SP certificate.
		public  void generatePDFSkillCertificateUnit3(HttpServletRequest httpRequest ,String reportPath , String fileName , String urlRootPath,TeacherDetail teacherDetail)
		{
			Font font20 = null;
			Font font16 = null;
			Font font16bold = null;
			Font font11bold = null;
			Font font7 = null;
			Font font9bold = null;
			Font font9 = null;
			BaseFont tahoma = null;
			String fontPath = httpRequest.getRealPath("/");
			try {
				tahoma = BaseFont.createFont(fontPath+"fonts/42933.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
				font20 = new Font(tahoma, 20,Font.NORMAL);
				font11bold = new Font(tahoma, 11,Font.BOLD);
				font16 = new Font(tahoma, 16 , Font.NORMAL);
				font16bold = new Font(tahoma, 12 , Font.BOLD);
				font7 = new Font(tahoma, 7);
				font9bold = new Font(tahoma, 7,Font.BOLD);
				font9 = new Font(tahoma, 7 );
				
			} catch (DocumentException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			Document document=null;
			FileOutputStream fos = null;
			PdfWriter writer = null;
			
			try{

				File file = new File(reportPath+"/temp");
				if(!file.exists())
					file.mkdirs();
				else
				Utility.deleteAllFileFromDir(reportPath+"/temp");
				document=new Document(PageSize.LETTER.rotate(),30f,30f,30f,55f);

				document.addAuthor("TeacherMatch");
				document.addCreator("TeacherMatch Inc.");
				document.addSubject("CRETIFICATION REPORT");
				document.addCreationDate();
				document.addTitle("CRETIFICATION REPORT");

				fos = new FileOutputStream(reportPath+"/"+fileName);
				writer = PdfWriter.getInstance(document, fos);
				writer.setStrictImageSequence(true);
				
				writer.setBoxSize("art", new Rectangle(29, 36, 760, 572));
				document.open();
				
				Rectangle rect= new Rectangle(29, 54, 760, 572);
		        rect.enableBorderSide(1);
		        rect.enableBorderSide(2);
		        rect.enableBorderSide(4);
		        rect.enableBorderSide(8);
		        rect.setBorder(Rectangle.BOX);
		        rect.setBorderWidth(1);
		        rect.setBorderColor(Color.BLACK);
		        document.add(rect);
		        
				PdfPTable mainTable = new PdfPTable(1);
				mainTable.setWidthPercentage(100);
				
				/////////////////////////// Start 1st page /////////////////////////////////////////
				Paragraph [] para = null;
				PdfPCell [] cell = null;

				para = new Paragraph[9];
				cell = new PdfPCell[9];
				para[0] = new Paragraph(" ",font20);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[0].setBorder(0);
				String teacherLenCheck=teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
				String hsscomp="has completed Unit 3 Smart Behaviors, of the";
				String smartPract="Smart Practices Substitute Educator Professional Development Program.";
				final BufferedImage image1 = ImageIO.read(new File(fontPath+"/images/skillCertificateHeaderUnit3New.png"));
				 Graphics g1 = image1.getGraphics();
				 Graphics g2 = image1.getGraphics();
				 Graphics2D g12d = (Graphics2D) g1;
				 java.awt.Font f=new java.awt.Font(teacherLenCheck, com.itextpdf.text.Font.BOLD, 26);
				 FontMetrics fm = g12d.getFontMetrics(f);
				 g1.setFont(g1.getFont().deriveFont(60f));
				 g1.setFont(g1.getFont().deriveFont(Font.BOLD));
				 g2.setFont(g2.getFont().deriveFont(60f));
				 System.out.println("String Width:"+fm.stringWidth(teacherLenCheck));
				 int x = ((image1.getWidth())/ 2-(int)(fm.stringWidth(teacherLenCheck)))+100;
			     int y = (int) (fm.getAscent() + (image1.getHeight() - (fm.getAscent() + fm.getDescent())) / 2);
			     g1.setColor(Color.BLACK);
				 g2.setColor(Color.BLACK);
				 System.out.println(" X Axis : "+x+"  Y Axis "+y);
				 g2.drawString(" This is to certify that", ((image1.getWidth()) / 2)-200, y-80);
				 g1.drawString(teacherLenCheck, x, y);
				 g2.drawString(hsscomp, ((image1.getWidth()) / 3)+50, y+80);
				 g2.drawString(smartPract, ((image1.getWidth()) / 4)-110, y+160);
				 g2.dispose();
				 g1.dispose();
			    
				ImageIO.write(image1, "png", new File(fontPath+"/images/skillCertificateHeaderUnit3Newtemp.png"));
			    Image logo = Image.getInstance (fontPath+"/images/skillCertificateHeaderUnit3Newtemp.png");
				logo.scalePercent((float) 50);
				logo.scaleAbsolute(729f, 516f);
				PdfPTable logotable = new PdfPTable(1);
				logotable.setWidthPercentage(100);
				PdfPCell logocol1 = new PdfPCell(new Paragraph("     ",font7));
				logocol1.setBorder(0);
				PdfPCell logocol2 = new PdfPCell(logo);
				logocol2.setBorder(0);
				
				logotable.addCell(logocol1);
				logotable.addCell(logocol2);
				cell[1]= new PdfPCell(logotable);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				
				mainTable.addCell(cell[1]);
				document.add(mainTable);
				document.newPage();
				
		        //////////Second Page ///////////////
				
			    rect= new Rectangle(25, 31, 765, 585);
		        rect.enableBorderSide(1);
		        rect.enableBorderSide(2);
		        rect.enableBorderSide(4);
		        rect.enableBorderSide(8);
		        rect.setBorder(Rectangle.BOX);
		        rect.setBorderWidth(1);
		        rect.setBorderColor(Color.BLACK);
		        document.add(rect);
				
		
		        para = new Paragraph[45];
				
		        Image logoSkillStart = Image.getInstance (fontPath+"/images/smartStartHeaderUnit3.png");
		        logoSkillStart.scaleAbsolute(738f, 105f);
		        logoSkillStart.setIndentationLeft(-4);
		        document.add(logoSkillStart);
		      //--------------------------------- Start 3.1------------------------------------------------------------------------------   
		        para[2] = new Paragraph(new Chunk("Competency and Objective",font9bold));
				para[2].setSpacingAfter(1);
				para[2].setIndentationLeft(60);
			    document.add(para[2]);
			    
			    
			    para[3] = new Paragraph(new Chunk("3.1              "+new Chunk( "Implementing the Classroom Management Plan ",font9bold),font9bold));
				para[3].setIndentationLeft(75);
				para[3].setIndentationRight(75);
			    document.add(para[3]);
			    
			    para[4] = new Paragraph(new Chunk("3.1a            Understand how to implement the permanent teacher�s defined rules and expectations",font9));
				para[4].setIndentationLeft(75);
				para[4].setIndentationRight(75);
			    document.add(para[4]);
			    
			    para[5] = new Paragraph(new Chunk("3.1b            Learn what resources in the school can be accessed for help",font9));
				para[5].setIndentationLeft(75);
				para[5].setIndentationRight(75);
			    document.add(para[5]);
		
			    para[6] = new Paragraph(new Chunk("3.1c            Understand how to provide consistent and leveled consequences based on the ",font9));
				para[6].setIndentationLeft(75);
				
				para[6].setIndentationRight(75);
			    document.add(para[6]);
			    
			    para[7] = new Paragraph(new Chunk("                   permanent teacher�s classroom management plan. ",font9));
				para[7].setIndentationLeft(75);
				para[7].setIndentationRight(75);
			    document.add(para[7]);
			  //--------------------------------- End 3.1------------------------------------------------------------------------------	    
			    
			    Chunk sigUnderline = new Chunk("   ");
			    sigUnderline.setUnderline(0.01f, 0.01f);
			    sigUnderline.setHorizontalScaling(75f);
			    para[37] = new Paragraph(new Chunk(" ",font9));
			    para[37].add(sigUnderline);
			    para[37].setFirstLineIndent(1f);
			    para[37].setIndentationLeft(75);
			    document.add(para[37]);
			    
			  //--------------------------------- Start 3.2------------------------------------------------------------------------------ 
			    para[10] = new Paragraph(new Chunk("3.2             "+new Chunk( " Knowing Pre-established Routines and Procedures",font9bold),font9bold));
				para[10].setIndentationLeft(75);
				para[10].setIndentationRight(75);
			    document.add(para[10]);
			    
			    para[11] = new Paragraph(new Chunk("3.2a            Learn typical pre-established routines and procedures for students entering the classroom in the morning",font9));
				para[11].setIndentationLeft(75);
				para[11].setIndentationRight(75);
			    document.add(para[11]);
			    
			    para[12] = new Paragraph(new Chunk("3.2b            Learn typical pre-established routines and procedures for student dismissal time",font9));
				para[12].setIndentationLeft(75);
				para[12].setIndentationRight(75);
			    document.add(para[12]);
			    
			    para[13] = new Paragraph(new Chunk("3.2c            Learn typical pre-established routines and procedures for transitions both in and out of the classroom",font9));
				para[13].setIndentationLeft(75);
				para[13].setIndentationRight(75);
			    document.add(para[13]);
			    
			    
			    sigUnderline.setUnderline(0.01f, 0.01f);
			    sigUnderline.setHorizontalScaling(75f);
			    para[39] = new Paragraph(new Chunk(" ",font9));
			    para[39].add(sigUnderline);
			    para[39].setFirstLineIndent(1f);
			    para[39].setIndentationLeft(75);
			    document.add(para[39]);
			   
//--------------------------------- End 3.2------------------------------------------------------------------------------
			    
//--------------------------------- Start 3.3------------------------------------------------------------------------------			    
			    para[14] = new Paragraph(new Chunk("3.3             "+new Chunk( " Utilizing Positive Framing",font9bold),font9bold));
				para[14].setIndentationLeft(75);
				para[14].setIndentationRight(75);
			    document.add(para[14]);
			   
			    para[16] = new Paragraph(new Chunk("3.3a            Learn strategic use of positive reinforcement",font9));
				para[16].setIndentationLeft(75);
				para[16].setIndentationRight(75);
			    document.add(para[16]);
			    
			    para[17] = new Paragraph(new Chunk("3.3b            Understand how to establish a positive academic environment",font9));
				para[17].setIndentationLeft(75);
				para[17].setIndentationRight(75);
			    document.add(para[17]);
			    
			    
			    para[19] = new Paragraph(new Chunk("3.3c            Understand how to interact with positive assumptions or messages",font9));
				para[19].setIndentationLeft(75);
				para[19].setIndentationRight(75);
			    document.add(para[19]);
			    
			    para[20] = new Paragraph(new Chunk("3.3d            Learn how to systematically recognize positive behavior",font9));
				para[20].setIndentationLeft(75);
				para[20].setIndentationRight(75);
			    document.add(para[20]);
			    
			    para[18] = new Paragraph(new Chunk("3.3e            Learn how to positively correct inappropriate behavior",font9));
				para[18].setIndentationLeft(75);
				para[18].setIndentationRight(75);
			    document.add(para[18]);
			    
			    
			    sigUnderline.setUnderline(0.01f, 0.01f);
			    sigUnderline.setHorizontalScaling(75f);
			    para[40] = new Paragraph(new Chunk(" ",font9));
			    para[40].add(sigUnderline);
			    para[40].setFirstLineIndent(1f);
			    para[40].setIndentationLeft(75);
			    document.add(para[40]);
		  
			  //--------------------------------- End 3.3------------------------------------------------------------------------------		
			    
			  //--------------------------------- start 3.4------------------------------------------------------------------------------ 
			    para[21] = new Paragraph(new Chunk("3.4            "+new Chunk( "  Redirecting Inappropriate Behavior and Misconduct ",font9bold),font9bold));
				para[21].setIndentationLeft(75);
				para[21].setIndentationRight(75);
			    document.add(para[21]);
			    
			    
			    para[22] = new Paragraph(new Chunk("3.4a            Understand that interventions should be progressive",font9));
				para[22].setIndentationLeft(75);
				para[22].setIndentationRight(75);
			    document.add(para[22]);
			    
			    para[23] = new Paragraph(new Chunk("3.4b            Learn how to stay calm and resolute",font9));
				para[23].setIndentationLeft(75);
				para[23].setIndentationRight(75);
			    document.add(para[23]);
			    
			  
			    para[24] = new Paragraph(new Chunk("3.4c            Learn how to use negative consequences only when necessary and as indicated by the classroom management plan ",font9));
				para[24].setIndentationLeft(75);
				para[24].setIndentationRight(75);
			    document.add(para[24]);
			    
			    para[25] = new Paragraph(new Chunk("3.4d            Learn how to intervene with individual students quietly",font9));
				para[25].setIndentationLeft(75);
				para[25].setIndentationRight(75);
			    document.add(para[25]);
			    
			    
			    para[33] = new Paragraph(new Chunk("3.4e            Understand that the focus should always be to redirect and stop misconduct rather than to punish",font9));
				
				para[33].setIndentationLeft(75);
				para[33].setIndentationRight(75);
			    document.add(para[33]);
		    
			    sigUnderline.setUnderline(0.01f, 0.01f);
			    sigUnderline.setHorizontalScaling(75f);
			    para[41] = new Paragraph(new Chunk(" ",font9));
			    para[41].add(sigUnderline);
			    para[41].setFirstLineIndent(1f);
			    para[41].setIndentationLeft(75);
			    document.add(para[41]);
		
			  //--------------------------------- End 3.4------------------------------------------------------------------------------ 
			   
			  //--------------------------------- Start 3.5------------------------------------------------------------------------------  
			    para[26] = new Paragraph(new Chunk("3.5            "+new Chunk( "  Maintaining academic flow and pacing ",font9bold),font9bold));
				para[26].setIndentationLeft(75);
				para[26].setIndentationRight(75);
			    document.add(para[26]);
			    
			    para[27] = new Paragraph(new Chunk("3.5a            Understand how to maintain bell-to-bell instruction",font9));
				para[27].setIndentationLeft(75);
				para[27].setIndentationRight(75);
			    document.add(para[27]);
			    
			    para[28] = new Paragraph(new Chunk("3.5b            Understand that having materials and supplies organized will help maintain the flow",font9));
				para[28].setIndentationLeft(75);
				para[28].setIndentationRight(75);
			    document.add(para[28]);
			    
			    para[29] = new Paragraph(new Chunk("3.5c            Learn that clear and concise directions will help maintain the flow",font9));
				para[29].setIndentationLeft(75);
				para[29].setIndentationRight(75);
			    document.add(para[29]);
			    
			    para[30] = new Paragraph(new Chunk("3.5d            Learn that smooth transitions will help maintain the flow",font9));
				para[30].setIndentationLeft(75);
				para[30].setIndentationRight(75);
			    document.add(para[30]);
			    
			    para[31] = new Paragraph(new Chunk("3.5e            Understand how to assist students with instructional purpose",font9));
				para[31].setIndentationLeft(75);
				para[31].setIndentationRight(75);
			    document.add(para[31]);
			    
			    sigUnderline.setUnderline(0.01f, 0.01f);
			    sigUnderline.setHorizontalScaling(75f);
			    para[42] = new Paragraph(new Chunk(" ",font9));
			    para[42].add(sigUnderline);
			    para[42].setFirstLineIndent(1f);
			    para[42].setIndentationLeft(75);
			    document.add(para[42]);
			  //--------------------------------- End 3.5------------------------------------------------------------------------------  
			  //--------------------------------- Start 3.6------------------------------------------------------------------------------ 
			    para[26] = new Paragraph(new Chunk("3.6            "+new Chunk( "  Mastering the Art of Questioning ",font9bold),font9bold));
				para[26].setIndentationLeft(75);
				para[26].setIndentationRight(75);
			    document.add(para[26]);
			    
			    para[27] = new Paragraph(new Chunk("3.6a            Learn to allow think time for appropriate use",font9));
				para[27].setIndentationLeft(75);
				para[27].setIndentationRight(75);
			    document.add(para[27]);
			    
			    para[28] = new Paragraph(new Chunk("3.6b            Understand how to effectively guide incorrect answers",font9));
				para[28].setIndentationLeft(75);
				para[28].setIndentationRight(75);
			    document.add(para[28]);
			    
			    para[29] = new Paragraph(new Chunk("3.6c            Learn how to use incorrect answers as a �teaching moment�",font9));
				para[29].setIndentationLeft(75);
				para[29].setIndentationRight(75);
			    document.add(para[29]);
			    
			  //--------------------------------- End 3.6------------------------------------------------------------------------------ 
				
			}catch (Exception e) {
				e.printStackTrace();
			}finally
			{

				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
		}	
    
		
		// Indra jeet
		// TPL-5393 Unit4 SP certificate.
			public  void generatePDFSkillCertificateUnit4(HttpServletRequest httpRequest ,String reportPath , String fileName , String urlRootPath,TeacherDetail teacherDetail)
			{
				Font font20 = null;
				Font font16 = null;
				Font font16bold = null;
				Font font11bold = null;
				Font font7 = null;
				Font font9bold = null;
				Font font9 = null;
				BaseFont tahoma = null;
				String fontPath = httpRequest.getRealPath("/");
				try {
					tahoma = BaseFont.createFont(fontPath+"fonts/42933.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
					font20 = new Font(tahoma, 20,Font.NORMAL);
					font11bold = new Font(tahoma, 11,Font.BOLD);
					font16 = new Font(tahoma, 16 , Font.NORMAL);
					font16bold = new Font(tahoma, 12 , Font.BOLD);
					font7 = new Font(tahoma, 7);
					font9bold = new Font(tahoma, 7,Font.BOLD);
					font9 = new Font(tahoma, 7 );
					
				} catch (DocumentException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				Document document=null;
				FileOutputStream fos = null;
				PdfWriter writer = null;
				
				try{

					File file = new File(reportPath+"/temp");
					if(!file.exists())
						file.mkdirs();
					else
					Utility.deleteAllFileFromDir(reportPath+"/temp");
					document=new Document(PageSize.LETTER.rotate(),30f,30f,30f,55f);

					document.addAuthor("TeacherMatch");
					document.addCreator("TeacherMatch Inc.");
					document.addSubject("CRETIFICATION REPORT");
					document.addCreationDate();
					document.addTitle("CRETIFICATION REPORT");

					fos = new FileOutputStream(reportPath+"/"+fileName);
					writer = PdfWriter.getInstance(document, fos);
					writer.setStrictImageSequence(true);
					
					writer.setBoxSize("art", new Rectangle(29, 36, 760, 572));
					document.open();
					
					Rectangle rect= new Rectangle(29, 54, 760, 572);
			        rect.enableBorderSide(1);
			        rect.enableBorderSide(2);
			        rect.enableBorderSide(4);
			        rect.enableBorderSide(8);
			        rect.setBorder(Rectangle.BOX);
			        rect.setBorderWidth(1);
			        rect.setBorderColor(Color.BLACK);
			        document.add(rect);
			        
					PdfPTable mainTable = new PdfPTable(1);
					mainTable.setWidthPercentage(100);
					
					/////////////////////////// Start 1st page /////////////////////////////////////////
					Paragraph [] para = null;
					PdfPCell [] cell = null;

					para = new Paragraph[9];
					cell = new PdfPCell[9];
					para[0] = new Paragraph(" ",font20);
					cell[0]= new PdfPCell(para[0]);
					cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[0].setBorder(0);
					String teacherLenCheck=teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
					String hsscomp="has completed Unit 4 Smart Skills, of the";
					String smartPract="Smart Practices Substitute Educator Professional Development Program.";
					
					 final BufferedImage image1 = ImageIO.read(new File(fontPath+"/images/skillCertificateHeaderUnit4.png"));
					 Graphics g1 = image1.getGraphics();
					 Graphics g2 = image1.getGraphics();
					 Graphics2D g12d = (Graphics2D) g1;
					 java.awt.Font f=new java.awt.Font(teacherLenCheck, com.itextpdf.text.Font.BOLD, 26);
					 FontMetrics fm = g12d.getFontMetrics(f);
					 g1.setFont(g1.getFont().deriveFont(60f));
					 g1.setFont(g1.getFont().deriveFont(Font.BOLD));
					 g2.setFont(g2.getFont().deriveFont(60f));
					 System.out.println("String Width:"+fm.stringWidth(teacherLenCheck));
					 int x = ((image1.getWidth())/ 2-(int)(fm.stringWidth(teacherLenCheck)))+100;
				     int y = (int) (fm.getAscent() + (image1.getHeight() - (fm.getAscent() + fm.getDescent())) / 2);
				     g1.setColor(Color.BLACK);
					 g2.setColor(Color.BLACK);
					 System.out.println(" X Axis : "+x+"  Y Axis "+y);
					 g2.drawString(" This is to certify that", ((image1.getWidth()) / 2)-200, y-80);
					 g1.drawString(teacherLenCheck, x, y);
					 g2.drawString(hsscomp, ((image1.getWidth()) / 3)+50, y+80);
					 g2.drawString(smartPract, ((image1.getWidth()) / 4)-110, y+160);
					 g2.dispose();
					 g1.dispose();
					 
					 
				    ImageIO.write(image1, "png", new File(fontPath+"/images/skillCertificateHeaderUnit4Newtemp.png"));
				    Image logo = Image.getInstance (fontPath+"/images/skillCertificateHeaderUnit4Newtemp.png");
					logo.scalePercent((float) 50);
					logo.scaleAbsolute(729f, 516f);
					PdfPTable logotable = new PdfPTable(1);
					logotable.setWidthPercentage(100);
					PdfPCell logocol1 = new PdfPCell(new Paragraph("     ",font7));
					logocol1.setBorder(0);
					PdfPCell logocol2 = new PdfPCell(logo);
					logocol2.setBorder(0);
					
					logotable.addCell(logocol1);
					logotable.addCell(logocol2);
					cell[1]= new PdfPCell(logotable);
					cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[1].setBorder(0);
					
					mainTable.addCell(cell[1]);
					document.add(mainTable);
					document.newPage();
					
			        //////////Second Page ///////////////
					
				    rect= new Rectangle(25, 31, 765, 585);
			        rect.enableBorderSide(1);
			        rect.enableBorderSide(2);
			        rect.enableBorderSide(4);
			        rect.enableBorderSide(8);
			        rect.setBorder(Rectangle.BOX);
			        rect.setBorderWidth(1);
			        rect.setBorderColor(Color.BLACK);
			        document.add(rect);
					
			
			        para = new Paragraph[45];
					
			        Image logoSkillStart = Image.getInstance (fontPath+"/images/smartStartHeaderUnit4.png");
			        logoSkillStart.scaleAbsolute(738f, 105f);
			        logoSkillStart.setIndentationLeft(-4);
			        document.add(logoSkillStart);
			      //--------------------------------- Start 4.1------------------------------------------------------------------------------   
			        para[2] = new Paragraph(new Chunk("Competency and Objective",font9bold));
					para[2].setSpacingAfter(1);
					para[2].setIndentationLeft(60);
				    document.add(para[2]);
				    
				    
				    para[3] = new Paragraph(new Chunk("4.1              "+new Chunk( "Higher-Order Thinking Skill 1: Knowing ",font9bold),font9bold));
					para[3].setIndentationLeft(75);
					para[3].setIndentationRight(75);
				    document.add(para[3]);
				    
				    para[4] = new Paragraph(new Chunk("4.1a            Learn the definition of the thinking skill �Knowing�",font9));
					para[4].setIndentationLeft(75);
					para[4].setIndentationRight(75);
				    document.add(para[4]);
				    
				    para[5] = new Paragraph(new Chunk("4.1b            Understand key words associated with the thinking skill �Knowing�",font9));
					para[5].setIndentationLeft(75);
					para[5].setIndentationRight(75);
				    document.add(para[5]);
			
				    para[6] = new Paragraph(new Chunk("4.1c            Learn lessons or activities associated with the thinking skill �Knowing� ",font9));
					para[6].setIndentationLeft(75);
					
					para[6].setIndentationRight(75);
				    document.add(para[6]);
				    
				    para[7] = new Paragraph(new Chunk("4.1d            Learn questions and sentence starters to use at the �Knowing� level ",font9));
					para[7].setIndentationLeft(75);
					para[7].setIndentationRight(75);
				    document.add(para[7]);
				  //--------------------------------- End 4.1------------------------------------------------------------------------------	    
				    
				    Chunk sigUnderline = new Chunk("   ");
				    sigUnderline.setUnderline(0.01f, 0.01f);
				    sigUnderline.setHorizontalScaling(75f);
				    para[37] = new Paragraph(new Chunk(" ",font9));
				    para[37].add(sigUnderline);
				    para[37].setFirstLineIndent(1f);
				    para[37].setIndentationLeft(75);
				    document.add(para[37]);
				    
				  //--------------------------------- Start 4.2------------------------------------------------------------------------------ 
				    para[10] = new Paragraph(new Chunk("4.2             "+new Chunk( " Higher-Order Thinking Skill 2: Understanding",font9bold),font9bold));
					para[10].setIndentationLeft(75);
					para[10].setIndentationRight(75);
				    document.add(para[10]);
				    
				    para[11] = new Paragraph(new Chunk("4.2a            Learn the definition of the thinking skill �Understanding�",font9));
					para[11].setIndentationLeft(75);
					para[11].setIndentationRight(75);
				    document.add(para[11]);
				    
				    para[12] = new Paragraph(new Chunk("4.2b            Understand key words associated with the thinking skill �Understanding�",font9));
					para[12].setIndentationLeft(75);
					para[12].setIndentationRight(75);
				    document.add(para[12]);
				    
				    para[13] = new Paragraph(new Chunk("4.2c            Learn lessons or activities associated with the thinking skill �Understanding�",font9));
					para[13].setIndentationLeft(75);
					para[13].setIndentationRight(75);
				    document.add(para[13]);
				    para[14] = new Paragraph(new Chunk("4.2d            Learn questions and sentence starters to use at the �Understanding� level",font9));
					para[14].setIndentationLeft(75);
					para[14].setIndentationRight(75);
				    document.add(para[14]);
				    
				    
				    sigUnderline.setUnderline(0.01f, 0.01f);
				    sigUnderline.setHorizontalScaling(75f);
				    para[39] = new Paragraph(new Chunk(" ",font9));
				    para[39].add(sigUnderline);
				    para[39].setFirstLineIndent(1f);
				    para[39].setIndentationLeft(75);
				    document.add(para[39]);
				   
	//--------------------------------- End 4.2------------------------------------------------------------------------------
				    
	//--------------------------------- Start 4.3------------------------------------------------------------------------------			    
				    para[15] = new Paragraph(new Chunk("4.3             "+new Chunk( " Higher-Order Thinking Skill 3: Applying",font9bold),font9bold));
					para[15].setIndentationLeft(75);
					para[15].setIndentationRight(75);
				    document.add(para[15]);
				   
				    para[16] = new Paragraph(new Chunk("4.3a            Learn the definition of the thinking skill �Applying�",font9));
					para[16].setIndentationLeft(75);
					para[16].setIndentationRight(75);
				    document.add(para[16]);
				    
				    para[17] = new Paragraph(new Chunk("4.3b            Understand key words associated with the thinking skill �Applying�",font9));
					para[17].setIndentationLeft(75);
					para[17].setIndentationRight(75);
				    document.add(para[17]);
				    
				    
				    para[19] = new Paragraph(new Chunk("4.3c            Learn lessons or activities associated with the thinking skill �Applying�",font9));
					para[19].setIndentationLeft(75);
					para[19].setIndentationRight(75);
				    document.add(para[19]);
				    
				    para[20] = new Paragraph(new Chunk("4.3d            Learn questions and sentence starters to use at the �Applying� level",font9));
					para[20].setIndentationLeft(75);
					para[20].setIndentationRight(75);
				    document.add(para[20]);
				    
				    sigUnderline.setUnderline(0.01f, 0.01f);
				    sigUnderline.setHorizontalScaling(75f);
				    para[40] = new Paragraph(new Chunk(" ",font9));
				    para[40].add(sigUnderline);
				    para[40].setFirstLineIndent(1f);
				    para[40].setIndentationLeft(75);
				    document.add(para[40]);
			  
				  //--------------------------------- End 4.3------------------------------------------------------------------------------		
				    
				  //--------------------------------- start 4.4------------------------------------------------------------------------------ 
				    para[21] = new Paragraph(new Chunk("4.4            "+new Chunk( "  Higher-Order Thinking Skill 4: Analyzing",font9bold),font9bold));
					para[21].setIndentationLeft(75);
					para[21].setIndentationRight(75);
				    document.add(para[21]);
				    
				    
				    para[22] = new Paragraph(new Chunk("4.4a            Learn the definition of the thinking skill �Analyzing�",font9));
					para[22].setIndentationLeft(75);
					para[22].setIndentationRight(75);
				    document.add(para[22]);
				    
				    para[23] = new Paragraph(new Chunk("4.4b           Understand key words associated with the thinking skill �Analyzing�",font9));
					para[23].setIndentationLeft(75);
					para[23].setIndentationRight(75);
				    document.add(para[23]);
				    
				  
				    para[24] = new Paragraph(new Chunk("4.4c            Learn lessons or activities associated with the thinking skill �Analyzing�",font9));
					para[24].setIndentationLeft(75);
					para[24].setIndentationRight(75);
				    document.add(para[24]);
				    
				    para[25] = new Paragraph(new Chunk("4.4d            Learn questions and sentence starters to use at the �Analyzing� level",font9));
					para[25].setIndentationLeft(75);
					para[25].setIndentationRight(75);
				    document.add(para[25]);
				    
				    sigUnderline.setUnderline(0.01f, 0.01f);
				    sigUnderline.setHorizontalScaling(75f);
				    para[41] = new Paragraph(new Chunk(" ",font9));
				    para[41].add(sigUnderline);
				    para[41].setFirstLineIndent(1f);
				    para[41].setIndentationLeft(75);
				    document.add(para[41]);
			
				  //--------------------------------- End 4.4------------------------------------------------------------------------------ 
				   
				  //--------------------------------- Start 4.5------------------------------------------------------------------------------  
				    para[26] = new Paragraph(new Chunk("4.5            "+new Chunk( "  Higher-Order Thinking Skill 5: Evaluating ",font9bold),font9bold));
					para[26].setIndentationLeft(75);
					para[26].setIndentationRight(75);
				    document.add(para[26]);
				    
				    para[27] = new Paragraph(new Chunk("4.5a            Learn the definition of the thinking skill �Evaluating�",font9));
					para[27].setIndentationLeft(75);
					para[27].setIndentationRight(75);
				    document.add(para[27]);
				    
				    para[28] = new Paragraph(new Chunk("4.5b            Understand key words associated with the thinking skill �Evaluating�",font9));
					para[28].setIndentationLeft(75);
					para[28].setIndentationRight(75);
				    document.add(para[28]);
				    
				    para[29] = new Paragraph(new Chunk("4.5c            Learn lessons or activities associated with the thinking skill �Evaluating�",font9));
					para[29].setIndentationLeft(75);
					para[29].setIndentationRight(75);
				    document.add(para[29]);
				    
				    para[30] = new Paragraph(new Chunk("4.5d            Learn questions and sentence starters to use at the �Evaluating� level",font9));
					para[30].setIndentationLeft(75);
					para[30].setIndentationRight(75);
				    document.add(para[30]);
				    
				    sigUnderline.setUnderline(0.01f, 0.01f);
				    sigUnderline.setHorizontalScaling(75f);
				    para[42] = new Paragraph(new Chunk(" ",font9));
				    para[42].add(sigUnderline);
				    para[42].setFirstLineIndent(1f);
				    para[42].setIndentationLeft(75);
				    document.add(para[42]);
				  //--------------------------------- End 4.5------------------------------------------------------------------------------  
				  //--------------------------------- Start 4.6------------------------------------------------------------------------------ 
				    para[26] = new Paragraph(new Chunk("4.6            "+new Chunk( "  Higher-Order Thinking Skill 6: Creating ",font9bold),font9bold));
					para[26].setIndentationLeft(75);
					para[26].setIndentationRight(75);
				    document.add(para[26]);
				    
				    para[27] = new Paragraph(new Chunk("4.6a            Learn the definition of the thinking skill �Creating�",font9));
					para[27].setIndentationLeft(75);
					para[27].setIndentationRight(75);
				    document.add(para[27]);
				    
				    para[28] = new Paragraph(new Chunk("4.6b            Understand key words associated with the thinking skill �Creating�",font9));
					para[28].setIndentationLeft(75);
					para[28].setIndentationRight(75);
				    document.add(para[28]);
				    
				    para[29] = new Paragraph(new Chunk("4.6c            Learn lessons or activities associated with the thinking skill �Creating�",font9));
					para[29].setIndentationLeft(75);
					para[29].setIndentationRight(75);
				    document.add(para[29]);
				    
				    para[30] = new Paragraph(new Chunk("4.6d            Learn questions and sentence starters to use at the �Creating� level",font9));
					para[30].setIndentationLeft(75);
					para[30].setIndentationRight(75);
				    document.add(para[30]);
				    
				  //--------------------------------- End 4.6------------------------------------------------------------------------------ 
					
				}catch (Exception e) {
					e.printStackTrace();
				}finally
				{

					if(document != null && document.isOpen())
						document.close();
					if(writer!=null){
						writer.flush();
						writer.close();
					}

				}
			}			
		
	}
