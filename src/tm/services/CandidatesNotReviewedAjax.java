package tm.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Colour;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import jxl.format.Alignment;
import java.util.Locale;
import java.util.Set;
import jxl.write.Label;


import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAcademics;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.utility.Utility;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

public class CandidatesNotReviewedAjax 
{	
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	public List<DistrictMaster> getFieldOfDistrictList(String DistrictName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		try{

			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			Criterion criterion1 = Restrictions.eq("status", "A");
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);

				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);

				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));

			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion1);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return districtMasterList;
	}
	
	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
		List<DistrictSchools> fieldOfSchoolList1 = null;
		List<DistrictSchools> fieldOfSchoolList2 = null;
		try 
		{
			Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
			if(SchoolName.length()>0){
				if(districtIdForSchool==0){
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
						Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					}else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}else{
					DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
					Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
					
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
						Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
						
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					}else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return schoolMasterList;
		
	}
	
	public String displayAllCandidateNotReviewedRecord(String districtName,int districtId,String jobStatus,int jobCategory,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate,String jobendstartDate,String jobendendDate)
	{
		
		System.out.println("::::::: displayAllCandidateNotReviewedRecord :::::::");
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
			
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}

			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			
			//------------------------------------
		 /** set default sorting fieldName **/
			String  sortOrderStrVal		=	null;
			String sortOrderFieldName	=	"districtName";
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
			}
			
			List<String[]> candidatesNotReviewedList =new ArrayList<String[]>();
			
			//candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId);
			System.out.println(":: entityID ::"+entityID);
			if(entityID==2||entityID==3)
			{
				candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
		    	totalRecord = candidatesNotReviewedList.size();
		    	System.out.println("totallllllllllll========="+totalRecord);
		    	 
			}
			else if(entityID==1) 
			{
				System.out.println("indside admin ");
				if(districtId!=0)
				{
					candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
			    	totalRecord = candidatesNotReviewedList.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
				}
				else
				{System.out.println(":::::::: Admin Login Starts ::::::::::");
					//candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedListForAdmin(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
					candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
			    	totalRecord = candidatesNotReviewedList.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
				}
		    	
			}
			
			totalRecord = candidatesNotReviewedList.size();
			
			List<String[]> finalcandidatesNotReviewedList =new ArrayList<String[]>();
		     if(totalRecord<end)
					end=totalRecord;
		     finalcandidatesNotReviewedList = candidatesNotReviewedList.subList(start,end);
			
			String responseText="";
			if(districtId!=0)
			{
				tmRecords.append("<table  id='tblGridCandidatesNotReviewed' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDistrictName", locale),sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
	    
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobCat", locale),sortOrderFieldName,"jobCategory",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJOBID", locale),sortOrderFieldName,"jobId",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoStatus", locale),sortOrderFieldName,"jobStatus",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobPostDate", locale),sortOrderFieldName,"jobPostDate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobEndDate", locale),sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblNoOfNotReviewed", locale),sortOrderFieldName,"noOfNotReviewed",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				
				if(finalcandidatesNotReviewedList.size()==0){
					 tmRecords.append("<tr><td colspan='9' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
					}
				
				if(finalcandidatesNotReviewedList.size()>0){
					for (Iterator it = finalcandidatesNotReviewedList.iterator(); it.hasNext();){
						String[] row = (String[]) it.next();
						String  myVal1="";
						
						Date jobStartDate 	= null;
						Date jobEndDate 	= null;
	  					DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
	  					
						 tmRecords.append("<tr>");
						 
						 myVal1 = (row[0]==null)? "N/A" : row[0].toString();
		                 tmRecords.append("<td>"+myVal1+"</td>");
		                 
		                 myVal1 = (row[1]==null)? "N/A" : row[1].toString();
		                 tmRecords.append("<td>"+myVal1+"</td>");
		                 
		                 myVal1 = (row[2]==null)? "N/A" : row[2].toString();
		                 tmRecords.append("<td>"+myVal1+"</td>");
		                 
		                 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
		                 tmRecords.append("<td>"+myVal1+"</td>");
		                 
		                 myVal1 = (row[4]==null)? "N/A" : row[4].toString();
		                 
		                 if(myVal1.equals("A"))
		                	 myVal1 = "Active";
		                 else
		                	 myVal1 = "Inactive";

		                 tmRecords.append("<td>"+myVal1+"</td>");
		                 
		                 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
		                 jobStartDate = df.parse(myVal1);
		                 tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jobStartDate)+"</td>");
		                 
		                 myVal1 = (row[6]==null)? "N/A" : row[6].toString();
		                 jobEndDate = df.parse(myVal1);
		                 tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jobEndDate)+"</td>");
		                 
		                 myVal1 = (row[7]==null)? "N/A" : row[7].toString();
		                 tmRecords.append("<td>"+myVal1+"</td>");
		                 
		                 tmRecords.append("</tr>");
		                    
					}
				}
				
				tmRecords.append("</table>");
				tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			}
		}catch(Exception exception){
			exception.printStackTrace();
		}
		
		 return tmRecords.toString();
	}
	
	public String displayAllCandidateNotReviewedRecordExcel(String districtName,int districtId,String jobStatus,int jobCategory,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate,String jobendstartDate,String jobendendDate)
	{

		System.out.println("::::::: displayAllCandidateNotReviewedRecordExcel :::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		String fileName = null;
		try{
			
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}

			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			
			//------------------------------------
		 /** set default sorting fieldName **/
			String  sortOrderStrVal		=	null;
			String sortOrderFieldName	=	"districtName";
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
			}
			
			List<String[]> candidatesNotReviewedList =new ArrayList<String[]>();
			
			if(entityID==2||entityID==3)
			{
				candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
		    	totalRecord = candidatesNotReviewedList.size();
		    	System.out.println("totallllllllllll========="+totalRecord);
		    	 
			}
			else if(entityID==1) 
			{
				System.out.println("indside admin ");
				if(districtId!=0)
				{
					candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
			    	totalRecord = candidatesNotReviewedList.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
				}
				else
				{
					//candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedListForAdmin(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
					candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
			    	totalRecord = candidatesNotReviewedList.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
				}
		    	
			}
			
			totalRecord = candidatesNotReviewedList.size();
			
			List<String[]> finalcandidatesNotReviewedList =new ArrayList<String[]>();
		     if(totalRecord<end)
					end=totalRecord;
		     finalcandidatesNotReviewedList	=	candidatesNotReviewedList;
		     
		     /* Excel Export */
		     
		     String time = String.valueOf(System.currentTimeMillis()).substring(6);
		     String basePath = request.getSession().getServletContext().getRealPath ("/")+"/candidatesNotReviewedByJobSummary";
		     System.out.println(":: File Path :: "+basePath);
				fileName ="candidatesNotReviewedByJobSummary"+time+".xls";
		     
				File file = new File(basePath);
				if(!file.exists())
					file.mkdirs();
				
				Utility.deleteAllFileFromDir(basePath);
				file = new File(basePath+"/"+fileName);
				
				WorkbookSettings wbSettings = new WorkbookSettings();

				wbSettings.setLocale(new Locale("en", "EN"));

				WritableCellFormat timesBoldUnderline;
				WritableCellFormat header;
				WritableCellFormat headerBold;
				WritableCellFormat times;

				WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
				workbook.createSheet("candidatesNotReviewed", 0);
				WritableSheet excelSheet = workbook.getSheet(0);

				WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
				// Define the cell format
				times = new WritableCellFormat(times10pt);
				// Lets automatically wrap the cells
				times.setWrap(true);

				// Create create a bold font with unterlines
				WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
				WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

				timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
				timesBoldUnderline.setAlignment(Alignment.CENTRE);
				// Lets automatically wrap the cells
				timesBoldUnderline.setWrap(true);

				header = new WritableCellFormat(times10ptBoldUnderline);
				headerBold = new WritableCellFormat(times10ptBoldUnderline);
				CellView cv = new CellView();
				cv.setFormat(times);
				cv.setFormat(timesBoldUnderline);
				cv.setAutosize(true);

				header.setBackground(Colour.GRAY_25);
				
				// Write a few headers
				excelSheet.mergeCells(0, 0, 7, 1);
				Label label;
				label = new Label(0, 0, Utility.getLocaleValuePropByKey("lblCandidatesNotReviewed", locale), timesBoldUnderline);
				excelSheet.addCell(label);
				excelSheet.mergeCells(0, 3, 7, 3);
				label = new Label(0, 3, "");
				excelSheet.addCell(label);
				excelSheet.getSettings().setDefaultColumnWidth(18);
				
				int k=4;
				int col=1;
				
				label = new Label(0, k, Utility.getLocaleValuePropByKey("lblDistrictName", locale),header); 
				excelSheet.addCell(label);
				
				label = new Label(1, k, Utility.getLocaleValuePropByKey("lblJoTil", locale),header); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJobCat", locale),header); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJOBID", locale),header); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJoStatus", locale),header);
				excelSheet.addCell(label);
				
				label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJobPostDate", locale),header); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJobEndDate", locale),header); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblNoOfNotReviewed", locale),header); 
				excelSheet.addCell(label);
				
				k=k+1;
				if(finalcandidatesNotReviewedList.size()==0)
				{	
					excelSheet.mergeCells(0, k, 9, k);
					label = new Label(0, k, Utility.getLocaleValuePropByKey("lblNoRecord", locale)); 
					excelSheet.addCell(label);
				}
				String sta="";
				
				if(finalcandidatesNotReviewedList.size()>0){
					for (Iterator it = finalcandidatesNotReviewedList.iterator(); it.hasNext();){
						String[] row = (String[]) it.next();
						String  myVal1="";
						
						Date jobStartDate 	= null;
						Date jobEndDate 	= null;
	  					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						
						col=1;
						
						myVal1 = (row[0]==null)? "N/A" : row[0].toString();
						label = new Label(0, k,myVal1); 
						excelSheet.addCell(label);
						
						myVal1 = (row[1]==null)? "N/A" : row[1].toString();
						label = new Label(1, k, myVal1); 
						excelSheet.addCell(label);
						
						myVal1 = (row[2]==null)? "N/A" : row[2].toString();
						label = new Label(2, k, myVal1); 
						excelSheet.addCell(label);
						
						myVal1 = (row[3]==null)? "N/A" : row[3].toString();
						label = new Label(3, k, myVal1);
						excelSheet.addCell(label);
						
						myVal1 = (row[4]==null)? "N/A" : row[4].toString();
						
						if(myVal1.equals("A"))
		                	 myVal1 = "Active";
		                 else
		                	 myVal1 = "Inactive";
						
						label = new Label(4, k, myVal1);
						excelSheet.addCell(label);
						
						myVal1 = (row[5]==null)? "N/A" : row[5].toString();
						jobStartDate = df.parse(myVal1);
						label = new Label(5, k, Utility.convertDateAndTimeToUSformatOnlyDate(jobStartDate));
						excelSheet.addCell(label);
						
						myVal1 = (row[6]==null)? "N/A" : row[6].toString();
						jobEndDate = df.parse(myVal1);
						label = new Label(6, k, Utility.convertDateAndTimeToUSformatOnlyDate(jobEndDate));
						excelSheet.addCell(label);
						
						myVal1 = (row[7]==null)? "N/A" : row[7].toString();
						label = new Label(7, k, myVal1);
						excelSheet.addCell(label);
						
						k++; 
					}
				}
				workbook.write();
				workbook.close();
			
		}catch(Exception exception){
			exception.printStackTrace();
		}
		 return fileName;
	}
	
	public String displayAllCandidateNotReviewedRecordPDF(String districtName,int districtId,String jobStatus,int jobCategory,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate,String jobendstartDate,String jobendendDate)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport(districtName, districtId, jobStatus, jobCategory, noOfRow, pageNo, sortOrder, sortOrderType, startDate, endDate, jobendstartDate, jobendendDate, basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	
	}
	
	public boolean generateCandidatePDReport(String districtName,int districtId,String jobStatus,int jobCategory,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate,String jobendstartDate,String jobendendDate,String path,String realPath)
	{
		
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		
			
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			int sortingcheck=1;
			
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				//------------------------------------
				 /** set default sorting fieldName **/
					String  sortOrderStrVal		=	null;
					String sortOrderFieldName	=	"districtName";
					
					if(!sortOrder.equals("") && !sortOrder.equals(null))
					{
						sortOrderFieldName		=	sortOrder;
					}
					
					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null))
					{
						if(sortOrderType.equals("0"))
						{
							sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
						}
						else
						{
							sortOrderTypeVal	=	"1";
							sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
						}
					}
					else
					{
						sortOrderTypeVal		=	"0";
						sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
					}
					
					List<String[]> candidatesNotReviewedList =new ArrayList<String[]>();
					
					//candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId);
					
					if(entityID==2||entityID==3)
					{
						candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
				    	totalRecord = candidatesNotReviewedList.size();
				    	System.out.println("totallllllllllll========="+totalRecord);
				    	 
					}
					else if(entityID==1) 
					{
						System.out.println("indside admin ");
						if(districtId!=0)
						{
							candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
					    	totalRecord = candidatesNotReviewedList.size();
					    	System.out.println("totallllllllllll========="+totalRecord);
						}
						else
						{
							//candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedListForAdmin(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
							candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
					    	totalRecord = candidatesNotReviewedList.size();
					    	System.out.println("totallllllllllll========="+totalRecord);
						}
				    	
					}
					
					totalRecord = candidatesNotReviewedList.size();
					
					List<String[]> finalcandidatesNotReviewedList =new ArrayList<String[]>();
				     if(totalRecord<end)
							end=totalRecord;
				     finalcandidatesNotReviewedList	=	candidatesNotReviewedList;
				
				     String fontPath = realPath;
				     
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 6, Font.NORMAL);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 8);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);

						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						
						document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject(Utility.getLocaleValuePropByKey("lblCandidatesNotReviewed", locale));
						document.addCreationDate();
						document.addTitle(Utility.getLocaleValuePropByKey("lblCandidatesNotReviewed", locale));

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;

						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));

						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];

						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblCandidatesNotReviewed", locale),font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
						
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph( Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						document.add(new Phrase("\n"));

						float[] tblwidth={.15f,.15f,.05f,.06f,.08f,.08f,.05f,.05f};
						
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[13];
						cell = new PdfPCell[13];

						// header
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblDistrictName", locale),font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJoTil", locale),font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJobCat", locale),font10_10);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[2]);
						
						para[3] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJOBID", locale),font10_10);
						cell[3]= new PdfPCell(para[3]);
						cell[3].setBackgroundColor(bluecolor);
						cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[3]);

						para[4] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJoStatus", locale),font10_10);
						cell[4]= new PdfPCell(para[4]);
						cell[4].setBackgroundColor(bluecolor);
						cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[4]);
						
						para[5] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJobPostDate", locale),font10_10);
						cell[5]= new PdfPCell(para[5]);
						cell[5].setBackgroundColor(bluecolor);
						cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[5]);
						
						para[6] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJobEndDate", locale),font10_10);
						cell[6]= new PdfPCell(para[6]);
						cell[6].setBackgroundColor(bluecolor);
						cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[6]);
						
						para[7] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblNoOfNotReviewed", locale),font10_10);
						cell[7]= new PdfPCell(para[7]);
						cell[7].setBackgroundColor(bluecolor);
						cell[7].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[7]);
						
						document.add(mainTable);
						
						if(finalcandidatesNotReviewedList.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblNoRecord", locale),font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
						
						 String sta="";
						 if(finalcandidatesNotReviewedList.size()>0){
							 for (Iterator it = finalcandidatesNotReviewedList.iterator(); it.hasNext();) 
							 {
								  String[] row = (String[]) it.next();
								  int index=0;
								 									
								  mainTable = new PdfPTable(tblwidth);
								  mainTable.setWidthPercentage(100);
								  para = new Paragraph[13];
								  cell = new PdfPCell[13];
								
	                              String myVal1="";
	                              String myVal2="";
	                              
	                              Date jobStartDate 	= null;
	      						  Date jobEndDate 	= null;
	      	  				 	  DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	      	  				 	  
	      	  				 	 myVal1 = (row[0]==null)? "N/A" : row[0].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[1]==null)? "N/A" : row[1].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[2]==null)? "N/A" : row[2].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[4]==null)? "N/A" : row[4].toString();
								 
								 if(myVal1.equals("A"))
				                	 myVal1 = "Active";
				                 else
				                	 myVal1 = "Inactive";
								 
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
								 jobStartDate = df.parse(myVal1);
								 para[index] = new Paragraph(Utility.convertDateAndTimeToUSformatOnlyDate(jobStartDate),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[6]==null)? "N/A" : row[6].toString();
								 jobEndDate = df.parse(myVal1);
								 para[index] = new Paragraph(Utility.convertDateAndTimeToUSformatOnlyDate(jobEndDate),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 myVal1 = (row[7]==null)? "N/A" : row[7].toString();
								 para[index] = new Paragraph(myVal1,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
	      	  				 	 
								 document.add(mainTable);

							 }
						 }

			} catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			return true;
	 }
	
	public String displayAllCandidateNotReviewedRecordPrintPreview(String districtName,int districtId,String jobStatus,int jobCategory,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate,String jobendstartDate,String jobendendDate)
	{
		
		System.out.println("::::::: displayAllCandidateNotReviewedRecord :::::::");
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
			
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}

			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			
			//------------------------------------
		 /** set default sorting fieldName **/
			String  sortOrderStrVal		=	null;
			String sortOrderFieldName	=	"districtName";
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
			}
			
			List<String[]> candidatesNotReviewedList =new ArrayList<String[]>();
			
			//candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId);
			System.out.println(":: entityID ::"+entityID);
			if(entityID==2||entityID==3)
			{
				candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
		    	totalRecord = candidatesNotReviewedList.size();
		    	System.out.println("totallllllllllll========="+totalRecord);
		    	 
			}
			else if(entityID==1) 
			{
				System.out.println("indside admin ");
				if(districtId!=0)
				{
					candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
			    	totalRecord = candidatesNotReviewedList.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
				}
				else
				{System.out.println(":::::::: Admin Login Starts ::::::::::");
					//candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedListForAdmin(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
					candidatesNotReviewedList = jobForTeacherDAO.candidatesNotReviewedList(districtId,jobStatus,jobCategory,sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName,startDate,endDate,jobendstartDate,jobendendDate);
			    	totalRecord = candidatesNotReviewedList.size();
			    	System.out.println("totallllllllllll========="+totalRecord);
				}
		    	
			}
			
			totalRecord = candidatesNotReviewedList.size();
			
			List<String[]> finalcandidatesNotReviewedList =new ArrayList<String[]>();
		     if(totalRecord<end)
					end=totalRecord;
		     finalcandidatesNotReviewedList	=	candidatesNotReviewedList;
			
			String responseText="";
			if(districtId!=0)
			{
				tmRecords.append("<table  id='tblGridCandidatesNotReviewedPrintPre' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblDistrictName", locale)+"</th>");
	    
				tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJoTil", locale)+"</th>");
				
				tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJobCat", locale)+"</th>");
				
				tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJOBID", locale)+"</th>");
				
				tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJoStatus", locale)+"</th>");
				
				tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJobPostDate", locale)+"</th>");
				
				tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJobEndDate", locale)+"</th>");
				
				tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblNoOfNotReviewed", locale)+"</th>");
				
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				
				if(finalcandidatesNotReviewedList.size()==0){
					 tmRecords.append("<tr><td colspan='9' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
					}
				// Utility.convertDateAndTimeToUSformatOnlyDate(
				if(finalcandidatesNotReviewedList.size()>0){
					for (Iterator it = finalcandidatesNotReviewedList.iterator(); it.hasNext();){
						String[] row = (String[]) it.next();
						String  myVal1="";
						
						Date jobStartDate 	= null;
						Date jobEndDate 	= null;
	  					DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
	  					
						 tmRecords.append("<tr>");
						 
						 myVal1 = (row[0]==null)? "N/A" : row[0].toString();
		                 tmRecords.append("<td>"+myVal1+"</td>");
		                 
		                 myVal1 = (row[1]==null)? "N/A" : row[1].toString();
		                 tmRecords.append("<td>"+myVal1+"</td>");
		                 
		                 myVal1 = (row[2]==null)? "N/A" : row[2].toString();
		                 tmRecords.append("<td>"+myVal1+"</td>");
		                 
		                 myVal1 = (row[3]==null)? "N/A" : row[3].toString();
		                 tmRecords.append("<td>"+myVal1+"</td>");
		                 
		                 myVal1 = (row[4]==null)? "N/A" : row[4].toString();
		                 if(myVal1.equals("A"))
		                	 myVal1 = "Active";
		                 else
		                	 myVal1 = "Inactive";

		                 tmRecords.append("<td>"+myVal1+"</td>");
		                 
		                 myVal1 = (row[5]==null)? "N/A" : row[5].toString();
		                 jobStartDate = df.parse(myVal1);
		                 tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jobStartDate)+"</td>");
		                 
		                 myVal1 = (row[6]==null)? "N/A" : row[6].toString();
		                 jobEndDate = df.parse(myVal1);
		                 tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jobEndDate)+"</td>");
		                 
		                 myVal1 = (row[7]==null)? "N/A" : row[7].toString();
		                 tmRecords.append("<td>"+myVal1+"</td>");
		                 
		                 tmRecords.append("</tr>");
		                    
					}
				}
				
				tmRecords.append("</table>");
				//tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			}
		}catch(Exception exception){
			exception.printStackTrace();
		}
		
		 return tmRecords.toString();
	}
	
	@Transactional(readOnly=false)
	public void updateNotReviewedFlag(JobForTeacher jobForTeacher)
	{
		System.out.println("::::::::::: updateNotReviewedFlag :::::::::::"); 
		Integer numberOfRecord = 0;
		try{
			numberOfRecord	=	jobForTeacherDAO.checkCandidatesNotReviewed(jobForTeacher.getDistrictId(), jobForTeacher.getJobId().getJobId(), jobForTeacher.getTeacherId().getTeacherId());
			/*if(jobForTeacher!=null){
				numberOfRecord	=	jobForTeacherDAO.checkCandidatesNotReviewed(jobForTeacher.getDistrictId(), jobForTeacher.getJobId().getJobId(), jobForTeacher.getTeacherId().getTeacherId());
				System.out.println("::::::::: numberOfRecord ::::::"+numberOfRecord);
				if(numberOfRecord>0) {
					// Set Flag as 1 for not Reviewed
					jobForTeacher.setNotReviewedFlag(true);
					jobForTeacherDAO.makePersistent(jobForTeacher);
				} else {
					// Set Flag as 0 for Reviewed
					jobForTeacher.setNotReviewedFlag(false);
					jobForTeacherDAO.makePersistent(jobForTeacher);
				}
			}*/

			List<JobForTeacher> jobForTeacherList = null;
			try{
				jobForTeacherList =  jobForTeacherDAO.getAllJobByGeoZone(jobForTeacher.getDistrictId(),jobForTeacher.getJobId().getJobTitle(),jobForTeacher.getTeacherId());
				System.out.println(jobForTeacherList+"::::::::: jobForTeacherList :::::::"+jobForTeacherList.size());
			} catch(Exception exception){
				exception.printStackTrace();
			}

			Boolean flag=false;
			if(numberOfRecord>0) flag=true;
			for(JobForTeacher allRecord:jobForTeacherList){
					allRecord.setNotReviewedFlag(flag);
					jobForTeacherDAO.getSessionFactory().openSession().update(allRecord);
					System.out.println(allRecord.getNotReviewedFlag());
			}
		}catch(Exception exception){
			exception.printStackTrace();
		}
	}
	
}

