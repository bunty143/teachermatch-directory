package tm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.DistrictAssessmentJobRelation;
import tm.bean.districtassessment.DistrictAssessmentQuestionOptions;
import tm.bean.districtassessment.DistrictAssessmentQuestions;
import tm.bean.districtassessment.DistrictAssessmentSection;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.ObjectiveMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.EpiQuestionDetailDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAssessmentAttemptDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.assessment.AssessmentLogDAO;
import tm.dao.assessment.AssessmentQuestionsDAO;
import tm.dao.assessment.AssessmentSectionDAO;
import tm.dao.assessment.QuestionOptionsDAO;
import tm.dao.assessment.QuestionsPoolDAO;
import tm.dao.districtassessment.DistrictAssessmentDetailDAO;
import tm.dao.districtassessment.DistrictAssessmentJobRelationDAO;
import tm.dao.districtassessment.DistrictAssessmentQuestionOptionsDAO;
import tm.dao.districtassessment.DistrictAssessmentQuestionsDAO;
import tm.dao.districtassessment.DistrictAssessmentSectionDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentStatusDAO;
import tm.dao.master.CompetencyMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.ObjectiveMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class DistrictAssessmentAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private DistrictAssessmentSectionDAO districtAssessmentSectionDAO;
	
	@Autowired
	private DistrictAssessmentDetailDAO districtAssessmentDetailDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	public void setAssessmentDetailDAO(AssessmentDetailDAO assessmentDetailDAO) {
		this.assessmentDetailDAO = assessmentDetailDAO;
	}
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(
			AssessmentJobRelationDAO assessmentJobRelationDAO) {
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}
	@Autowired
	private TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO;
	public void setTeacherAssessmentAttemptDAO(
			TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO) {
		this.teacherAssessmentAttemptDAO = teacherAssessmentAttemptDAO;
	}
	@Autowired
	private AssessmentSectionDAO assessmentSectionDAO;
	public void setAssessmentSectionDAO(AssessmentSectionDAO assessmentSectionDAO) {
		this.assessmentSectionDAO = assessmentSectionDAO;
	}
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) {
		this.domainMasterDAO = domainMasterDAO;
	}

	@Autowired
	private CompetencyMasterDAO competencyMasterDAO;
	public void setCompetencyMasterDAO(CompetencyMasterDAO competencyMasterDAO) {
		this.competencyMasterDAO = competencyMasterDAO;
	}

	@Autowired
	private ObjectiveMasterDAO objectiveMasterDAO;
	public void setObjectiveMasterDAO(ObjectiveMasterDAO objectiveMasterDAO) {
		this.objectiveMasterDAO = objectiveMasterDAO;
	}

	@Autowired
	private QuestionsPoolDAO questionsPoolDAO;
	public void setQuestionsPoolDAO(QuestionsPoolDAO questionsPoolDAO) {
		this.questionsPoolDAO = questionsPoolDAO;
	}

	@Autowired
	private QuestionOptionsDAO questionOptionsDAO;
	public void setQuestionOptionsDAO(QuestionOptionsDAO questionOptionsDAO) {
		this.questionOptionsDAO = questionOptionsDAO;
	}

	@Autowired
	private AssessmentQuestionsDAO assessmentQuestionsDAO;
	public void setAssessmentQuestionsDAO(
			AssessmentQuestionsDAO assessmentQuestionsDAO) {
		this.assessmentQuestionsDAO = assessmentQuestionsDAO;
	}
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	
	@Autowired
	private AssessmentLogDAO assessmentLogDAO;
	public void setAssessmentLogDAO(AssessmentLogDAO assessmentLogDAO) {
		this.assessmentLogDAO = assessmentLogDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	
	@Autowired
	private EpiQuestionDetailDAO epiQuestionDetailDAO;
	public void setEpiQuestionDetailDAO(EpiQuestionDetailDAO epiQuestionDetailDAO) {
		this.epiQuestionDetailDAO = epiQuestionDetailDAO;
	}
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private DistrictAssessmentJobRelationDAO districtAssessmentJobRelationDAO;
	
	@Autowired
	private DistrictAssessmentQuestionsDAO districtAssessmentQuestionsDAO;
	
	@Autowired
	private TeacherDistrictAssessmentStatusDAO teacherDistrictAssessmentStatusDAO; 
	
	@Autowired
	private DistrictAssessmentQuestionOptionsDAO districtAssessmentQuestionOptionsDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get assessment from database and to display on the grid.
	 */
	public String getAssessment(/*int assessmentType,String status,*/String districtId, String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		 
		StringBuffer sb =new StringBuffer();
		try{
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------
			

			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,20,"assessment.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}

			List<DistrictAssessmentDetail> assessmentDetails = null;
			
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"districtAssessmentName";
			String sortOrderNoField		=	"districtAssessmentName";
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			

			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("Responses")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("Responses")){
					 sortOrderNoField="Responses";
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			if(districtId==null)
				assessmentDetails=new ArrayList<DistrictAssessmentDetail>();
			else if(districtId.equals(""))
			{
				//assessmentDetails = assessmentDetailDAO.findByCriteria(Order.desc("createdDateTime"));
				assessmentDetails = districtAssessmentDetailDAO.findByCriteria(sortOrderStrVal);				
			
			}
			else 
			{
				Criterion criterion =null;
				if(!districtId.equals(""))
				criterion=Restrictions.eq("districtMaster", districtMasterDAO.findByDistrictId(districtId));
				if(/*assessmentType!=0 && !status.equals("") && */!districtId.equals(""))
				{
					//assessmentDetails = assessmentDetailDAO.findByCriteria(Order.desc("createdDateTime"),criterion,criterion1);
					assessmentDetails = districtAssessmentDetailDAO.findByCriteria(sortOrderStrVal,criterion);				
				
				}
			}

			List<DistrictAssessmentDetail> lstAssessmentDetail		=	new ArrayList<DistrictAssessmentDetail>();
			
			SortedMap<Integer,DistrictAssessmentDetail>	sortedMap = new TreeMap<Integer,DistrictAssessmentDetail>();
			if(sortOrderNoField.equals("Responses")){
				sortOrderFieldName="Responses";
			}

			//Map<Integer,Integer> assessmentAttempsMap = teacherAssessmentAttemptDAO.findAssessmentsResponses();
			Map<Integer,Integer> assessmentAttempsMap = teacherDistrictAssessmentStatusDAO.findDistrictAssessmentsResponses();
			int mapFlag=2;
			int sortingDynaVal=9999;
			for (DistrictAssessmentDetail assessmentDetail : assessmentDetails){
				int orderFieldInt=0;
				if(sortOrderFieldName.equals("Responses")){
					int noAttemptsorted=assessmentAttempsMap.get(assessmentDetail.getDistrictAssessmentId())==null?0:assessmentAttempsMap.get(assessmentDetail.getDistrictAssessmentId());
					sortingDynaVal--;
					orderFieldInt=Integer.parseInt(noAttemptsorted+""+sortingDynaVal);
					sortedMap.put(orderFieldInt,assessmentDetail);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					lstAssessmentDetail.add((DistrictAssessmentDetail) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					lstAssessmentDetail.add((DistrictAssessmentDetail) sortedMap.get(key));
				}
			}else{
				lstAssessmentDetail=assessmentDetails;
			}
			
			totalRecord =lstAssessmentDetail.size();

			if(totalRecord<end)
				end=totalRecord;
			List<DistrictAssessmentDetail> lstAssessmentDetails=lstAssessmentDetail.subList(start,end);
			
			sb.append("<table  id='tblGrid' width='100%' border='0'  >");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink( Utility.getLocaleValuePropByKey("lblNm", locale) ,sortOrderFieldName,"districtAssessmentName",sortOrderTypeVal,pgNo);
			sb.append("<th width='27%' valign='top'>"+responseText+"</th>");
		
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblcretedDate", locale), sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			sb.append("<th width='13%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblResponses1", locale) ,sortOrderFieldName,"Responses",sortOrderTypeVal,pgNo);
			sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblStatus", locale),sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			sb.append("<th width='4%%' valign='top'>"+responseText+"</th>");
			
			sb.append("<th width='36%'>"+Utility.getLocaleValuePropByKey("pdp2", locale)+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			
			int noAttempts = 0;
			for (DistrictAssessmentDetail assessmentDetail : lstAssessmentDetails) 
			{
				sb.append("<tr>" );
				sb.append("<td>"+assessmentDetail.getDistrictAssessmentName()+"</td>");
				sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(assessmentDetail.getCreatedDateTime())+"</td>");
				noAttempts=assessmentAttempsMap.get(assessmentDetail.getDistrictAssessmentId())==null?0:assessmentAttempsMap.get(assessmentDetail.getDistrictAssessmentId());
				sb.append("<td>"+noAttempts+"</td>");
				
				/*if(assessmentDetail.getAssessmentType()==1)
					sb.append("<td>Base</td>");
				else
					sb.append("<td>Job Specific</td>");*/
				sb.append("<td>");
				if(assessmentDetail.getStatus().equalsIgnoreCase("A"))
					sb.append(Utility.getLocaleValuePropByKey("lblActive", locale));
				else
					sb.append(Utility.getLocaleValuePropByKey("lblInactive", locale));
				sb.append("</td>");
				sb.append("<td>");
				boolean pipeFlag=false;
				if(roleAccess.indexOf("|2|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editAssessment("+assessmentDetail.getDistrictAssessmentId()+","+noAttempts+")'>"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a>");
					pipeFlag=true;
				}else if(roleAccess.indexOf("|4|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editAssessment("+assessmentDetail.getDistrictAssessmentId()+","+noAttempts+")'>"+Utility.getLocaleValuePropByKey("lnkV", locale)+"</a>");
					pipeFlag=true;
				}
				if(assessmentDetail.getStatus().equalsIgnoreCase("I"))
				{
					if(roleAccess.indexOf("|3|")!=-1){
						if(noAttempts==0){
							if(pipeFlag)sb.append(" | ");
							sb.append("<a href='javascript:void(0);' onclick='return deleteAssessment("+assessmentDetail.getDistrictAssessmentId()+")'>Delete</a>");
							pipeFlag=true;
						}
					}
				}
				if(roleAccess.indexOf("|7|")!=-1){
					if(pipeFlag)sb.append(" | ");
					if(assessmentDetail.getStatus().equalsIgnoreCase("A"))
						sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateAssessment("+assessmentDetail.getDistrictAssessmentId()+",'I')\">Deactivate</a>");
					else
						sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateAssessment("+assessmentDetail.getDistrictAssessmentId()+",'A')\">Activate</a>");
					pipeFlag=true;	
				}
				sb.append(" | <a href='districtassessmentsections.do?assessmentId="+assessmentDetail.getDistrictAssessmentId()+"' >"+Utility.getLocaleValuePropByKey("lblManageSections1", locale)+"</a>");
				sb.append("</td>");

			}
			if(assessmentDetails.size()==0)
				sb.append("<tr id='tr1' ><td id='tr1' colspan=7>"+Utility.getLocaleValuePropByKey("msgNoInventoryfound1", locale)+"</td></tr>" );

			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
				
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get a particular assessment from database.
	 */
	public DistrictAssessmentDetail getAssessmentById(int assessmentId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		 
		DistrictAssessmentDetail assessmentDetail =null;
		try{

			assessmentDetail=districtAssessmentDetailDAO.findById(assessmentId, false, false);

		}catch (Exception e) {
			e.printStackTrace();	
			return null;
		}
		return assessmentDetail;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to save a particular assessment to the database.
	 */
	//public int saveAssessment(AssessmentDetail assessmentDetail,String jobOrders )
	
	public String saveAssessment(DistrictAssessmentDetail assessmentDetail)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		String ipAddress = IPAddressUtility.getIpAddress(request);
		UserMaster user = null;

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");

		
		System.out.println(" assessmentDetail Name 1111111111 :: "+assessmentDetail.getDistrictAssessmentId());
		
		try
		{
			System.out.println(" assessmentDetail.getDistrictAssessmentId() :: "+assessmentDetail.getDistrictAssessmentId());
			
			if(assessmentDetail.getDistrictAssessmentId()!=null)
			{
				System.out.println("assessmentDetail.getAssessmentId(): "+assessmentDetail.getDistrictAssessmentId());
				DistrictAssessmentDetail assessmentDetail1=districtAssessmentDetailDAO.findById(assessmentDetail.getDistrictAssessmentId(), false, false);
				
				assessmentDetail.setUserMaster(assessmentDetail1.getUserMaster());
				assessmentDetail.setIpaddress(assessmentDetail1.getIpaddress());
				assessmentDetail.setStatus(assessmentDetail1.getStatus());
				assessmentDetail.setStatus(assessmentDetail1.getStatus());
				assessmentDetail.setCreatedDateTime(assessmentDetail1.getCreatedDateTime());
				
				districtAssessmentDetailDAO.makePersistent(assessmentDetail);
			}
			else
			{
			
				System.out.println(" assessmentDetail Name :: "+assessmentDetail.getDistrictAssessmentName());
			
				assessmentDetail.setUserMaster(user);
				assessmentDetail.setIpaddress(ipAddress);
				assessmentDetail.setStatus("A");
				assessmentDetail.setCreatedDateTime(new Date());
				districtAssessmentDetailDAO.makePersistent(assessmentDetail);
			}
		}
		catch (Exception e) {

			e.printStackTrace();
			//System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkk : "+e.getCause());
			if(e.getCause() instanceof com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException)
			{
				//return 3;
				return "3";
			}
			e.printStackTrace();
			//return 2;
			return "2";
		}

		//return 1;
		return "1";

	}


	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to delete a particular assessment from database.
	 */
	@Transactional(readOnly=false)
	public boolean deleteAssessment(DistrictAssessmentDetail assessmentDetail,String flag)
	{
		System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk >>>>>>>  districtAssessmentDetail ::"+assessmentDetail+" flag "+flag);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		UserMaster user = null;
	

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");
		
		try{
			assessmentDetail=districtAssessmentDetailDAO.findById(assessmentDetail.getDistrictAssessmentId(), false, false);
			List<DistrictAssessmentJobRelation> assessmentJobRelations = assessmentDetail.getDistrictAssessmentJobRelations();
			if(flag.equals("") || flag.equalsIgnoreCase("jobOrders"))
			{
				//System.out.println("assessmentJobRelations: "+assessmentJobRelations.size());
				if(assessmentJobRelations!=null)
				{
					for (DistrictAssessmentJobRelation assessmentJobRelation : assessmentJobRelations) 
					{
						districtAssessmentJobRelationDAO.makeTransient(assessmentJobRelation);
					}
				}
			}
			if(flag.equals(""))
			{
				List<DistrictAssessmentSection> assessmentSections = districtAssessmentSectionDAO.getSectionsByDistrictAssessmentId(assessmentDetail);
				//System.out.println("assessmentSections: "+assessmentSections.size());

				if(assessmentSections!=null)
				{
					for (DistrictAssessmentSection assessmentSection : assessmentSections) 
					{
						//System.out.println("LLLLLLLLLLLLLLLLLLLLLLL assessmentSection: "+assessmentSection);
						//assessmentSectionDAO.makeTransient(assessmentSection);
						//deleteAssessmentSection(assessmentSection.getSectionId());
						deleteDistrictAssessmentSection(assessmentSection);
					}
				}
			//	assessmentLogDAO.saveAssessmentLog(user, "Inventory Deleted", assessmentDetail.getAssessmentId()+" ---> "+assessmentDetail.getAssessmentName(), null, null, null);
			}
			
			districtAssessmentDetailDAO.clear();
			districtAssessmentDetailDAO.makeTransient(assessmentDetail);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to delete a particular assessment from database.
	 */
	@Transactional(readOnly=false)
	public String getSelectedJobs(AssessmentDetail assessmentDetail)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try{
			
			assessmentDetail=assessmentDetailDAO.findById(assessmentDetail.getAssessmentId(), false, false);
			List<AssessmentJobRelation> assessmentJobRelations = assessmentDetail.getAssessmentJobRelations();
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,20,"assessment.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("roleAccess="+roleAccess);
			if(assessmentJobRelations!=null)
			{
				StringBuffer sb =new StringBuffer();
								
				for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations) 
				{
					sb.append("<div class='col-sm-5 col-md-5'>");
					sb.append("<label><strong>");
					if(roleAccess.indexOf("|3|")!=-1){
						sb.append("<a href='javascript:void(0);' onclick='deleteJob("+assessmentJobRelation.getAssessmentJobRelationId()+","+assessmentJobRelation.getAssessmentId().getAssessmentId()+")' ><img width='15' height='15' class='can' src='images/can-icon.png' alt='' title='"+Utility.getLocaleValuePropByKey("lblRemove", locale)+"'></a>");
					}
					
					sb.append("&nbsp;"+assessmentJobRelation.getJobId().getJobTitle()+" ("+assessmentJobRelation.getJobId().getJobId()+")");
					sb.append("</label></strong></div>");
					
					sb.append("<input type='hidden' name='o_jobs' value='"+assessmentJobRelation.getJobId().getJobId()+"' />");
				}
				return sb.toString();
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
		return "0";
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to delete a particular Job from assessment.
	 */
	@Transactional(readOnly=false)
	public boolean deleteJob(AssessmentJobRelation assessmentJobRelation)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try{
			
			assessmentJobRelationDAO.makeTransient(assessmentJobRelation);
			
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to change a particular assessment status.
	 */
	@Transactional(readOnly=false)
	public DistrictAssessmentDetail activateDeactivateAssessment(int assessmentId,String status)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		UserMaster user = null;
		

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");
		
		List<DistrictAssessmentDetail> assessmentDetails = null;

		try{
			DistrictAssessmentDetail assessmentDetail=districtAssessmentDetailDAO.findById(assessmentId, false, false);

			/*
			// no active base checking  
			if(assessmentDetail.getAssessmentType()==1 && status.equalsIgnoreCase("A"))
			{
				assessmentDetails = assessmentDetailDAO.checkActiveBaseAssessment();
				if(assessmentDetails.size()>0)
					return assessmentDetails.get(0);
			}*/


			assessmentDetail.setStatus(status);
			districtAssessmentDetailDAO.makePersistent(assessmentDetail);
	//		String sts = status.equalsIgnoreCase("A")?"Activated":"Deactivated";
	//		assessmentLogDAO.saveAssessmentLog(user, "Inventory "+sts, assessmentDetail.getAssessmentId()+" ---> "+assessmentDetail.getAssessmentName(), null, null, null);

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	////////////////////////////////// Assessment Section /////////////////////////////////////////////////

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get assessment sections from database and to display on the grid.
	 */
	public String getAssessmentSections(int assessmentId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		System.out.println(" getAssessmentSections >>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
		
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		StringBuffer sb =new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord		= 	0;
			//------------------------------------
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,21,"assessmentsections.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			System.out.println("  >>>>>>>>> assessmentId <<<<<<<<<<<< ::::: "+assessmentId);
			
			DistrictAssessmentDetail assessmentDetail= districtAssessmentDetailDAO.findById(assessmentId, false, false);
			/*List<TeacherAssessmentAttempt> assessmentAttempts=teacherAssessmentAttemptDAO.findAssessmentResponses(assessmentDetail);
			int size=assessmentAttempts.size();*/
			List<DistrictAssessmentSection> assessmentSections = null;
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"sectionName";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			//domainMaster = domainmasterdao.findByCriteria(Order.asc("domainName"));
			System.out.println("===========  sortOrderStrVal ========="+sortOrderStrVal+" assessmentDetail "+assessmentDetail);
			Criterion criterion = Restrictions.eq("districtAssessmentDetail", assessmentDetail);
			totaRecord = districtAssessmentSectionDAO	.getRowCountWithSort(sortOrderStrVal,criterion);
			assessmentSections = districtAssessmentSectionDAO.findWithLimit(sortOrderStrVal,0,100,criterion);
			//assessmentSections = assessmentSectionDAO.findByCriteria(Order.desc("createdDateTime"),criterion);

			sb.append("<table border='0' class='table table-bordered mt30' id='tblGrid'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			//sb.append("<th width='18%'>Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblNm", locale),sortOrderFieldName,"sectionName",sortOrderTypeVal,pgNo);
			sb.append("<th width='18%' valign='top'>"+responseText+"</th>");
			
			//sb.append("<th width='12%'>Created Date</th>");
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblcretedDate", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			sb.append("<th width='12%' valign='top'>"+responseText+"</th>");
			
			//sb.append("<th width='23%'>Description</th>");
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDescription1", locale),sortOrderFieldName,"sectionDescription",sortOrderTypeVal,pgNo);
			sb.append("<th width='23%' valign='top'>"+responseText+"</th>");
			
			//sb.append("<th width='23%'>Instructions</th>");
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblInst", locale),sortOrderFieldName,"sectionInstructions",sortOrderTypeVal,pgNo);
			sb.append("<th width='23%' valign='top'>"+responseText+"</th>");
			
			sb.append("<th width='24%'>"+Utility.getLocaleValuePropByKey("pdp2", locale)+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			
			System.out.println(" assessmentSections >>>>>>>>>>>>>>>>>>>>>>>>>>>"+assessmentSections.size());
			
			for (DistrictAssessmentSection assessmentSection : assessmentSections) 
			{
				sb.append("<tr>");
				sb.append("<td>"+assessmentSection.getSectionName()+"</td>");
				sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(assessmentSection.getCreatedDateTime())+"</td>");
				sb.append("<td>"+assessmentSection.getSectionDescription()+"</td>");
				sb.append("<td>"+assessmentSection.getSectionInstructions()+"</td>");
				sb.append("<td>");
					
				boolean pipeFlag=false;
				if(roleAccess.indexOf("|2|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editAssessmentSection("+assessmentSection.getDistrictSectionId()+")'>"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a>");
					pipeFlag=true;
				}else if(roleAccess.indexOf("|4|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editAssessmentSection("+assessmentSection.getDistrictSectionId()+")'>"+Utility.getLocaleValuePropByKey("lnkV", locale)+"</a>");
					pipeFlag=true;
				}
					/*if(size==0)
					{
						if(roleAccess.indexOf("|3|")!=-1){
							if(pipeFlag)sb.append(" | ");
							sb.append("<a href='javascript:void(0);' onclick='return deleteAssessmentSection("+assessmentSection.getSectionId()+")'>Delete</a>");
							pipeFlag=true;
						}
					}*/
				if(pipeFlag)sb.append(" | ");
				sb.append("<a href='districtassessmentquestions.do?assessmentId="+assessmentDetail.getDistrictAssessmentId()+"&sectionId="+assessmentSection.getDistrictSectionId()+"' >Manage Questions</a>");
				/*if(pipeFlag)sb.append(" | ");
				sb.append("<a href='importquestion.do?assessmentId="+assessmentDetail.getDistrictAssessmentId()+"&sectionId="+assessmentSection.getDistrictSectionId()+"' >Import Questions</a>");
				*///sb.append("<a href='javascript:void(0);' onclick=\"importQuestion()\" >Import Questions</a>");
				/*if(assessmentSection.get!=null && !assessmentSection.getSectionVideoUrl().equals(""))
				sb.append(" | <a href='javascript:void(0);' onclick=\"displayVideo('"+assessmentSection.getSectionVideoUrl()+"')\" >Play</a>");*/
				sb.append("</td>");
			}
			if(assessmentSections.size()==0)
				sb.append("<tr id='tr1' ><td id='tr1' colspan=5>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );

			sb.append("</table>");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/* 
	 * @Discription: It is used to save  a particular assessment section to the database.
	 */
	public int saveAssessmentSection(DistrictAssessmentSection assessmentSection,int assessmentId )
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		String ipAddress = IPAddressUtility.getIpAddress(request);
		UserMaster user = null;

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");

		//Integer assSec = assessmentSection.getSectionId();
		
		assessmentSection.setUserMaster(user);
		assessmentSection.setIpaddress(ipAddress);
		assessmentSection.setStatus("A");
		if(assessmentSection.getDistrictSectionId()==null)
		{
			assessmentSection.setCreatedDateTime(new Date());
		}
		DistrictAssessmentDetail assessmentDetail = districtAssessmentDetailDAO.findById(assessmentId, false, false);	
		assessmentSection.setDistrictAssessmentDetail(assessmentDetail);

		try{
			/*List<AssessmentSections> assessmentSections=assessmentSectionDAO.checkDuplicateAssessmentSection(assessmentDetail,assessmentSection.getSectionName(),assessmentSection.getSectionId());
			int size = assessmentSections.size();
			if(size>0)
				return 3;*/
			districtAssessmentSectionDAO.makePersistent(assessmentSection);
			//assessmentSectionDAO.makePersistent(assessmentSection);
		
		}catch (Exception e) {
			e.printStackTrace();
			return 2;
		}

		return 1;

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get assessment section from database.
	 */
	public DistrictAssessmentSection getAssessmentSectionById(int assessmentId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		DistrictAssessmentSection assessmentSection =null;
		try{

			assessmentSection=districtAssessmentSectionDAO.findById(assessmentId, false, false);

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return assessmentSection;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to delete assessment section from database.
	 */
	@Transactional(readOnly=false)
	public boolean deleteDistrictAssessmentSection(DistrictAssessmentSection assessmentSection)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		UserMaster user = null;
		

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");
		
		try{
			List<DistrictAssessmentQuestions> assessmentQuestions = districtAssessmentQuestionsDAO.getSectionQuestions(assessmentSection);
			assessmentQuestionsDAO.clear();
			if(assessmentQuestions!=null)
			{
				for (DistrictAssessmentQuestions assessmentQuestions2 : assessmentQuestions) {
					districtAssessmentQuestionsDAO.makeTransient(assessmentQuestions2);
				}
			}
			assessmentSection = districtAssessmentSectionDAO.findById(assessmentSection.getDistrictSectionId(), false, false);
			districtAssessmentSectionDAO.makeTransient(assessmentSection);
			
		///// Log Saving /////////
			
			
		  DistrictAssessmentDetail	assessmentDetail = assessmentSection.getDistrictAssessmentDetail();
		 /* assessmentLogDAO.saveAssessmentLog(user, "Section Deleted", 
					assessmentDetail.getAssessmentId()+" ---> "+assessmentDetail.getAssessmentName(),
					assessmentSection.getSectionId()+" ---> "+assessmentSection.getSectionName(), null, null);
*/
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Competencies  from database.
	 */
	public List<CompetencyMaster> getCompetencies(int domainId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		List<CompetencyMaster> competencyMasters = null;

		try{
			DomainMaster domainMaster = domainMasterDAO.findById(domainId, false, false);
			competencyMasters = competencyMasterDAO.getCompetencesByDomain(domainMaster);
			CompetencyMaster competencyMaster= new CompetencyMaster();
			competencyMaster.setCompetencyName(Utility.getLocaleValuePropByKey("lblSelectCompetency1", locale));
			competencyMaster.setCompetencyId(0);
			competencyMasters.add(0,competencyMaster);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return competencyMasters;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Objectives  from database.
	 */
	public List<ObjectiveMaster> getObjectives(int competencyId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		List<ObjectiveMaster> objectiveMasters =null;
		try {
			CompetencyMaster competencyMaster = competencyMasterDAO.findById(competencyId, false, false);
			objectiveMasters = objectiveMasterDAO.getObjectivesByCompetency(competencyMaster);
			ObjectiveMaster objectiveMaster = new ObjectiveMaster();
			objectiveMaster.setObjectiveName(Utility.getLocaleValuePropByKey("msgSelectObjective", locale));
			objectiveMaster.setObjectiveId(0);
			objectiveMasters.add(0,objectiveMaster);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return objectiveMasters;
	}
	/* 
	 * @Discription: It is used to save Assessment Section Question.
	 */
	@Transactional(readOnly=false)
	public DistrictAssessmentQuestions saveAssessmentSectionQuestion(DistrictAssessmentDetail assessmentDetail,DistrictAssessmentSection districtAssessmentSection,String jsonObject,DistrictAssessmentQuestionOptions[] districtAssessmentQuestionOptions,DistrictAssessmentQuestions assessmentQuestion)
	{
		System.out.println(">>>>>>>>>>> Call ><?>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		String ipAddress = IPAddressUtility.getIpAddress(request);
		UserMaster user = null;

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");
		
		districtAssessmentSection = districtAssessmentSectionDAO.findById(districtAssessmentSection.getDistrictSectionId(), false, false);
		assessmentDetail = districtAssessmentSection.getDistrictAssessmentDetail();
		
		if (assessmentQuestion.getDistrictAssessmentQuestionId()==null)
		{
			assessmentQuestion.setCreatedDateTime(new Date());		
		}

		try{
			if(assessmentQuestion.getQuestionWeightage()==null)
				assessmentQuestion.setQuestionWeightage(0.00); 
			assessmentQuestion.setUserMaster(user);
			assessmentQuestion.setStatus("A");

			DistrictAssessmentQuestions districtAssessmentQuestionsNew = null;
			List<DistrictAssessmentQuestionOptions> qopt = null;
			if(assessmentQuestion.getDistrictAssessmentQuestionId()!=null)
			{
				districtAssessmentQuestionsNew =districtAssessmentQuestionsDAO.findById(assessmentQuestion.getDistrictAssessmentQuestionId(), false, false);
				qopt = districtAssessmentQuestionsNew.getDistrictAssessmentQuestionOptions();
			}
			
			Map<Integer,DistrictAssessmentQuestionOptions> map = new HashMap<Integer,DistrictAssessmentQuestionOptions>();
			if(qopt!=null)
				for (DistrictAssessmentQuestionOptions districtQuestionOptions2 : qopt) {
					map.put(districtQuestionOptions2.getOptionId(), districtQuestionOptions2);
				}

			DistrictAssessmentQuestions districtAssessmentQuestions =assessmentQuestion;
			districtAssessmentQuestions.setDistrictAssessmentDetail(assessmentDetail);
			districtAssessmentQuestions.setDistrictAssessmentSection(districtAssessmentSection);
			if(districtAssessmentQuestions.getDistrictAssessmentQuestionId()==null)
				districtAssessmentQuestions.setCreatedDateTime(new Date());

			districtAssessmentQuestions.setUserMaster(user);
			districtAssessmentQuestions.setStatus("A");

			districtAssessmentQuestionsDAO.clear();
			districtAssessmentQuestionsDAO.makePersistent(districtAssessmentQuestions);
		
			for (DistrictAssessmentQuestionOptions districtAssessmentQuestionOption : districtAssessmentQuestionOptions) {
				districtAssessmentQuestionOption.setDistrictAssessmentQuestions(assessmentQuestion);
				districtAssessmentQuestionOption.setCreatedDateTime(new Date());
				districtAssessmentQuestionOption.setUserMaster(user);
				districtAssessmentQuestionOption.setIpaddress(ipAddress);
				districtAssessmentQuestionOption.setStatus("A");
				districtAssessmentQuestionOptionsDAO.makePersistent(districtAssessmentQuestionOption);
				// OptionList 
				if(qopt!=null)
					map.remove(districtAssessmentQuestionOption.getOptionId());
				districtAssessmentQuestionOption=null;

			}
			if(qopt!=null)
				for(Integer aKey : map.keySet()) {
					districtAssessmentQuestionOptionsDAO.makeTransient(map.get(aKey));
				}

		}catch (Exception e) {
			e.printStackTrace();
		}

		return assessmentQuestion;
	}
	
	
	/* 
	 * @Discription: It is used to get Assessment Section Questions.
	 */
	public String getAssessmentSectionQuestions(DistrictAssessmentDetail districtassessmentDetail,DistrictAssessmentSection districtassessmentSections/*,DomainMaster domainMaster,CompetencyMaster competencyMaster,ObjectiveMaster objectiveMaster*/)
	{
		System.out.println("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		StringBuffer sb =new StringBuffer();
		try{
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			//String roleAccess=null;
			
			List<DistrictAssessmentQuestions> assessmentQuestions =null;
			Criterion criterion = Restrictions.eq("districtAssessmentDetail", districtassessmentDetail);
			Criterion criterion1 = Restrictions.eq("districtAssessmentSection", districtassessmentSections);

			DistrictAssessmentQuestions assessmentQuestion1 = new DistrictAssessmentQuestions();
			assessmentQuestion1.setDistrictAssessmentDetail(districtassessmentDetail);
			assessmentQuestion1.setDistrictAssessmentSection(districtassessmentSections);
			//assessmentQuestion1.setAssessmentSections(assessmentSections);
			/*if(domainMaster!=null && domainMaster.getDomainId()!=null && domainMaster.getDomainId()!=0)
				assessmentQuestions = assessmentQuestionsDAO.filterQuestions(assessmentQuestion1,domainMaster,competencyMaster,objectiveMaster);
			else*/
				assessmentQuestions = districtAssessmentQuestionsDAO.findByCriteria(criterion,criterion1);
			
			Integer assessmentType = 2;
			
			/*if(assessmentQuestions.size()>0)
				assessmentType = assessmentQuestions.get(0).getDistrictAssessmentDetail().getAssessmentType();*/
			
			int i=0;
			String uId = "";
			List<DistrictAssessmentQuestionOptions> questionOptionsList = null;
			String questionInstruction = null;
			String shortName = "";
			//Integer questionTaken = 0;
		//	QuestionsPool questionsPool = null;
			/*StageOneStatus stageOneStatus = null;
			StageTwoStatus stageTwoStatus = null;
			StageThreeStatus stageThreeStatus = null;*/
			Double questionWeightage = 0.0;
			for (DistrictAssessmentQuestions assessmentQuestion : assessmentQuestions) 
			{
				//questionsPool = assessmentQuestion.getQuestionsPool();
				shortName=assessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName();
				//shortName = questionsPool.getQuestionTypeMaster().getQuestionTypeShortName();
				questionWeightage = assessmentQuestion.getQuestionWeightage();
				
				sb.append("<tr>");
				sb.append("<td width='81%'>");
				sb.append("Question "+(++i)+": "+assessmentQuestion.getQuestion()+"<br/>");

				questionOptionsList = assessmentQuestion.getDistrictAssessmentQuestionOptions();
				if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel") || shortName.equalsIgnoreCase("lkts"))
				{
					sb.append("<table class='top12'>");
					for (DistrictAssessmentQuestionOptions questionOptions : questionOptionsList) {
						sb.append("<tr ><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' style='margin:0px;' name='opt"+i+"' /></td><td style='border:0px;background-color: transparent;padding: 2px 3px;'>"+questionOptions.getQuestionOption()+"</td></tr>");
					}
					sb.append("</table>");
				}else if(shortName.equalsIgnoreCase("rt"))
				{
			
					sb.append("<table class='top12'>");
					for (DistrictAssessmentQuestionOptions questionOptions : questionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' name='opt"+i+"' class='span1' maxlength='3'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+questionOptions.getQuestionOption()+"</td></tr>");
					}
					sb.append("</table>");
				}
				// Image Type
				if(shortName.equalsIgnoreCase("it"))
				{
					sb.append("<table >");
					for (DistrictAssessmentQuestionOptions questionOptions : questionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='opt"+i+"' /></td><td style='border:0px;background-color: transparent;padding: 2px 3px;vertical-align:middle;'><img src='showImage?image=ques_images/"+questionOptions.getQuestionOption()+"'></td></tr>");
					
					}
					sb.append("</table>");
				}else if(shortName.equalsIgnoreCase("sl"))
				{
					sb.append("<div class='col-sm-10 col-md-10'>");
					sb.append("&nbsp;&nbsp;<input type='text' name='opt"+i+"' class='form-control' maxlength='75'/><br/>");
					sb.append("</div>");
				}
				else if(shortName.equalsIgnoreCase("ml"))
				{
					sb.append("<div class='col-sm-10 col-md-10'>");
					sb.append("&nbsp;&nbsp;<textarea name='opt"+i+"' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\"/></textarea><br/>");
					sb.append("</div>");
				}if(shortName.equalsIgnoreCase("itq"))
				{
					sb.append("<table class='top12'>");
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'></td><td style='border:0px;background-color: transparent;padding: 2px 3px;vertical-align:middle;'><img src='showImage?image=ques_images/"+assessmentQuestion.getQuestionImage()+"'></td></tr>");
					sb.append("</table>");
					
					sb.append("<table class='top12'>");
					for (DistrictAssessmentQuestionOptions questionOptions : questionOptionsList) {
						sb.append("<tr ><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' style='margin:0px;' name='opt"+i+"' /></td><td style='border:0px;background-color: transparent;padding: 2px 3px;'>"+questionOptions.getQuestionOption()+"</td></tr>");
					}
					sb.append("</table>");
				}
				
				
				questionInstruction = assessmentQuestion.getQuestionInstruction()==null?"":assessmentQuestion.getQuestionInstruction();
				if(!questionInstruction.equals(""))
				{
					sb.append("<div class='col-sm-10 col-md-10' style='margin-left:-15px;margin-bottom:-20px;'>");
					sb.append("Instructions: </br>"+questionInstruction);
				}
				else
				{
					sb.append("<div class='col-sm-10 col-md-10' style='margin-left:-15px'>");
					//sb.append("<br/>");
				}
				
				sb.append("</div>");				
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append("<div style=''>");
				
					sb.append("<a href='districtassessmentsectionquestions.do?sectionId="+assessmentQuestion.getDistrictAssessmentSection().getDistrictSectionId()+"&assessmentQuestionId="+assessmentQuestion.getDistrictAssessmentQuestionId()+"' >"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a>");
					
					sb.append(" | ");
					sb.append("<a href='javascript:void(0)' onclick='deleteAssessmentQuestion("+assessmentQuestion.getDistrictAssessmentQuestionId()+")'>"+Utility.getLocaleValuePropByKey("msgRemovefromAssessment", locale)+"</a>");
				sb.append("</div>");
				sb.append("</td>");

			}
			if(assessmentQuestions.size()==0)

				sb.append("<tr id='tr1' ><td id='tr1' colspan=2>"+Utility.getLocaleValuePropByKey("lblNoQuestionfound1", locale)+"</td></tr>" );
	}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/* 
	 * @Discription: It is used to remove assessment question from database.
	 */
	@Transactional(readOnly=false)
	public boolean deleteAssessmentQuestion(Integer districtassessmentQuestionId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}
		DistrictAssessmentQuestions districtassessmentQuestions=null;
		try{
			districtassessmentQuestions = districtAssessmentQuestionsDAO.findById(districtassessmentQuestionId,false, false);
			List<DistrictAssessmentQuestionOptions> listAssessmentQuestionOptions=districtassessmentQuestions.getDistrictAssessmentQuestionOptions();
			if(listAssessmentQuestionOptions!=null && listAssessmentQuestionOptions.size()>0){
				Map<Integer,DistrictAssessmentQuestionOptions> map = new HashMap<Integer,DistrictAssessmentQuestionOptions>();
				for (DistrictAssessmentQuestionOptions districtQuestionOptions2 : listAssessmentQuestionOptions) {
					map.put(districtQuestionOptions2.getOptionId(), districtQuestionOptions2);
				}
				for(Integer aKey : map.keySet()) {
					districtAssessmentQuestionOptionsDAO.makeTransient(map.get(aKey));
				}
			}
			
			districtAssessmentQuestionsDAO.makeTransient(districtassessmentQuestions);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/* 
	 * @Discription: It is used to get a particular Assessment Question.
	 */
	@Transactional(readOnly=false)
	public DistrictAssessmentQuestions getAssessmentQuestionById(DistrictAssessmentQuestions assessmentQuestion)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try{
			assessmentQuestion = districtAssessmentQuestionsDAO.findById(assessmentQuestion.getDistrictAssessmentQuestionId(), false, false);

		}catch (Exception e) {
			e.printStackTrace();
		}

		return assessmentQuestion;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Job Orders.
	 */
	@Transactional(readOnly=false)
	public List<JobOrder> getJobOrders(AssessmentDetail assessmentDetail,DistrictMaster districtMaster,SchoolMaster schoolMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		Map<Integer,JobOrder> map = null;
		List<JobOrder> jobOrders = null;
		System.out.println("schoolMaster llllllllllllllllllllllllllll : "+schoolMaster.getSchoolId());
		
		if(schoolMaster.getSchoolId()!=null && schoolMaster.getSchoolId()>0)
			schoolMaster = schoolMasterDAO.findById(schoolMaster.getSchoolId(), false, false);
		else
			schoolMaster = null;
		
		if(assessmentDetail.getAssessmentId()!=0)
		{
			assessmentDetail= assessmentDetailDAO.findById(assessmentDetail.getAssessmentId(), false, false);
			if(assessmentDetail.getAssessmentType()==2)
			{
				List<AssessmentJobRelation> assessmentJobRelations = assessmentDetail.getAssessmentJobRelations();
				//System.out.println("PPPPPPPPPPPPPPPPPPPPPPPPPPP assessmentJobRelations "+assessmentJobRelations.size());

				map = new HashMap<Integer, JobOrder>();
				for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations) {
					map.put(assessmentJobRelation.getJobId().getJobId(),assessmentJobRelation.getJobId());
				}
			}
		}
		
		jobOrders = jobOrderDAO.findValidJobOrders(map,districtMaster,schoolMaster);
		/*JobOrder jobOrder = new JobOrder();
		jobOrder.setJobId(0);
		jobOrder.setJobTitle("");
		jobOrders.add(jobOrder);*/
		return jobOrders;
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Job Orders District.
	 */
	@Transactional(readOnly=false)
	public List<DistrictMaster> getJobOrderDistricts(int createdForEntity)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		List<DistrictMaster> districtMasters = null;
		districtMasters = jobOrderDAO.getDistrictsByJobOrders(createdForEntity);
		if(districtMasters!=null)
		{	DistrictMaster districtMaster = new DistrictMaster();
			districtMaster.setDistrictId(0);
			districtMaster.setDistrictName(Utility.getLocaleValuePropByKey("msgSelectDistrict", locale));
			districtMasters.add(0, districtMaster);
		}
		//System.out.println("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL districtMaster : "+districtMasters.size());
		return districtMasters;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Job Orders School.
	 */
	@Transactional(readOnly=false)
	public List<SchoolMaster> getJobOrderSchools(DistrictMaster districtMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		List<SchoolMaster> schoolMasters = null;
		schoolMasters = jobOrderDAO.getSchoolsByDistrictInJobOrders(districtMaster);
		
		if(schoolMasters!=null)
		{	SchoolMaster schoolMaster = new SchoolMaster();
			schoolMaster.setSchoolId(new Long(0));
			schoolMaster.setSchoolName(Utility.getLocaleValuePropByKey("msgStSchool", locale));
			schoolMasters.add(0, schoolMaster);
		}
		//System.out.println("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL schoolMasters : "+schoolMasters.size());
		return schoolMasters;
	}
	
	/*================ Display Vedio ==============*/
	@Transactional(readOnly=false)
	public String displayVideo(String urlFormat)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		return Utility.getYouTubeCode(urlFormat,350,250);
	}
	
	@Transactional(readOnly=true)
	public String displayJobCategoryByDistrictForJSI(Integer districtID,String txtjobCategory)
	{
		StringBuffer buffer;
		try {
			buffer = new StringBuffer();
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			Integer jobCategory=0;
			if(txtjobCategory!=null && !txtjobCategory.equals(""))
				jobCategory=Integer.parseInt(txtjobCategory);

			if(districtID>0)
			{
				//DistrictMaster districtMaster=districtMasterDAO.findById(districtID, false, false);
				DistrictMaster districtMaster=new DistrictMaster();
				districtMaster.setDistrictId(districtID);
				List<JobCategoryMaster> lstjobCategoryMasters=jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
				System.out.println(lstjobCategoryMasters.size());
				if(lstjobCategoryMasters.size()>0)
				{
					buffer.append("<select class='span5' name='jobCategory' id='jobCategory' >");
					buffer.append("<option selected='selected' value=''>"+Utility.getLocaleValuePropByKey("optStrJobCat", locale)+"</option>");
					for(JobCategoryMaster pojo:lstjobCategoryMasters)
						if(pojo!=null)
						{
							if(pojo.getJobCategoryId().equals(jobCategory))
								buffer.append("<option value='"+pojo.getJobCategoryId()+"' selected='selected'>"+pojo.getJobCategoryName()+"</option>");
							else
								buffer.append("<option value='"+pojo.getJobCategoryId()+"'>"+pojo.getJobCategoryName()+"</option>");
						}
					buffer.append("</select>");
				}else
				{
					buffer.append("<select class='span5' name='jobCategory' id='jobCategory' disabled='disabled' onchange='resetJobCategory();'>");
					buffer.append("<option selected='selected' value=''>"+Utility.getLocaleValuePropByKey("optStrJobCat", locale)+"</option>");
					buffer.append("</select>");
				}
			}
			else
			{
				buffer.append("<select class='span5' name='jobCategory' id='jobCategory' disabled='disabled' onchange='resetJobCategory();'>");
				buffer.append("<option selected='selected' value=''>"+Utility.getLocaleValuePropByKey("optStrJobCat", locale)+"</option>");
				buffer.append("</select>");
			}
			return buffer.toString();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		//return buffer.toString();
	}
	
	//Auto Complete of District Assessment
	/*public List<DistrictAssessmentDetail> getFieldOfQuesList(String quesSetName,String districtId)
	{
		 ========  For Session time Out Error =========
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictAssessmentDetail> districtMasterList =  new ArrayList<DistrictAssessmentDetail>();
		List<DistrictAssessmentDetail> fieldOfDistrictList1 = null;
		List<DistrictAssessmentDetail> fieldOfDistrictList2 = null;
		
		List DistrictAssessmentDetail = null;
		List<DistrictAssessmentDetail> quesSetInQSQ = new ArrayList<DistrictAssessmentDetail>();
		List<DistrictAssessmentDetail> finslList = new ArrayList<DistrictAssessmentDetail>();
		
		
		boolean flag_1=false;
		boolean flag_2=false;
		
		try{
			
			if(districtId!=null && !districtId.equals(""))
			{
				DistrictMaster districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
				Criterion crDistrict = Restrictions.eq("districtMaster",districtMaster);
				
				
				Criterion criterion = Restrictions.like("districtAssessmentName", quesSetName.trim(),MatchMode.START);
				if(quesSetName.trim()!=null && quesSetName.trim().length()>0){
					fieldOfDistrictList1 = districtAssessmentDetailDAO.findWithLimit(Order.asc("districtAssessmentName"), 0, 25, criterion,crDistrict);
					
					Criterion criterion2 = Restrictions.ilike("districtAssessmentName","% "+quesSetName.trim()+"%" );
					fieldOfDistrictList2 = districtAssessmentDetailDAO.findWithLimit(Order.asc("districtAssessmentName"), 0, 25, criterion2,crDistrict);
					
					districtMasterList.addAll(fieldOfDistrictList1);
					districtMasterList.addAll(fieldOfDistrictList2);
					Set<DistrictAssessmentDetail> setDistrict = new LinkedHashSet<DistrictAssessmentDetail>(districtMasterList);
					districtMasterList = new ArrayList<DistrictAssessmentDetail>(new LinkedHashSet<DistrictAssessmentDetail>(setDistrict));
					
				}else{
					fieldOfDistrictList1 = districtAssessmentDetailDAO.findWithLimit(Order.asc("districtAssessmentName"), 0, 25,crDistrict);
					districtMasterList.addAll(fieldOfDistrictList1);
				}
			}
			
			if(districtMasterList!=null && districtMasterList.size()>0)
				flag_1=true;

			
			
			ProjectionList projList = Projections.projectionList();
		    projList.add(Projections.groupProperty("questionSets"));
			
			i4QuestionSetQuestions = districtAssessmentDetailDAO.findByCriteriaProjection(projList);
			
			for(Object IQSQ:DistrictAssessmentDetail)
			{
				DistrictAssessmentDetail i4QObj =  (DistrictAssessmentDetail)IQSQ;
				quesSetInQSQ.add(i4QObj);
			}
			
			if(quesSetInQSQ.size()>0)
				flag_2=true;
			
			if(flag_1)
				finslList.addAll(districtMasterList);
			
			if(flag_2)
				finslList.retainAll(quesSetInQSQ);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	
		return finslList;
	}*/
	
	
	public List<DistrictAssessmentDetail> getFieldOfDistASMT(String DistASMTName,Integer districtId)
	{
		System.out.println(">>>>>>>>>>>>>>> getFieldOfDistASMT <<<<<<<<<<<<<<<<<<<<");
		
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictAssessmentDetail> districtMasterList =  new ArrayList<DistrictAssessmentDetail>();
		List<DistrictAssessmentDetail> fieldOfDistrictList1 = null;
		List<DistrictAssessmentDetail> fieldOfDistrictList2 = null;
		try{
			if(districtId!=null && districtId>0)
			{
				DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
				
				if(districtMaster!=null)
				{
					Criterion criterion = Restrictions.like("districtAssessmentName", DistASMTName.trim(),MatchMode.START);
					Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
					
					if(DistASMTName.trim()!=null && DistASMTName.trim().length()>0){
						fieldOfDistrictList1 = districtAssessmentDetailDAO.findWithLimit(Order.asc("districtAssessmentName"), 0, 25, criterion,criterion1);
						
						Criterion criterion2 = Restrictions.ilike("districtAssessmentName","% "+DistASMTName.trim()+"%" );
						fieldOfDistrictList2 = districtAssessmentDetailDAO.findWithLimit(Order.asc("districtAssessmentName"), 0, 25, criterion2,criterion1);
						
						districtMasterList.addAll(fieldOfDistrictList1);
						districtMasterList.addAll(fieldOfDistrictList2);
						Set<DistrictAssessmentDetail> setDistrict = new LinkedHashSet<DistrictAssessmentDetail>(districtMasterList);
						districtMasterList = new ArrayList<DistrictAssessmentDetail>(new LinkedHashSet<DistrictAssessmentDetail>(setDistrict));
						
					}else{
						fieldOfDistrictList1 = districtAssessmentDetailDAO.findWithLimit(Order.asc("districtAssessmentName"), 0, 25,criterion1);
						districtMasterList.addAll(fieldOfDistrictList1);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return districtMasterList;
	}
}


