package tm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4QuestionSets;
import tm.bean.master.DistrictMaster;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.ReferenceQuestionSets;
import tm.bean.user.UserMaster;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.QqQuestionSetsDAO;
import tm.dao.master.ReferenceQuestionSetsDAO;
import tm.utility.Utility;

public class DistrictSpecificQuestionsSetAjax {

	String locale = Utility.getValueOfPropByKey("locale");

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private QqQuestionSetsDAO qqQuestionSetsDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	// DIsplay Questions from question pool
	public String displayQuestionSet(String noOfRow, String pageNo,String sortOrder,String sortOrderType,String i4QuesSetStatus,String quesSetSearchText,String districtIdFilter,String headQuarterId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			boolean Statusflag 	= false;
			boolean districtFlag 	= false;
			boolean allData	   	= false;
			boolean quesTextflag= false;
			boolean hqFlag 	= false;
			DistrictMaster districtMaster = new DistrictMaster();
			HeadQuarterMaster headQuarterMaster = null;
			
			
			List<QqQuestionSets> qqQuestionSets  	=	new ArrayList<QqQuestionSets>();
			List<QqQuestionSets> i4QuesForStatus	=	new ArrayList<QqQuestionSets>();
			List<QqQuestionSets> i4QuesForQues		=	new ArrayList<QqQuestionSets>();
			List<QqQuestionSets> filterData			=	new ArrayList<QqQuestionSets>();
			List<QqQuestionSets> finalDatalist		=	new ArrayList<QqQuestionSets>();
			List<QqQuestionSets> filterByDistrict	=	new ArrayList<QqQuestionSets>();
			
			
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"QuestionSetText";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			
			qqQuestionSets = qqQuestionSetsDAO.findByCriteria(sortOrderStrVal) ;
			
			if(qqQuestionSets.size()>0)
			{
				filterData.addAll(qqQuestionSets);
				allData = true;
			}
			
			if(quesSetSearchText!=null && !quesSetSearchText.equalsIgnoreCase(""))
			{
				i4QuesForQues = qqQuestionSetsDAO.findi4QuesSetByQues(quesSetSearchText);
				quesTextflag = true;
			}
			
			if(quesTextflag==true)
				filterData.retainAll(i4QuesForQues);
			else
				filterData.addAll(i4QuesForQues);
			
			if(i4QuesSetStatus!=null && !i4QuesSetStatus.equalsIgnoreCase("") && !i4QuesSetStatus.equalsIgnoreCase("0"))
			{
				i4QuesForStatus = qqQuestionSetsDAO.findi4QuesSetByStatus(i4QuesSetStatus);
				Statusflag = true;
			}
			
			if(districtIdFilter!=null && !districtIdFilter.equalsIgnoreCase("") && !districtIdFilter.equalsIgnoreCase("0"))
			{
				districtMaster = districtMasterDAO.findById(Integer.parseInt(districtIdFilter), false, false);	
				filterByDistrict = qqQuestionSetsDAO.findByDistrict(districtMaster);
				session.setAttribute("districtMasterId", districtMaster.getDistrictId());
				session.setAttribute("districtMasterName", districtMaster.getDistrictName());
				
				districtFlag = true;
			}else if(headQuarterId!=null && !headQuarterId.equalsIgnoreCase("") && !headQuarterId.equalsIgnoreCase("0")){
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);	
				Criterion c = Restrictions.eq("headQuarterMaster", headQuarterMaster);
				Criterion c1= Restrictions.eq("Status", "A");
				filterByDistrict = qqQuestionSetsDAO.findByCriteria(c,c1);
				hqFlag = true;				
			}
			if(districtIdFilter==null || districtIdFilter.equalsIgnoreCase("") || districtIdFilter.equalsIgnoreCase("0")){
				session.setAttribute("districtMasterId", 0);
				session.setAttribute("districtMasterName", "");
			}
			
			
			
			
			if(districtFlag || hqFlag)
				filterData.retainAll(filterByDistrict);
			else
				filterData.addAll(filterByDistrict);
			
			if(Statusflag==true)
				filterData.retainAll(i4QuesForStatus);
			else
				filterData.addAll(i4QuesForStatus);
				
			
			totalRecord = filterData.size();
			if(totalRecord<end)
				end=totalRecord;
			
			System.out.println(" start :: "+start+" end "+end);
			
			finalDatalist	=	filterData.subList(start,end);
			
			dmRecords.append("<table  id='refChkQuesTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			//dmRecords.append("<th width='55%'>Domain Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("Question Set",sortOrderFieldName,"QuestionSetText",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='55%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Created Date",sortOrderFieldName,"DateCreated",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Status",sortOrderFieldName,"Status",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'><span style='color:#ffffff'>"+responseText+"</span></th>");
			
			dmRecords.append("<th width='15%'><span style='color:#ffffff'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</span></th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(finalDatalist.size()==0)
				dmRecords.append("<tr><td colspan='6'>No Question Set found</td></tr>" );
			System.out.println("No of Records "+finalDatalist.size());

			for (QqQuestionSets i4qp: finalDatalist) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+i4qp.getQuestionSetText()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(i4qp.getDateCreated())+"</td>");
				dmRecords.append("<td>");
				if(i4qp.getStatus().equalsIgnoreCase("A"))
					dmRecords.append("Active");
				else
					dmRecords.append("Inactive");
				dmRecords.append("</td>");
				if(userMaster.getEntityType()!=6){
					if(i4qp.getStatus().equalsIgnoreCase("A")){
						if(i4qp.getDistrictMaster()!=null)
							dmRecords.append("<td><a href='javascript:void(0);' onclick='editI4Question("+i4qp.getID()+")'>"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a> | <a href='javascript:void(0);' onclick=\"activateDeactivateQues("+i4qp.getID()+",'I')\">Deactivate</a> | <a href='assessmentQuestionSet.do?quesSetId="+i4qp.getID()+"&&districtId="+i4qp.getDistrictMaster().getDistrictId()+"'>"+Utility.getLocaleValuePropByKey("lblManageQuestions", locale)+"</a></td>");
						else
							dmRecords.append("<td><a href='javascript:void(0);' onclick='editI4Question("+i4qp.getID()+")'>"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a> | <a href='javascript:void(0);' onclick=\"activateDeactivateQues("+i4qp.getID()+",'I')\">Deactivate</a> | <a href='assessmentQuestionSet.do?quesSetId="+i4qp.getID()+"&&headQuarterId="+i4qp.getHeadQuarterMaster().getHeadQuarterId()+"'>"+Utility.getLocaleValuePropByKey("lblManageQuestions", locale)+"</a></td>");
					}
					else if(i4qp.getStatus().equalsIgnoreCase("I")){
						if(i4qp.getDistrictMaster()!=null)
							dmRecords.append("<td><a href='javascript:void(0);' onclick='editI4Question("+i4qp.getID()+")'>"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a> | <a href='javascript:void(0);' onclick=\"activateDeactivateQues("+i4qp.getID()+",'A')\">Activate</a> | <a href='assessmentQuestionSet.do?quesSetId="+i4qp.getID()+"&&districtId="+i4qp.getDistrictMaster().getDistrictId()+"'>"+Utility.getLocaleValuePropByKey("lblManageQuestions", locale)+"</a></td>");
						else
							dmRecords.append("<td><a href='javascript:void(0);' onclick='editI4Question("+i4qp.getID()+")'>"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a> | <a href='javascript:void(0);' onclick=\"activateDeactivateQues("+i4qp.getID()+",'A')\">Activate</a> | <a href='assessmentQuestionSet.do?quesSetId="+i4qp.getID()+"&&headQuarterId="+i4qp.getHeadQuarterMaster().getHeadQuarterId()+"'>"+Utility.getLocaleValuePropByKey("lblManageQuestions", locale)+"</a></td>");
					}
				}else{
					if(i4qp.getDistrictMaster()!=null){
						dmRecords.append("<td><a href='javascript:void(0);' onclick='editI4Question("+i4qp.getID()+")'>View</a> | <a href='assessmentQuestionSet.do?quesSetId="+i4qp.getID()+"&&districtId="+i4qp.getDistrictMaster().getDistrictId()+"'>"+Utility.getLocaleValuePropByKey("lblManageQuestions", locale)+"</a></td>");
					}else{
						dmRecords.append("<td><a href='javascript:void(0);' onclick='editI4Question("+i4qp.getID()+")'>View</a> | <a href='assessmentQuestionSet.do?quesSetId="+i4qp.getID()+"&&headQuarterId="+i4qp.getHeadQuarterMaster().getHeadQuarterId()+"'>"+Utility.getLocaleValuePropByKey("lblManageQuestions", locale)+"</a></td>");
					}
				}
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	public int saveRefChkQuestionSet(String headQuarterId,Integer quesSetId,String quesSetName,String quesSetStatus,String districtId)
	{
		System.out.println(" ==========saveI4Question ========");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		int res = 0;
		boolean chkDup = false;
		
		try
		{
			UserMaster userMaster = new UserMaster();
			
			if (session.getAttribute("userMaster") != null) 
			{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
			}
			
			DistrictMaster districtMaster = null;
			HeadQuarterMaster headQuarterMaster = null;
			
			if (session.getAttribute("userMaster") != null) 
				userMaster=	(UserMaster) session.getAttribute("userMaster");
			
			if(userMaster.getEntityType()==1)
			{
				if(districtId!=null && !districtId.equals(""))
					districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			}
			else if(userMaster.getEntityType()==2)
			{
				districtMaster = userMaster.getDistrictId();
			}
			
			if(headQuarterId!=null && !headQuarterId.equals("") && !headQuarterId.equals("0")){
				headQuarterMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);
			}
			
			if(quesSetId==null){
				if(checkDuplicateQuesSet(quesSetName, districtMaster))
					chkDup = true;
			}
			
			
			if(chkDup==false)
			{
				QqQuestionSets qqQuestionSets = new QqQuestionSets();
				Date date = new Date();
				
				qqQuestionSets.setQuestionSetText(quesSetName);
				qqQuestionSets.setStatus(quesSetStatus);
				qqQuestionSets.setDistrictMaster(districtMaster);
				qqQuestionSets.setHeadQuarterMaster(headQuarterMaster);
				qqQuestionSets.setDateCreated(date);
				
				if(quesSetId!=null)
				{
					qqQuestionSets.setID(quesSetId);
				}
				qqQuestionSetsDAO.makePersistent(qqQuestionSets);
			}
			else
			{
				System.out.println(" Question set is duplicate.");
				res = 1;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return res;
	}
	
	public QqQuestionSets editRefChkQuestionSet(String quesId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		QqQuestionSets qqQuestionSets = null;
		
		try
		{
			qqQuestionSets = qqQuestionSetsDAO.findById(Integer.parseInt(quesId), false, false);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return qqQuestionSets;
	}
	
	public boolean activateDeactivateQuestionSet(String quesSetId,String status)
	{
		/* ========  For Session time Out Error =========*/
				WebContext context;
				context = WebContextFactory.get();
				HttpServletRequest request = context.getHttpServletRequest();
				HttpSession session = request.getSession(false);
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				{
					throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			    }
				
				try{
					QqQuestionSets qqQuestionSets = qqQuestionSetsDAO.findById(Integer.parseInt(quesSetId), false, false);
					qqQuestionSets.setStatus(status);
					qqQuestionSetsDAO.makePersistent(qqQuestionSets);
					
				}catch (Exception e) 
				{
					e.printStackTrace();
					return false;
				}
				return true;
	}	
	
	public boolean checkDuplicateQuesSet(String quesTxt,DistrictMaster districtMaster)
	{
		boolean chkDup = false;
		List<QqQuestionSets> existQuesList = new ArrayList<QqQuestionSets>();
		
		try
		{
			existQuesList = qqQuestionSetsDAO.findExistQuestionSet(quesTxt,districtMaster);
			
			if(existQuesList!=null && existQuesList.size()>0)
				chkDup = true;
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return chkDup;
	}
	

}
