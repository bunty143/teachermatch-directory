package tm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentGroupDetails;
import tm.bean.assessment.AssessmentStepMaster;
import tm.bean.assessment.AssessmentTemplateMaster;
import tm.bean.assessment.AssessmentWiseStepMessage;
import tm.bean.assessment.TemplateWiseStepMessage;
import tm.bean.user.UserMaster;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentGroupDetailsDAO;
import tm.dao.assessment.AssessmentStepMasterDAO;
import tm.dao.assessment.AssessmentTemplateMasterDAO;
import tm.dao.assessment.AssessmentWiseStepMessageDAO;
import tm.dao.assessment.TemplateWiseStepMessageDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.utility.Utility;

public class AssessmentMessageAjax {
	
	String locale = Utility.getValueOfPropByKey("locale");
	String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
    String lblNm=Utility.getLocaleValuePropByKey("lblNm", locale);
    String lblGroupName1=Utility.getLocaleValuePropByKey("lblGroupName1", locale);
    String lblTpe=Utility.getLocaleValuePropByKey("lblTpe", locale);
    String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
    String lnkV=Utility.getLocaleValuePropByKey("lnkV", locale);
    String lblDeactivate=Utility.getLocaleValuePropByKey("lblDeactivate", locale);
    String lblActivate=Utility.getLocaleValuePropByKey("lblActivate", locale);
    String lblB=Utility.getLocaleValuePropByKey("lblB", locale);
    String optJSpe=Utility.getLocaleValuePropByKey("optJSpe", locale);
    String lblSmartP=Utility.getLocaleValuePropByKey("lblSmartP", locale);
    String lblIPI=Utility.getLocaleValuePropByKey("lblIPI", locale);
    String optAct=Utility.getLocaleValuePropByKey("optAct", locale);
    String optInActiv=Utility.getLocaleValuePropByKey("optInActiv", locale);
    
    @Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
    
    @Autowired
	private AssessmentTemplateMasterDAO assessmentTemplateMasterDAO;
    
    @Autowired
	private TemplateWiseStepMessageDAO templateWiseStepMessageDAO;
    
    @Autowired
    private AssessmentStepMasterDAO assessmentStepMasterDAO;
    
    @Autowired
    private AssessmentGroupDetailsDAO assessmentGroupDetailsDAO;
    
    @Autowired
    private AssessmentDetailDAO assessmentDetailDAO;
    
    @Autowired
    private AssessmentWiseStepMessageDAO assessmentwisestepmessageDAO;
    
    public String getTemplate(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
    	System.out.println("::::::::::::::::::::::::::::::: getTemplate :::::::::::::::::::::::::::::::");
    	WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }

		StringBuffer sb =new StringBuffer();
		
		try{
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;

			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,20,"assessment.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}

			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"templateName";
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			List<AssessmentTemplateMaster> assessmentTemplateMasterList1 = new ArrayList<AssessmentTemplateMaster>();
			assessmentTemplateMasterList1 = assessmentTemplateMasterDAO.findByCriteria(sortOrderStrVal);
			totalRecord = assessmentTemplateMasterList1.size();

			if(totalRecord<end)
				end=totalRecord;
			List<AssessmentTemplateMaster> assessmentTemplateMasterList = assessmentTemplateMasterList1.subList(start,end);
			
			sb.append("<table  id='templateTable' width='100%' border='0'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblNm,sortOrderFieldName,"templateName",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblTpe,sortOrderFieldName,"assessmentType",sortOrderTypeVal,pgNo);
			sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			sb.append("<th width='4%' valign='top'>"+responseText+"</th>");
			
			sb.append("<th width='33%'>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			
			for (AssessmentTemplateMaster assessmentTemplateMaster : assessmentTemplateMasterList) 
			{
				sb.append("<tr>" );
				sb.append("<td>"+assessmentTemplateMaster.getTemplateName()+"</td>");
				if(assessmentTemplateMaster.getAssessmentType()==1) {
					sb.append("<td>"+lblB+"</td>");
				} else if(assessmentTemplateMaster.getAssessmentType()==2) {
					sb.append("<td>"+optJSpe+"</td>");
				} else if(assessmentTemplateMaster.getAssessmentType()==3) {
					sb.append("<td>"+lblSmartP+"</td>");
				} else if(assessmentTemplateMaster.getAssessmentType()==4) {
					sb.append("<td>"+lblIPI+"</td>");
				}
				sb.append("<td>");
				if(assessmentTemplateMaster.getStatus().equalsIgnoreCase("A"))
					sb.append(optAct);
				else
					sb.append(optInActiv);
				sb.append("</td>");
				sb.append("<td>");
				boolean pipeFlag=false;
				if(roleAccess.indexOf("|2|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editTemplate("+assessmentTemplateMaster.getAssessmentTemplateId()+")'>"+lblEdit+"</a>");
					pipeFlag=true;
				}else if(roleAccess.indexOf("|4|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editTemplate("+assessmentTemplateMaster.getAssessmentTemplateId()+")'>"+lnkV+"</a>");
					pipeFlag=true;
				}
				if(roleAccess.indexOf("|7|")!=-1){
					if(pipeFlag)sb.append(" | ");
					if(assessmentTemplateMaster.getStatus().equalsIgnoreCase("A"))
						sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateTemplate("+assessmentTemplateMaster.getAssessmentTemplateId()+",'I')\">"+lblDeactivate+"</a>");
					else
						sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateTemplate("+assessmentTemplateMaster.getAssessmentTemplateId()+",'A')\">"+lblActivate+"</a>");
					pipeFlag=true;
				}
				sb.append("</td>");

			}
			if(assessmentTemplateMasterList.size()==0)
				sb.append("<tr id='tr1' ><td id='tr1' colspan=7>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );

			sb.append("</table>");
//			sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo, "pageSize1", "780"));
				
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
    
    public String saveTemplate(AssessmentTemplateMaster assessmentTemplateMaster)
	{
    	System.out.println("::::::::::::::::::::::::::::::: saveTemplate :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		else
			userMaster = (UserMaster)session.getAttribute("userMaster");
		
		String ipAddress = request.getRemoteAddr();
		try{
			List<AssessmentTemplateMaster> assessmentTemplateMasterList = new ArrayList<AssessmentTemplateMaster>();
			if(assessmentTemplateMaster!=null && assessmentTemplateMaster.getAssessmentTemplateId()==null && assessmentTemplateMaster.getAssessmentType()!=null && assessmentTemplateMaster.getIsDefault()!=null && assessmentTemplateMaster.getIsDefault()){
				assessmentTemplateMasterList = assessmentTemplateMasterDAO.findByCriteria(Restrictions.eq("assessmentType", assessmentTemplateMaster.getAssessmentType()), Restrictions.eq("isDefault", assessmentTemplateMaster.getIsDefault()));
			}
			if(assessmentTemplateMasterList.size()>0){
				return assessmentTemplateMasterList.get(0).getTemplateName();
			}
			else{
				assessmentTemplateMaster.setAssessmentType(assessmentTemplateMaster.getAssessmentType());
				if(assessmentTemplateMaster!=null && assessmentTemplateMaster.getAssessmentTemplateId()!=null){
					AssessmentTemplateMaster assessmentTemplateMaster2 = assessmentTemplateMasterDAO.findById(assessmentTemplateMaster.getAssessmentTemplateId(), false, false);
					assessmentTemplateMaster.setStatus(assessmentTemplateMaster2.getStatus());
					assessmentTemplateMaster.setUserMaster1(assessmentTemplateMaster2.getUserMaster1());
					assessmentTemplateMaster.setCreatedDateTime(assessmentTemplateMaster2.getCreatedDateTime());
					assessmentTemplateMaster.setUserMaster2(userMaster);
					assessmentTemplateMaster.setUpdatedDateTime(new Date());
				}
				else{
					assessmentTemplateMaster.setStatus("I");
					assessmentTemplateMaster.setUserMaster1(userMaster);
					assessmentTemplateMaster.setCreatedDateTime(new Date());
				}
				assessmentTemplateMaster.setIpaddress(ipAddress);
				assessmentTemplateMasterDAO.makePersistent(assessmentTemplateMaster);
			}
		}
		catch (Exception e) {
//			e.printStackTrace();
			if(e.getCause() instanceof com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException)
			{
				return "3";
			}
			e.printStackTrace();
			return "2";
		}
		return "1";
	}
    
    public AssessmentTemplateMaster getTemplateById(int assessmentTemplateId)
	{
    	System.out.println("::::::::::::::::::::::::::::::: getTemplateById :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		 
		AssessmentTemplateMaster assessmentTemplateMaster = null;
		try{
			assessmentTemplateMaster = assessmentTemplateMasterDAO.findById(assessmentTemplateId, false, false);
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return assessmentTemplateMaster;
	}
    
    @Transactional(readOnly=false)
	public AssessmentTemplateMaster activateDeactivateTemplate(int assessmentTemplateId,String status)
	{
    	System.out.println("::::::::::::::::::::::::::::::: activateDeactivateTemplate :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		else
			userMaster = (UserMaster)session.getAttribute("userMaster");
		
		try{
			AssessmentTemplateMaster assessmentTemplateMaster = assessmentTemplateMasterDAO.findById(assessmentTemplateId, false, false);
			assessmentTemplateMaster.setStatus(status);
			assessmentTemplateMaster.setUserMaster2(userMaster);
			assessmentTemplateMaster.setUpdatedDateTime(new Date());
			assessmentTemplateMasterDAO.makePersistent(assessmentTemplateMaster);
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
    
    public String getMessage(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
    	System.out.println("::::::::::::::::::::::::::::::: getMessage :::::::::::::::::::::::::::::::");
    	WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }

		StringBuffer sb =new StringBuffer();
		
		try{
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;

			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,20,"assessment.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}

			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"templateWiseStepMessageId";
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			List<TemplateWiseStepMessage> templateWiseStepMessageList1 = new ArrayList<TemplateWiseStepMessage>();
			templateWiseStepMessageList1 = templateWiseStepMessageDAO.findByCriteria(sortOrderStrVal);
			totalRecord = templateWiseStepMessageList1.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TemplateWiseStepMessage> templateWiseStepMessageList = templateWiseStepMessageList1.subList(start,end);
			
			sb.append("<table  id='messageTable' width='100%' border='0'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			String responseText="";
			
//			responseText=PaginationAndSorting.responseSortingLink(lblTpe,sortOrderFieldName,"stepMessage",sortOrderTypeVal,pgNo);
			responseText=lblTpe;
			sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
			
//			responseText=PaginationAndSorting.responseSortingLink("Template Name",sortOrderFieldName,"stepMessage",sortOrderTypeVal,pgNo);
			responseText="Template Name";
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
//			responseText=PaginationAndSorting.responseSortingLink("Step",sortOrderFieldName,"stepMessage",sortOrderTypeVal,pgNo);
			responseText="Step";
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
//			responseText=PaginationAndSorting.responseSortingLink("Message",sortOrderFieldName,"stepMessage",sortOrderTypeVal,pgNo);
			responseText="Message";
			sb.append("<th width='26%' valign='top'>"+responseText+"</th>");
			
//			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"stepMessage",sortOrderTypeVal,pgNo);
			responseText=lblStatus;
			sb.append("<th width='4%' valign='top'>"+responseText+"</th>");
			
			sb.append("<th width='20%'>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			
			for (TemplateWiseStepMessage templateWiseStepMessage : templateWiseStepMessageList) 
			{
				sb.append("<tr>" );
				if(templateWiseStepMessage.getAssessmentTemplateMaster().getAssessmentType()==1) {
					sb.append("<td>"+lblB+"</td>");
				} else if(templateWiseStepMessage.getAssessmentTemplateMaster().getAssessmentType()==2) {
					sb.append("<td>"+optJSpe+"</td>");
				} else if(templateWiseStepMessage.getAssessmentTemplateMaster().getAssessmentType()==3) {
					sb.append("<td>"+lblSmartP+"</td>");
				} else if(templateWiseStepMessage.getAssessmentTemplateMaster().getAssessmentType()==4) {
					sb.append("<td>"+lblIPI+"</td>");
				}
				sb.append("<td>"+templateWiseStepMessage.getAssessmentTemplateMaster().getTemplateName()+"</td>");
				sb.append("<td>"+templateWiseStepMessage.getAssessmentStepMaster().getStepName()+"</td>");
				sb.append("<td>"+templateWiseStepMessage.getStepMessage()+"</td>");
				sb.append("<td>");
				if(templateWiseStepMessage.getStatus().equalsIgnoreCase("A"))
					sb.append(optAct);
				else
					sb.append(optInActiv);
				sb.append("</td>");
				sb.append("<td>");
				boolean pipeFlag=false;
				if(roleAccess.indexOf("|2|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editMessage("+templateWiseStepMessage.getTemplateWiseStepMessageId()+")'>"+lblEdit+"</a>");
					pipeFlag=true;
				}else if(roleAccess.indexOf("|4|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editTemplate("+templateWiseStepMessage.getTemplateWiseStepMessageId()+")'>"+lnkV+"</a>");
					pipeFlag=true;
				}
				if(roleAccess.indexOf("|7|")!=-1){                
					if(pipeFlag)sb.append(" | ");
					if(templateWiseStepMessage.getStatus().equalsIgnoreCase("A"))
						sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateMessage("+templateWiseStepMessage.getTemplateWiseStepMessageId()+",'I')\">"+lblDeactivate+"</a>");
					else
						sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateMessage("+templateWiseStepMessage.getTemplateWiseStepMessageId()+",'A')\">"+lblActivate+"</a>");
					pipeFlag=true;	
				}
				sb.append("</td>");

			}
			if(templateWiseStepMessageList.size()==0)
				sb.append("<tr id='tr1' ><td id='tr1' colspan=7>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );

			sb.append("</table>");
//			sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo, "pageSize2", "780"));
				
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
    
	public String saveMessage(TemplateWiseStepMessage templateWiseStepMessage)
	{
    	System.out.println("::::::::::::::::::::::::::::::: saveMessage :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		else
			userMaster = (UserMaster)session.getAttribute("userMaster");
		
		String ipAddress = request.getRemoteAddr();
		try{
			templateWiseStepMessage.setAssessmentTemplateMaster(templateWiseStepMessage.getAssessmentTemplateMaster());
			templateWiseStepMessage.setAssessmentStepMaster(templateWiseStepMessage.getAssessmentStepMaster());
			templateWiseStepMessage.setStepMessage(templateWiseStepMessage.getStepMessage());
			templateWiseStepMessage.setStatus("A");
			if(templateWiseStepMessage!=null && templateWiseStepMessage.getTemplateWiseStepMessageId()!=null){
				TemplateWiseStepMessage templateWiseStepMessage2 = templateWiseStepMessageDAO.findById(templateWiseStepMessage.getTemplateWiseStepMessageId(), false, false);
				templateWiseStepMessage.setStatus(templateWiseStepMessage2.getStatus());
				templateWiseStepMessage.setUserMaster1(templateWiseStepMessage2.getUserMaster1());
				templateWiseStepMessage.setCreatedDateTime(templateWiseStepMessage2.getCreatedDateTime());
				templateWiseStepMessage.setUserMaster2(userMaster);
				templateWiseStepMessage.setUpdatedDateTime(new Date());
			}
			else{
				templateWiseStepMessage.setUserMaster1(userMaster);
				templateWiseStepMessage.setCreatedDateTime(new Date());
			}
			templateWiseStepMessage.setIpaddress(ipAddress);
			templateWiseStepMessageDAO.makePersistent(templateWiseStepMessage);
		}
		catch (Exception e) {
			e.printStackTrace();
			return "2";
		}
		return "1";
	}
    
    public TemplateWiseStepMessage getMessageById(int templateWiseStepMessageId)
	{
    	System.out.println("::::::::::::::::::::::::::::::: getMessageById :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		 
		TemplateWiseStepMessage templateWiseStepMessage = null;
		try{
			templateWiseStepMessage = templateWiseStepMessageDAO.findById(templateWiseStepMessageId, false, false);
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return templateWiseStepMessage;
	}
    
    @Transactional(readOnly=false)
	public TemplateWiseStepMessage activateDeactivateMessage(int templateWiseStepMessageId,String status)
	{
    	System.out.println("::::::::::::::::::::::::::::::: activateDeactivateTemplate :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		else
			userMaster = (UserMaster)session.getAttribute("userMaster");
		
		try{
			TemplateWiseStepMessage templateWiseStepMessage = templateWiseStepMessageDAO.findById(templateWiseStepMessageId, false, false);
			templateWiseStepMessage.setStatus(status);
			templateWiseStepMessage.setUserMaster2(userMaster);
			templateWiseStepMessage.setUpdatedDateTime(new Date());
			templateWiseStepMessageDAO.makePersistent(templateWiseStepMessage);
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
    
    public List<AssessmentTemplateMaster> getTemplatesByInventoryType(int assessmentType)
	{
    	System.out.println("::::::::::::::::::::::::::::::: getTemplatesByInventoryType :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		
		List<AssessmentTemplateMaster> assessmentTemplateMasterList = new ArrayList<AssessmentTemplateMaster>();
		assessmentTemplateMasterList = assessmentTemplateMasterDAO.findByCriteria(Restrictions.eq("assessmentType", assessmentType), Restrictions.eq("status", "A"));
		System.out.println("assessmentTemplateMasterList.size() : "+assessmentTemplateMasterList.size());
		return assessmentTemplateMasterList;
	}
    
    public String getSteps(String assessmentTemplateID)
	{
    	System.out.println("::::::::::::::::::::::::::::::: getSteps :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		
		StringBuffer sb = new StringBuffer("");
		sb.append("<option value='0'>Select Step</option>");
		List<AssessmentStepMaster> assessmentStepMasterList = new ArrayList<AssessmentStepMaster>();
		List<String> allStepList = new ArrayList<String>();
		List<String> tempAssessmentStepMasterList = new ArrayList<String>();
		List<TemplateWiseStepMessage> setpsToRemove = new ArrayList<TemplateWiseStepMessage>();// list of those steps which already used according to inventory type
		
		if(assessmentTemplateID!=null && !assessmentTemplateID.equals("") && !assessmentTemplateID.equals("0")){
			setpsToRemove=templateWiseStepMessageDAO.findByCriteria(Restrictions.eq("assessmentTemplateMaster.assessmentTemplateId", Integer.parseInt(assessmentTemplateID)),Restrictions.eq("status", "A"));
			if(setpsToRemove!=null && setpsToRemove.size()>0){
				for(TemplateWiseStepMessage setpsTo:setpsToRemove){
					tempAssessmentStepMasterList.add(setpsTo.getAssessmentStepMaster().getAssessmentStepId()+"##"+setpsTo.getAssessmentStepMaster().getStepName());
				}
			}
			assessmentStepMasterList = assessmentStepMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
			if(assessmentStepMasterList!=null && assessmentStepMasterList.size()>0){
				for(AssessmentStepMaster assmaster : assessmentStepMasterList){
					allStepList.add(assmaster.getAssessmentStepId()+"##"+assmaster.getStepName());
				}
			}
			allStepList.removeAll(tempAssessmentStepMasterList);
			System.out.println("allStepMap.size() : "+allStepList.size());
			if(allStepList!=null && allStepList.size()>0){
				Iterator<String> iterator=allStepList.iterator();
				while(iterator.hasNext()){
					String values[]=iterator.next().split("##");
					sb.append("<option value='"+values[0]+"'>"+values[1]+"</option>");
				}
			}
		}
		return sb.toString();
	}
    
    public List<AssessmentDetail> getAssessmentByGroup(int assessmentGroupId)
	{
    	System.out.println("::::::::::::::::::::::::::::::: getAssessmentByGroup :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		
		AssessmentGroupDetails assessmentGroupDetails = assessmentGroupDetailsDAO.findById(assessmentGroupId, false, false);
		List<AssessmentDetail> assessmentDetailList = new ArrayList<AssessmentDetail>();
		assessmentDetailList = assessmentDetailDAO.getAssessmentDetailListByGroup(assessmentGroupDetails);
		System.out.println(":::::::::::::::::::: assessmentDetailList.size(): "+assessmentDetailList.size());
		return assessmentDetailList;
	}
    
    public String getStepByAssessmentId(int assessmentTemplateId, int assessmentId)
	{
    	System.out.println("::::::::::::::::::::::::::::::: getStepByAssessmentId :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		
		StringBuffer sb = new StringBuffer("");
		sb.append("<option value='0'>Select Step</option>");
		List<Object[]> stepList = null;
		try{
			if(assessmentId!=0)
				stepList = assessmentStepMasterDAO.getStepByAssTempAndAss(assessmentTemplateId, assessmentId);
			if(stepList!=null && stepList.size()>0){
				for(Object[] object : stepList){
					sb.append("<option value='"+object[0]+"' msg='"+object[2]+"'>"+object[1]+"</option>");
				}
				System.out.println("stepList.size() : "+stepList.size());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
    
    public String saveAssessmentMessage(String assessmentwisestepmessageID, String assessmentTypeAmID,String assessmentGroupDetailsID,String assessmentDetailID,String defaultt,String assessmentTemplateMasterAmID,String assessmentStepMasterAmID,String stepMessageAm)
    {
		System.out.println("::::::::::::::::::::::::::::::: saveAssessmentMessage :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		else
			userMaster = (UserMaster)session.getAttribute("userMaster");
		
		try{
			List<AssessmentWiseStepMessage> assessmentWiseStepMessageList1 = null;
			AssessmentDetail assessmentDetail = new AssessmentDetail();
			AssessmentTemplateMaster assessmentTemplateMaster = new AssessmentTemplateMaster();
			AssessmentStepMaster assessmentStepMaster = new AssessmentStepMaster();
			assessmentDetail.setAssessmentId(Integer.parseInt(assessmentDetailID));
//			if(!defaultt.equals("1")){
				assessmentWiseStepMessageList1 = assessmentwisestepmessageDAO.findByCriteria(Restrictions.eq("assessmentDetail", assessmentDetail), Restrictions.eq("useDefault", true), Restrictions.eq("status", "A"));
				if(assessmentWiseStepMessageList1!=null && assessmentWiseStepMessageList1.size()>0){
					if(defaultt.equals("1"))
						return assessmentWiseStepMessageList1.get(0).getAssessmentDetail().getAssessmentName()+"#"+"true1";
					else
						return assessmentWiseStepMessageList1.get(0).getAssessmentDetail().getAssessmentName()+"#"+"true2";
				}
//			}
			List<AssessmentWiseStepMessage> assessmentWiseStepMessageList2 = null;
			assessmentWiseStepMessageList2 = assessmentwisestepmessageDAO.findByCriteria(Restrictions.eq("assessmentDetail", assessmentDetail), Restrictions.eq("useDefault", false), Restrictions.eq("status", "A"));
			if(assessmentWiseStepMessageList2!=null && assessmentWiseStepMessageList2.size()>0){
				if(defaultt.equals("1"))
					return assessmentWiseStepMessageList2.get(0).getAssessmentDetail().getAssessmentName()+"#"+"false1";
//				else
//					return assessmentWiseStepMessageList2.get(0).getAssessmentDetail().getAssessmentName()+"#"+"false2";
			}
			AssessmentWiseStepMessage assessmentwisestepmessage = null;
			if(assessmentwisestepmessageID.equals("") || assessmentwisestepmessageID==null || assessmentwisestepmessageID.equals("0")){
				assessmentwisestepmessage = new AssessmentWiseStepMessage();
				assessmentwisestepmessage.setAssessmentDetail(assessmentDetail);
				assessmentwisestepmessage.setAssessmentType(Integer.parseInt(assessmentTypeAmID));
				if(defaultt.equals("1")){
					assessmentwisestepmessage.setUseDefault(true);
				}
				else{
					assessmentwisestepmessage.setUseDefault(false);
					assessmentTemplateMaster.setAssessmentTemplateId(Integer.parseInt(assessmentTemplateMasterAmID));
					assessmentStepMaster.setAssessmentStepId(Integer.parseInt(assessmentStepMasterAmID));
					assessmentwisestepmessage.setAssessmentTemplateMaster(assessmentTemplateMaster);
					assessmentwisestepmessage.setAssessmentStepMaster(assessmentStepMaster);
					assessmentwisestepmessage.setStepMessage(stepMessageAm);
				}
				
				assessmentwisestepmessage.setStatus("A");
				assessmentwisestepmessage.setUserMaster(userMaster);
				assessmentwisestepmessage.setCreatedDateTime(new Date());
				assessmentwisestepmessage.setIpaddress(request.getLocalAddr());
			}
			else
			{
				assessmentwisestepmessage = assessmentwisestepmessageDAO.findById(Integer.parseInt(assessmentwisestepmessageID), false, false);
				if(defaultt.equals("1"))
					assessmentwisestepmessage.setUseDefault(true);
				else
					assessmentwisestepmessage.setUseDefault(false);
				if(!assessmentTemplateMasterAmID.equals("") || assessmentTemplateMasterAmID!=null || !assessmentTemplateMasterAmID.equals("0")){
					assessmentTemplateMaster.setAssessmentTemplateId(Integer.parseInt(assessmentTemplateMasterAmID));
					assessmentwisestepmessage.setAssessmentTemplateMaster(assessmentTemplateMaster);
				}
				if(!assessmentStepMasterAmID.equals("") || assessmentStepMasterAmID!=null || !assessmentStepMasterAmID.equals("0")){
					assessmentStepMaster.setAssessmentStepId(Integer.parseInt(assessmentStepMasterAmID));
					assessmentwisestepmessage.setAssessmentStepMaster(assessmentStepMaster);
				}
				assessmentwisestepmessage.setStepMessage(stepMessageAm);
				assessmentwisestepmessage.setUserMaster2(userMaster);
				assessmentwisestepmessage.setUpdatedDateTime(new Date());
			}
			assessmentwisestepmessageDAO.makePersistent(assessmentwisestepmessage);
		} catch (Exception e) {
			e.printStackTrace();
			return "2";
		}
		return "1";
    }
    
    public String getAssessmentMessage(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
    	System.out.println("::::::::::::::::::::::::::::::: getAssessmentMessage :::::::::::::::::::::::::::::::");
    	WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }

		StringBuffer sb =new StringBuffer();
		
		try{
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;

			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,20,"assessment.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}

			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"assessmentWiseStepMessageId";
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			List<AssessmentWiseStepMessage> assessmentWiseStepMessageList1 = new ArrayList<AssessmentWiseStepMessage>();
			assessmentWiseStepMessageList1 = assessmentwisestepmessageDAO.findByCriteria(sortOrderStrVal);
			totalRecord = assessmentWiseStepMessageList1.size();

			if(totalRecord<end)
				end=totalRecord;
			List<AssessmentWiseStepMessage> assessmentWiseStepMessageList = assessmentWiseStepMessageList1.subList(start,end);
			
			sb.append("<table  id='assessmentMessageTable' width='100%' border='0'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			String responseText="";
			
//			responseText=PaginationAndSorting.responseSortingLink(lblTpe,sortOrderFieldName,"stepMessage",sortOrderTypeVal,pgNo);
			responseText=lblTpe;
			sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
			
//			responseText=PaginationAndSorting.responseSortingLink("Assessment Name",sortOrderFieldName,"stepMessage",sortOrderTypeVal,pgNo);
			responseText="Assessment Name";
			sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
			
//			responseText=PaginationAndSorting.responseSortingLink("Template Name",sortOrderFieldName,"stepMessage",sortOrderTypeVal,pgNo);
			responseText="Template Name";
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
//			responseText=PaginationAndSorting.responseSortingLink("Step",sortOrderFieldName,"stepMessage",sortOrderTypeVal,pgNo);
			responseText="Step";
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
//			responseText=PaginationAndSorting.responseSortingLink("Message",sortOrderFieldName,"stepMessage",sortOrderTypeVal,pgNo);
			responseText="Message";
			sb.append("<th width='26%' valign='top'>"+responseText+"</th>");
			
//			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"stepMessage",sortOrderTypeVal,pgNo);
			responseText=lblStatus;
			sb.append("<th width='4%' valign='top'>"+responseText+"</th>");
			
			sb.append("<th width='20%'>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			
			for (AssessmentWiseStepMessage assessmentWiseStepMessage : assessmentWiseStepMessageList) 
			{
				sb.append("<tr>" );
				if(assessmentWiseStepMessage.getAssessmentDetail().getAssessmentType()==1) {
					sb.append("<td>"+lblB+"</td>");
				} else if(assessmentWiseStepMessage.getAssessmentDetail().getAssessmentType()==2) {
					sb.append("<td>"+optJSpe+"</td>");
				} else if(assessmentWiseStepMessage.getAssessmentDetail().getAssessmentType()==3) {
					sb.append("<td>"+lblSmartP+"</td>");
				} else if(assessmentWiseStepMessage.getAssessmentDetail().getAssessmentType()==4) {
					sb.append("<td>"+lblIPI+"</td>");
				}
				sb.append("<td>"+assessmentWiseStepMessage.getAssessmentDetail().getAssessmentName()+"</td>");
				if(assessmentWiseStepMessage.getAssessmentTemplateMaster()!=null && assessmentWiseStepMessage.getAssessmentTemplateMaster().getAssessmentTemplateId()!=0)
					sb.append("<td>"+assessmentWiseStepMessage.getAssessmentTemplateMaster().getTemplateName()+"</td>");
				else
					sb.append("<td>N/A</td>");
				if(assessmentWiseStepMessage.getAssessmentStepMaster()!=null && assessmentWiseStepMessage.getAssessmentStepMaster().getAssessmentStepId()!=0)
					sb.append("<td>"+assessmentWiseStepMessage.getAssessmentStepMaster().getStepName()+"</td>");
				else
					sb.append("<td>N/A</td>");
				if(assessmentWiseStepMessage.getStepMessage()!=null && !assessmentWiseStepMessage.getStepMessage().equals(""))
					sb.append("<td>"+assessmentWiseStepMessage.getStepMessage()+"</td>");
				else
					sb.append("<td>N/A</td>");
				sb.append("<td>");
				if(assessmentWiseStepMessage.getStatus().equalsIgnoreCase("A"))
					sb.append(optAct);
				else
					sb.append(optInActiv);
				sb.append("</td>");
				sb.append("<td>");
				boolean pipeFlag=false;
				if(roleAccess.indexOf("|2|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editAssessmentMessage("+assessmentWiseStepMessage.getAssessmentWiseStepMessageId()+")'>"+lblEdit+"</a>");
					pipeFlag=true;
				}else if(roleAccess.indexOf("|4|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editAssessmentMessage("+assessmentWiseStepMessage.getAssessmentWiseStepMessageId()+")'>"+lnkV+"</a>");
					pipeFlag=true;
				}
				if(roleAccess.indexOf("|7|")!=-1){                
					if(pipeFlag)sb.append(" | ");
					if(assessmentWiseStepMessage.getStatus().equalsIgnoreCase("A"))
						sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateAssessmentMessage("+assessmentWiseStepMessage.getAssessmentWiseStepMessageId()+",'I')\">"+lblDeactivate+"</a>");
					else
						sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateAssessmentMessage("+assessmentWiseStepMessage.getAssessmentWiseStepMessageId()+",'A')\">"+lblActivate+"</a>");
					pipeFlag=true;	
				}
				sb.append("</td>");

			}
			if(assessmentWiseStepMessageList.size()==0)
				sb.append("<tr id='tr1' ><td id='tr1' colspan=7>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );

			sb.append("</table>");
//			sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo, "pageSize3", "780"));
				
		}catch (Exception e) {
			e.printStackTrace();
		}
//		System.out.println("sb.toString() : "+sb.toString());
		return sb.toString();
	}
    
    public AssessmentWiseStepMessage getAssessmentWisesTepmMessageByID(int assessmentwisestepmessageID)
	{
    	System.out.println("::::::::::::::::::::::::::::::: getAssessmentWisesTepmMessageByID :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		 
		AssessmentWiseStepMessage assessmentWiseStepMessage = null;
		try{
			assessmentWiseStepMessage = assessmentwisestepmessageDAO.findById(assessmentwisestepmessageID, false, false);
			System.out.println("assessmentWiseStepMessage.getAssessmentTemplateMaster() : "+assessmentWiseStepMessage.getAssessmentTemplateMaster());
			System.out.println("assessmentWiseStepMessage.getAssessmentStepMaster() : "+assessmentWiseStepMessage.getAssessmentStepMaster());
			System.out.println("assessmentWiseStepMessage.getStepMessage() : "+assessmentWiseStepMessage.getStepMessage());
			if(assessmentWiseStepMessage!=null){
				if(assessmentWiseStepMessage.getAssessmentTemplateMaster()==null)
					assessmentWiseStepMessage.setAssessmentTemplateMaster(new AssessmentTemplateMaster());
				if(assessmentWiseStepMessage.getAssessmentStepMaster()==null)
					assessmentWiseStepMessage.setAssessmentStepMaster(new AssessmentStepMaster());
				if(assessmentWiseStepMessage.getStepMessage()==null || assessmentWiseStepMessage.getStepMessage().equals(""))
					assessmentWiseStepMessage.setStepMessage("");
			}
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return assessmentWiseStepMessage;
	}
    
    @Transactional(readOnly=false)
	public AssessmentWiseStepMessage activateDeactivateAssessmentMessage(int assessmentWiseStepMessageId,String status)
	{
    	System.out.println("::::::::::::::::::::::::::::::: activateDeactivateTemplate :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		else
			userMaster = (UserMaster)session.getAttribute("userMaster");
		
		try{
			AssessmentWiseStepMessage assessmentWiseStepMessage = assessmentwisestepmessageDAO.findById(assessmentWiseStepMessageId, false, false);
			assessmentWiseStepMessage.setStatus(status);
			assessmentWiseStepMessage.setUserMaster2(userMaster);
			assessmentWiseStepMessage.setUpdatedDateTime(new Date());
			assessmentwisestepmessageDAO.makePersistent(assessmentWiseStepMessage);
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
    
    public boolean checkIsUseDefaultTemplate(int assessmentType, int assessmentWiseStepMessageId)
	{
    	System.out.println("::::::::::::::::::::::::::::::: checkIsDefaultTemplate :::::::::::::::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null)
			throw new IllegalStateException(msgYrSesstionExp);
		 
		boolean isUseDefaultTemplate = false;
		List<AssessmentWiseStepMessage> assessmentWiseStepMessageList = null;
		try{
			if(assessmentType!=0){
				assessmentWiseStepMessageList = assessmentwisestepmessageDAO.findByCriteria(Restrictions.eq("assessmentType", assessmentType), Restrictions.eq("useDefault", true), Restrictions.eq("status", "A"));
			}
			if(assessmentWiseStepMessageId!=0){
				if(assessmentWiseStepMessageList!=null && assessmentWiseStepMessageList.size()>0){
					for(AssessmentWiseStepMessage assessmentWiseStepMessage : assessmentWiseStepMessageList){
						if(assessmentWiseStepMessage.getAssessmentWiseStepMessageId()==assessmentWiseStepMessageId){
							isUseDefaultTemplate = true;
							break;
						}
					}
				}
			}
			System.out.println("isUseDefaultTemplate : "+isUseDefaultTemplate);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return isUseDefaultTemplate;
	}
}
