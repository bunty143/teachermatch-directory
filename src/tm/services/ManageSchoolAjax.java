package tm.services;


import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.jms.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.TeacherDetail;
import tm.bean.UserLoginHistory;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictAttachment;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictNotes;
import tm.bean.master.DistrictSchools;
import tm.bean.master.FieldOfStudyMaster;
import tm.bean.master.GeoMapping;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.GeographyMaster;
import tm.bean.master.RegionMaster;
import tm.bean.master.SchoolKeyContact;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SchoolNotes;
import tm.bean.master.SchoolStandardMaster;
import tm.bean.master.SchoolTypeMaster;
import tm.bean.master.StateMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictAttachmentDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.SchoolKeyContactDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SchoolNotesDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.RoleMasterDAO;

import tm.dao.user.UserMasterDAO;
import tm.utility.IPAddressUtility;
import tm.utility.ImageResize;
import tm.utility.Utility;

import jsx3.net.Request;

public class ManageSchoolAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}
		
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private SchoolNotesDAO schoolNotesDAO;
	public void setSchoolNotesDAO(SchoolNotesDAO schoolNotesDAO) 
	{
		this.schoolNotesDAO = schoolNotesDAO;
	}
	
	@Autowired
	private UserMasterDAO usermasterdao;
	public void setUsermasterdao(UserMasterDAO usermasterdao) 
	{
		this.usermasterdao = usermasterdao;
	}
	
	@Autowired
	private  TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) 
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	
	@Autowired
	private  RoleMasterDAO roleMasterDAO;
	public void setRoleMasterDAO(RoleMasterDAO roleMasterDAO) 
	{
		this.roleMasterDAO = roleMasterDAO;
	}
	
	@Autowired
	private SchoolKeyContactDAO schoolKeyContactDAO;
	public void setSchoolKeyContactDAO(SchoolKeyContactDAO schoolKeyContactDAO) 
	{
		this.schoolKeyContactDAO = schoolKeyContactDAO;
	}
	
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
 	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) 
 	{
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}
 	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}

	@Autowired
	private DistrictAttachmentDAO districtAttachmentDAO;

	/*** sekhar ***/
	// Get School Records as per Entity.
	public String displayRecordsByEntityType(boolean resultFlag,int entityID,int schoolId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String status,String schoolnametext)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		    StringBuffer tmRecords =	new StringBuffer();
		    try{
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord = 0;
			//------------------------------------
			System.out.println("schoolId==============="+schoolId+":::"+status+":::"+schoolnametext.trim().length());			

			UserMaster userMaster = null;
			Integer entityIDLogin=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return false;
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				entityIDLogin=userMaster.getEntityType();
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,11,"manageschool.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<DistrictSchools> distSchools		=	districtSchoolsDAO.findSchoolIdAllList();
			List<SchoolMaster> schoolMaster	  =	new ArrayList<SchoolMaster>();	
			String sortOrderFieldName="schoolName";			
			Order  sortOrderStrVal=null;			
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			
			Criterion statuscheck=null;
			if(status.equalsIgnoreCase("A"))
			{
				statuscheck=Restrictions.eq("status", "A");
			}
			else if(status.equalsIgnoreCase("I"))
			{
				statuscheck=Restrictions.eq("status", "I");
			}
			else
			{
				statuscheck=Restrictions.isNotNull("status");
			}
			
			
			Criterion criterionDSchool= Restrictions.in("schoolId",distSchools);	
			if(schoolId==0 && schoolnametext.trim().length()!=0)		
			{
				
			}
			else if(entityIDLogin==2 && schoolId==0){
				DistrictMaster districtMaster = districtMasterDAO.findById(userMaster.getDistrictId().getDistrictId(), false, false);
				Criterion criterion2= Restrictions.eq("districtId",districtMaster);				
				totaRecord = schoolMasterDAO.getRowCount(criterion2,criterionDSchool);
				schoolMaster = schoolMasterDAO.findBySchool(sortOrderStrVal,start,noOfRowInPage,criterion2,criterionDSchool,statuscheck);
			    System.out.println("55555555555555555555555");
			}
			else if(entityIDLogin==2){
				Criterion criterion1= Restrictions.eq("schoolId", (long)schoolId);
				DistrictMaster districtMaster = districtMasterDAO.findById(userMaster.getDistrictId().getDistrictId(), false, false);
				
				Criterion criterion2= Restrictions.eq("districtId",districtMaster);				
				totaRecord = schoolMasterDAO.getRowCount(criterion1,criterion2,criterionDSchool);
				schoolMaster = schoolMasterDAO.findBySchool(sortOrderStrVal,start,noOfRowInPage,criterion1,criterion2,criterionDSchool,statuscheck);
				
			}				
		
			
			tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			String responseText="";

			//tmRecords.append("<th width='18%'>School Name</th>");
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSchoolName", locale),sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			//tmRecords.append("<th width='10%'>Final Decision Maker</th>");
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCreatedOn", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");			
						
			//tmRecords.append("<th  width='18%'>District</th>");
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("optDistrict", locale),sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			tmRecords.append(" <th  >"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
			tmRecords.append(" <th >"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			
				if(schoolMaster.size()==0)
					tmRecords.append("<tr><td colspan='8' align='center'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
				if(schoolMaster.size()>0)
				{
				for (SchoolMaster schoolMasterDetails : schoolMaster) 
				{
					tmRecords.append("<tr>" );
					tmRecords.append("<td nowrap >"+schoolMasterDetails.getSchoolName()+"</td>");
					
					tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(schoolMasterDetails.getCreatedDateTime())+"</td>");		
					
					tmRecords.append("<td>"+schoolMasterDetails.getDistrictId().getDistrictName()+""+"</td>");
					
					if(schoolMasterDetails.getStatus().equalsIgnoreCase("A"))
					{
						tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optAct", locale)+"</td>");
					}
					else
					{
						tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("msgDeactive", locale)+"</td>");
					}
					tmRecords.append("<td>");
					if(schoolMasterDetails.getStatus().equalsIgnoreCase("A"))
					{
						tmRecords.append("<a href='javascript:void(0);' data-original-title='"+Utility.getLocaleValuePropByKey("msgClickEditSchool", locale)+"' class='edit' rel='tooltip' onclick=\"return editSchool("+schoolMasterDetails.getSchoolId()+",'"+schoolMasterDetails.getSchoolName()+"')\">"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a>");
						tmRecords.append(" | "); 
						tmRecords.append("<a href='javascript:void(0);' data-original-title='"+Utility.getLocaleValuePropByKey("msgClickDectivateSchool", locale)+"' class='deactivate' rel='tooltip'  onclick=\"return activateDeactivateSchool("+entityID+","+schoolMasterDetails.getSchoolId()+",'I')\">"+Utility.getLocaleValuePropByKey("lblDeactivate", locale)+"");
					}
					else
					{
						tmRecords.append("<a href='javascript:void(0);' data-original-title='"+Utility.getLocaleValuePropByKey("msgClickEditSchool", locale)+"' class='edit' rel='tooltip' onclick=\"return editSchool("+schoolMasterDetails.getSchoolId()+",'"+schoolMasterDetails.getSchoolName()+"')\">"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a>");
						tmRecords.append(" | "); 
						tmRecords.append("<a href='javascript:void(0);' data-original-title='"+Utility.getLocaleValuePropByKey("msgClickActivateSchool", locale)+"' class='activate' rel='tooltip' onclick=\"return activateDeactivateSchool("+entityID+","+schoolMasterDetails.getSchoolId()+",'A')\">"+Utility.getLocaleValuePropByKey("lblActivate", locale)+"");
					}
					tmRecords.append("</td>");							
				}
				}
				else
				{			
					//tmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );				
			    }
			tmRecords.append("<script>$('.edit').tooltip();</script>");
			tmRecords.append("<script>$('.deactivate').tooltip();</script>");
			tmRecords.append("<script>$('.activate').tooltip();</script>");
			tmRecords.append("</table>");
			System.out.println("totaRecord"+totaRecord);
			tmRecords.append(PaginationAndSorting.getPaginationStringForManageJobOrdersAjax(request,totaRecord,noOfRow, pageNo));

		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tmRecords.toString();
	}
	@Transactional(readOnly=false)
	public boolean saveSchoolByUser(String schoolName)
	{
		boolean b=false;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }	
		try
		{
			UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");		
			SchoolMaster schoolMaster=new SchoolMaster();	
			DistrictMaster districtMaster=districtMasterDAO.findByDistrictId(userMaster.getDistrictId().getDistrictId()+"");
			
			StateMaster stateMaster=new StateMaster();
			stateMaster.setStateId(Long.valueOf(3));
			
			SchoolTypeMaster schoolTypeMaster=new SchoolTypeMaster();
			schoolTypeMaster.setSchoolTypeId(1);		
			
			GeographyMaster geographyMaster=new GeographyMaster();
			geographyMaster.setGeoId(11);			
			
			GeoMapping geoMapping=new GeoMapping();
			geoMapping.setActualGeoId(11);			
			
			RegionMaster regionMaster=new RegionMaster();
			regionMaster.setRegionId(5);			
			
			SchoolStandardMaster schoolStandardMaster=new SchoolStandardMaster();
			schoolStandardMaster.setSchoolStandardId(1);			
			
			schoolMaster.setDistrictId(districtMaster);
			schoolMaster.setDistrictName(userMaster.getDistrictId().getDistrictName());
			schoolMaster.setSchoolName(schoolName);
			schoolMaster.setAddress("");
			schoolMaster.setCityName("");
			schoolMaster.setStateMaster(stateMaster);
			schoolMaster.setZip("1");                                             //Change By Deepak  schoolMaster.setZip(1); 			
			
			schoolMaster.setSchoolTypeId(schoolTypeMaster);
			schoolMaster.setStatus("A");
			schoolMaster.setGeoMapping(geoMapping);
			schoolMaster.setRegionId(regionMaster);
			schoolMaster.setSchoolStandardId(schoolStandardMaster);
			schoolMaster.setDmName(userMaster.getFirstName()+" "+userMaster.getLastName());
			schoolMaster.setAcName("");
			schoolMaster.setAmName("");
			schoolMaster.setCanTMApproach(0);
			schoolMaster.setIsPortfolioNeeded(true);
			schoolMaster.setIsWeeklyCgReport(0);
			schoolMaster.setPostingOnDistrictWall(0);
			schoolMaster.setPostingOnSchoolWall(0);
			schoolMaster.setPostingOnTMWall(0);			
			schoolMaster.setCreatedDateTime(new Date());
	        schoolMasterDAO.makePersistent(schoolMaster);     
	       
	        
	        SchoolMaster schoolMaster2=new SchoolMaster();
	        DistrictSchools districtSchools=new DistrictSchools();
	        districtSchools.setDistrictMaster(districtMaster);
	        districtSchools.setSchoolMaster(schoolMaster);
	        districtSchools.setSchoolName(schoolName);
	        districtSchools.setCreatedBy(userMaster);
	        districtSchools.setCreatedDateTime(new Date());	 
	        districtSchoolsDAO.makePersistent(districtSchools);        
			b=true;	
			System.out.println("school save");
		}
		catch(Exception e)
		{e.printStackTrace();}
		
		return b;
		
	}
	public boolean activateDeactivateSchool(Long schoolId,String status)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null || session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
	    	userMaster=(UserMaster)session.getAttribute("userMaster");
	    }
		try{
			schoolMasterDAO.activateDeactivateSchool(schoolId, status);
			SchoolMaster schooolMaster=schoolMasterDAO.findById(schoolId, false,false);
			
			String messageSubject="";
			String messageSend="";
			SimpleDateFormat dformat=new SimpleDateFormat("HH:MM:SS");
			String schoolname=schooolMaster.getSchoolName();
			Date d=new Date();
			String to[]={"rclark@teachermatch.org","krogers@teachermatch.org"};
			if(status.equals("I"))
			{
				messageSubject=Utility.getLocaleValuePropByKey("msgDistrictSchoolDeactivated", locale);
				messageSend=""+Utility.getLocaleValuePropByKey("msgDearTMAdmin", locale)+",<br><br>" +schoolname+
						" "+Utility.getLocaleValuePropByKey("msgWasDeactivatedby", locale)+" " + userMaster.getFirstName()+" "+userMaster.getLastName()
						+" "+Utility.getLocaleValuePropByKey("msgAt3", locale)+" "+dformat.format(d)+". "+Utility.getLocaleValuePropByKey("msgPleaseVerifyWith", locale)+" " +
						Utility.getLocaleValuePropByKey("msgIntendedAction", locale);
				
			}
			
			Criterion crtentitytype=Restrictions.eq("entityType",1);
			RoleMaster rolm=roleMasterDAO.findById(1,false,false);
			Criterion crtrollid=Restrictions.eq("roleId",rolm);
			List<UserMaster> um=usermasterdao.findByCriteria(crtentitytype,crtrollid);
			
			if(status.equals("A"))
			{
				messageSubject=Utility.getLocaleValuePropByKey("msgDistrictSchoolActivated", locale);
				messageSend=""+Utility.getLocaleValuePropByKey("msgDearTMAdmin", locale)+",<br><br>" +schoolname+
				        Utility.getLocaleValuePropByKey("msgWasActivatedBy", locale) + userMaster.getFirstName()+" "+userMaster.getLastName()
						+Utility.getLocaleValuePropByKey("msgAt3", locale)+dformat.format(d)+". "+Utility.getLocaleValuePropByKey("msgPleaseVerifyWith", locale)+" " +
						Utility.getLocaleValuePropByKey("msgIntendedAction", locale);;	
				
			}
					
			System.out.println(messageSend);
			
			for(UserMaster sender:um)
			{
				String content="";
			try {
				
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom("admin@netsutra.com");
				dsmt.setMailto(sender.getEmailAddress());
				dsmt.setMailsubject(messageSubject);
				content=MailText.messageForDefaultFont(messageSend, userMaster);					
				dsmt.setMailcontent(content);
				
				System.out.println("content     "+content);
				try {
				//	dsmt.start();	
				} catch (Exception e) {}
				
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			}	
			try{
				String logType="";
				SchoolMaster schoolMaster = schoolMasterDAO.findById(schoolId, false, false);
				if(status.equalsIgnoreCase("A")){
					logType="Activate School";
				}else{
					logType="Deactivate School";
				}
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
			}catch(Exception e){
				e.printStackTrace();
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
			return true;
		}
		System.out.println("complete");
		return true;
	}
	
	@Transactional(readOnly=false)
	public boolean updateSchoolByUser(int schoolId,String schoolName)
	{
		boolean b=false;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }	
		try
		{
			UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");
			SchoolMaster schoolMaster=schoolMasterDAO.findById(Long.valueOf(schoolId), false, false);			
			schoolMaster.setSchoolName(schoolName);
			schoolMasterDAO.updatePersistent(schoolMaster);		
			
			DistrictMaster districtMaster=districtMasterDAO.findByDistrictId(userMaster.getDistrictId().getDistrictId()+"");
			
			DistrictSchools districtSchools=districtSchoolsDAO.findByDistrictAndSchool(districtMaster,schoolMaster);
			
			districtSchools.setSchoolName(schoolName);
			districtSchoolsDAO.updatePersistent(districtSchools);
			System.out.println("#################################");
			
			b=true;
		}
		catch (Exception e) {
		e.printStackTrace();
		}
		return b;
	}
}


