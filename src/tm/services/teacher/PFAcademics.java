package tm.services.teacher;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.EligibilityTranscript;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.master.DegreeMaster;
import tm.bean.master.FieldOfStudyMaster;
import tm.bean.master.UniversityMaster;
import tm.dao.EligibilityTranscriptDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.master.DegreeMasterDAO;
import tm.dao.master.FieldOfStudyMasterDAO;
import tm.dao.master.UniversityMasterDAO;
import tm.services.PaginationAndSorting;
import tm.utility.Utility;

public class PFAcademics 
{
	 String locale = Utility.getValueOfPropByKey("locale");
	 String optSchool=Utility.getLocaleValuePropByKey("tableheaderSchool", locale);
	 String lblDatAtt=Utility.getLocaleValuePropByKey("lblDatAtt", locale);
	 String lblDgr=Utility.getLocaleValuePropByKey("lblDgr", locale);
	 String lblGPA=Utility.getLocaleValuePropByKey("lblGPA", locale);
	 String lblFildOfStudy=Utility.getLocaleValuePropByKey("lblFildOfStudy", locale);
	 String headTranscript=Utility.getLocaleValuePropByKey("headTranscript", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	 String lnkDlt=Utility.getLocaleValuePropByKey("lnkDlt", locale);
	 String msgNorecordfound=Utility.getLocaleValuePropByKey("msgNorecordfound", locale);
	 
	 
	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	public void setTeacherAcademicsDAO(TeacherAcademicsDAO teacherAcademicsDAO) 
	{
		this.teacherAcademicsDAO = teacherAcademicsDAO;
	}
	
	@Autowired
	private DegreeMasterDAO degreeMasterDAO;
	public void setDegreeMasterDAO(DegreeMasterDAO degreeMasterDAO) 
	{
		this.degreeMasterDAO = degreeMasterDAO;
	}
	
	@Autowired
	private FieldOfStudyMasterDAO fieldOfStudyMasterDAO;
	public void setFieldOfStudyMasterDAO(FieldOfStudyMasterDAO fieldOfStudyMasterDAO) 
	{
		this.fieldOfStudyMasterDAO = fieldOfStudyMasterDAO;
	}
	
	@Autowired 
	private UniversityMasterDAO universityMasterDAO;
	public void setUniversityMasterDAO(UniversityMasterDAO universityMasterDAO) 
	{
		this.universityMasterDAO = universityMasterDAO;
	}
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) 
	{
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}
	@Autowired
	private EligibilityTranscriptDAO eligibilityTranscriptDAO;
	public String getPFAcademinGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer sb = new StringBuffer();		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"attendedInYear";
			String sortOrderNoField		=	"attendedInYear";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("degree") && !sortOrder.equals("fieldOfStudy") && !sortOrder.equals("university")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("degree")){
					 sortOrderNoField="degree";
				 }
				 if(sortOrder.equals("fieldOfStudy")){
					 sortOrderNoField="fieldOfStudy";
				 }
				 if(sortOrder.equals("university")){
					 sortOrderNoField="university";
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			
			List<TeacherAcademics> listTeacherAcademics = null;
			//listTeacherAcademics = teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
			listTeacherAcademics = teacherAcademicsDAO.findSortedAcadamicDetailByTeacher(sortOrderStrVal,teacherDetail);
			
			
			
			List<TeacherAcademics> sortedlistTeacherAcademics		=	new ArrayList<TeacherAcademics>();
			
			SortedMap<String,TeacherAcademics>	sortedMap = new TreeMap<String,TeacherAcademics>();
			if(sortOrderNoField.equals("degree"))
			{
				sortOrderFieldName	=	"degree";
			}
			if(sortOrderNoField.equals("fieldOfStudy"))
			{
				sortOrderFieldName	=	"fieldOfStudy";
			}
			if(sortOrderNoField.equals("university"))
			{
				sortOrderFieldName	=	"university";
			}
			int mapFlag=2;
			for (TeacherAcademics trAcademics : listTeacherAcademics){
				String orderFieldName=trAcademics.getAttendedInYear()+"";
				if(sortOrderFieldName.equals("degree")){
					orderFieldName=trAcademics.getDegreeId().getDegreeName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("fieldOfStudy")){
					orderFieldName=trAcademics.getFieldId().getFieldName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("university")){
					orderFieldName=trAcademics.getUniversityId().getUniversityName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherAcademics=listTeacherAcademics;
			}
			
			totalRecord =sortedlistTeacherAcademics.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherAcademics> listsortedTeacherCertificates		=	sortedlistTeacherAcademics.subList(start,end);
			
			sb.append("<table border='0' id='academicGrid'  width='100%' class='table table-striped'  >");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLink(optSchool,sortOrderFieldName,"university",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblDatAtt,sortOrderFieldName,"attendedInYear",sortOrderTypeVal,pgNo);
			sb.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblDgr,sortOrderFieldName,"degree",sortOrderTypeVal,pgNo);
			sb.append("<th width='15%' valign='top'>"+responseText+"</th>");
		
			sb.append("<th width='15%' class='net-header-text'>");
			sb.append(lblGPA);
			sb.append("</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblFildOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
			sb.append("<th width='12%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(headTranscript,sortOrderFieldName,"pathOfTranscript",sortOrderTypeVal,pgNo);
			sb.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			sb.append("<th width='10%' class='net-header-text'>");
			sb.append(lblAct);
			sb.append("</th>");			
			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherAcademics!=null)
			for(TeacherAcademics ta:listsortedTeacherCertificates)
			{
				sb.append("<tr>");
				
				sb.append("<td>");
				if(!Utility.isNC() && ta.getUniversityId()!=null  )
					sb.append(ta.getUniversityId().getUniversityName());
				else if(Utility.isNC() && ta.getUniversityId()!=null &&  
						ta.getUniversityId().getUniversityName()!=null && 
						!ta.getUniversityId().getUniversityName().equalsIgnoreCase("OTHER"))
					sb.append(ta.getUniversityId().getUniversityName());
				else if(Utility.isNC() && ta.getUniversityId()!=null &&  
						ta.getUniversityId().getUniversityName()!=null 
						&& ta.getUniversityId().getUniversityName().equalsIgnoreCase("OTHER") 
						&& ta.getOtherUniversityName()!=null )
					sb.append(ta.getOtherUniversityName());
				sb.append("</td>");
				
				sb.append("<td>");
				
				if(ta.getAttendedInYear()!=null && !ta.getAttendedInYear().equals(""))
					sb.append(ta.getAttendedInYear());
				if(ta.getAttendedInYear()!=null && !ta.getAttendedInYear().equals("") && ta.getLeftInYear()!=null && !ta.getLeftInYear().equals(""))
					sb.append(" "+ Utility.getLocaleValuePropByKey("lblTos", locale)+" " );
					if(ta.getLeftInYear()!=null && !ta.getLeftInYear().equals(""))
					sb.append(ta.getLeftInYear());
					sb.append("</td>");
				
				sb.append("<td>");
				//if(!Utility.isNC())
					sb.append(ta.getDegreeId().getDegreeName());
				/*else if(Utility.isNC() && ta.getDegreeId()!=null && ta.getDegreeId().getDegreeName()!=null
						&& !ta.getDegreeId().getDegreeName().equalsIgnoreCase("Other"))
				{
					sb.append(ta.getDegreeId().getDegreeName());
				}
				else if(Utility.isNC() && ta.getDegreeId()!=null && ta.getDegreeId().getDegreeName()!=null
						&& ta.getDegreeId().getDegreeName().equalsIgnoreCase("Other"))
				{
					sb.append(ta.getOtherDegreeName());
				}*/
				
				sb.append("</td>");
				
				String chkGpa="gpaNo";
				if(ta.getGpaCumulative()!=null){
					chkGpa="gpaYes";
				}
				sb.append("<td class='"+chkGpa+"'>");
				if(ta.getDegreeId().getDegreeType().trim().equalsIgnoreCase("b"))
				{
					sb.append("Freshman: "+(ta.getGpaFreshmanYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaFreshmanYear()))+"<br> " +
						" Sophomore: "+(ta.getGpaSophomoreYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaSophomoreYear()))+"<br>" +
						" Junior: "+(ta.getGpaJuniorYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaJuniorYear()))+"<br>" +
						" Senior: "+(ta.getGpaSeniorYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaSeniorYear()))+"<br>" +
						" Cumulative: "+(ta.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaCumulative())));
				}else{
					sb.append("Cumulative:"+(ta.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaCumulative())));
				}
				sb.append("</td>");
				
				sb.append("<td>");
				if(ta.getFieldId()!=null)
				sb.append(ta.getFieldId().getFieldName());	
				sb.append("</td>");
				
				
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				sb.append("<td style='text-align:center'>");								
				//sb.append(ta.getPathOfTranscript()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view transcript file !'  id='trans"+ta.getAcademicId()+"' onclick=\"downloadTranscript('"+ta.getAcademicId()+"','trans"+ta.getAcademicId()+"');"+windowFunc+"\">"+ta.getPathOfTranscript()+"</a>");
				sb.append(ta.getPathOfTranscript()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view Transcript !'  id='trans"+ta.getAcademicId()+"' onclick=\"downloadTranscript('"+ta.getAcademicId()+"','trans"+ta.getAcademicId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
				sb.append("<script>$('#trans"+ta.getAcademicId()+"').tooltip();</script>");
				sb.append("</td>");
				
				sb.append("<td>");
				if(!(ta.getStatus()!=null && (ta.getStatus().equalsIgnoreCase("V")))){
					sb.append("	<a href='#' onclick=\"return showRecordToForm('"+ta.getAcademicId()+"')\" >"+lblEdit+"</a>");
					sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return delRow_academic('"+ta.getAcademicId()+"')\">"+lnkDlt+"</a>");
				}
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			if(listTeacherAcademics==null || listTeacherAcademics.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
	public String getPFAcademinGridForDspq(String noOfRow, String pageNo,String sortOrder,String sortOrderType,boolean isMiami,int transcriptFlag)
	{	
		System.out.println("getPFAcademinGridForDspq isMiami "+isMiami +" transcriptFlag "+transcriptFlag);
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer sb = new StringBuffer();		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"attendedInYear";
			String sortOrderNoField		=	"attendedInYear";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("degree") && !sortOrder.equals("fieldOfStudy") && !sortOrder.equals("university")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("degree")){
					 sortOrderNoField="degree";
				 }
				 if(sortOrder.equals("fieldOfStudy")){
					 sortOrderNoField="fieldOfStudy";
				 }
				 if(sortOrder.equals("university")){
					 sortOrderNoField="university";
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			
			List<TeacherAcademics> listTeacherAcademics = null;
			//listTeacherAcademics = teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
			listTeacherAcademics = teacherAcademicsDAO.findSortedAcadamicDetailByTeacher(sortOrderStrVal,teacherDetail);
			
			
			
			List<TeacherAcademics> sortedlistTeacherAcademics		=	new ArrayList<TeacherAcademics>();
			
			SortedMap<String,TeacherAcademics>	sortedMap = new TreeMap<String,TeacherAcademics>();
			if(sortOrderNoField.equals("degree"))
			{
				sortOrderFieldName	=	"degree";
			}
			if(sortOrderNoField.equals("fieldOfStudy"))
			{
				sortOrderFieldName	=	"fieldOfStudy";
			}
			if(sortOrderNoField.equals("university"))
			{
				sortOrderFieldName	=	"university";
			}
			int mapFlag=2;
			for (TeacherAcademics trAcademics : listTeacherAcademics){
				String orderFieldName=trAcademics.getAttendedInYear()+"";
				if(sortOrderFieldName.equals("degree")){
					orderFieldName=trAcademics.getDegreeId().getDegreeName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("fieldOfStudy")){
					orderFieldName=trAcademics.getFieldId().getFieldName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("university")){
					orderFieldName=trAcademics.getUniversityId().getUniversityName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherAcademics=listTeacherAcademics;
			}
			
			totalRecord =sortedlistTeacherAcademics.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherAcademics> listsortedTeacherCertificates		=	sortedlistTeacherAcademics.subList(start,end);
			
			sb.append("<table border='0' id='academicGrid'  width='100%' class='table table-striped'  >");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLink(optSchool,sortOrderFieldName,"university",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblDatAtt,sortOrderFieldName,"attendedInYear",sortOrderTypeVal,pgNo);
			sb.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblDgr,sortOrderFieldName,"degree",sortOrderTypeVal,pgNo);
			sb.append("<th width='15%' valign='top'>"+responseText+"</th>");
		
			sb.append("<th width='15%' class='net-header-text'>");
			sb.append(lblGPA);
			sb.append("</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblFildOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
			sb.append("<th width='12%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(headTranscript,sortOrderFieldName,"pathOfTranscript",sortOrderTypeVal,pgNo);
			sb.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			sb.append("<th width='10%' class='net-header-text'>");
			sb.append(lblAct);
			sb.append("</th>");			
			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherAcademics!=null)
			for(TeacherAcademics ta:listsortedTeacherCertificates)
			{
				sb.append("<tr class='cntAcad'>");
				
				sb.append("<td>");
				if(!Utility.isNC() && ta.getUniversityId()!=null  )
					sb.append(ta.getUniversityId().getUniversityName());
				else if(Utility.isNC() && ta.getUniversityId()!=null &&  
						ta.getUniversityId().getUniversityName()!=null && 
						!ta.getUniversityId().getUniversityName().equalsIgnoreCase("OTHER"))
					sb.append(ta.getUniversityId().getUniversityName());
				else if(Utility.isNC() && ta.getUniversityId()!=null &&  
						ta.getUniversityId().getUniversityName()!=null 
						&& ta.getUniversityId().getUniversityName().equalsIgnoreCase("OTHER") 
						&& ta.getOtherUniversityName()!=null )
					sb.append(ta.getOtherUniversityName());	
				sb.append("</td>");
				
				sb.append("<td>");
				
				if(ta.getAttendedInYear()!=null && !ta.getAttendedInYear().equals(""))
					sb.append(ta.getAttendedInYear());
				if(ta.getAttendedInYear()!=null && !ta.getAttendedInYear().equals("") && ta.getLeftInYear()!=null && !ta.getLeftInYear().equals(""))
					sb.append(" to ");
					if(ta.getLeftInYear()!=null && !ta.getLeftInYear().equals(""))
					sb.append(ta.getLeftInYear());
					sb.append("</td>");
				
					sb.append("<td  class='degreeTypeVal' degreeTypeVal='"+ta.getDegreeId().getDegreeType()+"'>");
					if(!Utility.isNC())
						sb.append(ta.getDegreeId().getDegreeName());
					else if(Utility.isNC() && ta.getDegreeId()!=null && ta.getDegreeId().getDegreeName()!=null
							&& !ta.getDegreeId().getDegreeName().equalsIgnoreCase("Other"))
					{
						sb.append(ta.getDegreeId().getDegreeName());
					}
					else if(Utility.isNC() && ta.getDegreeId()!=null && ta.getDegreeId().getDegreeName()!=null
							&& ta.getDegreeId().getDegreeName().equalsIgnoreCase("Other"))
					{
						sb.append(ta.getOtherDegreeName());
					}
				sb.append("</td>");
				
				String chkGpa="gpaNo";
				if(ta.getGpaCumulative()!=null){
					chkGpa="gpaYes";
				}
				sb.append("<td class='"+chkGpa+"'>");
				if(ta.getDegreeId().getDegreeType().trim().equalsIgnoreCase("b"))
				{
					sb.append("Freshman: "+(ta.getGpaFreshmanYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaFreshmanYear()))+"<br> " +
						" Sophomore: "+(ta.getGpaSophomoreYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaSophomoreYear()))+"<br>" +
						" Junior: "+(ta.getGpaJuniorYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaJuniorYear()))+"<br>" +
						" Senior: "+(ta.getGpaSeniorYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaSeniorYear()))+"<br>" +
						" Cumulative: "+(ta.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaCumulative())));
				}else{
					sb.append("Cumulative:"+(ta.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaCumulative())));
				}
				sb.append("</td>");
				
				sb.append("<td>");
				if(!Utility.isNC())
				{
					if(ta.getFieldId()!=null)
						sb.append(ta.getFieldId().getFieldName());
				}
				else if(Utility.isNC() && ta.getFieldId()!=null && ta.getFieldId().getFieldName()!=null 
						&&  !ta.getFieldId().getFieldName().equalsIgnoreCase("Other"))
				{
					sb.append(ta.getFieldId().getFieldName());
				}
				else if(Utility.isNC() && ta.getFieldId()!=null && ta.getFieldId().getFieldName()!=null 
						&&  ta.getFieldId().getFieldName().equalsIgnoreCase("Other"))
				{
					sb.append(ta.getOtherFieldName());
				}
					
				sb.append("</td>");
				
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				sb.append("<td style='text-align:center'>");								
				//sb.append(ta.getPathOfTranscript()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view transcript file !'  id='trans"+ta.getAcademicId()+"' onclick=\"downloadTranscript('"+ta.getAcademicId()+"','trans"+ta.getAcademicId()+"');"+windowFunc+"\">"+ta.getPathOfTranscript()+"</a>");
				sb.append(ta.getPathOfTranscript()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view Transcript !'  id='trans"+ta.getAcademicId()+"' onclick=\"downloadTranscript('"+ta.getAcademicId()+"','trans"+ta.getAcademicId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
				sb.append("<script>$('#trans"+ta.getAcademicId()+"').tooltip();</script>");
				//sb.append("<input type='hidden' id='statusCheck' value='"+ta.getStatus()+"'/>");
				sb.append("</td>");
				
				sb.append("<td>");
				if(isMiami)
				{
					if(ta.getStatus()==null || !ta.getStatus().equalsIgnoreCase("V"))
					{
						sb.append("	<a href='#' onclick=\"return showRecordToForm('"+ta.getAcademicId()+"','"+ta.getStatus()+"')\" >"+lblEdit+"</a>"); 
						sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return delRow_academic('"+ta.getAcademicId()+"','"+ta.getStatus()+"')\">"+lnkDlt+"</a>");
					}
				}
				else
				{
					if(ta.getStatus()!=null && ta.getStatus().equalsIgnoreCase("V"))
					{
						if(transcriptFlag==1 && ta.getPathOfTranscript()==null)
							sb.append("	<a href='#' onclick=\"return showRecordToForm('"+ta.getAcademicId()+"','"+ta.getStatus()+"')\" >"+lblEdit+"</a>");
					}
					else
					{
						sb.append("	<a href='#' onclick=\"return showRecordToForm('"+ta.getAcademicId()+"','"+ta.getStatus()+"')\" >"+lblEdit+"</a>");
						sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return delRow_academic('"+ta.getAcademicId()+"')\">"+lnkDlt+"</a>");
					}
				}
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			if(listTeacherAcademics==null || listTeacherAcademics.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
	
	public TeacherAcademics showEditAcademics(String academicId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		TeacherAcademics teacherAcademics = teacherAcademicsDAO.findById(new Long(academicId), false, false);
		
		return teacherAcademics;
		
	}
	public String saveOrUpdateAcademics(String academicId, String degreeId, String universityId, String fieldId, 
			String attendedInYear, String leftInYear, String gpaFreshmanYear, String gpaJuniorYear, 
			String gpaSophomoreYear, String gpaSeniorYear, String gpaCumulative, String degreeType, String fileName,int international,String otherUniversityName,String otherDegreeName,String otherFieldName )
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherAcademics teacherAcademics = null;
			UniversityMaster universityMaster  = null;
			FieldOfStudyMaster fieldOfStudyMaster=null;
			DegreeMaster degreeMaster=null;
			
			if(degreeId!=null && !degreeId.equals(""))
			{
			degreeMaster = degreeMasterDAO.findById(new Long(degreeId.trim()), false, false);
			}
			else
			{				
				degreeMaster = degreeMasterDAO.getDMByShortCode("ND");
			}
			
			
			if(universityId!=null && !universityId.trim().equals("") && !universityId.trim().equals(0)){
				universityMaster = universityMasterDAO.findById(new Integer(universityId.trim()), false, false);
			}
			
			if(fieldId!=null && !fieldId.trim().equals("") && !fieldId.trim().equals(0))
			fieldOfStudyMaster = fieldOfStudyMasterDAO.findById(new Long(fieldId.trim()), false, false);
			
			if(academicId==null || academicId.equals(""))
			{
				teacherAcademics = new TeacherAcademics();				
			}
			else
			{
				teacherAcademics = teacherAcademicsDAO.findById(new Long(academicId.trim()), false, false);
			}
			
			teacherAcademics.setTeacherId(teacherDetail);
			teacherAcademics.setDegreeId(degreeMaster);
			teacherAcademics.setUniversityId(universityMaster);
			teacherAcademics.setFieldId(fieldOfStudyMaster);
			if(attendedInYear!=null && !attendedInYear.equals(""))
			teacherAcademics.setAttendedInYear(new Long(attendedInYear));
			
			if(leftInYear!=null && !leftInYear.equals(""))
			teacherAcademics.setLeftInYear(new Long(leftInYear));
			
			teacherAcademics.setCreatedDateTime(new Date());
			
			if(international==1)
            {
                  teacherAcademics.setInternational(true);  
            }
            else
            {
                  teacherAcademics.setInternational(false);
            }

			if(!fileName.equals("")){
				String filePath =Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+teacherAcademics.getPathOfTranscript();
				System.out.println("previous file path :: "+filePath);
				File file=new File(filePath);
				file.delete();
				teacherAcademics.setPathOfTranscript(fileName);
			}
			if(degreeType.trim().equalsIgnoreCase("B"))
			{
				/*if (teacherAcademics.getGpaFreshmanYear()!=null && !teacherAcademics.getGpaFreshmanYear().equals(""))
						teacherAcademics.setGpaFreshmanYear(new Double(gpaFreshmanYear));
				else
					teacherAcademics.setGpaFreshmanYear(null);
				
				if (teacherAcademics.getGpaJuniorYear()!=null && teacherAcademics.getGpaJuniorYear()==0)
					teacherAcademics.setGpaJuniorYear(new Double(gpaJuniorYear));
				else
					teacherAcademics.setGpaJuniorYear(null);*/
				
				
				if (!gpaFreshmanYear.trim().equals(""))
					teacherAcademics.setGpaFreshmanYear(new Double(gpaFreshmanYear));
				else
					teacherAcademics.setGpaFreshmanYear(null);
				
				if (!gpaJuniorYear.trim().equals(""))
					teacherAcademics.setGpaJuniorYear(new Double(gpaJuniorYear));
				else
					teacherAcademics.setGpaJuniorYear(null);
						
				if (!gpaSophomoreYear.trim().equals(""))
					teacherAcademics.setGpaSophomoreYear(new Double(gpaSophomoreYear));
				else
					teacherAcademics.setGpaSophomoreYear(null);
							
				if (!gpaSeniorYear.trim().equals(""))
					teacherAcademics.setGpaSeniorYear(new Double(gpaSeniorYear));
				else
					teacherAcademics.setGpaSeniorYear(null);
						System.out.println("gpaCumulative     "+gpaCumulative);		
				if (!gpaCumulative.trim().equals(""))
					teacherAcademics.setGpaCumulative(new Double(gpaCumulative));
				else
					teacherAcademics.setGpaCumulative(null);
			}
			else
			{
				if (!gpaCumulative.trim().equals(""))
					teacherAcademics.setGpaCumulative(new Double(gpaCumulative));
				else
					teacherAcademics.setGpaCumulative(null);
			}
			teacherAcademics.setOtherUniversityName(otherUniversityName);
			if(otherDegreeName!=null && !otherDegreeName.trim().equals(""))
				teacherAcademics.setOtherDegreeName(otherDegreeName);
			if(otherFieldName!=null && !otherFieldName.trim().equals(""))
				teacherAcademics.setOtherFieldName(otherFieldName);
			teacherAcademicsDAO.makePersistent(teacherAcademics);
			boolean bTeacherPortfolioStatus=false;
			TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(teacherPortfolioStatus==null)
			{
				bTeacherPortfolioStatus=true;
				teacherPortfolioStatus = new TeacherPortfolioStatus();
				teacherPortfolioStatus.setTeacherId(teacherDetail);
				teacherPortfolioStatus.setIsAcademicsCompleted(true);
			}
			else
			{
				teacherPortfolioStatus.setTeacherId(teacherDetail);
				teacherPortfolioStatus.setIsAcademicsCompleted(true);
			}
			
			
			try{
				if(teacherPortfolioStatus.getIsPersonalInfoCompleted()==null)
				{
					teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsCertificationsCompleted()==null){
				teacherPortfolioStatus.setIsCertificationsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsCertificationsCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsExperiencesCompleted()==null){
				teacherPortfolioStatus.setIsExperiencesCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsExperiencesCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsAffidavitCompleted()==null){
				teacherPortfolioStatus.setIsAffidavitCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAffidavitCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsAcademicsCompleted()==null){
				teacherPortfolioStatus.setIsAcademicsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAcademicsCompleted(false);
				e.printStackTrace();
			}
			
			
			
			//teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			TeacherPortfolioStatus tpsTemp=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(bTeacherPortfolioStatus && tpsTemp==null)
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			else
			{
				teacherPortfolioStatus.setId(tpsTemp.getId());
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "OK";
	}
	
	public boolean updateAcademicsPortfolioStatus()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		boolean updateStatus = false;
		try 
		{
			updateStatus = true;
			
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			boolean bTeacherPortfolioStatus=false;
			TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(teacherPortfolioStatus==null)
			{
				bTeacherPortfolioStatus=true;
				teacherPortfolioStatus = new TeacherPortfolioStatus();
				teacherPortfolioStatus.setIsAcademicsCompleted(true);
			}
			else
			{
				teacherPortfolioStatus.setIsAcademicsCompleted(true);
			}
			
			
			try{
				if(teacherPortfolioStatus.getIsPersonalInfoCompleted()==null)
				{
					teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsCertificationsCompleted()==null){
				teacherPortfolioStatus.setIsCertificationsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsCertificationsCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsExperiencesCompleted()==null){
				teacherPortfolioStatus.setIsExperiencesCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsExperiencesCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsAffidavitCompleted()==null){
				teacherPortfolioStatus.setIsAffidavitCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAffidavitCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsAcademicsCompleted()==null){
				teacherPortfolioStatus.setIsAcademicsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAcademicsCompleted(false);
				e.printStackTrace();
			}
			
			
			//teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			
			TeacherPortfolioStatus tpsTemp=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(bTeacherPortfolioStatus && tpsTemp==null)
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			else
			{
				teacherPortfolioStatus.setId(tpsTemp.getId());
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			}
		} 
		catch (Exception e) 
		{
			updateStatus = false;
			e.printStackTrace();
		}
		
		return updateStatus;
	}

	public Boolean removeTranscript(String academicId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherAcademics teacherAcademics = teacherAcademicsDAO.findById(new Long(academicId), false, false);
			if(teacherAcademics!=null && teacherAcademics.getPathOfTranscript()!=null){
			File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+teacherAcademics.getPathOfTranscript());
			System.out.println("1..removeTranscript >>\t"+file.exists()+"\t"+file.getPath()+"\t"+file.getName());
					if(file.delete()){
						System.out.println(file.getName() + " is deleted!");
					}else{
						System.out.println("Delete operation is failed.");
						return false;
					}
					teacherAcademics.setPathOfTranscript(null);
					teacherAcademicsDAO.makePersistent(teacherAcademics);
					System.out.println("2..removeTranscript >>");
					return true;
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return true;
	}
	
	
	public boolean deletePFAcademinGrid(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		Long academicId = Long.parseLong(id);
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		TeacherAcademics teacherAcademics = teacherAcademicsDAO.findById(academicId, false, false);
		Criterion criterion1 = Restrictions.eq("teacherAcademics",teacherAcademics); 
		List<EligibilityTranscript> eligibilityTranscript  = eligibilityTranscriptDAO.findByCriteria(criterion1);
		
		if(eligibilityTranscript.size()>0){
			for (EligibilityTranscript eligibilityTranscript2 : eligibilityTranscript) {
				eligibilityTranscriptDAO.makeTransient(eligibilityTranscript2);
			}
		}
		
		if(teacherAcademics.getPathOfTranscript()!=null && !teacherAcademics.getPathOfTranscript().equals(""))
		{
			try {
				File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+teacherAcademics.getPathOfTranscript());
				if(file.delete())
					System.out.println(file.getName() + " is deleted!");
				else
					System.out.println("Delete operation is failed.");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		try {
			//teacherAcademics.setPathOfTranscript(null);
			teacherAcademicsDAO.makeTransient(teacherAcademics);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public String downloadTranscript(String academicId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }

		String path="";
		
		try 
		{
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherAcademics teacherAcademics = teacherAcademicsDAO.findById(new Long(academicId), false, false);
			
			String fileName= teacherAcademics.getPathOfTranscript();
			String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+fileName;
	        
	        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
	        
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        if(sourceFile.exists()){
	        	FileUtils.copyFile(sourceFile, targetFile);
	         path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
	        }
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return path;
	
	}
	
	public String getPFAcademinGridForDspqWOT(String noOfRow, String pageNo,String sortOrder,String sortOrderType,boolean isMiami,int transcriptFlag)
	{
		
		System.out.println("getPFAcademinGridForDspq getPFAcademinGridForDspqWOT");
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer sb = new StringBuffer();		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"attendedInYear";
			String sortOrderNoField		=	"attendedInYear";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("degree") && !sortOrder.equals("fieldOfStudy") && !sortOrder.equals("university")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("degree")){
					 sortOrderNoField="degree";
				 }
				 if(sortOrder.equals("fieldOfStudy")){
					 sortOrderNoField="fieldOfStudy";
				 }
				 if(sortOrder.equals("university")){
					 sortOrderNoField="university";
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			
			List<TeacherAcademics> listTeacherAcademics = null;
			//listTeacherAcademics = teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
			listTeacherAcademics = teacherAcademicsDAO.findSortedAcadamicDetailByTeacher(sortOrderStrVal,teacherDetail);
			
			
			
			List<TeacherAcademics> sortedlistTeacherAcademics		=	new ArrayList<TeacherAcademics>();
			
			SortedMap<String,TeacherAcademics>	sortedMap = new TreeMap<String,TeacherAcademics>();
			if(sortOrderNoField.equals("degree"))
			{
				sortOrderFieldName	=	"degree";
			}
			if(sortOrderNoField.equals("fieldOfStudy"))
			{
				sortOrderFieldName	=	"fieldOfStudy";
			}
			if(sortOrderNoField.equals("university"))
			{
				sortOrderFieldName	=	"university";
			}
			int mapFlag=2;
			for (TeacherAcademics trAcademics : listTeacherAcademics){
				String orderFieldName=trAcademics.getAttendedInYear()+"";
				if(sortOrderFieldName.equals("degree")){
					orderFieldName=trAcademics.getDegreeId().getDegreeName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("fieldOfStudy")){
					orderFieldName=trAcademics.getFieldId().getFieldName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("university")){
					orderFieldName=trAcademics.getUniversityId().getUniversityName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherAcademics=listTeacherAcademics;
			}
			
			totalRecord =sortedlistTeacherAcademics.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherAcademics> listsortedTeacherCertificates		=	sortedlistTeacherAcademics.subList(start,end);
			
			sb.append("<table border='0' id='academicGrid'  width='100%' class='table table-striped'  >");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			String responseText="";
			
			responseText=PaginationAndSorting.responseSortingLink(optSchool,sortOrderFieldName,"university",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblDatAtt,sortOrderFieldName,"attendedInYear",sortOrderTypeVal,pgNo);
			sb.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblDgr,sortOrderFieldName,"degree",sortOrderTypeVal,pgNo);
			sb.append("<th width='15%' valign='top'>"+responseText+"</th>");
		
			sb.append("<th width='15%' class='net-header-text'>");
			sb.append(lblGPA);
			sb.append("</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblFildOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
			sb.append("<th width='12%' valign='top'>"+responseText+"</th>");
			
			/*responseText=PaginationAndSorting.responseSortingLink("Transcript",sortOrderFieldName,"pathOfTranscript",sortOrderTypeVal,pgNo);
			sb.append("<th width='14%' valign='top'>"+responseText+"</th>");*/
			sb.append("<th width='14%' valign='top'></th>");
			sb.append("<th width='10%' class='net-header-text'>");
			sb.append(lblAct);
			sb.append("</th>");			
			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherAcademics!=null)
			for(TeacherAcademics ta:listsortedTeacherCertificates)
			{
				sb.append("<tr>");
				
				sb.append("<td>");
				if(ta.getUniversityId()!=null)
				sb.append(ta.getUniversityId().getUniversityName());	
				sb.append("</td>");
				
				sb.append("<td>");
				if(ta.getAttendedInYear()!=null && !ta.getAttendedInYear().equals(""))
					sb.append(ta.getAttendedInYear());
				if(ta.getAttendedInYear()!=null && !ta.getAttendedInYear().equals("") && ta.getLeftInYear()!=null && !ta.getLeftInYear().equals(""))
					sb.append(" to ");
					if(ta.getLeftInYear()!=null && !ta.getLeftInYear().equals(""))
					sb.append(ta.getLeftInYear());
					sb.append("</td>");
					
				sb.append("<td  class='degreeTypeVal' degreeTypeVal='"+ta.getDegreeId().getDegreeType()+"'>");
				sb.append(ta.getDegreeId().getDegreeName());;
				sb.append("</td>");
				
				sb.append("<td>");
				if(ta.getDegreeId().getDegreeType().trim().equalsIgnoreCase("b"))
				{
					sb.append("Freshman: "+(ta.getGpaFreshmanYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaFreshmanYear()))+"<br> " +
						" Sophomore: "+(ta.getGpaSophomoreYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaSophomoreYear()))+"<br>" +
						" Junior: "+(ta.getGpaJuniorYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaJuniorYear()))+"<br>" +
						" Senior: "+(ta.getGpaSeniorYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaSeniorYear()))+"<br>" +
						" Cumulative: "+(ta.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaCumulative())));
				}else{
					sb.append("Cumulative:"+(ta.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaCumulative())));
				}
				sb.append("</td>");
				
				sb.append("<td colspan='2'>");
				if(ta.getFieldId()!=null)
				sb.append(ta.getFieldId().getFieldName());	
				sb.append("</td>");
				
				/*String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				sb.append("<td style='text-align:center'>");								
				//sb.append(ta.getPathOfTranscript()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view transcript file !'  id='trans"+ta.getAcademicId()+"' onclick=\"downloadTranscript('"+ta.getAcademicId()+"','trans"+ta.getAcademicId()+"');"+windowFunc+"\">"+ta.getPathOfTranscript()+"</a>");
				sb.append(ta.getPathOfTranscript()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view Transcript !'  id='trans"+ta.getAcademicId()+"' onclick=\"downloadTranscript('"+ta.getAcademicId()+"','trans"+ta.getAcademicId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
				sb.append("<script>$('#trans"+ta.getAcademicId()+"').tooltip();</script>");
				//sb.append("<input type='hidden' id='statusCheck' value='"+ta.getStatus()+"'/>");
				sb.append("</td>");*/
				
				sb.append("<td>");
				if(isMiami)
				{
					if(ta.getStatus()==null || !ta.getStatus().equalsIgnoreCase("V"))
					{
						sb.append("	<a href='#' onclick=\"return showRecordToForm('"+ta.getAcademicId()+"','"+ta.getStatus()+"')\" >"+lblEdit+"</a>");
						sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return delRow_academic('"+ta.getAcademicId()+"','"+ta.getStatus()+"')\">"+lnkDlt+"</a>");
					}
				}
				else
				{
					if(ta.getStatus()!=null && ta.getStatus().equalsIgnoreCase("V"))
					{
						if(transcriptFlag==1 && ta.getPathOfTranscript()==null)
							sb.append("	<a href='#' onclick=\"return showRecordToForm('"+ta.getAcademicId()+"','"+ta.getStatus()+"')\" >"+lblEdit+"</a>");
					}
					else
					{
						sb.append("	<a href='#' onclick=\"return showRecordToForm('"+ta.getAcademicId()+"','"+ta.getStatus()+"')\" >"+lblEdit+"</a>");
						sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return delRow_academic('"+ta.getAcademicId()+"')\">"+lnkDlt+"</a>");
					}
				}
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			if(listTeacherAcademics==null || listTeacherAcademics.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	
	}
	
	public boolean teacherAcademicStatus()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		boolean updateStatus = false;
		try 
		{
			String baseUrl=Utility.getBaseURL(request);
			
			if(!baseUrl.contains("nccloud") && !baseUrl.contains("nc.teachermatch"))
				return true;
			updateStatus = true;
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			List<TeacherDetail> list=new ArrayList<TeacherDetail>();
			list.add(teacherDetail);
			List<TeacherAcademics> teacherAcademicsList = teacherAcademicsDAO.findByTeacherDetailsObj(list);
			if(teacherAcademicsList==null)
			{
				return false;
			}
			else
			{
				return true;
			}
			
		} 
		catch (Exception e) 
		{
			updateStatus = false;
			e.printStackTrace();
		}
		
		return updateStatus;
	}
}
