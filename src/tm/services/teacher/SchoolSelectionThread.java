package tm.services.teacher;

import javax.servlet.http.HttpServletRequest;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.i4.I4QuestionSets;
import tm.bean.user.UserMaster;
import tm.services.report.CGInviteInterviewAjax;



public class SchoolSelectionThread extends Thread{

	private SchoolSelectionAjax schoolSelectionAjax;
	public void setSchoolSelectionAjax(SchoolSelectionAjax schoolSelectionAjax) {
		this.schoolSelectionAjax = schoolSelectionAjax;
	}
	private HttpServletRequest request;
	private UserMaster userMaster;
	
	private TeacherDetail  teacherDetail;
	private JobOrder jobOrder;
	

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	

	public SchoolSelectionThread() {
		super();
	}
	
	public void run()
	{
		try{
			System.out.println(":::::::::Call Here::::::::::::::");
			schoolSelectionAjax.mailToTeacherByThread(jobOrder,teacherDetail,userMaster,request);
			System.out.println("Call Here::::::::::::::");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
