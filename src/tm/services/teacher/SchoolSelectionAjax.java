package tm.services.teacher;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolSelectedByCandidate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.SchoolSelectedByCandidateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.services.PaginationAndSorting;
import tm.services.report.CandidateGridService;
import tm.utility.Utility;

public class SchoolSelectionAjax
{
	 String locale = Utility.getValueOfPropByKey("locale");
	 String lblJoTil=Utility.getLocaleValuePropByKey("lblJoTil", locale);
	 String lblDistrictName=Utility.getLocaleValuePropByKey("lblDistrictName", locale);
	 String lblAppliedOn=Utility.getLocaleValuePropByKey("lblAppliedOn", locale);
	 String lblSltSch=Utility.getLocaleValuePropByKey("lblSltSch", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String msgNorecordfound=Utility.getLocaleValuePropByKey("msgNorecordfound", locale);
	 String lnkAjaxAddEditShl=Utility.getLocaleValuePropByKey("lnkAjaxAddEditShl", locale);
	 String optSeSch=Utility.getLocaleValuePropByKey("optSeSch", locale);
	 String lblSchoolName=Utility.getLocaleValuePropByKey("lblSchoolName", locale);
	 String lblSchoolAddress=Utility.getLocaleValuePropByKey("lblSchoolAddress", locale);
	 String lblLtrOfIntent1=Utility.getLocaleValuePropByKey("lblLtrOfIntent1", locale);
	 String optNA=Utility.getLocaleValuePropByKey("optNA", locale);
	 String btnBack=Utility.getLocaleValuePropByKey("btnBack", locale);
	 
	 
	 
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	@Autowired
	private SchoolMasterDAO schoolMasterDAO; 
	@Autowired
	private SchoolSelectedByCandidateDAO schoolSelectedByCandidateDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	 @Autowired
     private SecondaryStatusDAO secondaryStatusDAO;
     
     @Autowired
     private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;

	public String getSelectedSchoolJobs(String noOfRows,String page,String sortOrderStr,String sortOrderType)
	{
		System.out.println("Calling getSelectedSchoolJobs Ajax");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			String noOfRow =noOfRows; 
			if(noOfRows==null)
				noOfRow="1";

			String pageNo =page;	
			if(page==null)
				pageNo="1";


			int noOfRowInPage =	Integer.parseInt(noOfRow);
			int pgNo =	Integer.parseInt(pageNo);
			String sortOrder =sortOrderStr; 

			System.out.println("sortOrder::::"+sortOrder);

			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;

			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDateTime";
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
			/**End ------------------------------------**/

			List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
			lstJobOrder=jobForTeacherDAO.getJobOrderBySelectedSchool(teacherDetail,sortOrderStrVal, start, noOfRowInPage);
			List<JobOrder> lstJobOrderTotal=jobForTeacherDAO.getJobOrderBySelectedSchoolAll(teacherDetail);
			Map<Integer,List<SchoolSelectedByCandidate>> schoolMap=new HashMap<Integer,List<SchoolSelectedByCandidate>>();
			List<SchoolSelectedByCandidate> schoolSelectedByCandidateTotalList=schoolSelectedByCandidateDAO.getJobOrderByTeacherAndJobList(teacherDetail,lstJobOrder);
			for (SchoolSelectedByCandidate schoolSelectedByCandidate : schoolSelectedByCandidateTotalList) {
				int jobId=schoolSelectedByCandidate.getJobOrder().getJobId();
				List<SchoolSelectedByCandidate> schoolMapList=schoolMap.get(jobId);
				if(schoolMap.get(jobId)==null){
					List<SchoolSelectedByCandidate> schoolList=new ArrayList<SchoolSelectedByCandidate>();
					schoolList.add(schoolSelectedByCandidate);
					schoolMap.put(jobId, schoolList);
				}else{
					schoolMapList.add(schoolSelectedByCandidate);
					schoolMap.put(jobId, schoolMapList);
				}
				
			}
			System.out.println("schoolMap::::::"+schoolMap.size());
			System.out.println("lstJobOrder:::>>:"+lstJobOrder.size());
			System.out.println("lstJobOrderTotal::::>>>>"+lstJobOrderTotal.size());
			totalRecord=lstJobOrderTotal.size();
			if(totalRecord<end)
				end=totalRecord;


			String responseText="";

			sb.append("<table border='0' id='tblGridSelectedSchoolJob'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			responseText=PaginationAndSorting.responseSortingLink(lblJoTil,sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblDistrictName,sortOrderFieldName,"districtMaster",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblAppliedOn,sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");

			sb.append("<th  valign='top'>"+lblSltSch+"</th>");

			sb.append("<th valign='top'>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");

			for(JobOrder jobOrder : lstJobOrder){
				sb.append("<tr>" );
				sb.append("<td>"+jobOrder.getJobTitle()+"</td>");
				sb.append("<td>"+jobOrder.getDistrictMaster().getDistrictName()+"</td>");
				sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getCreatedDateTime())+"</td>");
				if(schoolMap.get(jobOrder.getJobId())==null){
					sb.append("<td><span class='fa-th-list icon-large'></span></td>");
				}else{
					sb.append("<td><span class='fa-th-list icon-large iconcolor'></span></td>");
				}
				sb.append("<td><a href=\"././addselectschool.do?jobId="+jobOrder.getJobId()+"\">"+lnkAjaxAddEditShl+"</a></td>");
				sb.append("</tr>" );
			}
			
			if(lstJobOrder.size()==0){
				sb.append("<tr><td colspan='5'>"+msgNorecordfound+"</td></tr>" );
			}
			sb.append("</table>");
			sb.append("<table>");
			sb.append("<tr>" );
			sb.append("<td colspan=5>" );
			sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			sb.append("</td>" );
			sb.append("</tr>" );
			sb.append("</table>");
		}catch(Exception e){
			
		}
		return sb.toString();
	}

	public String getSelectedSchools(int jobId)
	{
		System.out.println("Calling getSelectedSchools");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try
		{
			System.out.println("jobId::"+jobId);
            JobOrder jobOrder=jobOrderDAO.findById(jobId,false,false);
			int isTeacher=0;
			System.out.println("jobId::"+jobId +" jobOrder.getJobCategoryMaster().getJobCategoryName()===="+jobOrder.getJobCategoryMaster().getJobCategoryName()+" jobOrder.getDistrictMaster().getDistrictId()====="+jobOrder.getDistrictMaster().getDistrictId());
			try{                    
				if(jobOrder.getDistrictMaster().getDistrictId()==4218990 && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Teacher")){
					isTeacher=1;
			    }
			}catch(Exception e){
				 e.printStackTrace();
			}
			sb.append("<input type='hidden' name='isTeacher' value='"+isTeacher+"'>");
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			List<SchoolMaster> attachedSchoolList	= schoolSelectedByCandidateDAO.findSchoolMasters(jobOrder,teacherDetail);
			System.out.println("attachedSchoolList:::;"+attachedSchoolList.size());
			List<SchoolMaster> lstSchoolMaster			= schoolInJobOrderDAO.findSchoolMastersByFilter(jobOrder,attachedSchoolList);
			System.out.println("lstSchoolMaster::::"+lstSchoolMaster.size());
				sb.append("<select id=\"lstSchoolMaster\" name=\"lstSchoolMaster\" class=\"form-control\" onchange=\"setSchoolId(this)\">");
				sb.append("<option value=\"\">"+optSeSch+"</option>");
				for (SchoolMaster schoolMaster : lstSchoolMaster) {
					sb.append("<option value="+schoolMaster.getSchoolId()+">"+schoolMaster.getSchoolName()+"</option>");
				}
				sb.append("</select>");
		}catch(Exception e){
			
		}
		return sb.toString();
	}
	public String saveSelectedSchools(int jobId,String schoolId,String fileName)
	{
		System.out.println("Calling getSelectedSchools:::::::>::::");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		SchoolMaster schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId),false,false);
		JobOrder jobOrder=jobOrderDAO.findById(jobId,false,false);
		JobForTeacher jft = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
		try{
			
			if(jft!=null){
				if(jft.getCandidateConsideration()!=null && jft.getCandidateConsideration()==false){
					jft.setCandidateConsideration(true);
					jobForTeacherDAO.updatePersistent(jft);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
				try{
				    /******************************************For set history for School Selection**********************************************/                
				    SecondaryStatus  secondaryStatusObj =null;
				    secondaryStatusObj=secondaryStatusDAO.findSecondaryStatusByJobOrder(jft.getJobId(),"School Selection");
				    if(secondaryStatusObj!=null){
				       Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				       Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
				       Criterion criterion3 = Restrictions.eq("status","A");
				       Criterion criterion4 = Restrictions.eq("status","S");
				       Criterion criterion5 = Restrictions.or(criterion3,criterion4);
				       Criterion criterion6 = Restrictions.eq("secondaryStatus",secondaryStatusObj);
				       List<TeacherStatusHistoryForJob> historyList=new ArrayList<TeacherStatusHistoryForJob>();
				       historyList=teacherStatusHistoryForJobDAO.findByCriteria(criterion1,criterion2,criterion5,criterion6);
				       System.out.println(":historyList::>>>>>>>>>>..:"+historyList.size());
					   if(historyList.size()==0){
					       DistrictMaster districtMaster=jobOrder.getDistrictMaster();
					       StatusMaster statusMasterCC=null;
					       SecondaryStatus secondaryStatusCC=null;
					       try{
					           if(districtMaster.getStatusMasterForCC()!=null && districtMaster.getSecondaryStatusForCC()==null){
					               statusMasterCC=districtMaster.getStatusMasterForCC();
					           }else if(districtMaster.getStatusMasterForCC()==null && districtMaster.getSecondaryStatusForCC()!=null){
					               secondaryStatusCC=districtMaster.getSecondaryStatusForCC();
					           }
					       }catch(Exception e){
					           e.printStackTrace();
					       }
					       System.out.println("statusMasterCC===="+statusMasterCC+"    secondaryStatusCC===="+secondaryStatusCC);
					       List<TeacherStatusHistoryForJob> checkSchoolSelectionHistoryList=null;
					       TeacherStatusHistoryForJob teacherStatusHistoryForJobforUserMaster=null;
					       if(statusMasterCC!=null){
					           checkSchoolSelectionHistoryList=teacherStatusHistoryForJobDAO.findByCriteria(criterion1,criterion2,criterion5,Restrictions.eq("statusMaster",statusMasterCC)); 
					       }else if(secondaryStatusCC!=null){
					           checkSchoolSelectionHistoryList=teacherStatusHistoryForJobDAO.findByCriteria(criterion1,criterion2,criterion5,Restrictions.eq("secondaryStatus",secondaryStatusCC));
					       }
					       if(checkSchoolSelectionHistoryList!=null && checkSchoolSelectionHistoryList.size()>0){
					           teacherStatusHistoryForJobforUserMaster= checkSchoolSelectionHistoryList.get(0);
					       }
				    	   try{
				              TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
				              tSHJ.setTeacherDetail(teacherDetail);
				              tSHJ.setJobOrder(jobOrder);
				              if(secondaryStatusObj.getStatusMaster()!=null){
				            	  tSHJ.setStatusMaster(secondaryStatusObj.getStatusMaster());
				              }else{
				            	  tSHJ.setSecondaryStatus(secondaryStatusObj);
				              }
				              tSHJ.setStatus("S");
				              tSHJ.setCreatedDateTime(new Date());
				              tSHJ.setUserMaster(teacherStatusHistoryForJobforUserMaster!=null?teacherStatusHistoryForJobforUserMaster.getUserMaster():null);
				              teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
				    	   }catch(Exception e){
				    		   e.printStackTrace();
				    	   }
					       /******************************************For Set Status Or Not**********************************************/
	
						    List<TeacherStatusHistoryForJob> historyList1=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jft.getTeacherId(),jft.getJobId());
		                    Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
	
		                    Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
		                    List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
		                    LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
		                    Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
		                    Map<String,String> cmpMap = new HashMap<String, String>();
		                    Map<Integer,List<String>> statusHistoryMap=new HashMap<Integer, List<String>>();
		                    boolean flagForJFTUpdateOrNot=true;
		                    System.out.println("historyList1================"+historyList1.size());
		                    List<String> statusList=new ArrayList<String>();
		                    for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList1) {
		                          if(teacherStatusHistoryForJob.getSecondaryStatus()!=null){           
		                                cmpMap.put("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId(),teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName());
		                          }
		                          if(teacherStatusHistoryForJob.getStatusMaster()!=null && !teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equalsIgnoreCase("vcomp"))
		                          {
		                                cmpMap.put(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0",teacherStatusHistoryForJob.getStatusMaster().getStatus());
		                          }
		                          if(statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null){
										statusList=new ArrayList<String>();
										if(teacherStatusHistoryForJob.getStatusMaster()!=null){
											statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
										}else{
											statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
										}
										statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
									}else{
										statusList=statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
										if(teacherStatusHistoryForJob.getStatusMaster()!=null){
											statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
										}else{
											statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
										}
										statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
									}
		                    }           
		                    try{
		                          int counter=0; 
		                          for(SecondaryStatus tree1: lstTreeStructure){
		                                if(tree1.getSecondaryStatus()==null){
		                                      if(tree1.getChildren().size()>0){                                                                           
		                                            counter=new CandidateGridService().getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree1.getChildren(),statusHistMap,statusNameMap,counter);
		                                      }
		                                }
		                          }
		                          boolean flagforcheckingStrart=false;
		                          System.out.println("statusNameMap======="+statusNameMap.size());
		                          for(Map.Entry<String,String> entry:statusNameMap.entrySet()){                                                                      
		                                String id[]=entry.getKey().toString().split("##");
		                                if(flagforcheckingStrart){
		                                      if(cmpMap.get(entry.getKey().toString())!=null){
		                                            flagForJFTUpdateOrNot=false;
		                                            break;
		                                      }                                                           
		                                }
		                                if(entry.getValue().toString().equalsIgnoreCase(secondaryStatusObj.getSecondaryStatusName()))
		                                      flagforcheckingStrart=true;                                             
		                          }     
		                          int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jft) ;
				                  StatusMaster statusMasterMain = assessmentDetailDAO.findByTeacherIdJobStaus(jft,flagList);
		                          if(flagForJFTUpdateOrNot){
		                        	  StatusMaster statusMasterObj=null;
		                        	  List<String> statusIds=statusHistoryMap.get(jft.getTeacherId().getTeacherId());
		                        	  String schoolStatusId=new CandidateGridService().getSchoolStatusId(statusIds, statusHistMap, statusNameMap);
		                        	  try{
											String statusIdMapValue[]=schoolStatusId.split("#");
											System.out.println("statusIdMapValue[]==="+statusIdMapValue);
											if(statusIdMapValue[0].equals("0") && statusIdMapValue[1].equals("0")){
												jft.setStatusMaster(statusMasterMain);												
												jft.setStatus(statusMasterMain);
												jft.setSecondaryStatus(null);
											}
											else if(statusIdMapValue[0].equals("0")){
													secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
													jft.setSecondaryStatus(secondaryStatusObj);												
													jft.setStatus(statusMasterMain);
													jft.setStatusMaster(null);
											}else{
													statusMasterObj=statusMasterMap.get(schoolStatusId);
													jft.setStatusMaster(statusMasterObj);												
													jft.setStatus(statusMasterObj);
													jft.setSecondaryStatus(null);																								
											}
											jobForTeacherDAO.updatePersistent(jft);
										}catch(Exception e){e.printStackTrace();}
		                          }
	
		                    }catch(Exception e){e.printStackTrace();}
	
		                    /******************************************end code Set Status**********************************************/
					       }
				 }

                /******************************************end history for school Selection**********************************************/
				SchoolSelectedByCandidate obj=new SchoolSelectedByCandidate();
				obj.setDistrictMaster(jobOrder.getDistrictMaster());
				obj.setSchoolMaster(schoolMaster);
				obj.setSchoolSelectionDate(new Date());
				obj.setJobOrder(jobOrder);
				obj.setTeacherDetail(teacherDetail);
				obj.setSchoolSelectionDate(new Date());
				obj.setLetterOfIntent(fileName);
				schoolSelectedByCandidateDAO.makePersistent(obj);
				
		}catch(Exception e){
			
		}
		return sb.toString();
	}
	public String getSelectedSchoolsByJob(String jobId,String noOfRows,String page,String sortOrderStr,String sortOrderType)
	{
		System.out.println("Calling getSelectedSchoolsByJob Ajax::"+jobId);

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			String noOfRow =noOfRows;
			if(noOfRows==null)
				noOfRow="1";

			String pageNo =page;	
			if(page==null)
				pageNo="1";


			int noOfRowInPage =	Integer.parseInt(noOfRow);
			int pgNo =	Integer.parseInt(pageNo);
			String sortOrder =sortOrderStr; 

			System.out.println("sortOrder::::"+sortOrder);

			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;

			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDateTime";
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
			/**End ------------------------------------**/

			JobOrder jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
			List<SchoolSelectedByCandidate> schoolSelectedByCandidateList=new ArrayList<SchoolSelectedByCandidate>();
			schoolSelectedByCandidateList=schoolSelectedByCandidateDAO.getJobOrderBySelectedSchool(teacherDetail,jobOrder,sortOrderStrVal, start, noOfRowInPage);
			List<SchoolSelectedByCandidate> schoolSelectedByCandidateTotalList=schoolSelectedByCandidateDAO.getJobOrderBySelectedSchoolForAll(teacherDetail,jobOrder);
			System.out.println("schoolSelectedByCandidateList:::>>:"+schoolSelectedByCandidateList.size());
			System.out.println("schoolSelectedByCandidateTotalList::::>>>>"+schoolSelectedByCandidateTotalList.size());
			totalRecord=schoolSelectedByCandidateTotalList.size();
			SchoolMaster schoolMaster=null;
			JobForTeacher jobForTeacher=jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail,jobOrder);
			if(jobForTeacher!=null){
				if(jobForTeacher.getSchoolMaster()!=null){
					schoolMaster=jobForTeacher.getSchoolMaster();
				}
			}
			if(totalRecord<end)
				end=totalRecord;

			String responseText="";

			sb.append("<table border='0' id='tblGridSelectedSchoolByJob'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th  valign='top'>"+lblJoTil+"</th>");

			sb.append("<th  valign='top'>"+lblAppliedOn+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblSchoolName,sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");

			sb.append("<th  valign='top'>"+lblLtrOfIntent1+"</th>");

			sb.append("<th valign='top'>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			for(SchoolSelectedByCandidate schoolSelectedByCandidate : schoolSelectedByCandidateList){
				sb.append("<tr>" );
				sb.append("<td>"+schoolSelectedByCandidate.getJobOrder().getJobTitle()+"</td>");
				sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(schoolSelectedByCandidate.getSchoolSelectionDate())+"</td>");
				sb.append("<td>"+schoolSelectedByCandidate.getSchoolMaster().getSchoolName()+"</td>");
				if(schoolSelectedByCandidate.getLetterOfIntent()!=null && !schoolSelectedByCandidate.getLetterOfIntent().equalsIgnoreCase("")){
					sb.append("<td  style='padding-left:40px;'><a href=\"#\" id=\"school"+schoolSelectedByCandidate.getSelectedSchoolId()+"\" onclick=\"showLetterOfIntent("+schoolSelectedByCandidate.getJobOrder().getJobId()+","+schoolSelectedByCandidate.getSchoolMaster().getSchoolId()+",'school"+schoolSelectedByCandidate.getSelectedSchoolId()+"');"+windowFunc+"\" ><span class='fa-file icon-large'></span></a></td>");
				}else{
					sb.append("<td   style='padding-left:40px;'>&nbsp;</td>");
				}
				
				if(schoolMaster!=null  &&  schoolSelectedByCandidate.getSchoolMaster().getSchoolId().equals(schoolMaster.getSchoolId())){
					sb.append("<td>&nbsp;</td>");
				}else{
					sb.append("<td><a href=\"#\" onclick=\"return removeSchool("+schoolSelectedByCandidate.getJobOrder().getJobId()+","+schoolSelectedByCandidate.getSchoolMaster().getSchoolId()+");\">Remove School</a></td>");
				}
				sb.append("</tr>" );
			}
			if(schoolSelectedByCandidateList.size()==0){
				sb.append("<tr><td colspan='5'>"+msgNorecordfound+"</td></tr>" );
			}
			sb.append("</table>");
			sb.append("<table>");
			sb.append("<tr>" );
			sb.append("<td colspan=5>" );
			sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			sb.append("</td>" );
			sb.append("</tr>" );
			sb.append("<tr>" );
			sb.append("<td colspan=5>" );
			sb.append("<a href=\"selectschool.do\" >"+btnBack+"</a>");
			sb.append("</td>" );
			sb.append("</tr>" );
			sb.append("</table>");
		}catch(Exception e){
			
		}
		return sb.toString();
	}
	public String removeSelectedSchool(int jobId,String schoolId)
	{
		System.out.println("Calling removeSelectedSchool:::::::>::::");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			System.out.println("jobId::"+jobId);
			
			System.out.println("schoolId::"+schoolId);
			
			SchoolMaster schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId),false,false);
			JobOrder jobOrder=jobOrderDAO.findById(jobId,false,false);
			SchoolSelectedByCandidate removeObj=schoolSelectedByCandidateDAO.getSchoolSelectedByCandidate(teacherDetail, jobOrder, schoolMaster);
			if(removeObj!=null){
				
				try{
					File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/"+jobId+"/"+schoolId+"/"+removeObj.getLetterOfIntent());
					if(file.delete()){
						System.out.println(file.getName() + " is deleted!");
					}else{
						System.out.println("Delete operation is failed.");
					}
				}catch(Exception e){}
				schoolSelectedByCandidateDAO.makeTransient(removeObj);
			}
				
		}catch(Exception e){
			
		}
		return sb.toString();
	}
	public String showLetterOfIntent(int jobId,int schoolId)
	{
		System.out.println("::::::::::showLetterOfIntent::::::::");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String path="";
		String source="";
		String target="";
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			SchoolMaster schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
			JobOrder jobOrder=jobOrderDAO.findById(jobId,false,false);
			SchoolSelectedByCandidate Obj=schoolSelectedByCandidateDAO.getSchoolSelectedByCandidate(teacherDetail, jobOrder, schoolMaster);
			int teacherId=teacherDetail.getTeacherId();
			String  docFileName=Obj.getLetterOfIntent();
			source=Utility.getValueOfPropByKey("teacherRootPath")+teacherId+"/"+jobId+"/"+schoolId+"/"+docFileName;
			System.out.println("source:::::::"+source);
			target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherId+"/"+jobId+"/"+schoolId+"/";
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
			path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherId+"/"+jobId+"/"+schoolId+"/"+docFileName;
			System.out.println("path:::::"+path);
		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
		}

		return path;
	}
	/*** Add by Sekhar, This method using for send automatically mail ( Staus Updated )***/
	public int mailToTeacherByThread(JobOrder jobOrder, TeacherDetail teacherDetail,UserMaster userMaster,HttpServletRequest request)
	{
		System.out.println("========== mailToTeacherByThread ============");
		try{
			String templatetoSend= MailText.mailSendToTeacherForSchoolSelection(request,jobOrder,teacherDetail);
			String subject="Congratulations! Instructions to apply for school specific vacancies";
			try{
				emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),subject,templatetoSend);
				System.out.println("Mail set successfully");
			}catch (Exception e) {
				e.printStackTrace();
			}
		}catch(Exception e)
		{
		    e.printStackTrace();
		}
		return 0;
	}
	public String getSelectedSchoolsList(Integer jobId,Integer teacherId,String noOfRows,String page,String sortOrderStr,String sortOrderType)
	{
		System.out.println("Calling getSelectedSchoolsList Ajax::"+jobId);

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		if(session == null ||(session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try
		{
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId,false,false);

			String noOfRow =noOfRows;
			if(noOfRows==null)
				noOfRow="1";

			String pageNo =page;	
			if(page==null)
				pageNo="1";


			int noOfRowInPage =	Integer.parseInt(noOfRow);
			int pgNo =	Integer.parseInt(pageNo);
			String sortOrder =sortOrderStr; 

			System.out.println("sortOrder::::"+sortOrder);

			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;

			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDateTime";
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
			/**End ------------------------------------**/

			JobOrder jobOrder=jobOrderDAO.findById(jobId, false,false);
			List<SchoolSelectedByCandidate> schoolSelectedByCandidateList=new ArrayList<SchoolSelectedByCandidate>();
			schoolSelectedByCandidateList=schoolSelectedByCandidateDAO.getJobOrderBySelectedSchool(teacherDetail,jobOrder,sortOrderStrVal, start, noOfRowInPage);
			List<SchoolSelectedByCandidate> schoolSelectedByCandidateTotalList=schoolSelectedByCandidateDAO.getJobOrderBySelectedSchoolForAll(teacherDetail,jobOrder);
			System.out.println("schoolSelectedByCandidateList:::>>:"+schoolSelectedByCandidateList.size());
			System.out.println("schoolSelectedByCandidateTotalList::::>>>>"+schoolSelectedByCandidateTotalList.size());
			totalRecord=schoolSelectedByCandidateTotalList.size();
			SchoolMaster schoolMaster=null;
			JobForTeacher jobForTeacher=jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail,jobOrder);
			if(jobForTeacher!=null){
				if(jobForTeacher.getSchoolMaster()!=null){
					schoolMaster=jobForTeacher.getSchoolMaster();
				}
			}
			if(totalRecord<end)
				end=totalRecord;

			String responseText="";

			sb.append("<table border='0' id='tblGridSelectedSchool'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th  valign='top'>"+lblAppliedOn+"</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblSchoolName,sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");
			sb.append("<th  valign='top'>"+lblSchoolAddress+"</th>");
			sb.append("<th  valign='top'>"+lblLtrOfIntent1+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			for(SchoolSelectedByCandidate schoolSelectedByCandidate : schoolSelectedByCandidateList){
				sb.append("<tr>" );
				sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(schoolSelectedByCandidate.getSchoolSelectionDate())+"</td>");
				sb.append("<td>"+schoolSelectedByCandidate.getSchoolMaster().getSchoolName()+"</td>");
				String schoolAddress=optNA;
				try{
					schoolAddress=schoolSelectedByCandidate.getSchoolMaster().getAddress();
				}catch(Exception e){
					e.printStackTrace();
				}
				sb.append("<td>"+schoolAddress+"</td>");
				if(schoolSelectedByCandidate.getLetterOfIntent()!=null && !schoolSelectedByCandidate.getLetterOfIntent().equalsIgnoreCase("")){
					sb.append("<td style='padding-left:40px;'><a href=\"javascript:void(0)\"  id=\"school"+schoolSelectedByCandidate.getSelectedSchoolId()+"\" onclick=\"showLetterOfIntent("+schoolSelectedByCandidate.getJobOrder().getJobId()+","+schoolSelectedByCandidate.getSchoolMaster().getSchoolId()+","+schoolSelectedByCandidate.getTeacherDetail().getTeacherId()+",'school"+schoolSelectedByCandidate.getSelectedSchoolId()+"');"+windowFunc+"\" ><span class='fa-file icon-large'></span></a></td>");
				}else{
					sb.append("<td style='padding-left:40px;'>&nbsp;</td>");
				}
				sb.append("</tr>" );
			}
			if(schoolSelectedByCandidateList.size()==0){
				sb.append("<tr><td colspan='5'>"+msgNorecordfound+"</td></tr>" );
			}
			sb.append("</table>");
			sb.append("<table width=670>");
			sb.append("<tr>" );
			sb.append("<td colspan=5>" );
			sb.append(PaginationAndSorting.getPaginationTrippleGrid(request,totalRecord,noOfRow, pageNo));
			//sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			sb.append("</td>" );
			sb.append("</tr>" );
			sb.append("</table>");
		}catch(Exception e){
			
		}
		return sb.toString();
	}
	public String showLetterOfIntentForCG(int jobId,int schoolId,int teacherId)
	{
		System.out.println("::::::::::showLetterOfIntentForCG::::::::");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String path="";
		String source="";
		String target="";
		try 
		{
			TeacherDetail teacherDetail =teacherDetailDAO.findById(teacherId,false,false);
			SchoolMaster schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
			JobOrder jobOrder=jobOrderDAO.findById(jobId,false,false);
			SchoolSelectedByCandidate Obj=schoolSelectedByCandidateDAO.getSchoolSelectedByCandidate(teacherDetail, jobOrder, schoolMaster);
			String  docFileName=Obj.getLetterOfIntent();
			source=Utility.getValueOfPropByKey("teacherRootPath")+teacherId+"/"+jobId+"/"+schoolId+"/"+docFileName;
			System.out.println("source:::::::"+source);
			target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherId+"/"+jobId+"/"+schoolId+"/";
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
			path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherId+"/"+jobId+"/"+schoolId+"/"+docFileName;
			System.out.println("path:::::"+path);
		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
		}

		return path;
	}
}	