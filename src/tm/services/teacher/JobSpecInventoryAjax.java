package tm.services.teacher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.master.StatusMasterDAO;
import tm.services.PaginationAndSorting;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

public class JobSpecInventoryAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	 String lblJoTil=Utility.getLocaleValuePropByKey("lblJoTil", locale);
	 String lblLocti=Utility.getLocaleValuePropByKey("lblLocti", locale);
	 String optDistrict=Utility.getLocaleValuePropByKey("optDistrict", locale);
	 String optSchool=Utility.getLocaleValuePropByKey("optSchool", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String msgNorecordfound=Utility.getLocaleValuePropByKey("msgNorecordfound", locale);
	 
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) 
	{
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) 
	{
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}
	/* @Author: Gagan 
	 * @Discription: It is used to displayNotesGrid in editdistrict Page in Account information  .
	 */
	public String displayJobSpecInventoryGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer jsInvtRecords =	new StringBuffer();
		
		try{
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			System.out.println("start"+start);
			System.out.println("end"+end);
			
			/***** Sorting by Domain Name,Competency Name and Objective Name Stating ( Sekhar ) ******/
			SortedMap map = new TreeMap();
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"jobId";
			String sortOrderNoField		=	"jobId";
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("jobTitle")&& !sortOrder.equals("location")  && !sortOrder.equals("districtName")&& !sortOrder.equals("schoolName")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("jobTitle"))
				 {
					 sortOrderNoField="jobTitle";
				 }
				 if(sortOrder.equals("location"))
				 {
					 sortOrderNoField="location";
				 }
				 if(sortOrder.equals("districtName"))
				 {
					 sortOrderNoField="districtName";
				 }
				 if(sortOrder.equals("schoolName"))
				 {
					 sortOrderNoField="schoolName";
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			List<JobForTeacher> lstJobForTeacher = null;
			//map.addAttribute("lstJobForTeacher", lstJobForTeacher);
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			List<JobOrder> lstjobOrder = new ArrayList<JobOrder>();
			
			Map<Integer, JobForTeacher> map1= new HashMap<Integer, JobForTeacher>();
			//lstJobForTeacher = jobForTeacherDAO.findJobsForJobSpecificInventory(teacherDetail,statusInComplete,true);
			lstJobForTeacher = jobForTeacherDAO.findJobListForJobSpecificInventory(teacherDetail,true);
			if(lstJobForTeacher.size()!=0){
			List<JobForTeacher> lstTempJobForTeacher = new ArrayList<JobForTeacher>(lstJobForTeacher);
			for (JobForTeacher jobForTeacher : lstTempJobForTeacher) {
				lstjobOrder.add(jobForTeacher.getJobId());
				map1.put(jobForTeacher.getJobId().getJobId(), jobForTeacher);
			}
			
			Map<Integer, Integer> jobAss = new HashMap<Integer, Integer>();
			List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
			List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 
			Map<Integer, Integer> techeridAssessmentId= new HashMap<Integer, Integer>();
			List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(lstjobOrder);
			
			if(assessmentJobRelations1.size()>0){
				for(AssessmentJobRelation ajr: assessmentJobRelations1){
					techeridAssessmentId.put(ajr.getAssessmentId().getAssessmentId(),ajr.getJobId().getJobId());
					adList.add(ajr.getAssessmentId());
				}
				if(adList.size()>0)
					lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentList(teacherDetail,adList);
			}
			
			for (TeacherAssessmentStatus teacherAssessmentStatus : lstJSI) {
				jobAss.put(teacherAssessmentStatus.getAssessmentDetail().getAssessmentId(),teacherAssessmentStatus.getJobOrder().getJobId());
				}
			
				for (AssessmentJobRelation jobRelObj : assessmentJobRelations1){
					int a=0;
					try{
					a=jobAss.get(jobRelObj.getAssessmentId().getAssessmentId());
					}catch(Exception e){
						//e.printStackTrace();
					}
					int b=techeridAssessmentId.get(jobRelObj.getAssessmentId().getAssessmentId());
					if(a==b)
					{
						lstJobForTeacher.remove(map1.get(jobRelObj.getJobId().getJobId()));
					}
				}
			}
			
			List<JobForTeacher> sortedlstJobForTeacher	=	new ArrayList<JobForTeacher>();
			
			SortedMap<String,JobForTeacher>	sortedMap = new TreeMap<String,JobForTeacher>();
			if(sortOrderNoField.equals("jobTitle"))
			{
				sortOrderFieldName	=	"jobTitle";
			}
			if(sortOrderNoField.equals("location"))
			{
				sortOrderFieldName	=	"location";
			}
			if(sortOrderNoField.equals("districtName"))
			{
				sortOrderFieldName	=	"districtName";
			}
			if(sortOrderNoField.equals("schoolName"))
			{
				sortOrderFieldName	=	"schoolName";
			}
			int mapFlag=2;
			System.out.println("***************** sortOrderFieldName "+sortOrderFieldName+"**********************");
			int t=0;
			for (JobForTeacher jb : lstJobForTeacher){
				String orderFieldName=jb.getJobForTeacherId()+"";
				if(sortOrderFieldName.equals("jobTitle")){
					orderFieldName=jb.getJobId().getJobTitle()+"||"+jb.getJobForTeacherId();
					sortedMap.put(orderFieldName+"||",jb);
					System.out.println("*******   Job Title ******** "+jb.getJobId().getJobTitle()+" ************ Job Id "+jb.getJobId().getJobId());
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("location")){
					if(jb.getJobId().getCreatedForEntity()!=3)
					{
						orderFieldName=jb.getJobId().getDistrictMaster().getAddress()+"||"+jb.getJobForTeacherId();
						System.out.println("******** Location  district Address "+jb.getJobId().getDistrictMaster().getAddress()+"*******   Job Title ******** "+jb.getJobId().getJobTitle()+" ************ Job Id "+jb.getJobId().getJobId());
					}
					else
					{
						orderFieldName=jb.getJobId().getSchool().get(0).getAddress()+"||"+jb.getJobForTeacherId();
						System.out.println("******* Location  School Address "+jb.getJobId().getSchool().get(0).getAddress()+" School Name "+jb.getJobId().getSchool().get(0).getSchoolName()+"*******   Job Title ******** "+jb.getJobId().getJobTitle()+" ************ Job Id "+jb.getJobId().getJobId());
					}
					sortedMap.put(orderFieldName+"||",jb);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("districtName")){
					orderFieldName=jb.getJobId().getDistrictMaster().getDistrictName()+"||"+jb.getJobForTeacherId();
					sortedMap.put(orderFieldName+"||",jb);
					System.out.println("*******   District Name ******** "+jb.getJobId().getJobTitle()+" ************ Job Id "+jb.getJobId().getJobId());
					if(sortOrderTypeVal.equals("0")){
						mapFlag=1;
					}else{
						mapFlag=0;
					}
				}
				if(sortOrderFieldName.equals("schoolName")){
					if(jb.getJobId().getCreatedForEntity()==3)
					{
						orderFieldName=jb.getJobId().getSchool().get(0).getSchoolName()+"||"+jb.getJobForTeacherId();
						System.out.println("*****orderFieldName ::"+orderFieldName);
						sortedMap.put(orderFieldName+"||",jb);
					}
					else
					{
						orderFieldName="0000"+"||"+jb.getJobForTeacherId();
						sortedMap.put(orderFieldName+"||",jb);
						System.out.println("*****orderFieldName ::"+orderFieldName);
						
					}
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstJobForTeacher.add((JobForTeacher) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstJobForTeacher.add((JobForTeacher) sortedMap.get(key));
				}
			}else{
				sortedlstJobForTeacher=lstJobForTeacher;
			}
			
			totalRecord =sortedlstJobForTeacher.size();

			if(totalRecord<end)
				end=totalRecord;
			List<JobForTeacher> lstsortedObjectiveMaster		=	sortedlstJobForTeacher.subList(start,end);
			
			
			int rowCount=0;
			jsInvtRecords.append("<table border='0' id='tblGridJSI' class='table table-bordered table-striped' >");
			jsInvtRecords.append("<thead class='bg'>");
			jsInvtRecords.append("<tr>");			
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblJoTil,sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
			jsInvtRecords.append("<th width='22%' valign='top'>"+responseText+"</th>");
			

			responseText=PaginationAndSorting.responseSortingLink(lblLocti,sortOrderFieldName,"location",sortOrderTypeVal,pgNo);
			jsInvtRecords.append("<th width='22%' valign='top'>"+responseText+"</th>");
			

			responseText=PaginationAndSorting.responseSortingLink(optDistrict,sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
			jsInvtRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
			

			responseText=PaginationAndSorting.responseSortingLink(optSchool,sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
			jsInvtRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
			jsInvtRecords.append("<th>"+lblAct+"</th>");
			jsInvtRecords.append("</tr>");
			jsInvtRecords.append("</thead>");			
			if(lstsortedObjectiveMaster.size()==0)
				jsInvtRecords.append("<tr><td colspan='6'>"+msgNorecordfound+"</td></tr>" );
			System.out.println("No of Records "+lstJobForTeacher.size());

			int jobForTeachercount	=	0;
			
			
			TeacherPortfolioStatus tPortfolioStatus;
			boolean portfolioStatus = false;
			tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(tPortfolioStatus!=null && tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted())
			{
				portfolioStatus = true;
			}
			
			for (JobForTeacher jbForTeacher : lstsortedObjectiveMaster) 
			{
				rowCount++;				
				jsInvtRecords.append("<tr>" );
				jsInvtRecords.append("<td>"+jbForTeacher.getJobId().getJobTitle()+"</td>");
					if(jbForTeacher.getJobId().getCreatedForEntity()==3)
					{
						
						for (SchoolMaster jbSchool : jbForTeacher.getJobId().getSchool()){
							jobForTeachercount=1;
							jsInvtRecords.append("<td>"+jbSchool.getAddress()+"</td>");
							
							if(!jbForTeacher.getJobId().getDistrictMaster().getDisplayName().isEmpty())	{
								jsInvtRecords.append("<td>"+jbForTeacher.getJobId().getDistrictMaster().getDisplayName()+"</td>");							
							}
							else{
								jsInvtRecords.append("<td>"+jbForTeacher.getJobId().getDistrictMaster().getDistrictName()+"</td>");							
							}
							jsInvtRecords.append("<td>"+jbSchool.getSchoolName()+"</td>");							
						}
						if(jobForTeachercount==0)
						{
							jsInvtRecords.append("<td></td>");
							jsInvtRecords.append("<td></td>");
							jsInvtRecords.append("<td></td>");
						}
						jobForTeachercount=0;
					}
					else
					{
						jsInvtRecords.append("<td>"+jbForTeacher.getJobId().getDistrictMaster().getAddress()+"</td>");
						if(!jbForTeacher.getJobId().getDistrictMaster().getDisplayName().isEmpty())
						{
							jsInvtRecords.append("<td>"+jbForTeacher.getJobId().getDistrictMaster().getDisplayName()+"</td>");
						}
						else
						{
							jsInvtRecords.append("<td>"+jbForTeacher.getJobId().getDistrictMaster().getDistrictName()+"</td>");						
						}
						jsInvtRecords.append("<td></td>");
					}
				jsInvtRecords.append("<td>");
				if(portfolioStatus){
					//jsInvtRecords.append("<a data-original-title='Complete Now' rel='tooltip' id='iconpophover"+rowCount+"' href='javascript:void(0)' onclick='checkInventory("+jbForTeacher.getJobId().getJobId()+");' >");
					jsInvtRecords.append("<a data-original-title='Complete Now' rel='tooltip' id='iconpophover"+rowCount+"' href='javascript:void(0)' onclick='getTeacherCriteria("+jbForTeacher.getJobId().getJobId()+");' >");
					jsInvtRecords.append("<img src='images/option02.png'>");
					jsInvtRecords.append("</a>");
					
				}
				else{
					//jsInvtRecords.append("<a data-original-title='Complete Now' rel='tooltip' id='iconpophover4' href='#' onclick=\"chkJobSpecificInventory(true,"+portfolioStatus+");\">");
					jsInvtRecords.append("<a data-original-title='Complete Now' rel='tooltip' id='iconpophover"+rowCount+"' href='javascript:void(0)' onclick='getTeacherCriteria("+jbForTeacher.getJobId().getJobId()+");' >");
					jsInvtRecords.append("<img src='images/option02.png'>");
					jsInvtRecords.append("</a>");
				}
				jsInvtRecords.append("</tr>");
				jsInvtRecords.append("</td>");
			}
			jsInvtRecords.append("</table>");
			jsInvtRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}
		catch (Exception e){
			e.printStackTrace();
		}
		System.out.println(jsInvtRecords.toString());
		return jsInvtRecords.toString();
	}
}
