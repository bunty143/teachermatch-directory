package tm.services.teacher;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.StafferMaster;
import tm.bean.TeacherDetail;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.OnlineActivityFitScore;
import tm.bean.master.PanelSchedule;
import tm.bean.master.SchoolMaster;
import tm.bean.teacher.OnlineActivityAnswers;
import tm.bean.teacher.OnlineActivityNotes;
import tm.bean.teacher.OnlineActivityOptions;
import tm.bean.teacher.OnlineActivityQuestionSet;
import tm.bean.teacher.OnlineActivityQuestions;
import tm.bean.teacher.OnlineActivityStatus;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.SchoolSelectedByCandidateDAO;
import tm.dao.StafferMasterDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherSecondaryStatusDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.teacher.OnlineActivityFitScoreDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.teacher.OnlineActivityAnswersDAO;
import tm.dao.teacher.OnlineActivityNotesDAO;
import tm.dao.teacher.OnlineActivityOptionsDAO;
import tm.dao.teacher.OnlineActivityQuestionSetDAO;
import tm.dao.teacher.OnlineActivityQuestionsDAO;
import tm.dao.teacher.OnlineActivityStatusDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.utility.Utility;

public class OnlineActivityAjax
{
	
	 String locale = Utility.getValueOfPropByKey("locale");
	 String msgDIRECTIONSTOCANDIDATE1=Utility.getLocaleValuePropByKey("msgDIRECTIONSTOCANDIDATE1", locale);
	 String msgDIRECTIONSTOCANDIDATE2=Utility.getLocaleValuePropByKey("msgDIRECTIONSTOCANDIDATE2", locale);
	 String msgDIRECTIONSTOCANDIDATE3=Utility.getLocaleValuePropByKey("msgDIRECTIONSTOCANDIDATE3", locale);
	 String msgDATAEXERCISECONTEXT1=Utility.getLocaleValuePropByKey("msgDATAEXERCISECONTEXT1", locale);
	 String msgDATAEXERCISECONTEXT2=Utility.getLocaleValuePropByKey("msgDATAEXERCISECONTEXT2", locale);
	 String lblPlease=Utility.getLocaleValuePropByKey("lblPlease", locale);
	 String lnkClickHere1=Utility.getLocaleValuePropByKey("lnkClickHere1", locale);
	 String msgreviewToRespondToquest=Utility.getLocaleValuePropByKey("msgreviewToRespondToquest", locale);
	 String lblQues=Utility.getLocaleValuePropByKey("lblQues", locale);
	 String lblChk=Utility.getLocaleValuePropByKey("lblChk", locale);
	 String msgOnlineActivity1=Utility.getLocaleValuePropByKey("msgOnlineActivity1", locale);
	 String msgOnlineActivity2=Utility.getLocaleValuePropByKey("msgOnlineActivity2", locale);
	 String msgOnlineActivity3=Utility.getLocaleValuePropByKey("msgOnlineActivity3", locale);
	 String msgOnlineActivity4=Utility.getLocaleValuePropByKey("msgOnlineActivity4", locale);
	 String lblQuesSet=Utility.getLocaleValuePropByKey("lblQuesSet", locale);
	 String lblScr=Utility.getLocaleValuePropByKey("lblScr", locale);
	 String lblResponse=Utility.getLocaleValuePropByKey("lblResponse", locale);
	 String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	 String lblDate=Utility.getLocaleValuePropByKey("lblDate", locale);
	 String lblAct =Utility.getLocaleValuePropByKey("lblAct", locale);
	 String optNA =Utility.getLocaleValuePropByKey("optNA", locale);
	 String btnResend =Utility.getLocaleValuePropByKey("btnResend", locale);
	 String lblPending =Utility.getLocaleValuePropByKey("lblPending", locale);
	 String btnSend =Utility.getLocaleValuePropByKey("btnSend", locale);
	 String msgNoQuestion =Utility.getLocaleValuePropByKey("msgNoQuestion", locale);
	 String msgOnlineActivityCompleted =Utility.getLocaleValuePropByKey("msgOnlineActivityCompleted", locale);
	 String msgInvitationOnlineActivity =Utility.getLocaleValuePropByKey("msgInvitationOnlineActivity", locale);
	 String lblLetterCommitment =Utility.getLocaleValuePropByKey("lblLetterCommitment", locale);
	 String lblEvaluationComplete =Utility.getLocaleValuePropByKey("lblEvaluationComplete", locale);
	 String lblTeacheracceptedposition =Utility.getLocaleValuePropByKey("lblTeacheracceptedposition", locale);
	 String lblTeacherdeclinedpos =Utility.getLocaleValuePropByKey("lblTeacherdeclinedpos", locale);
	 String msgYrSesstionExp =Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String blNote =Utility.getLocaleValuePropByKey("blNote", locale);
	 String lblCreatedBy =Utility.getLocaleValuePropByKey("lblCreatedBy", locale);
	 String lblNoNoteAdded =Utility.getLocaleValuePropByKey("lblNoNoteAdded", locale);
	 
	 
	@Autowired
	OnlineActivityFitScoreDAO onlineActivityFitScoreDAO;
	
	@Autowired
	private OnlineActivityNotesDAO onlineActivityNotesDAO;
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO; 
	@Autowired
	private SchoolSelectedByCandidateDAO schoolSelectedByCandidateDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private OnlineActivityQuestionsDAO onlineActivityQuestionsDAO;
	
	@Autowired
	private OnlineActivityAnswersDAO onlineActivityAnswersDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private  TeacherSecondaryStatusDAO teacherSecondaryStatusDAO;
	
	@Autowired
	private OnlineActivityOptionsDAO onlineActivityOptionsDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private OnlineActivityQuestionSetDAO onlineActivityQuestionSetDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;

	@Autowired
	private OnlineActivityStatusDAO onlineActivityStatusDAO;
	
	@Autowired
	private PanelScheduleDAO panelScheduleDAO;
	
	@Autowired
	private StafferMasterDAO stafferMasterDAO;
	
	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) {
		this.districtKeyContactDAO = districtKeyContactDAO;
	}
	public String getOnlineActivityQuestions(Integer teacherId,Integer jobCategoryId,Integer districtId,Integer questionSetId,String bothpreviousStatusAndsubmitflag)
	{
		System.out.println(questionSetId+"*****:::::::::::::::getOnlineActivityQuestions:::::::::::::::::: "+teacherId);
		String previousStatus=bothpreviousStatusAndsubmitflag.split("##~##")[0];
		String submitflag=bothpreviousStatusAndsubmitflag.split("##~##")[1].trim();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
	
		StringBuffer sb =new StringBuffer();
		StringBuffer sbTime =new StringBuffer();
		try{
			if(teacherId!=0){
				
				TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
				DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
				JobCategoryMaster jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				OnlineActivityQuestionSet onlineActivityQuestionSet=onlineActivityQuestionSetDAO.findById(questionSetId, false, false);
				OnlineActivityStatus onlineActivityStatus=onlineActivityStatusDAO.findOnlineActivityStatusBySetId(teacherDetail, districtMaster, jobCategoryMaster,onlineActivityQuestionSet);
				System.out.println("onlineActivityStatus=::::::::::"+onlineActivityStatus);
				//add by Ram nath
				//System.out.println("status======"+onlineActivityStatus.getStatus()+" submitflag=="+submitflag);
				sb.append("<input type='hidden' id='previousStatus' value='"+onlineActivityStatus.getStatus()+"'/>" );	
				SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
				String activityDate=sdf.format(onlineActivityStatus.getActivityDate());
				String currentDate=sdf.format(new Date());
				//end by Ram Nath
				
				String status=null;
				if(onlineActivityStatus!=null){
					status=onlineActivityStatus.getStatus();
				}
				boolean entryflagforQuestion=false;
				boolean isTeacher=onlineActivityQuestionSet.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Teacher");
				//Teachers should be able to get back into the Online Activity with the link (from the email) because it is not timed
				if(  status!=null && (   (isTeacher &&(status.equalsIgnoreCase("S") || status.equalsIgnoreCase("I")))   ||   (status.equalsIgnoreCase("S"))  ) )
					entryflagforQuestion=true;
							
				//if(status!=null && status.equalsIgnoreCase("S")){
				if(entryflagforQuestion){
					/*System.out.println("activity date=="+activityDate);
					System.out.println("onlineActivityStatus.getActivityDate().before(sdf.parse(03/03/2015)=="+sdf.parse(activityDate).before(sdf.parse("03/03/2015")));
					System.out.println("new Date().before(sdf.parse(03/03/2015)=="+sdf.parse(currentDate).before(sdf.parse("03/03/2015")));
					System.out.println("onlineActivityStatus.getActivityDate().after(sdf.parse(03/02/2015))=="+sdf.parse(activityDate).after(sdf.parse("03/02/2015")));
					System.out.println("new Date().after(sdf.parse(03/02/2015))=="+sdf.parse(currentDate).after(sdf.parse("03/02/2015")));*/			
					if(!isTeacher && ((sdf.parse(activityDate).before(sdf.parse("03/03/2015"))) && (sdf.parse(currentDate).before(sdf.parse("03/03/2015")))) || ((sdf.parse(activityDate).after(sdf.parse("03/02/2015"))) && (sdf.parse(currentDate).after(sdf.parse("03/02/2015"))))){ //add by rAm nath
					
				//	OnlineActivityQuestionSet onlineActivityQuestionSet=onlineActivityQuestionSetDAO.getOnlineActivityQuestionSet(districtMaster,jobCategoryMaster);
					System.out.println("onlineActivityQuestionSet::::::::::"+onlineActivityQuestionSet);
					List<OnlineActivityQuestions> onlineActivityQuestionsList=onlineActivityQuestionsDAO.getOnlineActivityQuestions(onlineActivityQuestionSet);
					System.out.println("onlineActivityQuestionsList:::::::::::"+onlineActivityQuestionsList.size());
					int totalQuestions = onlineActivityQuestionsList.size();
					int cnt = 0;
					if(onlineActivityQuestionsList.size()>0){
						List<OnlineActivityAnswers>	lastList =onlineActivityAnswersDAO.findOnlineActivityAnswersByQuestionSetId(teacherDetail,districtMaster,jobCategoryMaster,onlineActivityQuestionSet);
						System.out.println("lastList::::::::"+lastList.size());
						Map<Integer, OnlineActivityAnswers> map = new HashMap<Integer, OnlineActivityAnswers>();
		
						for(OnlineActivityAnswers OnlineActivityAnswers : lastList)
							map.put(OnlineActivityAnswers.getOnlineActivityQuestions().getQuestionId(), OnlineActivityAnswers);
		
						OnlineActivityAnswers onlineActivityAnswers = null;
						List<OnlineActivityOptions> questionOptionsList = null;
						String shortName = "";
						String selectedOptions = "",insertedRanks="";
						if(onlineActivityQuestionSet.getQuestionSetName().trim().equalsIgnoreCase("Principal")){
							sb.append("<div><b>"+msgDIRECTIONSTOCANDIDATE1+":</b>"+msgDIRECTIONSTOCANDIDATE2+"<br/>");
						}else{
							sb.append("<div><b>"+msgDIRECTIONSTOCANDIDATE1+":</b> "+msgDIRECTIONSTOCANDIDATE3+"<br/>");
						}
						String path ="";
						if(onlineActivityQuestionSet.getQuestionSetName().trim().equalsIgnoreCase("Principal")){
							path=Utility.getValueOfPropByKey("contextBasePath")+"/OnlineActivity.pdf";
							sb.append("<b>"+msgDATAEXERCISECONTEXT1+":</b> "+msgDATAEXERCISECONTEXT2+"");
						}else if(onlineActivityQuestionSet.getQuestionSetName().trim().equalsIgnoreCase("Teacher Selection for 3rd GRADE")){
							path=Utility.getValueOfPropByKey("contextBasePath")+"/Online_Activity_3rdGRADE.pdf";
						}else if(onlineActivityQuestionSet.getQuestionSetName().trim().equalsIgnoreCase("Teacher Selection for 8th GRADE")){
							path=Utility.getValueOfPropByKey("contextBasePath")+"/Online_Activity_8thGRADE.pdf";
						}
						sb.append("<div><br/>");
						sb.append("<div>"+lblPlease+" <a href='javascript:void(0);' onclick=\"window.open('"+path+"','_blank', 'width = 1050px, height = 600px');\" download>"+lnkClickHere1+"</a> "+msgreviewToRespondToquest+"<div><br/>");
						
							for (OnlineActivityQuestions OnlineActivityQuestions : onlineActivityQuestionsList) 
							{
								shortName = OnlineActivityQuestions.getQuestionTypeMaster().getQuestionTypeShortName();
								sb.append("<input type='hidden' name='dspqType' id='dspqType' value='"+1+"'  />");
								sb.append("<input type='hidden' name='jobCategoryIdForDSPQ'   id='jobCategoryIdForDSPQ' value='"+jobCategoryId+"'  />");
								sb.append("<tr>");
								sb.append("<td width='90%'>");
								sb.append("<div><b>"+lblQues+" "+(++cnt)+" of "+totalQuestions+"</b>");
								if(OnlineActivityQuestions.getIsRequired()==1){
									sb.append("<span class=\"required\" >*</span>");
								}
								sb.append("</div>");
								sb.append("<div id='QS"+cnt+"question'>"+Utility.getUTFToHTML(OnlineActivityQuestions.getQuestion())+"</div>");
								if(OnlineActivityQuestions.getQuestionInstructions()!=null && !OnlineActivityQuestions.getQuestionInstructions().trim().equals(""))
								sb.append("<div id='QS"+cnt+"question_instruction'><b>Instruction :</b>"+" "+Utility.getUTFToHTML(OnlineActivityQuestions.getQuestionInstructions())+"</div>");
		
								questionOptionsList = OnlineActivityQuestions.getQuestionOptions();
								onlineActivityAnswers = map.get(OnlineActivityQuestions.getQuestionId());
		
								String checked = "";
								Integer optId = 0;
								String insertedText = "";
								if(onlineActivityAnswers!=null)
								{
									if(!shortName.equalsIgnoreCase("mloet") && !shortName.equalsIgnoreCase("mlsel") && !shortName.equalsIgnoreCase("rt") && shortName.equalsIgnoreCase(onlineActivityAnswers.getQuestionType()))
									{
										if(onlineActivityAnswers.getSelectedOptions()!=null)
											optId = Integer.parseInt(onlineActivityAnswers.getSelectedOptions());
		
										insertedText = onlineActivityAnswers.getInsertedText();
									}
									if(shortName.equalsIgnoreCase("mloet"))
									{
										insertedText = onlineActivityAnswers.getInsertedText();
									}
								}
								if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel")|| shortName.equalsIgnoreCase("lkts"))
								{
									sb.append("<table>");
									for (OnlineActivityOptions questionOptions : questionOptionsList) {
										if(optId.equals(questionOptions.getOptionId()))
											checked = lblChk;
										else
											checked = "";
		
										sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
												" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
												"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
										sb.append("</td></tr>");
									}
									sb.append("</table>");
								}
								if(shortName.equalsIgnoreCase("rt")){	
										if(onlineActivityAnswers!=null && onlineActivityAnswers.getInsertedRanks()!=null){
											insertedRanks = onlineActivityAnswers.getInsertedRanks()==null?"":onlineActivityAnswers.getInsertedRanks();
										}
										String[] ranks = insertedRanks.split("\\|");
										int rank=1;
										int count=0;
										String ans = "";
										sb.append("<table>");
										Map<Integer,OnlineActivityOptions> optionMap=new HashMap<Integer, OnlineActivityOptions>();
										for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
											optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
										}
										for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
											try{ans =ranks[count];}catch(Exception e){}
											sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' id='rnkS"+rank+"' name='rankS' class='span1' maxlength='1' onkeypress='return checkForIntUpto6(event)' onblur='checkUniqueRankForPortfolio(this);' value='"+ans+"'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
											sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"' /><input type='hidden' name='optS' value='"+teacherQuestionOption.getOptionId()+"' />");
											sb.append("<input type='hidden' name='o_rankS' value='"+(teacherQuestionOption.getRank()==null?0:teacherQuestionOption.getRank())+"' />");
											sb.append("<script type=\"text/javascript\" language=\"javascript\">");
											sb.append("</script></td></tr>");
											rank++;
											count++;
										}
										sb.append("</table >");
								}
								if(shortName.equalsIgnoreCase("mlsel")){	
									if(onlineActivityAnswers!=null && onlineActivityAnswers.getSelectedOptions()!=null){
										selectedOptions = onlineActivityAnswers.getSelectedOptions()==null?"":onlineActivityAnswers.getSelectedOptions();
									}
									System.out.println("selectedOptions::::>:"+selectedOptions);
									String[] multiCounts = selectedOptions.split("\\|");
									int multiCount=1;
									
									sb.append("<table>");
									Map<Integer,OnlineActivityOptions> optionMap=new HashMap<Integer, OnlineActivityOptions>();
									for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
										optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
									}
									String ansId="";
									for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
										try{
											ansId="";
											for(int i=0;i<multiCounts.length;i++){
												try{ansId =multiCounts[i];}catch(Exception e){}
												if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
													if(ansId!=null && !ansId.equals("")){
														ansId="checked";
													}
													break;
												}
											}
										}catch(Exception e){
											e.printStackTrace();
										}
										
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
										sb.append("</td></tr>");
										multiCount++;
										
									}
									sb.append("</table >");
								}
								if(shortName.equalsIgnoreCase("mloet")){	
									if(onlineActivityAnswers!=null && onlineActivityAnswers.getSelectedOptions()!=null){
										selectedOptions = onlineActivityAnswers.getSelectedOptions()==null?"":onlineActivityAnswers.getSelectedOptions();
									}
									System.out.println("selectedOptions::::>:"+selectedOptions);
									String[] multiCounts = selectedOptions.split("\\|");
									int multiCount=1;
									
									sb.append("<table>");
									Map<Integer,OnlineActivityOptions> optionMap=new HashMap<Integer, OnlineActivityOptions>();
									for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
										optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
									}
									String ansId="";
									for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
										try{
											ansId="";
											for(int i=0;i<multiCounts.length;i++){
												try{ansId =multiCounts[i];}catch(Exception e){}
												if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
													if(ansId!=null && !ansId.equals("")){
														ansId="checked";
													}
													break;
												}
											}
										}catch(Exception e){
											e.printStackTrace();
										}
										
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
										sb.append("</td></tr>");
										multiCount++;
										
									}
									sb.append("</table >");
									String questionInstructions="";
									try{
										if(OnlineActivityQuestions.getQuestionInstructions()!=null){
											questionInstructions="&nbsp;&nbsp;"+OnlineActivityQuestions.getQuestionInstructions();	
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									
									sb.append(questionInstructions+"</br>&nbsp;&nbsp;<span style=''><textarea  name='QS"+cnt+"optmloet' id='QS"+cnt+"optmloet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
									sb.append("<script type=\"text/javascript\" language=\"javascript\">");
									sb.append("</script>");
								}
								if(shortName.equalsIgnoreCase("it"))
								{
									sb.append("<table >");
									for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='optS' value='"+teacherQuestionOption.getOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherQuestionOption.getQuestionOption()+"'> ");
										sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'  /></td></tr>");
									}
									sb.append("</table>");
								}
								if(shortName.equalsIgnoreCase("et"))
								{
									sb.append("<table>");
									for (OnlineActivityOptions questionOptions : questionOptionsList) {
										if(optId.equals(questionOptions.getOptionId()))
											checked = lblChk;
										else
											checked = "";
		
										sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
												" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
												"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
										sb.append("</td></tr>");
									}
									sb.append("</table>");
		
									String questionInstructions="";
									try{
										if(OnlineActivityQuestions.getQuestionInstructions()!=null){
											questionInstructions="&nbsp;&nbsp;"+OnlineActivityQuestions.getQuestionInstructions()+"</br>";	
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									
									sb.append(questionInstructions+"&nbsp;&nbsp;<span style=''><textarea  name='QS"+cnt+"optet' id='QS"+cnt+"optet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
									sb.append("<script type=\"text/javascript\" language=\"javascript\">");
									sb.append("</script>");
								}
								if(shortName.equalsIgnoreCase("sl"))
								{
									sb.append("&nbsp;&nbsp;<input type='text' name='QS"+cnt+"opt' id='QS"+cnt+"opt' value='"+insertedText+"' maxlength='100' style='width:98%;margin:5px;'/><br/>");					
									sb.append("<script type=\"text/javascript\" language=\"javascript\">");
									sb.append("</script>");
								}else if(shortName.equalsIgnoreCase("ml"))
								{
									sb.append("&nbsp;&nbsp;<textarea name='QS"+cnt+"opt' id='QS"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");					
									sb.append("<script type=\"text/javascript\" language=\"javascript\">");
									sb.append("</script>");
								}
								sb.append("<input type='hidden' id='QS"+cnt+"isRequired' value='"+OnlineActivityQuestions.getIsRequired()+"'/>" );
								sb.append("<input type='hidden' id='QS"+cnt+"questionId' value='"+OnlineActivityQuestions.getQuestionId()+"'/>" );
								sb.append("<input type='hidden' id='QS"+cnt+"questionTypeId' value='"+OnlineActivityQuestions.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
								sb.append("<input type='hidden' id='QS"+cnt+"questionTypeShortName' value='"+shortName+"'/>" );
		
								sb.append("</td>");
								sb.append("</tr>");
								insertedText = "";
							}
					    }
					sbTime.append(onlineActivityQuestionSet.getQuestionTime());
					sb.append("@##@"+cnt+"@##@"+sbTime);
					try{
						onlineActivityStatus.setStatus("I");
						onlineActivityStatus.setActivityStartTime(new Date());
						onlineActivityStatusDAO.updatePersistent(onlineActivityStatus);						
					}catch(Exception e){
						e.printStackTrace();
					}
					
				}else{//start by Ram Nath				
					sb.append("<table >");								
					sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'>Valid period for this online activity has been expired so you would not be able to take the online test now.  </td></tr>");						
				sb.append("</table>");
				sb.append("@##@"+0+"@##@"+0);					
				}
				//End by Ram nath
				}else{
					sb.append("<table >");
					//add by Ram Nath
					if(previousStatus!=null && ( (isTeacher && (previousStatus.equalsIgnoreCase("s") || previousStatus.equalsIgnoreCase("i")))  ||  previousStatus.equalsIgnoreCase("s") )){
							if(submitflag.equals("1"))
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'>"+msgOnlineActivity1+"</td></tr>");
								else if(submitflag.equals("2"))
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'>"+msgOnlineActivity2+"</td></tr>");
					}else{
					//End by Ram nath
						if(isTeacher)
							sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'>"+msgOnlineActivity3+"</td></tr>");
						else
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'>"+msgOnlineActivity4+" </td></tr>");
					}
					
					sb.append("</table>");
					sb.append("@##@"+0+"@##@"+0);
				}
			
				
		
			}
			}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	
		
	}
	
	public String getOnlineActivityQuestionsForCG(Integer teacherId,Integer jobId,Integer districtId,Integer questionSetId)
	{

		System.out.println("*****:::::::::::::::--getOnlineActivityQuestionsForCG:::::::::::::::::: "+teacherId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
	
		StringBuffer sb =new StringBuffer();
		StringBuffer sbTime =new StringBuffer();
		try{
			if(teacherId!=0){				
				TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
				DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
				JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
				JobCategoryMaster jobCategoryMaster=jobOrder.getJobCategoryMaster();
				OnlineActivityQuestionSet onlineActivityQuestionSet=onlineActivityQuestionSetDAO.findById(questionSetId, false,false);
				System.out.println("onlineActivityQuestionSet::::::::::"+onlineActivityQuestionSet);
				List<OnlineActivityQuestions> onlineActivityQuestionsList=onlineActivityQuestionsDAO.getOnlineActivityQuestions(onlineActivityQuestionSet);
				System.out.println("onlineActivityQuestionsList:::::::::::"+onlineActivityQuestionsList.size());

				//added by 09-04-2015
				try{
					List<OnlineActivityAnswers>	dspqList =onlineActivityAnswersDAO.findOnlineActivityAnswersByQuestionSetId(teacherDetail,districtMaster,jobCategoryMaster,onlineActivityQuestionSet);
					OnlineActivityStatus onlineActivityStatus=onlineActivityStatusDAO.findOnlineActivityStatusBySetId(teacherDetail, districtMaster, jobCategoryMaster,onlineActivityQuestionSet);				
					//System.out.println("dspqList.size()===="+dspqList.size()+"       onlineActivityStatus.getStatus()==="+onlineActivityStatus.getStatus());
					if(dspqList.size()==0 && onlineActivityStatus.getStatus().trim().equalsIgnoreCase("i")){
						for(OnlineActivityQuestions question:onlineActivityQuestionsList ){
							//System.out.println("question.getQuestionTypeMaster().getQuestionType()============="+question.getQuestionTypeMaster().getQuestionTypeShortName());
							OnlineActivityAnswers onlineActivityAnswers=new OnlineActivityAnswers();
							onlineActivityAnswers.setInsertedText("");
							onlineActivityAnswers.setCreatedDateTime(new Date());
							onlineActivityAnswers.setDistrictId(districtMaster.getDistrictId());
							onlineActivityAnswers.setIsActive(true);
							onlineActivityAnswers.setJobCategoryId(jobCategoryMaster.getJobCategoryId());
							onlineActivityAnswers.setOnlineActivityQuestions(question);
							onlineActivityAnswers.setOnlineActivityQuestionSet(onlineActivityQuestionSet);
							onlineActivityAnswers.setQuestion(question.getQuestion());
							onlineActivityAnswers.setQuestionType(question.getQuestionTypeMaster().getQuestionTypeShortName());
							onlineActivityAnswers.setQuestionTypeMaster(question.getQuestionTypeMaster());
							onlineActivityAnswers.setTeacherDetail(teacherDetail);				
							onlineActivityAnswersDAO.makePersistent(onlineActivityAnswers);
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				//ended by 09-04-2015
				
				
				int totalQuestions = onlineActivityQuestionsList.size();
				int cnt = 0;
					if(onlineActivityQuestionsList.size()>0){
						//System.out.println("jobCategoryMaster::::::::::"+jobCategoryMaster.getJobCategoryId());
						//System.out.println("teacherDetail::::::::::"+teacherDetail.getTeacherId());
						//System.out.println("districtMaster::::::::::"+districtMaster.getDistrictId());
						List<OnlineActivityAnswers>	lastList =onlineActivityAnswersDAO.findOnlineActivityAnswersByQuestionSetId(teacherDetail,districtMaster,jobCategoryMaster,onlineActivityQuestionSet);
						System.out.println("lastList::::::::"+lastList.size());
						Map<Integer, OnlineActivityAnswers> map = new HashMap<Integer, OnlineActivityAnswers>();
		
						for(OnlineActivityAnswers OnlineActivityAnswers : lastList)
							map.put(OnlineActivityAnswers.getOnlineActivityQuestions().getQuestionId(), OnlineActivityAnswers);
		
						OnlineActivityAnswers onlineActivityAnswers = null;
						List<OnlineActivityOptions> questionOptionsList = null;
						String shortName = "";
						String selectedOptions = "",insertedRanks="";
							
						
						int topMaxScore = 0;
						int interval 	= 10;
						int dslider 	= 0;
						int svalue 		= 0;
						int scoreProvided = 0;
						
							for (OnlineActivityQuestions onlineActivityQuestions : onlineActivityQuestionsList) 
							{
								shortName = onlineActivityQuestions.getQuestionTypeMaster().getQuestionTypeShortName();
								sb.append("<input type='hidden' name='dspqType' id='dspqType' value='"+1+"'  />");
								sb.append("<input type='hidden' name='jobCategoryIdForDSPQ'   id='jobCategoryIdForDSPQ' value='"+jobCategoryMaster.getJobCategoryId()+"'  />");
								sb.append("<tr>");
								sb.append("<td width='90%'>");
								sb.append("<div><b>"+lblQues+" "+(++cnt)+" of "+totalQuestions+"</b>");
								if(onlineActivityQuestions.getIsRequired()==1){
									sb.append("<span class=\"required\" >*</span>");
								}
								sb.append("</div>");
							
								sb.append("<div id='QOA"+cnt+"question'>"+Utility.getUTFToHTML(onlineActivityQuestions.getQuestion())+"</div>");
								
								questionOptionsList = onlineActivityQuestions.getQuestionOptions();
								onlineActivityAnswers = map.get(onlineActivityQuestions.getQuestionId());
								
		
								String checked = "";
								Integer optId = 0;
								String insertedText = "";
								if(onlineActivityAnswers!=null)
								{
									if(!shortName.equalsIgnoreCase("mloet") && !shortName.equalsIgnoreCase("mlsel") && !shortName.equalsIgnoreCase("rt") && shortName.equalsIgnoreCase(onlineActivityAnswers.getQuestionType()))
									{
										if(onlineActivityAnswers.getSelectedOptions()!=null)
											optId = Integer.parseInt(onlineActivityAnswers.getSelectedOptions());
		
										insertedText = onlineActivityAnswers.getInsertedText();
									}
									if(shortName.equalsIgnoreCase("mloet"))
									{
										insertedText = onlineActivityAnswers.getInsertedText();
									}
								}
								if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel")|| shortName.equalsIgnoreCase("lkts"))
								{
									sb.append("<table>");
									for (OnlineActivityOptions questionOptions : questionOptionsList) {
										if(optId.equals(questionOptions.getOptionId()))
											checked = "checked";
										else
											checked = "";
		
										sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QOA"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
												" <input type='hidden' id='QOA"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
												"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
										sb.append("</td></tr>");
									}
									sb.append("</table>");
								}
								if(shortName.equalsIgnoreCase("rt")){	
										if(onlineActivityAnswers!=null && onlineActivityAnswers.getInsertedRanks()!=null){
											insertedRanks = onlineActivityAnswers.getInsertedRanks()==null?"":onlineActivityAnswers.getInsertedRanks();
										}
										String[] ranks = insertedRanks.split("\\|");
										int rank=1;
										int count=0;
										String ans = "";
										sb.append("<table>");
										Map<Integer,OnlineActivityOptions> optionMap=new HashMap<Integer, OnlineActivityOptions>();
										for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
											optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
										}
										for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
											try{ans =ranks[count];}catch(Exception e){}
											sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' id='rnkS"+rank+"' name='rankS' class='span1' maxlength='1' onkeypress='return checkForIntUpto6(event)' onblur='checkUniqueRankForPortfolio(this);' value='"+ans+"'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
											sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"' /><input type='hidden' name='optS' value='"+teacherQuestionOption.getOptionId()+"' />");
											sb.append("<input type='hidden' name='o_rankS' value='"+(teacherQuestionOption.getRank()==null?0:teacherQuestionOption.getRank())+"' />");
											sb.append("<script type=\"text/javascript\" language=\"javascript\">");
											//sb.append("document.getElementById('rnkS1').focus();");
											sb.append("</script></td></tr>");
											rank++;
											count++;
										}
										sb.append("</table >");
								}
								if(shortName.equalsIgnoreCase("mlsel")){	
									if(onlineActivityAnswers!=null && onlineActivityAnswers.getSelectedOptions()!=null){
										selectedOptions = onlineActivityAnswers.getSelectedOptions()==null?"":onlineActivityAnswers.getSelectedOptions();
									}
									System.out.println("selectedOptions::::>:"+selectedOptions);
									String[] multiCounts = selectedOptions.split("\\|");
									int multiCount=1;
									
									sb.append("<table>");
									Map<Integer,OnlineActivityOptions> optionMap=new HashMap<Integer, OnlineActivityOptions>();
									for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
										optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
									}
									String ansId="";
									for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
										try{
											ansId="";
											for(int i=0;i<multiCounts.length;i++){
												try{ansId =multiCounts[i];}catch(Exception e){}
												if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
													if(ansId!=null && !ansId.equals("")){
														ansId="checked";
													}
													break;
												}
											}
										}catch(Exception e){
											e.printStackTrace();
										}
										
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
										sb.append("</td></tr>");
										multiCount++;
										
									}
									sb.append("</table >");
								}
								if(shortName.equalsIgnoreCase("mloet")){	
									if(onlineActivityAnswers!=null && onlineActivityAnswers.getSelectedOptions()!=null){
										selectedOptions = onlineActivityAnswers.getSelectedOptions()==null?"":onlineActivityAnswers.getSelectedOptions();
									}
									System.out.println("selectedOptions::::>:"+selectedOptions);
									String[] multiCounts = selectedOptions.split("\\|");
									int multiCount=1;
									
									sb.append("<table>");
									Map<Integer,OnlineActivityOptions> optionMap=new HashMap<Integer, OnlineActivityOptions>();
									for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
										optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
									}
									String ansId="";
									for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
										try{
											ansId="";
											for(int i=0;i<multiCounts.length;i++){
												try{ansId =multiCounts[i];}catch(Exception e){}
												if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
													if(ansId!=null && !ansId.equals("")){
														ansId="checked";
													}
													break;
												}
											}
										}catch(Exception e){
											e.printStackTrace();
										}
										
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
										sb.append("</td></tr>");
										multiCount++;
										
									}
									sb.append("</table >");
									String questionInstructions="";
									try{
										if(onlineActivityQuestions.getQuestionInstructions()!=null){
											questionInstructions="&nbsp;&nbsp;"+onlineActivityQuestions.getQuestionInstructions();	
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									
									sb.append(questionInstructions+"</br>&nbsp;&nbsp;<span style=''><textarea  name='QOA"+cnt+"optmloet' id='QOA"+cnt+"optmloet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
									sb.append("<script type=\"text/javascript\" language=\"javascript\">");
									sb.append("</script>");
								}
								if(shortName.equalsIgnoreCase("it"))
								{
									sb.append("<table >");
									for (OnlineActivityOptions teacherQuestionOption : questionOptionsList) {
										sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='optS' value='"+teacherQuestionOption.getOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherQuestionOption.getQuestionOption()+"'> ");
										sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'  /></td></tr>");
									}
									sb.append("</table>");
								}
								if(shortName.equalsIgnoreCase("et"))
								{
									sb.append("<table>");
									for (OnlineActivityOptions questionOptions : questionOptionsList) {
										if(optId.equals(questionOptions.getOptionId()))
											checked = lblChk;
										else
											checked = "";
		
										sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QOA"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
												" <input type='hidden' id='QOA"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
												"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
										sb.append("</td></tr>");
									}
									sb.append("</table>");
		
									String questionInstructions="";
									try{
										if(onlineActivityQuestions.getQuestionInstructions()!=null){
											questionInstructions="&nbsp;&nbsp;"+onlineActivityQuestions.getQuestionInstructions()+"</br>";	
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									
									sb.append(questionInstructions+"&nbsp;&nbsp;<span style=''><textarea  name='QOA"+cnt+"optet' id='QOA"+cnt+"optet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
									sb.append("<script type=\"text/javascript\" language=\"javascript\">");
									sb.append("</script>");
								}
								if(shortName.equalsIgnoreCase("sl"))
								{
									sb.append("&nbsp;&nbsp;<input type='text' name='QOA"+cnt+"opt' id='QOA"+cnt+"opt' value='"+insertedText+"' maxlength='100' style='width:98%;margin:5px;'/><br/>");					
									sb.append("<script type=\"text/javascript\" language=\"javascript\">");
									sb.append("</script>");
								}else if(shortName.equalsIgnoreCase("ml"))
								{
									sb.append("&nbsp;&nbsp;<textarea name='QOA"+cnt+"opt' id='QS"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");					
									sb.append("<script type=\"text/javascript\" language=\"javascript\">");
									sb.append("</script>");
								}
								
								///////////////////////
								
								topMaxScore		=	onlineActivityQuestions.getMaxScore();
								try{
									if(onlineActivityAnswers!=null && onlineActivityAnswers.getScore()!=null){
										scoreProvided=onlineActivityAnswers.getScore();
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								if(topMaxScore > 0){
									dslider = 1;
								} else {
									dslider = 0;
								}

								if(topMaxScore%10==0)
									interval=10; 
								else if(topMaxScore%5==0)
									interval=5;
								else if(topMaxScore%3==0)
									interval=3;
								else if(topMaxScore%2==0)
									interval=2;
								
								System.out.println("interval::::"+interval);
								System.out.println("topMaxScore::::"+topMaxScore);
								System.out.println("scoreProvided::::"+scoreProvided);
								sb.append("<br/><iframe id='QOA"+cnt+"ifrmOnlineActivity' src='slideract.do?name=onlineActivityFrm&amp;tickInterval="+interval+"&amp;max="+topMaxScore+"&amp;swidth=536&amp;dslider="+dslider+"&amp;svalue="+scoreProvided+"' scrolling='no' frameborder='0' style='border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:580px;margin-top:-11px;'></iframe>");						
								
								////////////////////////////
								sb.append("<input type='hidden' id='QOA"+cnt+"isRequired' value='"+onlineActivityQuestions.getIsRequired()+"'/>" );
								sb.append("<input type='hidden' id='QOA"+cnt+"questionId' value='"+onlineActivityQuestions.getQuestionId()+"'/>" );
								sb.append("<input type='hidden' id='QOA"+cnt+"questionTypeId' value='"+onlineActivityQuestions.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
								sb.append("<input type='hidden' id='QOA"+cnt+"questionTypeShortName' value='"+shortName+"'/>" );
								sb.append("<input type='hidden' id='QOA"+cnt+"totalScore' value='"+topMaxScore+"'/>" );
								sb.append("</td>");
								sb.append("</tr>");
								insertedText = "";
							}
					    }
					sb.append("@##@"+cnt+"@##@");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	
		
	}
	
	public String setOnlineActivityQuestions(OnlineActivityAnswers[] OnlineActivityAnswersList,Integer teacherId,Integer jobCategoryId,Integer districtId,Integer questionSetId,boolean errorCount)
	{
		System.out.println(":::::::::::::::    setOnlineActivityQuestions    :::::::::::::::::::;; "+jobCategoryId);

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		try {
			TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			JobCategoryMaster jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false,false);
			OnlineActivityQuestionSet onlineActivityQuestionSet=onlineActivityQuestionSetDAO.findById(questionSetId, false, false);
			OnlineActivityStatus onlineActivityStatus=onlineActivityStatusDAO.findOnlineActivityStatusBySetId(teacherDetail, districtMaster, jobCategoryMaster,onlineActivityQuestionSet);
			System.out.println("onlineActivityStatus=::::::::::"+onlineActivityStatus);
			if(!errorCount){
				try{
					if(onlineActivityStatus!=null){
						onlineActivityStatus.setStatus("C");
						onlineActivityStatus.setActivityEndTime(new Date());
						onlineActivityStatusDAO.updatePersistent(onlineActivityStatus);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			Map<Integer,Integer> map = new HashMap<Integer, Integer>();
			Map<Integer,OnlineActivityAnswers> mapOnlineActivityAnswers = new HashMap<Integer, OnlineActivityAnswers>();			
			Map<String, String> questionOptions = new HashMap<String, String>();
			Map<Long, SchoolMaster> schoolMap = new HashMap<Long, SchoolMaster>();
			try {
				Criterion criterionDis = Restrictions.eq("districtMaster",districtMaster);
				List<SchoolMaster> schList = schoolMasterDAO.findDistrictSchoolId(districtMaster);
				for (SchoolMaster schoolMaster : schList) {
					schoolMap.put(schoolMaster.getSchoolId(), schoolMaster);
				}

				List<OnlineActivityAnswers>	dspqList =onlineActivityAnswersDAO.findOnlineActivityAnswersByQuestionSetId(teacherDetail,districtMaster,jobCategoryMaster,onlineActivityQuestionSet);
				for (OnlineActivityAnswers OnlineActivityAnswers : dspqList) {
					map.put(OnlineActivityAnswers.getOnlineActivityQuestions().getQuestionId(), OnlineActivityAnswers.getAnswerId());
					mapOnlineActivityAnswers.put(OnlineActivityAnswers.getOnlineActivityQuestions().getQuestionId(), OnlineActivityAnswers);
				}
				
				for (OnlineActivityAnswers OnlineActivityAnswers : dspqList) {
					map.put(OnlineActivityAnswers.getOnlineActivityQuestions().getQuestionId(), OnlineActivityAnswers.getAnswerId());					
					
				}
				List<OnlineActivityQuestions> onlineActivityQuestionsLst = onlineActivityQuestionsDAO.findByCriteria(criterionDis);
				Criterion criterion1 = Restrictions.in("onlineActivityQuestions", onlineActivityQuestionsLst);
				List<OnlineActivityOptions> listPortfolioOptions = onlineActivityOptionsDAO.findByCriteria(criterion1);
				
				for (OnlineActivityOptions OnlineActivityOptions : listPortfolioOptions) {
					String questionOption = OnlineActivityOptions.getOnlineActivityQuestions().getQuestionId()+"##"+OnlineActivityOptions.getOptionId();
					questionOptions.put(questionOption, OnlineActivityOptions.getQuestionOption());
				}			
			} catch (Exception e) {
				e.printStackTrace();
			}

			List<Integer> answers = new ArrayList<Integer>();
			Integer ansId = null;
			for (OnlineActivityAnswers onlineActivityAnswers : OnlineActivityAnswersList) {
				onlineActivityAnswers.setTeacherDetail(teacherDetail);
				onlineActivityAnswers.setDistrictId(districtMaster.getDistrictId());
				onlineActivityAnswers.setCreatedDateTime(new Date());

				if(onlineActivityAnswers.getSchoolIdTemp()!=null && schoolMap.containsKey(onlineActivityAnswers.getSchoolIdTemp())){
					SchoolMaster schoolMasterObj=schoolMap.get(onlineActivityAnswers.getSchoolIdTemp());
					onlineActivityAnswers.setSchoolId(schoolMasterObj.getSchoolId());
				}
				
				ansId = map.get(onlineActivityAnswers.getOnlineActivityQuestions().getQuestionId());
				if(ansId!=null)
				{
					onlineActivityAnswers.setAnswerId(ansId);
					
					if(mapOnlineActivityAnswers.containsKey(onlineActivityAnswers.getOnlineActivityQuestions().getQuestionId())){
						OnlineActivityAnswers tempOnlineActivityAnswers =mapOnlineActivityAnswers.get(onlineActivityAnswers.getOnlineActivityQuestions().getQuestionId());
						onlineActivityAnswers.setScore(tempOnlineActivityAnswers.getScore());
						onlineActivityAnswers.setScoreBy(tempOnlineActivityAnswers.getScoreBy());
						onlineActivityAnswers.setScoreDateTime(tempOnlineActivityAnswers.getScoreDateTime());
					}
				}
				onlineActivityAnswers.setJobCategoryId(jobCategoryMaster.getJobCategoryId());
				onlineActivityAnswers.setOnlineActivityQuestionSet(onlineActivityQuestionSet);
				onlineActivityAnswers.setIsActive(true);
				onlineActivityAnswersDAO.makePersistent(onlineActivityAnswers);
				answers.add(onlineActivityAnswers.getAnswerId());
			}
			return "1";

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	public String saveOnlineActivity(OnlineActivityAnswers[] onlineActivityAnswersList,Integer teacherId,Integer jobId,Integer districtId,Integer questionSetId,boolean errorCount,String onlineActivityNote)
	{
		System.out.println(questionSetId+":::::::::::::::  onlineActivityQuestions-    :::::::::::::::::::;; "+errorCount);

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		try {
			TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			JobCategoryMaster jobCategoryMaster=jobOrder.getJobCategoryMaster();
			OnlineActivityQuestionSet onlineActivityQuestionSet=onlineActivityQuestionSetDAO.findById(questionSetId, false, false);
			Map<Integer,Integer> map = new HashMap<Integer, Integer>();
			Map<Integer,OnlineActivityAnswers> mapOnlineActivityAnswers = new HashMap<Integer, OnlineActivityAnswers>();
			
			boolean scoreFlag=false;
			try {
				List<OnlineActivityAnswers> dspqList=onlineActivityAnswersDAO.findOnlineActivityAnswersByQuestionSetId(teacherDetail,districtMaster,jobCategoryMaster,onlineActivityQuestionSet);
				for (OnlineActivityAnswers onlineActivityAnswers : dspqList) {
					map.put(onlineActivityAnswers.getOnlineActivityQuestions().getQuestionId(), onlineActivityAnswers.getAnswerId());
					mapOnlineActivityAnswers.put(onlineActivityAnswers.getOnlineActivityQuestions().getQuestionId(), onlineActivityAnswers);
				}
				for (OnlineActivityAnswers onlineActivityAnswers : dspqList) {
					if(onlineActivityAnswers.getTotalScore()!=null){
						scoreFlag=true;	
					}
					map.put(onlineActivityAnswers.getOnlineActivityQuestions().getQuestionId(), onlineActivityAnswers.getAnswerId());					
				}
				System.out.println("dspqList::::::"+dspqList.size());
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("scoreFlag::::::"+scoreFlag);
			
			int minPrveScore=0;
			int totalPrevScore=0;
			try{
				Double[] onlineStatusScoresAvg= onlineActivityAnswersDAO.getAllFinalizeStatusScoreAvg(teacherDetail,onlineActivityQuestionSet);
				if(onlineStatusScoresAvg.length>0){
						minPrveScore=Integer.parseInt(Math.round(onlineStatusScoresAvg[0])+"");
						totalPrevScore=Integer.parseInt(Math.round(onlineStatusScoresAvg[1])+"");
				}
			}catch(Exception e){ e.printStackTrace();}
			System.out.println("minPrveScore:::::::"+minPrveScore);
			System.out.println("totalPrevScore:::::::"+totalPrevScore);
			for (OnlineActivityAnswers onlineActivityAnswers : onlineActivityAnswersList) {
				if(mapOnlineActivityAnswers.get(onlineActivityAnswers.getOnlineActivityQuestions().getQuestionId())!=null){
					OnlineActivityAnswers onlineActivityAnswer=mapOnlineActivityAnswers.get(onlineActivityAnswers.getOnlineActivityQuestions().getQuestionId());
					onlineActivityAnswer.setScore(onlineActivityAnswers.getScore());
					onlineActivityAnswer.setTotalScore(onlineActivityAnswers.getTotalScore());
					onlineActivityAnswersDAO.makePersistent(onlineActivityAnswer);
				}
			}
			try{
				if(onlineActivityNote!=null && !onlineActivityNote.trim().equals("")){
					OnlineActivityNotes  onlineActivityNotes=new OnlineActivityNotes();
					onlineActivityNotes.setDistrictMaster(districtMaster);
					onlineActivityNotes.setJobOrder(jobOrder);
					onlineActivityNotes.setQuestionSetId(onlineActivityQuestionSet);
					onlineActivityNotes.setTeacherDetail(teacherDetail);
					onlineActivityNotes.setUserMaster(userMaster);
					onlineActivityNotes.setOnlineActivityNotes(onlineActivityNote);
					onlineActivityNotesDAO.makePersistent(onlineActivityNotes);
				}							
			}catch(Exception he){
				he.printStackTrace();
			}
			int minAfterScore=0;
			int totalAfterScore=0;
			try{
				try{
					Double[] onlineStatusScoresAvg= onlineActivityAnswersDAO.getAllFinalizeStatusScoreAvg(teacherDetail,onlineActivityQuestionSet);
					minAfterScore=Integer.parseInt(Math.round(onlineStatusScoresAvg[0])+"");
					totalAfterScore=Integer.parseInt(Math.round(onlineStatusScoresAvg[1])+"");
				}catch(Exception e){ e.printStackTrace();}
				
				System.out.println("minAfterScore:::::::"+minAfterScore);
				System.out.println("totalAfterScore:::::::"+totalAfterScore);
				int cScore=0;
				int maxScore=0;
				int minUpdateScore=0;
				int maxUpdateScore=0;
				if(scoreFlag){
					minUpdateScore=minAfterScore-minPrveScore;
				}else{
					minUpdateScore=minAfterScore;
					maxUpdateScore=totalAfterScore;
				}
				System.out.println("cScore:::::::"+cScore);
				System.out.println("maxScore:::::::"+maxScore);
				
				
				if(maxScore>-1 && cScore>-1){
					try{
						OnlineActivityFitScore oAFS=null;				
						oAFS=onlineActivityFitScoreDAO.getOAFSScore(teacherDetail,jobOrder);
						OnlineActivityFitScore oafsScore= new OnlineActivityFitScore();
						if(oAFS!=null){
							oafsScore=oAFS;
							if(scoreFlag){
								cScore=minUpdateScore+oAFS.getMinScore();	
							}else{
								cScore=minUpdateScore+oAFS.getMinScore();	
								maxScore=maxUpdateScore+oAFS.getMaxScore();
							}
						}else{
							cScore=minUpdateScore;	
							maxScore=maxUpdateScore;						
						}
						System.out.println("cScore-----------:::::::"+cScore);
						System.out.println("maxScore:::::::"+maxScore);
						oafsScore.setTeacherDetail(teacherDetail);
						oafsScore.setDistrictMaster(jobOrder.getDistrictMaster());
						oafsScore.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
						oafsScore.setMinScore(cScore);
						if(maxScore!=0){
							oafsScore.setMaxScore(maxScore);
						}
						onlineActivityFitScoreDAO.makePersistent(oafsScore);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			return "1";

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	public String displayOnlineActivityForCG(Integer teacherId,Integer jobId,Integer districtId)
	{
		System.out.println(":::::::::::::::    displayOnlineActivityForCG    :::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		StringBuffer sb=new StringBuffer();
		try {
			String clickable="class='notclickable'";			
			TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			JobCategoryMaster jobCategoryMaster=jobOrder.getJobCategoryMaster();
			
			sb.append("<tr><td><table border='0' class='table' id='tblOnlineActivity'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th valign=top>"+lblQuesSet+"</th>");
			sb.append("<th valign=top>lblScr</th>");
			sb.append("<th valign=top>"+lblResponse+"</th>");
			sb.append("<th valign=top>"+lblStatus+"</th>");
			sb.append("<th valign=top>"+lblDate+"</th>");
			sb.append("<th valign=top>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			
			List<OnlineActivityStatus> onlineActivityStatusList=onlineActivityStatusDAO.findOnlineActivityStatusList(teacherDetail,districtMaster,jobCategoryMaster);
			List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=onlineActivityQuestionSetDAO.getOnlineActivityQuestionSetList(districtMaster,jobCategoryMaster);
			List<OnlineActivityAnswers> dspqList=onlineActivityAnswersDAO.findOnlineActivityAnswers(teacherDetail,districtMaster,jobCategoryMaster);
			Map<String,Integer> scoreMap=new HashMap<String, Integer>();
			for (OnlineActivityAnswers onlineActivityAnswers : dspqList) {
				int totalScore=0;
				String key=onlineActivityAnswers.getTeacherDetail().getTeacherId()+"##"+onlineActivityAnswers.getJobCategoryId()+"##"+onlineActivityAnswers.getOnlineActivityQuestionSet().getQuestionSetId();
				int score=0;
				if(scoreMap.get(key)!=null)score=scoreMap.get(key);
				try{
					if(scoreMap.get(key)!=null){
						if(onlineActivityAnswers.getScore()!=null){
							totalScore=onlineActivityAnswers.getScore()+score;
							scoreMap.put(key,totalScore);
						}
					}else{
						if(onlineActivityAnswers.getScore()!=null){
							totalScore=onlineActivityAnswers.getScore();
							scoreMap.put(key,totalScore);
						}
					}
				}catch(Exception e){ 
					e.printStackTrace();
				}
			}
			int onlineActivityForStatus=0;
			Map<Integer,OnlineActivityStatus> onlineMap=new HashMap<Integer, OnlineActivityStatus>();
			for (OnlineActivityStatus onlineActivityStatus : onlineActivityStatusList) {
				onlineMap.put(onlineActivityStatus.getOnlineActivityQuestionSet().getQuestionSetId(),onlineActivityStatus);
			}
			
			for (OnlineActivityQuestionSet onlineActivityQuestionSet : onlineActivityQuestionSetList) {
				OnlineActivityStatus onlineActivityStatus=onlineMap.get(onlineActivityQuestionSet.getQuestionSetId());
				if(onlineActivityStatus!=null){
					String key=teacherDetail.getTeacherId()+"##"+jobCategoryMaster.getJobCategoryId()+"##"+onlineActivityQuestionSet.getQuestionSetId();
					sb.append("<tr>");				
					sb.append("<td valign=middle>"+onlineActivityStatus.getOnlineActivityQuestionSet().getQuestionSetName()+"</td>");
					if(scoreMap.get(key)!=null){
						sb.append("<td valign=middle>"+scoreMap.get(key)+"</td>");
					}else{
						sb.append("<td valign=middle>"+optNA+"</td>");
					}
					String statusName="";
					System.out.println("onlineActivityStatus.getStatus()==="+onlineActivityStatus.getStatus());
					if(onlineActivityStatus.getStatus()!=null && onlineActivityStatus.getStatus().equalsIgnoreCase("S")){
						if(onlineActivityForStatus!=2)
						onlineActivityForStatus=1;
						statusName="Link Sent";
					}else if(onlineActivityStatus.getStatus()!=null && onlineActivityStatus.getStatus().equalsIgnoreCase("I")){
						onlineActivityForStatus=2;
						statusName="InComplete";
					}else if(onlineActivityStatus.getStatus()!=null && onlineActivityStatus.getStatus().equalsIgnoreCase("C")){
						onlineActivityForStatus=2;
						statusName=msgOnlineActivityCompleted;
					}
					if(statusName.equalsIgnoreCase("Link Sent")|| statusName.equalsIgnoreCase("")){
						sb.append("<td valign=middle style='padding-top:10px;'><span class='fa-list-alt icon-large'></span></td>");
					}else{
						sb.append("<td valign=middle style='padding-top:10px;'><a href='#'  onclick=\"showOnlineActivityQuestion("+onlineActivityQuestionSet.getQuestionSetId()+");\"><span class='fa-list-alt icon-large iconcolor'></span></a></td>");
					}
					
					sb.append("<td valign=middle>"+statusName+"</td>");
					
					String activityDate="";
					System.out.println("statusName==="+statusName);
					if(statusName.equalsIgnoreCase("Link Sent")){
						activityDate=Utility.convertDateAndTimeToUSformatOnlyDate(onlineActivityStatus.getActivityDate());
					}else if(statusName.equalsIgnoreCase("InComplete")){
						activityDate=Utility.convertDateAndTimeToUSformatOnlyDate(onlineActivityStatus.getActivityStartTime());
					}else if(statusName.equalsIgnoreCase("Online Activity Completed")){
						activityDate=Utility.convertDateAndTimeToUSformatOnlyDate(onlineActivityStatus.getActivityEndTime());
					}
					sb.append("<td valign=middle>"+activityDate+"</td>");
					if(statusName.equalsIgnoreCase("Link Sent")){
						sb.append("<td valign=middle><a "+clickable+" href='#' onclick=\"sendOnlineActivityLink('"+onlineActivityQuestionSet.getQuestionSetId()+"','0');\">"+btnResend+"</a></td>");
						sb.append("</tr>");
					}else{
						if(onlineActivityQuestionSet.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Teacher") && statusName.equalsIgnoreCase("InComplete"))//added by 07-04-2015
							sb.append("<td valign=middle><a "+clickable+" href='#' onclick=\"sendOnlineActivityLink('"+onlineActivityQuestionSet.getQuestionSetId()+"','0');\">"+btnResend+"</a></td>");
						else
						sb.append("<td valign=middle>&nbsp;</td>");
						sb.append("</tr>");
					}
				}else{
					sb.append("<tr>");				
					sb.append("<td valign=middle>"+onlineActivityQuestionSet.getQuestionSetName()+"</td>");
					sb.append("<td valign=middle>"+optNA+"</td>");
					sb.append("<td valign=middle><span class='fa-list-alt icon-large'></span></td>");
					sb.append("<td valign=middle>"+lblPending+"</td>");
					String activityDate=Utility.convertDateAndTimeToUSformatOnlyDate(onlineActivityQuestionSet.getCreatedDateTime());
					sb.append("<td valign=middle>"+activityDate+"</td>");
					sb.append("<td valign=middle><a "+clickable+"  href='#' onclick=\"sendOnlineActivityLink('"+onlineActivityQuestionSet.getQuestionSetId()+"','1');\">"+btnSend+"</a></td>");
					sb.append("</tr>");
				}
			}
			sb.append("<input type='hidden' id='onlineActivityForStatus' value='"+onlineActivityForStatus+"'/>");			
			
			if(onlineActivityQuestionSetList.size()==0){
				sb.append("<tr>");				
				sb.append("<td colspan=4>"+msgNoQuestion+"</td>");
				sb.append("</tr>");
			}
			sb.append("</table></td></tr>");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();

	}
	 public String sendOnlineActivityMail(Integer teacherId,Integer jobId,Integer districtId,Integer questionSetId,HttpServletRequest request)throws Exception{
		 	System.out.println(":::::::::sendOnlineActivityMail>>:::::::::::::");
		 	JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		    Integer jobCategoryId =  jobOrder.getJobCategoryMaster().getJobCategoryId();
			String encodedStr="onlineactivity.do?key="+Utility.encodeInBase64(teacherId+"##"+jobCategoryId+"##"+districtId+"##"+questionSetId);
		   // String encodedStr="onlineactivity.do?key="+Utility.encodeInBase64(teacherId+"##"+jobCategoryId+"##"+districtId); //+"##"+questionSetId
			JobCategoryMaster jobCategoryMaster=jobOrder.getJobCategoryMaster();
			DistrictMaster dm=districtMasterDAO.findById(districtId, false, false);
			TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			OnlineActivityQuestionSet onlineActivityQuestionSet=onlineActivityQuestionSetDAO.findById(questionSetId, false, false);
			List<OnlineActivityStatus> lasLst=onlineActivityStatusDAO.findByCriteria(Restrictions.eq("onlineActivityQuestionSet", onlineActivityQuestionSet),Restrictions.eq("teacherDetail", teacherDetail),Restrictions.eq("districtMaster",dm),Restrictions.eq("jobCategoryMaster",jobCategoryMaster));
			System.out.println("lasLst=="+lasLst.size());
			if(lasLst.size()>0){
				OnlineActivityStatus oas=lasLst.get(0);
				oas.setActivityDate(new Date());
				onlineActivityStatusDAO.updatePersistent(oas);
			}
			boolean principalCategory=false;
			if(jobCategoryMaster!=null && jobCategoryMaster.getJobCategoryName().equalsIgnoreCase("Principal")){
				principalCategory=true;
			}
			System.out.println("principalCategory:::::::::"+principalCategory);
			int template=0;
			String subject="";
			if(getTempletebyDate()==true)
				template=1;
			String templatetoSend= MailText.sendMailLinkTemplate(request,jobOrder,teacherDetail,encodedStr,jobOrder.getDistrictMaster(),template,principalCategory);
			
			if(principalCategory){
				if(template!=1){
					subject=msgInvitationOnlineActivity;
				}else
				{
					subject=msgInvitationOnlineActivity;
				}
			}else{
				if(template!=1){
					subject=msgInvitationOnlineActivity;
				}else
				{
					subject=msgInvitationOnlineActivity;
				}
			}
			
			
			System.out.println(":"+templatetoSend);
			System.out.println("subject:::"+subject);
			System.out.println("email:::::"+teacherDetail.getEmailAddress());
			try{
				emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),subject,templatetoSend);
				System.out.println("Mail set successfully");
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		 return "success";
	   }
	 
	 public String resetOnlineActivityMail(Integer teacherId,Integer jobCategoryId,Integer districtId,HttpServletRequest request)throws Exception{
		 	System.out.println(":::::::::ResendOnlineActivityMail=:::::::::::::");
			String encodedStr="onlineactivity.do?key="+Utility.encodeInBase64(teacherId+"##"+jobCategoryId+"##"+districtId);
			TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			
			System.out.println(" district ID>>>>>>>>>>> "+districtId);
			DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
			JobCategoryMaster jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
			boolean principalCategory=false;
			if(jobCategoryMaster!=null && jobCategoryMaster.getJobCategoryName().equalsIgnoreCase("Principal")){
				principalCategory=true;
			}
			System.out.println("principalCategory:::::::::"+principalCategory);
			int template=0;
			String subject="";
			
			if(getTempletebyDate()==true)
				template=1;
			
			String templatetoSend= MailText.resetOnlineActEmail(request,teacherDetail,encodedStr,districtMaster,template,principalCategory);
			if(principalCategory){
				if(template!=1){
					subject=msgInvitationOnlineActivity;
				}else
				{
					subject=msgInvitationOnlineActivity;
				}
			}else{
				if(template!=1){
					subject=msgInvitationOnlineActivity;
				}else
				{
					subject=msgInvitationOnlineActivity;
				}
			}
			System.out.println(":"+templatetoSend);
			System.out.println("subject:::"+subject);
			System.out.println("email:::::"+teacherDetail.getEmailAddress());
			try{
				emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),subject,templatetoSend);
				System.out.println("Mail set successfully");
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		 return "success";
	   }
	 
	 public boolean getTempletebyDate()
	 {
		 boolean chk = false;
		try
		{
		 	Calendar cal = Calendar.getInstance();
			Calendar cal1 = Calendar.getInstance();

			//cal.set(2015, Calendar.APRIL, 02);
			
			cal1.set(2015, Calendar.MARCH, 02);
			Date d = cal.getTime();
	        Date d1 = cal1.getTime();
	        
	        if(d.compareTo(d1)<0 || d.compareTo(d1)==0)
	        {
	        	chk=true;
	        }
	        
	        
	        
	        return chk;
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		 
	    return chk;
	 }
	 
	 /*
	  * @Author Ram Nath
	  *
	  */
	 public String sendAdvOnBoarding(TeacherDetail teacherDetail,JobOrder jobOrder,Integer districtId,HttpServletRequest request,UserMaster userMaster)throws Exception{
		 	System.out.println(":::::::::sendOnlineActivityMailForLetterOfCommitment=:::::::::::::");
		    Integer jobCategoryId =  jobOrder.getJobCategoryMaster().getJobCategoryId();
	
			int template=0;
			String subject="";
			
			String templatetoSend= MailText.getTempAdvOnBoarding(request,jobOrder,teacherDetail,jobOrder.getDistrictMaster(),userMaster);
				subject=lblLetterCommitment;
			
			System.out.println(":"+templatetoSend);
			System.out.println("subject:::"+subject);
			System.out.println("email:::::"+teacherDetail.getEmailAddress());
			try{
				List<JobForTeacher> lstJFT=jobForTeacherDAO.findByCriteria(Restrictions.eq("teacherId", teacherDetail),Restrictions.eq("jobId", jobOrder));
				if(lstJFT.size()>0){
				JobForTeacher jft=lstJFT.get(0);
				jft.setOfferReady(true);
				jft.setOfferMadeDate(new Date());
				jobForTeacherDAO.updatePersistent(jft);
				emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),subject,templatetoSend);//
				System.out.println("Mail set successfully");
				}else{
				System.out.println("Mail not set");
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		 return "success";
	   }
	 
	 //added by Ram nath for staffer mail for philadelphia
	 public boolean sendEvlComplMail(TeacherDetail teacherDetail,JobOrder jobOrder,Integer districtId,UserMaster userMaster,int temp,UserMaster schoolUser,String statusFlagAndStatusId)throws Exception{
		 System.out.println(":::::::::sendOnlineActivityMailForLetterOfCommitment=:::::::::::::");
		 UserMaster stfferUser=userMaster;
		
		    Integer jobCategoryId =  jobOrder.getJobCategoryMaster().getJobCategoryId();
		    String email="";
			String subject="";
			if(temp==0){
				subject=lblEvaluationComplete;
				 try{
					 stfferUser=getStafferUser(jobOrder,teacherDetail,userMaster);
				 }catch(Exception e){e.printStackTrace();}
			}
			else if(temp==1)
				subject=lblEvaluationComplete;
			else if(temp==2)
				subject=lblTeacheracceptedposition;
			else if(temp==3)
				subject=lblTeacherdeclinedpos;			
			String templatetoSend= MailText.getTempEvlCompl(jobOrder,teacherDetail,jobOrder.getDistrictMaster(),stfferUser,temp,schoolUser.getSchoolId().getSchoolName(),statusFlagAndStatusId);			
			try{				
					if(temp==0){
						email=stfferUser.getEmailAddress();
						List<JobForTeacher> lstJFT=jobForTeacherDAO.findByCriteria(Restrictions.eq("teacherId", teacherDetail),Restrictions.eq("jobId", jobOrder));
						JobForTeacher jft=null;
						if(lstJFT.size()>0)
							jft=lstJFT.get(0);
						if(jft!=null){						
						jft.setOfferReady(false);
						jft.setOfferMadeDate(new Date());	
						jobForTeacherDAO.updatePersistent(jft);
						}
					}else if(temp==1){
						email=teacherDetail.getEmailAddress();
						List<JobForTeacher> lstJFT=jobForTeacherDAO.findByCriteria(Restrictions.eq("teacherId", teacherDetail),Restrictions.eq("jobId", jobOrder));
						JobForTeacher jft=null;
						if(lstJFT.size()>0)
							jft=lstJFT.get(0);
						if(jft!=null){						
						jft.setOfferReady(true);
						jft.setOfferMadeDate(new Date());
						jft.setOfferAccepted(null);
						jft.setOfferAcceptedDate(null);
						jobForTeacherDAO.updatePersistent(jft);
						}
					}else if(temp==2){
						email=userMaster.getEmailAddress();
						/*jft.setOfferAccepted(true);
						jft.setOfferAcceptedDate(new Date());*/
					}else if(temp==3){
						//email=userMaster.getEmailAddress();	
						email=schoolUser.getEmailAddress();
						/*jft.setOfferAccepted(false);
						jft.setOfferAcceptedDate(new Date());*/
					}
				System.out.println("email1:::::"+email+" Html Mail:::>>"+templatetoSend);				
				emailerService.sendMailAsHTMLText(email,subject,templatetoSend);//
				if(temp==2){
					System.out.println("email2:::::"+schoolUser.getEmailAddress()+" Html Mail:::>>"+templatetoSend);
				emailerService.sendMailAsHTMLText(schoolUser.getEmailAddress(),subject,templatetoSend);//
				}				
				System.out.println("Mail set successfully");
				return true;					
			}catch (Exception e) {
				e.printStackTrace();
				return false;
			}		
	 }
	 private UserMaster getStafferUser(JobOrder jobOrder,TeacherDetail teacherDetail,UserMaster userMaster)
	 {
		 SchoolMaster userSchoolMaster=null; 			
			if(userMaster.getEntityType()==3){
				if(userMaster.getSchoolId()!=null){
					userSchoolMaster=userMaster.getSchoolId();
				}
			}
			List<PanelSchedule> pSList=panelScheduleDAO.findPanelSchedules(teacherDetail,jobOrder);
			List<StafferMaster>	sList=stafferMasterDAO.findStafferMasterList(jobOrder.getDistrictMaster(),userSchoolMaster);
			List<DistrictKeyContact> dKCList=new ArrayList<DistrictKeyContact>();
			List<UserMaster> userMasters=new ArrayList<UserMaster>();
			UserMaster stafferUser=null;
			if(sList.size()==0){
				try{
					dKCList=districtKeyContactDAO.findByContactType(jobOrder.getDistrictMaster(),"Staffer");	
					if(dKCList.size()>0){
						userMasters=userMasterDAO.findByEmailAndDate(dKCList.get(0).getKeyContactEmailAddress());
						if(userMasters.size()>0){
							for (UserMaster userMasterObj : userMasters) {
								stafferUser=userMasterObj;
								break;
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}else{
				stafferUser= sList.get(0).getUserMaster();
			}
			return stafferUser;
	 }
	//added by 08-04-2014
	public String displayMultipleOnlineActivityForCG(String teacherIds,Integer jobId,Integer districtId)
	{
		System.out.println(":::::::::::::::    displayOnlineActivityForCG    :::::::::::::::::::;; teacherId==="+teacherIds);

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		StringBuffer sb=new StringBuffer();
		try {
			//TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherIds[0], false, false);
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			JobCategoryMaster jobCategoryMaster=jobOrder.getJobCategoryMaster();
			
			sb.append("<table border='0' class='table' id='tblOnlineActivityMass'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th valign=top>"+lblQuesSet+"</th>");
			//sb.append("<th valign=top>Date</th>");
			sb.append("<th valign=top>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			
			//List<OnlineActivityStatus> onlineActivityStatusList=onlineActivityStatusDAO.findOnlineActivityStatusList(teacherDetail,districtMaster,jobCategoryMaster);
			List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=onlineActivityQuestionSetDAO.getOnlineActivityQuestionSetList(districtMaster,jobCategoryMaster);
			
			
			for (OnlineActivityQuestionSet onlineActivityQuestionSet : onlineActivityQuestionSetList) {				
					sb.append("<tr>");				
					sb.append("<td valign=top>"+onlineActivityQuestionSet.getQuestionSetName()+"</td>");
					/*String activityDate=Utility.convertDateAndTimeToUSformatOnlyDate(onlineActivityQuestionSet.getCreatedDateTime());
					sb.append("<td>"+activityDate+"</td>");*/
					sb.append("<td valign=top><a id='' href='#' onclick=\"sendMultipleOnlineActivityLink(this,'"+onlineActivityQuestionSet.getQuestionSetId()+"','"+teacherIds+"');\">"+btnSend+"</a></td>");
					sb.append("</tr>");
				}

			if(onlineActivityQuestionSetList.size()==0){
				sb.append("<tr>");				
				sb.append("<td colspan=4>"+msgNoQuestion+"</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();

	}
		//ended by 08-04-2015
	//added by 08-04-2015
	public String displayOnlineActivityNotesForCG(Integer teacherId,Integer jobId,Integer districtId,Integer questionSetIdForCg)
	{
	System.out.println(":::::"+teacherId+"::::::"+jobId+"::::    displayOnlineActivityNotesForCG    :::"+districtId+":::::::"+questionSetIdForCg+":::::::::;; teacherId==="+teacherId);

	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	UserMaster userMaster=null;
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(msgYrSesstionExp);
	}
	else{
		userMaster = (UserMaster) session.getAttribute("userMaster");
	}
	StringBuffer sb=new StringBuffer();
	try {
		TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
		DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
		JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		JobCategoryMaster jobCategoryMaster=jobOrder.getJobCategoryMaster();
		OnlineActivityQuestionSet onlineActivityQuestionSet=onlineActivityQuestionSetDAO.findById(questionSetIdForCg, false, false);
		List<OnlineActivityNotes> lstOAN=onlineActivityNotesDAO.findByCriteria(Restrictions.eq("teacherDetail", teacherDetail),Restrictions.eq("jobOrder", jobOrder),Restrictions.eq("questionSetId", onlineActivityQuestionSet),Restrictions.eq("districtMaster", districtMaster),Restrictions.eq("userMaster", userMaster));
		
		sb.append("<table border='0' class='table' id='tblOnlineActivityNotes'>");
		sb.append("<thead class='bg'>");
		sb.append("<tr>");
		sb.append("<th class='thClick' valign=top>"+blNote+"</th>");
		sb.append("<th class='thClick' valign=top>"+lblCreatedBy+"</th>");
		sb.append("<th  class='thClick' valign=top>"+lblDate+"</th>");
		sb.append("</tr>");
		sb.append("</thead>");
		if(lstOAN.size()>0){
			for(OnlineActivityNotes oan:lstOAN){
				sb.append("<tr>");
				sb.append("<td>"+oan.getOnlineActivityNotes()+"</td>");
				String activityDate=Utility.convertDateAndTimeToUSformatOnlyDate(oan.getCreatedDateTime());
				sb.append("<td>"+oan.getUserMaster().getFirstName()+" "+oan.getUserMaster().getLastName()+"</td>");
				sb.append("<td>"+activityDate+"</td>");
				sb.append("</tr>");
			}
		}else if(lstOAN.size()==0){
			sb.append("<tr>");				
			sb.append("<td colspan=3>"+lblNoNoteAdded+"</td>");
			sb.append("</tr>");
		}
		sb.append("</table>");

	} catch (Exception e) {
		e.printStackTrace();
	}
	return sb.toString();
}

		
}	