package tm.services.teacher;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;



public class MailSendSchedulerThread extends Thread{

	@Autowired
	private OnlineActivityAjax onlineActivityAjax;
	
	private HttpServletRequest request;
	private Integer jobId;
	private Integer teacherId;
	private Integer jobCategoryId;
	private Integer districtId;
	private Integer onlineActivityFlag;
	private Integer questionSetId;
	
	public Integer getQuestionSetId() {
		return questionSetId;
	}

	public void setQuestionSetId(Integer questionSetId) {
		this.questionSetId = questionSetId;
	}

	public Integer getJobCategoryId() {
		return jobCategoryId;
	}

	public void setJobCategoryId(Integer jobCategoryId) {
		this.jobCategoryId = jobCategoryId;
	}

	public Integer getOnlineActivityFlag() {
		return onlineActivityFlag;
	}

	public void setOnlineActivityFlag(Integer onlineActivityFlag) {
		this.onlineActivityFlag = onlineActivityFlag;
	}

	public OnlineActivityAjax getOnlineActivityAjax() {
		return onlineActivityAjax;
	}

	public void setOnlineActivityAjax(OnlineActivityAjax onlineActivityAjax) {
		this.onlineActivityAjax = onlineActivityAjax;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public Integer getJobId() {
		return jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}

	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public MailSendSchedulerThread() {
		super();
	}
	
	public void run()
	{
	 try{
		System.out.println(":::::::::  Call ::::::::::::::::::"+getOnlineActivityFlag());
		if(getOnlineActivityFlag()==0)
			onlineActivityAjax.sendOnlineActivityMail(teacherId,jobId,districtId,questionSetId,request);
		else if(getOnlineActivityFlag()==1)
			onlineActivityAjax.resetOnlineActivityMail(teacherId,jobCategoryId,districtId,request);
		System.out.println("::::::::: Called ::::::::::::::::");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
