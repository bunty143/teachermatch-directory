package tm.services.teacher;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.districtassessment.CenterMaster;
import tm.bean.districtassessment.ExaminationCodeDetails;
import tm.bean.user.UserMaster;
import tm.services.EmailerService;
import tm.services.ExamAjax;



public class AssessmentInviteThread extends Thread{
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService){
		this.emailerService = emailerService;
	}
	public EmailerService getEmailerService() {
		return emailerService;
	}
	private ExamAjax examAjax;
	public void setExamAjax(ExamAjax examAjax) {
		this.examAjax = examAjax;
	}
	private HttpServletRequest request;
	private List<UserMaster> userMasterList;
	
	private TeacherDetail  teacherDetail;
	private JobOrder jobOrder;
	
	private ExaminationCodeDetails examinationCodeDetails;
	private String icalFile;
	
	private CenterMaster centerMaster;

	
	public String getIcalFile() {
		return icalFile;
	}
	public void setIcalFile(String icalFile) {
		this.icalFile = icalFile;
	}
	public CenterMaster getCenterMaster() {
		return centerMaster;
	}
	public void setCenterMaster(CenterMaster centerMaster) {
		this.centerMaster = centerMaster;
	}
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}	
	public List<UserMaster> getUserMasterList() {
		return userMasterList;
	}
	public void setUserMasterList(List<UserMaster> userMasterList) {
		this.userMasterList = userMasterList;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}

	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}

	public JobOrder getJobOrder() {
		return jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	
	public ExaminationCodeDetails getExaminationCodeDetails() {
		return examinationCodeDetails;
	}

	public void setExaminationCodeDetails(
			ExaminationCodeDetails examinationCodeDetails) {
		this.examinationCodeDetails = examinationCodeDetails;
	}

	public AssessmentInviteThread() {
		super();
	}
	
	public void run()
	{
		try{
			System.out.println(":::::::::Call Here::::::::::::::");
			examAjax.mailToTeacherByThread(jobOrder,teacherDetail,userMasterList,request,examinationCodeDetails,emailerService,icalFile,centerMaster);
			System.out.println("Call Here::::::::::::::");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
