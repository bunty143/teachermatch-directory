package tm.services.teacher;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.DistrictPortfolioConfig;
import tm.bean.DistrictSpecificTeacherTfaOptions;
import tm.bean.DistrictSpecificTfaOptions;
import tm.bean.DistrictWiseCandidateName;
import tm.bean.EmployeeMaster;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobWiseTeacherDetails;
import tm.bean.OctDetails;
import tm.bean.SchoolPreferencebyCandidate;
import tm.bean.TeacherAffidavit;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.TeacherHonor;
import tm.bean.TeacherInvolvement;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherResidencyDetails;
import tm.bean.TeacherRole;
import tm.bean.master.CityMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.CurrencyMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioAnswers;
import tm.bean.master.EthinicityMaster;
import tm.bean.master.EthnicOriginMaster;
import tm.bean.master.GenderMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.TFAAffiliateMaster;
import tm.bean.master.TFARegionMaster;
import tm.bean.teacher.StudentTeacherExperience;
import tm.bean.teacher.TeacherLanguages;
import tm.dao.DistrictPortfolioConfigDAO;
import tm.dao.DistrictSpecificTeacherTfaOptionsDAO;
import tm.dao.DistrictSpecificTfaOptionsDAO;
import tm.dao.DistrictWiseCandidateNameDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobWiseTeacherDetailsDAO;
import tm.dao.OctDetailsDAO;
import tm.dao.SchoolPreferenceByCandidateDAO;
import tm.dao.TeacherAffidavitDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherHonorDAO;
import tm.dao.TeacherInvolvementDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherResidencyDetailsDAO;
import tm.dao.TeacherRoleDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.CurrencyMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificPortfolioAnswersDAO;
import tm.dao.master.EmpRoleTypeMasterDAO;
import tm.dao.master.EthinicityMasterDAO;
import tm.dao.master.EthnicOriginMasterDAO;
import tm.dao.master.GenderMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.TFAAffiliateMasterDAO;
import tm.dao.master.TFARegionMasterDAO;
import tm.dao.teacher.StudentTeacherExperienceDAO;
import tm.dao.teacher.TeacherLanguagesDAO;
import tm.services.PaginationAndSorting;
import tm.utility.Utility;

public class PFExperiences 
{
	
	
	 String locale = Utility.getValueOfPropByKey("locale");
	 String lblOrga=Utility.getLocaleValuePropByKey("lblOrga", locale);
	 String lblTpe=Utility.getLocaleValuePropByKey("lblTpe", locale);
	 String lblNumberofPeople=Utility.getLocaleValuePropByKey("lblNumberofPeople", locale);
	 String lblNumPeLed=Utility.getLocaleValuePropByKey("lblNumPeLed", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String lblHonors=Utility.getLocaleValuePropByKey("lblHonors", locale);
	 String lblRole=Utility.getLocaleValuePropByKey("lblRole", locale);
	 String lblOrganizationEmp=Utility.getLocaleValuePropByKey("lblOrganizationEmp", locale);
	 String lblDuration=Utility.getLocaleValuePropByKey("lblDuration", locale);
	 String lblTypOfRole=Utility.getLocaleValuePropByKey("lblTypOfRole", locale);
	 String lbllDocName=Utility.getLocaleValuePropByKey("lbllDocName", locale);
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lbltoPresent=Utility.getLocaleValuePropByKey("lbltoPresent", locale);
	 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	 String lnkDlt=Utility.getLocaleValuePropByKey("lnkDlt", locale);
	 String msgNorecordfound=Utility.getLocaleValuePropByKey("msgNorecordfound", locale);
	 String lblSchoolName=Utility.getLocaleValuePropByKey("lblSchoolName", locale);
	 String lblSub=Utility.getLocaleValuePropByKey("lblSub", locale);
	 String lblFrDate=Utility.getLocaleValuePropByKey("lblFrDate", locale);
	 String lblToDate=Utility.getLocaleValuePropByKey("lblToDate", locale);
	 String lblLanguage=Utility.getLocaleValuePropByKey("lblLanguage", locale);
	 String lblOralSkill=Utility.getLocaleValuePropByKey("lblOralSkill", locale);
	 String lblWrittenSkill=Utility.getLocaleValuePropByKey("lblWrittenSkill", locale);
	 
	 
	@Autowired
	private TFAAffiliateMasterDAO tFAAffiliateMasterDAO;

	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	public void setTeacherExperienceDAO(TeacherExperienceDAO teacherExperienceDAO) 
	{
		this.teacherExperienceDAO = teacherExperienceDAO;
	}
	@Autowired
	private OctDetailsDAO octDetailsDAO;
	@Autowired
	private TeacherRoleDAO teacherRoleDAO;
	public void setTeacherRoleDAO(TeacherRoleDAO teacherRoleDAO) 
	{
		this.teacherRoleDAO = teacherRoleDAO;
	}

	//SWADESH
	@Autowired
	 private SchoolPreferenceByCandidateDAO schoolPreferenceByCandidateDAO;
	 public void setSchoolPreferenceByCandidateDAO(SchoolPreferenceByCandidateDAO schoolPreferenceByCandidateDAO) 
	 {
	  this.schoolPreferenceByCandidateDAO = schoolPreferenceByCandidateDAO;
	 }
	
	@Autowired
	private EmpRoleTypeMasterDAO empRoleTypeMasterDAO;
	public void setEmpRoleTypeMasterDAO(EmpRoleTypeMasterDAO empRoleTypeMasterDAO) 
	{
		this.empRoleTypeMasterDAO = empRoleTypeMasterDAO;
	}

	@Autowired
	private TeacherInvolvementDAO teacherInvolvementDAO;
	public void setTeacherInvolvementDAO(TeacherInvolvementDAO teacherInvolvementDAO) 
	{
		this.teacherInvolvementDAO = teacherInvolvementDAO;
	}

	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) 
	{
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}

	@Autowired
	private TeacherHonorDAO teacherHonorDAO;
	public void setTeacherHonorDAO(TeacherHonorDAO teacherHonorDAO) 
	{
		this.teacherHonorDAO = teacherHonorDAO;
	}
	@Autowired
	private CurrencyMasterDAO currencyMasterDAO;
	public void setCurrencyMasterDAO(CurrencyMasterDAO currencyMasterDAO) 
	{
		this.currencyMasterDAO = currencyMasterDAO;
	}

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired 
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private TFAAffiliateMasterDAO tfaAffiliateMasterDAO;
	public void setTfaAffiliateMasterDAO(TFAAffiliateMasterDAO tfaAffiliateMasterDAO) {
		this.tfaAffiliateMasterDAO = tfaAffiliateMasterDAO;
	}
	
	@Autowired
	private TFARegionMasterDAO tfaRegionMasterDAO;
	public void setTfaRegionMasterDAO(TFARegionMasterDAO tfaRegionMasterDAO) {
		this.tfaRegionMasterDAO = tfaRegionMasterDAO;
	}

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(
			TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}

	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO) {
		this.cityMasterDAO = cityMasterDAO;
	}

	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}

	@Autowired
	private TeacherAffidavitDAO teacherAffidavitDAO;
	public void setTeacherAffidavitDAO(TeacherAffidavitDAO teacherAffidavitDAO) {
		this.teacherAffidavitDAO = teacherAffidavitDAO;
	}

	@Autowired
	private DistrictPortfolioConfigDAO districtPortfolioConfigDAO;
	public void setDistrictPortfolioConfigDAO(
			DistrictPortfolioConfigDAO districtPortfolioConfigDAO) {
		this.districtPortfolioConfigDAO = districtPortfolioConfigDAO;
	}

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private DistrictWiseCandidateNameDAO districtWiseCandidateNameDAO;
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;

	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;

	@Autowired
	private EthnicOriginMasterDAO ethnicOriginMasterDAO;

	@Autowired
	private EthinicityMasterDAO ethinicityMasterDAO; 

	@Autowired
	private GenderMasterDAO genderMasterDAO;

	@Autowired
	private DistrictMasterDAO districtMasterDAO;

	@Autowired
	private TeacherResidencyDetailsDAO teacherResidencyDetailsDAO;
	
	@Autowired
	private CountryMasterDAO countryMasterDAO;

	@Autowired
	private DashboardAjax dashboardAjax;

	@Autowired
	private JobOrderDAO jobOrderDAO;

	@Autowired
	private DistrictSpecificTfaOptionsDAO districtSpecificTfaOptionsDAO;
	@Autowired
	private DistrictSpecificTeacherTfaOptionsDAO districtSpecificTeacherTfaOptionsDAO;
	@Autowired
	private StudentTeacherExperienceDAO studentTeacherExperienceDAO;
	@Autowired
	private TeacherLanguagesDAO teacherLanguagesDAO;
	@Autowired
	private DistrictSpecificPortfolioAnswersDAO districtSpecificPortfolioAnswersDAO;
	@Autowired
	private JobWiseTeacherDetailsDAO jobWiseTeacherDetailsDAO;
	public String addOrEditResume(String resume)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try 
		{


			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);

			if(teacherExperience==null)
			{
				teacherExperience = new TeacherExperience();
				teacherExperience.setTeacherId(teacherDetail);
				teacherExperience.setCanServeAsSubTeacher(2);
				TFAAffiliateMaster tfaAffiliateMaster	=	tFAAffiliateMasterDAO.findById(3, false, false);
				teacherExperience.setTfaAffiliateMaster(tfaAffiliateMaster);
			}
			teacherExperience.setResume(resume);
			teacherExperienceDAO.makePersistent(teacherExperience);

			boolean bTeacherPortfolioStatus=false;
			TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(teacherPortfolioStatus==null)
			{
				bTeacherPortfolioStatus=true;
				teacherPortfolioStatus = new TeacherPortfolioStatus();
				teacherPortfolioStatus.setIsExperiencesCompleted(true);
			}
			else
			{
				teacherPortfolioStatus.setIsExperiencesCompleted(true);
			}


			try{
				if(teacherPortfolioStatus.getIsPersonalInfoCompleted()==null)
				{
					teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsCertificationsCompleted()==null){
					teacherPortfolioStatus.setIsCertificationsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsCertificationsCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsExperiencesCompleted()==null){
					teacherPortfolioStatus.setIsExperiencesCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsExperiencesCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsAffidavitCompleted()==null){
					teacherPortfolioStatus.setIsAffidavitCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAffidavitCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsAcademicsCompleted()==null){
					teacherPortfolioStatus.setIsAcademicsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAcademicsCompleted(false);
				e.printStackTrace();
			}


			//teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			TeacherPortfolioStatus tpsTemp=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(bTeacherPortfolioStatus && tpsTemp==null)
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			else
			{
				teacherPortfolioStatus.setId(tpsTemp.getId());
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			}

			////////// change jft status after uploading resume 
			/*StatusMaster statusMasterCOMP = WorkThreadServlet.statusMap.get("comp");
			StatusMaster statusMasterICOMP = WorkThreadServlet.statusMap.get("icomp");
			List<JobForTeacher> jobForTeachers = jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail, statusMasterICOMP);
			if(jobForTeachers.size()>0)
			{
				List<JobOrder> jobOrders = new ArrayList<JobOrder>();
				List<JobForTeacher> jftNoBaseNoJSI = new ArrayList<JobForTeacher>();
				List<JobForTeacher> jftBaseAndJSI = new ArrayList<JobForTeacher>();
				List<JobForTeacher> jftBaseNoJSI = new ArrayList<JobForTeacher>();
				List<JobForTeacher> jftJSINoBase = new ArrayList<JobForTeacher>();
				Map<Integer,JobForTeacher> jftMap = new HashMap<Integer,JobForTeacher>();
				JobOrder jobOrder = null;
				for (JobForTeacher jobForTeacher : jobForTeachers) {
					jobOrder = jobForTeacher.getJobId();
					if(jobOrder.getIsJobAssessment()==false && jobOrder.getJobCategoryMaster().getBaseStatus()==false)
						jftNoBaseNoJSI.add(jobForTeacher);
					else if(jobOrder.getIsJobAssessment()==true && jobOrder.getJobCategoryMaster().getBaseStatus()==true)
					{
						jftBaseAndJSI.add(jobForTeacher);
						jobOrders.add(jobOrder);
						jftMap.put(jobOrder.getJobId(), jobForTeacher);
					}
					else if(jobOrder.getIsJobAssessment()==true && jobOrder.getJobCategoryMaster().getBaseStatus()==false)
					{
						jftJSINoBase.add(jobForTeacher);
						jobOrders.add(jobOrder);
						jftMap.put(jobOrder.getJobId(), jobForTeacher);
					}
					else if(jobOrder.getIsJobAssessment()==false && jobOrder.getJobCategoryMaster().getBaseStatus()==true)
						jftBaseNoJSI.add(jobForTeacher);

				}
				// jsi is required
				if(jobOrders.size()>0)
				{
					List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
					teacherAssessmentStatusList = teacherAssessmentStatusDAO.findJSITaken(teacherDetail,jobOrders);
					for (TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList) {
						JobForTeacher jft = jftMap.get(teacherAssessmentStatus.getJobOrder().getJobId());
						jft.setStatus(teacherAssessmentStatus.getStatusMaster());
						jft.setStatusMaster(teacherAssessmentStatus.getStatusMaster());
						jobForTeacherDAO.makePersistent(jft);
					}
				}

				//base required no jsi
				if(jftBaseNoJSI.size()>0)
				{
					JobOrder jOrder = new JobOrder();
					jOrder.setJobId(0);
					List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
					teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jOrder);
					StatusMaster statusMaster = null;
					if(teacherAssessmentStatusList.size()>0){
						TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
						statusMaster = teacherAssessmentStatus.getStatusMaster();
						for (JobForTeacher jobForTeacher : jftBaseNoJSI) {
							jobForTeacher.setStatus(statusMaster);
							jobForTeacher.setStatusMaster(statusMaster);
							jobForTeacherDAO.makePersistent(jobForTeacher);
						}
					}
				}
				// no base no jsi
				for (JobForTeacher jobForTeacher : jftNoBaseNoJSI) {
					jobForTeacher.setStatus(statusMasterCOMP);
					jobForTeacher.setStatusMaster(statusMasterCOMP);
					jobForTeacherDAO.makePersistent(jobForTeacher);
				}
			}
*/
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	//pavan
	public String addOrEditBackgroundCheck(String filename)
	{
		System.out.println("filename============="+filename);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
			if(teacherPersonalInfo!=null){				
				teacherPersonalInfo.setBackgroundCheckDocument(filename);
				teacherPersonalInfo.setBgCheckUploadedDate(new Date());
				teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
			}
			
		}catch(Exception e)	{
			
			
		}
		return null;
	}



	public Boolean deleteResume()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try 
		{

			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);


			if(teacherExperience==null)
			{
				teacherExperience = new TeacherExperience();
				teacherExperience.setTeacherId(teacherDetail);
				teacherExperience.setCanServeAsSubTeacher(2);
				TFAAffiliateMaster tfaAffiliateMaster	=	tFAAffiliateMasterDAO.findById(3, false, false);
				teacherExperience.setTfaAffiliateMaster(tfaAffiliateMaster);
			}

			File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"\\"+teacherExperience.getResume());



			if(file.delete())
			{
				//System.out.println(file.getName() + " is deleted!");
			}
			else
			{
				//System.out.println("Delete operation is failed.");
			}
			teacherExperience.setResume("");
			teacherExperienceDAO.makePersistent(teacherExperience);
			boolean bTeacherPortfolioStatus=false;
			TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(teacherPortfolioStatus==null)
			{
				bTeacherPortfolioStatus=true;
				teacherPortfolioStatus = new TeacherPortfolioStatus();
				teacherPortfolioStatus.setIsExperiencesCompleted(false);
			}
			else
			{
				teacherPortfolioStatus.setIsExperiencesCompleted(false);
			}


			try{
				if(teacherPortfolioStatus.getIsPersonalInfoCompleted()==null)
				{
					teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsCertificationsCompleted()==null){
					teacherPortfolioStatus.setIsCertificationsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsCertificationsCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsExperiencesCompleted()==null){
					teacherPortfolioStatus.setIsExperiencesCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsExperiencesCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsAffidavitCompleted()==null){
					teacherPortfolioStatus.setIsAffidavitCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAffidavitCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsAcademicsCompleted()==null){
					teacherPortfolioStatus.setIsAcademicsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAcademicsCompleted(false);
				e.printStackTrace();
			}


			//teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);

			TeacherPortfolioStatus tpsTemp=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(bTeacherPortfolioStatus && tpsTemp==null)
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			else
			{
				teacherPortfolioStatus.setId(tpsTemp.getId());
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return null;
	}
	public String downloadResume()
	{
		System.out.println(new Date()+" Calling downloadResume method for Porfilio Resume ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String path="";

		try 
		{

			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);

			//String fileName= teacherExperience.getResume();

			if(teacherExperience.getResume()!=null && !teacherExperience.getResume().equalsIgnoreCase(""))
			{
				String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+teacherExperience.getResume();
				System.out.println("Porfilio Resume source "+source);

				String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
				System.out.println("Porfilio Resume target "+target);

				File sourceFile = new File(source);
				File targetDir = new File(target);
				if(!targetDir.exists())
					targetDir.mkdirs();

				File targetFile = new File(targetDir+"/"+sourceFile.getName());

				FileUtils.copyFile(sourceFile, targetFile);

				path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
				System.out.println("Porfilio Resume path "+path);
			}
			else
			{
				System.out.println("Porfilio Resume is null or Blank");
			}

		} 
		catch (Exception e) 
		{
			System.out.println("Error in Porfilio Resume ");
			e.printStackTrace();
		}

		return path;
	}
	//Pavan Background Download Start
	public String downloadBackgroundCheck()
	{
		System.out.println(new Date()+" Calling download Background Check method for Porfilio Background Check ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String path="";

		try 
		{

			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);

			//String fileName= teacherExperience.getResume();

			if(teacherPersonalInfo.getBackgroundCheckDocument()!=null && !teacherPersonalInfo.getBackgroundCheckDocument().equalsIgnoreCase(""))
			{
				String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+teacherPersonalInfo.getBackgroundCheckDocument();
				System.out.println("Porfilio Background Check source "+source);

				String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
				System.out.println("Porfilio Background Check target "+target);

				File sourceFile = new File(source);
				File targetDir = new File(target);
				if(!targetDir.exists())
					targetDir.mkdirs();

				File targetFile = new File(targetDir+"/"+sourceFile.getName());

				FileUtils.copyFile(sourceFile, targetFile);

				path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
				System.out.println("Porfilio Background Check path "+path);
			}
			else
			{
				System.out.println("Porfilio Background Check is null or Blank");
			}

		} 
		catch (Exception e) 
		{
			System.out.println("Error in Porfilio Background Check ");
			e.printStackTrace();
		}

		return path;
	}
	
	//End
	
	public String getPFEmploymentGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{		
		System.out.println(" ===========getPFEmploymentGrid================== ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		StringBuffer sb = new StringBuffer();		
		try 
		{
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			List<TeacherRole> listTeacherRole= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"roleStartYear";
			String sortOrderNoField		=	"roleStartYear";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("type") && !sortOrder.equals("empRoleTypeMaster")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("type")){
					sortOrderNoField="type";
				}
				if(sortOrder.equals("empRoleTypeMaster")){
					sortOrderNoField="empRoleTypeMaster";
				}

			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
			/**End ------------------------------------**/

			listTeacherRole = teacherRoleDAO.findSortedTeacherRoleByTeacher(sortOrderStrVal,teacherDetail);

			List<TeacherRole> sortedTeacherRole		=	new ArrayList<TeacherRole>();

			SortedMap<String,TeacherRole>	sortedMap = new TreeMap<String,TeacherRole>();
			SortedMap<Long,TeacherRole>	sortedIntMap = new TreeMap<Long,TeacherRole>();
			if(sortOrderNoField.equals("type"))
			{
				sortOrderFieldName	=	"type";
			}
			if(sortOrderNoField.equals("empRoleTypeMaster"))
			{
				sortOrderFieldName	=	"empRoleTypeMaster";
			}
			int mapFlag=2;
			int sortingDynaVal=99999;
			for (TeacherRole trRole : listTeacherRole){
				String orderFieldName=trRole.getRole();
				if(sortOrderFieldName.equals("type")){
					orderFieldName=trRole.getFieldMaster().getFieldName()+"||"+trRole.getRoleId();
					sortedMap.put(orderFieldName+"||",trRole);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("empRoleTypeMaster")){
					orderFieldName=trRole.getEmpRoleTypeMaster().getEmpRoleTypeName()+"||"+trRole.getRoleId();
					sortedMap.put(orderFieldName+"||",trRole);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("roleStartYear")){
					sortingDynaVal--;
					Long orderFieldInt;
					if(trRole!=null && trRole.getRoleStartMonth()!=null && trRole.getRoleStartMonth().toString().length()==1){
						orderFieldInt=Long.parseLong(trRole.getRoleStartYear()+0+trRole.getRoleStartMonth()+sortingDynaVal);
					}else{
						orderFieldInt=Long.parseLong(trRole.getRoleStartYear()+trRole.getRoleStartMonth()+sortingDynaVal);
					}
					System.out.println(orderFieldInt);
					sortedIntMap.put(orderFieldInt,trRole);

					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
			}

			if(sortOrderFieldName.equals("roleStartYear")){
				if(mapFlag==1){
					NavigableSet<String> navig = ((TreeMap)sortedIntMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						sortedTeacherRole.add((TeacherRole) sortedIntMap.get(key));
					} 
				}else if(mapFlag==0){
					Iterator iterator = sortedIntMap.keySet().iterator();
					while (iterator.hasNext()) {
						Object key = iterator.next();
						sortedTeacherRole.add((TeacherRole) sortedIntMap.get(key));
					}
				}else{
					sortedTeacherRole=listTeacherRole;
				}
			}
			else{
				if(mapFlag==1){
					NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						sortedTeacherRole.add((TeacherRole) sortedMap.get(key));
					} 
				}else if(mapFlag==0){
					Iterator iterator = sortedMap.keySet().iterator();
					while (iterator.hasNext()) {
						Object key = iterator.next();
						sortedTeacherRole.add((TeacherRole) sortedMap.get(key));
					}
				}else{
					sortedTeacherRole=listTeacherRole;
				}
			}

			totalRecord =listTeacherRole.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherRole> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);

			String responseText="";
			sb.append("<table border='0' id='employeementGrid' class='table table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingLink(lblRole,sortOrderFieldName,"role",sortOrderTypeVal,pgNo);
			sb.append("<th width='200px;' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink("<span id='lblCompanyName'>"+lblOrganizationEmp+"</span>",sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
			sb.append("<th width='100px;' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblTpe,sortOrderFieldName,"type",sortOrderTypeVal,pgNo);
			sb.append("<th width='150px;' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblDuration,sortOrderFieldName,"roleStartYear",sortOrderTypeVal,pgNo);
			sb.append("<th width='150px;' valign='top' id='empDurationHeader'>"+responseText+"</th>");

			/*responseText=PaginationAndSorting.responseSortingLink("Annual Salary",sortOrderFieldName,"amount",sortOrderTypeVal,pgNo);
			sb.append("<th width='100px;' valign='top' id='empAnnSal'>"+responseText+"</th>");
*/
			responseText=PaginationAndSorting.responseSortingLink(lblTypOfRole,sortOrderFieldName,"empRoleTypeMaster",sortOrderTypeVal,pgNo);
			sb.append("<th width='200px;' valign='top'>"+responseText+"</th>");

			sb.append("<th width='100px;' class='net-header-text'>");
			sb.append(lblAct);
			sb.append("</th>");	

			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherRole!=null)
				for(TeacherRole tRole:listsortedTeacherRole)
				{
					if(tRole.getNoWorkExp()!=null && tRole.getNoWorkExp()==true)
					{
						sb.append("<tr>");
						sb.append("<td colspan='5'>");
						sb.append("I do not have any work experience.");
						sb.append("</td>");
						
						sb.append("<td>");

						sb.append("<a href='#' onclick=\"return showEditFormEmployment('"+tRole.getRoleId()+"')\" >"+lblEdit+"</a>");
						sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return deleteEmployment('"+tRole.getRoleId()+"')\">"+lnkDlt+"</a>");
						sb.append("</td>");
						
						sb.append("</tr>");
					}
					else
					{
						sb.append("<tr>");
						sb.append("<td>");
						sb.append(tRole.getRole());
						sb.append("</td>");
						
						sb.append("<td>");
						if(tRole.getOrganization()!=null)
						sb.append(tRole.getOrganization());
						sb.append("</td>");

						sb.append("<td>");
						sb.append(tRole.getFieldMaster().getFieldName());
						sb.append("</td>");

						sb.append("<td class='empDurationRec'>");

						if(tRole.getCurrentlyWorking())
						{
							sb.append(tRole.getRoleStartYear()+" to Present");
						}	
						else
						{
							sb.append(tRole.getRoleStartYear()+" to "+tRole.getRoleEndYear());
						}
						sb.append("</td>");
						String currency="";
						String amount="";
						if(tRole.getCurrencyMaster()!=null){
							currency=tRole.getCurrencyMaster().getCurrencyShortName();
						}
						if(tRole.getAmount()!=null){
							amount=tRole.getAmount()+"";
						}
						/*sb.append("<td class='empAnnSalRec'>");
						sb.append(currency+" "+amount);
						sb.append("</td>");*/

						sb.append("<td class='empRoleTypegrd' id='empRoleTypegrd_"+tRole.getRoleId()+"'>");
						if(tRole.getEmpRoleTypeMaster()!=null)
						{
							sb.append(tRole.getEmpRoleTypeMaster().getEmpRoleTypeName());
						}
						sb.append("</td>");	

						sb.append("<td>");

						sb.append("<a href='#' onclick=\"return showEditFormEmployment('"+tRole.getRoleId()+"')\" >"+lblEdit+"</a>");
						sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return deleteEmployment('"+tRole.getRoleId()+"')\">"+lnkDlt+"</a>");
						sb.append("</td>");
						sb.append("</tr>");
					}
					
				}

			if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}
	public String saveOrUpdateEmployment(TeacherRole teacherRole)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			teacherRole.setTeacherDetail(teacherDetail);
			teacherRole.setCreatedDateTime(new Date());
			CurrencyMaster currencyMaster = currencyMasterDAO.findById(1, false, false);
			if(teacherRole.getAmount()!=null){
				if(teacherRole.getAmount()!=0){
					teacherRole.setCurrencyMaster(currencyMaster);
				}else{
					teacherRole.setCurrencyMaster(null);
					teacherRole.setAmount(null);
				}
			}else{
				teacherRole.setCurrencyMaster(null);
				teacherRole.setAmount(null);
			}
			teacherRoleDAO.makePersistent(teacherRole);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}

	public TeacherRole showEditFormEmployment(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		TeacherRole teacherRole = null;
		try 
		{
			Integer roleId = Integer.parseInt(id.trim());
			teacherRole = teacherRoleDAO.findById(roleId, false, false);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return teacherRole;
	}
	public Boolean deleteEmployment(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		Integer roleId = Integer.parseInt(id.trim());
		TeacherRole teacherRole = teacherRoleDAO.findById(roleId, false, false);
		teacherRoleDAO.makeTransient(teacherRole);
		return true;		
	}


	public String getInvolvementGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		StringBuffer sb = new StringBuffer();		
		try 
		{
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------

			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			List<TeacherInvolvement> listTeacherInvolvements = null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"organization";
			String sortOrderNoField		=	"organization";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("orgtype")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("orgtype")){
					sortOrderNoField="orgtype";
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/


			listTeacherInvolvements = teacherInvolvementDAO.findSortedTeacherInvolvementByTeacher(sortOrderStrVal,teacherDetail);


			List<TeacherInvolvement> sortedlistTeacherInvolvement		=	new ArrayList<TeacherInvolvement>();

			SortedMap<String,TeacherInvolvement>	sortedMap = new TreeMap<String,TeacherInvolvement>();
			if(sortOrderNoField.equals("orgtype"))
			{
				sortOrderFieldName	=	"orgtype";
			}
			int mapFlag=2;
			for (TeacherInvolvement trInvolvement : listTeacherInvolvements){
				String orderFieldName=trInvolvement.getOrganization();
				if(sortOrderFieldName.equals("orgtype")){
					orderFieldName=trInvolvement.getOrgTypeMaster().getOrgType()+"||"+trInvolvement.getInvolvementId();
					sortedMap.put(orderFieldName+"||",trInvolvement);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}

			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherInvolvement.add((TeacherInvolvement) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherInvolvement.add((TeacherInvolvement) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherInvolvement=listTeacherInvolvements;
			}

			totalRecord =sortedlistTeacherInvolvement.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherInvolvement> listsortedTeacherCertificates		=	sortedlistTeacherInvolvement.subList(start,end);
			String responseText="";

			sb.append("<table border='0' id='involvementGrid' class='table table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingLink(lblOrga,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblTpe,sortOrderFieldName,"orgtype",sortOrderTypeVal,pgNo);
			sb.append("<th width='15%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblNumberofPeople,sortOrderFieldName,"peopleRangeMaster",sortOrderTypeVal,pgNo);
			sb.append("<th width='25%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblNumPeLed,sortOrderFieldName,"leadNoOfPeople",sortOrderTypeVal,pgNo);
			sb.append("<th width='25%' valign='top'>"+responseText+"</th>");

			sb.append("<th width='15%' class='net-header-text'>");
			sb.append(lblAct);
			sb.append("</th>");	

			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherInvolvements!=null)
				for(TeacherInvolvement tInv:listsortedTeacherCertificates)
				{
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(tInv.getOrganization());
					sb.append("</td>");

					sb.append("<td>");
					if(tInv.getOrgTypeMaster()!=null)
						sb.append(tInv.getOrgTypeMaster().getOrgType());
					sb.append("</td>");

					sb.append("<td>");
					if(tInv.getPeopleRangeMaster()!=null)
					sb.append(tInv.getPeopleRangeMaster().getRange());	
					sb.append("</td>");

					sb.append("<td>");
					sb.append(tInv.getLeadNoOfPeople()==null?"":tInv.getLeadNoOfPeople());	
					sb.append("</td>");	

					sb.append("<td>");				
					sb.append("<a href='#' onclick=\"return showEditFormInvolvement('"+tInv.getInvolvementId()+"')\" >"+lblEdit+"</a>");
					sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return deleteRecordInvolvment('"+tInv.getInvolvementId()+"')\">"+lnkDlt+"</a>");
					sb.append("</td>");
					sb.append("</tr>");
				}

			if(listTeacherInvolvements==null || listTeacherInvolvements.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}
	
	public String getInvolvementGridDistrictSpecific(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer districtId,String jobcategoryName)
	{	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		StringBuffer sb = new StringBuffer();		
		try 
		{
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------

			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			List<TeacherInvolvement> listTeacherInvolvements = null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"organization";
			String sortOrderNoField		=	"organization";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("orgtype")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("orgtype")){
					sortOrderNoField="orgtype";
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/


			listTeacherInvolvements = teacherInvolvementDAO.findSortedTeacherInvolvementByTeacher(sortOrderStrVal,teacherDetail);


			List<TeacherInvolvement> sortedlistTeacherInvolvement		=	new ArrayList<TeacherInvolvement>();

			SortedMap<String,TeacherInvolvement>	sortedMap = new TreeMap<String,TeacherInvolvement>();
			if(sortOrderNoField.equals("orgtype"))
			{
				sortOrderFieldName	=	"orgtype";
			}
			int mapFlag=2;
			for (TeacherInvolvement trInvolvement : listTeacherInvolvements){
				String orderFieldName=trInvolvement.getOrganization();
				if(sortOrderFieldName.equals("orgtype")){
					orderFieldName=trInvolvement.getOrgTypeMaster().getOrgType()+"||"+trInvolvement.getInvolvementId();
					sortedMap.put(orderFieldName+"||",trInvolvement);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}

			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherInvolvement.add((TeacherInvolvement) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherInvolvement.add((TeacherInvolvement) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherInvolvement=listTeacherInvolvements;
			}

			totalRecord =sortedlistTeacherInvolvement.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherInvolvement> listsortedTeacherCertificates		=	sortedlistTeacherInvolvement.subList(start,end);
			String responseText="";

			sb.append("<table border='0' id='involvementGrid' class='table table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingLink("Organization",sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
			sb.append("<th valign='top'>"+responseText+"</th>");

			if(!districtId.equals(3703120)){
				responseText=PaginationAndSorting.responseSortingLink("Type",sortOrderFieldName,"orgtype",sortOrderTypeVal,pgNo);
				sb.append("<th valign='top'>"+responseText+"</th>");
			}

			responseText=PaginationAndSorting.responseSortingLink("Number of People",sortOrderFieldName,"peopleRangeMaster",sortOrderTypeVal,pgNo);
			sb.append("<th valign='top' class='pplRang'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink("Number of People Led",sortOrderFieldName,"leadNoOfPeople",sortOrderTypeVal,pgNo);
			sb.append("<th valign='top' class='yNldPPl'>"+responseText+"</th>");

			sb.append("<th class='net-header-text'>");
			sb.append(""+Utility.getLocaleValuePropByKey("lblAct", locale)+"");
			sb.append("</th>");	

			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherInvolvements!=null)
				for(TeacherInvolvement tInv:listsortedTeacherCertificates)
				{
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(tInv.getOrganization());
					sb.append("</td>");
					if(!districtId.equals(3703120)){
						sb.append("<td id='invOrgTyp'>");
						if(tInv.getOrgTypeMaster()!=null)
							sb.append(tInv.getOrgTypeMaster().getOrgType());
						sb.append("</td>");
					}

					sb.append("<td class='pplRang'>");
					if(tInv.getPeopleRangeMaster()!=null)
					sb.append(tInv.getPeopleRangeMaster().getRange());	
					sb.append("</td>");

					sb.append("<td class='yNldPPl'>");
					sb.append(tInv.getLeadNoOfPeople()==null?"":tInv.getLeadNoOfPeople());	
					sb.append("</td>");	

					sb.append("<td>");				
					sb.append("<a href='#' onclick=\"return showEditFormInvolvement('"+tInv.getInvolvementId()+"')\" >Edit</a>");
					sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return deleteRecordInvolvment('"+tInv.getInvolvementId()+"')\">Delete</a>");
					sb.append("</td>");
					sb.append("</tr>");
				}

			if(listTeacherInvolvements==null || listTeacherInvolvements.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append("No Record Found.");
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}
	public String saveOrUpdateInvolvement(TeacherInvolvement teacherInvolvement)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
System.out.println(" =================");
		try 
		{						
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			if(teacherInvolvement.getPeopleRangeMaster()==null)
				teacherInvolvement.setPeopleRangeMaster(null);

			teacherInvolvement.setTeacherDetail(teacherDetail);			
			teacherInvolvementDAO.makePersistent(teacherInvolvement);

		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
		}
		return null;
	}
	public TeacherInvolvement showEditFormInvolvement(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		TeacherInvolvement teacherInvolvement = null;
		try 
		{
			Integer invId = Integer.parseInt(id.trim());
			teacherInvolvement = teacherInvolvementDAO.findById(invId, false, false);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return teacherInvolvement;
	}
	public Boolean deleteRecordInvolvment(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try 
		{

			Integer invId = Integer.parseInt(id.trim());
			TeacherInvolvement teacherInvolvement = teacherInvolvementDAO.findById(invId, false, false);
			teacherInvolvementDAO.makeTransient(teacherInvolvement);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return true;
	}
	public String getHonorsGrid()
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		StringBuffer sb = new StringBuffer();		
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			List<TeacherHonor> listTeacherHonors = null;
			listTeacherHonors = teacherHonorDAO.findTeacherHonersByTeacher(teacherDetail);

			String responseText="";

			sb.append("<table border='0' id='honorsGrid' class='table table-striped' >");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			//responseText=PaginationAndSorting.responseSortingLink("Organization",sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+lblHonors+"</th>");

			//responseText=PaginationAndSorting.responseSortingLink("Type",sortOrderFieldName,"orgtype",sortOrderTypeVal,pgNo);
			sb.append("<th width='15%' valign='top'>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");

			if(listTeacherHonors!=null)
				for(TeacherHonor tHon:listTeacherHonors)
				{
					sb.append("<tr>");
					sb.append("<td>"); 
						sb.append(tHon.getHonor()+" - "+tHon.getHonorYear());
					sb.append("</td>");
					
					sb.append("<td>");
						sb.append("<a href='#' onclick=\"return showEditHonors('"+tHon.getHonorId()+"')\" >"+lblEdit+"</a>");
						sb.append(" &nbsp;|&nbsp;<a href='#'  onclick=\"return deleteRecordHonors('"+tHon.getHonorId()+"')\" >"+lnkDlt+"</a>");
					sb.append("</td>");
					sb.append("</tr>");
				}

			if(listTeacherHonors==null || listTeacherHonors.size()==0)
			{
				/*sb.append("<div class='row'>");
				sb.append("<div class='span4'>No Record Found.</div>");
				sb.append("</div>");*/
				sb.append("<tr>");
				sb.append("<td>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
				
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}

	public String saveOrUpdateHonors(TeacherHonor teacherHonor )
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}


		try 
		{

			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");


			teacherHonor.setTeacherDetail(teacherDetail);			
			teacherHonorDAO.makePersistent(teacherHonor);

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;

	}
	public TeacherHonor showEditHonors(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		TeacherHonor teacherHonor = null;
		try 
		{
			Integer honorId = Integer.parseInt(id.trim());
			teacherHonor = teacherHonorDAO.findById(honorId, false, false);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return teacherHonor;

	}
	public Boolean deleteRecordHonors(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try 
		{

			Integer invId = Integer.parseInt(id.trim());
			TeacherHonor teacherHonor = teacherHonorDAO.findById(invId, false, false);

			teacherHonorDAO.makeTransient(teacherHonor);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return true;

	}

	public int[] insertOrUpdateTeacherTFA(Integer tfaAffiliateId,Integer corpsYear,Integer tfaRegionId,Integer willingAsSubstituteTeacher,
											String phoneNumber,String address1,String address2,String zipCode,String stateId,String cityId,
											String expCertTeacherTraining,String nationalBoardCert,boolean affidavit,Integer salutation_pi,
											String firstName_pi,String middleName_pi,String lastName_pi,String ssn_pi,String dob,
											String employeeType, String formerEmployeeNo, String currentEmployeeNo,String isCurrentFullTimeTeacher,
											String raceValue,String rtDate,String wdDate,String rwdDate,Integer veteranValue,Integer districtIdForDSPQ,Integer ethnicOriginValue,
											Integer ethinicityValue,String countryId,Integer genderValue,String candidateType,String portfolioStatus,String jobId,
											boolean isNonTeacher,String retireNo,String distForRetire,String stMForretire,Integer expectedSalary,String anotherName,
											String noLongerEmployed,String drivingLicState,String drivingLicNum,String praddressLine1,String prpraddressLine2,
											String przipCode,String prstateIdForDSPQ,String prcityIdForDSPQ,String prcountryId,String pfAll,String schoolIdAll,String schoolpreferenceAll,

                                            String location,String position,String customquestion,
                                            String sin_pi,String mobileNumber,String phonTypeUSNonUs)

										
	{
		System.out.println("Calling insertOrUpdateTeacherTFA Ajax Method  "+ssn_pi);
		System.out.println("Calling insertOrUpdateTeacherTFA Ajax Method  "+ssn_pi);
		System.out.println("Calling insertOrUpdateTeacherTFA Ajax Method::"+sin_pi);
		//System.out.println("salutation_pi "+salutation_pi+" firstName_pi "+firstName_pi+" middleName_pi "+middleName_pi+" lastName_pi "+lastName_pi+" ssn_pi "+ssn_pi+" dob "+dob);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		int retArray[] = new int[5];
		retArray[0]=1;
		retArray[1]=1;
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		TFAAffiliateMaster tfaAffiliateMaster	=	null;
		TFARegionMaster tfaRegionMaster			=	null;
		JobOrder jobOrder=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;

		try{
			
			/************************SWADESH**************/
			if(districtIdForDSPQ!=null && districtIdForDSPQ==7800292){
				String pfAll1[]=pfAll.split("~");
				String schoolIdAll1[]=schoolIdAll.split("~");
				String schoolpreferenceAll1[]=schoolpreferenceAll.split("~");
				
			SchoolPreferencebyCandidate schoolPreferencebyCandidate2=null;
			if (schoolPreferencebyCandidate2==null) {
				SchoolPreferencebyCandidate schoolPreferencebyCandidate = null;
				List<SchoolPreferencebyCandidate> schoolPreferencebyCandidate3 = null;
				districtMaster=districtMasterDAO.findById(districtIdForDSPQ, false, false);
				jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>"+pfAll1.length+"     "+schoolIdAll1.length+"    "+schoolpreferenceAll1.length);
				
				for(int i=0;i<pfAll1.length;i++)
				{
					if(!pfAll1[i].equalsIgnoreCase("0")){
					schoolPreferencebyCandidate=new SchoolPreferencebyCandidate();
					String str=schoolIdAll1[i].toString();
					schoolMaster=schoolMasterDAO.findById(Long.parseLong(str), false, false);
					System.out.println(schoolpreferenceAll1[i]);
					if (!schoolpreferenceAll1[i].equals("null")){
					schoolPreferencebyCandidate3=schoolPreferenceByCandidateDAO.getschoolpreferenceId(Integer.parseInt(schoolpreferenceAll1[i]));
					schoolPreferencebyCandidate.setSchoolpreferenceId(Integer.parseInt(schoolpreferenceAll1[i]));
					}
					schoolPreferencebyCandidate.setPreference(pfAll1[i]);
					schoolPreferencebyCandidate.setJobOrder(jobOrder);
					schoolPreferencebyCandidate.setTeacherDetail(teacherDetail);
					schoolPreferencebyCandidate.setDistrictMaster(districtMaster);
					schoolPreferencebyCandidate.setSchoolMaster(schoolMaster);
					if(schoolpreferenceAll1[i].equals("null"))
					schoolPreferencebyCandidate.setCreatedDate(new Date());
					else
					schoolPreferencebyCandidate.setCreatedDate(schoolPreferencebyCandidate3.get(0).getCreatedDate());
					schoolPreferencebyCandidate.setUpdateDate(new Date());
					schoolPreferencebyCandidate.setStatus("A");
					schoolPreferenceByCandidateDAO.makePersistent(schoolPreferencebyCandidate);
					}
				}
			}
			}
			
			/******************SWADESH********************************/
			
			TeacherExperience teacherExperience=null;
			teacherExperience= teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			//teacherPortfolioStatus =teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail); // Get Portfolio Status By teacher ID

			if(teacherExperience==null)
			{
				teacherExperience=new TeacherExperience();
				teacherExperience.setCanServeAsSubTeacher(2);
				teacherExperience.setCreatedDateTime(new Date());
				teacherExperience.setPotentialQuality(0);
				teacherExperience.setKnowOtherLanguages(false);
			}

			teacherExperience.setTeacherId(teacherDetail);

			if(tfaAffiliateId > 0)
				tfaAffiliateMaster	=	tfaAffiliateMasterDAO.findById(tfaAffiliateId, false, false);

			if(tfaAffiliateId==1 || tfaAffiliateId==2)
			{
				if(tfaRegionId > 0)
				{
					tfaRegionMaster	=tfaRegionMasterDAO.findById(tfaRegionId, false, false);
				}

				if(corpsYear >0 && tfaRegionMaster!=null && tfaAffiliateMaster!=null)
				{
					teacherExperience.setTfaAffiliateMaster(tfaAffiliateMaster);
					teacherExperience.setCorpsYear(corpsYear);
					teacherExperience.setTfaRegionMaster(tfaRegionMaster);
				}
			}
			else if(tfaAffiliateId==3 && tfaAffiliateMaster!=null)
			{
				teacherExperience.setCorpsYear(null);
				teacherExperience.setTfaRegionMaster(null);
				teacherExperience.setTfaAffiliateMaster(tfaAffiliateMaster);
			}
			else
			{
				teacherExperience.setCorpsYear(null);
				teacherExperience.setTfaRegionMaster(null);
				teacherExperience.setTfaAffiliateMaster(null);
			}

			if(willingAsSubstituteTeacher!=null)
				teacherExperience.setCanServeAsSubTeacher(willingAsSubstituteTeacher);

			if(expCertTeacherTraining!=null && !expCertTeacherTraining.equals(""))
				teacherExperience.setExpCertTeacherTraining(Double.parseDouble(expCertTeacherTraining));

			if(nationalBoardCert!=null && !nationalBoardCert.equals(""))
			{
				teacherExperience.setNationalBoardCertYear(Integer.parseInt(nationalBoardCert));
				System.out.println(">>>>>>>>>>>>>>>>>>>?????????????????????????????????    "+customquestion);
				teacherExperience.setCustomquestion(customquestion);
				teacherExperience.setNationalBoardCert(true);
			}
			else
			{
				teacherExperience.setNationalBoardCertYear(null);
				teacherExperience.setNationalBoardCert(false);
			}
			try{
				teacherExperience.setIsNonTeacher(isNonTeacher);
			}catch(Exception e)
			{
				e.printStackTrace();
			}


			teacherExperienceDAO.makePersistent(teacherExperience);
			//teacherPortfolioStatus.setIsExperiencesCompleted(true);

			TeacherPersonalInfo  teacherPersonalInfo =  new TeacherPersonalInfo();
			StateMaster stateMaster = new StateMaster();
			CityMaster cityMaster = new CityMaster();
			CountryMaster countryMaster = new CountryMaster();

			String ssnExisting = "";

			if(teacherDetail!=null)
			{

				teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
				if(teacherPersonalInfo==null)
				{
					teacherPersonalInfo = new TeacherPersonalInfo();

					if(firstName_pi!=null && !firstName_pi.equalsIgnoreCase("")){
						teacherPersonalInfo.setFirstName(firstName_pi);
						teacherDetail.setFirstName(firstName_pi);
					}
					else
					{	teacherDetail.setFirstName(firstName_pi);
					if(teacherDetail.getFirstName()!=null)
						teacherPersonalInfo.setFirstName(teacherDetail.getFirstName());
					else
						teacherPersonalInfo.setFirstName("");
					}

					if(middleName_pi!=null && !middleName_pi.equalsIgnoreCase("")){
						teacherPersonalInfo.setMiddleName(middleName_pi);
					}else{
						teacherPersonalInfo.setMiddleName("");
					}

					if(lastName_pi!=null && !lastName_pi.equalsIgnoreCase("")){
						teacherPersonalInfo.setLastName(lastName_pi);
						teacherDetail.setLastName(lastName_pi);
					}
					else
					{teacherDetail.setLastName(lastName_pi);
					if(teacherDetail.getLastName()!=null)
					{
						teacherPersonalInfo.setLastName(teacherDetail.getLastName());
					}
					else
					{
						teacherPersonalInfo.setLastName("");
					}
					}

				}
				else
				{
					System.out.println(" Have Personal Information.................");

					if(firstName_pi!=null && !firstName_pi.equalsIgnoreCase("")){
						teacherPersonalInfo.setFirstName(firstName_pi);
						teacherDetail.setFirstName(firstName_pi);
					}
					if(middleName_pi!=null && !middleName_pi.equalsIgnoreCase("")){
						teacherPersonalInfo.setMiddleName(middleName_pi);
					}else{
						teacherPersonalInfo.setMiddleName("");
					}

					if(lastName_pi!=null && !lastName_pi.equalsIgnoreCase("")){
						teacherPersonalInfo.setLastName(lastName_pi);
						teacherDetail.setLastName(lastName_pi);
					}

					ssnExisting = Utility.decodeBase64(teacherPersonalInfo.getSSN());

					System.out.println(" SSSSSSSSSSSSSSSSSSSNNNNNNNNNNNNNNN ::::::: "+ssnExisting);
					
				}
				
				try {
					if(drivingLicNum!=null && !drivingLicNum.equalsIgnoreCase(""))
						teacherPersonalInfo.setDrivingLicNum(Utility.encodeInBase64(drivingLicNum));
					teacherPersonalInfo.setDrivingLicState(drivingLicState);
				} catch (Exception e) {}
				
				
				teacherPersonalInfo.setAnotherName(anotherName);
				
				Date dob_converted=null;
				Date rtDate_converted=null;
				Date wdDate_converted=null;
				Date rwdDate_converted=null;
				try {
					
				
				//	if(dob!=null && !dob.equalsIgnoreCase(""))
					// 	
                    String[] dob1=dob.split("-");
                    if(dob1[0].equalsIgnoreCase("0") && dob1[1].equalsIgnoreCase("0")){
                    	teacherPersonalInfo.setDob(null);
                    }else{
					   if(dob!=null && !dob.equalsIgnoreCase(""))
				        dob_converted=Utility.getCurrentDateFormart(dob);
                    }
					if(rtDate!=null && !rtDate.equalsIgnoreCase(""))
						rtDate_converted=Utility.getCurrentDateFormart(rtDate);

					if(wdDate!=null && !wdDate.equalsIgnoreCase(""))
						wdDate_converted=Utility.getCurrentDateFormart(wdDate);
					
					if(rwdDate!=null && !rwdDate.equalsIgnoreCase(""))
						rwdDate_converted=Utility.getCurrentDateFormart(rwdDate);
              
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				teacherPersonalInfo.setSalutation(salutation_pi);
				teacherPersonalInfo.setExpectedSalary(expectedSalary);
				
				if(phoneNumber!=null && !phoneNumber.equalsIgnoreCase(""))
					teacherPersonalInfo.setPhoneNumber(phoneNumber);
				if(mobileNumber!=null && !mobileNumber.equalsIgnoreCase(""))
					teacherPersonalInfo.setMobileNumber(mobileNumber);
				if(phonTypeUSNonUs!=null && !phonTypeUSNonUs.equalsIgnoreCase(""))
					teacherPersonalInfo.setPhoneTypeUSNonUS(Integer.parseInt(phonTypeUSNonUs));
				
				/*********************swadesh**************/
					if (!location.equalsIgnoreCase("")  && !location.equalsIgnoreCase(null)) {
					teacherPersonalInfo.setLocation(location);
				}
				
				if (!location.equalsIgnoreCase("")  &&!position.equalsIgnoreCase(null)) {
					teacherPersonalInfo.setPosition(position);
				}

				//	String ssnExisting = teacherPersonalInfo.getSSN();

				ssn_pi=ssn_pi==null?"":ssn_pi;
				boolean validSSN=false;
				try {
					Integer.parseInt(ssn_pi);
					validSSN=true;
				} catch (Exception e) {
					// TODO: handle exception
				}
				
		        sin_pi=sin_pi==null?"":sin_pi;
				boolean validSIN=false;
				try {
					Integer.parseInt(sin_pi);
					validSIN=true;
				} catch (Exception e) {
					// TODO: handle exception
				}
				

				//Check ssn for Franklin-Essex-Hamilton District
				try
				{
					if(districtIdForDSPQ!=null && districtIdForDSPQ.equals(3680340))
					{
						if(ssn_pi!=null && !ssn_pi.equalsIgnoreCase("") && validSSN && ssn_pi.trim().length()==4)
						{
							if(ssnExisting!=null && !ssnExisting.equals(""))
							{
								System.out.println(" ssnExisting.substring(5).equals(ssn_pi)   "+ssnExisting.substring(5).equals(ssn_pi));
								String New_SSN = ssnExisting.substring(0, 5)+ssn_pi;
								System.out.println(" ============= Final Value of SSN :::::: "+New_SSN);
								if(!ssnExisting.substring(5).equals(ssn_pi))
									teacherPersonalInfo.setSSN(Utility.encodeInBase64(New_SSN));
							}else
							{
								System.out.println(" ============ When SSN is NOT Exist==============="+ssn_pi);
								teacherPersonalInfo.setSSN(Utility.encodeInBase64(ssn_pi));
							}
						}	
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}



        /// insert the sin for all canada_en in teacherpersanalinfo table khan
			    try{
					if(sin_pi!=null && !sin_pi.equalsIgnoreCase("") && validSIN && sin_pi.trim().length()==9)
					{
					    teacherPersonalInfo.setSIN(Utility.encodeInBase64(sin_pi));
					}	
				}catch(Exception e){
					e.printStackTrace();
				}
				
		//Check Duplicate Employee

        /// insert the sin for all canada_en in teacherpersanalinfo table
			    try{
					if(sin_pi!=null && !sin_pi.equalsIgnoreCase("") && validSIN && sin_pi.trim().length()==9)
					{
					    teacherPersonalInfo.setSIN(Utility.encodeInBase64(sin_pi));
					}	
				}catch(Exception e){
					e.printStackTrace();
				}
				
		//Check Duplicate Employee

				try
				{
				retArray[3]=0;
				//retArray[4]=1;
				if(districtIdForDSPQ!=null && (districtIdForDSPQ.equals(1200390) || districtIdForDSPQ.equals(4218990)))// Miami Check
				{
					System.out.println("102");
					if(ssn_pi!=null && !ssn_pi.equalsIgnoreCase("") && validSSN && ssn_pi.trim().length()==9)
					{
						System.out.println("103");
						List<TeacherPersonalInfo> chkDuplicateTeacher = new ArrayList<TeacherPersonalInfo>();
						List<TeacherDetail> activeTeacherList = new ArrayList<TeacherDetail>();
						List<Integer> tacherIds = new ArrayList<Integer>();

						chkDuplicateTeacher = teacherPersonalInfoDAO.checkTeachersBySSN(Utility.encodeInBase64(ssn_pi));

						try
						{
							System.out.println("104");
							if(chkDuplicateTeacher.size()>0)
							{
								System.out.println("105 chkDuplicateTeacher :: "+chkDuplicateTeacher.size());
								for(TeacherPersonalInfo tpi:chkDuplicateTeacher)
								{
									System.out.println(" dup teacher :: "+tpi.getTeacherId());
									tacherIds.add(tpi.getTeacherId());
								}
								activeTeacherList = teacherDetailDAO.getActiveTeacherList(tacherIds);

								System.out.println("106 .. 0 .. :: activeTeacherList "+activeTeacherList.size());

								if(activeTeacherList.size()>0)
								{
									System.out.println("106 :: activeTeacherList "+activeTeacherList.size());
									if(activeTeacherList.size()==1)
									{
										System.out.println("107");
										if(!activeTeacherList.get(0).getTeacherId().equals(teacherDetail.getTeacherId()) && Utility.decodeBase64(chkDuplicateTeacher.get(0).getSSN()).equals(ssn_pi))
										{
											retArray[3]=1;
										}
									}
									else
									{
										System.out.println("108");
										retArray[3]=1;
									}
								}
							}
						}catch(Exception e)
						{
							e.printStackTrace();
						}
						System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> chkDuplicateTeacher.size() >>>>>>>> "+chkDuplicateTeacher.size());
					}
				}
				}catch(Exception e)
				{
					e.printStackTrace();
				}

				
				
             try
                  {
	
				if(districtIdForDSPQ!=null && districtIdForDSPQ==804800 && ssn_pi!=null && !ssn_pi.equalsIgnoreCase("") && validSSN && ssn_pi.trim().length()>3)
					teacherPersonalInfo.setSSN(Utility.encodeInBase64(ssn_pi));  
                 }catch(Exception e){e.printStackTrace();}
                 
                 
                 
                 JobOrder jobOrderForNc= null;					
					if(jobId!=null && !jobId.equalsIgnoreCase("")){						
						int iJobId=0;
						try { iJobId=Utility.getIntValue(jobId); } catch (Exception e) {}
						try { jobOrderForNc=jobOrderDAO.findById(iJobId, false, false); } catch (Exception e) {}
					  }
					if(jobOrderForNc!=null && jobOrderForNc.getDistrictMaster()!=null && jobOrderForNc.getDistrictMaster().getHeadQuarterMaster()!=null && jobOrderForNc.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2)
					{
					//teacherPersonalInfo.setSSN(null);
					}
					else if(ssn_pi!=null && !ssn_pi.equalsIgnoreCase("") && validSSN && ssn_pi.trim().length()==9)
				{
					teacherPersonalInfo.setSSN(Utility.encodeInBase64(ssn_pi));
				}
				
				

				Integer empTypeExisting = teacherPersonalInfo.getEmployeeType();
				Integer fullTimeExisting = teacherPersonalInfo.getIsCurrentFullTimeTeacher();
				String empNoExisting = teacherPersonalInfo.getEmployeeNumber(); 

				if(employeeType!=null && !employeeType.equalsIgnoreCase("") && employeeType.equalsIgnoreCase("1"))
				{
					teacherPersonalInfo.setEmployeeNumber(currentEmployeeNo.trim());

					if(isCurrentFullTimeTeacher!=null && !isCurrentFullTimeTeacher.equalsIgnoreCase(""))
						teacherPersonalInfo.setIsCurrentFullTimeTeacher(Integer.parseInt(isCurrentFullTimeTeacher));

					teacherPersonalInfo.setRetirementdate(null);
					teacherPersonalInfo.setMoneywithdrawaldate(null);
				}
				else if(employeeType!=null && !employeeType.equalsIgnoreCase("") && employeeType.equalsIgnoreCase("0"))
				{
					teacherPersonalInfo.setEmployeeNumber(formerEmployeeNo);
					teacherPersonalInfo.setIsCurrentFullTimeTeacher(null);
					if(districtIdForDSPQ!=null && districtIdForDSPQ.equals(1302010))
							teacherPersonalInfo.setNoLongerEmployed(noLongerEmployed);

					if(rtDate_converted!=null)
						teacherPersonalInfo.setRetirementdate(rtDate_converted);

					if(wdDate_converted!=null)
						teacherPersonalInfo.setMoneywithdrawaldate(wdDate_converted);
					
					if(rwdDate_converted!=null)
						teacherPersonalInfo.setRetirementdatePERA(rwdDate_converted);

				}
				if(employeeType!=null && !employeeType.equalsIgnoreCase("") && employeeType.equalsIgnoreCase("2"))
				{
					teacherPersonalInfo.setEmployeeNumber(null);
					teacherPersonalInfo.setIsCurrentFullTimeTeacher(null);
					teacherPersonalInfo.setRetirementdate(null);
					teacherPersonalInfo.setMoneywithdrawaldate(null);
				}
				System.out.println("===================================== ");
				boolean onlySSNcheck = false;
				if(districtIdForDSPQ!=null && districtIdForDSPQ.equals(4218990))// Philadaphia Check
					onlySSNcheck=true;

				if(districtIdForDSPQ!=null && districtIdForDSPQ.equals(1200390) || onlySSNcheck)// Miami Check
				{
					System.out.println("----------------------------- ");
					String ssn = teacherPersonalInfo.getSSN()==null?"":teacherPersonalInfo.getSSN();
					String empNo = teacherPersonalInfo.getEmployeeNumber()==null?"":teacherPersonalInfo.getEmployeeNumber();
					System.out.println("empNoempNoempNoempNoempNoempNo: "+empNo);
					//if(ssn.equals("") || empNo.equals(""))
					if(ssn.equals(""))
					{
						//return true;
						retArray[0]=1;
						retArray[1]=1;
						//return retArray;
					}else{

						if(!ssn.equals(""))
						{
							ssn = ssn.trim();
							ssn = Utility.decodeBase64(ssn);
							//ssn = ssn_pi;

							if(ssn.length()>4 && districtIdForDSPQ.equals(1200390))
								ssn = ssn.substring(ssn.length() - 4, ssn.length());
						}
						List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
						System.out.println("pppppppp "+teacherPersonalInfo.getEmployeeNumber());
						DistrictMaster dm = new DistrictMaster();
						dm.setDistrictId(districtIdForDSPQ);
						if(onlySSNcheck)//Philedaphia
						{
							//if(ssn.length()==9)
							employeeMasters =	employeeMasterDAO.checkEmployeeByFullSSN(ssn,dm);
							
						}else
						{
							//Miami Only
							if(employeeType.equalsIgnoreCase("2"))// Not an employee
							{
								try {
									System.out.println("SSN: "+ssn);
									employeeMasters =	employeeMasterDAO.checkEmployeeBySSN(ssn,firstName_pi,lastName_pi,dm);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							else 
							{	// Miami requested to remove empNo check
								//employeeMasters =	employeeMasterDAO.checkEmployee(ssn, empNo,firstName_pi,lastName_pi,dm);
								try {
									System.out.println("SSN: "+ssn);
									employeeMasters =	employeeMasterDAO.checkEmployeeBySSN(ssn,firstName_pi,lastName_pi,dm);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
						System.out.println("employeeType:::: "+employeeType);
						System.out.println("llllllllllllllll employeeMasters:::::::::::::: "+employeeMasters.size());
						System.out.println("====vishwanath ========== onlySSNcheck= "+onlySSNcheck);
						if(employeeMasters.size()>0 && onlySSNcheck)
						{
							System.out.println("====vishwanath ==========");
							EmployeeMaster employeeMaster = employeeMasters.get(0);
							String PCCode=employeeMaster.getPCCode()==null?"":employeeMaster.getPCCode().trim();
							if(PCCode.equalsIgnoreCase("y"))
							{
									retArray[0]=0;
									retArray[1]=3;// restricted
							}

						}else if(districtIdForDSPQ!=null && districtIdForDSPQ.equals(1200390)) //Miami
						{
							JobOrder jobOrder1= null;
							String jobCategoryName = "";
							if(jobId!=null && !jobId.equalsIgnoreCase("")){
								
								int iJobId=0;
								try { iJobId=Utility.getIntValue(jobId); } catch (Exception e) {}
								try { jobOrder1=jobOrderDAO.findById(iJobId, false, false); } catch (Exception e) {}
								if(jobOrder1!=null)
								jobCategoryName = jobOrder1.getJobCategoryMaster().getJobCategoryName();
							}
							
							if(employeeMasters.size()>0)
							{
								EmployeeMaster employeeMaster = employeeMasters.get(0);

								Date grugTestFailDate = employeeMaster.getDrugTestFailDate();
								System.out.println(employeeMaster.getEmployeeId()+" grugTestFailDate:: "+grugTestFailDate);
								boolean isJobNotAllowed = false;
								if(grugTestFailDate!=null)
								{
									districtMaster = districtMasterDAO.findById(districtIdForDSPQ, false, false);
									if(districtMaster.getNoOfDaysRestricted()!=null && districtMaster.getNoOfDaysRestricted()!=0)
									{
										Calendar c = Calendar.getInstance();
										c.setTime(grugTestFailDate);
										//c.add(Calendar.YEAR,3);
										//c.add(Calendar.DATE, 1095);
										c.add(Calendar.DATE, districtMaster.getNoOfDaysRestricted());
										Date restrictedDate = c.getTime();
										Date currTime = Utility.getDateWithoutTime();
										isJobNotAllowed = Utility.between(currTime,grugTestFailDate, restrictedDate);
										System.out.println("allowed: "+!isJobNotAllowed);
									}
								}

								String FECode=employeeMaster.getFECode()==null?"":employeeMaster.getFECode().trim();
								if(FECode.equalsIgnoreCase("3"))
									FECode="1";

								if(isJobNotAllowed) //drug failed
								{
									retArray[0]=0;
									retArray[1]=2;// restricted
								}
								else if(employeeType.equalsIgnoreCase("1"))// current employee
								{
									System.out.println("getIsCurrentFullTimeTeacherL:: "+teacherPersonalInfo.getIsCurrentFullTimeTeacher());
									//String isFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?"N":(teacherPersonalInfo.getIsCurrentFullTimeTeacher()==1?"I":"N");
									String isFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?"N":((teacherPersonalInfo.getIsCurrentFullTimeTeacher()==1 || teacherPersonalInfo.getIsCurrentFullTimeTeacher()==2)?"I":"N");
									String staffType=employeeMaster.getStaffType()==null?"":employeeMaster.getStaffType().trim();
									System.out.println("{{{{{{{{{{{{ "+isFullTime+" "+staffType);
									//Integer isCurrentFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?0:teacherPersonalInfo.getIsCurrentFullTimeTeacher();
									//if(employeeType.toString().equals(FECode) && isFullTime.equalsIgnoreCase("I") && staffType.equalsIgnoreCase("I"))
									if(employeeType.toString().equals(FECode) && isFullTime.equalsIgnoreCase(staffType))
									{
										System.out.println("------------------------equal-----------1");
									}else
									{
										retArray[0]=0;
										retArray[1]=1;
									}

								}else if(employeeType.equalsIgnoreCase("0") || employeeType.equalsIgnoreCase("2"))
								{
									if(employeeType.toString().equals(FECode))
									{
										System.out.println("---FE--------------------equal-----------2");
									}else
									{
										//return false;
										retArray[0]=0;
										retArray[1]=1;
										//return retArray;
									}
								}
							}else{
								if(employeeType.equalsIgnoreCase("1"))
								{
									String isFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?"N":((teacherPersonalInfo.getIsCurrentFullTimeTeacher()==1 || teacherPersonalInfo.getIsCurrentFullTimeTeacher()==2)?"I":"N");
									Integer isCurrentFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?0:teacherPersonalInfo.getIsCurrentFullTimeTeacher();
									if(isFullTime.equalsIgnoreCase("I") && isCurrentFullTime==1)
									{
										if(!jobCategoryName.equals("Aspiring Assistant Principal")) // new change
										{
											retArray[0]=0;
											retArray[1]=0;
										}
									}
								}
							}
						}
					}
				}
				else if(districtIdForDSPQ!=null && !districtIdForDSPQ.equals(1200390))
				{
					
				}

				
				if(employeeType!=null && !employeeType.equalsIgnoreCase(""))
				{
					teacherPersonalInfo.setEmployeeType(Integer.parseInt(employeeType));
				}

				if(dob_converted!=null)
					teacherPersonalInfo.setDob(dob_converted);

				try
				{
					//----------Start:: Country, State, City---------
					if(countryId!=null && !countryId.equals(""))
					{
						CountryMaster countryMaster2 = countryMasterDAO.findById(Integer.parseInt(countryId), false, false);
						List<StateMaster> lstState = stateMasterDAO.findActiveStateByCountryId(countryMaster2);
						StateMaster stateMaster2 = new StateMaster();
						CityMaster cityMaster2 = new CityMaster();

						if(stateId!=null && !stateId.equals("") && (cityId!=null && !cityId.equals("")))
						{
							if(Integer.parseInt(countryId)==223)
							{
								stateMaster2 = stateMasterDAO.findById(Long.parseLong(stateId), false, false);
								cityMaster2 = cityMasterDAO.findById(Long.parseLong(cityId), false, false);

								teacherPersonalInfo.setStateId(stateMaster2);
								teacherPersonalInfo.setCityId(cityMaster2);

								teacherPersonalInfo.setOtherState(null);
								teacherPersonalInfo.setOtherCity(null);
							}
							else
							{
								if(lstState!=null && lstState.size()>0)
								{
									stateMaster2 = stateMasterDAO.findById(Long.parseLong(stateId), false, false);

									teacherPersonalInfo.setStateId(stateMaster2);
									teacherPersonalInfo.setOtherCity(cityId);

									teacherPersonalInfo.setOtherState(null);
									teacherPersonalInfo.setCityId(null);
								}
								else
								{
									teacherPersonalInfo.setOtherState(stateId);
									teacherPersonalInfo.setOtherCity(cityId);

									teacherPersonalInfo.setStateId(null);
									teacherPersonalInfo.setCityId(null);
								}
							}	
						}

						if((stateId!=null && !stateId.equals("")) && (cityId!=null && !cityId.equals("")))
						{
							countryMaster.setCountryId(Integer.parseInt(countryId));							
							teacherPersonalInfo.setCountryId(countryMaster);
						}

					}
					//---------- End---------

				}
				catch(Exception e)
				{
					e.printStackTrace();
				}

				try
				{
					//----------Start:: Country, State, City---------
					if(prcountryId!=null && !prcountryId.equals(""))
					{
						CountryMaster countryMaster3 = countryMasterDAO.findById(Integer.parseInt(prcountryId), false, false);
						List<StateMaster> lstState1 = stateMasterDAO.findActiveStateByCountryId(countryMaster3);
						StateMaster stateMaster3 = new StateMaster();
						CityMaster cityMaster3 = new CityMaster();
						teacherPersonalInfo.setPresentCountryId(countryMaster3);
						if(prstateIdForDSPQ!=null && !prstateIdForDSPQ.equals("") && (prcityIdForDSPQ!=null && !prcityIdForDSPQ.equals("")))
						{
							if(Integer.parseInt(prcountryId)==223)
							{
								stateMaster3 = stateMasterDAO.findById(Long.parseLong(prstateIdForDSPQ), false, false);
								cityMaster3 = cityMasterDAO.findById(Long.parseLong(prcityIdForDSPQ), false, false);
								
								teacherPersonalInfo.setPresentStateId(stateMaster3);
								teacherPersonalInfo.setPresentCityId(cityMaster3);

								teacherPersonalInfo.setPersentOtherState(null);
								teacherPersonalInfo.setPersentOtherCity(null);
							}
							else
							{
								if(lstState1!=null && lstState1.size()>0)
								{
									stateMaster3 = stateMasterDAO.findById(Long.parseLong(prstateIdForDSPQ), false, false);

									teacherPersonalInfo.setPresentStateId(stateMaster3);
									teacherPersonalInfo.setPersentOtherCity(prcityIdForDSPQ);

									teacherPersonalInfo.setPersentOtherState(null);
									teacherPersonalInfo.setPresentCityId(null);
								}
								else
								{
									teacherPersonalInfo.setPersentOtherState(prstateIdForDSPQ);
									teacherPersonalInfo.setPersentOtherCity(prcityIdForDSPQ);

									teacherPersonalInfo.setPresentStateId(null);
									teacherPersonalInfo.setPresentCityId(null);
								}
							}	
						}
					}
					//---------- End---------

				}
				catch(Exception e)
				{
					e.printStackTrace();
				}

				if(address1!=null && !address1.equals(""))
					teacherPersonalInfo.setAddressLine1(address1);

				if(address2!=null && !address2.equals(""))
					teacherPersonalInfo.setAddressLine2(address2);

				if(zipCode!=null && !zipCode.equals(""))
					teacherPersonalInfo.setZipCode(zipCode);

				
				//////present address ///////
				if(praddressLine1!=null && !praddressLine1.equals(""))
					teacherPersonalInfo.setPresentAddressLine1(praddressLine1);

				if(prpraddressLine2!=null && !prpraddressLine2.equals(""))
					teacherPersonalInfo.setPresentAddressLine2(prpraddressLine2);

				if(przipCode!=null && !przipCode.equals(""))
					teacherPersonalInfo.setPresentZipCode(przipCode);

				////end present address /////
				if(veteranValue!=null && !veteranValue.equals(""))
					teacherPersonalInfo.setIsVateran(veteranValue);

				if(teacherPersonalInfo.getTeacherId()==null)
				{
					teacherPersonalInfo.setTeacherId(teacherDetail.getTeacherId());
					teacherPersonalInfo.setCreatedDateTime(new Date());
					teacherPersonalInfo.setIsDone(false);
				}


				if(raceValue!=null)
					if(!raceValue.equals(""))
					{
						//RaceMaster raceMaster=raceMasterDAO.findById(raceValue, false, false);
						teacherPersonalInfo.setRaceId(raceValue);
					}

				if(genderValue!=null)
					if(genderValue!=-1)
					{
						GenderMaster genderMaster = genderMasterDAO.findById(genderValue, false, false);
						teacherPersonalInfo.setGenderId(genderMaster);
					}

				if(ethnicOriginValue!=null)
					if(ethnicOriginValue!=-1)
					{
						EthnicOriginMaster ethnicOriginMaster = ethnicOriginMasterDAO.findById(ethnicOriginValue, false, false);
						teacherPersonalInfo.setEthnicOriginId(ethnicOriginMaster);
					}

				if(ethinicityValue!=null)
					if(ethinicityValue!=-1)
					{
						EthinicityMaster ethinicityMaster = ethinicityMasterDAO.findById(ethinicityValue, false, false);
						teacherPersonalInfo.setEthnicityId(ethinicityMaster);
					}

				try
				{
					System.out.println(" ******** retire no :: "+retireNo);
					if(retireNo!=null && !retireNo.equals(""))
					{
						teacherPersonalInfo.setRetirementnumber(retireNo);
					}
					if(distForRetire!=null && !distForRetire.equals(""))
					{
						DistrictMaster distRetired = districtMasterDAO.findById(Integer.parseInt(distForRetire), false, false);
						teacherPersonalInfo.setDistrictmaster(distRetired);
					}
					if(stMForretire!=null && !stMForretire.equals(""))
					{
						StateMaster stReited = stateMasterDAO.findById(Long.parseLong(stMForretire), false, false);
						teacherPersonalInfo.setStateMaster(stReited);
					}


				}catch(Exception e)
				{
					e.printStackTrace();
				}
				jobOrder=null;
				System.out.println("teacherPersonalInfO:::::Savepp");				
				teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
				teacherDetailDAO.makePersistent(teacherDetail);
				if(teacherPersonalInfo!=null){
					if(teacherPersonalInfo.getEmployeeType()!=null){
						if(jobId!=null && !jobId.equalsIgnoreCase("")){
							
							int iJobId=0;
							try { iJobId=Utility.getIntValue(jobId); } catch (Exception e) {}
							try { jobOrder=jobOrderDAO.findById(iJobId, false, false); } catch (Exception e) {}

							if(jobOrder!=null && teacherDetail!=null){
								JobForTeacher forTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
								int iIsAffilated=0;
								if(forTeacher!=null &&  forTeacher.getIsAffilated()!=null){
									iIsAffilated=forTeacher.getIsAffilated();
								}
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType()==1)
									iIsAffilated=1;
								
								if(teacherPersonalInfo!=null)
									iIsAffilated=teacherPersonalInfo.getEmployeeType();
														
								if(forTeacher!=null){
									System.out.println(iIsAffilated+"::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::>>>>>>>23");
									System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@14");
									forTeacher.setIsAffilated(iIsAffilated);
									try { jobForTeacherDAO.updatePersistent(forTeacher); } catch (Exception e) { e.printStackTrace();}
								}
							}
						}
					}
				}

				//------------------Testing For Flag-------------------------------

				System.out.println(" >>>>>>>>>>>>> affidavit >>>>>>>>>>>>>>  "+affidavit);
				TeacherPortfolioStatus tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				try{


					if(affidavit==true)
					{
						TeacherAffidavit teacherAffidavit = teacherAffidavitDAO.findAffidavitByTeacher(teacherDetail);

						if(teacherAffidavit==null)
						{
							teacherAffidavit = new TeacherAffidavit();
							teacherAffidavit.setTeacherId(teacherDetail);
							teacherAffidavit.setCreatedDateTime(new Date());
						}

						teacherAffidavit.setAffidavitAccepted(true);
						teacherAffidavit.setIsDone(true);
						teacherAffidavitDAO.makePersistent(teacherAffidavit);
						
						tPortfolioStatus.setIsAffidavitCompleted(true);
						teacherPortfolioStatusDAO.makePersistent(tPortfolioStatus);
					}
					if(districtIdForDSPQ!=null)
					{
						districtMaster = districtMasterDAO.findById(districtIdForDSPQ, false, false);
						updateCredentialsFlag(candidateType,districtMaster,jobOrder);
					}

				}catch(Exception e)
				{
					e.printStackTrace();
				}
				
				int isPortfolio = 0;
				if(tPortfolioStatus!=null)
				{
					if((teacherDetail.getIsPortfolioNeeded()&& tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted()))
					{
						isPortfolio = 1;
					}
				}
				
				JobOrder jobOrderTemp=null;
				int iTempJobId1=0;
				try { iTempJobId1=Utility.getIntValue(jobId); } catch (Exception e) {}
				try { jobOrderTemp=jobOrderDAO.findById(iTempJobId1, false, false); } catch (Exception e) {}
				
				if(jobOrderTemp!=null && jobOrderTemp.getDistrictMaster()!=null && (jobOrderTemp.getDistrictMaster().getIsPortfolioNeeded()==null || (jobOrderTemp.getDistrictMaster().getIsPortfolioNeeded()!=null && jobOrderTemp.getDistrictMaster().getIsPortfolioNeeded().equals(false))))
					isPortfolio = 1;
					
				System.out.println("]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]] isPortfolio::: "+isPortfolio);
				System.out.println("]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]] portfolioStatus::: "+portfolioStatus);
				if(retArray[0]==1 && portfolioStatus!=null && portfolioStatus.equalsIgnoreCase("false"))
				{
					if(jobId!=null && !jobId.equalsIgnoreCase("")){
					//portfolioStatus,String jobId
					System.out.println("============================= inside ");
					retArray[1]=isPortfolio;
					String[] resp = new String[9];
					resp = dashboardAjax.getTeacherCriteria(jobId);
					String contnw = resp[3];
					System.out.println("555555555 contnw:::: "+contnw);
					if(contnw.equals("''"))
						retArray[2]=0;
					else if(contnw.equals("epi"))
						retArray[2]=1;
					else if(contnw.equals("jsi"))
						retArray[2]=2;
					else if(contnw.equals("done"))
						retArray[2]=3;
				}
				}
				//-----------------------------------------------------------------

			}
		} 
		catch (Exception e)	{
			e.printStackTrace();
			return retArray;
		}	
		System.out.println(retArray[0]+" retArray[0] : retArray[1]:"+retArray[1]);
		return retArray;
	}


	public TeacherPortfolioStatus getAffidavitValue()
	{
		System.out.println("Calling getAffidavitValue Ajax Method");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		TeacherPortfolioStatus teacherPortfolioStatus=null;

		try
		{
			teacherPortfolioStatus= teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
		} 
		catch (Exception e)	{
			e.printStackTrace();
			return teacherPortfolioStatus;
		}	
		return teacherPortfolioStatus;
	}

	public TeacherExperience getTFAValues()
	{
		System.out.println("Calling getTFAValues Ajax Method");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		TeacherExperience teacherExperience=null;
		try{
			teacherExperience= teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
		} 
		catch (Exception e)	{
			e.printStackTrace();
			return teacherExperience;
		}	
		return teacherExperience;
	}

	public String updatePortfolioStatus()
	{

		System.out.println("=========================updatePortfolioStatus ==============================================");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		try 
		{
			boolean bTeacherPortfolioStatus=false;
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(teacherPortfolioStatus==null)
			{
				bTeacherPortfolioStatus=true;
				teacherPortfolioStatus = new TeacherPortfolioStatus();
				teacherPortfolioStatus.setIsExperiencesCompleted(true);
			}
			else
			{
				teacherPortfolioStatus.setIsExperiencesCompleted(true);
			}



			try{
				if(teacherPortfolioStatus.getIsPersonalInfoCompleted()==null)
				{
					teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsCertificationsCompleted()==null){
					teacherPortfolioStatus.setIsCertificationsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsCertificationsCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsExperiencesCompleted()==null){
					teacherPortfolioStatus.setIsExperiencesCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsExperiencesCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsAffidavitCompleted()==null){
					teacherPortfolioStatus.setIsAffidavitCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAffidavitCompleted(false);
				e.printStackTrace();
			}

			try{
				if(teacherPortfolioStatus.getIsAcademicsCompleted()==null){
					teacherPortfolioStatus.setIsAcademicsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAcademicsCompleted(false);
				e.printStackTrace();
			}


			//teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			TeacherPortfolioStatus tpsTemp=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(bTeacherPortfolioStatus && tpsTemp==null)
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			else
			{
				teacherPortfolioStatus.setId(tpsTemp.getId());
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return "";
		}
		return "ok";
	}

	public TeacherPersonalInfo getTeacherPersonalInfoValues()
	{
		System.out.println("Calling getTFAValues Ajax Method");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

		teacherDetail=teacherDetailDAO.findById(teacherDetail.getTeacherId(), false, false);

		TeacherPersonalInfo teacherPersonalInfo =null;
		try{
			teacherPersonalInfo= teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
			if(teacherPersonalInfo!=null)
			{
				try {
					if(teacherPersonalInfo.getSSN()!=null)
						teacherPersonalInfo.setSSN(Utility.decodeBase64(teacherPersonalInfo.getSSN()));
				} catch (Exception e) {
					e.printStackTrace();
				}
				try{
					if(teacherPersonalInfo.getSIN()!=null){
						teacherPersonalInfo.setSIN(Utility.decodeBase64(teacherPersonalInfo.getSIN()));
					  }
					
				}catch(Exception e){
					e.printStackTrace();
				}			
				
				try {
					if(teacherPersonalInfo.getDrivingLicNum()!=null && teacherPersonalInfo.getDrivingLicNum()!="")
						teacherPersonalInfo.setDrivingLicNum(Utility.decodeBase64(teacherPersonalInfo.getDrivingLicNum()));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
			{
				teacherPersonalInfo =new TeacherPersonalInfo();
				if(teacherDetail.getFirstName()!=null)
					teacherPersonalInfo.setFirstName(teacherDetail.getFirstName());

				if(teacherDetail.getLastName()!=null)
					teacherPersonalInfo.setLastName(teacherDetail.getLastName());
			}
		} 
		catch (Exception e)	{
			e.printStackTrace();
			return teacherPersonalInfo;
		}	
		return teacherPersonalInfo;
	}

	/*@Transactional(readOnly=false)
	public TeacherPersonalInfo savePersonalInfo(Integer salutation,String firstName,String middleName,String lastName,String ssn,String dob,
			Integer employeeType,String formerEmployeeNo,Integer isRetiredEmployee,String currentEmployeeNo,Integer isCurrentFullTimeTeacher)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }

		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		TeacherPersonalInfo teacherPersonalInfo =null;
		try{
			teacherPersonalInfo= teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
			teacherPersonalInfo.setSalutation(salutation);
			teacherPersonalInfo.setFirstName(firstName);
			teacherPersonalInfo.setMiddleName(middleName);
			teacherPersonalInfo.setLastName(lastName);
			teacherPersonalInfo.setSSN(ssn);
			teacherPersonalInfo.setDob(Utility.getCurrentDateFormart(dob));
			teacherPersonalInfo.setEmployeeType(employeeType);
			if(employeeType==1)
			{
				teacherPersonalInfo.setEmployeeNumber(currentEmployeeNo);
				teacherPersonalInfo.setIsCurrentFullTimeTeacher(isCurrentFullTimeTeacher);
				teacherPersonalInfo.setIsRetiredEmployee(isRetiredEmployee);
			}else if(employeeType==2)
			{
				teacherPersonalInfo.setEmployeeNumber(formerEmployeeNo);
				teacherPersonalInfo.setIsRetiredEmployee(isRetiredEmployee);
				teacherPersonalInfo.setIsCurrentFullTimeTeacher(isCurrentFullTimeTeacher);
			}
			teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
		} 
		catch (Exception e)	{
			e.printStackTrace();
			return teacherPersonalInfo;
		}	
		return teacherPersonalInfo;

	}*/
	//--------------------------Testing for flag================================

	public void updateCredentialsFlag(String candidateType,DistrictMaster districtMaster,JobOrder jobOrder)
	{
		TeacherDetail teacherDetail = null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

		TeacherPortfolioStatus teacherPortfolioStatus = null;
		try
		{
			teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);

			if(teacherPortfolioStatus==null)
			{
				teacherPortfolioStatus = new TeacherPortfolioStatus();
				teacherPortfolioStatus.setTeacherId(teacherDetail);
			}

			//================================================================================

			DistrictPortfolioConfig dpcFlag = districtPortfolioConfigDAO.getPortfolioConfig(districtMaster, candidateType,jobOrder);
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);

			if(dpcFlag!=null && dpcFlag.getAddress())
			{
				TeacherPersonalInfo personalInfo = teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
				if(personalInfo==null)
				{
					personalInfo = new TeacherPersonalInfo();
					personalInfo.setFirstName(teacherDetail.getFirstName());
					personalInfo.setLastName(teacherDetail.getLastName());
					teacherPersonalInfoDAO.makePersistent(personalInfo);
				}

				if(personalInfo.getFirstName()!=null && !personalInfo.getFirstName().equals("") && personalInfo.getLastName()!=null && !personalInfo.getLastName().equals("") && personalInfo.getAddressLine1()!=null && !personalInfo.getAddressLine1().equals("") && personalInfo.getCountryId()!=null && !personalInfo.getCountryId().getCountryId().equals("") && personalInfo.getStateId()!=null && !personalInfo.getStateId().getStateId().equals("") && personalInfo.getCityId()!=null && !personalInfo.getCityId().getCityId().equals("")) 
				{
					teacherPortfolioStatus.setIsPersonalInfoCompleted(true);
				}
			}


			if(dpcFlag!=null && dpcFlag.getExpCertTeacherTraining()==true && dpcFlag.getTfaAffiliate()==true && dpcFlag.getNationalBoardCert()==true)
			{
				boolean nbcFlag = false;
				boolean tfaFlag = false;

				if(teacherExperience!=null)
				{
					if(teacherExperience.getNationalBoardCert()!=null && teacherExperience.getNationalBoardCert()==true)
						if(teacherExperience.getNationalBoardCertYear()!=null && !teacherExperience.getNationalBoardCertYear().equals(""))
							nbcFlag=true;

					if(teacherExperience.getNationalBoardCert()!=null && teacherExperience.getNationalBoardCert()==false)
						nbcFlag=true;

					if(teacherExperience.getTfaAffiliateMaster()!=null)
					{
						if(teacherExperience.getTfaAffiliateMaster().getTfaAffiliateId()!=null && teacherExperience.getTfaAffiliateMaster().getTfaAffiliateId()==3)
							tfaFlag = true;

						if(teacherExperience.getTfaAffiliateMaster().getTfaAffiliateId()!=null && teacherExperience.getTfaAffiliateMaster().getTfaAffiliateId()!=3)
							if(teacherExperience.getCorpsYear()!=null && !teacherExperience.getCorpsYear().equals("") && teacherExperience.getTfaRegionMaster()!=null)
								tfaFlag = true;
					}


					/*if(teacherExperience.getCorpsYear()!=null && !teacherExperience.getCorpsYear().equals("") && teacherExperience.getTfaRegionMaster()!=null)
							tfaFlag = true;*/

					if(teacherExperience.getExpCertTeacherTraining()!=null && !teacherExperience.getExpCertTeacherTraining().equals("") &&  nbcFlag==true && tfaFlag==true)
					{
						teacherPortfolioStatus.setIsCertificationsCompleted(true);
					}
				}
			}

			if(dpcFlag!=null && dpcFlag.getResume())
			{
				if(teacherExperience.getResume()!=null && !teacherExperience.getResume().equals(""))
				{
					teacherPortfolioStatus.setIsExperiencesCompleted(true);
				}
			}

			if(dpcFlag!=null && dpcFlag.getAffidavit())
			{
				TeacherAffidavit teacherAffidavit = teacherAffidavitDAO.findAffidavitByTeacher(teacherDetail);

				if(teacherAffidavit!=null)
				{
					if(teacherAffidavit.getAffidavitAccepted()!=null && teacherAffidavit.getAffidavitAccepted()!=false && teacherAffidavit.getIsDone()!=null && teacherAffidavit.getIsDone()!=false)
					{
						teacherPortfolioStatus.setIsAffidavitCompleted(true);
					}
				}
			}

			//================================================================================

			/*if(name!=null && name.equals("personal"))
			{
				TeacherPersonalInfo personalInfo = teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
				if(personalInfo==null)
				{
					personalInfo.setFirstName(teacherDetail.getFirstName());
					personalInfo.setLastName(teacherDetail.getLastName());
					teacherPersonalInfoDAO.makePersistent(personalInfo);
				}

				teacherPortfolioStatus.setIsPersonalInfoCompleted(true);

			}
			else if(name!=null && name.equals("credentials"))
			{
				teacherPortfolioStatus.setIsCertificationsCompleted(true);
			}
			else if(name!=null && name.equals("experiences"))
			{
				TeacherExperience	teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);

				if(teacherExperience.getResume()!=null && !teacherExperience.getResume().equals(""))
				{
					teacherPortfolioStatus.setIsExperiencesCompleted(true);
				}
			}
			else if(name!=null && name.equals("affidavit"))
			{
				TeacherAffidavit teacherAffidavit = null;
				teacherAffidavit = teacherAffidavitDAO.findAffidavitByTeacher(teacherDetail);

				if(teacherAffidavit==null)
				{
					teacherAffidavit = new TeacherAffidavit();
					teacherAffidavit.setTeacherId(teacherDetail);
				}

				teacherAffidavit.setAffidavitAccepted(true);
				teacherAffidavit.setIsDone(true);
				teacherAffidavitDAO.makePersistent(teacherAffidavit);


				teacherPortfolioStatus.setIsAffidavitCompleted(true);
			}*/
			try{
				if(teacherPortfolioStatus.getIsPersonalInfoCompleted()==null){
					teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				e.printStackTrace();
			}
			try{
				if(teacherPortfolioStatus.getIsCertificationsCompleted()==null){
					teacherPortfolioStatus.setIsCertificationsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsCertificationsCompleted(false);
				e.printStackTrace();
			}
			try{
				if(teacherPortfolioStatus.getIsExperiencesCompleted()==null){
					teacherPortfolioStatus.setIsExperiencesCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsExperiencesCompleted(false);
				e.printStackTrace();
			}
			try{
				if(teacherPortfolioStatus.getIsAffidavitCompleted()==null){
					teacherPortfolioStatus.setIsAffidavitCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAffidavitCompleted(false);
				e.printStackTrace();
			}
			teacherPortfolioStatus.setIsAcademicsCompleted(true);
			teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);

		}catch(Exception e){
			e.printStackTrace();
		}
	}

	//Delete job from jobforteacher, if candidate have multiple account and applied external job  
	public void chkDupCandidate(String teacherId,String jobId)
	{

		System.out.println(" ================= chkDupCandidate ===========================");
		try
		{
			System.out.println("1 "+teacherId+" "+jobId);
			List<JobForTeacher> jobExist = new ArrayList<JobForTeacher>(); 

			System.out.println(" 2 2 ");

			if(teacherId!=null && !teacherId.equals("") && jobId!=null && !jobId.equals(""))
			{
				System.out.println(" 3 2 ");

				TeacherDetail teacherDetail = teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);

				System.out.println(" 4 4 ");

				JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);

				System.out.println(" 5 5");

				jobExist = jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail, jobOrder);

				System.out.println(" 6 6 jobExist "+jobExist.size());

				if(jobExist!=null && jobExist.size()==1)
				{
					System.out.println(" 7 7 ");
					jobForTeacherDAO.makeTransient(jobExist.get(0));
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public String getDistrictSpecificTFAValues(DistrictMaster districtId)
	{
		System.out.println("Calling getDistrictSpecificTFAValues Ajax Method");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}


		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		List<DistrictSpecificTfaOptions> districtSpecificTfaOptions=null;
		List<DistrictSpecificTeacherTfaOptions> districtSpecificTeacherTfaOptions=null;
		StringBuffer tfadata = new StringBuffer();
		String selectedOptions = "";
		try{
			Criterion criterion = Restrictions.eq("districtMaster", districtId);
			Criterion criterion2 =Restrictions.eq("teacherDetail", teacherDetail);
			districtSpecificTfaOptions= districtSpecificTfaOptionsDAO.findByCriteria(criterion);

			districtSpecificTeacherTfaOptions = districtSpecificTeacherTfaOptionsDAO.findByCriteria(criterion,criterion2);

			if(districtSpecificTeacherTfaOptions!=null && districtSpecificTeacherTfaOptions.size()>0 && districtSpecificTeacherTfaOptions.get(0).getTfaOptions()!=null){
				selectedOptions = districtSpecificTeacherTfaOptions.get(0).getTfaOptions()==null?"":districtSpecificTeacherTfaOptions.get(0).getTfaOptions();
			}
			System.out.println("selectedOptions::::>:"+selectedOptions);
			String[] multiCounts = selectedOptions.split("\\|");
			int multiCount=1;

			if(districtSpecificTfaOptions!=null && districtSpecificTfaOptions.size()>0)
			{
				boolean othChecked=false;
				String insertedText="";
				String hideCss="hide";
				for(DistrictSpecificTfaOptions dtfa:districtSpecificTfaOptions)
				{
					String checkedVal="";

					for(int i=0;i<multiCounts.length;i++){
						try{checkedVal =multiCounts[i];}catch(Exception e){}
						if(checkedVal!=null && !checkedVal.equals("") && dtfa.getDistrictspecifictfaoptionsId()==Integer.parseInt(checkedVal)){
							if(checkedVal!=null && !checkedVal.equals("")){
								checkedVal="checked";

								if(dtfa.getTfaOptions().equalsIgnoreCase("Other")){
									othChecked=true;
									insertedText=districtSpecificTeacherTfaOptions.get(0).getOthersText();
									hideCss="show";
								}
							}
							break;
						}
					}
					String addOnclickFn="";
					String chkId="tfaOptId";
					if(dtfa.getTfaOptions().equalsIgnoreCase("Other")){
						addOnclickFn="onclick='openTFAOptionOther("+dtfa.getDistrictspecifictfaoptionsId()+");'";
						chkId="tfaOptId"+dtfa.getDistrictspecifictfaoptionsId();
					}

					tfadata.append("<div class='col-sm-12 col-md-12'>");
					tfadata.append("<label class='checkbox'>");
					tfadata.append("<input style='margin-right:5px;' "+checkedVal+" "+addOnclickFn+" type='checkbox' name='tfaOptId' id='"+chkId+"' class='tfaOptId' value='"+dtfa.getDistrictspecifictfaoptionsId()+"'>"+dtfa.getTfaOptions());
					tfadata.append("</label>");
					tfadata.append("</div>");
				}
				//	othChecked=true;



				tfadata.append("<script>$('#onlyOnetext').jqte();</script>");
				tfadata.append("<div class='col-sm-9 col-md-9 "+hideCss+"' id='tfaOthText'><textarea id='onlyOnetext' rows='' cols=''>"+insertedText+"</textarea></div>");					

			}
		} 
		catch (Exception e)	{
			e.printStackTrace();
			return tfadata.toString();
		}	
		return tfadata.toString();
	}

	public int setDistrictSpecificTFAValues(DistrictMaster districtMaster,String options,String othText)
	{
		System.out.println("Calling getDistrictSpecificTFAValues Ajax Method");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		List<DistrictSpecificTeacherTfaOptions> districtSpecificTeacherTfaOptions=null;
		try {
			Criterion criterion = Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2 =Restrictions.eq("teacherDetail", teacherDetail);			

			districtSpecificTeacherTfaOptions = districtSpecificTeacherTfaOptionsDAO.findByCriteria(criterion,criterion2);

			DistrictSpecificTeacherTfaOptions dSpecificTeacherTfaOptions = new DistrictSpecificTeacherTfaOptions();
			if(districtSpecificTeacherTfaOptions!=null && districtSpecificTeacherTfaOptions.size()>0){
				dSpecificTeacherTfaOptions = districtSpecificTeacherTfaOptions.get(0);
			}

			dSpecificTeacherTfaOptions.setDistrictMaster(districtMaster);
			dSpecificTeacherTfaOptions.setOthersText(othText);
			dSpecificTeacherTfaOptions.setTeacherDetail(teacherDetail);
			dSpecificTeacherTfaOptions.setTfaOptions(options);

			districtSpecificTeacherTfaOptionsDAO.makePersistent(dSpecificTeacherTfaOptions);
		} catch (Exception e) {
			// TODO: handle exception
		}

		return 1;
	}
	
	public int insertOrUpdateStdTchrExp(Integer studentTeacherExperienceId,String schoolName,String subject,Boolean pkOffered,Boolean kgOffered,Boolean g01Offered,Boolean g02Offered,Boolean g03Offered,Boolean g04Offered,Boolean g05Offered,Boolean g06Offered,Boolean g07Offered,Boolean g08Offered,Boolean g09Offered,Boolean g10Offered,Boolean g11Offered,Boolean g12Offered,String fromDate,String toDate){
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		try {
			
		
		StudentTeacherExperience studentTeacherExperience = new StudentTeacherExperience();
		if(studentTeacherExperienceId!=null && studentTeacherExperienceId!=0){
			studentTeacherExperience = studentTeacherExperienceDAO.findById(studentTeacherExperienceId, false, false);
		}
		
		studentTeacherExperience.setTeacherDetail(teacherDetail);
		studentTeacherExperience.setSchoolName(schoolName);
		studentTeacherExperience.setSubject(subject);
		studentTeacherExperience.setPkOffered(pkOffered);
		studentTeacherExperience.setKgOffered(kgOffered);
		studentTeacherExperience.setG01Offered(g01Offered);
		studentTeacherExperience.setG02Offered(g02Offered);
		studentTeacherExperience.setG03Offered(g03Offered);

		studentTeacherExperience.setG04Offered(g04Offered);
		studentTeacherExperience.setG05Offered(g05Offered);
		
		studentTeacherExperience.setG06Offered(g06Offered);
		studentTeacherExperience.setG07Offered(g07Offered);
		studentTeacherExperience.setG08Offered(g08Offered);
		studentTeacherExperience.setG09Offered(g09Offered);
		studentTeacherExperience.setG10Offered(g10Offered);
		studentTeacherExperience.setG11Offered(g11Offered);
		studentTeacherExperience.setG12Offered(g12Offered);
		studentTeacherExperience.setFromDate(fromDate);
		studentTeacherExperience.setToDate(toDate);
		studentTeacherExperience.setCreatedDateTime(new Date());
		studentTeacherExperienceDAO.makePersistent(studentTeacherExperience);

		
		
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		return 0;
		
	}
	
	public String displayStdTchrExp(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb = new StringBuffer();		
		try 
		{
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------
			System.out.println(start+" start "+end);
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			List<StudentTeacherExperience> listTeacherExp= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			
			listTeacherExp = studentTeacherExperienceDAO.findByCriteria(sortOrderStrVal,criterion1);		

			List<StudentTeacherExperience> sortedTeacherRole		=	new ArrayList<StudentTeacherExperience>();

			SortedMap<String,StudentTeacherExperience>	sortedMap = new TreeMap<String,StudentTeacherExperience>();
			int mapFlag=2;
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherRole.add((StudentTeacherExperience) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherRole.add((StudentTeacherExperience) sortedMap.get(key));
				}
			}else{
				sortedTeacherRole=listTeacherExp;
			}

			totalRecord =listTeacherExp.size();
			if(totalRecord<end)
				end=totalRecord;
			List<StudentTeacherExperience> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);

			String responseText="";
			sb.append("<table border='0' id='StdTchrExpLinktblGrid' class='table table-striped'>");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingMLink(lblSchoolName,sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLink(lblSub,sortOrderFieldName,"subject",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink(lblFrDate,sortOrderFieldName,"fromDate",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink(lblToDate,sortOrderFieldName,"toDate",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");			

			sb.append("<th  width='20%' valign='top'>");
			sb.append(lblAct);
			sb.append("</th>");	

			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherExp!=null)
				
				for(StudentTeacherExperience pojo:listsortedTeacherRole)
				{
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(pojo.getSchoolName());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(pojo.getSubject());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(pojo.getFromDate());
					sb.append("</td>");
					sb.append("<td>");
					sb.append(pojo.getToDate());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append("<a href='#' onclick=\"return editFormStdTchrExp('"+pojo.getStudentTeacherExperienceId()+"')\" >"+lblEdit+"</a>");
					sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return delStdTchrExp('"+pojo.getStudentTeacherExperienceId()+"')\" >"+lnkDlt+"</a>");
					sb.append("</td>");
					
					sb.append("</tr>");
				}

			if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='3'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationTrippleGrid(request,totalRecord,noOfRow, pageNo));
			sb.append("<input type='hidden' id='ttlRecStdExp' value='"+listsortedTeacherRole.size()+"'>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}
	
	public Boolean delStdTchrExp(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		Integer stdTchrAutoId = Integer.parseInt(id.trim());
		StudentTeacherExperience studentTeacherExperience = studentTeacherExperienceDAO.findById(stdTchrAutoId, false, false);
		studentTeacherExperienceDAO.makeTransient(studentTeacherExperience);
		return true;		
	}
	
	public StudentTeacherExperience editFormStdTchrExp(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		StudentTeacherExperience StudentTeacherExperience = null;
		try 
		{
			Integer elerefAutoId = Integer.parseInt(id.trim());
			StudentTeacherExperience = studentTeacherExperienceDAO.findById(elerefAutoId, false, false);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return StudentTeacherExperience;
	}
	
	public String getTeacherVeteran(DistrictMaster districtId)
    {
          System.out.println("Calling getDistrictSpecificVeteranValues Ajax Method   "+districtId);
          WebContext context;
          context = WebContextFactory.get();
          HttpServletRequest request = context.getHttpServletRequest();
          HttpSession session = request.getSession(false);
          if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
          {
               throw new IllegalStateException(msgYrSesstionExp);
          }

          TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");          
          List<DistrictSpecificTeacherTfaOptions> districtSpecificTeacherTfaOptions=null;
          
          String selectedOptions = "";
          try{
               Criterion criterion = Restrictions.eq("districtMaster", districtId);
               Criterion criterion2 =Restrictions.eq("teacherDetail", teacherDetail);
               districtSpecificTeacherTfaOptions = districtSpecificTeacherTfaOptionsDAO.findByCriteria(criterion,criterion2);

               if(districtSpecificTeacherTfaOptions!=null && districtSpecificTeacherTfaOptions.size()>0 && districtSpecificTeacherTfaOptions.get(0).getTfaOptions()!=null){
                    selectedOptions = districtSpecificTeacherTfaOptions.get(0).getTfaOptions()==null?"":districtSpecificTeacherTfaOptions.get(0).getTfaOptions();
                    System.out.println("selectedOptions    "+selectedOptions);
               }
          } 
          catch (Exception e)  {
               e.printStackTrace();
               return selectedOptions;
          }    
          return selectedOptions;
    }
	
	public String displayTeacherLanguage(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb = new StringBuffer();		
		try 
		{
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------
			System.out.println(start+" start "+end);
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			List<TeacherLanguages> listTeacherExp= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			
			listTeacherExp = teacherLanguagesDAO.findByCriteria(sortOrderStrVal,criterion1);		

			List<TeacherLanguages> sortedTeacherRole		=	new ArrayList<TeacherLanguages>();

			SortedMap<String,TeacherLanguages>	sortedMap = new TreeMap<String,TeacherLanguages>();
			int mapFlag=2;
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherRole.add((TeacherLanguages) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherRole.add((TeacherLanguages) sortedMap.get(key));
				}
			}else{
				sortedTeacherRole=listTeacherExp;
			}

			totalRecord =listTeacherExp.size();
			if(totalRecord<end)
				end=totalRecord;
			List<TeacherLanguages> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);

			String responseText="";
			sb.append("<table border='0' id='teacherLanguageGrid' class='table table-striped'>");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingMLink(lblLanguage,sortOrderFieldName,"language",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLink(lblOralSkill,sortOrderFieldName,"oralSkills",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			//responseText=PaginationAndSorting.responseSortingMLink("Written Skill",sortOrderFieldName,"writtenSkill",sortOrderTypeVal,pgNo);
			//sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			sb.append("<th width='20%' valign='top'>"+lblWrittenSkill+"</th>");

			sb.append("<th  width='20%' valign='top'>");
			sb.append(lblAct);
			sb.append("</th>");	

			sb.append("</tr>");
			sb.append("</thead>");
			Map<Integer,String> optionMap = new HashMap<Integer, String>();
			optionMap.put(1, "Polite");
			optionMap.put(2, "Literate");
			optionMap.put(3, "Fluent");
			if(listTeacherExp!=null)
				
				for(TeacherLanguages pojo:listsortedTeacherRole)
				{
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(pojo.getLanguage());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(optionMap.get(pojo.getOralSkills()));
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(optionMap.get(pojo.getWrittenSkills()));
					sb.append("</td>");
					
					
					sb.append("<td>");
					sb.append("<a href='#' onclick=\"return editTeacherLanguage('"+pojo.getTeacherLanguageId()+"')\" >"+lblEdit+"</a>");
					sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return delTeacherLanguage('"+pojo.getTeacherLanguageId()+"')\" >"+lnkDlt+"</a>");
					sb.append("</td>");
					
					sb.append("</tr>");
				}

			if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='3'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationTrippleGrid(request,totalRecord,noOfRow, pageNo));
			sb.append("<input type='hidden' id='ttlTeacherLanguage' value='"+listsortedTeacherRole.size()+"'>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}
		
	
	public Integer insertOrUpdateTchrLang(String language,Integer oralSkill,Integer writtenSkill,Integer teacherLanguageId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		try
		{
			TeacherLanguages teacherLanguage = new TeacherLanguages();
			
			System.out.println(language+"  "+oralSkill+"  "+writtenSkill+"  "+teacherLanguageId);
			if(teacherLanguageId!=null && teacherLanguageId!=0){
				teacherLanguage= teacherLanguagesDAO.findById(teacherLanguageId, false, false);
			}
					
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");		
			
			teacherLanguage.setTeacherDetail(teacherDetail);
			
			teacherLanguage.setLanguage(language);
			
			teacherLanguage.setOralSkills(oralSkill);
			
			teacherLanguage.setWrittenSkills(writtenSkill);
			
			teacherLanguage.setCreatedDateTime(new Date());
			
			teacherLanguagesDAO.makePersistent(teacherLanguage);
			
			return 1;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return 0;
		
	}
	
	public Boolean delTeacherLanguage(String id)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try 
		{

			Integer invId = Integer.parseInt(id.trim());
			TeacherLanguages teacherLanguages = teacherLanguagesDAO.findById(invId, false, false);

			teacherLanguagesDAO.makeTransient(teacherLanguages);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return true;

	}
	
	public TeacherLanguages showEditLang(String id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		TeacherLanguages teacherLanguages = null;
		try 
		{
			Integer invId = Integer.parseInt(id.trim());
			teacherLanguages = teacherLanguagesDAO.findById(invId, false, false);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return teacherLanguages;

	}
	
	public Integer updateLangeuage(Integer lanuageoption)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
			
		
		try
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			
			
			if(lanuageoption!=null && !lanuageoption.equals(""))
			{
				if(teacherDetail!=null)
				{
						TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
						
						if(teacherExperience==null){							
							teacherExperience = new TeacherExperience();
							teacherExperience.setTeacherId(teacherDetail);
							teacherExperience.setCanServeAsSubTeacher(2);
							teacherExperience.setIsNonTeacher(false);
						}
						
						if(lanuageoption.equals(1))							
						teacherExperience.setKnowOtherLanguages(true);
						else
							teacherExperience.setKnowOtherLanguages(false);
						teacherExperienceDAO.makePersistent(teacherExperience);
						return 1;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return 2;
	
	}
	
	public Boolean checkKnowLanguage(){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		Boolean result= false;
		try {
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			
			if(teacherDetail!=null)
			{
					TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
					
					if(teacherExperience!=null && teacherExperience.getKnowOtherLanguages()!=null){
						result=teacherExperience.getKnowOtherLanguages();
					}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}
		
	public String downloadAnswer(Integer answerId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String path="";

		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			List<DistrictSpecificPortfolioAnswers> districtSpecificPortfolioAnswersList = districtSpecificPortfolioAnswersDAO.findByCriteria(Restrictions.eq("answerId", answerId),Restrictions.eq("questionType", "UAT"));
			if(districtSpecificPortfolioAnswersList!=null && districtSpecificPortfolioAnswersList.size()>0)
			{	teacherDetail=districtSpecificPortfolioAnswersList.get(0).getTeacherDetail();
				if(districtSpecificPortfolioAnswersList.get(0).getFileName()!=null && !districtSpecificPortfolioAnswersList.get(0).getFileName().equalsIgnoreCase(""))
				{
					 String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+districtSpecificPortfolioAnswersList.get(0).getFileName();
				     String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
				     
				     File sourceFile = new File(source);
				     File targetDir = new File(target);
				     if(!targetDir.exists())
				    	 targetDir.mkdirs();
				     
				     File targetFile = new File(targetDir+"/"+sourceFile.getName());
				     
				     if(sourceFile.exists()){
				       	FileUtils.copyFile(sourceFile, targetFile);
				       	path =  Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
				     }
				     System.out.println("source=="+source);
				     System.out.println("target=="+target);
				     System.out.println("path=="+path);
				}
			}
			else
				System.out.println("Answer is null or Blank");
		}
		catch (Exception e) 
		{
			System.out.println("Error in Answer");
			e.printStackTrace();
		}
		return path;
	}
	
	public boolean saveOctDetails(Integer octId,Integer octOption,String octNumber,String octFileName,String octText){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
/*SessionFactory sessionFactory=octDetailsDAO.getSessionFactory();
StatelessSession statelesSsession = sessionFactory.openStatelessSession();
Transaction txOpen =statelesSsession.beginTransaction();*/
		try {
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			if(teacherDetail!=null){
				Criterion criterion = Restrictions.eq("teacherId", teacherDetail);
				OctDetails octDetails = null;
				if(octId!=null && octId!=0){
					octDetails = octDetailsDAO.findById(octId, false, false);
					System.out.println("aaaaaaaaa");
				}
				
				if(octDetails==null){
					octDetails = new OctDetails();
					//System.out.println("bbbbbbbbbbbbb");
				}
				octDetails.setTeacherId(teacherDetail);
				octDetails.setOctOption(octOption);
				octDetails.setOctNumber(octNumber);
				octDetails.setOctFileName(octFileName);
				octDetails.setOctText(octText);
				octDetails.setCreatedDate(new Date());
				octDetailsDAO.makePersistent(octDetails);
				/*txOpen =statelesSsession.beginTransaction();		    	
				statelesSsession.insert(octDetails);
				txOpen.commit();*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public OctDetails getOctDetails(){
		OctDetails octDetails = new OctDetails();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try {
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
				Criterion criterion = Restrictions.eq("teacherId", teacherDetail);				
				octDetails = octDetailsDAO.findByCriteria(criterion).get(0);
		} catch (Exception e) {
		}
		return octDetails;
	}
	
	public String downloadOctUpload(Integer answerId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String path="";

		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			Criterion criterion = Restrictions.eq("teacherId", teacherDetail);				
			OctDetails octDetails = octDetailsDAO.findByCriteria(criterion).get(0);
			if(octDetails!=null)
			{

				 String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+octDetails.getOctFileName();
			     String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
			     
			     File sourceFile = new File(source);
			     File targetDir = new File(target);
			     if(!targetDir.exists())
			    	 targetDir.mkdirs();
			     
			     File targetFile = new File(targetDir+"/"+sourceFile.getName());
			     
			     if(sourceFile.exists()){
			       	FileUtils.copyFile(sourceFile, targetFile);
			       	path =  Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
			     }
			     System.out.println("source=="+source);
			     System.out.println("target=="+target);
			     System.out.println("path=="+path);
			
			}
			else
				System.out.println("oct is null or Blank");
		}
		catch (Exception e) 
		{
			System.out.println("Error in Answer");
			e.printStackTrace();
		}
		return path;
	}
	
	//Seniority number
	public String addSeniorityNumber(String senNumber,Integer jobId){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		try {
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
			DistrictMaster districtMaster = null;
			if(jobOrder!=null)
				districtMaster = jobOrder.getDistrictMaster();
				
			JobWiseTeacherDetails jobWiseTeacherDetails = null;
			
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterionJob = Restrictions.eq("jobOrder", jobOrder);
			
			List<JobWiseTeacherDetails>  jobWiseTeacherDetailsExist = jobWiseTeacherDetailsDAO.findByCriteria(criterion,criterionJob);
			if(jobWiseTeacherDetailsExist!=null && jobWiseTeacherDetailsExist.size()>0){
				jobWiseTeacherDetails = jobWiseTeacherDetailsExist.get(0);
			}else{
				jobWiseTeacherDetails = new JobWiseTeacherDetails();
			}
			
				jobWiseTeacherDetails.setTeacherDetail(teacherDetail);
				jobWiseTeacherDetails.setJobOrder(jobOrder);
				jobWiseTeacherDetails.setDistrictMaster(districtMaster);
				jobWiseTeacherDetails.setSeniorityNumber(senNumber.trim());
				jobWiseTeacherDetails.setCreatedDateTime(new Date());				
			jobWiseTeacherDetailsDAO.makePersistent(jobWiseTeacherDetails);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return "";
	}
	public String getSeniorityNumber(JobOrder jobOrder){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String senNUmber="";
		try {
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			JobWiseTeacherDetails jobWiseTeacherDetails =  new JobWiseTeacherDetails();
			
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterionJob = Restrictions.eq("jobOrder", jobOrder);
			
			List<JobWiseTeacherDetails>  jobWiseTeacherDetailsExist = jobWiseTeacherDetailsDAO.findByCriteria(criterion,criterionJob);
			if(jobWiseTeacherDetailsExist!=null && jobWiseTeacherDetailsExist.size()>0){
				jobWiseTeacherDetails = jobWiseTeacherDetailsExist.get(0);
				senNUmber=jobWiseTeacherDetails.getSeniorityNumber();
			}
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return senNUmber;
	}
	//Pavan Gupta
	public int[] insertOrUpdateTeacherPersonaLinForRem(Integer tfaAffiliateId,Integer corpsYear,Integer tfaRegionId,Integer willingAsSubstituteTeacher,String phoneNumber,String mobileNumber,String address1,String address2,String zipCode,String stateId,String cityId,String expCertTeacherTraining,String nationalBoardCert,boolean affidavit,Integer salutation_pi,String firstName_pi,String middleName_pi,String lastName_pi,String ssn_pi,String dob,String employeeType, String formerEmployeeNo,String currentEmployeeNo,String isCurrentFullTimeTeacher,String raceValue,String rtDate,String wdDate,Integer veteranValue,Integer districtIdForDSPQ,Integer ethnicOriginValue,Integer ethinicityValue,String countryId,Integer genderValue,String candidateType,String portfolioStatus,String jobId,boolean isNonTeacher,String retireNo,String distForRetire,String stMForretire,Integer expectedSalary,String anotherName, String noLongerEmployed)
	{
		System.out.println("Calling insertOrUpdateTeacherTFA Ajax Method  "+ssn_pi);

		//System.out.println("salutation_pi "+salutation_pi+" firstName_pi "+firstName_pi+" middleName_pi "+middleName_pi+" lastName_pi "+lastName_pi+" ssn_pi "+ssn_pi+" dob "+dob);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		int retArray[] = new int[5];
		retArray[0]=1;
		retArray[1]=1;
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		TFAAffiliateMaster tfaAffiliateMaster	=	null;
		TFARegionMaster tfaRegionMaster			=	null;

		try{
			TeacherExperience teacherExperience=null;
			teacherExperience= teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			//teacherPortfolioStatus =teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail); // Get Portfolio Status By teacher ID

			if(teacherExperience==null)
			{
				teacherExperience=new TeacherExperience();
				teacherExperience.setCanServeAsSubTeacher(2);
				teacherExperience.setCreatedDateTime(new Date());
				teacherExperience.setPotentialQuality(0);
				teacherExperience.setKnowOtherLanguages(false);
			}

			teacherExperience.setTeacherId(teacherDetail);

			if(tfaAffiliateId > 0)
				tfaAffiliateMaster	=	tfaAffiliateMasterDAO.findById(tfaAffiliateId, false, false);

			if(tfaAffiliateId==1 || tfaAffiliateId==2)
			{
				if(tfaRegionId > 0)
				{
					tfaRegionMaster	=tfaRegionMasterDAO.findById(tfaRegionId, false, false);
				}

				if(corpsYear >0 && tfaRegionMaster!=null && tfaAffiliateMaster!=null)
				{
					teacherExperience.setTfaAffiliateMaster(tfaAffiliateMaster);
					teacherExperience.setCorpsYear(corpsYear);
					teacherExperience.setTfaRegionMaster(tfaRegionMaster);
				}
			}
			else if(tfaAffiliateId==3 && tfaAffiliateMaster!=null)
			{
				teacherExperience.setCorpsYear(null);
				teacherExperience.setTfaRegionMaster(null);
				teacherExperience.setTfaAffiliateMaster(tfaAffiliateMaster);
			}
			else
			{
				teacherExperience.setCorpsYear(null);
				teacherExperience.setTfaRegionMaster(null);
				teacherExperience.setTfaAffiliateMaster(null);
			}

			if(willingAsSubstituteTeacher!=null)
				teacherExperience.setCanServeAsSubTeacher(willingAsSubstituteTeacher);

			if(expCertTeacherTraining!=null && !expCertTeacherTraining.equals(""))
				teacherExperience.setExpCertTeacherTraining(Double.parseDouble(expCertTeacherTraining));

			if(nationalBoardCert!=null && !nationalBoardCert.equals(""))
			{
				teacherExperience.setNationalBoardCertYear(Integer.parseInt(nationalBoardCert));
				teacherExperience.setNationalBoardCert(true);
			}
			else
			{
				teacherExperience.setNationalBoardCertYear(null);
				teacherExperience.setNationalBoardCert(false);
			}
			try{
				teacherExperience.setIsNonTeacher(isNonTeacher);
			}catch(Exception e)
			{
				e.printStackTrace();
			}


			teacherExperienceDAO.makePersistent(teacherExperience);
			//teacherPortfolioStatus.setIsExperiencesCompleted(true);

			TeacherPersonalInfo  teacherPersonalInfo =  new TeacherPersonalInfo();
			StateMaster stateMaster = new StateMaster();
			CityMaster cityMaster = new CityMaster();
			CountryMaster countryMaster = new CountryMaster();

			String ssnExisting = "";

			if(teacherDetail!=null)
			{

				/*if(phoneNumber!=null)
					teacherDetail.setPhoneNumber(phoneNumber);
				else
					teacherDetail.setPhoneNumber(null);


				teacherDetailDAO.makePersistent(teacherDetail);
				session.setAttribute("teacherDetail", teacherDetail);*/


				teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
				if(teacherPersonalInfo==null)
				{
					teacherPersonalInfo = new TeacherPersonalInfo();

					if(firstName_pi!=null && !firstName_pi.equalsIgnoreCase("")){
						teacherPersonalInfo.setFirstName(firstName_pi);
						teacherDetail.setFirstName(firstName_pi);
					}
					else
					{	teacherDetail.setFirstName(firstName_pi);
					if(teacherDetail.getFirstName()!=null)
						teacherPersonalInfo.setFirstName(teacherDetail.getFirstName());
					else
						teacherPersonalInfo.setFirstName("");
					}

					if(middleName_pi!=null && !middleName_pi.equalsIgnoreCase("")){
						teacherPersonalInfo.setMiddleName(middleName_pi);
					}else{
						teacherPersonalInfo.setMiddleName("");
					}

					if(lastName_pi!=null && !lastName_pi.equalsIgnoreCase("")){
						teacherPersonalInfo.setLastName(lastName_pi);
						teacherDetail.setLastName(lastName_pi);
					}
					else
					{teacherDetail.setLastName(lastName_pi);
					if(teacherDetail.getLastName()!=null)
					{
						teacherPersonalInfo.setLastName(teacherDetail.getLastName());
					}
					else
					{
						teacherPersonalInfo.setLastName("");
					}
					}

				}
				else
				{
					System.out.println(" Have Personal Information.................");

					if(firstName_pi!=null && !firstName_pi.equalsIgnoreCase("")){
						teacherPersonalInfo.setFirstName(firstName_pi);
						teacherDetail.setFirstName(firstName_pi);
					}
					if(middleName_pi!=null && !middleName_pi.equalsIgnoreCase("")){
						teacherPersonalInfo.setMiddleName(middleName_pi);
					}else{
						teacherPersonalInfo.setMiddleName("");
					}

					if(lastName_pi!=null && !lastName_pi.equalsIgnoreCase("")){
						teacherPersonalInfo.setLastName(lastName_pi);
						teacherDetail.setLastName(lastName_pi);
					}

					ssnExisting = Utility.decodeBase64(teacherPersonalInfo.getSSN());

					System.out.println(" SSSSSSSSSSSSSSSSSSSNNNNNNNNNNNNNNN ::::::: "+ssnExisting);

				}
				teacherPersonalInfo.setAnotherName(anotherName);
				Date dob_converted=null;
				Date rtDate_converted=null;
				Date wdDate_converted=null;
				try {
					if(dob!=null && !dob.equalsIgnoreCase(""))
						dob_converted=Utility.getCurrentDateFormart(dob);

					if(rtDate!=null && !rtDate.equalsIgnoreCase(""))
						rtDate_converted=Utility.getCurrentDateFormart(rtDate);

					if(wdDate!=null && !wdDate.equalsIgnoreCase(""))
						wdDate_converted=Utility.getCurrentDateFormart(wdDate);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				teacherPersonalInfo.setSalutation(salutation_pi);
				teacherPersonalInfo.setExpectedSalary(expectedSalary);

				if(phoneNumber!=null && !phoneNumber.equalsIgnoreCase(""))
					teacherPersonalInfo.setPhoneNumber(phoneNumber);
				if(mobileNumber!=null && !mobileNumber.equalsIgnoreCase(""))
					teacherPersonalInfo.setMobileNumber(mobileNumber);

				//	String ssnExisting = teacherPersonalInfo.getSSN();

				ssn_pi=ssn_pi==null?"":ssn_pi;
				boolean validSSN=false;
				try {
					Integer.parseInt(ssn_pi);
					validSSN=true;
				} catch (Exception e) {
					// TODO: handle exception
				}

				//Check ssn for Franklin-Essex-Hamilton District
				try
				{
					if(districtIdForDSPQ!=null && districtIdForDSPQ.equals(3680340))
					{
						if(ssn_pi!=null && !ssn_pi.equalsIgnoreCase("") && validSSN && ssn_pi.trim().length()==4)
						{
							if(ssnExisting!=null && !ssnExisting.equals(""))
							{
								System.out.println(" ssnExisting.substring(5).equals(ssn_pi)   "+ssnExisting.substring(5).equals(ssn_pi));
								String New_SSN = ssnExisting.substring(0, 5)+ssn_pi;
								System.out.println(" ============= Final Value of SSN :::::: "+New_SSN);
								if(!ssnExisting.substring(5).equals(ssn_pi))
									teacherPersonalInfo.setSSN(Utility.encodeInBase64(New_SSN));
							}else
							{
								System.out.println(" ============ When SSN is NOT Exist==============="+ssn_pi);
								teacherPersonalInfo.setSSN(Utility.encodeInBase64(ssn_pi));
							}
						}	
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}





				//Check Duplicate Employee
				try
				{System.out.println("101");
				retArray[3]=0;
				//retArray[4]=1;
				if(districtIdForDSPQ!=null && (districtIdForDSPQ.equals(1200390) || districtIdForDSPQ.equals(4218990)))// Miami Check
				{
					System.out.println("102");
					if(ssn_pi!=null && !ssn_pi.equalsIgnoreCase("") && validSSN && ssn_pi.trim().length()==9)
					{
						System.out.println("103");
						List<TeacherPersonalInfo> chkDuplicateTeacher = new ArrayList<TeacherPersonalInfo>();
						List<TeacherDetail> activeTeacherList = new ArrayList<TeacherDetail>();
						List<Integer> tacherIds = new ArrayList<Integer>();

						chkDuplicateTeacher = teacherPersonalInfoDAO.checkTeachersBySSN(Utility.encodeInBase64(ssn_pi));

						try
						{
							System.out.println("104");
							if(chkDuplicateTeacher.size()>0)
							{
								System.out.println("105 chkDuplicateTeacher :: "+chkDuplicateTeacher.size());
								for(TeacherPersonalInfo tpi:chkDuplicateTeacher)
								{
									System.out.println(" dup teacher :: "+tpi.getTeacherId());
									tacherIds.add(tpi.getTeacherId());
								}
								activeTeacherList = teacherDetailDAO.getActiveTeacherList(tacherIds);

								System.out.println("106 .. 0 .. :: activeTeacherList "+activeTeacherList.size());

								if(activeTeacherList.size()>0)
								{
									System.out.println("106 :: activeTeacherList "+activeTeacherList.size());
									if(activeTeacherList.size()==1)
									{
										System.out.println("107");
										if(!activeTeacherList.get(0).getTeacherId().equals(teacherDetail.getTeacherId()) && Utility.decodeBase64(chkDuplicateTeacher.get(0).getSSN()).equals(ssn_pi))
										{
											retArray[3]=1;
										}
									}
									else
									{
										System.out.println("108");
										retArray[3]=1;
									}
								}
							}
						}catch(Exception e)
						{
							e.printStackTrace();
						}
						System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> chkDuplicateTeacher.size() >>>>>>>> "+chkDuplicateTeacher.size());
					}
				}
				}catch(Exception e)
				{
					e.printStackTrace();
				}



				if(ssn_pi!=null && !ssn_pi.equalsIgnoreCase("") && validSSN && ssn_pi.trim().length()==9)
					teacherPersonalInfo.setSSN(Utility.encodeInBase64(ssn_pi));

				Integer empTypeExisting = teacherPersonalInfo.getEmployeeType();
				Integer fullTimeExisting = teacherPersonalInfo.getIsCurrentFullTimeTeacher();
				String empNoExisting = teacherPersonalInfo.getEmployeeNumber(); 

				if(employeeType!=null && !employeeType.equalsIgnoreCase(""))
					teacherPersonalInfo.setEmployeeType(Integer.parseInt(employeeType));

				if(employeeType!=null && !employeeType.equalsIgnoreCase("") && employeeType.equalsIgnoreCase("1"))
				{
					teacherPersonalInfo.setEmployeeNumber(currentEmployeeNo.trim());

					if(isCurrentFullTimeTeacher!=null && !isCurrentFullTimeTeacher.equalsIgnoreCase(""))
						teacherPersonalInfo.setIsCurrentFullTimeTeacher(Integer.parseInt(isCurrentFullTimeTeacher));

					teacherPersonalInfo.setRetirementdate(null);
					teacherPersonalInfo.setMoneywithdrawaldate(null);
				}
				else if(employeeType!=null && !employeeType.equalsIgnoreCase("") && employeeType.equalsIgnoreCase("0"))
				{
					teacherPersonalInfo.setEmployeeNumber(formerEmployeeNo);
					teacherPersonalInfo.setIsCurrentFullTimeTeacher(null);
					if(districtIdForDSPQ!=null && districtIdForDSPQ.equals(1302010))
							teacherPersonalInfo.setNoLongerEmployed(noLongerEmployed);

					if(rtDate_converted!=null)
						teacherPersonalInfo.setRetirementdate(rtDate_converted);

					if(wdDate_converted!=null)
						teacherPersonalInfo.setMoneywithdrawaldate(wdDate_converted);

				}
				if(employeeType!=null && !employeeType.equalsIgnoreCase("") && employeeType.equalsIgnoreCase("2"))
				{
					teacherPersonalInfo.setEmployeeNumber(null);
					teacherPersonalInfo.setIsCurrentFullTimeTeacher(null);
					teacherPersonalInfo.setRetirementdate(null);
					teacherPersonalInfo.setMoneywithdrawaldate(null);
				}
				System.out.println("===================================== ");
				boolean onlySSNcheck = false;
				if(districtIdForDSPQ.equals(4218990))// Philadaphia Check
					onlySSNcheck=true;

				if(districtIdForDSPQ!=null && districtIdForDSPQ.equals(1200390) || onlySSNcheck)// Miami Check
				{
					System.out.println("----------------------------- ");
					String ssn = teacherPersonalInfo.getSSN()==null?"":teacherPersonalInfo.getSSN();
					String empNo = teacherPersonalInfo.getEmployeeNumber()==null?"":teacherPersonalInfo.getEmployeeNumber();
					System.out.println("empNoempNoempNoempNoempNoempNo: "+empNo);
					//if(ssn.equals("") || empNo.equals(""))
					if(ssn.equals(""))
					{
						//return true;
						retArray[0]=1;
						retArray[1]=1;
						//return retArray;
					}else{

						if(!ssn.equals(""))
						{
							ssn = ssn.trim();
							ssn = Utility.decodeBase64(ssn);
							//ssn = ssn_pi;

							if(ssn.length()>4 && districtIdForDSPQ.equals(1200390))
								ssn = ssn.substring(ssn.length() - 4, ssn.length());
						}
						List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();
						System.out.println("pppppppp "+teacherPersonalInfo.getEmployeeNumber());
						DistrictMaster dm = new DistrictMaster();
						dm.setDistrictId(districtIdForDSPQ);
						if(onlySSNcheck)//Philedaphia
						{
							//if(ssn.length()==9)
							employeeMasters =	employeeMasterDAO.checkEmployeeByFullSSN(ssn,dm);
							
						}else
						{
							//Miami Only
							if(employeeType.equalsIgnoreCase("2"))// Not an employee
							{
								try {
									System.out.println("SSN: "+ssn);
									employeeMasters =	employeeMasterDAO.checkEmployeeBySSN(ssn,firstName_pi,lastName_pi,dm);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							else 
							{	// Miami requested to remove empNo check
								//employeeMasters =	employeeMasterDAO.checkEmployee(ssn, empNo,firstName_pi,lastName_pi,dm);
								try {
									System.out.println("SSN: "+ssn);
									employeeMasters =	employeeMasterDAO.checkEmployeeBySSN(ssn,firstName_pi,lastName_pi,dm);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
						System.out.println("employeeType:::: "+employeeType);
						System.out.println("llllllllllllllll employeeMasters:::::::::::::: "+employeeMasters.size());
						System.out.println("====vishwanath ========== onlySSNcheck= "+onlySSNcheck);
						if(employeeMasters.size()>0 && onlySSNcheck)
						{
							System.out.println("====vishwanath ==========");
							EmployeeMaster employeeMaster = employeeMasters.get(0);
							String PCCode=employeeMaster.getPCCode()==null?"":employeeMaster.getPCCode().trim();
							if(PCCode.equalsIgnoreCase("y"))
							{
									retArray[0]=0;
									retArray[1]=3;// restricted
							}

						}else if(districtIdForDSPQ.equals(1200390)) //Miami
						{
							JobOrder jobOrder1= null;
							String jobCategoryName = "";
							if(jobId!=null && !jobId.equalsIgnoreCase("")){
								
								int iJobId=0;
								try { iJobId=Utility.getIntValue(jobId); } catch (Exception e) {}
								try { jobOrder1=jobOrderDAO.findById(iJobId, false, false); } catch (Exception e) {}
								if(jobOrder1!=null)
								jobCategoryName = jobOrder1.getJobCategoryMaster().getJobCategoryName();
							}
							
							if(employeeMasters.size()>0)
							{
								EmployeeMaster employeeMaster = employeeMasters.get(0);

								Date grugTestFailDate = employeeMaster.getDrugTestFailDate();
								System.out.println(employeeMaster.getEmployeeId()+" grugTestFailDate:: "+grugTestFailDate);
								boolean isJobNotAllowed = false;
								if(grugTestFailDate!=null)
								{
									DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForDSPQ, false, false);
									if(districtMaster.getNoOfDaysRestricted()!=null && districtMaster.getNoOfDaysRestricted()!=0)
									{
										Calendar c = Calendar.getInstance();
										c.setTime(grugTestFailDate);
										//c.add(Calendar.YEAR,3);
										//c.add(Calendar.DATE, 1095);
										c.add(Calendar.DATE, districtMaster.getNoOfDaysRestricted());
										Date restrictedDate = c.getTime();
										Date currTime = Utility.getDateWithoutTime();
										isJobNotAllowed = Utility.between(currTime,grugTestFailDate, restrictedDate);
										System.out.println("allowed: "+!isJobNotAllowed);
									}
								}

								String FECode=employeeMaster.getFECode()==null?"":employeeMaster.getFECode().trim();
								if(FECode.equalsIgnoreCase("3"))
									FECode="1";

								if(isJobNotAllowed) //drug failed
								{
									retArray[0]=0;
									retArray[1]=2;// restricted
								}
								else if(employeeType.equalsIgnoreCase("1"))// current employee
								{
									System.out.println("getIsCurrentFullTimeTeacherL:: "+teacherPersonalInfo.getIsCurrentFullTimeTeacher());
									//String isFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?"N":(teacherPersonalInfo.getIsCurrentFullTimeTeacher()==1?"I":"N");
									String isFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?"N":((teacherPersonalInfo.getIsCurrentFullTimeTeacher()==1 || teacherPersonalInfo.getIsCurrentFullTimeTeacher()==2)?"I":"N");
									String staffType=employeeMaster.getStaffType()==null?"":employeeMaster.getStaffType().trim();
									System.out.println("{{{{{{{{{{{{ "+isFullTime+" "+staffType);
									//Integer isCurrentFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?0:teacherPersonalInfo.getIsCurrentFullTimeTeacher();
									//if(employeeType.toString().equals(FECode) && isFullTime.equalsIgnoreCase("I") && staffType.equalsIgnoreCase("I"))
									if(employeeType.toString().equals(FECode) && isFullTime.equalsIgnoreCase(staffType))
									{
										System.out.println("------------------------equal-----------1");
									}else
									{
										retArray[0]=0;
										retArray[1]=1;
									}

								}else if(employeeType.equalsIgnoreCase("0") || employeeType.equalsIgnoreCase("2"))
								{
									if(employeeType.toString().equals(FECode))
									{
										System.out.println("---FE--------------------equal-----------2");
									}else
									{
										//return false;
										retArray[0]=0;
										retArray[1]=1;
										//return retArray;
									}
								}
							}else{
								if(employeeType.equalsIgnoreCase("1"))
								{
									String isFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?"N":((teacherPersonalInfo.getIsCurrentFullTimeTeacher()==1 || teacherPersonalInfo.getIsCurrentFullTimeTeacher()==2)?"I":"N");
									Integer isCurrentFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?0:teacherPersonalInfo.getIsCurrentFullTimeTeacher();
									if(isFullTime.equalsIgnoreCase("I") && isCurrentFullTime==1)
									{
										if(!jobCategoryName.equals("Aspiring Assistant Principal")) // new change
										{
											retArray[0]=0;
											retArray[1]=0;
										}
									}
								}
							}
						}
					}
				}
				else
				{
					/*teacherPersonalInfo.setEmployeeType(empTypeExisting);
					teacherPersonalInfo.setIsCurrentFullTimeTeacher(fullTimeExisting);
					teacherPersonalInfo.setEmployeeNumber(empNoExisting); 
					teacherPersonalInfo.setSSN(ssnExisting);*/
				}



				if(dob_converted!=null)
					teacherPersonalInfo.setDob(dob_converted);

				try
				{
					//----------Start:: Country, State, City---------
					if(countryId!=null && !countryId.equals(""))
					{
						CountryMaster countryMaster2 = countryMasterDAO.findById(Integer.parseInt(countryId), false, false);
						List<StateMaster> lstState = stateMasterDAO.findActiveStateByCountryId(countryMaster2);
						StateMaster stateMaster2 = new StateMaster();
						CityMaster cityMaster2 = new CityMaster();

						if(stateId!=null && !stateId.equals("") && (cityId!=null && !cityId.equals("")))
						{
							if(Integer.parseInt(countryId)==223)
							{
								stateMaster2 = stateMasterDAO.findById(Long.parseLong(stateId), false, false);
								cityMaster2 = cityMasterDAO.findById(Long.parseLong(cityId), false, false);

								teacherPersonalInfo.setStateId(stateMaster2);
								teacherPersonalInfo.setCityId(cityMaster2);

								teacherPersonalInfo.setOtherState(null);
								teacherPersonalInfo.setOtherCity(null);
							}
							else
							{
								if(lstState!=null && lstState.size()>0)
								{
									stateMaster2 = stateMasterDAO.findById(Long.parseLong(stateId), false, false);

									teacherPersonalInfo.setStateId(stateMaster2);
									teacherPersonalInfo.setOtherCity(cityId);

									teacherPersonalInfo.setOtherState(null);
									teacherPersonalInfo.setCityId(null);
								}
								else
								{
									teacherPersonalInfo.setOtherState(stateId);
									teacherPersonalInfo.setOtherCity(cityId);

									teacherPersonalInfo.setStateId(null);
									teacherPersonalInfo.setCityId(null);
								}
							}	
						}

						if((stateId!=null && !stateId.equals("")) && (cityId!=null && !cityId.equals("")))
						{
							countryMaster.setCountryId(Integer.parseInt(countryId)); 
							teacherPersonalInfo.setCountryId(countryMaster);
						}

					}
					//---------- End---------

				}
				catch(Exception e)
				{
					e.printStackTrace();
				}


				if(address1!=null && !address1.equals(""))
					teacherPersonalInfo.setAddressLine1(address1);

				if(address2!=null && !address2.equals(""))
					teacherPersonalInfo.setAddressLine2(address2);

				if(zipCode!=null && !zipCode.equals(""))
					teacherPersonalInfo.setZipCode(zipCode);

				if(veteranValue!=null && !veteranValue.equals(""))
					teacherPersonalInfo.setIsVateran(veteranValue);

				if(teacherPersonalInfo.getTeacherId()==null)
				{
					teacherPersonalInfo.setTeacherId(teacherDetail.getTeacherId());
					teacherPersonalInfo.setCreatedDateTime(new Date());
					teacherPersonalInfo.setIsDone(false);
				}


				if(raceValue!=null)
					if(!raceValue.equals(""))
					{
						//RaceMaster raceMaster=raceMasterDAO.findById(raceValue, false, false);
						teacherPersonalInfo.setRaceId(raceValue);
					}

				if(genderValue!=null)
					if(genderValue!=-1)
					{
						GenderMaster genderMaster = genderMasterDAO.findById(genderValue, false, false);
						teacherPersonalInfo.setGenderId(genderMaster);
					}

				if(ethnicOriginValue!=null)
					if(ethnicOriginValue!=-1)
					{
						EthnicOriginMaster ethnicOriginMaster = ethnicOriginMasterDAO.findById(ethnicOriginValue, false, false);
						teacherPersonalInfo.setEthnicOriginId(ethnicOriginMaster);
					}

				if(ethinicityValue!=null)
					if(ethinicityValue!=-1)
					{
						EthinicityMaster ethinicityMaster = ethinicityMasterDAO.findById(ethinicityValue, false, false);
						teacherPersonalInfo.setEthnicityId(ethinicityMaster);
					}

				try
				{
					System.out.println(" ******** retire no :: "+retireNo);
					if(retireNo!=null && !retireNo.equals(""))
					{
						teacherPersonalInfo.setRetirementnumber(retireNo);
					}
					if(distForRetire!=null && !distForRetire.equals(""))
					{
						DistrictMaster distRetired = districtMasterDAO.findById(Integer.parseInt(distForRetire), false, false);
						teacherPersonalInfo.setDistrictmaster(distRetired);
					}
					if(stMForretire!=null && !stMForretire.equals(""))
					{
						StateMaster stReited = stateMasterDAO.findById(Long.parseLong(stMForretire), false, false);
						teacherPersonalInfo.setStateMaster(stReited);
					}


				}catch(Exception e)
				{
					e.printStackTrace();
				}
				JobOrder jobOrder=null;
				System.out.println("teacherPersonalInfO:::::Savepp");				
				teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
				teacherDetailDAO.makePersistent(teacherDetail);
				if(teacherPersonalInfo!=null){
					if(teacherPersonalInfo.getEmployeeType()!=null){
						int iIsAffilated=0;
						int iEmployeeType=teacherPersonalInfo.getEmployeeType();
						if(iEmployeeType==1)
							iIsAffilated=1;
						if(jobId!=null && !jobId.equalsIgnoreCase("")){
							
							int iJobId=0;
							try { iJobId=Utility.getIntValue(jobId); } catch (Exception e) {}
							try { jobOrder=jobOrderDAO.findById(iJobId, false, false); } catch (Exception e) {}

							if(jobOrder!=null && teacherDetail!=null){
								JobForTeacher forTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
								if(forTeacher!=null){
									System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@15");
									forTeacher.setIsAffilated(iIsAffilated);
									try { jobForTeacherDAO.updatePersistent(forTeacher); } catch (Exception e) { e.printStackTrace();}
								}
							}
						}
					}
				}

				//------------------Testing For Flag-------------------------------

				System.out.println(" >>>>>>>>>>>>> affidavit >>>>>>>>>>>>>>  "+affidavit);

				try{


					if(affidavit==true)
					{
						TeacherAffidavit teacherAffidavit = teacherAffidavitDAO.findAffidavitByTeacher(teacherDetail);

						if(teacherAffidavit==null)
						{
							teacherAffidavit = new TeacherAffidavit();
							teacherAffidavit.setTeacherId(teacherDetail);
							teacherAffidavit.setCreatedDateTime(new Date());
						}

						teacherAffidavit.setAffidavitAccepted(true);
						teacherAffidavit.setIsDone(true);
						teacherAffidavitDAO.makePersistent(teacherAffidavit);														
					}

					DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForDSPQ, false, false);
					updateCredentialsFlag(candidateType,districtMaster,jobOrder);

				}catch(Exception e)
				{
					e.printStackTrace();
				}
				TeacherPortfolioStatus tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				int isPortfolio = 0;
				if(tPortfolioStatus!=null)
				{
					if((teacherDetail.getIsPortfolioNeeded()&& tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted()))
					{
						isPortfolio = 1;
					}
				}
				System.out.println("]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]] isPortfolio::: "+isPortfolio);
				System.out.println("]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]] portfolioStatus::: "+portfolioStatus);
				if(retArray[0]==1 && portfolioStatus!=null && portfolioStatus.equalsIgnoreCase("false"))
				{
					if(jobId!=null && !jobId.equalsIgnoreCase("")){
					//portfolioStatus,String jobId
					System.out.println("============================= inside ");
					retArray[1]=isPortfolio;
					String[] resp = new String[9];
					resp = dashboardAjax.getTeacherCriteria(jobId);
					String contnw = resp[3];
					System.out.println("555555555 contnw:::: "+contnw);
					if(contnw.equals("''"))
						retArray[2]=0;
					else if(contnw.equals("epi"))
						retArray[2]=1;
					else if(contnw.equals("jsi"))
						retArray[2]=2;
					else if(contnw.equals("done"))
						retArray[2]=3;
				}
				}
				//-----------------------------------------------------------------

			}
		} 
		catch (Exception e)	{
			e.printStackTrace();
			return retArray;
		}	
		System.out.println(retArray[0]+" retArray[0] : retArray[1]:"+retArray[1]);
		return retArray;
	}
	
	public String updateTeacherEmpAndZip(String empNum,String zipCode,String ssn,String dob,int isAffilated){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		try {
			
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
			
			if(teacherPersonalInfo==null){
				teacherPersonalInfo = new TeacherPersonalInfo();
				teacherPersonalInfo.setIsDone(false);
				teacherPersonalInfo.setCreatedDateTime(new Date());	
			}
				teacherPersonalInfo.setTeacherId(teacherDetail.getTeacherId());
				teacherPersonalInfo.setFirstName(teacherDetail.getFirstName());
				teacherPersonalInfo.setLastName(teacherDetail.getLastName());
					
				
				teacherPersonalInfo.setEmployeeNumber(empNum);
				teacherPersonalInfo.setZipCode(zipCode);
				teacherPersonalInfo.setEmployeeType(isAffilated);
				teacherPersonalInfo.setSSN(Utility.encodeInBase64(ssn));
				teacherPersonalInfo.setDob(new SimpleDateFormat("MMddyyyy").parse(dob));
				teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
					
		} catch (Exception e) {}
		return "";
	}	

	public String getResidencyGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{//displayStdTchrExp		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		StringBuffer sb = new StringBuffer();		
		try 
		{
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------
			System.out.println(start+" start "+end);
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			List<TeacherResidencyDetails> listTeacherExp= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			
			listTeacherExp = teacherResidencyDetailsDAO.findByCriteria(sortOrderStrVal,criterion1);		

			List<TeacherResidencyDetails> sortedTeacherRole		=	new ArrayList<TeacherResidencyDetails>();

			SortedMap<String,TeacherResidencyDetails>	sortedMap = new TreeMap<String,TeacherResidencyDetails>();
			int mapFlag=2;
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherRole.add((TeacherResidencyDetails) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherRole.add((TeacherResidencyDetails) sortedMap.get(key));
				}
			}else{
				sortedTeacherRole=listTeacherExp;
			}

			totalRecord =listTeacherExp.size();
			if(totalRecord<end)
				end=totalRecord;
			List<TeacherResidencyDetails> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);

			String responseText="";
			sb.append("<table border='0' id='tchrResidencytblGrid' class='table table-striped'>");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingMLink("Street&nbsp;Address",sortOrderFieldName,"residencyStreetAddress",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLink("City",sortOrderFieldName,"residencyCity",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink("State",sortOrderFieldName,"residencyState",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink("Zip",sortOrderFieldName,"residencyZip",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingMLink("Coutry",sortOrderFieldName,"residencyCountry",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			//responseText=PaginationAndSorting.responseSortingMLink("Dates",sortOrderFieldName,"residencyCountry",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>Dates</th>");

			sb.append("<th  width='20%' valign='top'>");
			sb.append("Actions");
			sb.append("</th>");	

			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherExp!=null)
				
				for(TeacherResidencyDetails pojo:listsortedTeacherRole)
				{
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(pojo.getResidencyStreetAddress());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(pojo.getResidencyCity());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(pojo.getResidencyState());
					sb.append("</td>");
					sb.append("<td>");
					sb.append(pojo.getResidencyZip());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(pojo.getResidencyCountry());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(pojo.getResidencyFromDate()+" To "+pojo.getResidencyToDate());
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append("<a href='#' onclick=\"return editFormResidency('"+pojo.getTeacherResidencyId()+"')\" >Edit</a>");
					sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return delResidency('"+pojo.getTeacherResidencyId()+"')\" >Delete</a>");
					sb.append("</td>");
					
					sb.append("</tr>");
				}

			if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append("No Record Found.");
				sb.append("</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"residencyGridPager","952"));
			sb.append("<input type='hidden' id='ttlRecResidency' value='"+listsortedTeacherRole.size()+"'>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}
	
	public String insertOrUpdateResidency(TeacherResidencyDetails teacherResidencyDetails){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		//TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		try {
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			teacherResidencyDetails.setTeacherId(teacherDetail);
			teacherResidencyDetails.setCreatedDateTime(new Date());
			teacherResidencyDetailsDAO.makePersistent(teacherResidencyDetails);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "";
	}
	
	public TeacherResidencyDetails editResidency(Integer teacherResidency){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		//TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		TeacherResidencyDetails teacherResidencyDetails=null;
		try {
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");			
			teacherResidencyDetails = teacherResidencyDetailsDAO.findById(teacherResidency, false, false);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return teacherResidencyDetails;
	}
	
	public String deleteResidency(Integer teacherResidency){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		//TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		String msg="0";
		try {
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			
			TeacherResidencyDetails teacherResidencyDetails= new TeacherResidencyDetails();
			teacherResidencyDetails.setTeacherResidencyId(teacherResidency);
			teacherResidencyDetailsDAO.makeTransient(teacherResidencyDetails);
			msg="1";
		} catch (Exception e) {
		}
		return msg;
	}
	
	public String updateTeacherInfo(Integer teacherid, Integer candidateType,String f_name,String l_name, String location,String position ,Date empaproretdatefe ,String nameWhileEmployed,String districtid){
		WebContext context;
		context = WebContextFactory.get();
		if(context!=null){
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		}
		//TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		String msg="0";
		try {
				//TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			Criterion teacheridCreitria=Restrictions.eq("teacherId", teacherid);
			List<TeacherPersonalInfo> teacherpersonalInfo =teacherPersonalInfoDAO.findByCriteria(teacheridCreitria);
				
			if(teacherpersonalInfo!=null && teacherpersonalInfo.size() > 0 && teacherpersonalInfo.get(0)!=null){
				TeacherPersonalInfo tperinfo=teacherpersonalInfo.get(0);
				
				if(!location.equalsIgnoreCase("")){
					tperinfo.setLocation(location);
				}
				if(!position.equalsIgnoreCase("")){
					tperinfo.setPosition(position);
				}
				
				tperinfo.setEmployeeType(candidateType);
				if(empaproretdatefe!=null)
				tperinfo.setRetirementdatePERA(empaproretdatefe);
				teacherPersonalInfoDAO.updatePersistent(tperinfo);
				}
			
			DistrictWiseCandidateName disCandidateName =null;
			
			try{
				
				disCandidateName=districtWiseCandidateNameDAO.findById(teacherid, false, false);
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			if(disCandidateName!=null){
				if(!nameWhileEmployed.equalsIgnoreCase("")){
					disCandidateName.setNameWhileEmployed(nameWhileEmployed);
				}
				districtWiseCandidateNameDAO.updatePersistent(disCandidateName);
			}else{
				TeacherDetail tdetail=teacherDetailDAO.findById(teacherid, false, false);
				DistrictMaster districtMaster=districtMasterDAO.findById(Integer.parseInt(districtid), false, false);
				DistrictWiseCandidateName candidateName=new DistrictWiseCandidateName();
				if(!nameWhileEmployed.equalsIgnoreCase("")){
					candidateName.setNameWhileEmployed(nameWhileEmployed);
				}
				candidateName.setDistrictId(districtMaster);
				candidateName.setTeacherId(tdetail);
				candidateName.setStatus("A");
				candidateName.setCreatedDateTime(new Date());
				try{
				districtWiseCandidateNameDAO.makePersistent(candidateName);
				}catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
		} catch (Exception e) {
		}
		return msg;
	}
	
	public String updateTeacherFormerEmpInfo(Integer teacherId, String empPos,String districtEmail,Integer isAffilated){
		WebContext context;
		TeacherDetail teacherDetail = null;
		context = WebContextFactory.get();
		if(context!=null)
		{
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException("Your session has expired!");
			}
			
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			teacherId = teacherDetail.getTeacherId();
		}
		if(teacherId!=null)			
		try {
			
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherId, false, false);
			
			if(teacherPersonalInfo!=null){
				if(empPos!="")
					teacherPersonalInfo.setLastPositionWhenEmployed(empPos);
				if(districtEmail!="")
					teacherPersonalInfo.setDistrictEmail(districtEmail);
				teacherPersonalInfo.setEmployeeType(isAffilated);
				teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/*********************************************************
		Current Employee Of Martin District By Dhananjay Verma
	*************************************************************/
	
	public String updateTeacherCurrentEmployee(Integer districtId,Integer teacherId, String jotTitle,String location,String yearInLocation,String supervisor, Integer isAffilated){
		WebContext context;
		TeacherDetail teacherDetail = null;
		
		context = WebContextFactory.get();
		if(context!=null)
		{
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException("Your session has expired!");
			}
			
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			teacherId = teacherDetail.getTeacherId();
		}
		if(teacherId!=null)			
		try {
			
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherId, false, false);
			
			if(teacherPersonalInfo!=null){
				
				teacherPersonalInfo.setEmployeeType(isAffilated);
				
				//Teacher Information Update
				teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
				//End
				
				
				DistrictWiseCandidateName disCandidateName =null;
				
				try{
					TeacherDetail teacherDetailId = null;
					teacherDetailId = teacherDetailDAO.findById(teacherId, false, false);
					
					DistrictMaster districtmasterId = null; 
					districtmasterId =  districtMasterDAO.findById(districtId, false, false);
					
					disCandidateName = districtWiseCandidateNameDAO.getDistrictWiseCandidateNameByDistrict(districtmasterId,teacherDetailId);
					
					
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
				if(disCandidateName!=null){
					
					if(jotTitle!="")
						disCandidateName.setCurrentempjobtitle(jotTitle);
						
					if(location!="")
						disCandidateName.setCurrentemplocation(location);
					
					if(yearInLocation!="")
						disCandidateName.setCurrentempyearinlocation(yearInLocation);
					
					if(supervisor!="")
						disCandidateName.setCurrentempsupervisor(supervisor);
					
					if(isAffilated!=null)
						disCandidateName.setEmployeeType(isAffilated);
					
					//Update District Wise Candidate Info If Record Is Found.
					districtWiseCandidateNameDAO.updatePersistent(disCandidateName);
					
				}else{
					TeacherDetail tdetail=teacherDetailDAO.findById(teacherId, false, false);
					
					DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
					
					DistrictWiseCandidateName candidateName=new DistrictWiseCandidateName();
					
					if(jotTitle!="")
						candidateName.setCurrentempjobtitle(jotTitle);
						
					if(location!="")
						candidateName.setCurrentemplocation(location);
					
					if(yearInLocation!="")
						candidateName.setCurrentempyearinlocation(yearInLocation);
					
					if(supervisor!="")
						candidateName.setCurrentempsupervisor(supervisor);
					
					if(isAffilated!=null)
						candidateName.setEmployeeType(isAffilated);
					
					candidateName.setDistrictId(districtMaster);
					candidateName.setTeacherId(tdetail);
					candidateName.setStatus("A");
					candidateName.setCreatedDateTime(new Date());
					try{
						
					////Save District Wise Candidate Info., If Record Is Not Found.
					districtWiseCandidateNameDAO.makePersistent(candidateName);
					}catch(Exception e)
					{
						e.printStackTrace();
					}
				}
				
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	/*********************************************************
	Formal Employee Of Martin District By Dhananjay Verma
*************************************************************/

public String updateTeacherFormalEmployee(Integer districtId,Integer teacherId, String jotTitle,Date dateOfEmployment,String nameWhileEmployed, Integer isAffilated){
	
	
	
	WebContext context;
	TeacherDetail teacherDetail = null;
	context = WebContextFactory.get();
	if(context!=null)
	{
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		teacherId = teacherDetail.getTeacherId();
		
	}
	if(teacherId!=null)			
	try {
		
		
		
		
		TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherId, false, false);
		
		if(teacherPersonalInfo!=null){
			
			
			teacherPersonalInfo.setEmployeeType(isAffilated);
			
			//Teacher Information Update
			teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
			//End
			
			DistrictWiseCandidateName disCandidateName =null;
			
			try{
				
				TeacherDetail teacherDetailId = null;
				teacherDetailId = teacherDetailDAO.findById(teacherId, false, false);
				
				DistrictMaster districtmasterId = null; 
				districtmasterId =  districtMasterDAO.findById(districtId, false, false);
				
				disCandidateName = districtWiseCandidateNameDAO.getDistrictWiseCandidateNameByDistrict(districtmasterId,teacherDetailId);
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			if(disCandidateName!=null){
				
				if(jotTitle!="")
					disCandidateName.setFormalempjobtitle(jotTitle);
					
				if(dateOfEmployment!=null)
					disCandidateName.setDatesofEmployment(dateOfEmployment);
				
				if(nameWhileEmployed!="")
					disCandidateName.setNameWhileEmployed(nameWhileEmployed);
				if(isAffilated!=null)
					disCandidateName.setEmployeeType(isAffilated);
				
				//Update District Wise Candidate Info If Record Is Found.
				districtWiseCandidateNameDAO.updatePersistent(disCandidateName);
				
			}else{
				TeacherDetail tdetail=teacherDetailDAO.findById(teacherId, false, false);
				
				DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
				
				DistrictWiseCandidateName candidateName=new DistrictWiseCandidateName();
				
				if(jotTitle!="")
					candidateName.setFormalempjobtitle(jotTitle);
				
				if(dateOfEmployment!=null)
					candidateName.setDatesofEmployment(Utility.convertDateAndTimeToDbFormat(dateOfEmployment));
				
				if(nameWhileEmployed!="")
					candidateName.setNameWhileEmployed(nameWhileEmployed);
				
				if(isAffilated!=null)
					candidateName.setEmployeeType(isAffilated);
				
				candidateName.setDistrictId(districtMaster);
				candidateName.setTeacherId(tdetail);
				candidateName.setStatus("A");
				candidateName.setCreatedDateTime(new Date());
				try{
					
				////Save District Wise Candidate Info., If Record Is Not Found.
				districtWiseCandidateNameDAO.makePersistent(candidateName);
				}catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
			
			
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	return "";
}


/*********************************************************
Retired Employee Of Martin District By Dhananjay Verma
*************************************************************/

public String updateTeacherRetiredEmployee(Integer districtId,Integer teacherId, Date DateofRetirement, Integer isAffilated){
	WebContext context;
	TeacherDetail teacherDetail = null;
	context = WebContextFactory.get();
	if(context!=null)
	{
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		teacherId = teacherDetail.getTeacherId();
		
	}
	if(teacherId!=null)			
	try {
		
		TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherId, false, false);
		
		if(teacherPersonalInfo!=null){
			
			teacherPersonalInfo.setEmployeeType(isAffilated);
			
			//Teacher Information Update
			teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
			//End
			
			DistrictWiseCandidateName disCandidateName =null;
			
			try{
				
				TeacherDetail teacherDetailId = null;
				teacherDetailId = teacherDetailDAO.findById(teacherId, false, false);
				
				DistrictMaster districtmasterId = null; 
				districtmasterId =  districtMasterDAO.findById(districtId, false, false);
				
				disCandidateName = districtWiseCandidateNameDAO.getDistrictWiseCandidateNameByDistrict(districtmasterId,teacherDetailId);
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			if(disCandidateName!=null){
				
				if(DateofRetirement!=null)
					disCandidateName.setDateofRetirement(Utility.convertDateAndTimeToDbFormat(DateofRetirement));
				
				if(isAffilated!=null)
					disCandidateName.setEmployeeType(isAffilated);
				
				//Update District Wise Candidate Info If Record Is Found.
				districtWiseCandidateNameDAO.updatePersistent(disCandidateName);
				
			}else{
				TeacherDetail tdetail=teacherDetailDAO.findById(teacherId, false, false);
				
				DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
				
				DistrictWiseCandidateName candidateName=new DistrictWiseCandidateName();
				
				if(DateofRetirement!=null)
				{
					candidateName.setDateofRetirement(Utility.convertDateAndTimeToDbFormat(DateofRetirement));
					
				}
				if(isAffilated!=null)
					candidateName.setEmployeeType(isAffilated);
				
				candidateName.setDistrictId(districtMaster);
				candidateName.setTeacherId(tdetail);
				candidateName.setStatus("A");
				candidateName.setCreatedDateTime(new Date());
				try{
					
				////Save District Wise Candidate Info., If Record Is Not Found.
				districtWiseCandidateNameDAO.makePersistent(candidateName);
				}catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
			
			
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	return "";
}


/*********************************************************
Current Substitute Employee Of Martin District By Dhananjay Verma
*************************************************************/

public String updateTeacherCurrentSubstituteEmployee(Integer districtId,Integer teacherId, String jotTitle, Integer isAffilated){
	WebContext context;
	TeacherDetail teacherDetail = null;
	context = WebContextFactory.get();
	if(context!=null)
	{
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		
		teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		teacherId = teacherDetail.getTeacherId();
		
	}
	if(teacherId!=null)			
	try {
		
		TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherId, false, false);
		
		if(teacherPersonalInfo!=null){
			
			teacherPersonalInfo.setEmployeeType(isAffilated);
			
			//Teacher Information Update
			teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
			//End
			
			
			DistrictWiseCandidateName disCandidateName =null;
			
			try{
				
				TeacherDetail teacherDetailId = null;
				teacherDetailId = teacherDetailDAO.findById(teacherId, false, false);
				
				DistrictMaster districtmasterId = null; 
				districtmasterId =  districtMasterDAO.findById(districtId, false, false);
				
				disCandidateName = districtWiseCandidateNameDAO.getDistrictWiseCandidateNameByDistrict(districtmasterId,teacherDetailId);
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			if(disCandidateName!=null){
				
				if(jotTitle!="")
					disCandidateName.setCurrentsubstitutejobtitle(jotTitle);
				
				if(isAffilated!=null)
					disCandidateName.setEmployeeType(isAffilated);
				
				//Update District Wise Candidate Info If Record Is Found.
				districtWiseCandidateNameDAO.updatePersistent(disCandidateName);
				
			}else{
				TeacherDetail tdetail=teacherDetailDAO.findById(teacherId, false, false);
				
				DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
				
				DistrictWiseCandidateName candidateName=new DistrictWiseCandidateName();
				
				if(jotTitle!="")
					candidateName.setCurrentsubstitutejobtitle(jotTitle);
				
				if(isAffilated!=null)
					candidateName.setEmployeeType(isAffilated);
				
				candidateName.setDistrictId(districtMaster);
				candidateName.setTeacherId(tdetail);
				candidateName.setStatus("A");
				candidateName.setCreatedDateTime(new Date());
				try{
					
				////Save District Wise Candidate Info., If Record Is Not Found.
				districtWiseCandidateNameDAO.makePersistent(candidateName);
				}catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	return "";

}

}


