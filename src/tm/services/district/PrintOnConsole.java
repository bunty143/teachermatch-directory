package tm.services.district;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PrintOnConsole 
{
	/**
	 * 
	 * @param strInput
	 * @author Ramesh Kumar Bhartiya
	 * Date Jun 24, 2014
	 */
	public static void debugPrintln(String strInput)
	{
		debugPrintln("",strInput);
	}
	
	/**
	 * 
	 * @param strModuleName e.g Login, DashBoard
	 * @param strInput 
	 * @author Ramesh Kumar Bhartiya
	 * Date Jun 24, 2014
	 */
	public static void debugPrintln(String strModuleName,String strInput)
	{
		String sPrintLine="";
		sPrintLine="["+getDateForConsole()+"]";
		if(strModuleName!=null && !strModuleName.equals(""))
			sPrintLine=sPrintLine+"[M/S:"+strModuleName+"]";
		
		if(strInput!=null && !strInput.equals(""))
			sPrintLine=sPrintLine+"::"+strInput;
		
		System.out.println(sPrintLine);
	}
	
	public static String getDateForConsole()
	{
		try {
			SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
			return sdf.format(new Date());
		} catch (Exception e) {
			return "";
		}
	}
	
	
	public static void debugPrintlnCGMass(String strInput)
	{
		String sPrintLine="";
		sPrintLine="["+getDateForConsole()+"]";
		sPrintLine=sPrintLine+"[M/S:CGMass]";
		if(strInput!=null && !strInput.equals(""))
			sPrintLine=sPrintLine+"::"+strInput;
		System.out.println(sPrintLine);
	}
	
	
	public static void debugPrintRCNFS(String strInput)
	{
		String sPrintLine="";
		sPrintLine="["+getDateForConsole()+"]";
		sPrintLine=sPrintLine+"[RC NFS]";
		if(strInput!=null && !strInput.equals(""))
			sPrintLine=sPrintLine+"::"+strInput;
		System.out.println(sPrintLine);
		
	}
	
	public static void debugPrintJOI(String strInput)
	{
		String sPrintLine="";
		sPrintLine="["+getDateForConsole()+"]";
		sPrintLine=sPrintLine+"[JOI]";
		if(strInput!=null && !strInput.equals(""))
			sPrintLine=sPrintLine+"::"+strInput;
		System.out.println(sPrintLine);
		
	}
	
	
	public static void getJFTPrint(String strInput)
	{
		String sPrintLine="";
		sPrintLine="["+getDateForConsole()+"]";
		sPrintLine=sPrintLine+"[JobForTeacher]";
		if(strInput!=null && !strInput.equals(""))
			sPrintLine=sPrintLine+"::"+strInput;
		System.out.println(sPrintLine);
		
	}
}
