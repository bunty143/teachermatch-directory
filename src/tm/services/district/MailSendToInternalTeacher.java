package tm.services.district;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.InternalMailSend;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.utility.Utility;

public class MailSendToInternalTeacher  extends Thread{
	
    String locale = Utility.getValueOfPropByKey("locale");
    
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	List<InternalMailSend> internalMailSendTrdVal=new ArrayList<InternalMailSend>();
	public MailSendToInternalTeacher(List<InternalMailSend> internalMailSendList){
		internalMailSendTrdVal=internalMailSendList;
	}
	public void run() {
		try {
			Thread.sleep(1000);
			System.out.println("::Trd size>>>::"+internalMailSendTrdVal.size());
			for(InternalMailSend internalMailSend :internalMailSendTrdVal){
				TeacherDetail teacherDetailTrdVal=null;
				String jobTitleTrdVal="";
				String loginURLTrdVal="";
				String[] arrHrDetailTdVal=null;
				String toTrdVal="";
				String subjectTrdVal="";
				JobOrder jobOrder = null;
				
				if(internalMailSend.getTeacherDetail()!=null)
					teacherDetailTrdVal=internalMailSend.getTeacherDetail();
				
				if(internalMailSend.getJobOrder()!=null)
				{
					jobOrder = internalMailSend.getJobOrder();
					jobTitleTrdVal=jobOrder.getJobTitle();
				}
				
				if(internalMailSend.getLoginURL()!=null)
					loginURLTrdVal=internalMailSend.getLoginURL();
				
				if(internalMailSend.getArrHrDetail()!=null)
					arrHrDetailTdVal=internalMailSend.getArrHrDetail();
				
				if(teacherDetailTrdVal!=null){
					toTrdVal=teacherDetailTrdVal.getEmailAddress();	
				}
				String DistrictDName = arrHrDetailTdVal[10];
				subjectTrdVal= Utility.getLocaleValuePropByKey("msgMAilSendToItTeacher1", locale)+" "+jobTitleTrdVal+" "+Utility.getLocaleValuePropByKey("msgpositionatthe", locale)+" "+DistrictDName;
				try{
					emailerService.sendMailAsHTMLText(toTrdVal,subjectTrdVal,MailText.getMailToInternalTeacher(loginURLTrdVal, teacherDetailTrdVal,jobTitleTrdVal, arrHrDetailTdVal));
				}catch(Exception e){}
			}
		}catch (NullPointerException en) {
			en.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
  }
}	
