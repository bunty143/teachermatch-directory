package tm.services.district;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;

/*
*@Author: Ram Nath
*/
public class GlobalServices {
	public static boolean IS_PRINT=false;
	private static final Logger LOG = Logger.getLogger(GlobalServices.class.getName());
	public static String MAILCSS_FONT_SIZE = "'font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'";
	public static String CSS_FONT_SIZE = "'font-family: Century Gothic,Open Sans, sans-serif;font-size: 14px;'";
	public static String MAILFOOTERCSS_FONT_SIZE = "'font-family: Century Gothic,Open Sans, sans-serif;font-size: 12px;'";	
	public static SimpleDateFormat DATE_FORMAT =new SimpleDateFormat("MMMM dd, yyyy");	
	public static SimpleDateFormat DATE_TIME_FORMAT =new SimpleDateFormat("MMMM dd, yyyy hh:mm:ss");
	public static String CURRENT_DATE=DATE_FORMAT.format(new Date());	
	public static Map<String,Boolean> OFFER_READY_MAP=new LinkedHashMap<String,Boolean>();
	public static Map<String,Boolean> NOTIFICATION_MAP=new LinkedHashMap<String,Boolean>();
	public static Map<String,Boolean> CHECK_BEFORE_GREEN_MAP=new LinkedHashMap<String,Boolean>();
	public static Map<String,Boolean> IS_ALWAYS_REQUIRED_SCH_MAP=new LinkedHashMap<String,Boolean>();
	public static Map<String,Boolean> IS_ALREADY_OFFERED=new LinkedHashMap<String,Boolean>();
	public static Map<String,Boolean> IS_ALREADY_OFFERED_MSG=new LinkedHashMap<String,Boolean>();
	public static Map<String,Boolean> SCH_INPUTBOX_HIDE=new LinkedHashMap<String,Boolean>();
	public static Map<String,Boolean> CG_REPORT_SHOW_DECLINED=new LinkedHashMap<String,Boolean>();
	public static Map<String,Boolean> IS_ALREADY_OFFERMADE_ONLYONE_IN_SAMEJOB=new LinkedHashMap<String,Boolean>();
	public static Map<Integer,String> AFFIDAVIT_MASTER=new LinkedHashMap<Integer,String>();//HeadquaterId,Content
	public static Map<Integer,String> AFFIDAVIT_MASTER_HEADING=new LinkedHashMap<Integer,String>();//HeadquaterId,heading
	
	static{
		//    5304860   1200390
		/**********************specify which user send conditional offer**********************/
		OFFER_READY_MAP.put("Head##5##Restart Workflow", true);
		OFFER_READY_MAP.put("Head##6##Restart Workflow", true);
		OFFER_READY_MAP.put("5304860##2##HR Review/Offer", true);
		OFFER_READY_MAP.put("4218990##2##Evaluation Complete", true);
		OFFER_READY_MAP.put("4218990##3##Evaluation Complete", true);
		OFFER_READY_MAP.put("1201470##2##Offer Made", true);
		OFFER_READY_MAP.put("1201470##3##Offer Made", true);
		OFFER_READY_MAP.put("806900##2##Recommend for Hire", true);
		OFFER_READY_MAP.put("806900##3##Recommend for Hire", true);
		
		OFFER_READY_MAP.put("806810##2##Preliminary Offer", true);
		OFFER_READY_MAP.put("806810##3##Preliminary Offer", true);
		OFFER_READY_MAP.put("804800##2##Offer Made", true);
		OFFER_READY_MAP.put("804800##3##Offer Made", true);
		
		OFFER_READY_MAP.put("806810##2##Preliminary Offer", true);
		OFFER_READY_MAP.put("806810##3##Preliminary Offer", true);
		
		NOTIFICATION_MAP.put("806810##2##Preliminary Offer", true);
		NOTIFICATION_MAP.put("806810##3##Preliminary Offer", true);
		NOTIFICATION_MAP.put("806900##2##HR Review Successful", true);
		NOTIFICATION_MAP.put("806900##3##HR Review Successful", true);
		NOTIFICATION_MAP.put("806900##2##HR Approval", true);
		NOTIFICATION_MAP.put("806900##3##HR Approval", true);
		OFFER_READY_MAP.put("1201290##2##Offer Extended", true);
		OFFER_READY_MAP.put("1201290##3##Offer Extended", true);
		/*if(Utility.getValueOfPropByKey("basePath").contains("platform")){
			//***********************Notification Send map*********************//*
			NOTIFICATION_MAP.put("804800##2##Offer Made", true);
			NOTIFICATION_MAP.put("804800##3##Offer Made", true);
			//************************end notification*********************************//*
		}else{
			OFFER_READY_MAP.put("804800##2##Offer Made", true);
			OFFER_READY_MAP.put("804800##3##Offer Made", true);
		}*/
		/*************************end*********************************/
		
		/************************all previous status green required********************/
		//CHECK_BEFORE_GREEN_MAP.put("5304860##HR Review/Offer", true);
		CHECK_BEFORE_GREEN_MAP.put("4218990##Evaluation Complete", true);
		/************************end*********************************/
		
		/***********************On Teacher already offered Conditional offer *********************/
		IS_ALREADY_OFFERED.put("5304860##HR Review/Offer", true);
		IS_ALREADY_OFFERED.put("4218990##Evaluation Complete", true);
		IS_ALREADY_OFFERED.put("1201470##Offer Made", true);
		//IS_ALREADY_OFFERED.put("804800##Offer Made", true);
		/****************************end*********************************/
		
		/*************************School always required for DA*******************/
		IS_ALWAYS_REQUIRED_SCH_MAP.put("5304860##2##HR Review/Offer", true);
		IS_ALWAYS_REQUIRED_SCH_MAP.put("1201470##2##Offer Made", true);
		/*********************************end*********************************/
		
		/***********************Hide school for DA*********************/
		SCH_INPUTBOX_HIDE.put("1201470##2##Offer Made", true);
		/************************end*********************************/
		
		/***********************School Wise view declined in Report CG DA/SA*********************/
		CG_REPORT_SHOW_DECLINED.put("4218990", true);
		CG_REPORT_SHOW_DECLINED.put("5304860", true);
		CG_REPORT_SHOW_DECLINED.put("1201470", true);
		/************************end*********************************/
		

		/***********************Notification Send map*********************/
		/************************end notification*********************************/
		
		
		/**********************specify which user send conditional offer**********************/
		IS_ALREADY_OFFERMADE_ONLYONE_IN_SAMEJOB.put("804800##Offer Made", true);
		/************************end*********************************/
		
		//print("CHECK_BEFORE_GREEN_MAP=="+CHECK_BEFORE_GREEN_MAP.get("1200390##HR Review/Offer"));
	}
	
	
	@PostConstruct
    private void init() {
     LOG.log(Level.INFO,"GlobalServices Object Created"); 
    }
	
	//***********************************Normal Table Start as a (teachermatch) css *************************************************//
	public synchronized static String getTMTableStart(){
		return new StringBuffer("<table width='700' style="+MAILCSS_FONT_SIZE+" align='center' border='0'>").toString();
	}
	
	public synchronized static String getTMTableStartWithClassAndId(String className, String idName, String css){
		return new StringBuffer("<table class='"+className+"' Id='"+idName+"' style="+css+" align='center' border='0'>").toString();
	}
	
	public synchronized static String getTMTableStartWithClass(String className, String css){
		return new StringBuffer("<table class='"+className+"' style='"+css+"' align='center' border='0'>").toString();
	}
	
	public synchronized static String getTMTableStartWithId(String idName, String css){
		return new StringBuffer("<table Id='"+idName+"' style='"+css+"' align='center' border='0'>").toString();
	}
	
	public synchronized static String getTMEmailIdHtml(String mailToEmailId, String viewEmailId){
		return "<a href='mailto:"+mailToEmailId+"'>"+viewEmailId+"</a>";
	}
	
	static class TableCreateException extends Exception {
			TableCreateException(String s){  
			  super(s);  
			 }  
	}
	
	//***********************************Table as a (teachermatch) css and Id with all DATA*************************************************//
	public synchronized static String getTrHeaderHTML(int totalColumn, String[] header, boolean headerColSpan, String[] content, String tableClassName, String tableIdName,  String thClassName, String thIdName, String tdClassName, String tdIdName)throws TableCreateException {
		StringBuffer sb=new StringBuffer();
		sb.append(getTMTableStartWithClassAndId(tableClassName, tableIdName,""));
		if(totalColumn>=header.length){
		if(headerColSpan){
			if(header.length>0){
				sb.append("<tr><th colspan='"+totalColumn+"'>"+header[0]+"</th></tr>");
			}
		}else{
			if(totalColumn>0 && header.length==totalColumn){
			sb.append("<tr>");
			for(int i=0;i<totalColumn;i++){
				sb.append("<th>"+header[i]+"</th>");
			}
			}
			
		}
		}else{
			throw new TableCreateException("Invalid totalcolumn of table or headerContent");
		}
		if(content.length%totalColumn==0){
		int totalRow=content.length/totalColumn;
		for(int i=0;i<totalRow;i++){
			sb.append("<tr>");
				for(int j=0;i<totalRow;j++){
					sb.append("<td>"+header[j]+"</td>");
				}
			sb.append("</tr>");
		  }
		}else{
			throw new TableCreateException("Invalid totalcolumn or content");
		}
		sb.append(getTMTableEnd());
		return sb.toString();		
	}
	
	
	//***********************************Normal Table End as a (teachermatch) css *************************************************//
	public synchronized static String getTMTableEnd(){
		return new StringBuffer("</table>").toString();
	}
	
	//***********************************Normal tr of Table as a (teachermatch) css *************************************************//
	public synchronized static String getMultipleThOfTrHTML(String[] content){
		StringBuffer sb=new StringBuffer();
		sb.append("<tr>");
		for(int i=0;i<content.length;i++){
			sb.append("<th style="+MAILCSS_FONT_SIZE+">"+content[i]+"</th>");
		}
		sb.append("</tr>");
		return sb.toString();		
	}
	
	//***********************************Normal tr of Table as a (teachermatch) css *************************************************//
	public synchronized static String getMultipleTdOfTrHTML(List<String> content){
		StringBuffer sb=new StringBuffer();
		sb.append("<tr>");
		for(int i=0;i<content.size();i++){
			sb.append("<th style="+MAILCSS_FONT_SIZE+">"+content.get(i)+"</th>");
		}
		sb.append("</tr>");
		return sb.toString();		
	}
	
	//***********************************Normal tr of Table as a (teachermatch) css *************************************************//
	public synchronized static String getMultipleThOfTrWithCssHTML(List<String> content, String css, String idName, String className){
		StringBuffer sb=new StringBuffer();
		String classNameEntry=(className.trim().equalsIgnoreCase(""))?"":"class='"+className+"'";
		String idNameEntry=(idName.trim().equalsIgnoreCase(""))?"":"id='"+idName+"'";
		String cssEntry=(css.trim().equalsIgnoreCase(""))?"":"style='"+css+"'";
		sb.append("<tr>");
		for(int i=0;i<content.size();i++){
			sb.append("<th "+classNameEntry+" "+idNameEntry+" "+cssEntry+">"+content.get(i)+"</th>");
		}
		sb.append("</tr>");
		return sb.toString();		
	}
	
	//***********************************(TH) tr with th of Table as a (teachermatch) css *************************************************//
	public synchronized static String getMultipleTdOfTrWithCssHTML(List<String> content, String className,String idName,  String css){
		StringBuffer sb=new StringBuffer();
		String classNameEntry=(className.trim().equalsIgnoreCase(""))?"":"class='"+className+"'";
		String idNameEntry=(idName.trim().equalsIgnoreCase(""))?"":"id='"+idName+"'";
		String cssEntry=(css.trim().equalsIgnoreCase(""))?"":"style='"+css+"'";
		sb.append("<tr>");
		for(int i=0;i<content.size();i++){
			sb.append("<td "+classNameEntry+" "+idNameEntry+" "+cssEntry+">"+content.get(i)+"</td>");
		}
		sb.append("</tr>");
		return sb.toString();		
	}
	
	//***********************************(TD)  tr with td of Table as a (teachermatch) css *************************************************//
	public synchronized static String getTrHeaderHTML(String content, int howManyBr){
		StringBuffer sb=new StringBuffer();
		sb.append("<tr><th style="+MAILCSS_FONT_SIZE+">");
		sb.append(content);
		for(int i=0;i<howManyBr;i++){sb.append("<br/>");}
		sb.append("</th></tr>");
		return sb.toString();		
	}
	
	//***********************************(TD)  tr with td of Table as a (teachermatch) css *************************************************//
	public synchronized static String getTrHeaderHTML(String content, int colspan, String className, String idName, String css){
		StringBuffer sb=new StringBuffer();
		String colspanEntry=(colspan>0)?"colspan='"+colspan+"'":"";
		String classNameEntry=(className.trim().equalsIgnoreCase(""))?"":"class='"+className+"'";
		String idNameEntry=(idName.trim().equalsIgnoreCase(""))?"":"id='"+idName+"'";
		String cssEntry=(css.trim().equalsIgnoreCase(""))?"":"style='"+css+"'";
		sb.append("<tr><th "+colspanEntry+" "+idNameEntry+" "+classNameEntry+" "+cssEntry+">");
		sb.append(content);
		sb.append("</th></tr>");
		return sb.toString();		
	}
	
	//***********************************(TD)  tr with td of Table as a (teachermatch) css *************************************************//
	public synchronized static String getTdOfTrHTML(String content, int colspan, String className, String idName, String css){
		StringBuffer sb=new StringBuffer();
		String colspanEntry=(colspan>0)?"colspan='"+colspan+"'":"";
		String classNameEntry=(className.trim().equalsIgnoreCase(""))?"":"class='"+className+"'";
		String idNameEntry=(idName.trim().equalsIgnoreCase(""))?"":"id='"+idName+"'";
		String cssEntry=(css.trim().equalsIgnoreCase(""))?"":"style='"+css+"'";
		sb.append("<tr><td "+colspanEntry+" "+idNameEntry+" "+classNameEntry+" "+cssEntry+">");
		sb.append(content);
		sb.append("</td></tr>");
		return sb.toString();		
	}
	
	//***********************************Normal tr of Table as a (teachermatch) css *************************************************//
	public synchronized static String getTrNormalHTML(String content, int howManyBr){
		StringBuffer sb=new StringBuffer();
		sb.append("<tr><td style="+MAILCSS_FONT_SIZE+">");
		sb.append(content);
		for(int i=0;i<howManyBr;i++){sb.append("<br/>");}
		sb.append("</td></tr>");
		return sb.toString();		
	}
	
	//***********************************Footer tr of Table as a (teachermatch) css *************************************************//
	public synchronized static String getTrFooterHTML(String content, int howManyBr){
		StringBuffer sb=new StringBuffer();
		sb.append("<tr><td style="+MAILFOOTERCSS_FONT_SIZE+">");
		sb.append(content);
		for(int i=0;i<howManyBr;i++){sb.append("<br/>");}
		sb.append("</td></tr>");
		return sb.toString();		
	}
	
	//***********************************Create a div Start (teachermatch) *************************************************//
	public synchronized static String getDivStartHTMLWithoutContent(String className, String idName, String css){
		String classNameEntry=(className.trim().equalsIgnoreCase(""))?"":"class='"+className+"'";
		String idNameEntry=(idName.trim().equalsIgnoreCase(""))?"":"id='"+idName+"'";
		String cssEntry=(css.trim().equalsIgnoreCase(""))?"":"style='"+css+"'";
		StringBuffer sb=new StringBuffer();
		sb.append("<div "+idNameEntry+" "+classNameEntry+" "+cssEntry+">");		
		return sb.toString();		
	}
	
	//***********************************Create a div Start (teachermatch) *************************************************//
	public synchronized static String getDivStartHTMLWithContent(String content, String className, String idName, String css){
		String classNameEntry=(className.trim().equalsIgnoreCase(""))?"":"class='"+className+"'";
		String idNameEntry=(idName.trim().equalsIgnoreCase(""))?"":"id='"+idName+"'";
		String cssEntry=(css.trim().equalsIgnoreCase(""))?"":"style='"+css+"'";
		StringBuffer sb=new StringBuffer();
		sb.append("<div "+idNameEntry+" "+classNameEntry+" "+cssEntry+">"+content+"</div>");		
		return sb.toString();		
	}
	
	
	//***********************************Create a div (teachermatch) *************************************************//
	public synchronized static String getDivStartHTML(String content){
		StringBuffer sb=new StringBuffer();
		sb.append("<div>"+content+"</div>");		
		return sb.toString();		
	}
	
	//***********************************Create a div End (teachermatch) *************************************************//
	public synchronized static String getDivEndHTML(){
		return "</div>";		
	}
	
	//***********************************Create a Anchor Tag for open New Page (teachermatch) *************************************************//
	public synchronized static String getAnchorTag(String event, String href, String label){
		return "<a href='"+href+"' "+event+">"+label+"</a>";		
	}
	
	//***********************************Create a Anchor Tag for open New Page (teachermatch) *************************************************//
	public synchronized static String getAnchorTagWithNewPage(String event, String href, String label){
		return "<a target='blank'  href='"+href+"' "+event+">"+label+"</a>";		
	}
	
	
	public synchronized static String getModal(String id, String modalTitle, String buttonValue, String content){
		StringBuffer sb=new StringBuffer();
					sb.append("<div class='modal hide in' id='"+id+"' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true' style='z-index:999999;'>"+
					"<div class='modal-dialog' style='width:550px;'>"+
					"<div class='modal-content'>"+
					"<div class='modal-header'>"+
			        "<button type='button' class='close' aria-label='Close' onclick=\"$('#"+id+"').hide();\"><span aria-hidden='true'>&times;</span></button>"+
			        "<h4 class='modal-title' id='myModalLabel' style='color:white;'>"+modalTitle+"</h4>"+
			      "</div>"+
			      "<div class='modal-body' id='confirmMessage'>"+content+	      
			      "</div>"+
			      "<div class='modal-footer'>"+
			      //"<button type='button' class='btn btn-primary'>"+buttonValue+"</button>"+
			        "<button type='button' class='btn btn-default' id='' onclick=\"$('#"+id+"').hide();\">"+buttonValue+"</button>"+	        
			      "</div>"+
			    "</div>"+
			  "</div>"+
			"</div>");
					//System.out.println("jjjjjjjjj sb===="+sb.toString());
					return sb.toString();
	}
	//***********************************Get Teacher Name from Teacher Detail  (teachermatch) css *************************************************//
	public synchronized static String getTeacherOrUserName(Object userMasterOrTeacherDetail){
		String name="";
		UserMaster userMaster=null;
		TeacherDetail teacherDetail=null;
		String firstName=null;
		String lastName=null;
		try{
			teacherDetail=(TeacherDetail)userMasterOrTeacherDetail;
			firstName=teacherDetail.getFirstName();
			lastName=teacherDetail.getLastName();
		}catch(ClassCastException cle){
			try{
				userMaster=(UserMaster)userMasterOrTeacherDetail;
				firstName=userMaster.getFirstName();
				lastName=userMaster.getLastName();
			}catch(ClassCastException cce){
				cce.printStackTrace();
			}
		}
		
		try{
			if(firstName!=null){
				name=firstName;
			}
			if(firstName!=null && lastName!=null){
				name=firstName+" "+lastName;
			}
		}catch(Exception e){e.printStackTrace(); }
		return name!=null?name:"";
	}
	
	/*  *********************How to call this method*******************************
	 * Author :: Ram Nath
	 * properties message key : offerMadeSubject = Offer Made (Job #brk#- #brk#)
	 * String message=GlobalServices.getConvertedString(Utility.getLocaleValuePropByKey("offerMadeSubject", locale),"#brk#",new String[]{jobOrder.getJobId().toString(),jobOrder.getJobTitle()});
	 */
	public synchronized static String  getConvertedString(String messageString,  String replaceString, String... arrayOfString){
		 String[] arrayMsg= messageString.split(replaceString);
		 StringBuffer sb=new StringBuffer();
		 if((arrayMsg.length-1)<=arrayOfString.length){
			 int count=-1;
			 boolean flag=false;
			 boolean flagcount=true;
			 for(String str:arrayMsg){
				 //String str=strData.trim();
				 if(flagcount && str.trim().equalsIgnoreCase("") && count==-1){
					 flag=true; 
					 flagcount=false;
				 }else if(flagcount){
					 sb.append(str);
					 flag=false; 
					 flagcount=false;
				 }
				 if(flag){
					 if(count==0){
						 sb.append(arrayOfString[count]+str);
					 }else if(count>0)
						 sb.append(arrayOfString[count]+str);
						 count++;
				 }else{
				 if(count==0)
					 sb.append(arrayOfString[count]+str);
				 else if(count>0){
					 sb.append(arrayOfString[count]+str);
				 }
					 count++;
				 }
			 }
		 }else{
			 return "Check your arrayOfString array";
		 }
		 return sb.toString();
	 }
	
	
	public synchronized static void print(String printMessage){
		if(IS_PRINT)
		try{
		System.out.print(printMessage);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	public synchronized static void println(String printMessage){
		if(IS_PRINT)
		try{
			System.out.println(printMessage);
			}catch(Exception ex){
				ex.printStackTrace();
			}
	}
	
	public synchronized static <T> List<T> createList() {
	    return new ArrayList<T>();
	 }
	
	public static String printStackTrace(Throwable e) {
		synchronized(GlobalServices.class){
			Writer writer = new StringWriter();
			PrintWriter printWriter = new PrintWriter(writer);
			e.printStackTrace(printWriter);
			return writer.toString();
		}
	}
	
}
