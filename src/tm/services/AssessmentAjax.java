package tm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentAttempt;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentGroupDetails;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.assessment.AssessmentQuestions;
import tm.bean.assessment.AssessmentSections;
import tm.bean.assessment.QuestionOptions;
import tm.bean.assessment.QuestionsPool;
import tm.bean.assessment.ScoreLookupMaster;
import tm.bean.assessment.StageOneStatus;
import tm.bean.assessment.StageThreeStatus;
import tm.bean.assessment.StageTwoStatus;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.ObjectiveMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.EpiQuestionDetailDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAssessmentAttemptDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentGroupDetailsDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.assessment.AssessmentLogDAO;
import tm.dao.assessment.AssessmentQuestionsDAO;
import tm.dao.assessment.AssessmentSectionDAO;
import tm.dao.assessment.QuestionOptionsDAO;
import tm.dao.assessment.QuestionsPoolDAO;
import tm.dao.assessment.ScoreLookupMasterDAO;
import tm.dao.master.CompetencyMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.ObjectiveMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class AssessmentAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");	
	
	 String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
     String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
     String lblNm=Utility.getLocaleValuePropByKey("lblNm", locale);
     String lblGroupName1=Utility.getLocaleValuePropByKey("lblGroupName1", locale);
     String lblcretedDate=Utility.getLocaleValuePropByKey("lblcretedDate", locale);
     String lblResponses1=Utility.getLocaleValuePropByKey("lblResponses1", locale);
     String lblTpe=Utility.getLocaleValuePropByKey("lblTpe", locale);
     String optNA=Utility.getLocaleValuePropByKey("optNA", locale);
     String lblDecr=Utility.getLocaleValuePropByKey("lblDecr", locale);
     String lblInst=Utility.getLocaleValuePropByKey("lblInst", locale);
     String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
     String lnkV=Utility.getLocaleValuePropByKey("lnkV", locale);
     String lnkDlt=Utility.getLocaleValuePropByKey("lnkDlt", locale);
     String lblDeactivate=Utility.getLocaleValuePropByKey("lblDeactivate", locale);
     String lblActivate=Utility.getLocaleValuePropByKey("lblActivate", locale);
     String lblManageSections1=Utility.getLocaleValuePropByKey("lblManageSections1", locale);
     String msgNoInventoryfound1=Utility.getLocaleValuePropByKey("msgNoInventoryfound1", locale);
     String lblB=Utility.getLocaleValuePropByKey("lblB", locale);
     String optJSpe=Utility.getLocaleValuePropByKey("optJSpe", locale);
     String lblSmartP=Utility.getLocaleValuePropByKey("lblSmartP", locale);
     String lblIPI=Utility.getLocaleValuePropByKey("lblIPI", locale);
     
     String optAct=Utility.getLocaleValuePropByKey("optAct", locale);
     String optInActiv=Utility.getLocaleValuePropByKey("optInActiv", locale);
     String lblManageQuestions=Utility.getLocaleValuePropByKey("lblManageQuestions", locale);
     String headImpQues=Utility.getLocaleValuePropByKey("headImpQues", locale);
     String lblPlay1=Utility.getLocaleValuePropByKey("lblPlay1", locale);
     String msgNoSectionfound1=Utility.getLocaleValuePropByKey("msgNoSectionfound1", locale);
     String lblStageOne1=Utility.getLocaleValuePropByKey("lblStageOne1", locale);
     String lblStageOneNA1=Utility.getLocaleValuePropByKey("lblStageOneNA1", locale);
     String lblStageTwo1=Utility.getLocaleValuePropByKey("lblStageTwo1", locale);
     String lblStageTwoNA1=Utility.getLocaleValuePropByKey("lblStageTwoNA1", locale);
     String lblStageThree1=Utility.getLocaleValuePropByKey("lblStageThree1", locale);
     String lblStageThreeNA1=Utility.getLocaleValuePropByKey("lblStageThreeNA1", locale);
     String lblQuestionWeightage1=Utility.getLocaleValuePropByKey("lblQuestionWeightage1", locale);
     String lblExperimental1=Utility.getLocaleValuePropByKey("lblExperimental1", locale);
     String lblNoQuestionfound1=Utility.getLocaleValuePropByKey("lblNoQuestionfound1", locale);
     String optStrJobCat=Utility.getLocaleValuePropByKey("optStrJobCat", locale);
     
     
     
	 
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	public void setAssessmentDetailDAO(AssessmentDetailDAO assessmentDetailDAO) {
		this.assessmentDetailDAO = assessmentDetailDAO;
	}
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(
			AssessmentJobRelationDAO assessmentJobRelationDAO) {
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}
	@Autowired
	private TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO;
	public void setTeacherAssessmentAttemptDAO(
			TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO) {
		this.teacherAssessmentAttemptDAO = teacherAssessmentAttemptDAO;
	}
	@Autowired
	private AssessmentSectionDAO assessmentSectionDAO;
	public void setAssessmentSectionDAO(AssessmentSectionDAO assessmentSectionDAO) {
		this.assessmentSectionDAO = assessmentSectionDAO;
	}
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) {
		this.domainMasterDAO = domainMasterDAO;
	}

	@Autowired
	private CompetencyMasterDAO competencyMasterDAO;
	public void setCompetencyMasterDAO(CompetencyMasterDAO competencyMasterDAO) {
		this.competencyMasterDAO = competencyMasterDAO;
	}

	@Autowired
	private ObjectiveMasterDAO objectiveMasterDAO;
	public void setObjectiveMasterDAO(ObjectiveMasterDAO objectiveMasterDAO) {
		this.objectiveMasterDAO = objectiveMasterDAO;
	}

	@Autowired
	private QuestionsPoolDAO questionsPoolDAO;
	public void setQuestionsPoolDAO(QuestionsPoolDAO questionsPoolDAO) {
		this.questionsPoolDAO = questionsPoolDAO;
	}

	@Autowired
	private QuestionOptionsDAO questionOptionsDAO;
	public void setQuestionOptionsDAO(QuestionOptionsDAO questionOptionsDAO) {
		this.questionOptionsDAO = questionOptionsDAO;
	}

	@Autowired
	private AssessmentQuestionsDAO assessmentQuestionsDAO;
	public void setAssessmentQuestionsDAO(
			AssessmentQuestionsDAO assessmentQuestionsDAO) {
		this.assessmentQuestionsDAO = assessmentQuestionsDAO;
	}
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	
	@Autowired
	private AssessmentLogDAO assessmentLogDAO;
	public void setAssessmentLogDAO(AssessmentLogDAO assessmentLogDAO) {
		this.assessmentLogDAO = assessmentLogDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	
	@Autowired
	private EpiQuestionDetailDAO epiQuestionDetailDAO;
	public void setEpiQuestionDetailDAO(EpiQuestionDetailDAO epiQuestionDetailDAO) {
		this.epiQuestionDetailDAO = epiQuestionDetailDAO;
	}
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private AssessmentGroupDetailsDAO assessmentGroupDetailsDAO;
	
	@Autowired
	private ScoreLookupMasterDAO scoreLookupMasterDAO;
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get assessment from database and to display on the grid.
	 */
	public String getAssessment(int assessmentType,String status,String districtId, String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		 
		StringBuffer sb =new StringBuffer();
		try{
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------
			

			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,20,"assessment.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}

			List<AssessmentDetail> assessmentDetails = null;
			
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"assessmentName";
			String sortOrderNoField		=	"assessmentName";
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			

			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("Responses")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("Responses")){
					 sortOrderNoField="Responses";
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			if(districtId==null)
				assessmentDetails=new ArrayList<AssessmentDetail>();
			else if(assessmentType==0 && status.equals("") && districtId.equals(""))
			{
				//assessmentDetails = assessmentDetailDAO.findByCriteria(Order.desc("createdDateTime"));
				assessmentDetails = assessmentDetailDAO.findByCriteria(sortOrderStrVal);				
			
			}
			else 
			{
				Criterion criterion = Restrictions.eq("status", status);
				Criterion criterion1 = Restrictions.eq("assessmentType", assessmentType);
				Criterion criterion2 =null;
				if(!districtId.equals(""))
				criterion2=Restrictions.eq("districtMaster", districtMasterDAO.findByDistrictId(districtId));
				if(assessmentType!=0 && !status.equals("") && !districtId.equals(""))
				{
					//assessmentDetails = assessmentDetailDAO.findByCriteria(Order.desc("createdDateTime"),criterion,criterion1);
					assessmentDetails = assessmentDetailDAO.findByCriteria(sortOrderStrVal,criterion,criterion1,criterion2);				
				
				}
					else if(!status.equals("") && districtId.equals("") && assessmentType==0)
					{
						//assessmentDetails = assessmentDetailDAO.findByCriteria(Order.desc("createdDateTime"),criterion);
						assessmentDetails = assessmentDetailDAO.findByCriteria(sortOrderStrVal,criterion);				
					}
					else if(status.equals("") && districtId.equals("") && assessmentType!=0)
					{
						//assessmentDetails = assessmentDetailDAO.findByCriteria(Order.desc("createdDateTime"),criterion1);
						assessmentDetails = assessmentDetailDAO.findByCriteria(sortOrderStrVal,criterion1);				
					
					}	
					else if(status.equals("") && !districtId.equals("") && assessmentType==0){
						assessmentDetails = assessmentDetailDAO.findByCriteria(sortOrderStrVal,criterion2);
					}
					else if(!status.equals("") && districtId.equals("") && assessmentType!=0)
					{
						assessmentDetails = assessmentDetailDAO.findByCriteria(sortOrderStrVal,criterion,criterion1);				
					}
					else if(status.equals("") && !districtId.equals("") && assessmentType!=0)
					{
						assessmentDetails = assessmentDetailDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2);				
					
					}	
					else if(!status.equals("") && !districtId.equals("") && assessmentType==0){
						assessmentDetails = assessmentDetailDAO.findByCriteria(sortOrderStrVal,criterion,criterion2);
					}
			}

			List<AssessmentDetail> lstAssessmentDetail		=	new ArrayList<AssessmentDetail>();
			
			SortedMap<Integer,AssessmentDetail>	sortedMap = new TreeMap<Integer,AssessmentDetail>();
			if(sortOrderNoField.equals("Responses")){
				sortOrderFieldName="Responses";
			}

			//Map<Integer,Integer> assessmentAttempsMap = teacherAssessmentAttemptDAO.findAssessmentsResponses();
			Map<Integer,Integer> assessmentAttempsMap = teacherAssessmentStatusDAO.findAssessmentsResponses();
			int mapFlag=2;
			int sortingDynaVal=9999;
			for (AssessmentDetail assessmentDetail : assessmentDetails){
				int orderFieldInt=0;
				if(sortOrderFieldName.equals("Responses")){
					int noAttemptsorted=assessmentAttempsMap.get(assessmentDetail.getAssessmentId())==null?0:assessmentAttempsMap.get(assessmentDetail.getAssessmentId());
					sortingDynaVal--;
					orderFieldInt=Integer.parseInt(noAttemptsorted+""+sortingDynaVal);
					sortedMap.put(orderFieldInt,assessmentDetail);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					lstAssessmentDetail.add((AssessmentDetail) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					lstAssessmentDetail.add((AssessmentDetail) sortedMap.get(key));
				}
			}else{
				lstAssessmentDetail=assessmentDetails;
			}
			
			totalRecord =lstAssessmentDetail.size();

			if(totalRecord<end)
				end=totalRecord;
			List<AssessmentDetail> lstAssessmentDetails=lstAssessmentDetail.subList(start,end);
			
			sb.append("<table  id='tblGrid' width='100%' border='0'  >");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblNm,sortOrderFieldName,"assessmentName",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblGroupName1,sortOrderFieldName,"assessmentGroupDetails",sortOrderTypeVal,pgNo);
			sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
		
			responseText=PaginationAndSorting.responseSortingLink(lblcretedDate,sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			sb.append("<th width='13%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblResponses1,sortOrderFieldName,"Responses",sortOrderTypeVal,pgNo);
			sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblTpe,sortOrderFieldName,"assessmentType",sortOrderTypeVal,pgNo);
			sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			sb.append("<th width='4%%' valign='top'>"+responseText+"</th>");
			
			sb.append("<th width='33%'>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			
			int noAttempts = 0;
			for (AssessmentDetail assessmentDetail : lstAssessmentDetails) 
			{
				sb.append("<tr>" );
				sb.append("<td>"+assessmentDetail.getAssessmentName()+"</td>");
				if(assessmentDetail.getAssessmentGroupDetails()!=null)
					sb.append("<td>"+assessmentDetail.getAssessmentGroupDetails().getAssessmentGroupName()+"</td>");
				else
					sb.append("<td>"+optNA+"</td>");
				sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(assessmentDetail.getCreatedDateTime())+"</td>");
				noAttempts=assessmentAttempsMap.get(assessmentDetail.getAssessmentId())==null?0:assessmentAttempsMap.get(assessmentDetail.getAssessmentId());
				sb.append("<td>"+noAttempts+"</td>");
				
				if(assessmentDetail.getAssessmentType()==1) {
					sb.append("<td>"+lblB+"</td>");
				} else if(assessmentDetail.getAssessmentType()==2) {
					sb.append("<td>"+optJSpe+"</td>");
				} else if(assessmentDetail.getAssessmentType()==3) {
					sb.append("<td>"+lblSmartP+"</td>");
				} else if(assessmentDetail.getAssessmentType()==4) {
					sb.append("<td>"+lblIPI+"</td>");
				}
				
				sb.append("<td>");
				if(assessmentDetail.getStatus().equalsIgnoreCase("A"))
					sb.append(optAct);
				else
					sb.append(optInActiv);
				sb.append("</td>");
				sb.append("<td>");
				boolean pipeFlag=false;
				if(roleAccess.indexOf("|2|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editAssessment("+assessmentDetail.getAssessmentId()+","+noAttempts+")'>"+lblEdit+"</a>");
					pipeFlag=true;
				}else if(roleAccess.indexOf("|4|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editAssessment("+assessmentDetail.getAssessmentId()+","+noAttempts+")'>"+lnkV+"</a>");
					pipeFlag=true;
				}
				if(assessmentDetail.getStatus().equalsIgnoreCase("I"))
				{
					if(roleAccess.indexOf("|3|")!=-1){
						if(noAttempts==0){
							if(pipeFlag)sb.append(" | ");
							sb.append("<a href='javascript:void(0);' onclick='return deleteAssessment("+assessmentDetail.getAssessmentId()+")'>"+lnkDlt+"</a>");
							pipeFlag=true;
						}
						
					}
				}
				if(roleAccess.indexOf("|7|")!=-1){                
					if(pipeFlag)sb.append(" | ");
					if(assessmentDetail.getStatus().equalsIgnoreCase("A"))
						sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateAssessment("+assessmentDetail.getAssessmentId()+",'I')\">"+lblDeactivate+"</a>");
					else
						sb.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateAssessment("+assessmentDetail.getAssessmentId()+",'A')\">"+lblActivate+"</a>");
					pipeFlag=true;	
				}
				sb.append(" | <a href='assessmentsections.do?assessmentId="+assessmentDetail.getAssessmentId()+"' >"+lblManageSections1+"</a>");
				sb.append("</td>");

			}
			if(assessmentDetails.size()==0)
				sb.append("<tr id='tr1' ><td id='tr1' colspan=7>"+Utility.getLocaleValuePropByKey("msgNoInventoryfound1", locale)+"</td></tr>" );

			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
				
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get a particular assessment from database.
	 */
	public AssessmentDetail getAssessmentById(int assessmentId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		 
		AssessmentDetail assessmentDetail =null;
		try{

			assessmentDetail=assessmentDetailDAO.findById(assessmentId, false, false);

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return assessmentDetail;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to save a particular assessment to the database.
	 */
	//public int saveAssessment(AssessmentDetail assessmentDetail,String jobOrders )
	
	public String saveAssessment(AssessmentDetail assessmentDetail,String jobOrders,Integer defaultEPIGroupId)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		System.out.println("assessmentDetail.getIsResearchEPI() : "+assessmentDetail.getIsResearchEPI());
		//System.out.println("assessmentDetail.getJobCategoryMaster():::::::::::: "+assessmentDetail.getJobCategoryMaster().getJobCategoryId());
		
		// isDefault checking 
		
		List<AssessmentDetail> assessmentDetails = null;
		if(assessmentDetail.getAssessmentType()==2 && assessmentDetail.getIsDefault()==true)
		{
			assessmentDetails = assessmentDetailDAO.checkDefaultAssessmentForDistrictOrSchool(assessmentDetail);
			//System.out.println("assessmentDetails.size(): "+assessmentDetails.size());
			//return "3";
			/*if(true)
				throw new NullPointerException();*/
			if(assessmentDetails.size()>0)
			{
				return assessmentDetails.get(0).getAssessmentName();
			}
		}
		
		Integer assD = assessmentDetail.getAssessmentId();
		
		String ipAddress = request.getRemoteAddr();
		UserMaster user = null;

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");

		try{
			if(assessmentDetail.getAssessmentId()!=null)
			{
				System.out.println("assessmentDetail.getAssessmentId(): "+assessmentDetail.getAssessmentId());
				AssessmentDetail assessmentDetail1=assessmentDetailDAO.findById(assessmentDetail.getAssessmentId(), false, false);
				List<AssessmentJobRelation> assessmentJobRelations = assessmentDetail1.getAssessmentJobRelations();
				
				//assessmentDetail.setNoOfExperimentQuestions(assessmentDetail1.getNoOfExperimentQuestions());
				if(assessmentDetail.getScoreLookupMaster()!=null && assessmentDetail.getScoreLookupMaster().getLookupId()!= 0)
					assessmentDetail.setScoreLookupMaster(assessmentDetail.getScoreLookupMaster());
				else if(assessmentDetail.getScoreLookupMaster()!=null && assessmentDetail.getScoreLookupMaster().getLookupId()== 0)
					assessmentDetail.setScoreLookupMaster(null);
				assessmentDetail.setUserMaster(assessmentDetail1.getUserMaster());
				assessmentDetail.setIpaddress(assessmentDetail1.getIpaddress());
				//System.out.println("relations: "+assessmentJobRelations.size());
				if(assessmentJobRelations!=null)
					for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations) 
						assessmentJobRelationDAO.makeTransient(assessmentJobRelation);
			}
			else
			{
				//assessmentDetail.setNoOfExperimentQuestions(0);
				if(assessmentDetail.getScoreLookupMaster()!=null && assessmentDetail.getScoreLookupMaster().getLookupId()!= 0)
					assessmentDetail.setScoreLookupMaster(assessmentDetail.getScoreLookupMaster());
				else if(assessmentDetail.getScoreLookupMaster()!=null && assessmentDetail.getScoreLookupMaster().getLookupId()== 0)
					assessmentDetail.setScoreLookupMaster(null);
				assessmentDetail.setUserMaster(user);
				assessmentDetail.setIpaddress(ipAddress);
				
				if(assessmentDetail.getAssessmentType()==1)
					assessmentDetail.setStatus("I");
				else
					assessmentDetail.setStatus("A");
				
				assessmentDetail.setCreatedDateTime(new Date());
			}
			if(assessmentDetail.getIsResearchEPI()==null)
				assessmentDetail.setIsResearchEPI(false);

			List<AssessmentGroupDetails> assessmentGroupDetailsList = new ArrayList<AssessmentGroupDetails>();
			List<String> assessmentGroupNameList = new ArrayList<String>();
			if(assessmentDetail.getAssessmentType() == 1 || assessmentDetail.getAssessmentType() == 3 || assessmentDetail.getAssessmentType() == 4)
			{
				if(assessmentDetail.getAssessmentGroupDetails().getAssessmentGroupId() != 0)
					assessmentDetail.setAssessmentGroupDetails(assessmentDetail.getAssessmentGroupDetails());	
				else
				{
					//AssessmentGroupDetails a = assessmentGroupDetailsDAO.saveAssessmentGroupDetails(assessmentDetail,user,ipAddress);
					//assessmentDetail.setAssessmentGroupDetails(a);
					assessmentGroupDetailsList = assessmentGroupDetailsDAO.getAllAssessmentGroupDetailsList(assessmentDetail.getAssessmentType(), 0);
					for(AssessmentGroupDetails assessmentGroupDetails : assessmentGroupDetailsList)
						assessmentGroupNameList.add(assessmentGroupDetails.getAssessmentGroupName());
					if(assessmentGroupNameList!=null && assessmentGroupNameList.size()>0 && assessmentGroupNameList.contains(assessmentDetail.getAssessmentGroupDetails().getAssessmentGroupName()))
						return "4";
					else
					{
						AssessmentGroupDetails assessmentGroupDetails = new AssessmentGroupDetails();
						assessmentGroupDetails.setAssessmentGroupName(assessmentDetail.getAssessmentGroupDetails().getAssessmentGroupName());
						assessmentGroupDetails.setAssessmentType(assessmentDetail.getAssessmentType());
						assessmentGroupDetails.setStatus("A");
						assessmentGroupDetails.setUserMaster(user);
						assessmentGroupDetails.setCreatedDateTime(new Date());
						assessmentGroupDetails.setIpaddress(ipAddress);
						assessmentGroupDetailsDAO.makePersistent(assessmentGroupDetails);
						assessmentDetail.setAssessmentGroupDetails(assessmentGroupDetails);
					}
					System.out.println(":::::::: group name is : " + assessmentDetail.getAssessmentGroupDetails().getAssessmentGroupName());
				}
				if(defaultEPIGroupId!=null && defaultEPIGroupId>0){
					assessmentGroupDetailsDAO.updateAGDs(assessmentDetail.getAssessmentType(), true, null);
					assessmentGroupDetailsDAO.updateAGDs(assessmentDetail.getAssessmentType(), false, defaultEPIGroupId);
				}
			}/* else if(assessmentDetail.getAssessmentType() == 4) {
				if(assessmentDetail.getAssessmentGroupDetails().getAssessmentGroupId() != 0) {
					assessmentDetail.setAssessmentGroupDetails(assessmentDetail.getAssessmentGroupDetails());	
				} else {
					
					AssessmentGroupDetails a = assessmentGroupDetailsDAO.saveAssessmentGroupDetails(assessmentDetail,user,ipAddress);
					assessmentDetail.setAssessmentGroupDetails(a);
				}
			}*/ 
			else
				assessmentDetail.setAssessmentGroupDetails(null);
			
			assessmentDetailDAO.makePersistent(assessmentDetail);

			//System.out.println("LLLLLLLLLLLLLLLLLLLLLLLLLL "+assessmentDetail.getAssessmentType());
			if(assessmentDetail.getAssessmentType()==2)
			{

				AssessmentJobRelation assessmentJobRelation;
				String arr[]=jobOrders.split(",");
				
				if(!jobOrders.equals(""))
				for(int i=0;i<arr.length;i++)
				{
					//System.out.println("PPPPPPPPPPPPPPPPPPPPP "+arr[i]);
					assessmentJobRelation = new AssessmentJobRelation();
					assessmentJobRelation.setJobId(jobOrderDAO.findById(Integer.parseInt(arr[i]), false, false));
					assessmentJobRelation.setCreatedBy(user);
					assessmentJobRelation.setAssessmentId(assessmentDetail);
					assessmentJobRelation.setStatus("A");
					assessmentJobRelation.setIpaddress(ipAddress);
					assessmentJobRelationDAO.makePersistent(assessmentJobRelation);
				}
			} 
			///// Log Saving /////////
			
			if(assD==null)
			{
				assessmentLogDAO.saveAssessmentLog(user,  Utility.getLocaleValuePropByKey("msgInventoryCreated", locale), assessmentDetail.getAssessmentId()+" ---> "+assessmentDetail.getAssessmentName(), null, null, null);
				
			}else
			{
				assessmentLogDAO.saveAssessmentLog(user, Utility.getLocaleValuePropByKey("msgInventoryEdited", locale), assessmentDetail.getAssessmentId()+" ---> "+assessmentDetail.getAssessmentName(), null, null, null);
			}
		}
		catch (Exception e) {

			e.printStackTrace();
			//System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkk : "+e.getCause());
			if(e.getCause() instanceof com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException)
			{
				//return 3;
				return "3";
			}
			e.printStackTrace();
			//return 2;
			return "2";
		}

		//return 1;
		return "1";
	}


	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to delete a particular assessment from database.
	 */
	@Transactional(readOnly=false)
	public boolean deleteAssessment(AssessmentDetail assessmentDetail,String flag)
	{
		System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		UserMaster user = null;
	

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");
		
		try{
			assessmentDetail=assessmentDetailDAO.findById(assessmentDetail.getAssessmentId(), false, false);
			List<AssessmentJobRelation> assessmentJobRelations = assessmentDetail.getAssessmentJobRelations();
			if(flag.equals("") || flag.equalsIgnoreCase("jobOrders"))
			{
				//System.out.println("assessmentJobRelations: "+assessmentJobRelations.size());
				if(assessmentJobRelations!=null)
				{
					for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations) 
					{
						assessmentJobRelationDAO.makeTransient(assessmentJobRelation);
					}
				}
			}
			if(flag.equals(""))
			{
				List<AssessmentSections> assessmentSections = assessmentSectionDAO.getSectionsByAssessmentId(assessmentDetail);
				//System.out.println("assessmentSections: "+assessmentSections.size());

				if(assessmentSections!=null)
				{
					for (AssessmentSections assessmentSection : assessmentSections) 
					{
						//System.out.println("LLLLLLLLLLLLLLLLLLLLLLL assessmentSection: "+assessmentSection);
						//assessmentSectionDAO.makeTransient(assessmentSection);
						//deleteAssessmentSection(assessmentSection.getSectionId());
						deleteAssessmentSection(assessmentSection);
					}
				}
				
				assessmentLogDAO.saveAssessmentLog(user, Utility.getLocaleValuePropByKey("msgInventoryDeleted", locale), assessmentDetail.getAssessmentId()+" ---> "+assessmentDetail.getAssessmentName(), null, null, null);
				
			}
			
			assessmentDetailDAO.clear();
			assessmentDetailDAO.makeTransient(assessmentDetail);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to delete a particular assessment from database.
	 */
	@Transactional(readOnly=false)
	public String getSelectedJobs(AssessmentDetail assessmentDetail)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		try{
			
			assessmentDetail=assessmentDetailDAO.findById(assessmentDetail.getAssessmentId(), false, false);
			List<AssessmentJobRelation> assessmentJobRelations = assessmentDetail.getAssessmentJobRelations();
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,20,"assessment.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("roleAccess="+roleAccess);
			if(assessmentJobRelations!=null)
			{
				StringBuffer sb =new StringBuffer();
								
				for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations) 
				{
					sb.append("<div class='col-sm-5 col-md-5'>");
					sb.append("<label><strong>");
					if(roleAccess.indexOf("|3|")!=-1){
						sb.append("<a href='javascript:void(0);' onclick='deleteJob("+assessmentJobRelation.getAssessmentJobRelationId()+","+assessmentJobRelation.getAssessmentId().getAssessmentId()+")' ><img width='15' height='15' class='can' src='images/can-icon.png' alt='' title='Remove'></a>");
					}
					
					sb.append("&nbsp;"+assessmentJobRelation.getJobId().getJobTitle()+" ("+assessmentJobRelation.getJobId().getJobId()+")");
					sb.append("</label></strong></div>");
					
					sb.append("<input type='hidden' name='o_jobs' value='"+assessmentJobRelation.getJobId().getJobId()+"' />");
				}
				return sb.toString();
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
		return "0";
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to delete a particular Job from assessment.
	 */
	@Transactional(readOnly=false)
	public boolean deleteJob(AssessmentJobRelation assessmentJobRelation)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		try{
			
			assessmentJobRelationDAO.makeTransient(assessmentJobRelation);
			
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to change a particular assessment status.
	 */
	@Transactional(readOnly=false)
	public AssessmentDetail activateDeactivateAssessment(int assessmentId,String status)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		UserMaster user = null;
		

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");
		
		List<AssessmentDetail> assessmentDetails = null;

		try{
			AssessmentDetail assessmentDetail=assessmentDetailDAO.findById(assessmentId, false, false);

			/*
			// no active base checking  
			if(assessmentDetail.getAssessmentType()==1 && status.equalsIgnoreCase("A"))
			{
				assessmentDetails = assessmentDetailDAO.checkActiveBaseAssessment();
				if(assessmentDetails.size()>0)
					return assessmentDetails.get(0);
			}*/


			assessmentDetail.setStatus(status);
			assessmentDetailDAO.makePersistent(assessmentDetail);
			String sts = status.equalsIgnoreCase("A")?"Activated":"Deactivated";
			assessmentLogDAO.saveAssessmentLog(user, "Inventory "+sts, assessmentDetail.getAssessmentId()+" ---> "+assessmentDetail.getAssessmentName(), null, null, null);

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	////////////////////////////////// Assessment Section /////////////////////////////////////////////////

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get assessment sections from database and to display on the grid.
	 */
	public String getAssessmentSections(int assessmentId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		StringBuffer sb =new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord		= 	0;
			//------------------------------------
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,21,"assessmentsections.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			AssessmentDetail assessmentDetail= assessmentDetailDAO.findById(assessmentId, false, false);
			List<TeacherAssessmentAttempt> assessmentAttempts=teacherAssessmentAttemptDAO.findAssessmentResponses(assessmentDetail);
			int size=assessmentAttempts.size();
			List<AssessmentSections> assessmentSections = null;
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"sectionName";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			//domainMaster = domainmasterdao.findByCriteria(Order.asc("domainName"));
			System.out.println("===========  sortOrderStrVal ========="+sortOrderStrVal);
			Criterion criterion = Restrictions.eq("assessmentDetail", assessmentDetail);
			totaRecord = assessmentSectionDAO.getRowCountWithSort(sortOrderStrVal,criterion);
			assessmentSections = assessmentSectionDAO.findWithLimit(sortOrderStrVal,0,100,criterion);
			//assessmentSections = assessmentSectionDAO.findByCriteria(Order.desc("createdDateTime"),criterion);

			sb.append("<table border='0' class='table table-bordered mt30' id='tblGrid'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			//sb.append("<th width='18%'>Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblNm,sortOrderFieldName,"sectionName",sortOrderTypeVal,pgNo);
			sb.append("<th width='18%' valign='top'>"+responseText+"</th>");
			
			//sb.append("<th width='12%'>Created Date</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblcretedDate,sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			sb.append("<th width='12%' valign='top'>"+responseText+"</th>");
			
			//sb.append("<th width='23%'>Description</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblDecr,sortOrderFieldName,"sectionDescription",sortOrderTypeVal,pgNo);
			sb.append("<th width='23%' valign='top'>"+responseText+"</th>");
			
			//sb.append("<th width='23%'>Instructions</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblInst,sortOrderFieldName,"sectionInstructions",sortOrderTypeVal,pgNo);
			sb.append("<th width='23%' valign='top'>"+responseText+"</th>");
			
			sb.append("<th width='24%'>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			for (AssessmentSections assessmentSection : assessmentSections) 
			{
				sb.append("<tr>");
				sb.append("<td>"+assessmentSection.getSectionName()+"</td>");
				sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(assessmentSection.getCreatedDateTime())+"</td>");
				sb.append("<td>"+assessmentSection.getSectionDescription()+"</td>");
				sb.append("<td>"+assessmentSection.getSectionInstructions()+"</td>");
				sb.append("<td>");
					
				boolean pipeFlag=false;
				if(roleAccess.indexOf("|2|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editAssessmentSection("+assessmentSection.getSectionId()+")'>"+lblEdit+"</a>");
					pipeFlag=true;
				}else if(roleAccess.indexOf("|4|")!=-1){
					sb.append("<a href='javascript:void(0);' onclick='return editAssessmentSection("+assessmentSection.getSectionId()+")'>"+lnkV+"</a>");
					pipeFlag=true;
				}
					if(size==0)
					{
						if(roleAccess.indexOf("|3|")!=-1){
							if(pipeFlag)sb.append(" | ");
							sb.append("<a href='javascript:void(0);' onclick='return deleteAssessmentSection("+assessmentSection.getSectionId()+")'>"+lnkDlt+"</a>");
							pipeFlag=true;
						}
					}
				if(pipeFlag)sb.append(" | ");
				sb.append("<a href='assessmentquestions.do?assessmentId="+assessmentDetail.getAssessmentId()+"&sectionId="+assessmentSection.getSectionId()+"' >"+lblManageQuestions+"</a>");
				if(pipeFlag)sb.append(" | ");
				sb.append("<a href='importquestion.do?assessmentId="+assessmentDetail.getAssessmentId()+"&sectionId="+assessmentSection.getSectionId()+"' >"+headImpQues+"</a>");
				//sb.append("<a href='javascript:void(0);' onclick=\"importQuestion()\" >Import Questions</a>");
				if(assessmentSection.getSectionVideoUrl()!=null && !assessmentSection.getSectionVideoUrl().equals(""))
				sb.append(" | <a href='javascript:void(0);' onclick=\"displayVideo('"+assessmentSection.getSectionVideoUrl()+"')\" >"+lblPlay1+"</a>");
				sb.append("</td>");
			}
			if(assessmentSections.size()==0)
				sb.append("<tr id='tr1' ><td id='tr1' colspan=5>"+msgNoSectionfound1+"</td></tr>" );

			sb.append("</table>");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to save  a particular assessment section to the database.
	 */
	public int saveAssessmentSection(AssessmentSections assessmentSection,int assessmentId )
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		String ipAddress = IPAddressUtility.getIpAddress(request);
		UserMaster user = null;

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");

		Integer assSec = assessmentSection.getSectionId();
		
		assessmentSection.setUserMaster(user);
		assessmentSection.setIpaddress(ipAddress);
		assessmentSection.setStatus("A");
		if(assessmentSection.getSectionId()==null)
		{
			assessmentSection.setCreatedDateTime(new Date());
		}
		AssessmentDetail assessmentDetail = assessmentDetailDAO.findById(assessmentId, false, false);	
		assessmentSection.setAssessmentDetail(assessmentDetail);

		try{

			List<AssessmentSections> assessmentSections=assessmentSectionDAO.checkDuplicateAssessmentSection(assessmentDetail,assessmentSection.getSectionName(),assessmentSection.getSectionId());
			int size = assessmentSections.size();
			if(size>0)
				return 3;


			assessmentSectionDAO.makePersistent(assessmentSection);
			
			
			//// Log Generation 
			assessmentDetail = assessmentSection.getAssessmentDetail();
			String sts ="" ;
			if(assSec==null)
				sts = "Created";
			else
				sts = "Edited";

			assessmentLogDAO.saveAssessmentLog(user, "Section "+sts, 
					assessmentDetail.getAssessmentId()+" ---> "+assessmentDetail.getAssessmentName(),
					assessmentSection.getSectionId()+" ---> "+assessmentSection.getSectionName(), null, null);
			
		}catch (Exception e) {
			e.printStackTrace();
			return 2;
		}

		return 1;

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get assessment section from database.
	 */
	public AssessmentSections getAssessmentSectionById(int assessmentId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		AssessmentSections assessmentSection =null;
		try{

			assessmentSection=assessmentSectionDAO.findById(assessmentId, false, false);

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return assessmentSection;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to delete assessment section from database.
	 */
	@Transactional(readOnly=false)
	public boolean deleteAssessmentSection(AssessmentSections assessmentSection)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		UserMaster user = null;
		

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");
		
		try{
			List<AssessmentQuestions> assessmentQuestions = assessmentQuestionsDAO.getSectionQuestions(assessmentSection);
			assessmentQuestionsDAO.clear();
			if(assessmentQuestions!=null)
			{
				for (AssessmentQuestions assessmentQuestions2 : assessmentQuestions) {
					assessmentQuestionsDAO.makeTransient(assessmentQuestions2);
				}
			}
			assessmentSection = assessmentSectionDAO.findById(assessmentSection.getSectionId(), false, false);
			assessmentSectionDAO.makeTransient(assessmentSection);
			
		///// Log Saving /////////
			
		  AssessmentDetail	assessmentDetail = assessmentSection.getAssessmentDetail();
		  assessmentLogDAO.saveAssessmentLog(user, "Section Deleted", 
					assessmentDetail.getAssessmentId()+" ---> "+assessmentDetail.getAssessmentName(),
					assessmentSection.getSectionId()+" ---> "+assessmentSection.getSectionName(), null, null);

		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Competencies  from database.
	 */
	public List<CompetencyMaster> getCompetencies(int domainId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		List<CompetencyMaster> competencyMasters = null;

		try{
			DomainMaster domainMaster = domainMasterDAO.findById(domainId, false, false);
			competencyMasters = competencyMasterDAO.getCompetencesByDomain(domainMaster);
			CompetencyMaster competencyMaster= new CompetencyMaster();
			competencyMaster.setCompetencyName("Select Competency");
			competencyMaster.setCompetencyId(0);
			competencyMasters.add(0,competencyMaster);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return competencyMasters;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Objectives  from database.
	 */
	public List<ObjectiveMaster> getObjectives(int competencyId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		List<ObjectiveMaster> objectiveMasters =null;
		try {
			CompetencyMaster competencyMaster = competencyMasterDAO.findById(competencyId, false, false);
			objectiveMasters = objectiveMasterDAO.getObjectivesByCompetency(competencyMaster);
			ObjectiveMaster objectiveMaster = new ObjectiveMaster();
			objectiveMaster.setObjectiveName("Select Objective");
			objectiveMaster.setObjectiveId(0);
			objectiveMasters.add(0,objectiveMaster);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return objectiveMasters;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to save Assessment Section Question.
	 */
	@Transactional(readOnly=false)
	public QuestionsPool saveAssessmentSectionQuestion(QuestionsPool questionsPool,AssessmentDetail assessmentDetail,AssessmentSections assessmentSections,String jsonObject,QuestionOptions[] questionOptions,AssessmentQuestions assessmentQuestion,String weightage,String itemCode,Boolean isExperimental)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		Integer assQ = assessmentQuestion.getAssessmentQuestionId();
		String ipAddress = IPAddressUtility.getIpAddress(request);
		UserMaster user = null;

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");
		
		assessmentSections = assessmentSectionDAO.findById(assessmentSections.getSectionId(), false, false);
		assessmentDetail = assessmentSections.getAssessmentDetail();
		//JSONObject json = (JSONObject) JSONSerializer.toJSON(jsonObject);
		//System.out.println("json: "+json);

		//System.out.println("questionOptions.length: "+questionOptions.length);
		//System.out.println("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV questionsPool: "+questionsPool.getQuestionId());
		
		String optionsList = "";
		if (questionsPool.getQuestionId()==null)
		{
			questionsPool.setCreatedDateTime(new Date());		
			questionsPool.setQuestionTaken(0);
			if(itemCode!=null)
			questionsPool.setItemCode(itemCode);
		}

		try{
			if(questionsPool.getQuestionWeightage()==null)
				questionsPool.setQuestionWeightage(0.00); 
			
			questionsPool.setUserMaster(user);
			questionsPool.setIpaddress(ipAddress);
			questionsPool.setStatus("A");

			QuestionsPool questionsPoolNew = null;
			List<QuestionOptions> qopt = null;
			if(questionsPool.getQuestionId()!=null)
			{
				questionsPoolNew=questionsPoolDAO.findById(questionsPool.getQuestionId(), false, false);
				qopt = questionsPoolNew.getQuestionOptions();
				questionsPool.setQuestionTaken(questionsPoolNew.getQuestionTaken());
				questionsPool.setQuestionWeightage(questionsPoolNew.getQuestionWeightage());
				if(itemCode!=null)
				questionsPool.setItemCode(itemCode);
			}
				
			
			questionsPoolDAO.clear();
			questionsPoolDAO.makePersistent(questionsPool);
			
			if(assessmentDetail.getAssessmentType().equals(2)){//JSI
				questionsPool.setQuestionUId("jsi."+Utility.addZeroBeforeNo(questionsPool.getQuestionId(),5));
			}
			else{//Base -1
				DomainMaster dm = domainMasterDAO.findById(questionsPool.getDomainMaster().getDomainId(), false, false);
				CompetencyMaster cm = competencyMasterDAO.findById(questionsPool.getCompetencyMaster().getCompetencyId(),false,false);
				ObjectiveMaster om = objectiveMasterDAO.findById(questionsPool.getObjectiveMaster().getObjectiveId(), false, false);
				
				questionsPool.setQuestionUId(dm.getDomainUId()+"."+cm.getCompetencyUId()+"."+om.getObjectiveUId()+"."+Utility.addZeroBeforeNo(questionsPool.getQuestionId(),5));
			}
			questionsPoolDAO.makePersistent(questionsPool);

			Map<Integer,QuestionOptions> map = new HashMap<Integer,QuestionOptions>();
			if(qopt!=null)
				for (QuestionOptions questionOptions2 : qopt) {
					map.put(questionOptions2.getOptionId(), questionOptions2);
				}

			Integer questionOptionTag = 1;
			for (QuestionOptions questionOption : questionOptions) {
				questionOption.setQuestionsPool(questionsPool);
				questionOption.setCreatedDateTime(new Date());
				questionOption.setUserMaster(user);
				questionOption.setIpaddress(ipAddress);
				questionOption.setStatus("A");
				questionOption.setQuestionOptionTag(questionOptionTag);
				questionOptionsDAO.makePersistent(questionOption);
				// OptionList 
				optionsList+=questionOption.getOptionId()+" ---> "+questionOption.getQuestionOption()+" | ";
				
				if(qopt!=null)
					map.remove(questionOption.getOptionId());
				questionOption=null;
				questionOptionTag++;
			}
			if(qopt!=null)
				for(Integer aKey : map.keySet()) {
					questionOptionsDAO.makeTransient(map.get(aKey));
				}

			AssessmentQuestions assessmentQuestions =assessmentQuestion;
			assessmentQuestions.setAssessmentDetail(assessmentDetail);
			assessmentQuestions.setAssessmentSections(assessmentSections);
			assessmentQuestions.setQuestionsPool(questionsPool);
			assessmentQuestions.setIsMandatory(false);
			if(assessmentQuestions.getAssessmentQuestionId()==null)
				assessmentQuestions.setCreatedDateTime(new Date());
			if(isExperimental!=null)
				assessmentQuestions.setIsExperimental(isExperimental);
			assessmentQuestions.setUserMaster(user);
			assessmentQuestions.setStatus("A");
			assessmentQuestions.setQuestionPosition(0);
			if(weightage!=null)
			assessmentQuestions.setQuestionWeightage(Double.parseDouble(weightage));
			assessmentQuestionsDAO.makePersistent(assessmentQuestions);
			
			/// Log saving ..................................
			String sts = "";
			if(assQ==null)
				sts = "Created";
			else
				sts = "Edited";
			
			 AssessmentSections assessmentSection = assessmentQuestions.getAssessmentSections();
			 assessmentDetail = assessmentSection.getAssessmentDetail();
			
			 
			  assessmentLogDAO.saveAssessmentLog(user, "Question "+sts, 
						assessmentDetail.getAssessmentId()+" ---> "+assessmentDetail.getAssessmentName(),
						assessmentSection.getSectionId()+" ---> "+assessmentSection.getSectionName(), 
						assessmentQuestions.getAssessmentQuestionId()+" ---> "+questionsPool.getQuestion(), optionsList);

		}catch (Exception e) {
			e.printStackTrace();
		}

		return questionsPool;
	}
	
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment Section Questions.
	 */
	public String getAssessmentSectionQuestions(AssessmentDetail assessmentDetail,AssessmentSections assessmentSections,DomainMaster domainMaster,CompetencyMaster competencyMaster,ObjectiveMaster objectiveMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		StringBuffer sb =new StringBuffer();
		try{
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,22,"assessmentquestions.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<AssessmentQuestions> assessmentQuestions =null;
			Criterion criterion = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion1 = Restrictions.eq("assessmentSections", assessmentSections);

			AssessmentQuestions assessmentQuestion1 = new AssessmentQuestions();
			assessmentQuestion1.setAssessmentDetail(assessmentDetail);
			assessmentQuestion1.setAssessmentSections(assessmentSections);
			if(domainMaster!=null && domainMaster.getDomainId()!=null && domainMaster.getDomainId()!=0)
				assessmentQuestions = assessmentQuestionsDAO.filterQuestions(assessmentQuestion1,domainMaster,competencyMaster,objectiveMaster);
			else
				assessmentQuestions = assessmentQuestionsDAO.findByCriteria(criterion,criterion1);
			
			Integer assessmentType = 2;
			
			if(assessmentQuestions.size()>0)
				assessmentType = assessmentQuestions.get(0).getAssessmentDetail().getAssessmentType();
			
			int i=0;
			String uId = "";
			List<QuestionOptions> questionOptionsList = null;
			String questionInstruction = null;
			String shortName = "";
			Integer questionTaken = 0;
			QuestionsPool questionsPool = null;
			StageOneStatus stageOneStatus = null;
			StageTwoStatus stageTwoStatus = null;
			StageThreeStatus stageThreeStatus = null;
			Double questionWeightage = 0.0;
			for (AssessmentQuestions assessmentQuestion : assessmentQuestions) 
			{
				questionsPool = assessmentQuestion.getQuestionsPool();
				shortName = questionsPool.getQuestionTypeMaster().getQuestionTypeShortName();
				questionTaken = questionsPool.getQuestionTaken()==null?0:questionsPool.getQuestionTaken();
				
				//questionWeightage = questionsPool.getQuestionWeightage();
				
				questionWeightage =  assessmentQuestion.getQuestionWeightage();
				
				if(assessmentType==1 || assessmentType==3 || assessmentType==4)
				{
					stageOneStatus = questionsPool.getStageOneStatus();
					stageTwoStatus = questionsPool.getStageTwoStatus();
					stageThreeStatus = questionsPool.getStageThreeStatus();
				}
				
				if(questionsPool.getQuestionUId() == null){
					uId = "";
				}
				else{
					uId = questionsPool.getQuestionUId();
				}			
				sb.append("<tr>");
				sb.append("<td width='81%'>");
				
				if(questionsPool.getQuestionUId() != null){
				
					uId = questionsPool.getQuestionUId();
					sb.append("UID: "+uId+",");
					if(assessmentType==1 || assessmentType==3 || assessmentType==4)
					{
						if(stageOneStatus!=null)
							sb.append("&nbsp;&nbsp;"+lblStageOne1+": "+stageOneStatus.getName()+",");
						else
							sb.append("&nbsp;&nbsp;"+lblStageOneNA1+",");
//						boolean isExperimental = false;
						if(stageTwoStatus!=null)
						{
							sb.append("&nbsp;&nbsp;"+lblStageTwo1+": "+stageTwoStatus.getName()+",");
//							if(stageTwoStatus.getName().equalsIgnoreCase("Experimental"))
//								isExperimental = true;
						}
						else
							sb.append("&nbsp;&nbsp;"+lblStageTwoNA1+",");
						
						if(stageThreeStatus!=null)
							sb.append("&nbsp;&nbsp;"+lblStageThree1+": "+stageThreeStatus.getName()+",");
						else
							sb.append("&nbsp;&nbsp;"+lblStageThreeNA1+",");
						
						if(assessmentQuestion.getIsExperimental()!=null && assessmentQuestion.getIsExperimental())
							sb.append("&nbsp;&nbsp;"+lblQuestionWeightage1+": "+questionWeightage+"&nbsp;&nbsp;<font color='red'>"+lblExperimental1+"</font>");
						else
							sb.append("&nbsp;&nbsp;"+lblQuestionWeightage1+": "+questionWeightage);
						//sb.append("<br>");
					}
					sb.append("<br>");
				}
				sb.append( Utility.getLocaleValuePropByKey("headQues", locale)+(++i)+": "+questionsPool.getQuestion()+"<br/>");

				questionOptionsList = questionsPool.getQuestionOptions();
				if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel") || shortName.equalsIgnoreCase("lkts"))
				{
					sb.append("<table class='top12'>");
					for (QuestionOptions questionOptions : questionOptionsList) {
						sb.append("<tr ><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' style='margin:0px;' name='opt"+i+"' /></td><td style='border:0px;background-color: transparent;padding: 2px 3px;'>"+questionOptions.getQuestionOption()+"</td></tr>");
					}
					sb.append("</table>");
				}else if(shortName.equalsIgnoreCase("rt"))
				{
			
					sb.append("<table class='top12'>");
					for (QuestionOptions questionOptions : questionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' name='opt"+i+"' class='span1' maxlength='3'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+questionOptions.getQuestionOption()+"</td></tr>");
					}
					sb.append("</table>");
				}
				else if(shortName.equalsIgnoreCase("mlsel"))
				{
					sb.append("<table class='top12'>");
					for (QuestionOptions questionOptions : questionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' name='opt"+i+"' class='span1' /></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+questionOptions.getQuestionOption()+"</td></tr>");
					}
					sb.append("</table>");
				}
				else if(shortName.equalsIgnoreCase("mloet"))
				{
					sb.append("<table class='top12'>");
					for (QuestionOptions questionOptions : questionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' name='opt"+i+"' class='span1' /></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+questionOptions.getQuestionOption()+"</td></tr>");
					}
					sb.append("</table>");
					
					sb.append("<table>");
					sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><textarea name='insertedText' id='insertedText' class='form-control' maxlength='5000' rows='15' cols='100' style='width:98%;margin:5px;' /></textarea></td></tr>");
					sb.append("</table>");
					
					
				}
				else if(shortName.equalsIgnoreCase("sloet"))
				{
					sb.append("<table class='top12'>");
					for (QuestionOptions questionOptions : questionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='opt"+i+"' class='span1' /></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+questionOptions.getQuestionOption()+"</td></tr>");
					}
					sb.append("</table>");
					
					if(shortName.equalsIgnoreCase("sloet"))
					{
						sb.append("<table >");
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><textarea name='insertedText' id='insertedText' class='form-control' maxlength='5000' rows='15' cols='100' style='width:98%;margin:5px;' /></textarea></td></tr>");
						sb.append("</table>");
					}
					
				}
				// Image Type
				if(shortName.equalsIgnoreCase("it"))
				{
					sb.append("<table >");
					for (QuestionOptions questionOptions : questionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='opt"+i+"' /></td><td style='border:0px;background-color: transparent;padding: 2px 3px;vertical-align:middle;'><img src='showImage?image=ques_images/"+questionOptions.getQuestionOption()+"'></td></tr>");
					
					}
					sb.append("</table>");
				}else if(shortName.equalsIgnoreCase("sl"))
				{
					sb.append("<div class='col-sm-10 col-md-10'>");
					sb.append("&nbsp;&nbsp;<input type='text' name='opt"+i+"' class='form-control' maxlength='75'/><br/>");
					sb.append("</div>");
				}
				else if(shortName.equalsIgnoreCase("ml"))
				{
					sb.append("<div class='col-sm-10 col-md-10'>");
					sb.append("&nbsp;&nbsp;<textarea name='opt"+i+"' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\"/></textarea><br/>");
					sb.append("</div>");
				}
				
				questionInstruction = questionsPool.getQuestionInstruction()==null?"":questionsPool.getQuestionInstruction();
				if(!questionInstruction.equals(""))
				{
					sb.append("<div class='col-sm-10 col-md-10' style='margin-left:-15px;margin-bottom:-20px;'>");
					sb.append(""+lblInst+" : </br>"+questionInstruction);
				}
				else
				{
					sb.append("<div class='col-sm-10 col-md-10' style='margin-left:-15px'>");
					//sb.append("<br/>");
				}
				
				sb.append("</div>");				
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append("<div style=''>");
				boolean pipeFlag=false;
				if(roleAccess.indexOf("|2|")!=-1){
					sb.append("<a href='assessmentsectionquestions.do?sectionId="+assessmentQuestion.getAssessmentSections().getSectionId()+"&assessmentQuestionId="+assessmentQuestion.getAssessmentQuestionId()+"' >"+lblEdit+"</a>");
					pipeFlag=true;
				}else if(roleAccess.indexOf("|4|")!=-1){
					sb.append("<a href='assessmentsectionquestions.do?sectionId="+assessmentQuestion.getAssessmentSections().getSectionId()+"&assessmentQuestionId="+assessmentQuestion.getAssessmentQuestionId()+"' >"+lnkV+"</a>");
					pipeFlag=true;
				}else{
					sb.append("&nbsp;");
				}
				if(roleAccess.indexOf("|3|")!=-1){
					if(pipeFlag)sb.append(" | ");
					sb.append("<a href='javascript:void(0)' onclick='deleteAssessmentQuestion("+assessmentQuestion.getAssessmentQuestionId()+")'>"+Utility.getLocaleValuePropByKey("lnkRemovefromInventory", locale)+"</a>");
				}	
				sb.append("</div>");
				sb.append("</td>");

			}
			if(assessmentQuestions.size()==0)

				sb.append("<tr id='tr1' ><td id='tr1' colspan=2>"+lblNoQuestionfound1+"</td></tr>" );
	}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to remove assessment question from database.
	 */
	@Transactional(readOnly=false)
	public boolean deleteAssessmentQuestion(AssessmentQuestions assessmentQuestions)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		UserMaster user = null;

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");
		
		try{
			assessmentQuestions = assessmentQuestionsDAO.findById(assessmentQuestions.getAssessmentQuestionId(), false, false);
			assessmentQuestionsDAO.makeTransient(assessmentQuestions);
			 
		///// Log Saving /////////
			
			  AssessmentDetail	assessmentDetail = assessmentQuestions.getAssessmentDetail();
			  AssessmentSections assessmentSection = assessmentQuestions.getAssessmentSections();
			  assessmentLogDAO.saveAssessmentLog(user, "Question Deleted", 
						assessmentDetail.getAssessmentId()+" ---> "+assessmentDetail.getAssessmentName(),
						assessmentSection.getSectionId()+" ---> "+assessmentSection.getSectionName(), 
						assessmentQuestions.getAssessmentQuestionId()+" ---> "+assessmentQuestions.getQuestionsPool().getQuestion(), null);

		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get a particular Assessment Question.
	 */
	@Transactional(readOnly=false)
	public AssessmentQuestions getAssessmentQuestionById(AssessmentQuestions assessmentQuestion)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		
		String ipAddress = IPAddressUtility.getIpAddress(request);
		UserMaster user = null;

		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			user=(UserMaster)session.getAttribute("userMaster");

		System.out.println(" getAssessmentQuestionById "+assessmentQuestion.getAssessmentQuestionId());
		try{
			assessmentQuestion = assessmentQuestionsDAO.findById(assessmentQuestion.getAssessmentQuestionId(), false, false);
			System.out.println("assessmentQuestionItemCode::"+assessmentQuestion.getQuestionsPool().getItemCode());

		}catch (Exception e) {
			e.printStackTrace();
		}

		return assessmentQuestion;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Job Orders.
	 */
	@Transactional(readOnly=false)
	public List<JobOrder> getJobOrders(AssessmentDetail assessmentDetail,DistrictMaster districtMaster,SchoolMaster schoolMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		Map<Integer,JobOrder> map = null;
		List<JobOrder> jobOrders = null;
		System.out.println("schoolMaster llllllllllllllllllllllllllll : "+schoolMaster.getSchoolId());
		
		if(schoolMaster.getSchoolId()!=null && schoolMaster.getSchoolId()>0)
			schoolMaster = schoolMasterDAO.findById(schoolMaster.getSchoolId(), false, false);
		else
			schoolMaster = null;
		
		if(assessmentDetail.getAssessmentId()!=0)
		{
			assessmentDetail= assessmentDetailDAO.findById(assessmentDetail.getAssessmentId(), false, false);
			if(assessmentDetail.getAssessmentType()==2)
			{
				List<AssessmentJobRelation> assessmentJobRelations = assessmentDetail.getAssessmentJobRelations();
				//System.out.println("PPPPPPPPPPPPPPPPPPPPPPPPPPP assessmentJobRelations "+assessmentJobRelations.size());

				map = new HashMap<Integer, JobOrder>();
				for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations) {
					map.put(assessmentJobRelation.getJobId().getJobId(),assessmentJobRelation.getJobId());
				}
			}
		}
		
		jobOrders = jobOrderDAO.findValidJobOrders(map,districtMaster,schoolMaster);
		/*JobOrder jobOrder = new JobOrder();
		jobOrder.setJobId(0);
		jobOrder.setJobTitle("");
		jobOrders.add(jobOrder);*/
		return jobOrders;
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Job Orders District.
	 */
	@Transactional(readOnly=false)
	public List<DistrictMaster> getJobOrderDistricts(int createdForEntity)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		List<DistrictMaster> districtMasters = null;
		districtMasters = jobOrderDAO.getDistrictsByJobOrders(createdForEntity);
		if(districtMasters!=null)
		{	DistrictMaster districtMaster = new DistrictMaster();
			districtMaster.setDistrictId(0);
			districtMaster.setDistrictName("Select District");
			districtMasters.add(0, districtMaster);
		}
		//System.out.println("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL districtMaster : "+districtMasters.size());
		return districtMasters;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Job Orders School.
	 */
	@Transactional(readOnly=false)
	public List<SchoolMaster> getJobOrderSchools(DistrictMaster districtMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		List<SchoolMaster> schoolMasters = null;
		schoolMasters = jobOrderDAO.getSchoolsByDistrictInJobOrders(districtMaster);
		
		if(schoolMasters!=null)
		{	SchoolMaster schoolMaster = new SchoolMaster();
			schoolMaster.setSchoolId(new Long(0));
			schoolMaster.setSchoolName("Select School");
			schoolMasters.add(0, schoolMaster);
		}
		//System.out.println("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL schoolMasters : "+schoolMasters.size());
		return schoolMasters;
	}
	
	/*================ Display Vedio ==============*/
	@Transactional(readOnly=false)
	public String displayVideo(String urlFormat)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		return Utility.getYouTubeCode(urlFormat,350,250);
	}
	
	@Transactional(readOnly=true)
	public String displayJobCategoryByDistrictForJSI(Integer districtID,String txtjobCategory)
	{
		StringBuffer buffer;
		try {
			buffer = new StringBuffer();
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(msgYrSesstionExp);
			}
			Integer jobCategory=0;
			if(txtjobCategory!=null && !txtjobCategory.equals(""))
				jobCategory=Integer.parseInt(txtjobCategory);

			if(districtID>0)
			{
				//DistrictMaster districtMaster=districtMasterDAO.findById(districtID, false, false);
				DistrictMaster districtMaster=new DistrictMaster();
				districtMaster.setDistrictId(districtID);
				//List<JobCategoryMaster> lstjobCategoryMasters=jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
				List<JobCategoryMaster> lstjobCategoryMasters=jobCategoryMasterDAO.findAllJobCategoryNameByDistrictTotal(districtMaster);
				System.out.println(lstjobCategoryMasters.size());
				if(lstjobCategoryMasters.size()>0)
				{
					buffer.append("<select class='span5' name='jobCategory' id='jobCategory' >");
					buffer.append("<option selected='selected' value=''>"+optStrJobCat+"</option>");
					for(JobCategoryMaster pojo:lstjobCategoryMasters)
						if(pojo!=null)
						{
							if(pojo.getJobCategoryId().equals(jobCategory))
								buffer.append("<option value='"+pojo.getJobCategoryId()+"' selected='selected'>"+pojo.getJobCategoryName()+"</option>");
							else
								buffer.append("<option value='"+pojo.getJobCategoryId()+"'>"+pojo.getJobCategoryName()+"</option>");
						}
					buffer.append("</select>");
				}else
				{
					buffer.append("<select class='span5' name='jobCategory' id='jobCategory' disabled='disabled' onchange='resetJobCategory();'>");
					buffer.append("<option selected='selected' value=''>"+optStrJobCat+"</option>");
					buffer.append("</select>");
				}
			}
			else
			{
				buffer.append("<select class='span5' name='jobCategory' id='jobCategory' disabled='disabled' onchange='resetJobCategory();'>");
				buffer.append("<option selected='selected' value=''>"+optStrJobCat+"</option>");
				buffer.append("</select>");
			}
			return buffer.toString();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		//return buffer.toString();
	}
	/* @Author: Ashish Chaudhary
	 * @Discription: It is used to get the list of all Assessment Group.
	 */
	
	public List<AssessmentGroupDetails> getAllAssessmentGroupDetails() {
		return assessmentGroupDetailsDAO.getAllAssessmentGroupDetails();
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param assessmentType 3 or 4
	 * @param from 0 for all and 1 for status A
	 * @return assessmentGroupDetailsList
	 */
	@Transactional(readOnly=false)
	public List<AssessmentGroupDetails> getAllAssessmentGroupDetailsList(int assessmentType, int from)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null))
			throw new IllegalStateException(msgYrSesstionExp);
		
		List<AssessmentGroupDetails> assessmentGroupDetailsList = new ArrayList<AssessmentGroupDetails>();
		assessmentGroupDetailsList = assessmentGroupDetailsDAO.getAllAssessmentGroupDetailsList(assessmentType, from);
		System.out.println(":::::::::::::::::::: assessmentGroupDetailsList.size()== "+assessmentGroupDetailsList.size());
		
		return assessmentGroupDetailsList;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param assessmentType 1 or 3 or 4
	 * @return scoreLookupMasterList
	 */
	@Transactional(readOnly=false)
	public List<ScoreLookupMaster> getScoringMethods(int assessmentType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null))
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		
		String shortName = "EPI";
		if(assessmentType==3)
			shortName = "SP";
		else if(assessmentType==4)
			shortName = "IPI";
		
		List<ScoreLookupMaster> scoreLookupMasterList = new ArrayList<ScoreLookupMaster>();
		scoreLookupMasterList = scoreLookupMasterDAO.getScoringMethods(shortName);
		System.out.println(":::::::::::::::::::: scoreLookupMasterList.size()== "+scoreLookupMasterList.size());
		
		return scoreLookupMasterList;
	}
}


