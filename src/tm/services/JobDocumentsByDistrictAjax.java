package tm.services;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictDocumentUpload;
import tm.bean.DistrictSpecificDocuments;
import tm.bean.DocumentFileMaster;
import tm.bean.TempdocsData;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictDocumentUploadDAO;
import tm.dao.DistrictSpecificDocumentsDAO;
import tm.dao.DocumentCategoryMasterDAO;
import tm.dao.DocumentFileMasterDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class JobDocumentsByDistrictAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;

	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;

	@Autowired
	private DistrictSpecificDocumentsDAO districtSpecificDocumentsDAO;

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;

	@Autowired
	private JobOrderDAO jobOrderDAO;

	@Autowired
	private DocumentCategoryMasterDAO documentCategoryMasterDAO;

	@Autowired
	private DocumentFileMasterDAO documentFileMasterDAO;

	@Autowired
	private DistrictDocumentUploadDAO districtDocumentUploadDAO;

	@Transactional(readOnly=false)
	public String saveJobDocuments(int docFlag,int districtId,int uploadedBy, int entityType, String jobCategory,String documentName,int saveEditFlag,int jobDocsId,String expDate,String defaultDocId,String[] uploadedFiles)
	{
		System.out.println("======== Save Documents =====");
		WebContext context;
		String result ="";
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		Date expireDate = null;
		DocumentFileMaster documentFileMaster = null;
		DistrictMaster districtMaster = null;
		Boolean fileFound = false;
		if(session == null || session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			UserMaster userSession = (UserMaster) session.getAttribute("userMaster");
			System.out.println(" districtId :: "+districtId);
			System.out.println(" documentName :: "+documentName);
			System.out.println(" uploaded by :: "+uploadedBy);
			System.out.println(" entityType :: "+entityType);
			System.out.println(" jobCategory :: "+jobCategory);
			System.out.println(" saveEditFlag :: "+saveEditFlag);
			System.out.println(" jobDocsId :: "+jobDocsId);
			System.out.println(" expDate :: "+expDate);
			System.out.println(" defaultDocId :: "+defaultDocId);
			System.out.println(" uploadedFiles :: "+uploadedFiles);
			System.out.println(" docFlag :: "+docFlag);
			Map<Integer,JobCategoryMaster> map = new HashMap<Integer, JobCategoryMaster>();
			List<JobCategoryMaster> jobCategoryMasters = null;
			try
			{
				if(userSession.getEntityType()==2)
				{
					jobCategoryMasters = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(userSession.getDistrictId());
					for(JobCategoryMaster jbCategory : jobCategoryMasters)
					{
						map.put(jbCategory.getJobCategoryId(), jbCategory);
					}
				}
				else
				{
					jobCategoryMasters = jobCategoryMasterDAO.findAll();
					for(JobCategoryMaster jbCategory : jobCategoryMasters)
					{
						map.put(jbCategory.getJobCategoryId(), jbCategory);
					}
				}
				if(expDate!=null && expDate.length()>0)
				{
					String DATE_FORMAT = "MM-dd-yyyy";
					SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
					expireDate	= sdf.parse(expDate);
				}

				int id = uploadedBy;
				UserMaster usermasters = userMasterDAO.findById(id, false, false);
				if((defaultDocId!=null) && (!defaultDocId.equals("-1")) && (defaultDocId.length()>0))
				{
					documentFileMaster = documentFileMasterDAO.findById(Integer.parseInt(defaultDocId), false, false);
				}
				String[] jbCat= jobCategory.split("#");
				System.out.println(" Job category length :: "+jbCat.length);
				if(jbCat.length!=0)
				{					
					if(saveEditFlag==0) // add
					{
						SessionFactory factoryDSD = districtSpecificDocumentsDAO.getSessionFactory();
						StatelessSession statelessSessionDSD=factoryDSD.openStatelessSession();

						SessionFactory factoryDDU = districtDocumentUploadDAO.getSessionFactory();
						StatelessSession statelessSessionDDU=factoryDDU.openStatelessSession();

						Transaction transactionDSD=statelessSessionDSD.beginTransaction();
						Transaction transactionDDU=statelessSessionDDU.beginTransaction();
						if(usermasters.getEntityType()==1)
						{
							districtMaster = districtMasterDAO.findById(districtId, false, false);
						}
						else if(usermasters.getEntityType()==2)
						{
							districtMaster = usermasters.getDistrictId();
						}
						if(uploadedFiles!=null && uploadedFiles.length>0)
						{
							for(int i=0;i<jbCat.length;i++)
							{
								String ipAddress = IPAddressUtility.getIpAddress(request);
								DistrictSpecificDocuments districtSpecificDocuments = new DistrictSpecificDocuments();
								districtSpecificDocuments.setDocumentName(documentName);
								JobCategoryMaster jobCategoryMaster = map.get(Integer.parseInt(jbCat[i]));
								districtSpecificDocuments.setJobCategoryMaster(jobCategoryMaster);
								districtSpecificDocuments.setDistrictMaster(districtMaster);
								districtSpecificDocuments.setStatus("A");
								districtSpecificDocuments.setUserMaster(usermasters);
								districtSpecificDocuments.setIpAddress(ipAddress);
								if(documentFileMaster!=null)
								{
									districtSpecificDocuments.setDocumentFileMaster(documentFileMaster);	
								}
								districtSpecificDocuments.setUploadFile(true);
								if(expDate!=null && expDate.length()>0)
								{
									districtSpecificDocuments.setExpireDate(expireDate);
								}
								if(docFlag==1)
									districtSpecificDocuments.setDocType(true);
								else
									districtSpecificDocuments.setDocType(false);
								
								if(uploadedFiles!=null && uploadedFiles.length>0){
										districtSpecificDocuments.setUploadFile(true);
								}
								else{
									districtSpecificDocuments.setUploadFile(false);
								}
								statelessSessionDSD.insert(districtSpecificDocuments);
								for(String file : uploadedFiles)
								{
									DistrictDocumentUpload  districtDocumentUpload = new DistrictDocumentUpload();
									districtDocumentUpload.setDistrictSpecificDocuments(districtSpecificDocuments);
									districtDocumentUpload.setUploadedFileName(file);										

									statelessSessionDDU.insert(districtDocumentUpload);
								}
							}
						}
						else
						{
							for(int i=0;i<jbCat.length;i++)
							{
								String ipAddress = IPAddressUtility.getIpAddress(request);
								DistrictSpecificDocuments districtSpecificDocuments = new DistrictSpecificDocuments();
								districtSpecificDocuments.setDocumentName(documentName);
								JobCategoryMaster jobCategoryMaster = map.get(Integer.parseInt(jbCat[i]));
								districtSpecificDocuments.setJobCategoryMaster(jobCategoryMaster);
								districtSpecificDocuments.setDistrictMaster(districtMaster);
								districtSpecificDocuments.setStatus("A");
								districtSpecificDocuments.setUserMaster(usermasters);
								districtSpecificDocuments.setIpAddress(ipAddress);
								if(documentFileMaster!=null)
								{
									districtSpecificDocuments.setDocumentFileMaster(documentFileMaster);
								}
								if(expDate!=null && expDate.length()>0)
								{
									districtSpecificDocuments.setExpireDate(expireDate);
								}
								
								if(docFlag==1)
									districtSpecificDocuments.setDocType(true);
								else
									districtSpecificDocuments.setDocType(false);
								
								if(uploadedFiles!=null && uploadedFiles.length>0){
										districtSpecificDocuments.setUploadFile(true);
								}
								else{
									districtSpecificDocuments.setUploadFile(false);
								}	
								statelessSessionDSD.insert(districtSpecificDocuments);
							}
						}

						result = "save";
						transactionDSD.commit();
						transactionDDU.commit();
						System.out.println("transaction commit");
						statelessSessionDSD.close();
						statelessSessionDDU.close();
					}
					else if(saveEditFlag==1 && jobDocsId!=0) // update
					{
						SessionFactory factoryDSD = districtSpecificDocumentsDAO.getSessionFactory();
						StatelessSession statelessSessionDSD=factoryDSD.openStatelessSession();

						SessionFactory factoryDDU = districtDocumentUploadDAO.getSessionFactory();
						StatelessSession statelessSessionDDU=factoryDDU.openStatelessSession();

						//Transaction transactionDSD=statelessSessionDSD.beginTransaction();
						//Transaction transactionDDU=statelessSessionDDU.beginTransaction();
						if(usermasters.getEntityType()==1)
						{
							districtMaster = districtMasterDAO.findById(districtId, false, false);
						}
						else if(usermasters.getEntityType()==2)
						{
							districtMaster = usermasters.getDistrictId();
						}
						
						DistrictSpecificDocuments districtSpecificDocuments = districtSpecificDocumentsDAO.findById(jobDocsId, false, false);
						if(districtSpecificDocuments!=null)
						{
							// for job category start
							if(jbCat.length==1)
							{
								String ipAddress = IPAddressUtility.getIpAddress(request);
								Transaction transactionDSD = statelessSessionDSD.beginTransaction();
								districtSpecificDocuments.setDocumentName(documentName);
								JobCategoryMaster jobCategoryMaster = new JobCategoryMaster();
								jobCategoryMaster = map.get(Integer.parseInt(jbCat[0]));
								districtSpecificDocuments.setJobCategoryMaster(jobCategoryMaster);
								districtSpecificDocuments.setUserMaster(usermasters);
								districtSpecificDocuments.setDistrictMaster(districtMaster);
								districtSpecificDocuments.setIpAddress(ipAddress);
								if(documentFileMaster!=null)
								{
									districtSpecificDocuments.setDocumentFileMaster(documentFileMaster);	
								}
								if(docFlag==1)
									districtSpecificDocuments.setDocType(true);
								else
									districtSpecificDocuments.setDocType(false);
								
								if(uploadedFiles!=null && uploadedFiles.length>0){
										districtSpecificDocuments.setUploadFile(true);
								}
								else{
									districtSpecificDocuments.setUploadFile(false);
								}
								
								if(expDate!=null && expDate.length()>0)
								{
									districtSpecificDocuments.setExpireDate(expireDate);
								}
								statelessSessionDSD.update(districtSpecificDocuments);
								transactionDSD.commit();
							}
							else
							{
								int count = 0;
								for(int i=0;i<jbCat.length;i++)
								{
									if(count==0)
									{
										Transaction transactionDSD = statelessSessionDSD.beginTransaction();
										String ipAddress = IPAddressUtility.getIpAddress(request);
										districtSpecificDocuments.setDocumentName(documentName);
										JobCategoryMaster jobCategoryMaster = new JobCategoryMaster();
										jobCategoryMaster = map.get(Integer.parseInt(jbCat[i])); 
										districtSpecificDocuments.setJobCategoryMaster(jobCategoryMaster);
										districtSpecificDocuments.setUserMaster(usermasters);
										districtSpecificDocuments.setDistrictMaster(districtMaster);
										districtSpecificDocuments.setIpAddress(ipAddress);
										if(documentFileMaster!=null)
										{
											districtSpecificDocuments.setDocumentFileMaster(documentFileMaster);	
										}
										if(expDate!=null && expDate.length()>0)
										{
											districtSpecificDocuments.setExpireDate(expireDate);
										}
										
										if(uploadedFiles!=null && uploadedFiles.length>0){
												districtSpecificDocuments.setUploadFile(true);
										}
										else{
											districtSpecificDocuments.setUploadFile(false);
										}
										
										if(docFlag==1)
											districtSpecificDocuments.setDocType(true);
										else
											districtSpecificDocuments.setDocType(false);
										
										statelessSessionDSD.update(districtSpecificDocuments);
										transactionDSD.commit();
									}
									else
									{
										String ipAddress = IPAddressUtility.getIpAddress(request);
										Transaction transactionDSD = statelessSessionDSD.beginTransaction();
										districtSpecificDocuments = new DistrictSpecificDocuments();
										districtSpecificDocuments.setDocumentName(documentName);
										JobCategoryMaster jobCategoryMaster = new JobCategoryMaster();
										jobCategoryMaster = map.get(Integer.parseInt(jbCat[i])); 
										districtSpecificDocuments.setJobCategoryMaster(jobCategoryMaster);
										districtSpecificDocuments.setDistrictMaster(districtMaster);
										districtSpecificDocuments.setUserMaster(usermasters);
										districtSpecificDocuments.setIpAddress(ipAddress);
										if(documentFileMaster!=null)
										{
											districtSpecificDocuments.setDocumentFileMaster(documentFileMaster);	
										}
										if(expDate!=null && expDate.length()>0)
										{
											districtSpecificDocuments.setExpireDate(expireDate);
										}
										if(uploadedFiles!=null && uploadedFiles.length>0)
												districtSpecificDocuments.setUploadFile(true);
										else
												districtSpecificDocuments.setUploadFile(false);
										
										if(docFlag==1)
											districtSpecificDocuments.setDocType(true);
										else
											districtSpecificDocuments.setDocType(false);
										statelessSessionDSD.insert(districtSpecificDocuments);
										transactionDSD.commit();
									}
									count++;									
								}
								// for job category  end
							}
							Criterion criterion = Restrictions.eq("districtSpecificDocuments", districtSpecificDocuments);
							List<DistrictDocumentUpload> districtDocumentUploadData = districtDocumentUploadDAO.findByCriteria(criterion);
							if(districtDocumentUploadData!=null && districtDocumentUploadData.size()>0)
							{
								if(districtDocumentUploadData.size()==uploadedFiles.length)
								{
									for(int i=0;i<districtDocumentUploadData.size();i++) 
									{
										Transaction transactionDDU=statelessSessionDDU.beginTransaction();
										DistrictDocumentUpload  districtDocumentUpload = districtDocumentUploadData.get(i);
										districtDocumentUpload.setDistrictSpecificDocuments(districtSpecificDocuments);
										districtDocumentUpload.setUploadedFileName(uploadedFiles[i]);										

										statelessSessionDDU.update(districtDocumentUpload);
										transactionDDU.commit();
									}
								}
								else if(districtDocumentUploadData.size()<uploadedFiles.length)
								{
									int uploadData = districtDocumentUploadData.size();
									int uploadFiles = uploadedFiles.length;
									for(int i=0;i<districtDocumentUploadData.size();i++) 
									{
										Transaction transactionDDU=statelessSessionDDU.beginTransaction();
										DistrictDocumentUpload  districtDocumentUpload = districtDocumentUploadData.get(i);
										districtDocumentUpload.setDistrictSpecificDocuments(districtSpecificDocuments);
										districtDocumentUpload.setUploadedFileName(uploadedFiles[i]);										

										statelessSessionDDU.update(districtDocumentUpload);
										transactionDDU.commit();
									}
									int newSize = 0;
									for(int i=0;i<uploadFiles-uploadData;i++) 
									{
										Transaction transactionDDU=statelessSessionDDU.beginTransaction();
										newSize = uploadData + i;
										DistrictDocumentUpload  districtDocumentUpload = new DistrictDocumentUpload();
										districtDocumentUpload.setDistrictSpecificDocuments(districtSpecificDocuments);
										districtDocumentUpload.setUploadedFileName(uploadedFiles[newSize]);										

										statelessSessionDDU.insert(districtDocumentUpload);
										transactionDDU.commit();
										newSize = 0;
									}
								}
								else if(districtDocumentUploadData.size() > uploadedFiles.length)
								{
									int uploadData = districtDocumentUploadData.size();
									int uploadFiles = uploadedFiles.length;
									Map<String, String> uploadedDATA = new HashMap<String, String>();
									for(DistrictDocumentUpload ddu : districtDocumentUploadData)
									{
										uploadedDATA.put(ddu.getUploadedFileName(), ddu.getUploadedFileName());
									}
									for(int i=0;i<uploadFiles;i++) 
									{
										Transaction transactionDDU=statelessSessionDDU.beginTransaction();
										DistrictDocumentUpload  districtDocumentUpload = districtDocumentUploadData.get(i);
										districtDocumentUpload.setDistrictSpecificDocuments(districtSpecificDocuments);
										districtDocumentUpload.setUploadedFileName(uploadedFiles[i]);										

										statelessSessionDDU.update(districtDocumentUpload);
										transactionDDU.commit();
									}
									int newSize = 0;
									for(int i=0;i<uploadData-uploadFiles;i++) 
									{
										Transaction transactionDDU=statelessSessionDDU.beginTransaction();
										newSize = uploadFiles + i;
										DistrictDocumentUpload  districtDocumentUpload = districtDocumentUploadData.get(newSize);
										statelessSessionDDU.delete(districtDocumentUpload);
										transactionDDU.commit();
									}
								}
								
							}
							else
							{
								if(uploadedFiles!=null && uploadedFiles.length>0)
								{
									for(String file : uploadedFiles)
									{
										DistrictDocumentUpload  districtDocumentUpload = new DistrictDocumentUpload();
										districtDocumentUpload.setDistrictSpecificDocuments(districtSpecificDocuments);
										districtDocumentUpload.setUploadedFileName(file);										

										statelessSessionDDU.insert(districtDocumentUpload);
									}
								}
							}
							
						}

						result = "update";
						System.out.println("transaction commit-update");
						statelessSessionDSD.close();
						statelessSessionDDU.close();
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();	
			}
		}
		return result;
	}

	public String displayJobsDocumentsRecord(String districtId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		System.out.println("================== displayJobsDocumentsRecord Grid ==========");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer tmRecords = new StringBuffer();
		List<DistrictSpecificDocuments> districtSpecificDocuments = new ArrayList<DistrictSpecificDocuments>();
		List<DistrictDocumentUpload> allRecord = new ArrayList<DistrictDocumentUpload>();
		List<Integer> filterList = new ArrayList<Integer>();
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		DistrictMaster districtMaster =null;
		SchoolMaster schoolMaster = null;
		if(session == null || session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			//System.out.println(" District Id :: "+districtId+" SchoolId :: "+schoolId);
			String sortOrderFieldName	=	"documentName";
			String sortOrderNoField		=	"documentName";
			int checkShortOrder=0;
			if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("documentName"))
				checkShortOrder=2;
			else if(sortOrder.equalsIgnoreCase("lastName") && userMaster.getEntityType()==1){
				checkShortOrder=1;
			}
			else if(sortOrder.equalsIgnoreCase("createdDateTime")){
				checkShortOrder=3;
			}
			else if(sortOrder.equalsIgnoreCase("jobCategoryName")){
				checkShortOrder=4;
			}

			System.out.println("noofRow :: "+noOfRow+" pageNo :: "+sortOrder+" sortOrderType :: "+sortOrderType);

			Order  sortOrderStrVal=null;
			Boolean flag = false;
			if(sortOrder!=null){	
				flag = true;
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("lastName") && userMaster.getEntityType()==1)
				{
					sortOrderNoField="lastName";
				}
				if(sortOrder.equals("documentName"))
				{
					sortOrderNoField="documentName";
				}
				if(sortOrder.equals("createdDateTime"))
				{
					sortOrderNoField="createdDateTime";
				}
				if(sortOrder.equals("jobCategoryName"))
				{
					sortOrderNoField="jobCategoryName";
				}
				if(sortOrder.equals("firstName"))
				{
					sortOrderNoField="firstName";
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("")){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			System.out.println("rows :: "+noOfRowInPage+" pgNo :: "+pgNo+" start :: "+start+" end :: "+end);	
			System.out.println(" district Id :: "+districtId);
			if(userMaster!=null)
			{	
				if(userMaster.getEntityType()==2)
				{					
					System.out.println(" District Search");
					districtMaster = districtMasterDAO.findByDistrictId(districtId);
					try {
						allRecord = districtDocumentUploadDAO.findAll();
						filterList = districtSpecificDocumentsDAO.getAllRecords(districtMaster);
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(filterList!=null && filterList.size()>0)
					{
						districtSpecificDocuments = districtSpecificDocumentsDAO.getJobDocumentsDetails(filterList,checkShortOrder,sortOrderStrVal,start,end,flag);
						totalRecord = districtSpecificDocumentsDAO.getTotalJobDocumentsDetails(filterList,checkShortOrder,sortOrderStrVal,start,end);
					}
				}
				else if(userMaster.getEntityType()==1)
				{
					if(districtId.length()==0)
					{
						System.out.println("Initial Search");
						try {
							//Criterion criterion = Restrictions.eq("districtId", districtMaster);
							allRecord = districtDocumentUploadDAO.findAll();
						} catch (Exception e) {
							e.printStackTrace();
						}
						if(filterList!=null && filterList.size()>0)
						{
							districtSpecificDocuments = districtSpecificDocumentsDAO.getJobDocumentsDetails(filterList,checkShortOrder,sortOrderStrVal,start,end,flag);
							totalRecord = districtSpecificDocumentsDAO.getTotalJobDocumentsDetails(filterList,checkShortOrder,sortOrderStrVal,start,end);
						}
					}
					else if(districtId.length() > 0)
					{
						System.out.println(" District Search");
						districtMaster=districtMasterDAO.findByDistrictId(districtId);
						try {
							allRecord = districtDocumentUploadDAO.findAll();
							filterList = districtSpecificDocumentsDAO.getAllRecords(districtMaster);
						} catch (Exception e) {
							e.printStackTrace();
						}
						if(filterList!=null && filterList.size()>0)
						{
							districtSpecificDocuments = districtSpecificDocumentsDAO.getJobDocumentsDetails(filterList,checkShortOrder,sortOrderStrVal,start,end,flag);
							totalRecord = districtSpecificDocumentsDAO.getTotalJobDocumentsDetails(filterList,checkShortOrder,sortOrderStrVal,start,end);
						}
					}				
				}
			}

			if(totalRecord<end)
				end=totalRecord;

			tmRecords.append("<table id='jobDocumentsTable' width='100%' class='tablecgtbl'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			String responseText="";

			responseText=PaginationAndSorting.responseSortingLink(""+Utility.getLocaleValuePropByKey("lbllDocName", locale)+"",sortOrderFieldName,"documentName",sortOrderTypeVal,pgNo);		
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobCat", locale),sortOrderFieldName,"jobCategoryName",sortOrderTypeVal,pgNo);		
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblDoc", locale)+"</th>");
			if(userMaster.getEntityType()==1)
			{
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCandidateName", locale),sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			}
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblUploadedDate", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			int counter=0 ;
			Map<Integer,Integer> map = new HashMap<Integer,Integer>();
			Map<Integer,DistrictDocumentUpload> filesMap = new HashMap<Integer, DistrictDocumentUpload>();
			int valCount = 0;
			for(DistrictDocumentUpload  dsd : allRecord)
			{
				map.put(dsd.getDistrictSpecificDocuments().getDocumentId(), map.get(dsd.getDistrictSpecificDocuments().getDocumentId())==null?1:map.get(dsd.getDistrictSpecificDocuments().getDocumentId())+1);
				filesMap.put(dsd.getDistrictSpecificDocuments().getDocumentId(), dsd);
			}
			System.out.println(" map size :: "+map.size());
			if(districtSpecificDocuments!=null && districtSpecificDocuments.size()>0)
			{
				for(DistrictSpecificDocuments jbDoc : districtSpecificDocuments)
				{
					counter++;
					String gridColor="class='bggrid'";
					if(counter%2==0){
						gridColor="style='background-color:white;'";
					}
					tmRecords.append("<tr "+gridColor+">");
					tmRecords.append("<td>"+jbDoc.getDocumentName()+"</td>");
					tmRecords.append("<td>"+jbDoc.getJobCategoryMaster().getJobCategoryName()+"</td>");

					if(map!=null && map.containsKey(jbDoc.getDocumentId()))
					{
						valCount = map.get(jbDoc.getDocumentId());
						if(valCount>1)
						{
							tmRecords.append("<td><a href='javascript:void(0)' id='docToolTip"+counter+"' rel='tooltip' data-original-title='View Documents' onclick=\"viewMultipleJobDoc('"+jbDoc.getDocumentName()+"',"+jbDoc.getJobCategoryMaster().getJobCategoryId()+");\"><img src='images/option06.png' style='width:21px; height:21px;'></a></td>");
						}
						else
						{
							if(jbDoc.getUploadFile()!=null && jbDoc.getUploadFile())
							{
								if(filesMap.size()>0)
								{
									tmRecords.append("<td><a href='javascript:void(0)' id='docToolTip"+counter+"' rel='tooltip' data-original-title='View Document' onclick=\"downloadJobDoc('"+jbDoc.getUserMaster().getUserId()+"','"+filesMap.get(jbDoc.getDocumentId()).getUploadedFileName()+"',"+jbDoc.getJobDocsId()+")\">"+filesMap.get(jbDoc.getDocumentId()).getUploadedFileName()+"</a></td>");	
								}
							}
							else
							{
								if(jbDoc.getDocumentFileMaster()!=null)
								{
									tmRecords.append("<td><a href='javascript:void(0)' id='docToolTip"+counter+"' rel='tooltip' data-original-title='View Document' onclick=\"showDefDocs("+jbDoc.getDocumentFileMaster().getDocumentCategoryMaster().getDocumentCategoryId()+")\">"+jbDoc.getDocumentFileMaster().getDocumentCategoryMaster().getDocumentCategory()+"</a></td>");
								}
								else
								{
									tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
								}
							}
						}
					}
					else
					{
						if(jbDoc.getDocumentFileMaster()!=null)
						{
							tmRecords.append("<td><a href='javascript:void(0)' id='docToolTip"+counter+"' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgViewDocument", locale)+"' onclick=\"showDefDocs("+jbDoc.getDocumentFileMaster().getDocumentCategoryMaster().getDocumentCategoryId()+")\">"+jbDoc.getDocumentFileMaster().getDocumentCategoryMaster().getDocumentCategory()+"</a>&nbsp;&nbsp;- "+Utility.getLocaleValuePropByKey("msgDefault3", locale)+"</td>");
						}
						else
						{
							tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
						}
					}

					if(userMaster.getEntityType()==1)
					{
						tmRecords.append("<td>"+jbDoc.getUserMaster().getFirstName()+" "+jbDoc.getUserMaster().getLastName()+"<br/>\""+jbDoc.getUserMaster().getEmailAddress()+"\""+"</td>");
					}
					tmRecords.append("<td>"+Utility.convertDateAndTimeFormatForEpiAndJsi(jbDoc.getCreatedDateTime())+"</td>");
					tmRecords.append("<script>$('#docToolTip"+counter+"').tooltip();</script>");
					tmRecords.append("<td><a href='javascript:void(0)' id='docEditToolTip"+counter+"' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgEditDocument", locale)+"' onclick=\"editJobDoc("+jbDoc.getJobDocsId()+","+jbDoc.getDistrictMaster().getDistrictId()+",'"+jbDoc.getDocumentName()+"',"+jbDoc.getJobCategoryMaster().getJobCategoryId()+")\"><i class='icon-edit icon-large iconcolor'></i></a>");
					tmRecords.append("<script>$('#docEditToolTip"+counter+"').tooltip();</script>");
					tmRecords.append("&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0);' id='removeDocTooTip"+counter+"' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgRemoveDocument", locale)+"' onclick=\"openRemoveDocModal("+jbDoc.getJobDocsId()+")\"><i class='icon-remove icon-large iconcolor'></i></a></td>");
					tmRecords.append("<script>$('#removeDocTooTip"+counter+"').tooltip();</script>");
					tmRecords.append("</tr>");
				}
			}
			else
			{
				tmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForTeacherInfoAjax(request,totalRecord,noOfRow, pageNo));
			
			map.clear();
			filesMap.clear();
		}
		
		return tmRecords.toString();
	}


	public String downloadDocument(int uploadedBy,String docName, int userId)
	{
		System.out.println(new Date()+" Calling Download Document ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		//JobDocumentsByDistrict jbDocList = jobDocumentsByDistrictDAO.findById(userId, false, false);
		String path="";
		try 
		{
			UserMaster userMaster = userMasterDAO.findById(uploadedBy, false, false);

			if(userMaster!=null)
			{
				String source = "";
				String target = "";
				if(userMaster.getEntityType()==2)
				{					
					source = Utility.getValueOfPropByKey("districtRootPath")+userMaster.getDistrictId().getDistrictId()+"/JobDocuments/"+docName;
					System.out.println(" Job Document source "+source);

					target = context.getServletContext().getRealPath("/")+"/"+"/district/"+userMaster.getDistrictId().getDistrictId()+"/JobDocuments";
					System.out.println(" Job Document target "+target);	
				}
				else if(userMaster.getEntityType()==1)
				{/*
					source = Utility.getValueOfPropByKey("tmRootPath")+userMaster.getDistrictId().getDistrictId()+"/JobDocuments/"+docName;
					System.out.println(" Job Document source "+source);

					target = context.getServletContext().getRealPath("/")+"/"+"/TM/"+uploadedBy+"/JobDocuments";
					System.out.println(" Job Document target "+target);
				*/}
				File sourceFile = new File(source);
				File targetDir = new File(target);
				if(!targetDir.exists())
					targetDir.mkdirs();
				File targetFile = new File(targetDir+"/"+sourceFile.getName());
				FileUtils.copyFile(sourceFile, targetFile);
				if(userMaster.getEntityType()==2)
				{
					path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+userMaster.getDistrictId().getDistrictId()+"/JobDocuments/"+sourceFile.getName();
				}
				else if(userMaster.getEntityType()==1)
				{/*
					path = Utility.getValueOfPropByKey("contextBasePath")+"/TM/"+uploadedBy+"/JobDocuments/"+sourceFile.getName();
				*/}
				System.out.println(" Job Document path "+path);
			}
			else
			{
				System.out.println(" Job Document is null or Blank");
			}
		} 
		catch (Exception e) 
		{
			System.out.println(" Error in Job Document ");
			e.printStackTrace();
		}
		return path;
	}	

	public String getJobCategories(int districtId)
	{
		System.out.println("==============Reset Job Categories==============");
		UserMaster userSession=null;
		StringBuffer sb = new StringBuffer();
		try 
		{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();

			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			userSession		=	(UserMaster) session.getAttribute("userMaster");
			List<JobCategoryMaster> jobCategoryMasters = new ArrayList<JobCategoryMaster>();
			if(userSession.getEntityType()==2)
			{
				DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
				jobCategoryMasters = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(userSession.getDistrictId());
			}
			else if(userSession.getEntityType()==1 && districtId==0)
			{
				jobCategoryMasters = jobCategoryMasterDAO.findAll();				
			}
			else if(userSession.getEntityType()==1 && districtId!=0)
			{
				DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
				jobCategoryMasters = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);				
			}
			/*	List<JobDocumentsByDistrict> jbDocList =new ArrayList<JobDocumentsByDistrict>();
			jbDocList = jobDocumentsByDistrictDAO.checkJobCategoryExists(userSession);
			if(jbDocList.size()==0)
			{				
				//System.out.println(" Job Categories Get :: "+jobCategoryMasters.size());
			}
			else
			{
				List<JobCategoryMaster> jobCategoryUsedList =new ArrayList<JobCategoryMaster>();
				for(int i=0;i<jbDocList.size();i++)
				{
					jobCategoryUsedList.add(jbDocList.get(i).getJobCategoryMaster());
				}
				jobCategoryMasters.removeAll(jobCategoryUsedList);
				if(jobCategoryMasters!=null && jobCategoryMasters.size()>0)
				{
					for(JobCategoryMaster jb : jobCategoryMasters)
					{
						sb.append("<option value="+jb.getJobCategoryId()+">"+jb.getJobCategoryName()+"</option>");
					}
				}
				else
				{
					sb.append("<option value='-1'>All Job Categories Attached</option>");
				}
			}*/
			if(jobCategoryMasters!=null && jobCategoryMasters.size()>0)
			{
				for(JobCategoryMaster jb : jobCategoryMasters)
				{
					sb.append("<option value="+jb.getJobCategoryId()+">"+jb.getJobCategoryName()+"</option>");
				}
			}
			else
			{
				sb.append("<option value='-1'>"+Utility.getLocaleValuePropByKey("msgNoCategoryFound", locale)+"</option>");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public String removeJobDocumentsRecord(int jobDocId)
	{

		System.out.println("============== Remove jobDocumentsRecord ==============");
		System.out.println(" Remove record for JobId :: "+jobDocId);
		String result = "";
		DistrictSpecificDocuments districtSpecificDocuments = districtSpecificDocumentsDAO.findById(jobDocId, false, false);
		try {
			if(districtSpecificDocuments!=null)
			{
				System.out.println(" record found......");
				SessionFactory factoryDDU = districtDocumentUploadDAO.getSessionFactory();
				StatelessSession statelessSessionDDU=factoryDDU.openStatelessSession();
				Transaction transactionDDU = statelessSessionDDU.beginTransaction();
				
				Criterion criterion = Restrictions.eq("districtSpecificDocuments", districtSpecificDocuments);
				List<DistrictDocumentUpload> districtDocumentUpload = districtDocumentUploadDAO.findByCriteria(criterion);
				if(districtDocumentUpload!=null && districtDocumentUpload.size()>0)
				{
					for(DistrictDocumentUpload ddu : districtDocumentUpload)
					{
						statelessSessionDDU.delete(ddu);					
					}
				}
				districtSpecificDocumentsDAO.makeTransient(districtSpecificDocuments);
				transactionDDU.commit();
				result = "success";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}


	public String getJobCategoryForEdit(int jobDocId)
	{
		StringBuffer sb = new StringBuffer();
		UserMaster userSession=null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null){
			return "redirect:index.jsp";
		}
		List<JobCategoryMaster> jobCategoryMasters = new ArrayList<JobCategoryMaster>();
		userSession		=	(UserMaster) session.getAttribute("userMaster");
		if(userSession.getEntityType()==2)
		{
			jobCategoryMasters = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(userSession.getDistrictId());
		}
		else
		{
			jobCategoryMasters = jobCategoryMasterDAO.findAll();
		}
		/*JobDocumentsByDistrict jbDocList =new JobDocumentsByDistrict();
		jbDocList = jobDocumentsByDistrictDAO.findById(jobDocId, false, false);
		if(jbDocList!=null)
		{				
			// List<JobCategoryMaster> jobCategoryUsedList =new ArrayList<JobCategoryMaster>();
			// jobCategoryMasters.removeAll(jobCategoryUsedList);
			if(jobCategoryMasters!=null && jobCategoryMasters.size()>0)
			{
				for(int i=0;i<jobCategoryMasters.size();i++)
				{
					if(jbDocList.getJobCategoryMaster().getJobCategoryId().equals(jobCategoryMasters.get(i).getJobCategoryId()))
					{							
						sb.append("<option value="+jobCategoryMasters.get(i).getJobCategoryId()+" selected>"+jobCategoryMasters.get(i).getJobCategoryName()+"</option>");
					}
					else
					{
						sb.append("<option value="+jobCategoryMasters.get(i).getJobCategoryId()+">"+jobCategoryMasters.get(i).getJobCategoryName()+"</option>");
					}
				}
			}
			else
			{
				sb.append("<option value='-1'>None</option>");
			}
		}*/
		return sb.toString();
	}

	public String getDetailsByJobDocsId(String documentName, int jobCategoryId)
	{
		StringBuffer sb = new StringBuffer();
		UserMaster userSession=null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null){
			return "redirect:index.jsp";
		}
		userSession		=	(UserMaster) session.getAttribute("userMaster");
		String s ="";
		try {

			Criterion criterion = Restrictions.eq("documentName", documentName);
			JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
			Criterion criterion2 = Restrictions.eq("jobCategoryMaster", jobCategoryMaster);
			List<DistrictSpecificDocuments> jobDocumentsByDistrict = districtSpecificDocumentsDAO.findByCriteria(criterion,criterion2);
			Criterion criterion3 = Restrictions.eq("districtSpecificDocuments", jobDocumentsByDistrict.get(0));
			List<DistrictDocumentUpload> documentUploads = districtDocumentUploadDAO.findByCriteria(criterion3);

			if(documentUploads!=null)
			{
				if(documentUploads.size()==1)
				{
					sb.append(documentUploads.get(0).getUploadedFileName());
					//sb.append("@@@");
				}
				else
				{
					for(DistrictDocumentUpload dsd : documentUploads)
					{
						sb.append(s).append(dsd.getUploadedFileName());
						s = "@@@";
					}
				}
			}
			//System.out.println("get Details :: "+sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public String getSelectedJobCategory(int jobDocId)
	{
		StringBuffer sb = new StringBuffer();
		UserMaster userSession=null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null){
			return "redirect:index.jsp";
		}
		userSession		=	(UserMaster) session.getAttribute("userMaster");
		try {
			DistrictSpecificDocuments districtSpecificDocuments = districtSpecificDocumentsDAO.findById(jobDocId, false, false);
			if(districtSpecificDocuments!=null)
			{	
				sb.append("<option value="+districtSpecificDocuments.getJobCategoryMaster().getJobCategoryId()+">"+districtSpecificDocuments.getJobCategoryMaster().getJobCategoryName()+"</option>");
			}
			else
			{
				sb.append("<option value='-1'>"+Utility.getLocaleValuePropByKey("lblNone", locale)+"</option>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public String getRemSelectedJobCategory(int jobDocId)
	{
		System.out.println("===== getRemSelectedJobCategory =====");
		StringBuffer sb = new StringBuffer();
		UserMaster userSession=null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null){
			return "redirect:index.jsp";
		}
		List<JobCategoryMaster> jobCategoryMasters = new ArrayList<JobCategoryMaster>();
		userSession		=	(UserMaster) session.getAttribute("userMaster");
		if(userSession.getEntityType()==2)
		{
			jobCategoryMasters = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(userSession.getDistrictId());
		}
		else
		{
			jobCategoryMasters = jobCategoryMasterDAO.findAll();
		}
		try {
			DistrictSpecificDocuments jbDocList =new DistrictSpecificDocuments();
			jbDocList = districtSpecificDocumentsDAO.findById(jobDocId, false, false);
			if(jbDocList!=null)
			{				
				if(jobCategoryMasters!=null && jobCategoryMasters.size()>0)
				{
					for(int i=0;i<jobCategoryMasters.size();i++)
					{
						if(!jobCategoryMasters.get(i).getJobCategoryId().equals(jbDocList.getJobCategoryMaster().getJobCategoryId()))
						{							
							sb.append("<option value="+jobCategoryMasters.get(i).getJobCategoryId()+">"+jobCategoryMasters.get(i).getJobCategoryName()+"</option>");
						}
					}
				}
				else
				{
					sb.append("<option value='-1'>"+Utility.getLocaleValuePropByKey("lblNone", locale)+"</option>");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public String displayJobsDocuments(String teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{/*
		System.out.println("================== displayJobsDocuments Grid ==========");
		 ========  For Session time Out Error =========
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer tmRecords = new StringBuffer();
		List<JobDocumentsByDistrict> jobDocumentsByDistricts = new ArrayList<JobDocumentsByDistrict>();
		List<JobDocumentsByDistrict> jobDocumentFinal = new ArrayList<JobDocumentsByDistrict>();
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		List<JobForTeacher> jobForTeachers = new ArrayList<JobForTeacher>();
		Map<Integer,JobForTeacher> mapJbF=new HashMap<Integer, JobForTeacher>();

		DistrictMaster districtMaster =null;
		SchoolMaster schoolMaster = null;
		if(session == null || teacherDetail==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			Criterion criterion = Restrictions.eq("teacherId",teacherDetail);
			jobForTeachers = jobForTeacherDAO.findByCriteria(criterion);

	 *//** set default sorting fieldName **//*
			String sortOrderFieldName	=	"districtName";
			String sortOrderNoField		=	"districtName";
			int checkShortOrder=0;
			if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("districtName"))
				checkShortOrder=2;
			else if(sortOrder.equalsIgnoreCase("docsName")){
				checkShortOrder=1;
			}
			else if(sortOrder.equalsIgnoreCase("uploadedFileName")){
				checkShortOrder=3;
			}
			System.out.println("noofRow :: "+noOfRow+" pageNo :: "+sortOrder+" sortOrderType :: "+sortOrderType);

			Order  sortOrderStrVal=null;
			Boolean flag = false;
			if(sortOrder!=null){	
				flag = true;
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("districtName"))
				{
					sortOrderNoField="districtName";
				}
				if(sortOrder.equals("docsName"))
				{
					sortOrderNoField="docsName";
				}
				if(sortOrder.equals("uploadedFileName"))
				{
					sortOrderNoField="uploadedFileName";
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("")){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			System.out.println("rows :: "+noOfRowInPage+" pgNo :: "+pgNo+" start :: "+start+" end :: "+end);	
			jobDocumentsByDistricts = jobDocumentsByDistrictDAO.findAll();
			List<Integer> lstJobDocs= new ArrayList<Integer>();
			if(jobForTeachers!=null && jobDocumentsByDistricts!=null)
			{
				for(JobDocumentsByDistrict jbDoc : jobDocumentsByDistricts)
				{
					for(JobForTeacher jbft : jobForTeachers)
					{
						if(jbft.getJobId().getDistrictMaster().getDistrictId().equals(jbDoc.getDistrictMaster().getDistrictId())){
							lstJobDocs.add(jbDoc.getJobDocsId());	
						}
					}
				}
			}
			jobDocumentFinal = jobDocumentsByDistrictDAO.getJobDocumentsForCandidate(lstJobDocs,checkShortOrder,sortOrderStrVal,start,end,flag);
			if(totalRecord<end)
				end=totalRecord;

			tmRecords.append("<table id='jobDocumentsTable' width='100%' class='tablecgtbl'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("District Name",sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);		
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			responseText=PaginationAndSorting.responseSortingLink(""+Utility.getLocaleValuePropByKey("lbllDocName", locale)+"",sortOrderFieldName,"docsName",sortOrderTypeVal,pgNo);		
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobCat", locale),sortOrderFieldName,"jobCategory",sortOrderTypeVal,pgNo);
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			int counter=0 ;
			if(jobDocumentsByDistricts!=null && jobDocumentsByDistricts.size()>0)
			{
				for(JobDocumentsByDistrict jbDoc : jobDocumentFinal)
				{
					counter++;
					String gridColor="class='bggrid'";
					if(counter%2==0){
						gridColor="style='background-color:white;'";
					}
					tmRecords.append("<tr "+gridColor+">");
					tmRecords.append("<td>"+jbDoc.getDistrictMaster().getDistrictName()+"</td>");
					tmRecords.append("<td>"+jbDoc.getDocsName()+"</td>");
					tmRecords.append("<td>"+jbDoc.getJobCategoryMaster().getJobCategoryName()+"</td>");
					//tmRecords.append("<td>"+Utility.convertDateAndTimeFormatForEpiAndJsi(jbDoc.getCreatedDateTime())+"</td>");
					tmRecords.append("<td><a href='javascript:void(0)' id='docToolTip"+counter+"' rel='tooltip' data-original-title='View Document' onclick=\"downloadJobDoc('"+jbDoc.getUserMaster().getUserId()+"','"+jbDoc.getUploadedFileName()+"',"+jbDoc.getJobDocsId()+")\"><img src='images/option06.png' style='width:21px; height:21px;'></a>");
					tmRecords.append("<script>$('#docToolTip"+counter+"').tooltip();</script>");
					tmRecords.append("&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' id='docEditToolTip"+counter+"' rel='tooltip' data-original-title='Edit Document' onclick=\"editJobDoc("+jbDoc.getJobDocsId()+","+jbDoc.getDistrictMaster().getDistrictId()+")\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
					tmRecords.append("<script>$('#docEditToolTip"+counter+"').tooltip();</script>");
					tmRecords.append("&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0);' id='removeDocTooTip"+counter+"' rel='tooltip' data-original-title='Remove Document' onclick=\"openRemoveDocModal("+jbDoc.getJobDocsId()+")\"><img src='images/option03.png' style='width:21px; height:21px;'></a></td>");
					tmRecords.append("<script>$('#removeDocTooTip"+counter+"').tooltip();</script>");
					tmRecords.append("</tr>");
				}
			}
			else
			{
				tmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForTeacherInfoAjax(request,totalRecord,noOfRow, pageNo));
		}
		return tmRecords.toString();
	  */
		return "";
	}

	public String showDocumentList()
	{
		System.out.println(" ===== showDocumentList =====");
		StringBuffer sb = new StringBuffer();
		UserMaster userSession=null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null){
			return "redirect:index.jsp";
		}
		userSession		=	(UserMaster) session.getAttribute("userMaster");
		List<DocumentFileMaster> documentFileMasters = documentFileMasterDAO.findAll();
		System.out.println(" jobDocumentMasters :: "+documentFileMasters.size());
		sb.append("<option value='-1'>Select Document</option>");
		if(documentFileMasters!=null)
		{
			for(DocumentFileMaster jbMaster : documentFileMasters)
			{
				sb.append("<option value="+jbMaster.getDocumentFileId()+">"+jbMaster.getDocumentCategoryMaster().getDocumentCategory()+"</option>");
			}
		}
		return sb.toString();
	}

	public String viewDefaultDocument(int docFileId)
	{
		System.out.println(new Date()+" Calling viewDefaultDocument ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		List<DocumentFileMaster> documentFileMaster = new ArrayList<DocumentFileMaster>();
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		if(docFileId!=0)
		{
			Criterion criterion = Restrictions.eq("documentFileId", docFileId);
			documentFileMaster = documentFileMasterDAO.findByCriteria(criterion);
		}

		String path="";
		try 
		{
			UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");

			if(userMaster!=null && documentFileMaster.size()>0)
			{
				String source = "";
				String target = "";

				source = Utility.getValueOfPropByKey("rootPath")+"/Job Documents/"+documentFileMaster.get(0).getFileName();
				System.out.println(" Job Document source "+source);

				target = context.getServletContext().getRealPath("/")+"/Job Documents";
				System.out.println(" Job Document target "+target);	

				File sourceFile = new File(source);
				File targetDir = new File(target);
				if(!targetDir.exists())
					targetDir.mkdirs();
				File targetFile = new File(targetDir+"/"+sourceFile.getName());
				FileUtils.copyFile(sourceFile, targetFile);
				path = Utility.getValueOfPropByKey("contextBasePath")+"/Job Documents/"+sourceFile.getName();
				//path = source;
				System.out.println(" Default Job Document path "+path);
			}
			else
			{
				System.out.println(" Default Job Document is null or Blank");
			}
		} 
		catch (Exception e) 
		{
			System.out.println(" Error in Default Job Document ");
			e.printStackTrace();
		}
		return path;
	}

	public String showDefaultDocumentName(Integer docFileId)
	{
		System.out.println(new Date()+" Calling Download Document ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DocumentFileMaster documentFileMaster = new DocumentFileMaster();
		String fileName="";
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		System.out.println(" docFileId :: "+docFileId);
		if(docFileId!=null && docFileId!=0)
		{
			try {
				documentFileMaster = documentFileMasterDAO.findById(docFileId, false, false);
				if(documentFileMaster!=null)
				{
					fileName = documentFileMaster.getFileName();	
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return fileName;
	}

	public String  displayUploadedFiles(String[] uploadedFiles){
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		System.out.println(" arry length :: "+uploadedFiles.length);
		StringBuffer sb =	new StringBuffer();
		try{
			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
			}
			if(uploadedFiles!=null && uploadedFiles.length>0 && uploadedFiles.length==1)
			{
				sb.append("<span style='margin-top:10px;margin-left:10px;' nowrap>" +
						"<a href='javascript:void(0);' onclick=\"downloadJobDoc("+userMaster.getUserId()+",'"+uploadedFiles[0]+"',"+userMaster.getUserId()+");\">"+uploadedFiles[0]+"</a></span>" );
				sb.append("<a href=\"javascript:void(0);\" onclick=\"removeUploadedFiles('"+uploadedFiles[0]+"');\">&nbsp;&nbsp;<img width=\"15\" height=\"15\"  class=\"can\" src=\"images/can-icon.png\"></a>" );
			}
			else if(uploadedFiles!=null && uploadedFiles.length>1)
			{
				for (int i=0 ;i<uploadedFiles.length;i++) 
				{
					//System.out.println("uploadedFiles[i] :: "+uploadedFiles[i]);
					if(!uploadedFiles[i].equalsIgnoreCase(""))
					{			
						sb.append("<span style='margin-top:10px;margin-left:10px;' nowrap>" +
								"<a href='javascript:void(0);' onclick=\"downloadJobDoc("+userMaster.getUserId()+",'"+uploadedFiles[i]+"',"+userMaster.getUserId()+");\">"+uploadedFiles[i]+"</a></span>" );
						sb.append("<a href=\"javascript:void(0);\" onclick=\"removeUploadedFiles('"+uploadedFiles[i]+"');\">&nbsp;&nbsp;<img width=\"15\" height=\"15\"  class=\"can\" src=\"images/can-icon.png\"></a>" );
					}
				}
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}

	public String showDocumentListById(Integer documentId)
	{
		System.out.println(" ===== showDocumentList =====");
		StringBuffer sb = new StringBuffer();
		UserMaster userSession=null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null){
			return "redirect:index.jsp";
		}
		userSession		=	(UserMaster) session.getAttribute("userMaster");
		try {
			DistrictSpecificDocuments districtSpecificDocuments = districtSpecificDocumentsDAO.findById(documentId, false, false);
			List<DocumentFileMaster> documentFileMasters = documentFileMasterDAO.findAll();
			System.out.println(" jobDocumentMasters :: "+documentFileMasters.size());
			boolean found = false;
			sb.append("<option value='-1'>"+Utility.getLocaleValuePropByKey("sltDoc", locale)+"</option>");
			if(districtSpecificDocuments!=null)
			{
				if(documentFileMasters!=null)
				{
					for(DocumentFileMaster jbMaster : documentFileMasters)
					{
						if(districtSpecificDocuments.getDocumentFileMaster()!=null)
						{
							if(districtSpecificDocuments.getDocumentFileMaster().getDocumentFileId().equals(jbMaster.getDocumentFileId()))
							{
								sb.append("<option value="+jbMaster.getDocumentFileId()+" selected>"+jbMaster.getDocumentCategoryMaster().getDocumentCategory()+"</option>");
								//found = true;
							}
						}
						else
							sb.append("<option value="+jbMaster.getDocumentFileId()+">"+jbMaster.getDocumentCategoryMaster().getDocumentCategory()+"</option>");
					}
				}
				sb.append("@@@");
				sb.append(districtSpecificDocuments.getDocumentName());
				sb.append("@@@");
				if(districtSpecificDocuments.getExpireDate()!=null)
					sb.append(Utility.convertDateAndTimeToDatabaseformatOnlyDate(districtSpecificDocuments.getExpireDate()));
				else
					sb.append("N/A");
				if(districtSpecificDocuments.getDocType()!=null && districtSpecificDocuments.getDocType())
				{
					found = true;
				}
				if(!found)
				{
					sb.append("@@@");
					sb.append("notfound");
				}
				else
				{
					sb.append("@@@");
					sb.append("found");
				}
			}


		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public String showMultipleDocuments(String documentName, int jobCategoryId)
	{
		System.out.println(" ===== showMultipleDocuments =====");
		StringBuffer sb = new StringBuffer();
		UserMaster userSession=null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null){
			return "redirect:index.jsp";
		}
		userSession		=	(UserMaster) session.getAttribute("userMaster");

		try {
			int counter = 0;
			Criterion criterion = Restrictions.eq("documentName", documentName);
			JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
			Criterion criterion2 = Restrictions.eq("jobCategoryMaster", jobCategoryMaster);
			List<DistrictSpecificDocuments> districtSpecificDocuments = districtSpecificDocumentsDAO.findByCriteria(criterion,criterion2);
			Criterion criterion3 = Restrictions.eq("districtSpecificDocuments", districtSpecificDocuments.get(0));
			List<DistrictDocumentUpload> documentUploads = districtDocumentUploadDAO.findByCriteria(criterion3);

			if(documentUploads!=null && documentUploads.size()>0)
			{
				sb.append("<table id='multiDocTable' width='100%' class='tablecgtbl'>");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				sb.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lbllDocName", locale)+"</th>");			
				sb.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblJoCatN", locale)+"</th>");
				sb.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblDoc", locale)+"</th>");
				sb.append("</tr>");
				sb.append("</thead>");
				for(DistrictDocumentUpload dsd : documentUploads)
				{
					counter++;
					String gridColor="class='bggrid'";
					if(counter%2==0){
						gridColor="style='background-color:white;'";
					}
					sb.append("<tr "+gridColor+">");
					sb.append("<td>"+dsd.getDistrictSpecificDocuments().getDocumentName()+"</td>");
					sb.append("<td>"+dsd.getDistrictSpecificDocuments().getJobCategoryMaster().getJobCategoryName()+"</td>");
					sb.append("<td><a href='javascript:void(0)' id='docToolTipNew"+counter+"' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgViewDocument", locale)+"' onclick=\"downloadJobDocNew('"+dsd.getDistrictSpecificDocuments().getUserMaster().getUserId()+"','"+dsd.getUploadedFileName()+"',"+dsd.getDistrictSpecificDocuments().getJobDocsId()+","+counter+")\">"+dsd.getUploadedFileName()+"</a></td>");
					sb.append("</tr>");
				}
				sb.append("</table>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	
   public String checkforDuplicasy(String districtHiddenId,String defaultDocId,String arrReqNum,String flag,String jobCatId)
	{
		String errorMsg="";
		UserMaster userSession=null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null){
			return "redirect:index.jsp";
		}
		DocumentFileMaster fileMaster= documentFileMasterDAO.findById(Integer.parseInt(defaultDocId), false, false);  
		userSession		=	(UserMaster) session.getAttribute("userMaster");
		DistrictMaster districtMaster= districtMasterDAO.findById(Integer.parseInt(districtHiddenId), false, false);
        Map<String,DistrictSpecificDocuments> mapdistrictspecificdocuments= new  HashMap<String, DistrictSpecificDocuments>();
        List<DistrictSpecificDocuments> lstDistrictSpecificDocuments= new ArrayList<DistrictSpecificDocuments>();
        lstDistrictSpecificDocuments=districtSpecificDocumentsDAO.findByDistrict(districtMaster);
          if(lstDistrictSpecificDocuments!=null && lstDistrictSpecificDocuments.size()>0){
        		for(DistrictSpecificDocuments dsd : lstDistrictSpecificDocuments){
        			if(dsd.getDocumentFileMaster()!=null)
        			  mapdistrictspecificdocuments.put(dsd.getDocumentFileMaster().getDocumentFileId()+"", dsd);
        		}
          }
        JobCategoryMaster jobCategoryMaster=null;
		try {
			if(!jobCatId.equals("0")){
				jobCategoryMaster = jobCategoryMasterDAO.findById(Integer.parseInt(jobCatId), false, false);
			}
			
			List<Integer> listjobCatIds = new ArrayList<Integer>();
			List<JobCategoryMaster> lstJobCategoryMasters= new ArrayList<JobCategoryMaster>(); 
			
			String catArr[]=arrReqNum.split("#");
			for (String str : catArr) {
				listjobCatIds.add(Integer.parseInt(str));
			}
			
			lstJobCategoryMasters = jobCategoryMasterDAO.findByCategoryID(listjobCatIds);
			
			List<DistrictSpecificDocuments> lstDistrictspecificdocuments = new ArrayList<DistrictSpecificDocuments>(); 
			lstDistrictspecificdocuments= districtSpecificDocumentsDAO.findByDistrictJobCatgoryAndDocumentName(districtMaster, lstJobCategoryMasters, fileMaster);
			if(lstDistrictspecificdocuments!=null && lstDistrictspecificdocuments.size()>0)
			{
				if(flag.equals("0")){
					for (DistrictSpecificDocuments dsd : lstDistrictspecificdocuments) {
						if(errorMsg==""){
							errorMsg=""+Utility.getLocaleValuePropByKey("lblJobCat", locale)+" "+ dsd.getJobCategoryMaster().getJobCategoryName();
						}else{
							errorMsg+=","+dsd.getJobCategoryMaster().getJobCategoryName();
						}
					}
					errorMsg+=" "+Utility.getLocaleValuePropByKey("msgDocAlreadyExist", locale)+"";
				}else{String errorJobCat="";boolean bflag=false;
					for (DistrictSpecificDocuments dsd : lstDistrictspecificdocuments) {
						if(mapdistrictspecificdocuments.get(dsd.getDocumentFileMaster().getDocumentFileId()+"")!=null && jobCategoryMaster.equals(dsd.getJobCategoryMaster())){
							if(!bflag)
							errorMsg="success";
						}else if(mapdistrictspecificdocuments.get(dsd.getDocumentFileMaster().getDocumentFileId()+"")==null){
							if(!bflag)
							errorMsg="success";
						}else{bflag=true;
							if(errorMsg==""){
								errorMsg=""+Utility.getLocaleValuePropByKey("lblJobCat", locale)+" "+ dsd.getJobCategoryMaster().getJobCategoryName();
								errorJobCat= dsd.getJobCategoryMaster().getJobCategoryName();
								//break;
							}else{
								errorMsg+=","+dsd.getJobCategoryMaster().getJobCategoryName();
								errorJobCat+=","+dsd.getJobCategoryMaster().getJobCategoryName();
								//break;
							}
						}
					}
					if(!errorMsg.equalsIgnoreCase("success")){
						errorMsg+=" "+Utility.getLocaleValuePropByKey("msgDocumAlreadyExit", locale)+" "+ errorJobCat+" "+Utility.getLocaleValuePropByKey("lblJobCat", locale)+".";
					}
				}
				
			}else{
				errorMsg="success";
			}
			
				
		} catch (Exception e) {
			e.printStackTrace();
		}

		return errorMsg;
	}
  public String getDocsDiv(String districtId,String flag)
  {
	  WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		try{
			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
			}
			List<DocumentFileMaster> lstDocumentfilemasters= new ArrayList<DocumentFileMaster>(); 
			
			if(session.getAttribute("lstTempdocsData")!=null){
               session.setAttribute("lstTempdocsData", null);
			}
			
			String sessionId=session.getId();
			
			lstDocumentfilemasters =documentFileMasterDAO.findAll();
			
			
			List<TempdocsData> listTempdocsData= new ArrayList<TempdocsData>();
				for(DocumentFileMaster ddu : lstDocumentfilemasters)
				{
					TempdocsData data= new TempdocsData();
					
					data.setDocsId(ddu.getDocumentFileId());
					data.setTitle(ddu.getDocumentCategoryMaster().getDocumentCategory());
					data.setSessionid(sessionId);
					data.setType("");
					data.setStatus("I");
					listTempdocsData.add(data);
				}
				
			session.setAttribute("lstTempdocsData",listTempdocsData);	
			
			
			List<TempdocsData> 	listTempdocsData1=	(List<TempdocsData>) session.getAttribute("lstTempdocsData");
			System.out.println("PPPPPPPPPPPPPPPPPP :: "+listTempdocsData1.size());

			 tmRecords.append("<table  id='tblGridDocs' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblTitle", locale)+"</th>");

				tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgType3", locale)+"</th>");
				
				tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblDecr", locale)+"</th>");
				
				tmRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
				
			
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				tmRecords.append("<tbody>");
			if(listTempdocsData!=null && listTempdocsData.size()>0){
				for (TempdocsData tempdocsData : listTempdocsData) {
					tmRecords.append("<tr>");
					
						tmRecords.append("<td>"+tempdocsData.getTitle()+"</td>");
	
						tmRecords.append("<td>"+tempdocsData.getType()+"</td>");
						
						tmRecords.append("<td>"+tempdocsData.getDescription()+"</td>");
						
						tmRecords.append("<td>");
						
						  tmRecords.append("<a href='javascript:void(0);' onclick='return editDocs()'>"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a>");
						  if(tempdocsData.getStatus().equalsIgnoreCase("I"))
							  tmRecords.append("|&nbsp;<a href='javascript:void(0);' onclick='return deactivate()'>"+Utility.getLocaleValuePropByKey("lblActivate", locale)+"</a>");
						  else
							  tmRecords.append("|&nbsp;<a href='javascript:void(0);' onclick='return deactivate()'>"+Utility.getLocaleValuePropByKey("lblDeactivate", locale)+"</a>");
						tmRecords.append("</td>");
					
					tmRecords.append("</tr>");	
				}
			}
			
			
		}catch(Exception e){
		  e.printStackTrace();		
		}
		
		return tmRecords.toString();
  }
	
}
