package tm.services;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.apache.commons.beanutils.BeanUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.python.modules._hashlib.Hash;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.DistrictPortfolioConfig;
import tm.bean.JobOrder;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentQuestions;
import tm.bean.assessment.AssessmentSections;
import tm.bean.assessment.QuestionOptions;
import tm.bean.assessment.QuestionsPool;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.DistrictSpecificRefChkOptions;
import tm.bean.master.DistrictSpecificRefChkQuestions;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.OptionsForDistrictSpecificQuestions;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.QqQuestionsetQuestions;
import tm.bean.master.SecondaryStatus;
import tm.bean.user.UserMaster;
import tm.controller.district.DistrictManageController;
import tm.dao.DistrictPortfolioConfigDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentQuestionsDAO;
import tm.dao.assessment.AssessmentSectionDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.DistrictSpecificRefChkQuestionsDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.QqQuestionSetsDAO;
import tm.dao.master.QqQuestionsetQuestionsDAO;
import tm.dao.master.ReferenceQuestionSetQuestionsDAO;
import tm.dao.master.ReferenceQuestionSetsDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.Utility;

public class ScriptAjax {
	
	
	String locale = Utility.getValueOfPropByKey("locale");

	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private DistrictPortfolioConfigDAO districtPortfolioConfigDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private QqQuestionsetQuestionsDAO qqQuestionsetQuestionsDAO;
	
	@Autowired
	private QqQuestionSetsDAO qqQuestionSetsDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	
	@Autowired
	private AssessmentSectionDAO assessmentSectionDAO;
	
	@Autowired private AssessmentQuestionsDAO assessmentQuestionsDAO;
	
	@Autowired private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired private DistrictManageController districtManageController;
	
	@Autowired
	private ReferenceQuestionSetsDAO referenceQuestionSetsDAO;
	@Autowired
	private DistrictSpecificRefChkQuestionsDAO districtSpecificRefChkQuestionsDAO;
	@Autowired
	private ReferenceQuestionSetQuestionsDAO referenceQuestionSetQuestionsDAO;
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	private static final Logger logger = Logger.getLogger(ScriptAjax.class.getName());

	public String getDistrictSettings(Integer districtId)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		List<DistrictMaster> districtMasters = new ArrayList<DistrictMaster>();
		DistrictMaster districtMaster = new DistrictMaster();
		districtMaster.setDistrictId(districtId);
		System.out.println("districtId:: "+districtId);
		districtMasters.add(districtMaster);
		List<JobCategoryMaster> lstJobCategoryMaster= null;
		lstJobCategoryMaster = jobCategoryMasterDAO.findJobCategorysByDistricts(districtMasters);
		Map<String,Boolean> catMap = new HashMap<String, Boolean>();
		for (JobCategoryMaster jobCategoryMaster : lstJobCategoryMaster) {
			catMap.put(jobCategoryMaster.getJobCategoryName().toLowerCase().trim(), true);
		}
		System.out.println("lstJobCategoryMaster::::::::::: "+lstJobCategoryMaster.size());
		
		
		List<JobCategoryMaster> lstHeadQuaterJobCategoryMaster= null;
		HeadQuarterMaster headQuarterMaster = new HeadQuarterMaster();
		headQuarterMaster.setHeadQuarterId(2);
		
		List<DistrictPortfolioConfig> districtPortfolioConfigs=new ArrayList<DistrictPortfolioConfig>(); 	
		
		districtPortfolioConfigs = districtPortfolioConfigDAO.getPortfolioConfigs(districtMaster);
		System.out.println("districtPortfolioConfigs:: "+districtPortfolioConfigs.size());
		Map<String,String> dspqMap = new HashMap<String, String>();
		for (DistrictPortfolioConfig districtPortfolioConfig : districtPortfolioConfigs) {
			System.out.println("districtPortfolioConfig.getDspqName()::::::::::: "+districtPortfolioConfig.getDspqName());
			dspqMap.put(districtPortfolioConfig.getJobCategoryMaster().getJobCategoryName().toLowerCase().trim(), districtPortfolioConfig.getDspqName().trim());
		}
		List<DistrictPortfolioConfig> headQuaterDistrictPortfolioConfigs = new ArrayList<DistrictPortfolioConfig>(); 	
		System.out.println("111111111111111111111111111111111 ");
		headQuaterDistrictPortfolioConfigs = districtPortfolioConfigDAO.getDSPQByHeadquater(headQuarterMaster);
		System.out.println("headQuaterDistrictPortfolioConfigs:: "+headQuaterDistrictPortfolioConfigs.size());
		Set<String> dspqs = new HashSet<String>();
		for (DistrictPortfolioConfig districtPortfolioConfig : headQuaterDistrictPortfolioConfigs) {
			dspqs.add(districtPortfolioConfig.getDspqName());
		}
		StringBuilder dspqOpt = new StringBuilder();
		dspqOpt.append("<option value='0'>Select DSPQ</option>");
		for (String string : dspqs) {
			dspqOpt.append("<option value='"+string+"'>"+string+"</option>");
		}
		
		
		List<QqQuestionSets> existQuestionList = new ArrayList<QqQuestionSets>();
		existQuestionList = qqQuestionSetsDAO.findByHeadQuater(headQuarterMaster);
		StringBuilder qqOpt = new StringBuilder();
		qqOpt.append("<option value='0'>Select QQ Set</option>");
		for (QqQuestionSets qqQuestionSets : existQuestionList) {
			qqOpt.append("<option value=\""+qqQuestionSets.getQuestionSetText()+"\">"+qqQuestionSets.getQuestionSetText()+"</option>");
		}
		
		List<AssessmentDetail> assessmentDetails = assessmentDetailDAO.findAssessmentsByHeadQuarter( headQuarterMaster);
		StringBuilder jsiOpt = new StringBuilder();
		jsiOpt.append("<option value='0'>Select JSI</option>");
		for (AssessmentDetail assessmentDetail : assessmentDetails) {
			jsiOpt.append("<option value=\""+assessmentDetail.getAssessmentName()+"\">"+assessmentDetail.getAssessmentName()+"</option>");
		}
		
		List<String> lstStatus= new ArrayList<String>();
		lstStatus = secondaryStatusDAO.findSLCByHeadQuarter(headQuarterMaster);
		
		StringBuilder slcOpt = new StringBuilder();
		slcOpt.append("<option value='0'>Select SLC</option>");
		for (String slcName : lstStatus) {
			slcOpt.append("<option value=\""+slcName+"\">"+slcName+"</option>");
		}
		
		lstHeadQuaterJobCategoryMaster = jobCategoryMasterDAO.findJobCategorysByHeadQuarter(headQuarterMaster);
		List<DistrictSpecificRefChkQuestions> districtRefQuestion=districtSpecificRefChkQuestionsDAO.findDistrict(districtMaster);
				
		StringBuilder sb = new StringBuilder();
		sb.append("<table>");
		sb.append("<tr><th>Job Category</th><th>DSPQ</th><th>QQ</th><th>JSI</th><th>SLC</th></tr>");
		for (JobCategoryMaster jobCategoryMaster : lstHeadQuaterJobCategoryMaster) {
			String jobCateSel = catMap.get(jobCategoryMaster.getJobCategoryName().toLowerCase().trim())==null?"":"checked";
			String dspqKey =  dspqMap.get(jobCategoryMaster.getJobCategoryName().toLowerCase().trim())==null?"":dspqMap.get(jobCategoryMaster.getJobCategoryName().toLowerCase().trim());
			String jobDSPQ = dspqKey==null?"":dspqKey+"'";
			String jobDSPQSel = dspqKey==null?"":dspqKey+"' selected";
			String dspq = dspqOpt.toString();
			
			try {
				dspq = dspqKey.equals("")?dspq:dspq.replace(jobDSPQ, jobDSPQSel);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			sb.append("<tr><td><input type='checkbox' name='jobCat' "+jobCateSel+" value='"+jobCategoryMaster.getJobCategoryId()+"' >&nbsp;"+jobCategoryMaster.getJobCategoryName()+"</td>" +
					"<td><select name='dspq' "+(dspqKey.equals("")?"":"disabled")+">"+dspq+"</select></td>" +
					"<td><select name='qq'>"+qqOpt.toString()+"</select></td>" +
					"<td><select name='jsi'>"+jsiOpt.toString()+"</select></td>" +
					"<td><select name='slc'>"+slcOpt.toString()+"</select></td>" +
					//"<td><select name='eReference'>"+slcOptEreference.toString()+"</select></td>" +
					"</tr>");			
		}		
		sb.append("</table>");
		
		sb.append("<table>");
		sb.append("<tr><td>SLC Default Setting</td></tr>");
		sb.append("<tr><td><input type='checkbox' name='slcDefault'id='slcDefault' >&nbsp;SLC Setting</td></tr>");
		
		sb.append("<tr><td>E-Reference</td></tr>");	
		if(districtRefQuestion.size()==0)
		{
			sb.append("<tr><td><input type='checkbox' name='eReference'id='eReference' >&nbsp;E-Reference Question</td></tr>");	
		}
		else
		{
			sb.append("<tr><td><input type='checkbox' disabled name='eReference'id='eReference' >&nbsp;E-Reference Question</td></tr>");	
		}
		sb.append("</table>");
		
		sb.append("<div class='row col-sm-3 col-md-3' >"+	
				"<button class='btn btn-primary fl top25-sm' onclick='copyDistrictSettings();' type='button'>Copy&nbsp;Settings&nbsp;<i class='icon'></i>"+
				"</button></div>");
		return sb.toString();
	}
	public String copyDistrictSettings(Integer districtId,String jsonArray,String ereference,String slcDeafult)
	{
System.out.println("slcDeafult========================="+slcDeafult);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		//System.out.println("jsonArray:: "+jsonArray);
		JSONArray jsonRequest = (JSONArray) JSONSerializer.toJSON( jsonArray ); 
		System.out.println("jsonRequest:: "+jsonRequest.toString());
		
		List<Integer> jobCatlist = new ArrayList<Integer>();
		List<String> dspqList = new ArrayList<String>();
		Map<String,String> dspqMap = new HashMap<String, String>();  
		Map<String,String> qqMap = new HashMap<String, String>();  
		Map<String,String> qqMapReq = new HashMap<String, String>();
		
		Map<String,String> jsiMap = new HashMap<String, String>();  
		Map<String,String> jsiMapReq = new HashMap<String, String>();
		
		Map<String,String> slcMap = new HashMap<String, String>();  
		Map<String,String> slcMapReq = new HashMap<String, String>();
		
		Map<String,String> eReferenceMap = new HashMap<String, String>();  
		Map<String,String> eReferenceMapReq = new HashMap<String, String>();
		
		
		
		for (Object object : jsonRequest) {
			JSONObject jsonItems = (JSONObject)object;
			jobCatlist.add(jsonItems.getInt("jobCategoryId"));
			if(!jsonItems.getString("dspq").equals("0"))
			{
				dspqList.add(jsonItems.getString("dspq"));
				dspqMap.put(""+jsonItems.getInt("jobCategoryId"), jsonItems.getString("dspq"));
			}
			if(!jsonItems.getString("qq").equals("0"))
			{
				qqMap.put(""+jsonItems.getInt("jobCategoryId"), jsonItems.getString("qq"));
				qqMapReq.put(jsonItems.getString("qq"), ""+jsonItems.getInt("jobCategoryId"));
			}
			if(!jsonItems.getString("jsi").equals("0"))
			{
				jsiMap.put(""+jsonItems.getInt("jobCategoryId"), jsonItems.getString("jsi"));
				jsiMapReq.put(jsonItems.getString("jsi"), ""+jsonItems.getInt("jobCategoryId"));
			}
			if(!jsonItems.getString("slc").equals("0"))
			{
				slcMap.put(""+jsonItems.getInt("jobCategoryId"), jsonItems.getString("slc"));
				slcMapReq.put(jsonItems.getString("slc"), ""+jsonItems.getInt("jobCategoryId"));
			}
			
			/*if(!jsonItems.getString("eReference").equals("0"))
			{
				eReferenceMap.put(""+jsonItems.getInt("jobCategoryId"), jsonItems.getString("eReference"));
				eReferenceMapReq.put(jsonItems.getString("eReference"), ""+jsonItems.getInt("jobCategoryId"));
			}*/
			/*System.out.println(jsonItems.get("dspq"));
			System.out.println(jsonItems.get("qq"));
			System.out.println(jsonItems.get("jsi"));
			System.out.println(jsonItems.get("slc"));*/
		}
		SessionFactory sessionFactory=jobCategoryMasterDAO.getSessionFactory();
		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
 	    Transaction txOpen =statelesSsession.beginTransaction();
 	    
		List<DistrictMaster> districtMasters = new ArrayList<DistrictMaster>();
		DistrictMaster districtMaster = new DistrictMaster();
		districtMaster.setDistrictId(districtId);
		System.out.println("districtId:: "+districtId);
		districtMasters.add(districtMaster);
		List<JobCategoryMaster> lstJobCategoryMaster= null;
		lstJobCategoryMaster = jobCategoryMasterDAO.findJobCategorysByDistricts(districtMasters);
		Map<String,Boolean> catMap = new HashMap<String, Boolean>();
		for (JobCategoryMaster jobCategoryMaster : lstJobCategoryMaster) {
			catMap.put(jobCategoryMaster.getJobCategoryName().toLowerCase().trim(), true);
			districtMaster = jobCategoryMaster.getDistrictMaster();
		}
		System.out.println("lstJobCategoryMaster::::::::::: "+lstJobCategoryMaster.size());
		List<JobCategoryMaster> lstHeadQuaterJobCategoryMaster= null;
		HeadQuarterMaster headQuarterMaster = new HeadQuarterMaster();
		headQuarterMaster.setHeadQuarterId(2);
		//lstHeadQuaterJobCategoryMaster = jobCategoryMasterDAO.findJobCategorysByHeadQuarter(headQuarterMaster);
		try
		{
		if(jobCatlist!=null && jobCatlist.size()>0 )
		lstHeadQuaterJobCategoryMaster = jobCategoryMasterDAO.findJobCategorysByHeadQuarterAndID(headQuarterMaster,jobCatlist);
		
		}catch (Exception e) {
			e.printStackTrace();
		}
	
		Map<String,JobCategoryMaster> hqCatMap = new HashMap<String, JobCategoryMaster>();
		if(lstHeadQuaterJobCategoryMaster!=null && lstHeadQuaterJobCategoryMaster.size()>0)
		for (JobCategoryMaster jobCategoryMaster : lstHeadQuaterJobCategoryMaster) {
			hqCatMap.put(""+jobCategoryMaster.getJobCategoryId(), jobCategoryMaster);
			
			if(catMap.get(jobCategoryMaster.getJobCategoryName().toLowerCase().trim())==null)
			{
				JobCategoryMaster jCat = new JobCategoryMaster();
				try {
					BeanUtils.copyProperties(jCat, jobCategoryMaster);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
				
				jCat.setJobCategoryId(null);
				jCat.setHeadQuarterMaster(null);
				jCat.setDistrictMaster(districtMaster);
				jCat.setStatus("A");
				statelesSsession.insert(jCat);
			}
		}
		
		/////////////////////////////////////DSPQ///////////////////////////////////////////////
		List<DistrictPortfolioConfig> districtPortfolioConfigs=new ArrayList<DistrictPortfolioConfig>(); 	
		
		districtPortfolioConfigs = districtPortfolioConfigDAO.getPortfolioConfigs(districtMaster);
		System.out.println("districtPortfolioConfigs:: "+districtPortfolioConfigs.size());
		Map<String,Boolean> dspqCatmap = new HashMap<String, Boolean>();
		for (DistrictPortfolioConfig districtPortfolioConfig : districtPortfolioConfigs) {
			dspqCatmap.put(districtPortfolioConfig.getJobCategoryMaster().getJobCategoryName().trim()+"###"+districtPortfolioConfig.getDspqName(),true);
		}
		List<DistrictPortfolioConfig> headQuaterDistrictPortfolioConfigs = new ArrayList<DistrictPortfolioConfig>(); 	
		//dspqList
		
		lstJobCategoryMaster = jobCategoryMasterDAO.findJobCategorysByDistricts(districtMasters);
		Map<String,JobCategoryMaster> jobCategoryMap = new HashMap<String, JobCategoryMaster>();
		for (JobCategoryMaster jobCategoryMaster : lstJobCategoryMaster) {
			jobCategoryMap.put(jobCategoryMaster.getJobCategoryName().trim(),jobCategoryMaster);
		}
		
		if(dspqList.size()>0)
		{
			//headQuaterDistrictPortfolioConfigs = districtPortfolioConfigDAO.getDSPQByHeadquaterAndName(headQuarterMaster,dspqList);
			headQuaterDistrictPortfolioConfigs = districtPortfolioConfigDAO.getDSPQByHeadquater(headQuarterMaster);
			
			Map<Integer,List<DistrictPortfolioConfig>> hdDspq = new HashMap<Integer, List<DistrictPortfolioConfig>>();
			for (DistrictPortfolioConfig districtPortfolioConfig : headQuaterDistrictPortfolioConfigs) {
				List<DistrictPortfolioConfig> lstdpq = hdDspq.get(districtPortfolioConfig.getJobCategoryMaster().getJobCategoryId());
				if(lstdpq==null)
				{
					lstdpq = new ArrayList<DistrictPortfolioConfig>();
				}
				lstdpq.add(districtPortfolioConfig);
				hdDspq.put(districtPortfolioConfig.getJobCategoryMaster().getJobCategoryId(), lstdpq);
			}
			
			

			for (Map.Entry<String, String> entry : dspqMap.entrySet())
			{
				JobCategoryMaster jobcatHq = hqCatMap.get(entry.getKey());
				JobCategoryMaster jobcat = jobCategoryMap.get(jobcatHq.getJobCategoryName().trim());
				DistrictPortfolioConfig dpc = districtPortfolioConfigDAO.getPortfolioConfigByJobCategory(districtMaster, jobcat);
				if(dpc==null)
				{
					System.out.println(jobcat.getJobCategoryName()+" "+entry.getKey() + "/" + entry.getValue());
					
					for (DistrictPortfolioConfig districtPortfolioConfig : headQuaterDistrictPortfolioConfigs) {
						if(districtPortfolioConfig.getDspqName().equalsIgnoreCase(entry.getValue()))
						{
							DistrictPortfolioConfig dspq = new DistrictPortfolioConfig();
							try {
								BeanUtils.copyProperties(dspq, districtPortfolioConfig);
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
							//JobCategoryMaster jobCategoryMaster = jobCategoryMap.get(districtPortfolioConfig.getJobCategoryMaster().getJobCategoryName().trim());
							dspq.setDistrictPortfolioConfigId(null);
							dspq.setHeadQuarterMaster(null);
							dspq.setJobCategoryMaster(jobcat);
							dspq.setDistrictMaster(districtMaster);
							dspq.setCreatedDateTime(new Date());
							statelesSsession.insert(dspq);
						}
					}
					
				}
				
			    
			}
			
			
		}
		///////////////////////////////////////////////////////////////////////////////
		if(qqMap.size()>0)
		{
			List<QqQuestionSets> HQQQuestionList = new ArrayList<QqQuestionSets>();
			HQQQuestionList = qqQuestionSetsDAO.findByHeadQuater(headQuarterMaster);
			Map<String,QqQuestionSets> HQQqQuestionSetMap = new HashMap<String, QqQuestionSets>();
			for (QqQuestionSets qqQuestionSets : HQQQuestionList) {
				System.out.println("QQQQQ : "+qqQuestionSets.getQuestionSetText());
				HQQqQuestionSetMap.put(qqQuestionSets.getQuestionSetText(), qqQuestionSets);
			}
			
			List<QqQuestionSets> existQuestionList = new ArrayList<QqQuestionSets>();
			existQuestionList = qqQuestionSetsDAO.findByDistrict(districtMaster);
			Map<String,QqQuestionSets> QqQuestionSetMap = new HashMap<String, QqQuestionSets>();
			for (QqQuestionSets qqQuestionSets : existQuestionList) {
				QqQuestionSetMap.put(qqQuestionSets.getQuestionSetText(), qqQuestionSets);
			}
			
			List<DistrictSpecificQuestions> districtquestion = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(districtMaster);
			
			if(districtquestion.size()==0)
			{
				List<DistrictSpecificQuestions> headquarterquestion = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYHeadQuater(headQuarterMaster);
				
				for (DistrictSpecificQuestions districtSpecificQuestions : headquarterquestion) {
					DistrictSpecificQuestions dsq = new DistrictSpecificQuestions();
					try {
						BeanUtils.copyProperties(dsq, districtSpecificQuestions);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
					dsq.setQuestionId(null);
					dsq.setParentQuestionId(districtSpecificQuestions.getQuestionId());
					dsq.setDistrictMaster(districtMaster);
					dsq.setCreatedDateTime(new Date());
					dsq.setHeadQuarterMaster(null);
					statelesSsession.insert(dsq);
					//System.out.println("pppppppppp "+districtSpecificQuestions.getQuestion());
					List<OptionsForDistrictSpecificQuestions> questionOptions = districtSpecificQuestions.getQuestionOptions();
					
					for (OptionsForDistrictSpecificQuestions optionsForDistrictSpecificQuestions : questionOptions) {
						OptionsForDistrictSpecificQuestions ofdsq = new OptionsForDistrictSpecificQuestions();
						try {
							BeanUtils.copyProperties(ofdsq, optionsForDistrictSpecificQuestions);
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						}
						ofdsq.setDistrictSpecificQuestions(dsq);
						ofdsq.setOptionId(null);
						ofdsq.setCreatedDateTime(new Date());
						statelesSsession.insert(ofdsq);
					}
				}
				
			}
			
			txOpen.commit();
			
			txOpen =statelesSsession.beginTransaction();
			
			for (Map.Entry<String, String> entry : qqMap.entrySet())
			{
				System.out.println(entry.getKey() + "/" + entry.getValue());
				QqQuestionSets qqQuestionSets = QqQuestionSetMap.get(entry.getValue());
				System.out.println("qqSet::: "+qqQuestionSets);
				JobCategoryMaster jobcatHq = hqCatMap.get(entry.getKey());
				JobCategoryMaster jobcat = jobCategoryMap.get(jobcatHq.getJobCategoryName().trim());
				if(qqQuestionSets==null)
				{
					QqQuestionSets hqqqQuestionSets = HQQqQuestionSetMap.get(entry.getValue());
					qqQuestionSets  = new QqQuestionSets();
					try {
						BeanUtils.copyProperties(qqQuestionSets, hqqqQuestionSets);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
					qqQuestionSets.setID(null);
					qqQuestionSets.setDistrictMaster(districtMaster);
					qqQuestionSets.setDateCreated(new Date());
					qqQuestionSets.setHeadQuarterMaster(null);
					statelesSsession.insert(qqQuestionSets);
					
					List<QqQuestionsetQuestions> QuestionList = qqQuestionsetQuestionsDAO.findByQuestionSet(hqqqQuestionSets);
					List<Integer> parentQuestions = new ArrayList<Integer>();
					for (QqQuestionsetQuestions qqQuestionsetQuestions : QuestionList) {
						parentQuestions.add(qqQuestionsetQuestions.getDistrictSpecificQuestions().getParentQuestionId());
					}
					System.out.println("QuestionList.size()::: "+QuestionList.size());
					System.out.println("kkkkkkkkkkkkkk : "+parentQuestions.size());
					if(parentQuestions.size()>0)
					{
						List<DistrictSpecificQuestions> districtQuestions = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(districtMaster,parentQuestions);
						
						System.out.println("districtQuestions.size()::: "+districtQuestions.size());
						
						Map<Integer,DistrictSpecificQuestions> districtQuestionMap = new HashMap<Integer, DistrictSpecificQuestions>();
						for (DistrictSpecificQuestions districtSpecificQuestions2 : districtQuestions) {
							districtQuestionMap.put(districtSpecificQuestions2.getParentQuestionId(), districtSpecificQuestions2);
						}
						
						System.out.println("ddddddddddddddddddddddd: "+districtQuestionMap.size());
						
						for (QqQuestionsetQuestions qqQuestionsetQuestions : QuestionList) {
							
							QqQuestionsetQuestions questionsetQuestions = new QqQuestionsetQuestions();
							try {
								BeanUtils.copyProperties(questionsetQuestions, qqQuestionsetQuestions);
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
							System.out.println("qqQuestionsetQuestions.getDistrictSpecificQuestions().getQuestionId()::: "+qqQuestionsetQuestions.getDistrictSpecificQuestions().getQuestionId());
							System.out.println("districtQuestionMap.get(qqQuestionsetQuestions.getDistrictSpecificQuestions().getQuestionId())::: "+districtQuestionMap.get(qqQuestionsetQuestions.getDistrictSpecificQuestions().getQuestionId()));
							questionsetQuestions.setID(null);
							questionsetQuestions.setDistrictSpecificQuestions(districtQuestionMap.get(qqQuestionsetQuestions.getDistrictSpecificQuestions().getQuestionId()));
							questionsetQuestions.setQuestionSets(qqQuestionSets);
							statelesSsession.insert(questionsetQuestions);
						}
					}
					
				}
				//Attaching qq to jobCategoryjjjjj
				jobcat.setQuestionSets(qqQuestionSets);	
				statelesSsession.update(jobcat);
				
			}
		}
		// e-Reference
		if(ereference.equals("true"))
		{				
			List<DistrictSpecificRefChkQuestions> districtRefQuestion=districtSpecificRefChkQuestionsDAO.findDistrict(districtMaster);
			if(districtRefQuestion.size()==0)
			{
				List<DistrictSpecificRefChkQuestions> headRefQuestion=districtSpecificRefChkQuestionsDAO.getDistrictSpecificRefQuestionBYHeadQuater(headQuarterMaster);
				
				for(DistrictSpecificRefChkQuestions headRefQuestionList: headRefQuestion)
				{
					DistrictSpecificRefChkQuestions dsrcq= new DistrictSpecificRefChkQuestions();
					try {
						BeanUtils.copyProperties(dsrcq, headRefQuestionList);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
					dsrcq.setQuestionId(null);
					dsrcq.setHeadQuarterMaster(null);
					dsrcq.setBranchMaster(null);
					dsrcq.setDistrictMaster(districtMaster);
					dsrcq.setCreatedDateTime(new Date());
					statelesSsession.insert(dsrcq);
					
					List<DistrictSpecificRefChkOptions> lstDistrictRefOptions=headRefQuestionList.getQuestionOptions();
					for(DistrictSpecificRefChkOptions questionOptionList: lstDistrictRefOptions)
					{
						DistrictSpecificRefChkOptions dsrco=new DistrictSpecificRefChkOptions();
						try {
							BeanUtils.copyProperties(dsrco, questionOptionList);
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						}
						
						dsrco.setCreatedDateTime(new Date());
						dsrco.setOptionId(null);
						dsrco.setDistrictSpecificRefChkQuestions(dsrcq);
						statelesSsession.insert(dsrco);						
						
					}
					
				}
				
			}			
            						
		}
		
		if(slcDeafult.equals("true"))
		{
			try
			{
			List<SecondaryStatus> lstSecondaryStatus=new ArrayList<SecondaryStatus>();
			List<SecondaryStatus> lstSecondaryStatusWithJobCategory=new ArrayList<SecondaryStatus>();
			
			DistrictMaster distMaster=null;
			JobCategoryMaster jobCategoryMaster=null;
			UserMaster usermaster=userMasterDAO.findById(1, false, false);
			Map<String, SecondaryStatus> screenMAP=new HashMap<String, SecondaryStatus>();
			Map<String, SecondaryStatus> evaluteMAP=new HashMap<String, SecondaryStatus>();
			Map<String, SecondaryStatus> vetMAP=new HashMap<String, SecondaryStatus>();
			
			Map<Integer, SecondaryStatus> screenMAPHead=new HashMap<Integer, SecondaryStatus>();
			Map<Integer, SecondaryStatus> evaluteMAPHead=new HashMap<Integer, SecondaryStatus>();
			Map<Integer, SecondaryStatus> vetMAPHead=new HashMap<Integer, SecondaryStatus>();
			
			if(districtId!=null)
				distMaster=districtMasterDAO.findById(districtId, false, false);
			if(distMaster!=null)
			{
				lstSecondaryStatus=secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategory(distMaster,jobCategoryMaster);
				lstSecondaryStatusWithJobCategory=secondaryStatusDAO.findSSByDistrictPlus(distMaster);
			}
			if(lstSecondaryStatus.size()>0)
			{
				System.out.println("lstSecondaryStatus=="+lstSecondaryStatus.size());
				System.out.println("lstSecondaryStatusWithJobCategory========"+lstSecondaryStatusWithJobCategory.size());	
				for(SecondaryStatus secondaryStatusObj:lstSecondaryStatus)
				{
					if(secondaryStatusObj.getStatusNodeMaster()!=null)
					{
						if(secondaryStatusObj.getStatusNodeMaster().getStatusNodeId()==1)
						{
							screenMAPHead.put(secondaryStatusObj.getStatusNodeMaster().getStatusNodeId(), secondaryStatusObj);
																		
						}else if(secondaryStatusObj.getStatusNodeMaster().getStatusNodeId()==2)
						{
							evaluteMAPHead.put(secondaryStatusObj.getStatusNodeMaster().getStatusNodeId(), secondaryStatusObj);
						}
						else if(secondaryStatusObj.getStatusNodeMaster().getStatusNodeId()==3)
						{
							vetMAPHead.put(secondaryStatusObj.getStatusNodeMaster().getStatusNodeId(), secondaryStatusObj);		
						}
						
					}
					
				}
			}
			Vector<SecondaryStatus> finalSecondaryStatusObj=new Vector<SecondaryStatus>(lstSecondaryStatusWithJobCategory);
			System.out.println("lstSecondaryStatusWithJobCategory.size()>>>>>>>>>>>>>>>>>>> :: "+lstSecondaryStatusWithJobCategory.size());
			if(lstSecondaryStatusWithJobCategory.size()>0)
			{
				for(SecondaryStatus ss:lstSecondaryStatusWithJobCategory)
				{
					for(SecondaryStatus headObj:lstSecondaryStatus)
					{
						if(ss.getStatusNodeMaster()==null && ss.getSecondaryStatusName().equals(headObj.getSecondaryStatusName()))
						{
							finalSecondaryStatusObj.remove(ss);
							
						}
					}
					
				}
				lstSecondaryStatusWithJobCategory.clear();
				lstSecondaryStatusWithJobCategory=finalSecondaryStatusObj;
				System.out.println("lstSecondaryStatusWithJobCategory====after remove==="+lstSecondaryStatusWithJobCategory.size());
			}
			if(lstSecondaryStatusWithJobCategory.size()>0)
			{		
			for(SecondaryStatus secondaryStatus:lstSecondaryStatusWithJobCategory)
			{
				if(secondaryStatus.getParentSecondaryStatus()!=null &&  secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster()!=null && secondaryStatus.getStatusMaster()==null)
				{
				if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==1)
				{
					screenMAP.put(secondaryStatus.getSecondaryStatusName(), secondaryStatus);
				}
				else if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==2)
				{
					evaluteMAP.put(secondaryStatus.getSecondaryStatusName(), secondaryStatus);
				}
				else if(secondaryStatus.getParentSecondaryStatus().getStatusNodeMaster().getStatusNodeId()==3)
				{
					vetMAP.put(secondaryStatus.getSecondaryStatusName(), secondaryStatus);		
				}
				}			
			}
			}
			if(screenMAP.size()>0)
			{
				for(Map.Entry<String, SecondaryStatus> entry : screenMAP.entrySet())
				{					
					SecondaryStatus secondaryStatus=screenMAP.get(entry.getValue());
					if(secondaryStatus==null)
					{
						secondaryStatus=new SecondaryStatus();
						secondaryStatus.setDistrictMaster(distMaster);
						secondaryStatus.setJobCategoryMaster(jobCategoryMaster);
						secondaryStatus.setSecondaryStatusName(screenMAP.get(entry.getKey()).getSecondaryStatusName());						
						secondaryStatus.setSecondaryStatus(screenMAPHead.get(1));
						secondaryStatus.setCreatedDateTime(new Date());
						secondaryStatus.setStatus("A");
						secondaryStatus.setUsermaster(usermaster);
						secondaryStatus.setOrderNumber(1);
						statelesSsession.insert(secondaryStatus);
						System.out.println("screenMAPHead.get(1)=="+screenMAPHead.get(1).getSecondaryStatusId());
					}
				}				
			}
			if(evaluteMAP.size()>0)
			{				
				for(Map.Entry<String, SecondaryStatus> entry : evaluteMAP.entrySet())
				{					
					SecondaryStatus secondaryStatus=evaluteMAP.get(entry.getValue());
					if(secondaryStatus==null)
					{
						secondaryStatus=new SecondaryStatus();
						secondaryStatus.setDistrictMaster(distMaster);
						secondaryStatus.setJobCategoryMaster(jobCategoryMaster);
						secondaryStatus.setSecondaryStatusName(evaluteMAP.get(entry.getKey()).getSecondaryStatusName());						
						secondaryStatus.setSecondaryStatus(evaluteMAPHead.get(2));
						secondaryStatus.setCreatedDateTime(new Date());
						secondaryStatus.setStatus("A");
						secondaryStatus.setUsermaster(usermaster);
						secondaryStatus.setOrderNumber(2);
						statelesSsession.insert(secondaryStatus);
						System.out.println("screenMAPHead.get(2)=="+evaluteMAPHead.get(2).getSecondaryStatusId());
					}
				}				
			}
			if(vetMAP.size()>0)
			{
				for(Map.Entry<String, SecondaryStatus> entry : vetMAP.entrySet())
				{					
					SecondaryStatus secondaryStatus=vetMAP.get(entry.getValue());
					if(secondaryStatus==null)
					{
						secondaryStatus=new SecondaryStatus();
						secondaryStatus.setDistrictMaster(distMaster);
						secondaryStatus.setJobCategoryMaster(jobCategoryMaster);
						secondaryStatus.setSecondaryStatusName(vetMAP.get(entry.getKey()).getSecondaryStatusName());						
						secondaryStatus.setSecondaryStatus(vetMAPHead.get(3));
						secondaryStatus.setCreatedDateTime(new Date());
						secondaryStatus.setStatus("A");
						secondaryStatus.setUsermaster(usermaster);
						secondaryStatus.setOrderNumber(3);
						statelesSsession.insert(secondaryStatus);
						System.out.println("screenMAPHead.get(3)=="+vetMAPHead.get(3).getSecondaryStatusId());
					}
				}				
			}				
			System.out.println("insideeeeeeeeeeeeee==screen map=="+screenMAP.size());
			System.out.println("insideeeeeeeeeeeeee==eve map=="+evaluteMAP.size());
			System.out.println("insideeeeeeeeeeeeee==vet map=="+vetMAP.size());	
			
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		if(jsiMap.size()>0)
		{
			List<AssessmentDetail> hqAssessmentDetails = new ArrayList<AssessmentDetail>();
			hqAssessmentDetails = assessmentDetailDAO.findAssessmentsByHeadQuarter(headQuarterMaster);
			Map<String,AssessmentDetail> hqAssessmentDetailsMap = new HashMap<String, AssessmentDetail>();
			for (AssessmentDetail assessmentDetail : hqAssessmentDetails) {
				hqAssessmentDetailsMap.put(assessmentDetail.getAssessmentName(), assessmentDetail);
			}
			
			for (Map.Entry<String, String> entry : jsiMap.entrySet())
			{
				System.out.println(entry.getKey() + "/" + entry.getValue());
				AssessmentDetail assessmentDetail = hqAssessmentDetailsMap.get(entry.getValue());
				//System.out.println("hqAssessmentDetailsMap::: "+hqAssessmentDetailsMap);
				JobCategoryMaster jobcatHq = hqCatMap.get(entry.getKey());
				JobCategoryMaster jobcat = jobCategoryMap.get(jobcatHq.getJobCategoryName().trim());
				AssessmentDetail assessmentDetailnew = new AssessmentDetail();
				try {
					BeanUtils.copyProperties(assessmentDetailnew, assessmentDetail);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
				assessmentDetailnew.setAssessmentId(null);
				assessmentDetailnew.setDistrictMaster(districtMaster);
				assessmentDetailnew.setJobCategoryMaster(jobcat);
				assessmentDetailnew.setCreatedDateTime(new Date());
				assessmentDetailnew.setHeadQuarterMaster(null);
				String assess = assessmentDetail.getAssessmentName().replace("Headquater", districtMaster.getDistrictName());
				System.out.println("dddddddddddd: "+assess);
				try {
					assessmentDetailnew.setAssessmentName(assess);
					statelesSsession.insert(assessmentDetailnew);
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
				List<AssessmentSections> assessmentSections = assessmentSectionDAO.getSectionsByAssessmentId(assessmentDetail);
				for (AssessmentSections assessmentSections2 : assessmentSections) {
					AssessmentSections assessmentsectionnew = new AssessmentSections();
					try {
						BeanUtils.copyProperties(assessmentsectionnew, assessmentSections2);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
					assessmentsectionnew.setSectionId(null);
					assessmentsectionnew.setAssessmentDetail(assessmentDetailnew);
					assessmentsectionnew.setCreatedDateTime(new Date());
					try {
						statelesSsession.insert(assessmentsectionnew);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					List<AssessmentQuestions> assessmentQuestions = assessmentQuestionsDAO.getSectionQuestions(assessmentSections2);
					for (AssessmentQuestions assessmentQuestions2 : assessmentQuestions) {
						
					QuestionsPool questionsPool = assessmentQuestions2.getQuestionsPool();
					QuestionsPool questionsPoolNew = new QuestionsPool();
					try {
						BeanUtils.copyProperties(questionsPoolNew, questionsPool);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
					questionsPoolNew.setQuestionId(null);
					questionsPoolNew.setCreatedDateTime(new Date());
					try {
						statelesSsession.insert(questionsPoolNew);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
						if(assessmentDetail.getAssessmentType().equals(2)){//JSI
							questionsPoolNew.setQuestionUId("jsi."+Utility.addZeroBeforeNo(questionsPoolNew.getQuestionId(),5));
							try {
								statelesSsession.update(questionsPoolNew);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						
						List<QuestionOptions> questionOptions = questionsPool.getQuestionOptions();
						for (QuestionOptions questionOptions2 : questionOptions) {
							QuestionOptions questionOptionsNew = new QuestionOptions();
							try {
								BeanUtils.copyProperties(questionOptionsNew, questionOptions2);
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
							questionOptionsNew.setQuestionsPool(questionsPoolNew);
							questionOptionsNew.setCreatedDateTime(new Date());
							questionOptionsNew.setOptionId(null);
							
							try {
								statelesSsession.insert(questionOptionsNew);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								System.out.println("option Exllllll");
							}
						}
						
						AssessmentQuestions assessmentQuestionsNew = new AssessmentQuestions();
						try {
							BeanUtils.copyProperties(assessmentQuestionsNew, assessmentQuestions2);
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						}
						assessmentQuestionsNew.setAssessmentDetail(assessmentDetailnew);
						assessmentQuestionsNew.setCreatedDateTime(new Date());
						assessmentQuestionsNew.setAssessmentSections(assessmentsectionnew);
						assessmentQuestionsNew.setQuestionsPool(questionsPoolNew);
						try {
							statelesSsession.insert(assessmentQuestionsNew);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		if(slcMap.size()>0)
		{
			try {// District Default Settings
				districtManageController.createDistrictStatusHome(request,districtMaster);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			List<SecondaryStatus> secondaryStatusList = new ArrayList<SecondaryStatus>();
			secondaryStatusList = secondaryStatusDAO.findSecondaryStatusByHeadQuarter(headQuarterMaster);
			Map<String,List<SecondaryStatus>> hqSecondaryStatusMap = new HashMap<String, List<SecondaryStatus>>();
			
			for (SecondaryStatus secondaryStatus : secondaryStatusList) {
				List<SecondaryStatus> lstSlc = hqSecondaryStatusMap.get(secondaryStatus.getSlcName());
				if(lstSlc==null)
				{
					lstSlc = new ArrayList<SecondaryStatus>();
				}
				lstSlc.add(secondaryStatus);
				
				hqSecondaryStatusMap.put(secondaryStatus.getSlcName(), lstSlc);
			}
			
			List<SecondaryStatus> existSecondaryStatusList = new ArrayList<SecondaryStatus>();
			
			existSecondaryStatusList = secondaryStatusDAO.findSecondaryStatusByJobCategory(lstJobCategoryMaster);
			
			Map<String,SecondaryStatus> secondaryStatusExitMap = new HashMap<String, SecondaryStatus>();
			Map<Integer,SecondaryStatus> secondaryStatusExitJobCategoryMap = new HashMap<Integer, SecondaryStatus>();
			for (SecondaryStatus secondaryStatus : existSecondaryStatusList) {
				secondaryStatusExitJobCategoryMap.put(secondaryStatus.getJobCategoryMaster().getJobCategoryId(), secondaryStatus);
			}
			
			for (Map.Entry<String, String> entry : slcMap.entrySet())
			{
				System.out.println(entry.getKey() + "/" + entry.getValue());
				List<SecondaryStatus> secondaryStatusListVal = hqSecondaryStatusMap.get(entry.getValue());
				System.out.println("secondaryStatus::: "+secondaryStatusListVal);
				JobCategoryMaster jobcatHq = hqCatMap.get(entry.getKey());
				JobCategoryMaster jobcat = jobCategoryMap.get(jobcatHq.getJobCategoryName().trim());
				if(jobcat!=null)
				{	
					SecondaryStatus secondaryStatusl = secondaryStatusExitJobCategoryMap.get(jobcat.getJobCategoryId());
					
					if(secondaryStatusl==null)
					{
						List<SecondaryStatus> secondaryStatusListCategory = new ArrayList<SecondaryStatus>();
						secondaryStatusListCategory = secondaryStatusDAO.findSecondaryStatusByJobCategory(jobcat);
						Map<Integer,SecondaryStatus> statusNoteMap = new HashMap<Integer, SecondaryStatus>();
						for (SecondaryStatus secondaryStatus : secondaryStatusListCategory) {
							statusNoteMap.put(secondaryStatus.getStatusNodeMaster().getStatusNodeId(), secondaryStatus);
						}
						
						
						JobOrder jobOrder = new JobOrder();
						jobOrder.setDistrictMaster(districtMaster);
						jobOrder.setJobCategoryMaster(jobcat);
						UserMaster userMaster = new UserMaster();
						userMaster.setUserId(1);
						userMaster.setEntityType(1);
						
						secondaryStatusDAO.copyDataFromDistrictToJobCategoryBanchAndHead(null,null,districtMaster,jobOrder.getJobCategoryMaster(),userMaster);
						List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
						Map<Integer,SecondaryStatus> jobCatSLCMap = new HashMap<Integer, SecondaryStatus>();
						Map<Integer,List<String>> jobCatSLCMap1 = new HashMap<Integer, List<String>>();
						//
						String slcName = "";
						for (SecondaryStatus secondaryStatus : secondaryStatusListVal) {
							
							if(secondaryStatus.getStatusMaster()!=null){
								jobCatSLCMap.put(secondaryStatus.getStatusMaster().getStatusId(), secondaryStatus);
								System.out.println("secondaryStatus:: "+secondaryStatus.getSecondaryStatusName());
								System.out.println("secondaryStatusID:: "+secondaryStatus.getSecondaryStatusId());
							}
							slcName = secondaryStatus.getSlcName();
							
							if(secondaryStatus.getSecondaryStatus()!=null && secondaryStatus.getStatusMaster()==null)
							{
								List<String> slcs = jobCatSLCMap1.get(secondaryStatus.getSecondaryStatus().getStatusNodeMaster().getStatusNodeId());
								
								if(slcs==null)
									slcs = new ArrayList<String>();
								
								slcs.add(secondaryStatus.getSecondaryStatusName());
								
								jobCatSLCMap1.put(secondaryStatus.getSecondaryStatus().getStatusNodeMaster().getStatusNodeId(), slcs);
							}
						}
						Map<Integer,SecondaryStatus> jobNodes = new HashMap<Integer, SecondaryStatus>();
						StatelessSession statelesSsession1 = sessionFactory.openStatelessSession();
				 	    Transaction txOpenTr =statelesSsession1.beginTransaction();
						
						for (SecondaryStatus secondaryStatus : lstTreeStructure) {
							SecondaryStatus ssNote = null;
							if(secondaryStatus.getStatusMaster()!=null)
							  ssNote = jobCatSLCMap.get(secondaryStatus.getStatusMaster().getStatusId());
							
							if(secondaryStatus.getStatusNodeMaster()!=null)
							{
								List<String> slcs = jobCatSLCMap1.get(secondaryStatus.getStatusNodeMaster().getStatusNodeId());
								
								for (String sname : slcs) {
									SecondaryStatus status=new SecondaryStatus();
										status.setDistrictMaster(secondaryStatus.getDistrictMaster());
									
									
									status.setSchoolMaster(secondaryStatus.getSchoolMaster());
									status.setJobCategoryMaster(secondaryStatus.getJobCategoryMaster());
									status.setSecondaryStatusName(sname);
									status.setStatusMaster(secondaryStatus.getStatusMaster());
									status.setSecondaryStatus(secondaryStatus);
									//status.setStatusNodeMaster(secondaryStatus.getStatusNodeMaster());
									status.setOrderNumber(secondaryStatus.getOrderNumber());
									status.setStatus(secondaryStatus.getStatus());
									status.setUsermaster(userMaster);
									status.setSecondaryStatus_copy(secondaryStatus);
									status.setCreatedDateTime(secondaryStatus.getCreatedDateTime());
									status.setSlcName(slcName);
									statelesSsession1.insert(status);
									
								}
							}
								//jobNodes.put(secondaryStatus.getStatusNodeMaster().getStatusNodeId(), secondaryStatus);
							
							/*System.out.println("sssss: "+ssNote);
							if(ssNote!=null)
								System.out.println("ppppp: "+ssNote.getSecondaryStatusId());*/
							
							if(ssNote!=null && secondaryStatus.getStatusMaster()!=null)
							{
								secondaryStatus.setSecondaryStatusName(ssNote.getSecondaryStatusName());
								System.out.println(ssNote.getSecondaryStatusName());
							}
							secondaryStatus.setSlcName(slcName);
							statelesSsession1.update(secondaryStatus);
						}
						txOpenTr.commit();
						
						/*for (SecondaryStatus secondaryStatus : secondaryStatusListVal) {
							SecondaryStatus secondaryStatusNew = new SecondaryStatus();
							try {
								BeanUtils.copyProperties(secondaryStatusNew, secondaryStatus);
								secondaryStatusNew.setSecondaryStatusId(null);
								secondaryStatusNew.setHeadQuarterMaster(null);
								secondaryStatusNew.setJobCategoryMaster(jobcat);
								secondaryStatusNew.setDistrictMaster(districtMaster);
								secondaryStatusNew.setCreatedDateTime(new Date());
								try {
									//statelesSsession.insert(secondaryStatusNew);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
						}*/
					}
				}
				
			}
			
		}
		
		txOpen.commit();
		return "Setting has been successfully set";

	}

	
}
