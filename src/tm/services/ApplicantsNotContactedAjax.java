package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.lang.ArrayUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class ApplicantsNotContactedAjax {
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired 
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
		List<DistrictSchools> fieldOfSchoolList1 = null;
		List<DistrictSchools> fieldOfSchoolList2 = null;
		try 
		{
			Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
			if(SchoolName.length()>0){
				if(districtIdForSchool==0){
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
						Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					}else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}else{
					DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
					Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
					
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
						Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
						
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					}else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return schoolMasterList;
		
	}
	
	public String displayRecordsByEntityType(String districtName,String schoolName,int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,boolean exportcheck,String startDate,String endDate,String jobOrderId,String jobStatus){
		System.out.println("Job idd======="+jobStatus);
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
			boolean flagfordata=true;
			if(!districtName.trim().equals("") && districtOrSchoolId==0)
				flagfordata=false;
			else if(!schoolName.equals("") && schoolId==0)
				flagfordata=false;
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="firstName";
				String sortOrderNoField="firstName";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("firstName"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("jobTitle"))
					sortingcheck=2;
				else  if(sortOrder.equalsIgnoreCase("createdDateTime"))
					sortingcheck=3;
					else  if(sortOrder.equalsIgnoreCase("status")){
						sortingcheck=4;
				}else
					sortingcheck=5;
				
				//System.out.println("Sort order :: "+sortingcheck);
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				
				 
				boolean checkflag = false;
				
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				System.out.println("jobStatusjobStatusjobStatus :: "+jobStatus);
				   /********status filter*********/
			     boolean statusflag=false;
			     List<SecondaryStatus> lstSecondaryStatus =new ArrayList<SecondaryStatus>();
					List<JobForTeacher>  listByJobStatus = new ArrayList<JobForTeacher>();
					
					 if(!jobStatus.equals("") && !jobStatus.equals("0")){
						 System.out.println(" Filter StatusId Start ::  "+jobStatus);
						 String statusNames[] = jobStatus.split("#@");
						
					      if(!ArrayUtils.contains(statusNames,"0"))
					      {	 
					    	 statusflag=true;
							 DistrictMaster districtMaster2 = districtMasterDAO.findById(districtOrSchoolId, false, false);
							 lstSecondaryStatus = secondaryStatusDAO.findByMultipleStatusName(districtMaster2, statusNames);
					
							  List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
							  Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
							  for (StatusMaster statusMaster1 : statusMasterList) {
								  mapStatus.put(statusMaster1.getStatusShortName(),statusMaster1);
							  }
							  StatusMaster stMaster =null;
							  List <StatusMaster> statusMaster1st=new ArrayList<StatusMaster>();
							  
							  if(ArrayUtils.contains(statusNames,"Available")){
									  stMaster = mapStatus.get("comp");
									  statusMaster1st.add(stMaster);
							  }
							  if(ArrayUtils.contains(statusNames,"Rejected")){
								    stMaster = mapStatus.get("rem");
								    statusMaster1st.add(stMaster);
						     }
							  if(ArrayUtils.contains(statusNames,"Timed Out")){
								    stMaster = mapStatus.get("vlt");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Withdrew")){
								    stMaster = mapStatus.get("widrw");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Incomplete")){
								    stMaster = mapStatus.get("icomp");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Hired")){
								    stMaster = mapStatus.get("hird");
								    statusMaster1st.add(stMaster);
						     }   
							 
							 for(SecondaryStatus ss: lstSecondaryStatus)
							  {
								 if(ss.getStatusMaster()!=null)
									 statusMaster1st.add(ss.getStatusMaster()); 
							  }

						     listByJobStatus = jobForTeacherDAO.findAvailableCandidtesForReport(districtMaster2,statusMaster1st,lstSecondaryStatus);

							  
				         }
							  
					 }
					List<Long> statuswisejftList=new ArrayList<Long>();
					 if(statusflag){
						 if(listByJobStatus!=null && listByJobStatus.size()>0){
							 for (JobForTeacher jft : listByJobStatus) {
								 statuswisejftList.add(jft.getJobForTeacherId());
							} 
						 }
					 }
				
				System.out.println(">>>>>>>>>>>>>>>>>>statuswisejftList	"+ statuswisejftList.size());	
				/*********status filter end***************/
				
				
				List<String []> lstJobForTeachers =new ArrayList<String []>();
				
				if(flagfordata)
				if(entityID==2)
				{
					totalRecord=jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictCountRowForStatus(districtMaster,sortingcheck,sortOrderStrVal,schoolId,startDate,endDate,jobOrderId,statuswisejftList,statusflag);
				 lstJobForTeachers = jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictForStatus(districtMaster,sortingcheck,sortOrderStrVal,start, noOfRowInPage,schoolId,false,startDate,endDate,jobOrderId,statuswisejftList,statusflag);
				
				}
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0)
						 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					totalRecord=jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictCountRow(districtMaster,sortingcheck,sortOrderStrVal,schoolId,startDate,endDate,jobOrderId);
					lstJobForTeachers = jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,start, noOfRowInPage,schoolId,false,startDate,endDate,jobOrderId);
					
				}
			
				if(flagfordata)
		     if(schoolId!=0)
			 {
				 lstJobForTeachers.clear();	
				totalRecord=jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictCountRow(districtMaster,sortingcheck,sortOrderStrVal,schoolId,startDate,endDate,jobOrderId);
				lstJobForTeachers.addAll(jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,start, noOfRowInPage,schoolId,false,startDate,endDate,jobOrderId));
				
			 }	 
				
		     List<String []> finalJobForTeachers =new ArrayList<String []>();
		     if(totalRecord<end)
					end=totalRecord;
		     
			
			finalJobForTeachers	=	lstJobForTeachers;//.subList(start,end);
			
			String responseText="";
			tmRecords.append("<table  id='tblGridJobList' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDistrictName", locale),sortOrderNoField,"districtName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblApplicantName", locale),sortOrderNoField,"firstName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
    
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderNoField,"jobTitle",sortOrderTypeVal,pgNo);
			tmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobAppliedDate", locale),sortOrderNoField,"createdDateTime",sortOrderTypeVal,pgNo);
			tmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Job Status",sortOrderNoField,"status",sortOrderTypeVal,pgNo);
			tmRecords.append("<th valign='top'>"+responseText+"</th>");
						
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(finalJobForTeachers.size()==0){
			 tmRecords.append("<tr><td colspan='4' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			}
			
			String sta="";
			if(finalJobForTeachers.size()>0){
				for (Iterator it = finalJobForTeachers.iterator(); it.hasNext();) {
		            Object[] row = (Object[]) it.next();
		            tmRecords.append("<tr>");
		            tmRecords.append("<td>"+row[0].toString()+"</td>");
					tmRecords.append("<td>"+row[1].toString()+" "+row[2].toString()+"<br/>"+"("+row[3].toString()+")"+"</td>");
					tmRecords.append("<td>"+row[4].toString()+"("+row[7].toString()+")"+"</td>");					
					tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate((Date)row[5])+"</td>");	
					if(row[6].toString().equalsIgnoreCase("A"))
					{
						sta="Active";
					}
					else
					{
						sta="Inactive";
					}
						
					tmRecords.append("<td>"+sta+"</td>");	
		            tmRecords.append("</tr>");
		        }			
			 }

		tmRecords.append("</tbody>");
		tmRecords.append("</table>");
		
		tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
  }
	
	public String applicantsListExportPDF(String districtName,String schoolName,int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,boolean exportcheck,String startDate,String endDate,String jobOrderId,String jobStatus)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

			

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"applicantscontactedbyschool.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateApplicantPDReport(districtName,schoolName,districtOrSchoolId,schoolId,noOfRow,pageNo,sortOrder,sortOrderType,exportcheck,startDate,endDate,jobOrderId,jobStatus,basePath+"/"+fileName,context.getServletContext().getRealPath("/"),userMaster,entityID,districtMaster,schoolMaster);
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}
	public boolean generateApplicantPDReport(String districtName,String schoolName,int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,boolean exportcheck,String startDate,String endDate,String jobOrderId,String jobStatus,String path,String realPath,UserMaster userMaster,Integer entityID,DistrictMaster districtMaster,SchoolMaster schoolMaster){
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		
			
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			
			int sortingcheck=1;
			try{
				
				boolean flagfordata=true;
				if(!districtName.trim().equals("") && districtOrSchoolId==0)
					flagfordata=false;
				else if(!schoolName.equals("") && schoolId==0)
					flagfordata=false;
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position

					int noOfRowInPage = Integer.parseInt(noOfRow);
					int pgNo = Integer.parseInt(pageNo);
					int start =((pgNo-1)*noOfRowInPage);
					int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
					int totalRecord = 0;
					
					//------------------------------------
				 /** set default sorting fieldName **/
					String sortOrderFieldName="firstName";
					String sortOrderNoField="firstName";

					if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("firstName"))
						sortingcheck=1;
					else if(sortOrder.equalsIgnoreCase("jobTitle"))
						sortingcheck=2;
					else  if(sortOrder.equalsIgnoreCase("createdDateTime"))
						sortingcheck=3;
						else  if(sortOrder.equalsIgnoreCase("status")){
							sortingcheck=4;
					}else
						sortingcheck=5;
					
					/**Start set dynamic sorting fieldName **/
					
					Order  sortOrderStrVal=null;

					if(sortOrder!=null){
						if(!sortOrder.equals("") && !sortOrder.equals(null)){
							sortOrderFieldName=sortOrder;
							sortOrderNoField=sortOrder;
						}
					}

					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
						if(sortOrderType.equals("0")){
							sortOrderStrVal=Order.asc(sortOrderFieldName);
						}else{
							sortOrderTypeVal="1";
							sortOrderStrVal=Order.desc(sortOrderFieldName);
						}
					}else{
						sortOrderTypeVal="0";
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}
					
					
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					 /********status filter*********/
				     boolean statusflag=false;
				     List<SecondaryStatus> lstSecondaryStatus =new ArrayList<SecondaryStatus>();
						List<JobForTeacher>  listByJobStatus = new ArrayList<JobForTeacher>();
						
						 if(!jobStatus.equals("") && !jobStatus.equals("0")){
							 System.out.println(" Filter StatusId Start ::  "+jobStatus);
							 String statusNames[] = jobStatus.split("#@");
							
						      if(!ArrayUtils.contains(statusNames,"0"))
						      {	 
						    	 statusflag=true;
								 DistrictMaster districtMaster2 = districtMasterDAO.findById(districtOrSchoolId, false, false);
								 lstSecondaryStatus = secondaryStatusDAO.findByMultipleStatusName(districtMaster2, statusNames);
						
								  List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
								  Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
								  for (StatusMaster statusMaster1 : statusMasterList) {
									  mapStatus.put(statusMaster1.getStatusShortName(),statusMaster1);
								  }
								  StatusMaster stMaster =null;
								  List <StatusMaster> statusMaster1st=new ArrayList<StatusMaster>();
								  
								  if(ArrayUtils.contains(statusNames,"Available")){
										  stMaster = mapStatus.get("comp");
										  statusMaster1st.add(stMaster);
								  }
								  if(ArrayUtils.contains(statusNames,"Rejected")){
									    stMaster = mapStatus.get("rem");
									    statusMaster1st.add(stMaster);
							     }
								  if(ArrayUtils.contains(statusNames,"Timed Out")){
									    stMaster = mapStatus.get("vlt");
									    statusMaster1st.add(stMaster);
							     }
								 if(ArrayUtils.contains(statusNames,"Withdrew")){
									    stMaster = mapStatus.get("widrw");
									    statusMaster1st.add(stMaster);
							     }
								 if(ArrayUtils.contains(statusNames,"Incomplete")){
									    stMaster = mapStatus.get("icomp");
									    statusMaster1st.add(stMaster);
							     }
								 if(ArrayUtils.contains(statusNames,"Hired")){
									    stMaster = mapStatus.get("hird");
									    statusMaster1st.add(stMaster);
							     }   
								 
								 for(SecondaryStatus ss: lstSecondaryStatus)
								  {
									 if(ss.getStatusMaster()!=null)
										 statusMaster1st.add(ss.getStatusMaster()); 
								  }

							     listByJobStatus = jobForTeacherDAO.findAvailableCandidtesForReport(districtMaster2,statusMaster1st,lstSecondaryStatus);

								  
					         }
								  
						 }
						List<Long> statuswisejftList=new ArrayList<Long>();
						 if(statusflag){
							 if(listByJobStatus!=null && listByJobStatus.size()>0){
								 for (JobForTeacher jft : listByJobStatus) {
									 statuswisejftList.add(jft.getJobForTeacherId());
								} 
							 }
						 }
					
					System.out.println(">>>>>>>>>>>>>>>>>>statuswisejftList	"+ statuswisejftList.size());	
					/*********status filter end***************/
					
					//List<JobForTeacher> lstJobForTeachers =new ArrayList<JobForTeacher>();
					List<String[]> lstJobForTeachers =new ArrayList<String[]>();
					
					if(flagfordata)
					if(entityID==2)
					{
						totalRecord=jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictCountRowForStatus(districtMaster,sortingcheck,sortOrderStrVal,schoolId,startDate,endDate,jobOrderId,statuswisejftList,statusflag);
						 lstJobForTeachers = jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictForStatus(districtMaster,sortingcheck,sortOrderStrVal,start, noOfRowInPage,schoolId,true,startDate,endDate,jobOrderId,statuswisejftList,statusflag);
						
					}
					else if(entityID==1) 
					{
						if(districtOrSchoolId!=0)
							 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
						totalRecord=jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictCountRow(districtMaster,sortingcheck,sortOrderStrVal,schoolId,startDate,endDate,jobOrderId);
						lstJobForTeachers = jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,start, noOfRowInPage,schoolId,true,startDate,endDate,jobOrderId);	 
					}
				
					
				 if(flagfordata)
			     if(schoolId!=0)
				 {			    
					 lstJobForTeachers.clear();	 
					totalRecord=jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictCountRow(districtMaster,sortingcheck,sortOrderStrVal,schoolId,startDate,endDate,jobOrderId);
					 lstJobForTeachers.addAll(jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,start, noOfRowInPage,schoolId,true,startDate,endDate,jobOrderId));
				}	
				 
				   
					if(totalRecord<end)
						end=totalRecord;
				
				
				 //List<JobForTeacher> finalJobForTeachers =new ArrayList<JobForTeacher>();
				 List<String[]> finalJobForTeachers =new ArrayList<String[]>();			
				//System.out.println("Total Records=="+totalRecord+" sub list== start=="+start+" end=="+end);
				finalJobForTeachers	=	lstJobForTeachers;//.subList(start,end);
				String fontPath = realPath;
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 8, Font.NORMAL);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 10);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							//Color bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						document = new Document(PageSize.A4,10f,10f,10f,10f);//PageSize.A4.rotate()		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject(Utility.getLocaleValuePropByKey("lblapplicantsnotcontacted", locale));
						document.addCreationDate();
						document.addTitle(Utility.getLocaleValuePropByKey("lblapplicantsnotcontacted", locale));

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;


						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						//para[1] = new Paragraph(" ",font20bold);
						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));

						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];
						
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblapplicantsnotcontacted", locale),font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						
						
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						
						
						document.add(new Phrase("\n"));


						float[] tblwidth={.30f,.30f,.30f,.20f,.16f};
						
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[5];
						cell = new PdfPCell[5];
				// header
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblDistrictName", locale),font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[0].setBorder(1);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(Utility.getLocaleValuePropByKey("lblCandidateName", locale),font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[1].setBorder(1);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJoTil", locale),font10_10);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
					//	cell[2].setBorder(1);
						mainTable.addCell(cell[2]);

						para[3] = new Paragraph(Utility.getLocaleValuePropByKey("lblJobAppliedDate", locale),font10_10);
						cell[3]= new PdfPCell(para[3]);
						cell[3].setBackgroundColor(bluecolor);
						cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[3].setBorder(1);
						mainTable.addCell(cell[3]);
						
						para[4] = new Paragraph(""+"Job Status",font10_10);
						cell[4]= new PdfPCell(para[4]);
						cell[4].setBackgroundColor(bluecolor);
						cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
						//cell[3].setBorder(1);
						mainTable.addCell(cell[4]);
					
						document.add(mainTable);
						
						if(finalJobForTeachers.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgNorecordfound", locale),font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 //cell[index].setBorder(1);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
				     String sta="";
						if(finalJobForTeachers.size()>0){
							// for(JobForTeacher jft:finalJobForTeachers) 
							for (Iterator it = finalJobForTeachers.iterator(); it.hasNext();) 					            
							 {
								Object[] row = (Object[]) it.next();
								int index=0;
									
								 mainTable = new PdfPTable(tblwidth);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[5];
								 cell = new PdfPCell[5];
								
								 
								 para[index] = new Paragraph(row[0].toString(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 para[index] = new Paragraph(""+row[1].toString()+" "+row[2].toString()+"\n("+row[3].toString()+")",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 para[index] = new Paragraph(""+row[4].toString()+"("+row[7].toString()+")",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate((Date)row[5]),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 
								 if(row[6].toString().equalsIgnoreCase("A"))
									{
										sta="Active";
									}
									else
									{
										sta="Inactive";
									}
								 
								 para[index] = new Paragraph(""+sta,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								 
								 document.add(mainTable);
							 }
						}
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
			return true;
	}
	
	public String applicantsListExportEXL(String districtName,String schoolName,int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,boolean exportcheck,String startDate,String endDate,String jobOrderId,String jobStatus)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = null;
		int sortingcheck=0;
		try{
			boolean flagfordata=true;
			if(!districtName.trim().equals("") && districtOrSchoolId==0)
				flagfordata=false;
			else if(!schoolName.equals("") && schoolId==0)
				flagfordata=false;
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="firstName";
				String sortOrderNoField="firstName";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("firstName"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("jobTitle"))
					sortingcheck=2;
				else  if(sortOrder.equalsIgnoreCase("createdDateTime"))
					sortingcheck=3;
					else  if(sortOrder.equalsIgnoreCase("createdDateTime")){
						sortingcheck=4;
				}else
					sortingcheck=5;
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				 
				
				
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				
				
				 /********status filter*********/
			     boolean statusflag=false;
			     List<SecondaryStatus> lstSecondaryStatus =new ArrayList<SecondaryStatus>();
					List<JobForTeacher>  listByJobStatus = new ArrayList<JobForTeacher>();
					
					 if(!jobStatus.equals("") && !jobStatus.equals("0")){
						 System.out.println(" Filter StatusId Start ::  "+jobStatus);
						 String statusNames[] = jobStatus.split("#@");
						
					      if(!ArrayUtils.contains(statusNames,"0"))
					      {	 
					    	 statusflag=true;
							 DistrictMaster districtMaster2 = districtMasterDAO.findById(districtOrSchoolId, false, false);
							 lstSecondaryStatus = secondaryStatusDAO.findByMultipleStatusName(districtMaster2, statusNames);
					
							  List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
							  Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
							  for (StatusMaster statusMaster1 : statusMasterList) {
								  mapStatus.put(statusMaster1.getStatusShortName(),statusMaster1);
							  }
							  StatusMaster stMaster =null;
							  List <StatusMaster> statusMaster1st=new ArrayList<StatusMaster>();
							  
							  if(ArrayUtils.contains(statusNames,"Available")){
									  stMaster = mapStatus.get("comp");
									  statusMaster1st.add(stMaster);
							  }
							  if(ArrayUtils.contains(statusNames,"Rejected")){
								    stMaster = mapStatus.get("rem");
								    statusMaster1st.add(stMaster);
						     }
							  if(ArrayUtils.contains(statusNames,"Timed Out")){
								    stMaster = mapStatus.get("vlt");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Withdrew")){
								    stMaster = mapStatus.get("widrw");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Incomplete")){
								    stMaster = mapStatus.get("icomp");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Hired")){
								    stMaster = mapStatus.get("hird");
								    statusMaster1st.add(stMaster);
						     }   
							 
							 for(SecondaryStatus ss: lstSecondaryStatus)
							  {
								 if(ss.getStatusMaster()!=null)
									 statusMaster1st.add(ss.getStatusMaster()); 
							  }

						     listByJobStatus = jobForTeacherDAO.findAvailableCandidtesForReport(districtMaster2,statusMaster1st,lstSecondaryStatus);

							  
				         }
							  
					 }
					List<Long> statuswisejftList=new ArrayList<Long>();
					 if(statusflag){
						 if(listByJobStatus!=null && listByJobStatus.size()>0){
							 for (JobForTeacher jft : listByJobStatus) {
								 statuswisejftList.add(jft.getJobForTeacherId());
							} 
						 }
					 }
				
				System.out.println(">>>>>>>>>>>>>>>>>>statuswisejftList	"+ statuswisejftList.size());	
				/*********status filter end***************/
				//List<JobForTeacher> lstJobForTeachers =new ArrayList<JobForTeacher>();
				List<String[]> lstJobForTeachers =new ArrayList<String[]>();
				
				if(flagfordata)
				if(entityID==2)
				{
					totalRecord=jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictCountRowForStatus(districtMaster,sortingcheck,sortOrderStrVal,schoolId,startDate,endDate,jobOrderId,statuswisejftList,statusflag);
					 lstJobForTeachers = jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictForStatus(districtMaster,sortingcheck,sortOrderStrVal,start, noOfRowInPage,schoolId,true,startDate,endDate,jobOrderId,statuswisejftList,statusflag);
					}
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0)
						 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					totalRecord=jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictCountRow(districtMaster,sortingcheck,sortOrderStrVal,schoolId,startDate,endDate,jobOrderId);
					lstJobForTeachers = jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,start, noOfRowInPage,schoolId,true,startDate,endDate,jobOrderId);	 
				}
			
				//System.out.println("=====================lstForTeachers.size() :: "+lstJobForTeachers.size());
				
			
			 if(flagfordata)
		     if(schoolId!=0)
			 {
				 lstJobForTeachers.clear();	
				 totalRecord=jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictCountRow(districtMaster,sortingcheck,sortOrderStrVal,schoolId,startDate,endDate,jobOrderId);
				 lstJobForTeachers.addAll(jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,start, noOfRowInPage,schoolId,true,startDate,endDate,jobOrderId));
			}	 
			 if(totalRecord<end)
					end=totalRecord;
				//System.out.println("Total Records=="+totalRecord+" sub list== start=="+start+" end=="+end);	
			 List<String[]> finalJobForTeachers =new ArrayList<String[]>();
		
			//System.out.println("Total Records=="+totalRecord+" sub list== start=="+start+" end=="+end);
			finalJobForTeachers	=	lstJobForTeachers;//.subList(start,end);
			
			 //  Excel   Exporting	
					String time = String.valueOf(System.currentTimeMillis()).substring(6);
					//String basePath = request.getRealPath("/")+"/candidate";
					String basePath = request.getSession().getServletContext().getRealPath ("/")+"/joblist";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="Applicantscontactedbyschool"+time+".xls";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);

					WorkbookSettings wbSettings = new WorkbookSettings();

					wbSettings.setLocale(new Locale("en", "EN"));

					WritableCellFormat timesBoldUnderline;
					WritableCellFormat header;
					WritableCellFormat headerBold;
					WritableCellFormat times;
					WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
					workbook.createSheet("Applicantscontactedbyschool", 0);
					WritableSheet excelSheet = workbook.getSheet(0);
					WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
					// Define the cell format
					times = new WritableCellFormat(times10pt);
					// Lets automatically wrap the cells
					times.setWrap(true);

					// Create create a bold font with unterlines
					WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
					WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

					timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
					timesBoldUnderline.setAlignment(Alignment.CENTRE);
					// Lets automatically wrap the cells
					timesBoldUnderline.setWrap(true);

					header = new WritableCellFormat(times10ptBoldUnderline);
					headerBold = new WritableCellFormat(times10ptBoldUnderline);
					CellView cv = new CellView();
					cv.setFormat(times);
					cv.setFormat(timesBoldUnderline);
					cv.setAutosize(true);
					header.setBackground(Colour.GRAY_25);

					// Write a few headers
					excelSheet.mergeCells(0, 0, 4, 1);
					Label label;
					label = new Label(0, 0,  Utility.getLocaleValuePropByKey("lblapplicantsnotcontacted", locale) , timesBoldUnderline);
					excelSheet.addCell(label);
					excelSheet.mergeCells(0, 4, 4, 4);
					label = new Label(0, 4, "");
					excelSheet.addCell(label);
					excelSheet.getSettings().setDefaultColumnWidth(18);
					
					int k=5;
					int col=1;
					label = new Label(0, k, Utility.getLocaleValuePropByKey("lblDistrictName", locale),header); 
					excelSheet.addCell(label);
					label = new Label(1, k, Utility.getLocaleValuePropByKey("lblCandidateName", locale) ,header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJoTil", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJobAppliedDate", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, "Job Status",header); 
					excelSheet.addCell(label);
					
					
					k=k+1;
					if(finalJobForTeachers.size()==0)
					{	
						excelSheet.mergeCells(0, k, 4, k);
						label = new Label(0, k, Utility.getLocaleValuePropByKey("lblNoRecord", locale)); 
						excelSheet.addCell(label);
					}
					String sta="";
			if(finalJobForTeachers.size()>0)
			// for(JobForTeacher jft:finalJobForTeachers) 
			 for (Iterator it = finalJobForTeachers.iterator(); it.hasNext();) 					            
					{
					Object[] row = (Object[]) it.next();			
						col=1;
					    label = new Label(0, k, row[0].toString()); 
						excelSheet.addCell(label);
						 
						label = new Label(1, k,row[1].toString()+" "+row[2].toString()+"\n("+row[3].toString()+")"); 
						excelSheet.addCell(label);
						
						label = new Label(++col, k, row[4].toString()+"("+row[7].toString()+")"); 
						excelSheet.addCell(label);
						
						label = new Label(++col, k,Utility.convertDateAndTimeToUSformatOnlyDate((Date)row[5])); 
						excelSheet.addCell(label);
						
						if(row[6].toString().equalsIgnoreCase("A"))
						{
							sta="Active";
						}
						else
						{
							sta="Inactive";
						}
						
						label = new Label(++col, k,sta); 
						excelSheet.addCell(label);	
						
					   k++; 
			}
			workbook.write();
			workbook.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return fileName;
  }
	
	public String applicantsListPrintPreview(String districtName,String schoolName,int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,boolean exportcheck,String startDate,String endDate,String jobOrderId,String jobStatus)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
			boolean flagfordata=true;
			if(!districtName.trim().equals("") && districtOrSchoolId==0)
				flagfordata=false;
			else if(!schoolName.equals("") && schoolId==0)
				flagfordata=false;
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="firstName";
				String sortOrderNoField="firstName";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("firstName"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("jobTitle"))
					sortingcheck=2;
				else  if(sortOrder.equalsIgnoreCase("createdDateTime"))
					sortingcheck=3;
					else  if(sortOrder.equalsIgnoreCase("status")){
						sortingcheck=4;
				}else
					sortingcheck=5;
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				
				
				
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				
				 /********status filter*********/
			     boolean statusflag=false;
			     List<SecondaryStatus> lstSecondaryStatus =new ArrayList<SecondaryStatus>();
					List<JobForTeacher>  listByJobStatus = new ArrayList<JobForTeacher>();
					
					 if(!jobStatus.equals("") && !jobStatus.equals("0")){
						 System.out.println(" Filter StatusId Start ::  "+jobStatus);
						 String statusNames[] = jobStatus.split("#@");
						
					      if(!ArrayUtils.contains(statusNames,"0"))
					      {	 
					    	 statusflag=true;
							 DistrictMaster districtMaster2 = districtMasterDAO.findById(districtOrSchoolId, false, false);
							 lstSecondaryStatus = secondaryStatusDAO.findByMultipleStatusName(districtMaster2, statusNames);
					
							  List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
							  Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
							  for (StatusMaster statusMaster1 : statusMasterList) {
								  mapStatus.put(statusMaster1.getStatusShortName(),statusMaster1);
							  }
							  StatusMaster stMaster =null;
							  List <StatusMaster> statusMaster1st=new ArrayList<StatusMaster>();
							  
							  if(ArrayUtils.contains(statusNames,"Available")){
									  stMaster = mapStatus.get("comp");
									  statusMaster1st.add(stMaster);
							  }
							  if(ArrayUtils.contains(statusNames,"Rejected")){
								    stMaster = mapStatus.get("rem");
								    statusMaster1st.add(stMaster);
						     }
							  if(ArrayUtils.contains(statusNames,"Timed Out")){
								    stMaster = mapStatus.get("vlt");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Withdrew")){
								    stMaster = mapStatus.get("widrw");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Incomplete")){
								    stMaster = mapStatus.get("icomp");
								    statusMaster1st.add(stMaster);
						     }
							 if(ArrayUtils.contains(statusNames,"Hired")){
								    stMaster = mapStatus.get("hird");
								    statusMaster1st.add(stMaster);
						     }   
							 
							 for(SecondaryStatus ss: lstSecondaryStatus)
							  {
								 if(ss.getStatusMaster()!=null)
									 statusMaster1st.add(ss.getStatusMaster()); 
							  }

						     listByJobStatus = jobForTeacherDAO.findAvailableCandidtesForReport(districtMaster2,statusMaster1st,lstSecondaryStatus);

							  
				         }
							  
					 }
					List<Long> statuswisejftList=new ArrayList<Long>();
					 if(statusflag){
						 if(listByJobStatus!=null && listByJobStatus.size()>0){
							 for (JobForTeacher jft : listByJobStatus) {
								 statuswisejftList.add(jft.getJobForTeacherId());
							} 
						 }
					 }
				
				System.out.println(">>>>>>>>>>>>>>>>>>statuswisejftList	"+ statuswisejftList.size());	
				/*********status filter end***************/
				//List<JobForTeacher> lstJobForTeachers =new ArrayList<JobForTeacher>();
				List<String[]> lstJobForTeachers =new ArrayList<String[]>();
				
				//System.out.println("entityID=="+entityID);
				if(flagfordata)
				if(entityID==2)
				{
					totalRecord=jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictCountRowForStatus(districtMaster,sortingcheck,sortOrderStrVal,schoolId,startDate,endDate,jobOrderId,statuswisejftList,statusflag);
				 lstJobForTeachers = jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictForStatus(districtMaster,sortingcheck,sortOrderStrVal,start, noOfRowInPage,schoolId,true,startDate,endDate,jobOrderId,statuswisejftList,statusflag);
					}
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0)
						 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					totalRecord=jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictCountRow(districtMaster,sortingcheck,sortOrderStrVal,schoolId,startDate,endDate,jobOrderId);
				lstJobForTeachers = jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,start, noOfRowInPage,schoolId,true,startDate,endDate,jobOrderId);	 
				}
			
				System.out.println("=====================lstForTeachers.size() :: "+lstJobForTeachers.size());
				
			
			 if(flagfordata)
		     if(schoolId!=0)
			 {		    	
				 lstJobForTeachers.clear();	 
				 totalRecord=jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrictCountRow(districtMaster,sortingcheck,sortOrderStrVal,schoolId,startDate,endDate,jobOrderId);
				lstJobForTeachers.addAll(jobForTeacherDAO.applicantsNotContactedListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,start, noOfRowInPage,schoolId,true,startDate,endDate,jobOrderId));
			}	
			 if(totalRecord<end)
					end=totalRecord;
				//System.out.println("Total Records=="+totalRecord+" sub list== start=="+start+" end=="+end);
			
			// List<JobForTeacher> finalJobForTeachers =new ArrayList<JobForTeacher>(); 
			 List<String[]> finalJobForTeachers =new ArrayList<String[]>();
			
			//System.out.println("Total Records=="+totalRecord+" sub list== start=="+start+" end=="+end);
			finalJobForTeachers	=	lstJobForTeachers;//.subList(start,end);

			tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'> "+Utility.getLocaleValuePropByKey("lblapplicantsnotcontacted", locale)+"</div><br/>");
			tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");
			
			tmRecords.append("<table  id='tblGridPrint' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			tmRecords.append("<th width='25%'  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblDistrictName", locale)+"</th>");
			tmRecords.append("<th width='20%' style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblCandidateName", locale)+"</th>");
			tmRecords.append("<th width='25%' style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblJoTil", locale)+"</th>");
			tmRecords.append("<th width='15%' style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblJobAppliedDate", locale)+"</th>");
		
			tmRecords.append("<th width='15%' style='text-align:left' valign='top'>Job Status</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(finalJobForTeachers.size()==0){
			 tmRecords.append("<tr><td colspan='4' align='left' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			}
			String sta="";
		
			if(finalJobForTeachers.size()>0){
			 //for(JobForTeacher jft:finalJobForTeachers) 
			   for (Iterator it = finalJobForTeachers.iterator(); it.hasNext();) 					            
				{
				Object[] row = (Object[]) it.next();
				 tmRecords.append("<tr>");	
				tmRecords.append("<td style='font-size:12px;'>"+row[0].toString()+"</td>");				
				tmRecords.append("<td style='font-size:12px;'>"+row[1].toString()+" "+row[2].toString()+"<br/>("+row[3].toString()+")</td>");
				tmRecords.append("<td style='font-size:12px;'>"+row[4].toString()+"("+row[7].toString()+")"+"</td>");
				tmRecords.append("<td style='font-size:12px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate((Date)row[5])+"</td>");
				
				if(row[6].toString().equalsIgnoreCase("A"))
				{
					sta="Active";
				}
				else
				{
					sta="Inactive";
				}
				
				tmRecords.append("<td style='font-size:12px;'>"+sta+"</td>");
			   tmRecords.append("</tr>");
			 }
			}
	

		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){}
		
	 return tmRecords.toString();
  }
}
