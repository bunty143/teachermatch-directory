package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobOrder;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAffidavit;
import tm.bean.TeacherAnswerDetail;
import tm.bean.TeacherAssessmentAttempt;
import tm.bean.TeacherAssessmentOption;
import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.TeacherHonor;
import tm.bean.TeacherInvolvement;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherRole;
import tm.bean.assessment.AssessmentCompetencyScore;
import tm.bean.assessment.AssessmentDomainScore;
import tm.bean.assessment.ScoreLookupMaster;
import tm.bean.cgreport.PercentileCalculation;
import tm.bean.cgreport.PercentileCalculationCompetency;
import tm.bean.cgreport.RawDataForCompetency;
import tm.bean.cgreport.RawDataForDomain;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.ObjectiveMaster;
import tm.bean.master.PdStrengthsAndOpportunities;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAffidavitDAO;
import tm.dao.TeacherAnswerDetailDAO;
import tm.dao.TeacherAssessmentAttemptDAO;
import tm.dao.TeacherAssessmentQuestionDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherHonorDAO;
import tm.dao.TeacherInvolvementDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherRoleDAO;
import tm.dao.assessment.AssessmentCompetencyScoreDAO;
import tm.dao.assessment.AssessmentDomainScoreDAO;
import tm.dao.cgreport.PercentileCalculationCompetencyDAO;
import tm.dao.cgreport.PercentileCalculationDAO;
import tm.dao.cgreport.RawDataForCompetencyDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.dao.master.CompetencyMasterDAO;
import tm.dao.master.ObjectiveMasterDAO;
import tm.dao.master.PdStrengthsAndOpportunitiesDAO;
import tm.utility.HTML2Text;
import tm.utility.Utility;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.List;
import com.lowagie.text.ListItem;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfGState;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class CandidateReportService {

	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private CompetencyMasterDAO competencyMasterDAO;
	public void setCompetencyMasterDAO(CompetencyMasterDAO competencyMasterDAO) {
		this.competencyMasterDAO = competencyMasterDAO;
	}

	@Autowired
	private ObjectiveMasterDAO objectiveMasterDAO;
	public void setObjectiveMasterDAO(ObjectiveMasterDAO objectiveMasterDAO) {
		this.objectiveMasterDAO = objectiveMasterDAO;
	}

	@Autowired
	private TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO;
	public void setTeacherAssessmentQuestionDAO(TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO) {
		this.teacherAssessmentQuestionDAO = teacherAssessmentQuestionDAO;
	}

	@Autowired
	private TeacherAnswerDetailDAO teacherAnswerDetailDAO;
	public void setTeacherAnswerDetailDAO(TeacherAnswerDetailDAO teacherAnswerDetailDAO) {
		this.teacherAnswerDetailDAO = teacherAnswerDetailDAO;
	}

	@Autowired
	private PdStrengthsAndOpportunitiesDAO pdStrengthsAndOpportunitiesDAO;
	public void setPdStrengthsAndOpportunitiesDAO(PdStrengthsAndOpportunitiesDAO pdStrengthsAndOpportunitiesDAO) {
		this.pdStrengthsAndOpportunitiesDAO = pdStrengthsAndOpportunitiesDAO;
	}

	@Autowired
	private PercentileCalculationService percentileCalculationService;
	public void setPercentileCalculationService(PercentileCalculationService percentileCalculationService) {
		this.percentileCalculationService = percentileCalculationService;
	}

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(TeacherPersonalInfoDAO teacherPersonalInfoDAO) 
	{
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}

	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	public void setTeacherAcademicsDAO(TeacherAcademicsDAO teacherAcademicsDAO) 
	{
		this.teacherAcademicsDAO = teacherAcademicsDAO;
	}

	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	public void setTeacherCertificateDAO(	TeacherCertificateDAO teacherCertificateDAO) 
	{
		this.teacherCertificateDAO = teacherCertificateDAO;
	}

	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	public void setTeacherExperienceDAO(TeacherExperienceDAO teacherExperienceDAO) 
	{
		this.teacherExperienceDAO = teacherExperienceDAO;
	}

	@Autowired
	private TeacherRoleDAO teacherRoleDAO;
	public void setTeacherRoleDAO(TeacherRoleDAO teacherRoleDAO) 
	{
		this.teacherRoleDAO = teacherRoleDAO;
	}

	@Autowired
	private TeacherInvolvementDAO teacherInvolvementDAO;
	public void setTeacherInvolvementDAO(TeacherInvolvementDAO teacherInvolvementDAO) 
	{
		this.teacherInvolvementDAO = teacherInvolvementDAO;
	}

	@Autowired
	private TeacherHonorDAO teacherHonorDAO;
	public void setTeacherHonorDAO(TeacherHonorDAO teacherHonorDAO) 
	{
		this.teacherHonorDAO = teacherHonorDAO;
	}

	@Autowired
	private TeacherAffidavitDAO teacherAffidavitDAO;
	public void setTeacherAffidavitDAO(TeacherAffidavitDAO teacherAffidavitDAO) 
	{
		this.teacherAffidavitDAO = teacherAffidavitDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private RawDataForDomainDAO rawDataForDomainDAO;
	public void setRawDataForDomainDAO(RawDataForDomainDAO rawDataForDomainDAO) {
		this.rawDataForDomainDAO = rawDataForDomainDAO;
	}
	
	@Autowired
	private PercentileCalculationDAO percentileCalculationDAO;
	public void setPercentileCalculationDAO(PercentileCalculationDAO percentileCalculationDAO) {
		this.percentileCalculationDAO = percentileCalculationDAO;
	}
	
	@Autowired
	private RawDataForCompetencyDAO rawDataForCompetencyDAO;
	public void setRawDataForCompetencyDAO(RawDataForCompetencyDAO rawDataForCompetencyDAO) {
		this.rawDataForCompetencyDAO = rawDataForCompetencyDAO;
	}
	
	@Autowired
	private PercentileCalculationCompetencyDAO percentileCalculationCompetencyDAO;
	public void setPercentileCalculationCompetencyDAO(PercentileCalculationCompetencyDAO percentileCalculationCompetencyDAO) {
		this.percentileCalculationCompetencyDAO = percentileCalculationCompetencyDAO;
	}
	
	@Autowired
	private TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO;
	
	@Autowired
	private AssessmentDomainScoreDAO assessmentDomainScoreDAO;
	
	@Autowired
	private AssessmentCompetencyScoreDAO assessmentCompetencyScoreDAO;
	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;
	
	public boolean generatePDReport(HttpServletRequest request,TeacherDetail teacherDetail,String reportPath,String fileName,String urlRootPath)
	{
		Font font60 = null;
		Font font20 = null;
		Font font16 = null;
		Font font12 = null;
		Font font12bold = null;
		Font font12Italic = null;
		Font font12ItalicBold = null;
		Font font12boldBlack = null;
		Font font12boldBlue = null;
		Font font7 = null;
		Font font9bold = null;
		Font font9 = null;
		BaseFont tahoma = null;
		Color blueColor =null;
		Color greenColor =null;
		String fontPath = request.getRealPath("/");
		try {
			//BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			//tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			tahoma = BaseFont.createFont(fontPath+"fonts/42933.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			//tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			blueColor = new Color(0,122,180); 
			greenColor = new Color(149,203,110);
			
			font60 = new Font(tahoma, 60,Font.BOLD);
			font20 = new Font(tahoma, 20,Font.BOLD);
			font12 = new Font(tahoma, 12);
			font12boldBlack = new Font(tahoma,12,Font.BOLD);
			font12boldBlue = new Font(tahoma,12,Font.BOLD,blueColor);
			font12bold = new Font(tahoma, 12,Font.BOLD);
			font12Italic = new Font(tahoma, 12,Font.ITALIC);
			font12ItalicBold = new Font(tahoma, 12,Font.ITALIC|Font.BOLD,greenColor);
			font16 = new Font(tahoma, 16,Font.BOLD);
			font7 = new Font(tahoma, 7);
			font9bold = new Font(tahoma, 8,Font.BOLD);
			font9 = new Font(tahoma, 9);
			
			//font9bold = new Font(tahoma, 9);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		//HeaderAndFooter 

		try{
			String locale = Utility.getValueOfPropByKey("locale");
			String epi = Utility.getLocaleValuePropByKey("epi", locale);
			String pdr =  Utility.getLocaleValuePropByKey("pdr", locale);
			String pdr1 =  Utility.getLocaleValuePropByKey("pdr1", locale);
			String epiTaken = Utility.getLocaleValuePropByKey("epiTaken", locale);
			String printDate = Utility.getLocaleValuePropByKey("printDate", locale); 
			String labelContents =  Utility.getLocaleValuePropByKey("contents", locale);
			String interprt = Utility.getLocaleValuePropByKey("interprt", locale);
			String guidelines = Utility.getLocaleValuePropByKey("guidelines", locale);
			String useUrResult = Utility.getLocaleValuePropByKey("useUrResult", locale);
			String labelScore =  Utility.getLocaleValuePropByKey("score", locale);
			String strengths = Utility.getLocaleValuePropByKey("strengths", locale);
			String opportun = Utility.getLocaleValuePropByKey("opportun", locale);
			String pdp = Utility.getLocaleValuePropByKey("pdp", locale);
			String opening = Utility.getLocaleValuePropByKey("opening", locale);
			String congrats = Utility.getLocaleValuePropByKey("congrats", locale);
			String pdInterpretPara = Utility.getLocaleValuePropByKey("pdInterpretPara", locale);
			String pdguidelines = Utility.getLocaleValuePropByKey("pdguidelines", locale);
			String howUndrstndReport= Utility.getLocaleValuePropByKey("howUndrstndReport", locale);
			String urTMPersonlized = Utility.getLocaleValuePropByKey("urTMPersonlized", locale);
			String pdGuidelinesPara1 = Utility.getLocaleValuePropByKey("pdGuidelinesPara1", locale);
			String planCreateInstrAnalzAdjst = Utility.getLocaleValuePropByKey("planCreateInstrAnalzAdjst", locale);
			String pdGuidelinesPara2  = Utility.getLocaleValuePropByKey("pdGuidelinesPara2", locale);
		    String howUseReport = Utility.getLocaleValuePropByKey("howUseReport", locale);
			String pdGuidelinesPara34 = Utility.getLocaleValuePropByKey("pdGuidelinesPara34", locale);
			String resourse =  Utility.getLocaleValuePropByKey("resourse", locale);
			String resourse1 = Utility.getLocaleValuePropByKey("resourse1", locale);			
			String resourse2 = Utility.getLocaleValuePropByKey("resourse2", locale);
			String resourse3 = Utility.getLocaleValuePropByKey("resourse3", locale);
			String resourse4 = Utility.getLocaleValuePropByKey("resourse4", locale);
			String resourse5 = Utility.getLocaleValuePropByKey("resourse5", locale);
			String resourse6 = Utility.getLocaleValuePropByKey("resourse6", locale);		
			String resourse7 = Utility.getLocaleValuePropByKey("resourse7", locale);	
			String resourse8 = Utility.getLocaleValuePropByKey("resourse8", locale);
			String resourse9 = Utility.getLocaleValuePropByKey("resourse9", locale);	
			String resultPara  = Utility.getLocaleValuePropByKey("resultPara", locale);
			String resultPara1  = Utility.getLocaleValuePropByKey("resultPara1", locale);
			String resultPara111 = Utility.getLocaleValuePropByKey("resultPara111", locale);
			String resultPara2  = Utility.getLocaleValuePropByKey("resultPara2", locale);
			String resultPara3  = Utility.getLocaleValuePropByKey("resultPara3", locale);
			String resultPara4  = Utility.getLocaleValuePropByKey("resultPara4", locale);
			String resultPara5  = Utility.getLocaleValuePropByKey("resultPara5", locale);
			String resultPara6  = Utility.getLocaleValuePropByKey("resultPara6", locale);
			String resultPara7  = Utility.getLocaleValuePropByKey("resultPara7", locale);
			String resultPara8  = Utility.getLocaleValuePropByKey("resultPara8", locale);
			String resultPara9  = Utility.getLocaleValuePropByKey("resultPara9", locale);
			String resultPara10  = Utility.getLocaleValuePropByKey("resultPara10", locale);
			String resultPara11  = Utility.getLocaleValuePropByKey("resultPara11", locale);
			String teachingSkills = Utility.getLocaleValuePropByKey("teachingSkills", locale);
			String totalScore  = Utility.getLocaleValuePropByKey("totalScore", locale);
			String scoreSummry1   = Utility.getLocaleValuePropByKey("scoreSummry1", locale);
			String scoreSummry2   = Utility.getLocaleValuePropByKey("scoreSummry2", locale);
			String scoreSummry3   = Utility.getLocaleValuePropByKey("scoreSummry3", locale);
			String scoreSummry4   = Utility.getLocaleValuePropByKey("scoreSummary4", locale);
			String scoreSummry5   = Utility.getLocaleValuePropByKey("scoreSummary5", locale);
			String scoreSummry6   = Utility.getLocaleValuePropByKey("scoreSummary6", locale);
			String scoreSummry7   = Utility.getLocaleValuePropByKey("scoreSummary7", locale);
			String scoreSummry8   = Utility.getLocaleValuePropByKey("scoreSummary8", locale);
			String scoreSummry9   = Utility.getLocaleValuePropByKey("scoreSummary9", locale);
			String scoreSummry10   = Utility.getLocaleValuePropByKey("scoreSummary10", locale);
			String strength1    = Utility.getLocaleValuePropByKey("strength1 ", locale);
			String strength2    = Utility.getLocaleValuePropByKey("strength2", locale);
			
			String pdp1    = Utility.getLocaleValuePropByKey("pdp1", locale);
			String pdp2    = Utility.getLocaleValuePropByKey("pdp2", locale);
			String pdp3    = Utility.getLocaleValuePropByKey("pdp3", locale);
			String pdp4    = Utility.getLocaleValuePropByKey("pdp4", locale);
			String pdp5    = Utility.getLocaleValuePropByKey("pdp5", locale);
			String pdp6    = Utility.getLocaleValuePropByKey("pdp6", locale);
			String pdp7    = Utility.getLocaleValuePropByKey("pdp7", locale);
			String pdp8    = Utility.getLocaleValuePropByKey("pdp8", locale);
			String pdp9    = Utility.getLocaleValuePropByKey("pdp9", locale);
			String nationalNorm  = Utility.getLocaleValuePropByKey("nationalNorm", locale);

			File file = new File(reportPath+"/temp");
			if(!file.exists())
				file.mkdirs();
			else
				Utility.deleteAllFileFromDir(reportPath+"/temp");

			document=new Document(PageSize.A4,10f,10f,20f,30f);
			//document = new Document(PageSize.A4, 20, 10, 10, 10);
			 //document.setMarginMirroringTopBottom(true);
			//document.setMargins(15, 15, 15, 15);

			document.addAuthor("TeacherMatch");
			document.addCreator("TeacherMatch Inc.");
			document.addSubject(epi);
			document.addCreationDate();
			document.addTitle(pdr);

			fos = new FileOutputStream(reportPath+"/"+fileName);
			writer = PdfWriter.getInstance(document, fos);
			writer.setStrictImageSequence(true);
			//PdfWriter.getInstance(document, fos);
			//writer.setSpaceCharRatio(PdfWriter.NO_SPACE_CHAR_RATIO);

			//footerpara = new Paragraph("Generated by TeacherMatch: Created on - "+new Date()+"",font6);
			/*footerpara = new Paragraph(firstName+" "+lastName,font7);
			headerFooter = new HeaderFooter(footerpara,false);
			headerFooter.setAlignment(HeaderFooter.ALIGN_LEFT);
			headerFooter.setBorder(0);
			document.setFooter(headerFooter);

			footerpara = new Paragraph("",font12);
			headerFooter = new HeaderFooter(footerpara,false);
			headerFooter.setAlignment(HeaderFooter.ALIGN_RIGHT);
			headerFooter.setBorder(0);
			document.setHeader(headerFooter);*/

			//HeaderFooter event = new HeaderFooter((firstName+" "+lastName),font7);
			String firstName = teacherDetail.getFirstName();
			String lastName = teacherDetail.getLastName()==null?"":teacherDetail.getLastName();
			
			HeaderAndFooter event = new HeaderAndFooter((firstName+" "+lastName),font7);
			writer.setBoxSize("art", new Rectangle(36, 54, 559, 788));
			writer.setPageEvent(event);

			document.open();

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);
			/////////////////////////// Start 1st page /////////////////////////////////////////
			Paragraph [] para = null;
			PdfPCell [] cell = null;

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			PdfPCell blankcell = new PdfPCell(new Paragraph("     ",font7));
			blankcell.setBorder(0);
			//Image logo = Image.getInstance (urlRootPath+"/images/TMatch-Logo.png");//images/logo.png
			//Image logo = Image.getInstance (urlRootPath+"/images/Logo%20with%20Beta300.png");
			Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
			
			//System.out.println("Height::::::::: "+logo.getHeight());
			//System.out.println("Width::::::::: "+logo.getWidth());
			logo.scalePercent(75);
			//logo=null;
			//System.out.println("Height::::::::: "+logo.getHeight());
			//logo.scaleAbsolute(300,55);
			float[] widthParticularlogo = {.14f,.2f};
			PdfPTable logotable = new PdfPTable(widthParticularlogo);
			logotable.setWidthPercentage(90);
			PdfPCell logocol1 = new PdfPCell(new Paragraph("     ",font7));
			logocol1.setBorder(0);
			PdfPCell logocol2 = new PdfPCell(logo);
			logocol2.setBorder(0);
			
			logotable.addCell(logocol1);
			logotable.addCell(logocol2);
			////////////////////////////////////////////////////////////////
			cell[1]= new PdfPCell(logotable);
			//cell[1]= new PdfPCell(logo);
			cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[1].setBorder(0);

			para[2] = new Paragraph(pdr,font20);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);

			for(int i=0;i<2;i++)
				mainTable.addCell(cell[0]);

			mainTable.addCell(cell[1]);
			for(int i=0;i<3;i++)
				mainTable.addCell(cell[0]);
			mainTable.addCell(cell[2]);
			mainTable.addCell(cell[0]);

			para[2] = new Paragraph(epi,font20);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);

			mainTable.addCell(cell[2]);
			
			for(int i=0;i<1;i++)
				mainTable.addCell(cell[0]);

			//List<> teacherAssessmentAttempts = new ArrayList<TeacherAssessmentAttempt>();
			
			java.util.List<TeacherAssessmentAttempt> teacherAssessmentAttempts = new ArrayList<TeacherAssessmentAttempt>();
			java.util.List<TeacherAssessmentStatus> teacherAssessmentStatusList= null;
			try{
				
				java.util.List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
				teacherDetails.add(teacherDetail);
				
				JobOrder jobOrder = new JobOrder();
				jobOrder.setJobId(0);
				teacherAssessmentStatusList=teacherAssessmentStatusDAO.findAssessmentTakenIPIandEPI(teacherDetail,jobOrder);
				
				TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
				
				/*java.util.List<TeacherNormScore> lstTeacherNormScore= teacherNormScoreDAO.findTeacersNormScoresList(teacherDetails);
				System.out.println("ppppppppppppppppppppppppppp: "+lstTeacherNormScore.size());
				TeacherAssessmentdetail teacherAssessmentdetail = null;
				if(lstTeacherNormScore.size()>0)
				{
					teacherAssessmentdetail = lstTeacherNormScore.get(0).getTeacherAssessmentdetail();
				}
				System.out.println("teacherAssessmentdetail: "+teacherAssessmentdetail.getTeacherAssessmentId());
				if(teacherAssessmentdetail!=null)
					teacherAssessmentAttempts = teacherAssessmentAttemptDAO.findEpiCompletionDate(teacherAssessmentdetail);
				else
					teacherAssessmentAttempts = teacherAssessmentAttemptDAO.findEpiCompletionDate(teacherDetail);*/
			
				
				//para[2] = new Paragraph(epiTaken+" "+Utility.convertDateAndTimeToUSformatOnlyDate(teacherAssessmentAttempts.get(0).getAssessmentEndTime()),font12);
				para[2] = new Paragraph(epiTaken+" "+Utility.convertDateAndTimeToUSformatOnlyDate(teacherAssessmentStatus.getAssessmentCompletedDateTime()),font12);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[2].setBorder(0);
				
				mainTable.addCell(cell[2]);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			
			for(int i=0;i<3;i++)
				mainTable.addCell(cell[0]);
			
			
			para[2] = new Paragraph(firstName+" "+lastName,font20);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);

			mainTable.addCell(cell[2]);
			for(int i=0;i<3;i++)
				mainTable.addCell(cell[0]);

			try
			{
				para[2] = new Paragraph(printDate+" ",font12);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[2].setBorder(0);
				
				mainTable.addCell(cell[2]);
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			/*for(int i=0;i<3;i++)
				mainTable.addCell(cell[0]);*/
			
			
			para[2] = new Paragraph(Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font20);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);

			/*for(int i=0;i<3;i++)
				mainTable.addCell(cell[0]);*/

			mainTable.addCell(cell[2]);

			document.add(mainTable);
			document.newPage();

			/////////////////////End 1st page ///////////////////////////////////

			/////////////////////Start 2nd page ////////////////////////////////
			
			
			font20 = new Font(tahoma, 20,Font.BOLD,blueColor);
			font16 = new Font(tahoma, 16,Font.BOLD,blueColor);
			font12bold = new Font(tahoma, 12,Font.BOLD,greenColor);
			
			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);
			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph(labelContents+" ",font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			//mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);

			String dis = labelContents;
			String contents[] = {labelContents,interprt ,guidelines ,useUrResult ,labelScore ,strengths ,opportun ,pdp};

			for(int i=0;i<contents.length;i++)
			{
				para[2] = new Paragraph("\u2022 "+contents[i],font12);
				//para[2] = new Paragraph(Utility.addNoOfDots(contents[i], ".", 163),font12);
				//para[2] = new Paragraph(Utility.addNoOfDots(contents[i], ".", 148),font12);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[2].setLeading(0f,1.5f);
				cell[2].setBorder(0);
				mainTable.addCell(cell[2]);
			}

			document.add(mainTable);
			document.newPage();
			/////////////////////End 2nd page ////////////////////////////////		

			/////////////////////Start 3rd page ////////////////////////////////

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph(interprt+"",font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			//mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);

			para[1] = new Paragraph(opening ,font16);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);

			dis = congrats ;
			para[2] = new Paragraph(dis,font12);
			para[2].add(new Chunk(epi, font12Italic));

			dis = " "+pdInterpretPara;

			para[2].add(new Chunk(dis, font12));

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0f,1.5f);
			cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[2].setBorder(0);


			//mainTable.addCell(cell[0]);
			mainTable.addCell(cell[2]);

			document.add(mainTable);
			document.newPage();

			/////////////////////End 3rd page //////////////////////////////////

			/////////////////////Start 4th page ////////////////////////////////



			HTMLWorker htmlWorker = null;
			//HTMLWorker htmlWorker = new HTMLWorker(document);
			/*String str="<br><br><br><table align='center'><tr><td><IMG SRC='"+urlRootPath+"images/learning_framework.png' width='500'   BORDER='0'>"+
			"</td></tr></table>";*/
			/*	String str="<br><table align='center'><tr><td><IMG SRC='"+urlRootPath+"images/learning_framework.png' width='600' height='750' BORDER='0'>"+
			"</td></tr></table>";*/
		/*	String str="<br><table align='center'><tr><td><IMG SRC='"+urlRootPath+"images/learning_framework.png' BORDER='0'>"+
			"</td></tr></table>";*/

			//htmlWorker.parse(new StringReader(str));

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);
			cell = new PdfPCell[3];

			//Image lf = Image.getInstance (urlRootPath+"/images/learning_framework.png");
			Image lf = Image.getInstance (fontPath+"/images/learning_framework.png");
			lf.scalePercent(75);

			cell[1]= new PdfPCell(lf);
			cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
			//cell[1].setTop(200);
			cell[1].setBorder(0);

			cell[0]= new PdfPCell(new Paragraph(" ",font16));
			cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[0].setBorder(0);

			mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);
			document.add(mainTable);
			document.newPage();

			/////////////////////end 4th page ////////////////////////////////			

			/////////////////////Start 5th page ////////////////////////////////
			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);
			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph(pdguidelines ,font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			//mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);

			para[1] = new Paragraph(howUndrstndReport,font16);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);

			para[2] = new Paragraph(urTMPersonlized+" ",font12);

			dis = pdGuidelinesPara1;

			para[2].add(new Chunk(pdr1, font12Italic));
			para[2].add(new Chunk(dis, font12));
			para[2].add(new Chunk(planCreateInstrAnalzAdjst, font12Italic));

			dis = pdGuidelinesPara2;
			para[2].add(new Chunk(dis, font12));
			para[2].setLeading(1f);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0f,1.5f);
			cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[2].setBorder(0);


			mainTable.addCell(cell[2]);
			mainTable.addCell(blankcell);

			para[1] = new Paragraph(howUseReport,font16);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(cell[1]);
			//mainTable.addCell(cell[0]);

			dis = pdGuidelinesPara34;
			para[2] = new Paragraph(dis,font12);

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0f,1.5f);
			cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[2].setBorder(0);

			mainTable.addCell(cell[2]);

			para[1] = new Paragraph(resourse ,font12bold);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);
			//mainTable.addCell(cell[0]);

			document.add(mainTable);

			List sublist =  new List(false, 12);
			//List sublist =  new List(List.ORDERED, 12);
			sublist.setListSymbol(new Chunk("\u2022", font9));
			dis = resourse1;
			sublist.add(new ListItem(dis,font12));

			dis = resourse2;
			sublist.add(new ListItem(dis,font12));

			dis = resourse3;
			sublist.add(new ListItem(dis,font12));

			dis = resourse4;
			sublist.add(new ListItem(dis,font12));

			List list = new List(false, 15);
			list.setListSymbol(new Chunk("\u2022", font12));
			list.setIndentationLeft(32);
			list.setIndentationRight(32);

			list.add(new ListItem(resourse5,font12));
			list.add(sublist);
			list.add(new ListItem(resourse6,font12));
			list.add(new ListItem(resourse7,font12));
			list.add(new ListItem(resourse8,font12));

			document.add(list);

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);


			dis=resourse9;

			para[1] = new Paragraph(dis,font12);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setLeading(0f,1.5f);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);

			document.add(mainTable);
			document.newPage();



			/////////////////////End 5th page //////////////////////////////////

			///////////////////// Start 6th page //////////////////////////////
			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph(resultPara,font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			//mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);


			para[1] = new Paragraph(resultPara1,font12bold);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(cell[1]);
			//mainTable.addCell(cell[0]);


			para[1] = new Paragraph("\u2022"+resultPara111+"",font12);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);
			mainTable.addCell(cell[1]);

			document.add(mainTable);

			sublist =  new List(false, 12);
			sublist.setListSymbol(new Chunk("\u2022", font9));

			dis = resultPara2;
			sublist.add(new ListItem(dis,font12));

			list = new List(false, 15);
			list.setListSymbol(new Chunk("\u2022", font12));
			list.setIndentationLeft(32);
			list.setIndentationRight(32);

			list.add(sublist);
			document.add(list);


			list = new List(false, 15);
			list.setListSymbol(new Chunk("\u2022", font12));
			list.setIndentationLeft(32);
			list.setIndentationRight(32);
			list.add(new ListItem(resultPara3,font12));

			sublist =  new List(false, 12);
			sublist.setListSymbol(new Chunk("\u2022", font9));

			dis =resultPara4;
			sublist.add(new ListItem(dis,font12));

			list.add(sublist);
			document.add(list);

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			dis = resultPara5;
			para[1] = new Paragraph(dis,font12);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setLeading(0f,1.5f);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			//mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);
			//mainTable.addCell(blankcell);

			para[1] = new Paragraph(resultPara6,font12bold);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setLeading(0f,1.5f);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(cell[1]);
			//mainTable.addCell(cell[0]);
			document.add(mainTable);

			list = new List(false, 15);
			list.setListSymbol(new Chunk("\u2022", font12));
			list.setIndentationLeft(32);
			list.setIndentationRight(32);

			list.add(new ListItem(resultPara7,font12));
			list.add(new ListItem(resultPara8,font12));
			list.add(new ListItem(resultPara9,font12));
			list.add(new ListItem(resultPara10,font12));

			document.add(list);

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);
			para[1] = new Paragraph(resultPara11,font12bold);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setLeading(0f,1.5f);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);
			mainTable.addCell(blankcell);
			document.add(mainTable);
			document.newPage();
			///////////////////// Start 6th page //////////////////////////////

			/////////////////////Start 7th page ////////////////////////////////

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph(labelScore,font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			para[2] = new Paragraph(scoreSummry1+" ",font12);
			para[2].add(new Chunk(teachingSkills, font12Italic));
			dis = scoreSummry3;
			para[2].add(new Chunk(dis, font12));
			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0f,1.5f);
			cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[2].setBorder(0);

			mainTable.addCell(cell[1]);
			mainTable.addCell(blankcell);
			mainTable.addCell(cell[2]);
			mainTable.addCell(blankcell);
		
			ScoreLookupMaster scoreLookupMaster = null;
			if(teacherAssessmentStatusList.size()>0)
			{
				scoreLookupMaster = teacherAssessmentStatusList.get(0).getTeacherAssessmentdetail().getScoreLookupMaster(); 
			}
			java.util.List<CompetencyMaster> competencyMasters = competencyMasterDAO.getCompetenciesForPDR(true);
			System.out.println("domainName: ::::::::::::::::::::::::: :::::::::::::::::::: ");
			DomainMaster domainMaster=null;
			if(competencyMasters.size()>0)
				domainMaster = competencyMasters.get(0).getDomainMaster();
			
			double []ar = null;
			double teacherPercentage = 0;
			double totalPercentage = 0;
			double candidatePercentile = 0;
			int tep = 0;
			int top = 0;
			
			/////////////////////// new Process ///////////////////////////
			java.util.List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>(); 
			teacherDetails.add(teacherDetail);
			System.out.println("TeacherId: :      KL: "+teacherDetail.getTeacherId());
			//java.util.List<RawDataForDomain> lstRawDataForDomain = rawDataForDomainDAO.findByDomainAndTeachers(domainMaster,teacherDetails);
			java.util.List<TeacherAssessmentdetail> teacherAssessmentdetails = new ArrayList<TeacherAssessmentdetail>();
			
			teacherAssessmentdetails.add(teacherAssessmentStatusList.get(0).getTeacherAssessmentdetail());
			
			java.util.List<RawDataForDomain> lstRawDataForDomain = rawDataForDomainDAO.findByDomainAndTeachersNew(domainMaster,teacherAssessmentdetails);
			
			
			Double score = null;
			if(lstRawDataForDomain.size()>0)
			{
				RawDataForDomain rawDataForDomain = lstRawDataForDomain.get(0);
				teacherPercentage = (rawDataForDomain.getScore()/rawDataForDomain.getMaxMarks())*100;
				score = rawDataForDomain.getScore();
				System.out.println("score: "+score);
			}
			
			java.util.List<Integer> scores = new ArrayList<Integer>();
			tep=(int)Math.round(teacherPercentage);
			scores.add((int)Math.round(score));
			AssessmentDomainScore assessmentDomainScore = null;
			if(scoreLookupMaster!=null)
			{
				java.util.List<DomainMaster> domainMasters = new ArrayList<DomainMaster>(); 
				domainMasters.add(domainMaster);
				
				java.util.List<AssessmentDomainScore>  assessmentDomainScores = assessmentDomainScoreDAO.findAllDomainByTeacher(teacherDetail, scoreLookupMaster, domainMasters);
				if(assessmentDomainScores.size()>0)
					assessmentDomainScore = assessmentDomainScores.get(0);
			}
			System.out.println("assessmentDomainScore:: "+assessmentDomainScore);
			if(assessmentDomainScore==null)
			{
				java.util.List<PercentileCalculation> lstPercentileCalculation = percentileCalculationDAO.findPercentileCalculationByScore(scores,domainMaster);
				System.out.println("lstPercentileCalculation: "+lstPercentileCalculation.size());
				if(lstPercentileCalculation.size()>0)
				{
					PercentileCalculation percentileCalculation = lstPercentileCalculation.get(0);
					totalPercentage = percentileCalculation.getPercentile();
					System.out.println("null totalPercentage: ::::: "+totalPercentage);
				}
			}else
			{
				totalPercentage = assessmentDomainScore.getPercentile();
				System.out.println("DomainName() : "+assessmentDomainScore.getDomainMaster().getDomainName()+" : totalPercentage : "+totalPercentage);
			}
			
			top = (int)Math.round(totalPercentage);
			
			////////////////////////////////////////////////////////////////////////////////////
			PDServices pds = new PDServices();

			//// Here need to change from new table.
			String rularImage = pds.getRular(teacherDetail.getTeacherId(), reportPath,urlRootPath);
			String competencyGridImage = pds.getProgressGridCompetencyWise(teacherDetail.getTeacherId(), reportPath, tep, top,"",urlRootPath);

			////////////////////////////////////////////////////////////////////////////////////

			////////////////////// New PDR ////////
			float[] widthMain = {.29f,.06f,.65f};
			PdfPTable rularTable = new PdfPTable(widthMain);
			rularTable.setWidthPercentage(90);

			cell[1]= new PdfPCell(new Paragraph(" ",font9bold));
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].setBorder(0);
			rularTable.addCell(cell[1]);

			cell[1]= new PdfPCell(new Paragraph(" ",font9bold));
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);
			rularTable.addCell(cell[1]);

			logo = Image.getInstance (reportPath+"/temp/"+rularImage);
			logo.scalePercent(64);
			cell[1]= new PdfPCell(logo);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setVerticalAlignment(Element.ALIGN_BOTTOM);
			cell[1].setBorder(0);
			rularTable.addCell(cell[1]);

			cell[1]= new PdfPCell(new Paragraph(teachingSkills +"         "+totalScore,font9bold));
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].setBorder(0);
			rularTable.addCell(cell[1]);


			cell[1]= new PdfPCell(new Paragraph(tep+"% ",font9bold));
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);
			rularTable.addCell(cell[1]);

			logo = Image.getInstance (reportPath+"/temp/"+competencyGridImage);
			logo.scalePercent(64);
			cell[1]= new PdfPCell(logo);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setVerticalAlignment(Element.ALIGN_BOTTOM);
			cell[1].setBorder(0);
			rularTable.addCell(cell[1]);


			cell[1]= new PdfPCell(rularTable);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].setBorder(0);

			mainTable.addCell(cell[1]);

			document.add(mainTable);

			///////////////////////////////


			/// vishwanath


			//java.util.List<CompetencyMaster> competencyMasters = competencyMasterDAO.getCompetenciesForPDR(true);
			Map<Integer,CompetencyMaster> map = new HashMap<Integer, CompetencyMaster>();
			Map<CompetencyMaster,Integer> map2 = new HashMap<CompetencyMaster,Integer >();
			
			PdfPTable headerTable = new PdfPTable(1);
			headerTable.setWidthPercentage(90);
			Paragraph pr = new Paragraph();
			PdfPCell col = new PdfPCell();
			dis = scoreSummry4;
			pr.add(new Chunk(dis, font12));
			pr.add(new Chunk(""+Math.round(teacherPercentage)+"%", font12boldBlue));
			dis = " "+scoreSummry5;
			pr.add(new Chunk(dis, font12));
			pr.add(new Chunk(""+Math.round(totalPercentage)+"%", font12boldBlue));
			dis = " "+scoreSummry6;
			pr.add(new Chunk(dis, font12));
			col= new PdfPCell(pr);
			col.setLeading(0f,1.5f);
			col.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			col.setBorder(0);
			headerTable.addCell(col);
			document.add(headerTable);

			
			///////////////// New process competency //////////////////////////////////////
			java.util.List<RawDataForCompetency> rawDataForCompetencieList = new LinkedList<RawDataForCompetency>();
			//rawDataForCompetencieList = rawDataForCompetencyDAO.findByCompetenciesAndTeacher(competencyMasters,teacherDetail);
			rawDataForCompetencieList = rawDataForCompetencyDAO.findByCompetenciesAndTeacher(competencyMasters,teacherAssessmentdetails);
			
			System.out.println("000000 rawDataForCompetencieList.size() : "+rawDataForCompetencieList.size());
			
			Map<Integer,RawDataForCompetency> rawDataForCompetencyMap = new HashMap<Integer, RawDataForCompetency>();
			for (RawDataForCompetency rawDataForCompetency : rawDataForCompetencieList) {
				rawDataForCompetencyMap.put(rawDataForCompetency.getCompetencyMaster().getCompetencyId(), rawDataForCompetency);
			}
			Map<Integer, AssessmentCompetencyScore> mapCompetency = new HashMap<Integer, AssessmentCompetencyScore>();
			java.util.List<AssessmentCompetencyScore>  assessmentCompetencyScores = assessmentCompetencyScoreDAO.findAllCompetencyByTeacher(teacherDetail, scoreLookupMaster, competencyMasters);
			for (AssessmentCompetencyScore assessmentCompetencyScore : assessmentCompetencyScores) {
				mapCompetency.put(assessmentCompetencyScore.getCompetencyMaster().getCompetencyId(), assessmentCompetencyScore);
			}
			///////////////////////////////////////////////////////////////////////////////
			RawDataForCompetency rawDataForCompetency = null;
			String rowData = "";
			for(CompetencyMaster competencyMaster: competencyMasters)
			{
				/////////////////// From rawDataFromCompetency ////////////////
				totalPercentage=0;
				score = 0.0;
				rawDataForCompetency = rawDataForCompetencyMap.get(competencyMaster.getCompetencyId());
				if(rawDataForCompetency!=null)
				{
					teacherPercentage = (rawDataForCompetency.getScore()/rawDataForCompetency.getMaxMarks())*100;
					tep = (int)Math.round(teacherPercentage);
					score = rawDataForCompetency.getScore();
					System.out.println("score: "+score);
				}
				/////////////////////////////////////////////
				
				//===================================================
				java.util.List<Integer> scores1 = new ArrayList<Integer>();
				tep=(int)Math.round(teacherPercentage);
				scores1.add((int)Math.round(score));
				System.out.println(competencyMaster.getCompetencyId()+" : "+scores1.get(0));
				
				if(mapCompetency.size()==0)
				{
					java.util.List<PercentileCalculationCompetency> lstPercentileCalculationCompetencies = percentileCalculationCompetencyDAO.findPercentileCalculationByScore(scores1,competencyMaster);
					System.out.println("lstPercentileCalculationCompetencies1: "+lstPercentileCalculationCompetencies.size());
					if(lstPercentileCalculationCompetencies.size()>0)
					{
						PercentileCalculationCompetency percentileCalculation = lstPercentileCalculationCompetencies.get(0);
						totalPercentage = percentileCalculation.getPercentile();
						System.out.println("totalPercentage1: ::::: "+totalPercentage);
					}
				}else
				{
					AssessmentCompetencyScore assessmentCompetencyScore = mapCompetency.get(competencyMaster.getCompetencyId());
					totalPercentage = assessmentCompetencyScore.getPercentile();
					System.out.println("CompetencyName: "+assessmentCompetencyScore.getCompetencyMaster().getCompetencyName()+" : totalPercentage : "+totalPercentage);
				}
				
				
				top = (int)Math.round(totalPercentage);
				//===================================================

				map.put(tep,competencyMaster);
				map2.put(competencyMaster, tep);
				competencyGridImage = pds.getProgressGridCompetencyWise(teacherDetail.getTeacherId(), reportPath, tep, top,competencyMaster.getCompetencyName(),urlRootPath);

				rowData = reportPath+"/temp/"+competencyGridImage;

				//////////////
				competencyMaster.setTeacherScore((double)tep);
				competencyMaster.setTotalScore((double)top);
				competencyMaster.setRowResult(rowData);
				rowData = "";

			}



			Collections.sort(competencyMasters,CompetencyMaster.competencyMasterComparator);

			rularTable = new PdfPTable(widthMain);
			rularTable.setWidthPercentage(90);

			cell[1]= new PdfPCell(new Paragraph(" ",font9bold));
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].setBorder(0);
			rularTable.addCell(cell[1]);

			cell[1]= new PdfPCell(new Paragraph(" ",font9bold));
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);
			rularTable.addCell(cell[1]);

			logo = Image.getInstance (reportPath+"/temp/"+rularImage);
			logo.scalePercent(64);
			cell[1]= new PdfPCell(logo);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setVerticalAlignment(Element.ALIGN_BOTTOM);
			cell[1].setBorder(0);
			rularTable.addCell(cell[1]);

			PdfPTable bootomTable = new PdfPTable(1);
			bootomTable.setWidthPercentage(90);
			Paragraph p1 = null;
			PdfPCell c1 = null;
			
			for(CompetencyMaster competencyMaster: competencyMasters)
			{
				p1 = new Paragraph(scoreSummry7+" ",font12);
				p1.add(new Chunk(competencyMaster.getCompetencyName(), font12ItalicBold));
				dis = " "+scoreSummry8+" ";
				p1.add(new Chunk(dis, font12));
				p1.add(new Chunk(""+Math.round(competencyMaster.getTeacherScore())+"%", font12boldBlue));
				dis = scoreSummry9 +" ";
				p1.add(new Chunk(dis, font12));
				p1.add(new Chunk(""+Math.round(competencyMaster.getTotalScore())+"%", font12boldBlue));
				dis = " "+scoreSummry10 ;
				p1.add(new Chunk(dis, font12));
				c1= new PdfPCell(p1);
				c1.setLeading(0f,1.5f);
				c1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
				c1.setBorder(0);
				bootomTable.addCell(c1);

				cell[1]= new PdfPCell(new Paragraph(competencyMaster.getCompetencyName(),font9bold));
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].setBorder(0);
				rularTable.addCell(cell[1]);

				cell[1]= new PdfPCell(new Paragraph(Math.round(competencyMaster.getTeacherScore())+"% ",font9bold));
				cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell[1].setBorder(0);
				rularTable.addCell(cell[1]);

				logo = Image.getInstance (competencyMaster.getRowResult());
				logo.scalePercent(64);
				cell[1]= new PdfPCell(logo);
				cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell[1].setVerticalAlignment(Element.ALIGN_BOTTOM);
				cell[1].setBorder(0);
				rularTable.addCell(cell[1]);
			}

			document.add(rularTable);

			float[] widthPersonalInfo = {.66f,.34f};
			mainTable = new PdfPTable(widthPersonalInfo);
			mainTable.setWidthPercentage(90);


			mainTable.addCell(cell[0]);
			mainTable.addCell(cell[0]);

			//logo = Image.getInstance (urlRootPath+"/images/triangleSmall.png");
			logo = Image.getInstance (fontPath+"/images/triangleSmall.png");

			cell[1]= new PdfPCell(logo);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setVerticalAlignment(Element.ALIGN_BOTTOM);
			cell[1].setBorder(0);
			mainTable.addCell(cell[1]);

			cell[1]= new PdfPCell(new Paragraph(nationalNorm,font12boldBlack));
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			//cell[1].setVerticalAlignment(Element.ALIGN_BOTTOM);
			cell[1].setBorder(0);
			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);
			mainTable.addCell(cell[0]);
			
			document.add(mainTable);
			document.add(bootomTable);

			//htmlWorker = new HTMLWorker(document);
			//htmlWorker.parse(new StringReader(sb.toString()));
			

			Collection<Integer> c = map.keySet();
			int max=Collections.max(c);
			int min=Collections.min(c);

			//map.containsValue("foo")

			document.newPage();

			/////////////////////End 7th page ////////////////////////////////

			//////////////////// Start 8th page /////////////////////////////

			CompetencyMaster competencyMasterMax = map.get(max);
			CompetencyMaster competencyMasterMin = map.get(min);

			CompetencyMaster compMax = null;
			CompetencyMaster compMin = null;

			int rankMax = competencyMasterMax.getRank();
			int rankMin = competencyMasterMin.getRank();
			for(CompetencyMaster competencyMaster: competencyMasters)
			{
				int p=map2.get(competencyMaster);


				if(p==(max))
				{
					//if((competencyMaster.getRank()==null?0:competencyMaster.getRank())>rankMax)
					if((competencyMaster.getRank()==null?0:competencyMaster.getRank())<rankMax)
					{
						compMax = competencyMaster;
						//new con
						rankMax = competencyMaster.getRank();
					}
				}

				if(p==(min))
				{
					//if((competencyMaster.getRank()==null?0:competencyMaster.getRank())>rankMin)
					if((competencyMaster.getRank()==null?0:competencyMaster.getRank())<rankMin)
					{
						compMin = competencyMaster;
						//new con
						rankMin = competencyMaster.getRank();
					}
				}



			}

			if(compMin!=null)
				competencyMasterMin = compMin;

			if(compMax!=null)
				competencyMasterMax = compMax;

			if(competencyMasterMin.getCompetencyId().equals(competencyMasterMax.getCompetencyId()))
			{
				//compMin = 
				for(CompetencyMaster competencyMaster: competencyMasters)
				{
					if((rankMin+1)==competencyMaster.getRank())
					{
						competencyMasterMin = competencyMaster;
						break;
					}
				}
			}

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph(strengths,font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			//mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);
			para[1] = new Paragraph(competencyMasterMax.getCompetencyName(),font16);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);
			//mainTable.addCell(cell[0]);

			dis = strength1 +" "+competencyMasterMax.getCompetencyName()+". " +strength2 ;
			
			para[2] = new Paragraph(dis,font12);

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0f,1.5f);
			cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[2].setBorder(0);

			mainTable.addCell(cell[2]);
			mainTable.addCell(blankcell);

			document.add(mainTable);


			java.util.List<ObjectiveMaster> objectiveMasters = objectiveMasterDAO.getObjectivesByCompetencyNoOrdering(competencyMasterMax);

			String maxCompetencyPoint = competencyMasterMax.getCompetencyShortName();
			java.util.List<PdStrengthsAndOpportunities> pdStrengthsAndOpportunitiesList =  null;
			java.util.List<PdStrengthsAndOpportunities> pdStrengthsAndOpportunitiesChildList =  null;

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			int point=1;
			for(ObjectiveMaster objectiveMaster : objectiveMasters)
			{
				mainTable = new PdfPTable(1);
				mainTable.setWidthPercentage(90);

				pdStrengthsAndOpportunitiesList = pdStrengthsAndOpportunitiesDAO.getStatementByObjective(objectiveMaster,"s");

				para[2] = new Paragraph(maxCompetencyPoint+point+": "+objectiveMaster.getObjectiveName()+"",font12bold);
				//para[2] = new Paragraph("P"+point+": "+objectiveMaster.getObjectiveName()+"",font12bold);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setLeading(0f,1.5f);
				cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
				cell[2].setBorder(0);

				mainTable.addCell(cell[2]);
				//mainTable.addCell(cell[0]);

				if(objectiveMaster.getObjectiveStrength()!=null && !objectiveMaster.getObjectiveStrength().trim().equals(""))
				{

					para[2] = new Paragraph(objectiveMaster.getObjectiveStrength(),font12);
					cell[2]= new PdfPCell(para[2]);
					cell[2].setLeading(0f,1.5f);
					cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
					cell[2].setBorder(0);

					mainTable.addCell(cell[2]);
					//mainTable.addCell(cell[0]);
				}
				document.add(mainTable);

				if(pdStrengthsAndOpportunitiesList.size()>0)
				{
					pdStrengthsAndOpportunitiesChildList = pdStrengthsAndOpportunitiesDAO.getStatementByObjectiveChild(objectiveMaster,"s");


					//list = new List(false, pdStrengthsAndOpportunitiesList.size());
					list = new List(false, 15);
					list.setListSymbol(new Chunk("\u2022", font12));
					list.setIndentationLeft(32);
					list.setIndentationRight(32);
					//list.add(new ListItem("Third line"));

					// document.add(list);

					for(PdStrengthsAndOpportunities  strengthsAndOpportunities: pdStrengthsAndOpportunitiesList)
					{

						list.add(new ListItem(new ListItem(strengthsAndOpportunities.getStatement(),font12)));

						if(pdStrengthsAndOpportunitiesChildList.size()>0)
						{
							int p=0;
							sublist =  new List(false, 12);
							sublist.setListSymbol(new Chunk("\u2022", font9));
							for(PdStrengthsAndOpportunities child : pdStrengthsAndOpportunitiesChildList)
							{
								if(child.getStrengthsAndOpportunities().getStatementId().equals(strengthsAndOpportunities.getStatementId()))
								{
									sublist.add(new ListItem(child.getStatement(),font12));
									p++;
								}
							}
							list.add(sublist);
							p=0;
						}

					}

					document.add(list);
					document.add(new Paragraph(new Chunk(" ")));
				}else
					mainTable.addCell(blankcell);

				point++;
			}


			document.newPage();
			//////////////////// End 8th page ///////////////////////////////

			////////////////////Start 9th page Opportunities/////////////////////////////

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph(opportun,font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			//mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);
			para[1] = new Paragraph(competencyMasterMin.getCompetencyName(),font16);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);
			//mainTable.addCell(cell[0]);


			dis = strength1+" "+competencyMasterMin.getCompetencyName()+". " +strength2 ;
			

			para[2] = new Paragraph(dis,font12);

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0f,1.5f);
			cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[2].setBorder(0);

			mainTable.addCell(cell[2]);
			mainTable.addCell(blankcell);

			document.add(mainTable);


			objectiveMasters = objectiveMasterDAO.getObjectivesByCompetencyNoOrdering(competencyMasterMin);


			pdStrengthsAndOpportunitiesList =  null;
			pdStrengthsAndOpportunitiesChildList =  null;

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);
			String minCompetencyPoint = competencyMasterMin.getCompetencyShortName();
			point=1;
			for(ObjectiveMaster objectiveMaster : objectiveMasters)
			{
				mainTable = new PdfPTable(1);
				mainTable.setWidthPercentage(90);

				pdStrengthsAndOpportunitiesList = pdStrengthsAndOpportunitiesDAO.getStatementByObjective(objectiveMaster,"o");


				//para[2] = new Paragraph("AA"+point+": "+objectiveMaster.getObjectiveName()+"",font12bold);
				para[2] = new Paragraph(minCompetencyPoint+point+": "+objectiveMaster.getObjectiveName()+"",font12bold);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setLeading(0f,1.5f);
				cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
				cell[2].setBorder(0);

				mainTable.addCell(cell[2]);
				//mainTable.addCell(cell[0]);

				if(objectiveMaster.getObjectiveStrength()!=null && !objectiveMaster.getObjectiveOpportunity().trim().equals(""))
				{

					para[2] = new Paragraph(objectiveMaster.getObjectiveOpportunity(),font12);
					cell[2]= new PdfPCell(para[2]);
					cell[2].setLeading(0f,1.5f);
					cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
					cell[2].setBorder(0);

					mainTable.addCell(cell[2]);
					//mainTable.addCell(cell[0]);
				}
				document.add(mainTable);

				if(pdStrengthsAndOpportunitiesList.size()>0)
				{
					pdStrengthsAndOpportunitiesChildList = pdStrengthsAndOpportunitiesDAO.getStatementByObjectiveChild(objectiveMaster,"o");

					list = new List(false, 15);
					//list.setAlignindent(true);
					list.setListSymbol(new Chunk("\u2022", font12));
					list.setIndentationLeft(32);
					list.setIndentationRight(32);

					for(PdStrengthsAndOpportunities  strengthsAndOpportunities: pdStrengthsAndOpportunitiesList)
					{

						list.add(new ListItem(strengthsAndOpportunities.getStatement(),font12));

						if(pdStrengthsAndOpportunitiesChildList.size()>0)
						{
							int p=0;
							sublist =  new List(false, 12);
							//sublist.setAlignindent(true);
							sublist.setListSymbol(new Chunk("\u2022", font9));
							for(PdStrengthsAndOpportunities child : pdStrengthsAndOpportunitiesChildList)
							{
								if(child.getStrengthsAndOpportunities().getStatementId().equals(strengthsAndOpportunities.getStatementId()))
								{
									sublist.add(new ListItem(child.getStatement(),font12));
									p++;
								}
							}
							list.add(sublist);
							p=0;
						}


					}


					document.add(list);
					document.add(new Paragraph(new Chunk(" ")));
				}else
					mainTable.addCell(blankcell);

				point++;
			}

			document.newPage();
			//////////////////// End 9th page ///////////////////////////////

			////////////////////Start 10th page /////////////////////////////
			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph(pdp,font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			//mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);

			para[1] = new Paragraph(pdp1,font12);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);
			mainTable.addCell(blankcell);


			para[1] = new Paragraph(" ",font60);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			//cell[1].setBorder(2);

			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);

			document.add(mainTable);

			float[] widthParticular = {.4f,.2f,.2f,.3f};
			PdfPTable tbl = new PdfPTable(widthParticular);
			tbl.setWidthPercentage(90);
			String actions[] = {pdp2 ,pdp3 ,pdp4 ,pdp5 };
			for(int i=0;i<4;i++)
			{
				para[1] = new Paragraph(actions[i],font12);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				tbl.addCell(cell[1]);
			}

			for(int i=0;i<4;i++)
			{
				para[1] = new Paragraph("",font12);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				tbl.addCell(cell[1]);
			}

			for(int i=0;i<32;i++)
			{
				para[1] = new Paragraph(" ",font60);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				//cell[1].setBorder(0);
				tbl.addCell(cell[1]);
			}

			document.add(tbl);

			/*htmlWorker = new HTMLWorker(document);
			str="<br><br><br><table align='center'><tr><td><IMG SRC='"+urlRootPath+"images/p1.PNG' width='500'   BORDER='0' style=\"font-family:'Tahoma'\">"+
			"</td></tr></table><br><br><br><br><br><br>";
			htmlWorker.parse(new StringReader(str));*/

			document.newPage();

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para[1] = new Paragraph(pdp9,font12);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].setBorder(0);

			mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);

			para[1] = new Paragraph(" ",font60);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);

			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);

			document.add(mainTable);

			float[] widthParticular2 = {.3f,.3f,.3f};
			PdfPTable tbl2 = new PdfPTable(widthParticular2);
			tbl2.setWidthPercentage(90);
			String actions2[] = {pdp6,pdp7,pdp8};
			for(int i=0;i<3;i++)
			{
				para[1] = new Paragraph(actions2[i],font12);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				tbl2.addCell(cell[1]);
			}

			for(int i=0;i<3;i++)
			{
				para[1] = new Paragraph("",font12);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				tbl2.addCell(cell[1]);
			}
			for(int i=0;i<27;i++)
			{
				para[1] = new Paragraph(" ",font60);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				//cell[1].setBorder(0);
				tbl2.addCell(cell[1]);
			}

			document.add(tbl2);

			document.newPage();

			//////////////////// End 10th page ///////////////////////////////

		}catch (Exception e) {
			e.printStackTrace();
		}finally
		{

			if(document != null && document.isOpen())
				document.close();
			if(writer!=null){
				writer.flush();
				writer.close();
			}

		}
		return false;
	}

	public boolean generatePilarReport(HttpServletRequest request,JobOrder jobOrder,TeacherDetail teacherDetail,TeacherAssessmentdetail teacherAssessmentdetail,String reportPath,String fileName,String urlRootPath,TeacherAssessmentStatus teacherAssessmentStatus)
	{

		Font font20 = null;
		Font font16 = null;
		Font font12 = null;
		Font font12bold = null;
		Font font12List = null;
		Font font6 = null;
		Font font7 = null;
		String fontPath = request.getRealPath("/");
		try {

			
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			font20 = new Font(tahoma, 20,Font.BOLD);
			font12 = new Font(tahoma, 12);
			font12List = new Font(tahoma, 14);
			font12bold = new Font(tahoma, 12,Font.BOLD);
			font16 = new Font(tahoma, 16,Font.BOLD);
			font6 = new Font(tahoma, 6);
			font7 = new Font(tahoma, 7);

		} catch (DocumentException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;

		try{

			File file = new File(reportPath+"/temp");
			if(!file.exists())
				file.mkdirs();
			else
				Utility.deleteAllFileFromDir(reportPath+"/temp");

			document=new Document(PageSize.A4,10f,10f,10f,10f);
			//document = new Document(PageSize.A4, 20, 10, 10, 10);
			// document.setMarginMirroringTopBottom(true);
			//document.setMargins(15, 15, 15, 15);

			document.addAuthor("TeacherMatch");
			document.addCreator("TeacherMatch Inc.");
			document.addSubject(Utility.getLocaleValuePropByKey("msgJobSpecInvtRpt", locale));
			document.addCreationDate();
			document.addTitle(Utility.getLocaleValuePropByKey("msgJobSpecInvtRpt", locale));

			fos = new FileOutputStream(reportPath+"/"+fileName);
			writer = PdfWriter.getInstance(document, fos);
			//PdfWriter.getInstance(document, fos);
			//writer.setSpaceCharRatio(PdfWriter.NO_SPACE_CHAR_RATIO);

			/*	footerpara = new Paragraph("Generated by TeacherMatch: Created on - "+new Date()+"",font6);
			headerFooter = new HeaderFooter(footerpara,false);
			headerFooter.setAlignment(HeaderFooter.ALIGN_RIGHT);
			headerFooter.setBorder(0);
			document.setFooter(headerFooter);*/

			footerpara = new Paragraph("",font12);
			headerFooter = new HeaderFooter(footerpara,false);
			headerFooter.setAlignment(HeaderFooter.ALIGN_RIGHT);
			headerFooter.setBorder(0);
			document.setHeader(headerFooter);
			document.open();

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);
			/////////////////////////// Start 1st page /////////////////////////////////////////
			Paragraph [] para = null;
			PdfPCell [] cell = null;

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			
			//Image logo = Image.getInstance (urlRootPath+"/images/Logo%20with%20Beta300.png");
			Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
			//System.out.println("Height::::::::: "+logo.getHeight());
			//System.out.println("Width::::::::: "+logo.getWidth());
			logo.scalePercent(75);
			//logo=null;
			//System.out.println("Height::::::::: "+logo.getHeight());
			//logo.scaleAbsolute(300,55);
			float[] widthParticularlogo = {.14f,.2f};
			PdfPTable logotable = new PdfPTable(widthParticularlogo);
			logotable.setWidthPercentage(90);
			PdfPCell logocol1 = new PdfPCell(new Paragraph("     ",font7));
			logocol1.setBorder(0);
			PdfPCell logocol2 = new PdfPCell(logo);
			logocol2.setBorder(0);
			
			logotable.addCell(logocol1);
			logotable.addCell(logocol2);
			////////////////////////////////////////////////////////////////
			cell[1]= new PdfPCell(logotable);
			//cell[1]= new PdfPCell(logo);
			cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[1].setBorder(0);
			
			para[2] = new Paragraph("JOB SPECIFIC INVENTORY REPORT",font20);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);

			for(int i=0;i<2;i++)
				mainTable.addCell(cell[0]);

			mainTable.addCell(cell[1]);
			for(int i=0;i<3;i++)
				mainTable.addCell(cell[0]);
			mainTable.addCell(cell[2]);
			mainTable.addCell(cell[0]);

			para[2] = new Paragraph(Utility.getLocaleValuePropByKey("msgEducatorJbSpecificInventory", locale),font20);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);

			mainTable.addCell(cell[2]);

			for(int i=0;i<3;i++)
				mainTable.addCell(cell[0]);


			para[2] = new Paragraph(Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font20);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);

			for(int i=0;i<3;i++)
				mainTable.addCell(cell[0]);

			mainTable.addCell(cell[2]);

			for(int i=0;i<10;i++)
				mainTable.addCell(cell[0]);

			para[2] = new Paragraph("Teacher: "+teacherDetail.getFirstName()+" "+(teacherDetail.getLastName()==null?"":teacherDetail.getLastName()),font12bold);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[2].setBorder(0);
			mainTable.addCell(cell[2]);

			//mainTable.addCell(cell[0]);

			para[2] = new Paragraph("Job: "+jobOrder.getJobTitle(),font12bold);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[2].setBorder(0);
			mainTable.addCell(cell[2]);

			document.add(mainTable);
			document.newPage();

			//////////////////////////////////////////


			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph(jobOrder.getJobTitle(),font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[1].setBorder(0);

			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);
			document.add(mainTable);


			TeacherAssessmentQuestion teacherAssessmentQuestion = null;
			java.util.List<TeacherAssessmentOption> teacherQuestionOptionsList = null;

			if(teacherAssessmentdetail!=null)
			{

				para[0] = new Paragraph(" ",font6);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell[0].setBorder(0);


				java.util.List<TeacherAnswerDetail> teacherAnswerDetails =null;

				Criterion criterion = Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail);
				Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
				Criterion criterion2 = Restrictions.eq("jobOrder", teacherAssessmentStatus.getJobOrder());

				teacherAnswerDetails = teacherAnswerDetailDAO.findByCriteria(criterion,criterion1,criterion2);

				String selectedOptions = "",insertedRanks="";
				String questionTypeShortName = "";
				long optionId = 0;
				String answerText = "";
				int questionCounter = 0;

				float[] innerwidth = {.1f,.9f};
				PdfPTable tblRankResult = null;

				float[] widthAnswers = {.1f,.9f};
				PdfPTable tblAnswers = new PdfPTable(widthAnswers);
				tblAnswers.setWidthPercentage(90);
				HTML2Text html2Text = new HTML2Text();
				for(TeacherAnswerDetail teacherAnswerDetail : teacherAnswerDetails) 
				{
					++questionCounter;
					teacherAssessmentQuestion = teacherAnswerDetail.getTeacherAssessmentQuestion();
					selectedOptions = teacherAnswerDetail.getSelectedOptions();

					para[1] = new Paragraph("Q. "+questionCounter+"",font12);
					cell[1]= new PdfPCell(para[1]);
					cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
					cell[1].setBorder(0);

					tblAnswers.addCell(cell[1]);

					para[1] = new Paragraph(html2Text.convert(teacherAssessmentQuestion.getQuestion()),font12);
					cell[1]= new PdfPCell(para[1]);
					cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
					cell[1].setBorder(0);

					tblAnswers.addCell(cell[1]);

					for(int j=0;j<2;j++)
						tblAnswers.addCell(cell[0]);

					questionTypeShortName = teacherAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName();
					teacherQuestionOptionsList = teacherAssessmentQuestion.getTeacherAssessmentOptions();
					if(questionTypeShortName.equalsIgnoreCase("tf") || questionTypeShortName.equalsIgnoreCase("slsel") || questionTypeShortName.equalsIgnoreCase("lkts"))
					{
						if(selectedOptions!=null && !selectedOptions.equals(""))
						{ 
							try{optionId = Long.parseLong(selectedOptions);}catch(Exception e){optionId = 0;}
						}
						else
							optionId = 0;

						for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {

							para[1] = new Paragraph("",font12);
							cell[1]= new PdfPCell(para[1]);
							cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							cell[1].setBorder(0);

							tblAnswers.addCell(cell[1]);

							para[1] = new Paragraph(teacherQuestionOption.getQuestionOption(),font12);
							cell[1]= new PdfPCell(para[1]);
							cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							cell[1].setBorder(0);

							tblAnswers.addCell(cell[1]);

							if(optionId==teacherQuestionOption.getTeacherAssessmentOptionId())
							{
								answerText=teacherQuestionOption.getQuestionOption();
							}
						}

						for(int j=0;j<2;j++)
							tblAnswers.addCell(cell[0]);



						para[1] = new Paragraph( Utility.getLocaleValuePropByKey("lblAnswer", locale)+":",font12bold);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cell[1].setBorder(0);

						tblAnswers.addCell(cell[1]);

						para[1] = new Paragraph(answerText,font12bold);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cell[1].setBorder(0);

						tblAnswers.addCell(cell[1]);
						answerText="";
					}else if(questionTypeShortName.equalsIgnoreCase("rt"))
					{

						for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {


							para[1] = new Paragraph(" ",font12);
							cell[1]= new PdfPCell(para[1]);
							cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							cell[1].setBorder(0);

							tblAnswers.addCell(cell[1]);

							para[1] = new Paragraph(teacherQuestionOption.getQuestionOption(),font12);
							cell[1]= new PdfPCell(para[1]);
							cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							cell[1].setBorder(0);

							tblAnswers.addCell(cell[1]);
						}
						answerText=insertedRanks;
						insertedRanks="";
						for(int j=0;j<2;j++)
							tblAnswers.addCell(cell[0]);



						para[1] = new Paragraph(Utility.getLocaleValuePropByKey("lblAnswer", locale)+":",font12bold);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cell[1].setBorder(0);

						tblAnswers.addCell(cell[1]);

						//////////////////////////////////////////////////////////

						insertedRanks = teacherAnswerDetail.getInsertedRanks()==null?"":teacherAnswerDetail.getInsertedRanks();
						selectedOptions = selectedOptions==null?"":selectedOptions;
						String[] ranks = insertedRanks.split("\\|");

						tblRankResult = new PdfPTable(innerwidth);
						tblRankResult.setWidthPercentage(70);
						String ans = "";
						int rank = 0;
						for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {


							try{ans = ranks[rank];}catch(Exception e){ans = "";}

							para[1] = new Paragraph(ans,font12bold);
							cell[1]= new PdfPCell(para[1]);
							cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							cell[1].setBorder(0);

							tblRankResult.addCell(cell[1]);

							para[1] = new Paragraph(teacherQuestionOption.getQuestionOption(),font12bold);
							cell[1]= new PdfPCell(para[1]);
							cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
							cell[1].setBorder(0);

							tblRankResult.addCell(cell[1]);
							rank++;
						}

						cell[1]= new PdfPCell(tblRankResult);
						cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cell[1].setBorder(0);

						tblAnswers.addCell(cell[1]);
						/////////////////////////////////////////////////////////

						answerText="";
					}else if(questionTypeShortName.equalsIgnoreCase("sl"))
					{

						para[1] = new Paragraph(Utility.getLocaleValuePropByKey("lblAnswer", locale)+":",font12bold);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cell[1].setBorder(0);

						tblAnswers.addCell(cell[1]);

						para[1] = new Paragraph(teacherAnswerDetail.getInsertedText(),font12bold);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cell[1].setBorder(0);

						tblAnswers.addCell(cell[1]);
					}
					else if(questionTypeShortName.equalsIgnoreCase("ml"))
					{

						para[1] = new Paragraph(Utility.getLocaleValuePropByKey("lblAnswer", locale)+":",font12bold);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cell[1].setBorder(0);

						tblAnswers.addCell(cell[1]);

						para[1] = new Paragraph(html2Text.convert(teacherAnswerDetail.getInsertedText()),font12bold);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
						cell[1].setBorder(0);

						tblAnswers.addCell(cell[1]);
					}

					for(int j=0;j<2;j++)
						tblAnswers.addCell(cell[0]);
				}

				document.add(tblAnswers);
			}


			//document.add(mainTable);

			/////////////////////End 1st page ///////////////////////////////////
		}catch (Exception e) {
			e.printStackTrace();
		}finally
		{

			if(document != null && document.isOpen())
				document.close();
			if(writer!=null){
				writer.flush();
				writer.close();
			}

		}
		return false;
	}

	public String generatePortfolioReport(TeacherDetail teacherDetail, HttpServletRequest request, String file)
	{

		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		String fontPath = request.getRealPath("/");
		try
		{

			Font font9 = null;
			Font font9bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font7 = null;
			try {
				//tahoma = BaseFont.createFont("c:\\font\\tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

				
				BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

				font9 = new Font(tahoma, 9);
				font9bold = new Font(tahoma, 9, Font.BOLD);
				font11 = new Font(tahoma, 11);
				font11bold = new Font(tahoma, 11,Font.BOLD);
				font7 = new Font(tahoma, 7);

			} 
			catch (DocumentException e1) 
			{
				e1.printStackTrace();
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
			}

			document = new Document(PageSize.A4,10f,10f,10f,10f);		
			document.addAuthor("TeacherMatch");
			document.addCreator("TeacherMatch Inc.");
			document.addSubject(Utility.getLocaleValuePropByKey("msgEducatorProfInvtry", locale));
			document.addCreationDate();
			document.addTitle(Utility.getLocaleValuePropByKey("msgCandidateGridReport", locale));

			fos = new FileOutputStream(file);
			PdfWriter.getInstance(document, fos);

			/*footerpara = new Paragraph("Generated by TeacherMatch: Created on - "+new Date(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 6,Font.BOLDITALIC,Color.black));
			headerFooter = new HeaderFooter(footerpara,false);
			headerFooter.setAlignment(HeaderFooter.ALIGN_RIGHT);
			headerFooter.setBorder(0);
			document.setFooter(headerFooter);*/
			document.open();

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			Paragraph [] para = null;
			PdfPCell [] cell = null;


			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",FontFactory.getFont(FontFactory.HELVETICA, 20,Font.BOLD,Color.black));
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			/*//Image logo = Image.getInstance(Utility.getBaseURL(request)+"images/logo.png");//images/logo.png
			Image logo = Image.getInstance (Utility.getBaseURL(request)+"images/TMatch-Logo.png");//images/logo.png
			logo.scalePercent(75);

			cell[1] = new PdfPCell(logo);
			cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[1].setBorder(0);*/
			
			PdfPCell blankcell = new PdfPCell(new Paragraph("     ",font7));
			blankcell.setBorder(0);
			//Image logo = Image.getInstance (urlRootPath+"/images/TMatch-Logo.png");//images/logo.png
			//Image logo = Image.getInstance (Utility.getValueOfPropByKey("contextBasePath")+"/images/Logo%20with%20Beta300.png");
			Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
			
			//System.out.println("Height::::::::: "+logo.getHeight());
			//System.out.println("Width::::::::: "+logo.getWidth());
			logo.scalePercent(75);
			//logo=null;
			//System.out.println("Height::::::::: "+logo.getHeight());
			//logo.scaleAbsolute(300,55);
			float[] widthParticularlogo = {.14f,.2f};
			PdfPTable logotable = new PdfPTable(widthParticularlogo);
			logotable.setWidthPercentage(90);
			PdfPCell logocol1 = new PdfPCell(new Paragraph("     ",font7));
			logocol1.setBorder(0);
			PdfPCell logocol2 = new PdfPCell(logo);
			logocol2.setBorder(0);
			
			logotable.addCell(logocol1);
			logotable.addCell(logocol2);
			
			cell[1]= new PdfPCell(logotable);
			//cell[1]= new PdfPCell(logo);
			cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[1].setBorder(0);
			
			//mainTable.addCell(logotable);
			mainTable.addCell(cell[1]);
			for(int i=0;i<1;i++)
				mainTable.addCell(cell[0]);

			mainTable.addCell(cell[0]);

			document.add(mainTable);

			float[] widthPersonalInfo = {.3f,.6f};
			PdfPTable tblPersonalInfo = new PdfPTable(widthPersonalInfo);

			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false); 
			Integer salutation = teacherPersonalInfo.getSalutation()==null?0:teacherPersonalInfo.getSalutation();     
			String salutationName = "";
			if(salutation==1)
				salutationName = "Mrs";
			else if(salutation==2)
				salutationName = "Mr";
			else if(salutation==3)
				salutationName = "Miss";
			else if(salutation==4)
				salutationName = "Dr";
			else 
				salutationName = "";

			String firstName = teacherPersonalInfo.getFirstName()==null?"":teacherPersonalInfo.getFirstName();                              
			String lastName = teacherPersonalInfo.getLastName()==null?"":teacherPersonalInfo.getLastName();                           
			String addressLine1 = teacherPersonalInfo.getAddressLine1()==null?"":teacherPersonalInfo.getAddressLine1();       
			String addressLine2 = teacherPersonalInfo.getAddressLine2()==null?"":teacherPersonalInfo.getAddressLine2();     
			String zipCode = teacherPersonalInfo.getZipCode()==null?"":teacherPersonalInfo.getZipCode();     
			String state = "";
			if(teacherPersonalInfo.getStateId()!=null)
			state = teacherPersonalInfo.getStateId().getStateName()==null?"":teacherPersonalInfo.getStateId().getStateName();
			String city = "";
			if(teacherPersonalInfo.getCityId()!=null)
				city = teacherPersonalInfo.getCityId().getCityName()==null?"":teacherPersonalInfo.getCityId().getCityName();     
			String phoneNumber = teacherPersonalInfo.getPhoneNumber()==null?"":teacherPersonalInfo.getPhoneNumber();     
			String mobileNumber = teacherPersonalInfo.getMobileNumber()==null?"":teacherPersonalInfo.getMobileNumber();     

			para[0] = new Paragraph("Candidate Portfolio\n\n",font11bold);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].setBorder(0);			
			tblPersonalInfo.addCell(cell[0]);

			para[1] = new Paragraph("",font9bold);
			cell[1] = new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);			
			cell[1].setBorder(0);
			tblPersonalInfo.addCell(cell[1]);

			para[0] = new Paragraph("Personal",font9bold);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].setBorder(0);			
			tblPersonalInfo.addCell(cell[0]);

			para[1] = new Paragraph("",font9bold);
			cell[1] = new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);			
			cell[1].setBorder(0);
			tblPersonalInfo.addCell(cell[1]);

			String nameText = "";
			if(!salutationName.equals(""))
				nameText=salutationName+" ";

			nameText = nameText+firstName.trim()+" "+lastName;

			para[0] = new Paragraph(""+nameText,font9);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].setBorder(0);
			cell[0].setColspan(2);
			tblPersonalInfo.addCell(cell[0]);



			String addressText=""+addressLine1;
			if(!addressLine2.equals(""))
				addressText+=", "+addressLine2;
			if(!city.equals(""))
				addressText = addressText+", "+city+", "+state+", "+zipCode;
			else
				addressText = addressText+", "+state+", "+zipCode;
			para[0] = new Paragraph(""+addressText,font9);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].setBorder(0);		
			cell[0].setColspan(2);
			tblPersonalInfo.addCell(cell[0]);




			para[0] = new Paragraph("Phone: "+phoneNumber,font9);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].setBorder(0);			
			tblPersonalInfo.addCell(cell[0]);

			para[1] = new Paragraph("Mobile: "+mobileNumber,font9);
			cell[1] = new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);			
			cell[1].setBorder(0);
			tblPersonalInfo.addCell(cell[1]);

			para[0] = new Paragraph("\nAcademics",font9bold);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].setPaddingBottom(5);
			cell[0].setBorder(0);			
			tblPersonalInfo.addCell(cell[0]);

			para[1] = new Paragraph("",font9bold);
			cell[1] = new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);			
			cell[1].setBorder(0);
			tblPersonalInfo.addCell(cell[1]);

			document.add(tblPersonalInfo);			
			java.util.List<TeacherAcademics> lstTeacherAcademics = teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
			float[] widthAcademics = {.15f,.15f,.15f,.15f,.15f,.15f};
			PdfPTable tblAcademics = new PdfPTable(widthAcademics);

			para = new Paragraph[6];
			cell = new PdfPCell[6];

			para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblDegree", locale),font9bold);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].setPaddingBottom(5);
			cell[0].enableBorderSide(1);

			tblAcademics.addCell(cell[0]);

			para[1] = new Paragraph(Utility.getLocaleValuePropByKey("lblFieldOfStudy", locale),font9bold);
			cell[1] = new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].enableBorderSide(1);
			tblAcademics.addCell(cell[1]);


			para[2] = new Paragraph(Utility.getLocaleValuePropByKey("lblDatAtt", locale),font9bold);
			cell[2] = new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);			
			cell[2].enableBorderSide(1);
			tblAcademics.addCell(cell[2]);

			para[3] = new Paragraph(Utility.getLocaleValuePropByKey("lblSchool", locale),font9bold);
			cell[3] = new PdfPCell(para[3]);
			cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[3].enableBorderSide(1);
			tblAcademics.addCell(cell[3]);


			para[4] = new Paragraph(Utility.getLocaleValuePropByKey("headTranscript", locale),font9bold);
			cell[4] = new PdfPCell(para[4]);
			cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[4].enableBorderSide(1);
			tblAcademics.addCell(cell[4]);

			para[5] = new Paragraph(Utility.getLocaleValuePropByKey("lblGPA", locale),font9bold);
			cell[5] = new PdfPCell(para[5]);
			cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[5].enableBorderSide(1);
			tblAcademics.addCell(cell[5]);

			String transcript = "";

			for(TeacherAcademics teacherAcademics: lstTeacherAcademics)
			{
				transcript = teacherAcademics.getPathOfTranscript() ==null?"":teacherAcademics.getPathOfTranscript();
				para[0] = new Paragraph(teacherAcademics.getDegreeId().getDegreeName(),font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].enableBorderSide(1);
				cell[0].setPaddingBottom(5);
				tblAcademics.addCell(cell[0]);

				String fName="";
				if(teacherAcademics.getFieldId()!=null)
					fName=teacherAcademics.getFieldId().getFieldName();
				para[1] = new Paragraph(fName,font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].setPaddingBottom(5);
				cell[1].enableBorderSide(1);
				tblAcademics.addCell(cell[1]);

				String aIY = "";
				String lIY = "";
				
				if(teacherAcademics.getAttendedInYear()!=null)
					aIY=teacherAcademics.getAttendedInYear().toString();
				
				if(teacherAcademics.getLeftInYear()!=null)
					lIY=teacherAcademics.getLeftInYear().toString();
				para[2] = new Paragraph(aIY+" - "+lIY,font9);
				cell[2] = new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[2].setPaddingBottom(5);
				cell[2].enableBorderSide(1);
				tblAcademics.addCell(cell[2]);

				String uName="";
				if(teacherAcademics.getUniversityId()!=null)
					uName=teacherAcademics.getUniversityId().getUniversityName();
				para[3] = new Paragraph(uName,font9);
				cell[3] = new PdfPCell(para[3]);
				cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[3].enableBorderSide(1);
				cell[3].setPaddingBottom(5);
				tblAcademics.addCell(cell[3]);


				para[4] = new Paragraph(transcript,font9);
				cell[4] = new PdfPCell(para[4]);
				cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[4].setPaddingBottom(5);
				cell[4].enableBorderSide(1);				
				tblAcademics.addCell(cell[4]);
				String gpaText = "";
				if(teacherAcademics.getDegreeId().getDegreeType().trim().equalsIgnoreCase("b"))
				{
					gpaText=Utility.getLocaleValuePropByKey("lblFreshman", locale)+":"+(teacherAcademics.getGpaFreshmanYear()==null?"":teacherAcademics.getGpaFreshmanYear())+"\n" +
					Utility.getLocaleValuePropByKey("lblSophomore", locale)+":"+(teacherAcademics.getGpaSophomoreYear()==null?"":teacherAcademics.getGpaSophomoreYear())+"\n" +
					Utility.getLocaleValuePropByKey("lblJunior", locale)+":"+(teacherAcademics.getGpaJuniorYear()==null?"":teacherAcademics.getGpaJuniorYear())+"\n" +
					Utility.getLocaleValuePropByKey("lblSenior", locale)+":"+(teacherAcademics.getGpaSeniorYear()==null?"":teacherAcademics.getGpaSeniorYear())+"\n" +
					Utility.getLocaleValuePropByKey("lblCumulative", locale)+":"+(teacherAcademics.getGpaCumulative()==null?"":teacherAcademics.getGpaCumulative());
				}
				else
				{
					gpaText= Utility.getLocaleValuePropByKey("lblCumulative", locale)+":"+(teacherAcademics.getGpaCumulative()==null?"":teacherAcademics.getGpaCumulative());
				}
				para[5] = new Paragraph(gpaText,font9);
				cell[5] = new PdfPCell(para[5]);
				cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[5].enableBorderSide(1);
				cell[5].setPaddingBottom(5);
				tblAcademics.addCell(cell[5]);
			}
			if(lstTeacherAcademics==null || lstTeacherAcademics.size()==0)
			{
				para[0] = new Paragraph(" ",font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].enableBorderSide(1);
				cell[0].setColspan(6);
				tblAcademics.addCell(cell[0]);
			}					

			document.add(tblAcademics);


			java.util.List<TeacherCertificate> lstTeacherCertificate = teacherCertificateDAO.findCertificateByTeacher(teacherDetail);
			TeacherExperience teacherExperiences = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			{
				float[] width = {.35f,.55f};
				PdfPTable tbl= new PdfPTable(width);

				para[0] = new Paragraph("\n"+Utility.getLocaleValuePropByKey("lblCertifications", locale) ,font9bold);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].setBorder(0);			
				cell[0].setColspan(2);
				tbl.addCell(cell[0]);

				para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblNatiBoardCerti/Lice", locale),font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].setBorder(0);			
				tbl.addCell(cell[0]);

				String nbcText = Utility.getLocaleValuePropByKey("optN", locale);
				if(teacherExperiences.getNationalBoardCert())
				{
					nbcText = Utility.getLocaleValuePropByKey("optY", locale);
				}
				para[1] = new Paragraph(nbcText,font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].setBorder(0);
				tbl.addCell(cell[1]);

				para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblCertiTeachExp", locale),font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].setBorder(0);			
				tbl.addCell(cell[0]);

				String year = teacherExperiences.getExpCertTeacherTraining()==null?"":""+teacherExperiences.getExpCertTeacherTraining();


				para[1] = new Paragraph(year,font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].setPaddingBottom(5);
				cell[1].setBorder(0);
				tbl.addCell(cell[1]);

				document.add(tbl);
			}



			float[] widthCertification = {.4f,.2f,.2f,.3f};
			PdfPTable tblCertification = new PdfPTable(widthCertification);

			para = new Paragraph[3];
			cell = new PdfPCell[3];

			para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblSt", locale),font9bold);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].enableBorderSide(1);
			cell[0].setPaddingBottom(5);
			tblCertification.addCell(cell[0]);

			para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblStatus", locale),font9bold);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].enableBorderSide(1);
			cell[0].setPaddingBottom(5);
			tblCertification.addCell(cell[0]);
			
			para[1] = new Paragraph(Utility.getLocaleValuePropByKey("lblYearReceived", locale),font9bold);
			cell[1] = new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].enableBorderSide(1);
			tblCertification.addCell(cell[1]);


			para[2] = new Paragraph( Utility.getLocaleValuePropByKey("lblType", locale) ,font9bold);
			cell[2] = new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);			
			cell[2].enableBorderSide(1);
			tblCertification.addCell(cell[2]);




			String stateName="";
			String yearReceived="";
			String type="";
			String status="";
			for(TeacherCertificate tc: lstTeacherCertificate)
			{
				stateName=tc.getStateMaster()==null?"":tc.getStateMaster().getStateName();
				status=tc.getCertificationStatusMaster().getCertificationStatusName()==null?"":tc.getCertificationStatusMaster().getCertificationStatusName();
				yearReceived=tc.getYearReceived()==null?"":""+tc.getYearReceived();				
				type=tc.getCertType()==null?"":tc.getCertType();

				para[0] = new Paragraph(""+stateName,font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].enableBorderSide(1);
				cell[0].setPaddingBottom(5);
				tblCertification.addCell(cell[0]);

				para[0] = new Paragraph(""+status,font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].enableBorderSide(1);
				cell[0].setPaddingBottom(5);
				tblCertification.addCell(cell[0]);
				
				para[1] = new Paragraph(""+yearReceived,font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].setPaddingBottom(5);
				cell[1].enableBorderSide(1);
				tblCertification.addCell(cell[1]);

				para[2] = new Paragraph(""+type,font9);
				cell[2] = new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[2].setPaddingBottom(5);
				cell[2].enableBorderSide(1);
				tblCertification.addCell(cell[2]);				

			}
			if(lstTeacherCertificate==null || lstTeacherCertificate.size()==0)
			{
				para[0] = new Paragraph(" ",font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].enableBorderSide(1);
				cell[0].setColspan(3);
				tblCertification.addCell(cell[0]);

				para[1] = new Paragraph("",font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].setBorder(0);
				cell[1].setColspan(3);
				tblCertification.addCell(cell[1]);
			}



			document.add(tblCertification);




			{
				float[] width = {.2f,.7f};
				PdfPTable tbl= new PdfPTable(width);

				para[0] = new Paragraph("\n"+Utility.getLocaleValuePropByKey("headExp", locale),font9bold);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].setBorder(0);			
				cell[0].setColspan(2);
				tbl.addCell(cell[0]);

				para[0] = new Paragraph( Utility.getLocaleValuePropByKey("msgResumesubmitted", locale) ,font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].setPaddingBottom(5);
				cell[0].setBorder(0);			
				tbl.addCell(cell[0]);

				String resumeText = Utility.getLocaleValuePropByKey("optN", locale);
				if(teacherExperiences.getResume()!=null && (!teacherExperiences.getResume().equals("")))
				{
					resumeText = Utility.getLocaleValuePropByKey("optY", locale);
				}
				para[1] = new Paragraph(resumeText,font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].setBorder(0);
				tbl.addCell(cell[1]);

				document.add(tbl);
			}



			java.util.List<TeacherRole> listTeacherRole= null;
			listTeacherRole = teacherRoleDAO.findTeacherRoleByTeacher(teacherDetail);

			float[] widthExp = {.2f,.4f,.15f,.15f};
			PdfPTable tblExp = new PdfPTable(widthExp);

			para = new Paragraph[5];
			cell = new PdfPCell[5];

			para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblRole", locale),font9bold);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].setPaddingBottom(5);
			cell[0].enableBorderSide(1);
			tblExp.addCell(cell[0]);

			para[1] = new Paragraph( Utility.getLocaleValuePropByKey("lblType", locale),font9bold);
			cell[1] = new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].enableBorderSide(1);
			tblExp.addCell(cell[1]);


			para[2] = new Paragraph(Utility.getLocaleValuePropByKey("lblDuration", locale) ,font9bold);
			cell[2] = new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);			
			cell[2].enableBorderSide(1);
			tblExp.addCell(cell[2]);

			para[3] = new Paragraph(Utility.getLocaleValuePropByKey("lblTypeOfRole", locale) ,font9bold);
			cell[3] = new PdfPCell(para[3]);
			cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);			
			cell[3].enableBorderSide(1);
			tblExp.addCell(cell[3]);



			String role = "";
			String roleType = "";
			String duration = "";
			String typeOfRole = "";
			for(TeacherRole teacherRole: listTeacherRole)
			{

				role = teacherRole.getRole()==null?"":teacherRole.getRole();
				roleType = teacherRole.getFieldMaster()==null?"":teacherRole.getFieldMaster().getFieldName();
				typeOfRole = teacherRole.getEmpRoleTypeMaster()==null?"":teacherRole.getEmpRoleTypeMaster().getEmpRoleTypeName();
				if(teacherRole.getCurrentlyWorking())
				{
					duration = teacherRole.getRoleStartYear()+ Utility.getLocaleValuePropByKey("lbltoPresent", locale);
				}	
				else
				{
					duration = teacherRole.getRoleStartYear()+" to "+teacherRole.getRoleEndYear();
				}

				para[0] = new Paragraph(role,font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].enableBorderSide(1);
				cell[0].setPaddingBottom(5);
				tblExp.addCell(cell[0]);

				para[1] = new Paragraph(roleType,font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].enableBorderSide(1);
				cell[1].setPaddingBottom(5);
				tblExp.addCell(cell[1]);


				para[2] = new Paragraph(duration,font9);
				cell[2] = new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);			
				cell[2].enableBorderSide(1);
				cell[2].setPaddingBottom(5);
				tblExp.addCell(cell[2]);

				para[3] = new Paragraph(typeOfRole,font9);
				cell[3] = new PdfPCell(para[3]);
				cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);			
				cell[3].enableBorderSide(1);
				cell[3].setPaddingBottom(5);
				tblExp.addCell(cell[3]);



			}


			if(listTeacherRole==null || listTeacherRole.size()==0)
			{
				para[0] = new Paragraph(" ",font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].enableBorderSide(1);
				cell[0].setColspan(4);
				tblExp.addCell(cell[0]);

				para[1] = new Paragraph("",font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].setBorder(0);
				cell[1].setColspan(3);
				tblExp.addCell(cell[1]);
			}

			document.add(tblExp);
			{
				float[] width = {.2f,.7f};
				PdfPTable tbl= new PdfPTable(width);

				para[0] = new Paragraph("\n"+Utility.getLocaleValuePropByKey("lblInvolvement", locale)+"",font9bold);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].setBorder(0);			
				cell[0].setPaddingBottom(5);
				cell[0].setColspan(2);
				tbl.addCell(cell[0]);			

				document.add(tbl);
			}



			java.util.List<TeacherInvolvement> listInvolvement = null;
			listInvolvement = teacherInvolvementDAO.findTeacherInvolvementByTeacher(teacherDetail);



			float[] widthInv = {.3f,.3f,.15f,.15f};
			PdfPTable tblInv = new PdfPTable(widthInv);

			para = new Paragraph[5];
			cell = new PdfPCell[5];

			para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblOrga", locale),font9bold);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].enableBorderSide(1);
			cell[0].setPaddingBottom(5);
			tblInv.addCell(cell[0]);

			para[1] = new Paragraph(Utility.getLocaleValuePropByKey("lblType", locale),font9bold);
			cell[1] = new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].enableBorderSide(1);
			cell[1].setPaddingBottom(5);
			tblInv.addCell(cell[1]);


			para[2] = new Paragraph(Utility.getLocaleValuePropByKey("lblNumberofPeople", locale),font9bold);
			cell[2] = new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);			
			cell[2].enableBorderSide(1);
			cell[2].setPaddingBottom(5);
			tblInv.addCell(cell[2]);

			para[3] = new Paragraph(Utility.getLocaleValuePropByKey("msgLeadPeople", locale),font9bold);
			cell[3] = new PdfPCell(para[3]);
			cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);			
			cell[3].enableBorderSide(1);
			cell[3].setPaddingBottom(5);
			tblInv.addCell(cell[3]);




			String org = "";
			String orgType = "";
			String noOfPeople = "";
			String ledPeople = "";
			for(TeacherInvolvement teacherInv: listInvolvement)
			{
				org = teacherInv.getOrganization()==null?"":teacherInv.getOrganization();
				orgType = teacherInv.getOrgTypeMaster()==null?"":teacherInv.getOrgTypeMaster().getOrgType();
				noOfPeople = teacherInv.getPeopleRangeMaster()==null?"":teacherInv.getPeopleRangeMaster().getRange();
				ledPeople = teacherInv.getLeadNoOfPeople()==null?"":""+teacherInv.getLeadNoOfPeople();

				para[0] = new Paragraph(org,font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].enableBorderSide(1);
				cell[0].setPaddingBottom(5);
				tblInv.addCell(cell[0]);

				para[1] = new Paragraph(orgType,font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].enableBorderSide(1);
				cell[1].setPaddingBottom(5);
				tblInv.addCell(cell[1]);


				para[2] = new Paragraph(noOfPeople,font9);
				cell[2] = new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);			
				cell[2].enableBorderSide(1);
				cell[2].setPaddingBottom(5);
				tblInv.addCell(cell[2]);

				para[3] = new Paragraph(ledPeople,font9);
				cell[3] = new PdfPCell(para[3]);
				cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);			
				cell[3].enableBorderSide(1);
				cell[3].setPaddingBottom(5);
				tblInv.addCell(cell[3]);

			}

			if(listInvolvement==null || listInvolvement.size()==0)
			{
				para[0] = new Paragraph(" ",font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].enableBorderSide(1);
				cell[0].setColspan(4);
				tblInv.addCell(cell[0]);

				para[1] = new Paragraph("",font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].setBorder(0);
				cell[1].setColspan(3);
				tblInv.addCell(cell[1]);
			}		


			document.add(tblInv);	

			{
				float[] width = {.2f,.7f};
				PdfPTable tbl= new PdfPTable(width);

				para[0] = new Paragraph("\n"+Utility.getLocaleValuePropByKey("lblHonors", locale)+"",font9bold);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].setBorder(0);			
				cell[0].setColspan(2);
				cell[0].setPaddingBottom(5);
				tbl.addCell(cell[0]);			

				document.add(tbl);
			}

			java.util.List<TeacherHonor> lstTeacherHonors = teacherHonorDAO.findTeacherHonersByTeacher(teacherDetail);

			float[] widthHonors = {.45f,.45f};
			PdfPTable tblHonors = new PdfPTable(widthHonors);

			para = new Paragraph[5];
			cell = new PdfPCell[5];

			para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgAwardName", locale),font9bold);
			cell[0] = new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].enableBorderSide(1);
			cell[0].setPaddingBottom(5);
			tblHonors.addCell(cell[0]);

			para[1] = new Paragraph(Utility.getLocaleValuePropByKey("lblYearReceived", locale),font9bold);
			cell[1] = new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].enableBorderSide(1);
			cell[1].setPaddingBottom(5);
			tblHonors.addCell(cell[1]);

			String awardName = "";
			String awardYear = "";
			for (TeacherHonor honor : lstTeacherHonors) 
			{
				awardName = honor.getHonor()==null?"":honor.getHonor();
				awardYear = honor.getHonorYear()==null?"":""+honor.getHonorYear();

				para[0] = new Paragraph(awardName,font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].enableBorderSide(1);
				cell[0].setPaddingBottom(5);
				tblHonors.addCell(cell[0]);

				para[1] = new Paragraph(awardYear,font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].enableBorderSide(1);
				cell[1].setPaddingBottom(5);
				tblHonors.addCell(cell[1]);

			}
			if(lstTeacherHonors==null || lstTeacherHonors.size()==0)
			{

				para[0] = new Paragraph(" ",font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].enableBorderSide(1);
				cell[0].setColspan(2);
				tblHonors.addCell(cell[0]);

				para[1] = new Paragraph("",font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].setBorder(0);
				cell[1].setColspan(3);
				tblHonors.addCell(cell[1]);
			}	

			document.add(tblHonors);

			{
				String affidavitText=Utility.getLocaleValuePropByKey("optN", locale);
				TeacherAffidavit teacherAffidavit = teacherAffidavitDAO.findAffidavitByTeacher(teacherDetail);
				if(teacherAffidavit!=null)
				{
					if(teacherAffidavit.getAffidavitAccepted()!=null && teacherAffidavit.getAffidavitAccepted())
					{
						affidavitText = Utility.getLocaleValuePropByKey("optY", locale);
					}
				}
				float[] width = {.2f,.7f};
				PdfPTable tbl= new PdfPTable(width);

				para[0] = new Paragraph("\n"+Utility.getLocaleValuePropByKey("headAffidavit", locale),font9bold);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].setBorder(0);
				tbl.addCell(cell[0]);	

				para[1] = new Paragraph("",font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].setBorder(0);
				tbl.addCell(cell[1]);

				para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgAffidavitaccepted", locale),font9);
				cell[0] = new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].setBorder(0);
				tbl.addCell(cell[0]);	

				para[1] = new Paragraph(""+affidavitText,font9);
				cell[1] = new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[1].setBorder(0);
				tbl.addCell(cell[1]);

				document.add(tbl);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{

			if(document != null && document.isOpen())
				document.close();
			if(writer!=null){
				writer.flush();
				writer.close();
			}

		}

		return "";
	}

	public void cellLayout(Rectangle arg1, PdfContentByte[] arg2) {
		try {
			PdfContentByte pdfContentByte = arg2[PdfPTable.BACKGROUNDCANVAS];
			Image bgImage = Image.getInstance("URL_TO_YOUR_IMAGE");
			pdfContentByte.addImage(bgImage, arg1.getWidth(), 0, 0, arg1
					.getHeight(), arg1.getLeft(), arg1.getBottom());

		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException, DocumentException  {

		Document document = new Document(new Rectangle(200, 120));
		// step 2
		PdfWriter writer
		= PdfWriter.getInstance(document, new FileOutputStream("C:\\one.pdf"));
		// step 3
		document.open();
		// step 4
		PdfContentByte canvas = writer.getDirectContent();
		// state 1:
		/* canvas.setRGBColorFill(0xFF, 0x45, 0x00);
	        // fill a rectangle in state 1
	        canvas.rectangle(10, 10, 60, 60);
	        canvas.fill();
	        canvas.saveState();
	        // state 2;
	        canvas.setLineWidth(3);
	        canvas.setRGBColorFill(0x8B, 0x00, 0x00);
	        // fill and stroke a rectangle in state 2
	        canvas.rectangle(40, 20, 60, 60);
	        canvas.fillStroke();
	        canvas.saveState();
	        // state 3:
	        canvas.concatCTM(1, 0, 0.1f, 1, 0, 0);
	        canvas.setRGBColorStroke(0xFF, 0x45, 0x00);
	        canvas.setRGBColorFill(0xFF, 0xD7, 0x00);
	        // fill and stroke a rectangle in state 3
	        canvas.rectangle(70, 30, 60, 60);
	        canvas.fillStroke();
	        canvas.restoreState();
	        // stroke a rectangle in state 2
	        canvas.rectangle(100, 40, 60, 60);
	        canvas.stroke();
	        canvas.restoreState();
	        // fill and stroke a rectangle in state 1
	        canvas.rectangle(130, 50, 60, 60);
	        canvas.fillStroke();
	        // step 5*/
		drawRectangle(canvas, 10, 10);



		document.close();
	}


	public static void drawRectangle(PdfContentByte content, float width, float height) {
		content.saveState();
		PdfGState state = new PdfGState();
		state.setFillOpacity(0.6f);
		content.setGState(state);
		content.setRGBColorFill(0xFF, 0xFF, 0xFF);
		content.setLineWidth(3);
		content.rectangle(0, 0, width, height);
		content.fillStroke();
		content.restoreState();
	}

	@SuppressWarnings("deprecation")
	public void generateIPIPDReport(HttpServletRequest request,TeacherAssessmentStatus teacherAssessmentStatus,String reportPath,String fileName,String urlRootPath)
	{
		System.out.println("::::::::::::::::::::::: generateIPIPDReport :::::::::::::::::::::::");
        TeacherDetail teacherDetail = null;
		try{
			teacherDetail = teacherAssessmentStatus.getTeacherDetail();
			System.out.println("teacherDetail.getTeacherId() : "+teacherDetail.getTeacherId());
		}catch(Exception e){
			e.printStackTrace();
		}
		
		Font font60 = null;
		Font font20 = null;
		Font font16 = null;
		Font font12 = null;
		Font font12bold = null;
		Font font12Italic = null;
		Font font12ItalicBold = null;
		Font font12boldBlack = null;
		Font font12boldBlue = null;
		Font font7 = null;
		Font font9bold = null;
		Font font9 = null;
		BaseFont tahoma = null;
		Color blueColor =null;
		Color greenColor =null;
		String fontPath = request.getRealPath("/");
		
		try {
			tahoma = BaseFont.createFont(fontPath+"fonts/42933.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			blueColor = new Color(0,122,180); 
			greenColor = new Color(149,203,110);
			font60 = new Font(tahoma, 60,Font.BOLD);
			font20 = new Font(tahoma, 20,Font.BOLD);
			font12 = new Font(tahoma, 12);
			font12boldBlack = new Font(tahoma,12,Font.BOLD);
			font12boldBlue = new Font(tahoma,12,Font.BOLD,blueColor);
			font12bold = new Font(tahoma, 12,Font.BOLD);
			font12Italic = new Font(tahoma, 12,Font.ITALIC);
			font12ItalicBold = new Font(tahoma, 12,Font.ITALIC|Font.BOLD,greenColor);
			font16 = new Font(tahoma, 16,Font.BOLD);
			font7 = new Font(tahoma, 7);
			font9bold = new Font(tahoma, 8,Font.BOLD);
			font9 = new Font(tahoma, 9);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		
		try{
			File file = new File(reportPath+"/temp");
			if(!file.exists())
				file.mkdirs();
			else
				Utility.deleteAllFileFromDir(reportPath+"/temp");

			document=new Document(PageSize.A4,10f,10f,20f,30f);
			
			document.addAuthor("TeacherMatch");
			document.addCreator("TeacherMatch Inc.");
			document.addSubject("Instructional Proficiency Inventory");
			document.addCreationDate();
			document.addTitle("PROFESSIONAL DEVELOPMENT REPORT");

			fos = new FileOutputStream(reportPath+"/"+fileName);
			writer = PdfWriter.getInstance(document, fos);
			writer.setStrictImageSequence(true);
			
			String firstName = teacherDetail.getFirstName();
			String lastName = teacherDetail.getLastName()==null?"":teacherDetail.getLastName();
			
			HeaderAndFooter event = new HeaderAndFooter((firstName+" "+lastName),font7);
			writer.setBoxSize("art", new Rectangle(36, 54, 559, 788));
			writer.setPageEvent(event);

			document.open();

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);
			/////////////////////////// Start 1st page /////////////////////////////////////////
			Paragraph [] para = null;
			PdfPCell [] cell = null;

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			PdfPCell blankcell = new PdfPCell(new Paragraph("     ",font7));
			blankcell.setBorder(0);
			
			Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
			logo.scalePercent(75);
			float[] widthParticularlogo = {.14f,.2f};
			PdfPTable logotable = new PdfPTable(widthParticularlogo);
			logotable.setWidthPercentage(90);
			PdfPCell logocol1 = new PdfPCell(new Paragraph("     ",font7));
			logocol1.setBorder(0);
			PdfPCell logocol2 = new PdfPCell(logo);
			logocol2.setBorder(0);
			
			logotable.addCell(logocol1);
			logotable.addCell(logocol2);
			////////////////////////////////////////////////////////////////
			cell[1]= new PdfPCell(logotable);
			//cell[1]= new PdfPCell(logo);
			cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[1].setBorder(0);

			para[2] = new Paragraph("PROFESSIONAL DEVELOPMENT REPORT",font20);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);

			for(int i=0;i<2;i++)
				mainTable.addCell(cell[0]);

			mainTable.addCell(cell[1]);
			for(int i=0;i<3;i++)
				mainTable.addCell(cell[0]);
			mainTable.addCell(cell[2]);
			mainTable.addCell(cell[0]);

			para[2] = new Paragraph("Instructional Proficiency Inventory",font20);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);

			mainTable.addCell(cell[2]);
			
			for(int i=0;i<1;i++)
				mainTable.addCell(cell[0]);

			//java.util.List<TeacherAssessmentStatus> teacherAssessmentStatusList1 = new ArrayList<TeacherAssessmentStatus>();
			
			try{
				//teacherAssessmentStatusList1 = teacherAssessmentStatusDAO.findAssessmentCompletedDateTime(teacherAssessmentStatus);
				para[2] = new Paragraph("IPI taken on "+Utility.convertDateAndTimeToUSformatOnlyDate(teacherAssessmentStatus.getAssessmentCompletedDateTime()),font12);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[2].setBorder(0);
				
				mainTable.addCell(cell[2]);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			for(int i=0;i<3;i++)
				mainTable.addCell(cell[0]);
			
			para[2] = new Paragraph(firstName+" "+lastName,font20);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);

			mainTable.addCell(cell[2]);
			for(int i=0;i<3;i++)
				mainTable.addCell(cell[0]);

			try
			{
				para[2] = new Paragraph("Printed on ",font12);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[2].setBorder(0);
				
				mainTable.addCell(cell[2]);
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			para[2] = new Paragraph(Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font20);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);
			mainTable.addCell(cell[2]);

			document.add(mainTable);
			
			document.newPage();

			/////////////////////End 1st page ///////////////////////////////////

			/////////////////////Start 2nd page ////////////////////////////////
			
			
			font20 = new Font(tahoma, 20,Font.BOLD,blueColor);
			font16 = new Font(tahoma, 16,Font.BOLD,blueColor);
			font12bold = new Font(tahoma, 12,Font.BOLD,greenColor);
			
			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);
			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph("Contents",font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);

			String dis = "Contents";
			String contents[] = {"Contents","Interpretation","Using Your Results","Profile Summary","Leverage your Strengths","Target Opportunities for Growth","Professional Development Plan"};

			for(int i=0;i<contents.length;i++)
			{
				para[2] = new Paragraph("\u2022 "+contents[i],font12);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[2].setLeading(0f,1.5f);
				cell[2].setBorder(0);
				mainTable.addCell(cell[2]);
			}

			document.add(mainTable);
			mainTable.deleteBodyRows();
			document.newPage();
			/////////////////////End 2nd page ////////////////////////////////		

			/////////////////////Start 3rd page ////////////////////////////////

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph("Interpretation",font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);
			mainTable.addCell(cell[1]);

			para[1] = new Paragraph("Opening",font16);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);

			dis = "Congratulations! You have completed the TeacherMatch ";
			para[2] = new Paragraph(dis,font12);
			para[2].add(new Chunk("Instructional Proficiency Inventory", font12Italic));

			dis = " (IPI). The IPI was developed to help hiring authorities learn which knowledge, skills, and abilities have " +
					"the greatest impact on teaching effectiveness. Efforts to improve these �impact competencies� allow users to " +
					"zero in on those issues that can create the greatest overall change in teaching effectiveness, so these results " +
					"can be a guide for professional development.\n\nThe questions on the IPI correspond to one of the competencies on the " +
					"Essential Elements for Effective Teaching Framework. The diagram below offers a graphic display of the iterative process" +
					" of teaching. Each of its four competencies [shown in the color spheres] is an essential part of the cycle of highly " +
					"effective teaching. Under each competency is a set of objectives that define measurable and actionable ways to develop " +
					"those competencies. Your results have been aggregated for each competency, allowing you to see strengths you can leverage " +
					"in your practice as well as any opportunities to target for personal growth.  ";

			para[2].add(new Chunk(dis, font12));

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0f,1.5f);
			cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[2].setBorder(0);


			mainTable.addCell(cell[2]);

			document.add(mainTable);
		
			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);
			cell = new PdfPCell[3];
			Image lf1 = Image.getInstance (fontPath+"/images/essentialelement.png");
			lf1.scalePercent(38);

			cell[1]= new PdfPCell(lf1);
			cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
			//cell[1].setTop(200);
			cell[1].setBorder(0);

			cell[0]= new PdfPCell(new Paragraph(" ",font16));
			cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[0].setBorder(0);

			mainTable.addCell(cell[0]);
			mainTable.addCell(cell[0]);
			mainTable.addCell(cell[0]);
			mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);
			
			document.add(mainTable);
			mainTable.deleteBodyRows();
			document.newPage();

			/////////////////////end 3rd page ////////////////////////////////			

			/////////////////////Start 4th page ////////////////////////////////
			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);
			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph("Guidelines",font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			mainTable.addCell(cell[1]);

			para[1] = new Paragraph("How to Understand Your Report",font16);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);

			para[2] = new Paragraph("Your TeacherMatch personalized ",font12);

			dis = " offers you specific suggestions on how to " +
			"leverage personal strengths and opportunities for growth, based on the pattern of responses derived from the IPI." +
			" For each area of strength, we provide a research-based description of the behaviors of a highly effective teacher." +
			" You will no doubt see yourself and your classroom in some of those behaviors and can select others to adopt." +
			" You will also have explanations and descriptions of your opportunities for growth, with concrete suggestions of steps you can take." +
			" Finally, the last section of your report offers some suggestions on concrete ways to use this information in your next teaching position." +
			"\n\nThis feedback report is organized by each of the four competencies within the Teaching for Learning Framework;";

			para[2].add(new Chunk("Professional Development Report", font12Italic));
			para[2].add(new Chunk(dis, font12));
			para[2].add(new Chunk("Planning for Successful Outcomes, Creating a Learning Environment, Instructing, and Analyzing and Adjusting. " +
					"Your leverage, reinforce, and target areas have been identified based on your responses to questions within each" +
					" competency", font12Italic));

			dis = " Scores in the report represent the percent of " +
			"items you answered correctly within each competency. Your scores are then compared to the national group norm," +
			" which represents how your scores compare to all test candidates who completed the same items." +
			" For example, \"Jane's score on this competency was higher than the scores of 85% of the norm group.\" ";
		
			para[2].setLeading(1f);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0f,1.5f);
			cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[2].setBorder(0);


			mainTable.addCell(cell[2]);
			mainTable.addCell(blankcell);

			para[1] = new Paragraph("How to Use This Report",font16);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(cell[1]);
			

			dis = "This report can be used as a blueprint for your professional development. To be a highly effective teacher, " +
					"you need to be a reflective practitioner: thinking about your work, identifying strengths and areas that " +
					"could be stronger, tracking your progress, and planning for growth. The Essential Elements of Effective Teaching " +
					"breaks down the iterative process of teaching into manageable, actionable segments. \n\n Your leverage areas are identified" +
					" strengths and are an affirmation of your work. With your leverage areas for support, the categories identified as target" +
					" areas can be improved using suggest specific tactics to grow your practice as a highly effective teacher. By prioritizing" +
					" a manageable selection of these tactics, you can focus on specific steps in your professional growth. The reinforce areas " +
					"should not be ignored but they may not receive the same level of focus as the target areas. The suggestions in the reinforce " +
					"area can be used to hone your practice. By tracking your progress, you can identify tactics that are working for you, and then " +
					"move on to additional steps. ";
			para[2] = new Paragraph(dis,font12);

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0f,1.5f);
			cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[2].setBorder(0);

			mainTable.addCell(cell[2]);

			para[1] = new Paragraph("Resources and Strategies",font12bold);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);
			//mainTable.addCell(cell[0]);

			document.add(mainTable);

			List sublist =  new List(false, 12);
			//List sublist =  new List(List.ORDERED, 12);
			sublist.setListSymbol(new Chunk("\u2022", font9));
			dis = "If you are teaching in a school with a mentoring program, share this with your mentor and seek advice and support." +
			" Ask your mentor to help you track your progress on those steps you've identified to work on. ";
			sublist.add(new ListItem(dis,font12));

			dis = "If you don't have a formal mentoring program available," +
			" ask to observe respected teachers and consider how their behaviors model the steps you are working on.";
			sublist.add(new ListItem(dis,font12));

			dis = "Ask teachers you trust, who are not involved in your evaluation, to observe you in your classroom," +
			" giving them specific tactics and/or issues to watch for, getting feedback from them afterwards.";
			sublist.add(new ListItem(dis,font12));

			dis = "Seek out teachers who are willing to have a conversation with you about some of these issues and what has worked for them. ";
			sublist.add(new ListItem(dis,font12));

			List list = new List(false, 15);
			list.setListSymbol(new Chunk("\u2022", font12));
			list.setIndentationLeft(32);
			list.setIndentationRight(32);

			list.add(new ListItem("Work alone or in collaboration with another supportive person or two.",font12));
			list.add(sublist);
			list.add(new ListItem("Keep a written record of your goals and your progress toward them to see what works for you and to keep growing professionally.",font12));
			list.add(new ListItem("Explore conferences and materials available through professional organizations in your field, either by grade level or subject specialty.",font12));
			list.add(new ListItem("Research written and online resources in those areas you are working on.",font12));

			document.add(list);

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);


			dis="The action plan form located at the end of this report empowers you to identify a manageable number of objectives to tackle. " +
			"You can then select specific tactics for those objectives. By building in \"check-ins\" to assess your progress for each objective," +
			" you can track your success and recognize when it's time to move on to other objectives. The very best teachers never stop growing professionally, " +
			"and their commitment to that growth energizes them and their students. ";

			para[1] = new Paragraph(dis,font12);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setLeading(0f,1.5f);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);

			document.add(mainTable);
			mainTable.deleteBodyRows();
			document.newPage();




			/////////////////////End 5th page //////////////////////////////////

			///////////////////// Start 6th page //////////////////////////////
			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph("Using Your Results",font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			//mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);


			para[1] = new Paragraph("As you review your report, please keep in mind the following guidelines:",font12bold);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(cell[1]);
			//mainTable.addCell(cell[0]);


			para[1] = new Paragraph("\u2022  What competencies are the most positive?",font12);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);
			mainTable.addCell(cell[1]);

			document.add(mainTable);

			sublist =  new List(false, 12);
			sublist.setListSymbol(new Chunk("\u2022", font9));

			dis = "Do you recognize why these scores might be higher than others?" +
			" Can you think of specific examples of your past behavior and experiences that explain that difference?";
			sublist.add(new ListItem(dis,font12));

			list = new List(false, 15);
			list.setListSymbol(new Chunk("\u2022", font12));
			list.setIndentationLeft(32);
			list.setIndentationRight(32);

			list.add(sublist);
			document.add(list);


			list = new List(false, 15);
			list.setListSymbol(new Chunk("\u2022", font12));
			list.setIndentationLeft(32);
			list.setIndentationRight(32);
			list.add(new ListItem("Review opportunities for growth in the report - both at the competency and objective levels.",font12));

			sublist =  new List(false, 12);
			sublist.setListSymbol(new Chunk("\u2022", font9));

			dis = "Ask yourself why these areas might be a weakness - can you come up with specific examples?";
			sublist.add(new ListItem(dis,font12));

			list.add(sublist);
			document.add(list);

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			dis = "It is perfectly normal to find this information a bit overwhelming and even emotionally loaded." +
			" The feedback will be most helpful if you give yourself some time to take it all in before you develop " +
			"an action plan on how to use it. ";
			para[1] = new Paragraph(dis,font12);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setLeading(0f,1.5f);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			//mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);
			//mainTable.addCell(blankcell);

			para[1] = new Paragraph("After you have analyzed your Professional Development Report, please complete the following steps, using the action plan template at the end of this report:",font12bold);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setLeading(0f,1.5f);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(cell[1]);
			//mainTable.addCell(cell[0]);
			document.add(mainTable);

			list = new List(false, 15);
			list.setListSymbol(new Chunk("\u2022", font12));
			list.setIndentationLeft(32);
			list.setIndentationRight(32);

			list.add(new ListItem("Summarize the top 2-3 strengths and 1-2 opportunities for growth that you'd like to emphasize for action in the coming 12 months.",font12));
			list.add(new ListItem("Ask yourself what you can do to continue building your areas of strength.",font12));
			list.add(new ListItem("For weaker scores, identify resources that might be helpful - for example, suggestions and coaching from those who have strengths that complement your own.",font12));
			list.add(new ListItem("Once you have identified specific areas of focus, develop a specific action plan.",font12));

			document.add(list);

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);
			para[1] = new Paragraph("You are now ready to interpret your Professional Development Report. Thank you for participating!",font12bold);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setLeading(0f,1.5f);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);
			mainTable.addCell(blankcell);
			document.add(mainTable);
			mainTable.deleteBodyRows();
			document.newPage();
			///////////////////// Start 6th page //////////////////////////////

			/////////////////////Start 7th page ////////////////////////////////

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph("Score Summary",font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			para[2] = new Paragraph("This section displays your results for ",font12);
			para[2].add(new Chunk("Teaching Skills", font12Italic));
			dis = ". Your total score represents the total percent correct across all items. Competency scores are also displayed and sorted from most to least positive. Use this section to compare your scores to those in the national norm group.";
			para[2].add(new Chunk(dis, font12));
			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0f,1.5f);
			cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[2].setBorder(0);

			mainTable.addCell(cell[1]);
			mainTable.addCell(blankcell);
			mainTable.addCell(cell[2]);
			mainTable.addCell(blankcell);
			java.util.List<CompetencyMaster> competencyMasters = competencyMasterDAO.getCompetenciesForPDR(true);
//			java.util.List<CompetencyMaster> competencyMasters = teacherAssessmentQuestionDAO.findTeacherCompetency(teacherAssessmentStatus.getTeacherAssessmentdetail());

			Map<Integer,CompetencyMaster> map = new HashMap<Integer, CompetencyMaster>();
			Map<CompetencyMaster,Integer> map2 = new HashMap<CompetencyMaster,Integer >();
			Map<Integer, CompetencyMaster> map3 = new HashMap<Integer, CompetencyMaster>();
			
			///////////////// New process competency //////////////////////////////////////
			
			Map<Integer, AssessmentCompetencyScore> mapCompetency = new HashMap<Integer, AssessmentCompetencyScore>();
			java.util.List<AssessmentCompetencyScore>  assessmentCompetencyScores = assessmentCompetencyScoreDAO.findAllCompetencyByTAS(competencyMasters,teacherAssessmentStatus);
			for (AssessmentCompetencyScore assessmentCompetencyScore : assessmentCompetencyScores) {
				mapCompetency.put(assessmentCompetencyScore.getCompetencyMaster().getCompetencyId(), assessmentCompetencyScore);
			}
			AssessmentCompetencyScore assessmentCompetencyScore=null;
			for(CompetencyMaster competencyMaster: competencyMasters)
			{
				if(assessmentCompetencyScore==null){
				 assessmentCompetencyScore = mapCompetency.get(competencyMaster.getCompetencyId());
				}else
					assessmentCompetencyScore = mapCompetency.get(competencyMaster.getCompetencyId());
				if(assessmentCompetencyScore!=null){
				map.put(assessmentCompetencyScore.getNormscore().intValue(),competencyMaster);
				map2.put(competencyMaster, assessmentCompetencyScore.getNormscore().intValue());
				map3.put(competencyMaster.getCompetencyId(), competencyMaster);
				
				competencyMaster.setTeacherScore(assessmentCompetencyScore.getNormscore());
				}
			}
			
			ArrayList<CompetencyMaster> comTeacherScore=new ArrayList<CompetencyMaster>();
			for(CompetencyMaster cmp:competencyMasters){
				if(cmp.getTeacherScore()==null)
				comTeacherScore.add(cmp);
			}
			competencyMasters.removeAll(comTeacherScore);	
			Collections.sort(competencyMasters,CompetencyMaster.competencyMasterComparator);

			Collection<Integer> c = map.keySet();
			int max=Collections.max(c);
			int min=Collections.min(c);
			
			CompetencyMaster competencyMasterMax = map.get(max);
			CompetencyMaster competencyMasterMin = map.get(min);

			CompetencyMaster compMax = null;
			CompetencyMaster compMin = null;

			int rankMax = competencyMasterMax.getRank();
			int rankMin = competencyMasterMin.getRank();
			for(CompetencyMaster competencyMaster: competencyMasters)
			{
				int p=map2.get(competencyMaster);
				if(p==(max))
				{
					//if((competencyMaster.getRank()==null?0:competencyMaster.getRank())>rankMax)
					if((competencyMaster.getRank()==null?0:competencyMaster.getRank())<rankMax)
					{
						compMax = competencyMaster;
						//new con
						rankMax = competencyMaster.getRank();
					}
				}
				if(p==(min))
				{
					//if((competencyMaster.getRank()==null?0:competencyMaster.getRank())>rankMin)
					if((competencyMaster.getRank()==null?0:competencyMaster.getRank())<rankMin)
					{
						compMin = competencyMaster;
						//new con
						rankMin = competencyMaster.getRank();
					}
				}
			}

			if(compMin!=null)
				competencyMasterMin = compMin;

			if(compMax!=null)
				competencyMasterMax = compMax;

			if(competencyMasterMin.getCompetencyId().equals(competencyMasterMax.getCompetencyId()))
			{
				//compMin = 
				for(CompetencyMaster competencyMaster: competencyMasters)
				{
					if((rankMin+1)==competencyMaster.getRank())
					{
						competencyMasterMin = competencyMaster;
						break;
					}
				}
			}
			
			if(map3.containsKey(competencyMasterMax.getCompetencyId())){
				map3.remove(competencyMasterMax.getCompetencyId());
			}
			if(map3.containsKey(competencyMasterMin.getCompetencyId())){
				map3.remove(competencyMasterMin.getCompetencyId());
			}
			
			mainTable.deleteBodyRows();
			document.newPage();

			/////////////////////End 7th page ////////////////////////////////
			
			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph("Profile Summary",font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);
			
			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);
			
			dis = "This section displays your strengths to leverage, competencies to reinforce, and targets that present opportunities for growth.";
			para[2] = new Paragraph(dis,font12);

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0f,1.5f);
			cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[2].setBorder(0);

			mainTable.addCell(cell[2]);
			mainTable.addCell(blankcell);
			mainTable.addCell(cell[0]);
			mainTable.addCell(cell[0]);
			mainTable.addCell(cell[0]);
			
			font16 = new Font(tahoma, 16,Font.ITALIC);
			PdfPTable leverageTable = new PdfPTable(2);
			
			Image lf2 = Image.getInstance (fontPath+"/images/learning_framework.png");
			lf2.scalePercent(38);

			cell[1]= new PdfPCell(lf2);
			cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
			//cell[1].setTop(200);
			cell[1].setBorder(com.itextpdf.text.Rectangle.LEFT);
			
			cell[1].setRowspan(6);
			
			leverageTable.setTotalWidth(new float[]{ 175 ,100});
			leverageTable.setLockedWidth(true);
			leverageTable.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			dis = "Leverage your strength �";
			para[2] = new Paragraph(dis,font16);

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0,2);
			cell[2].setMinimumHeight(25);
			cell[2].setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell[2].setBackgroundColor(Color.lightGray);
			cell[2].setBorder(Rectangle.BOX);
			
			leverageTable.addCell(cell[2]);
			leverageTable.addCell(cell[1]);
			
			//dis = "Planning for Successful Outcomes";
			dis = competencyMasterMax.getCompetencyName();
			para[2] = new Paragraph(dis+"\n\n",font12);

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0,2);
			cell[2].setMinimumHeight(25);
			cell[2].setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell[2].setBorder(Rectangle.BOX);
			
			leverageTable.addCell(cell[2]);
			
			dis = "�to Reinforce �";
			para[2] = new Paragraph(dis,font12);

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0,2);
			cell[2].setMinimumHeight(25);
			cell[2].setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell[2].setBackgroundColor(Color.lightGray);
			cell[2].setBorder(Rectangle.BOX);
			
			leverageTable.addCell(cell[2]);
			
			//dis = "Analyzing and Adjusting";
			dis = "";
			for(Integer key : map3.keySet()){
				System.out.println("map3 : "+map3.keySet());
				if(dis.equals(""))
					dis = map3.get(key).getCompetencyName();
				else
					dis += "\n"+map3.get(key).getCompetencyName();
			}
			para[2] = new Paragraph(dis+"\n\n",font12);

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0,3);
			cell[2].setMinimumHeight(25);
			cell[2].setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell[2].setBorder(Rectangle.BOX);
			
			leverageTable.addCell(cell[2]);
			
			dis = "�and Target opportunities for growth";
			para[2] = new Paragraph(dis,font12);

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0,3);
			cell[2].setMinimumHeight(25);
			cell[2].setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell[2].setBackgroundColor(Color.lightGray);
			cell[2].setBorder(Rectangle.BOX);
			
			leverageTable.addCell(cell[2]);
			 
			//dis = "Instructing";
			dis = competencyMasterMin.getCompetencyName();
			para[2] = new Paragraph(dis+"\n\n",font12);

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0,2);
			cell[2].setMinimumHeight(25);
			cell[2].setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell[2].setBorder(Rectangle.BOX);
			
			leverageTable.addCell(cell[2]);
			
			cell[1]= new PdfPCell(leverageTable);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].setBorder(0);
			
			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);
			
			document.add(mainTable);
			mainTable.deleteBodyRows();
			document.newPage();
			
			//////////////////// Start 8th page /////////////////////////////

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph("Leverage your Strengths",font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			//mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);
			para[1] = new Paragraph(competencyMasterMax.getCompetencyName(),font16);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);
			//mainTable.addCell(cell[0]);

			dis = "Based on your responses, you demonstrate particular strength in  "+competencyMasterMax.getCompetencyName()+". " +
			"Here are possibilities for each of the objectives under this competency. As you read through them and consider both them" +
			" and your current practice, you can choose two to three for your professional development plan.";
			para[2] = new Paragraph(dis,font12);

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0f,1.5f);
			cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[2].setBorder(0);

			mainTable.addCell(cell[2]);
			mainTable.addCell(blankcell);

			document.add(mainTable);


			java.util.List<ObjectiveMaster> objectiveMasters = objectiveMasterDAO.getObjectivesByCompetencyNoOrdering(competencyMasterMax);

			String maxCompetencyPoint = competencyMasterMax.getCompetencyShortName();
			java.util.List<PdStrengthsAndOpportunities> pdStrengthsAndOpportunitiesList =  null;
			java.util.List<PdStrengthsAndOpportunities> pdStrengthsAndOpportunitiesChildList =  null;

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			int point=1;
			for(ObjectiveMaster objectiveMaster : objectiveMasters)
			{
				mainTable = new PdfPTable(1);
				mainTable.setWidthPercentage(90);

				pdStrengthsAndOpportunitiesList = pdStrengthsAndOpportunitiesDAO.getStatementByObjective(objectiveMaster,"s");

				para[2] = new Paragraph(maxCompetencyPoint+point+": "+objectiveMaster.getObjectiveName()+"",font12bold);
				//para[2] = new Paragraph("P"+point+": "+objectiveMaster.getObjectiveName()+"",font12bold);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setLeading(0f,1.5f);
				cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
				cell[2].setBorder(0);

				mainTable.addCell(cell[2]);
				//mainTable.addCell(cell[0]);

				if(objectiveMaster.getObjectiveStrength()!=null && !objectiveMaster.getObjectiveStrength().trim().equals(""))
				{

					para[2] = new Paragraph(objectiveMaster.getObjectiveStrength(),font12);
					cell[2]= new PdfPCell(para[2]);
					cell[2].setLeading(0f,1.5f);
					cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
					cell[2].setBorder(0);

					mainTable.addCell(cell[2]);
					//mainTable.addCell(cell[0]);
				}
				document.add(mainTable);

				if(pdStrengthsAndOpportunitiesList.size()>0)
				{
					pdStrengthsAndOpportunitiesChildList = pdStrengthsAndOpportunitiesDAO.getStatementByObjectiveChild(objectiveMaster,"s");


					//list = new List(false, pdStrengthsAndOpportunitiesList.size());
					list = new List(false, 15);
					list.setListSymbol(new Chunk("\u2022", font12));
					list.setIndentationLeft(32);
					list.setIndentationRight(32);
					//list.add(new ListItem("Third line"));

					// document.add(list);

					for(PdStrengthsAndOpportunities  strengthsAndOpportunities: pdStrengthsAndOpportunitiesList)
					{

						list.add(new ListItem(new ListItem(strengthsAndOpportunities.getStatement(),font12)));

						if(pdStrengthsAndOpportunitiesChildList.size()>0)
						{
							int p=0;
							sublist =  new List(false, 12);
							sublist.setListSymbol(new Chunk("\u2022", font9));
							for(PdStrengthsAndOpportunities child : pdStrengthsAndOpportunitiesChildList)
							{
								if(child.getStrengthsAndOpportunities().getStatementId().equals(strengthsAndOpportunities.getStatementId()))
								{
									sublist.add(new ListItem(child.getStatement(),font12));
									p++;
								}
							}
							list.add(sublist);
							p=0;
						}

					}

					document.add(list);
					document.add(new Paragraph(new Chunk(" ")));
				}else
					mainTable.addCell(blankcell);

				point++;
			}

			mainTable.deleteBodyRows();
			document.newPage();
			//////////////////// End 8th page ///////////////////////////////

			////////////////////Start 9th page Opportunities/////////////////////////////

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph("Target Opportunities for Growth",font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			//mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);
			para[1] = new Paragraph(competencyMasterMin.getCompetencyName(),font16);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);
			//mainTable.addCell(cell[0]);


			dis = "Based on your scores, you demonstrate opportunities for growth in  "+competencyMasterMin.getCompetencyName()+". " +
			"Here are possibilities for each of the objectives under this competency. As you read through them and consider both them " +
			"and your current practice, you can choose two to three for your professional development plan.";

			para[2] = new Paragraph(dis,font12);

			cell[2]= new PdfPCell(para[2]);
			cell[2].setLeading(0f,1.5f);
			cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			cell[2].setBorder(0);

			mainTable.addCell(cell[2]);
			mainTable.addCell(blankcell);

			document.add(mainTable);


			objectiveMasters = objectiveMasterDAO.getObjectivesByCompetencyNoOrdering(competencyMasterMin);


			pdStrengthsAndOpportunitiesList =  null;
			pdStrengthsAndOpportunitiesChildList =  null;

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);
			String minCompetencyPoint = competencyMasterMin.getCompetencyShortName();
			point=1;
			for(ObjectiveMaster objectiveMaster : objectiveMasters)
			{
				mainTable = new PdfPTable(1);
				mainTable.setWidthPercentage(90);

				pdStrengthsAndOpportunitiesList = pdStrengthsAndOpportunitiesDAO.getStatementByObjective(objectiveMaster,"o");


				//para[2] = new Paragraph("AA"+point+": "+objectiveMaster.getObjectiveName()+"",font12bold);
				para[2] = new Paragraph(minCompetencyPoint+point+": "+objectiveMaster.getObjectiveName()+"",font12bold);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setLeading(0f,1.5f);
				cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
				cell[2].setBorder(0);

				mainTable.addCell(cell[2]);
				//mainTable.addCell(cell[0]);

				if(objectiveMaster.getObjectiveStrength()!=null && !objectiveMaster.getObjectiveOpportunity().trim().equals(""))
				{

					para[2] = new Paragraph(objectiveMaster.getObjectiveOpportunity(),font12);
					cell[2]= new PdfPCell(para[2]);
					cell[2].setLeading(0f,1.5f);
					cell[2].setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
					cell[2].setBorder(0);

					mainTable.addCell(cell[2]);
					//mainTable.addCell(cell[0]);
				}
				document.add(mainTable);

				if(pdStrengthsAndOpportunitiesList.size()>0)
				{
					pdStrengthsAndOpportunitiesChildList = pdStrengthsAndOpportunitiesDAO.getStatementByObjectiveChild(objectiveMaster,"o");

					list = new List(false, 15);
					//list.setAlignindent(true);
					list.setListSymbol(new Chunk("\u2022", font12));
					list.setIndentationLeft(32);
					list.setIndentationRight(32);

					for(PdStrengthsAndOpportunities  strengthsAndOpportunities: pdStrengthsAndOpportunitiesList)
					{

						list.add(new ListItem(strengthsAndOpportunities.getStatement(),font12));

						if(pdStrengthsAndOpportunitiesChildList.size()>0)
						{
							int p=0;
							sublist =  new List(false, 12);
							//sublist.setAlignindent(true);
							sublist.setListSymbol(new Chunk("\u2022", font9));
							for(PdStrengthsAndOpportunities child : pdStrengthsAndOpportunitiesChildList)
							{
								if(child.getStrengthsAndOpportunities().getStatementId().equals(strengthsAndOpportunities.getStatementId()))
								{
									sublist.add(new ListItem(child.getStatement(),font12));
									p++;
								}
							}
							list.add(sublist);
							p=0;
						}


					}


					document.add(list);
					document.add(new Paragraph(new Chunk(" ")));
				}else
					mainTable.addCell(blankcell);

				point++;
			}
			mainTable.deleteBodyRows();
			document.newPage();
			//////////////////// End 9th page ///////////////////////////////

			////////////////////Start 10th page /////////////////////////////
			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			para[1] = new Paragraph("Professional Development Plan",font20);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);

			//mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);

			para[1] = new Paragraph("Change Objective:",font12);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].setBorder(0);

			mainTable.addCell(blankcell);
			mainTable.addCell(cell[1]);
			mainTable.addCell(blankcell);


			para[1] = new Paragraph(" ",font60);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			//cell[1].setBorder(2);

			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);

			document.add(mainTable);

			float[] widthParticular = {.4f,.2f,.2f,.3f};
			PdfPTable tbl = new PdfPTable(widthParticular);
			tbl.setWidthPercentage(90);
			//String actions[] = {"Actions","Start Date","End Date","Potential Obstacles"};
			String actions[] = {"Actions","IPI","IPI","IPI"};
			for(int i=0;i<4;i++)
			{
				para[1] = new Paragraph(actions[i],font12);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				tbl.addCell(cell[1]);
			}

			for(int i=0;i<4;i++)
			{
				para[1] = new Paragraph("",font12);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				tbl.addCell(cell[1]);
			}

			for(int i=0;i<32;i++)
			{
				para[1] = new Paragraph(" ",font60);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				//cell[1].setBorder(0);
				tbl.addCell(cell[1]);
			}

			document.add(tbl);

			mainTable.deleteBodyRows();

			document.newPage();

			mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			para[1] = new Paragraph("Follow up:",font12);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].setBorder(0);

			mainTable.addCell(cell[0]);
			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);

			para[1] = new Paragraph(" ",font60);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);

			mainTable.addCell(cell[1]);
			mainTable.addCell(cell[0]);

			document.add(mainTable);

			float[] widthParticular2 = {.3f,.3f,.3f};
			PdfPTable tbl2 = new PdfPTable(widthParticular2);
			tbl2.setWidthPercentage(90);
			//String actions2[] = {"Possible Solutions","Types of Support Needed","Possible Sources of Support"};
			String actions2[] = {"IPI","IPI","IPI"};
			for(int i=0;i<3;i++)
			{
				para[1] = new Paragraph(actions2[i],font12);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				tbl2.addCell(cell[1]);
			}

			for(int i=0;i<3;i++)
			{
				para[1] = new Paragraph("",font12);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				tbl2.addCell(cell[1]);
			}
			for(int i=0;i<27;i++)
			{
				para[1] = new Paragraph(" ",font60);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				//cell[1].setBorder(0);
				tbl2.addCell(cell[1]);
			}

			document.add(tbl2);
			mainTable.deleteBodyRows();
			document.newPage();

			//////////////////// End 10th page ///////////////////////////////

		}catch (Exception e) {
			e.printStackTrace();
		}finally
		{

			if(document != null && document.isOpen())
				document.close();
			if(writer!=null){
				writer.flush();
				writer.close();
			}

		}
		//return false;
	}
}
