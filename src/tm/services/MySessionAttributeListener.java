package tm.services;


import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;
 
public class MySessionAttributeListener implements HttpSessionAttributeListener { 
	private static int countTeacher;
	private static int countAdmin;
	
    public static int getActiveTeacher() {
        return countTeacher;
    }
    public static int getActiveUser() {
        return countAdmin;
    }
	
	public void attributeAdded(HttpSessionBindingEvent event) {
		String attributeName = event.getName();
		Object attributeValue = event.getValue();
		
		
		if(event.getValue() instanceof TeacherDetail){			
			countTeacher++;			
		}
		else if(event.getValue() instanceof UserMaster){
			countAdmin++;						
		}
		else{
			
		}
	}
 
	
	public void attributeRemoved(HttpSessionBindingEvent event) {
		String attributeName = event.getName();
		Object attributeValue = event.getValue();		
		if(event.getValue() instanceof TeacherDetail && countTeacher>0){
			countTeacher--;
		}
		else if(event.getValue() instanceof UserMaster && countAdmin>0){			
			countAdmin--;
		}
		else{
			
		}
	}
 
	
	public void attributeReplaced(HttpSessionBindingEvent event){
		String attributeName = event.getName();
		Object attributeValue = event.getValue();			
	}
}