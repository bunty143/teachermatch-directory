package tm.services;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.criterion.Order;

import tm.bean.CandidateRawScore;
import tm.bean.JobForTeacher;
import tm.bean.NetchemiaDistricts;
import tm.bean.TeacherAnswerDetail;
import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.assessment.AssessmentCompetencyScore;
import tm.bean.assessment.AssessmentDomainScore;
import tm.bean.cgreport.PercentileZscoreTscore;
import tm.bean.cgreport.RawDataForCompetency;
import tm.bean.cgreport.RawDataForDomain;
import tm.bean.cgreport.TmpPercentileWiseZScore;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DomainMaster;
import tm.dao.CandidateRawScoreDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.NetchemiaDistrictsDAO;
import tm.dao.TeacherAnswerDetailDAO;
import tm.dao.TeacherAssessmentQuestionDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.assessment.AssessmentCompetencyScoreDAO;
import tm.dao.assessment.AssessmentDomainScoreDAO;
import tm.dao.assessment.ScoreLookupDAO;
import tm.dao.cgreport.RawDataForCompetencyDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.dao.cgreport.TmpPercentileWiseZScoreDAO;
import tm.services.report.CGReportService;
import tm.utility.TMCommonUtil;
import tm.utility.Utility;

public class CadidateRawScoreThread extends Thread{


	private TeacherDetail teacherDetail;
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	private CandidateRawScoreDAO candidateRawScoreDAO;
	public void setCandidateRawScoreDAO(CandidateRawScoreDAO candidateRawScoreDAO) {
		this.candidateRawScoreDAO = candidateRawScoreDAO;
	}
	private TeacherAnswerDetailDAO teacherAnswerDetailDAO;
	public void setTeacherAnswerDetailDAO(TeacherAnswerDetailDAO teacherAnswerDetailDAO) {
		this.teacherAnswerDetailDAO = teacherAnswerDetailDAO;
	}
	private TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO;
	public void setTeacherAssessmentQuestionDAO(TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO) {
		this.teacherAssessmentQuestionDAO = teacherAssessmentQuestionDAO;
	}
	private CGReportService reportService;
	public void setReportService(CGReportService reportService) {
		this.reportService = reportService;
	}
	
	private TeacherAssessmentdetail teacherAssessmentdetail;
	public void setTeacherAssessmentdetail(TeacherAssessmentdetail teacherAssessmentdetail) {
		this.teacherAssessmentdetail = teacherAssessmentdetail;
	}
	
	private TeacherAssessmentStatus teacherAssessmentStatus;
	public void setTeacherAssessmentStatus(TeacherAssessmentStatus teacherAssessmentStatus) {
		this.teacherAssessmentStatus = teacherAssessmentStatus;
	}
	
	private DistrictMaster districtMaster;
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	
	private String baseURL;
	public void setBaseURL(String baseURL){
		this.baseURL = baseURL;
	}
	
	private String ipiReferralURL;
	public void setIpiReferralURL(String ipiReferralURL){
		this.ipiReferralURL = ipiReferralURL;
	}
	List<TmpPercentileWiseZScore> tmpPercentileWiseZScoreList = null;
	List<PercentileZscoreTscore> pztList = null;
	
	public CadidateRawScoreThread() {
		super();
	}
	public void run()
	{
		System.out.println("\nRun....... ========================== Scoring data  \n");
		System.out.println("teacherId : "+teacherDetail.getTeacherId());
		try {
			List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
			teacherDetails.add(teacherDetail);
			if(teacherAssessmentdetail!=null && teacherAssessmentdetail.getScoreLookupMaster()!=null && teacherAssessmentdetail.getAssessmentDetail()!=null)
			{
				System.out.println("::::::::::::::::::::::::::::::::::::::");
				if(teacherAssessmentdetail.getAssessmentType()==3)
					Thread.sleep(2*1000);
				else
					Thread.sleep(5*1000);
				
				reportService.saveRawDataOfTeacher(teacherDetails, teacherAssessmentdetail);
				Integer lookupId = teacherAssessmentdetail.getScoreLookupMaster().getLookupId();
				Date assessmentDateTime = null;
				System.out.println("lookupId == "+lookupId);
				RawDataForDomainDAO rawDataForDomainDAO =  reportService.getRawDataForDomainDAO();
				List<RawDataForDomain> lstRawDataForDomains = rawDataForDomainDAO.findAllInCurrentYearByTeacher(teacherDetails, teacherAssessmentdetail);
				System.out.println("lstRawDataForDomains.size() == "+lstRawDataForDomains.size());
				String sql = "";
				int i=0;
				Map<Integer, RawDataForDomain> rawDataForDomainMap = new HashMap<Integer, RawDataForDomain>();
				Map<Integer, RawDataForCompetency> rawDataForCompetencyMap = new HashMap<Integer, RawDataForCompetency>();
				RawDataForDomain rawDataForDomainObj = null;
				RawDataForCompetency rawDataForCompetencyObj = null;
				int size = lstRawDataForDomains.size();
				for (RawDataForDomain rawDataForDomain : lstRawDataForDomains) {
					rawDataForDomainMap.put(rawDataForDomain.getDomainMaster().getDomainId(), rawDataForDomain);
					if(i<size-1)
						sql+="(domainId = "+rawDataForDomain.getDomainMaster().getDomainId()+" AND totalscore = "+rawDataForDomain.getScore()+" AND lookupId = "+lookupId+") OR ";
					else
						sql+="(domainId = "+rawDataForDomain.getDomainMaster().getDomainId()+" AND totalscore = "+rawDataForDomain.getScore()+" AND lookupId = "+lookupId+")";
					i++;
				}
				System.out.println("sql domain == "+sql);
				DomainMaster domainMaster = null;
				//String domainScores = " (domainId =1 AND totalscore =80 ) OR ( domainId =2 AND  totalscore =7 ) OR ( domainId =3 AND  totalscore =42) ";
				ScoreLookupDAO scoreLookupDAO = reportService.getScoreLookupDAO();
				TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO = reportService.getTmpPercentileWiseZScoreDAO();
				String isPassFail=null;
				List one = scoreLookupDAO.getCandidatesNormScoreDomainWise(sql);
				System.out.println("one.size() == "+one.size());
				AssessmentDomainScoreDAO assessmentDomainScoreDAO = reportService.getAssessmentDomainScoreDAO();
				for (Iterator iterator = one.iterator(); iterator.hasNext();) {
					Object object[] = (Object[]) iterator.next();
					rawDataForDomainObj = new RawDataForDomain();
					rawDataForDomainObj = rawDataForDomainMap.get(object[0]);
					assessmentDateTime = rawDataForDomainObj.getAssessmentDateTime();
					AssessmentDomainScore assessmentDomainScore = new AssessmentDomainScore();
					domainMaster = new DomainMaster();
					domainMaster.setDomainId((Integer) object[0]);
					isPassFail = (String) object[7];
					System.out.println("isPassFail : "+isPassFail);
					assessmentDomainScore.setTeacherDetail(teacherDetail);
					assessmentDomainScore.setRawDataForDomain(rawDataForDomainObj);
					assessmentDomainScore.setDomainMaster(domainMaster);
					assessmentDomainScore.setPercentile((Double) object[1]);
					assessmentDomainScore.setZscore((Double) object[2]);
					assessmentDomainScore.setTscore((Double) object[3]);
					assessmentDomainScore.setNcevalue((Double) object[4]);
					assessmentDomainScore.setNormscore((Double) object[5]);
					assessmentDomainScore.setRitvalue((Double) object[6]);
					assessmentDomainScore.setScoreLookupMaster(teacherAssessmentdetail.getScoreLookupMaster());
					assessmentDomainScore.setAssessmentDetail(teacherAssessmentdetail.getAssessmentDetail());
					assessmentDomainScore.setAssessmentType(teacherAssessmentdetail.getAssessmentType());
					assessmentDomainScore.setAssessmentTakenCount(teacherAssessmentdetail.getAssessmentTakenCount());
					assessmentDomainScore.setCreatedDateTime(new Date());
					assessmentDomainScore.setAssessmentDateTime(assessmentDateTime);
					assessmentDomainScore.setUpdateDateTime(new Date());
					assessmentDomainScoreDAO.makePersistent(assessmentDomainScore);
				}
				
				TeacherNormScore teacherNormScore = new TeacherNormScore();
				if(teacherAssessmentdetail.getAssessmentType()==1)
				{
					List assessmentDomainData = new ArrayList();
					assessmentDomainData = assessmentDomainScoreDAO.calculateCandidatesNormScore(teacherDetails, teacherAssessmentdetail);
					System.out.println("assessmentDomainData.size() == "+assessmentDomainData.size());
					CGReportService cGReportService = new CGReportService();
					
					if(tmpPercentileWiseZScoreList==null)
					{
						tmpPercentileWiseZScoreList = tmpPercentileWiseZScoreDAO.findByCriteria(Order.asc("zScore"));
						System.out.println("tmpPercentileWiseZScoreList.size() == "+tmpPercentileWiseZScoreList.size());
					}
					if(pztList==null)
					{
						pztList = cGReportService.populateZScoreMap(tmpPercentileWiseZScoreList);
						System.out.println("pztList.size() == "+pztList.size());
					}
					
					try {
						TeacherNormScoreDAO teacherNormScoreDAO = reportService.getTeacherNormScoreDAO();
						for(Object oo: assessmentDomainData){
							Object obj[] = (Object[])oo;
							Double normscoreD = (Double)obj[0];
							//Integer normscore = Math.round(normscoreD);
							Integer normscore = Integer.valueOf((int) Math.round(normscoreD));
							teacherNormScore.setTeacherNormScore( normscore);
							//double percentile = ((normscore-50)*3+50);
							double percentile=cGReportService.getPercentile(pztList,normscore);
							teacherNormScore.setPercentile(percentile);
							Object[] ob =cGReportService.getDeciles(percentile);
							teacherNormScore.setDecileColor(""+ob[0]);
							teacherNormScore.setMinDecileValue((Double)ob[1]);
							teacherNormScore.setMaxDecileValue((Double)ob[2]);
						}
						teacherNormScore.setTeacherDetail(teacherDetail);
						teacherNormScore.setTeacherAssessmentdetail(teacherAssessmentdetail);
						teacherNormScore.setCreatedDateTime(new Date());
						teacherNormScore.setAssessmentDateTime(assessmentDateTime);
						teacherNormScore.setUpdateDateTime(new Date());
						teacherNormScoreDAO.makePersistent(teacherNormScore);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				RawDataForCompetencyDAO rawDataForCompetencyDAO =  reportService.getRawDataForCompetencyDAO();
				AssessmentCompetencyScoreDAO assessmentCompetencyScoreDAO = reportService.getAssessmentCompetencyScoreDAO();
				
				try {
					List<RawDataForCompetency> lstRawDataForCompetency = rawDataForCompetencyDAO.findAllInCurrentYearByTeacher(teacherDetails, teacherAssessmentdetail);
					System.out.println("lstRawDataForCompetency.size() == "+lstRawDataForCompetency.size());
					String sqlComp = "";
					int j=0;
					int size1 = lstRawDataForCompetency.size();
					for (RawDataForCompetency rawDataForCompetency : lstRawDataForCompetency) {
						rawDataForCompetencyMap.put(rawDataForCompetency.getCompetencyMaster().getCompetencyId(), rawDataForCompetency);
						if(j<size1-1)
							sqlComp+="(competencyId = "+rawDataForCompetency.getCompetencyMaster().getCompetencyId()+" AND totalscore = "+rawDataForCompetency.getScore()+" AND lookupId = "+lookupId+") OR ";
						else
							sqlComp+="(competencyId = "+rawDataForCompetency.getCompetencyMaster().getCompetencyId()+" AND totalscore = "+rawDataForCompetency.getScore()+" AND lookupId = "+lookupId+")";
						j++;
					}
					System.out.println("sql competency == "+sqlComp);
					CompetencyMaster competencyMaster = null;
					List two = scoreLookupDAO.getCandidatesNormScoreCompetencyWise(sqlComp);
					System.out.println("two.size() == "+two.size());
					
					for (Iterator iterator = two.iterator(); iterator.hasNext();) {
						Object object[] = (Object[]) iterator.next();
						rawDataForCompetencyObj = new RawDataForCompetency();
						rawDataForCompetencyObj = rawDataForCompetencyMap.get(object[0]);
						AssessmentCompetencyScore assessmentCompetencyScore = new AssessmentCompetencyScore();
						competencyMaster = new CompetencyMaster();
						competencyMaster.setCompetencyId((Integer) object[0]);
						assessmentCompetencyScore.setTeacherDetail(teacherDetail);
						assessmentCompetencyScore.setRawDataForCompetency(rawDataForCompetencyObj);
						assessmentCompetencyScore.setCompetencyMaster(competencyMaster);
						assessmentCompetencyScore.setPercentile((Double) object[1]);
						assessmentCompetencyScore.setZscore((Double) object[2]);
						assessmentCompetencyScore.setTscore((Double) object[3]);
						assessmentCompetencyScore.setNcevalue((Double) object[4]);
						assessmentCompetencyScore.setNormscore((Double) object[5]);
						assessmentCompetencyScore.setRitvalue((Double) object[6]);
						assessmentCompetencyScore.setScoreLookupMaster(teacherAssessmentdetail.getScoreLookupMaster());
						assessmentCompetencyScore.setAssessmentDetail(teacherAssessmentdetail.getAssessmentDetail());
						assessmentCompetencyScore.setAssessmentType(teacherAssessmentdetail.getAssessmentType());
						assessmentCompetencyScore.setAssessmentTakenCount(teacherAssessmentdetail.getAssessmentTakenCount());
						assessmentCompetencyScore.setCreatedDateTime(new Date());
						assessmentCompetencyScore.setAssessmentDateTime(assessmentDateTime);
						assessmentCompetencyScore.setUpdateDateTime(new Date());
						assessmentCompetencyScoreDAO.makePersistent(assessmentCompetencyScore);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
		   //check for fail and pass for SP(Smart Practices)	
				if(teacherAssessmentdetail.getAssessmentType()==3)
				{
					if(isPassFail!=null && !isPassFail.equals("")){
						if(isPassFail.equals("0"))
							isPassFail="F";
						else if(isPassFail.equals("1"))
							isPassFail="P";
					}
					else if(isPassFail.equals(""))
						isPassFail = null;
					System.out.println("isPassFail : "+isPassFail);
					teacherAssessmentStatus.setPass(isPassFail);	
				}
				teacherAssessmentStatus.setCgUpdated(true);
				teacherAssessmentStatus.setAssessmentCompletedDateTime(assessmentDateTime);
				TeacherAssessmentStatusDAO teacherAssessmentStatusDAO =  reportService.getTeacherAssessmentStatusDAO();
				teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);
				
				if(teacherAssessmentdetail.getAssessmentType()==1)
				{
					try
					{
						if(districtMaster!=null)
						{
							NetchemiaDistrictsDAO netchemiaDistrictsDAO =  reportService.getNetchemiaDistrictsDAO();
							System.out.println("dfdfdfdf:::::::::::::::"+districtMaster.getDistrictId());
							if(netchemiaDistrictsDAO!=null)
							{
								NetchemiaDistricts netchemiaDistricts = netchemiaDistrictsDAO.findByDistrictId(districtMaster,false);
								
								if(netchemiaDistricts!=null && netchemiaDistricts.getApiURL()!=null)
								{
									String apiURL = netchemiaDistricts.getApiURL();
									if(!apiURL.equals(""))
									{
										//if(districtMaster.getDistrictId()==7800052)
										TMCommonUtil.sendComposteScoreJSON(teacherNormScore,apiURL);
										if(districtMaster.getDistributionEmail()!=null && !districtMaster.getDistributionEmail().equals(""))
										{
											try {
												DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
												dsmt.setEmailerService(reportService.getEmailerService());
												dsmt.setMailcontent(MailText.notifyDistrictDistribution(teacherDetail));
												dsmt.setMailto(districtMaster.getDistributionEmail());
												dsmt.setMailsubject("Applicant "+MailText.getTeacherName(teacherDetail)+" has complete the EPI.");
												dsmt.start();
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									}
								}
							}
							
						}/*else
						{
							DistrictMaster districtMaster=new DistrictMaster();
							districtMaster.setDistrictId(7800052);
							JobForTeacherDAO jobForTeacherDAO=reportService.getJobForTeacherDAO();
							List<JobForTeacher> jobForTeacher=jobForTeacherDAO.verifyJobByDistrictId(teacherDetail, districtMaster);
							if(jobForTeacher.size()>0)
							{
								TMCommonUtil.sendComposteScoreJSON(teacherNormScore);
							}
						}*/
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
				
				if(teacherAssessmentdetail.getAssessmentType()==4)
				{
					try
					{
						TMCommonUtil.postIPIJSON(ipiReferralURL,baseURL,teacherAssessmentStatus,teacherAssessmentStatusDAO,assessmentDomainScoreDAO,assessmentCompetencyScoreDAO);
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(teacherAssessmentdetail.getAssessmentType()==1)
		{
			List<CandidateRawScore> candidateRawScores = null; 
			candidateRawScores = candidateRawScoreDAO.findTeacherCopyInCadidateRawData(teacherDetail);
			if(candidateRawScores.size()==0)
			{
				try {
					Map<String, Integer> uIdMap = new HashMap<String, Integer>();
					List<String> uniqueQuestionsIdList = teacherAssessmentQuestionDAO.findUniqueQuestionUId(); 
					int i=0;
					for (String string : uniqueQuestionsIdList) {
						++i;
						uIdMap.put(string, i);
					}
	
					List<TeacherAnswerDetail> teacherAnswerDetails = null;
					teacherAnswerDetails = teacherAnswerDetailDAO.findTeacherEPIQuestions(teacherDetail);
	
					CandidateRawScore candidateRawScore = new CandidateRawScore();
					TeacherAssessmentQuestion teacherAssessmentQuestion = null;  
					for (TeacherAnswerDetail teacherAnswerDetail : teacherAnswerDetails) {
	
						teacherAssessmentQuestion = teacherAnswerDetail.getTeacherAssessmentQuestion();
						
						Integer sid = uIdMap.get(teacherAssessmentQuestion.getQuestionUId());
						if(sid!=null && teacherAssessmentQuestion.getQuestionWeightage()>0)
							PropertyUtils.setProperty(candidateRawScore, "q"+sid,teacherAnswerDetail.getTotalScore());
					}
	
					candidateRawScore.setCreatedDateTime(new Date());
					candidateRawScore.setTeacherDetail(teacherDetail);
	
					candidateRawScoreDAO.makePersistent(candidateRawScore);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
