package tm.services;

import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.master.PortfolioItem;
import tm.dao.master.PortfolioitemDAO;

public class ManagePortfolioItemAjax {
	
	@Autowired
	private PortfolioitemDAO portfolioitemDAO;
	public void setPortfolioitemDAO(PortfolioitemDAO portfolioitemDAO) {
		this.portfolioitemDAO = portfolioitemDAO;
	}
	public String getItemList(){
		return null;
	}
	
	public Integer getItemQuantityRequiredStatus(Integer itemId){
		Integer status=0;
		try{
			PortfolioItem portfolioItem=portfolioitemDAO.findById(itemId, false, false);
			if(portfolioItem!=null){
				status=portfolioItem.getQuantityRequired();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
}
