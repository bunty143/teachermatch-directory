package tm.services.report;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherVideoLink;
import tm.bean.districtassessment.AssessmentNotesHistory;
import tm.bean.districtassessment.TeacherDistrictAssessmentAnswerDetail;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.DegreeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.DistrictSpecificPortfolioAnswers;
import tm.bean.master.RegionMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.UniversityMaster;
import tm.bean.teacher.OnlineActivityAnswers;
import tm.services.AdminDashboardAjax;
import tm.services.BatchJobOrdersAjax;
import tm.services.ExamAjax;
import tm.services.ManageJobOrdersAjax;
import tm.services.ONRAjax;
import tm.services.TeacherInfoAjax;
import tm.services.teacher.DWRAutoComplete;
import tm.services.teacher.OnlineActivityAjax;
import tm.services.teacher.PFCertifications;
import tm.services.teacher.SchoolSelectionAjax;
import tm.utility.TestTool;
import tm.utility.Utility;

public class CGServiceAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);	
	
	@Autowired
	private ExamAjax examAjax;
	
	@Autowired
	private TeacherInfoAjax teacherInfoAjax;
	@Autowired
	private SchoolSelectionAjax schoolSelectionAjax;
	
	@Autowired
	private PFCertifications pFCertifications;
	
	@Autowired
	private ONRAjax onrAjax;
	
	@Autowired
	private BatchJobOrdersAjax batchJobOrdersAjax;
	
	@Autowired
	private DWRAutoComplete dWRAutoComplete;
	
	@Autowired
	private CandidateGridAjax candidateGridAjax;
	
	@Autowired
	private TeacherSendMessageAjax teacherSendMessageAjax;
	
	@Autowired
	private AdminDashboardAjax adminDashboardAjax;
	
	@Autowired
	private ManageJobOrdersAjax manageJobOrdersAjax;
	
	@Autowired
	private	CandidateGridSubAjax candidateGridSubAjax;
	
	@Autowired
	private OnlineActivityAjax onlineActivityAjax;
	
	public String getJobListByTeacher(int teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,boolean showCommunication)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		return getJobListByTeacherByProfile(teacherId,noOfRow,pageNo,sortOrder,sortOrderType,showCommunication,"",0,0,null);
	}
	
	public String getJobListByTeacherByProfile(int teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,boolean showCommunication,String visitLocation,int jobCategoryId,int subjectId,String zoneIdd)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		return teacherInfoAjax.getJobListByTeacherByProfile(teacherId,noOfRow, pageNo,sortOrder,sortOrderType,showCommunication,visitLocation,jobCategoryId,subjectId,zoneIdd);
	}
	
	public String getJobDescription(int jobId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		return teacherInfoAjax.getJobDescription(jobId);
	}
	
	public Integer updateTFAByUser(String teacherId,String tfaId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		return teacherInfoAjax.updateTFAByUser(teacherId, tfaId);
	}
	
	public Integer displayselectedTFA(String teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		return teacherInfoAjax.displayselectedTFA(teacherId);
	}
	
	
	public String getOnlineActivityData(Integer teacherId,Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		return teacherInfoAjax.getOnlineActivityData(teacherId,districtId);
	}
	
	public String changeExpiryActivityStatus(Integer OnlineActivityId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		System.out.println(" changeExpiryActivityStatus >>>>>>>>>>>>>>>>> ");
		return teacherInfoAjax.changeExpiryActivityStatus(OnlineActivityId);
	}
	
	public String downloadReferenceForCandidate(Integer referenceId,Integer teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		return pFCertifications.downloadReferenceForCandidate(referenceId, teacherId);
	}
	
	public String downloadUploadedDocumentForCg(Integer additionDocumentId,Integer teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		return pFCertifications.downloadUploadedDocumentForCg(additionDocumentId, teacherId);
	}
	
	public String findDuplicateReferences(TeacherElectronicReferences teacherElectronicReferences, Integer teacherId)
	{
		return pFCertifications.findDuplicateReferences(teacherElectronicReferences, teacherId);
	}
	
	public String saveUpdateElectronicReferences(TeacherElectronicReferences teacherElectronicReferences, Integer teacherId)
	{
		return pFCertifications.saveUpdateElectronicReferences(teacherElectronicReferences, teacherId);
	}
	
	public Boolean removeReferencesForNoble(Integer referenceId,Integer teacherId)
	{
		return pFCertifications.removeReferencesForNoble(referenceId, teacherId);
	}
	
	public Boolean changeStatusElectronicReferences(String id,Integer status)
	{
		return pFCertifications.changeStatusElectronicReferences(id, status);
	}
	
	public String saveOrUpdateVideoLink(TeacherVideoLink teacherVideoLink,Integer teacherId)
	{
		return pFCertifications.saveOrUpdateVideoLink(teacherVideoLink, teacherId);
	}
	
	public String getVideoPlayDivForProfile(Integer videoLinkId,Integer teacherId)
	{
		return pFCertifications.getVideoPlayDivForProfile(videoLinkId,teacherId);
	}
	
	public TeacherVideoLink editVideoLink(String id)
	{
		return pFCertifications.editVideoLink(id);
	}
	
	public Boolean deleteVIdeoLink(String id)
	{
		return pFCertifications.deleteVIdeoLink(id);
	}
	
	public TeacherElectronicReferences editElectronicReferences(String id)
	{
		return pFCertifications.editElectronicReferences(id);
	}
	public boolean saveGraduationDate(Integer academicId,String graduationdate) 
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
	    return onrAjax.saveGraduationDate(academicId, graduationdate);
	}
	
	public boolean canleGraduationDate(Integer academicId) 
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
	    return onrAjax.canleGraduationDate(academicId);
	}
	
	public List<DistrictMaster> getFieldOfDistrictList(String DistrictName)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
	    return batchJobOrdersAjax.getFieldOfDistrictList(DistrictName);
	}
	
	public List<SchoolMaster> getFieldOfSchoolList(String fieldOfStudy)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
	    return batchJobOrdersAjax.getFieldOfSchoolList(fieldOfStudy);
	}
	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
	{
		return batchJobOrdersAjax.getFieldOfSchoolList(districtIdForSchool,SchoolName);
	}
	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName,String all){
		return batchJobOrdersAjax.getFieldOfSchoolList(districtIdForSchool,SchoolName,all);
	}
	public List<UniversityMaster> getUniversityMasterList(String universityName)
	{
		return dWRAutoComplete.getUniversityMasterList(universityName);
	}
	
	public List<RegionMaster> getRegionMasterList(String regionName)
	{
		return dWRAutoComplete.getRegionMasterList(regionName);
	}
	
	public List<DegreeMaster> getDegreeMasterList(String degreeName)
	{
		return dWRAutoComplete.getDegreeMasterList(degreeName);
	}
	
	public String getSubjectByDistrict(Integer districtId)
	{
		return dWRAutoComplete.getSubjectByDistrict(districtId);
	}
	public String getJobCategoryByDistrict(Integer districtId)
	{
		return dWRAutoComplete.getJobCategoryByDistrict(districtId);
	}
	
	public List<CertificateTypeMaster> getFilterCertificateTypeList(String certType,String stateId)
	{
		return dWRAutoComplete.getFilterCertificateTypeList(certType,stateId);
	}
	
	public String downloadGeneralKnowledge(Integer generalKnowledgeExamId,Integer teacherId)
	{
		return candidateGridAjax.downloadGeneralKnowledge(generalKnowledgeExamId, teacherId);
	}
	
	public String downloadSubjectAreaExam(Integer subjectAreaExamId,Integer teacherId)
	{
		return candidateGridAjax.downloadSubjectAreaExam(subjectAreaExamId, teacherId);
	}
	
	public String getCoverLetter(String teacherId, String jobId)
	{
		return candidateGridAjax.getCoverLetter(teacherId, jobId);
	}
	
	public String getTranscriptGrid(String teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		return candidateGridAjax.getTranscriptGrid(teacherId, noOfRow, pageNo, sortOrder, sortOrderType);
	}
	
	public String getCertificationGrid(int teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		return candidateGridAjax.getCertificationGrid(teacherId, noOfRow, pageNo, sortOrder, sortOrderType);
	}
	
	public String downloadTranscript(String academicId)
	{
		return candidateGridAjax.downloadTranscript(academicId);
	}
	
	public String getPhoneDetail(String teacherId){
		return candidateGridAjax.getPhoneDetail(teacherId);
	}
	
	public String downloadResume(String teacherId)
	{
		return candidateGridAjax.downloadResume(teacherId);
	}
	
	public String downloadPortfolioReport(String teacherId)
	{
		return candidateGridAjax.downloadPortfolioReport(teacherId);
	}
	
	public String checkBaseCompeleted(Integer teacher)
	{
		return candidateGridAjax.checkBaseCompeleted(teacher);
	}
	public String getIncompleteInfoHtml(Integer jobForTeacherId)
	{
		return candidateGridAjax.getIncompleteInfoHtml(jobForTeacherId);
	}
	public String generatePDReport(Integer teacher)
	{
		return candidateGridAjax.generatePDReport(teacher);
	}
	// Add PDP Configuration 
	public String generatePDPReportForMultipleJobApplyDistrict(String districtId ,Integer teacher)
	{
		return candidateGridAjax.generatePDPReportForMultipleJobApplyDistrict(districtId,teacher);
	}
	
	public Boolean saveMessage(Long[] jobForTeacherIds,String messageSubject,String messageSend,int jobId,String fileName,int msgType,String document)
	{
		return teacherSendMessageAjax.saveMessage(jobForTeacherIds, messageSubject, messageSend, jobId, fileName, msgType, document);
	}
	
	public String getEmailAddress(Long[] jobForTeacherIds)
	{
		return teacherSendMessageAjax.getEmailAddress(jobForTeacherIds);
	}
	
	public String getTeacherIds(Long[] jobForTeacherIds)
	{
		return teacherSendMessageAjax.getTeacherIds(jobForTeacherIds);
	}
	
	
	public String getSlider(String Id,String name,String tickInterval,String max,String swidth,String svalue,String style)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String sReturnValue="<iframe id='"+Id+"'  src='slideract.do?name="+name+"&tickInterval="+tickInterval+"&max="+max+"&swidth="+swidth+"&svalue="+svalue+"' scrolling='no' frameBorder='0' style='"+style+"'></iframe>";
		return sReturnValue;
	}
	
	public String getiFrameForTree()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String sReturnValue="<iframe id='iframeSaveCandidate'  src='tree.do?pageFlag=0' class='pull-left' scrolling='auto'  frameBorder='1' style='border: 0px; padding: 0px; margin: 0px; overflow:visible; text-align: top; vertical-align: top;width:220px;height:420px;'></iframe>";
		return sReturnValue;
	}
	
	synchronized public String getBreakUpData(int index,String sessionListName)
	{
		TestTool.getTraceTime("300");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		List<String> lstAbvGrdData=new ArrayList<String>();
		//lstAbvGrdData=(List<String>)session.getAttribute("lstAbvGrdData");
		lstAbvGrdData=(List<String>)session.getAttribute(sessionListName);
		System.out.println("lstAbvGrdData size "+lstAbvGrdData.size()+" Calling index "+index);
		String sReturnValue=lstAbvGrdData.get(index);
		//System.out.println("index "+index+" sReturnValue "+sReturnValue);
		TestTool.getTraceTime("310");
		return sReturnValue;
	}
	
	@Transactional(readOnly=false)
	public String makeEPIIncomplete(TeacherDetail teacherDetail,String reason)
	{
		return adminDashboardAjax.makeEPIIncomplete(teacherDetail,reason);
	}
	
	public String getZoneListAll(int districtId)
	{
		return manageJobOrdersAjax.getZoneListAll(districtId);
	}
	
	public String displayGeoZoneSchoolGrid(Integer geozoneId,Integer districtIdShow,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		return manageJobOrdersAjax.displayGeoZoneSchoolGrid(geozoneId, districtIdShow, noOfRow, pageNo, sortOrder, sortOrderType);
	}
	
	public String getDistrictQPortfoliouestions(Integer jobId,Integer teacherId)
	{
		return candidateGridSubAjax.getDistrictSpecificPortfolioQuestions(jobId, teacherId);
	}
	
	public String setDistrictQPortfoliouestions(DistrictSpecificPortfolioAnswers[] districtSpecificPortfolioAnswersList,int dspqType,int teacherId)
	{
		return candidateGridSubAjax.setDistrictQPortfoliouestions(districtSpecificPortfolioAnswersList, dspqType, teacherId);
	}
	public String getOnlineActivityQuestionsForCG(Integer teacherId,Integer jobId,Integer districtId,Integer questionSetId)
	{
		return onlineActivityAjax.getOnlineActivityQuestionsForCG(teacherId,jobId,districtId,questionSetId);
	}
	public String saveOnlineActivity(OnlineActivityAnswers[] onlineActivityAnswersList,Integer teacherId,Integer jobId,Integer districtId,Integer questionSetId,boolean errorCount,String onlineActivityNote)
	{
		return onlineActivityAjax.saveOnlineActivity(onlineActivityAnswersList,teacherId,jobId,districtId,questionSetId,errorCount,onlineActivityNote);
	}
	public String displayOnlineActivityForCG(Integer teacherId,Integer jobId,Integer districtId)
	{
		return onlineActivityAjax.displayOnlineActivityForCG(teacherId,jobId,districtId);
	}
	public String displayMultipleOnlineActivityForCG(String teacherIds,Integer jobId,Integer districtId)
	{
		return onlineActivityAjax.displayMultipleOnlineActivityForCG(teacherIds,jobId,districtId);
	}
	public String getSelectedSchoolsList(Integer jobId,Integer teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		return schoolSelectionAjax.getSelectedSchoolsList(jobId, teacherId, noOfRow, pageNo, sortOrder, sortOrderType);
	}
	public String showLetterOfIntentForCG(Integer jobId,Integer schoolId,Integer teacherId)
	{
		return schoolSelectionAjax.showLetterOfIntentForCG(jobId, schoolId,teacherId);
	}
	public String getDistrictAssessmentDetails(Integer teacherId,Integer jobId)
	{
		return examAjax.getDistrictAssessmentDetails(teacherId, jobId);
	}
	public String saveAssessmentInvite(Integer teacherId,Integer jobId,Integer districtAssessmentId,Integer centerId)
	{
		return examAjax.saveAssessmentInvite(teacherId,jobId,districtAssessmentId,centerId);
	}
	public String saveTags(Integer districtId,Integer teacherId,String tagsId, String teacherIds)
	{
		return teacherInfoAjax.saveTags(districtId, teacherId, tagsId, teacherIds);
	}
	public String getAssessmentInviteQuestionAnswers(Integer teacherId,Integer jobId,Integer districtAssessmentId)
	{
		return examAjax.getAssessmentInviteQuestionAnswers(teacherId, jobId,districtAssessmentId);
	}
	public String getAssessmentDetails(Integer districtAssessmentId, Integer centerId)
	{
		return examAjax.getAssessmentDetails(districtAssessmentId,centerId);
	}
	public String sendAssessmentInvite(Integer teacherId, Integer jobId,Integer districtAssessmentId)
	{
		return examAjax.sendAssessmentInvite(teacherId,jobId,districtAssessmentId);
	}
	public JSONArray[] displayExamCalendar(Integer districtAssessmentId, Integer centerId,Integer teacherId,Integer jobId, String startDate)
	{
		return examAjax.displayExamCalendar(districtAssessmentId,centerId,teacherId,jobId,startDate);
	}
	public String showAssessmentDetails(Integer teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		return examAjax.showAssessmentDetails(teacherId,noOfRow, pageNo,sortOrder,sortOrderType);
	}
	public String getColorAndEvent(Integer districtAssessmentId, Integer centerId,Integer teacherId,Integer jobId)
	{
		return examAjax.getColorAndEvent(districtAssessmentId,centerId,teacherId,jobId);
	}
	public String getDisabledSlotIds()
	{
		return examAjax.getDisabledSlotIds(); 
	}
	public String getStartEndDateByAssessment(Integer centerId,Integer districtAssessmentId,Integer teacherId)
	{
		return examAjax.getStartEndDateByAssessment(centerId,districtAssessmentId,teacherId); 
	}
	public String getMultipleInviteAssessment(String teacherIds)
	{
		return examAjax.getMultipleInviteAssessment(teacherIds); 
	}
	public String sendMultipleAssessmentInvite(String teacherIds,Integer jobId,Integer districtAssessmentId)
	{
		return examAjax.sendMultipleAssessmentInvite(teacherIds,jobId,districtAssessmentId);
	}
	public String getScoreForQuestions(TeacherDistrictAssessmentAnswerDetail[] teacherDistrictAssessmentAnswerDetails, Integer teacherId,AssessmentNotesHistory[] assessmentNotesHistories)
	{
		return examAjax.getScoreForQuestions(teacherDistrictAssessmentAnswerDetails,teacherId,assessmentNotesHistories);
	}
	public String getAvailability(String centerScheduleId)
	{
		return examAjax.getAvailability(centerScheduleId);
	}
	public String displayOnlineActivityNotesForCG(Integer teacherId,Integer jobId,Integer districtId,Integer questionSetIdForCg)
	{
		return onlineActivityAjax.displayOnlineActivityNotesForCG(teacherId,jobId,districtId,questionSetIdForCg);
	}
	public String getJobCategoryByHQAndBranch(Integer branchId,Integer headQuarterId)
	{
		return dWRAutoComplete.getJobCategoryByHQAndBranch(branchId,headQuarterId);
	}
	public String getJobSubCategoryByCategory(String jobCategoryIds)
	{
		return dWRAutoComplete.getJobSubCategoryByCategory(jobCategoryIds);
	}
	public String checkSpCompleted(Integer teacher)
	{
		return candidateGridAjax.checkSpCompleted(teacher);
	}
	public String makeSpIncomplete(TeacherDetail teacherDetail, String spResetMsg)
	{
		return adminDashboardAjax.makeSpIncomplete(teacherDetail,spResetMsg);
	}
	public String checkAssessmentCompleted(Integer teacher, Integer assessmentType, Integer teacherAssessmentStatusId)
	{
		return candidateGridAjax.checkAssessmentCompleted(teacher,assessmentType,teacherAssessmentStatusId);
	}
	public String makeAssessmentIncomplete(TeacherDetail teacherDetail, Integer assessmentType, Integer teacherAssessmentStatusId, String resetMsg)
	{
		return adminDashboardAjax.makeAssessmentIncomplete(teacherDetail,assessmentType,teacherAssessmentStatusId,resetMsg);
	}
	public String hireVueIntegration(Long jobForTeacherId)
	{
		return candidateGridAjax.hireVueIntegration(jobForTeacherId);
	}
	public String addACandidateToAPosition(String posId, Long jobForTeacherId)
	{
		return candidateGridAjax.addACandidateToAPosition(posId, jobForTeacherId);
	}
}