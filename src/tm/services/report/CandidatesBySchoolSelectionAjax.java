package tm.services.report;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import tm.bean.JobForTeacher;
import tm.bean.SchoolSelectedByCandidate;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.SchoolSelectedByCandidateDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.services.PaginationAndSorting;
import tm.utility.Utility;

public class CandidatesBySchoolSelectionAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	String lblCandidateName=Utility.getLocaleValuePropByKey("lblCandidateName", locale);
	String lblSchoolSelected=Utility.getLocaleValuePropByKey("lblSchoolSelected", locale);
	String lblDateSelected1=Utility.getLocaleValuePropByKey("lblDateSelected1", locale);
	String lblNoTeacher=Utility.getLocaleValuePropByKey("lblNoTeacher", locale);
	String msgSchlSlctByCandRept=Utility.getLocaleValuePropByKey("msgSchlSlctByCandRept", locale);
	String msgNorecordfound=Utility.getLocaleValuePropByKey("msgNorecordfound", locale);
	
	
	
	@Autowired
	private SchoolSelectedByCandidateDAO schoolSelectedByCandidateDAO;
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	private Order sortOrderGlobal = null;

	public String displaySelectedSchoolCandidates(String districtId,String schoolId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		System.out.println("================== display School Selected By Candidate Report Grid==========");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer tmRecords = new StringBuffer();
		List<SchoolSelectedByCandidate> schoolSelectedByCandidates = new ArrayList<SchoolSelectedByCandidate>();
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		DistrictMaster districtMaster =null;
		SchoolMaster schoolMaster = null;
		if(session == null || session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			System.out.println(" District Id :: "+districtId+" SchoolId :: "+schoolId);
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"lastName";
			String sortOrderNoField		=	"lastName";
			int checkShortOrder=0;
			if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("lastName"))
				checkShortOrder=1;
			else if(sortOrder.equalsIgnoreCase("schoolName")){
				checkShortOrder=2;
			}
			else if(sortOrder.equalsIgnoreCase("schoolSelectionDate")){
				checkShortOrder=3;
			}

			System.out.println("noofRow :: "+noOfRow+" pageNo :: "+sortOrder+" sortOrderType :: "+sortOrderType);

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			Boolean flag = false;
			if(sortOrder!=null){	
				flag = true;
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("lastName"))
				{
					sortOrderNoField="lastName";
				}
				if(sortOrder.equals("schoolName"))
				{
					sortOrderNoField="schoolName";
				}
				if(sortOrder.equals("schoolSelectionDate"))
				{
					sortOrderNoField="schoolSelectionDate";
				}
				if(sortOrder.equals("firstName"))
				{
					sortOrderNoField="firstName";
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("")){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			System.out.println("rows :: "+noOfRowInPage+" pgNo :: "+pgNo+" start :: "+start+" end :: "+end);	

			if(userMaster!=null)
			{	
				if(userMaster.getEntityType()==2)
				{
					if(districtId.length()>0 && schoolId.length()==0)
					{
						System.out.println(" District Search");
						districtMaster=districtMasterDAO.findByDistrictId(districtId);
						schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,start,end,flag);
						totalRecord = schoolSelectedByCandidateDAO.getTotalSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,start,end);
						sortOrderGlobal = sortOrderStrVal;
					}
					else if(districtId.length()>0 && schoolId.length()>0)
					{
						System.out.println(" District & School Search");
						districtMaster=districtMasterDAO.findByDistrictId(districtId);
						schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId), false, false);
						schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidatesSchools(districtMaster,schoolMaster,checkShortOrder,sortOrderStrVal,start,end,flag);
						sortOrderGlobal = sortOrderStrVal;
						totalRecord = schoolSelectedByCandidates.size();
					}
				}
				else if(userMaster.getEntityType()==1)
				{
					if(districtId.length()==0 && schoolId.length()==0)
					{
						System.out.println("Initial Search");
						districtMaster=districtMasterDAO.findByDistrictId(districtId);
						schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,start,end,flag);
						sortOrderGlobal = sortOrderStrVal;
						totalRecord = schoolSelectedByCandidateDAO.getTotalSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,start,end);
					}
					else if(districtId.length()>0 && schoolId.length()==0)
					{
						System.out.println(" District Search");
						districtMaster=districtMasterDAO.findByDistrictId(districtId);
						schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,start,end,flag);
						sortOrderGlobal = sortOrderStrVal;
						totalRecord = schoolSelectedByCandidateDAO.getTotalSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,start,end);
					}
					else if(districtId.length()>0 && schoolId.length()>0)
					{
						System.out.println(" District & School Search");
						districtMaster=districtMasterDAO.findByDistrictId(districtId);
						schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId), false, false);
						schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidatesSchools(districtMaster,schoolMaster,checkShortOrder,sortOrderStrVal,start,end,flag);
						sortOrderGlobal = sortOrderStrVal;
						totalRecord = schoolSelectedByCandidates.size();
					}
				}
			}
			if(totalRecord<end)
				end=totalRecord;

			tmRecords.append("<table id='selectedCandidateTable' width='100%' class='tablecgtbl'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblCandidateName,sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblSchoolSelected,sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);		
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblDateSelected1,sortOrderFieldName,"schoolSelectionDate",sortOrderTypeVal,pgNo);		
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			int counter=0 ;
			if(schoolSelectedByCandidates!=null && schoolSelectedByCandidates.size()>0)
			{
				for(SchoolSelectedByCandidate candidate : schoolSelectedByCandidates)
				{
					counter++;
					String gridColor="class='bggrid'";
					if(counter%2==0){
						gridColor="style='background-color:white;'";
					}
					tmRecords.append("<tr "+gridColor+">");
					tmRecords.append("<td>"+candidate.getTeacherDetail().getFirstName()+" "+candidate.getTeacherDetail().getLastName()+"<br/>\""+candidate.getTeacherDetail().getEmailAddress()+"\""+"</td>");
					tmRecords.append("<td>"+candidate.getSchoolMaster().getSchoolName()+"</td>");
					tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(candidate.getSchoolSelectionDate())+"</td>");
					tmRecords.append("</tr>");
				}
			}
			else
			{
				tmRecords.append("<tr><td colspan='6'>"+lblNoTeacher+"</td></tr>" );
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForTeacherInfoAjax(request,totalRecord,noOfRow, pageNo));
		}
		return tmRecords.toString();
	}


	public String generateSelectedSchoolByCandidateExcel(String districtId,String schoolId,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = "";
		try
		{
			System.out.println(" District Id :: "+districtId+" SchoolId :: "+schoolId);
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"lastName";
			String sortOrderNoField		=	"lastName";
			int checkShortOrder=0;
			if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("lastName"))
				checkShortOrder=1;
			else if(sortOrder.equalsIgnoreCase("schoolName")){
				checkShortOrder=2;
			}
			else if(sortOrder.equalsIgnoreCase("schoolSelectionDate")){
				checkShortOrder=3;
			}

			System.out.println(" sortOrder :: "+sortOrder+" sortOrderType :: "+sortOrderType);

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			Boolean flag = false;
			if(sortOrder!=null){	
				//flag = true;
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("lastName"))
				{
					sortOrderNoField="lastName";
				}
				if(sortOrder.equals("schoolName"))
				{
					sortOrderNoField="schoolName";
				}
				if(sortOrder.equals("schoolSelectionDate"))
				{
					sortOrderNoField="schoolSelectionDate";
				}
				if(sortOrder.equals("firstName"))
				{
					sortOrderNoField="firstName";
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("")){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			List<SchoolSelectedByCandidate> schoolSelectedByCandidates = new ArrayList<SchoolSelectedByCandidate>();
			UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
			DistrictMaster districtMaster =null;
			SchoolMaster schoolMaster = null;
			if(userMaster!=null)
			{	
				System.out.println(" sortOrderGlobal :: "+sortOrderGlobal);
				if(userMaster.getEntityType()==2)
				{
					if(districtId.length()>0 && schoolId.length()==0)
					{
						System.out.println(" District Search");
						districtMaster=districtMasterDAO.findByDistrictId(districtId);
						schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
					}
					else if(districtId.length()>0 && schoolId.length()>0)
					{
						System.out.println(" District & School Search");
						districtMaster=districtMasterDAO.findByDistrictId(districtId);
						schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId), false, false);
						schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidatesSchools(districtMaster,schoolMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
					}
				}
				else if(userMaster.getEntityType()==1)
				{
					if(districtId.length()==0 && schoolId.length()==0)
					{
						System.out.println("Initial Search");
						districtMaster=districtMasterDAO.findByDistrictId(districtId);
						schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
					}
					else if(districtId.length()>0 && schoolId.length()==0)
					{
						System.out.println(" District Search");
						districtMaster=districtMasterDAO.findByDistrictId(districtId);
						schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
					}
					else if(districtId.length()>0 && schoolId.length()>0)
					{
						System.out.println(" District & School Search");
						districtMaster=districtMasterDAO.findByDistrictId(districtId);
						schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId), false, false);
						schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidatesSchools(districtMaster,schoolMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
					}
				}
			}

			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			//String basePath = request.getRealPath("/")+"/selectedcandidate";
			String basePath = request.getSession().getServletContext().getRealPath ("/")+"/schoolselectedbycandidatereport";
			System.out.println("File Path :: "+basePath);
			fileName ="schoolselectedbycandidate"+time+".xls";
			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();
			Utility.deleteAllFileFromDir(basePath);
			file = new File(basePath+"/"+fileName);
			WorkbookSettings wbSettings = new WorkbookSettings();
			wbSettings.setLocale(new Locale("en", "EN"));
			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;
			WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
			workbook.createSheet(" schoolselectedbycandidate", 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);
			// Create create a bold font with unterlines
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 15, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);
			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);
			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			header.setBackground(Colour.GRAY_25);
			// Write a few headers
			excelSheet.mergeCells(0, 0, 2, 1);
			Label label;
			label = new Label(0, 0, "School Selected By Candidate Report", timesBoldUnderline);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 3,2, 3);
			label = new Label(0, 3, "");
			excelSheet.addCell(label);
			excelSheet.getSettings().setDefaultColumnWidth(25);
			int k=4;
			int col=1;
			label = new Label(0, k, "Candidate Name",header); 
			excelSheet.addCell(label);
			label = new Label(1, k, "School Selected",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k, "Date Selected",header); 
			excelSheet.addCell(label);
			k=k+1;
			if(schoolSelectedByCandidates.size()==0)
			{	
				excelSheet.mergeCells(0, k, 8, k);
				label = new Label(0, k, "No Record Found");
				excelSheet.addCell(label);
			}
			HashMap<String, WritableCellFormat> mapColors= new HashMap<String, WritableCellFormat>();


			if (schoolSelectedByCandidates.size()>0) {
				WritableCellFormat cellFormat = new WritableCellFormat();
				Color color = Color.decode("0xFF0000");
				int red = color.getRed();
				int blue = color.getBlue();
				int green = color.getGreen();
				workbook.setColourRGB(Colour.AQUA, red, green, blue);
				cellFormat.setBackground(Colour.AQUA);
				mapColors.put("FF0000", cellFormat);
				WritableCellFormat cellFormat1 = new WritableCellFormat();
				Color color1 = Color.decode("0xFF6666");
				workbook.setColourRGB(Colour.BLUE, color1.getRed(), color1
						.getGreen(), color1.getBlue());
				cellFormat1.setBackground(Colour.BLUE);
				mapColors.put("FF6666", cellFormat1);
				WritableCellFormat cellFormat2 = new WritableCellFormat();
				Color color2 = Color.decode("0xFFCCCC");
				workbook.setColourRGB(Colour.BROWN, color2.getRed(), color2
						.getGreen(), color2.getBlue());
				cellFormat2.setBackground(Colour.BROWN);
				mapColors.put("FFCCCC", cellFormat2);
				WritableCellFormat cellFormat3 = new WritableCellFormat();
				Color color3 = Color.decode("0xFF9933");
				workbook.setColourRGB(Colour.CORAL, color3.getRed(), color3
						.getGreen(), color3.getBlue());
				cellFormat3.setBackground(Colour.CORAL);
				mapColors.put("FF9933", cellFormat3);
				
				for(SchoolSelectedByCandidate ssbc:schoolSelectedByCandidates) 
				{
					col=1;
					String name=ssbc.getTeacherDetail().getFirstName()+" "+ssbc.getTeacherDetail().getLastName()+"("+ssbc.getTeacherDetail().getEmailAddress()+")";
					label = new Label(0, k, name); 
					excelSheet.addCell(label);

					label = new Label(1, k, ssbc.getSchoolMaster().getSchoolName()); 
					excelSheet.addCell(label);

					label = new Label(++col, k, Utility.convertDateAndTimeToUSformatOnlyDate(ssbc.getSchoolSelectionDate())); 
					excelSheet.addCell(label);
					++k;
				}
			}
			workbook.write();
			workbook.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}


	public String generateSelectedSchoolByCandidateReportPdf(String districtId, String schoolId,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Font font8 = null;
		Font font8Green = null;
		Font font8bold = null;
		Font font9 = null;
		Font font9bold = null;
		Font font10 = null;
		Font font10_10 = null;
		Font font10bold = null;
		Font font11 = null;
		Font font11bold = null;
		Font font20bold = null;
		Font font11b   =null;
		Color bluecolor =null;
		Font font8bold_new = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			try {

				System.out.println(" District Id :: "+districtId+" SchoolId :: "+schoolId);
				/** set default sorting fieldName **/
				String sortOrderFieldName	=	"lastName";
				String sortOrderNoField		=	"lastName";
				int checkShortOrder=0;
				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("lastName"))
					checkShortOrder=1;
				else if(sortOrder.equalsIgnoreCase("schoolName")){
					checkShortOrder=2;
				}
				else if(sortOrder.equalsIgnoreCase("schoolSelectionDate")){
					checkShortOrder=3;
				}

				System.out.println(" sortOrder :: "+sortOrder+" sortOrderType :: "+sortOrderType);

				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal=null;
				Boolean flag = false;
				if(sortOrder!=null){	
					//flag = true;
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
					if(sortOrder.equals("lastName"))
					{
						sortOrderNoField="lastName";
					}
					if(sortOrder.equals("schoolName"))
					{
						sortOrderNoField="schoolName";
					}
					if(sortOrder.equals("schoolSelectionDate"))
					{
						sortOrderNoField="schoolSelectionDate";
					}
					if(sortOrder.equals("firstName"))
					{
						sortOrderNoField="firstName";
					}
				}
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("")){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				String fontPath = request.getRealPath("/");
				BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				int userId = 0;
				UserMaster userMaster = null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else
					userMaster = (UserMaster)session.getAttribute("userMaster");
				userId = userMaster.getUserId();
				String time = String.valueOf(System.currentTimeMillis()).substring(6);
				String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
				String fileName =time+"Report.pdf";

				File file = new File(basePath);
				if(!file.exists())
					file.mkdirs();
				Utility.deleteAllFileFromDir(basePath);
				System.out.println("user/"+userId+"/"+fileName);
				BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
				font8 = new Font(tahoma, 8);
				font8Green = new Font(tahoma, 8);
				font8Green.setColor(Color.BLUE);
				font8bold = new Font(tahoma, 8, Font.NORMAL);
				font8bold_new = new Font(tahoma, 7, Font.NORMAL);
				font9 = new Font(tahoma, 9);
				font9bold = new Font(tahoma, 9, Font.BOLD);
				font10 = new Font(tahoma, 10);
				font10_10 = new Font(tahoma, 8);
				font10bold = new Font(tahoma, 10, Font.BOLD);
				font11 = new Font(tahoma, 11);
				font11bold = new Font(tahoma, 11,Font.BOLD);
				bluecolor =  new Color(0,122,180); 
				font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
				font20bold.setColor(Color.BLUE);
				font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);
				document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
				document.addAuthor("TeacherMatch");
				document.addCreator("TeacherMatch Inc.");
				document.addSubject("Selected School By Candidate Report");
				document.addCreationDate();
				document.addTitle("Selected School By Candidate Report");
				fos = new FileOutputStream(basePath+"/"+fileName);
				PdfWriter.getInstance(document, fos);
				document.open();
				PdfPTable mainTable = new PdfPTable(1);
				mainTable.setWidthPercentage(100);
				Paragraph [] para = null;
				PdfPCell [] cell = null;
				para = new Paragraph[3];
				cell = new PdfPCell[3];
				//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
				Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
				logo.scalePercent(75);
				document.add(new Phrase("\n"));
				cell[0]= new PdfPCell(logo);
				cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[0].setBorder(0);
				mainTable.addCell(cell[0]);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				mainTable.addCell(cell[1]);
				document.add(new Phrase("\n"));
				document.add(mainTable);
				document.add(new Phrase("\n"));
				float[] tblwidthz={.15f};
				mainTable = new PdfPTable(tblwidthz);
				mainTable.setWidthPercentage(100);
				para = new Paragraph[1];
				cell = new PdfPCell[1];
				para[0] = new Paragraph("Selected School By Candidate Report",font20bold);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[0].setBorder(0);
				mainTable.addCell(cell[0]);
				document.add(mainTable);
				document.add(new Phrase("\n"));
				float[] tblwidths={.15f,.20f};
				mainTable = new PdfPTable(tblwidths);
				mainTable.setWidthPercentage(100);
				para = new Paragraph[2];
				cell = new PdfPCell[2];
				String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
				para[0] = new Paragraph("Created By: "+name1,font10);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[0].setBorder(0);
				mainTable.addCell(cell[0]);
				para[1] = new Paragraph(""+"Created on: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell[1].setBorder(0);
				mainTable.addCell(cell[1]);
				document.add(mainTable);
				document.add(Chunk.NEWLINE);
				float[] tblwidth={1.0f,1.0f,1.0f};

				mainTable = new PdfPTable(tblwidth);
				mainTable.setWidthPercentage(100);
				para = new Paragraph[16];
				cell = new PdfPCell[16];
				// header
				para[0] = new Paragraph("Candidate Name",font10_10);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setBackgroundColor(bluecolor);
				cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[0]);

				para[1] = new Paragraph(""+"School Selected",font10_10);
				cell[1]= new PdfPCell(para[1]);
				cell[1].setBackgroundColor(bluecolor);
				cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[1]);

				para[2] = new Paragraph(""+"Date Selected",font10_10);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setBackgroundColor(bluecolor);
				cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[2]);

				document.add(mainTable);
				List<SchoolSelectedByCandidate> schoolSelectedByCandidates = new ArrayList<SchoolSelectedByCandidate>();
				DistrictMaster districtMaster =null;
				SchoolMaster schoolMaster = null;
				if(userMaster!=null)
				{	
					System.out.println(" sortOrderGlobal :: "+sortOrderGlobal);
					//Boolean flag = false;
					if(userMaster.getEntityType()==2)
					{
						if(districtId.length()>0 && schoolId.length()==0)
						{
							System.out.println(" District Search");
							districtMaster=districtMasterDAO.findByDistrictId(districtId);
							schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
						}
						else if(districtId.length()>0 && schoolId.length()>0)
						{
							System.out.println(" District & School Search");
							districtMaster=districtMasterDAO.findByDistrictId(districtId);
							schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId), false, false);
							schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidatesSchools(districtMaster,schoolMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
						}
					}
					else if(userMaster.getEntityType()==1)
					{
						if(districtId.length()==0 && schoolId.length()==0)
						{
							System.out.println("Initial Search");
							districtMaster=districtMasterDAO.findByDistrictId(districtId);
							schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
						}
						else if(districtId.length()>0 && schoolId.length()==0)
						{
							System.out.println(" District Search");
							districtMaster=districtMasterDAO.findByDistrictId(districtId);
							schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
						}
						else if(districtId.length()>0 && schoolId.length()>0)
						{
							System.out.println(" District & School Search");
							districtMaster=districtMasterDAO.findByDistrictId(districtId);
							schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId), false, false);
							schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidatesSchools(districtMaster,schoolMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
						}
					}
				}

				if(schoolSelectedByCandidates.size()==0){
					float[] tblwidth11={.10f};
					mainTable = new PdfPTable(tblwidth11);
					mainTable.setWidthPercentage(100);
					para = new Paragraph[1];
					cell = new PdfPCell[1];
					para[0] = new Paragraph("No Record Found.",font8bold);
					cell[0]= new PdfPCell(para[0]);
					cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[0]);
					document.add(mainTable);
				}
				if(schoolSelectedByCandidates.size()>0){
					for(SchoolSelectedByCandidate ssbc : schoolSelectedByCandidates){
						int index=0;
						float[] tblwidth1={1.0f,1.0f,1.0f};
						mainTable = new PdfPTable(tblwidth1);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[3];
						cell = new PdfPCell[3];
						//Candidate Name
						String name=ssbc.getTeacherDetail().getFirstName()+" "+ssbc.getTeacherDetail().getLastName()+"\n"+"("+ssbc.getTeacherDetail().getEmailAddress()+")";
						para[index] = new Paragraph(name,font8bold_new);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						//School Name
						para[index] = new Paragraph(""+ssbc.getSchoolMaster().getSchoolName(),font8bold_new);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						//Selected Date
						para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(ssbc.getSchoolSelectionDate()),font8bold_new);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						document.add(mainTable);					 
					}			
				}	
				return "user/"+userId+"/"+fileName;
			}
			catch (DocumentException e1) 
			{
				e1.printStackTrace();
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
			}
			catch(Exception e)
			{e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
		}
		return "";
	}


	public String generateSelectedSchoolByCandidatePrintPreview(String districtId, String schoolId,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		String fileName = "";
		StringBuffer tmRecords =	new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			try
			{
				System.out.println(" District Id :: "+districtId+" SchoolId :: "+schoolId);
				/** set default sorting fieldName **/
				String sortOrderFieldName	=	"lastName";
				String sortOrderNoField		=	"lastName";
				int checkShortOrder=0;
				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("lastName"))
					checkShortOrder=1;
				else if(sortOrder.equalsIgnoreCase("schoolName")){
					checkShortOrder=2;
				}
				else if(sortOrder.equalsIgnoreCase("schoolSelectionDate")){
					checkShortOrder=3;
				}
				System.out.println(" sortOrder :: "+sortOrder+" sortOrderType :: "+sortOrderType);

				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal=null;
				Boolean flag = false;
				if(sortOrder!=null){	
					//flag = true;
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
					if(sortOrder.equals("lastName"))
					{
						sortOrderNoField="lastName";
					}
					if(sortOrder.equals("schoolName"))
					{
						sortOrderNoField="schoolName";
					}
					if(sortOrder.equals("schoolSelectionDate"))
					{
						sortOrderNoField="schoolSelectionDate";
					}
					if(sortOrder.equals("firstName"))
					{
						sortOrderNoField="firstName";
					}
				}
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("")){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				List<SchoolSelectedByCandidate> schoolSelectedByCandidates = new ArrayList<SchoolSelectedByCandidate>();
				UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
				DistrictMaster districtMaster =null;
				SchoolMaster schoolMaster = null;
				if(userMaster!=null)
				{	
					System.out.println(" sortOrderGlobal :: "+sortOrderGlobal);
					if(userMaster.getEntityType()==2)
					{
						if(districtId.length()>0 && schoolId.length()==0)
						{
							System.out.println(" District Search");
							districtMaster=districtMasterDAO.findByDistrictId(districtId);
							schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
						}
						else if(districtId.length()>0 && schoolId.length()>0)
						{
							System.out.println(" District & School Search");
							districtMaster=districtMasterDAO.findByDistrictId(districtId);
							schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId), false, false);
							schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidatesSchools(districtMaster,schoolMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
						}
					}
					else if(userMaster.getEntityType()==1)
					{
						if(districtId.length()==0 && schoolId.length()==0)
						{
							System.out.println("Initial Search");
							districtMaster=districtMasterDAO.findByDistrictId(districtId);
							schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
						}
						else if(districtId.length()>0 && schoolId.length()==0)
						{
							System.out.println(" District Search");
							districtMaster=districtMasterDAO.findByDistrictId(districtId);
							schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidates(districtMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
						}
						else if(districtId.length()>0 && schoolId.length()>0)
						{
							System.out.println(" District & School Search");
							districtMaster=districtMasterDAO.findByDistrictId(districtId);
							schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId), false, false);
							schoolSelectedByCandidates = schoolSelectedByCandidateDAO.getSchoolSelectedCandidatesSchools(districtMaster,schoolMaster,checkShortOrder,sortOrderStrVal,0,0,flag);
						}
					}
				}

				tmRecords.append("<div style='text-align: center; font-size: 25px; text-decoration: underline; font-weight:bold;'>"+msgSchlSlctByCandRept+"</div><br/>");
				tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+"Created by: "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>Date of Print: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
				tmRecords.append("</div>");
				tmRecords.append("<table  id='selectedCandidatePrintPre' width='100%' border='0'>");
				tmRecords.append("<thead class='bg' style='display: table-header-group; '>");
				tmRecords.append("<tr>");
				tmRecords.append("<th  style='text-align:left; font-size:12px;' valign='top'>"+lblCandidateName+"</th>");
				tmRecords.append("<th  style='text-align:left; font-size:12px;' valign='top'>"+lblSchoolSelected+"</th>");
				tmRecords.append("<th  style='text-align:left ;font-size:12px;' valign='top'>"+lblDateSelected1+"</th>");
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				tmRecords.append("<tbody>");
				if(schoolSelectedByCandidates==null || schoolSelectedByCandidates.size()==0){
					tmRecords.append("<tr><td style='font-size:12px;' >"+msgNorecordfound+"</td></tr>" );
				}
				if(schoolSelectedByCandidates.size()>0){
					for(SchoolSelectedByCandidate ssbc  : schoolSelectedByCandidates){
						tmRecords.append("<tr>");
						tmRecords.append("<td  style='font-size:11px;'>");
						tmRecords.append("<div>");
						tmRecords.append("<span>");		
						tmRecords.append("<span style='font-size:11px;'>");
						tmRecords.append(ssbc.getTeacherDetail().getFirstName()+" "+ssbc.getTeacherDetail().getLastName());
						tmRecords.append("</br>");
						tmRecords.append(ssbc.getTeacherDetail().getEmailAddress());		 	
						tmRecords.append("</span>");
						tmRecords.append("</span>");
						tmRecords.append("</div>");
						tmRecords.append("</td>");
						tmRecords.append("<td style='font-size:11px;'>"+ssbc.getSchoolMaster().getSchoolName()+"</td>");
						tmRecords.append("<td style='font-size:11px;'>"+ssbc.getSchoolSelectionDate()+"</td>");	
						tmRecords.append("</tr>");
					}
					tmRecords.append("</tbody>");
					tmRecords.append("</table>");
				}
			}
			catch(Exception e)
			{e.printStackTrace();}
		}
		return tmRecords.toString();
	}
}