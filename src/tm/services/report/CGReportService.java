package tm.services.report;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.cgreport.PercentileCalculation;
import tm.bean.cgreport.PercentileCalculationCompetency;
import tm.bean.cgreport.PercentileNationalCompositeTScore;
import tm.bean.cgreport.PercentileScoreByJob;
import tm.bean.cgreport.PercentileZscoreTscore;
import tm.bean.cgreport.RawDataForCompetency;
import tm.bean.cgreport.RawDataForDomain;
import tm.bean.cgreport.RawDataForObjective;
import tm.bean.cgreport.TmpPercentileWiseZScore;
import tm.bean.cgreport.ZScore;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.ObjectiveMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.NetchemiaDistrictsDAO;
import tm.dao.TeacherAnswerDetailDAO;
import tm.dao.TeacherAssessmentQuestionDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.assessment.AssessmentCompetencyScoreDAO;
import tm.dao.assessment.AssessmentDomainScoreDAO;
import tm.dao.assessment.ScoreLookupDAO;
import tm.dao.cgreport.PercentileCalculationCompetencyDAO;
import tm.dao.cgreport.PercentileCalculationDAO;
import tm.dao.cgreport.PercentileScoreByJobDAO;
import tm.dao.cgreport.RawDataForCompetencyDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.dao.cgreport.TmpPercentileWiseZScoreDAO;
import tm.dao.cgreport.ZScoreDAO;
import tm.dao.master.CompetencyMasterDAO;
import tm.dao.master.DomainMasterDAO;
import tm.services.EmailerService;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

public class CGReportService {
	String locale = Utility.getValueOfPropByKey("locale");
	String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	public JobForTeacherDAO getJobForTeacherDAO() {
		return jobForTeacherDAO;
	}
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	
	@Autowired
	private ZScoreDAO zScoreDAO;
	public void setzScoreDAO(ZScoreDAO zScoreDAO) {
		this.zScoreDAO = zScoreDAO;
	}
	
	@Autowired
	private PercentileCalculationDAO percentileCalculationDAO;
	public void setPercentileCalculationDAO(PercentileCalculationDAO percentileCalculationDAO) {
		this.percentileCalculationDAO = percentileCalculationDAO;
	}
	
	@Autowired
	private PercentileScoreByJobDAO percentileScoreByJobDAO;
	public void setPercentileScoreByJobDAO(PercentileScoreByJobDAO percentileScoreByJobDAO) {
		this.percentileScoreByJobDAO = percentileScoreByJobDAO;
	}
	
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) {
		this.domainMasterDAO = domainMasterDAO;
	}
	
	@Autowired
	private SessionFactory sessionFactory;
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Autowired
	private TeacherAnswerDetailDAO teacherAnswerDetailDAO;
	public void setTeacherAnswerDetailDAO(TeacherAnswerDetailDAO teacherAnswerDetailDAO) {
		this.teacherAnswerDetailDAO = teacherAnswerDetailDAO;
	}
	
	@Autowired
	private CompetencyMasterDAO competencyMasterDAO;
	public void setCompetencyMasterDAO(CompetencyMasterDAO competencyMasterDAO) {
		this.competencyMasterDAO = competencyMasterDAO;
	}
	
	@Autowired
	private RawDataForDomainDAO rawDataForDomainDAO;
	public void setRawDataForDomainDAO(RawDataForDomainDAO rawDataForDomainDAO) {
		this.rawDataForDomainDAO = rawDataForDomainDAO;
	}
	public RawDataForDomainDAO getRawDataForDomainDAO() {
		return rawDataForDomainDAO;
	}

	@Autowired
	private PercentileCalculationCompetencyDAO percentileCalculationCompetencyDAO;
	public void setPercentileCalculationCompetencyDAO(PercentileCalculationCompetencyDAO percentileCalculationCompetencyDAO) {
		this.percentileCalculationCompetencyDAO = percentileCalculationCompetencyDAO;
	}
	
	@Autowired
	private RawDataForCompetencyDAO rawDataForCompetencyDAO;
	public void setRawDataForCompetencyDAO(RawDataForCompetencyDAO rawDataForCompetencyDAO) {
		this.rawDataForCompetencyDAO = rawDataForCompetencyDAO;
	}
	public RawDataForCompetencyDAO getRawDataForCompetencyDAO() {
		return rawDataForCompetencyDAO;
	}
	
	@Autowired
	private TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO;
	public void setTmpPercentileWiseZScoreDAO(
			TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO) {
		this.tmpPercentileWiseZScoreDAO = tmpPercentileWiseZScoreDAO;
	}
	public TmpPercentileWiseZScoreDAO getTmpPercentileWiseZScoreDAO() {
		return tmpPercentileWiseZScoreDAO;
	}

	@Autowired
	private ScoreLookupDAO scoreLookupDAO;
	public void setScoreLookupDAO(ScoreLookupDAO scoreLookupDAO) {
		this.scoreLookupDAO = scoreLookupDAO;
	}
	public ScoreLookupDAO getScoreLookupDAO() {
		return scoreLookupDAO;
	}
	
	@Autowired
	private AssessmentDomainScoreDAO assessmentDomainScoreDAO;
	public AssessmentDomainScoreDAO getAssessmentDomainScoreDAO() {
		return assessmentDomainScoreDAO;
	}
	
	@Autowired
	private AssessmentCompetencyScoreDAO assessmentCompetencyScoreDAO; 
	public AssessmentCompetencyScoreDAO getAssessmentCompetencyScoreDAO() {
		return assessmentCompetencyScoreDAO;
	}
	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;
	public TeacherNormScoreDAO getTeacherNormScoreDAO() {
		return teacherNormScoreDAO;
	}
	public TeacherAssessmentStatusDAO getTeacherAssessmentStatusDAO() {
		return teacherAssessmentStatusDAO;
	}
	
	@Autowired
	private NetchemiaDistrictsDAO netchemiaDistrictsDAO;
	public void setNetchemiaDistrictsDAO(
			NetchemiaDistrictsDAO netchemiaDistrictsDAO) {
		this.netchemiaDistrictsDAO = netchemiaDistrictsDAO;
	}
	public NetchemiaDistrictsDAO getNetchemiaDistrictsDAO() {
		return netchemiaDistrictsDAO;
	}
	
	@Autowired
	private TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO;
	public TeacherAssessmentQuestionDAO getTeacherAssessmentQuestionDAO() {
		return teacherAssessmentQuestionDAO;
	}
	
	@Autowired
	private EmailerService emailerService;
	public EmailerService getEmailerService() {
		return emailerService;
	}
	
	Map<Double, ZScore> mapPercentileWithZscore = new HashMap<Double, ZScore>();
	
	StatelessSession statelesssessionHome = null;
	Transaction txHome = null;
	
	public void setZScore(){
		if(mapPercentileWithZscore==null || mapPercentileWithZscore.size()==0){
			List<ZScore> lstZScore = zScoreDAO.findAll();		
			for(ZScore zScore:lstZScore){
				mapPercentileWithZscore.put(zScore.getPercentile(), zScore);
			}
		}
		statelesssessionHome = sessionFactory.openStatelessSession();
		txHome = statelesssessionHome.beginTransaction();
	}
	
	//Insert records in rawDataForDomain and rawDataForCompetency by passing teacher list	
	@SuppressWarnings("unchecked")
	public void saveRawDataOfTeacher(List<TeacherDetail> lstTeacherDetails, TeacherAssessmentdetail teacherAssessmentdetail){
		try {
			if(lstTeacherDetails==null || lstTeacherDetails.size()==0){
				return;
			}
			List<DomainMaster> lstDomain = null;
			Date assessmentDateTime = null;
			if(teacherAssessmentdetail.getAssessmentType()==1)
				lstDomain = WorkThreadServlet.domainMastersPDRList;
			else
				lstDomain = teacherAssessmentQuestionDAO.findTeacherDomains(teacherAssessmentdetail);
			try {
				List questionIdList = new ArrayList();
				questionIdList = teacherAnswerDetailDAO.findDuplicateQuestionId(lstTeacherDetails, teacherAssessmentdetail);
				for(Object object : questionIdList){
					Object obj[] = (Object[])object;
					System.out.println("obj[0] cnt : "+obj[0]);
					System.out.println("obj[1] teacherAssessmentQuestionId : "+obj[1]);
					System.out.println("obj[2] teacherId : "+obj[2]);
					Integer cnt = Integer.parseInt(""+obj[0]);
					Long teacherAssessmentQuestionId = Long.parseLong(""+obj[1]);
					Integer teacherId = Integer.parseInt(""+obj[2]);
					teacherAnswerDetailDAO.removeAnswerByTeacherIdAndQuestionId(teacherId, teacherAssessmentQuestionId, cnt);
				}
				assessmentDateTime = teacherAnswerDetailDAO.findDateTimeOfLastAnswer(lstTeacherDetails, teacherAssessmentdetail);
			} catch (Exception e) {
				e.printStackTrace();
			}
			Map<Integer,Map<Integer, Object>> mapDomainScore = new HashMap<Integer, Map<Integer,Object>>();
			Map<Integer, Object> mapDomainScoreTotal = null;
			
			List lstAns = null;
			TeacherDetail tDetail = null;
			for(DomainMaster dm: lstDomain){
				mapDomainScoreTotal = new HashMap<Integer, Object>();
				if(teacherAssessmentdetail!=null)
					lstAns = teacherAnswerDetailDAO.findSumOfAnsByDomainAndTeacherList(lstTeacherDetails, dm, teacherAssessmentdetail);
				
				for(Object oo: lstAns){
					Object obj[] = (Object[])oo;
					tDetail = (TeacherDetail) obj[0];				
					mapDomainScoreTotal.put(tDetail.getTeacherId(), obj);
				}
				mapDomainScore.put(dm.getDomainId(), mapDomainScoreTotal);
			}
			
			List<CompetencyMaster> lstCompetencyMasters = null;
			if(teacherAssessmentdetail.getAssessmentType()==1)
				lstCompetencyMasters = competencyMasterDAO.getCompetenciesForPDR(true);
			else
				lstCompetencyMasters = teacherAssessmentQuestionDAO.findTeacherCompetency(teacherAssessmentdetail);
			
			Map<Integer,Map<Integer, Object>> mapCompetencyScore = new HashMap<Integer, Map<Integer,Object>>();
			Map<Integer, Object> mapCompetencyScoreTotal = null;
			for(CompetencyMaster competencyMaster: lstCompetencyMasters){
				mapCompetencyScoreTotal = new HashMap<Integer, Object>();
				if(teacherAssessmentdetail!=null && teacherAssessmentdetail.getAssessmentDetail()!=null && teacherAssessmentdetail.getAssessmentType()!=null)
					lstAns = teacherAnswerDetailDAO.findSumOfAnsByComptencyAndTeacherList(lstTeacherDetails, competencyMaster, teacherAssessmentdetail);
				
				for(Object oo: lstAns){
					Object obj[] = (Object[])oo;
					tDetail = (TeacherDetail) obj[0];
					mapCompetencyScoreTotal.put( tDetail.getTeacherId(), obj);
				}
				mapCompetencyScore.put(competencyMaster.getCompetencyId(), mapCompetencyScoreTotal);
			}
			
			List<ObjectiveMaster> objectiveMasterList = null;
			objectiveMasterList = teacherAssessmentQuestionDAO.findTeacherObjective(teacherAssessmentdetail);
			Map<Integer,Map<Integer, Object>> mapObjectiveScore = new HashMap<Integer, Map<Integer,Object>>();
			Map<Integer, Object> mapObjectiveScoreTotal = null;
			for(ObjectiveMaster objectiveMaster : objectiveMasterList){
				mapObjectiveScoreTotal = new HashMap<Integer, Object>();
				if(teacherAssessmentdetail!=null && teacherAssessmentdetail.getAssessmentDetail()!=null && teacherAssessmentdetail.getAssessmentType()!=null)
					lstAns = teacherAnswerDetailDAO.findSumOfAnsByObjectiveAndTeacherList(lstTeacherDetails, objectiveMaster, teacherAssessmentdetail);
				
				for(Object oo: lstAns){
					Object obj[] = (Object[])oo;
					tDetail = (TeacherDetail) obj[0];
					mapObjectiveScoreTotal.put(tDetail.getTeacherId(), obj);
				}
				mapObjectiveScore.put(objectiveMaster.getObjectiveId(), mapObjectiveScoreTotal);
			}
			
			RawDataForDomain rawdatafordomain = null;
			RawDataForCompetency rawDataForCompetency = null;
			RawDataForObjective rawDataForObjective = null;
			StatelessSession statelesssession = sessionFactory.openStatelessSession();
			Transaction tx = statelesssession.beginTransaction();
			for(TeacherDetail teacherDetail : lstTeacherDetails){
				for(DomainMaster dm: lstDomain){				
					Object obj[] = (Object[]) mapDomainScore.get(dm.getDomainId()).get(teacherDetail.getTeacherId());							
					if(obj!=null){
						rawdatafordomain = new RawDataForDomain();
						rawdatafordomain.setTeacherDetail(teacherDetail);
						rawdatafordomain.setTeacherAssessmentdetail(teacherAssessmentdetail);
						rawdatafordomain.setDomainMaster(dm);
						rawdatafordomain.setScore(new Double(""+obj[1]));
						rawdatafordomain.setMaxMarks(new Double(""+obj[2]));
						rawdatafordomain.setAssessmentDetail(teacherAssessmentdetail.getAssessmentDetail());
						rawdatafordomain.setAssessmentType(teacherAssessmentdetail.getAssessmentType());
						rawdatafordomain.setAssessmentTakenCount(teacherAssessmentdetail.getAssessmentTakenCount());
						rawdatafordomain.setCreatedDateTime(new Date());
						rawdatafordomain.setAssessmentDateTime(assessmentDateTime);
						rawdatafordomain.setUpdateDateTime(new Date());
						statelesssession.insert(rawdatafordomain);
					}				
				}
				
				for(CompetencyMaster competencyMaster : lstCompetencyMasters){
					Object obj[] = (Object[]) mapCompetencyScore.get(competencyMaster.getCompetencyId()).get(teacherDetail.getTeacherId());
					if(obj!=null){
						rawDataForCompetency = new RawDataForCompetency();
						rawDataForCompetency.setTeacherDetail(teacherDetail);
						rawDataForCompetency.setTeacherAssessmentdetail(teacherAssessmentdetail);
						rawDataForCompetency.setDomainMaster(competencyMaster.getDomainMaster());
						rawDataForCompetency.setCompetencyMaster(competencyMaster);
						rawDataForCompetency.setScore(new Double(""+obj[1]));
						rawDataForCompetency.setMaxMarks(new Double(""+obj[2]));
						rawDataForCompetency.setAssessmentDetail(teacherAssessmentdetail.getAssessmentDetail());
						rawDataForCompetency.setAssessmentType(teacherAssessmentdetail.getAssessmentType());
						rawDataForCompetency.setAssessmentTakenCount(teacherAssessmentdetail.getAssessmentTakenCount());
						rawDataForCompetency.setCreatedDateTime(new Date());
						rawDataForCompetency.setAssessmentDateTime(assessmentDateTime);
						rawDataForCompetency.setUpdateDateTime(new Date());
						statelesssession.insert(rawDataForCompetency);
					}		
				}
				
				for(ObjectiveMaster objectiveMaster : objectiveMasterList){
					Object obj[] = (Object[]) mapObjectiveScore.get(objectiveMaster.getObjectiveId()).get(teacherDetail.getTeacherId());
					if(obj!=null){
						rawDataForObjective = new RawDataForObjective();
						rawDataForObjective.setTeacherDetail(teacherDetail);
						rawDataForObjective.setTeacherAssessmentdetail(teacherAssessmentdetail);
						rawDataForObjective.setDomainMaster(objectiveMaster.getCompetencyMaster().getDomainMaster());
						rawDataForObjective.setCompetencyMaster(objectiveMaster.getCompetencyMaster());
						rawDataForObjective.setObjectiveMaster(objectiveMaster);
						rawDataForObjective.setScore(new Double(""+obj[1]));
						rawDataForObjective.setMaxMarks(new Double(""+obj[2]));
						rawDataForObjective.setAssessmentDetail(teacherAssessmentdetail.getAssessmentDetail());
						rawDataForObjective.setAssessmentType(teacherAssessmentdetail.getAssessmentType());
						rawDataForObjective.setAssessmentTakenCount(teacherAssessmentdetail.getAssessmentTakenCount());
						rawDataForObjective.setCreatedDateTime(new Date());
						rawDataForObjective.setAssessmentDateTime(assessmentDateTime);
						rawDataForObjective.setUpdateDateTime(new Date());
						statelesssession.insert(rawDataForObjective);
					}
				}
			}
			tx.commit();
			statelesssession.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void insertOrUpdatePercentile(List<Integer> lstScore, DomainMaster domainMaster){
		System.out.println("Insert or update percentile score domain master"+domainMaster);
		try {
			setZScore();
			PercentileCalculation percentileCalculation = null;
			List<PercentileCalculation> lstPerCalculations = percentileCalculationDAO.findPercentileCalculationByDomain(domainMaster);
			Map<Integer, PercentileCalculation> mapPCalculation = new HashMap<Integer, PercentileCalculation>();
			
			for(PercentileCalculation pCalculation: lstPerCalculations){
				mapPCalculation.put(pCalculation.getScore(), pCalculation);	
			}
			
			StatelessSession statelesssession = sessionFactory.openStatelessSession();
			Transaction tx = statelesssession.beginTransaction();
			
			for(Integer score:lstScore){				
				percentileCalculation = mapPCalculation.get(score);
				if(percentileCalculation!=null){
					percentileCalculation.setFrequency(percentileCalculation.getFrequency()+1);
					statelesssession.update(percentileCalculation);
				}
				else{
					percentileCalculation = new PercentileCalculation();
					percentileCalculation.setDomainMaster(domainMaster);
					percentileCalculation.setScore(score);
					percentileCalculation.setFrequency(1);
					percentileCalculation.setStatus("A");
					percentileCalculation.setCreatedDateTime(new Date());					
					statelesssession.insert(percentileCalculation);
					lstPerCalculations.add(percentileCalculation);
					mapPCalculation.put(percentileCalculation.getScore(),percentileCalculation);
				}				
			}
			
			tx.commit();
			statelesssession.close();			
			Collections.sort(lstPerCalculations);
						
			int N=0;			
			double f;
			double cf=0.0;
			double fby2;			
			double cf_fby2byn;
			double percentile;
			double zScore;
			double tScore;
			for(PercentileCalculation pCal: lstPerCalculations){
				//System.out.println(pCal.getScore());
				N=N+pCal.getFrequency();
			}
			statelesssession = sessionFactory.openStatelessSession();
			tx = statelesssession.beginTransaction();
			for(PercentileCalculation pCal: lstPerCalculations){
				f = pCal.getFrequency();
				cf = cf+f;
				fby2 = f/2;
				cf_fby2byn = (cf-fby2)/N;
				percentile = Double.valueOf(Utility.roundTwoDecimalsAsString(cf_fby2byn*100));
				zScore = mapPercentileWithZscore.get(percentile).getzScore();
				tScore = mapPercentileWithZscore.get(percentile).gettScore(); 				
				//System.out.println(percentile+">"+zScore+">"+tScore);				
				pCal.setCumulativeFrequency((int)Math.round(cf));
				pCal.setPercentile(percentile);
				pCal.setzValue(zScore);
				pCal.settValue(tScore);
				statelesssession.update(pCal);
			}
			tx.commit();
			statelesssession.close();
			//System.out.println(N);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void insertOrUpdatePercentileCompetencyWise(List<Integer> lstScore, CompetencyMaster competencyMaster){
		System.out.println("Insert or update percentile score competency master"+competencyMaster);
		try {
			setZScore();
			PercentileCalculationCompetency percentileCalculationCompetency = null;
			List<PercentileCalculationCompetency> lstPerCalculationsCompetencyList = percentileCalculationCompetencyDAO.findPercentileCalculationByCompetency(competencyMaster);
			Map<Integer, PercentileCalculationCompetency> mapPCalculation = new HashMap<Integer, PercentileCalculationCompetency>();
			
			for(PercentileCalculationCompetency pCalculation: lstPerCalculationsCompetencyList){
				mapPCalculation.put(pCalculation.getScore(), pCalculation);	
			}
			
			StatelessSession statelesssession = sessionFactory.openStatelessSession();
			Transaction tx = statelesssession.beginTransaction();
			
			for(Integer score:lstScore){				
				percentileCalculationCompetency = mapPCalculation.get(score);
				if(percentileCalculationCompetency!=null){
					percentileCalculationCompetency.setFrequency(percentileCalculationCompetency.getFrequency()+1);
					statelesssession.update(percentileCalculationCompetency);
				}
				else{
					percentileCalculationCompetency = new PercentileCalculationCompetency();
					percentileCalculationCompetency.setCompetencyMaster(competencyMaster);
					percentileCalculationCompetency.setScore(score);
					percentileCalculationCompetency.setFrequency(1);
					percentileCalculationCompetency.setStatus("A");
					percentileCalculationCompetency.setCreatedDateTime(new Date());					
					statelesssession.insert(percentileCalculationCompetency);
					lstPerCalculationsCompetencyList.add(percentileCalculationCompetency);
					mapPCalculation.put(percentileCalculationCompetency.getScore(),percentileCalculationCompetency);
				}				
			}
			
			tx.commit();
			statelesssession.close();			
			Collections.sort(lstPerCalculationsCompetencyList);
						
			int N=0;			
			double f;
			double cf=0.0;
			double fby2;			
			double cf_fby2byn;
			double percentile;
			double zScore;
			double tScore;
			for(PercentileCalculationCompetency pCal: lstPerCalculationsCompetencyList){
				//System.out.println(pCal.getScore());
				N=N+pCal.getFrequency();
			}
			statelesssession = sessionFactory.openStatelessSession();
			tx = statelesssession.beginTransaction();
			for(PercentileCalculationCompetency pCal: lstPerCalculationsCompetencyList){
				f = pCal.getFrequency();
				cf = cf+f;
				fby2 = f/2;
				cf_fby2byn = (cf-fby2)/N;
				percentile = Double.valueOf(Utility.roundTwoDecimalsAsString(cf_fby2byn*100));
				zScore = mapPercentileWithZscore.get(percentile).getzScore();
				tScore = mapPercentileWithZscore.get(percentile).gettScore(); 				
				//System.out.println(percentile+">"+zScore+">"+tScore);				
				pCal.setCumulativeFrequency((int)Math.round(cf));
				pCal.setPercentile(percentile);
				pCal.setzValue(zScore);
				pCal.settValue(tScore);
				statelesssession.update(pCal);
			}
			tx.commit();
			statelesssession.close();
			//System.out.println(N);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void insertorUpdateCompositeTValue(List<Double> lstScore){
		try {
			PercentileNationalCompositeTScore percentileNationalCompositeTScore = null;
			
			List<PercentileNationalCompositeTScore> lstPercentileNationalCompositeTScore = new ArrayList<PercentileNationalCompositeTScore>();
			Map<Double, PercentileNationalCompositeTScore> mapPCalculation = new HashMap<Double, PercentileNationalCompositeTScore>();
			
			
			StatelessSession statelesssessionCompositeTScore = null;
			Transaction txCompositeTScore = null;
			statelesssessionCompositeTScore = sessionFactory.openStatelessSession();
			txCompositeTScore = statelesssessionCompositeTScore.beginTransaction();
			
			
			for(Double compTScore:lstScore){				
				percentileNationalCompositeTScore = mapPCalculation.get(compTScore);
				if(percentileNationalCompositeTScore!=null){
					percentileNationalCompositeTScore.setFrequency(percentileNationalCompositeTScore.getFrequency()+1);
					statelesssessionCompositeTScore.update(percentileNationalCompositeTScore);
				}
				else{
					percentileNationalCompositeTScore = new PercentileNationalCompositeTScore();					
					percentileNationalCompositeTScore.setFrequency(1);
					percentileNationalCompositeTScore.setCompositeTValue(compTScore);
					percentileNationalCompositeTScore.setCreatedDateTime(new Date());					
					statelesssessionCompositeTScore.insert(percentileNationalCompositeTScore);
					lstPercentileNationalCompositeTScore.add(percentileNationalCompositeTScore);
					mapPCalculation.put(compTScore, percentileNationalCompositeTScore);
				}				
			}
			
					
			
			Collections.sort(lstPercentileNationalCompositeTScore);
			
			
			double f;
			double cf=0.0;			
			
			for(PercentileNationalCompositeTScore pCal: lstPercentileNationalCompositeTScore){
				f = pCal.getFrequency();
				cf = cf+f;			
				
				pCal.setCumulativeFrequency((int)Math.round(cf));
				statelesssessionCompositeTScore.update(pCal);
			}
			txCompositeTScore.commit();
			statelesssessionCompositeTScore.close();	
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void insertOrUpdatePercentileByJobOneTime(List<Integer> lstScore, DomainMaster domainMaster, JobOrder jobOrder){
		try {
			PercentileScoreByJob percentileScoreByJob = null;
			List<PercentileScoreByJob> lstPercentileScoreByJob = new ArrayList<PercentileScoreByJob>();
			Map<Integer, PercentileScoreByJob> mapPCalculation = new HashMap<Integer, PercentileScoreByJob>();
			
			for(PercentileScoreByJob pCalculation : lstPercentileScoreByJob){
				mapPCalculation.put(pCalculation.getScore(), pCalculation);			
			}
			
			for(Integer score:lstScore){				
				percentileScoreByJob = mapPCalculation.get(score);
				if(percentileScoreByJob!=null){
					percentileScoreByJob.setFrequency(percentileScoreByJob.getFrequency()+1);
					statelesssessionHome.update(percentileScoreByJob);
				}
				else{
					percentileScoreByJob = new PercentileScoreByJob();
					percentileScoreByJob.setJobOrder(jobOrder);
					percentileScoreByJob.setDomainMaster(domainMaster);
					percentileScoreByJob.setScore(score);
					percentileScoreByJob.setFrequency(1);
					percentileScoreByJob.setStatus("A");
					percentileScoreByJob.setCreatedDateTime(new Date());					
					statelesssessionHome.insert(percentileScoreByJob);
					lstPercentileScoreByJob.add(percentileScoreByJob);
					mapPCalculation.put(score, percentileScoreByJob);
				}				
			}
			
			/*txHome.commit();
			statelesssessionHome.close();	*/		
			
			Collections.sort(lstPercentileScoreByJob);
			
			
			int N=0;			
			double f;
			double cf=0.0;
			double fby2;			
			double cf_fby2byn;
			double percentile;
			double zScore;
			double tScore;
			for(PercentileScoreByJob pCal: lstPercentileScoreByJob){				
				N=N+pCal.getFrequency();
			}
			
			for(PercentileScoreByJob pCal: lstPercentileScoreByJob){
				f = pCal.getFrequency();
				cf = cf+f;
				fby2 = f/2;
				cf_fby2byn = (cf-fby2)/N;
				percentile = Double.valueOf(Utility.roundTwoDecimalsAsString(cf_fby2byn*100));
				zScore = mapPercentileWithZscore.get(percentile).getzScore();
				tScore = mapPercentileWithZscore.get(percentile).gettScore(); 
				
				
				pCal.setCumulativeFrequency((int)Math.round(cf));
				pCal.setPercentile(percentile);
				pCal.setzValue(zScore);
				pCal.settValue(tScore);
				statelesssessionHome.update(pCal);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void insertOrUpdatePercentileJobByThreadOrUser(List<Integer> lstScore, DomainMaster domainMaster, JobOrder jobOrder){
		try {
			PercentileScoreByJob percentileScoreByJob = null;
			List<PercentileScoreByJob> lstPercentileScoreByJob = percentileScoreByJobDAO.findPercentileScoreByJobAndDomain(domainMaster,jobOrder);			
			Map<Integer, PercentileScoreByJob> mapPCalculation = new HashMap<Integer, PercentileScoreByJob>();
			
			for(PercentileScoreByJob pCalculation : lstPercentileScoreByJob){
				mapPCalculation.put(pCalculation.getScore(), pCalculation);			
			}
			
			for(Integer score:lstScore){				
				percentileScoreByJob = mapPCalculation.get(score);
				if(percentileScoreByJob!=null){
					percentileScoreByJob.setFrequency(percentileScoreByJob.getFrequency()+1);
					statelesssessionHome.update(percentileScoreByJob);
				}
				else{
					percentileScoreByJob = new PercentileScoreByJob();
					percentileScoreByJob.setJobOrder(jobOrder);
					percentileScoreByJob.setDomainMaster(domainMaster);
					percentileScoreByJob.setScore(score);
					percentileScoreByJob.setFrequency(1);
					percentileScoreByJob.setStatus("A");
					percentileScoreByJob.setCreatedDateTime(new Date());					
					statelesssessionHome.insert(percentileScoreByJob);
					lstPercentileScoreByJob.add(percentileScoreByJob);
					mapPCalculation.put(score, percentileScoreByJob);
				}				
			}
			
			/*txHome.commit();
			statelesssessionHome.close();	*/		
			
			Collections.sort(lstPercentileScoreByJob);
			
			
			int N=0;			
			double f;
			double cf=0.0;
			double fby2;			
			double cf_fby2byn;
			double percentile;
			double zScore;
			double tScore;
			for(PercentileScoreByJob pCal: lstPercentileScoreByJob){				
				N=N+pCal.getFrequency();
			}
			
			for(PercentileScoreByJob pCal: lstPercentileScoreByJob){
				f = pCal.getFrequency();
				cf = cf+f;
				fby2 = f/2;
				cf_fby2byn = (cf-fby2)/N;
				percentile = Double.valueOf(Utility.roundTwoDecimalsAsString(cf_fby2byn*100));
				zScore = mapPercentileWithZscore.get(percentile).getzScore();
				tScore = mapPercentileWithZscore.get(percentile).gettScore(); 
				
				
				pCal.setCumulativeFrequency((int)Math.round(cf));
				pCal.setPercentile(percentile);
				pCal.setzValue(zScore);
				pCal.settValue(tScore);
				statelesssessionHome.update(pCal);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void insertOrUpdatePercentileByJob(List<Integer> lstScore, DomainMaster domainMaster, JobOrder jobOrder){
		System.out.println("Insert or update percentile score domain master"+domainMaster);
		try {
			PercentileScoreByJob percentileScoreByJob = null;
			List<PercentileScoreByJob> lstPercentileScoreByJob = percentileScoreByJobDAO.findPercentileScoreByJobAndDomain(domainMaster,jobOrder);
			Map<Integer, PercentileScoreByJob> mapPCalculation = new HashMap<Integer, PercentileScoreByJob>();
			
			for(PercentileScoreByJob pCalculation : lstPercentileScoreByJob){
				mapPCalculation.put(pCalculation.getScore(), pCalculation);			
			}
			
			StatelessSession statelesssession = sessionFactory.openStatelessSession();
			Transaction tx = statelesssession.beginTransaction();
			
			for(Integer score:lstScore){				
				percentileScoreByJob = mapPCalculation.get(score);
				if(percentileScoreByJob!=null){
					percentileScoreByJob.setFrequency(percentileScoreByJob.getFrequency()+1);
					statelesssession.update(percentileScoreByJob);
				}
				else{
					percentileScoreByJob = new PercentileScoreByJob();
					percentileScoreByJob.setJobOrder(jobOrder);
					percentileScoreByJob.setDomainMaster(domainMaster);
					percentileScoreByJob.setScore(score);
					percentileScoreByJob.setFrequency(1);
					percentileScoreByJob.setStatus("A");
					percentileScoreByJob.setCreatedDateTime(new Date());					
					statelesssession.insert(percentileScoreByJob);
					lstPercentileScoreByJob.add(percentileScoreByJob);
					mapPCalculation.put(score, percentileScoreByJob);
				}				
			}
			
			tx.commit();
			statelesssession.close();			
			
			Collections.sort(lstPercentileScoreByJob);			
			//System.out.println(lstPercentileScoreByJob);
						
			
			List<ZScore> lstZScore = zScoreDAO.findAll();
			Map<Double, ZScore> mapPercentileWithZscore = new HashMap<Double, ZScore>();
			for(ZScore zScore:lstZScore){
				mapPercentileWithZscore.put(zScore.getPercentile(), zScore);
			}
			
			
			int N=0;			
			double f;
			double cf=0.0;
			double fby2;			
			double cf_fby2byn;
			double percentile;
			double zScore;
			double tScore;
			for(PercentileScoreByJob pCal: lstPercentileScoreByJob){
				//System.out.println(pCal.getScore());
				N=N+pCal.getFrequency();
			}
			statelesssession = sessionFactory.openStatelessSession();
			tx = statelesssession.beginTransaction();
			for(PercentileScoreByJob pCal: lstPercentileScoreByJob){
				f = pCal.getFrequency();
				cf = cf+f;
				fby2 = f/2;
				cf_fby2byn = (cf-fby2)/N;
				percentile = Double.valueOf(Utility.roundTwoDecimalsAsString(cf_fby2byn*100));
				zScore = mapPercentileWithZscore.get(percentile).getzScore();
				tScore = mapPercentileWithZscore.get(percentile).gettScore(); 
				
				//System.out.println(percentile+">"+zScore+">"+tScore);
				
				pCal.setCumulativeFrequency((int)Math.round(cf));
				pCal.setPercentile(percentile);
				pCal.setzValue(zScore);
				pCal.settValue(tScore);
				statelesssession.update(pCal);
			}
			tx.commit();
			statelesssession.close();
			//System.out.println(N);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void populateZScore(){
		try {
			Map<Double, Double> mapData = new HashMap<Double, Double>();
			Connection connection = getConnection();
			Statement stmt=connection.createStatement();
			
			ResultSet rs = stmt.executeQuery("select * from tmppercentilewisezscore");
			while(rs.next()){
				mapData.put(rs.getDouble(2), rs.getDouble(3));
			}
			//System.out.println(mapData);
			Double prePercentile=0.00;
			Double postPercentile=0.00;
			Double preZscore = 0.00;
			Double postZscore = 0.00;
			Double zScoreDiff = 0.00;
			Double percentileDiff = 0.00;
			Double diffForOnePer=0.00;
			
			PreparedStatement  pstmt=null;
			Connection con = getConnection();
			con.setAutoCommit(true);		
			pstmt = con.prepareStatement("insert into tmpextpercentilewisezscore(percentile,zScore) value(?,?)");
			
			for(double i=.01;i<100;i+=0.01){				
				if(mapData.get(new Double(Utility.roundTwoDecimalsAsString(i)))==null || mapData.get(new Double(Utility.roundTwoDecimalsAsString(i)))==0.0){
					prePercentile = new Double(Utility.roundTwoDecimalsAsString(i-0.01));					
					preZscore = mapData.get(prePercentile);
					while(mapData.get(new Double(Utility.roundTwoDecimalsAsString(i)))==null || mapData.get(new Double(Utility.roundTwoDecimalsAsString(i)))==0.0){
						i+=0.01;
					}
					postPercentile = new Double(Utility.roundTwoDecimalsAsString(i));
					postZscore = mapData.get(postPercentile);
					
					zScoreDiff = new Double(Utility.roundTwoDecimalsAsString(postZscore-preZscore));
					percentileDiff = new Double(Utility.roundTwoDecimalsAsString(postPercentile-prePercentile));
					
					diffForOnePer = (.01)*(zScoreDiff/percentileDiff);
					
					/*System.out.println("\nprePercentile:"+prePercentile);
					System.out.println("preZscore:"+preZscore);
					System.out.println("postPercentile:"+postPercentile);
					System.out.println("postZscore:"+postZscore);
					System.out.println("zScoreDiff:"+zScoreDiff);
					System.out.println("percentileDiff:"+percentileDiff);
					System.out.println("diffForOnePer"+diffForOnePer);*/
					
					prePercentile = new Double(Utility.roundTwoDecimalsAsString(prePercentile + 0.01));
					//System.out.println("Before prePercentile"+prePercentile+" zScore:"+preZscore +" diffForOnePer:"+diffForOnePer);
					preZscore=preZscore+diffForOnePer;
					while(prePercentile < postPercentile){						
					//	System.out.println("Insert prePercentile"+prePercentile+" zScore:"+preZscore);
						pstmt.setDouble(1, prePercentile);
						pstmt.setDouble(2, preZscore);
						pstmt.addBatch();
						prePercentile = new Double(Utility.roundTwoDecimalsAsString(prePercentile + 0.01));
						preZscore=preZscore+diffForOnePer;
					}		
					pstmt.executeBatch();
				}
			}			
		} 
		catch (Exception e){
			e.printStackTrace();
		}		
	}
	
	public void updateRawDataFirstTime(){		
		setZScore();
		JobOrder jOrder = new JobOrder();
		jOrder.setJobId(0);
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentDone(null,jOrder);		
		List<TeacherDetail> lstTeacherDetails = new LinkedList<TeacherDetail>();
		StatelessSession statelesssession = sessionFactory.openStatelessSession();
		Transaction tx = statelesssession.beginTransaction();
		for(TeacherAssessmentStatus teacherAssessmentStatus : lstTeacherAssessmentStatus){			
			lstTeacherDetails.add(teacherAssessmentStatus.getTeacherDetail());
			teacherAssessmentStatus.setCgUpdated(true);
			statelesssession.update(teacherAssessmentStatus);
		}
		saveRawDataOfTeacher(lstTeacherDetails, null);		
		List<DomainMaster> lstDomainMasters = domainMasterDAO.findAll();
		List<Integer> lstScore = new LinkedList<Integer>();
		List<RawDataForDomain> lstRawDataForDomains = null;
		Integer score = null;
		for(DomainMaster domainMaster : lstDomainMasters){
			lstRawDataForDomains = rawDataForDomainDAO.findByDomain(domainMaster);
			lstScore = new LinkedList<Integer>();
			for(RawDataForDomain rawDataForDomain : lstRawDataForDomains){
				score = new Integer(""+Math.round(rawDataForDomain.getScore()));
				lstScore.add(score);
			}
			insertOrUpdatePercentile(lstScore, domainMaster);
		}
		
		//---------------- insert recore in 
		//----------------
		
		tx.commit();
		
		/////////////////////////////////////////////////
		////////////////////////vishwanath Competency percentile ////////////////////////
		tx = statelesssession.beginTransaction();
		List<CompetencyMaster> lstcompetencyMaster = competencyMasterDAO.getCompetenciesForPDR(true);
		List<Integer> lstScoreCompetency = new LinkedList<Integer>();
		List<RawDataForCompetency> lstRawDataForCompetencies = null;
		score = null;
		for(CompetencyMaster competencyMaster : lstcompetencyMaster){
			lstRawDataForCompetencies = rawDataForCompetencyDAO.findByCompetencyAndTeachers(competencyMaster, lstTeacherDetails);
			lstScore = new LinkedList<Integer>();
			for(RawDataForCompetency rawDataForCompetency : lstRawDataForCompetencies){
				score = new Integer(""+Math.round(rawDataForCompetency.getScore()));
				lstScoreCompetency.add(score);
			}
			insertOrUpdatePercentileCompetencyWise(lstScoreCompetency, competencyMaster);
		}
		tx.commit();
		//////////////////////////////////////////////////
		statelesssession.close();
	}
	
	public void updateRawDataByUser(){
		setZScore();
		JobOrder jOrder = new JobOrder();
		jOrder.setJobId(0);
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentDoneWithCGStatus(null,jOrder);		
		List<TeacherDetail> lstTeacherDetails = new LinkedList<TeacherDetail>();
		StatelessSession statelesssession = sessionFactory.openStatelessSession();
		Transaction tx = statelesssession.beginTransaction();
		
		for(TeacherAssessmentStatus teacherAssessmentStatus : lstTeacherAssessmentStatus){			
			lstTeacherDetails.add(teacherAssessmentStatus.getTeacherDetail());
			teacherAssessmentStatus.setCgUpdated(true);
			statelesssession.update(teacherAssessmentStatus);
		}
		saveRawDataOfTeacher(lstTeacherDetails, null);		
		
		List<DomainMaster> lstDomainMasters = domainMasterDAO.findAll();
		List<Integer> lstScore = new LinkedList<Integer>();
		List<RawDataForDomain> lstRawDataForDomains = null;
		Integer score = null;
		for(DomainMaster domainMaster : lstDomainMasters){
			lstRawDataForDomains = rawDataForDomainDAO.findByDomainAndTeachers(domainMaster, lstTeacherDetails);
			lstScore = new LinkedList<Integer>();
			for(RawDataForDomain rawDataForDomain : lstRawDataForDomains){
				score = new Integer(""+Math.round(rawDataForDomain.getScore()));
				lstScore.add(score);
			}
			insertOrUpdatePercentile(lstScore, domainMaster);
		}
		tx.commit();
		System.out.println("domain data inserted ...............");
		////////////////////////vishwanath Competency percentile ////////////////////////
		tx = statelesssession.beginTransaction();
		List<CompetencyMaster> lstcompetencyMaster = competencyMasterDAO.getCompetenciesForPDR(true);
		List<Integer> lstScoreCompetency = new LinkedList<Integer>();
		List<RawDataForCompetency> lstRawDataForCompetencies = null;
		score = null;
		for(CompetencyMaster competencyMaster : lstcompetencyMaster){
			lstRawDataForCompetencies = rawDataForCompetencyDAO.findByCompetencyAndTeachers(competencyMaster, lstTeacherDetails);
			lstScore = new LinkedList<Integer>();
			for(RawDataForCompetency rawDataForCompetency : lstRawDataForCompetencies){
				score = new Integer(""+Math.round(rawDataForCompetency.getScore()));
				lstScoreCompetency.add(score);
			}
			insertOrUpdatePercentileCompetencyWise(lstScoreCompetency, competencyMaster);
		}
		tx.commit();
		//////////////////////////////////////////////////
		statelesssession.close();
	}
	
	public void updateJobPercentileByUserOrThread(){		
		try {
			setZScore();
			System.out.println(":::updateJobPercentileByUserOrThread:::");
			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;						
			Map<String, DomainMaster> mapDomainMaster = new HashMap<String, DomainMaster>();
			for(DomainMaster domainMaster:lstDomain){
				mapDomainMaster.put(""+domainMaster.getDomainId(), domainMaster);
			}

			List<TeacherDetail> lstTeacherDetail = new LinkedList<TeacherDetail>();			
			Set<TeacherDetail> setTeacherDetails = new HashSet<TeacherDetail>();			
			
			
			List<JobForTeacher> lstJobForTeachers = jobForTeacherDAO.findJobForUpdateInCG();
			for(JobForTeacher jft: lstJobForTeachers){
				setTeacherDetails.add(jft.getTeacherId());
			}
			
			
			lstTeacherDetail = new ArrayList<TeacherDetail>(setTeacherDetails);
			//System.out.println("lstTeacherDetail.size()"+lstTeacherDetail.size());
			
			List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentDoneByTeachers(lstTeacherDetail);
			
			lstTeacherDetail = new LinkedList<TeacherDetail>();
			for(TeacherAssessmentStatus teacherAssessmentStatus: lstTeacherAssessmentStatus){
				lstTeacherDetail.add(teacherAssessmentStatus.getTeacherDetail());
			}
			//System.out.println("lstTeacherDetail:::::::::::::::::::> "+lstTeacherDetail.size());
			
			Map<Integer, Boolean> baseCompleted = new HashMap<Integer, Boolean>();
			
			Map<String, Double> mapScore = new HashMap<String, Double>();
			List<RawDataForDomain>   lstRawDataForDomain = rawDataForDomainDAO.findByTeachers(lstTeacherDetail);			
			for(RawDataForDomain rawDataForDomain : lstRawDataForDomain){
				mapScore.put(rawDataForDomain.getTeacherDetail().getTeacherId()+"@"+rawDataForDomain.getDomainMaster().getDomainId(), rawDataForDomain.getScore());
				baseCompleted.put(rawDataForDomain.getTeacherDetail().getTeacherId(), true);
			}
			//System.out.println(mapScore);
			
			List<JobForTeacher> lstJFT = new ArrayList<JobForTeacher>(lstJobForTeachers);
			for(JobForTeacher jft : lstJFT){
				if(!lstTeacherDetail.contains(jft.getTeacherId())){
					lstJobForTeachers.remove(jft);
				}
			}
			//System.out.println("JFT.size"+ lstJobForTeachers.size());
			
			Map<String,List<Integer>> mapJobScore = new HashMap<String, List<Integer>>();
			Map<String, JobOrder> mapJobOrder = new HashMap<String, JobOrder>();
						
			List<Integer> lstScore = null;
			String key = "";
			
			/*String[] statuss = {"comp","icomp","hird","rem","vlt"};
			List<StatusMaster> lstStatusMasters = null;
			try{
				lstStatusMasters = Utility.getStaticMasters(statuss);
			}catch (Exception e) {
				e.printStackTrace();
			}*/
			StatelessSession statelesssession = sessionFactory.openStatelessSession();
			Transaction tx = statelesssession.beginTransaction();
			/*List<JobForTeacher> JobForTeacherList = jobForTeacherDAO.findJobForUpdateVltInCG(lstStatusMasters);
			
			Set<TeacherDetail> teacherDetails = new HashSet<TeacherDetail>();	
			List<TeacherDetail> teacherDetailList = new LinkedList<TeacherDetail>();	
			
			for(JobForTeacher jft2nd: JobForTeacherList){
				teacherDetails.add(jft2nd.getTeacherId());
			}
			teacherDetailList = new ArrayList<TeacherDetail>(teacherDetails);
			List<TeacherAssessmentStatus> lstTeacherAssessmentStatusVlt = teacherAssessmentStatusDAO.findAssessmentVltByTeachers(teacherDetailList);
			
			//updateVltCgUpdate(statelesssession,JobForTeacherList,lstTeacherAssessmentStatusVlt);*/
			
			for(JobForTeacher jft:lstJobForTeachers){
				if(baseCompleted.get(jft.getTeacherId().getTeacherId())!=null)
				{
					System.out.println("jobId:"+jft.getJobId().getJobId()+" teacherId:"+jft.getTeacherId().getTeacherId());
					jft.setCgUpdated(true);
					statelesssession.update(jft);
					
					for(DomainMaster domainMaster : lstDomain){
						key = jft.getJobId().getJobId()+"##"+domainMaster.getDomainId();
						if(mapJobScore.get(key) == null){
							mapJobScore.put(key, new LinkedList<Integer>());
							mapJobOrder.put(""+jft.getJobId().getJobId(), jft.getJobId());
						}
						lstScore = mapJobScore.get(key);
						//System.out.println("TeacherId"+jft.getTeacherId().getTeacherId());
						//System.out.println(mapScore.get(jft.getTeacherId().getTeacherId()+"@"+domainMaster.getDomainId())+"  "+jft.getTeacherId().getTeacherId()+"@"+domainMaster.getDomainId());
						if(mapScore.get(jft.getTeacherId().getTeacherId()+"@"+domainMaster.getDomainId())!=null){
							lstScore.add(new Integer(""+Math.round(mapScore.get(jft.getTeacherId().getTeacherId()+"@"+domainMaster.getDomainId()))));
						}
					}
				}
			}
			
			//System.out.println("mapJobScore>>"+mapJobScore);
			//System.out.println("mapJobScore.size^^^^"+mapJobScore.size());
			System.gc();			
					
			for (Map.Entry<String,List<Integer>> entry : mapJobScore.entrySet()){
			    //System.out.println("("+(++i)+")Key>" + entry.getKey() + " ,Value>" + entry.getValue());			
			    insertOrUpdatePercentileJobByThreadOrUser(entry.getValue(),mapDomainMaster.get(entry.getKey().split("##")[1]),mapJobOrder.get(entry.getKey().split("##")[0]));
			}
			tx.commit();
			statelesssession.close();
			
			txHome.commit();
			statelesssessionHome.close();
		} 
		catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public void updateVltCgUpdate(StatelessSession statelesssession,List<JobForTeacher> lstJobForTeachers,List<TeacherAssessmentStatus> lstTeacherAssessmentStatusVlt){
		System.out.println("lstJobForTeachers inside:::"+lstJobForTeachers.size());
		System.out.println("lstTeacherAssessmentStatusVlt inside:::"+lstTeacherAssessmentStatusVlt.size());
		if(lstTeacherAssessmentStatusVlt.size()>0)
		for(JobForTeacher jft:lstJobForTeachers){
			for(TeacherAssessmentStatus teacherAssessmentStatus:lstTeacherAssessmentStatusVlt){
				if(teacherAssessmentStatus.getTeacherDetail().equals(jft.getTeacherId())){
					//System.out.println(jft.getTeacherId().getTeacherId()+"::::Teacher Id::: inside :::: Job Id:::::"+jft.getJobId());
					jft.setCgUpdated(true);
					statelesssession.update(jft);
				}
			}
		}
		
	}
	
	public void updateJobWisePercentileOneTime(){
		try {
			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			
			Map<String, Double> mapScore = new HashMap<String, Double>();
			List<RawDataForDomain>   lstRawDataForDomain = rawDataForDomainDAO.findAll();
			
			for(RawDataForDomain rawDataForDomain : lstRawDataForDomain){
				mapScore.put(rawDataForDomain.getTeacherDetail().getTeacherId()+"@"+rawDataForDomain.getDomainMaster().getDomainId(), rawDataForDomain.getScore());
			}
			//System.out.println(mapScore);
			
			JobOrder jOrder = new JobOrder();
			jOrder.setJobId(0);
			List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentDone(null,jOrder);
			List<TeacherDetail> lstTeacherDetail = new LinkedList<TeacherDetail>();
			Set<TeacherDetail> setTeacherDetails = new HashSet<TeacherDetail>();			
			for(TeacherAssessmentStatus teacherAssessmentStatus : lstTeacherAssessmentStatus){			
				setTeacherDetails.add(teacherAssessmentStatus.getTeacherDetail());
			}
			
			lstTeacherDetail = new ArrayList<TeacherDetail>(setTeacherDetails);
			setTeacherDetails = null;
			lstTeacherAssessmentStatus = null;
			List<JobForTeacher> lstJobForTeachers = jobForTeacherDAO.findJobForCG(lstTeacherDetail);			
			//System.out.println(lstJobForTeachers.size());
			Map<String,List<Integer>> mapJobScore = new HashMap<String, List<Integer>>();
			Map<String, JobOrder> mapJobOrder = new HashMap<String, JobOrder>();
			Map<String, DomainMaster> mapDomainMaster = new HashMap<String, DomainMaster>();
			for(DomainMaster domainMaster:lstDomain){
				mapDomainMaster.put(""+domainMaster.getDomainId(), domainMaster);
			}
			
			List<Integer> lstScore = null;
			String key = "";
			
			StatelessSession statelesssession = sessionFactory.openStatelessSession();
			Transaction tx = statelesssession.beginTransaction();
			
			for(JobForTeacher jft:lstJobForTeachers){
				jft.setCgUpdated(true);
				statelesssession.update(jft);
				for(DomainMaster domainMaster : lstDomain){
					key = jft.getJobId().getJobId()+"##"+domainMaster.getDomainId();
					if(mapJobScore.get(key) == null){
						mapJobScore.put(key, new LinkedList<Integer>());
						mapJobOrder.put(""+jft.getJobId().getJobId(), jft.getJobId());
					}
					lstScore = mapJobScore.get(key);
					//System.out.println("TeacherId"+jft.getTeacherId().getTeacherId());
					System.out.println(mapScore.get(jft.getTeacherId().getTeacherId()+"@"+domainMaster.getDomainId())+"  "+jft.getTeacherId().getTeacherId()+"@"+domainMaster.getDomainId());
					if(mapScore.get(jft.getTeacherId().getTeacherId()+"@"+domainMaster.getDomainId())!=null){
						lstScore.add(new Integer(""+Math.round(mapScore.get(jft.getTeacherId().getTeacherId()+"@"+domainMaster.getDomainId()))));
					}
				}
			}
			
			//System.out.println("mapJobScore>>"+mapJobScore);
			//System.out.println("mapJobScore.size^^^^"+mapJobScore.size());
			System.gc();			
			setZScore();		
			for (Map.Entry<String,List<Integer>> entry : mapJobScore.entrySet()) {
			   //System.out.println("("+(++i)+")Key>" + entry.getKey() + " ,Value>" + entry.getValue());			
			    insertOrUpdatePercentileByJobOneTime(entry.getValue(),mapDomainMaster.get(entry.getKey().split("##")[1]),mapJobOrder.get(entry.getKey().split("##")[0]));
			}
			tx.commit();
			statelesssession.close();
			
			txHome.commit();
			statelesssessionHome.close();
		} 
		catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public Connection getConnection(){
		Connection conn=null;
		try{
			Class.forName("com.mysql.jdbc.Driver");			
			conn =  (Connection) DriverManager.getConnection(Utility.getValueOfHBPropByKey("hibernate.connection.url"),Utility.getValueOfHBPropByKey("hibernate.connection.username"),Utility.getValueOfHBPropByKey("hibernate.connection.password"));			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public String getColorName(Double teacherPercentile){
		String colorName="";
		if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile1Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile1Max"))){
			colorName=Utility.getValueOfPropByKey("decile1");
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile2Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile2Max"))){
			colorName=Utility.getValueOfPropByKey("decile2");
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile3Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile3Max"))){
			colorName=Utility.getValueOfPropByKey("decile3");
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile4Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile4Max"))){
			colorName=Utility.getValueOfPropByKey("decile4");
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile5Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile5Max"))){
			colorName=Utility.getValueOfPropByKey("decile5");
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile6Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile6Max"))){
			colorName=Utility.getValueOfPropByKey("decile6");
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile7Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile7Max"))){
			colorName=Utility.getValueOfPropByKey("decile7");
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile8Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile8Max"))){
			colorName=Utility.getValueOfPropByKey("decile8");
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile9Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile9Max"))){
			colorName=Utility.getValueOfPropByKey("decile9");
		}
		return colorName;
	}
	
	public Object[] getDeciles(Double teacherPercentile){
		String colorName="";
		Object[] obj = new Object[3];
		if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile1Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile1Max"))){
			colorName=Utility.getValueOfPropByKey("decile1");
			obj[0]=colorName;obj[1]=Double.parseDouble(Utility.getValueOfPropByKey("decile1Min"));obj[2]=Double.parseDouble(Utility.getValueOfPropByKey("decile1Max"));
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile2Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile2Max"))){
			colorName=Utility.getValueOfPropByKey("decile2");
			obj[0]=colorName;obj[1]=Double.parseDouble(Utility.getValueOfPropByKey("decile2Min"));obj[2]=Double.parseDouble(Utility.getValueOfPropByKey("decile2Max"));
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile3Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile3Max"))){
			colorName=Utility.getValueOfPropByKey("decile3");
			obj[0]=colorName;obj[1]=Double.parseDouble(Utility.getValueOfPropByKey("decile3Min"));obj[2]=Double.parseDouble(Utility.getValueOfPropByKey("decile3Max"));
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile4Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile4Max"))){
			colorName=Utility.getValueOfPropByKey("decile4");
			obj[0]=colorName;obj[1]=Double.parseDouble(Utility.getValueOfPropByKey("decile4Min"));obj[2]=Double.parseDouble(Utility.getValueOfPropByKey("decile4Max"));
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile5Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile5Max"))){
			colorName=Utility.getValueOfPropByKey("decile5");
			obj[0]=colorName;obj[1]=Double.parseDouble(Utility.getValueOfPropByKey("decile5Min"));obj[2]=Double.parseDouble(Utility.getValueOfPropByKey("decile5Max"));
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile6Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile6Max"))){
			colorName=Utility.getValueOfPropByKey("decile6");
			obj[0]=colorName;obj[1]=Double.parseDouble(Utility.getValueOfPropByKey("decile6Min"));obj[2]=Double.parseDouble(Utility.getValueOfPropByKey("decile6Max"));
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile7Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile7Max"))){
			colorName=Utility.getValueOfPropByKey("decile7");
			obj[0]=colorName;obj[1]=Double.parseDouble(Utility.getValueOfPropByKey("decile7Min"));obj[2]=Double.parseDouble(Utility.getValueOfPropByKey("decile7Max"));
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile8Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile8Max"))){
			colorName=Utility.getValueOfPropByKey("decile8");
			obj[0]=colorName;obj[1]=Double.parseDouble(Utility.getValueOfPropByKey("decile8Min"));obj[2]=Double.parseDouble(Utility.getValueOfPropByKey("decile8Max"));
		}else if(teacherPercentile>=Double.parseDouble(Utility.getValueOfPropByKey("decile9Min")) && teacherPercentile<=Double.parseDouble(Utility.getValueOfPropByKey("decile9Max"))){
			colorName=Utility.getValueOfPropByKey("decile9");
			obj[0]=colorName;obj[1]=Double.parseDouble(Utility.getValueOfPropByKey("decile9Min"));obj[2]=Double.parseDouble(Utility.getValueOfPropByKey("decile9Max"));
		}
		return obj;
	}

	public List<PercentileZscoreTscore> populateZScoreMap(List<TmpPercentileWiseZScore> tmpPercentileWiseZScoreList){
		List<PercentileZscoreTscore> pZTscore= new ArrayList<PercentileZscoreTscore>();
		try {
			Map<Double, Double> mapData = new HashMap<Double, Double>();
			for(TmpPercentileWiseZScore tmpPercentileWiseZScore: tmpPercentileWiseZScoreList){	
				mapData.put(tmpPercentileWiseZScore.getPercentile(),tmpPercentileWiseZScore.getzScore());
				PercentileZscoreTscore pzTscoreObj= new PercentileZscoreTscore();
				pzTscoreObj.setPercentile(tmpPercentileWiseZScore.getPercentile());
				pzTscoreObj.setzScore(Double.parseDouble(Utility.roundTwoDecimalsAsString(tmpPercentileWiseZScore.getzScore())));
				pzTscoreObj.settScore(Double.parseDouble(Utility.roundOneDecimalsAsString((50+10*tmpPercentileWiseZScore.getzScore()))));
				pZTscore.add(pzTscoreObj);
			}
			Double prePercentile=0.00;
			Double postPercentile=0.00;
			Double preZscore = 0.00;
			Double postZscore = 0.00;
			Double zScoreDiff = 0.00;
			Double percentileDiff = 0.00;
			Double diffForOnePer=0.00;
			
			for(double i=.01;i<100;i+=0.01){	
				if(mapData.get(new Double(Utility.roundTwoDecimalsAsString(i)))==null || mapData.get(new Double(Utility.roundTwoDecimalsAsString(i)))==0.0){
					prePercentile = new Double(Utility.roundTwoDecimalsAsString(i-0.01));					
					preZscore = mapData.get(prePercentile);
					while(mapData.get(new Double(Utility.roundTwoDecimalsAsString(i)))==null || mapData.get(new Double(Utility.roundTwoDecimalsAsString(i)))==0.0){
						i+=0.01;
					}
					postPercentile = new Double(Utility.roundTwoDecimalsAsString(i));
					postZscore = mapData.get(postPercentile);
					zScoreDiff = new Double(Utility.roundTwoDecimalsAsString(postZscore-preZscore));
					percentileDiff = new Double(Utility.roundTwoDecimalsAsString(postPercentile-prePercentile));
					diffForOnePer = (.01)*(zScoreDiff/percentileDiff);
					
					prePercentile = new Double(Utility.roundTwoDecimalsAsString(prePercentile + 0.01));
					preZscore=preZscore+diffForOnePer;
					while(prePercentile < postPercentile){
						PercentileZscoreTscore pzTscoreObj= new PercentileZscoreTscore();
						pzTscoreObj.setPercentile(prePercentile);
						pzTscoreObj.setzScore(Double.parseDouble(Utility.roundTwoDecimalsAsString(preZscore)));
						pzTscoreObj.settScore(Double.parseDouble(Utility.roundOneDecimalsAsString((50+10*preZscore))));
						pZTscore.add(pzTscoreObj);
						prePercentile = new Double(Utility.roundTwoDecimalsAsString(prePercentile + 0.01));
						preZscore=preZscore+diffForOnePer;
					}
					
				}
			}	
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return pZTscore;
	}
	
	public double getPercentile(List<PercentileZscoreTscore> percentileZscoreTscoresList,double normScore){
		double percentile=0.0;
		boolean percentileFlag=false;
		Collections.sort(percentileZscoreTscoresList,PercentileZscoreTscore.percentileZscoreTscore);
		for(PercentileZscoreTscore pztObj:percentileZscoreTscoresList){
			if(pztObj.gettScore().equals(normScore)){
				percentile=pztObj.getPercentile();
				percentileFlag=true;
				break;
			}
		}
		if(percentileFlag==false){
			int minCounter=0;
			int maxCounter=0;
	    	double minLength=0;
			double maxLength=0;
			double preMinLength=0;
			double preMaxLength=0;
			double pretScore=0.00;
			double minPercentile=0.00;
			double maxPercentile=0.00;
			for(PercentileZscoreTscore pztObj:percentileZscoreTscoresList){
				pretScore=pztObj.gettScore();
				if(normScore>pretScore){
					minLength=normScore-pretScore;
					minCounter++;
				}
				if(normScore<pretScore){
					maxCounter++;
					maxLength=pretScore-normScore;
				}
				if(minCounter==1){
					minCounter++;
					preMinLength=minLength;
					minPercentile=pztObj.getPercentile();
				}
				if(maxCounter==1){
					maxCounter++;
					preMaxLength=maxLength;
					maxPercentile=pztObj.getPercentile();
				}
				if(preMinLength>minLength){
					preMinLength=minLength;
					minPercentile=pztObj.getPercentile();
				}
				if(preMaxLength>maxLength){
					preMaxLength=maxLength;
					maxPercentile=pztObj.getPercentile();
				}
				if(preMinLength<=preMaxLength){
					if(preMinLength!=0){
						percentile=minPercentile;
					}else{
						percentile=maxPercentile;
					}
				}else{
					percentile=maxPercentile;
				}
			}
		}
		return percentile;
	}
	
	public void removeDuplicateQQ(){
		try {
			
			Connection connection = getConnection();
			Statement stmt=connection.createStatement();
			PreparedStatement pstmt=null;
			Connection con = getConnection();
			
			ResultSet rs = stmt.executeQuery("SELECT teacherId, districtId, jobId FROM ( SELECT teacherId, districtId, jobId FROM  `teacheranswerdetailsfordistrictspecificquestions` ORDER BY answerId DESC ) AS  `teacheranswerdetailsfordistrictspecificquestions` GROUP BY districtId, teacherId");
			while(rs.next()){
				pstmt = con.prepareStatement("delete FROM `teacheranswerdetailsfordistrictspecificquestions` WHERE teacherId=? and districtId=? and jobId not in(?);");
				pstmt.setString(1, rs.getString("teacherId"));
				pstmt.setString(2, rs.getString("districtId"));
				pstmt.setString(3, rs.getString("jobId"));
				pstmt.execute();
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}		
	}
}
