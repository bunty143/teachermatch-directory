package tm.services.report;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.cgreport.TeacherSendMessageEmail;
import tm.bean.user.PersonalEmailTrack;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.user.PersonalEmailTrackDAO;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class TeacherSendMessageAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	String lnkClickDownload=Utility.getLocaleValuePropByKey("lnkClickDownload", locale);

	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}
	
	@Autowired
	private MessageToTeacherDAO messageToTeacherDAO;
	public void setMessageToTeacherDAO(MessageToTeacherDAO messageToTeacherDAO) {
		this.messageToTeacherDAO = messageToTeacherDAO;
	}
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private PersonalEmailTrackDAO personalEmailTrackDAO;
	
	public String getEmailAddress(Long[] jobForTeacherIds)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		
		List<JobForTeacher> jobForTeachersList=null;
		
		if(jobForTeacherIds.length>0)
		{
			//Criterion criterion = Restrictions.in("jobForTeacherId",jobForTeacherIds);	
			//jobForTeachersList =	jobForTeacherDAO.findByCriteria(criterion);
			
			jobForTeachersList=jobForTeacherDAO.getJobForTeacherIDs(jobForTeacherIds);
			
		}
		
		String sEmailAddress="";
		if(jobForTeachersList.size()>0)
			for(JobForTeacher pojo:jobForTeachersList)
				if(pojo!=null && pojo.getTeacherId()!=null)
					if(pojo.getTeacherId().getEmailAddress()!=null && !pojo.getTeacherId().getEmailAddress().equalsIgnoreCase(""))
						sEmailAddress=sEmailAddress+pojo.getTeacherId().getEmailAddress()+",";
		
		if(!sEmailAddress.equalsIgnoreCase("") && sEmailAddress.length()>1)
			sEmailAddress=sEmailAddress.substring(0, sEmailAddress.length()-1);
		
		String[] sEmailArray=sEmailAddress.split(","); 
		Arrays.sort(sEmailArray);
		
		return Arrays.toString(sEmailArray).replace("[", "").replace("]", "");
	}
	
	
	public String getTeacherIds(Long[] jobForTeacherIds)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		
		List<JobForTeacher> jobForTeachersList=null;
		
		if(jobForTeacherIds.length>0)
			jobForTeachersList=jobForTeacherDAO.getJobForTeacherIDs(jobForTeacherIds);
		
		String sTeacherIds="";
		if(jobForTeachersList.size()>0)
			for(JobForTeacher pojo:jobForTeachersList)
				if(pojo!=null && pojo.getTeacherId()!=null)
					if(pojo.getTeacherId().getTeacherId()!=null)
						sTeacherIds=sTeacherIds+pojo.getTeacherId().getTeacherId()+",";
		
		if(!sTeacherIds.equalsIgnoreCase("") && sTeacherIds.length()>1)
			sTeacherIds=sTeacherIds.substring(0, sTeacherIds.length()-1);
		return sTeacherIds;
	}
	
	public Boolean saveMessage(Long[] jobForTeacherIds,String messageSubject,String messageSend,int jobId,String fileName,int msgType,String document)
	{
		System.out.println("Document="+document+" 2");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));		
		}
		
		UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
		JobOrder   jobOrder =null;
		//Ajay Jain
		int districtId=0;
		int headQuarterId=0;
		if(userMaster.getDistrictId()!=null)
			districtId = userMaster.getDistrictId().getDistrictId();
		else if(userMaster.getHeadQuarterMaster()!=null)
			headQuarterId=userMaster.getHeadQuarterMaster().getHeadQuarterId();
		
		List<TeacherSendMessageEmail> lstteacherSendMessageEmails=new ArrayList<TeacherSendMessageEmail>();
		
		
		if(jobId!=0)
			jobOrder=jobOrderDAO.findById(jobId, false, false);
		
		try{
			if(msgType==2)
			{
				   String filePath = "";
					 String target="";
						String source="";
						String path="";
				List<JobForTeacher> lstjobforteacher=jobForTeacherDAO.getJobForTeacherIDs(jobForTeacherIds);
				
	        	SessionFactory sessionFactory=messageToTeacherDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	        	Transaction txOpen =statelesSsession.beginTransaction();
	        	String attachmentUrl = "";
	        	String link="";	        	
	        	if(document!=null && !document.equals("")){
	        		System.out.println("==========attachment path======");
	        		filePath = districtId+"/DistAttachmentFile";
	        		if(userMaster.getDistrictId()!=null)
	        		{
		        		source 	= 	Utility.getValueOfPropByKey("districtRootPath")+filePath+"/"+document;
						target 	= 	context.getServletContext().getRealPath("/")+"/district/"+filePath+"/";
	        		}
	        		else if(userMaster.getHeadQuarterMaster()!=null)
	        		{
		        		source 	= 	Utility.getValueOfPropByKey("headQuarterRootPath")+filePath+"/"+document;
						target 	= 	context.getServletContext().getRealPath("/")+"/headquarter/"+filePath+"/";
	        		}
					
					
					File sourceFile = 	new File(source);
					File targetDir 	= 	new File(target);
					if(!targetDir.exists())
						targetDir.mkdirs();
					File targetFile = new File(targetDir+"/"+sourceFile.getName());
					FileUtils.copyFile(sourceFile, targetFile);
					if(userMaster.getDistrictId()!=null)
						path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+filePath+"/"+document;
	        		else if(userMaster.getHeadQuarterMaster()!=null)
	        			path = Utility.getValueOfPropByKey("contextBasePath")+"/headquarter/"+filePath+"/"+document;
										 
	        		attachmentUrl=Utility.getShortURL(path);
	        		System.out.println(attachmentUrl);
	        		link ="<a href='"+attachmentUrl+"'>"+lnkClickDownload+"</a>";
	        	}
	        	
	        	List<PersonalEmailTrack> lstPersonalEmailTrack=new ArrayList<PersonalEmailTrack>();
				Map<String, PersonalEmailTrack> emailMap=new HashMap<String, PersonalEmailTrack>();	
	        	if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictEmailFlag())
				{						
					lstPersonalEmailTrack=personalEmailTrackDAO.getUserByDistrict(districtId);
					System.out.println("lstPersonalEmailTrack==="+lstPersonalEmailTrack.size());
					if(lstPersonalEmailTrack!=null && lstPersonalEmailTrack.size()>0)
					{
						for(PersonalEmailTrack pet:lstPersonalEmailTrack)
						{					
						emailMap.put(pet.getDistrictId()+"#"+pet.getUserId(), pet);
						}
					}	
					System.out.println("emailMap size==========="+emailMap.size());						
						System.out.println("District id and user id from database==================="+districtId+"#"+userMaster.getUserId());				
									
				}				
	        	      	
	        	
	        	   	
	        	if(lstjobforteacher!=null)
	        	{
	        		for(JobForTeacher pojo :lstjobforteacher)
	        		{
	        			String messageSend1=messageSend;
	        			
	        			if(messageSend.contains("&lt; Teacher First Name &gt;") || messageSend.contains("&lt; Teacher Last Name &gt;") || messageSend.contains("&lt; Teacher Email &gt;")){
        					messageSend1	=	messageSend1.replaceAll("&lt; Teacher First Name &gt;",pojo.getTeacherId().getFirstName());
        					messageSend1	=	messageSend1.replaceAll("&lt; Teacher Last Name &gt;",pojo.getTeacherId().getLastName());
        					messageSend1	=	messageSend1.replaceAll("&lt; Teacher Email &gt;",pojo.getTeacherId().getEmailAddress());
        				}
	        			if(messageSend.contains("&lt; Admin First Name &gt;") || messageSend.contains("&lt; Admin Last Name &gt;") || messageSend.contains("&lt; Admin Email &gt;")){
        					messageSend1	=	messageSend1.replaceAll("&lt; Admin First Name &gt;",userMaster.getFirstName());
        					messageSend1	=	messageSend1.replaceAll("&lt; Admin Last Name &gt;",userMaster.getLastName());
        					messageSend1	=	messageSend1.replaceAll("&lt; Admin Email &gt;",userMaster.getEmailAddress());
        				}
	        			if(jobOrder!=null && messageSend.contains("&lt; Job Title &gt;")){
        					messageSend1	=	messageSend1.replaceAll("&lt; Job Title &gt;",jobOrder.getJobTitle());
        				}
	        			if(jobOrder!=null && messageSend.contains("&lt; District or School Name &gt;")){
	        				if(userMaster!=null && userMaster.getEntityType()==2)
        					messageSend1	=	messageSend1.replaceAll("&lt; District or School Name &gt;",jobOrder.getDistrictMaster().getDistrictName());
	        				else if(userMaster!=null && userMaster.getEntityType()==3)
	        					messageSend1	=	messageSend1.replaceAll("&lt; District or School Name &gt;",userMaster.getSchoolId().getSchoolName());
        				}
	        			if(jobOrder!=null && messageSend.contains("&lt; District or School Address &gt;")){
	        				if(userMaster!=null && userMaster.getEntityType()==2)
	        				messageSend1	=	messageSend1.replaceAll("&lt; District or School Address &gt;",jobOrder.getDistrictMaster().getAddress());
	        				else if(userMaster!=null && userMaster.getEntityType()==3)
		        				messageSend1	=	messageSend1.replaceAll("&lt; District or School Address &gt;",userMaster.getSchoolId().getAddress());
        				}
	        			
	        			System.out.println("::::::::::::::::::::::::::::::::Formatted Message:"+messageSend1);
	        			System.out.println("<br>"+attachmentUrl);
	        			MessageToTeacher  messageToTeacher= new MessageToTeacher();
        				messageToTeacher.setTeacherId(pojo.getTeacherId());
        				if(jobId!=0)
        					messageToTeacher.setJobId(jobOrder);
        				
        				messageToTeacher.setTeacherEmailAddress(pojo.getTeacherId().getEmailAddress());
        				messageToTeacher.setMessageSubject(messageSubject);
        				messageToTeacher.setMessageSend(messageSend1+"<br><br>"+link);
        				messageToTeacher.setSenderId(userMaster);
        				messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
        				messageToTeacher.setEntityType(userMaster.getEntityType());
        				messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
        				if(document!=null && !document.equals(""))
        					messageToTeacher.setDocument(document);
        				if(!fileName.equals(""))
        					messageToTeacher.setMessageFileName(fileName);
        				statelesSsession.insert(messageToTeacher);
        				
        				String email_filePath=Utility.getValueOfPropByKey("communicationRootPath")+"/message/"+pojo.getTeacherId().getTeacherId()+"/"+fileName;
        				
        				
        				TeacherSendMessageEmail teacherSendMessageEmail=new TeacherSendMessageEmail();
        				teacherSendMessageEmail.setToEmailId(pojo.getTeacherId().getEmailAddress());
        				
        				if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictEmailFlag())
        				{
        					if(emailMap!=null && emailMap.size()>0 && emailMap.containsKey(districtId+"#"+userMaster.getUserId()))
        						{						
        							teacherSendMessageEmail.setFromEmailId(emailMap.get(districtId+"#"+userMaster.getUserId()).getPersonalEmailAddress());						
        						}
        						else
        							{						
        								teacherSendMessageEmail.setFromEmailId("");
        							}	
        				}
        				else
        				{
        				teacherSendMessageEmail.setFromEmailId("");
        				}
        				
        				
        				teacherSendMessageEmail.setMessageSubject(messageSubject);
        				        				
        				teacherSendMessageEmail.setMessageSendBody(messageSend1+"<br><br>"+link);
        				
        				if(userMaster!=null)
        					teacherSendMessageEmail.setUserMaster(userMaster);
        				
        				if(!fileName.equals(""))
        				{
        					teacherSendMessageEmail.setFileName(fileName);
        					teacherSendMessageEmail.setEmail_filePath(email_filePath);
        				}
        				else
        				{
        					teacherSendMessageEmail.setFileName("");
        					teacherSendMessageEmail.setEmail_filePath("");
        				}
        				
        				lstteacherSendMessageEmails.add(teacherSendMessageEmail);
	        		 }
	        	 }
	        	 
	        	 txOpen.commit();
	        	 //statelesSsession.close();
	        	 
	        	 sessionFactory=jobForTeacherDAO.getSessionFactory();
				 statelesSsession = sessionFactory.openStatelessSession();
	        	 Transaction txJft =statelesSsession.beginTransaction();
	        	 
	        	 if(lstjobforteacher!=null)
	        	 {
	        		 for(JobForTeacher pojo :lstjobforteacher)
	        		 {
						JobForTeacher jobForTeacher=pojo;
						jobForTeacher.setLastActivity("Message Sent");
						jobForTeacher.setLastActivityDate(new Date());
						jobForTeacher.setUserMaster(userMaster);
						statelesSsession.update(jobForTeacher);
	        		 }
					}
	        	 txJft.commit();
	        	 statelesSsession.close();
	        	 
	        	 
	        	 mailSendByThread(lstteacherSendMessageEmails);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return true;
	}
	
	
	public void mailSendByThread(List<TeacherSendMessageEmail> lstteacherSendMessageEmails){
		MailSendByThread mst =new MailSendByThread(lstteacherSendMessageEmails);
		Thread currentThread = new Thread(mst);
		currentThread.start(); 
	}
	
	
	
public class MailSendByThread extends Thread{
		
	List<TeacherSendMessageEmail> lstteacherSendMessageEmail=new ArrayList<TeacherSendMessageEmail>();
		public MailSendByThread(List<TeacherSendMessageEmail> lstteacherSendMessageEmails){
			lstteacherSendMessageEmail=lstteacherSendMessageEmails;
		 }
		 public void run() {
				try {
					Thread.sleep(1000);
					int iCount=0;
					int iCountTotal=lstteacherSendMessageEmail.size();
					for(TeacherSendMessageEmail pojo :lstteacherSendMessageEmail){
						
						String toEmailId=pojo.getToEmailId();
						String messageSubject=pojo.getMessageSubject();   
						String messageSendBody=pojo.getMessageSendBody();
						
						String fileName=pojo.getFileName();
						String email_filePath=pojo.getEmail_filePath();
						
						UserMaster userMaster=pojo.getUserMaster();
						
						
						
						String fromMail="";							
						if(pojo.getFromEmailId()!=null && !pojo.getFromEmailId().equals(""))
						 fromMail= pojo.getFromEmailId();
						
						
						iCount++;
						
						if(fileName.equals("") || fileName==null)
						{
        					emailerService.sendMailAsHTMLText(toEmailId+"#"+fromMail,messageSubject,MailText.messageForDefaultFont(messageSendBody,userMaster));
        					System.out.println("Send Message via CG "+iCount+" of "+iCountTotal+" Email Id is "+toEmailId);
						}
        				else
        				{
        					emailerService.sendMailWithAttachments(toEmailId, messageSubject, fromMail, MailText.messageForDefaultFont(messageSendBody,userMaster),email_filePath,fileName);
        					System.out.println("Send Message via CG "+iCount+" of "+iCountTotal+" Email Id is "+toEmailId +" with attached file "+fileName);
        				}
						
					}
				}catch (NullPointerException en) {
					en.printStackTrace();
				}catch (InterruptedException e) {
					e.printStackTrace();
				}
		  }
	}
	
}


