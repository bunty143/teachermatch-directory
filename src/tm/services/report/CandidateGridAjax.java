package tm.services.report;

import static tm.services.district.GlobalServices.CG_REPORT_SHOW_DECLINED;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.api.UtilityAPI;
import tm.bean.DistrictPortfolioConfig;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.DistrictSpecificVideoInterviewDetails;
import tm.bean.InternalTransferCandidates;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.TeacherGeneralKnowledgeExam;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherNotes;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherSecondaryStatus;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.TeacherSubjectAreaExam;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentGroupDetails;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.cgreport.RawDataForDomain;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentAnswerDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentStatus;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.invitecandidatetoposition.InviteCandidateToPositions;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.DegreeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioAnswers;
import tm.bean.master.DistrictSpecificPortfolioOptions;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.bean.master.DomainMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.UniversityMaster;
import tm.bean.teacher.OnlineActivityQuestionSet;
import tm.bean.teacher.SpInboundAPICallRecord;
import tm.bean.user.UserMaster;
import tm.dao.DistrictPortfolioConfigDAO;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.DistrictSpecificTeacherTfaOptionsDAO;
import tm.dao.DistrictSpecificVideoInterviewDetailsDAO;
import tm.dao.DistrictTemplatesforMessagesDAO;
import tm.dao.EligibilityStatusMasterDAO;
import tm.dao.EligibilityVerificationHistroyDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.EventParticipantsListDAO;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobWiseTeacherDetailsDAO;
import tm.dao.JsiCommunicationLogDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.OctDetailsDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.SchoolSelectedByCandidateDAO;
import tm.dao.SchoolWiseCandidateStatusDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAchievementScoreDAO;
import tm.dao.TeacherAnswerDetailDAO;
import tm.dao.TeacherAssessmentQuestionDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherElectronicReferencesHistoryDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherGeneralKnowledgeExamDAO;
import tm.dao.TeacherHonorDAO;
import tm.dao.TeacherInvolvementDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherNotesDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherPreferenceDAO;
import tm.dao.TeacherRoleDAO;
import tm.dao.TeacherSecondaryStatusDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.TeacherSubjectAreaExamDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentDomainScoreDAO;
import tm.dao.assessment.AssessmentGroupDetailsDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.cgreport.PercentileCalculationDAO;
import tm.dao.cgreport.PercentileScoreByJobDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.dao.cgreport.TeacherPhoneCallHistoryDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.cgreport.TeacherStatusScoresDAO;
import tm.dao.cgreport.TmpPercentileWiseZScoreDAO;
import tm.dao.districtassessment.DistrictAssessmentJobRelationDAO;
import tm.dao.districtassessment.ExaminationCodeDetailsDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentAnswerDetailDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentStatusDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.i4.I4InterviewInvitesDAO;
import tm.dao.inviteCandidateToPositionDAO.InviteCandidateDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DegreeMasterDAO;
import tm.dao.master.DemoClassNotesDetailsDAO;
import tm.dao.master.DemoClassScheduleDAO;
import tm.dao.master.DistrictAttachmentDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.DistrictSpecificPortfolioAnswersDAO;
import tm.dao.master.DistrictSpecificPortfolioQuestionsDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.DistrictSpecificRefChkAnswersDAO;
import tm.dao.master.DistrictSpecificRefChkQuestionsDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.EligibilityMasterDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.JobWisePanelStatusDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.QqQuestionSetsDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.ReferenceCheckFitScoreDAO;
import tm.dao.master.ReferenceCheckQuestionSetDAO;
import tm.dao.master.SaveCandidatesToFolderDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.SecondaryStatusMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.StatusSpecificQuestionsDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.TeacherAnswerDetailsForDistrictSpecificQuestionsDAO;
import tm.dao.master.UniversityMasterDAO;
import tm.dao.master.UserFolderStructureDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.teacher.OnlineActivityAnswersDAO;
import tm.dao.teacher.OnlineActivityFitScoreDAO;
import tm.dao.teacher.OnlineActivityQuestionSetDAO;
import tm.dao.teacher.SpInboundAPICallRecordDAO;
import tm.dao.textfile.LicenseDAO;
import tm.dao.textfile.LicensureNBPTSDao;
import tm.dao.user.UserMasterDAO;
import tm.services.CandidateReportService;
import tm.services.CommonService;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.services.PaginationAndSorting;
import tm.services.teacher.DashboardAjax;
import tm.servlet.WorkThreadServlet;
import tm.utility.AESEncryption;
import tm.utility.IPAddressUtility;
import tm.utility.PrintDataPDF;
import tm.utility.TMCommonUtil;
import tm.utility.Utility;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONObject;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class CandidateGridAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	String lblNumberCandidates1=Utility.getLocaleValuePropByKey("lblNumberCandidates1", locale);
	String lblPDF=Utility.getLocaleValuePropByKey("lblPDF", locale);
	String lblSearch=Utility.getLocaleValuePropByKey("lblSearch", locale);
	String blLegen=Utility.getLocaleValuePropByKey("blLegen", locale);
	String lblAttitudinal=Utility.getLocaleValuePropByKey("lblAttitudinal", locale);
	String lblCompetencyDomainDefinitions=Utility.getLocaleValuePropByKey("lblCompetencyDomainDefinitions", locale);
	String msgReprepresentsattitudinalfactorsaffecting=Utility.getLocaleValuePropByKey("msgReprepresentsattitudinalfactorsaffecting", locale);
	String msgCognitiveAbility=Utility.getLocaleValuePropByKey("msgCognitiveAbility", locale);
	String msgReprepresentsattitudinalfactorsaffecting1=Utility.getLocaleValuePropByKey("msgReprepresentsattitudinalfactorsaffecting1", locale);
	String msgTeachingSkills=Utility.getLocaleValuePropByKey("msgTeachingSkills", locale);
	String msgReprepresentsattitudinalfactorsaffecting2=Utility.getLocaleValuePropByKey("msgReprepresentsattitudinalfactorsaffecting2", locale);
	String lblKey1=Utility.getLocaleValuePropByKey("lblKey1", locale);
	String lblRemoved=Utility.getLocaleValuePropByKey("lblRemoved", locale);
	String lblIndicatesRemovaAdmi=Utility.getLocaleValuePropByKey("lblIndicatesRemovaAdmi", locale);
	 String lblAvailable=Utility.getLocaleValuePropByKey("lblAvailable", locale);
	 String msgAppliedPosition=Utility.getLocaleValuePropByKey("msgAppliedPosition", locale);
	 String lblHired=Utility.getLocaleValuePropByKey("lblHired", locale);
	 String msgIndicatesHired=Utility.getLocaleValuePropByKey("msgIndicatesHired", locale);
	 String lblTimedOut=Utility.getLocaleValuePropByKey("lblTimedOut", locale);
	 String msgIndicatesviolation=Utility.getLocaleValuePropByKey("msgIndicatesviolation", locale);
	 String headCand=Utility.getLocaleValuePropByKey("headCand", locale);
	 String lblCompositeScore=Utility.getLocaleValuePropByKey("lblCompositeScore", locale);
	 String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	 String lblDetails=Utility.getLocaleValuePropByKey("lblDetails", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String optAvble=Utility.getLocaleValuePropByKey("optAvble", locale);
	 String lblResume=Utility.getLocaleValuePropByKey("lblResume", locale);
	 String lblNoResume=Utility.getLocaleValuePropByKey("lblNoResume", locale);
	 String lblPortfolio=Utility.getLocaleValuePropByKey("lblPortfolio", locale);
	 String lblNoPortfolio=Utility.getLocaleValuePropByKey("lblNoPortfolio", locale);
	 String lblCandidateNotCompletedJSI=Utility.getLocaleValuePropByKey("lblCandidateNotCompletedJSI", locale);
	 String lbllblCandidateTimedOut=Utility.getLocaleValuePropByKey("lbllblCandidateTimedOut", locale);
	 String lblJSI=Utility.getLocaleValuePropByKey("lblJSI", locale);
	 String headCoverLetr=Utility.getLocaleValuePropByKey("headCoverLetr", locale);
	 String NoJSI=Utility.getLocaleValuePropByKey("NoJSI", locale);
	 String lblNoCoverLetter1=Utility.getLocaleValuePropByKey("lblNoCoverLetter1", locale);
	 String lblNoPhone1=Utility.getLocaleValuePropByKey("lblNoPhone1", locale);
	 String lblTranscript=Utility.getLocaleValuePropByKey("lblTranscript", locale);
	 String lblPhone=Utility.getLocaleValuePropByKey("lblPhone", locale);
	 String lblNoTranscript1=Utility.getLocaleValuePropByKey("lblNoTranscript1", locale);
	 String lblCertifications=Utility.getLocaleValuePropByKey("lblCertifications", locale);
	 String lblNoCertifications1=Utility.getLocaleValuePropByKey("lblNoCertifications1", locale);
	 String headPDRep=Utility.getLocaleValuePropByKey("headPDRep", locale);
	 String msgEPIcompleted1=Utility.getLocaleValuePropByKey("msgEPIcompleted1", locale);
	 String lblPDReportnotincomplete=Utility.getLocaleValuePropByKey("lblPDReportnotincomplete", locale);
	 String headTakeAct=Utility.getLocaleValuePropByKey("headTakeAct", locale);
	 String msgSendaMessage=Utility.getLocaleValuePropByKey("msgSendaMessage", locale);
	 String msgVCNotes=Utility.getLocaleValuePropByKey("msgVCNotes", locale);
	 String lblNoCoverLetter=Utility.getLocaleValuePropByKey("lblNoCoverLetter", locale);
	 String lblNoRecord=Utility.getLocaleValuePropByKey("lblNoRecord", locale);
	 String lblPhoneNo=Utility.getLocaleValuePropByKey("lblPhoneNo", locale);
	 String headPhone=Utility.getLocaleValuePropByKey("headPhone", locale);
	 String lblMobileNumber=Utility.getLocaleValuePropByKey("lblMobileNumber", locale);
	 String lblMobile=Utility.getLocaleValuePropByKey("lblMobile", locale);
	 String lblCertiTeachExp=Utility.getLocaleValuePropByKey("lblCertiTeachExp", locale);
	 String lblNatiBoardCertiLice=Utility.getLocaleValuePropByKey("lblNatiBoardCertiLice", locale);
	 String lblNoCertiprovided=Utility.getLocaleValuePropByKey("lblNoCertiprovided", locale);
	 String lblSt=Utility.getLocaleValuePropByKey("lblSt", locale);
	 String lblYearRece=Utility.getLocaleValuePropByKey("lblYearRece", locale);
	 String lblTpe=Utility.getLocaleValuePropByKey("lblTpe", locale);
	 String msgNorecordfound=Utility.getLocaleValuePropByKey("msgNorecordfound", locale);
	 String lblDegree=Utility.getLocaleValuePropByKey("lblDegree", locale);
	 String lblFieldOfStudy=Utility.getLocaleValuePropByKey("lblFieldOfStudy", locale);
	 String lblDatesAttended=Utility.getLocaleValuePropByKey("lblDatesAttended", locale);
	 String optSchool=Utility.getLocaleValuePropByKey("optSchool", locale);
	 String lblGPA=Utility.getLocaleValuePropByKey("lblGPA", locale);
	 String lblDate=Utility.getLocaleValuePropByKey("lblDate", locale);
	 String lblSubmittedBy1=Utility.getLocaleValuePropByKey("lblSubmittedBy1", locale);
	 String lblNotes=Utility.getLocaleValuePropByKey("lblNotes", locale);
	 String lblView=Utility.getLocaleValuePropByKey("lblView	", locale);
	 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	 String msgApplicationRequirements1=Utility.getLocaleValuePropByKey("msgApplicationRequirements1", locale);
	 String lblDSPQ1=Utility.getLocaleValuePropByKey("lblDSPQ1", locale);
	 String lblJobSpeInve=Utility.getLocaleValuePropByKey("lblJobSpeInve", locale);
	 String epi=Utility.getLocaleValuePropByKey("epi", locale);
	 String lblSchoolName=Utility.getLocaleValuePropByKey("lblSchoolName", locale);
	 
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	
	@Autowired
	private AssessmentGroupDetailsDAO assessmentGroupDetailsDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	
	@Autowired
	private TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO;
	@Autowired
	private DistrictPortfolioConfigDAO districtPortfolioConfigDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) 
	{
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}

	@Autowired
	private TeacherGeneralKnowledgeExamDAO teacherGeneralKnowledgeExamDAO;
	
	@Autowired
	private TeacherSubjectAreaExamDAO teacherSubjectAreaExamDAO;
	
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) 
	{
		this.domainMasterDAO = domainMasterDAO;
	}
	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	public void setTeacherCertificateDAO(TeacherCertificateDAO teacherCertificateDAO) 
	{
		this.teacherCertificateDAO = teacherCertificateDAO;
	}
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) 
	{
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO;
	public void setTeacherAssessmentQuestionDAO(TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO) 
	{
		this.teacherAssessmentQuestionDAO = teacherAssessmentQuestionDAO;
	}

	@Autowired
	private TeacherAnswerDetailDAO teacherAnswerDetailDAO;
	public void setTeacherAnswerDetailDAO(TeacherAnswerDetailDAO teacherAnswerDetailDAO) 
	{
		this.teacherAnswerDetailDAO = teacherAnswerDetailDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private CandidateReportService candidateReportService;
	public void setCandidateReportService(CandidateReportService candidateReportService) {
		this.candidateReportService = candidateReportService;
	}

	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) 
	{
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}

	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	public void setTeacherExperienceDAO(TeacherExperienceDAO teacherExperienceDAO) 
	{
		this.teacherExperienceDAO = teacherExperienceDAO;
	}	

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) {
		this.teacherDetailDAO = teacherDetailDAO;
	}

	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}

	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}

	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}

	@Autowired
	private TeacherNotesDAO teacherNotesDAO;
	public void setTeacherNotesDAO(TeacherNotesDAO teacherNotesDAO) {
		this.teacherNotesDAO = teacherNotesDAO;
	}	
	@Autowired
	private SecondaryStatusMasterDAO secondaryStatusMasterDAO;
	public void setSecondaryStatusMasterDAO(SecondaryStatusMasterDAO secondaryStatusMasterDAO) {
		this.secondaryStatusMasterDAO = secondaryStatusMasterDAO;
	}

	@Autowired
	private TeacherSecondaryStatusDAO teacherSecondaryStatusDAO;
	public void setTeacherSecondaryStatusDAO(TeacherSecondaryStatusDAO teacherSecondaryStatusDAO) {
		this.teacherSecondaryStatusDAO = teacherSecondaryStatusDAO;
	}

	@Autowired
	private MessageToTeacherDAO messageToTeacherDAO;
	public void setMessageToTeacherDAO(MessageToTeacherDAO messageToTeacherDAO) {
		this.messageToTeacherDAO = messageToTeacherDAO;
	}

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}

	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	public void setTeacherAcademicsDAO(TeacherAcademicsDAO teacherAcademicsDAO) {
		this.teacherAcademicsDAO = teacherAcademicsDAO;
	}
	//===================================
	
	@Autowired
	private SpInboundAPICallRecordDAO spInboundAPICallRecordDAO;
	
	@Autowired
	private GeoZoneMasterDAO geoZoneMasterDAO;
	
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	
	@Autowired
	private AssessmentDomainScoreDAO assessmentDomainScoreDAO;
	
	@Autowired
	private OnlineActivityAnswersDAO onlineActivityAnswersDAO;
	
	@Autowired
	private OnlineActivityQuestionSetDAO onlineActivityQuestionSetDAO;
	
	@Autowired
	private TeacherDistrictAssessmentStatusDAO teacherDistrictAssessmentStatusDAO;
	
	@Autowired
	private TeacherDistrictAssessmentAnswerDetailDAO teacherDistrictAssessmentAnswerDetailDAO;
	
	@Autowired
	private EventParticipantsListDAO eventParticipantsListDAO;
	
	@Autowired
	private DistrictAssessmentJobRelationDAO districtAssessmentJobRelationDAO;
	
	@Autowired
	private ExaminationCodeDetailsDAO examinationCodeDetailsDAO;
	
	@Autowired
	private DistrictSpecificRefChkQuestionsDAO districtSpecificRefChkQuestionsDAO;
	
	@Autowired
	private DistrictSpecificRefChkAnswersDAO districtSpecificRefChkAnswersDAO;
	
	@Autowired
	private SchoolSelectedByCandidateDAO schoolSelectedByCandidateDAO;
	
	@Autowired
	private I4InterviewInvitesDAO interviewInvitesDAO;
	
	@Autowired
	private DistrictSpecificPortfolioAnswersDAO districtSpecificPortfolioAnswersDAO;
	
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	
	@Autowired
	private SchoolWiseCandidateStatusDAO schoolWiseCandidateStatusDAO;
	
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;

	@Autowired
	private TeacherAchievementScoreDAO teacherAchievementScoreDAO;

	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO; 

	@Autowired
	private DemoClassScheduleDAO demoClassScheduleDAO;

	@Autowired
	private TeacherHonorDAO teacherHonorDAO;
	
	@Autowired
	private TeacherRoleDAO teacherRoleDAO;
	
	@Autowired
	private TeacherInvolvementDAO teacherInvolvementDAO;
	
	@Autowired
	private DistrictAttachmentDAO districtAttachmentDAO;
	
	@Autowired
	private TeacherSendMessageAjax teacherSendMessageAjax;
	
	@Autowired
	public void setDistrictAttachmentDAO(DistrictAttachmentDAO districtAttachmentDAO) {
		this.districtAttachmentDAO = districtAttachmentDAO;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private TeacherPhoneCallHistoryDAO teacherPhoneCallHistoryDAO;

	@Autowired
	private StateMasterDAO stateMasterDAO;


	@Autowired
	private TeacherPreferenceDAO teacherPreferenceDAO;
	public void setTeacherPreferenceDAO(
			TeacherPreferenceDAO teacherPreferenceDAO) {
		this.teacherPreferenceDAO = teacherPreferenceDAO;
	}
	@Autowired
	private UniversityMasterDAO universityMasterDAO;
	public void setUniversityMasterDAO(UniversityMasterDAO universityMasterDAO) {
		this.universityMasterDAO = universityMasterDAO;
	}

	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	public void setTeacherStatusHistoryForJobDAO(
			TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO) {
		this.teacherStatusHistoryForJobDAO = teacherStatusHistoryForJobDAO;
	}

	@Autowired
	private CGReportService cGReportService;

	@Autowired
	private RawDataForDomainDAO rawDataForDomainDAO;
	public void setRawDataForDomainDAO(RawDataForDomainDAO rawDataForDomainDAO) {
		this.rawDataForDomainDAO = rawDataForDomainDAO;
	}

	@Autowired
	private PercentileCalculationDAO percentileCalculationDAO;
	public void setPercentileCalculationDAO(PercentileCalculationDAO percentileCalculationDAO) {
		this.percentileCalculationDAO = percentileCalculationDAO;
	}

	@Autowired
	private PercentileScoreByJobDAO percentileScoreByJobDAO;
	public void setPercentileScoreByJobDAO(	PercentileScoreByJobDAO percentileScoreByJobDAO) {
		this.percentileScoreByJobDAO = percentileScoreByJobDAO;
	}

	@Autowired
	private DistrictTemplatesforMessagesDAO districtTemplatesforMessagesDAO;
	public void setDistrictTemplatesforMessagesDAO(
			DistrictTemplatesforMessagesDAO districtTemplatesforMessagesDAO) {
		this.districtTemplatesforMessagesDAO = districtTemplatesforMessagesDAO;
	}

	@Autowired
	private TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO;
	public void setTmpPercentileWiseZScoreDAO(
			TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO) {
		this.tmpPercentileWiseZScoreDAO = tmpPercentileWiseZScoreDAO;
	}

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}

	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	@Autowired
	private UserFolderStructureDAO userFolderStructureDAO;
	public void setUserFolderStructureDAO(
			UserFolderStructureDAO userFolderStructureDAO) {
		this.userFolderStructureDAO = userFolderStructureDAO;
	}
	@Autowired
	private SaveCandidatesToFolderDAO saveCandidateDAO;
	public void setSaveCandidateDAO(SaveCandidatesToFolderDAO saveCandidateDAO) {
		this.saveCandidateDAO = saveCandidateDAO;
	}

	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;
	public void setCertificateTypeMasterDAO(
			CertificateTypeMasterDAO certificateTypeMasterDAO) {
		this.certificateTypeMasterDAO = certificateTypeMasterDAO;
	}
	@Autowired
	private DegreeMasterDAO degreeMasterDAO;
	public void setDegreeMasterDAO(DegreeMasterDAO degreeMasterDAO) {
		this.degreeMasterDAO = degreeMasterDAO;
	}

	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO; 
	public void setTeacherNormScoreDAO(TeacherNormScoreDAO teacherNormScoreDAO) {
		this.teacherNormScoreDAO = teacherNormScoreDAO;
	}
	@Autowired
	private DemoClassNotesDetailsDAO demoClassNotesDetailsDAO;
	public void setDemoClassNotesDetailsDAO(
			DemoClassNotesDetailsDAO demoClassNotesDetailsDAO) {
		this.demoClassNotesDetailsDAO = demoClassNotesDetailsDAO;
	}
	
	
	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;
	public void setTeacherElectronicReferencesDAO(
			TeacherElectronicReferencesDAO teacherElectronicReferencesDAO) {
		this.teacherElectronicReferencesDAO = teacherElectronicReferencesDAO;
	}
	
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}
	
	@Autowired
	private PanelScheduleDAO panelScheduleDAO;
	
	@Autowired
	private JobWisePanelStatusDAO jobWisePanelStatusDAO;
	
	
	@Autowired
	private TeacherStatusNotesDAO teacherStatusNotesDAO;
	
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	
	@Autowired
	private TeacherStatusScoresDAO teacherStatusScoresDAO;
	
	@Autowired
	private I4InterviewInvitesDAO i4InterviewInvitesDAO;
	
	@Autowired
	private TeacherElectronicReferencesHistoryDAO teacherElectronicReferencesHistoryDAO;
	
	@Autowired
	private DistrictSpecificVideoInterviewDetailsDAO districtSpecificVideoInterviewDetailsDAO;
	
	@Autowired
	private ReferenceCheckFitScoreDAO referenceCheckFitScoreDAO;
	
	@Autowired
	private OnlineActivityFitScoreDAO onlineActivityFitScoreDAO;
	
	@Autowired
	private JsiCommunicationLogDAO jsiCommunicationLogDAO;
	
	@Autowired
	private ReferenceCheckQuestionSetDAO referenceCheckQuestionSetDAO;
	
	@Autowired
	private EligibilityVerificationHistroyDAO eligibilityVerificationHistroyDAO;
	
	@Autowired
	private EligibilityMasterDAO eligibilityMasterDAO;
	
	@Autowired
	private EligibilityStatusMasterDAO eligibilityStatusMasterDAO;
	
	@Autowired
	private DistrictSpecificTeacherTfaOptionsDAO districtSpecificTeacherTfaOptionsDAO;
	
	@Autowired
	private DistrictSpecificPortfolioQuestionsDAO districtSpecificPortfolioQuestionsDAO;
	
	@Autowired
	private JobWiseTeacherDetailsDAO jobWiseTeacherDetailsDAO;
	
	@Autowired
	private OctDetailsDAO OctDetailsDAO;
	
	@Autowired
	private StatusSpecificQuestionsDAO statusSpecificQuestionsDAO;
	
	@Autowired
	private LicenseDAO licenseDAO;
	
	@Autowired
	private LicensureNBPTSDao licensureNBPTSDao;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	@Autowired
	private QqQuestionSetsDAO qqQuestionSetsDAO;
	
	@Autowired
	private InviteCandidateDAO inviteCandidateDAO;

	public String getCandidateReportGrid(String jobId, String orderColumn, String sortingOrder)
	{
		System.out.println(orderColumn);
		System.out.println(sortingOrder);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		int roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(msgYrSesstionExp);
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		StringBuffer sb = new StringBuffer();	
		
		return sb.toString();
	}

	@Transactional(readOnly=true)
	public String getHiredCandidateReports(String jobId,String firstName,String lastName,String emailAddress,String certType,String regionId,String degreeId,String universityId,String normScoreSelectVal,String 
			normScoreVal,String CGPA,String CGPASelectVal,String contactedVal,String candidateStatus,String stateId,String orderColumn, String sortingOrder,int totalNoOfRecord,String tagsId,boolean callbreakup,int callNoofRecords,
			Integer[] certIds,String YOTESelectVal,String YOTE,String stateId2,String zipCode,String epiFromDate,String epiToDate)
	{	
		
		System.out.println("================================== getHiredCandidateReports ==========================");
		
		Integer districtIdForSpecific = 3702970;
		Map<String,RawDataForDomain> mapRawDomainScore = null;
		List<JobForTeacher> lstJobForTeacher = null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		TeacherAssessmentStatus assessmentStatus = null;
		StatusMaster status = null;
		StringBuffer tableHtml = new StringBuffer();
		List<String> hiredlst=new ArrayList<String>(); // add by ram nath for 10-10 record
		double[] meanArray=null;
		int noOfRecordCheck =totalNoOfRecord;
		boolean smartPractices=false,achievementScore=false,tFA=false,demoClass=false,displayPhoneInterview=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,CGPADis=false,fitFirstNoble=false,fitSecondNoble=false,fitMiami=false,jobAppliedDate=false,videoInterview=false,schoolSelection=false,PNQ=false,senNum=false;
		CandidateGridService cgService=new CandidateGridService();
		CGInviteInterviewAjax cgInviteInterviewAjax = new CGInviteInterviewAjax();
		boolean statusPrivilege=false;
		List<TeacherDetail> teacherList=new ArrayList<TeacherDetail>();
		Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
		Map<String,String> statusNameMap=new HashMap<String,String>();
		List<DistrictAssessmentDetail> districtAssessmentDetailList=new ArrayList<DistrictAssessmentDetail>();
		List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=new ArrayList<OnlineActivityQuestionSet>(); 
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList=new ArrayList<TeacherDistrictAssessmentStatus>();
		List<TeacherDistrictAssessmentAnswerDetail> teacherAssessmentAnswerDetailsList = new ArrayList<TeacherDistrictAssessmentAnswerDetail>();
		try 
		{	
			String tFname = "",tLname = "",roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			//List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			meanArray = new double[lstDomain.size()];

			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			
			try{
				districtAssessmentDetailList=districtAssessmentJobRelationDAO.findDistrictAssessmentDetailByJobOrder(jobOrder);
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990))
					onlineActivityQuestionSetList=onlineActivityQuestionSetDAO.getOnlineActivityQuestionSetList(jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster());
				
				if(userMaster.getEntityType()==3){
					SecondaryStatus secondaryStatus=secondaryStatusDAO.findSecondaryStatusObjByJobCategoryAndHBD(jobOrder);
					teacherList=teacherStatusHistoryForJobDAO.findTeacherListByStatusAndSecStatusHBD(jobOrder,secondaryStatus);
					if(teacherList!=null && userMaster.getEntityType()==3){
						statusPrivilege=true;
					}
				}
				
				
			if(jobOrder.getDistrictMaster()!=null){
					DistrictMaster districtMasterObj=jobOrder.getDistrictMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					if(districtMasterObj.getDistrictId()==7800038){
						PNQ=true;
						fitFirstNoble=true;
						fitSecondNoble=true;
					}
					if(districtMasterObj.getDistrictId()==1200390){
						try{
							if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Instructional")){
								fitMiami=true;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					if(districtMasterObj.getJobAppliedDate()!=null && districtMasterObj.getJobAppliedDate()){
						jobAppliedDate=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(districtMasterObj.getStatusMasterForCC()!=null && districtMasterObj.getSecondaryStatusForCC()==null){
							schoolSelection=true;
						}else if(districtMasterObj.getStatusMasterForCC()==null && districtMasterObj.getSecondaryStatusForCC()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(jobOrder.getBranchMaster()!=null){
					BranchMaster districtMasterObj=jobOrder.getBranchMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(jobOrder.getHeadQuarterMaster()!=null){
					HeadQuarterMaster hqMaster=jobOrder.getHeadQuarterMaster();
					if(hqMaster.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(hqMaster.getDisplayTFA()){
						tFA=true;
					}
					if(hqMaster.getDisplayDemoClass()){
						demoClass=true;
					}
					if(hqMaster.getDisplayJSI()){
						JSI=true;
					}
					if(hqMaster.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(hqMaster.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(hqMaster.getDisplayFitScore()){
						fitScore=true;
					}
					if(hqMaster.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(hqMaster.getDisplayPhoneInterview()!=null && hqMaster.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{ 
						if(hqMaster.getCandidateConsiderationStatusId()!=null && hqMaster.getCandidateConsiderationSecondaryStatusId()==null){
							schoolSelection=true;
						}else if(hqMaster.getCandidateConsiderationStatusId()==null && hqMaster.getCandidateConsiderationSecondaryStatusId()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}catch(Exception e){ e.printStackTrace();}
			if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null){
				if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
					smartPractices=true;
				}
			}

			List<StatusMaster> lstStatusMastersForAll = new ArrayList<StatusMaster>();
			String[] statussForAll = {"hird","comp","scomp","ecomp","vcomp","dcln","icomp","rem","vlt","widrw"};
			try{
				lstStatusMastersForAll = Utility.getStaticMasters(statussForAll);
			}catch (Exception e) {
				e.printStackTrace();
			}
			/* filter Add  */
			List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
			List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean certFlag=false;
			boolean teacherFlag=false;
			StateMaster stateMaster=null;
			List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
			if(certType!=null && !certType.equals("") && !certType.equals("0")||!stateId.equals("0")){
				if(!stateId.equals("0")){
					stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
				}
				certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
				if(certificateTypeMasterList.size()>0)
					certTeacherDetailList = teacherCertificateDAO.findCertificateByJobForTeacher(certificateTypeMasterList);
				certFlag=true;
			}
			if(certType.equals("")){
				certFlag=true;
			}
			if(certFlag && certTeacherDetailList.size()>0){
				filterTeacherList.addAll(certTeacherDetailList);
			}
			if(certFlag){
				teacherFlag=true;
			}
			
			//multiple certificate filter
			boolean mCertFlag=false;
			List<Integer> certTypeIdList = new ArrayList<Integer>();
			List<Integer> teacherIdList = new ArrayList<Integer>();
			List<TeacherDetail> mCertTeacherDetailList = new ArrayList<TeacherDetail>();
			if(certIds!=null && !certIds.equals("") && certIds.length>0){
				for(int i=0;i<certIds.length;i++)
				{
					certTypeIdList.add(certIds[i]);
				}
				teacherIdList = teacherCertificateDAO.findTeacherByCertificates(certTypeIdList);
				if(teacherIdList!=null && !teacherIdList.equals("") && teacherIdList.size()>0){
					for(int i=0;i<teacherIdList.size();i++)
					{
						TeacherDetail teacherDetail = new TeacherDetail();
						teacherDetail.setTeacherId(teacherIdList.get(i));
						mCertTeacherDetailList.add(teacherDetail);
					}
				}
				mCertFlag=true;
			}
			if(mCertFlag && mCertTeacherDetailList.size()>0){
				filterTeacherList.addAll(mCertTeacherDetailList);
			}
			if(mCertFlag){
				teacherFlag=true;
			}
			//yote filter
			List<TeacherDetail> expTeacherList = new ArrayList<TeacherDetail>();
			boolean expFlag=false;
			if(YOTE!=null && !YOTE.equals("")&& !YOTE.equals("0")){
				expFlag=true;
				expTeacherList=teacherExperienceDAO.findTeachersByExperience(YOTE,YOTESelectVal);
			}
			if(expFlag && expTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(expTeacherList);
				}else{
					filterTeacherList.addAll(expTeacherList);
				}
			}
			if(expFlag){
				teacherFlag=true;
			}
			//state filter
			List<TeacherDetail> stateTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean stateFlag=false;
			if(stateId2!=null && !stateId2.equals("") && !stateId2.equals("0"))
			{
				stateFlag=true;
				StateMaster stateMaster1 = stateMasterDAO.findById(Long.valueOf(stateId2), false, false);
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByState(stateMaster1);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					stateTeacherDetailList.add(teacherDetail);
				}
			}
			
			if(stateFlag && stateTeacherDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(stateTeacherDetailList);
				else
					filterTeacherList.addAll(stateTeacherDetailList);
			}
			if(stateFlag)
				teacherFlag=true;
			//zip code filter
			List<TeacherDetail> zipCodeTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			boolean zipCodeFlag=false;
			if(zipCode!=null && !zipCode.equals("") && !zipCode.equals("0"))
			{
				zipCodeFlag=true;
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByZipCode(zipCode);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					zipCodeTeacherdetaDetailList.add(teacherDetail);
				}
			}
			
			if(zipCodeFlag && zipCodeTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(zipCodeTeacherdetaDetailList);
				else
					filterTeacherList.addAll(zipCodeTeacherdetaDetailList);
			}
			if(zipCodeFlag)
				teacherFlag=true;
			
			//********************** EPI Date Filter By Ravindra ***************************
			Date epifDate=null;
			Date epitDate=null;
			boolean epiDateFlag=false;
			List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			try{
				if(!epiFromDate.equals("")){
					epifDate=Utility.getCurrentDateFormart(epiFromDate);
					epiDateFlag=true;
				}
				if(!epiToDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(epiToDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					epitDate=cal2.getTime();
					epiDateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(epiDateFlag)
			{
				StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.getAssessmentCompletedDateTimeTeacherList(1, statusMaster, epifDate, epitDate);
				for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherAssessmentStatus.getTeacherDetail().getTeacherId());
					epiDateTeacherdetaDetailList.add(teacherDetail);
				}
			}
			if(epiDateFlag && epiDateTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag){
					filterTeacherList.retainAll(epiDateTeacherdetaDetailList);
				}
				else{
					filterTeacherList.addAll(epiDateTeacherdetaDetailList);
				}
			}
			if(epiDateFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> degreeTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean degreeIdFlag=false;
			if(degreeId!=null && !degreeId.equals("") && !degreeId.equals("0")){
				degreeIdFlag=true;
				DegreeMaster degreeMaster=degreeMasterDAO.findById(Long.valueOf(degreeId), false,false);
				degreeTeacherDetailList=teacherAcademicsDAO.findTeacherListByDegree(degreeMaster);
			}
			if(degreeId.equals("")){
				degreeIdFlag=true;
			}
			if(degreeIdFlag && degreeTeacherDetailList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(degreeTeacherDetailList);
				}else{
					filterTeacherList.addAll(degreeTeacherDetailList);
				}
			}
			if(degreeIdFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> universityTeacherList = new ArrayList<TeacherDetail>();
			boolean universityFlag=false;
			if(universityId!=null && !universityId.equals("") && !universityId.equals("0")){
				universityFlag=true;
				UniversityMaster universityMaster=universityMasterDAO.findById(Integer.valueOf(universityId), false,false);
				universityTeacherList=teacherAcademicsDAO.findTeacherListByUniversity(universityMaster);
			}
			if(universityId.equals("")){
				universityFlag=true;
			}
			if(universityFlag && universityTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(universityTeacherList);
				}else{
					filterTeacherList.addAll(universityTeacherList);
				}
			}
			if(universityFlag){
				teacherFlag=true;
			}
			//****************************tags filter*************************************************
			List<TeacherDetail> tagsTeacherList = new ArrayList<TeacherDetail>();
			boolean tagsFlag=false;
			if(tagsId!=null && !tagsId.equals("") && !tagsId.equals("0")){
				tagsFlag=true;
				SecondaryStatusMaster secondaryStatusMaster= secondaryStatusMasterDAO.findById(Integer.valueOf(tagsId), false,false);
				//tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndJob(secondaryStatusMaster,jobOrder);
				tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndDistrict(secondaryStatusMaster,districtMaster);
			}
			
			if(tagsFlag && tagsTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(tagsTeacherList);
				}else{
					filterTeacherList.addAll(tagsTeacherList);
				}
			}
			if(tagsFlag){
				teacherFlag=true;
			}
			
			List<TeacherDetail> regionTeacherList = new ArrayList<TeacherDetail>();
			boolean regionFlag=false;
			if(regionId!=null && !regionId.equals("") && !regionId.equals("0")){
				regionFlag=true;
				regionTeacherList=teacherPreferenceDAO.findRegionByJobForTeacher(regionId);
			}
			if(regionId.equals("")){
				regionFlag=true;
			}
			if(regionFlag && regionTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(regionTeacherList);
				}else{
					filterTeacherList.addAll(regionTeacherList);
				}
			}
			if(regionFlag){
				teacherFlag=true;
			}
			List<TeacherDetail> cgpaTeacherList = new ArrayList<TeacherDetail>();
			boolean cgpaFlag=false;
			if(CGPA!=null && !CGPA.equals("")&& !CGPA.equals("0")){
				cgpaFlag=true;
				cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA(CGPA,CGPASelectVal);
			}
			if(cgpaFlag && cgpaTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(cgpaTeacherList);
				}else{
					filterTeacherList.addAll(cgpaTeacherList);
				}
			}
			if(cgpaFlag){
				teacherFlag=true;
			}
			if(statusPrivilege){
				teacherFlag=true;
				filterTeacherList.addAll(teacherList);
			}

			if((teacherFlag && filterTeacherList.size()==0) || (!normScoreVal.equals("") && !normScoreVal.equals("0"))){
				lstJobForTeacher = new ArrayList<JobForTeacher>();
			}else{
				//lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCG(null,jobOrder,lstStatusMastersForAll,7,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,true,false,null);
				lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCGOpSql(userMaster,jobOrder,lstStatusMastersForAll,7,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,true,false,null,0,0,orderColumn,sortingOrder);
			}
			
			System.out.println("lstJobForTeacher::::>>>>>>>>."+lstJobForTeacher.size());
			int hiredTeacherCount=0;
			if(lstJobForTeacher.size()>0){
				boolean baseStatusFlag=false;
				boolean epiFlag=false;
				if(jobOrder.getDistrictMaster()!=null){
					if(jobOrder.getDistrictMaster().getNoEPI()==null ||(jobOrder.getDistrictMaster().getNoEPI()!=null && jobOrder.getDistrictMaster().getNoEPI()==false)){
						epiFlag=true;
					}
				}else if(jobOrder.getBranchMaster()!=null){
					if(jobOrder.getBranchMaster().getNoEPI()==null ||(jobOrder.getBranchMaster().getNoEPI()!=null && jobOrder.getBranchMaster().getNoEPI()==false)){
						epiFlag=true;
					}
				}else if(jobOrder.getHeadQuarterMaster()!=null){
					if(jobOrder.getHeadQuarterMaster().getNoEPI()==null ||(jobOrder.getHeadQuarterMaster().getNoEPI()!=null && jobOrder.getHeadQuarterMaster().getNoEPI()==false)){
						epiFlag=true;
					}
				}
				if(epiFlag){
					//if(jobCategoryMasterDAO.baseStatusChecked(jobOrder.getJobCategoryMaster().getJobCategoryId())){
					if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getStatus().equals("A") && jobOrder.getJobCategoryMaster().getBaseStatus()){
						baseStatusFlag=true;
					}
				}
				if(lstJobForTeacher.size()>0){
					
					List<TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();
					for(JobForTeacher jft:lstJobForTeacher){
						lstTeacherDetails.add(jft.getTeacherId());
					}
				
					List<Integer> statusIdList = new ArrayList<Integer>();
					String[] statusShrotName = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw","ielig","iielig"};
					
					//////////////////////////////////Get All Hired Teacher///////////////////////////////////////////////
					List<StatusMaster> statusMasters=Utility.getStaticMasters(statusShrotName);
					Map<String,StatusMaster> statusMap=new HashMap<String, StatusMaster>();
					for (StatusMaster statusMasterObj : statusMasters) {
						statusMap.put(statusMasterObj.getStatusShortName(),statusMasterObj);
					}
					Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
					List<TeacherStatusHistoryForJob> historyForJobFilterList = teacherStatusHistoryForJobDAO.findHireTeacherListByHBD(lstTeacherDetails,jobOrder,statusMap);
					List<TeacherStatusHistoryForJob> historyForJobList =new ArrayList<TeacherStatusHistoryForJob>();
					try{
						Map<String,String> historyMap=new HashMap<String, String>();
						Map<String,TeacherStatusHistoryForJob> historyObjMap=new HashMap<String,TeacherStatusHistoryForJob>();
						if(historyForJobFilterList.size()>0)
						for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobFilterList) {
							String id= teacherStatusHistoryForJob.getJobOrder().getJobId()+"#"+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId();
							String idValue=historyMap.get(id);
							if(idValue!=null){
								idValue+="#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#";
								historyMap.put(id,idValue);
							}else{
								historyMap.put(id,"#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#");
							}
							if(teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp") || teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("hird")){
								historyObjMap.put(id, teacherStatusHistoryForJob);
							}
						}
						if(historyObjMap.size()>0)
						for (Entry<String, TeacherStatusHistoryForJob> entry : historyObjMap.entrySet()){
							TeacherStatusHistoryForJob  historyObj=entry.getValue();
							String id= historyObj.getJobOrder().getJobId()+"#"+historyObj.getTeacherDetail().getTeacherId();
							String idValue=historyMap.get(id);
							if(idValue!=null && !(idValue.contains("rem")|| idValue.contains("dcln") || idValue.contains("widrw"))){
								historyForJobList.add(historyObj);
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				
					List<JobForTeacher> jftOffers=jobForTeacherDAO.findJobByTeacherListForOffers(lstTeacherDetails,jobOrder.getDistrictMaster(),statusMap.get("hird"),statusMap.get("vcomp"));
					System.out.println("jftOffers::::::::::::::::"+jftOffers.size());
					System.out.println("lstTeacherDetails::::::::::::::"+lstTeacherDetails.size());
					List<String> posList=new ArrayList<String>();
					Map<String,String> reqMap=new HashMap<String, String>();
					Map<String,List<SchoolMaster>> hiredSchol=new HashMap<String,List<SchoolMaster>>();

					
					for (JobForTeacher jobForTeacher : jftOffers){
						try{

							if(jobForTeacher.getRequisitionNumber()!=null){	
								if(jobForTeacher.getSchoolMaster()!=null){	
									String teacherIdAndJobId=jobForTeacher.getTeacherId().getTeacherId()+"#"+jobForTeacher.getJobId().getJobId();	
									List<SchoolMaster> allList=hiredSchol.get(teacherIdAndJobId);	
									if(hiredSchol.get(teacherIdAndJobId)!=null){	
									allList.add(jobForTeacher.getSchoolMaster());	
									hiredSchol.put(teacherIdAndJobId,allList);	
									}else{	
									List<SchoolMaster> tList=new ArrayList<SchoolMaster>();	
									tList.add(jobForTeacher.getSchoolMaster());	
									hiredSchol.put(teacherIdAndJobId,tList);	
									}	
								}			
								posList.add(jobForTeacher.getRequisitionNumber());			
								reqMap.put(jobForTeacher.getJobId().getJobId()+"#"+jobForTeacher.getTeacherId().getTeacherId(),jobForTeacher.getRequisitionNumber());		
							}
					}catch(Exception e){
						e.printStackTrace();
					}

					}
					
					List<DistrictRequisitionNumbers> districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(jobOrder.getDistrictMaster(),posList);
					Map<String,DistrictRequisitionNumbers> dReqNoMap = new HashMap<String,DistrictRequisitionNumbers>();
					try{
						for (DistrictRequisitionNumbers districtRequisitionNumbers2 : districtRequisitionNumbers) {
							dReqNoMap.put(districtRequisitionNumbers2.getRequisitionNumber(),districtRequisitionNumbers2);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					System.out.println("historyForJobList::::::::::::"+historyForJobList.size());
					System.out.println(" R: "+reqMap.size()+" RM: "+dReqNoMap.size()+" T: "+lstTeacherDetails.size()+" H "+historyForJobList.size()+" J "+jobOrder.getJobId());
					if(historyForJobList.size()>0){
						mapForHiredTeachers=cgService.getHiredList(reqMap,dReqNoMap,lstTeacherDetails,historyForJobList,jobOrder);
					}
					System.out.println("mapForHiredTeachers::::"+mapForHiredTeachers.size());
			
					System.out.println("lstJobForTeacher::::::::::::::"+lstJobForTeacher.size());
					///////////////////////////////////////////////////////////////////////////
					List<JobForTeacher> JFTList = new ArrayList<JobForTeacher>(lstJobForTeacher);
					for(JobForTeacher jft:JFTList)
					{	
						boolean isNoHired=true;
						try{
							if(mapForHiredTeachers.get(jft.getTeacherId().getTeacherId())!=null && mapForHiredTeachers.get(jft.getTeacherId().getTeacherId()).size()>0){
								isNoHired=false;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						
						if(isNoHired){
							lstJobForTeacher.remove(jft);
						}
					}
				}
			}
			System.out.println("lstJobForTeacher::For Hired:::::::::::"+lstJobForTeacher.size());
			tableHtml.append(lstJobForTeacher.size());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tableHtml.toString();	
	}
	
	@Transactional(readOnly=true)
	public String getInCompleteCandidateReports(String jobId,String firstName,String lastName,String emailAddress,String certType,String regionId,String degreeId,String universityId,String normScoreSelectVal,String 
			normScoreVal,String CGPA,String CGPASelectVal,String contactedVal,String candidateStatus,String stateId,String orderColumn, String sortingOrder,int totalNoOfRecord,String tagsId,boolean callbreakup,int callNoofRecords,
			Integer[] certIds,String YOTESelectVal,String YOTE,String stateId2,String zipCode,String epiFromDate,String epiToDate)
	{	
		System.out.println("==========================  getInCompleteCandidateReports =============================================== ");
		
		Integer districtIdForSpecific = 3702970;
		List<JobForTeacher> lstJobForTeacher = null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		TeacherAssessmentStatus assessmentStatus = null;
		StatusMaster status = null;

		StringBuffer tableHtml = new StringBuffer();
		List<String> inCompllst=new ArrayList<String>();//add by Ram Nath
		
		int noOfRecordCheck =totalNoOfRecord;
		boolean smartPractices=false,achievementScore=false,tFA=false,demoClass=false,displayPhoneInterview=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,CGPADis=false,fitFirstNoble=false,fitSecondNoble=false,fitMiami=false,jobAppliedDate=false,videoInterview=false,schoolSelection=false,PNQ=false,senNum=false;
		CandidateGridService cgService=new CandidateGridService();
		CGInviteInterviewAjax cgInviteInterviewAjax = new CGInviteInterviewAjax();
		boolean statusPrivilege=false;
		List<TeacherDetail> teacherList=new ArrayList<TeacherDetail>();
		Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
		Map<String,String> statusNameMap=new HashMap<String,String>();
		List<DistrictAssessmentDetail> districtAssessmentDetailList=new ArrayList<DistrictAssessmentDetail>();
		List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=new ArrayList<OnlineActivityQuestionSet>(); 
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList=new ArrayList<TeacherDistrictAssessmentStatus>();
		List<TeacherDistrictAssessmentAnswerDetail> teacherAssessmentAnswerDetailsList = new ArrayList<TeacherDistrictAssessmentAnswerDetail>();
		try 
		{	
			String tFname = "",tLname = "",roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			try{
				if(userMaster.getEntityType()==3){
					SecondaryStatus secondaryStatus=secondaryStatusDAO.findSecondaryStatusObjByJobCategoryAndHBD(jobOrder);
					teacherList=teacherStatusHistoryForJobDAO.findTeacherListByStatusAndSecStatusHBD(jobOrder,secondaryStatus);
					if(teacherList!=null && userMaster.getEntityType()==3){
						statusPrivilege=true;
					}
				}
				
				if(jobOrder.getDistrictMaster()!=null){
					DistrictMaster districtMasterObj=jobOrder.getDistrictMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					if(districtMasterObj.getDistrictId()==7800038){
						PNQ=true;
						fitFirstNoble=true;
						fitSecondNoble=true;
					}
					if(districtMasterObj.getDistrictId()==1200390){
						try{
							if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Instructional")){
								fitMiami=true;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					if(districtMasterObj.getJobAppliedDate()!=null && districtMasterObj.getJobAppliedDate()){
						jobAppliedDate=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(districtMasterObj.getStatusMasterForCC()!=null && districtMasterObj.getSecondaryStatusForCC()==null){
							schoolSelection=true;
						}else if(districtMasterObj.getStatusMasterForCC()==null && districtMasterObj.getSecondaryStatusForCC()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(jobOrder.getBranchMaster()!=null){
					BranchMaster districtMasterObj=jobOrder.getBranchMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(jobOrder.getHeadQuarterMaster()!=null){
					HeadQuarterMaster hqMasterobj=jobOrder.getHeadQuarterMaster();
					if(hqMasterobj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(hqMasterobj.getDisplayTFA()){
						tFA=true;
					}
					if(hqMasterobj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(hqMasterobj.getDisplayJSI()){
						JSI=true;
					}
					if(hqMasterobj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(hqMasterobj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(hqMasterobj.getDisplayFitScore()){
						fitScore=true;
					}
					if(hqMasterobj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(hqMasterobj.getDisplayPhoneInterview()!=null && hqMasterobj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(hqMasterobj.getCandidateConsiderationStatusId()!=null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()==null){
							schoolSelection=true;
						}else if(hqMasterobj.getCandidateConsiderationStatusId()==null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}catch(Exception e){ e.printStackTrace();}
			if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null){
				if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
					smartPractices=true;
				}
			}

			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			String[] statuss = {"icomp","hird","dcln","hird","scomp","ecomp","vcomp","ielig","iielig"}; 
   //String[] statuss = {"icomp","comp","hird","dcln","hird","scomp","ecomp","vcomp"};
			try{
				lstStatusMasters = Utility.getStaticMasters(statuss);
			}catch (Exception e) {
				e.printStackTrace();
			}
			/* filter Add  */
			List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
			List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean certFlag=false;
			boolean teacherFlag=false;
			StateMaster stateMaster=null;
			List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
			if(certType!=null && !certType.equals("") && !certType.equals("0")||!stateId.equals("0")){
				if(!stateId.equals("0")){
					stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
				}
				certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
				if(certificateTypeMasterList.size()>0)
					certTeacherDetailList = teacherCertificateDAO.findCertificateByJobForTeacher(certificateTypeMasterList);
				certFlag=true;
			}
			if(certType.equals("")){
				certFlag=true;
			}
			if(certFlag && certTeacherDetailList.size()>0){
				filterTeacherList.addAll(certTeacherDetailList);
			}
			if(certFlag){
				teacherFlag=true;
			}
			
			//multiple certificate filter
			boolean mCertFlag=false;
			List<Integer> certTypeIdList = new ArrayList<Integer>();
			List<Integer> teacherIdList = new ArrayList<Integer>();
			List<TeacherDetail> mCertTeacherDetailList = new ArrayList<TeacherDetail>();
			if(certIds!=null && !certIds.equals("") && certIds.length>0){
				for(int i=0;i<certIds.length;i++)
				{
					certTypeIdList.add(certIds[i]);
				}
				teacherIdList = teacherCertificateDAO.findTeacherByCertificates(certTypeIdList);
				if(teacherIdList!=null && !teacherIdList.equals("") && teacherIdList.size()>0){
					for(int i=0;i<teacherIdList.size();i++)
					{
						TeacherDetail teacherDetail = new TeacherDetail();
						teacherDetail.setTeacherId(teacherIdList.get(i));
						mCertTeacherDetailList.add(teacherDetail);
					}
				}
				mCertFlag=true;
			}
			if(mCertFlag && mCertTeacherDetailList.size()>0){
				filterTeacherList.addAll(mCertTeacherDetailList);
			}
			if(mCertFlag){
				teacherFlag=true;
			}
			//yote filter
			List<TeacherDetail> expTeacherList = new ArrayList<TeacherDetail>();
			boolean expFlag=false;
			if(YOTE!=null && !YOTE.equals("")&& !YOTE.equals("0")){
				expFlag=true;
				expTeacherList=teacherExperienceDAO.findTeachersByExperience(YOTE,YOTESelectVal);
			}
			if(expFlag && expTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(expTeacherList);
				}else{
					filterTeacherList.addAll(expTeacherList);
				}
			}
			if(expFlag){
				teacherFlag=true;
			}
			//state filter
			List<TeacherDetail> stateTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean stateFlag=false;
			if(stateId2!=null && !stateId2.equals("") && !stateId2.equals("0"))
			{
				stateFlag=true;
				StateMaster stateMaster1 = stateMasterDAO.findById(Long.valueOf(stateId2), false, false);
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByState(stateMaster1);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					stateTeacherDetailList.add(teacherDetail);
				}
			}
			
			if(stateFlag && stateTeacherDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(stateTeacherDetailList);
				else
					filterTeacherList.addAll(stateTeacherDetailList);
			}
			if(stateFlag)
				teacherFlag=true;
			//zip code filter
			List<TeacherDetail> zipCodeTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			boolean zipCodeFlag=false;
			if(zipCode!=null && !zipCode.equals("") && !zipCode.equals("0"))
			{
				zipCodeFlag=true;
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByZipCode(zipCode);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					zipCodeTeacherdetaDetailList.add(teacherDetail);
				}
			}
			
			if(zipCodeFlag && zipCodeTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(zipCodeTeacherdetaDetailList);
				else
					filterTeacherList.addAll(zipCodeTeacherdetaDetailList);
			}
			if(zipCodeFlag)
				teacherFlag=true;
			//********************** EPI Date Filter By Ravindra ***************************
			Date epifDate=null;
			Date epitDate=null;
			boolean epiDateFlag=false;
			List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			try{
				if(!epiFromDate.equals("")){
					epifDate=Utility.getCurrentDateFormart(epiFromDate);
					epiDateFlag=true;
				}
				if(!epiToDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(epiToDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					epitDate=cal2.getTime();
					epiDateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(epiDateFlag)
			{
				StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.getAssessmentCompletedDateTimeTeacherList(1, statusMaster, epifDate, epitDate);
				for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherAssessmentStatus.getTeacherDetail().getTeacherId());
					epiDateTeacherdetaDetailList.add(teacherDetail);
				}
			}
			if(epiDateFlag && epiDateTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag){
					filterTeacherList.retainAll(epiDateTeacherdetaDetailList);
				}
				else{
					filterTeacherList.addAll(epiDateTeacherdetaDetailList);
				}
			}
			if(epiDateFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> degreeTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean degreeIdFlag=false;
			if(degreeId!=null && !degreeId.equals("") && !degreeId.equals("0")){
				degreeIdFlag=true;
				DegreeMaster degreeMaster=degreeMasterDAO.findById(Long.valueOf(degreeId), false,false);
				degreeTeacherDetailList=teacherAcademicsDAO.findTeacherListByDegree(degreeMaster);
			}
			if(degreeId.equals("")){
				degreeIdFlag=true;
			}
			if(degreeIdFlag && degreeTeacherDetailList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(degreeTeacherDetailList);
				}else{
					filterTeacherList.addAll(degreeTeacherDetailList);
				}
			}
			if(degreeIdFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> universityTeacherList = new ArrayList<TeacherDetail>();
			boolean universityFlag=false;
			if(universityId!=null && !universityId.equals("") && !universityId.equals("0")){
				universityFlag=true;
				UniversityMaster universityMaster=universityMasterDAO.findById(Integer.valueOf(universityId), false,false);
				universityTeacherList=teacherAcademicsDAO.findTeacherListByUniversity(universityMaster);
			}
			if(universityId.equals("")){
				universityFlag=true;
			}
			if(universityFlag && universityTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(universityTeacherList);
				}else{
					filterTeacherList.addAll(universityTeacherList);
				}
			}
			if(universityFlag){
				teacherFlag=true;
			}
			//****************************tags filter*************************************************
			List<TeacherDetail> tagsTeacherList = new ArrayList<TeacherDetail>();
			boolean tagsFlag=false;
			if(tagsId!=null && !tagsId.equals("") && !tagsId.equals("0")){
				tagsFlag=true;
				SecondaryStatusMaster secondaryStatusMaster= secondaryStatusMasterDAO.findById(Integer.valueOf(tagsId), false,false);
				//tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndJob(secondaryStatusMaster,jobOrder);
				tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndDistrict(secondaryStatusMaster,districtMaster);
			}
			
			if(tagsFlag && tagsTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(tagsTeacherList);
				}else{
					filterTeacherList.addAll(tagsTeacherList);
				}
			}
			if(tagsFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> regionTeacherList = new ArrayList<TeacherDetail>();
			boolean regionFlag=false;
			if(regionId!=null && !regionId.equals("") && !regionId.equals("0")){
				regionFlag=true;
				regionTeacherList=teacherPreferenceDAO.findRegionByJobForTeacher(regionId);
			}
			if(regionId.equals("")){
				regionFlag=true;
			}
			if(regionFlag && regionTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(regionTeacherList);
				}else{
					filterTeacherList.addAll(regionTeacherList);
				}
			}
			if(regionFlag){
				teacherFlag=true;
			}
			List<TeacherDetail> cgpaTeacherList = new ArrayList<TeacherDetail>();
			boolean cgpaFlag=false;
			if(CGPA!=null && !CGPA.equals("")&& !CGPA.equals("0")){
				cgpaFlag=true;
				cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA(CGPA,CGPASelectVal);
			}
			if(cgpaFlag && cgpaTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(cgpaTeacherList);
				}else{
					filterTeacherList.addAll(cgpaTeacherList);
				}
			}
			if(cgpaFlag){
				teacherFlag=true;
			}
			if(statusPrivilege){
				teacherFlag=true;
				filterTeacherList.addAll(teacherList);
			}

			System.out.println("filterTeacherList:::"+filterTeacherList.size());
			System.out.println("teacherFlag::::::"+teacherFlag);

			if((teacherFlag && filterTeacherList.size()==0)){
				lstJobForTeacher = new ArrayList<JobForTeacher>();
			}else{
				lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCG(null,jobOrder,lstStatusMasters,0,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,false,false,null);
			}

			System.out.println("lstJobForTeacher Size ::>>"+lstJobForTeacher.size());
			if(lstJobForTeacher.size()>0){

				boolean baseStatusFlag=false;
				boolean epiFlag=false;
				if(jobOrder.getDistrictMaster()!=null){
					if(jobOrder.getDistrictMaster().getNoEPI()==null ||(jobOrder.getDistrictMaster().getNoEPI()!=null && jobOrder.getDistrictMaster().getNoEPI()==false)){
						epiFlag=true;
					}
				}else if(jobOrder.getBranchMaster()!=null){
					if(jobOrder.getBranchMaster().getNoEPI()==null ||(jobOrder.getBranchMaster().getNoEPI()!=null && jobOrder.getBranchMaster().getNoEPI()==false)){
						epiFlag=true;
					}
				}else if(jobOrder.getHeadQuarterMaster()!=null){
					if(jobOrder.getHeadQuarterMaster().getNoEPI()==null ||(jobOrder.getHeadQuarterMaster().getNoEPI()!=null && jobOrder.getHeadQuarterMaster().getNoEPI()==false)){
						epiFlag=true;
					}
				}
				if(epiFlag){
					if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getStatus().equals("A") && jobOrder.getJobCategoryMaster().getBaseStatus()){
						baseStatusFlag=true;
					}
				}
				//--------------------------------------------------set global map start----------------------------------			
				Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();		
				List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
				List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
				
				for (JobForTeacher jbforteacher : lstJobForTeacher){ 
					teacherDetails.add(jbforteacher.getTeacherId());
				}
				
				int checkBoxId=0;
				List<TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();
				List<Integer> lstTeacherDetailsPInfo = new ArrayList<Integer>();
				SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
				SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();
				List<JobForTeacher> jobForTeachersList	  =	 new ArrayList<JobForTeacher>();
				List<String> lstTeacherEmails = new ArrayList<String>();
				List<Integer>lstTeacherDetailIds=new ArrayList<Integer>();
				
				for(JobForTeacher jft:lstJobForTeacher){
					lstTeacherDetails.add(jft.getTeacherId());
				}
				
				List<JobForTeacher> lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacher);
				TeacherAssessmentStatus teAssesStatusTempBase = null;
				TeacherAssessmentStatus teAssesStatusTempJSI = null;
				InternalTransferCandidates internalITRA = null;
				Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
				List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); //teacherAssessmentStatusDAO.findAssessmentTaken(null, jobOrder);
				
				Map<Integer, InternalTransferCandidates> mapITRA = new HashMap<Integer, InternalTransferCandidates>();
				List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrderNoDateOP(jobOrder);
				if(assessmentJobRelations1!=null && assessmentJobRelations1.size()>0 && lstTeacherDetails.size()>0){
					AssessmentJobRelation assessmentJobRelation1 = assessmentJobRelations1.get(0);
					lstJSI = teacherAssessmentStatusDAO.findAssessmentTakenByTeacherList(lstTeacherDetails,assessmentJobRelation1.getAssessmentId());
				}
				
				for(TeacherAssessmentStatus ts : lstJSI){
					mapJSI.put(ts.getTeacherDetail().getTeacherId(), ts);
				}
				
				if(teacherDetails!=null && teacherDetails.size()>0){
					teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachersCGOp(teacherDetails);
					for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
						baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
					}
				}
				
				
				
				List<JobForTeacher> lstJFTNormFilter = new ArrayList<JobForTeacher>(lstJobForTeacher);
				List<TeacherPortfolioStatus> tPortfolioStatusList = teacherPortfolioStatusDAO.findAllTeacherPortfolioStatus(lstTeacherDetails);
				
				Map<Integer, TeacherPortfolioStatus> mapPort = new HashMap<Integer, TeacherPortfolioStatus>();
				
				if(tPortfolioStatusList!=null)
				for (TeacherPortfolioStatus teacherPortfolioStatus : tPortfolioStatusList) {
					mapPort.put(teacherPortfolioStatus.getTeacherId().getTeacherId(), teacherPortfolioStatus);
				}
				TeacherPortfolioStatus tPortfolioStatus=null;
				StatusMaster statusMaster=null;
				DistrictMaster districtMasterInternal=null;
				List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
				Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
				for (StatusMaster statusMasterObj : statusMasterList) {
					mapStatus.put(statusMasterObj.getStatusShortName(),statusMasterObj);
				}
				JobOrder jobOrderObj= new JobOrder();
				System.out.println("lstJFTremove:::::::::::::::Before=:::::::::::::"+lstJFTremove.size());
				System.out.println("lstJobForTeacher:::::::::::::Before:::::::::"+lstJobForTeacher.size());
				for(JobForTeacher jft:lstJFTremove){
					if(!candidateStatus.equals("0")){
						if(candidateStatus.equals("icomp")){
						}else{
							lstJobForTeacher.remove(jft);
						}
					}
					if(!contactedVal.equals("0")){
						if(contactedVal.equalsIgnoreCase("1")){
							if(jft.getLastActivity()==null || jft.getLastActivity().equals("")){
								lstJobForTeacher.remove(jft);
							}
						}else if(contactedVal.equalsIgnoreCase("2")){
							if(jft.getLastActivity()!=null && !jft.getLastActivity().equals("")){
								lstJobForTeacher.remove(jft);
							}
						}
					}
					teAssesStatusTempBase = baseTakenMap.get(jft.getTeacherId().getTeacherId());				
					teAssesStatusTempJSI =mapJSI.get(jft.getTeacherId().getTeacherId());
					internalITRA=mapITRA.get(jft.getTeacherId().getTeacherId());
					tPortfolioStatus=mapPort.get(jft.getTeacherId().getTeacherId());
					districtMasterInternal=null;
					if(internalITRA!=null){
						districtMasterInternal=internalITRA.getDistrictMaster();
					}
					if(!jft.getStatus().getStatusShortName().equalsIgnoreCase("icomp")){
						DashboardAjax dbajax=new DashboardAjax();
						statusMaster=dbajax.findByTeacherIdJobStausForLoop(internalITRA,jft,null,teAssesStatusTempJSI,mapJSI,districtMasterInternal,tPortfolioStatus,mapStatus,teAssesStatusTempBase);	
						if(statusMaster!=null && !statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
							lstJobForTeacher.remove(jft);	
						}
					}
				}
				
				System.out.println("lstJFTremove:::::::::::::::After:::::::::::::"+lstJFTremove.size());
				double domainTotalNational = 0,tScoreNW = 0.0;
				List<TeacherNormScore> listTeacherNormScores =new ArrayList<TeacherNormScore>();
				Map<Integer,TeacherNormScore> mapofNormScoresByTeacher= new HashMap<Integer, TeacherNormScore>();
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0){
					listTeacherNormScores=teacherNormScoreDAO.findNormScoreByTeacherList(lstTeacherDetails);
					if(listTeacherNormScores!=null && listTeacherNormScores.size()>0){
						for (TeacherNormScore teacherNormScore : listTeacherNormScores) {
							mapofNormScoresByTeacher.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
						}
					}
				}
				for(JobForTeacher jft:lstJFTNormFilter){
					domainTotalNational=0.0;
					tScoreNW = 0.0;
					try{
						if(mapofNormScoresByTeacher.get(jft.getTeacherId().getTeacherId())!=null){
						   domainTotalNational=(double)(mapofNormScoresByTeacher.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore());
					    }else{
					       domainTotalNational=0.0;
					    }
						
						}catch(Exception e){}
					try{
						if(!normScoreVal.equals("") && !normScoreVal.equals("0")){
							if(normScoreSelectVal.equals("1")){
								if(Math.round(domainTotalNational)!=Integer.parseInt(normScoreVal)){
									lstJobForTeacher.remove(jft);
								}
							}else if(normScoreSelectVal.equals("2")){
								if(Math.round(domainTotalNational)<Integer.parseInt(normScoreVal)){
								}else{
									lstJobForTeacher.remove(jft);
								}
							}else if(normScoreSelectVal.equals("3")){
								if(Math.round(domainTotalNational)<=Integer.parseInt(normScoreVal)){
								}else{
									lstJobForTeacher.remove(jft);
								}
							}else if(normScoreSelectVal.equals("4")){
								if(Math.round(domainTotalNational)>Integer.parseInt(normScoreVal)){
								}else{
									lstJobForTeacher.remove(jft);
								}
							}else if(normScoreSelectVal.equals("5")){
								if(Math.round(domainTotalNational)>=Integer.parseInt(normScoreVal)){
								}else{
									lstJobForTeacher.remove(jft);
								}
							}
						}
					}catch(Exception e){}
				}
				System.out.println("lstJobForTeacher:::::::::::::After:::::::::"+lstJobForTeacher.size());
				
				String[] statusShrotName = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw","ielig","iielig"};
		
				//////////////////////////////////Get All Hired Teacher///////////////////////////////////////////////
				List<StatusMaster> statusMasters=Utility.getStaticMasters(statusShrotName);
				Map<String,StatusMaster> statusMap=new HashMap<String, StatusMaster>();
				for (StatusMaster statusMasterObj : statusMasters) {
					statusMap.put(statusMasterObj.getStatusShortName(),statusMasterObj);
				}
				Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
				List<TeacherStatusHistoryForJob> historyForJobFilterList = teacherStatusHistoryForJobDAO.findHireTeacherListByHBD(lstTeacherDetails,jobOrder,statusMap);
				List<TeacherStatusHistoryForJob> historyForJobList =new ArrayList<TeacherStatusHistoryForJob>();
				try{
					Map<String,String> historyMap=new HashMap<String, String>();
					Map<String,TeacherStatusHistoryForJob> historyObjMap=new HashMap<String,TeacherStatusHistoryForJob>();
					if(historyForJobFilterList.size()>0)
					for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobFilterList) {
						String id= teacherStatusHistoryForJob.getJobOrder().getJobId()+"#"+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId();
						String idValue=historyMap.get(id);
						if(idValue!=null){
							idValue+="#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#";
							historyMap.put(id,idValue);
						}else{
							historyMap.put(id,"#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#");
						}
						if(teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp") || teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("hird")){
							historyObjMap.put(id, teacherStatusHistoryForJob);
						}
					}
					if(historyObjMap.size()>0)
					for (Entry<String, TeacherStatusHistoryForJob> entry : historyObjMap.entrySet()){
						TeacherStatusHistoryForJob  historyObj=entry.getValue();
						String id= historyObj.getJobOrder().getJobId()+"#"+historyObj.getTeacherDetail().getTeacherId();
						String idValue=historyMap.get(id);
						if(idValue!=null && !(idValue.contains("rem")|| idValue.contains("dcln") || idValue.contains("widrw"))){
							historyForJobList.add(historyObj);
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				List<JobForTeacher> jftOffers=jobForTeacherDAO.findJobByTeacherListForOffers(lstTeacherDetails,jobOrder.getDistrictMaster(),statusMap.get("hird"),statusMap.get("vcomp"));
				List<String> posList=new ArrayList<String>();
				Map<String,String> reqMap=new HashMap<String, String>();
				for (JobForTeacher jobForTeacher : jftOffers){
					try{
						if(jobForTeacher.getRequisitionNumber()!=null){
							posList.add(jobForTeacher.getRequisitionNumber());
							reqMap.put(jobForTeacher.getJobId().getJobId()+"#"+jobForTeacher.getTeacherId().getTeacherId(),jobForTeacher.getRequisitionNumber());
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
				List<DistrictRequisitionNumbers> districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(jobOrder.getDistrictMaster(),posList);
				Map<String,DistrictRequisitionNumbers> dReqNoMap = new HashMap<String,DistrictRequisitionNumbers>();
				try{
					for (DistrictRequisitionNumbers districtRequisitionNumbers2 : districtRequisitionNumbers) {
						dReqNoMap.put(districtRequisitionNumbers2.getRequisitionNumber(),districtRequisitionNumbers2);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(historyForJobList.size()>0){
					mapForHiredTeachers=cgService.getHiredList(reqMap,dReqNoMap,lstTeacherDetails,historyForJobList,jobOrder);
				}
				List<JobForTeacher> JFTList = new ArrayList<JobForTeacher>(lstJobForTeacher);
				System.out.println("lstJobForTeacher:::::::::::::beeee:::::::::"+lstJobForTeacher.size());
				for(JobForTeacher jft:JFTList)
				{	
					boolean isNoHired=true;
					try{
						if(mapForHiredTeachers.get(jft.getTeacherId().getTeacherId())!=null && mapForHiredTeachers.get(jft.getTeacherId().getTeacherId()).size()>0){
							isNoHired=false;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					
					if(!isNoHired){
						lstJobForTeacher.remove(jft);
					}
				}
			}
 			System.out.println("lstJobForTeacher:::::::::::::After2:::::::::"+lstJobForTeacher.size());
			tableHtml.append(lstJobForTeacher.size());	
			System.out.println("lstJobForTeacher:: Incomplete::::::::::::::::::::::::::::::::::"+lstJobForTeacher.size());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tableHtml.toString();	
	}

	
	@Transactional(readOnly=true)
	public String getRemoveCandidateReports(String jobId,String firstName,String lastName,String emailAddress,String certType,String regionId,String degreeId,String universityId,String normScoreSelectVal,String 
			normScoreVal,String CGPA,String CGPASelectVal,String contactedVal,String candidateStatus,String stateId,String orderColumn, String sortingOrder,int totalNoOfRecord,String tagsId,boolean callbreakup,int callNoofRecords,
			Integer[] certIds,String YOTESelectVal,String YOTE,String stateId2,String zipCode,String epiFromDate,String epiToDate)
	{
		
		System.out.println("===================== getRemoveCandidateReports ==========================================");
		
		
		Integer districtIdForSpecific = 3702970;
		List<JobForTeacher> lstJobForTeacher = null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		TeacherAssessmentStatus assessmentStatus = null;
		StatusMaster status = null;

		StringBuffer tableHtml = new StringBuffer(); //change name sb to tableHtml for fetch 10-10 record by Ram nath
		List<String> rejectedlst=new ArrayList<String>(); //add by Ram Nath for 10-10 record
		double[] meanArray=null;
		int noOfRecordCheck =totalNoOfRecord;
		boolean smartPractices=false,achievementScore=false,tFA=false,demoClass=false,displayPhoneInterview=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,CGPADis=false,fitFirstNoble=false,fitSecondNoble=false,fitMiami=false,jobAppliedDate=false,videoInterview=false,schoolSelection=false,PNQ=false,senNum=false;
		CandidateGridService cgService=new CandidateGridService();
		CGInviteInterviewAjax cgInviteInterviewAjax = new CGInviteInterviewAjax();
		boolean statusPrivilege=false;
		List<TeacherDetail> teacherList=new ArrayList<TeacherDetail>();
		Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
		Map<String,String> statusNameMap=new HashMap<String,String>();
		List<DistrictAssessmentDetail> districtAssessmentDetailList=new ArrayList<DistrictAssessmentDetail>();
		List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=new ArrayList<OnlineActivityQuestionSet>(); 
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList=new ArrayList<TeacherDistrictAssessmentStatus>();
		List<TeacherDistrictAssessmentAnswerDetail> teacherAssessmentAnswerDetailsList = new ArrayList<TeacherDistrictAssessmentAnswerDetail>();
		try 
		{	
			String tFname = "",tLname = "",roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			//List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			meanArray = new double[lstDomain.size()];

			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			try{
				districtAssessmentDetailList=districtAssessmentJobRelationDAO.findDistrictAssessmentDetailByJobOrder(jobOrder);
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990))
					onlineActivityQuestionSetList=onlineActivityQuestionSetDAO.getOnlineActivityQuestionSetList(jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster());
				if(userMaster.getEntityType()==3){
					SecondaryStatus secondaryStatus=secondaryStatusDAO.findSecondaryStatusObjByJobCategoryAndHBD(jobOrder);
					teacherList=teacherStatusHistoryForJobDAO.findTeacherListByStatusAndSecStatusHBD(jobOrder,secondaryStatus);
					if(teacherList!=null && userMaster.getEntityType()==3){
						statusPrivilege=true;
					}
				}
			if(jobOrder.getDistrictMaster()!=null){
					DistrictMaster districtMasterObj=jobOrder.getDistrictMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					if(districtMasterObj.getDistrictId()==7800038){
						PNQ=true;
						fitFirstNoble=true;
						fitSecondNoble=true;
					}
					if(districtMasterObj.getDistrictId()==1200390){
						try{
							if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Instructional")){
								fitMiami=true;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					if(districtMasterObj.getJobAppliedDate()!=null && districtMasterObj.getJobAppliedDate()){
						jobAppliedDate=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(districtMasterObj.getStatusMasterForCC()!=null && districtMasterObj.getSecondaryStatusForCC()==null){
							schoolSelection=true;
						}else if(districtMasterObj.getStatusMasterForCC()==null && districtMasterObj.getSecondaryStatusForCC()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(jobOrder.getBranchMaster()!=null){
					BranchMaster districtMasterObj=jobOrder.getBranchMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(jobOrder.getHeadQuarterMaster()!=null){
					HeadQuarterMaster hqMasterobj=jobOrder.getHeadQuarterMaster();
					if(hqMasterobj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(hqMasterobj.getDisplayTFA()){
						tFA=true;
					}
					if(hqMasterobj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(hqMasterobj.getDisplayJSI()){
						JSI=true;
					}
					if(hqMasterobj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(hqMasterobj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(hqMasterobj.getDisplayFitScore()){
						fitScore=true;
					}
					if(hqMasterobj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(hqMasterobj.getDisplayPhoneInterview()!=null && hqMasterobj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(hqMasterobj.getCandidateConsiderationStatusId()!=null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()==null){
							schoolSelection=true;
						}else if(hqMasterobj.getCandidateConsiderationStatusId()==null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}catch(Exception e){ e.printStackTrace();}
			if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null){
				if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
					smartPractices=true;
				}
			}

			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			String[] statuss = {"rem"};
			try{
				lstStatusMasters = Utility.getStaticMasters(statuss);
			}catch (Exception e) {
				e.printStackTrace();
			}
			/* filter Add  */
			List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
			List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean certFlag=false;
			boolean teacherFlag=false;
			StateMaster stateMaster=null;
			List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
			if(certType!=null && !certType.equals("") && !certType.equals("0")||!stateId.equals("0")){
				if(!stateId.equals("0")){
					stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
				}
				certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
				if(certificateTypeMasterList.size()>0)
					certTeacherDetailList = teacherCertificateDAO.findCertificateByJobForTeacher(certificateTypeMasterList);
				certFlag=true;
			}
			if(certType.equals("")){
				certFlag=true;
			}
			if(certFlag && certTeacherDetailList.size()>0){
				filterTeacherList.addAll(certTeacherDetailList);
			}
			if(certFlag){
				teacherFlag=true;
			}
			
			//multiple certificate filter
			boolean mCertFlag=false;
			List<Integer> certTypeIdList = new ArrayList<Integer>();
			List<Integer> teacherIdList = new ArrayList<Integer>();
			List<TeacherDetail> mCertTeacherDetailList = new ArrayList<TeacherDetail>();
			if(certIds!=null && !certIds.equals("") && certIds.length>0){
				for(int i=0;i<certIds.length;i++)
				{
					certTypeIdList.add(certIds[i]);
				}
				teacherIdList = teacherCertificateDAO.findTeacherByCertificates(certTypeIdList);
				if(teacherIdList!=null && !teacherIdList.equals("") && teacherIdList.size()>0){
					for(int i=0;i<teacherIdList.size();i++)
					{
						TeacherDetail teacherDetail = new TeacherDetail();
						teacherDetail.setTeacherId(teacherIdList.get(i));
						mCertTeacherDetailList.add(teacherDetail);
					}
				}
				mCertFlag=true;
			}
			if(mCertFlag && mCertTeacherDetailList.size()>0){
				filterTeacherList.addAll(mCertTeacherDetailList);
			}
			if(mCertFlag){
				teacherFlag=true;
			}
			//yote filter
			List<TeacherDetail> expTeacherList = new ArrayList<TeacherDetail>();
			boolean expFlag=false;
			if(YOTE!=null && !YOTE.equals("")&& !YOTE.equals("0")){
				expFlag=true;
				expTeacherList=teacherExperienceDAO.findTeachersByExperience(YOTE,YOTESelectVal);
			}
			if(expFlag && expTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(expTeacherList);
				}else{
					filterTeacherList.addAll(expTeacherList);
				}
			}
			if(expFlag){
				teacherFlag=true;
			}
			//state filter
			List<TeacherDetail> stateTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean stateFlag=false;
			if(stateId2!=null && !stateId2.equals("") && !stateId2.equals("0"))
			{
				stateFlag=true;
				StateMaster stateMaster1 = stateMasterDAO.findById(Long.valueOf(stateId2), false, false);
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByState(stateMaster1);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					stateTeacherDetailList.add(teacherDetail);
				}
			}
			
			if(stateFlag && stateTeacherDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(stateTeacherDetailList);
				else
					filterTeacherList.addAll(stateTeacherDetailList);
			}
			if(stateFlag)
				teacherFlag=true;
			//zip code filter
			List<TeacherDetail> zipCodeTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			boolean zipCodeFlag=false;
			if(zipCode!=null && !zipCode.equals("") && !zipCode.equals("0"))
			{
				zipCodeFlag=true;
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByZipCode(zipCode);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					zipCodeTeacherdetaDetailList.add(teacherDetail);
				}
			}
			
			if(zipCodeFlag && zipCodeTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(zipCodeTeacherdetaDetailList);
				else
					filterTeacherList.addAll(zipCodeTeacherdetaDetailList);
			}
			if(zipCodeFlag)
				teacherFlag=true;
			//********************** EPI Date Filter By Ravindra ***************************
			Date epifDate=null;
			Date epitDate=null;
			boolean epiDateFlag=false;
			List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			try{
				if(!epiFromDate.equals("")){
					epifDate=Utility.getCurrentDateFormart(epiFromDate);
					epiDateFlag=true;
				}
				if(!epiToDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(epiToDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					epitDate=cal2.getTime();
					epiDateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(epiDateFlag)
			{
				StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.getAssessmentCompletedDateTimeTeacherList(1, statusMaster, epifDate, epitDate);
				for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherAssessmentStatus.getTeacherDetail().getTeacherId());
					epiDateTeacherdetaDetailList.add(teacherDetail);
				}
			}
			if(epiDateFlag && epiDateTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag){
					filterTeacherList.retainAll(epiDateTeacherdetaDetailList);
				}
				else{
					filterTeacherList.addAll(epiDateTeacherdetaDetailList);
				}
			}
			if(epiDateFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> degreeTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean degreeIdFlag=false;
			if(degreeId!=null && !degreeId.equals("") && !degreeId.equals("0")){
				degreeIdFlag=true;
				DegreeMaster degreeMaster=degreeMasterDAO.findById(Long.valueOf(degreeId), false,false);
				degreeTeacherDetailList=teacherAcademicsDAO.findTeacherListByDegree(degreeMaster);
			}
			if(degreeId.equals("")){
				degreeIdFlag=true;
			}
			if(degreeIdFlag && degreeTeacherDetailList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(degreeTeacherDetailList);
				}else{
					filterTeacherList.addAll(degreeTeacherDetailList);
				}
			}
			if(degreeIdFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> universityTeacherList = new ArrayList<TeacherDetail>();
			boolean universityFlag=false;
			if(universityId!=null && !universityId.equals("") && !universityId.equals("0")){
				universityFlag=true;
				UniversityMaster universityMaster=universityMasterDAO.findById(Integer.valueOf(universityId), false,false);
				universityTeacherList=teacherAcademicsDAO.findTeacherListByUniversity(universityMaster);
			}
			if(universityId.equals("")){
				universityFlag=true;
			}
			if(universityFlag && universityTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(universityTeacherList);
				}else{
					filterTeacherList.addAll(universityTeacherList);
				}
			}
			if(universityFlag){
				teacherFlag=true;
			}
			//****************************tags filter*************************************************
			List<TeacherDetail> tagsTeacherList = new ArrayList<TeacherDetail>();
			boolean tagsFlag=false;
			if(tagsId!=null && !tagsId.equals("") && !tagsId.equals("0")){
				tagsFlag=true;
				SecondaryStatusMaster secondaryStatusMaster= secondaryStatusMasterDAO.findById(Integer.valueOf(tagsId), false,false);
				//tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndJob(secondaryStatusMaster,jobOrder);
				tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndDistrict(secondaryStatusMaster,districtMaster);
			}
			
			if(tagsFlag && tagsTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(tagsTeacherList);
				}else{
					filterTeacherList.addAll(tagsTeacherList);
				}
			}
			if(tagsFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> regionTeacherList = new ArrayList<TeacherDetail>();
			boolean regionFlag=false;
			if(regionId!=null && !regionId.equals("") && !regionId.equals("0")){
				regionFlag=true;
				regionTeacherList=teacherPreferenceDAO.findRegionByJobForTeacher(regionId);
			}
			if(regionId.equals("")){
				regionFlag=true;
			}
			if(regionFlag && regionTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(regionTeacherList);
				}else{
					filterTeacherList.addAll(regionTeacherList);
				}
			}
			if(regionFlag){
				teacherFlag=true;
			}
			List<TeacherDetail> cgpaTeacherList = new ArrayList<TeacherDetail>();
			boolean cgpaFlag=false;
			if(CGPA!=null && !CGPA.equals("")&& !CGPA.equals("0")){
				cgpaFlag=true;
				cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA(CGPA,CGPASelectVal);
			}
			if(cgpaFlag && cgpaTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(cgpaTeacherList);
				}else{
					filterTeacherList.addAll(cgpaTeacherList);
				}
			}
			if(cgpaFlag){
				teacherFlag=true;
			}
			if(statusPrivilege){
				teacherFlag=true;
				filterTeacherList.addAll(teacherList);
			}
			System.out.println("filterTeacherList:::"+filterTeacherList.size());
			System.out.println("teacherFlag::::::"+teacherFlag);

			if((teacherFlag && filterTeacherList.size()==0) || (!normScoreVal.equals("") && !normScoreVal.equals("0"))){
				lstJobForTeacher = new ArrayList<JobForTeacher>();
			}else{
				lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCG(userMaster,jobOrder,lstStatusMasters,5,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,false,false,null);
			}
			List<JobForTeacher> lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacher);
			List<TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();
			for(JobForTeacher jft:lstJFTremove){
				lstTeacherDetails.add(jft.getTeacherId());
				if(!candidateStatus.equals("0")){
					if(candidateStatus.equals("rem")){
					}else{
						lstJobForTeacher.remove(jft);
					}
				}
				if(!contactedVal.equals("0")){
					if(contactedVal.equalsIgnoreCase("1")){
						if(jft.getLastActivity()==null || jft.getLastActivity().equals("")){
							lstJobForTeacher.remove(jft);
						}
					}else if(contactedVal.equalsIgnoreCase("2")){
						if(jft.getLastActivity()!=null && !jft.getLastActivity().equals("")){
							lstJobForTeacher.remove(jft);
						}
					}
				}
			}
			//////////////////////////////////Get All Hired Teacher///////////////////////////////////////////////
			String[] statusShrotName = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw","ielig","iielig"};
			List<StatusMaster> statusMasters=Utility.getStaticMasters(statusShrotName);
			Map<String,StatusMaster> statusMap=new HashMap<String, StatusMaster>();
			for (StatusMaster statusMasterObj : statusMasters) {
				statusMap.put(statusMasterObj.getStatusShortName(),statusMasterObj);
			}
			Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
			List<TeacherStatusHistoryForJob> historyForJobFilterList = teacherStatusHistoryForJobDAO.findHireTeacherListByHBD(lstTeacherDetails,jobOrder,statusMap);
			List<TeacherStatusHistoryForJob> historyForJobList =new ArrayList<TeacherStatusHistoryForJob>();
			try{
				Map<String,String> historyMap=new HashMap<String, String>();
				Map<String,TeacherStatusHistoryForJob> historyObjMap=new HashMap<String,TeacherStatusHistoryForJob>();
				if(historyForJobFilterList.size()>0)
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobFilterList) {
					String id= teacherStatusHistoryForJob.getJobOrder().getJobId()+"#"+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId();
					String idValue=historyMap.get(id);
					if(idValue!=null){
						idValue+="#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#";
						historyMap.put(id,idValue);
					}else{
						historyMap.put(id,"#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#");
					}
					if(teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp") || teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("hird")){
						historyObjMap.put(id, teacherStatusHistoryForJob);
					}
				}
				if(historyObjMap.size()>0)
				for (Entry<String, TeacherStatusHistoryForJob> entry : historyObjMap.entrySet()){
					TeacherStatusHistoryForJob  historyObj=entry.getValue();
					String id= historyObj.getJobOrder().getJobId()+"#"+historyObj.getTeacherDetail().getTeacherId();
					String idValue=historyMap.get(id);
					if(idValue!=null && !(idValue.contains("rem")|| idValue.contains("dcln") || idValue.contains("widrw"))){
						historyForJobList.add(historyObj);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			List<JobForTeacher> jftOffers=jobForTeacherDAO.findJobByTeacherListForOffers(lstTeacherDetails,jobOrder.getDistrictMaster(),statusMap.get("hird"),statusMap.get("vcomp"));
			List<String> posList=new ArrayList<String>();
			Map<String,String> reqMap=new HashMap<String, String>();
			for (JobForTeacher jobForTeacher : jftOffers){
				try{
					if(jobForTeacher.getRequisitionNumber()!=null){
						posList.add(jobForTeacher.getRequisitionNumber());
						reqMap.put(jobForTeacher.getJobId().getJobId()+"#"+jobForTeacher.getTeacherId().getTeacherId(),jobForTeacher.getRequisitionNumber());
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			List<DistrictRequisitionNumbers> districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(jobOrder.getDistrictMaster(),posList);
			Map<String,DistrictRequisitionNumbers> dReqNoMap = new HashMap<String,DistrictRequisitionNumbers>();
			try{
				for (DistrictRequisitionNumbers districtRequisitionNumbers2 : districtRequisitionNumbers) {
					dReqNoMap.put(districtRequisitionNumbers2.getRequisitionNumber(),districtRequisitionNumbers2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(historyForJobList.size()>0){
				mapForHiredTeachers=cgService.getHiredList(reqMap,dReqNoMap,lstTeacherDetails,historyForJobList,jobOrder);
			}
			List<JobForTeacher> JFTList = new ArrayList<JobForTeacher>(lstJobForTeacher);
			for(JobForTeacher jft:JFTList)
			{	
				boolean isNoHired=true;
				try{
					if(mapForHiredTeachers.get(jft.getTeacherId().getTeacherId())!=null && mapForHiredTeachers.get(jft.getTeacherId().getTeacherId()).size()>0){
						isNoHired=false;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				if(!isNoHired){
					lstJobForTeacher.remove(jft);
				}
			}
			System.out.println(":::::::::remove ::::::::::::::"+lstJobForTeacher.size());
			tableHtml.append(lstJobForTeacher.size());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tableHtml.toString();		
	}
	
	
	@Transactional(readOnly=true)
	public String getVltCandidateReports(String jobId,String firstName,String lastName,String emailAddress,String certType,String regionId,String degreeId,String universityId,String normScoreSelectVal,String 
			normScoreVal,String CGPA,String CGPASelectVal,String contactedVal,String candidateStatus,String stateId,String orderColumn, String sortingOrder,int totalNoOfRecord,String tagsId,boolean callbreakup,int callNoofRecords,
			Integer[] certIds,String YOTESelectVal,String YOTE,String stateId2,String zipCode,String epiFromDate,String epiToDate,String ssnSearch)
	{	
		
		System.out.println("================================== getVltCandidateReports =======================================");
		
		Integer districtIdForSpecific = 3702970;
		Map<String,RawDataForDomain> mapRawDomainScore = null;
		List<JobForTeacher> lstJobForTeacher = null;
		List<JobForTeacher> lstJobForTeacherVlt = null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		TeacherAssessmentStatus assessmentStatus = null;
		StatusMaster status = null;
		StringBuffer tableHtml = new StringBuffer();
		List<String> timeOutlst=new ArrayList<String>(); // add by ram nath for 10-10 record
		double[] meanArray=null;
		int noOfRecordCheck =totalNoOfRecord;
		lstJobForTeacherVlt = new ArrayList<JobForTeacher>();
		boolean achievementScore=false,smartPractices=false,tFA=false,demoClass=false,displayPhoneInterview=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,CGPADis=false,fitFirstNoble=false,fitSecondNoble=false,fitMiami=false,jobAppliedDate=false,videoInterview=false,schoolSelection=false,PNQ=false,senNum=false;
		CandidateGridService cgService=new CandidateGridService();
		CGInviteInterviewAjax cgInviteInterviewAjax = new CGInviteInterviewAjax();
		boolean statusPrivilege=false;
		List<TeacherDetail> teacherList=new ArrayList<TeacherDetail>();
		Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
		Map<String,String> statusNameMap=new HashMap<String,String>();
		List<DistrictAssessmentDetail> districtAssessmentDetailList=new ArrayList<DistrictAssessmentDetail>();
		List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=new ArrayList<OnlineActivityQuestionSet>(); 
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList=new ArrayList<TeacherDistrictAssessmentStatus>();
		List<TeacherDistrictAssessmentAnswerDetail> teacherAssessmentAnswerDetailsList = new ArrayList<TeacherDistrictAssessmentAnswerDetail>();
		try 
		{	
			String tFname = "",tLname = "",roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			//List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			meanArray = new double[lstDomain.size()];

			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			
			try{
				districtAssessmentDetailList=districtAssessmentJobRelationDAO.findDistrictAssessmentDetailByJobOrder(jobOrder);
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990))
					onlineActivityQuestionSetList=onlineActivityQuestionSetDAO.getOnlineActivityQuestionSetList(jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster());
				if(userMaster.getEntityType()==3){
					SecondaryStatus secondaryStatus=secondaryStatusDAO.findSecondaryStatusObjByJobCategoryAndHBD(jobOrder);
					teacherList=teacherStatusHistoryForJobDAO.findTeacherListByStatusAndSecStatusHBD(jobOrder,secondaryStatus);
					if(teacherList!=null && userMaster.getEntityType()==3){
						statusPrivilege=true;
					}
				}
				
				if(jobOrder.getDistrictMaster()!=null){
					DistrictMaster districtMasterObj=jobOrder.getDistrictMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					if(districtMasterObj.getDistrictId()==7800038){
						PNQ=true;
						fitFirstNoble=true;
						fitSecondNoble=true;
					}
					if(districtMasterObj.getDistrictId()==1200390){
						try{
							if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Instructional")){
								fitMiami=true;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					if(districtMasterObj.getJobAppliedDate()!=null && districtMasterObj.getJobAppliedDate()){
						jobAppliedDate=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(districtMasterObj.getStatusMasterForCC()!=null && districtMasterObj.getSecondaryStatusForCC()==null){
							schoolSelection=true;
						}else if(districtMasterObj.getStatusMasterForCC()==null && districtMasterObj.getSecondaryStatusForCC()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(jobOrder.getBranchMaster()!=null){
					BranchMaster districtMasterObj=jobOrder.getBranchMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(jobOrder.getHeadQuarterMaster()!=null){
					HeadQuarterMaster hqMasterobj=jobOrder.getHeadQuarterMaster();
					if(hqMasterobj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(hqMasterobj.getDisplayTFA()){
						tFA=true;
					}
					if(hqMasterobj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(hqMasterobj.getDisplayJSI()){
						JSI=true;
					}
					if(hqMasterobj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(hqMasterobj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(hqMasterobj.getDisplayFitScore()){
						fitScore=true;
					}
					if(hqMasterobj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(hqMasterobj.getDisplayPhoneInterview()!=null && hqMasterobj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(hqMasterobj.getCandidateConsiderationStatusId()!=null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()==null){
							schoolSelection=true;
						}else if(hqMasterobj.getCandidateConsiderationStatusId()==null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}catch(Exception e){ e.printStackTrace();}
			if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null){
				if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
					smartPractices=true;
				}
			}

			TeacherAssessmentStatus teAssesStatusTempBase = null;
			TeacherAssessmentStatus teAssesStatusTempJSI = null;

			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			String[] statuss = {"comp","icomp","hird","scomp","ecomp","vcomp","dcln","ielig","iielig"};
			try{
				lstStatusMasters = Utility.getStaticMasters(statuss);
			}catch (Exception e) {
				e.printStackTrace();
			}
			/* filter Add  */
			List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
			List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean certFlag=false;
			boolean teacherFlag=false;
			StateMaster stateMaster=null;
			List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
			if(certType!=null && !certType.equals("") && !certType.equals("0")||!stateId.equals("0")){
				if(!stateId.equals("0")){
					stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
				}
				certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
				if(certificateTypeMasterList.size()>0)
					certTeacherDetailList = teacherCertificateDAO.findCertificateByJobForTeacher(certificateTypeMasterList);
				certFlag=true;
			}
			if(certType.equals("")){
				certFlag=true;
			}
			if(certFlag && certTeacherDetailList.size()>0){
				filterTeacherList.addAll(certTeacherDetailList);
			}
			if(certFlag){
				teacherFlag=true;
			}
			
			//multiple certificate filter
			boolean mCertFlag=false;
			List<Integer> certTypeIdList = new ArrayList<Integer>();
			List<Integer> teacherIdList = new ArrayList<Integer>();
			List<TeacherDetail> mCertTeacherDetailList = new ArrayList<TeacherDetail>();
			if(certIds!=null && !certIds.equals("") && certIds.length>0){
				for(int i=0;i<certIds.length;i++)
				{
					certTypeIdList.add(certIds[i]);
				}
				teacherIdList = teacherCertificateDAO.findTeacherByCertificates(certTypeIdList);
				if(teacherIdList!=null && !teacherIdList.equals("") && teacherIdList.size()>0){
					for(int i=0;i<teacherIdList.size();i++)
					{
						TeacherDetail teacherDetail = new TeacherDetail();
						teacherDetail.setTeacherId(teacherIdList.get(i));
						mCertTeacherDetailList.add(teacherDetail);
					}
				}
				mCertFlag=true;
			}
			if(mCertFlag && mCertTeacherDetailList.size()>0){
				filterTeacherList.addAll(mCertTeacherDetailList);
			}
			if(mCertFlag){
				teacherFlag=true;
			}
			//yote filter
			List<TeacherDetail> expTeacherList = new ArrayList<TeacherDetail>();
			boolean expFlag=false;
			if(YOTE!=null && !YOTE.equals("")&& !YOTE.equals("0")){
				expFlag=true;
				expTeacherList=teacherExperienceDAO.findTeachersByExperience(YOTE,YOTESelectVal);
			}
			if(expFlag && expTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(expTeacherList);
				}else{
					filterTeacherList.addAll(expTeacherList);
				}
			}
			if(expFlag){
				teacherFlag=true;
			}
			//state filter
			List<TeacherDetail> stateTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean stateFlag=false;
			if(stateId2!=null && !stateId2.equals("") && !stateId2.equals("0"))
			{
				stateFlag=true;
				StateMaster stateMaster1 = stateMasterDAO.findById(Long.valueOf(stateId2), false, false);
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByState(stateMaster1);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					stateTeacherDetailList.add(teacherDetail);
				}
			}
			
			if(stateFlag && stateTeacherDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(stateTeacherDetailList);
				else
					filterTeacherList.addAll(stateTeacherDetailList);
			}
			if(stateFlag)
				teacherFlag=true;
			//zip code filter
			List<TeacherDetail> zipCodeTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			boolean zipCodeFlag=false;
			if(zipCode!=null && !zipCode.equals("") && !zipCode.equals("0"))
			{
				zipCodeFlag=true;
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByZipCode(zipCode);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					zipCodeTeacherdetaDetailList.add(teacherDetail);
				}
			}
			
			if(zipCodeFlag && zipCodeTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(zipCodeTeacherdetaDetailList);
				else
					filterTeacherList.addAll(zipCodeTeacherdetaDetailList);
			}
			if(zipCodeFlag)
				teacherFlag=true;
			
			//********************** EPI Date Filter By Ravindra ***************************
			Date epifDate=null;
			Date epitDate=null;
			boolean epiDateFlag=false;
			List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			try{
				if(!epiFromDate.equals("")){
					epifDate=Utility.getCurrentDateFormart(epiFromDate);
					epiDateFlag=true;
				}
				if(!epiToDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(epiToDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					epitDate=cal2.getTime();
					epiDateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(epiDateFlag)
			{
				StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.getAssessmentCompletedDateTimeTeacherList(1, statusMaster, epifDate, epitDate);
				for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherAssessmentStatus.getTeacherDetail().getTeacherId());
					epiDateTeacherdetaDetailList.add(teacherDetail);
				}
			}
			if(epiDateFlag && epiDateTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag){
					filterTeacherList.retainAll(epiDateTeacherdetaDetailList);
				}
				else{
					filterTeacherList.addAll(epiDateTeacherdetaDetailList);
				}
			}
			if(epiDateFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> degreeTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean degreeIdFlag=false;
			if(degreeId!=null && !degreeId.equals("") && !degreeId.equals("0")){
				degreeIdFlag=true;
				DegreeMaster degreeMaster=degreeMasterDAO.findById(Long.valueOf(degreeId), false,false);
				degreeTeacherDetailList=teacherAcademicsDAO.findTeacherListByDegree(degreeMaster);
			}
			if(degreeId.equals("")){
				degreeIdFlag=true;
			}
			if(degreeIdFlag && degreeTeacherDetailList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(degreeTeacherDetailList);
				}else{
					filterTeacherList.addAll(degreeTeacherDetailList);
				}
			}
			if(degreeIdFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> universityTeacherList = new ArrayList<TeacherDetail>();
			boolean universityFlag=false;
			if(universityId!=null && !universityId.equals("") && !universityId.equals("0")){
				universityFlag=true;
				UniversityMaster universityMaster=universityMasterDAO.findById(Integer.valueOf(universityId), false,false);
				universityTeacherList=teacherAcademicsDAO.findTeacherListByUniversity(universityMaster);
			}
			if(universityId.equals("")){
				universityFlag=true;
			}
			if(universityFlag && universityTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(universityTeacherList);
				}else{
					filterTeacherList.addAll(universityTeacherList);
				}
			}
			if(universityFlag){
				teacherFlag=true;
			}
			//****************************tags filter*************************************************
			List<TeacherDetail> tagsTeacherList = new ArrayList<TeacherDetail>();
			boolean tagsFlag=false;
			if(tagsId!=null && !tagsId.equals("") && !tagsId.equals("0")){
				tagsFlag=true;
				SecondaryStatusMaster secondaryStatusMaster= secondaryStatusMasterDAO.findById(Integer.valueOf(tagsId), false,false);
				tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndDistrict(secondaryStatusMaster,districtMaster);
			}
			
			if(tagsFlag && tagsTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(tagsTeacherList);
				}else{
					filterTeacherList.addAll(tagsTeacherList);
				}
			}
			if(tagsFlag){
				teacherFlag=true;
			}
			
			
			List<TeacherDetail> regionTeacherList = new ArrayList<TeacherDetail>();
			boolean regionFlag=false;
			if(regionId!=null && !regionId.equals("") && !regionId.equals("0")){
				regionFlag=true;
				regionTeacherList=teacherPreferenceDAO.findRegionByJobForTeacher(regionId);
			}
			if(regionId.equals("")){
				regionFlag=true;
			}
			if(regionFlag && regionTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(regionTeacherList);
				}else{
					filterTeacherList.addAll(regionTeacherList);
				}
			}
			if(regionFlag){
				teacherFlag=true;
			}
			List<TeacherDetail> cgpaTeacherList = new ArrayList<TeacherDetail>();
			boolean cgpaFlag=false;
			if(CGPA!=null && !CGPA.equals("")&& !CGPA.equals("0")){
				cgpaFlag=true;
				cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA(CGPA,CGPASelectVal);
			}
			if(cgpaFlag && cgpaTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(cgpaTeacherList);
				}else{
					filterTeacherList.addAll(cgpaTeacherList);
				}
			}
			if(cgpaFlag){
				teacherFlag=true;
			}
			if(statusPrivilege){
				teacherFlag=true;
				filterTeacherList.addAll(teacherList);
			}
			
			/*
			 * 	SSN
			 * */
			TeacherDetail ssnTeacherDetail=null;
			try{
				if(ssnSearch!=null && !ssnSearch.equals("")){
					ssnTeacherDetail = teacherPersonalInfoDAO.findTeacherDetailBySSN(ssnSearch);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}

			/* END */
			
			if((teacherFlag && filterTeacherList.size()==0) || (!normScoreVal.equals("") && !normScoreVal.equals("0"))){
				lstJobForTeacher = new ArrayList<JobForTeacher>();
			}else{
				lstJobForTeacher = new ArrayList<JobForTeacher>();
				//lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCG(null,jobOrder,lstStatusMasters,7,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,false,false,null);
				lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCGWithSSN(null,jobOrder,lstStatusMasters,7,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,false,false,null,ssnTeacherDetail);
			}
			System.out.println("lstJobForTeacher: VLt:::"+lstJobForTeacher.size());
			//------------------map creation start-------
			Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();		
			Map<Integer, Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
			List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
			List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
			for (JobForTeacher jbforteacher : lstJobForTeacher) 
				teacherDetails.add(jbforteacher.getTeacherId());
			if(teacherDetails!=null && teacherDetails.size()>0){
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachersCGOp(teacherDetails);
				for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
					baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
					if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
						isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
					}
				}
			}

			ArrayList<Integer> teacherIds = new ArrayList<Integer>();
			List<String> ssnList = new ArrayList<String>();
			List<String> ssnNBList = new ArrayList<String>();
			
			boolean baseStatusFlag=false;
			boolean epiFlag=false;
			if(jobOrder.getDistrictMaster()!=null){
				if(jobOrder.getDistrictMaster().getNoEPI()==null ||(jobOrder.getDistrictMaster().getNoEPI()!=null && jobOrder.getDistrictMaster().getNoEPI()==false)){
					epiFlag=true;
				}
			}else if(jobOrder.getBranchMaster()!=null){
				if(jobOrder.getBranchMaster().getNoEPI()==null ||(jobOrder.getBranchMaster().getNoEPI()!=null && jobOrder.getBranchMaster().getNoEPI()==false)){
					epiFlag=true;
				}
			}else if(jobOrder.getHeadQuarterMaster()!=null){
				if(jobOrder.getHeadQuarterMaster().getNoEPI()==null ||(jobOrder.getHeadQuarterMaster().getNoEPI()!=null && jobOrder.getHeadQuarterMaster().getNoEPI()==false)){
					epiFlag=true;
				}
			}
			if(epiFlag){
				if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getStatus().equals("A") && jobOrder.getJobCategoryMaster().getBaseStatus()){
					baseStatusFlag=true;
				}
			}
			
			
			List<JobForTeacher> lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacher);	
			
			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI = new ArrayList<TeacherAssessmentStatus>(); //teacherAssessmentStatusDAO.findAssessmentTaken(null, jobOrder);
			List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrderNoDateOP(jobOrder);
			if(assessmentJobRelations1!=null && assessmentJobRelations1.size()>0 && teacherDetails.size()>0){
				AssessmentJobRelation assessmentJobRelation1 = assessmentJobRelations1.get(0);
				lstJSI = teacherAssessmentStatusDAO.findAssessmentTakenByTeacherList(teacherDetails,assessmentJobRelation1.getAssessmentId());
			}
			for(TeacherAssessmentStatus ts : lstJSI){
				mapJSI.put(ts.getTeacherDetail().getTeacherId(), ts);
			}
			System.out.println("lstJFTremove::::::::lstJobForTeacher::::::::::::::::::"+lstJFTremove.size());
			System.out.println("lstJobForTeacherVlt:::::::>>>::::::::::::"+lstJobForTeacherVlt.size());
			for(JobForTeacher jft : lstJFTremove){	

				teAssesStatusTempBase = baseTakenMap.get(jft.getTeacherId().getTeacherId());				
				teAssesStatusTempJSI =mapJSI.get(jft.getTeacherId().getTeacherId()); 
				if((teAssesStatusTempBase!=null && baseStatusFlag) && (teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))){	
					lstJobForTeacherVlt.add(jft);
				}
				else if(teAssesStatusTempJSI!=null && teAssesStatusTempJSI.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
					lstJobForTeacherVlt.add(jft);
				}
			}
			System.out.println("lstJobForTeacherVlt::::after:::::::::::::::"+lstJobForTeacherVlt.size());
			lstStatusMasters=null;
			lstStatusMasters = new ArrayList<StatusMaster>();			
			String[] statussvlt = {"vlt"};
			try{
				lstStatusMasters = Utility.getStaticMasters(statussvlt);
			}catch (Exception e) {
				e.printStackTrace();
			}
			if((teacherFlag && filterTeacherList.size()==0) || (!normScoreVal.equals("") && !normScoreVal.equals("0"))){
				lstJobForTeacher = new ArrayList<JobForTeacher>();
			}else{
				lstJobForTeacher = new ArrayList<JobForTeacher>();
				lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCG(null,jobOrder,lstStatusMasters,7,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,false,false,null);
			}
			System.out.println("lstJobForTeacher:::::::::::::::::::::::"+lstJobForTeacher.size());
			lstJobForTeacherVlt.addAll(lstJobForTeacher);
			System.out.println("lstJobForTeacherVlt::::::::::4:::::::::::::"+lstJobForTeacherVlt.size());
			
			if(teacherDetails!=null && teacherDetails.size()>0){
				for (JobForTeacher jbforteacher : lstJobForTeacherVlt){ 
					teacherDetails.add(jbforteacher.getTeacherId());
					teacherIds.add(jbforteacher.getTeacherId().getTeacherId());
				}
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachersCGOp(teacherDetails);
				for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {				
					baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
				}
			}
			System.out.println("lstJobForTeacherVlt::::::new::::::::::"+lstJobForTeacherVlt.size());
			lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacherVlt);	
			for(JobForTeacher jft : lstJFTremove)
			{	
				if(!candidateStatus.equals("0")){
					if(candidateStatus.equals("vlt")){
					}else if(candidateStatus.equals("hird")){
						if(jft.getStatus().getStatusShortName().equalsIgnoreCase("vlt") || jft.getStatus().getStatusShortName().equalsIgnoreCase("comp") || jft.getStatus().getStatusShortName().equalsIgnoreCase("icomp")){
							lstJobForTeacherVlt.remove(jft);
						}
					}else{
						lstJobForTeacherVlt.remove(jft);
					}
				}
				if(!contactedVal.equals("0")){
					if(contactedVal.equalsIgnoreCase("1")){
						if(jft.getLastActivity()==null || jft.getLastActivity().equals("")){
							lstJobForTeacherVlt.remove(jft);
						}
					}else if(contactedVal.equalsIgnoreCase("2")){
						if(jft.getLastActivity()!=null && !jft.getLastActivity().equals("")){
							lstJobForTeacherVlt.remove(jft);
						}
					}
				}
				teAssesStatusTempBase = baseTakenMap.get(jft.getTeacherId().getTeacherId());
				if((teAssesStatusTempBase==null && baseStatusFlag) || (baseStatusFlag && teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")))
				{	
					lstJobForTeacherVlt.remove(jft);
				}	
			}
			System.out.println("lstJobForTeacherVlt::::::::::::::::"+lstJobForTeacherVlt.size());
			tableHtml.append(lstJobForTeacherVlt.size());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tableHtml.toString();	
	}

	public String getWithdrawnCandidateReports(String jobId,String firstName,String lastName,String emailAddress,String certType,String regionId,String degreeId,String universityId,String normScoreSelectVal,String 
			normScoreVal,String CGPA,String CGPASelectVal,String contactedVal,String candidateStatus,String stateId,String orderColumn, String sortingOrder,int totalNoOfRecord,String tagsId,boolean callbreakup,int callNoofRecords,
			Integer[] certIds,String YOTESelectVal,String YOTE,String stateId2,String zipCode,String epiFromDate,String epiToDate)
	{	
		
		System.out.println(" =============================== getWithdrawnCandidateReports =========================================");
		
		Integer districtIdForSpecific = 3702970;
		//Integer districtIdForSpecific = 100002;

		List<JobForTeacher> lstJobForTeacher = null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		TeacherAssessmentStatus assessmentStatus = null;
		StatusMaster status = null;

		StringBuffer tableHtml = new StringBuffer();
		List<String> withDrawLst=new ArrayList<String>();//add by Ram Nath for 10-10 record
		double[] meanArray=null;
		int noOfRecordCheck =totalNoOfRecord;
		DecimalFormat oneDForm = new DecimalFormat("###,###.0");
		boolean smartPractices=false,achievementScore=false,tFA=false,demoClass=false,displayPhoneInterview=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,CGPADis=false,fitFirstNoble=false,fitSecondNoble=false,fitMiami=false,jobAppliedDate=false,videoInterview=false,schoolSelection=false,PNQ=false,senNum=false;
		CandidateGridService cgService=new CandidateGridService();
		CGInviteInterviewAjax cgInviteInterviewAjax = new CGInviteInterviewAjax();
		boolean statusPrivilege=false;
		List<TeacherDetail> teacherList=new ArrayList<TeacherDetail>();
		Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
		Map<String,String> statusNameMap=new HashMap<String,String>();	
		List<DistrictAssessmentDetail> districtAssessmentDetailList=new ArrayList<DistrictAssessmentDetail>();
		List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=new ArrayList<OnlineActivityQuestionSet>(); 
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList=new ArrayList<TeacherDistrictAssessmentStatus>();
		List<TeacherDistrictAssessmentAnswerDetail> teacherAssessmentAnswerDetailsList = new ArrayList<TeacherDistrictAssessmentAnswerDetail>();
		try 
		{	
			String tFname = "",tLname = "",roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			try{
				districtAssessmentDetailList=districtAssessmentJobRelationDAO.findDistrictAssessmentDetailByJobOrder(jobOrder);
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990))
					onlineActivityQuestionSetList=onlineActivityQuestionSetDAO.getOnlineActivityQuestionSetList(jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster());
				if(userMaster.getEntityType()==3){
					SecondaryStatus secondaryStatus=secondaryStatusDAO.findSecondaryStatusObjByJobCategoryAndHBD(jobOrder);
					teacherList=teacherStatusHistoryForJobDAO.findTeacherListByStatusAndSecStatusHBD(jobOrder,secondaryStatus);
					if(teacherList!=null && userMaster.getEntityType()==3){
						statusPrivilege=true;
					}
				}
				
				if(jobOrder.getDistrictMaster()!=null){
					DistrictMaster districtMasterObj=jobOrder.getDistrictMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					if(districtMasterObj.getDistrictId()==7800038){
						PNQ=true;
						fitFirstNoble=true;
						fitSecondNoble=true;
					}
					if(districtMasterObj.getDistrictId()==1200390){
						try{
							if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Instructional")){
								fitMiami=true;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					if(districtMasterObj.getJobAppliedDate()!=null && districtMasterObj.getJobAppliedDate()){
						jobAppliedDate=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(districtMasterObj.getStatusMasterForCC()!=null && districtMasterObj.getSecondaryStatusForCC()==null){
							schoolSelection=true;
						}else if(districtMasterObj.getStatusMasterForCC()==null && districtMasterObj.getSecondaryStatusForCC()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(jobOrder.getBranchMaster()!=null){
					BranchMaster districtMasterObj=jobOrder.getBranchMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					/*try{
						if(districtMasterObj.getStatusMasterForCC()!=null && districtMasterObj.getSecondaryStatusForCC()==null){
							schoolSelection=true;
						}else if(districtMasterObj.getStatusMasterForCC()==null && districtMasterObj.getSecondaryStatusForCC()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}*/
				}else if(jobOrder.getHeadQuarterMaster()!=null){
					HeadQuarterMaster hqMasterobj=jobOrder.getHeadQuarterMaster();
					if(hqMasterobj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(hqMasterobj.getDisplayTFA()){
						tFA=true;
					}
					if(hqMasterobj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(hqMasterobj.getDisplayJSI()){
						JSI=true;
					}
					if(hqMasterobj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(hqMasterobj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(hqMasterobj.getDisplayFitScore()){
						fitScore=true;
					}
					if(hqMasterobj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(hqMasterobj.getDisplayPhoneInterview()!=null && hqMasterobj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(hqMasterobj.getCandidateConsiderationStatusId()!=null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()==null){
							schoolSelection=true;
						}else if(hqMasterobj.getCandidateConsiderationStatusId()==null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}catch(Exception e){ e.printStackTrace();}
			if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null){
				if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
					smartPractices=true;
				}
			}
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			String[] statuss = {"widrw"};
			try{
				lstStatusMasters = Utility.getStaticMasters(statuss);
			}catch (Exception e) {
				e.printStackTrace();
			}
			/* filter Add  */

			List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
			List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean certFlag=false;
			boolean teacherFlag=false;
			StateMaster stateMaster=null;
			List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
			if(certType!=null && !certType.equals("") && !certType.equals("0")||!stateId.equals("0")){
				if(!stateId.equals("0")){
					stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
				}
				certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
				if(certificateTypeMasterList.size()>0)
					certTeacherDetailList = teacherCertificateDAO.findCertificateByJobForTeacher(certificateTypeMasterList);
				certFlag=true;
			}
			if(certType.equals("")){
				certFlag=true;
			}
			if(certFlag && certTeacherDetailList.size()>0){
				filterTeacherList.addAll(certTeacherDetailList);
			}
			if(certFlag){
				teacherFlag=true;
			}
			
			//multiple certificate filter
			boolean mCertFlag=false;
			List<Integer> certTypeIdList = new ArrayList<Integer>();
			List<Integer> teacherIdList = new ArrayList<Integer>();
			List<TeacherDetail> mCertTeacherDetailList = new ArrayList<TeacherDetail>();
			if(certIds!=null && !certIds.equals("") && certIds.length>0){
				for(int i=0;i<certIds.length;i++)
				{
					certTypeIdList.add(certIds[i]);
				}
				teacherIdList = teacherCertificateDAO.findTeacherByCertificates(certTypeIdList);
				if(teacherIdList!=null && !teacherIdList.equals("") && teacherIdList.size()>0){
					for(int i=0;i<teacherIdList.size();i++)
					{
						TeacherDetail teacherDetail = new TeacherDetail();
						teacherDetail.setTeacherId(teacherIdList.get(i));
						mCertTeacherDetailList.add(teacherDetail);
					}
				}
				mCertFlag=true;
			}
			if(mCertFlag && mCertTeacherDetailList.size()>0){
				filterTeacherList.addAll(mCertTeacherDetailList);
			}
			if(mCertFlag){
				teacherFlag=true;
			}
			//yote filter
			List<TeacherDetail> expTeacherList = new ArrayList<TeacherDetail>();
			boolean expFlag=false;
			if(YOTE!=null && !YOTE.equals("")&& !YOTE.equals("0")){
				expFlag=true;
				expTeacherList=teacherExperienceDAO.findTeachersByExperience(YOTE,YOTESelectVal);
			}
			if(expFlag && expTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(expTeacherList);
				}else{
					filterTeacherList.addAll(expTeacherList);
				}
			}
			if(expFlag){
				teacherFlag=true;
			}
			//state filter
			List<TeacherDetail> stateTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean stateFlag=false;
			if(stateId2!=null && !stateId2.equals("") && !stateId2.equals("0"))
			{
				stateFlag=true;
				StateMaster stateMaster1 = stateMasterDAO.findById(Long.valueOf(stateId2), false, false);
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByState(stateMaster1);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					stateTeacherDetailList.add(teacherDetail);
				}
			}
			
			if(stateFlag && stateTeacherDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(stateTeacherDetailList);
				else
					filterTeacherList.addAll(stateTeacherDetailList);
			}
			if(stateFlag)
				teacherFlag=true;
			//zip code filter
			List<TeacherDetail> zipCodeTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			boolean zipCodeFlag=false;
			if(zipCode!=null && !zipCode.equals("") && !zipCode.equals("0"))
			{
				zipCodeFlag=true;
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByZipCode(zipCode);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					zipCodeTeacherdetaDetailList.add(teacherDetail);
				}
			}
			
			if(zipCodeFlag && zipCodeTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(zipCodeTeacherdetaDetailList);
				else
					filterTeacherList.addAll(zipCodeTeacherdetaDetailList);
			}
			if(zipCodeFlag)
				teacherFlag=true;
			
			//********************** EPI Date Filter By Ravindra ***************************
			Date epifDate=null;
			Date epitDate=null;
			boolean epiDateFlag=false;
			List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			try{
				if(!epiFromDate.equals("")){
					epifDate=Utility.getCurrentDateFormart(epiFromDate);
					epiDateFlag=true;
				}
				if(!epiToDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(epiToDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					epitDate=cal2.getTime();
					epiDateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(epiDateFlag)
			{
				StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.getAssessmentCompletedDateTimeTeacherList(1, statusMaster, epifDate, epitDate);
				for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherAssessmentStatus.getTeacherDetail().getTeacherId());
					epiDateTeacherdetaDetailList.add(teacherDetail);
				}
			}
			if(epiDateFlag && epiDateTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag){
					filterTeacherList.retainAll(epiDateTeacherdetaDetailList);
				}
				else{
					filterTeacherList.addAll(epiDateTeacherdetaDetailList);
				}
			}
			if(epiDateFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> degreeTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean degreeIdFlag=false;
			if(degreeId!=null && !degreeId.equals("") && !degreeId.equals("0")){
				degreeIdFlag=true;
				DegreeMaster degreeMaster=degreeMasterDAO.findById(Long.valueOf(degreeId), false,false);
				degreeTeacherDetailList=teacherAcademicsDAO.findTeacherListByDegree(degreeMaster);
			}
			if(degreeId.equals("")){
				degreeIdFlag=true;
			}
			if(degreeIdFlag && degreeTeacherDetailList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(degreeTeacherDetailList);
				}else{
					filterTeacherList.addAll(degreeTeacherDetailList);
				}
			}
			if(degreeIdFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> universityTeacherList = new ArrayList<TeacherDetail>();
			boolean universityFlag=false;
			if(universityId!=null && !universityId.equals("") && !universityId.equals("0")){
				universityFlag=true;
				UniversityMaster universityMaster=universityMasterDAO.findById(Integer.valueOf(universityId), false,false);
				universityTeacherList=teacherAcademicsDAO.findTeacherListByUniversity(universityMaster);
			}
			if(universityId.equals("")){
				universityFlag=true;
			}
			if(universityFlag && universityTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(universityTeacherList);
				}else{
					filterTeacherList.addAll(universityTeacherList);
				}
			}
			if(universityFlag){
				teacherFlag=true;
			}
			//****************************tags filter*************************************************
			List<TeacherDetail> tagsTeacherList = new ArrayList<TeacherDetail>();
			boolean tagsFlag=false;
			if(tagsId!=null && !tagsId.equals("") && !tagsId.equals("0")){
				tagsFlag=true;
				SecondaryStatusMaster secondaryStatusMaster= secondaryStatusMasterDAO.findById(Integer.valueOf(tagsId), false,false);
				//tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndJob(secondaryStatusMaster,jobOrder);
				tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndDistrict(secondaryStatusMaster,districtMaster);
			}
			
			if(tagsFlag && tagsTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(tagsTeacherList);
				}else{
					filterTeacherList.addAll(tagsTeacherList);
				}
			}
			if(tagsFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> regionTeacherList = new ArrayList<TeacherDetail>();
			boolean regionFlag=false;
			if(regionId!=null && !regionId.equals("") && !regionId.equals("0")){
				regionFlag=true;
				regionTeacherList=teacherPreferenceDAO.findRegionByJobForTeacher(regionId);
			}
			if(regionId.equals("")){
				regionFlag=true;
			}
			if(regionFlag && regionTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(regionTeacherList);
				}else{
					filterTeacherList.addAll(regionTeacherList);
				}
			}
			if(regionFlag){
				teacherFlag=true;
			}
			List<TeacherDetail> cgpaTeacherList = new ArrayList<TeacherDetail>();
			boolean cgpaFlag=false;
			if(CGPA!=null && !CGPA.equals("")&& !CGPA.equals("0")){

				cgpaFlag=true;
				cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA(CGPA,CGPASelectVal);
			}
			if(cgpaFlag && cgpaTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(cgpaTeacherList);
				}else{
					filterTeacherList.addAll(cgpaTeacherList);
				}
			}
			if(cgpaFlag){
				teacherFlag=true;
			}
			if(statusPrivilege){
				teacherFlag=true;
				filterTeacherList.addAll(teacherList);
			}
			if((teacherFlag && filterTeacherList.size()==0) || (!normScoreVal.equals("") && !normScoreVal.equals("0"))){
				lstJobForTeacher = new ArrayList<JobForTeacher>();
			}else{
				//lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCG(null,jobOrder,lstStatusMasters,2,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,false,false,null);
				lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCGOpSql(null,jobOrder,lstStatusMasters,2,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,false,false,null,0,0,orderColumn,sortingOrder);
			}
			System.out.println("Size ::>>"+lstJobForTeacher.size());
			List<JobForTeacher> lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacher);
			List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
			for(JobForTeacher jft:lstJFTremove){
				lstTeacherDetails.add(jft.getTeacherId());
				if(!candidateStatus.equals("0")){
					if(candidateStatus.equals("widrw")){
					}else{
						lstJobForTeacher.remove(jft);
					}
				}
				if(!contactedVal.equals("0")){
					if(contactedVal.equalsIgnoreCase("1")){
						if(jft.getLastActivity()==null || jft.getLastActivity().equals("")){
							lstJobForTeacher.remove(jft);
						}
					}else if(contactedVal.equalsIgnoreCase("2")){
						if(jft.getLastActivity()!=null && !jft.getLastActivity().equals("")){
							lstJobForTeacher.remove(jft);
						}
					}
				}
			}
			System.out.println("After Withdrew ::>>"+lstJobForTeacher.size());
			String[] statusShrotName ={"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw","ielig","iielig"};
			List<StatusMaster> statusMasters=Utility.getStaticMasters(statusShrotName);
			Map<String,StatusMaster> statusMap=new HashMap<String, StatusMaster>();
			for (StatusMaster statusMasterObj : statusMasters) {
				statusMap.put(statusMasterObj.getStatusShortName(),statusMasterObj);
			}
			Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
			List<TeacherStatusHistoryForJob> historyForJobFilterList = teacherStatusHistoryForJobDAO.findHireTeacherListByHBD(lstTeacherDetails,jobOrder,statusMap);
			List<TeacherStatusHistoryForJob> historyForJobList =new ArrayList<TeacherStatusHistoryForJob>();
			try{
				Map<String,String> historyMap=new HashMap<String, String>();
				Map<String,TeacherStatusHistoryForJob> historyObjMap=new HashMap<String,TeacherStatusHistoryForJob>();
				if(historyForJobFilterList.size()>0)
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobFilterList) {
					String id= teacherStatusHistoryForJob.getJobOrder().getJobId()+"#"+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId();
					String idValue=historyMap.get(id);
					if(idValue!=null){
						idValue+="#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#";
						historyMap.put(id,idValue);
					}else{
						historyMap.put(id,"#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#");
					}
					if(teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp") || teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("hird")){
						historyObjMap.put(id, teacherStatusHistoryForJob);
					}
				}
				if(historyObjMap.size()>0)
				for (Entry<String, TeacherStatusHistoryForJob> entry : historyObjMap.entrySet()){
					TeacherStatusHistoryForJob  historyObj=entry.getValue();
					String id= historyObj.getJobOrder().getJobId()+"#"+historyObj.getTeacherDetail().getTeacherId();
					String idValue=historyMap.get(id);
					if(idValue!=null && !(idValue.contains("rem")|| idValue.contains("dcln") || idValue.contains("widrw"))){
						historyForJobList.add(historyObj);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			List<JobForTeacher> jftOffers=jobForTeacherDAO.findJobByTeacherListForOffers(lstTeacherDetails,jobOrder.getDistrictMaster(),statusMap.get("hird"),statusMap.get("vcomp"));
			List<String> posList=new ArrayList<String>();
			Map<String,String> reqMap=new HashMap<String, String>();

			for (JobForTeacher jobForTeacher : jftOffers){
				try{
						if(jobForTeacher.getRequisitionNumber()!=null){
							posList.add(jobForTeacher.getRequisitionNumber());			
							reqMap.put(jobForTeacher.getJobId().getJobId()+"#"+jobForTeacher.getTeacherId().getTeacherId(),jobForTeacher.getRequisitionNumber());		
						}
				}catch(Exception e){

				e.printStackTrace();

				}

			}
			
			List<DistrictRequisitionNumbers> districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(jobOrder.getDistrictMaster(),posList);
			Map<String,DistrictRequisitionNumbers> dReqNoMap = new HashMap<String,DistrictRequisitionNumbers>();
			try{
				for (DistrictRequisitionNumbers districtRequisitionNumbers2 : districtRequisitionNumbers) {
					dReqNoMap.put(districtRequisitionNumbers2.getRequisitionNumber(),districtRequisitionNumbers2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(historyForJobList.size()>0){
				mapForHiredTeachers=cgService.getHiredList(reqMap,dReqNoMap,lstTeacherDetails,historyForJobList,jobOrder);
			}
			List<JobForTeacher> JFTList = new ArrayList<JobForTeacher>(lstJobForTeacher);
			System.out.println("lstJobForTeacher:::::::::::before:::::::::::::"+lstJobForTeacher.size());
			for(JobForTeacher jft:JFTList)
			{	
				boolean isNoHired=true;
				try{
					if(mapForHiredTeachers.get(jft.getTeacherId().getTeacherId())!=null && mapForHiredTeachers.get(jft.getTeacherId().getTeacherId()).size()>0){
						isNoHired=false;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				if(!isNoHired){
					lstJobForTeacher.remove(jft);
				}
			}
			
			System.out.println("lstJobForTeacher:::::::::::::::::::::::::::After:::>>>>"+lstJobForTeacher.size());
			tableHtml.append(lstJobForTeacher.size());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tableHtml.toString();	
	}



	@Transactional(readOnly=true)
	
	
	public String showActDiv(String status,int teacherAssessmentStatusId,String jobForTeacherId)
	{	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");		

		StringBuffer sb = new StringBuffer("");
		StatusMaster statusMaster = WorkThreadServlet.statusMap.get(status);

		TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findById(new Integer(teacherAssessmentStatusId), false, false);
		JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
		int teacherAssessmentStatus_Id=0;
		if(teacherAssessmentStatus!=null){
			teacherAssessmentStatus_Id=teacherAssessmentStatus.getTeacherAssessmentStatusId();
		}
		String disableText="";



		sb.append("<table>");
		if(statusMaster.getStatusShortName().equalsIgnoreCase("hird"))
		{	
			disableText = "disabled";
			sb.append("<tr>");
			sb.append("<td class='tdAct'>");
			sb.append("<input type='radio' style='vertical-align: middle;' name='rdoSecStatus'  value=\"unhird,"+teacherAssessmentStatus_Id+","+jobForTeacher.getJobForTeacherId()+"\">");
			sb.append("<td class='tdAct'>&nbsp;Unhire");
			sb.append("</td>");				
			sb.append("</tr>");
		}
		else if(statusMaster.getStatusShortName().equalsIgnoreCase("rem"))
		{	
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("&nbsp;&nbsp;");
			sb.append("</td>");
			sb.append("</tr>");
		}
		else
		{	

			sb.append("<tr>");
			sb.append("<td class='tdAct'>");
			sb.append("<input type='radio' name='rdoSecStatus' value=\"hird,"+teacherAssessmentStatus_Id+","+jobForTeacher.getJobForTeacherId()+"\">");			
			sb.append("</td>");
			sb.append("<td class='tdAct'>&nbsp;Hire");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td class='tdAct'>");			
			sb.append("<input type='radio'  name='rdoSecStatus' value=\"remove,"+teacherAssessmentStatus_Id+","+jobForTeacher.getJobForTeacherId()+"\">");
			sb.append("</td>");

			sb.append("<td class='tdAct'>&nbsp;Remove");
			sb.append("</td>");

			sb.append("</tr>");
		}
		List<SecondaryStatusMaster>  lstSecondaryStatusMaster = secondaryStatusMasterDAO.findActiveSecStaus();

		TeacherSecondaryStatus teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(jobForTeacher.getTeacherId(), jobForTeacher.getJobId(), userMaster.getSchoolId());

		for(SecondaryStatusMaster secondaryStatusMaster : lstSecondaryStatusMaster){
			if(teacherSecondaryStatus!=null && secondaryStatusMaster.equals(teacherSecondaryStatus.getSecondaryStatusMaster())){
				sb.append("<tr>");
				sb.append("<td class='tdAct'>");
				sb.append("<input type='radio'  "+disableText+"  name='rdoSecStatus' checked value='"+jobForTeacher.getJobForTeacherId()+","+secondaryStatusMaster.getSecStatusId()+","+jobForTeacher.getTeacherId().getTeacherId()+"'>");
				sb.append("</td>");
				sb.append("<td class='tdAct'>&nbsp;"+secondaryStatusMaster.getSecStatusName());
				sb.append("</td>");
				sb.append("</tr>");
			}
			else{
				sb.append("<tr>");
				sb.append("<td class='tdAct' >");
				sb.append("<input type='radio'  "+disableText+" name='rdoSecStatus' value='"+jobForTeacher.getJobForTeacherId()+","+secondaryStatusMaster.getSecStatusId()+","+jobForTeacher.getTeacherId().getTeacherId()+"'>");
				sb.append("</td>");
				sb.append("<td class='tdAct' >&nbsp;"+secondaryStatusMaster.getSecStatusName());
				sb.append("</td>");				
				sb.append("</tr>");
			}			
		}
		sb.append("</table>");

		return sb.toString();
	}


	//*** Save message by Sekhar ****//
	public Boolean saveMessage(int teacherId,String messageSubject,String messageSend,int jobId){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");

		TeacherDetail   teacherdetail =  teacherDetailDAO.findById(teacherId, false, false);
		JobOrder   jobOrder =  jobOrderDAO.findById(jobId, false, false);
		String to=teacherdetail.getEmailAddress();
		try{
			UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
			MessageToTeacher  messageToTeacher= new MessageToTeacher();
			messageToTeacher.setTeacherId(teacherdetail);
			messageToTeacher.setJobId(jobOrder);
			messageToTeacher.setTeacherEmailAddress(to);
			messageToTeacher.setMessageSubject(messageSubject);
			messageToTeacher.setMessageSend(messageSend);
			messageToTeacher.setSenderId(userMaster);
			messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
			messageToTeacher.setEntityType(userMaster.getEntityType());
			messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
			messageToTeacherDAO.makePersistent(messageToTeacher);
			emailerService.sendMailAsHTMLText(to,messageSubject,MailText.messageForDefaultFont(messageSend,userSession));
		}catch(Exception e){
			e.printStackTrace();
		}

		return true;
	}

	public Boolean hireTeacher(String assessmentStatusId, String jobForTeacherId)
	{	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		boolean canHire = true;
		try 
		{
			List<SchoolMaster> lstSchoolMasters = null;
			SchoolInJobOrder schoolInJobOrder = null;
			DistrictMaster districtMaster = null;
			SchoolMaster schoolMaster = null;
			StatusMaster statusMaster = null;
			int noOfHire = 0;

			UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
			JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
			JobOrder jobOrder = jobForTeacher.getJobId();
			if(userMaster.getEntityType()==2)//District - 2 user login type
			{
				districtMaster = userMaster.getDistrictId();				
				statusMaster= WorkThreadServlet.statusMap.get("hird");
				noOfHire = (jobForTeacherDAO.findHireByJobForDistrict(jobOrder, districtMaster)).size();

				lstSchoolMasters = jobOrder.getSchool();

				if(lstSchoolMasters!=null && lstSchoolMasters.size()>0)
				{						
					canHire = false;
				}
				else
				{	
					if(jobOrder.getNoOfExpHires()<=noOfHire)
					{	
						canHire = false;
					}
				}							
			}
			else
			{
				schoolMaster = userMaster.getSchoolId();				
				statusMaster= WorkThreadServlet.statusMap.get("hird");
				noOfHire = (jobForTeacherDAO.findHireByJobForSchool(jobOrder, schoolMaster)).size();
				lstSchoolMasters = jobOrder.getSchool();

				schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMaster);

				if(schoolInJobOrder==null || schoolInJobOrder.getNoOfSchoolExpHires()==null || noOfHire>=schoolInJobOrder.getNoOfSchoolExpHires())
				{	
					canHire = false;
				}
			}


			if(canHire)
			{	
				statusMaster  = WorkThreadServlet.statusMap.get("hird");
				if(statusMaster!=null){
					jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
				}
				jobForTeacher.setStatus(statusMaster);
				jobForTeacher.setUpdatedBy(userMaster);
				jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
				jobForTeacher.setUpdatedDate(new Date());
				jobForTeacherDAO.makePersistent(jobForTeacher);

				TeacherSecondaryStatus teacherSecondaryStatus = null;
				teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(jobForTeacher.getTeacherId(), jobForTeacher.getJobId(), schoolMaster);
				if(teacherSecondaryStatus!=null)
					teacherSecondaryStatusDAO.makeTransient(teacherSecondaryStatus);

				/*if(jobForTeacher.getJobId().getCreatedForEntity().equals(3)){
					teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(jobForTeacher.getTeacherId(), jobForTeacher.getJobId(), schoolMaster);
				}
				else{
					teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(jobForTeacher.getTeacherId(), jobForTeacher.getJobId(), schoolMaster);
				}*/
				//TeacherSecondaryStatus teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(jobForTeacher.getTeacherId(), jobForTeacher.getJobId(), schoolMaster)

			}			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			canHire = false;
		}
		return canHire;
	}

	public String getPhoneDetail(String teacherId){



		StringBuffer sb = new StringBuffer("");
		try {
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(new Integer(teacherId), false, false);

			String phone = null;
			String mobile = null;
			try
			{
				phone = teacherPersonalInfo.getPhoneNumber()==null?"":teacherPersonalInfo.getPhoneNumber();
				mobile = teacherPersonalInfo.getMobileNumber()==null?"":teacherPersonalInfo.getMobileNumber();
			}
			catch(NullPointerException e)
			{
				phone="";
				mobile="";
			}
			if(teacherPersonalInfo !=null && (!(phone.trim().equals("") && mobile.trim().equals("")))){			
				sb.append("<table>");

				if(!phone.trim().equals("")){
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(lblPhoneNo);
					sb.append("</td>");				
					sb.append("<td>");
					sb.append(headPhone);
					sb.append("</td>");
					sb.append("</tr>");
				}

				if(!mobile.trim().equals("")){
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(lblMobileNumber);
					sb.append("</td>");				
					sb.append("<td>");
					sb.append(lblMobile);
					sb.append("</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");	
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

		sb.append("");

		return sb.toString();
	}
	public String getCertificationGrid(int teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType){

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb = new StringBuffer();		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"certType";
			String sortOrderNoField		=	"certType";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("state")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("state")){
					sortOrderNoField="state";
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	


			TeacherDetail teacherDetail =  teacherDetailDAO.findById(teacherId, false, false);;

			List<TeacherCertificate> listTeacherCertificates = null;
			listTeacherCertificates = teacherCertificateDAO.findSortedCertificateByTeacher(sortOrderStrVal,teacherDetail);

			List<TeacherCertificate> sortedlistTeacherCertificates		=	new ArrayList<TeacherCertificate>();

			SortedMap<String,TeacherCertificate>	sortedMap = new TreeMap<String,TeacherCertificate>();
			if(sortOrderNoField.equals("state"))
			{
				sortOrderFieldName	=	"state";
			}
			int mapFlag=2;
			for (TeacherCertificate trCertificate : listTeacherCertificates){
				String orderFieldName=trCertificate.getCertType();
				if(sortOrderFieldName.equals("state")){
					String stateName="";
					if(trCertificate.getStateMaster()==null){
						stateName=" ";
					}else{
						stateName=trCertificate.getStateMaster().getStateName();
					}
					orderFieldName=stateName+"||"+trCertificate.getCertId();
					sortedMap.put(orderFieldName+"||",trCertificate);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}

			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherCertificates=listTeacherCertificates;
			}

			totalRecord =sortedlistTeacherCertificates.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherCertificate> listsortedTeacherCertificates		=	sortedlistTeacherCertificates.subList(start,end);
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);

			String nationalBoard="No";
			if(teacherExperience!=null){
				if(teacherExperience.getNationalBoardCert()){
					nationalBoard="Yes";
				}
				sb.append("<table border='0' width='100%'>");
				sb.append("<tr>");
				sb.append("<td>"+lblCertiTeachExp+": "+teacherExperience.getExpCertTeacherTraining()+"</td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td>"+lblNatiBoardCertiLice+": "+nationalBoard+"</td>");
				sb.append("</tr>");

				if(teacherExperience.getNationalBoardCert()){	
					sb.append("<tr>");
					sb.append("<td>"+lblNatiBoardCertiLice+" Year: "+teacherExperience.getNationalBoardCertYear()+"</td>");
					sb.append("</tr>");
				}
			}else{
				sb.append("<tr>");
				sb.append("<td>"+lblNoCertiprovided+"</td>");
				sb.append("</tr>");
			}
			sb.append("<tr>");
			sb.append("<td  width='100%'><table border='0' id='tblCert' width='100%' class='table table-striped mt30'  >");

			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblSt,sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblYearRece,sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblTpe,sortOrderFieldName,"certType",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			sb.append("</tr>");
			sb.append("</thead>");
			if(teacherCertificateDAO!=null)
				for(TeacherCertificate tCertificate:listsortedTeacherCertificates)
				{
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(tCertificate.getStateMaster().getStateName());
					sb.append("</td>");

					sb.append("<td>");
					sb.append(tCertificate.getYearReceived());
					sb.append("</td>");

					sb.append("<td>");
					sb.append(tCertificate.getCertType());
					sb.append("</td>");
					sb.append("</tr>");
				}

			if(listTeacherCertificates==null || listTeacherCertificates.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}

			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();

	}
	public String getTranscriptGrid(String teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType){

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb = new StringBuffer();		
		try 
		{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"attendedInYear";
			String sortOrderNoField		=	"attendedInYear";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("degree") && !sortOrder.equals("fieldOfStudy") && !sortOrder.equals("university")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("degree")){
					sortOrderNoField="degree";
				}
				if(sortOrder.equals("fieldOfStudy")){
					sortOrderNoField="fieldOfStudy";
				}
				if(sortOrder.equals("university")){
					sortOrderNoField="university";
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/	



			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);

			List<TeacherAcademics> listTeacherAcademics = null;
			//listTeacherAcademics = teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
			listTeacherAcademics = teacherAcademicsDAO.findSortedAcadamicDetailByTeacher(sortOrderStrVal,teacherDetail);


			List<TeacherAcademics> sortedlistTeacherAcademics =	new ArrayList<TeacherAcademics>();

			SortedMap<String,TeacherAcademics>	sortedMap = new TreeMap<String,TeacherAcademics>();
			if(sortOrderNoField.equals("degree"))
			{
				sortOrderFieldName	=	"degree";
			}
			if(sortOrderNoField.equals("fieldOfStudy"))
			{
				sortOrderFieldName	=	"fieldOfStudy";
			}
			if(sortOrderNoField.equals("university"))
			{
				sortOrderFieldName	=	"university";
			}
			int mapFlag=2;
			for (TeacherAcademics trAcademics : listTeacherAcademics){
				String orderFieldName=trAcademics.getAttendedInYear()+"";
				if(sortOrderFieldName.equals("degree")){
					orderFieldName=trAcademics.getDegreeId().getDegreeName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("fieldOfStudy")){
					orderFieldName=trAcademics.getFieldId().getFieldName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("university")){
					orderFieldName=trAcademics.getUniversityId().getUniversityName()+"||"+trAcademics.getAcademicId();
					sortedMap.put(orderFieldName+"||",trAcademics);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}

			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				}
			}else{
				sortedlistTeacherAcademics=listTeacherAcademics;
			}

			totalRecord =sortedlistTeacherAcademics.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherAcademics> listsortedTeacherCertificates	=	sortedlistTeacherAcademics.subList(start,end);

			sb.append("<table border='0' class='table table-striped mt30'   id='tblTrans'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");			
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblDegree,sortOrderFieldName,"degree",sortOrderTypeVal,pgNo);
			sb.append("<th width='15%' valign='top'>"+responseText+"</th>");			
			responseText=PaginationAndSorting.responseSortingLink(lblFieldOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
			sb.append("<th width='12%' valign='top'>"+responseText+"</th>");			
			responseText=PaginationAndSorting.responseSortingLink(lblDatesAttended,sortOrderFieldName,"attendedInYear",sortOrderTypeVal,pgNo);
			sb.append("<th width='14%' valign='top'>"+responseText+"</th>");			
			responseText=PaginationAndSorting.responseSortingLink(optSchool,sortOrderFieldName,"university",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");			
			responseText=PaginationAndSorting.responseSortingLink(lblTranscript,sortOrderFieldName,"pathOfTranscript",sortOrderTypeVal,pgNo);
			sb.append("<th width='14%' valign='top'>"+responseText+"</th>");

			sb.append("<th width='15%'>");
			sb.append(lblGPA);
			sb.append("</th>");						
			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherAcademics!=null)
				for(TeacherAcademics ta:listsortedTeacherCertificates)
				{
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(ta.getDegreeId().getDegreeName());;
					sb.append("</td>");

					sb.append("<td>");
					sb.append(ta.getFieldId().getFieldName());	
					sb.append("</td>");

					sb.append("<td>");
					sb.append(ta.getAttendedInYear()+" to "+ta.getLeftInYear());
					sb.append("</td>");

					sb.append("<td>");
					sb.append(ta.getUniversityId().getUniversityName());	
					sb.append("</td>");

					String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
					sb.append("<td>");								
					sb.append(ta.getPathOfTranscript()==null?"":"<a href='javascript:void(0);' id='trans"+ta.getAcademicId()+"' onclick=\"downloadTranscript('"+ta.getAcademicId()+"');\">"+ta.getPathOfTranscript()+"</a>");
					sb.append("</td>");

					sb.append("<td>");
					if(ta.getDegreeId().getDegreeType().trim().equalsIgnoreCase("b"))
					{
						sb.append("Freshman: "+(ta.getGpaFreshmanYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaFreshmanYear()))+"<br> " +
								" Sophomore: "+(ta.getGpaSophomoreYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaSophomoreYear()))+"<br>" +
								" Junior: "+(ta.getGpaJuniorYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaJuniorYear()))+"<br>" +
								" Senior: "+(ta.getGpaSeniorYear()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaSeniorYear()))+"<br>" +
								" Cumulative: "+(ta.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaCumulative())));
					}
					else
					{
						sb.append("Cumulative:"+(ta.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(ta.getGpaCumulative())));
					}
					sb.append("</td>");				
					sb.append("</tr>");
				}

			if(listTeacherAcademics==null || listTeacherAcademics.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='7'>");
				sb.append(msgNorecordfound);
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();

	}

	public String downloadTranscript(String academicId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		String path="";

		try 
		{


			TeacherAcademics teacherAcademics = teacherAcademicsDAO.findById(new Long(academicId), false, false);

			TeacherDetail teacherDetail = teacherAcademics.getTeacherId();

			String fileName= teacherAcademics.getPathOfTranscript();

			String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+fileName;


			String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";


			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());

			FileUtils.copyFile(sourceFile, targetFile);


			path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();


		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return path;

	}


	public Boolean unHireTeacher(String assessmentStatusId, String jobForTeacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}


		Boolean canUnhire = true;
		try 
		{
			DistrictMaster loginDistrict = null;
			SchoolMaster loginSchool = null;

			DistrictMaster hireDistrict = null;
			SchoolMaster hireSchool = null;

			UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
			JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);

			if(userMaster.getEntityType()==2)//District - 2 user login type
			{
				loginDistrict = userMaster.getDistrictId();	

				if(jobForTeacher.getUpdatedBy().getUserId().equals(userMaster.getUserId()))
				{
					if(!(loginDistrict.getDistrictId().equals(jobForTeacher.getUpdatedBy().getDistrictId().getDistrictId())))
					{

						canUnhire = false;
					}
				}
				else
				{

					canUnhire = false;
				}
			}
			else
			{
				loginSchool = userMaster.getSchoolId();
				if(!loginSchool.getSchoolId().equals(jobForTeacher.getUpdatedBy().getSchoolId().getSchoolId()))
				{

					canUnhire = false;
				}
			}

			if(canUnhire)
			{
				StatusMaster statusMaster  = null;
				boolean isAssesmetnStatus = jobForTeacher.getJobId().getIsJobAssessment();
				if(isAssesmetnStatus)
				{	
					List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(), jobForTeacher.getJobId());
					if(lstTeacherAssessmentStatus==null || lstTeacherAssessmentStatus.size()==0)
					{

						statusMaster  = WorkThreadServlet.statusMap.get("icomp");
					}
					else
					{

						TeacherAssessmentStatus teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
						statusMaster = teacherAssessmentStatus.getStatusMaster();						
					}

				}
				else
				{

					statusMaster  = WorkThreadServlet.statusMap.get("comp");
				}

				if(statusMaster!=null){
					jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
				}
				jobForTeacher.setStatus(statusMaster);
				jobForTeacher.setUpdatedBy(userMaster);
				jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
				jobForTeacher.setUpdatedDate(new Date());
				jobForTeacherDAO.makePersistent(jobForTeacher);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			canUnhire = false;
		}
		return canUnhire;
	}

	public String removeTeacher(String assessmentStatusId, String jobForTeacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		try 
		{

			UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");

			JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
			StatusMaster statusMaster  = WorkThreadServlet.statusMap.get("rem");
			if(statusMaster!=null){
				jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
			}
			jobForTeacher.setStatus(statusMaster);
			jobForTeacher.setUpdatedBy(userMaster);
			jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
			jobForTeacher.setUpdatedDate(new Date());
			jobForTeacherDAO.makePersistent(jobForTeacher);

		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}

	public String downloadResume(String teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String path="";

		try 
		{
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);

			String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+teacherExperience.getResume();
			if(teacherExperience==null)
				return "";

			String resume = teacherExperience.getResume();
			if(resume==null || resume.trim().equals(""))
				return "";

			String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";


			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
			path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			path="";
		}
		System.out.println("Printng Final Path ===="+path);
		return path;
	}

	public String downloadCandidateGridReport(String jobId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}


		try
		{	
			Font font9 = null;
			Font font9bold = null;
			Font font11 = null;
			Font font11bold = null;

			try {
				//tahoma = BaseFont.createFont("c:\\font\\tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

				String fontPath = request.getRealPath("/");
				BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

				font9 = new Font(tahoma, 9);
				font9bold = new Font(tahoma, 9, Font.BOLD);
				font11 = new Font(tahoma, 11);
				font11bold = new Font(tahoma, 11,Font.BOLD);


			} 
			catch (DocumentException e1) 
			{
				e1.printStackTrace();
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
			}


			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);

			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			
			generateCandidateReport(userMaster, jobOrder, basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}

	public String downloadPortfolioReport(String teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String path="";

		try 
		{


			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);

			TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);

			if(!teacherPortfolioStatus.getIsAffidavitCompleted()){
				return "";
			}

			String sourceDir = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+""+teacherDetail.getTeacherId()+"/";
			File file = new File(sourceDir);

			if(!file.exists())
			{
				file.mkdirs();
			}


			path = candidateReportService.generatePortfolioReport(teacherDetail,request,sourceDir+"/"+"portfolio.pdf");

			return Utility.getValueOfPropByKey("contextBasePath")+"teacher/"+""+teacherDetail.getTeacherId()+"/"+"portfolio.pdf";		

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return path;
	}

	public String getCoverLetter(String teacherId, String jobId)
	{
		String coverLetter="";
		try 
		{
			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);			
			JobForTeacher jobForTeacher = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail,jobOrder);
			if(jobForTeacher.getCoverLetter()!=null)
				coverLetter=jobForTeacher.getCoverLetter();

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return coverLetter;
	}

	public String getTranscriptText(String teacherId)
	{	
		StringBuffer sb = new StringBuffer("");
		try {


		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	public String getNotesDetail(String teacherId, String pageNo, String noOfRow,String sortOrder,String sortOrderType)
	{
		StringBuffer sb = new StringBuffer("");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();

		try {		
			//System.out.println("===================="+pageNo+", String "+noOfRow+",String "+sortOrder+" String "+sortOrderType);
			HttpSession session = request.getSession();
			UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
			int roleId=0;
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			String roleAccess=null;
			try
			{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			//-- set start and end position			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord = 0;
			//System.out.println("*************  start "+start+" end "+end);
			String sortOrderFieldName	=	"notesOrder";
			Order  sortOrderStrVal		=	null;

			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			//------------------------------------

			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			List<TeacherNotes> lstTeacherNotes = null;

			if(userMaster.getEntityType().equals(1)){

				lstTeacherNotes = teacherNotesDAO.findByTeacher(teacherDetail,sortOrderStrVal,start,noOfRowInPage);
				totaRecord = teacherNotesDAO.getRowcountByTeacher(teacherDetail);	
			}
			else if(userMaster.getEntityType().equals(2)){//For District

				lstTeacherNotes = teacherNotesDAO.findByTeacherAndDistrict(teacherDetail,userMaster.getDistrictId(),sortOrderStrVal,start,noOfRowInPage);
				totaRecord = teacherNotesDAO.getRowcountByTeacherAndDistrict(teacherDetail,userMaster.getDistrictId());				
			}
			else if(userMaster.getEntityType().equals(3)){//For School

				lstTeacherNotes = teacherNotesDAO.findByTeacherAndSchool(teacherDetail,userMaster.getSchoolId(),sortOrderStrVal,start,noOfRowInPage);
				totaRecord = teacherNotesDAO.getRowcountByTeacherAndSchool(teacherDetail,userMaster.getSchoolId());
			}
			//System.out.println("****************** totaRecord "+totaRecord);
			sb.append("<table border='0' class='table table-bordered table-striped'  id='tblNotes'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th>"+PaginationAndSorting.responseSortingLink(lblDate,sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo)+"</th>");
			sb.append("<th>"+PaginationAndSorting.responseSortingLink(lblSubmittedBy1,sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo)+"</th>");
			sb.append("<th>"+PaginationAndSorting.responseSortingLink(lblNotes,sortOrderFieldName,"note",sortOrderTypeVal,pgNo)+"</th>");
			sb.append("<th>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			String submitedBy = "";
			if(lstTeacherNotes !=null)
				for(TeacherNotes teacherNotes : lstTeacherNotes){
					submitedBy = teacherNotes.getNoteCreatedByUser().getFirstName() +" "+teacherNotes.getNoteCreatedByUser().getLastName();


					sb.append("<tr>");
					sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(teacherNotes.getCreatedDateTime())+"</td>");
					sb.append("<td>"+submitedBy+"</td>");
					sb.append("<td>"+teacherNotes.getNote()+"</td>");
					if(userMaster.getEntityType().equals(1)){

						sb.append("<td><a href='javascript:void(0);' onclick=\"return editNoted('"+teacherNotes.getNoteId()+"','view')\" >"+lblView+"</a></td>");
					}
					else if(userMaster.getEntityType().equals(2)){

						if(teacherNotes.getNoteCreatedByUser().getSchoolId()!=null){
							sb.append("<td><a href='javascript:void(0);' onclick=\"return editNoted('"+teacherNotes.getNoteId()+"','view')\" >"+lblView+"</a></td>");
						}
						else{
							if(roleAccess.indexOf("|2|")!=-1){
								sb.append("<td><a href='javascript:void(0);' onclick=\"return editNoted('"+teacherNotes.getNoteId()+"')\" >"+lblEdit+"</a></td>");
							}else{
								sb.append("<td><a href='javascript:void(0);' onclick=\"return editNoted('"+teacherNotes.getNoteId()+"','view')\" >"+lblView+"</a></td>");					
							}
						}
					}
					else if(userMaster.getEntityType().equals(3)){

						if(roleAccess.indexOf("|2|")!=-1){
							sb.append("<td><a href='javascript:void(0);' onclick=\"return editNoted('"+teacherNotes.getNoteId()+"')\" >"+lblEdit+"</a></td>");
						}else{
							sb.append("<td><a href='javascript:void(0);' onclick=\"return editNoted('"+teacherNotes.getNoteId()+"','view')\" >"+lblView+"</a></td>");					
						}
					}								
					sb.append("</tr>");
				}
			if(lstTeacherNotes == null || lstTeacherNotes.size()==0){
				sb.append("<tr>");				
				sb.append("<td colspan=4>"+msgNorecordfound+"</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationTrippleGrid(request,totaRecord,noOfRow, pageNo));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public String saveNotes(String teacherId, String note, String noteId)
	{	


		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		try{

			TeacherNotes tNotes = null;
			try {
				tNotes = teacherNotesDAO.findById(new Integer(noteId), false, false);				
			} catch (Exception e) {

			}

			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			if(tNotes==null){				
				TeacherNotes teacherNotes = new TeacherNotes();

				teacherNotes.setTeacherDetail(teacherDetail);
				teacherNotes.setNote(note);
				teacherNotes.setNotesOrder(0);
				teacherNotes.setNoteCreatedByUser(userMaster);
				teacherNotes.setNoteCreatedByEntity(userMaster.getEntityType());			
				teacherNotes.setStatus("A");
				teacherNotes.setIpaddress(IPAddressUtility.getIpAddress(request));
				teacherNotes.setCreatedDateTime(new Date());

				teacherNotesDAO.makePersistent(teacherNotes);
				teacherNotes.setNotesOrder(teacherNotes.getNoteId());
				teacherNotesDAO.makePersistent(teacherNotes);
			}
			else{
				if(!note.equals(tNotes.getNote())){

					tNotes.setStatus("I");
					teacherNotesDAO.makePersistent(tNotes);

					TeacherNotes teacherNotes = new TeacherNotes();

					teacherNotes.setTeacherDetail(teacherDetail);
					teacherNotes.setNote(note);
					teacherNotes.setNotesOrder(tNotes.getNotesOrder());
					teacherNotes.setParentNoteId(tNotes);
					teacherNotes.setNoteCreatedByUser(userMaster);
					teacherNotes.setNoteCreatedByEntity(userMaster.getEntityType());			
					teacherNotes.setStatus("A");
					teacherNotes.setIpaddress(IPAddressUtility.getIpAddress(request));
					teacherNotes.setCreatedDateTime(new Date());

					teacherNotesDAO.makePersistent(teacherNotes);
				}
			}


		}catch (Exception e) {
			e.printStackTrace();
		}
		return "Hello saveNotes";
	}

	public TeacherNotes showEditNotes(String notesId)
	{
		TeacherNotes teacherNotes = null;
		try {
			teacherNotes = teacherNotesDAO.findById(new Integer(notesId), false, false);
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherNotes;
	}

	public String saveOrUpdateTeacherSecStatus(String jobForTeacherId, String secStatusId, String teacherId)
	{


		JobForTeacher jobForTeacher = null;
		SecondaryStatusMaster secondaryStatusMaster = null;
		TeacherDetail teacherDetail = null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(msgYrSesstionExp);
		}
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		try {
			jobForTeacher = jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
			secondaryStatusMaster = secondaryStatusMasterDAO.findById(new Integer(secStatusId), false, false);
			teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);

			TeacherSecondaryStatus teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(teacherDetail, jobForTeacher.getJobId(),userMaster.getSchoolId());




			if(teacherSecondaryStatus==null){
				teacherSecondaryStatus = new TeacherSecondaryStatus();
				teacherSecondaryStatus.setTeacherDetail(teacherDetail);
				teacherSecondaryStatus.setJobOrder(jobForTeacher.getJobId());
				teacherSecondaryStatus.setSecondaryStatusMaster(secondaryStatusMaster);
				teacherSecondaryStatus.setCreatedByUser(userMaster);
				teacherSecondaryStatus.setCreatedByEntity(userMaster.getEntityType());
				teacherSecondaryStatus.setDistrictMaster(userMaster.getDistrictId());
				teacherSecondaryStatus.setSchoolMaster(userMaster.getSchoolId());
				teacherSecondaryStatus.setCreatedDateTime(new Date());
			}
			else{
				teacherSecondaryStatus.setSecondaryStatusMaster(secondaryStatusMaster);
			}




			teacherSecondaryStatusDAO.makePersistent(teacherSecondaryStatus);


		} catch (Exception e) {
			e.printStackTrace();
		}

		return "Hello";
	}




	public int getPercentScoreOfBasesAssement(DomainMaster domainMaster, TeacherDetail teacherDetail,Map<Integer,Map<Integer, Object>> mapDomainScore)
	{
	
		double sum=0;
		double maxMark=0;
		double percent=0;

		try 
		{
			Object obj[] = (Object[]) mapDomainScore.get(domainMaster.getDomainId()).get(teacherDetail.getTeacherId());			
			sum = obj[1]==null?0:(Double)obj[1];				
			maxMark = obj[2]==null?0:(Double)obj[2];
			percent = sum/maxMark*100;		


		}		
		catch (Exception e) 
		{
			//System.out.println();
			//e.printStackTrace();
		}

		return (int) Math.round(percent);
	}



	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to generate PDReport.
	 */
	public String generatePDReport(Integer teacher)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		try{

			System.out.println("============ generate PDReport ============");

			String path = request.getContextPath();
			String urlRootPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

			TeacherDetail teacherDetail = null;

			teacherDetail = teacherDetailDAO.findById(teacher, false, false);
			int teacherId = teacherDetail.getTeacherId();

			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/candidatereports/"+teacherId;
			String firstName = Utility.eliminateInvalidUTFChars(teacherDetail.getFirstName().trim());
			String lastName = Utility.eliminateInvalidUTFChars(teacherDetail.getLastName().trim());
			String fileName = firstName+"-"+lastName+"_"+Utility.convertDateAndTimeToDatabaseformatTime(new Date())+".pdf";
			System.out.println("fileName::: "+fileName);
			
			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			//candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);
			String pdpTypeFlag=generatePDPReportNew(request,teacherDetail, basePath,fileName,urlRootPath);
			System.out.println("inside generatePDReport pdpTypeFlag:  "+pdpTypeFlag);
			String temp[]=null;
			if(pdpTypeFlag!=null)
				temp=pdpTypeFlag.split(" ##");
			//System.out.println("Size.."+pdpTypeFlag.split("?").length);
			if(pdpTypeFlag==null)
				return urlRootPath+"candidatereports/"+teacherId+"/"+fileName;
			if(pdpTypeFlag!=null){
				if(temp[1].equals("IPI"))
				return temp[0];
				else if(temp[1].equals("Case3DistrictList"))
					return pdpTypeFlag;
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "Not available";
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to check Pilar Report.
	 */
	public String generatePilarReport(Integer teacher,JobOrder jobOrder)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		try{

			TeacherDetail teacherDetail = null;
			jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);

			if(jobOrder!=null)
			{
				if(jobOrder.getIsJobAssessment()!=true)
				{
					return "0";
				}
			}

			teacherDetail = teacherDetailDAO.findById(teacher, false, false);

			TeacherAssessmentStatus teacherAssessmentStatus = null;
			java.util.List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;

			teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail, jobOrder);

			if(teacherAssessmentStatusList.size()>0)
				teacherAssessmentStatus = teacherAssessmentStatusList.get(0);


			String status = "";
			if(teacherAssessmentStatus==null)
			{
				return "1";
			}else
			{

				status = teacherAssessmentStatus.getStatusMaster().getStatusShortName();

				if(status.equalsIgnoreCase("icomp"))
				{
					return "1";
				}else if(status.equalsIgnoreCase("vlt"))
				{
					return "2";
				}
			}

			return "3";

		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "Not available";
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to generate JSI Report.
	 */
	public String generateJSAReport(Integer teacher,JobOrder jobOrder)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		try{


			String path = request.getContextPath();
			String urlRootPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

			TeacherDetail teacherDetail = null;
			jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);

			teacherDetail = teacherDetailDAO.findById(teacher, false, false);

			int teacherId = teacherDetail.getTeacherId();

			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/candidatereports/"+teacherId;

			String fileName =time+"JSIReport.pdf";

			TeacherAssessmentStatus teacherAssessmentStatus = null;
			TeacherAssessmentdetail teacherAssessmentdetail = null;
			java.util.List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;

			teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail, jobOrder);

			if(teacherAssessmentStatusList.size()>0)
				teacherAssessmentStatus = teacherAssessmentStatusList.get(0);


			String status = "";
			if(teacherAssessmentStatus==null)
			{
				return "1";
			}else
			{

				status = teacherAssessmentStatus.getStatusMaster().getStatusShortName();

				teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();
				if(status.equalsIgnoreCase("icomp"))
				{
					return "1";
				}else if(status.equalsIgnoreCase("vlt"))
				{
					return "2";
				}
			}

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);

			if(teacherAssessmentdetail!=null)
				candidateReportService.generatePilarReport(request,jobOrder,teacherDetail,teacherAssessmentdetail, basePath,fileName,urlRootPath,teacherAssessmentStatus);


			return urlRootPath+"candidatereports/"+teacherId+"/"+fileName;


		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "Not available";

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to checkBaseCompeleted.
	 */
	public String checkBaseCompeleted(Integer teacher)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		try{

			TeacherDetail teacherDetail = null;

			teacherDetail = teacherDetailDAO.findById(teacher, false, false);
			List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
			TeacherAssessmentStatus teacherAssessmentStatus = null;
			JobOrder jobOrder = new JobOrder();
			jobOrder.setJobId(0);
			teacherAssessmentStatusList=teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jobOrder);

			if(teacherAssessmentStatusList.size()!=0)
			{
				teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
				if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
				{
					return "1";
				}else if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
				{
					return "3";
				}
			}



		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "2";
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to notify TM Admins.
	 */
	public String notifyTMAdmins(Integer teacher)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		try{



			TeacherDetail teacherDetail = null;

			teacherDetail = teacherDetailDAO.findById(teacher, false, false);
			emailerService.sendMailAsHTMLText("clientservices@teachermatch.net", "Request for Professional Development (PD) report",MailText.getTeacherPDReportMailText(request,teacherDetail));
			//emailerService.sendMailAsHTMLText("vishwanath@netsutra.com", "Request for Professional Development (PD) report",MailText.getTeacherPDReportMailText(request,teacherDetail));

			return "1";

		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "0";
	}
	
	@Transactional(readOnly=false)
	public Map<String,Object> getCgData(UserMaster userMaster,Integer jobId)
	{
		
		Map<String,Object> dataMap = new HashMap<String, Object>();
		
		Map<Integer,Map<Integer, Object>> mapDomainScore = null;
		double[] compositeScore = null;
		int max = 0;
		int min = 0;
		List<JobForTeacher> lstJobForTeacher = null;
		List<JobForTeacher> lstJobForTeacherVlt = null;
		
		
		int roleId=0;
		if(userMaster.getRoleId().getRoleId()!=null){
			roleId=userMaster.getRoleId().getRoleId();
		}
		
		double domainTotatlScore = 0;
		double score = 0;
		StringBuffer sb = new StringBuffer();	
		double[] meanArray=null;
		int noOfRecordCheck = 0;
		int composite = 0;
		lstJobForTeacherVlt = new ArrayList<JobForTeacher>();
		
		try{
			
			String tFname = "";
			String tLname = "";
			String tEmail = "";
			String roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			//List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;

			meanArray = new double[lstDomain.size()];


			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			TeacherAssessmentStatus teAssesStatusTempBase = null;
			TeacherAssessmentStatus teAssesStatusTempJSI = null;
			//List<TeacherAssessmentStatus> lstTAssesStatusTempJSI=null;


			StatusMaster statusComp = WorkThreadServlet.statusMap.get("comp");
			StatusMaster statusIcomp = WorkThreadServlet.statusMap.get("icomp");
			StatusMaster statusHired = WorkThreadServlet.statusMap.get("hird");
			StatusMaster statusRemove = WorkThreadServlet.statusMap.get("rem");
			StatusMaster statusVlt = WorkThreadServlet.statusMap.get("vlt");

			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			lstStatusMasters.add(statusComp);
			lstStatusMasters.add(statusIcomp);
			lstStatusMasters.add(statusHired);
			lstStatusMasters.add(statusRemove);			

			lstJobForTeacher = jobForTeacherDAO.findJFTbyJobOrder(jobOrder,lstStatusMasters);
			List<JobForTeacher> lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacher);

			boolean baseStatusFlag=false;
			if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getStatus().equals("A") && jobOrder.getJobCategoryMaster().getBaseStatus()){
				baseStatusFlag=true;
			}

			//--------------------------------------------------set global map			
			Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();		
			Map<Integer, Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
			List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
			List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
			for (JobForTeacher jbforteacher : lstJobForTeacher) 
				teacherDetails.add(jbforteacher.getTeacherId());
			if(teacherDetails!=null && teacherDetails.size()>0){
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachersCGOp(teacherDetails);
				for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
					baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
					if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
					{
						isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
						//System.out.println(" Condition Match "+teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")+" ");
					}
				}
			}
			mapDomainScore = new HashMap<Integer, Map<Integer,Object>>();
			Map<Integer, Object> mapPercentScore = null;
			List lstAns = null;
			TeacherDetail tDetail = null;
			for(DomainMaster dm: lstDomain){
				mapPercentScore = new HashMap<Integer, Object>();
				lstAns = teacherAnswerDetailDAO.findSumOfAnsByDomainAndTeacher(null, dm);
				for(Object oo: lstAns){
					Object obj[] = (Object[])oo;
					tDetail = (TeacherDetail) obj[0];
					mapPercentScore.put( tDetail.getTeacherId(), obj);
				}
				mapDomainScore.put(dm.getDomainId(), mapPercentScore);
			}

			/*List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = teacherSecondaryStatusDAO.findByJobAndSchool(jobOrder, userMaster.getSchoolId());
			Map<Integer,TeacherSecondaryStatus> mapTSecStatus = new HashMap<Integer, TeacherSecondaryStatus>();
			for(TeacherSecondaryStatus tSecondaryStatus: lstTeacherSecondaryStatus ){
				mapTSecStatus.put(tSecondaryStatus.getTeacherDetail().getTeacherId(), tSecondaryStatus);
			}*/	


			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI = teacherAssessmentStatusDAO.findAssessmentTaken(null, jobOrder);
			for(TeacherAssessmentStatus ts : lstJSI){
				mapJSI.put(ts.getTeacherDetail().getTeacherId(), ts);
			}
			//--------------------------------------------------set global map----


			for(JobForTeacher jft : lstJFTremove)
			{
				//System.out.println("jft"+jft.getJobForTeacherId());
				//teAssesStatusTempBase = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jft.getTeacherId());
				teAssesStatusTempBase = baseTakenMap.get(jft.getTeacherId().getTeacherId());
				//System.out.println(teAssesStatusTempBase);



				//lstTAssesStatusTempJSI = teacherAssessmentStatusDAO.findAssessmentTaken(jft.getTeacherId(), jft.getJobId());


				/*if(lstTAssesStatusTempJSI!=null && lstTAssesStatusTempJSI.size()>0){

					teAssesStatusTempJSI = teacherAssessmentStatusDAO.findAssessmentTaken(jft.getTeacherId(), jft.getJobId()).get(0);
				}
				else{
					teAssesStatusTempJSI = null;
				}*/

				teAssesStatusTempJSI =mapJSI.get(jft.getTeacherId().getTeacherId()); 


				//if(teAssesStatusTempBase==null || teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
				if((teAssesStatusTempBase==null && baseStatusFlag) || (baseStatusFlag && teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")))
				{
					lstJobForTeacher.remove(jft);
				}
				if((teAssesStatusTempBase!=null && baseStatusFlag) && (teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")))
				{	
					lstJobForTeacherVlt.add(jft);
					lstJobForTeacher.remove(jft);
				}
				else if(teAssesStatusTempJSI!=null && teAssesStatusTempJSI.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
				{
					lstJobForTeacherVlt.add(jft);
					lstJobForTeacher.remove(jft);					
				}
			}

			//------------Check user can hire, unhire or remove start

			DistrictMaster districtMaster = null;
			StatusMaster statusMaster = null;
			List<SchoolMaster> lstSchoolMaster =null;
			SchoolMaster schoolMaster = null;
			SchoolInJobOrder schoolInJobOrder = null;
			boolean doActivity = true;
			if(userMaster.getEntityType()==2)//District - 2 user login type
			{
				districtMaster = userMaster.getDistrictId();
				lstSchoolMaster = jobOrder.getSchool();

				if(lstSchoolMaster!=null && lstSchoolMaster.size()>0){										
					doActivity= false;
				}

				if(jobOrder.getCreatedForEntity().equals(3)){
					doActivity= false;
				}				
			}
			else
			{
				schoolMaster = userMaster.getSchoolId();				
				schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMaster);
				if(schoolInJobOrder==null)
				{					
					doActivity = false;
				}
			}



			int k=0;
			compositeScore = new double[lstJobForTeacher.size()];
			for(JobForTeacher jft:lstJobForTeacher)
			{		
				domainTotatlScore=0.0;
				for(DomainMaster domain: lstDomain){
					score =  getPercentScoreOfBasesAssement(domain,jft.getTeacherId(),mapDomainScore); 
					//domainTotatlScore = domainTotatlScore+score;					
					domainTotatlScore = domainTotatlScore+score*domain.getMultiplier();
				}
				jft.setCompositeScore(domainTotatlScore);
				//compositeScore[k]=domainTotatlScore/lstDomain.size();
				compositeScore[k]=domainTotatlScore;
				k++;
			}

			if(lstJobForTeacher !=null && lstJobForTeacher.size()!=0)				
			{				
				String compData="";
				if(baseStatusFlag)
					if(compositeScore.length>1)
					{
						for(double d:compositeScore){
							compData = compData+d+",";
						}
						compData=compData.substring(0,compData.length()-1);

						String s = null;
						Runtime rt = Runtime.getRuntime();
						//String[]callAndArgs= {"C:\\Python33\\python",Utility.getValueOfPropByKey("rootPath")+"/FinalK.py",compData};
						String[]callAndArgs = {Utility.getValueOfPropByKey("pythonpath"),Utility.getValueOfPropByKey("rootPath")+"/FinalK.py",compData};
						Process proc = rt.exec(callAndArgs);


						BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
						BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

						double[] twoNumbers = new double[2];
						for (int i = 0;(s = stdInput.readLine()) != null; i++) 
						{
							twoNumbers[i] = Double.parseDouble(s);
						}

						min = (int) twoNumbers[0];
						max = (int) twoNumbers[1];
						
						//System.out.println("min: "+min);
						//System.out.println("max: "+max);

						while ((s = stdError.readLine()) != null) 
						{

						}	            
						int exitVal = proc.waitFor();
						proc.destroy();

					}
					else
					{
						min = 0;
						max = 0;					
					}
			}
			else
			{
				max=0;
				min=0;
			}
			
			
			//-- sorting CG view start
			List<JobForTeacher> lstjft = new ArrayList<JobForTeacher>(lstJobForTeacher);
			List<JobForTeacher> lstremoveJFT = new ArrayList<JobForTeacher>();
			for(JobForTeacher jft: lstjft){
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					lstJobForTeacher.remove(jft);
					lstremoveJFT.add(jft);
				}
			}
			Collections.sort(lstJobForTeacher,JobForTeacher.jobForTeacherComparator);
			Collections.sort(lstremoveJFT,JobForTeacher.jobForTeacherComparator);
			lstJobForTeacher.addAll(lstremoveJFT);
			//-- sorting CG view end

			//this is case for one teacher in candidate grid report
			if(compositeScore.length==1)
			{

				for(JobForTeacher jft:lstJobForTeacher)
				{		

					for(DomainMaster domain: lstDomain)
					{
						max =  getPercentScoreOfBasesAssement(domain,jft.getTeacherId(),mapDomainScore);
						break;
					}
					for(DomainMaster domain: lstDomain)
					{
						score =  getPercentScoreOfBasesAssement(domain,jft.getTeacherId(),mapDomainScore); 

						if(max>score)
						{
							max= (int)score;
						}					

					}
				}

			}
			
			///////////////

			List<TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();			
			for(JobForTeacher jft:lstJobForTeacher){
				lstTeacherDetails.add(jft.getTeacherId());
			}

			Map<Integer,Boolean> mapActiveExp = teacherExperienceDAO.findActiveExpStatus(lstTeacherDetails);

			System.out.println("mapActiveExp"+mapActiveExp);
			
			//////////////////
			lstStatusMasters=null;
			lstStatusMasters = new ArrayList<StatusMaster>();			
			lstStatusMasters.add(statusVlt);
			//System.out.println("lstJobForTeacherVlt"+lstJobForTeacherVlt);
			lstJobForTeacherVlt.addAll(jobForTeacherDAO.findJFTbyJobOrder(jobOrder,lstStatusMasters));
			//System.out.println("lstJobForTeacherVlt"+lstJobForTeacherVlt);

			//------------------map creation start-------

			if(teacherDetails!=null && teacherDetails.size()>0){
				for (JobForTeacher jbforteacher : lstJobForTeacherVlt) 
					teacherDetails.add(jbforteacher.getTeacherId());
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachersCGOp(teacherDetails);
				for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {				
					baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
				}
			}


			//------------------map creation end--------
			lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacherVlt);			
			for(JobForTeacher jft : lstJFTremove)
			{				
				//assessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jft.getTeacherId());
				teAssesStatusTempBase = baseTakenMap.get(jft.getTeacherId().getTeacherId());

				if((teAssesStatusTempBase==null && baseStatusFlag) || (baseStatusFlag && teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")))
					//if(teAssesStatusTempBase==null  || teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
				{	
					lstJobForTeacherVlt.remove(jft);
				}				
			}

			//--Sorting CG report data -- start
			lstjft = new ArrayList<JobForTeacher>(lstJobForTeacherVlt);
			lstremoveJFT = new ArrayList<JobForTeacher>();
			for(JobForTeacher jft: lstjft){
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					lstJobForTeacherVlt.remove(jft);
					lstremoveJFT.add(jft);
				}
			}
			Collections.sort(lstJobForTeacherVlt,JobForTeacher.jobForTeacherComparatorTeacher);
			Collections.sort(lstremoveJFT,JobForTeacher.jobForTeacherComparatorTeacher);
			lstJobForTeacherVlt.addAll(lstremoveJFT);
			//--Sorting CG report data -- end


			lstTeacherDetails = new ArrayList<TeacherDetail>();

			for(JobForTeacher jft:lstJobForTeacherVlt){
				lstTeacherDetails.add(jft.getTeacherId());
			}

			dataMap.put("mapDomainScore",mapDomainScore );
			dataMap.put("compositeScore",compositeScore );
			dataMap.put("max", max);
			dataMap.put("min", min);
			dataMap.put("lstJobForTeacher", lstJobForTeacher);
			dataMap.put("lstJobForTeacherVlt", lstJobForTeacherVlt);
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return dataMap;
	}
	@Transactional(readOnly=false)
	public boolean generateCandidateReport(UserMaster userMaster,JobOrder jobOrder, String path,String realPath)
	{

	
		int countNoOfTeacher = 0;
		Map<String,Object> dataMap = getCgData(userMaster,jobOrder.getJobId());
		
		Map<Integer,Map<Integer, Object>> mapDomainScore = (Map<Integer,Map<Integer, Object>>)dataMap.get("mapDomainScore");
		double[] compositeScore = (double[])dataMap.get("compositeScore");
		int max = (Integer)dataMap.get("max");
		int min = (Integer)dataMap.get("min");
		List<JobForTeacher> lstJobForTeacher = (List<JobForTeacher>)dataMap.get("lstJobForTeacher");
		List<JobForTeacher> lstJobForTeacherVlt = (List<JobForTeacher>)dataMap.get("lstJobForTeacherVlt");
		
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		boolean baseStatusFlag=false;
		if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getStatus().equals("A") && jobOrder.getJobCategoryMaster().getBaseStatus()){
			baseStatusFlag=true;
		}

		try
		{	
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;

			try {

				String fontPath = realPath;
				BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
				font8 = new Font(tahoma, 8);

				font8Green = new Font(tahoma, 8);
				font8Green.setColor(Color.BLUE);
				font8bold = new Font(tahoma, 8, Font.BOLD);


				font9 = new Font(tahoma, 9);
				font9bold = new Font(tahoma, 9, Font.BOLD);
				font10 = new Font(tahoma, 10);
				font10bold = new Font(tahoma, 10, Font.BOLD);
				font11 = new Font(tahoma, 11);
				font11bold = new Font(tahoma, 11,Font.BOLD);
				font20bold = new Font(tahoma, 20,Font.BOLD);


			} 
			catch (DocumentException e1) 
			{
				e1.printStackTrace();
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
			}


			//java.util.List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;

			document = new Document(PageSize.A4,10f,10f,10f,10f);		
			document.addAuthor("TeacherMatch");
			document.addCreator("TeacherMatch Inc.");
			document.addSubject("Candidate Grid Report");
			document.addCreationDate();
			document.addTitle("CANDIDATE GRID REPORT");

			fos = new FileOutputStream(path);
			PdfWriter.getInstance(document, fos);

			footerpara = new Paragraph("Generated by TeacherMatch: Created on - "+new Date(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 6,Font.BOLDITALIC,Color.black));
			headerFooter = new HeaderFooter(footerpara,false);
			headerFooter.setAlignment(HeaderFooter.ALIGN_RIGHT);
			headerFooter.setBorder(0);
			document.setFooter(headerFooter);
			document.open();

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			Paragraph [] para = null;
			PdfPCell [] cell = null;


			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20bold);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);

			Image logo = null;
			try {
				logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/TMatch-Logo.png");//images/logo.png
			} catch (Exception e) {
				System.out.println("pathNotFound: "+Utility.getValueOfPropByKey("basePath")+"/images/TMatch-Logo.png");
				logo = Image.getInstance ("https://platform.teachermatch.org/images/TMatch-Logo.png");
				e.printStackTrace();
			}
			//https://platform.teachermatch.org/images/TMatch-Logo.png
			if(logo!=null)
			{
				logo.scalePercent(75);
				cell[1]= new PdfPCell(logo);
			}else
				cell[1]= new PdfPCell(new Paragraph("TeacherMatch",font8bold));

			cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[1].setBorder(0);
			
			para[2] = new Paragraph("CANDIDATE GRID REPORT",font20bold);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);

			for(int i=0;i<2;i++)
				mainTable.addCell(cell[0]);

			mainTable.addCell(cell[1]);
			for(int i=0;i<3;i++)
				mainTable.addCell(cell[0]);
			mainTable.addCell(cell[2]);
			mainTable.addCell(cell[0]);

			if(baseStatusFlag){
				cellSize = lstDomain.size()+3;
			}else{
				cellSize=2;
			}
			if(jobOrder.getDistrictMaster().getDistrictId().equals(3702970)){
				cellSize=cellSize-1;
			}
			document.add(mainTable);

			float[] tblwidth={.07f,.83f};
			mainTable = new PdfPTable(tblwidth);
			mainTable.setWidthPercentage(90);
			para = new Paragraph[2];
			cell = new PdfPCell[2];

			para[0] = new Paragraph("Job Title:",font8bold);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].setBorder(0);
			mainTable.addCell(cell[0]);

			para[1] = new Paragraph(""+jobOrder.getJobTitle(),font8bold);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[1].setBorder(0);
			mainTable.addCell(cell[1]);

			document.add(mainTable);

			int cellCount=0;
			
			//***
			//System.out.println("cellSize>>>>>"+cellSize);
			if(cellSize==6){
				float[] tblwidth1 = {.4f,.12f,.1f,.1f,.12f,.12f};;
				mainTable = new PdfPTable(tblwidth1);
			}
			else if(cellSize==5){
				float[] tblwidth1 = {.5f,.12f,.1f,.1f,.12f};;
				mainTable = new PdfPTable(tblwidth1);
			}
			else{
				mainTable = new PdfPTable(cellSize);
			}
			
			mainTable.setWidthPercentage(90);
			para = new Paragraph[cellSize];
			cell = new PdfPCell[cellSize];

			para[cellCount] = new Paragraph("Candidate",font8bold);
			cell[cellCount] = new PdfPCell(para[cellCount]);
			cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[cellCount].enableBorderSide(1);
			mainTable.addCell(cell[cellCount]);
			cellCount++;

			if(baseStatusFlag)
				for(DomainMaster domain:lstDomain)
				{				
					para[cellCount] = new Paragraph(""+domain.getDomainName(),font8bold);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
					cell[cellCount].enableBorderSide(1);
					mainTable.addCell(cell[cellCount]);
					cellCount++;
				}

			if(baseStatusFlag){
				para[cellCount] = new Paragraph("Composite Score",font8bold);
				cell[cellCount] = new PdfPCell(para[cellCount]);
				cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[cellCount].enableBorderSide(1);
				mainTable.addCell(cell[cellCount]);
				cellCount++;
			}

			if(!jobOrder.getDistrictMaster().getDistrictId().equals(3702970)){
				para[cellCount] = new Paragraph("Status",font8bold);
				cell[cellCount] = new PdfPCell(para[cellCount]);
				cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
	
				cell[cellCount].enableBorderSide(1);
				mainTable.addCell(cell[cellCount]);
				cellCount++;
			}
			
			

			TeacherAssessmentStatus teAssesStatusTemp = null;			


			cellCount = 0;
			String tFname = "";
			String tLname = "";
			String tEmail = "";
			double domainTotatlScore = 0;
			double score = 0;
			int composite = 0;
			TeacherAssessmentStatus assessmentStatus = null;
			double[] meanArray=null;
			String statusShow="";
			StatusMaster status = null;
			Color color = null;

			meanArray = new double[lstDomain.size()];

			TeacherSecondaryStatus teacherSecondaryStatus = null;
			String teacherSecStatus = "";

			
			List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = teacherSecondaryStatusDAO.findByJobAndSchool(jobOrder, userMaster.getSchoolId());
			Map<Integer,TeacherSecondaryStatus> mapTSecStatus = new HashMap<Integer, TeacherSecondaryStatus>();
			for(TeacherSecondaryStatus tSecondaryStatus: lstTeacherSecondaryStatus ){
				mapTSecStatus.put(tSecondaryStatus.getTeacherDetail().getTeacherId(), tSecondaryStatus);
			}
			
			for(JobForTeacher jft:lstJobForTeacher)
			{
				countNoOfTeacher++;
				//teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(jft.getTeacherId(), jft.getJobId(), userMaster.getSchoolId());
				teacherSecondaryStatus = mapTSecStatus.get(jft.getTeacherId().getTeacherId());
				if((teacherSecondaryStatus!=null) && (!userMaster.getEntityType().equals(1))){
					teacherSecStatus = teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusName();
				}
				else{
					teacherSecStatus="";
				}

				cellCount=0;
				if(jft.getTeacherId()!=null)
				{
					tFname = jft.getTeacherId().getFirstName()==null?"":jft.getTeacherId().getFirstName();
					tLname = jft.getTeacherId().getLastName()==null?"":jft.getTeacherId().getLastName();
					tEmail = jft.getTeacherId().getEmailAddress()==null?"":jft.getTeacherId().getEmailAddress();
				}
				assessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jft.getTeacherId());


				para[cellCount] = new Paragraph(""+tFname+" "+tLname+" ("+tEmail+")",font8);
				cell[cellCount] = new PdfPCell(para[cellCount]);
				cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					cell[cellCount].setBackgroundColor(Color.GRAY);
				}
				cell[cellCount].enableBorderSide(1);
				if(!teacherSecStatus.trim().equals("")){									
					Chunk chunk = new Chunk("\n"+teacherSecStatus, font8Green);					
					para[cellCount].add(chunk);
				}
				mainTable.addCell(cell[cellCount]);
				cellCount++;

				domainTotatlScore=0;
				int i=0;
				String colorName="";

				if(baseStatusFlag){
					for(DomainMaster domain: lstDomain)
					{	
						score = getPercentScoreOfBasesAssement(domain,jft.getTeacherId(),mapDomainScore); 
						domainTotatlScore = domainTotatlScore+score;

						if(score >= max)
						{
							color = Color.GREEN;
						}
						else if(score < max && score>=min)
						{
							color = Color.YELLOW;
						}
						else
						{
							color = Color.RED;
						}

						//BaseColor myColor = WebColors.getRGBColor("#A00000");

						para[cellCount] = new Paragraph(""+Math.round(score)+"%",font8);
						cell[cellCount] = new PdfPCell(para[cellCount]);
						cell[cellCount].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell[cellCount].enableBorderSide(1);
						cell[cellCount].setBackgroundColor(color);
						mainTable.addCell(cell[cellCount]);
						cellCount++;


						meanArray[i]=meanArray[i]+score;
						i++;
					}
					//composite =(int) Math.round(domainTotatlScore/lstDomain.size());
					composite =  (int)Math.round(jft.getCompositeScore());

					if(composite >= max)
					{
						color = Color.GREEN;
					}
					else if(composite < max && composite>=min)
					{
						color = Color.YELLOW;
					}
					else
					{
						color = Color.RED;
					}


					para[cellCount] = new Paragraph(""+composite+"%",font8);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell[cellCount].enableBorderSide(1);
					cell[cellCount].setBackgroundColor(color);
					mainTable.addCell(cell[cellCount]);
					cellCount++;
				}
				status = jft.getStatus();

				if(status.getStatusShortName().equalsIgnoreCase("comp") || status.getStatusShortName().equalsIgnoreCase("icomp"))
				{
					statusShow="Available";
				}
				else if(status.getStatusShortName().equalsIgnoreCase("rem"))
				{
					statusShow="Removed";
				}
				else
				{
					statusShow="Hired";
				}

				if(!jobOrder.getDistrictMaster().getDistrictId().equals(3702970)){
					para[cellCount] = new Paragraph(""+statusShow,font8);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
					cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell[cellCount].enableBorderSide(1);
					mainTable.addCell(cell[cellCount]);
					cellCount++;
				}
				
			}

			if(baseStatusFlag)
				if(lstJobForTeacher!=null && lstJobForTeacher.size()>0)
				{
					cellCount = 0;

					para[cellCount] = new Paragraph("Mean",font8bold);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[cellCount].enableBorderSide(1);

					mainTable.addCell(cell[cellCount]);
					double compositMean = 0;
					for(double sum:meanArray)
					{

						para[cellCount] = new Paragraph(""+Math.round(sum/lstJobForTeacher.size())+"%",font8bold);
						cell[cellCount] = new PdfPCell(para[cellCount]);
						cell[cellCount].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell[cellCount].enableBorderSide(1);

						mainTable.addCell(cell[cellCount]);


						compositMean = compositMean+sum/lstJobForTeacher.size();
					}

					double compositSum=0.0;
					for(double d:compositeScore){
						compositSum=compositSum+d;
					}

					//para[cellCount] = new Paragraph(""+Math.round(compositMean/meanArray.length)+"%",font8bold);
					para[cellCount] = new Paragraph(""+Math.round(compositSum/compositeScore.length)+"%",font8bold);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_CENTER);
					cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell[cellCount].enableBorderSide(1);
					mainTable.addCell(cell[cellCount]);


					if(!jobOrder.getDistrictMaster().getDistrictId().equals(3702970)){
						para[cellCount] = new Paragraph("",font8bold);
						cell[cellCount] = new PdfPCell(para[cellCount]);
						cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[cellCount].enableBorderSide(1);
						mainTable.addCell(cell[cellCount]);
					}
					cellCount++;
				}

						
			for(JobForTeacher jft:lstJobForTeacherVlt)
			{	
				countNoOfTeacher++;
				//teacherSecondaryStatus = teacherSecondaryStatusDAO.findByTeacherJobAndSchool(jft.getTeacherId(), jft.getJobId(), userMaster.getSchoolId());
				teacherSecondaryStatus = mapTSecStatus.get(jft.getTeacherId().getTeacherId());
				if((teacherSecondaryStatus!=null) && (!userMaster.getEntityType().equals(1))){
					teacherSecStatus = teacherSecondaryStatus.getSecondaryStatusMaster().getSecStatusName();
				}
				else{
					teacherSecStatus="";
				}

				cellCount=0;
				if(jft.getTeacherId()!=null)
				{
					tFname = jft.getTeacherId().getFirstName()==null?"":jft.getTeacherId().getFirstName();
					tLname = jft.getTeacherId().getLastName()==null?"":jft.getTeacherId().getLastName();
					tEmail = jft.getTeacherId().getEmailAddress()==null?"":jft.getTeacherId().getEmailAddress();
				}
				assessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jft.getTeacherId());


				para[cellCount] = new Paragraph(""+tFname+" "+tLname+" ("+tEmail+")",font8);

				cell[cellCount] = new PdfPCell(para[cellCount]);
				cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					cell[cellCount].setBackgroundColor(Color.GRAY);
				}
				if(!teacherSecStatus.trim().equals("")){
					Chunk chunk = new Chunk("\n"+teacherSecStatus, font8Green);										
					para[cellCount].add(chunk);
				}
				cell[cellCount].enableBorderSide(1);
				mainTable.addCell(cell[cellCount]);
				cellCount++;
				font8.setColor(Color.black);
				domainTotatlScore=0;
				int i=0;
				String colorName="";
				if(baseStatusFlag)
					for(DomainMaster domain: lstDomain)
					{	
						score = getPercentScoreOfBasesAssement(domain,jft.getTeacherId(),mapDomainScore); 
						domainTotatlScore = domainTotatlScore+score;
						//BaseColor myColor = WebColors.getRGBColor("#A00000");
						para[cellCount] = new Paragraph("",font8);
						cell[cellCount] = new PdfPCell(para[cellCount]);
						cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[cellCount].enableBorderSide(1);
						mainTable.addCell(cell[cellCount]);
						cellCount++;
						meanArray[i]=meanArray[i]+score;
						i++;
					}

				status = jft.getStatus();
				if(status.getStatusShortName().equalsIgnoreCase("comp") || status.getStatusShortName().equalsIgnoreCase("icomp") || status.getStatusShortName().equalsIgnoreCase("vlt") )
				{
					statusShow="Timed Out";					
				}
				else if(status.getStatusShortName().equalsIgnoreCase("rem")){
					statusShow="Removed";
				}
				else{
					statusShow="Hired";					
				}	
				if(baseStatusFlag){
					para[cellCount] = new Paragraph("",font8);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
					cell[cellCount].enableBorderSide(1);
					mainTable.addCell(cell[cellCount]);
					cellCount++;
				}

				if(!jobOrder.getDistrictMaster().getDistrictId().equals(3702970)){
					para[cellCount] = new Paragraph(statusShow,font8);
					cell[cellCount] = new PdfPCell(para[cellCount]);
					cell[cellCount].setHorizontalAlignment(Element.ALIGN_LEFT);
					cell[cellCount].setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell[cellCount].enableBorderSide(1);
					mainTable.addCell(cell[cellCount]);
					cellCount++;
				}
			}

			int cellDomainSize=0;
			if(baseStatusFlag){
				cellDomainSize=lstDomain.size()+3;
			}else{
				cellDomainSize=2;
			}
			if(jobOrder.getDistrictMaster().getDistrictId().equals(3702970)){
				cellDomainSize=cellDomainSize-1;
			}
			

			for(int i=0;i<cellDomainSize;i++)
			{

				cellCount=0;
				para[i] = new Paragraph("",font8);
				cell[i] = new PdfPCell(para[i]);
				cell[i].setHorizontalAlignment(Element.ALIGN_LEFT);
				cell[i].setBorder(0);
				mainTable.addCell(cell[i]);				
			}

			PdfPTable tableStaticContent= new PdfPTable(1);
			tableStaticContent .setWidthPercentage(90);

			Paragraph [] paraStaticContent= null;
			PdfPCell [] cellStaticContent = null;

			paraStaticContent = new Paragraph[2];
			cellStaticContent = new PdfPCell[2];


			paraStaticContent[0] = new Paragraph("Competency Domain Definitions",font10bold);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);


			paraStaticContent[0] = new Paragraph("Attitudinal",font10bold);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);

			paraStaticContent[0] = new Paragraph("This score represents the attitudinal factors affecting a candidate�s ability to be a highly effective teacher. Competencies include persistence in the face of adversity, maintaining a positive attitude, and motivation to succeed.",font8);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);



			paraStaticContent[0] = new Paragraph("Cognitive Ability",font10bold);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);

			paraStaticContent[0] = new Paragraph("Candidate scores in the competencies of verbal ability, quantitative ability, and analytical reasoning.",font8);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);


			paraStaticContent[0] = new Paragraph("Teaching Skills",font10bold);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);

			paraStaticContent[0] = new Paragraph("Candidate scores in instructing, creating a learning environment, planning for successful outcomes, and analyzing and adjusting.",font8);
			cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
			cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellStaticContent[0].setBorder(0);
			tableStaticContent.addCell(cellStaticContent[0]);

			float[] PartucularWidth={.05f,.9f};
			PdfPTable table = new PdfPTable(PartucularWidth); 

			Image image = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/greenbox5px.png");//images/logo.png

			PdfPCell cell1 = new PdfPCell(image, false);
			PdfPCell cell2 = new PdfPCell(new Paragraph("Green is "+Math.round(max)+"% - 100% (Better than most on this job order)",font8));


			if(baseStatusFlag){
				paraStaticContent[0] = new Paragraph("Key",font10bold);
				cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);

				cell1.setPaddingTop(4.5f);
				cell1.setPaddingLeft(3);
				cell1.setBorder(0);
				cell2.setBorder(0);

				table.addCell(cell1);
				table.addCell(cell2);

				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);



				table = new PdfPTable(PartucularWidth); 
				image = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/yellowbox5px.png");//images/logo.png            
				cell1 = new PdfPCell(image, false);
				cell1.setPaddingTop(4.5f);
				cell1.setPaddingLeft(3);
				cell1.setBorder(0);
				cell2 = new PdfPCell(new Paragraph("Yellow is "+Math.round(min)+"% - "+((Math.round(max)-1)<0?"0":""+(Math.round(max)-1))+"% (About the same as most on this job order)",font8));
				cell2.setBorder(0);            
				table.addCell(cell1);
				table.addCell(cell2);
				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);


				table = new PdfPTable(PartucularWidth); 
				image = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/redbox5px.png");//images/logo.png            
				cell1 = new PdfPCell(image, false);
				cell1.setPaddingTop(4.5f);
				cell1.setPaddingLeft(3);
				cell1.setBorder(0);
				cell2 = new PdfPCell(new Paragraph("Red is 0% - "+((Math.round(min)-1)<0?"0":""+(Math.round(min)-1))+"% (Worse than most on this job order)",font8));
				cell2.setBorder(0);            
				table.addCell(cell1);
				table.addCell(cell2);
				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);
			}

			if(!jobOrder.getDistrictMaster().getDistrictId().equals(3702970)){
				float[] PartucularWidth1={.21f,.74f};
				table = new PdfPTable(PartucularWidth1); 
	
				cell1 = new PdfPCell(new Paragraph("Removed",font8bold));
				cell1.setBorder(0);
				cell2 = new PdfPCell(new Paragraph("Indicates removal by an Administrator",font8));
				cell2.setBorder(0);            
				table.addCell(cell1);
				table.addCell(cell2);
				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);
	
	
				table = new PdfPTable(PartucularWidth1);
				cell1 = new PdfPCell(new Paragraph("Available",font8bold));
				cell1.setBorder(0);
				cell2 = new PdfPCell(new Paragraph("Applied for position and is available for Hire",font8));
				cell2.setBorder(0);            
				table.addCell(cell1);
				table.addCell(cell2);
				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);
	
	
				table = new PdfPTable(PartucularWidth1);            
				cell1 = new PdfPCell(new Paragraph("Hired",font8bold));
				cell1.setBorder(0);
				cell2 = new PdfPCell(new Paragraph("Indicates hired for this position (Can be Unhired)",font8));
				cell2.setBorder(0);            
				table.addCell(cell1);
				table.addCell(cell2);
				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);
	
				table = new PdfPTable(PartucularWidth1);            
				cell1 = new PdfPCell(new Paragraph("Timed Out",font8bold));
				cell1.setBorder(0);
				cell2 = new PdfPCell(new Paragraph("Indicates a timeout violation; no scores displayed",font8));
				cell2.setBorder(0);            
				table.addCell(cell1);
				table.addCell(cell2);
				cellStaticContent[0] = new PdfPCell(table);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);
	
	
				paraStaticContent[0] = new Paragraph("",font8);
				cellStaticContent[0] = new PdfPCell(paraStaticContent[0]);
				cellStaticContent[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				cellStaticContent[0].setBorder(0);
				tableStaticContent.addCell(cellStaticContent[0]);
			}

			float[] PartucularWidth0={.6f,.3f};
			PdfPTable tableContainer = new PdfPTable(PartucularWidth0);
			tableContainer .setWidthPercentage(90);


			PdfPCell [] cellContainer = null;


			cellContainer = new PdfPCell[2];



			cellContainer[0] = new PdfPCell(mainTable);
			cellContainer[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellContainer[0].setPaddingTop(5);
			cellContainer[0].setBorder(0);


			tableContainer.addCell(cellContainer[0]);

			cellContainer[1] = new PdfPCell(tableStaticContent);
			cellContainer[1].setHorizontalAlignment(Element.ALIGN_LEFT);
			cellContainer[1].setBorder(0);
			tableContainer.addCell(cellContainer[1]);


			document.add(tableContainer);
			document.newPage();

		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(document != null && document.isOpen())
				document.close();
			if(writer!=null){
				writer.flush();
				writer.close();
			}
		}
		
		if(countNoOfTeacher==0)			
			return false;
		else
			return true;
	}
	
	@Transactional(readOnly=false)
	public boolean generateCandidateReportData(UserMaster userMaster,JobOrder jobOrder, String path,String realPath,List<StatusMaster> lstStatusMasters)
	{

		
		List<JobForTeacher> lstJobForTeacher = null;
		List<JobForTeacher> lstJobForTeacherVlt = null;
		
		lstJobForTeacherVlt = new ArrayList<JobForTeacher>();
		
		try{

			TeacherAssessmentStatus teAssesStatusTempBase = null;
			TeacherAssessmentStatus teAssesStatusTempJSI = null;
			StatusMaster statusVlt = null;
			
			for (StatusMaster statusMaster : lstStatusMasters) {
				if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt"))
					{
						statusVlt = statusMaster;
						break;
					}
			}

			lstJobForTeacher = jobForTeacherDAO.findJFTbyJobOrder(jobOrder,lstStatusMasters);
			List<JobForTeacher> lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacher);

			boolean baseStatusFlag=false;
			if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getStatus().equals("A") && jobOrder.getJobCategoryMaster().getBaseStatus()){
				baseStatusFlag=true;
			}

			//--------------------------------------------------set global map			
			Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();		
			Map<Integer, Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
			List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
			List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
			for (JobForTeacher jbforteacher : lstJobForTeacher) 
				teacherDetails.add(jbforteacher.getTeacherId());
			if(teacherDetails!=null && teacherDetails.size()>0){
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachersCGOp(teacherDetails);
				for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
					baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
					if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
					{
						isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
						//System.out.println(" Condition Match "+teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")+" ");
					}
				}
			}
			
			/*List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = teacherSecondaryStatusDAO.findByJobAndSchool(jobOrder, userMaster.getSchoolId());
			Map<Integer,TeacherSecondaryStatus> mapTSecStatus = new HashMap<Integer, TeacherSecondaryStatus>();
			for(TeacherSecondaryStatus tSecondaryStatus: lstTeacherSecondaryStatus ){
				mapTSecStatus.put(tSecondaryStatus.getTeacherDetail().getTeacherId(), tSecondaryStatus);
			}	
*/

			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI = teacherAssessmentStatusDAO.findAssessmentTaken(null, jobOrder);
			for(TeacherAssessmentStatus ts : lstJSI){
				mapJSI.put(ts.getTeacherDetail().getTeacherId(), ts);
			}
			//--------------------------------------------------set global map----


			for(JobForTeacher jft : lstJFTremove)
			{
				teAssesStatusTempBase = baseTakenMap.get(jft.getTeacherId().getTeacherId());

				teAssesStatusTempJSI =mapJSI.get(jft.getTeacherId().getTeacherId()); 


				if((teAssesStatusTempBase==null && baseStatusFlag) || (baseStatusFlag && teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")))
				{
					lstJobForTeacher.remove(jft);
				}
				if((teAssesStatusTempBase!=null && baseStatusFlag) && (teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")))
				{	
					lstJobForTeacherVlt.add(jft);
					lstJobForTeacher.remove(jft);
				}
				else if(teAssesStatusTempJSI!=null && teAssesStatusTempJSI.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
				{
					lstJobForTeacherVlt.add(jft);
					lstJobForTeacher.remove(jft);					
				}
			}

			//-- sorting CG view start
			List<JobForTeacher> lstjft = new ArrayList<JobForTeacher>(lstJobForTeacher);
			List<JobForTeacher> lstremoveJFT = new ArrayList<JobForTeacher>();
			for(JobForTeacher jft: lstjft){
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					lstJobForTeacher.remove(jft);
					lstremoveJFT.add(jft);
				}
			}
			Collections.sort(lstJobForTeacher,JobForTeacher.jobForTeacherComparator);
			Collections.sort(lstremoveJFT,JobForTeacher.jobForTeacherComparator);
			lstJobForTeacher.addAll(lstremoveJFT);
			//-- sorting CG view end
			///////////////

			List<TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();			
			for(JobForTeacher jft:lstJobForTeacher){
				lstTeacherDetails.add(jft.getTeacherId());
			}

			/*Map<Integer,Boolean> mapActiveExp = teacherExperienceDAO.findActiveExpStatus(lstTeacherDetails);
			System.out.println("mapActiveExp"+mapActiveExp);*/
			
			//////////////////
			lstStatusMasters=null;
			lstStatusMasters = new ArrayList<StatusMaster>();			
			lstStatusMasters.add(statusVlt);
			//System.out.println("lstJobForTeacherVlt"+lstJobForTeacherVlt);
			lstJobForTeacherVlt.addAll(jobForTeacherDAO.findJFTbyJobOrder(jobOrder,lstStatusMasters));
			//System.out.println("lstJobForTeacherVlt"+lstJobForTeacherVlt);

			//------------------map creation start-------

			if(teacherDetails!=null && teacherDetails.size()>0){
				for (JobForTeacher jbforteacher : lstJobForTeacherVlt) 
					teacherDetails.add(jbforteacher.getTeacherId());
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachersCGOp(teacherDetails);
				for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {				
					baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
				}
			}


			//------------------map creation end--------
			lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacherVlt);			
			for(JobForTeacher jft : lstJFTremove)
			{				
				//assessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jft.getTeacherId());
				teAssesStatusTempBase = baseTakenMap.get(jft.getTeacherId().getTeacherId());

				if((teAssesStatusTempBase==null && baseStatusFlag) || (baseStatusFlag && teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")))
				{	
					lstJobForTeacherVlt.remove(jft);
				}				
			}

			//--Sorting CG report data -- start
			lstjft = new ArrayList<JobForTeacher>(lstJobForTeacherVlt);
			lstremoveJFT = new ArrayList<JobForTeacher>();
			for(JobForTeacher jft: lstjft){
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
					lstJobForTeacherVlt.remove(jft);
					lstremoveJFT.add(jft);
				}
			}
			Collections.sort(lstJobForTeacherVlt,JobForTeacher.jobForTeacherComparatorTeacher);
			Collections.sort(lstremoveJFT,JobForTeacher.jobForTeacherComparatorTeacher);
			lstJobForTeacherVlt.addAll(lstremoveJFT);
			//--Sorting CG report data -- end


			lstTeacherDetails = new ArrayList<TeacherDetail>();

			for(JobForTeacher jft:lstJobForTeacherVlt){
				lstTeacherDetails.add(jft.getTeacherId());
			}

			int countNoOfTeacher = 0;
			
			try
			{	
				for(JobForTeacher jft:lstJobForTeacher)
				{
					countNoOfTeacher++;
				}		
				for(JobForTeacher jft:lstJobForTeacherVlt)
				{	
					countNoOfTeacher++;
				}

			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			if(countNoOfTeacher==0)			
				return false;
			else
				return true;
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public String downloadGeneralKnowledge(Integer generalKnowledgeExamId,Integer teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		String path="";
		try 
		{
			TeacherDetail  teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam = teacherGeneralKnowledgeExamDAO.findTeacherGeneralKnowledgeExam(teacherDetail);
			String fileName= teacherGeneralKnowledgeExam.getGeneralKnowledgeScoreReport();
	        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+fileName;
	        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        if(sourceFile.exists()){
	        	FileUtils.copyFile(sourceFile, targetFile);
	        	path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
	        }	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;
	
	}
	public String downloadSubjectAreaExam(Integer subjectAreaExamId,Integer teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		String path="";
		try 
		{
			TeacherDetail  teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			TeacherSubjectAreaExam teacherSubjectAreaExam = teacherSubjectAreaExamDAO.findTeacherSubjectAreaExam(teacherDetail);
			String fileName= teacherSubjectAreaExam.getScoreReport();
	        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+fileName;
	        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
	        File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        if(sourceFile.exists()){
	        	FileUtils.copyFile(sourceFile, targetFile);
	        	path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
	        }	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;
	
	}
	//added by Ram Nath 16-04-2015
	public String getIncompleteInfoHtml(Integer jobForTeacherId){
		JobForTeacher jobForTeacher=jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId+""),false,false);
		System.out.println("::::::::::-:Job For Teacher :"+jobForTeacher.getJobForTeacherId());
		StringBuffer buffer=new StringBuffer();
		buffer.append(""+msgApplicationRequirements1+": <br/><br/>");
		boolean status=true;
		int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
		List<DistrictMaster> districtDSQList = new ArrayList<DistrictMaster>();
		List<DistrictMaster> districts = new ArrayList<DistrictMaster>();
			districts.add(jobForTeacher.getJobId().getDistrictMaster());
		
			//if(jobForTeacher.getFlagForDspq())
			String isAffilated="";
			if(jobForTeacher.getIsAffilated()!=null && jobForTeacher.getIsAffilated().equals(1))
				isAffilated="I";
			else
				isAffilated="E";
			
			try {
				
			
			//DistrictPortfolioConfig districtPortfolioConfig=districtPortfolioConfigDAO.getPortfolioConfig(jobForTeacher.getJobId().getDistrictMaster(),isAffilated,jobForTeacher.getJobId());
			DistrictPortfolioConfig districtPortfolioConfig=null;
			if(jobForTeacher.getJobId().getDistrictMaster()!=null){
				districtPortfolioConfig=districtPortfolioConfigDAO.getPortfolioConfig(jobForTeacher.getJobId().getDistrictMaster(),isAffilated,jobForTeacher.getJobId());
			}	
			boolean dspq=false;
			if(districtPortfolioConfig!=null){
				//dspq=true;
				if(jobForTeacher.getFlagForDspq()==null || !jobForTeacher.getFlagForDspq()){
					status=false;
					buffer.append("-"+lblDSPQ1+"<br/>");
				}
			}
			} catch (Exception e) {
				// TODO: handle exception
				//e.printStackTrace();
			}
			
		int teacherQQTaken=0;
		if(districts.size()>0){
			districtDSQList = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistricts(districts);
			if(districtDSQList.size()>0){
				teacherQQTaken=1;
				districtDSQList = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findByQQTakenByTeacher(districts,jobForTeacher.getTeacherId());
				if(districtDSQList.size()>0){
					teacherQQTaken=2;
				}
			}
		}
		if(teacherQQTaken==1){
			status=false;
			buffer.append("-Qualification Items<br/>");
		}
		TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
		JobOrder jobOrder =  jobForTeacher.getJobId();
		int jobAssessmentStatus =0;
		if(jobOrder.getJobAssessmentStatus()!=null){
			jobAssessmentStatus=jobOrder.getJobAssessmentStatus();
		}
		if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getDistrictMaster()!=null){
		 	if(jobOrder.getDistrictMaster().getDistrictId()==4218990 && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Principal") && jobForTeacher.getIsAffilated()==1){
		 		jobAssessmentStatus=0;
		 	}
		 }
		 System.out.println("Teacher Id "+jobForTeacher.getTeacherId().getTeacherId()+" jobAssessmentStatus::: "+jobAssessmentStatus);
		 
		TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
		StatusMaster statusMaster = null;
		statusMaster = WorkThreadServlet.statusMap.get("icomp");
		AssessmentDetail assessmentDetail = null;
		TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jobForTeacher.getTeacherId());
		try
		{
			
			
			System.out.println("isAffilated:::"+flagList[0]+ " isPortfolio::::"+flagList[1] +" internalFlag::::"+flagList[2]+" offerEPI:::"+flagList[2] +" offerJSI:::"+flagList[4]);
			if((teacherDetail.getIsPortfolioNeeded()) && teacherPortfolioStatus==null && flagList[2]==0 && flagList[1]==1)
			{
				System.out.println("1");
				status=false;
				buffer.append("-"+lblPortfolio+"<br/>");
			}
			if(flagList[1]==1 && teacherPortfolioStatus!=null  && (!(teacherDetail.getIsPortfolioNeeded()&& teacherPortfolioStatus.getIsPersonalInfoCompleted() && teacherPortfolioStatus.getIsAcademicsCompleted() && teacherPortfolioStatus.getIsCertificationsCompleted() && teacherPortfolioStatus.getIsExperiencesCompleted() && teacherPortfolioStatus.getIsAffidavitCompleted()))){
				status=false;
				buffer.append("-"+lblPortfolio+"<br/>");
				System.out.println("Pportfolio::::::::::");
			}
			if(jobOrder.getIsJobAssessment()){
				System.out.println("::::::::::::::::::::::1");
				if(jobAssessmentStatus==1){
					System.out.println("::::::::::::::::::::::2");
					List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrderForCG(jobOrder);
					if(lstAssessmentJobRelation!=null && lstAssessmentJobRelation.size()>0){
						System.out.println(lstAssessmentJobRelation.get(0).getAssessmentJobRelationId()+"::::::::::::::::::::::3");
						AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
						assessmentDetail = assessmentJobRelation.getAssessmentId();
						System.out.println("assessmentDetail:::"+assessmentDetail);
						
						if(assessmentDetail.getAssessmentId()!=null){
							System.out.println("assessmentDetail::::"+assessmentDetail.getAssessmentId());
						}
						List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail, assessmentDetail);
						if(lstTeacherAssessmentStatus!=null && lstTeacherAssessmentStatus.size()>0){	
							System.out.println("3");
							TeacherAssessmentStatus teacherAssessmentStatusJSI = lstTeacherAssessmentStatus.get(0);
							statusMaster = teacherAssessmentStatusJSI.getStatusMaster();
							if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
								System.out.println("4");
								status=false;
								buffer.append("-"+lblJobSpeInve+"<br/>");
							}
						}else{
							status=false;
							buffer.append("-"+lblJobSpeInve+"<br/>");
							System.out.println("5");
						}	
					}else{
							status=false;
							buffer.append("-"+lblJobSpeInve+"<br/>");
							System.out.println("6");
					}					
				}/*else{
					buffer.append("-"+lblJobSpeInve+"<br/>");
					System.out.println("7");
				}*/
			}
			if(jobOrder.getJobCategoryMaster().getBaseStatus()!=null && jobOrder.getJobCategoryMaster().getBaseStatus() && flagList[0]==0){
				if(teacherAssessmentStatus==null){
					System.out.println("8");
					status=false;
					buffer.append("-"+epi+"<br/>");
				}else{
					System.out.println("::::::::::::::::::::::9");
					statusMaster = teacherAssessmentStatus.getStatusMaster();
					if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
						System.out.println("::::::::::::::::::::::10");
						status=false;
						buffer.append("-"+epi+"<br/>");
					}
				}
			}
			if(jobForTeacher.getJobId().getIsInviteOnly()!=null && jobForTeacher.getJobId().getIsInviteOnly()){
				buffer.append("-Has not accepted Invitation<br/>");
				status=false;
			}
			if(status){
				List<JobOrder> list=jobOrderDAO.findJobWithPoolConditionByJobId(jobForTeacher.getJobId().getJobId());
				if(list.size()==0){
					buffer.append("-Job is expired<br/>");
				}
			}
		}
		catch(Exception n)
		{
			n.printStackTrace();
			//return 0;
		}
		return buffer.toString();
	}
	//ended by Ram Nath 16-04-2015
	
	public String getDspqQuestions(Map<String ,List<DistrictSpecificPortfolioAnswers>>  mapDistSpeciAnswerDetailList,List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestions,String teacherId,PrintDataPDF printDataPDF){
		StringBuffer sb=new StringBuffer();
		try {
			List<DistrictSpecificPortfolioAnswers> districtSpecificPortfolioAnswers= mapDistSpeciAnswerDetailList.get(teacherId);
			System.out.println("districtSpecificPortfolioAnswers   "+districtSpecificPortfolioAnswers.size());
			Map<Integer, DistrictSpecificPortfolioAnswers> map = new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
			for (DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers2 : districtSpecificPortfolioAnswers) {
				map.put(districtSpecificPortfolioAnswers2.getDistrictSpecificPortfolioQuestions().getQuestionId(), districtSpecificPortfolioAnswers2);
			}
			if(districtSpecificPortfolioAnswers.size()>0)
			{
				sb.append("<tr><td style='font-size:17px; padding-top:10px;padding-bottom:10px; margin-left:0px;'><b>District Specific Questions</b></td></tr>");
				printDataPDF.appendTextIntoPdf("<tr><td style='font-size:17px; padding-top:10px;padding-bottom:10px; margin-left:0px;'><b>District Specific Questions</b></td></tr>", false);
			}
			DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswersLop = null;
			List<DistrictSpecificPortfolioOptions> questionOptionsList = null;
			String questionInstruction = null;
			String shortName = "";
			int jobCategoryId=0;
			String selectedOptions = "",insertedRanks="";
			int totalQuestions = districtSpecificPortfolioQuestions.size();
			int cnt = 0;
				for (DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions2 : districtSpecificPortfolioQuestions) 
				{cnt++;
					shortName = districtSpecificPortfolioQuestions2.getQuestionTypeMaster().getQuestionTypeShortName();
					sb.append("<tr style='pointer-events: none'>");
					sb.append("<td width='90%'>");
					sb.append("<div style='font-size: 15px;font-weight: normal; line-height: 20px;'><b>Q "+cnt+". </b>"+Utility.getUTFToHTML(districtSpecificPortfolioQuestions2.getQuestion())+"</div>");				
					sb.append("<div style='font-size: 15px;font-weight: normal; line-height: 20px;'><b>Ans "+cnt+". </b></div>");
					
					printDataPDF.appendTextIntoPdf("<tr><td><b>Q "+cnt+". </b>"+districtSpecificPortfolioQuestions2.getQuestion()+"</td></tr>", false);
					printDataPDF.appendTextIntoPdf("<tr><td><b>Ans "+cnt+". </b></td></tr>", false);
					
					questionOptionsList = districtSpecificPortfolioQuestions2.getQuestionOptions();
					districtSpecificPortfolioAnswersLop = map.get(districtSpecificPortfolioQuestions2.getQuestionId());

					String checked = "";
					Integer optId = 0;
					String insertedText = "";
					if(districtSpecificPortfolioAnswers!=null)
					{
						if(!shortName.equalsIgnoreCase("OSONP") && !shortName.equalsIgnoreCase("mloet") && !shortName.equalsIgnoreCase("DD") && !shortName.equalsIgnoreCase("mlsel") && !shortName.equalsIgnoreCase("rt") && !shortName.equalsIgnoreCase("sscb") && shortName.equalsIgnoreCase(districtSpecificPortfolioAnswersLop.getQuestionType()))
						{
							if(districtSpecificPortfolioAnswersLop.getSelectedOptions()!="" && districtSpecificPortfolioAnswersLop.getSelectedOptions()!=null)
								optId = Integer.parseInt(districtSpecificPortfolioAnswersLop.getSelectedOptions());

							insertedText = districtSpecificPortfolioAnswersLop.getInsertedText();
						}
						if(shortName.equalsIgnoreCase("mloet") || shortName.equalsIgnoreCase("OSONP") || shortName.equalsIgnoreCase("sscb"))
						{
							insertedText = districtSpecificPortfolioAnswersLop.getInsertedText();
						}
					}
					
					if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel")|| shortName.equalsIgnoreCase("lkts"))
					{
						sb.append("<table>");
						for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
							if(optId.equals(questionOptions.getOptionId()))
								checked = "checked";
							else
								checked = "";
							if(checked.equalsIgnoreCase("checked")){
								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
										" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
								
								printDataPDF.appendTextIntoPdf("<tr><td>"+questionOptions.getQuestionOption()+"</td></tr>", false);
							}
						}
						sb.append("</table>");
					}
					if(shortName.equalsIgnoreCase("rt")){	
							if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswersLop.getInsertedRanks()!=null){
								insertedRanks = districtSpecificPortfolioAnswersLop.getInsertedRanks()==null?"":districtSpecificPortfolioAnswersLop.getInsertedRanks();
							}
							String[] ranks = insertedRanks.split("\\|");
							int rank=1;
							int count=0;
							String ans = "";
							sb.append("<table>");
							Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
							for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
								optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
							}
							for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
								try{ans =ranks[count];}catch(Exception e){}
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' id='rnkS"+rank+"' name='rankS' class='span1' maxlength='1' onkeypress='return checkForIntUpto6(event)' onblur='checkUniqueRankForPortfolio(this);' value='"+ans+"'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
								printDataPDF.appendTextIntoPdf("<tr><td>"+ans+"</td></tr>",false);
								printDataPDF.appendTextIntoPdf("<tr><td>"+teacherQuestionOption.getQuestionOption()+"</td></tr>",false);
								sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"' /><input type='hidden' name='optS' value='"+teacherQuestionOption.getOptionId()+"' />");
								sb.append("<input type='hidden' name='o_rankS' value='"+(teacherQuestionOption.getRank()==null?0:teacherQuestionOption.getRank())+"' />");
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('rnkS1').focus();");
								sb.append("</script></td></tr>");
								rank++;
								count++;
							}
							sb.append("</table >");
					}
					if(shortName.equalsIgnoreCase("mlsel")){	
						if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswersLop.getSelectedOptions()!=null){
							selectedOptions = districtSpecificPortfolioAnswersLop.getSelectedOptions()==null?"":districtSpecificPortfolioAnswersLop.getSelectedOptions();
						}
						System.out.println("selectedOptions::::>:"+selectedOptions);
						String[] multiCounts = selectedOptions.split("\\|");
						int multiCount=1;
						
						sb.append("<table>");
						Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
						for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
							optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
						}
						String ansId="";
						for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
							try{
								ansId="";
								for(int i=0;i<multiCounts.length;i++){
									try{ansId =multiCounts[i];}catch(Exception e){}
									//System.out.println("ansId:::"+ansId);
									if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
										//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
										if(ansId!=null && !ansId.equals("")){
											ansId="checked";
										}
										break;
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							if(ansId.equalsIgnoreCase("checked")){
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
								sb.append("</td></tr>");
								printDataPDF.appendTextIntoPdf("<tr><td>"+teacherQuestionOption.getQuestionOption()+"</td></tr>",false);
							}
							multiCount++;
							
						}
						sb.append("</table >");
					}
											
					if(shortName.equalsIgnoreCase("mloet")){	
						if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswersLop.getSelectedOptions()!=null){
							selectedOptions = districtSpecificPortfolioAnswersLop.getSelectedOptions()==null?"":districtSpecificPortfolioAnswersLop.getSelectedOptions();
						}
						System.out.println("selectedOptions::::>:"+selectedOptions);
						String[] multiCounts = selectedOptions.split("\\|");
						int multiCount=1;
						
						sb.append("<table>");
						Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
						for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
							optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
						}
						String ansId="";
						for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
							try{
								ansId="";
								for(int i=0;i<multiCounts.length;i++){
									try{ansId =multiCounts[i];}catch(Exception e){}
									if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
										if(ansId!=null && !ansId.equals("")){
											ansId="checked";
										}
										break;
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							if(ansId.equalsIgnoreCase("checked")){
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
								sb.append("</td></tr>");
								printDataPDF.appendTextIntoPdf("<tr><td>"+teacherQuestionOption.getQuestionOption()+"</td></tr>",false);
							}
							multiCount++;
							
						}
						sb.append("</table >");
						String questionInstructions="";
						try{
							if(districtSpecificPortfolioQuestions2.getQuestionInstructions()!=null){
								questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions2.getQuestionInstructions();	
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						
						sb.append(questionInstructions+"</br><span style=''><textarea  name='QS"+cnt+"optmloet' id='QS"+cnt+"optmloet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
						sb.append("<script type=\"text/javascript\" language=\"javascript\">");
						//sb.append("document.getElementById('QS"+cnt+"optmloet').focus();");
						sb.append("</script>");
						printDataPDF.appendTextIntoPdf("<tr><td>"+questionInstructions+"</td></tr>",false);
						printDataPDF.appendTextIntoPdf("<tr><td>"+insertedText+"</td></tr>",false);
					}
					if(shortName.equalsIgnoreCase("it"))
					{
						sb.append("<table >");
						for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
							sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='optS' value='"+teacherQuestionOption.getOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherQuestionOption.getQuestionOption()+"'> ");
							sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'  /></td></tr>");
							//printDataPDF.appendTextIntoPdf("<tr><td>"+questionInstructions+"</td></tr>",false);
						}
						sb.append("</table>");
					}
					if(shortName.equalsIgnoreCase("et"))
					{
						sb.append("<table>");
						for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
							if(optId.equals(questionOptions.getOptionId()))
								checked = "checked";
							else
								checked = "";
							if(checked.equalsIgnoreCase("checked")){
								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
										" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
								printDataPDF.appendTextIntoPdf("<tr><td>"+questionOptions.getQuestionOption()+"</td></tr>",false);
							}
						}
						sb.append("</table>");

						String questionInstructions="";
						try{
							if(districtSpecificPortfolioQuestions2.getQuestionInstructions()!=null){
								questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions2.getQuestionInstructions()+"</br>";	
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						
						sb.append(questionInstructions+"&nbsp;&nbsp;<span style=''><textarea  name='QS"+cnt+"optet' id='QS"+cnt+"optet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
						sb.append("<script type=\"text/javascript\" language=\"javascript\">");
						//sb.append("document.getElementById('QS"+cnt+"optet').focus();");
						sb.append("</script>");
						printDataPDF.appendTextIntoPdf("<tr><td>"+questionInstructions+"</td></tr>",false);
						printDataPDF.appendTextIntoPdf("<tr><td>"+insertedText+"</td></tr>",false);
					}
					if(shortName.equalsIgnoreCase("sl"))
					{
						//Years of certified teaching experience
						if(districtSpecificPortfolioQuestions2.getQuestion().contains("Years of certified teaching experience")){									
							String uniqueChkId ="QS"+cnt+"opt";
							String checkedCheckBox="";
							String fieldDisable="";
							if(districtSpecificPortfolioAnswers==null){
								checkedCheckBox="";
								fieldDisable="";
							}else{
								if(insertedText.equalsIgnoreCase("0.0")){
									checkedCheckBox="checked";
									fieldDisable="disabled";
								}
							}
							
							String questionCustomInstructions="";
							try{
								if(districtSpecificPortfolioQuestions2.getQuestionInstructions()!=null){
									questionCustomInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions2.getQuestionInstructions();	
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							sb.append("&nbsp;&nbsp;<div class='row'><div class='col-sm-4 col-md-4'><input type='text' name='QS"+cnt+"opt' "+fieldDisable+" onkeypress='return checkForDecimalTwo(event);' id='QS"+cnt+"opt' value='"+insertedText+"' maxlength='100' class='form-control'/></div>");
							sb.append("<div class='col-sm-4 col-md-4'><input type='checkbox' id='QS"+cnt+"exp' "+checkedCheckBox+" style='margin-top: 10px;'>"+questionCustomInstructions+"</div></div><br>");
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								sb.append("$('#QS"+cnt+"exp').change(function(){ $(this).each(function(){if($(this).prop('checked')){$('#"+uniqueChkId+"').val('0.0');$('#"+uniqueChkId+"').prop('disabled',true);}else{$('#"+uniqueChkId+"').val('');$('#"+uniqueChkId+"').prop('disabled',false);}});});");
							sb.append("</script>");
							printDataPDF.appendTextIntoPdf("<tr><td>"+insertedText+"</td></tr>",false);
							printDataPDF.appendTextIntoPdf("<tr><td>"+questionCustomInstructions+"</td></tr>",false);
						}else{
							sb.append("&nbsp;&nbsp;<input type='text' name='QS"+cnt+"opt' id='QS"+cnt+"opt' value='"+insertedText+"' maxlength='100' style='width:98%;margin:5px;'/><br/>");
							printDataPDF.appendTextIntoPdf("<tr><td>"+insertedText+"</td></tr>",false);
						}								
					}else if(shortName.equalsIgnoreCase("ml"))
					{
						sb.append("&nbsp;&nbsp;<textarea name='QS"+cnt+"opt' id='QS"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");					
						sb.append("<script type=\"text/javascript\" language=\"javascript\">");
						//sb.append("document.getElementById('QS"+cnt+"opt').focus();");
						sb.append("</script>");
						printDataPDF.appendTextIntoPdf("<tr><td>"+insertedText+"</td></tr>",false);
						if(districtSpecificPortfolioQuestions2.getQuestionId().equals(8)){
							String dbScName= "";
							String dbScId="";
							try{
								if(districtSpecificPortfolioAnswersLop.getSchoolMaster()!=null && !districtSpecificPortfolioAnswersLop.getSchoolMaster().equals("")){
									dbScName=districtSpecificPortfolioAnswersLop.getSchoolMaster().getSchoolName();
									dbScId=Long.toString(districtSpecificPortfolioAnswersLop.getSchoolMaster().getSchoolId());
								}
							}catch (Exception e) {
								// TODO: handle exception
							}
							sb.append("<label>"+lblSchoolName+"</label><br><input type='text' value='"+dbScName+"' id='schoolName' maxlength='100' name='schoolName' class='form-control' placeholder='' onfocus=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onkeyup=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onblur=\"hideSchoolMasterDivQuestion(this,'QS"+cnt+"schoolId','divTxtShowData2');\"   style='width: 50%;'/>	");
							sb.append("<input type='hidden' id='schoolName' name='schoolName'/><input type='hidden' id='QS"+cnt+"schoolId' class='school"+cnt+"' name='schoolId' value='"+dbScId+"'/>");
							sb.append("<div id='divTxtShowData2'  onmouseover=\"mouseOverChk('divTxtShowData2','schoolName')\" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>");
							printDataPDF.appendTextIntoPdf("<tr><td><label>School Name</label><br>"+dbScName+"</td></tr>",false);
						}
					}
					if(shortName.equalsIgnoreCase("OSONP"))
					{
						String optionClass="OSONP"+cnt;
						sb.append("<table>");
						Integer p1=0;
						
						if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswersLop.getSelectedOptions()!=null){
							selectedOptions = districtSpecificPortfolioAnswersLop.getSelectedOptions()==null?"":districtSpecificPortfolioAnswersLop.getSelectedOptions();
						}
						System.out.println("selectedOptions::::>:"+selectedOptions);
						String[] multiCounts = selectedOptions.split("\\|");
						int multiCount=1;
						
						sb.append("<table>");
						Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
						for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
							optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
						}
						String ansId="";
						boolean openOther=false;
						for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
							
							try{
								ansId="";
								for(int i=0;i<multiCounts.length;i++){
									try{ansId =multiCounts[i];}catch(Exception e){}
									if(ansId!=null && !ansId.equals("") && questionOptions.getOptionId()==Integer.parseInt(ansId)){
										if(ansId!=null && !ansId.equals("")){
											ansId="checked";
										}
										break;
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
																		
									if(questionOptions.getOpenText() && ansId.equalsIgnoreCase("checked")){
										sb.append("<script>onSelectOpenOptions('show','"+cnt+"');</script>");
									}
									
									String checkBox="";
									
									if(questionOptions.getOpenText()){
										checkBox="<input type='radio' name='"+optionClass+"' class='"+optionClass+"' value='"+questionOptions.getOptionId()+"' "+ansId+" onclick=\"onSelectOpenOptions('show','"+cnt+"');\" />";	
									}else{
										checkBox="<input type='radio' name='"+optionClass+"' class='"+optionClass+"' value='"+questionOptions.getOptionId()+"' "+ansId+" onclick=\"onSelectOpenOptions('hide','"+cnt+"');\"  />";
									}
									
									sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'>"+checkBox+"</div>" +
											" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
											"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
									sb.append("</td></tr>");
									printDataPDF.appendTextIntoPdf("<tr><td>"+questionOptions.getQuestionOption()+"</td></tr>",false);
						}
						sb.append("</table>");
						String questionCustomInstructions="";
						try{
							if(districtSpecificPortfolioQuestions2.getQuestionInstructions()!=null){
								questionCustomInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions2.getQuestionInstructions();	
							}
						}catch(Exception e){
							e.printStackTrace();
						}
							sb.append("<span class='hide textareaOp"+cnt+"' style='margin-left: 10px;'>"+questionCustomInstructions+"<textarea  name='QS"+cnt+"OSONP' id='QS"+cnt+"OSONP' class='form-control "+cnt+"OSONPtext' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea></span>");
							printDataPDF.appendTextIntoPdf("<tr><td>"+questionCustomInstructions+"</td></tr>",false);
							printDataPDF.appendTextIntoPdf("<tr><td>"+insertedText+"</td></tr>",false);
					}
					
					if(shortName.equalsIgnoreCase("DD")){	
						if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswersLop.getSelectedOptions()!=null){
							selectedOptions = districtSpecificPortfolioAnswersLop.getSelectedOptions()==null?"":districtSpecificPortfolioAnswersLop.getSelectedOptions();
						}
						System.out.println("selectedOptions::::>:"+selectedOptions);
						String[] multiCounts = selectedOptions.split("\\|");
						int multiCount=1;
						
						String questionInstructions="&nbsp;";
						try{
							if(districtSpecificPortfolioQuestions2.getQuestionInstructions()!=null){
								questionInstructions=districtSpecificPortfolioQuestions2.getQuestionInstructions();	
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						sb.append(questionInstructions.trim());
						sb.append("<table>");
						printDataPDF.appendTextIntoPdf("<tr><td>"+questionInstructions+"</td></tr>",false);
						Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
						for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
							optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
						}
						//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
						String ansId="";
						String selectedAns="";
						
						for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
							try{
								ansId="";
								
								for(int i=0;i<multiCounts.length;i++){
									try{ansId =multiCounts[i];}catch(Exception e){}
									//System.out.println("ansId:::"+ansId);
									if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
										//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
										if(ansId!=null && !ansId.equals("")){
											selectedAns=teacherQuestionOption.getQuestionOption();
										}
										break;
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}						
							multiCount++;
							
						}
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'>"+selectedAns+"</td></tr>");
						sb.append("</table><br>");
						printDataPDF.appendTextIntoPdf("<tr><td>"+selectedAns+"</td></tr>",false);
					}
					if(shortName.equalsIgnoreCase("sswc"))
					{
						sb.append("<table style='margin-bottom:10px'>");
						for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
							if(optId.equals(questionOptions.getOptionId()))
								checked = "checked";
							else
								checked = "";
							if(checked.equalsIgnoreCase("checked")){
								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
										" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
								printDataPDF.appendTextIntoPdf("<tr><td>"+questionOptions.getQuestionOption()+"</td></tr>",false);
							}
						}
						sb.append("</table>");

						String questionInstructions="";
						try{
							if(districtSpecificPortfolioQuestions2.getQuestionInstructions()!=null){
								questionInstructions="&nbsp;&nbsp;"+districtSpecificPortfolioQuestions2.getQuestionInstructions();	
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						
						sb.append(questionInstructions+"<br>");
						printDataPDF.appendTextIntoPdf("<tr><td>"+questionInstructions+"</td></tr>",false);
						
						if(districtSpecificPortfolioQuestions2.getQuestion().equalsIgnoreCase("Have you previously worked for UCSN?")){
							
							System.out.println("insertedText.trim()   "+insertedText.trim());
							String[] arr = new String[2]; 
							if(insertedText.equalsIgnoreCase("##")){
								arr[0]="";
								arr[1]="";
							}else if(insertedText.contains("##")){
								arr = insertedText.split("##");
							}else{
								arr[0]=insertedText;
								arr[1]="";
							}
							
							sb.append("<div class='row'><div class='col-sm-3 col-md-3'><input type='text' name='QS"+cnt+"optet' id='QS"+cnt+"optet1' class='form-control' value='"+arr[0].trim()+"'></div>");
							sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet1', 'QS"+cnt+"optet1', '%m-%d-%Y');</script>");									
							sb.append("<div class='col-sm-3 col-md-3'><input type='text'  name='QS"+cnt+"optet' id='QS"+cnt+"optet2' class='form-control' value='"+arr[1].trim()+"'></div>");
							sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet2', 'QS"+cnt+"optet2', '%m-%d-%Y');</script>");
							printDataPDF.appendTextIntoPdf("<tr><td>"+arr[0].trim()+"</td></tr>",false);
							printDataPDF.appendTextIntoPdf("<tr><td>"+arr[1].trim()+"</td></tr>",false);
						}else{
							sb.append("<div class='row'><div class='col-sm-3 col-md-3'><input type='text' name='QS"+cnt+"optet' id='QS"+cnt+"optet' class='form-control' value='"+insertedText+"'></div>");
							sb.append("<script type='text/javascript'>cal.manageFields('QS"+cnt+"optet', 'QS"+cnt+"optet', '%m-%d-%Y');</script>");								
							printDataPDF.appendTextIntoPdf("<tr><td>"+insertedText+"</td></tr>",false);
							
						}
														
						sb.append("</div>");
						sb.append("</br>");
					}
					
					if(shortName.equalsIgnoreCase("sscb")){	
						if(districtSpecificPortfolioAnswersLop!=null && districtSpecificPortfolioAnswersLop.getSelectedOptions()!=null){
							selectedOptions = districtSpecificPortfolioAnswersLop.getSelectedOptions()==null?"":districtSpecificPortfolioAnswersLop.getSelectedOptions();
						}
						System.out.println("selectedOptions::::>:"+selectedOptions);
						String[] multiCounts = selectedOptions.split("\\|");
						int multiCount=1;
						
						sb.append("<table>");
						Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
						for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
							optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
						}
						String ansId="";
						int subsecCount=0; 
						
						String classSubSec="";
						for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
							try{
								ansId="";
								for(int i=0;i<multiCounts.length;i++){
									try{ansId =multiCounts[i];}catch(Exception e){}
									//System.out.println("ansId:::"+ansId);
									if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
										//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
										if(ansId!=null && !ansId.equals("")){
											ansId="checked";
										}
										break;
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						
							if(teacherQuestionOption.getSubSectionTitle()!=null && teacherQuestionOption.getSubSectionTitle().equalsIgnoreCase("Y")){
								subsecCount++;
								classSubSec="sectionCnt"+subsecCount+"";
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;' class='secHeading' id='subSection"+cnt+"'>"+teacherQuestionOption.getQuestionOption()+"");
								sb.append("</td></tr>");
								printDataPDF.appendTextIntoPdf("<tr><td>"+teacherQuestionOption.getQuestionOption()+"</td></tr>",false);
							}else{
								if(ansId.equalsIgnoreCase("checked")){
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='"+classSubSec+"' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'>&nbsp;&nbsp;&nbsp;"+teacherQuestionOption.getQuestionOption()+"<input type='hidden' id='QS"+teacherQuestionOption.getOptionId()+"validQuestion' value='"+teacherQuestionOption.getValidOption()+"'>");
								printDataPDF.appendTextIntoPdf("<tr><td>"+teacherQuestionOption.getQuestionOption()+"</td></tr>",false);
								if(districtSpecificPortfolioQuestions2.getDistrictMaster().getDistrictId().equals(7800040) && teacherQuestionOption.getQuestionOption().equalsIgnoreCase("College or University if selected, please choose the name of the school from the drop down"))
								{
									String dbUName= "";
									String dbUId="";
									try{
										if(districtSpecificPortfolioAnswersLop.getUniversityId()!=null && !districtSpecificPortfolioAnswersLop.getUniversityId().equals("")){
											dbUName=districtSpecificPortfolioAnswersLop.getUniversityId().getUniversityName();
											dbUId=districtSpecificPortfolioAnswersLop.getUniversityId().getUniversityId()+"";
										}
									}catch (Exception e) {
										// TODO: handle exception
									}											
									sb.append("<br><input type='text' value='"+dbUName+"' id='schoolName' maxlength='100' name='schoolName' class='form-control' placeholder='' onfocus=\"getUniversityAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onkeyup=\"getUniversityAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onblur=\"hideSchoolMasterDivQuestion(this,'QS"+cnt+"schoolId','divTxtShowData2');\" style='width:58%;display:inline;margin-left:5px;'/>");
									//sb.append("<label>School Name</label><br><input type='text' value='"+dbScName+"' id='schoolName' maxlength='100' name='schoolName' class='form-control' placeholder='' onfocus=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onkeyup=\"getSchoolMasterAutoCompQuestion(this, event, 'divTxtShowData2', 'schoolName','QS"+cnt+"schoolId','');\" onblur=\"hideSchoolMasterDivQuestion(this,'QS"+cnt+"schoolId','divTxtShowData2');\" style='width:58%;display:inline;'/>	");
									sb.append("<input type='hidden' id='schoolName' name='schoolName'/><input type='hidden' id='QS"+cnt+"schoolId' class='school"+cnt+"' name='schoolId' value='"+dbUId+"'/>");
									sb.append("<div id='divTxtShowData2'  onmouseover=\"mouseOverChk('divTxtShowData2','schoolName')\" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>");
									printDataPDF.appendTextIntoPdf("<tr><td>"+dbUName+"</td></tr>",false);
								}
								if(districtSpecificPortfolioQuestions2.getDistrictMaster().getDistrictId().equals(7800040) && teacherQuestionOption.getQuestionOption().contains("UNO Charter School Network Employee"))
								{
									sb.append("<input type='text'  name='QS"+cnt+"multiselectText' id='QS"+cnt+"multiselectText' class='form-control' style='width:35%;margin-left:5px;' value='"+insertedText+"'>");
									printDataPDF.appendTextIntoPdf("<tr><td>"+insertedText+"</td></tr>",false);
								}
								//UNO Charter School Network Employee 
								sb.append("</td></tr>");
								}
								multiCount++;										
							}		
							
							
						}
						sb.append("</table >");
						
					}
					if(shortName.equalsIgnoreCase("UAT"))
					{
						//sb.append("&nbsp;&nbsp;<input type='file' name='QS"+cnt+"opt' id='QS"+cnt+"opt' value='"+insertedText+"' maxlength='100' style='width:98%;margin:5px;'/><br/>");
						
						if(districtSpecificPortfolioAnswersLop!=null && districtSpecificPortfolioAnswersLop.getSelectedOptions()!=null){
							selectedOptions = districtSpecificPortfolioAnswersLop.getSelectedOptions()==null?"":districtSpecificPortfolioAnswersLop.getSelectedOptions();
						}
						System.out.println("selectedOptions::::>:"+selectedOptions);
						String[] multiCounts = selectedOptions.split("\\|");
						int multiCount=1;
						
						String questionInstructions="&nbsp;";
						try{
							if(districtSpecificPortfolioQuestions2.getQuestionInstructions()!=null){
								questionInstructions=districtSpecificPortfolioQuestions2.getQuestionInstructions();	
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						//sb.append(questionInstructions.trim());
						
						if(questionOptionsList!=null && questionOptionsList.size()>0){
								sb.append("<table>");
								Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
								}
								//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
								String ansId="";
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><select name='dropdown"+cnt+"' id='dropdown"+cnt+"' class='form-control'><option value=''>Select</option>");
								for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
									try{
										ansId="";
										for(int i=0;i<multiCounts.length;i++){
											try{ansId =multiCounts[i];}catch(Exception e){}
											//System.out.println("ansId:::"+ansId);
											if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
												//System.out.println(i+"count ansId::::::"+ansId +" multiCounts:::"+multiCounts.length);
												if(ansId!=null && !ansId.equals("")){
													ansId="selected";
												}
												break;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									sb.append("<option "+ansId+" value='"+teacherQuestionOption.getOptionId()+"'>"+teacherQuestionOption.getQuestionOption()+"</option>");
									printDataPDF.appendTextIntoPdf("<tr><td>"+teacherQuestionOption.getQuestionOption()+"</td></tr>", false);
									//sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' "+ansId+" name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
									
									multiCount++;
									
								}
								sb.append("</select></td></tr>");
								//sb.append("<tr><td>"+questionInstructions.trim()+"</td></tr>");
								sb.append("</table>");
						}
						sb.append("<div id='answerDiv'>");								
							sb.append("<iframe id='uploadFrameAnswerId' name='uploadFrameAnswer' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");
							sb.append("<form id='frmAnswerUpload' enctype='multipart/form-data' method='post' target='uploadFrameAnswer' action='answerUploadServlet.do' class='form-inline'>");
								sb.append("<div class='row'>");
									sb.append("<div class='col-sm-12 col-md-12'>");
										sb.append("<div class='divErrorMsg' id='errAnswer' style='display: block;'></div>");
									sb.append("</div>");
									
									sb.append("<div class='col-sm-12 col-md-12'>");
										if(questionInstructions!="" && !questionInstructions.equals("&nbsp;"))
											sb.append("<span>"+questionInstructions.trim()+"</span><br/>");
										sb.append("<div class='col-sm-6 col-md-6 top5'>");
											sb.append("<input name='QS"+cnt+"File' id='QS"+cnt+"File' type='file'/>");
										sb.append("</div>");
										sb.append("<div class='col-sm-6 col-md-6 top5' id='divAnswerTxt'>");
											String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
											String fileName="";
											if(districtSpecificPortfolioAnswersLop!=null && districtSpecificPortfolioAnswersLop.getFileName()!=null && !districtSpecificPortfolioAnswersLop.getFileName().equalsIgnoreCase(""))
											{
												fileName=districtSpecificPortfolioAnswersLop.getFileName();
												sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view Answer !' id='hrefAnswer' onclick=\"downloadAnswer(0,'"+districtSpecificPortfolioAnswersLop.getAnswerId()+"');"+windowFunc+"\">"+lblView+"</a>");
												sb.append("<script>$('#hrefAnswer').tooltip();</script>");
											}
										sb.append("</div>");

									sb.append("</div>");
									sb.append("<input type='hidden' name='answerFileName' id='answerFileName' value='"+fileName+"'>");
									sb.append("<input type='hidden' name='editCaseFile' id='editCaseFile' value='"+fileName+"'>");
									sb.append("<input type='hidden' name='answerFilePath' id='answerFilePath'>");
									sb.append("<input type='hidden' name='hdnQues' id='hdnQues' value='QS"+cnt+"File'>");										
								sb.append("</div>");
							sb.append("</form>");
						sb.append("</div>");
						if(districtSpecificPortfolioQuestions2.getDistrictMaster().getDistrictId()==1302010 && districtSpecificPortfolioQuestions2.getQuestion().equalsIgnoreCase("List sports clubs/activities you would be willing to coach/sponsor (attach coaching experience resume to profile)."))
						{
							sb.append("</br><span style=''><textarea  name='QS"+cnt+"Text' id='QS"+cnt+"Text' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
						}
					}

					sb.append("</td>");
					sb.append("</tr>");
					insertedText = "";
				}
			sb.append("</table>");
		} catch (Exception e) {
		}
		return sb.toString();
	}
	public boolean isSchoolWiseDeclined(JobOrder jobOrder){
		Boolean isSchoolWiseDeclined=false;
		try{
			if(jobOrder.getDistrictMaster()!=null){
				Boolean flag=CG_REPORT_SHOW_DECLINED.get(jobOrder.getDistrictMaster().getDistrictId().toString());
				if(flag!=null){
					isSchoolWiseDeclined= flag;
				}else{
					isSchoolWiseDeclined= false;
				}
			}
		}catch(Exception e){}
		return isSchoolWiseDeclined;
	}
	public Map<Integer, TeacherNormScore> normScorePart1(List<TeacherDetail> lstTeacherDetails)
	{
		List<TeacherNormScore> teacherNormScoreList= new ArrayList<TeacherNormScore>();
		try {
			teacherNormScoreList=teacherNormScoreDAO.findNormScoreByTeacherList(lstTeacherDetails);
		} catch (Exception e) {
		}

		Map<Integer, TeacherNormScore> mapTeacherNormScore=new HashMap<Integer, TeacherNormScore>(); 
		if(teacherNormScoreList!=null && teacherNormScoreList.size() >0)
		{
			for(TeacherNormScore teacherNormScore:teacherNormScoreList)
			{
				if(teacherNormScore!=null)
				{
					if(mapTeacherNormScore.get(teacherNormScore.getTeacherDetail().getTeacherId())==null)
					{
						mapTeacherNormScore.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
					}
				}
			}
		}
		
		return mapTeacherNormScore;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param teacher
	 * @Discription: It is used to check smart practices completed or not.
	 * @return {@link String}
	 */
	public String checkSpCompleted(Integer teacher)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		try{
			TeacherDetail teacherDetail = null;
			teacherDetail = teacherDetailDAO.findById(teacher, false, false);
			List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
			TeacherAssessmentStatus teacherAssessmentStatus = null;
			JobOrder jobOrder = new JobOrder();
			jobOrder.setJobId(-3);
			teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail, jobOrder, 3, null);

			if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0)
			{
				teacherAssessmentStatus = teacherAssessmentStatusList.get((teacherAssessmentStatusList.size()-1));
				if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
				{
					return "1";
				}else if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
				{
					return "3";
				}
			}

		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "2";
	}
	public String getHiddenCandidateReports(String jobId,String firstName,String lastName,String emailAddress,String certType,String regionId,String degreeId,String universityId,String normScoreSelectVal,String 
			normScoreVal,String CGPA,String CGPASelectVal,String contactedVal,String candidateStatus,String stateId,String orderColumn, String sortingOrder,int totalNoOfRecord,String tagsId,boolean callbreakup,int callNoofRecords,
			Integer[] certIds,String YOTESelectVal,String YOTE,String stateId2,String zipCode,String epiFromDate,String epiToDate)
	{

		
		System.out.println(" =============================== getHiddenCandidateReports =========================================");
		
		Integer districtIdForSpecific = 3702970;
		//Integer districtIdForSpecific = 100002;

		List<JobForTeacher> lstJobForTeacher = null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		TeacherAssessmentStatus assessmentStatus = null;
		StatusMaster status = null;

		StringBuffer tableHtml = new StringBuffer();
		List<String> withDrawLst=new ArrayList<String>();//add by Ram Nath for 10-10 record
		double[] meanArray=null;
		int noOfRecordCheck =totalNoOfRecord;
		DecimalFormat oneDForm = new DecimalFormat("###,###.0");
		boolean smartPractices=false,achievementScore=false,tFA=false,demoClass=false,displayPhoneInterview=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,CGPADis=false,fitFirstNoble=false,fitSecondNoble=false,fitMiami=false,jobAppliedDate=false,videoInterview=false,schoolSelection=false,PNQ=false,senNum=false;
		CandidateGridService cgService=new CandidateGridService();
		CGInviteInterviewAjax cgInviteInterviewAjax = new CGInviteInterviewAjax();
		boolean statusPrivilege=false;
		List<TeacherDetail> teacherList=new ArrayList<TeacherDetail>();
		Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
		Map<String,String> statusNameMap=new HashMap<String,String>();	
		List<DistrictAssessmentDetail> districtAssessmentDetailList=new ArrayList<DistrictAssessmentDetail>();
		List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=new ArrayList<OnlineActivityQuestionSet>(); 
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList=new ArrayList<TeacherDistrictAssessmentStatus>();
		List<TeacherDistrictAssessmentAnswerDetail> teacherAssessmentAnswerDetailsList = new ArrayList<TeacherDistrictAssessmentAnswerDetail>();
		try 
		{	
			String tFname = "",tLname = "",roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			try{
				districtAssessmentDetailList=districtAssessmentJobRelationDAO.findDistrictAssessmentDetailByJobOrder(jobOrder);
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990))
					onlineActivityQuestionSetList=onlineActivityQuestionSetDAO.getOnlineActivityQuestionSetList(jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster());
				if(userMaster.getEntityType()==3){
					SecondaryStatus secondaryStatus=secondaryStatusDAO.findSecondaryStatusObjByJobCategoryAndHBD(jobOrder);
					teacherList=teacherStatusHistoryForJobDAO.findTeacherListByStatusAndSecStatusHBD(jobOrder,secondaryStatus);
					if(teacherList!=null && userMaster.getEntityType()==3){
						statusPrivilege=true;
					}
				}
				
				if(jobOrder.getDistrictMaster()!=null){
					DistrictMaster districtMasterObj=jobOrder.getDistrictMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					if(districtMasterObj.getDistrictId()==7800038){
						PNQ=true;
						fitFirstNoble=true;
						fitSecondNoble=true;
					}
					if(districtMasterObj.getDistrictId()==1200390){
						try{
							if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Instructional")){
								fitMiami=true;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					if(districtMasterObj.getJobAppliedDate()!=null && districtMasterObj.getJobAppliedDate()){
						jobAppliedDate=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(districtMasterObj.getStatusMasterForCC()!=null && districtMasterObj.getSecondaryStatusForCC()==null){
							schoolSelection=true;
						}else if(districtMasterObj.getStatusMasterForCC()==null && districtMasterObj.getSecondaryStatusForCC()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(jobOrder.getBranchMaster()!=null){
					BranchMaster districtMasterObj=jobOrder.getBranchMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					/*try{
						if(districtMasterObj.getStatusMasterForCC()!=null && districtMasterObj.getSecondaryStatusForCC()==null){
							schoolSelection=true;
						}else if(districtMasterObj.getStatusMasterForCC()==null && districtMasterObj.getSecondaryStatusForCC()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}*/
				}else if(jobOrder.getHeadQuarterMaster()!=null){
					HeadQuarterMaster hqMasterobj=jobOrder.getHeadQuarterMaster();
					if(hqMasterobj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(hqMasterobj.getDisplayTFA()){
						tFA=true;
					}
					if(hqMasterobj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(hqMasterobj.getDisplayJSI()){
						JSI=true;
					}
					if(hqMasterobj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(hqMasterobj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(hqMasterobj.getDisplayFitScore()){
						fitScore=true;
					}
					if(hqMasterobj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(hqMasterobj.getDisplayPhoneInterview()!=null && hqMasterobj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(hqMasterobj.getCandidateConsiderationStatusId()!=null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()==null){
							schoolSelection=true;
						}else if(hqMasterobj.getCandidateConsiderationStatusId()==null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}catch(Exception e){ e.printStackTrace();}
			if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null){
				if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
					smartPractices=true;
				}
			}
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			String[] statuss = {"cghide"};
			try{
				lstStatusMasters = Utility.getStaticMasters(statuss);
			}catch (Exception e) {
				e.printStackTrace();
			}
			/* filter Add  */

			List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
			List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean certFlag=false;
			boolean teacherFlag=false;
			StateMaster stateMaster=null;
			List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
			if(certType!=null && !certType.equals("") && !certType.equals("0")||!stateId.equals("0")){
				if(!stateId.equals("0")){
					stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
				}
				certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
				if(certificateTypeMasterList.size()>0)
					certTeacherDetailList = teacherCertificateDAO.findCertificateByJobForTeacher(certificateTypeMasterList);
				certFlag=true;
			}
			if(certType.equals("")){
				certFlag=true;
			}
			if(certFlag && certTeacherDetailList.size()>0){
				filterTeacherList.addAll(certTeacherDetailList);
			}
			if(certFlag){
				teacherFlag=true;
			}
			
			//multiple certificate filter
			boolean mCertFlag=false;
			List<Integer> certTypeIdList = new ArrayList<Integer>();
			List<Integer> teacherIdList = new ArrayList<Integer>();
			List<TeacherDetail> mCertTeacherDetailList = new ArrayList<TeacherDetail>();
			if(certIds!=null && !certIds.equals("") && certIds.length>0){
				for(int i=0;i<certIds.length;i++)
				{
					certTypeIdList.add(certIds[i]);
				}
				teacherIdList = teacherCertificateDAO.findTeacherByCertificates(certTypeIdList);
				if(teacherIdList!=null && !teacherIdList.equals("") && teacherIdList.size()>0){
					for(int i=0;i<teacherIdList.size();i++)
					{
						TeacherDetail teacherDetail = new TeacherDetail();
						teacherDetail.setTeacherId(teacherIdList.get(i));
						mCertTeacherDetailList.add(teacherDetail);
					}
				}
				mCertFlag=true;
			}
			if(mCertFlag && mCertTeacherDetailList.size()>0){
				filterTeacherList.addAll(mCertTeacherDetailList);
			}
			if(mCertFlag){
				teacherFlag=true;
			}
			//yote filter
			List<TeacherDetail> expTeacherList = new ArrayList<TeacherDetail>();
			boolean expFlag=false;
			if(YOTE!=null && !YOTE.equals("")&& !YOTE.equals("0")){
				expFlag=true;
				expTeacherList=teacherExperienceDAO.findTeachersByExperience(YOTE,YOTESelectVal);
			}
			if(expFlag && expTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(expTeacherList);
				}else{
					filterTeacherList.addAll(expTeacherList);
				}
			}
			if(expFlag){
				teacherFlag=true;
			}
			//state filter
			List<TeacherDetail> stateTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean stateFlag=false;
			if(stateId2!=null && !stateId2.equals("") && !stateId2.equals("0"))
			{
				stateFlag=true;
				StateMaster stateMaster1 = stateMasterDAO.findById(Long.valueOf(stateId2), false, false);
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByState(stateMaster1);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					stateTeacherDetailList.add(teacherDetail);
				}
			}
			
			if(stateFlag && stateTeacherDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(stateTeacherDetailList);
				else
					filterTeacherList.addAll(stateTeacherDetailList);
			}
			if(stateFlag)
				teacherFlag=true;
			//zip code filter
			List<TeacherDetail> zipCodeTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			boolean zipCodeFlag=false;
			if(zipCode!=null && !zipCode.equals("") && !zipCode.equals("0"))
			{
				zipCodeFlag=true;
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByZipCode(zipCode);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					zipCodeTeacherdetaDetailList.add(teacherDetail);
				}
			}
			
			if(zipCodeFlag && zipCodeTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(zipCodeTeacherdetaDetailList);
				else
					filterTeacherList.addAll(zipCodeTeacherdetaDetailList);
			}
			if(zipCodeFlag)
				teacherFlag=true;
			
			//********************** EPI Date Filter By Ravindra ***************************
			Date epifDate=null;
			Date epitDate=null;
			boolean epiDateFlag=false;
			List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			try{
				if(!epiFromDate.equals("")){
					epifDate=Utility.getCurrentDateFormart(epiFromDate);
					epiDateFlag=true;
				}
				if(!epiToDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(epiToDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					epitDate=cal2.getTime();
					epiDateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(epiDateFlag)
			{
				StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.getAssessmentCompletedDateTimeTeacherList(1, statusMaster, epifDate, epitDate);
				for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherAssessmentStatus.getTeacherDetail().getTeacherId());
					epiDateTeacherdetaDetailList.add(teacherDetail);
				}
			}
			if(epiDateFlag && epiDateTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag){
					filterTeacherList.retainAll(epiDateTeacherdetaDetailList);
				}
				else{
					filterTeacherList.addAll(epiDateTeacherdetaDetailList);
				}
			}
			if(epiDateFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> degreeTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean degreeIdFlag=false;
			if(degreeId!=null && !degreeId.equals("") && !degreeId.equals("0")){
				degreeIdFlag=true;
				DegreeMaster degreeMaster=degreeMasterDAO.findById(Long.valueOf(degreeId), false,false);
				degreeTeacherDetailList=teacherAcademicsDAO.findTeacherListByDegree(degreeMaster);
			}
			if(degreeId.equals("")){
				degreeIdFlag=true;
			}
			if(degreeIdFlag && degreeTeacherDetailList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(degreeTeacherDetailList);
				}else{
					filterTeacherList.addAll(degreeTeacherDetailList);
				}
			}
			if(degreeIdFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> universityTeacherList = new ArrayList<TeacherDetail>();
			boolean universityFlag=false;
			if(universityId!=null && !universityId.equals("") && !universityId.equals("0")){
				universityFlag=true;
				UniversityMaster universityMaster=universityMasterDAO.findById(Integer.valueOf(universityId), false,false);
				universityTeacherList=teacherAcademicsDAO.findTeacherListByUniversity(universityMaster);
			}
			if(universityId.equals("")){
				universityFlag=true;
			}
			if(universityFlag && universityTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(universityTeacherList);
				}else{
					filterTeacherList.addAll(universityTeacherList);
				}
			}
			if(universityFlag){
				teacherFlag=true;
			}
			//****************************tags filter*************************************************
			List<TeacherDetail> tagsTeacherList = new ArrayList<TeacherDetail>();
			boolean tagsFlag=false;
			if(tagsId!=null && !tagsId.equals("") && !tagsId.equals("0")){
				tagsFlag=true;
				SecondaryStatusMaster secondaryStatusMaster= secondaryStatusMasterDAO.findById(Integer.valueOf(tagsId), false,false);
				//tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndJob(secondaryStatusMaster,jobOrder);
				tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndDistrict(secondaryStatusMaster,districtMaster);
			}
			
			if(tagsFlag && tagsTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(tagsTeacherList);
				}else{
					filterTeacherList.addAll(tagsTeacherList);
				}
			}
			if(tagsFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> regionTeacherList = new ArrayList<TeacherDetail>();
			boolean regionFlag=false;
			if(regionId!=null && !regionId.equals("") && !regionId.equals("0")){
				regionFlag=true;
				regionTeacherList=teacherPreferenceDAO.findRegionByJobForTeacher(regionId);
			}
			if(regionId.equals("")){
				regionFlag=true;
			}
			if(regionFlag && regionTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(regionTeacherList);
				}else{
					filterTeacherList.addAll(regionTeacherList);
				}
			}
			if(regionFlag){
				teacherFlag=true;
			}
			List<TeacherDetail> cgpaTeacherList = new ArrayList<TeacherDetail>();
			boolean cgpaFlag=false;
			if(CGPA!=null && !CGPA.equals("")&& !CGPA.equals("0")){

				cgpaFlag=true;
				cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA(CGPA,CGPASelectVal);
			}
			if(cgpaFlag && cgpaTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(cgpaTeacherList);
				}else{
					filterTeacherList.addAll(cgpaTeacherList);
				}
			}
			if(cgpaFlag){
				teacherFlag=true;
			}
			if(statusPrivilege){
				teacherFlag=true;
				filterTeacherList.addAll(teacherList);
			}
			if((teacherFlag && filterTeacherList.size()==0) || (!normScoreVal.equals("") && !normScoreVal.equals("0"))){
				lstJobForTeacher = new ArrayList<JobForTeacher>();
			}else{
				lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCG(null,jobOrder,lstStatusMasters,2,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,false,false,null);
			}
			System.out.println("Size ::>>"+lstJobForTeacher.size());
			List<JobForTeacher> lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacher);
			List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
			for(JobForTeacher jft:lstJFTremove){
				lstTeacherDetails.add(jft.getTeacherId());
				if(!candidateStatus.equals("0")){
					if(candidateStatus.equals("cghide")){
					}else{
						lstJobForTeacher.remove(jft);
					}
				}
				if(!contactedVal.equals("0")){
					if(contactedVal.equalsIgnoreCase("1")){
						if(jft.getLastActivity()==null || jft.getLastActivity().equals("")){
							lstJobForTeacher.remove(jft);
						}
					}else if(contactedVal.equalsIgnoreCase("2")){
						if(jft.getLastActivity()!=null && !jft.getLastActivity().equals("")){
							lstJobForTeacher.remove(jft);
						}
					}
				}
			}
			System.out.println("After Withdrew ::>>"+lstJobForTeacher.size());
			String[] statusShrotName ={"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw","cghide","ielig","iielig"};
			List<StatusMaster> statusMasters=Utility.getStaticMasters(statusShrotName);
			Map<String,StatusMaster> statusMap=new HashMap<String, StatusMaster>();
			for (StatusMaster statusMasterObj : statusMasters) {
				statusMap.put(statusMasterObj.getStatusShortName(),statusMasterObj);
			}
			Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
			List<TeacherStatusHistoryForJob> historyForJobFilterList = teacherStatusHistoryForJobDAO.findHireTeacherListByHBD(lstTeacherDetails,jobOrder,statusMap);
			List<TeacherStatusHistoryForJob> historyForJobList =new ArrayList<TeacherStatusHistoryForJob>();
			try{
				Map<String,String> historyMap=new HashMap<String, String>();
				Map<String,TeacherStatusHistoryForJob> historyObjMap=new HashMap<String,TeacherStatusHistoryForJob>();
				if(historyForJobFilterList.size()>0)
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobFilterList) {
					String id= teacherStatusHistoryForJob.getJobOrder().getJobId()+"#"+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId();
					String idValue=historyMap.get(id);
					if(idValue!=null){
						idValue+="#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#";
						historyMap.put(id,idValue);
					}else{
						historyMap.put(id,"#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#");
					}
					if(teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp") || teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("hird")){
						historyObjMap.put(id, teacherStatusHistoryForJob);
					}
				}
				if(historyObjMap.size()>0)
				for (Entry<String, TeacherStatusHistoryForJob> entry : historyObjMap.entrySet()){
					TeacherStatusHistoryForJob  historyObj=entry.getValue();
					String id= historyObj.getJobOrder().getJobId()+"#"+historyObj.getTeacherDetail().getTeacherId();
					String idValue=historyMap.get(id);
					if(idValue!=null && !(idValue.contains("rem")|| idValue.contains("dcln") || idValue.contains("widrw"))){
						historyForJobList.add(historyObj);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			List<JobForTeacher> jftOffers=jobForTeacherDAO.findJobByTeacherListForOffers(lstTeacherDetails,jobOrder.getDistrictMaster(),statusMap.get("hird"),statusMap.get("vcomp"));
			List<String> posList=new ArrayList<String>();
			Map<String,String> reqMap=new HashMap<String, String>();

			for (JobForTeacher jobForTeacher : jftOffers){
				try{
						if(jobForTeacher.getRequisitionNumber()!=null){
							posList.add(jobForTeacher.getRequisitionNumber());			
							reqMap.put(jobForTeacher.getJobId().getJobId()+"#"+jobForTeacher.getTeacherId().getTeacherId(),jobForTeacher.getRequisitionNumber());		
						}
				}catch(Exception e){

				e.printStackTrace();

				}

			}
			
			List<DistrictRequisitionNumbers> districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(jobOrder.getDistrictMaster(),posList);
			Map<String,DistrictRequisitionNumbers> dReqNoMap = new HashMap<String,DistrictRequisitionNumbers>();
			try{
				for (DistrictRequisitionNumbers districtRequisitionNumbers2 : districtRequisitionNumbers) {
					dReqNoMap.put(districtRequisitionNumbers2.getRequisitionNumber(),districtRequisitionNumbers2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(historyForJobList.size()>0){
				mapForHiredTeachers=cgService.getHiredList(reqMap,dReqNoMap,lstTeacherDetails,historyForJobList,jobOrder);
			}
			List<JobForTeacher> JFTList = new ArrayList<JobForTeacher>(lstJobForTeacher);
			System.out.println("lstJobForTeacher:::::::::::before:::::::::::::"+lstJobForTeacher.size());
		/*	for(JobForTeacher jft:JFTList)
			{	
				boolean isNoHired=true;
				try{
					if(mapForHiredTeachers.get(jft.getTeacherId().getTeacherId())!=null && mapForHiredTeachers.get(jft.getTeacherId().getTeacherId()).size()>0){
						isNoHired=false;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				if(!isNoHired){
					lstJobForTeacher.remove(jft);
				}
			}  */
			
			System.out.println("lstJobForTeacher:::::::::::::::::::::::::::After:::>>>>"+lstJobForTeacher.size());
			tableHtml.append(lstJobForTeacher.size());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tableHtml.toString();	
	}

	/**
	 * It is used to check assessment completed or timed out by <code>teacherId</code>,<code>assessmentType</code>,<code>teacherAssessmentStatusId</code>
	 * @author Amit Chaudhary
	 * @param teacher
	 * @param assessmentType
	 * @param teacherAssessmentStatusId
	 * @return It returns "1" if assessment is "complete" else returns "3" if assessment is "timed out" else returns "2" as {@link String}.
	 */
	public String checkAssessmentCompleted(Integer teacherId, Integer assessmentType, Integer teacherAssessmentStatusId)
	{
		System.out.println(":::::::::::::::::::::::::: CandidateGridAjax : checkAssessmentCompleted ::::::::::::::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		try{
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
			TeacherAssessmentStatus teacherAssessmentStatus = null;
			
			teacherAssessmentStatusList = teacherAssessmentStatusDAO.findTASByTeacherAndTAS(teacherDetail, teacherAssessmentStatusId);

			if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0){
				teacherAssessmentStatus = teacherAssessmentStatusList.get((teacherAssessmentStatusList.size()-1));
				if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
					return "1";
				}else if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
					return "3";
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return "2";
	}
	public String getOnBoardingCandidateReports(String jobId,String firstName,String lastName,String emailAddress,String certType,String regionId,String degreeId,String universityId,String normScoreSelectVal,String 
			normScoreVal,String CGPA,String CGPASelectVal,String contactedVal,String candidateStatus,String stateId,String orderColumn, String sortingOrder,int totalNoOfRecord,String tagsId,boolean callbreakup,int callNoofRecords,
			Integer[] certIds,String YOTESelectVal,String YOTE,String stateId2,String zipCode,String epiFromDate,String epiToDate)
	{	
		
		System.out.println("================================== getOnBoardingCandidateReports Kelly==========================");
		
		Integer districtIdForSpecific = 3702970;
		Map<String,RawDataForDomain> mapRawDomainScore = null;
		List<JobForTeacher> lstJobForTeacher = null;
		List<JobForTeacher> lstJobForTeacherVlt = null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		TeacherAssessmentStatus assessmentStatus = null;
		StatusMaster status = null;
		StringBuffer tableHtml = new StringBuffer();
		List<String> timeOutlst=new ArrayList<String>(); // add by ram nath for 10-10 record
		double[] meanArray=null;
		int noOfRecordCheck =totalNoOfRecord;
		lstJobForTeacherVlt = new ArrayList<JobForTeacher>();
		boolean achievementScore=false,smartPractices=false,tFA=false,demoClass=false,displayPhoneInterview=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,CGPADis=false,fitFirstNoble=false,fitSecondNoble=false,fitMiami=false,jobAppliedDate=false,videoInterview=false,schoolSelection=false,PNQ=false,senNum=false,jobCompletionDate=false,certified=false;
		CandidateGridService cgService=new CandidateGridService();
		CGInviteInterviewAjax cgInviteInterviewAjax = new CGInviteInterviewAjax();
		boolean statusPrivilege=false;
		List<TeacherDetail> teacherList=new ArrayList<TeacherDetail>();
		Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
		Map<String,String> statusNameMap=new HashMap<String,String>();
		List<DistrictAssessmentDetail> districtAssessmentDetailList=new ArrayList<DistrictAssessmentDetail>();
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList=new ArrayList<TeacherDistrictAssessmentStatus>();
		List<TeacherDistrictAssessmentAnswerDetail> teacherAssessmentAnswerDetailsList = new ArrayList<TeacherDistrictAssessmentAnswerDetail>();

		try 
		{	
			String tFname = "",tLname = "",roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			//List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			meanArray = new double[lstDomain.size()];

			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			BranchMaster branchMasterObj = null;
			DistrictMaster districtMasterObj = null;
			HeadQuarterMaster hqMasterobj = null;
			try{
				districtAssessmentDetailList=districtAssessmentJobRelationDAO.findDistrictAssessmentDetailByJobOrder(jobOrder);
				if(userMaster.getEntityType()==3){
					SecondaryStatus secondaryStatus=secondaryStatusDAO.findSecondaryStatusObjByJobCategoryAndHBD(jobOrder);
					teacherList=teacherStatusHistoryForJobDAO.findTeacherListByStatusAndSecStatusHBD(jobOrder,secondaryStatus);
					if(teacherList!=null && userMaster.getEntityType()==3){
						statusPrivilege=true;
					}
				}
				
				if(jobOrder.getBranchMaster()!=null && userMaster.getBranchMaster()!=null){
					 branchMasterObj=jobOrder.getBranchMaster();
					if(branchMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(branchMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(branchMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(branchMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(branchMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(branchMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(branchMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(branchMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(branchMasterObj.getDisplayPhoneInterview()!=null && branchMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				else if(jobOrder.getHeadQuarterMaster()!=null){
					hqMasterobj=jobOrder.getHeadQuarterMaster();
					if(hqMasterobj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(hqMasterobj.getDisplayTFA()){
						tFA=true;
					}
					if(hqMasterobj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(hqMasterobj.getDisplayJSI()){
						JSI=true;
					}
					if(hqMasterobj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(hqMasterobj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(hqMasterobj.getDisplayFitScore()){
						fitScore=true;
					}
					if(hqMasterobj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(hqMasterobj.getDisplayPhoneInterview()!=null && hqMasterobj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					//Added by Gaurav Kumar
					if(hqMasterobj.getJobCompletionNeeded()!=null && hqMasterobj.getJobCompletionNeeded()){
						jobCompletionDate=true;
					}//End
					
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(hqMasterobj.getCandidateConsiderationStatusId()!=null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()==null){
							schoolSelection=true;
						}else if(hqMasterobj.getCandidateConsiderationStatusId()==null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}catch(Exception e){ e.printStackTrace();}
			if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null){
				if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
					smartPractices=true;
				}
			}

			TeacherAssessmentStatus teAssesStatusTempBase = null;
			TeacherAssessmentStatus teAssesStatusTempJSI = null;

			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			//String[] statuss = {"comp","icomp","hird","scomp","ecomp","vcomp","dcln"};
			String[] statuss = {"comp","icomp","dcln","scomp","ecomp","vcomp","hird"};  
			try{
				lstStatusMasters = Utility.getStaticMasters(statuss);
			}catch (Exception e) {
				e.printStackTrace();
			}
			/* filter Add  */
			List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
			List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean certFlag=false;
			boolean teacherFlag=false;
			StateMaster stateMaster=null;
			List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
			if(certType!=null && !certType.equals("") && !certType.equals("0")||!stateId.equals("0")){
				if(!stateId.equals("0")){
					stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
				}
				certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
				if(certificateTypeMasterList.size()>0)
					certTeacherDetailList = teacherCertificateDAO.findCertificateByJobForTeacher(certificateTypeMasterList);
				certFlag=true;
			}
			if(certType.equals("")){
				certFlag=true;
			}
			if(certFlag && certTeacherDetailList.size()>0){
				filterTeacherList.addAll(certTeacherDetailList);
			}
			if(certFlag){
				teacherFlag=true;
			}
			
			//multiple certificate filter
			boolean mCertFlag=false;
			List<Integer> certTypeIdList = new ArrayList<Integer>();
			List<Integer> teacherIdList = new ArrayList<Integer>();
			List<TeacherDetail> mCertTeacherDetailList = new ArrayList<TeacherDetail>();
			if(certIds!=null && !certIds.equals("") && certIds.length>0){
				for(int i=0;i<certIds.length;i++)
				{
					certTypeIdList.add(certIds[i]);
				}
				teacherIdList = teacherCertificateDAO.findTeacherByCertificates(certTypeIdList);
				if(teacherIdList!=null && !teacherIdList.equals("") && teacherIdList.size()>0){
					for(int i=0;i<teacherIdList.size();i++)
					{
						TeacherDetail teacherDetail = new TeacherDetail();
						teacherDetail.setTeacherId(teacherIdList.get(i));
						mCertTeacherDetailList.add(teacherDetail);
					}
				}
				mCertFlag=true;
			}
			if(mCertFlag && mCertTeacherDetailList.size()>0){
				filterTeacherList.addAll(mCertTeacherDetailList);
			}
			if(mCertFlag){
				teacherFlag=true;
			}
			//yote filter
			List<TeacherDetail> expTeacherList = new ArrayList<TeacherDetail>();
			boolean expFlag=false;
			if(YOTE!=null && !YOTE.equals("")&& !YOTE.equals("0")){
				expFlag=true;
				expTeacherList=teacherExperienceDAO.findTeachersByExperience(YOTE,YOTESelectVal);
			}
			if(expFlag && expTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(expTeacherList);
				}else{
					filterTeacherList.addAll(expTeacherList);
				}
			}
			if(expFlag){
				teacherFlag=true;
			}
			//state filter
			List<TeacherDetail> stateTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean stateFlag=false;
			if(stateId2!=null && !stateId2.equals("") && !stateId2.equals("0"))
			{
				stateFlag=true;
				StateMaster stateMaster1 = stateMasterDAO.findById(Long.valueOf(stateId2), false, false);
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByState(stateMaster1);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					stateTeacherDetailList.add(teacherDetail);
				}
			}
			
			if(stateFlag && stateTeacherDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(stateTeacherDetailList);
				else
					filterTeacherList.addAll(stateTeacherDetailList);
			}
			if(stateFlag)
				teacherFlag=true;
			//zip code filter
			List<TeacherDetail> zipCodeTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			boolean zipCodeFlag=false;
			if(zipCode!=null && !zipCode.equals("") && !zipCode.equals("0"))
			{
				zipCodeFlag=true;
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByZipCode(zipCode);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					zipCodeTeacherdetaDetailList.add(teacherDetail);
				}
			}
			
			if(zipCodeFlag && zipCodeTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(zipCodeTeacherdetaDetailList);
				else
					filterTeacherList.addAll(zipCodeTeacherdetaDetailList);
			}
			if(zipCodeFlag)
				teacherFlag=true;
			
			//********************** EPI Date Filter By Ravindra ***************************
			Date epifDate=null;
			Date epitDate=null;
			boolean epiDateFlag=false;
			List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			try{
				if(!epiFromDate.equals("")){
					epifDate=Utility.getCurrentDateFormart(epiFromDate);
					epiDateFlag=true;
				}
				if(!epiToDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(epiToDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					epitDate=cal2.getTime();
					epiDateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(epiDateFlag)
			{
				StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.getAssessmentCompletedDateTimeTeacherList(1, statusMaster, epifDate, epitDate);
				for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherAssessmentStatus.getTeacherDetail().getTeacherId());
					epiDateTeacherdetaDetailList.add(teacherDetail);
				}
			}
			if(epiDateFlag && epiDateTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag){
					filterTeacherList.retainAll(epiDateTeacherdetaDetailList);
				}
				else{
					filterTeacherList.addAll(epiDateTeacherdetaDetailList);
				}
			}
			if(epiDateFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> degreeTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean degreeIdFlag=false;
			if(degreeId!=null && !degreeId.equals("") && !degreeId.equals("0")){
				degreeIdFlag=true;
				DegreeMaster degreeMaster=degreeMasterDAO.findById(Long.valueOf(degreeId), false,false);
				degreeTeacherDetailList=teacherAcademicsDAO.findTeacherListByDegree(degreeMaster);
			}
			if(degreeId.equals("")){
				degreeIdFlag=true;
			}
			if(degreeIdFlag && degreeTeacherDetailList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(degreeTeacherDetailList);
				}else{
					filterTeacherList.addAll(degreeTeacherDetailList);
				}
			}
			if(degreeIdFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> universityTeacherList = new ArrayList<TeacherDetail>();
			boolean universityFlag=false;
			if(universityId!=null && !universityId.equals("") && !universityId.equals("0")){
				universityFlag=true;
				UniversityMaster universityMaster=universityMasterDAO.findById(Integer.valueOf(universityId), false,false);
				universityTeacherList=teacherAcademicsDAO.findTeacherListByUniversity(universityMaster);
			}
			if(universityId.equals("")){
				universityFlag=true;
			}
			if(universityFlag && universityTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(universityTeacherList);
				}else{
					filterTeacherList.addAll(universityTeacherList);
				}
			}
			if(universityFlag){
				teacherFlag=true;
			}
			//****************************tags filter*************************************************
			List<TeacherDetail> tagsTeacherList = new ArrayList<TeacherDetail>();
			boolean tagsFlag=false;
			if(tagsId!=null && !tagsId.equals("") && !tagsId.equals("0")){
				tagsFlag=true;
				SecondaryStatusMaster secondaryStatusMaster= secondaryStatusMasterDAO.findById(Integer.valueOf(tagsId), false,false);
				//tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndJob(secondaryStatusMaster,jobOrder);
				tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndDistrict(secondaryStatusMaster,districtMaster);
			}
			
			if(tagsFlag && tagsTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(tagsTeacherList);
				}else{
					filterTeacherList.addAll(tagsTeacherList);
				}
			}
			if(tagsFlag){
				teacherFlag=true;
			}
			
			
			List<TeacherDetail> regionTeacherList = new ArrayList<TeacherDetail>();
			boolean regionFlag=false;
			if(regionId!=null && !regionId.equals("") && !regionId.equals("0")){
				regionFlag=true;
				regionTeacherList=teacherPreferenceDAO.findRegionByJobForTeacher(regionId);
			}
			if(regionId.equals("")){
				regionFlag=true;
			}
			if(regionFlag && regionTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(regionTeacherList);
				}else{
					filterTeacherList.addAll(regionTeacherList);
				}
			}
			if(regionFlag){
				teacherFlag=true;
			}
			List<TeacherDetail> cgpaTeacherList = new ArrayList<TeacherDetail>();
			boolean cgpaFlag=false;
			if(CGPA!=null && !CGPA.equals("")&& !CGPA.equals("0")){
				cgpaFlag=true;
				cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA(CGPA,CGPASelectVal);
			}
			if(cgpaFlag && cgpaTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(cgpaTeacherList);
				}else{
					filterTeacherList.addAll(cgpaTeacherList);
				}
			}
			if(cgpaFlag){
				teacherFlag=true;
			}
			if(statusPrivilege){
				teacherFlag=true;
				filterTeacherList.addAll(teacherList);
			}

			if((teacherFlag && filterTeacherList.size()==0)){
				lstJobForTeacher = new ArrayList<JobForTeacher>();
			}else{
				lstJobForTeacher = jobForTeacherDAO.findByJobOrderForKellyCGOpSql(null,jobOrder,lstStatusMasters,1,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,false,false,null,0,0,orderColumn,sortingOrder,2);   // orderColumn added by Anurag
								
			}
			List<JobForTeacher> lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacher);

			boolean baseStatusFlag=false;
			boolean epiFlag=false;
			if(jobOrder.getDistrictMaster()!=null){
				if(jobOrder.getDistrictMaster().getNoEPI()==null ||(jobOrder.getDistrictMaster().getNoEPI()!=null && jobOrder.getDistrictMaster().getNoEPI()==false)){
					epiFlag=true;
				}
			}else if(jobOrder.getBranchMaster()!=null){
				if(jobOrder.getBranchMaster().getNoEPI()==null ||(jobOrder.getBranchMaster().getNoEPI()!=null && jobOrder.getBranchMaster().getNoEPI()==false)){
					epiFlag=true;
				}
			}else if(jobOrder.getHeadQuarterMaster()!=null){
				if(jobOrder.getHeadQuarterMaster().getNoEPI()==null ||(jobOrder.getHeadQuarterMaster().getNoEPI()!=null && jobOrder.getHeadQuarterMaster().getNoEPI()==false)){
					epiFlag=true;
				}
			}
			if(epiFlag){
				//if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getStatus().equals("A") && jobOrder.getJobCategoryMaster().getBaseStatus()){
				if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getStatus().equals("A") && jobOrder.getJobCategoryMaster().getBaseStatus()){
					baseStatusFlag=true;
				}
			}
			//--------------------------------------------------set global map start----------------------------------			
			Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();		
			Map<Integer, Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
			List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
			List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
			for (JobForTeacher jbforteacher : lstJobForTeacher) 
				teacherDetails.add(jbforteacher.getTeacherId());
			if(teacherDetails!=null && teacherDetails.size()>0){
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachersCGOp(teacherDetails);
				for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
					baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
					if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
						isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
					}
				}
			}
			
			/*Added by Gaurav Kumar for Certified teacher*/
			List<TeacherDetail> lstcertificates = new ArrayList<TeacherDetail>();
			ArrayList<Integer> certifiedTeacherList = new ArrayList<Integer>();
			if(certified){
				lstcertificates = teacherCertificateDAO.getTeachersCertificate(teacherDetails);
				
				if(lstcertificates!=null && lstcertificates.size()>0){
					for (TeacherDetail tcObj:lstcertificates)
					   {
						   if(tcObj!=null)
						   certifiedTeacherList.add(tcObj.getTeacherId());
					   }
				}
			}
			//End 
			/* Smart Practices Start*/
			Map<Integer,Integer> spStatusMap=new HashMap<Integer, Integer>();
			Map<Integer,Integer> lessonNoMap=new HashMap<Integer, Integer>();
			if(smartPractices){
				List<SpInboundAPICallRecord> spInboundAPICallRecordList= spInboundAPICallRecordDAO.getDetailsByTeacherList(teacherDetails);
				for (SpInboundAPICallRecord spInboundAPICallRecord : spInboundAPICallRecordList) {
					if(spInboundAPICallRecord.getCurrentLessonNo()!=null){
						lessonNoMap.put(spInboundAPICallRecord.getTeacherDetail().getTeacherId(),spInboundAPICallRecord.getCurrentLessonNo());
					}
				}
				List<TeacherAssessmentStatus>  teacherAssessmentStatusList=new ArrayList<TeacherAssessmentStatus>();
				if(teacherDetails.size()>0){
					teacherAssessmentStatusList=teacherAssessmentStatusDAO.findSmartPracticesTakenByTeachers(teacherDetails);
					if(teacherAssessmentStatusList.size()>0)
					for (TeacherAssessmentStatus teacherAssessmentStatusObj : teacherAssessmentStatusList) {
						int keyAss=teacherAssessmentStatusObj.getTeacherDetail().getTeacherId();
						if(spStatusMap.get(keyAss)==null){
							if(teacherAssessmentStatusObj.getPass()!=null){
								if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("P")){
									spStatusMap.put(keyAss,1);
								}else if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("F")){
									spStatusMap.put(keyAss,0);
								}
							}else if(teacherAssessmentStatusObj.getStatusMaster()!=null && teacherAssessmentStatusObj.getStatusMaster().getStatusShortName().equals("vlt")){
								spStatusMap.put(keyAss,5);
							}else{
								spStatusMap.put(keyAss,2);
							}
						}else{
							int prevValue=spStatusMap.get(keyAss);
							if(teacherAssessmentStatusObj.getPass()!=null){
								if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("P")){
									if(prevValue==1){
										spStatusMap.put(keyAss,3);
									}else{
										spStatusMap.put(keyAss,1);
									}
								}else if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("F")){
									if(prevValue==0){
										spStatusMap.put(keyAss,4);
									}else{
										spStatusMap.put(keyAss,0);
									}
								}
							}else if(teacherAssessmentStatusObj.getStatusMaster()!=null && teacherAssessmentStatusObj.getStatusMaster().getStatusShortName().equals("vlt")){
								spStatusMap.put(keyAss,5);
							}else{
								spStatusMap.put(keyAss,2);
							}
						}
					}
				}
			}
			/* Smart Practices End*/
			/*Map<String, PercentileCalculation> mapNationalPercentile = new HashMap<String, PercentileCalculation>();
			Map<String, PercentileScoreByJob> mapJobPercentile = new HashMap<String, PercentileScoreByJob>();*/

			
			/*List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = teacherSecondaryStatusDAO.findByJobAndSchool(jobOrder, userMaster.getSchoolId());
			Map<Integer,TeacherSecondaryStatus> mapTSecStatus = new HashMap<Integer, TeacherSecondaryStatus>();
			for(TeacherSecondaryStatus tSecondaryStatus: lstTeacherSecondaryStatus ){
				mapTSecStatus.put(tSecondaryStatus.getTeacherDetail().getTeacherId(), tSecondaryStatus);
			}*/	

			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI = new ArrayList<TeacherAssessmentStatus>(); //teacherAssessmentStatusDAO.findAssessmentTaken(null, jobOrder);
			List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrderNoDateOP(jobOrder);
			if(assessmentJobRelations1!=null && assessmentJobRelations1.size()>0 && teacherDetails.size()>0){
				AssessmentJobRelation assessmentJobRelation1 = assessmentJobRelations1.get(0);
				lstJSI = teacherAssessmentStatusDAO.findAssessmentTakenByTeacherList(teacherDetails,assessmentJobRelation1.getAssessmentId());
			}
			for(TeacherAssessmentStatus ts : lstJSI){
				mapJSI.put(ts.getTeacherDetail().getTeacherId(), ts);
			}
			//--------------------------------------------------set global map end----------------------------------
			boolean isMiami=false;
			boolean isPhiladelphia=false;
			try{
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1200390))
					isMiami=true;
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(0000000))
					isPhiladelphia=true;
			}catch(Exception e){};
			Map<Integer,StatusMaster> swcsMap= new HashMap<Integer,StatusMaster>();
			List<String> lstTeacherEmails = new ArrayList<String>();
			for(JobForTeacher jft : lstJFTremove){
				if(jft.getTeacherId().getKSNID()!=null){	
					lstJobForTeacherVlt.add(jft);
				}
			}
			Map<String,String> videoMap = new HashMap<String, String>();
			boolean isNoble = false;
			if(entityID==2 && districtMaster!=null && districtMaster.getDistrictId().equals(7800038))
			{
				List<DistrictSpecificVideoInterviewDetails> districtSpecificVideoInterviewDetailList = districtSpecificVideoInterviewDetailsDAO.checkCandidateDataSubmittedByEmails(lstTeacherEmails, districtMaster);
				for (DistrictSpecificVideoInterviewDetails districtSpecificVideoInterviewDetails : districtSpecificVideoInterviewDetailList) {
					videoMap.put(districtSpecificVideoInterviewDetails.getEmailAddress(), districtSpecificVideoInterviewDetails.getVideoURL());
				}
				isNoble = true;
			}
			//------------Check user can hire, unhire or remove start
			boolean outerJob=true;
			SchoolInJobOrder schoolInJobOrder = null;
			if(userMaster.getEntityType()==3){
				schoolMaster = userMaster.getSchoolId();				
				schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMaster);
				if(schoolInJobOrder!=null){
					outerJob=false;
				}
			}else{
				outerJob=false;
			}
			boolean doActivity =cgService.isVisible(jobOrder,userMaster,schoolInJobOrder);
			///////////////////////////////////School Status///////////////////////////
			Map<String,String> mapHistrory = new HashMap<String,String>();
			List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobList=new ArrayList<TeacherStatusHistoryForJob>();
			Map<Integer,List<String>> statusHistoryMap=new HashMap<Integer, List<String>>();
			if(userMaster.getEntityType()==3){
				List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategoryAndHeadOp(jobOrder.getHeadQuarterMaster(), jobOrder.getBranchMaster(), jobOrder.getDistrictMaster(), jobOrder.getJobCategoryMaster());
				try{
					int counter=0; 
					for(SecondaryStatus tree: lstTreeStructure)
					{
						if(tree.getSecondaryStatus()==null)
						{
							if(tree.getChildren().size()>0){
								counter=cgService.getAllStatusCG(tree.getChildren(),statusHistMap,statusNameMap,counter);
							}
						}
					}
				}catch(Exception e){}
				try{		
					teacherStatusHistoryForJobList=teacherStatusHistoryForJobDAO.findTeacherStatusByJobOrderAndUser(userMaster, jobOrder);
					//System.out.println("teacherStatusHistoryForJobList::::::::::"+teacherStatusHistoryForJobList.size());
					
					List<String> statusList=new ArrayList<String>();
					for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : teacherStatusHistoryForJobList) {
						if(statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null){
							statusList=new ArrayList<String>();
							if(teacherStatusHistoryForJob.getStatusMaster()!=null){
								statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
							}else{
								statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
							}
							statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
						}else{
							statusList=statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
							if(teacherStatusHistoryForJob.getStatusMaster()!=null){
								statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
							}else{
								statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
							}
							statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
						}
					}
				}catch(Exception e){}
			}
			
			Map<Integer, Double> listCGPAByTeacherMap = new HashMap<Integer, Double>();
			Map<Integer, String> listCGPAByTeacherToolTipMap = new HashMap<Integer, String>();			
			//teacherDetails CGPA Records
			if(teacherDetails!=null && teacherDetails.size()>0 && CGPADis){
				List<TeacherAcademics> listCGPAByTeacher = teacherAcademicsDAO.findCGPAByTeacher(teacherDetails);
				
				try {
					for (TeacherAcademics teacherAcademicsVal : listCGPAByTeacher) {
						if(!listCGPAByTeacherMap.containsKey(teacherAcademicsVal.getTeacherId().getTeacherId())){
							listCGPAByTeacherMap.put(teacherAcademicsVal.getTeacherId().getTeacherId(), teacherAcademicsVal.getGpaCumulative());
							listCGPAByTeacherToolTipMap.put(teacherAcademicsVal.getTeacherId().getTeacherId(), teacherAcademicsVal.getDegreeId().getDegreeName());
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}				
			}
			
			Map<Integer, Double> teacherExperienceMap = new HashMap<Integer, Double>();			
			//teacherDetails yote Records
			List<TeacherExperience> teacherExperienceList = new ArrayList<TeacherExperience>();
			if(teacherDetails!=null && teacherDetails.size()>0 && teachingOfYear){
				teacherExperienceList = teacherExperienceDAO.findExperienceForTeachers(teacherDetails);
				try {
					if(teacherExperienceList.size()>0)
					{
						for (TeacherExperience teacherExperience : teacherExperienceList) {
							if(!teacherExperienceMap.containsKey(teacherExperience.getTeacherId().getTeacherId())){
								teacherExperienceMap.put(teacherExperience.getTeacherId().getTeacherId(), teacherExperience.getExpCertTeacherTraining());
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}				
			}
			String[] statusShrotName = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw","ielig","iielig"};
			List<StatusMaster> statusMasters=Utility.getStaticMasters(statusShrotName);;
			Map<String,StatusMaster> statusMap=new HashMap<String, StatusMaster>();
			for (StatusMaster statusMasterObj : statusMasters) {
				statusMap.put(statusMasterObj.getStatusShortName(),statusMasterObj);
			}
			Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
			List<TeacherStatusHistoryForJob> historyForJobFilterList = teacherStatusHistoryForJobDAO.findHireTeacherListByHBD(teacherDetails,jobOrder,statusMap);
			List<TeacherStatusHistoryForJob> historyForJobList =new ArrayList<TeacherStatusHistoryForJob>();
			List<TeacherStatusHistoryForJob> historyForJobs = teacherStatusHistoryForJobDAO.findByTeacherListAndJob(teacherDetails,jobOrder);
			if(historyForJobs.size()>0){
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobs) {
					if(teacherStatusHistoryForJob.getStatus()!=null && teacherStatusHistoryForJob.getStatus().equalsIgnoreCase("W")){
						String sStatus=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##"+teacherStatusHistoryForJob.getStatus();
						mapHistrory.put(sStatus, teacherStatusHistoryForJob.getStatus());
					}
				}
			}
			try{
				Map<String,String> historyMap=new HashMap<String, String>();
				Map<String,TeacherStatusHistoryForJob> historyObjMap=new HashMap<String,TeacherStatusHistoryForJob>();
				if(historyForJobFilterList.size()>0)
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobFilterList) {
					String id= teacherStatusHistoryForJob.getJobOrder().getJobId()+"#"+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId();
					String idValue=historyMap.get(id);
					if(idValue!=null){
						idValue+="#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#";
						historyMap.put(id,idValue);
					}else{
						historyMap.put(id,"#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#");
					}
					if(teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp") || teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("hird")){
						historyObjMap.put(id, teacherStatusHistoryForJob);
					}
				}
				if(historyObjMap.size()>0)
				for (Entry<String, TeacherStatusHistoryForJob> entry : historyObjMap.entrySet()){
					TeacherStatusHistoryForJob  historyObj=entry.getValue();
					String id= historyObj.getJobOrder().getJobId()+"#"+historyObj.getTeacherDetail().getTeacherId();
					String idValue=historyMap.get(id);
					if(idValue!=null && !(idValue.contains("rem")|| idValue.contains("dcln") || idValue.contains("widrw"))){
						historyForJobList.add(historyObj);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			List<JobForTeacher> jftOffers=jobForTeacherDAO.findJobByTeacherListForOffers(teacherDetails,jobOrder.getDistrictMaster(),statusMap.get("hird"),statusMap.get("vcomp"));
			List<String> posList=new ArrayList<String>();
			Map<String,String> reqMap=new HashMap<String, String>();
			for (JobForTeacher jobForTeacher : jftOffers){
				try{
					if(jobForTeacher.getRequisitionNumber()!=null){
						posList.add(jobForTeacher.getRequisitionNumber());
						reqMap.put(jobForTeacher.getJobId().getJobId()+"#"+jobForTeacher.getTeacherId().getTeacherId(),jobForTeacher.getRequisitionNumber());
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			List<DistrictRequisitionNumbers> districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(jobOrder.getDistrictMaster(),posList);
			Map<String,DistrictRequisitionNumbers> dReqNoMap = new HashMap<String,DistrictRequisitionNumbers>();
			try{
				for (DistrictRequisitionNumbers districtRequisitionNumbers2 : districtRequisitionNumbers) {
					dReqNoMap.put(districtRequisitionNumbers2.getRequisitionNumber(),districtRequisitionNumbers2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(historyForJobList.size()>0){
				mapForHiredTeachers=cgService.getHiredList(reqMap,dReqNoMap,teacherDetails,historyForJobList,jobOrder);
			}
			///////////////////////
			List<JobForTeacher> JFTList = new ArrayList<JobForTeacher>(lstJobForTeacherVlt);
			for(JobForTeacher jft:JFTList){	
				System.out.println("jft::::::::::::::::;"+jft.getTeacherId().getTeacherId());
				boolean isNoHired=true;
				try{
					if(mapForHiredTeachers.get(jft.getTeacherId().getTeacherId())!=null && mapForHiredTeachers.get(jft.getTeacherId().getTeacherId()).size()>0){
						isNoHired=false;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				boolean talentType=true;
				if(smartPractices){
					if(spStatusMap.get(jft.getTeacherId().getTeacherId())!=null){
						int spValue=spStatusMap.get(jft.getTeacherId().getTeacherId());
						if(spValue==0 || spValue==2 ||spValue==4|| spValue==5){
							talentType=false;
						}
					}else{
						if(lessonNoMap.get(jft.getTeacherId().getTeacherId())!=null){
							int lessonNo=lessonNoMap.get(jft.getTeacherId().getTeacherId());
							if(lessonNo>0){
								talentType=false;
							}
						}else{
							talentType=false;
						}
					}
				}
				String sStatus=jft.getTeacherId().getTeacherId()+"##"+jft.getJobId().getJobId()+"##W";
				if(mapHistrory.get(sStatus)!=null){
					talentType=true;
				}
				if(!isNoHired && talentType){
					lstJobForTeacherVlt.remove(jft);
				}
			}
			
			////////////////////////////////////////////////////////////////
			lstStatusMasters=null;
			lstStatusMasters = new ArrayList<StatusMaster>();			
			String[] statussvlt = {"vlt"};
			try{
				lstStatusMasters = Utility.getStaticMasters(statussvlt);
			}catch (Exception e) {
				e.printStackTrace();
			}
			if((teacherFlag && filterTeacherList.size()==0) || (!normScoreVal.equals("") && !normScoreVal.equals("0"))){
				lstJobForTeacher = new ArrayList<JobForTeacher>();
			}else{
				lstJobForTeacher = new ArrayList<JobForTeacher>();
				lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCG(null,jobOrder,lstStatusMasters,7,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,false,false,null);
			}
			lstJobForTeacherVlt.addAll(lstJobForTeacher);
			
			System.out.println("lstJobForTeacherVlt::::::::::::::::::::"+lstJobForTeacherVlt.size());
			
			tableHtml.append(lstJobForTeacherVlt.size());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tableHtml.toString();	
	}

	@Transactional(readOnly=true)
	public String getInactiveCandidateReports(String jobId,String firstName,String lastName,String emailAddress,String certType,String regionId,String degreeId,String universityId,String normScoreSelectVal,String 
			normScoreVal,String CGPA,String CGPASelectVal,String contactedVal,String candidateStatus,String stateId,String orderColumn, String sortingOrder,int totalNoOfRecord,String tagsId,boolean callbreakup,int callNoofRecords,
			Integer[] certIds,String YOTESelectVal,String YOTE,String stateId2,String zipCode,String epiFromDate,String epiToDate)
	{
		
		System.out.println("===================== getInactiveCandidateReports Kelly =================");
		
		
		Integer districtIdForSpecific = 3702970;
		//Integer districtIdForSpecific = 100002;

		List<JobForTeacher> lstJobForTeacher = null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		TeacherAssessmentStatus assessmentStatus = null;
		StatusMaster status = null;

		StringBuffer tableHtml = new StringBuffer(); //change name sb to tableHtml for fetch 10-10 record by Ram nath
		List<String> rejectedlst=new ArrayList<String>(); //add by Ram Nath for 10-10 record
		double[] meanArray=null;
		int noOfRecordCheck =totalNoOfRecord;
		boolean smartPractices=false,achievementScore=false,tFA=false,demoClass=false,displayPhoneInterview=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,CGPADis=false,fitFirstNoble=false,fitSecondNoble=false,fitMiami=false,jobAppliedDate=false,videoInterview=false,schoolSelection=false,PNQ=false,senNum=false,jobCompletionDate=false,certified=false;
		CandidateGridService cgService=new CandidateGridService();
		CGInviteInterviewAjax cgInviteInterviewAjax = new CGInviteInterviewAjax();
		boolean statusPrivilege=false;
		List<TeacherDetail> teacherList=new ArrayList<TeacherDetail>();
		Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
		Map<String,String> statusNameMap=new HashMap<String,String>();
		List<DistrictAssessmentDetail> districtAssessmentDetailList=new ArrayList<DistrictAssessmentDetail>();
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList=new ArrayList<TeacherDistrictAssessmentStatus>();
		List<TeacherDistrictAssessmentAnswerDetail> teacherAssessmentAnswerDetailsList = new ArrayList<TeacherDistrictAssessmentAnswerDetail>();
		
		
		try 
		{	
			String tFname = "",tLname = "",roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			//List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			meanArray = new double[lstDomain.size()];

			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			BranchMaster branchMasterObj = null;
			DistrictMaster districtMasterObj = null;
			HeadQuarterMaster hqMasterobj = null;
			try{
				districtAssessmentDetailList=districtAssessmentJobRelationDAO.findDistrictAssessmentDetailByJobOrder(jobOrder);
				
				if(userMaster.getEntityType()==3){
					SecondaryStatus secondaryStatus=secondaryStatusDAO.findSecondaryStatusObjByJobCategoryAndHBD(jobOrder);
					teacherList=teacherStatusHistoryForJobDAO.findTeacherListByStatusAndSecStatusHBD(jobOrder,secondaryStatus);
					if(teacherList!=null && userMaster.getEntityType()==3){
						statusPrivilege=true;
					}
				}
			if(jobOrder.getBranchMaster()!=null && userMaster.getBranchMaster()!=null){
				branchMasterObj=jobOrder.getBranchMaster();
				if(branchMasterObj.getDisplayAchievementScore()){
					achievementScore=true;
				}
				if(branchMasterObj.getDisplayTFA()){
					tFA=true;
				}
				if(branchMasterObj.getDisplayDemoClass()){
					demoClass=true;
				}
				if(branchMasterObj.getDisplayJSI()){
					JSI=true;
				}
				if(branchMasterObj.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}
				if(branchMasterObj.getDisplayExpectedSalary()){
					expectedSalary=true;
				}
				if(branchMasterObj.getDisplayFitScore()){
					fitScore=true;
				}
				if(branchMasterObj.getDisplayCGPA()){
					CGPADis=true;
				}

				if(branchMasterObj.getDisplayPhoneInterview()!=null && branchMasterObj.getDisplayPhoneInterview()){
					displayPhoneInterview=true;
				}
				try{
					if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
						videoInterview=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			else if(jobOrder.getHeadQuarterMaster()!=null){
				hqMasterobj=jobOrder.getHeadQuarterMaster();
				if(hqMasterobj.getDisplayAchievementScore()){
					achievementScore=true;
				}
				if(hqMasterobj.getDisplayTFA()){
					tFA=true;
				}
				if(hqMasterobj.getDisplayDemoClass()){
					demoClass=true;
				}
				if(hqMasterobj.getDisplayJSI()){
					JSI=true;
				}
				if(hqMasterobj.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}
				if(hqMasterobj.getDisplayExpectedSalary()){
					expectedSalary=true;
				}
				if(hqMasterobj.getDisplayFitScore()){
					fitScore=true;
				}
				if(hqMasterobj.getDisplayCGPA()){
					CGPADis=true;
				}
				
				if(hqMasterobj.getDisplayPhoneInterview()!=null && hqMasterobj.getDisplayPhoneInterview()){
					displayPhoneInterview=true;
				}
				//Added by Gaurav Kumar
				if(hqMasterobj.getJobCompletionNeeded()!=null && hqMasterobj.getJobCompletionNeeded()){
					jobCompletionDate=true;
				}	
				//End
				try{
					if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
						videoInterview=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				} 
				try{
					if(hqMasterobj.getCandidateConsiderationStatusId()!=null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()==null){
						schoolSelection=true;
					}else if(hqMasterobj.getCandidateConsiderationStatusId()==null && hqMasterobj.getCandidateConsiderationSecondaryStatusId()!=null){
						schoolSelection=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				}
			}catch(Exception e){ e.printStackTrace();}
			if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null){
				if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
					smartPractices=true;
				}
			}

			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			String[] statuss = {"ielig","iielig"};
			try{
				lstStatusMasters = Utility.getStaticMasters(statuss);
			}catch (Exception e) {
				e.printStackTrace();
			}
			/* filter Add  */
			List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
			List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean certFlag=false;
			boolean teacherFlag=false;
			StateMaster stateMaster=null;
			List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
			if(certType!=null && !certType.equals("") && !certType.equals("0")||!stateId.equals("0")){
				if(!stateId.equals("0")){
					stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
				}
				certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
				if(certificateTypeMasterList.size()>0)
					certTeacherDetailList = teacherCertificateDAO.findCertificateByJobForTeacher(certificateTypeMasterList);
				certFlag=true;
			}
			if(certType.equals("")){
				certFlag=true;
			}
			if(certFlag && certTeacherDetailList.size()>0){
				filterTeacherList.addAll(certTeacherDetailList);
			}
			if(certFlag){
				teacherFlag=true;
			}
			
			//multiple certificate filter
			boolean mCertFlag=false;
			List<Integer> certTypeIdList = new ArrayList<Integer>();
			List<Integer> teacherIdList = new ArrayList<Integer>();
			List<TeacherDetail> mCertTeacherDetailList = new ArrayList<TeacherDetail>();
			if(certIds!=null && !certIds.equals("") && certIds.length>0){
				for(int i=0;i<certIds.length;i++)
				{
					certTypeIdList.add(certIds[i]);
				}
				teacherIdList = teacherCertificateDAO.findTeacherByCertificates(certTypeIdList);
				if(teacherIdList!=null && !teacherIdList.equals("") && teacherIdList.size()>0){
					for(int i=0;i<teacherIdList.size();i++)
					{
						TeacherDetail teacherDetail = new TeacherDetail();
						teacherDetail.setTeacherId(teacherIdList.get(i));
						mCertTeacherDetailList.add(teacherDetail);
					}
				}
				mCertFlag=true;
			}
			if(mCertFlag && mCertTeacherDetailList.size()>0){
				filterTeacherList.addAll(mCertTeacherDetailList);
			}
			if(mCertFlag){
				teacherFlag=true;
			}
			//yote filter
			List<TeacherDetail> expTeacherList = new ArrayList<TeacherDetail>();
			boolean expFlag=false;
			if(YOTE!=null && !YOTE.equals("")&& !YOTE.equals("0")){
				expFlag=true;
				expTeacherList=teacherExperienceDAO.findTeachersByExperience(YOTE,YOTESelectVal);
			}
			if(expFlag && expTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(expTeacherList);
				}else{
					filterTeacherList.addAll(expTeacherList);
				}
			}
			if(expFlag){
				teacherFlag=true;
			}
			//state filter
			List<TeacherDetail> stateTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean stateFlag=false;
			if(stateId2!=null && !stateId2.equals("") && !stateId2.equals("0"))
			{
				stateFlag=true;
				StateMaster stateMaster1 = stateMasterDAO.findById(Long.valueOf(stateId2), false, false);
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByState(stateMaster1);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					stateTeacherDetailList.add(teacherDetail);
				}
			}
			
			if(stateFlag && stateTeacherDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(stateTeacherDetailList);
				else
					filterTeacherList.addAll(stateTeacherDetailList);
			}
			if(stateFlag)
				teacherFlag=true;
			//zip code filter
			List<TeacherDetail> zipCodeTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			boolean zipCodeFlag=false;
			if(zipCode!=null && !zipCode.equals("") && !zipCode.equals("0"))
			{
				zipCodeFlag=true;
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByZipCode(zipCode);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					zipCodeTeacherdetaDetailList.add(teacherDetail);
				}
			}
			
			if(zipCodeFlag && zipCodeTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(zipCodeTeacherdetaDetailList);
				else
					filterTeacherList.addAll(zipCodeTeacherdetaDetailList);
			}
			if(zipCodeFlag)
				teacherFlag=true;
			//********************** EPI Date Filter By Ravindra ***************************
			Date epifDate=null;
			Date epitDate=null;
			boolean epiDateFlag=false;
			List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			try{
				if(!epiFromDate.equals("")){
					epifDate=Utility.getCurrentDateFormart(epiFromDate);
					epiDateFlag=true;
				}
				if(!epiToDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(epiToDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					epitDate=cal2.getTime();
					epiDateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(epiDateFlag)
			{
				StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.getAssessmentCompletedDateTimeTeacherList(1, statusMaster, epifDate, epitDate);
				for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherAssessmentStatus.getTeacherDetail().getTeacherId());
					epiDateTeacherdetaDetailList.add(teacherDetail);
				}
			}
			if(epiDateFlag && epiDateTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag){
					filterTeacherList.retainAll(epiDateTeacherdetaDetailList);
				}
				else{
					filterTeacherList.addAll(epiDateTeacherdetaDetailList);
				}
			}
			if(epiDateFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> degreeTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean degreeIdFlag=false;
			if(degreeId!=null && !degreeId.equals("") && !degreeId.equals("0")){
				degreeIdFlag=true;
				DegreeMaster degreeMaster=degreeMasterDAO.findById(Long.valueOf(degreeId), false,false);
				degreeTeacherDetailList=teacherAcademicsDAO.findTeacherListByDegree(degreeMaster);
			}
			if(degreeId.equals("")){
				degreeIdFlag=true;
			}
			if(degreeIdFlag && degreeTeacherDetailList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(degreeTeacherDetailList);
				}else{
					filterTeacherList.addAll(degreeTeacherDetailList);
				}
			}
			if(degreeIdFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> universityTeacherList = new ArrayList<TeacherDetail>();
			boolean universityFlag=false;
			if(universityId!=null && !universityId.equals("") && !universityId.equals("0")){
				universityFlag=true;
				UniversityMaster universityMaster=universityMasterDAO.findById(Integer.valueOf(universityId), false,false);
				universityTeacherList=teacherAcademicsDAO.findTeacherListByUniversity(universityMaster);
			}
			if(universityId.equals("")){
				universityFlag=true;
			}
			if(universityFlag && universityTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(universityTeacherList);
				}else{
					filterTeacherList.addAll(universityTeacherList);
				}
			}
			if(universityFlag){
				teacherFlag=true;
			}
			//****************************tags filter*************************************************
			List<TeacherDetail> tagsTeacherList = new ArrayList<TeacherDetail>();
			boolean tagsFlag=false;
			if(tagsId!=null && !tagsId.equals("") && !tagsId.equals("0")){
				tagsFlag=true;
				SecondaryStatusMaster secondaryStatusMaster= secondaryStatusMasterDAO.findById(Integer.valueOf(tagsId), false,false);
				//tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndJob(secondaryStatusMaster,jobOrder);
				tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndDistrict(secondaryStatusMaster,districtMaster);
			}
			
			if(tagsFlag && tagsTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(tagsTeacherList);
				}else{
					filterTeacherList.addAll(tagsTeacherList);
				}
			}
			if(tagsFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> regionTeacherList = new ArrayList<TeacherDetail>();
			boolean regionFlag=false;
			if(regionId!=null && !regionId.equals("") && !regionId.equals("0")){
				regionFlag=true;
				regionTeacherList=teacherPreferenceDAO.findRegionByJobForTeacher(regionId);
			}
			if(regionId.equals("")){
				regionFlag=true;
			}
			if(regionFlag && regionTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(regionTeacherList);
				}else{
					filterTeacherList.addAll(regionTeacherList);
				}
			}
			if(regionFlag){
				teacherFlag=true;
			}
			List<TeacherDetail> cgpaTeacherList = new ArrayList<TeacherDetail>();
			boolean cgpaFlag=false;
			if(CGPA!=null && !CGPA.equals("")&& !CGPA.equals("0")){
				cgpaFlag=true;
				cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA(CGPA,CGPASelectVal);
			}
			if(cgpaFlag && cgpaTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(cgpaTeacherList);
				}else{
					filterTeacherList.addAll(cgpaTeacherList);
				}
			}
			if(cgpaFlag){
				teacherFlag=true;
			}
			if(statusPrivilege){
				teacherFlag=true;
				filterTeacherList.addAll(teacherList);
			}
			System.out.println("filterTeacherList:::"+filterTeacherList.size());
			System.out.println("teacherFlag::::::"+teacherFlag);

			if((teacherFlag && filterTeacherList.size()==0) || (!normScoreVal.equals("") && !normScoreVal.equals("0"))){
				lstJobForTeacher = new ArrayList<JobForTeacher>();
			}else{
				lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCG(userMaster,jobOrder,lstStatusMasters,5,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,false,false,null);
			}
			tableHtml.append(lstJobForTeacher.size());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tableHtml.toString();		
	}
	
	/*public String hireVueIntegration(Long jobForTeacherId)
	{
		JSONObject jsonObjects;
		WebContext context;
		context = WebContextFactory.get();
		DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
		DateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sbuffer = new StringBuffer();
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		int entityID=0,roleId=0;
		String createdDate;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		
		try{
			//TeacherDetail teacherDetail = null;
			//teacherDetail = teacherDetailDAO.findById(teacher, false, false);
			sbuffer.append("<input type='hidden' id='jobForTeacherId' value='"+jobForTeacherId+"'>");

			if(districtMaster!=null && districtMaster.getIsHireVueIntegration()!=null && districtMaster.getIsHireVueIntegration())
			{
				String hireVueAPIKey = districtMaster.getHireVueAPIKey();
				String csrftoken = "";
				String sessionId = "";
				if(session.getAttribute("csrftoken")==null)
				{
					String tokens[] = TMCommonUtil.loginToHireVue(hireVueAPIKey);
					csrftoken = tokens[0];
					sessionId = tokens[1];
					session.setAttribute("csrftoken",csrftoken);
					session.setAttribute("hireVueSessionId",sessionId);
				}else
				{
					csrftoken = (String)session.getAttribute("csrftoken");
					sessionId = (String)session.getAttribute("hireVueSessionId");
				}
				if(!sessionId.equals("") && !csrftoken.equals(""))
				{
					 String positions = TMCommonUtil.getPositions(csrftoken,sessionId);
					 System.out.println("Positions:: "+positions);
					 //JSONObject objects = (JSONObject) JSONSerializer.toJSON(positions); 
					 JSONArray jsonArr = new JSONArray(positions.toString());
					 sbuffer.append("<table id='content_del' cellspacing='0' cellpadding='1' class='table table-striped table-bordered'>");
					 sbuffer.append("<thead class='bg'><tr><th style='width: 5%'></th> <th style='width: 45%'>Position</th> <th style='width: 25%'>Interview Type</th> <th style='width: 25%'>Created Date</th></tr></thead>");
					 for(int i=0; i<jsonArr.length(); i++)
					 {
						 jsonObjects= new JSONObject(jsonArr.get(i).toString()); 
						 createdDate = dateformat.format(originalFormat.parse((String) jsonObjects.get("createDate")));
						 sbuffer.append("<tr><td style='vertical-align: top;text-align:left;'><input type='checkbox' name='posChkName' id="+jsonObjects.get("id")+" value='"+jsonObjects.get("id")+"'></td> <td>"+jsonObjects.get("title")+"</td>");
						 sbuffer.append("<td>"+jsonObjects.get("interviewType")+"</td> <td>"+createdDate+"</td></tr>");
						
				     }
					 sbuffer.append("</table>");
				}
			}
			

		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return sbuffer.toString();
	}*/
	
	public String hireVueIntegration(Long jobForTeacherId)
	 {
	  JSONObject jsonObjects;
	  WebContext context;
	  context = WebContextFactory.get();
	  DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
	  DateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
	  HttpServletRequest request = context.getHttpServletRequest();
	  HttpSession session = request.getSession(false);
	  StringBuffer sbuffer = new StringBuffer();
	  UserMaster userMaster=null;
	  DistrictMaster districtMaster=null;
	  SchoolMaster schoolMaster=null;
	  int entityID=0,roleId=0;
	  String createdDate;
	  
	  
	  
	  if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
	   throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	  }else{
	   userMaster= (UserMaster) session.getAttribute("userMaster");
	   if(userMaster.getRoleId().getRoleId()!=null){
	    roleId=userMaster.getRoleId().getRoleId();
	   }
	   if(userMaster.getDistrictId()!=null){
	    districtMaster=userMaster.getDistrictId();
	   }
	   if(userMaster.getSchoolId()!=null){
	    schoolMaster =userMaster.getSchoolId();
	   } 
	   entityID=userMaster.getEntityType();
	  }

	  
	  try{
	  
	   sbuffer.append("<input type='hidden' id='jobForTeacherId' value='"+jobForTeacherId+"'>");

	   if(districtMaster!=null && districtMaster.getIsHireVueIntegration()!=null && districtMaster.getIsHireVueIntegration())
	   {
	    String hireVueAPIKey = districtMaster.getHireVueAPIKey();
	    String csrftoken = "";
	    String sessionId = "";
	    if(session.getAttribute("csrftoken")==null)
	    {
	     String tokens[] = TMCommonUtil.loginToHireVue(hireVueAPIKey);
	     csrftoken = tokens[0];
	     sessionId = tokens[1];
	     session.setAttribute("csrftoken",csrftoken);
	     session.setAttribute("hireVueSessionId",sessionId);
	    }else
	    {
	     csrftoken = (String)session.getAttribute("csrftoken");
	     sessionId = (String)session.getAttribute("hireVueSessionId");
	    }
	    if(!sessionId.equals("") && !csrftoken.equals(""))
	    {
	      String positions = TMCommonUtil.getPositions(csrftoken,sessionId);
	     
	     //Check Apllied Position
	      List<InviteCandidateToPositions> inviteCandidateToPositions = null;
	      TeacherDetail teacherDetail = null;     
	      JobForTeacher jobForTeacher = null;
	      String[] checkPositionIdtArray =null;
	      
	      jobForTeacher = jobForTeacherDAO.findById(jobForTeacherId, false, false);
	      
	      if(jobForTeacher.getTeacherId()!=null){
	       teacherDetail =jobForTeacher.getTeacherId();
	      }
	      
	      inviteCandidateToPositions = inviteCandidateDAO.findByCriteria(Restrictions.eq("teacherId", teacherDetail));
	      
	       if(inviteCandidateToPositions!=null && inviteCandidateToPositions.size()>0){
	        
	         checkPositionIdtArray = new String[inviteCandidateToPositions.size()];
	        
	         int c=0;
	          
	         for(InviteCandidateToPositions checkedpositionId :inviteCandidateToPositions)
	         {
	          checkPositionIdtArray[c] = checkedpositionId.getPositionId()+"__"+checkedpositionId.getInterviewId();
	          c++;
	         }
	       }
	      //End Check Apllied Position
	     
	      JSONArray jsonArr = new JSONArray(positions.toString());
	      sbuffer.append("<table id='content_del' cellspacing='0' cellpadding='1' class='table table-striped table-bordered'>");
	      sbuffer.append("<thead class='bg'><tr><th style='width: 5%'></th> <th style='width: 45%'>Position</th> <th style='width: 25%'>Interview Type</th> <th style='width: 25%'>Created Date</th></tr></thead>");
	       int positionflag=0;
	       String posarr[];
	       String pos = "";
	       String interviewId = "";
	      for(int i=0; i<jsonArr.length(); i++)
	      {

	       jsonObjects= new JSONObject(jsonArr.get(i).toString()); 
	       createdDate = dateformat.format(originalFormat.parse((String) jsonObjects.get("createDate")));
	       
	       if(checkPositionIdtArray!=null){
	        
	        for(String positionss : checkPositionIdtArray){
	        	posarr = positionss.split("__");
	        	pos = posarr[0];
		         if(jsonObjects.get("id").toString().trim().equals(pos+""))
		         {
		        	 interviewId = posarr[1];
		        	 sbuffer.append("<tr><td style='vertical-align: top;text-align:left;'><input type='checkbox' name='posChkName' id="+jsonObjects.get("id")+"" +
		             " value='"+jsonObjects.get("id")+"__"+jsonObjects.get("title")+"'  disabled='disabled' ></td> <td><a href='https://app.hirevue.com/#/evaluations/?evi="+interviewId+"'>"+jsonObjects.get("title")+"</a></td>");
		        	 sbuffer.append("<td>"+jsonObjects.get("interviewType")+"</td> <td>"+createdDate+"</td></tr>");
		        	 positionflag++;
		        	 break;
		         }else
		        	 positionflag=0;
	        }	
	        if(positionflag==0)
	        {
	         sbuffer.append("<tr><td style='vertical-align: top;text-align:left;'><input type='checkbox' name='posChkName' id="+jsonObjects.get("id")+"" +
	            " value='"+jsonObjects.get("id")+"__"+jsonObjects.get("title")+"'></td> <td>"+jsonObjects.get("title")+"</td>");
	          sbuffer.append("<td>"+jsonObjects.get("interviewType")+"</td> <td>"+createdDate+"</td></tr>");
	        }
	       }
	       else
	       {
	        sbuffer.append("<tr><td style='vertical-align: top;text-align:left;'><input type='checkbox' name='posChkName' id="+jsonObjects.get("id")+"" +
	           " value='"+jsonObjects.get("id")+"__"+jsonObjects.get("title")+"'></td> <td>"+jsonObjects.get("title")+"</td>");
	         sbuffer.append("<td>"+jsonObjects.get("interviewType")+"</td> <td>"+createdDate+"</td></tr>");
	       }
	         }
	      sbuffer.append("</table>");
	    }
	   }

	  }catch(Exception e)
	  {
	   e.printStackTrace();
	  }
	  return sbuffer.toString();
	 }
	
	public String addACandidateToAPosition(String posId, Long jobForTeacherId)
	{
		try{
			String csrftoken = "";
			String sessionId = "";
			String firstName = "" , lastName = "", email = "";
			
			WebContext context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			csrftoken = (String)session.getAttribute("csrftoken");
			sessionId = (String)session.getAttribute("hireVueSessionId");
			
			JobForTeacher jobForTeacher = null;
			jobForTeacher = jobForTeacherDAO.findById(jobForTeacherId, false, false);
			firstName = jobForTeacher.getTeacherId().getFirstName();
			lastName = jobForTeacher.getTeacherId().getLastName();
			email = jobForTeacher.getTeacherId().getEmailAddress();

			String posIds[] = posId.split(",");
			for(int i=0; i<posIds.length; i++)
			{
				//String interviewStatus = TMCommonUtil.addACandidateToAPosition(csrftoken, sessionId, posIds[i], firstName, lastName, email);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "";
	}


	// Start TPL-4318 Add PDP Configuration  *****************************************
	// Start generatePDPReportNew Function ***************************************************
	public String generatePDPReportNew(HttpServletRequest request,TeacherDetail teacherDetail,String basePath,String fileName,String urlRootPath)
	{
		System.out.println("In Side CandidateGridAjax->generatePDPReportNew");
		WebContext context;
		context = WebContextFactory.get();
		request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		String pdpTypeFlag=null;
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}	
		try{
			Map<Integer,String> jobappliedfordistrict=new HashMap<Integer, String>();	
			List<JobForTeacher> jftForPDFReport=null;
			DistrictMaster districtmaster=null;
			UserMaster userMaster=null;
			List<AssessmentDetail> assessmentdetail=null;
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			jftForPDFReport=jobForTeacherDAO.findAllAppliedJobsOfTeacher(teacherDetail.getTeacherId());
			
			System.out.println("jftForPDFReport.."+jftForPDFReport.size());
		
		if(jftForPDFReport!=null && jftForPDFReport.size()!=0){
			Set<Integer> tlist = new HashSet<Integer>();
			for(JobForTeacher tlis :jftForPDFReport)
			{
				tlist.add(tlis.getDistrictId());
			}
			
			System.out.println("Set Size :"+tlist.size());
			Criterion criterion = Restrictions.in("districtId",tlist);
			List<DistrictMaster> districtnamelist= districtMasterDAO.findByCriteria(criterion);
			if(districtnamelist!=null)
			for(DistrictMaster dm :districtnamelist)
			{
				jobappliedfordistrict.put(dm.getDistrictId(), dm.getDistrictName());
			}
		 }
		System.out.println("Job Applied in : "+jobappliedfordistrict.size()+"  District");
		
		if(jftForPDFReport!=null && jftForPDFReport.size()==0)
		{
			System.out.println("Add PDPD Configuration Case 1 :::: ");
			System.out.println("In Side CandidateGridAjax->generatePDPReportNew");
			if(userMaster!=null && userMaster.getEntityType()==1||userMaster.getEntityType()==2||userMaster.getEntityType()==3)
				pdpTypeFlag=eitherIpiorEpiPDPCall(request, teacherDetail, basePath, fileName, urlRootPath);
			else{
				candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);// For EPI PDP
				pdpTypeFlag=null;
			}
		}
		
		// For Single job Applied and multiple job applied for only one  district
		else if((jftForPDFReport!=null && jobappliedfordistrict!=null) && (jftForPDFReport.size()==1 || jobappliedfordistrict.size()==1))
		{
			System.out.println("Add PDPD Configuration Case 2 & 3.");
			System.out.println("In Side CandidateGridAjax->generatePDPReportNew");
			// DA Users
			if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getEntityType()==2)
				districtmaster=districtMasterDAO.findByDistrictId(userMaster.getDistrictId().getDistrictId()+"");
			// TM Admin 
			if(userMaster.getEntityType()!=null && userMaster.getEntityType()==1)
				{ 
				//jftForPDFReport.get(0).getDistrictId();
				districtmaster=districtMasterDAO.findById(jftForPDFReport.get(0).getDistrictId(), false, false);
				}	
			if(districtmaster!=null && districtmaster.getDefaultPDP()!=null && districtmaster.getDefaultPDP()!=3){
			// EPI PDP ***************	
				if(districtmaster.getDefaultPDP()==1)
				{
				candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);// For EPI PDP
				pdpTypeFlag=null;
				}
			// IPI PDP *****************	
				else if(districtmaster.getDefaultPDP()==2)
				{
					pdpTypeFlag=genrateIpiPdpCall(teacherDetail);
				}
				else
				{
					//Comming Soon
					pdpTypeFlag="Not Available";
				}
			}else if(userMaster!=null && userMaster.getEntityType()!=null && userMaster.getEntityType()!=1 ||userMaster.getEntityType()!=2){
				candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);// For EPI PDP
				pdpTypeFlag=null;
				}
			else{
				pdpTypeFlag=eitherIpiorEpiPDPCall(request, teacherDetail, basePath, fileName, urlRootPath);
			}
			
		}
		
		else if((jftForPDFReport!=null && jobappliedfordistrict!=null) && (jftForPDFReport.size()>1 && jobappliedfordistrict.size()>1))
		{
			// DA Users
			if(userMaster!= null && userMaster.getEntityType()!=null && userMaster.getEntityType()==2){ 
				System.out.println("Add PDPD Configuration Case 4 For DA users::: ");
				System.out.println("In Side CandidateGridAjax->generatePDPReportNew");
				Integer districtID=userMaster.getDistrictId().getDistrictId();	
				districtmaster=districtMasterDAO.findById(districtID, false, false);
				if(districtmaster!=null && districtmaster.getDefaultPDP()!=null && districtmaster.getDefaultPDP()!=3){
				if(districtmaster!=null && districtmaster.getDefaultPDP()==1)
					{
					candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);// For EPI PDP// For EPI PDP
					pdpTypeFlag=null;
					}
					else if(districtmaster!=null && districtmaster.getDefaultPDP()==2)
					{
						//open IPI PDP for multiple job
						pdpTypeFlag=genrateIpiPdpCall(teacherDetail);
					}
					else
					{
						//Comming Soon
						pdpTypeFlag="Not Available";
					}
				}else{
					pdpTypeFlag=eitherIpiorEpiPDPCall(request, teacherDetail, basePath, fileName, urlRootPath);
					}
				}
			else if(userMaster!= null && userMaster.getEntityType()!=null && userMaster.getEntityType()==1){
				System.out.println("Case 4 Display District Popup :::::::::");
				System.out.println("In Side CandidateGridAjax->generatePDPReportNew");
				StringBuffer temp = null; 
					for (Map.Entry<Integer, String> entry : jobappliedfordistrict.entrySet()){ 					
					if(temp == null){
						temp = new StringBuffer();
						temp.append( entry.getKey()+"!!!");
						temp.append(entry.getValue());
					}else{
						temp.append( "!!!!"+entry.getKey()+"!!!");
						temp.append(entry.getValue());
					}
				}
				pdpTypeFlag=temp.toString()+" ##"+"Case3DistrictList";
			  }
			else{
				candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);// For EPI PDP
				pdpTypeFlag=null;
			}
		}else
		{
			candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);// For EPI PDP
			pdpTypeFlag=null;
		}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return pdpTypeFlag;
	}
	
	public String eitherIpiorEpiPDPCall(HttpServletRequest request,TeacherDetail teacherDetail,String basePath,String fileName,String urlRootPath)
	{
		System.out.println("In Side CandidateGridAjax->eitherIpiorEpiPDPCall");
		 String pdpTypeFlag=null;
		List<TeacherAssessmentStatus> teacherassessmentEPI=teacherAssessmentStatusDAO.findLatestAssessmentTakenByTeachersEPI(teacherDetail);
		List<TeacherAssessmentStatus> teacherassessmentIPI=teacherAssessmentStatusDAO.findLatestAssessmentTakenByTeachersIPI(teacherDetail);
		// Only EPI Given
		if((teacherassessmentEPI!=null &&teacherassessmentEPI.size()>0) && ( teacherassessmentIPI!=null&&teacherassessmentIPI.size()==0)){
			Integer pdpTypeinAssessmentDetailEPI=teacherassessmentEPI.get(0).getAssessmentDetail().getPdpreporttype();
			Integer assessmentTypeEPI=teacherassessmentEPI.get(0).getAssessmentDetail().getAssessmentType();
	
			if(pdpTypeinAssessmentDetailEPI!=null && pdpTypeinAssessmentDetailEPI!=0 && pdpTypeinAssessmentDetailEPI!=3){
			if(pdpTypeinAssessmentDetailEPI==1){
				candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);// For EPI PDP
				pdpTypeFlag=null;
			}else if(pdpTypeinAssessmentDetailEPI==2){
				pdpTypeFlag=genrateIpiPdpCall(teacherDetail);
			}else
				pdpTypeFlag="Not Available";
		
		}else{
			if(assessmentTypeEPI!=null && assessmentTypeEPI==1 || assessmentTypeEPI==4){
				if(assessmentTypeEPI==1){
					candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);// For EPI PDP
					pdpTypeFlag=null;	
				}
				else if(assessmentTypeEPI==4){
					pdpTypeFlag=genrateIpiPdpCall(teacherDetail);
				}	
				else
					pdpTypeFlag="Not Available";
				}
			else
			 pdpTypeFlag="Not Available";
			}
		}// Only IPI Given 
		else if((teacherassessmentEPI!=null && teacherassessmentIPI.size()==0) && (teacherassessmentIPI!=null && teacherassessmentIPI.size()>0)){
			Integer pdpTypeinAssessmentDetailIPI=teacherassessmentIPI.get(0).getAssessmentDetail().getPdpreporttype();
			Integer assessmentTypeIPI=teacherassessmentIPI.get(0).getAssessmentDetail().getAssessmentType();
	
			if(pdpTypeinAssessmentDetailIPI!=null && pdpTypeinAssessmentDetailIPI!=0 && pdpTypeinAssessmentDetailIPI!=3){
			if(pdpTypeinAssessmentDetailIPI==1){
				candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);// For EPI PDP
				pdpTypeFlag=null;
			}else if(pdpTypeinAssessmentDetailIPI==2){
				pdpTypeFlag=genrateIpiPdpCall(teacherDetail);
			}else
				pdpTypeFlag="Not Available";
		
		}else{
			if(assessmentTypeIPI!=null && assessmentTypeIPI==1 || assessmentTypeIPI==4){
				if(assessmentTypeIPI==1){
					candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);// For EPI PDP
					pdpTypeFlag=null;	
				}
				else if(assessmentTypeIPI==4){
					pdpTypeFlag=genrateIpiPdpCall(teacherDetail);
				}	
				else
					pdpTypeFlag="Not Available";
				}
			else
			 pdpTypeFlag="Not Available";
			}
		
			
		}
		// EPI and IPI Both given
		else if((teacherassessmentEPI!=null && teacherassessmentEPI.size()>0) && (teacherassessmentIPI!=null && teacherassessmentIPI.size()>0)){
			
			candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);// For EPI PDP
		}
		else
			pdpTypeFlag="Not Available";
		
		return pdpTypeFlag;
	}
	
	// END generatePDPReportNew Function ***************************************************	
	public String generatePDPReportForMultipleDistrict(HttpServletRequest request,TeacherDetail teacherDetail,String basePath,String fileName,String urlRootPath,String districtId)
	{
		System.out.println("In Side CandidateGridAjax->generatePDPReportForMultipleDistrict");
		WebContext context;
		context = WebContextFactory.get();
		request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		String pdpTypeFlag=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}	
		try{
			List<JobForTeacher> jftForPDFReport=null;
			DistrictMaster districtmaster=null;
			UserMaster userMaster=null;
			List<AssessmentDetail> assessmentdetail=null;
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			Integer districtid=Integer.parseInt(districtId);
			districtmaster=districtMasterDAO.findByDistrictId(districtid+"");
			jftForPDFReport=jobForTeacherDAO.findAllTeacherByDistrictId(teacherDetail, districtmaster);
			if(jftForPDFReport!=null)
			{
			if(districtmaster!=null && districtmaster.getDefaultPDP()!=null && districtmaster.getDefaultPDP()!=3){
			// EPI PDP **********
				if(districtmaster!=null && districtmaster.getDefaultPDP()!=null && districtmaster.getDefaultPDP()==1)
				{
					candidateReportService.generatePDReport(request,teacherDetail, basePath,fileName,urlRootPath);// For EPI PDP
					pdpTypeFlag=null;
				}
				// IPI PDP **********	
				else if(districtmaster!=null &&  districtmaster.getDefaultPDP()!=null && districtmaster.getDefaultPDP()==2)
				{
					//open IPI PDP
					pdpTypeFlag=genrateIpiPdpCall(teacherDetail);
				}
				else
				{
					//Comming Soon
					pdpTypeFlag="Not Available";
				}
			}else
			{
				pdpTypeFlag=eitherIpiorEpiPDPCall(request, teacherDetail, basePath, fileName, urlRootPath);
				}
		}else 
			{pdpTypeFlag="Not Available";}
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return pdpTypeFlag;
	}
	public String genrateIpiPdpCall(TeacherDetail teacherDetail){
		System.out.println("In Side CandidateGridAjax->genrateIpiPdpCall");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String ipipdp=null;
		boolean isError = true;
		ServletContext servletContext = request.getSession().getServletContext();
		String fileName = "", basePath = "", urlRootPath = "";
		try{
		List<TeacherAssessmentStatus> teacherassessment=null;
		teacherassessment=teacherAssessmentStatusDAO.findLatestAssessmentTakenByTeachersIPI(teacherDetail);
	    if(teacherassessment!=null && teacherassessment.size()==0)
	    	teacherassessment=teacherAssessmentStatusDAO.findLatestAssessmentTakenByTeachersEPI(teacherDetail);
		Integer	assessmentAttemptNo=teacherassessment.get(0).getAssessmentTakenCount();
	    Integer assessmentType=teacherassessment.get(0).getAssessmentDetail().getAssessmentType();
	     String grpName=teacherassessment.get(0).getAssessmentDetail().getAssessmentGroupDetails().getAssessmentGroupName();
		List<AssessmentGroupDetails> assessmentGroupDetailList = assessmentGroupDetailsDAO.getAssessmentGroupDetailsListByTypeAndName(assessmentType, grpName);
		if(assessmentGroupDetailList!=null && assessmentGroupDetailList.size()>0){
			List<StatusMaster> statusMasterList = Utility.getStaticMasters(new String[]{"comp","vlt"});
			
			System.out.println("teacherDetail.getTeacherId() : "+teacherDetail.getTeacherId());
			List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.findByTeacherAssessmentTypeAndStatus(teacherDetail, assessmentType, statusMasterList);
			TeacherAssessmentStatus teacherAssessmentStatus = null;
			if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0 && assessmentAttemptNo<=teacherAssessmentStatusList.size()){
				System.out.println("teacherAssessmentStatusList.size() : "+teacherAssessmentStatusList.size());
				if(assessmentAttemptNo==0){
					assessmentAttemptNo = teacherAssessmentStatusList.size();
					teacherAssessmentStatus = teacherAssessmentStatusList.get(teacherAssessmentStatusList.size()-1);
				}
				else
				teacherAssessmentStatus = teacherAssessmentStatusList.get(assessmentAttemptNo-1);
				if(teacherAssessmentStatus!=null){
					System.out.println("teacherAssessmentStatus.getTeacherAssessmentStatusId() : "+teacherAssessmentStatus.getTeacherAssessmentStatusId());
					fileName = teacherDetail.getFirstName()+"-"+teacherDetail.getLastName()+"_"+Utility.convertDateAndTimeToDatabaseformatTime(new Date())+".pdf";
					basePath = servletContext.getRealPath("/")+"/candidatereports/"+teacherDetail.getTeacherId()+"/IPIPdReport/";
					urlRootPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
					System.out.println("fileName : "+fileName);
					System.out.println("basePath : "+basePath);
					System.out.println("urlRootPath : "+urlRootPath);
					File file = new File(basePath);
					if(!file.exists())
						 file.mkdirs();
					FileUtils.cleanDirectory(file);
					if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equals("comp")){
						candidateReportService.generateIPIPDReport(request, teacherAssessmentStatus, basePath, fileName, urlRootPath);
						File file2 = new File(basePath+fileName);
						if(file2.exists()){
							isError=false;
							String urlipi=urlRootPath+"candidatereports/"+teacherDetail.getTeacherId()+"/IPIPdReport/"+fileName;
							ipipdp=urlipi+" ##"+"IPI";
					        System.out.println("Success");
						}
					}
				}
			}
		}	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return ipipdp;
	}
	// ************************************
	public String generatePDPReportForMultipleJobApplyDistrict(String districtId,Integer teacher){
		System.out.println("In Side CandidateGridAjax->generatePDPReportForMultipleJobApplyDistrict");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try{
			System.out.println("============ generate PDReport ============");
			String path = request.getContextPath();
			String urlRootPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
			TeacherDetail teacherDetail = null;
			teacherDetail = teacherDetailDAO.findById(teacher, false, false);
			int teacherId = teacherDetail.getTeacherId();
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/candidatereports/"+teacherId;
			String firstName = Utility.eliminateInvalidUTFChars(teacherDetail.getFirstName().trim());
			String lastName = Utility.eliminateInvalidUTFChars(teacherDetail.getLastName().trim());
			String fileName = firstName+"-"+lastName+"_"+Utility.convertDateAndTimeToDatabaseformatTime(new Date())+".pdf";
			System.out.println("fileName::: "+fileName);
			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();
			Utility.deleteAllFileFromDir(basePath);
			String pdpTypeFlag=generatePDPReportForMultipleDistrict(request,teacherDetail, basePath,fileName,urlRootPath,districtId);
			String temp[]=null;
				if(pdpTypeFlag!=null)
					temp=pdpTypeFlag.split(" ##");
				if(pdpTypeFlag==null)
					return urlRootPath+"candidatereports/"+teacherId+"/"+fileName;
				if(pdpTypeFlag!=null){
					if(temp[1].equals("IPI"))
					return temp[0];
				}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "Not available";
	}
// End TPL-4318 Add PDP Configuration indrajeet *****************************************	

	@Transactional(readOnly=true)
	public String getHiredForKellyCandidateReports(String jobId,String firstName,String lastName,String emailAddress,String certType,String regionId,String degreeId,String universityId,String normScoreSelectVal,String 
			normScoreVal,String CGPA,String CGPASelectVal,String contactedVal,String candidateStatus,String stateId,String orderColumn, String sortingOrder,int totalNoOfRecord,String tagsId,boolean callbreakup,int callNoofRecords,
			Integer[] certIds,String YOTESelectVal,String YOTE,String stateId2,String zipCode,String epiFromDate,String epiToDate)
	{	
		
		System.out.println("================================== getHiredForKellyCandidateReports ==========================");
		
		Integer districtIdForSpecific = 3702970;
		Map<String,RawDataForDomain> mapRawDomainScore = null;
		List<JobForTeacher> lstJobForTeacher = null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		int entityID=0,roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}

		TeacherAssessmentStatus assessmentStatus = null;
		StatusMaster status = null;
		StringBuffer tableHtml = new StringBuffer();
		List<String> hiredlst=new ArrayList<String>(); // add by ram nath for 10-10 record
		double[] meanArray=null;
		int noOfRecordCheck =totalNoOfRecord;
		boolean smartPractices=false,achievementScore=false,tFA=false,demoClass=false,displayPhoneInterview=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,CGPADis=false,fitFirstNoble=false,fitSecondNoble=false,fitMiami=false,jobAppliedDate=false,videoInterview=false,schoolSelection=false,PNQ=false,senNum=false;
		CandidateGridService cgService=new CandidateGridService();
		CGInviteInterviewAjax cgInviteInterviewAjax = new CGInviteInterviewAjax();
		boolean statusPrivilege=false;
		List<TeacherDetail> teacherList=new ArrayList<TeacherDetail>();
		Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
		Map<String,String> statusNameMap=new HashMap<String,String>();
		List<DistrictAssessmentDetail> districtAssessmentDetailList=new ArrayList<DistrictAssessmentDetail>();
		List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=new ArrayList<OnlineActivityQuestionSet>(); 
		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList=new ArrayList<TeacherDistrictAssessmentStatus>();
		List<TeacherDistrictAssessmentAnswerDetail> teacherAssessmentAnswerDetailsList = new ArrayList<TeacherDistrictAssessmentAnswerDetail>();
		try 
		{	
			String tFname = "",tLname = "",roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}

			//List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			meanArray = new double[lstDomain.size()];

			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			
			try{
				districtAssessmentDetailList=districtAssessmentJobRelationDAO.findDistrictAssessmentDetailByJobOrder(jobOrder);
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990))
					onlineActivityQuestionSetList=onlineActivityQuestionSetDAO.getOnlineActivityQuestionSetList(jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster());
				
				if(userMaster.getEntityType()==3){
					SecondaryStatus secondaryStatus=secondaryStatusDAO.findSecondaryStatusObjByJobCategoryAndHBD(jobOrder);
					teacherList=teacherStatusHistoryForJobDAO.findTeacherListByStatusAndSecStatusHBD(jobOrder,secondaryStatus);
					if(teacherList!=null && userMaster.getEntityType()==3){
						statusPrivilege=true;
					}
				}
				
				
			if(jobOrder.getDistrictMaster()!=null){
					DistrictMaster districtMasterObj=jobOrder.getDistrictMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					if(districtMasterObj.getDistrictId()==7800038){
						PNQ=true;
						fitFirstNoble=true;
						fitSecondNoble=true;
					}
					if(districtMasterObj.getDistrictId()==1200390){
						try{
							if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Instructional")){
								fitMiami=true;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					if(districtMasterObj.getJobAppliedDate()!=null && districtMasterObj.getJobAppliedDate()){
						jobAppliedDate=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(districtMasterObj.getStatusMasterForCC()!=null && districtMasterObj.getSecondaryStatusForCC()==null){
							schoolSelection=true;
						}else if(districtMasterObj.getStatusMasterForCC()==null && districtMasterObj.getSecondaryStatusForCC()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(jobOrder.getBranchMaster()!=null){
					BranchMaster districtMasterObj=jobOrder.getBranchMaster();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(districtMasterObj.getDisplayPhoneInterview()!=null && districtMasterObj.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(jobOrder.getHeadQuarterMaster()!=null){
					HeadQuarterMaster hqMaster=jobOrder.getHeadQuarterMaster();
					if(hqMaster.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(hqMaster.getDisplayTFA()){
						tFA=true;
					}
					if(hqMaster.getDisplayDemoClass()){
						demoClass=true;
					}
					if(hqMaster.getDisplayJSI()){
						JSI=true;
					}
					if(hqMaster.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(hqMaster.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(hqMaster.getDisplayFitScore()){
						fitScore=true;
					}
					if(hqMaster.getDisplayCGPA()){
						CGPADis=true;
					}
					
					if(hqMaster.getDisplayPhoneInterview()!=null && hqMaster.getDisplayPhoneInterview()){
						displayPhoneInterview=true;
					}
					try{
						if(jobOrder.getOfferVirtualVideoInterview()!=null && jobOrder.getOfferVirtualVideoInterview()){
							videoInterview=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{ 
						if(hqMaster.getCandidateConsiderationStatusId()!=null && hqMaster.getCandidateConsiderationSecondaryStatusId()==null){
							schoolSelection=true;
						}else if(hqMaster.getCandidateConsiderationStatusId()==null && hqMaster.getCandidateConsiderationSecondaryStatusId()!=null){
							schoolSelection=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}catch(Exception e){ e.printStackTrace();}
			if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null){
				if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
					smartPractices=true;
				}
			}

			List<StatusMaster> lstStatusMastersForAll = new ArrayList<StatusMaster>();
			String[] statussForAll = {"hird","comp","scomp","ecomp","vcomp","dcln","icomp","rem","vlt","widrw"};
			try{
				lstStatusMastersForAll = Utility.getStaticMasters(statussForAll);
			}catch (Exception e) {
				e.printStackTrace();
			}
			/* filter Add  */
			List<TeacherDetail> filterTeacherList = new ArrayList<TeacherDetail>();
			List<TeacherDetail> certTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean certFlag=false;
			boolean teacherFlag=false;
			StateMaster stateMaster=null;
			List<CertificateTypeMaster> certificateTypeMasterList =new ArrayList<CertificateTypeMaster>();
			if(certType!=null && !certType.equals("") && !certType.equals("0")||!stateId.equals("0")){
				if(!stateId.equals("0")){
					stateMaster=stateMasterDAO.findById(Long.valueOf(stateId), false,false);
				}
				certificateTypeMasterList=certificateTypeMasterDAO.findCertificationByJob(stateMaster,certType);
				if(certificateTypeMasterList.size()>0)
					certTeacherDetailList = teacherCertificateDAO.findCertificateByJobForTeacher(certificateTypeMasterList);
				certFlag=true;
			}
			if(certType.equals("")){
				certFlag=true;
			}
			if(certFlag && certTeacherDetailList.size()>0){
				filterTeacherList.addAll(certTeacherDetailList);
			}
			if(certFlag){
				teacherFlag=true;
			}
			
			//multiple certificate filter
			boolean mCertFlag=false;
			List<Integer> certTypeIdList = new ArrayList<Integer>();
			List<Integer> teacherIdList = new ArrayList<Integer>();
			List<TeacherDetail> mCertTeacherDetailList = new ArrayList<TeacherDetail>();
			if(certIds!=null && !certIds.equals("") && certIds.length>0){
				for(int i=0;i<certIds.length;i++)
				{
					certTypeIdList.add(certIds[i]);
				}
				teacherIdList = teacherCertificateDAO.findTeacherByCertificates(certTypeIdList);
				if(teacherIdList!=null && !teacherIdList.equals("") && teacherIdList.size()>0){
					for(int i=0;i<teacherIdList.size();i++)
					{
						TeacherDetail teacherDetail = new TeacherDetail();
						teacherDetail.setTeacherId(teacherIdList.get(i));
						mCertTeacherDetailList.add(teacherDetail);
					}
				}
				mCertFlag=true;
			}
			if(mCertFlag && mCertTeacherDetailList.size()>0){
				filterTeacherList.addAll(mCertTeacherDetailList);
			}
			if(mCertFlag){
				teacherFlag=true;
			}
			//yote filter
			List<TeacherDetail> expTeacherList = new ArrayList<TeacherDetail>();
			boolean expFlag=false;
			if(YOTE!=null && !YOTE.equals("")&& !YOTE.equals("0")){
				expFlag=true;
				expTeacherList=teacherExperienceDAO.findTeachersByExperience(YOTE,YOTESelectVal);
			}
			if(expFlag && expTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(expTeacherList);
				}else{
					filterTeacherList.addAll(expTeacherList);
				}
			}
			if(expFlag){
				teacherFlag=true;
			}
			//state filter
			List<TeacherDetail> stateTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean stateFlag=false;
			if(stateId2!=null && !stateId2.equals("") && !stateId2.equals("0"))
			{
				stateFlag=true;
				StateMaster stateMaster1 = stateMasterDAO.findById(Long.valueOf(stateId2), false, false);
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByState(stateMaster1);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					stateTeacherDetailList.add(teacherDetail);
				}
			}
			
			if(stateFlag && stateTeacherDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(stateTeacherDetailList);
				else
					filterTeacherList.addAll(stateTeacherDetailList);
			}
			if(stateFlag)
				teacherFlag=true;
			//zip code filter
			List<TeacherDetail> zipCodeTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			boolean zipCodeFlag=false;
			if(zipCode!=null && !zipCode.equals("") && !zipCode.equals("0"))
			{
				zipCodeFlag=true;
				List<TeacherPersonalInfo> teacherPersonalInfoList = teacherPersonalInfoDAO.findByZipCode(zipCode);
				for(TeacherPersonalInfo teacherPersonalInfo : teacherPersonalInfoList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherPersonalInfo.getTeacherId());
					zipCodeTeacherdetaDetailList.add(teacherDetail);
				}
			}
			
			if(zipCodeFlag && zipCodeTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag)
					filterTeacherList.retainAll(zipCodeTeacherdetaDetailList);
				else
					filterTeacherList.addAll(zipCodeTeacherdetaDetailList);
			}
			if(zipCodeFlag)
				teacherFlag=true;
			
			//********************** EPI Date Filter By Ravindra ***************************
			Date epifDate=null;
			Date epitDate=null;
			boolean epiDateFlag=false;
			List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			try{
				if(!epiFromDate.equals("")){
					epifDate=Utility.getCurrentDateFormart(epiFromDate);
					epiDateFlag=true;
				}
				if(!epiToDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(epiToDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					epitDate=cal2.getTime();
					epiDateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(epiDateFlag)
			{
				StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.getAssessmentCompletedDateTimeTeacherList(1, statusMaster, epifDate, epitDate);
				for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList)
				{
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherAssessmentStatus.getTeacherDetail().getTeacherId());
					epiDateTeacherdetaDetailList.add(teacherDetail);
				}
			}
			if(epiDateFlag && epiDateTeacherdetaDetailList.size()>0)
			{
				if(teacherFlag){
					filterTeacherList.retainAll(epiDateTeacherdetaDetailList);
				}
				else{
					filterTeacherList.addAll(epiDateTeacherdetaDetailList);
				}
			}
			if(epiDateFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> degreeTeacherDetailList = new ArrayList<TeacherDetail>();
			boolean degreeIdFlag=false;
			if(degreeId!=null && !degreeId.equals("") && !degreeId.equals("0")){
				degreeIdFlag=true;
				DegreeMaster degreeMaster=degreeMasterDAO.findById(Long.valueOf(degreeId), false,false);
				degreeTeacherDetailList=teacherAcademicsDAO.findTeacherListByDegree(degreeMaster);
			}
			if(degreeId.equals("")){
				degreeIdFlag=true;
			}
			if(degreeIdFlag && degreeTeacherDetailList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(degreeTeacherDetailList);
				}else{
					filterTeacherList.addAll(degreeTeacherDetailList);
				}
			}
			if(degreeIdFlag){
				teacherFlag=true;
			}

			List<TeacherDetail> universityTeacherList = new ArrayList<TeacherDetail>();
			boolean universityFlag=false;
			if(universityId!=null && !universityId.equals("") && !universityId.equals("0")){
				universityFlag=true;
				UniversityMaster universityMaster=universityMasterDAO.findById(Integer.valueOf(universityId), false,false);
				universityTeacherList=teacherAcademicsDAO.findTeacherListByUniversity(universityMaster);
			}
			if(universityId.equals("")){
				universityFlag=true;
			}
			if(universityFlag && universityTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(universityTeacherList);
				}else{
					filterTeacherList.addAll(universityTeacherList);
				}
			}
			if(universityFlag){
				teacherFlag=true;
			}
			//****************************tags filter*************************************************
			List<TeacherDetail> tagsTeacherList = new ArrayList<TeacherDetail>();
			boolean tagsFlag=false;
			if(tagsId!=null && !tagsId.equals("") && !tagsId.equals("0")){
				tagsFlag=true;
				SecondaryStatusMaster secondaryStatusMaster= secondaryStatusMasterDAO.findById(Integer.valueOf(tagsId), false,false);
				//tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndJob(secondaryStatusMaster,jobOrder);
				tagsTeacherList= teacherSecondaryStatusDAO.findTeacherBySecondarystatusAndDistrict(secondaryStatusMaster,districtMaster);
			}
			
			if(tagsFlag && tagsTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(tagsTeacherList);
				}else{
					filterTeacherList.addAll(tagsTeacherList);
				}
			}
			if(tagsFlag){
				teacherFlag=true;
			}
			
			List<TeacherDetail> regionTeacherList = new ArrayList<TeacherDetail>();
			boolean regionFlag=false;
			if(regionId!=null && !regionId.equals("") && !regionId.equals("0")){
				regionFlag=true;
				regionTeacherList=teacherPreferenceDAO.findRegionByJobForTeacher(regionId);
			}
			if(regionId.equals("")){
				regionFlag=true;
			}
			if(regionFlag && regionTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(regionTeacherList);
				}else{
					filterTeacherList.addAll(regionTeacherList);
				}
			}
			if(regionFlag){
				teacherFlag=true;
			}
			List<TeacherDetail> cgpaTeacherList = new ArrayList<TeacherDetail>();
			boolean cgpaFlag=false;
			if(CGPA!=null && !CGPA.equals("")&& !CGPA.equals("0")){
				cgpaFlag=true;
				cgpaTeacherList=teacherAcademicsDAO.findTeacherListByCGPA(CGPA,CGPASelectVal);
			}
			if(cgpaFlag && cgpaTeacherList.size()>0){
				if(teacherFlag){
					filterTeacherList.retainAll(cgpaTeacherList);
				}else{
					filterTeacherList.addAll(cgpaTeacherList);
				}
			}
			if(cgpaFlag){
				teacherFlag=true;
			}
			if(statusPrivilege){
				teacherFlag=true;
				filterTeacherList.addAll(teacherList);
			}

			if((teacherFlag && filterTeacherList.size()==0) || (!normScoreVal.equals("") && !normScoreVal.equals("0"))){
				lstJobForTeacher = new ArrayList<JobForTeacher>();
			}else{
				//lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCG(null,jobOrder,lstStatusMastersForAll,7,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,true,false,null);
				lstJobForTeacher = jobForTeacherDAO.findByJobOrderForCGOpSql(userMaster,jobOrder,lstStatusMastersForAll,7,firstName,lastName,emailAddress,teacherFlag,filterTeacherList,true,false,null,0,0,orderColumn,sortingOrder);
			}
			
			System.out.println("lstJobForTeacher::::>>>>>>>>."+lstJobForTeacher.size());
			int hiredTeacherCount=0;
			if(lstJobForTeacher.size()>0){
				boolean baseStatusFlag=false;
				boolean epiFlag=false;
				if(jobOrder.getDistrictMaster()!=null){
					if(jobOrder.getDistrictMaster().getNoEPI()==null ||(jobOrder.getDistrictMaster().getNoEPI()!=null && jobOrder.getDistrictMaster().getNoEPI()==false)){
						epiFlag=true;
					}
				}else if(jobOrder.getBranchMaster()!=null){
					if(jobOrder.getBranchMaster().getNoEPI()==null ||(jobOrder.getBranchMaster().getNoEPI()!=null && jobOrder.getBranchMaster().getNoEPI()==false)){
						epiFlag=true;
					}
				}else if(jobOrder.getHeadQuarterMaster()!=null){
					if(jobOrder.getHeadQuarterMaster().getNoEPI()==null ||(jobOrder.getHeadQuarterMaster().getNoEPI()!=null && jobOrder.getHeadQuarterMaster().getNoEPI()==false)){
						epiFlag=true;
					}
				}
				if(epiFlag){
					//if(jobCategoryMasterDAO.baseStatusChecked(jobOrder.getJobCategoryMaster().getJobCategoryId())){
					if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getStatus().equals("A") && jobOrder.getJobCategoryMaster().getBaseStatus()){
						baseStatusFlag=true;
					}
				}
				if(lstJobForTeacher.size()>0){
					
					List<TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();
					for(JobForTeacher jft:lstJobForTeacher){
						lstTeacherDetails.add(jft.getTeacherId());
					}
				
					List<Integer> statusIdList = new ArrayList<Integer>();
					String[] statusShrotName = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw","ielig","iielig"};
					
					//////////////////////////////////Get All Hired Teacher///////////////////////////////////////////////
					List<StatusMaster> statusMasters=Utility.getStaticMasters(statusShrotName);
					Map<String,StatusMaster> statusMap=new HashMap<String, StatusMaster>();
					for (StatusMaster statusMasterObj : statusMasters) {
						statusMap.put(statusMasterObj.getStatusShortName(),statusMasterObj);
					}
					Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
					List<TeacherStatusHistoryForJob> historyForJobFilterList = teacherStatusHistoryForJobDAO.findHireTeacherListByHBD(lstTeacherDetails,jobOrder,statusMap);
					List<TeacherStatusHistoryForJob> historyForJobList =new ArrayList<TeacherStatusHistoryForJob>();
					
					Map<String,String> mapHistrory = new HashMap<String,String>();
					List<TeacherStatusHistoryForJob> historyForJobs = teacherStatusHistoryForJobDAO.findByTeacherListAndJob(lstTeacherDetails,jobOrder);
					if(historyForJobs.size()>0){
						for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobs) {
							if(teacherStatusHistoryForJob.getStatus()!=null && teacherStatusHistoryForJob.getStatus().equalsIgnoreCase("W")){
								String sStatus=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##"+teacherStatusHistoryForJob.getStatus();
								mapHistrory.put(sStatus, teacherStatusHistoryForJob.getStatus());
							}
						}
					}
					try{
						Map<String,String> historyMap=new HashMap<String, String>();
						Map<String,TeacherStatusHistoryForJob> historyObjMap=new HashMap<String,TeacherStatusHistoryForJob>();
						if(historyForJobFilterList.size()>0)
						for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobFilterList) {
							String id= teacherStatusHistoryForJob.getJobOrder().getJobId()+"#"+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId();
							String idValue=historyMap.get(id);
							if(idValue!=null){
								idValue+="#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#";
								historyMap.put(id,idValue);
							}else{
								historyMap.put(id,"#"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName()+"#");
							}
							if(teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp") || teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("hird")){
								historyObjMap.put(id, teacherStatusHistoryForJob);
							}
						}
						if(historyObjMap.size()>0)
						for (Entry<String, TeacherStatusHistoryForJob> entry : historyObjMap.entrySet()){
							TeacherStatusHistoryForJob  historyObj=entry.getValue();
							String id= historyObj.getJobOrder().getJobId()+"#"+historyObj.getTeacherDetail().getTeacherId();
							String idValue=historyMap.get(id);
							if(idValue!=null && !(idValue.contains("rem")|| idValue.contains("dcln") || idValue.contains("widrw"))){
								historyForJobList.add(historyObj);
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				
					List<JobForTeacher> jftOffers=jobForTeacherDAO.findJobByTeacherListForOffers(lstTeacherDetails,jobOrder.getDistrictMaster(),statusMap.get("hird"),statusMap.get("vcomp"));
					System.out.println("jftOffers::::::::::::::::"+jftOffers.size());
					System.out.println("lstTeacherDetails::::::::::::::"+lstTeacherDetails.size());
					List<String> posList=new ArrayList<String>();
					Map<String,String> reqMap=new HashMap<String, String>();
					Map<String,List<SchoolMaster>> hiredSchol=new HashMap<String,List<SchoolMaster>>();

					
					for (JobForTeacher jobForTeacher : jftOffers){
						try{

							if(jobForTeacher.getRequisitionNumber()!=null){	
								if(jobForTeacher.getSchoolMaster()!=null){	
									String teacherIdAndJobId=jobForTeacher.getTeacherId().getTeacherId()+"#"+jobForTeacher.getJobId().getJobId();	
									List<SchoolMaster> allList=hiredSchol.get(teacherIdAndJobId);	
									if(hiredSchol.get(teacherIdAndJobId)!=null){	
									allList.add(jobForTeacher.getSchoolMaster());	
									hiredSchol.put(teacherIdAndJobId,allList);	
									}else{	
									List<SchoolMaster> tList=new ArrayList<SchoolMaster>();	
									tList.add(jobForTeacher.getSchoolMaster());	
									hiredSchol.put(teacherIdAndJobId,tList);	
									}	
								}			
								posList.add(jobForTeacher.getRequisitionNumber());			
								reqMap.put(jobForTeacher.getJobId().getJobId()+"#"+jobForTeacher.getTeacherId().getTeacherId(),jobForTeacher.getRequisitionNumber());		
							}
					}catch(Exception e){
						e.printStackTrace();
					}

					}
					
					List<DistrictRequisitionNumbers> districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(jobOrder.getDistrictMaster(),posList);
					Map<String,DistrictRequisitionNumbers> dReqNoMap = new HashMap<String,DistrictRequisitionNumbers>();
					try{
						for (DistrictRequisitionNumbers districtRequisitionNumbers2 : districtRequisitionNumbers) {
							dReqNoMap.put(districtRequisitionNumbers2.getRequisitionNumber(),districtRequisitionNumbers2);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					System.out.println("historyForJobList::::::::::::"+historyForJobList.size());
					System.out.println(" R: "+reqMap.size()+" RM: "+dReqNoMap.size()+" T: "+lstTeacherDetails.size()+" H "+historyForJobList.size()+" J "+jobOrder.getJobId());
					if(historyForJobList.size()>0){
						mapForHiredTeachers=cgService.getHiredList(reqMap,dReqNoMap,lstTeacherDetails,historyForJobList,jobOrder);
					}
					System.out.println("mapForHiredTeachers::::"+mapForHiredTeachers.size());
			
					System.out.println("lstJobForTeacher::::::::::::::"+lstJobForTeacher.size());
					
					
					/* Smart Practices Start*/
					Map<Integer,Integer> spStatusMap=new HashMap<Integer, Integer>();
					Map<Integer,Integer> lessonNoMap=new HashMap<Integer, Integer>();
					if(smartPractices){
						List<SpInboundAPICallRecord> spInboundAPICallRecordList= spInboundAPICallRecordDAO.getDetailsByTeacherList(lstTeacherDetails);
						for (SpInboundAPICallRecord spInboundAPICallRecord : spInboundAPICallRecordList) {
							if(spInboundAPICallRecord.getCurrentLessonNo()!=null){
								lessonNoMap.put(spInboundAPICallRecord.getTeacherDetail().getTeacherId(),spInboundAPICallRecord.getCurrentLessonNo());
							}
						}
						List<TeacherAssessmentStatus>  teacherAssessmentStatusList=new ArrayList<TeacherAssessmentStatus>();
						if(lstTeacherDetails.size()>0){
							teacherAssessmentStatusList=teacherAssessmentStatusDAO.findSmartPracticesTakenByTeachers(lstTeacherDetails);
							if(teacherAssessmentStatusList.size()>0)
							for (TeacherAssessmentStatus teacherAssessmentStatusObj : teacherAssessmentStatusList) {
								int keyAss=teacherAssessmentStatusObj.getTeacherDetail().getTeacherId();
								if(spStatusMap.get(keyAss)==null){
									if(teacherAssessmentStatusObj.getPass()!=null){
										if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("P")){
											spStatusMap.put(keyAss,1);
										}else if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("F")){
											spStatusMap.put(keyAss,0);
										}
									}else if(teacherAssessmentStatusObj.getStatusMaster()!=null && teacherAssessmentStatusObj.getStatusMaster().getStatusShortName().equals("vlt")){
										spStatusMap.put(keyAss,5);
									}else{
										spStatusMap.put(keyAss,2);
									}
								}else{
									int prevValue=spStatusMap.get(keyAss);
									if(teacherAssessmentStatusObj.getPass()!=null){
										if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("P")){
											if(prevValue==1){
												spStatusMap.put(keyAss,3);
											}else{
												spStatusMap.put(keyAss,1);
											}
										}else if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("F")){
											if(prevValue==0){
												spStatusMap.put(keyAss,4);
											}else{
												spStatusMap.put(keyAss,0);
											}
										}
									}else if(teacherAssessmentStatusObj.getStatusMaster()!=null && teacherAssessmentStatusObj.getStatusMaster().getStatusShortName().equals("vlt")){
										spStatusMap.put(keyAss,5);
									}else{
										spStatusMap.put(keyAss,2);
									}
								}
							}
						}
					}
					List<JobForTeacher> JFTList = new ArrayList<JobForTeacher>(lstJobForTeacher);
					for(JobForTeacher jft:JFTList)
					{	
						boolean isNoHired=true;
						try{
							if(mapForHiredTeachers.get(jft.getTeacherId().getTeacherId())!=null && mapForHiredTeachers.get(jft.getTeacherId().getTeacherId()).size()>0){
								isNoHired=false;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						
						boolean talentType=true;
						if(smartPractices){
							if(spStatusMap.get(jft.getTeacherId().getTeacherId())!=null){
								int spValue=spStatusMap.get(jft.getTeacherId().getTeacherId());
								if(spValue==0 || spValue==2 ||spValue==4|| spValue==5){
									talentType=false;
								}
							}else{
								if(lessonNoMap.get(jft.getTeacherId().getTeacherId())!=null){
									int lessonNo=lessonNoMap.get(jft.getTeacherId().getTeacherId());
									if(lessonNo>0){
										talentType=false;
									}
								}else{
									talentType=false;
								}
							}
						}
						String sStatus=jft.getTeacherId().getTeacherId()+"##"+jft.getJobId().getJobId()+"##W";
						if(mapHistrory.get(sStatus)!=null){
							talentType=true;
						}
						if(isNoHired ||  talentType==false){
							lstJobForTeacher.remove(jft);
						}
					}
				}
			}
			System.out.println("lstJobForTeacher::For Hired:::::::::::"+lstJobForTeacher.size());
			tableHtml.append(lstJobForTeacher.size());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tableHtml.toString();	
	}
	
	

}