package tm.services;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.util.UidGenerator;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.EventDetails;
import tm.bean.EventFacilitatorsList;
import tm.bean.EventParticipantsList;
import tm.bean.EventParticipantsTemp;
import tm.bean.EventSchedule;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.VVIResponse;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.EventDetailsDAO;
import tm.dao.EventFacilitatorsListDAO;
import tm.dao.EventParticipantsListDAO;
import tm.dao.EventParticipantsTempDAO;
import tm.dao.EventScheduleDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.i4.VVIResponseDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.IPAddressUtility;
import tm.utility.ImageResize;
import tm.utility.Utility;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class ParticipantsAjax {
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;
	@Autowired
	private JobOrderDAO jobOrderDAO;
	@Autowired
	private EventParticipantsTempDAO eventParticipantsTempDAO;
	@Autowired
	private EventParticipantsListDAO eventParticipantsListDAO;
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	@Autowired
	private EventDetailsDAO eventDetailsDAO;
	@Autowired
	private UserMasterDAO userMasterDAO;
	@Autowired
	private EventScheduleDAO eventScheduleDAO;
	@Autowired
	private EventFacilitatorsListDAO eventFacilitatorsListDAO;
	@Autowired
	private EmailerService emailerService;
	@Autowired
	private VVIResponseDAO vviResponseDAO;
	
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	public String displayParticipantsGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer eventId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer ptRecords =	new StringBuffer();
		System.out.println("call");
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		try {
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
					
				
			}
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"participantLastName";
			String sortOrderNoField		=	"participantLastName";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			EventDetails eventDetails = eventDetailsDAO.findById(eventId, false, false);
			districtMaster=eventDetails.getDistrictMaster();	
			List<EventParticipantsList> eventParticipantsLists = eventParticipantsListDAO.findListOfParticipants(sortOrderStrVal,start,noOfRowInPage,districtMaster,eventDetails);
			totalRecord= eventParticipantsListDAO.findListOfParticipantsCount(districtMaster,eventDetails).size();
			
			ptRecords.append("<table  id='participantsGrid' width='100%' border='0' >");
			ptRecords.append("<thead class='bg'>");
			ptRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblFname", locale),sortOrderFieldName,"participantFirstName",sortOrderTypeVal,pgNo,"participantsGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblLname", locale),sortOrderFieldName,"participantLastName",sortOrderTypeVal,pgNo,"participantsGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblEmailAddress", locale),sortOrderFieldName,"participantEmailAddress",sortOrderTypeVal,pgNo,"participantsGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblNormScore", locale),sortOrderFieldName,"normScore",sortOrderTypeVal,pgNo,"participantsGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
						
			ptRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			ptRecords.append("</tr>");
			ptRecords.append("</thead>");
			
			if(eventParticipantsLists.size()>0){
				for (EventParticipantsList eventParticipantsList : eventParticipantsLists) {
					ptRecords.append("<tr>");
					ptRecords.append("<td><input type='hidden' id='participantIds' value='"+eventParticipantsList.getEventParticipantId()+"'/>"+eventParticipantsList.getParticipantFirstName()+"</td>");
					ptRecords.append("<td>"+eventParticipantsList.getParticipantLastName()+"</td>");
					ptRecords.append("<td>"+eventParticipantsList.getParticipantEmailAddress()+"</td>");
					if(eventParticipantsList.getNormScore()==null || eventParticipantsList.getNormScore().equals(0)){
						ptRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");	
					}else{
						ptRecords.append("<td>"+eventParticipantsList.getNormScore()+"</td>");
					}
					ptRecords.append("<td><a href='javascript:void(0);' onclick='deleteParticipant("+eventParticipantsList.getEventParticipantId()+")'>"+Utility.getLocaleValuePropByKey("lnkRemo", locale)+"</td>");
					ptRecords.append("</tr>");
				}
				ptRecords.append("</table>" );
				ptRecords.append(PaginationAndSorting.getPaginationStringForParticipantsAjax(request,totalRecord,noOfRow, pageNo,"pageSize3","945","participantsGrid"));

			}else{
				ptRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
				ptRecords.append("</table>" );
			}
			
		//	
			
		} catch (Exception e) {
		}
		return ptRecords.toString();
	}
	
	public String displayCandidatesOnSearch(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer jobId, String firstName,String lastName,
			String emailAddress)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer ptRecords =	new StringBuffer();
		try {
			UserMaster userMaster=null;
			DistrictMaster districtMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}
			}
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"lastName";
			String sortOrderNoField		=	"lastName";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			List<Criterion> basicSearch = new ArrayList<Criterion>();
			
			if(firstName!=null && !firstName.equals(""))
			{
				Criterion criterion = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
				basicSearch.add(criterion);
			}
				
			if(lastName!=null && !lastName.equals(""))
			{
				Criterion criterion = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
				basicSearch.add(criterion);
			}
			
			if(emailAddress!=null && !emailAddress.equals(""))
			{
				Criterion criterion = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);
				basicSearch.add(criterion);
			}
			
			
			JobOrder joborder=null;
			if(jobId!=null && jobId!=0){
				joborder =jobOrderDAO.findById(jobId, false, false);
				districtMaster = joborder.getDistrictMaster();
			}
			List<TeacherDetail> teacherList=new ArrayList<TeacherDetail>();;
			//List<TeacherDetail> teacherList = jobForTeacherDAO.findAvailableCandidteBYDistrict(sortOrderStrVal,start,noOfRowInPage,districtMaster,joborder,basicSearch);
			totalRecord = jobForTeacherDAO.findAvailableCandidteBYDistrictCount(districtMaster,joborder,basicSearch).size();

			Map<Integer, Integer> tNormScore = new HashMap<Integer, Integer>();
			if(teacherList.size()>0){
				List<TeacherNormScore> teacherNormscoreLIst = teacherNormScoreDAO.findNormScoreByTeacherList(teacherList);
				
				if(teacherNormscoreLIst.size()>0)
					for (TeacherNormScore teacherNormScore : teacherNormscoreLIst) {						
						tNormScore.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore.getTeacherNormScore());
					}
			}
			
			ptRecords.append("<table  id='candidatesGrid' width='100%' border='0' >");
			ptRecords.append("<thead class='bg'>");
			ptRecords.append("<tr>");
			String responseText="";
			
			ptRecords.append("<th valign='top'>&nbsp;</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblFname", locale),sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo,"candidatesGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblLname", locale),sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo,"candidatesGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkCommonMul("Email Addess",sortOrderFieldName,"emailAddress",sortOrderTypeVal,pgNo,"candidatesGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblNormScore", locale),sortOrderFieldName,"normScore",sortOrderTypeVal,pgNo,"candidatesGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
						
			ptRecords.append("</tr>");
			ptRecords.append("</thead>");
			if(teacherList.size()>0){
				int i=1;
					for (TeacherDetail teacher : teacherList) {
							Integer teacherId=teacher.getTeacherId();
							String fName=teacher.getFirstName();
							String lName=teacher.getLastName();
							String email=teacher.getEmailAddress();
							Integer nScore=0;
							if(tNormScore.containsKey(teacherId)){
								nScore = tNormScore.get(teacherId);
							}
							
							
							ptRecords.append("<tr>");
							ptRecords.append("<td><input type='checkbox' id='"+teacherId+"' value='"+teacherId+"' onclick=\"saveParticipant('"+fName+"','"+lName+"','"+email+"','"+nScore+"','"+teacherId+"');\"></td>");
							ptRecords.append("<td>"+fName+"</td>");
							ptRecords.append("<td>"+lName+"</td>");
							ptRecords.append("<td>"+email+"</td>");
							if(!nScore.equals(0))
								ptRecords.append("<td>"+nScore+"</td>");
							else						
								ptRecords.append("<td>N/A</td>");
							
							i++;			
				}
					ptRecords.append("</table>" );
					ptRecords.append(PaginationAndSorting.getPaginationStringForParticipantsAjax(request,totalRecord,noOfRow, pageNo,"pageSize1","945","candidatesGrid"));
			}else{
				ptRecords.append("<tr><td colspan='5'>No record found</td></tr>" );
				ptRecords.append("</table>" );
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ptRecords.toString();
	}
	
	/*public String displayCandidatesOnPros(String noOfRow, String pageNo,String sortOrder,String sortOrderType,String firstName,String lastName,
			String emailAddress)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer ptRecords =	new StringBuffer();
		try {
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			============= For Sorting ===========
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			*//** set default sorting fieldName **//*
			String sortOrderFieldName	=	"lastName";
			String sortOrderNoField		=	"lastName";

			*//**Start set dynamic sorting fieldName **//*
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			*//**End ------------------------------------**//*
			
			List<Criterion> basicSearch = new ArrayList<Criterion>();
			if(firstName!=null && !firstName.equals(""))
			{
				Criterion criterion = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
				basicSearch.add(criterion);
			}
				
			if(lastName!=null && !lastName.equals(""))
			{
				Criterion criterion = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
				basicSearch.add(criterion);
			}
			
			if(emailAddress!=null && !emailAddress.equals(""))
			{
				Criterion criterion = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);
				basicSearch.add(criterion);
			}
			
			
			List teacherListNA = jobForTeacherDAO.findUniqueApplicants();
			List<Integer> teID = new ArrayList<Integer>();
			
			for (Object tDlist : teacherListNA) {
				 int ds = Utility.getIntValue(String.valueOf(tDlist));
				teID.add(ds);
			}

			List<TeacherDetail> teacherList = teacherDetailDAO.findAvailableCandidteNotApplYAnyJOb(sortOrderStrVal,start,noOfRowInPage,teID,basicSearch);
			totalRecord = teacherDetailDAO.findAvailableCandidteNotApplYAnyJObCount(teID,basicSearch).size();
			
			ptRecords.append("<table  id='prosGrid' width='100%' border='0' >");
			ptRecords.append("<thead class='bg'>");
			ptRecords.append("<tr>");
			String responseText="";
			ptRecords.append("<th valign='top'>&nbsp;</th>");
			responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblFname", locale),sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo,"prosGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblLname", locale),sortOrderFieldName,"LastName",sortOrderTypeVal,pgNo,"prosGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblEmailAddress", locale),sortOrderFieldName,"emailAddress",sortOrderTypeVal,pgNo,"prosGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
			
			ptRecords.append("</tr>");
			ptRecords.append("</thead>");
			if(teacherList.size()>0){
				for (TeacherDetail teacher : teacherList) {
					Integer teacherId=teacher.getTeacherId();
					String fName=teacher.getFirstName();
					String lName=teacher.getLastName();
					String email=teacher.getEmailAddress();
					
					ptRecords.append("<tr>");
					ptRecords.append("<td><input type='checkbox' id='"+teacherId+"' value='"+teacherId+"' onclick=\"saveParticipant('"+fName+"','"+lName+"','"+email+"','"+0+"','"+teacherId+"' );\"></td>");
					ptRecords.append("<td>"+fName+"</td>");
					ptRecords.append("<td>"+lName+"</td>");
					ptRecords.append("<td>"+email+"</td>");
					ptRecords.append("</tr>");					
				}
				ptRecords.append("</table>" );
				ptRecords.append(PaginationAndSorting.getPaginationStringForParticipantsAjax(request,totalRecord,noOfRow, pageNo,"pageSize2","945","prosGrid"));
			}else{
				ptRecords.append("<tr><td colspan='4'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
				ptRecords.append("</table>" );
			}
			
			
		} catch (Exception e) {
		}
		return ptRecords.toString();
	}*/
	
	
	public String displayTempGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer ptRecords =	new StringBuffer();
		System.out.println("call");
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		try {
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				/*if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				if(userMaster.getDistrictId().getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}*/
				
			}
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"participantLastName";
			String sortOrderNoField		=	"participantLastName";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			
			List<EventParticipantsTemp> eventParticipantsLists = eventParticipantsTempDAO.findListOfParticipants(sortOrderStrVal,start,noOfRowInPage,session.getId());
			System.out.println("MMMMMMMMMMMMMMMMM  "+eventParticipantsLists.size());
			totalRecord= eventParticipantsTempDAO.findListOfParticipantsCount(session.getId()).size();
			
			ptRecords.append("<table  id='tempGrid' width='100%' border='0' >");
			ptRecords.append("<thead class='bg'>");
			ptRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblFname", locale),sortOrderFieldName,"participantFirstName",sortOrderTypeVal,pgNo,"tempGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblLname", locale),sortOrderFieldName,"participantLastName",sortOrderTypeVal,pgNo,"tempGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
			
            responseText=PaginationAndSorting.responseSortingLinkCommonMul("Email Addess",sortOrderFieldName,"participantEmailAddress",sortOrderTypeVal,pgNo,"tempGrid");
			ptRecords.append("<th valign='top'>"+responseText+"</th>");
									
			ptRecords.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("msgErrors3", locale)+"</th>");
			ptRecords.append("</tr>");
			ptRecords.append("</thead>");
			
			if(eventParticipantsLists.size()>0){
				for (EventParticipantsTemp eventParticipantsList : eventParticipantsLists) {
					
					if(eventParticipantsList.getError().equalsIgnoreCase("")){
						ptRecords.append("<tr>");
					}else{
						ptRecords.append("<tr style='background-color:red'>");
					}
					ptRecords.append("<td>"+eventParticipantsList.getParticipantFirstName()+"</td>");
					ptRecords.append("<td>"+eventParticipantsList.getParticipantLastName()+"</td>");
					ptRecords.append("<td>"+eventParticipantsList.getParticipantEmailAddress()+"</td>");
					
					
					ptRecords.append("<td >"+eventParticipantsList.getError()+"</td>");
					ptRecords.append("</tr>");
				}
				ptRecords.append("</table>" );
				ptRecords.append(PaginationAndSorting.getPaginationStringForParticipantsAjax(request,totalRecord,noOfRow, pageNo,"pageSize4","945","tempGrid"));
				
			}else{
				ptRecords.append("<tr><td colspan='4'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
				ptRecords.append("</table>" );
			}
			
			System.out.println("ptRecords :: "+ptRecords.toString());
			
		} catch (Exception e) {
		}
		return ptRecords.toString();
	}
	
	public int addTempProspectCand(String fName,String lName,String emailAddress,Integer eventId){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster =	null;
		DistrictMaster districtMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
	    	userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
	    }
		
		try {
				Criterion criterion = Restrictions.eq("participantEmailAddress", emailAddress);
				EventDetails eventDetails = eventDetailsDAO.findById(eventId, false, false);
				Criterion criterion1 = Restrictions.eq("eventDetails", eventDetails);
				List<EventParticipantsList> eventparticipantsList = eventParticipantsListDAO.findByCriteria(criterion,criterion1);
				List<EventParticipantsList> eventparticipantsList1 = eventParticipantsListDAO.findByCriteria(criterion1);
			if(eventDetails.getEventTypeId().getEventTypeId()==1){
				if(eventparticipantsList1!=null  && eventparticipantsList1.size()>0){
					return 5;
				}else{
					Criterion criteriaforEmail = Restrictions.eq("emailAddress", emailAddress);
					List<TeacherDetail> teacherDetails = teacherDetailDAO.findByCriteria(criteriaforEmail);
					if(teacherDetails!=null && teacherDetails.size()>0){
						EventParticipantsList eventParticipantsList = new EventParticipantsList();
						eventParticipantsList.setParticipantFirstName(fName);
						eventParticipantsList.setParticipantLastName(lName);
						eventParticipantsList.setParticipantEmailAddress(emailAddress);
						eventParticipantsList.setNormScore(0);
						eventParticipantsList.setDistrictMaster(eventDetails.getDistrictMaster());
						
						eventParticipantsList.setHeadQuarterMaster(eventDetails.getHeadQuarterMaster());
						eventParticipantsList.setBranchMaster(eventDetails.getBranchMaster());
						 
						eventParticipantsList.setInvitationEmailSent(false);
						eventParticipantsList.setStatus("A");
						eventParticipantsList.setCreatedBY(userMaster);
						eventParticipantsList.setEventDetails(eventDetails);
						eventParticipantsList.setCreatedDateTime(new Date());
						eventParticipantsList.setTeacherId(teacherDetails.get(0).getTeacherId());
						eventParticipantsListDAO.makePersistent(eventParticipantsList);
						return 2;
					}else{
						//System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
						return 6;
					}
					
				}
			}else{
				if(eventparticipantsList.size()>0){
					return 1;
				}else{
					
					try {
						EventParticipantsList eventParticipantsList = new EventParticipantsList();
						eventParticipantsList.setParticipantFirstName(fName);
						eventParticipantsList.setParticipantLastName(lName);
						eventParticipantsList.setParticipantEmailAddress(emailAddress);
						eventParticipantsList.setNormScore(0);
						eventParticipantsList.setDistrictMaster(eventDetails.getDistrictMaster());
						
						eventParticipantsList.setHeadQuarterMaster(eventDetails.getHeadQuarterMaster());
						eventParticipantsList.setBranchMaster(eventDetails.getBranchMaster());
						
						eventParticipantsList.setInvitationEmailSent(false);
						eventParticipantsList.setStatus("A");
						eventParticipantsList.setCreatedBY(userMaster);
						eventParticipantsList.setEventDetails(eventDetails);
						eventParticipantsList.setCreatedDateTime(new Date());
						eventParticipantsListDAO.makePersistent(eventParticipantsList);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return 2;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public int saveParticipant(String fName,String lName,String emailAddress,Integer nscore,Integer eventId,Integer teacherId){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster =	null;
		DistrictMaster districtMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
	    	userMaster=	(UserMaster) session.getAttribute("userMaster");
	    }
		
		try {
		
			String tempId=session.getId();

				EventDetails eventDetails = eventDetailsDAO.findById(eventId, false, false);
				districtMaster=eventDetails.getDistrictMaster();
				Criterion criterion = Restrictions.eq("participantEmailAddress", emailAddress);
				Criterion criterion1 = Restrictions.eq("eventDetails", eventDetails);
				
				if(eventDetails.getEventTypeId().getEventTypeId()==1){
					List<EventParticipantsList> eventparticipantsList1 = eventParticipantsListDAO.findByCriteria(criterion1);
					if(eventparticipantsList1!=null  && eventparticipantsList1.size()>0 ){
						if(eventDetails.getEventTypeId().getEventTypeId()==1)
						   return 5;
						else
						   return 6;
					}else{
						EventParticipantsList eventParticipantsList = new EventParticipantsList();
						eventParticipantsList.setTeacherId(teacherId);
						eventParticipantsList.setParticipantFirstName(fName);
						eventParticipantsList.setParticipantLastName(lName);
						eventParticipantsList.setParticipantEmailAddress(emailAddress);
						eventParticipantsList.setNormScore(nscore);
						eventParticipantsList.setDistrictMaster(districtMaster);
						eventParticipantsList.setInvitationEmailSent(false);
						eventParticipantsList.setStatus("A");
						eventParticipantsList.setCreatedBY(userMaster);
						eventParticipantsList.setEventDetails(eventDetails);
						eventParticipantsList.setCreatedDateTime(new Date());
						eventParticipantsListDAO.makePersistent(eventParticipantsList);
						return 2;
				       }
				}else{
					  List<EventParticipantsList> eventparticipantsList = eventParticipantsListDAO.findByCriteria(criterion,criterion1);
					  if(eventparticipantsList.size()>0){
						return 1;
					}else{
						EventParticipantsList eventParticipantsList = new EventParticipantsList();
						eventParticipantsList.setTeacherId(teacherId);
						eventParticipantsList.setParticipantFirstName(fName);
						eventParticipantsList.setParticipantLastName(lName);
						eventParticipantsList.setParticipantEmailAddress(emailAddress);
						eventParticipantsList.setNormScore(nscore);
						eventParticipantsList.setDistrictMaster(districtMaster);
						eventParticipantsList.setInvitationEmailSent(false);
						eventParticipantsList.setStatus("A");
						eventParticipantsList.setCreatedBY(userMaster);
						eventParticipantsList.setEventDetails(eventDetails);
						eventParticipantsList.setCreatedDateTime(new Date());
						eventParticipantsListDAO.makePersistent(eventParticipantsList);
						return 2;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public int delParticipant(Integer parId){
		//System.out.println("call delete");
		try {
			EventParticipantsList eventParticipantsList = eventParticipantsListDAO.findById(parId, false, false);
			eventParticipantsListDAO.makeTransient(eventParticipantsList);
			return 1;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return 0;
	}
	
	private Vector vectorDataExcelXLSX = new Vector();
	/*public String saveImportList(String fileName){
		String[] arr = new String[2];
		
		
		 if(fileName.contains(".xlsx")){
    		 vectorDataExcelXLSX = readDataExcelXLSX(filePath);
    	 }else{
    		 vectorDataExcelXLSX = readDataExcelXLS(filePath);
    	 }
		return "";
	}*/
	
	public String[] saveImportList(String fileName,String sessionId,int eventId)
	{
	      /* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster =	null;
		DistrictMaster districtMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
	    	userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
	    }
	      System.out.println("::::::::saveFacilitator:::::::::");
	      String[] returnVal=new String[2];
	      String tempId=session.getId();
	    try{
	       String root = request.getRealPath("/")+"/facilitatoruploads/";
	       String filePath=root+fileName;
	       System.out.println("filePath :"+filePath);
	       if(fileName.contains(".xlsx")){
	             vectorDataExcelXLSX = readDataExcelXLSX(filePath);
	       }else{
	             vectorDataExcelXLSX = readDataExcelXLS(filePath);
	       }
	       
	       Map<String, Boolean> participantMap = new HashMap<String, Boolean>();
	       Map<Integer, String[]>  tempMap = new HashMap<Integer, String[]>();
	       EventDetails eventDetails = eventDetailsDAO.findById(eventId, false, false);
	       Criterion criterion = Restrictions.eq("eventDetails", eventDetails);
	       
	       List<EventParticipantsList> participantsLists = eventParticipantsListDAO.findByCriteria(criterion);
	       	       	
	       	if(participantsLists!=null && participantsLists.size()>0){
	       		for (EventParticipantsList eventParticipantsList : participantsLists) {
	       			participantMap.put(eventParticipantsList.getParticipantEmailAddress(), true);
				}
	       	}
	       
	        	int count=0;
	            int firstnameCcount=11;
	            int lastnameCcount=11;
	            int emailaddressCcount=11;
	            
	            boolean erorrInFile=false;
	            boolean row_error=false;
	            String rowErrorText="Field(s) ";
	            for(int A=0; A<vectorDataExcelXLSX.size(); A++) {
					Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(A);
					String firstName="";
					String lastName="";
					String emailAddress="";
					for(int j=0; j<vectorCellEachRowData.size(); j++) {

						if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("participantFirstName")){
							firstnameCcount=j;
						}
						if(firstnameCcount==j)
							firstName=vectorCellEachRowData.get(j).toString().trim();
						
						if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("participantlastName")){
							lastnameCcount=j;
						}
						if(lastnameCcount==j)
							lastName=vectorCellEachRowData.get(j).toString().trim();
						
						if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("participantEmailAddress")){
							emailaddressCcount=j;
						}
						if(emailaddressCcount==j)
							emailAddress=vectorCellEachRowData.get(j).toString().trim();
					}
					/*============process start ============*/
						System.out.println();	
						String error="";
					if(A==0){
						
						if(firstName.equals("") || !firstName.equals("participantFirstName")){
							row_error=true;
							rowErrorText+="participantFirstName";							
						}
						if(lastName.equals("") || !lastName.equals("participantlastName")){
							if(row_error==true){
								rowErrorText+=", ";
							}							
							rowErrorText+="participantLastName";
							
							
						}
						if(emailAddress.equals("") || !emailAddress.equals("participantEmailAddress")){
							if(row_error==true){
								rowErrorText+=", ";
							}
							rowErrorText+="participantEmailAddress";
						}

						if(row_error){ rowErrorText+=" does not found"; break; }
						//
					}
					
					if(A!=0){
						String errorText="";
						if(firstName.equalsIgnoreCase(""))
			            {
			                 
			                  errorText+=Utility.getLocaleValuePropByKey("msgFirstNameblank", locale);
;
			            }
			            
			            
			            if(lastName.equalsIgnoreCase(""))
			            {
			            	if(!errorText.equalsIgnoreCase("")){
			                  errorText+=" </br>";
			            	}
			                  errorText+=Utility.getLocaleValuePropByKey("msgLastNameblank", locale);
			            }
			            
			             String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
			             Boolean b = emailAddress.matches(EMAIL_REGEX);
			             //System.out.println(A+" ==  "+firstName+" || "+lastName+" || "+emailAddress);
			             
			             if(emailAddress.equalsIgnoreCase(""))
			             {
			            	 if(!errorText.equalsIgnoreCase("")){
				                  errorText+=" </br>";
				            	}
			                   errorText+="Email Address can't be blank";
			             }
			              
			             else if(!b)
			             {
			            	 if(!errorText.equalsIgnoreCase("")){
				                  errorText+=" </br>";
				            	}
			                   errorText+=Utility.getLocaleValuePropByKey("msgEmailAddressnotvalid", locale); 
			             }     
			             else if(participantMap.containsKey(emailAddress) && participantMap.get(emailAddress).equals(true))
			             {
			            	 if(!errorText.equalsIgnoreCase("")){
				                  errorText+=" </br>";
				            	}
			                   errorText+=Utility.getLocaleValuePropByKey("msgEmailAddressAlreadyExists", locale);      
			             }   
			             
			             if(!errorText.equalsIgnoreCase("")){
			            	 erorrInFile=true;
			            	 System.out.println("error   "+A);
			             }
			             String[] arr = new String[4];
			             arr[0]=firstName;
			             arr[1]=lastName;
			             arr[2]=emailAddress;
			             arr[3]=errorText;
			        // System.out.println(firstName+" || "+lastName+" "+emailAddress);    
			             tempMap.put(A, arr);
					}
					/*============process start end============*/
				}
	          System.out.println("--------------------------fianl list ----------------   "+erorrInFile);
	          
	          Integer delete = eventParticipantsTempDAO.deleterecords(tempId);
	            if(row_error){
	            	returnVal[0]="rowError";
	            	returnVal[1]=rowErrorText;
	            }else{
	            	if(tempMap.size()>0){
	            		for (Entry<Integer, String[]> entry : tempMap.entrySet()){
	          				String[] arr = entry.getValue(); 
	          
	          				EventParticipantsTemp eventParticipantsTemp = new EventParticipantsTemp();
	          				eventParticipantsTemp.setParticipantFirstName(arr[0]);
	          				eventParticipantsTemp.setParticipantLastName(arr[1]);
	    					eventParticipantsTemp.setParticipantEmailAddress(arr[2]);
	    					eventParticipantsTemp.setError(arr[3]);
	    					eventParticipantsTemp.setTempId(tempId);
	    					eventParticipantsTempDAO.makePersistent(eventParticipantsTemp);
	        	          }
	            	}
	            	returnVal[0]="recError";
	            	returnVal[1]=sessionId;
	            }/*else{	            		            	
	            	if(tempMap.size()>0){
	            		for (Entry<Integer, String[]> entry : tempMap.entrySet()){
	          				String[] arr = entry.getValue();
	          	
	          				EventParticipantsList eventParticipantsList = new EventParticipantsList();
	    					eventParticipantsList.setParticipantFirstName(arr[0]);
	    					eventParticipantsList.setParticipantLastName(arr[1]);
	    					eventParticipantsList.setParticipantEmailAddress(arr[2]);
	    					eventParticipantsList.setNormScore(0);
	    					eventParticipantsList.setDistrictMaster(districtMaster);
	    					eventParticipantsList.setInvitationEmailSent(false);
	    					eventParticipantsList.setStatus("A");
	    					eventParticipantsList.setCreatedBY(userMaster);
	    					eventParticipantsList.setEventDetails(eventDetails);
	    					eventParticipantsList.setCreatedDateTime(new Date());
	    					eventParticipantsListDAO.makePersistent(eventParticipantsList);
	        	          }
	            	}
	            	returnVal[0]="success";
	            	returnVal[1]=sessionId;
	            }*/
	      }catch (Exception e){
	            e.printStackTrace();
	      }
	      
	      return returnVal;
	}

	public int acceptImport(Integer eventId){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster =	null;
		DistrictMaster districtMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
	    	userMaster=	(UserMaster) session.getAttribute("userMaster");
			/*if(userMaster.getRoleId().getRoleId()!=null){
				districtMaster=userMaster.getDistrictId();
			}*/
	    }
		
		try {
			EventDetails eventDetails = eventDetailsDAO.findById(eventId, false, false);
			Criterion criterion = Restrictions.eq("tempId", session.getId());
			Criterion criterion2 = Restrictions.eq("error","");
			List<EventParticipantsTemp> eventparticipantsTemp = eventParticipantsTempDAO.findByCriteria(criterion,criterion2);

			for (EventParticipantsTemp eventParticipantsTemp2 : eventparticipantsTemp) 
			{
				try {
					EventParticipantsList eventParticipantsList = new EventParticipantsList();
					eventParticipantsList.setParticipantFirstName(eventParticipantsTemp2.getParticipantFirstName());
					eventParticipantsList.setParticipantLastName(eventParticipantsTemp2.getParticipantLastName());
					eventParticipantsList.setParticipantEmailAddress(eventParticipantsTemp2.getParticipantEmailAddress());
					eventParticipantsList.setNormScore(0);
					eventParticipantsList.setDistrictMaster(eventDetails.getDistrictMaster());
					
					eventParticipantsList.setHeadQuarterMaster(eventDetails.getHeadQuarterMaster());
					eventParticipantsList.setBranchMaster(eventDetails.getBranchMaster());
					
					eventParticipantsList.setInvitationEmailSent(false);
					eventParticipantsList.setStatus("A");
					eventParticipantsList.setCreatedBY(userMaster);
					eventParticipantsList.setEventDetails(eventDetails);
					eventParticipantsList.setCreatedDateTime(new Date());
					eventParticipantsListDAO.makePersistent(eventParticipantsList);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			clearAllTempRec(eventDetails.getEventId());
			return 2;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return 1;
	}
	
	public int clearAllTempRec(Integer eventId){
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster =	null;
		DistrictMaster districtMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
	    	userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
	    }
		try {
			 Integer delete = eventParticipantsTempDAO.deleterecords(session.getId());
			 return 2;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return 1;
	}
	
	public static Vector readDataExcelXLS(String fileName) throws IOException {

		System.out.println(":::::::::::::::::readDataExcel XLS::::::::::::");
		Vector vectorData = new Vector();
		
		try{
			FileInputStream ExcelFileToRead = new FileInputStream(fileName);
			HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);
	 
			HSSFSheet sheet=wb.getSheetAt(0);
			HSSFRow row; 
			HSSFCell cell;
	 
			Iterator rows = sheet.rowIterator();
		   	int participantFirstName_count=11;
	    	int participantLastName_count=11;
	    	int participantEmailAddress_count=11;
	    	
	    	
	       String indexCheck="";
			 
			while (rows.hasNext()){
				row=(HSSFRow) rows.next();
				Iterator cells = row.cellIterator();
				Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false;
				String indexPrev="";
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
				while (cells.hasNext())
				{
					cell=(HSSFCell) cells.next();
					cIndex=cell.getColumnIndex();
								  
					
					
					if(cell.toString().equalsIgnoreCase("participantFirstName")){
						participantFirstName_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(participantFirstName_count==cIndex){
						cellFlag=true;
					}
					
					
					if(cell.toString().equalsIgnoreCase("participantLastName")){
						participantLastName_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(participantLastName_count==cIndex){
						cellFlag=true;
					}
					
				   if(cell.toString().equalsIgnoreCase("participantEmailAddress")){
					   participantEmailAddress_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(participantEmailAddress_count==cIndex){
						cellFlag=true;
					}					
					
					if(cellFlag){
						
	            			mapCell.put(Integer.valueOf(cIndex),cell+"");
	            		}
					}
					cellFlag=false;
			
			    vectorCellEachRowData=cellValuePopulate(mapCell);
				vectorData.addElement(vectorCellEachRowData);
			}
		}catch(Exception e){}
		return vectorData;
	}

	public static Vector readDataExcelXLSX(String fileName) {
		System.out.println(":::::::::::::::::readDataExcel XLSX ::::::::::::");
	    Vector vectorData = new Vector();
	    try {
	        FileInputStream fileInputStream = new FileInputStream(fileName);
	        XSSFWorkbook xssfWorkBook = new XSSFWorkbook(fileInputStream);
	        // Read data at sheet 0
	        XSSFSheet xssfSheet = xssfWorkBook.getSheetAt(0);
	        Iterator rowIteration = xssfSheet.rowIterator();
	       	
	    	int participantFirstName_count=11;
	    	int participantLastName_count=11;
	    	int participantEmailAddress_count=11;
	    	int schoolId_count=11;
	    	int invitationEmailSent_count=11;
	    	
	        // Looping every row at sheet 0
	        String indexCheck="";
	        while (rowIteration.hasNext()) {
	            XSSFRow xssfRow = (XSSFRow) rowIteration.next();
	            Iterator cellIteration = xssfRow.cellIterator();
	            Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false;
				String indexPrev="";
	            // Looping every cell in each row at sheet 0
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
	            while (cellIteration.hasNext()){
	                XSSFCell xssfCell = (XSSFCell) cellIteration.next();
	                cIndex=xssfCell.getColumnIndex();
	                
	                   	
	            	if(xssfCell.toString().equalsIgnoreCase("participantFirstName")){
	            		participantFirstName_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(participantFirstName_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("participantLastName")){
	            		participantLastName_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(participantLastName_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("participantEmailAddress")){
	            		participantEmailAddress_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(participantEmailAddress_count==cIndex){
	            		cellFlag=true;
	            	}	            	
	            	
	            	if(cellFlag){
	            	
	            			mapCell.put(Integer.valueOf(cIndex),xssfCell+"");
	            		
	            	}
	            	  	cellFlag=false;
	            }
	            vectorCellEachRowData=cellValuePopulate(mapCell);
	            vectorData.addElement(vectorCellEachRowData);
	        }
	    } catch (Exception ex) {
	        ex.printStackTrace();
	    }
	     
	    return vectorData;
	}

	public static Vector cellValuePopulate(Map<Integer,String> mapCell){
		 Vector vectorCellEachRowData = new Vector();
		 Map<Integer,String> mapCellTemp = new TreeMap<Integer, String>();
		 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false;
		 for(Map.Entry<Integer, String> entry : mapCell.entrySet()){
			int key=entry.getKey();
			String cellValue=null;
			if(entry.getValue()!=null)
				cellValue=entry.getValue().trim();
			
			if(key==0){
				mapCellTemp.put(key, cellValue);
				flag0=true;
			}
			if(key==1){
				flag1=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==2){
				flag2=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==3){
				flag3=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==4){
				flag4=true;
				mapCellTemp.put(key, cellValue);
			}
		 }
		 if(flag0==false){
			 mapCellTemp.put(0, "");
		 }
		 if(flag1==false){
			 mapCellTemp.put(1, "");
		 }
		 if(flag2==false){
			 mapCellTemp.put(2, "");
		 }
		 if(flag3==false){
			 mapCellTemp.put(3, "");
		 }
		 if(flag4==false){
			 mapCellTemp.put(4, "");
		 }
		 for(Map.Entry<Integer, String> entry : mapCellTemp.entrySet()){
			 vectorCellEachRowData.addElement(entry.getValue());
		 }
		 return vectorCellEachRowData;
	}
 public int checkCandidates(int eventId)
 {
	  	 EventDetails eventDetails=eventDetailsDAO.findById(eventId,false,false);
		 Criterion criterion=Restrictions.eq("eventDetails",eventDetails);
		 return eventParticipantsListDAO.getRowCount(criterion);
 }
 public boolean sendEmail(int eventId)
 {
	 return callSendEmail(eventId,null);
 }
 public boolean sendEmailWithTeacherId(int eventId,String teacherIds)
 {
	// System.out.println("eventId=="+eventId+"teacherIds==="+teacherIds);
	 return callSendEmail(eventId,teacherIds.split(","));
 }
private boolean callSendEmail(int eventId,String[] teacherIds)
{	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	UserMaster userMaster =	null;
	//Statales session for Log
	SessionFactory factoryForMsg=teacherDetailDAO.getSessionFactory();
	StatelessSession statelessSessionForMsg=factoryForMsg.openStatelessSession();
	Transaction transactionForMsg=statelessSessionForMsg.beginTransaction();
	//DistrictMaster districtMaster=null;	
	boolean sendmailFlag=false;
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }else{
    	userMaster=	(UserMaster) session.getAttribute("userMaster");
    }
	try
	{
		
	EventDetails eventdetails=eventDetailsDAO.findById(eventId,false,false);
	Criterion crt=Restrictions.eq("eventDetails", eventdetails);	
	System.out.println("eventScheduleId ..."+ eventId);
	List<EventSchedule> eventschlist=eventScheduleDAO.findByCriteria(crt);
	List<EventFacilitatorsList> eventFacilitatorsList=eventFacilitatorsListDAO.findByCriteria(crt);
	List<EventParticipantsList> eventParticipantsLst=eventParticipantsListDAO.findByCriteria(crt);
	List<TeacherDetail> teacherIdsLst=null;
	Map<String, String>  teacherIdMap=new HashMap<String, String>();
	List<EventParticipantsList>  eventListForRetain=new ArrayList<EventParticipantsList>();
	
	System.out.println("eventParticipantsLst==========>>>>>>>>>>>>>>"+eventParticipantsLst.size());
	
	List<Integer> teacherIdList = new ArrayList<Integer>();
	try
	{
	   if(teacherIds!=null){
		  for(String tId:teacherIds)
			{
			   teacherIdList.add(Integer.parseInt(tId));
			}
	   }
	 }catch (Exception e) {
		e.printStackTrace();
	 }
		
	
	if(teacherIdList!=null && teacherIdList.size()>0)
		teacherIdsLst=teacherDetailDAO.findByTeacherIds(teacherIdList);
	
	if(teacherIdsLst!=null && teacherIdsLst.size()>0)
	{
		for(TeacherDetail tids:teacherIdsLst)
		{
			teacherIdMap.put(tids.getEmailAddress(), tids.getEmailAddress());			
		}		
	}
	for(EventParticipantsList eventList:eventParticipantsLst)
	{
		if(teacherIdMap.containsKey(eventList.getParticipantEmailAddress()))
		{
			eventListForRetain.add(eventList);
		}		
	}
	
	if(eventListForRetain!=null && eventListForRetain.size()>0)
	{
		eventParticipantsLst.retainAll(eventListForRetain);
	}
	
	//creating Map for mentaining logs......
	Map<String,TeacherDetail> teachersMapForLog= new HashMap<String, TeacherDetail>();
	List<String> participantsemails= new ArrayList<String>();
	for(EventParticipantsList epl :  eventParticipantsLst){
		participantsemails.add(epl.getParticipantEmailAddress());
	}
	List<TeacherDetail> listTeacherDetails =teacherDetailDAO.findByEmails(participantsemails);
	if(listTeacherDetails!=null && listTeacherDetails.size()>0) {
		for(TeacherDetail teacher: listTeacherDetails){
			  teachersMapForLog.put(teacher.getEmailAddress().trim(), teacher);
		  }
	}
	
	
		String distrctOrSchoolName="";
		String districtOrSchoolAdd=getAddressDistHeadBranch(eventdetails);
		if(eventdetails.getDistrictMaster()!=null){
			if(eventdetails.getCreatedBY().getEntityType()==3){
				distrctOrSchoolName=eventdetails.getCreatedBY().getSchoolId()==null?"":eventdetails.getCreatedBY().getSchoolId().getSchoolName();
			}else{
				distrctOrSchoolName=eventdetails.getDistrictMaster().getDistrictName();	
			}
			
		}else if(eventdetails.getBranchMaster()!=null){
			distrctOrSchoolName=eventdetails.getBranchMaster().getBranchName();
		}else if(eventdetails.getHeadQuarterMaster()!=null){
			distrctOrSchoolName=eventdetails.getHeadQuarterMaster().getHeadQuarterName();
		}
		
	
	if(eventdetails.getEventTypeId().getEventTypeId()!=1)
	{
	if(eventschlist!=null && eventschlist.size()>0 && eventFacilitatorsList!=null && eventFacilitatorsList.size()>0)
	{
		sendmailFlag=true;
		String link="",qrcode="",forMated="";
		String baseURL=Utility.getValueOfPropByKey("basePath");
		String mailid="";
		//Logo Path
		String  path="";
		String fileName= eventdetails.getLogo();
		
		if(fileName!=null){
			String source="";
			String target="";
			
			if(eventdetails.getDistrictMaster()!=null){
				source = Utility.getValueOfPropByKey("districtRootPath")+""+eventdetails.getDistrictMaster().getDistrictId()+"/Event/"+eventdetails.getEventId()+"/"+fileName;
			}
			else if(eventdetails.getHeadQuarterMaster()!=null){
				source = Utility.getValueOfPropByKey("headQuarterRootPath")+""+eventdetails.getHeadQuarterMaster().getHeadQuarterId()+"/Event/"+eventdetails.getEventId()+"/"+fileName;
			}
			
			//source = Utility.getValueOfPropByKey("districtRootPath")+""+eventdetails.getDistrictMaster().getDistrictId()+"/Event/"+eventdetails.getEventId()+"/"+fileName;
			
			if(eventdetails.getDistrictMaster()!=null){
				target = context.getServletContext().getRealPath("/")+"/"+"/Event/"+eventdetails.getDistrictMaster().getDistrictId()+"/"+eventdetails.getEventId()+"/"+"OptimizeImage/";
			}else if(eventdetails.getHeadQuarterMaster()!=null){
				target = context.getServletContext().getRealPath("/")+"/"+"/Event/"+eventdetails.getHeadQuarterMaster().getHeadQuarterId()+"/"+eventdetails.getEventId()+"/"+"OptimizeImage/";
			}
			//target = context.getServletContext().getRealPath("/")+"/"+"/Event/"+eventdetails.getDistrictMaster().getDistrictId()+"/"+eventdetails.getEventId()+"/"+"OptimizeImage/";
			
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();
	
			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
			ImageResize.resizeImage(targetFile.toString());
			
			if(eventdetails.getDistrictMaster()!=null){
				 path = Utility.getBaseURL(request)+"Event/"+eventdetails.getDistrictMaster().getDistrictId()+"/"+eventdetails.getEventId()+"/OptimizeImage/"+sourceFile.getName();
			}else if(eventdetails.getHeadQuarterMaster()!=null){
				 path = Utility.getBaseURL(request)+"Event/"+eventdetails.getHeadQuarterMaster().getHeadQuarterId()+"/"+eventdetails.getEventId()+"/OptimizeImage/"+sourceFile.getName();
			}
				// path = Utility.getBaseURL(request)+"Event/"+eventdetails.getDistrictMaster().getDistrictId()+"/"+eventdetails.getEventId()+"/OptimizeImage/"+sourceFile.getName();
		}	
		if(path!=null && path.length()>0)
			path="<img src="+path+">";
		System.out.println("path :: "+path);
		//Logo Path
		String content="";
		String eventIdes=Utility.encryptNo(eventdetails.getEventId());
		String eventIds = Utility.encodeInBase64(eventIdes);
		
		// String 
		String eventSubjectforFacilator=eventdetails.getSubjectforFacilator();
		String eventSubjectforParticipants=eventdetails.getSubjectforParticipants();
		
		String eventSubject=eventdetails.getEventName();				
		String desc=eventdetails.getDescription();
		String messageToParticipants=eventdetails.getMsgtoparticipants();		
		String[] glink = null;
		String calFileName="";		

		//Create ICS File 
		if(eventdetails.getEventTypeId().getEventTypeName().equalsIgnoreCase("Networking") || eventdetails.getEventTypeId().getEventTypeName().equalsIgnoreCase("Job Fair"))
		{

			calFileName= createIcalFile(eventSubject, desc,eventschlist);			
			for(EventFacilitatorsList sender:eventFacilitatorsList)
			{
				try{
					content=path+"<br><br>"+MailText.mailTextForEvents(eventdetails.getMsgtofacilitators());				
					//content=content+"<br><br>\n"+glink[1];				
					String email=sender.getFacilitatorEmailAddress();
					
					if(content.contains("&lt; First Name &gt;") || content.contains("&lt; Last Name &gt;") || content.contains("&lt; Email &gt;") || content.contains("&lt; Job Title &gt;") || content.contains("&lt; District or School Name &gt;") || content.contains("&lt; District or School Address &gt;"))
					{
						if(eventdetails.getJobOrder()!=null)
							  content	=	content.replaceAll("&lt; Job Title &gt;",eventdetails.getJobOrder().getJobTitle());	
						
						content	=	content.replaceAll("&lt; First Name &gt;",sender.getFacilitatorFirstName());
						content	=	content.replaceAll("&lt; Last Name &gt;",sender.getFacilitatorLastName());
						content	=	content.replaceAll("&lt; Email &gt;",sender.getFacilitatorEmailAddress());
						content	=	content.replaceAll("&lt; District or School Name &gt;",distrctOrSchoolName);
						content	=	content.replaceAll("&lt; District or School Address &gt;",districtOrSchoolAdd);
					}
					
					System.out.println("content "+content);
					emailerService.sendMailWithAttachments(email, eventSubjectforFacilator, "noreply@teachermatch.net", content,calFileName.toString());
				}catch(Exception e)
				{e.printStackTrace();}
			}			
			if(eventParticipantsLst!=null && eventParticipantsLst.size() >0)
			{
				for(EventParticipantsList sender:eventParticipantsLst)
				{
				try{
					
					mailid=(sender.getParticipantEmailAddress());
					String linkIds= eventIds+"###"+mailid;
					forMated = Utility.encodeInBase64(linkIds);
					forMated = baseURL+"cmssignup.do?id="+forMated;
					createQRCode(forMated,"candqrcode"+session.getId()+".png",request);				
					qrcode="<img src='"+Utility.getBaseURL(request)+"/qrcode/candqrcode"+session.getId()+".png"+"'style=''/>";
					String tinnyUrl =	Utility.getShortURL(forMated);
					link=Utility.getLocaleValuePropByKey("msgToRegisterForThisEvent", locale)+" <br>"+qrcode+"<br>"+Utility.getLocaleValuePropByKey("msgToRegisterForThisEventl2", locale)+"  <a href='"+tinnyUrl+"'>"+tinnyUrl+"</a>";			
											
					content="";
					content=path+"<br><br>"+MailText.mailTextForEvents(messageToParticipants)+"<br><br>"+link;
					
					//Logic changing into dynamic content........
					if(content.contains("&lt; First Name &gt;") || content.contains("&lt; Last Name &gt;") || content.contains("&lt; Email &gt;") || content.contains("&lt; Job Title &gt;") || content.contains("&lt; District or School Name &gt;") || content.contains("&lt; District or School Address &gt;"))
					{
						if(eventdetails.getJobOrder()!=null)
							  content	=	content.replaceAll("&lt; Job Title &gt;",eventdetails.getJobOrder().getJobTitle());	
						
						content	=	content.replaceAll("&lt; First Name &gt;",sender.getParticipantFirstName());
						content	=	content.replaceAll("&lt; Last Name &gt;",sender.getParticipantLastName());
						content	=	content.replaceAll("&lt; Email &gt;",sender.getParticipantEmailAddress());
						content	=	content.replaceAll("&lt; District or School Name &gt;",distrctOrSchoolName);
						content	=	content.replaceAll("&lt; District or School Address &gt;",districtOrSchoolAdd);
					}
					
					System.out.println("content :: "+ content);
					
					emailerService.sendMailWithAttachments(mailid, eventSubjectforParticipants, "noreply@teachermatch.net", content,calFileName.toString());
					 
					TeacherDetail teacherDetail= teachersMapForLog.get(mailid.trim());
					if(teacherDetail!=null){
						MessageToTeacher  messageToTeacher= new MessageToTeacher();
						messageToTeacher.setTeacherId(teacherDetail);
						//messageToTeacher.setJobId(jobOrder);
						messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
						messageToTeacher.setSenderId(userMaster);
						messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
						messageToTeacher.setEntityType(userMaster.getEntityType());
						messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
						messageToTeacher.setMessageSubject(eventSubjectforParticipants);
						messageToTeacher.setMessageSend(content);
						
						statelessSessionForMsg.insert(messageToTeacher);
						
					}
				
				}catch(Exception e)
				{e.printStackTrace();}
				}
			}
						
		}
		else
		{
			calFileName= createIcalFile(eventSubject, desc,eventschlist);
			if(eventschlist.size()>1)
			{
				for(EventFacilitatorsList sender:eventFacilitatorsList)
				{
					try{										
						String email=sender.getFacilitatorEmailAddress();	
						String messageToFacilitators =eventdetails.getMsgtofacilitators();
						content="";
						content=path+"<br><br>"+MailText.mailTextForEvents(messageToFacilitators);	
						System.out.println("content :: "+content);
						
						if(content.contains("&lt; First Name &gt;") || content.contains("&lt; Last Name &gt;") || content.contains("&lt; Email &gt;") || content.contains("&lt; Job Title &gt;") || content.contains("&lt; District or School Name &gt;") || content.contains("&lt; District or School Address &gt;"))
						{
							if(eventdetails.getJobOrder()!=null)
								  content	=	content.replaceAll("&lt; Job Title &gt;",eventdetails.getJobOrder().getJobTitle());	
							
							content	=	content.replaceAll("&lt; First Name &gt;",sender.getFacilitatorFirstName());
							content	=	content.replaceAll("&lt; Last Name &gt;",sender.getFacilitatorLastName());
							content	=	content.replaceAll("&lt; Email &gt;",sender.getFacilitatorEmailAddress());
							content	=	content.replaceAll("&lt; District or School Name &gt;",distrctOrSchoolName);
							content	=	content.replaceAll("&lt; District or School Address &gt;",districtOrSchoolAdd);
						}
						System.out.println("content "+content);
						//emailerService.sendMailAsHTMLText(email, eventSubjectforFacilator, content, "noreply@teachermatch.net");	
						emailerService.sendMailWithAttachments(email, eventSubjectforFacilator, "noreply@teachermatch.net", content,calFileName.toString());
					}catch(Exception e)
					{e.printStackTrace();}
				}
			}
			else
			{		
				for(EventFacilitatorsList sender:eventFacilitatorsList)
				{
					try{					
						content=path+"<br><br>"+MailText.mailTextForEvents(eventdetails.getMsgtofacilitators());
						String email=sender.getFacilitatorEmailAddress();
						
						if(content.contains("&lt; First Name &gt;") || content.contains("&lt; Last Name &gt;") || content.contains("&lt; Email &gt;") || content.contains("&lt; Job Title &gt;") || content.contains("&lt; District or School Name &gt;") || content.contains("&lt; District or School Address &gt;"))
						{
							if(eventdetails.getJobOrder()!=null)
								  content	=	content.replaceAll("&lt; Job Title &gt;",eventdetails.getJobOrder().getJobTitle());	
							
							content	=	content.replaceAll("&lt; First Name &gt;",sender.getFacilitatorFirstName());
							content	=	content.replaceAll("&lt; Last Name &gt;",sender.getFacilitatorLastName());
							content	=	content.replaceAll("&lt; Email &gt;",sender.getFacilitatorEmailAddress());
							content	=	content.replaceAll("&lt; District or School Name &gt;",distrctOrSchoolName);
							content	=	content.replaceAll("&lt; District or School Address &gt;",districtOrSchoolAdd);
						}
						
						emailerService.sendMailWithAttachments(email, eventSubjectforFacilator, "noreply@teachermatch.net", content,calFileName.toString());
					}catch(Exception e)
					{e.printStackTrace();}
				}
			}
			if(eventParticipantsLst!=null && eventParticipantsLst.size() >0)
			{
				
				if(eventschlist.size()>1)
				{
					Map<String,EventParticipantsList> mapofNewCandidate = new HashMap<String, EventParticipantsList>();
					Map<String,String> mapofNewCandidateForMail = new HashMap<String, String>();
					Map<String,TeacherDetail> registeredTeacherList= new HashMap<String, TeacherDetail>();
					
					if(listTeacherDetails!=null && listTeacherDetails.size()>0) 
					  for(TeacherDetail teacher: listTeacherDetails){
							registeredTeacherList.put(teacher.getEmailAddress(), teacher);
					  }
					for(EventParticipantsList epl :  eventParticipantsLst){
						if(registeredTeacherList.get(epl.getParticipantEmailAddress())==null){
							mapofNewCandidate.put(epl.getParticipantEmailAddress(), epl);
							//newTeacherList.add(epl.getParticipantEmailAddress());
						}
					}
					SessionFactory factory=teacherDetailDAO.getSessionFactory();
					StatelessSession statelessSession=factory.openStatelessSession();
					Transaction transaction=statelessSession.beginTransaction();
					
					 for(Map.Entry<String, EventParticipantsList> entry : mapofNewCandidate.entrySet()){
						 EventParticipantsList participants=entry.getValue();
						 int authorizationkey=(int) Math.round(Math.random() * 2000000);
						 
							TeacherDetail teacherDetail=new  TeacherDetail();  
							String uniquepass=Utility.randomString(8);
							String firstName=participants.getParticipantFirstName()==null?"":participants.getParticipantFirstName();
							String lastName=participants.getParticipantLastName()==null?"":participants.getParticipantLastName();
							String emailAddress=participants.getParticipantEmailAddress()==null?"":participants.getParticipantEmailAddress();
							
						    teacherDetail.setFirstName(firstName);
						    teacherDetail.setLastName(lastName);
						    teacherDetail.setEmailAddress(emailAddress);
							teacherDetail.setUserType("N");
							teacherDetail.setPassword(MD5Encryption.toMD5(uniquepass));	
							teacherDetail.setFbUser(false);
							teacherDetail.setQuestCandidate(0);
							teacherDetail.setAuthenticationCode(""+authorizationkey);
							teacherDetail.setVerificationCode(""+authorizationkey);
							teacherDetail.setVerificationStatus(1);
							teacherDetail.setIsPortfolioNeeded(true);
							teacherDetail.setNoOfLogin(0);
							teacherDetail.setStatus("A");
							teacherDetail.setSendOpportunity(false);
							teacherDetail.setIsResearchTeacher(false);
							teacherDetail.setForgetCounter(0);
							teacherDetail.setCreatedDateTime(new Date());
							teacherDetail.setIpAddress(IPAddressUtility.getIpAddress(request));
							teacherDetail.setInternalTransferCandidate(false);
							statelessSession.insert(teacherDetail);
							mapofNewCandidateForMail.put(emailAddress, uniquepass);
					 }
			
					 transaction.commit();
					 statelessSession.close();
					
					/*for(Map.Entry<String, String> entry : mapofNewCandidateForMail.entrySet()){
						System.out.println(entry.getKey() +" @@@@@@@@@@@@ "+entry.getValue());
					}*/
					
					
					//System.out.println("Total Slotsss="+eventschlist.size());
					for(EventParticipantsList sender:eventParticipantsLst)
					{
						//String password=mapofNewCandidateForMail.get(sender.getParticipantEmailAddress());
						String password  =	mapofNewCandidateForMail.get(sender.getParticipantEmailAddress())==null?"":mapofNewCandidateForMail.get(sender.getParticipantEmailAddress());
						
						mailid=(sender.getParticipantEmailAddress());
						System.out.println(mailid);	
						
						String linkIds= eventId+"###"+mailid;
						forMated = Utility.encodeInBase64(linkIds);
						forMated = baseURL+"slotselection.do?id="+forMated;
						System.out.println("forMatedddddddddddd "+forMated);
						String slotSelectionLInk="<a href='"+forMated+"'> click here to login for slot selection</a>";
						if(password!="" &&password.length()>0){
							slotSelectionLInk+="<br><br> Login Id:&nbsp;"+mailid+"<br> "+Utility.getLocaleValuePropByKey("lblPass", locale)+":&nbsp;"+password;
						}
						
						content="";
						content=path+"<br><br>"+MailText.mailTextForEvents(messageToParticipants)+"<br><span style='font-size: 20px;'>"+slotSelectionLInk+"</span>";
						System.out.println("CCCCCCCCCCCCCCCCCC  content :: "+content);
						System.out.println("slotSelectionLInk:: "+slotSelectionLInk);
						
						if(content.contains("&lt; First Name &gt;") || content.contains("&lt; Last Name &gt;") || content.contains("&lt; Email &gt;") || content.contains("&lt; Job Title &gt;") || content.contains("&lt; District or School Name &gt;") || content.contains("&lt; District or School Address &gt;"))
						{
							if(eventdetails.getJobOrder()!=null)
								  content	=	content.replaceAll("&lt; Job Title &gt;",eventdetails.getJobOrder().getJobTitle());	
							
							content	=	content.replaceAll("&lt; First Name &gt;",sender.getParticipantFirstName());
							content	=	content.replaceAll("&lt; Last Name &gt;",sender.getParticipantLastName());
							content	=	content.replaceAll("&lt; Email &gt;",sender.getParticipantEmailAddress());
							content	=	content.replaceAll("&lt; District or School Name &gt;",distrctOrSchoolName);
							content	=	content.replaceAll("&lt; District or School Address &gt;",districtOrSchoolAdd);
						}
						
						emailerService.sendMailAsHTMLText(mailid, eventSubjectforParticipants, content, "noreply@teachermatch.net");
						
						TeacherDetail teacherDetail= teachersMapForLog.get(mailid.trim());
						if(teacherDetail!=null){
							MessageToTeacher  messageToTeacher= new MessageToTeacher();
							messageToTeacher.setTeacherId(teacherDetail);
							//messageToTeacher.setJobId(jobOrder);
							messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
							messageToTeacher.setSenderId(userMaster);
							messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
							messageToTeacher.setEntityType(userMaster.getEntityType());
							messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
							messageToTeacher.setMessageSubject(eventSubjectforParticipants);
							messageToTeacher.setMessageSend(content);
							
							statelessSessionForMsg.insert(messageToTeacher);
							
						}
						
					}
				}
				else
				{	
					
					for(EventParticipantsList sender:eventParticipantsLst)
					{
					try{
						
						mailid=(sender.getParticipantEmailAddress());
						String eventTypeName=eventdetails.getEventTypeId().getEventTypeName();
						System.out.println("EventTypeName "+eventTypeName);								
						content="";
					   //content=messageToParticipants+"<br><br>\n"+glink[1];
						content=path+"<br><br>"+MailText.mailTextForEvents(messageToParticipants);
						
						if(content.contains("&lt; First Name &gt;") || content.contains("&lt; Last Name &gt;") || content.contains("&lt; Email &gt;") || content.contains("&lt; Job Title &gt;") || content.contains("&lt; District or School Name &gt;") || content.contains("&lt; District or School Address &gt;"))
						{
							if(eventdetails.getJobOrder()!=null)
								  content	=	content.replaceAll("&lt; Job Title &gt;",eventdetails.getJobOrder().getJobTitle());	
							
							content	=	content.replaceAll("&lt; First Name &gt;",sender.getParticipantFirstName());
							content	=	content.replaceAll("&lt; Last Name &gt;",sender.getParticipantLastName());
							content	=	content.replaceAll("&lt; Email &gt;",sender.getParticipantEmailAddress());
							content	=	content.replaceAll("&lt; District or School Name &gt;",distrctOrSchoolName);
							content	=	content.replaceAll("&lt; District or School Address &gt;",districtOrSchoolAdd);
						}
						
						emailerService.sendMailWithAttachments(mailid, eventSubjectforParticipants, "noreply@teachermatch.net", content,calFileName.toString());
						System.out.println("content     "+content);
						
						TeacherDetail teacherDetail= teachersMapForLog.get(mailid.trim());
						if(teacherDetail!=null){
							MessageToTeacher  messageToTeacher= new MessageToTeacher();
							messageToTeacher.setTeacherId(teacherDetail);
							//messageToTeacher.setJobId(jobOrder);
							messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
							messageToTeacher.setSenderId(userMaster);
							messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
							messageToTeacher.setEntityType(userMaster.getEntityType());
							messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
							messageToTeacher.setMessageSubject(eventSubjectforParticipants);
							messageToTeacher.setMessageSend(content);
							
							statelessSessionForMsg.insert(messageToTeacher);
							
						}
					
					}catch(Exception e)
					{e.printStackTrace();}
					}				
				  }
			}
			
			}
		}	
	 }	
	}catch(Exception e)
	{
		e.printStackTrace();
	//System.out.println(e);
	}finally{
		transactionForMsg.commit();
		statelessSessionForMsg.close();
	}
return sendmailFlag;
}
public boolean checkInviteInterviewButton(String eventId)
	 {
		 boolean chkInviteFlag = false;
		 
		 try
		 {
			 EventDetails eventDetails=eventDetailsDAO.findById(Integer.parseInt(eventId),false,false);
			 List<EventParticipantsList> eventParticipantsList = new ArrayList<EventParticipantsList>();
			 
			 
			 if(eventDetails!=null)
			 {
				 if(eventDetails.getEventTypeId()!=null && eventDetails.getEventTypeId().getEventTypeId()==1)
				 {
					 eventParticipantsList = eventParticipantsListDAO.findByEventDetails(eventDetails);
					 
					 if(eventParticipantsList!=null && eventParticipantsList.size()>0)
					 {
						/* if(eventParticipantsList.get(0).getI4InterviewInvites()!=null && eventParticipantsList.get(0).getI4InterviewInvites().getCompletedDateTime()!=null && eventParticipantsList.get(0).getI4InterviewInvites().getVideoUrl()!=null)
						 {
							 if(!eventParticipantsList.get(0).getI4InterviewInvites().getCompletedDateTime().equals("") && !eventParticipantsList.get(0).getI4InterviewInvites().getVideoUrl().equals(""))
								 chkInviteFlag = true;
						 } */
						 if(eventParticipantsList.get(0).getI4InterviewInvites()!=null && eventParticipantsList.get(0).getI4InterviewInvites().getCompletedDateTime()!=null){
							 List<VVIResponse> vviResponseList = vviResponseDAO.findByI4InviteId(eventParticipantsList.get(0).getI4InterviewInvites().getI4inviteid());
								if(vviResponseList!=null && vviResponseList.size()>0){
									chkInviteFlag = true;
								}
						 }
					 }
				 }
				 else
					 chkInviteFlag = true;
			 }
		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		 return chkInviteFlag;
	 }

public String createQRCode(String url,String filename,HttpServletRequest request)
{
	String filePath="";
	 //String myCodeText = "http://Crunchify.com/";
	 // String filePath = "D:/RahulTyagi/QRCode Liberary/"+filename;
	 String root = request.getRealPath("/")+"/qrcode/";
	 File path=new File(root);
	 if (!path.exists()) {
			boolean status = path.mkdirs();
		}
	 
	 filePath = root +filename;
     int size = 125;
     String fileType = "png";
     File myFile = new File(filePath);
     try {
         Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
         hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
         QRCodeWriter qrCodeWriter = new QRCodeWriter();
         BitMatrix byteMatrix = qrCodeWriter.encode(url,BarcodeFormat.QR_CODE, size, size, hintMap);
         int CrunchifyWidth = byteMatrix.getWidth();
         BufferedImage image = new BufferedImage(CrunchifyWidth, CrunchifyWidth,
                 BufferedImage.TYPE_INT_RGB);
         image.createGraphics();

         Graphics2D graphics = (Graphics2D) image.getGraphics();
         graphics.setColor(Color.WHITE);
         graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyWidth);
         graphics.setColor(Color.BLACK);

         for (int i = 0; i < CrunchifyWidth; i++) {
             for (int j = 0; j < CrunchifyWidth; j++) {
                 if (byteMatrix.get(i, j)) {
                     graphics.fillRect(i, j, 1, 1);
                 }
             }
         }
         ImageIO.write(image, fileType, myFile);
     } catch (WriterException e) {
         e.printStackTrace();
     } catch (IOException e) {
         e.printStackTrace();
     }
    return filePath;	
}
	public String createIcalFile(String subject,String description,List<EventSchedule>  eventschlist) throws ParseException 
			{	
		Calendar icsCalendar = new Calendar();
		String calFileName = String.valueOf(System.currentTimeMillis()).substring(6);	
		File calFile = new File(Utility.getValueOfPropByKey("iclcalendarRootPath")+"/"+calFileName+"cal.ics");
		String email="";
		String fromdate="";
		String fromtime="";
		String enddate="";
		String endtime="";
		String googleStartTime="";
		String googleEndTime="";
		String location="";
		//EventSchedule eventShedule=eventschlist.get(0);
		for(int i=0;i<eventschlist.size();i++)
		{			
			EventSchedule eventShedule=eventschlist.get(i);					
			location=eventShedule.getLocation();		
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			fromdate=dateFormat.format(eventShedule.getEventDateTime());
			enddate=dateFormat.format(eventShedule.getEventDateTime());
			fromtime=eventShedule.getEventStartTime()+" "+eventShedule.getEventStartTimeFormat();
			endtime=eventShedule.getEventEndTime()+" "+eventShedule.getEventEndTimeFormat();
			
			//convert time AM/PM to 24 hour
			SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
			SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
			Date sdate = parseFormat.parse(fromtime);
			Date edate = parseFormat.parse(endtime);
			String[] convertStartTime =displayFormat.format(sdate).split(":");
			String[] convertEndTime =displayFormat.format(edate).split(":");       
			
			/////StartTime
			int Atime=Integer.parseInt(convertStartTime[0]);
			int Btime=Integer.parseInt(convertStartTime[1]);
			
			/////EndTime
			int Ctime=Integer.parseInt(convertEndTime[0]);
			int Dtime=Integer.parseInt(convertEndTime[1]);
			
			String[] convertStartDates =fromdate.split("-");
			String[] convertEndDates =enddate.split("-");
			
			/////////Start Date       
			
			if(Integer.parseInt(convertStartDates[1])==1)
			{			
				convertStartDates[1]="12";
				convertStartDates[0]=String.valueOf((Integer.parseInt(convertStartDates[0])-1));
			}
			else
			{
				convertStartDates[1]=String.valueOf((Integer.parseInt(convertStartDates[1])-1));			
			}
			int A=(Integer.parseInt(convertStartDates[1]));
			int B=(Integer.parseInt(convertStartDates[2]));
			int C=(Integer.parseInt(convertStartDates[0]));			
			/////////////End Date		
			if(Integer.parseInt(convertEndDates[1])==1)
			{			
				convertEndDates[1]="12";
				convertEndDates[0]=String.valueOf((Integer.parseInt(convertEndDates[0])-1));
			}
			else
			{
				convertEndDates[1]=String.valueOf((Integer.parseInt(convertEndDates[1])-1));			
			}
			int D=(Integer.parseInt(convertEndDates[1]));
			int E=(Integer.parseInt(convertEndDates[2]));
			int F=(Integer.parseInt(convertEndDates[0]));		
		
			try {
			
			// Start Date is on:
			java.util.Calendar startDate = new GregorianCalendar();
			//startDate.setTimeZone(timezone);
			startDate.set(java.util.Calendar.MONTH, A);
			startDate.set(java.util.Calendar.DAY_OF_MONTH, B);
			startDate.set(java.util.Calendar.YEAR, C);
			startDate.set(java.util.Calendar.HOUR_OF_DAY, Atime);
			startDate.set(java.util.Calendar.MINUTE, Btime);
			startDate.set(java.util.Calendar.SECOND, 0);
			
			// End Date is on:
			java.util.Calendar endDate = new GregorianCalendar();
			//endDate.setTimeZone(timezone);
			endDate.set(java.util.Calendar.MONTH, D);
			endDate.set(java.util.Calendar.DAY_OF_MONTH, E);
			endDate.set(java.util.Calendar.YEAR, F);
			endDate.set(java.util.Calendar.HOUR_OF_DAY, Ctime);
			endDate.set(java.util.Calendar.MINUTE, Dtime);	
			endDate.set(java.util.Calendar.SECOND, 0);
			
			// Create the event props
			String eventName = subject;
			DateTime start = new DateTime(startDate.getTime());
			DateTime end = new DateTime(endDate.getTime());			
			googleStartTime=start.toString();
			googleEndTime=end.toString();
			// Create the event
			VEvent meeting = new VEvent(start, end, eventName);	
			
			// add timezone to vEvent
			//meeting.getProperties().add(tz.getTimeZoneId());
			if(location!=null && location!=""){
				Location loc = new Location(location);
				meeting.getProperties().add(loc);
			}
			
			Description sum = new Description(description);
			meeting.getProperties().add(sum);
			
			// generate unique identifier and add it to vEvent
			UidGenerator ug;
			ug = new UidGenerator("uidGen");
			Uid uid = ug.generateUid();			
			meeting.getProperties().add(uid);
			
			// assign props to calendar object
			icsCalendar.getProperties().add(new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
			icsCalendar.getProperties().add(CalScale.GREGORIAN);
			
			// Add the event and print
			System.out.println(meeting);
			icsCalendar.getComponents().add(meeting);
			CalendarOutputter outputter = new CalendarOutputter();
			outputter.setValidating(false);
			
			FileOutputStream fout = new FileOutputStream(calFile);
			outputter.output(icsCalendar, fout);
			}
			catch (Exception e)
			{
			System.err.println("Error in method icalfile() " + e);		
			}
		}
		
		return calFile.toString();
	}	
	
	
	public List<TeacherDetail> getJobAppliedTeachers(String seachTxt,Integer eventId)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		List<TeacherDetail> finalList = new ArrayList<TeacherDetail>();
		
		try {
				UserMaster userMaster=null;
				DistrictMaster districtMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) 
				{
					System.out.println(":::::: Session is null ::::::");
				}else{
					userMaster=	(UserMaster) session.getAttribute("userMaster");
					
					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}
				}
			
				EventDetails eventDetails = eventDetailsDAO.findById(eventId, false, false);
				
				districtMaster = eventDetails.getDistrictMaster();
			JobOrder joborder = null;
				
			
			List<TeacherDetail> teacherDetails1 = new ArrayList<TeacherDetail>();
			List<TeacherDetail> teacherDetails2 = new ArrayList<TeacherDetail>();
			
			Criterion crFirstName =null;
			Criterion crLstName =null;
			Criterion crEmail =null;
			
			Criterion crFirstName1 =null;
			Criterion crLstName1 =null;
			Criterion crEmail1 =null;
			Criterion finalCriteria =null;
			Criterion finalCriteria1 =null;
			if(seachTxt!=null && !seachTxt.equals("") && seachTxt.length()>1)
			{
				crFirstName = Restrictions.like("firstName", seachTxt.trim(),MatchMode.ANYWHERE);
				crLstName = Restrictions.like("lastName", seachTxt.trim(),MatchMode.ANYWHERE);
				crEmail = Restrictions.like("emailAddress", seachTxt.trim(),MatchMode.ANYWHERE);
				finalCriteria =Restrictions.or(crFirstName, crLstName);
				
				
				crFirstName1 = Restrictions.ilike("firstName","% "+seachTxt.trim()+"%" );
				crLstName1 = Restrictions.like("lastName","% "+seachTxt.trim()+"%");
				crEmail1 = Restrictions.like("emailAddress","% "+seachTxt.trim()+"%");

				
				Criterion criterionZA=Restrictions.or(finalCriteria, crEmail);
				finalCriteria1 =Restrictions.or(crFirstName1, crLstName1);
				
				Criterion criterionZB=Restrictions.or(finalCriteria1, crEmail1);
				teacherDetails1 = jobForTeacherDAO.findAvailableCandidteBYDistrict(Order.asc("firstName"), 0,25, districtMaster, joborder, criterionZA,eventDetails,userSession);
				teacherDetails2 = jobForTeacherDAO.findAvailableCandidteBYDistrict(Order.asc("lastName"), 0, 25, districtMaster, joborder, criterionZB,eventDetails,userSession);
				finalList.addAll(teacherDetails1);
				finalList.addAll(teacherDetails2);
				Set<TeacherDetail> setTeacher = new LinkedHashSet<TeacherDetail>(finalList);
				finalList = new ArrayList<TeacherDetail>(new LinkedHashSet<TeacherDetail>(setTeacher));
				
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}	
		
		
		return finalList;
	}
	
	///////////////////////// Save Candidate //////////////////////////////////////////////////////
	public int saveCandidate(Integer teacherId,Integer eventId){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster =	null;
		DistrictMaster districtMaster=null;
		TeacherDetail teacherDetail = null;
		
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
	    	userMaster=	(UserMaster) session.getAttribute("userMaster");
	    }
		
		try {
		
			teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			
			if(teacherDetail!=null)
			{
				Integer nscore = null;
				List<TeacherDetail> teacherList = new ArrayList<TeacherDetail>();
				teacherList.add(teacherDetail);
				
				List<TeacherNormScore> teacherNormScore  = teacherNormScoreDAO.findTeacersNormScoresList(teacherList);
				
				if(teacherNormScore!=null && teacherNormScore.size()>0)
					nscore	= teacherNormScore.get(0).getTeacherNormScore();
				
				String tempId=session.getId();

				EventDetails eventDetails = eventDetailsDAO.findById(eventId, false, false);
				districtMaster=eventDetails.getDistrictMaster();
				Criterion criterion = Restrictions.eq("participantEmailAddress", teacherDetail.getEmailAddress());
				Criterion criterion1 = Restrictions.eq("eventDetails", eventDetails);
				
				if(eventDetails.getEventTypeId().getEventTypeId()==1 ){
					List<EventParticipantsList> eventparticipantsList1 = eventParticipantsListDAO.findByCriteria(criterion1);
					if(eventparticipantsList1!=null  && eventparticipantsList1.size()>0 ){
						if(eventDetails.getEventTypeId().getEventTypeId()==1)
						   return 5;
						
					}else{
						EventParticipantsList eventParticipantsList = new EventParticipantsList();
						eventParticipantsList.setTeacherId(teacherId);
						eventParticipantsList.setParticipantFirstName(teacherDetail.getFirstName());
						eventParticipantsList.setParticipantLastName(teacherDetail.getLastName());
						eventParticipantsList.setParticipantEmailAddress(teacherDetail.getEmailAddress());
						eventParticipantsList.setNormScore(nscore);
						eventParticipantsList.setDistrictMaster(districtMaster);
						eventParticipantsList.setInvitationEmailSent(false);
						eventParticipantsList.setStatus("A");
						eventParticipantsList.setCreatedBY(userMaster);
						eventParticipantsList.setEventDetails(eventDetails);
						eventParticipantsList.setCreatedDateTime(new Date());
						eventParticipantsListDAO.makePersistent(eventParticipantsList);
						return 2;
				       }
				}else{
					  List<EventParticipantsList> eventparticipantsList = eventParticipantsListDAO.findByCriteria(criterion,criterion1);
					  if(eventparticipantsList.size()>0){
						return 1;
					}else{
						EventParticipantsList eventParticipantsList = new EventParticipantsList();
						eventParticipantsList.setTeacherId(teacherId);
						eventParticipantsList.setParticipantFirstName(teacherDetail.getFirstName());
						eventParticipantsList.setParticipantLastName(teacherDetail.getLastName());
						eventParticipantsList.setParticipantEmailAddress(teacherDetail.getEmailAddress());
						eventParticipantsList.setNormScore(nscore);
						eventParticipantsList.setDistrictMaster(districtMaster);
						eventParticipantsList.setInvitationEmailSent(false);
						eventParticipantsList.setStatus("A");
						eventParticipantsList.setCreatedBY(userMaster);
						eventParticipantsList.setEventDetails(eventDetails);
						eventParticipantsList.setCreatedDateTime(new Date());
						eventParticipantsListDAO.makePersistent(eventParticipantsList);
						return 2;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public List<TeacherDetail> getPropectsArray(String seachTxt,Integer eventId)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		List<TeacherDetail> finalList = new ArrayList<TeacherDetail>();
		
		try {
				UserMaster userMaster=null;
				DistrictMaster districtMaster=null;
				EventDetails eventDetails = eventDetailsDAO.findById(eventId, false, false);
				
				if (session == null || session.getAttribute("userMaster") == null) 
				{
					System.out.println(":::::: Session is null ::::::");
				}else{
					userMaster=	(UserMaster) session.getAttribute("userMaster");
					
					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}
				}
			
			JobOrder joborder = null;
			
			districtMaster = 	eventDetails.getDistrictMaster();
			
			List<TeacherDetail> teacherDetails1 = new ArrayList<TeacherDetail>();
			List<TeacherDetail> teacherDetails2 = new ArrayList<TeacherDetail>();
			
			List<Criterion> basicSearch = new ArrayList<Criterion>();
			List<Criterion> basicSearch1 = new ArrayList<Criterion>();
			
			Criterion crFirstName =null;
			Criterion crLstName =null;
			Criterion crEmail =null;
			
			Criterion crFirstName1 =null;
			Criterion crLstName1 =null;
			Criterion crEmail1 =null;
			Criterion finalCriteria =null;
			Criterion finalCriteria1 =null;
			
			if(seachTxt!=null && !seachTxt.equals("") && seachTxt.length()>1)
			{
				crFirstName = Restrictions.like("firstName", seachTxt.trim(),MatchMode.ANYWHERE);
				crLstName = Restrictions.like("lastName", seachTxt.trim(),MatchMode.ANYWHERE);
				crEmail = Restrictions.like("emailAddress", seachTxt.trim(),MatchMode.ANYWHERE);
				finalCriteria =Restrictions.or(crFirstName, crLstName);
				
				/*basicSearch.add(crFirstName);
				basicSearch.add(crLstName);
				basicSearch.add(crEmail);*/
				
				crFirstName1 = Restrictions.ilike("firstName","% "+seachTxt.trim()+"%" );
				crLstName1 = Restrictions.like("lastName","% "+seachTxt.trim()+"%");
				crEmail1 = Restrictions.like("emailAddress","% "+seachTxt.trim()+"%");
				
				
				Criterion criterionZA=Restrictions.or(finalCriteria, crEmail);
				
				/*basicSearch1.add(crFirstName);
				basicSearch1.add(crLstName);
				basicSearch1.add(crEmail);*/
				
				finalCriteria1 =Restrictions.or(crFirstName1, crLstName1);
				
				Criterion criterionZB=Restrictions.or(finalCriteria1, crEmail1);
			    
				// To Get Applied Candidate	
				List teacherListNA = jobForTeacherDAO.findUniqueApplicants(districtMaster,eventDetails);
				
				List<Integer> teID = new ArrayList<Integer>();
				for (Object tDlist : teacherListNA) {
					 int ds = Utility.getIntValue(String.valueOf(tDlist));
					teID.add(ds);
				}

				teacherDetails1 = teacherDetailDAO.findAvailableCandidteNotApplYAnyJOb(Order.asc("firstName"),0,25,teID,criterionZA);
				teacherDetails2 = teacherDetailDAO.findAvailableCandidteNotApplYAnyJOb(Order.asc("firstName"),0,25,teID,criterionZB);
				
		//		teacherDetails1 = jobForTeacherDAO.findAvailableCandidteBYDistrict(Order.asc("firstName"), 0, 25, districtMaster, joborder, criterionZA);
		//		teacherDetails2 = jobForTeacherDAO.findAvailableCandidteBYDistrict(Order.asc("lastName"), 0, 25, districtMaster, joborder, criterionZB);
				
				
				finalList.addAll(teacherDetails1);
				finalList.addAll(teacherDetails2);
				Set<TeacherDetail> setTeacher = new LinkedHashSet<TeacherDetail>(finalList);
				finalList = new ArrayList<TeacherDetail>(new LinkedHashSet<TeacherDetail>(setTeacher));
				
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}	
		
		
		return finalList;
	}

	
	public String displayParticipantsGridNew(Integer eventId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer ptRecords =	new StringBuffer();
		System.out.println("call");
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		try {
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
					
				
			}
			
			EventDetails eventDetails = eventDetailsDAO.findById(eventId, false, false);
			//districtMaster=eventDetails.getDistrictMaster();	
			
			Criterion crt=Restrictions.eq("eventDetails", eventDetails);
			
			Order order=Order.asc("participantLastName");
			//List<EventParticipantsList> eventParticipantsLists = eventParticipantsListDAO.findListOfParticipants(sortOrderStrVal,start,noOfRowInPage,districtMaster,eventDetails);
			List<EventParticipantsList> eventParticipantsLists = eventParticipantsListDAO.findByCriteria(order,crt);
			
			
			int cnt = 1;
			if(eventParticipantsLists.size()>0){
				
				for (EventParticipantsList eventParticipantsList : eventParticipantsLists) {
					String normScore="N/A";
					
					if(eventParticipantsList.getNormScore()!=null && eventParticipantsList.getNormScore()!=0){
						normScore=eventParticipantsList.getNormScore()+"";
					}
					//System.out.println(" ::::::::::::: "+normScore);
					ptRecords.append("<input type='hidden' id='participantIds' value='"+eventParticipantsList.getEventParticipantId()+"'/>");
					ptRecords.append("<div class='col-sm-4 col-md-4' style='margin-bottom:10px;'><a data-original-title='Norm Score: "+normScore+"' rel='tooltip' id='nsId"+cnt+"' href='javascript:void(0);'>"+eventParticipantsList.getParticipantFirstName()+" "+eventParticipantsList.getParticipantLastName()+"</a><br>("+eventParticipantsList.getParticipantEmailAddress()+")");
					
					ptRecords.append(" <a data-original-title='"+Utility.getLocaleValuePropByKey("lnkRemo", locale)+"' rel='tooltip' id='rmId"+cnt+"' href='javascript:void(0);' onclick=\"return deleteParticipant("+eventParticipantsList.getEventParticipantId()+");\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a>");
				
					ptRecords.append("</div>");					
					
					/*ptRecords.append("<td><input type='hidden' id='participantId' value='"+eventParticipantsList.getEventParticipantId()+"'/>"+eventParticipantsList.getParticipantFirstName()+"</td>");
					ptRecords.append("<td>"+eventParticipantsList.getParticipantLastName()+"</td>");
					ptRecords.append("<td>"+eventParticipantsList.getParticipantEmailAddress()+"</td>");
					if(eventParticipantsList.getNormScore()==null || eventParticipantsList.getNormScore().equals(0)){
						ptRecords.append("<td>N/A</td>");	
					}else{
						ptRecords.append("<td>"+eventParticipantsList.getNormScore()+"</td>");
					}
					ptRecords.append("<td><a href='javascript:void(0);' onclick='deleteParticipant("+eventParticipantsList.getEventParticipantId()+")'>Remove</td>");*/
					cnt++;
				}
				
				/*ptRecords.append("<script>");
				for(int i=0;i<cnt;i++)
					ptRecords.append("$('#nsId0"+i+"').tooltip();");
				ptRecords.append("</script>");*/
			}else{
				ptRecords.append("<div class='col-sm-3 col-md-3'>"+Utility.getLocaleValuePropByKey("lblNoCandidate", locale)+"</div>");
			}
			ptRecords.append("####"+cnt);
			
		} catch (Exception e) {
		}
		return ptRecords.toString();
	}
	
	
	public static String getAddressDistHeadBranch(EventDetails eventDetails)
	{
		
		String address="";
		String districtAddress="";
		String city="";
		String state="";
		String zipcode="";
		String districtPnNo="";
		DistrictMaster districtMaster=eventDetails.getDistrictMaster();
		
		try{
			if(districtMaster!=null){
				if(eventDetails.getCreatedBY().getEntityType()==3){
					SchoolMaster schoolMaster=eventDetails.getCreatedBY().getSchoolId();
					if(schoolMaster!=null){
						if(schoolMaster.getAddress()!=null && !schoolMaster.getAddress().equals(""))
						{
							districtAddress=schoolMaster.getAddress()+", ";	
						}
						if(schoolMaster.getCityName()!=null && !schoolMaster.getCityName().equals(""))
						{
							city=schoolMaster.getCityName()+", ";	
						}
						if(schoolMaster.getStateMaster().getStateName()!=null && !schoolMaster.getStateMaster().getStateName().equals(""))
						{
							state=schoolMaster.getStateMaster().getStateName()+", ";	
						}
						if(schoolMaster.getZip()!=null && !schoolMaster.getZip().equals(""))
						{
							zipcode=schoolMaster.getZip()+", ";	
						}
						if(schoolMaster.getPhoneNumber()!=null && !schoolMaster.getPhoneNumber().equals(""))
						{
							districtPnNo=schoolMaster.getPhoneNumber()+", ";	
						}
					}
				}else{
					if(districtMaster.getAddress()!=null && !districtMaster.getAddress().equals(""))
					{
						districtAddress=districtMaster.getAddress()+", ";	
					}
					if(districtMaster.getCityName()!=null && !districtMaster.getCityName().equals(""))
					{
						city=districtMaster.getCityName()+", ";	
					}
					if(districtMaster.getStateId().getStateName()!=null && !districtMaster.getStateId().getStateName().equals(""))
					{
						state=districtMaster.getStateId().getStateName()+", ";	
					}
					if(districtMaster.getZipCode()!=null && !districtMaster.getZipCode().equals(""))
					{
						zipcode=districtMaster.getZipCode()+", ";	
					}
					if(districtMaster.getPhoneNumber()!=null && !districtMaster.getPhoneNumber().equals(""))
					{
						districtPnNo=districtMaster.getPhoneNumber()+", ";	
					}
				}
				
			}else if(eventDetails.getBranchMaster()!=null){
				BranchMaster branchMaster=eventDetails.getBranchMaster();
				if(branchMaster.getAddress()!=null && !branchMaster.getAddress().equals(""))
				{
					districtAddress=branchMaster.getAddress()+", ";	
				}
				if(branchMaster.getCityName()!=null && !branchMaster.getCityName().equals(""))
				{
					city=branchMaster.getCityName()+", ";	
				}
				if(branchMaster.getStateMaster().getStateName()!=null && !branchMaster.getStateMaster().getStateName().equals(""))
				{
					state=branchMaster.getStateMaster().getStateName()+", ";	
				}
				if(branchMaster.getZipCode()!=null && !branchMaster.getZipCode().equals(""))
				{
					zipcode=branchMaster.getZipCode()+", ";	
				}
				if(branchMaster.getPhoneNumber()!=null && !branchMaster.getPhoneNumber().equals(""))
				{
					districtPnNo=branchMaster.getPhoneNumber()+", ";	
				}
			}else if(eventDetails.getHeadQuarterMaster()!=null){
				HeadQuarterMaster  headQuarterMaster=eventDetails.getHeadQuarterMaster();
				if(headQuarterMaster.getAddress()!=null && !headQuarterMaster.getAddress().equals(""))
				{
					districtAddress=headQuarterMaster.getAddress()+", ";	
				}
				if(headQuarterMaster.getCityName()!=null && !headQuarterMaster.getCityName().equals(""))
				{
					city=headQuarterMaster.getCityName()+", ";	
				}
				if(headQuarterMaster.getStateId().getStateName()!=null && !headQuarterMaster.getStateId().getStateName().equals(""))
				{
					state=headQuarterMaster.getStateId().getStateName()+", ";	
				}
				if(headQuarterMaster.getZipCode()!=null && !headQuarterMaster.getZipCode().equals(""))
				{
					zipcode=headQuarterMaster.getZipCode()+", ";	
				}
				if(headQuarterMaster.getPhoneNumber()!=null && !headQuarterMaster.getPhoneNumber().equals(""))
				{
					districtPnNo=headQuarterMaster.getPhoneNumber()+", ";	
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		address = districtAddress+city+state+zipcode+districtPnNo;
		System.out.println("address :: "+address);
		return address;
	}
	
	
	
}