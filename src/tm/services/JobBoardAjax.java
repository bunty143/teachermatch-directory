package tm.services;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobCertification;
import tm.bean.JobOrder;
import tm.bean.master.CityMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobCertificationDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.utility.Utility;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.net.URI;
import java.net.URISyntaxException;
import tm.utility.ElasticSearchConfig;
import javax.ws.rs.core.MediaType;
import net.sf.json.JSONObject;


public class JobBoardAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	public static String smallFontSize = "'font-family: Tahoma;font-size: 11px;'";


	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	public void setJobCertificationDAO(JobCertificationDAO jobCertificationDAO) {
		this.jobCertificationDAO = jobCertificationDAO;
	}

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	public void setSubjectMasterDAO(SubjectMasterDAO subjectMasterDAO) {
		this.subjectMasterDAO = subjectMasterDAO;
	}
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO) {
		this.cityMasterDAO = cityMasterDAO;
	}
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}
	
	public String displayJobsByTM(String months,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String districtId,String schoolId1,String cityName,String stateId,String zipCode,String subjectId,String certificateTypeMaster)
	{
		//System.out.println(" Reached :: ");
		System.out.println(" "+months+" :: "+noOfRow+" :: "+pageNo+" :: "+sortOrder+" :: "+sortOrderType+" :: "+districtId+" :: "+schoolId1+" :: "+cityName+" :: "+stateId+" :: "+zipCode+" :: "+subjectId+" :: "+certificateTypeMaster );
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		List<JobOrder> jobOrderAllList = new ArrayList<JobOrder>();		
		List<JobOrder> jobLst = new ArrayList<JobOrder>();
		List<String> locations= new ArrayList<String>();
		StringBuffer tmRecords =	new StringBuffer();
		String result ="";
		try{
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------
		

			boolean flagJobOrderList = false; 
			
			ArrayList<Integer> subjectIdList=  new ArrayList<Integer>();
			List<SubjectMaster> ObjSubjectList = new ArrayList<SubjectMaster>();
			
			 
			List<JobOrder> districtMasterlst = new ArrayList<JobOrder>(); 
			List<JobOrder> schoolMasterlst = new ArrayList<JobOrder>(); 
			List<JobOrder> zipCodeList = new ArrayList<JobOrder>(); 
			List<JobOrder> jobIdList = new ArrayList<JobOrder>(); 
			List<JobOrder> subjectList = new ArrayList<JobOrder>(); 
			List<JobOrder> zipCodeList2 = new ArrayList<JobOrder>();
			List<String> zipList	=	new ArrayList<String>();
			List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
			List<CityMaster> lstcityMasters = new ArrayList<CityMaster>();
			
			
			if(!cityName.equals("0")){
				System.out.println(" city is  NOT 0 ....");
				zipList = cityMasterDAO.findCityByName(cityName);
			}else{
				System.out.println(" city is 0 ....");
				//find StateMaster Object
				if(stateId!=null && !stateId.equals("")){
					StateMaster stateMaster = stateMasterDAO.findById(Long.parseLong(stateId), false, false);
					
					// city List By State Id
					lstcityMasters	=	cityMasterDAO.findCityByState(stateMaster);
					
					for(CityMaster c:lstcityMasters)
					{
						//System.out.println(" city name :: "+c.getCityName()+" zip code :: "+c.getZipCode());
						zipList.add(c.getZipCode());
					}
				}
			}
			
			
			
			Order  sortOrderStrVal=null;
			sortOrderStrVal=Order.desc("createdDateTime");

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
		    Date dateWithoutTime = sdf.parse(sdf.format(new Date()));
		    
		    List<JobOrder> lstJobOrder	  =	 new ArrayList<JobOrder>();
			List<JobOrder> lstJobOrderTotal	  =	 new ArrayList<JobOrder>();
			Criterion criterion1 = Restrictions.eq("status","A");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
		
		/* @Start
		 * @Ashish Kumar
		 * @Description :: Searching */
			
			lstJobOrder =jobOrderDAO.findAllActiveJobForJobboard(sortOrderStrVal,start,end,criterion1,criterion2,criterion3,criterion4,true);
			
			if(lstJobOrder.size()>0){
				flagJobOrderList=true;
				jobOrderAllList.addAll(lstJobOrder);
				
				System.out.println(" All job List :: "+jobOrderAllList.size());
			}
			
			// For District Search
			if(!districtId.equals("") && !districtId.equals(""))
			{
				System.out.println(">>>>>>> Inside Block 1 Search by District <<<<<<<<");
				
				districtMaster  = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
				districtMasterlst = jobOrderDAO.findJobOrderByDistrict(districtMaster);
				
				if(flagJobOrderList==true){
					jobOrderAllList.retainAll(districtMasterlst);
				}else{
					jobOrderAllList.addAll(districtMasterlst);
				}
			}
			
			// For Selected Month Search
			if(months!=null && !months.equals("0"))
			{
				int daysNo=0;
				try{
					daysNo=Integer.parseInt(months);
				}catch(Exception e){}
				
				Calendar c = Calendar.getInstance(); 
				c.add(Calendar.MONTH, -+daysNo);  
				Date date = c.getTime();
				System.out.println(" date :: "+date);
				Criterion criterion5 = Restrictions.ge("createdDateTime",date);
				lstJobOrder =jobOrderDAO.findAllActiveJobForJobboardWithDays(sortOrderStrVal,start,end,criterion1,criterion2,criterion3,criterion5,true);
				
				
				if(flagJobOrderList==true)
					jobOrderAllList.retainAll(lstJobOrder);
				else
					jobOrderAllList.addAll(lstJobOrder);
			}
						
			// For School Search
			if(!schoolId1.equals("0") && !schoolId1.equals(""))
			{
				System.out.println(">>>>>>> Inside Block 2 Search by School <<<<<<<<");
			
				schoolMaster 	=	schoolMasterDAO.findById(new Long(schoolId1), false, false);
				schoolMasterlst.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, null,ObjSubjectList));
				
				if(flagJobOrderList==true)
					jobOrderAllList.retainAll(schoolMasterlst);
				else
					jobOrderAllList.addAll(schoolMasterlst);
				  
			}
			
			
			// For Subject
			if(subjectId !=null && !subjectId.equals("0,") && !subjectId.equals(""))
			{
				System.out.println(">>>>>>> Inside Block 3 Search by SUbject <<<<<<<<"+subjectId);
					
				subjectId = subjectMasterDAO.getIdsFormMasterSubjectIds(subjectId);
				String subIdList[] = subjectId.split(",");
				if(Integer.parseInt(subIdList[0])!=0)
				{
					for(int i=0;i<subIdList.length;i++)
					{
						subjectIdList.add(Integer.parseInt(subIdList[i]));
					}
					ObjSubjectList = subjectMasterDAO.getSubjectMasterlist(subjectIdList);
					
					subjectList = jobOrderDAO.findJobOrderBySubjectList(ObjSubjectList);
					
					if(flagJobOrderList==true){
						jobOrderAllList.retainAll(subjectList);
					}else{
						jobOrderAllList.addAll(subjectList);
					}
				}
			}
			
			// For Cities (ZipCode List)
			if(zipList!=null && zipList.size()>0)
			{
				System.out.println(">>>>>>> Inside Block 5 Search by ZipCOde  List<<<<<<<<");
				
				List<DistrictMaster> lstDistrictMasters = new ArrayList<DistrictMaster>();
				List<SchoolMaster> lstSchoolMasters = null;
				
				lstSchoolMasters = schoolMasterDAO.findByZipCodeList(zipList);
				if(lstSchoolMasters!=null)
				{
					for(SchoolMaster s:lstSchoolMasters)
					{
							lstDistrictMasters.add(s.getDistrictId());
					}
				}
				
				if(lstDistrictMasters!=null && lstDistrictMasters.size()>0)
					zipCodeList2 = jobOrderDAO.findJobOrderByDistrictList(lstDistrictMasters);
		
				if(flagJobOrderList==true){
					jobOrderAllList.retainAll(zipCodeList2);
				}else{
					jobOrderAllList.addAll(zipCodeList2);
				}
				
			}
			
			
			
			// For ZipCode
			if(zipCode!=null && !zipCode.equals("0,") && !zipCode.equals(""))
			{
				System.out.println(">>>>>>> Inside Block 4 Search by ZipCOde <<<<<<<<");
				
				List<DistrictMaster> dm = districtMasterDAO.findDistrictByZipCode(zipCode);
				if(dm!=null && dm.size()>0)
					zipCodeList = jobOrderDAO.findJobOrderByDistrictList(dm);
				
				if(flagJobOrderList==true){
					jobOrderAllList.retainAll(zipCodeList);
				}else{
					jobOrderAllList.addAll(zipCodeList);
				}
			}
			
			// For Certification
			if(certificateTypeMaster!=null && !certificateTypeMaster.equals(""))
			{
				System.out.println(">>>>>>> Inside Block 4 Search by Certification  <<<<<<<<");
				
				certJobOrderList=jobCertificationDAO.findCertificationByJob(certificateTypeMaster);
				
				if(flagJobOrderList==true){
					jobOrderAllList.retainAll(certJobOrderList);
				}else{
					jobOrderAllList.addAll(certJobOrderList);
				}
			}
			
			
			
	/* @End
	 * @Ashish Kumar
	 * @Description :: Searching */
			
			totalRecord =jobOrderAllList.size();
			System.out.println("totalRecord::"+totalRecord);
			
			/**
			 * added by ankit for quest job board
			 * get distinct locations for map
			 */
			/* start */			
			List<JobOrder> mapLoc = jobOrderDAO.findAllActiveJobForLocations(criterion1,criterion2,criterion3,criterion4);
			if(mapLoc!=null && mapLoc.size()>0)
			{
				for(JobOrder jOrder : mapLoc)
				{	
					if(jOrder.getDistrictMaster()!=null)
					{
						if(!locations.contains(jOrder.getDistrictMaster().getDistrictName()+", "+jOrder.getDistrictMaster().getStateId().getStateName()+", "+jOrder.getDistrictMaster().getStateId().getCountryMaster().getName()))
						{
							locations.add(jOrder.getDistrictMaster().getDistrictName()+", "+jOrder.getDistrictMaster().getStateId().getStateName()+", "+jOrder.getDistrictMaster().getStateId().getCountryMaster().getName());
							result = result + jOrder.getDistrictMaster().getDistrictName()+", "+jOrder.getDistrictMaster().getStateId().getStateName()+", "+jOrder.getDistrictMaster().getStateId().getCountryMaster().getName()+"||";
						}
					}
				}
			}	
			/* end */

			if(totalRecord<end)
				end=totalRecord;
			
			jobLst=jobOrderAllList.subList(start,end);
			
			String baseurl = Utility.getBaseURL(request);
			Date startDate = null;
			Date endDate = new Date();
			int difInDays = 0;
			Map<Integer,String> certificatiosMap = new HashMap<Integer, String>();
			List<JobCertification> jobCertificationList = new ArrayList<JobCertification>();
			
			if(jobOrderAllList.size()>0)
			jobCertificationList = jobCertificationDAO.findCertificationByJobOrders(jobOrderAllList);
		
			if(jobCertificationList.size()>0)
			{
				for (JobCertification jobCertification : jobCertificationList) {
					if(jobCertification.getCertificateTypeMaster()!=null)
					{
						String certs = certificatiosMap.get(jobCertification.getJobId().getJobId());
						if(certs!=null)
							certificatiosMap.put(jobCertification.getJobId().getJobId(), certs+jobCertification.getCertificateTypeMaster().getCertType()+"</br>");
						else
							certificatiosMap.put(jobCertification.getJobId().getJobId(), jobCertification.getCertificateTypeMaster().getCertType()+"</br>");
					}
				}
			}
			int i=0;
			String untillCheck ="";
			tmRecords.append("<div  class=''  style='padding:2px; background-color:white; margin-left:0px;width:100%;min-height:150px;' >");
			for (JobOrder jobOrderDetails : jobLst){
				++i;
				tmRecords.append("<div  class=''  style='padding:8px; background-color:#E4EBEF;width:100%;color:black;box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.4);border-radius: 8px;' >");
				tmRecords.append("<span style='font-size:14px;'><b class='titlecolor'>"+jobOrderDetails.getJobTitle()+" </b></span><br/>");
				startDate = jobOrderDetails.getJobStartDate();
				endDate = jobOrderDetails.getJobEndDate();
				untillCheck = Utility.convertDateAndTimeToUSformatOnlyDate(endDate);
				/*difInDays = (int) ((endDate.getTime() - startDate.getTime())/(1000*60*60*24));
				if(difInDays<=30)
				tmRecords.append("<img src='images/icon_new.gif'>");*/
				//<button class=\"btn btn-primary\" type=\"button\" style=\"float:right;\" onclick=\"chkApplyJobNonClient('"+jobOrderDetails.getJobId()+"','"+jobOrderDetails.getDistrictMaster().getJobApplicationCriteriaForProspects()+"','"+jobOrderDetails.getExitURL()+"','"+jobOrderDetails.getDistrictMaster().getDistrictId()+"','') \"><strong>Apply <i class=\"icon\"></i></strong></button>
				tmRecords.append("<div class='pull-right' style='border: 0px solid green;font-weight:bold;'>"+Utility.getLocaleValuePropByKey("lbljobPostedAs", locale)+" "+Utility.convertDateAndTimeToUSformatOnlyDate(startDate)+""+Utility.getLocaleValuePropByKey("lblDateUntill", locale)+" "+(untillCheck.indexOf("2099")!=-1?""+Utility.getLocaleValuePropByKey("msguntillfilled", locale)+"":untillCheck)+"</div>");
				//tmRecords.append("<br/>"+jobOrderDetails.getDistrictMaster().getDistrictName()+"<div class='pull-right' style='border: 0px solid green;margin-left: -40px;padding-right:10px;'><a target='_blank' href='"+baseurl+"applyteacherjob.do?jobId="+jobOrderDetails.getJobId()+"'>View Detail</a></div><br/>");
				//tmRecords.append("<br/>"+jobOrderDetails.getDistrictMaster().getDistrictName()+"<div class='pull-right' id='showLink"+i+"' style='border: 0px solid green;margin-left: -40px;padding-right:10px;'><a href='javascript:void(0);' onclick='showApply("+i+")'>View&nbsp;Detail</a></div><br/>");
				if(jobOrderDetails.getDistrictMaster()!=null)
					tmRecords.append("<div style='font-weight:bold;' >"+jobOrderDetails.getDistrictMaster().getDistrictName()+"</div>");
				else
					tmRecords.append("<div style='font-weight:bold;' >"+jobOrderDetails.getHeadQuarterMaster().getHeadQuarterName()+"</div>");
				
				tmRecords.append("<div style=''>");
				if(jobOrderDetails.getCreatedForEntity()==2)
					tmRecords.append(""+getDistrictAddress(jobOrderDetails)+"");
				else
					tmRecords.append(""+getDistrictAddress(jobOrderDetails)+"");
				tmRecords.append("</div>");	

				String certs = certificatiosMap.get(jobOrderDetails.getJobId());
				if(certs!=null)
					tmRecords.append("<div style='' ><b>"+Utility.getLocaleValuePropByKey("msgCertificationRequired", locale)+" </b><br>"+certs+"</div>");
	
				if(jobOrderDetails.getGeoZoneMaster()!=null){
					tmRecords.append("<div style=''><b>Zone: </b>");
					if(jobOrderDetails.getDistrictMaster().getDistrictId().equals(1200390)){
					//	tmRecords.append("<a href='javascript:void(0);' onclick=\"return showZoneSchoolPics("+jobOrderDetails.getGeoZoneMaster().getGeoZoneId()+",'"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"')\";>"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"</a>");
	                     tmRecords.append("<a href='javascript:void(0);' onclick='showMiamiPdf();'>"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"</a>");
					}else{
						tmRecords.append("<a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jobOrderDetails.getGeoZoneMaster().getGeoZoneId()+",'"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"',"+jobOrderDetails.getDistrictMaster().getDistrictId()+")\";>"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"</a>");
					}					
					tmRecords.append("</div>");
					
				}				
				if(jobOrderDetails.getJobDescriptionHTML()!=null && jobOrderDetails.getJobDescriptionHTML().length()>0)
					tmRecords.append("<div  class='titlecolor'><b>"+Utility.getLocaleValuePropByKey("lblDetails", locale)+": </b><br></div>");

				tmRecords.append("<table style=''><tr><td style=''>");
				tmRecords.append("<div style=''>");	
				if(jobOrderDetails.getJobDescriptionHTML()!=null && jobOrderDetails.getJobDescriptionHTML().length()>0)
					tmRecords.append("<div id='des"+i+"' style='display:none;'>"+jobOrderDetails.getJobDescriptionHTML()+"</div> ");
				else
					tmRecords.append("<div id='des"+i+"' style='display:none;'></div>");				
				//tmRecords.append("<div id='aply"+i+"' style='display:none;padding-top:5px;'><button class=\"btn btn-primary\" type=\"button\" onclick=\"chkApplyJob('"+jobOrderDetails.getJobId()+"') \"><strong>Apply <i class=\"icon\"></i></strong></button>&nbsp;&nbsp;<a href=\"javascript:void(0);\" onclick='hideApply("+i+")'>Cancel</a><br></div>");
				tmRecords.append("<div id='desShow"+i+"' style='display:none;'></div>");

				tmRecords.append("</div>");
				if(jobOrderDetails.getJobDescriptionHTML()!=null && jobOrderDetails.getJobDescriptionHTML().length()>0)
					tmRecords.append("<div>"+jobOrderDetails.getJobDescriptionHTML()+"</div><br/>");
					//tmRecords.append("<div style='' id='showLink"+i+"' ><a href='javascript:void(0);' onclick='showApply("+i+")'>View&nbsp;Detail</a></div>");				
				tmRecords.append("</td><td style='' valign=\"bottom\">");
				//tmRecords.append("<span class='pull-right' id='showLink"+i+"' style='border: 0px solid green;margin-left: -40px;padding-right:10px;'><a href='javascript:void(0);' onclick='showApply("+i+")'>View&nbsp;Detail</a></span>");
				tmRecords.append("</td></tr></table>");
				//tmRecords.append("<div style=\"height:30px;\"><button class=\"btn btn-primary\" type=\"button\" style=\"float:right;right:0;\" onclick=\"chkApplyJobNonClient('"+jobOrderDetails.getJobId()+"','"+jobOrderDetails.getDistrictMaster().getJobApplicationCriteriaForProspects()+"','"+jobOrderDetails.getExitURL()+"','"+jobOrderDetails.getDistrictMaster().getDistrictId()+"','') \"><strong>Apply <i class=\"icon\"></i></strong></button></div>");
				tmRecords.append("<div style=\"height:30px;\"><a href=\"applyteacherjob.do?jobId="+jobOrderDetails.getJobId()+"\"><button class=\"btn btn-primary\" type=\"button\" style=\"float:right;right:0;\"><strong>"+Utility.getLocaleValuePropByKey("msgApply3", locale)+" <i class=\"icon\"></i></strong></button></a></div>");
				tmRecords.append("</div>");				
				tmRecords.append("<div class='span10 jboardFooter' style='height:5px;background-color: white;  border: 0px hidden white;'><br/>");
				tmRecords.append("</div>");
			}
			tmRecords.append("<input type='hidden' id='rowsappended' value="+i+">");
			if(jobLst.size()==0){
				tmRecords.append("<div  class='span16' style='height:20px;padding-left: 10px; width: 970px; border: 0px hidden white; background-color: white; margin-left: -5px; ' >");
				tmRecords.append(""+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</div>");
			}
			tmRecords.append("</div>");
			tmRecords.append("</div>");
			tmRecords.append("<div style='height:10px;'></div>");
			tmRecords.append("<script>jobBoardFooter();</script>");
			tmRecords.append("<div id='jobBoardFooter' style='width:100%;border: 0px hidden white; background-color: white; border-style: solid; margin-left: 0px; margin-top: -10px;'>"+PaginationAndSorting.getPaginationStringForJobboard(request,totalRecord,noOfRow, pageNo)+"</div>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println(" Total Locations Get :: "+locations.size());
		//System.out.println("Locations :: "+locations);
		//System.out.println("Locations :: "+result);
		
		return tmRecords.toString()+"@@@"+result;
	}
	
/* @Start
 * @Ashish Kumar
 * @description :: Get District Address
 * */
	public String getDistrictAddress(JobOrder jobOrderDetails)
	{
		String districtAddress="";
		String address="";
		String state="";
		String city = "";
		String zipcode="";
		
		//Get district address
		if(jobOrderDetails.getDistrictMaster()!=null)
		{
			if(jobOrderDetails.getDistrictMaster().getAddress()!=null && !jobOrderDetails.getDistrictMaster().getAddress().equals(""))
			{
				if(!jobOrderDetails.getDistrictMaster().getCityName().equals(""))
				{
					districtAddress = jobOrderDetails.getDistrictMaster().getAddress()+", ";
				}
				else
				{
					districtAddress = jobOrderDetails.getDistrictMaster().getAddress();
				}
			}
			if(!jobOrderDetails.getDistrictMaster().getCityName().equals(""))
			{
				if(jobOrderDetails.getDistrictMaster().getStateId()!=null && !jobOrderDetails.getDistrictMaster().getStateId().getStateName().equals(""))
				{
					city = jobOrderDetails.getDistrictMaster().getCityName()+", ";
				}else{
					city = jobOrderDetails.getDistrictMaster().getCityName();
				}
			}
			if(jobOrderDetails.getDistrictMaster().getStateId()!=null && !jobOrderDetails.getDistrictMaster().getStateId().getStateName().equals(""))
			{
				//System.out.println(jobOrderDetails.getDistrictMaster().getDistrictId()+" jobOrderDetails.getDistrictMaster().getStateId() :: "+jobOrderDetails.getDistrictMaster().getStateId().getStateName());
				
				if(jobOrderDetails.getDistrictMaster().getZipCode()!=null && !jobOrderDetails.getDistrictMaster().getZipCode().equals(""))
				{
					state = jobOrderDetails.getDistrictMaster().getStateId().getStateName()+", ";
				}
				else
				{
					state = jobOrderDetails.getDistrictMaster().getStateId().getStateName();
				}	
			}
			if(jobOrderDetails.getDistrictMaster().getZipCode()!=null && !jobOrderDetails.getDistrictMaster().getZipCode().equals(""))
			{
					zipcode = jobOrderDetails.getDistrictMaster().getZipCode();
			}
			
			if(districtAddress !="" || city!="" ||state!="" || zipcode!=""){
				address = districtAddress+city+state+zipcode;
			}else{
				address="";
			}
		}
	
		return address;
	}
/* @End
 * @Ashish Kumar
 * @description :: Get District Address
 * */
	
	
//shadab start
	
	public String searchByDocument(String searchTerm,int months,String districtName,String districtId,String stateName,String cityName,
			String schoolName,String certificateTypeMaster,String zipCode,int from,String subjectIdList,String noOfRow, String pageNo)
	{
		
		String result="";
		String address="";
		int j=1; //		this is in reference of i of displayJobsByTM
		StringBuffer tmRecords =	new StringBuffer();
		Map<Integer,Integer> jobIdMap=new HashMap<Integer, Integer>();
		Map<Integer,Integer> map=new HashMap<Integer, Integer>();
		
		
		int pgNo = Integer.parseInt(pageNo);
		int noOfRowInPage	=	Integer.parseInt(noOfRow);
		int start 			= 	((pgNo-1)*noOfRowInPage);
		int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		
		end=noOfRowInPage;
		
		
		
		/*if(searchTerm!=null && !searchTerm.trim().equals(""))
		wildCardMatch(searchTerm, tmRecords, map,j,from,jobIdMap);
		*/
		
		
		
		ClientConfig config = new DefaultClientConfig();
		URI uri=null;
	    Client client = Client.create(config);
	    List<String> list=new ArrayList<String>();
	    try
	    {
	    	
	   
	    try {
			uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/document/_search?pretty");
		} catch (URISyntaxException e) {
			
			e.printStackTrace();
		}
	    WebResource service = client.resource(uri);
	    ClientResponse response = null;
	    
	    
	    StringBuffer input =new StringBuffer();
	   
	    //input.append("{\"query\": { \"match_all\": {} },\"size\":10,\"from\":"+from+",\"sort\": { \"createdDateTime\": { \"order\": \"desc\" }}}");
	    input.append("{\"query\": { \"match_all\": {} },\"size\":"+end+",\"from\":"+start+",\"sort\": { \"createdDateTime\": { \"order\": \"desc\" }}}");
	    Calendar c = Calendar.getInstance(); 
	    if(months!=0)
  		{
  			
			 
			c.add(Calendar.MONTH, -+months);
			//input+= "\"must\" : {\"range\" : {\"jobstartdate\" : {\"from\" : \""+c.get(Calendar.YEAR)+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.DATE)+"T00:00:00.000+05:30\",\"to\" : \"2099-04-14T00:00:00.000+05:30\"}}},";
			
		/*	input="{\"query\": {\"filtered\": {\"query\": {\"match_all\": {}}"
					+ ",\"filter\": {\"range\": "
					+ "{\"createdDateTime\": {\"gte\": \""+c.get(Calendar.YEAR)+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.DATE)+"T00:00:00.000+05:30\"}}}}}"
		    		+ ",\"size\":100000}";*/
			
  		}
	   // input="{\"query\": {\"filtered\": {\"query\": {\"match_all\": {}},\"filter\": {\"range\": {\"jobstartdate\": {\"gte\": \"2014-09-30T00:00:00.000+05:30\"}}}}}"
	    //		+ ",\"size\":100000}";
	    //input="{\"query\": {\"bool\": {\"must\": [{\"wildcard\": {\"districtName\": \"*Nobl*\"}}]}}}";
	    //input="{\"query\": {\"bool\": {\"should\": [{\"wildcard\": {\"jobtitle\": \"*Rau*\"}},{\"wildcard\": {\"subjectName\": \"*Eng*\"}}],\"minimum_should_match\" : \"100%\"}}}";
	    //input="{\"query\": {\"bool\": { \"should\" : [{\"term\" : {\"jobtitle\" : \"Rauner\"}}, { \"term\" :{\"districtName\" : \"Noble\"} } ]}}}";
	 
	    if((searchTerm!=null && !searchTerm.trim().equals(""))
	    		|| (districtName!=null && !districtName.trim().equals(""))
	    		|| (stateName!=null && (!stateName.trim().equals("")) && (!stateName.trim().equalsIgnoreCase("ALL")))
	    		|| (cityName!=null && (!cityName.trim().equals("")) && (!cityName.trim().equals("0")))
	    		|| (schoolName!=null && !schoolName.trim().equals(""))
	    		|| (certificateTypeMaster!=null && !certificateTypeMaster.trim().equals(""))
	    		|| (zipCode!=null && !zipCode.trim().equals(""))
	    		|| (months!=0)
	    		|| (subjectIdList!=null && !subjectIdList.trim().equals("") && !subjectIdList.trim().equals("0"))
	    		)
	    {
	    	input.delete(0, input.length());
	    }
	  // if(searchTerm!=null && !searchTerm.trim().equals(""))
	    if((searchTerm!=null && !searchTerm.trim().equals(""))
	    		|| (districtName!=null && !districtName.trim().equals(""))
	    		|| (stateName!=null && (!stateName.trim().equals("")) && (!stateName.trim().equalsIgnoreCase("ALL")))
	    		|| (cityName!=null && (!cityName.trim().equals("")) && (!cityName.trim().equals("0")))
	    		|| (schoolName!=null && !schoolName.trim().equals(""))
	    		|| (certificateTypeMaster!=null && !certificateTypeMaster.trim().equals(""))
	    		|| (zipCode!=null && !zipCode.trim().equals(""))
	    		|| (months!=0)
	    		|| (subjectIdList!=null && !subjectIdList.trim().equals("") && !subjectIdList.trim().equals("0"))
	    		)
	  {
		 //  input.delete(0, input.length());
		  input.append("{\"query\": {\"bool\": {");
		  input.append(" \"must\" : [");
					if(districtName!=null && !districtName.trim().equals(""))
					{
						if(districtId==null || districtId.trim().equals(""))
							districtId="0";
						input.append("   {\"match_phrase\" : { \"districtName\" : \""+districtName+"\" }},");
						input.append("   {\"match_phrase\" : { \"districtid\" : \""+districtId+"\" }},");
						//input.append("\"match\":{ \"districtName\": { \"query\":    \""+districtName+"\", \"operator\": \"and\" }},");
					}
					
					if( (months!=0))
					input.append("{\"range\": {\"createdDateTime\": {\"gte\": \""+c.get(Calendar.YEAR)+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.DATE)+"T00:00:00.000+05:30\"}}},");
					if(stateName!=null && (!stateName.trim().equals("")) && (!stateName.trim().equalsIgnoreCase("ALL")))
					{
						input.append("   {\"match_phrase\" : { \"stateName\" : \""+stateName+"\" }},");
					}
					if(cityName!=null && (!cityName.trim().equals("")) && (!cityName.trim().equals("0")))
					{
						input.append("   {\"match_phrase\" : { \"cityname\" : \""+cityName+"\" }},");
					}
					if(schoolName!=null && !schoolName.trim().equals(""))
					{
						input.append("   {\"match_phrase\" : { \"schoolName\" : \""+schoolName+"\" }},");
					}
					if(certificateTypeMaster!=null && !certificateTypeMaster.trim().equals(""))
					{
						input.append("   {\"match_phrase\" : { \"certType\" : \""+certificateTypeMaster+"\" }},");
					}
					if(subjectIdList!=null && !subjectIdList.trim().equals("") && !subjectIdList.trim().equals("0") && !subjectIdList.equalsIgnoreCase("ALL,"))
					{
						//input.append("{ \"match\": { \"subjectName\": \""+subjectIdList+"\"   }},");
						input.append("{ \"match\": { \"subjectId\": \""+subjectIdList+"\"   }},");
					}
					
					if(zipCode!=null && (!zipCode.trim().equals("")))
					{
						input.append("   {\"match_phrase\" : { \"zipCode\" : \""+zipCode+"\" }},");
					}
					if(input.indexOf(",")!=-1)
					input.replace(input.lastIndexOf(","), input.lastIndexOf(",")+1, "");
					input.append("],"
					//		+ "\"must_not\": { \"match\": { \"jobid\":  7188 or 6657 }},"
							+ "");
				  
					 
			    		     
					input.append("\"should\": [{"
							 +"  \"multi_match\" : {"
			    		        +"    \"fields\" : [\"zipCode\",\"jobtitle\",\"cityname\",\"stateName\",\"schoolName\",\"districtName\",\"subjectName\",\"distaddress\", \"jobDescription\",\"certType\"],"
			    		      +"      \"query\" : \""+searchTerm+"\","
			    		    +"        \"type\" : \"phrase_prefix\""
			    		  +"      }"
			    		+"    },"
			  			+ "{ \"match\": { \"jobtitle\":  \""+searchTerm+"\" }},"
				  		+ "{ \"match\": { \"cityname\": \""+searchTerm+"\"   }},"
				  		+ "{ \"match\": { \"zipCode\": \""+searchTerm+"\"   }},"
				  		+ "{ \"match\": { \"distaddress\": \""+searchTerm+"\"   }},"
				  		+ "{ \"match\": { \"jobDescription\": \""+searchTerm+"\"   }},"
				  		+ "{ \"match\": { \"schoolName\": \""+searchTerm+"\"   }},"
				  		+ "{ \"match\": { \"certType\": \""+searchTerm+"\"   }},"
				  		+ "{ \"match\": { \"subjectName\": \""+searchTerm+"\"   }},"
				  		+ "{ \"match\": { \"stateName\": \""+searchTerm+"\"   }},"
				  		+ "{ \"match\": { \"districtName\": \""+searchTerm+"\"}}"
			  		+ "],");
			  		//+ "\"must\" : {\"match\" : { \"schoolName\" : \""+searchTerm+"\" }},"
			  if(months!=0)
		  		{
		  			
					//Calendar c = Calendar.getInstance(); 
					//c.add(Calendar.MONTH, -+months);
					//input+= "\"must\" : {\"range\" : {\"createdDateTime\" : {\"from\" : \""+c.get(Calendar.YEAR)+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.DATE)+"T00:00:00.000+05:30\",\"to\" : \"2099-04-14T00:00:00.000+05:30\"}}},";
		  		}
			  	
			  if(searchTerm==null || searchTerm.trim().equals(""))
			  {
				  //input.append("\"minimum_should_match\" : \"1%\"}},\"size\":10,\"from\":"+from+"},\"sort\": { \"createdDateTime\": { \"order\": \"desc\" }}");
				  input.append("\"minimum_should_match\" : \"1%\"}},\"size\":"+end+",\"from\":"+start+",\"sort\": { \"createdDateTime\": { \"order\": \"desc\" }}}");
			  }
			  		
			  		else
			  		{
			  			//input.append("\"minimum_should_match\" : 1}},\"size\":10,\"from\":"+from+"},\"sort\": { \"createdDateTime\": { \"order\": \"desc\" }}");
			  			input.append("\"minimum_should_match\" : 1}},\"size\":"+end+",\"from\":"+start+",\"sort\": { \"createdDateTime\": { \"order\": \"desc\" }}}");
			  		}
						
			//  input.append("\"minimum_should_match\" : 1}},\"size\":100000}");
	  }
	   //tommorow we will see
/*	String matchPhrase="{"
    		   +" \"query\" : {"
    		      +"  \"multi_match\" : {"
    		        +"    \"fields\" : [\"jobtitle\",\"cityname\",\"schoolName\",\"districtName\", \"jobDescription\"],"
    		      +"      \"query\" : \""+searchTerm+"\","
    		    +"        \"type\" : \"phrase_prefix\""
    		  +"      }"
    		+"    }"
    		+"}";
response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, matchPhrase);
*/	    //input="{\"filtered\": {\"query\": {\"match\": {\"districtName\": \"SYRACUSE\" }},\"filter\": {\"match\": {\"jobtitle\": \"Rauner\" }}}}";
	    //input = "{\"query\": { \"match\": {\"districtName\":\"DISTRICT\"} },\"size\":30}";
	  //System.out.println(input.toString());
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input.toString());
	    
	   // System.out.println("Form response11 " + response.getEntity(String.class));
	    String s=response.getEntity(String.class);
	    //System.out.println(s);
	    //System.out.println("***********************************************************");
	    
	  //  JSONParser jsonParser =new JSONParser();
	   // JSONObject jsonObject;
	    
	    net.sf.json.JSONObject jsonObject = new net.sf.json.JSONObject();
	    String untillCheck="";
	    if(tmRecords.length()<2)
	    tmRecords.append("<div  class=''  style='padding:2px; background-color:white; margin-left:0px;width:100%;min-height:0px;' >");
		try {
		    jsonObject=jsonObject.fromObject(s);
			//jsonObject = (JSONObject) jsonParser.parse(s);
			//System.out.println(((JSONObject)jsonObject.get("hits")));
			 //System.out.println(((JSONObject)jsonObject.get("hits")).get("hits"));
		    System.out.println("total searchbydocument size=="+((net.sf.json.JSONObject)jsonObject.get("hits")).get("total"));
		    int total=((net.sf.json.JSONObject)jsonObject.get("hits")).getInt("total");
		    net.sf.json.JSONArray hits= (net.sf.json.JSONArray) ((net.sf.json.JSONObject)jsonObject.get("hits")).get("hits");
			 
			 for(int i=0; i<hits.size(); i++)
			 {
				 
				 
				//System.out.println("The " + i + " element of the array: "+hits.get(i));
				jsonObject=(net.sf.json.JSONObject)hits.get(i);
				/*System.out.print("index="+jsonObject.get("_index")+"\t");
				System.out.print("type="+jsonObject.get("_type")+"\t");
				System.out.print("id="+jsonObject.get("_id")+"\t");*/
				String index=(String)jsonObject.get("_index");
				String type=(String)jsonObject.get("_type");
				String id=(String)jsonObject.get("_id");
				
				jsonObject=(net.sf.json.JSONObject)jsonObject.get("_source");
			//	System.out.println(jsonObject);
				jobIdMap.put(Integer.parseInt(jsonObject.get("jobid").toString()),Integer.parseInt(jsonObject.get("jobid").toString()));
				/*System.out.print("json={\"jobid\":"+jsonObject.get("jobid")+",");
				System.out.print("\"districtid\":"+jsonObject.get("districtid")+",");
				System.out.print("\"jobtitle\":\""+jsonObject.get("jobtitle")+"\",");
				System.out.print("\"jobstartdate\":\""+jsonObject.get("jobstartdate")+"\",");
				System.out.print("\"jobenddate\":\""+jsonObject.get("jobenddate")+"\",");
				System.out.print("\"subjectName\":\""+jsonObject.get("subjectName")+"\",");
				System.out.print("\"cityname\":\""+jsonObject.get("cityname")+"\",");
				System.out.print("\"zipCode\":"+jsonObject.get("zipCode")+",");
				System.out.print("\"schoolName\":\""+jsonObject.get("schoolName")+"\"");
				System.out.println("}");*/
			//	System.out.println("jobId=="+jsonObject.get("jobid"));
				
				StringBuilder json=new StringBuilder();
				//json.append("{\"jobid\":"+jsonObject.get("jobid")+",");
				//json.append("\"districtid\":"+jsonObject.get("districtid")+",");
				json.append("<b>\"jobtitle\":\"</b>"+jsonObject.get("jobtitle")+"\",");
				json.append("\"jobstartdate\":\""+jsonObject.get("jobstartdate")+"\",");
				json.append("\"jobenddate\":\""+jsonObject.get("jobenddate")+"\",");
				json.append("\"subjectName\":\""+jsonObject.get("subjectName")+"\",");
				json.append("\"cityname\":\""+jsonObject.get("cityname")+"\",");
				json.append("\"districtName\":\""+jsonObject.get("districtName")+"\",");
				json.append("\"zipCode\":"+jsonObject.get("zipCode")+",");
				json.append("\"zip\":"+jsonObject.get("zip")+",");
				json.append("\"schoolName\":\""+jsonObject.get("schoolName")+"\"");
				
				json.append("}<br>");
				list.add(json.toString().replaceAll("T00:00:00.000+05:30",""));
			//	System.out.println(json.toString().replaceAll("T00:00:00.000+05:30",""));
				
				//--------------------------------------------------------------------------

				
				
			//	++i;
			//	System.out.print(map.get(Integer.parseInt(jsonObject.get("jobid").toString()))==null);
			//	System.out.println("\t"+jsonObject.get("jobid").toString()+"\t"+map.get(Integer.parseInt(jsonObject.get("jobid").toString())));
				//if(map.get(Integer.parseInt(jsonObject.get("jobid").toString()))==null)
				{
					
				
					tmRecords.append("<div  class=''  style='padding:8px; background-color:#EEEEEE;width:100%;color:black;box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.4);border-radius: 8px;' >");
					tmRecords.append("<span style='font-size:14px;'><b class='titlecolor'>"+jsonObject.get("jobtitle")+" </b></span><br/>");
					Date startDate = new Date();
					Date endDate = new Date();
					//System.out.println("startdate===="+jsonObject.get("jobstartdate")+"\tenddate==="+jsonObject.get("jobenddate"));
					//startDate = new Date(jsonObject.get("jobstartdate").toString().substring(0, 10));
					//endDate = new Date(jsonObject.get("jobenddate").toString().substring(0, 10));
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
					startDate=sdf.parse(jsonObject.get("jobstartdate").toString().substring(0, 10));
					endDate=sdf.parse(jsonObject.get("jobenddate").toString().substring(0, 10));
					
					untillCheck = Utility.convertDateAndTimeToUSformatOnlyDate(endDate);
					
					tmRecords.append("<div class='pull-right' style='border: 0px solid green;font-weight:bold;'>Job posted as of "+Utility.convertDateAndTimeToUSformatOnlyDate(startDate)+",12:01 AM CST "+(untillCheck.indexOf("2099")!=-1?"untill filled":untillCheck)+"</div>");
					tmRecords.append("<div style='font-weight:bold;' >"+jsonObject.get("districtName")+"</div>");
					tmRecords.append("<div style=''>");
					//tmRecords.append(""+jsonObject.get("distaddress")+"");
					tmRecords.append(getESDistrictAddress(jsonObject));
					tmRecords.append("</div>");	

			String certs = jsonObject.getString("certType");
			
			if(!certs.contains("[null") && !certs.contains("[]"))
			{
				
				//certs=certs.replaceAll("\\[", "");
				//certs=certs.replaceAll("\\]", "");
				certs=certs.replaceAll("\"","");
				certs=certs.substring(1, certs.lastIndexOf("]"));
				tmRecords.append("<div style='' ><b>Certification(s) Required: </b><br>"+certs+"</div>");
			}
			
		
			if(!jsonObject.getString("geoZoneName").trim().equals("")){
				tmRecords.append("<div style=''><b>Zone: </b>");
							if(jsonObject.get("districtid").toString().equals("1200390")){
							        tmRecords.append("<a href='javascript:void(0);' onclick='showMiamiPdf();'>"+jsonObject.getString("geoZoneName")+"</a>");
							}else{
								tmRecords.append("<a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jsonObject.get("geoZoneId")+",'"+jsonObject.getString("geoZoneName")+"',"+jsonObject.get("districtid")+")\";>"+jsonObject.getString("geoZoneName")+"</a>");
							}					
							tmRecords.append("</div>");
							
						}			
					if(jsonObject.get("jobDescription")!=null && jsonObject.get("jobDescription").toString().length()>0)
						tmRecords.append("<div  class='titlecolor'><b>Details: </b><br></div>");

					tmRecords.append("<table style=''><tr><td style=''>");
					tmRecords.append("<div style=''>");	
					if(jsonObject.get("jobDescription")!=null && jsonObject.get("jobDescription").toString().length()>0)
					{
						tmRecords.append("<div id='des"+j+"' style='display:none;'>"+jsonObject.get("jobDescription").toString()+"</div> ");
					}
						
					else
						tmRecords.append("<div id='des"+j+"' style='display:none;'></div>");				
					
					tmRecords.append("<div id='desShow"+j+"' style='display:none;'></div>");

					tmRecords.append("</div>");
					if(jsonObject.get("jobDescription")!=null && jsonObject.get("jobDescription").toString().length()>0)
						tmRecords.append("<div>"+jsonObject.get("jobDescription").toString()+"</div><br/>");
										
					tmRecords.append("</td><td style='' valign=\"bottom\">");
					tmRecords.append("</td></tr></table>");
					
					if(ElasticSearchConfig.serverName!=null && ElasticSearchConfig.serverName.equalsIgnoreCase("platform") && 
							jsonObject.containsKey("serverName") && 
							jsonObject.getString("serverName").equalsIgnoreCase("NC"))
					{
						//redirect to NC url
						//tmRecords.append("<div style=\"height:30px;\"><a href=\"https://nc.teachermatch.org/applyteacherjob.do?jobId="+jsonObject.get("jobid")+"\"><button class=\"btn btn-primary\" type=\"button\" style=\"float:right;right:0;\"><strong>Apply <i class=\"icon\"></i></strong></button></a></div>");
						//tmRecords.append("<div style=\"height:30px;\"><a href=\"https://cloud.teachermatch.org/applyteacherjob.do?jobId="+jsonObject.get("jobid")+"\"><button class=\"btn btn-primary\" type=\"button\" style=\"float:right;right:0;\"><strong>Apply <i class=\"icon\"></i></strong></button></a></div>");
						tmRecords.append("<div style=\"height:30px;\"><a href=\""+ElasticSearchConfig.serviceURL+"/applyteacherjob.do?jobId="+jsonObject.get("jobid")+"\"><button class=\"btn btn-primary\" type=\"button\" style=\"float:right;right:0;\"><strong>Apply <i class=\"icon\"></i></strong></button></a></div>");
					}
					else
						tmRecords.append("<div style=\"height:30px;\"><a href=\"applyteacherjob.do?jobId="+jsonObject.get("jobid")+"\"><button class=\"btn btn-primary\" type=\"button\" style=\"float:right;right:0;\"><strong>Apply <i class=\"icon\"></i></strong></button></a></div>");
					tmRecords.append("</div>");				
					tmRecords.append("<div class='span10 jboardFooter' style='height:5px;background-color: white;  border: 0px hidden white;'><br/>");
					tmRecords.append("</div>");
					j++;
				}
				
				map.put(Integer.parseInt(jsonObject.get("jobid").toString()), Integer.parseInt(jsonObject.get("jobid").toString()));
				//---------------------------------------------------------------------------

			}
		 System.out.println("terecords===="+map.size());
			 //=============================================
			 tmRecords.append("<input type='hidden' id='rowsappended' value="+(--j)+">");
			 	if(map.size()==0 && from==0){
					//tmRecords.append("<div  class='span16' style='height:20px;padding-left: 10px; width: 970px; border: 0px hidden white; background-color: white; margin-left: -5px; ' >");
					//tmRecords.append("No Records Found</div>");
//			 		tmRecords.append("<div class='' style='padding:2px; background-color:#D3E8F4; margin-left:0px;width: 98%;min-height:20px;border-radius: 15px;'><input type='hidden' id='rowsappended' value='0'><div class='span16' style='height:20px;padding-left: 10px; width: 100%; border: 0px hidden white; background-color: #D3E8F4; margin-left: -0px; '><b> South San Antonio ISD: TEACHER - ELEMENTARY SCHOOL (GRADES K-5) - POTENTIAL OPENINGS FOR 2014-2015 SCHOOL YEAR Job posted as of Jun 22, 2014,12:01 AM CST untill filled SOUTH SAN ANTONIO ISD 5622 RAY ELLISON BLVD, SAN ANTONIO, Texas, 78242</b></div></div>");
			 		tmRecords.append("<div style='height: 50px;;border-radius:8px;box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.4);background-color:#EEEEEE;'>");
			 		tmRecords.append("<div style='margin-left: 10px;padding-top: 10px;'><b>No Records Found</b></div>");
			 		tmRecords.append("</div>");
			 		
				}
				tmRecords.append("</div>");
				tmRecords.append("</div>");
				//tmRecords.append("<div style='height:10px;'></div>");
				tmRecords.append("<script>jobBoardFooter();</script>");
				WebContext context;
				context = WebContextFactory.get();
				HttpServletRequest request = context.getHttpServletRequest();
				String baseurl = Utility.getBaseURL(request);
				//border: 2px solid;border-radius: 25px;width:100%;border: 0px hidden rgb(216, 53, 53); background-color: red; border-style: solid; margin-left: 0px; margin-top: -10px;border-radius:0px 0px 2em 2em !important;
				//tmRecords.append("<div id='jobBoardFooter' style='border: 2px solid;border-radius: 25px;width:100%;border: 0px hidden white; background-color: white; border-style: solid; margin-left: 0px; margin-top: -10px;'>"+PaginationAndSorting.getESPaginationStringForJobboard(request,total,noOfRow, pageNo)+"</div>");
				//tmRecords.append("<div id='jobBoardFooter' style='border: 2px solid;border-radius: 25px;width:100%;border: 0px hidden rgb(216, 53, 53); background-color: #ECF5FA; border-style: solid; margin-left: 0px; margin-top: -10px;border-radius:0px 0px 2em 2em !important;box-shadow: inset 0 0 2px rgba(0, 0, 0, 0.6);border-radius: 8px;margin-left:-3px;'>"+PaginationAndSorting.getESPaginationStringForJobboard(request,total,noOfRow, pageNo)+"</div>");
				tmRecords.append("<div id='jobBoardFooter' style='border: 2px solid;border-radius: 25px;width:592px;border: 0px hidden rgb(216, 53, 53); background-color: white; border-style: solid; margin-left: 0px; margin-top: -10px;border-radius:0px 0px 2em 2em !important;box-shadow: inset 0 0 2px rgba(0, 0, 0, 0.6);border-radius: 8px;margin-left:2px;'>"+PaginationAndSorting.getESPaginationStringForJobboard(request,total,noOfRow, pageNo)+"</div>");
				
				//tmRecords.append("<div id='jobBoardFooter' style='width:100%;border: 0px hidden white; background-color: white; border-style: solid; margin-left: 0px; margin-top: -10px;'></div>");
			 //==============================================
			
			 //return tmRecords.toString()+"@@@"+result;
			 //System.out.println("tmrecord========"+tmRecords.toString());
				System.out.println("list size==="+hits.size());
			 //return tmRecords.toString()+"@@@"+getResult();
				//System.out.println("jobid============="+jobIdMap);
				
				
				//********************************************new query for map display
				
				if(pageNo.equals("1"))
				{
					input =new StringBuffer();
					input.append("{\"query\": { \"match_all\": {} },\"size\":100000,\"from\":0,\"_source\": [\"jobid\",\"latitude\",\"districtName\",\"stateName\",\"longitude\",\"countryName\"]}");
				    c = Calendar.getInstance(); 
				    if(months!=0)
			  		{
			  			c.add(Calendar.MONTH, -+months);
										
			  		}
				    if((searchTerm!=null && !searchTerm.trim().equals(""))
				    		|| (districtName!=null && !districtName.trim().equals(""))
				    		|| (stateName!=null && (!stateName.trim().equals("")) && (!stateName.trim().equalsIgnoreCase("ALL")))
				    		|| (cityName!=null && (!cityName.trim().equals("")) && (!cityName.trim().equals("0")))
				    		|| (schoolName!=null && !schoolName.trim().equals(""))
				    		|| (certificateTypeMaster!=null && !certificateTypeMaster.trim().equals(""))
				    		|| (zipCode!=null && !zipCode.trim().equals(""))
				    		|| (months!=0)
				    		|| (subjectIdList!=null && !subjectIdList.trim().equals("") && !subjectIdList.trim().equals("0"))
				    		)
				    {
				    	input.delete(0, input.length());
				    }
				    if((searchTerm!=null && !searchTerm.trim().equals(""))
				    		|| (districtName!=null && !districtName.trim().equals(""))
				    		|| (stateName!=null && (!stateName.trim().equals("")) && (!stateName.trim().equalsIgnoreCase("ALL")))
				    		|| (cityName!=null && (!cityName.trim().equals("")) && (!cityName.trim().equals("0")))
				    		|| (schoolName!=null && !schoolName.trim().equals(""))
				    		|| (certificateTypeMaster!=null && !certificateTypeMaster.trim().equals(""))
				    		|| (zipCode!=null && !zipCode.trim().equals(""))
				    		|| (months!=0)
				    		|| (subjectIdList!=null && !subjectIdList.trim().equals("") && !subjectIdList.trim().equals("0"))
				    		)
				  {
					 
					  input.append("{\"query\": {\"bool\": {");
					  input.append(" \"must\" : [");
								if(districtName!=null && !districtName.trim().equals(""))
								{
									if(districtId==null || districtId.trim().equals(""))
										districtId="0";
									input.append("   {\"match_phrase\" : { \"districtName\" : \""+districtName+"\" }},");
									input.append("   {\"match_phrase\" : { \"districtid\" : \""+districtId+"\" }},");
								}
								
								if( (months!=0))
								input.append("{\"range\": {\"createdDateTime\": {\"gte\": \""+c.get(Calendar.YEAR)+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.DATE)+"T00:00:00.000+05:30\"}}},");
								if(stateName!=null && (!stateName.trim().equals("")) && (!stateName.trim().equalsIgnoreCase("ALL")))
								{
									input.append("   {\"match_phrase\" : { \"stateName\" : \""+stateName+"\" }},");
								}
								if(cityName!=null && (!cityName.trim().equals("")) && (!cityName.trim().equals("0")))
								{
									input.append("   {\"match_phrase\" : { \"cityname\" : \""+cityName+"\" }},");
								}
								if(schoolName!=null && !schoolName.trim().equals(""))
								{
									input.append("   {\"match_phrase\" : { \"schoolName\" : \""+schoolName+"\" }},");
								}
								if(certificateTypeMaster!=null && !certificateTypeMaster.trim().equals(""))
								{
									input.append("   {\"match_phrase\" : { \"certType\" : \""+certificateTypeMaster+"\" }},");
								}
								if(subjectIdList!=null && !subjectIdList.trim().equals("") && !subjectIdList.trim().equals("0") && !subjectIdList.equalsIgnoreCase("ALL,"))
								{
									//input.append("{ \"match\": { \"subjectName\": \""+subjectIdList+"\"   }},");
									input.append("{ \"match\": { \"subjectId\": \""+subjectIdList+"\"   }},");
								}
								
								if(zipCode!=null && (!zipCode.trim().equals("")))
								{
									input.append("   {\"match_phrase\" : { \"zipCode\" : \""+zipCode+"\" }},");
								}
								if(input.indexOf(",")!=-1)
								input.replace(input.lastIndexOf(","), input.lastIndexOf(",")+1, "");
								input.append("],"
					
										+ "");
							  
								input.append("\"should\": [{"
										 +"  \"multi_match\" : {"
						    		        +"    \"fields\" : [\"zipCode\",\"jobtitle\",\"cityname\",\"stateName\",\"schoolName\",\"districtName\",\"subjectName\",\"distaddress\", \"jobDescription\",\"certType\"],"
						    		      +"      \"query\" : \""+searchTerm+"\","
						    		    +"        \"type\" : \"phrase_prefix\""
						    		  +"      }"
						    		+"    },"
						  			+ "{ \"match\": { \"jobtitle\":  \""+searchTerm+"\" }},"
							  		+ "{ \"match\": { \"cityname\": \""+searchTerm+"\"   }},"
							  		+ "{ \"match\": { \"zipCode\": \""+searchTerm+"\"   }},"
							  		+ "{ \"match\": { \"distaddress\": \""+searchTerm+"\"   }},"
							  		+ "{ \"match\": { \"jobDescription\": \""+searchTerm+"\"   }},"
							  		+ "{ \"match\": { \"schoolName\": \""+searchTerm+"\"   }},"
							  		+ "{ \"match\": { \"certType\": \""+searchTerm+"\"   }},"
							  		+ "{ \"match\": { \"subjectName\": \""+searchTerm+"\"   }},"
							  		+ "{ \"match\": { \"stateName\": \""+searchTerm+"\"   }},"
							  		+ "{ \"match\": { \"districtName\": \""+searchTerm+"\"}}"
						  		+ "],");
						  		
						   if(searchTerm==null || searchTerm.trim().equals(""))
						  {
							  input.append("\"minimum_should_match\" : \"1%\"}},\"size\":100000,\"from\":0,\"_source\": [\"jobid\",\"latitude\",\"districtName\",\"stateName\",\"longitude\",\"countryName\"]}");
						  }
						  		
					  		else
					  		{
					  			input.append("\"minimum_should_match\" : 1}},\"size\":100000,\"from\":0,\"_source\": [\"jobid\",\"latitude\",\"districtName\",\"stateName\",\"longitude\",\"countryName\"]}");
					  		}
									
						
				  }
				  System.out.println("for geomapping");
				  //System.out.println(input.toString());
				  //if(input.indexOf("match_all")==-1)
				  {
					  response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input.toString());
					  s=response.getEntity(String.class);
					  //System.out.println(s);
					  jsonObject = new net.sf.json.JSONObject();
					    untillCheck="";
					   
						try {
						    jsonObject=jsonObject.fromObject(s);
							//System.out.println("total searchbydo=="+((net.sf.json.JSONObject)jsonObject.get("hits")).get("total"));
						    total=((net.sf.json.JSONObject)jsonObject.get("hits")).getInt("total");
						    hits= (net.sf.json.JSONArray) ((net.sf.json.JSONObject)jsonObject.get("hits")).get("hits");
						    jobIdMap=new HashMap<Integer, Integer>();
							 for(int i=0; i<hits.size(); i++)
							 {
								jsonObject=(net.sf.json.JSONObject)hits.get(i);
								jsonObject=(net.sf.json.JSONObject)jsonObject.get("_source");
								jobIdMap.put(Integer.parseInt(jsonObject.get("jobid").toString()),Integer.parseInt(jsonObject.get("jobid").toString()));
								result = result + jsonObject.get("districtName").toString()+", "+jsonObject.get("stateName").toString()+", "+jsonObject.get("countryName").toString()
								+"---"+jsonObject.get("latitude").toString()+","+jsonObject.get("longitude").toString()+"||";
							 
							 }
						}
							 catch(Exception e)
							 {
								 e.printStackTrace();
							 }
				  }
				  
					
					//end
				  //COMMENT 14-10-2015
					/*try
					{
						//if(from==0)
						{
							//System.out.println("searching geolocationsssss");
							SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");    
							Date dateWithoutTime;
							dateWithoutTime = sdf1.parse(sdf1.format(new Date()));
							Criterion criterion1 = Restrictions.eq("status","A");
							Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
							Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
							Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
							Criterion criterion5 = Restrictions.in("jobId",jobIdMap.keySet());
							//Restrictions.in(jobId, values)
							
							List<String> locations= new ArrayList<String>();
							
							List<JobOrder> mapLoc =null;
						//	System.out.println("*********************************");
							if(jobIdMap.size()>0)
							{
								if(input.indexOf("match_all")==-1)
								{
									mapLoc=jobOrderDAO.findAllActiveJobForLocationsES(criterion1,criterion2,criterion3,criterion4,criterion5);
								}
								else
								{
									mapLoc=jobOrderDAO.findAllActiveJobForLocations(criterion1,criterion2,criterion3,criterion4);
								}
								
							}
								
							if(mapLoc!=null && mapLoc.size()>0)
							{
								for(JobOrder jOrder : mapLoc)
								{
									if(jOrder.getDistrictMaster()!=null && !locations.contains(jOrder.getDistrictMaster().getDistrictName()+", "+jOrder.getDistrictMaster().getStateId().getStateName()+", "+jOrder.getDistrictMaster().getStateId().getCountryMaster().getName()))
									{
										locations.add(jOrder.getDistrictMaster().getDistrictName()+", "+jOrder.getDistrictMaster().getStateId().getStateName()+", "+jOrder.getDistrictMaster().getStateId().getCountryMaster().getName());
										//result = result + jOrder.getDistrictMaster().getDistrictName()+", "+jOrder.getDistrictMaster().getStateId().getStateName()+", "+jOrder.getDistrictMaster().getStateId().getCountryMaster().getName()
										//			+"---"+jOrder.getDistrictMaster().getLatitude()+","+jOrder.getDistrictMaster().getLongitude()+"||";
										
									}									
								}
							}
						}
						
						
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}*/
					
					//new
					/*System.out.println("==============================================");
					for(int i=0;i<result.split("||").length;i++)
					{
						service = client.resource("https://maps.googleapis.com/maps/api/geocode/json?address="+result.split("||")[i]+"&sensor=false");
						response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class);
						s=response.getEntity(String.class);
						jsonObject = new net.sf.json.JSONObject();
						System.out.println(s);
						//jsonObject=jsonObject.fromObject(s);
						//System.out.println(jsonObject);
						hits= (net.sf.json.JSONArray) ((net.sf.json.JSONObject)jsonObject.get("hits")).get("hits");
					    jobIdMap=new HashMap<Integer, Integer>();
						jsonObject=(net.sf.json.JSONObject)hits.get(i);
						jsonObject=(net.sf.json.JSONObject)jsonObject.get("_source");
						jobIdMap.put(Integer.parseInt(jsonObject.get("jobid").toString()),Integer.parseInt(jsonObject.get("jobid").toString()));
						
							
					}*/
					
				    
					
					
					//new end
					
				}
				//System.out.println(result);
				//result="78210||78242";
			//result="PHILADELPHIA, Pennsylvania, United States||Noble, Illinois, United States||SYRACUSE, New York, United States||MICCOSUKEE, Florida, United States";
				return tmRecords.toString()+"@@@"+from+"@@@"+result+"@@@"+address;
				//return tmRecords.toString()+"@@@"+from;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			tmRecords.append("<div style='height: 50px;;border-radius:8px;box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.4);background-color:#EEEEEE;'>");
	 		tmRecords.append("<div style='margin-left: 10px;padding-top: 10px;'><b>No Records Found</b></div>");
	 		tmRecords.append("</div>");
			e.printStackTrace();
		}
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	  // return list;
		//return tmRecords.toString()+"@@@"+getResult();
	    return tmRecords.toString()+"@@@"+"";

	}

	
	public String getESDistrictAddress(JSONObject jsonObject)
	{
		String districtAddress="";
		String address="";
		String state="";
		String city = "";
		String zipcode="";
		
		//Get district for ElasticSearch address
		if(!jsonObject.getString("districtName").equals("null") && !jsonObject.getString("districtName").trim().equals(""))
		{
			if(!jsonObject.getString("distaddress").equals("null") && !jsonObject.getString("distaddress").trim().equals(""))
			{
				if(!jsonObject.getString("cityname").equals(""))
				{
					districtAddress = jsonObject.getString("distaddress")+", ";
				}
				else
				{
					districtAddress = jsonObject.getString("distaddress");
				}
			}
			if(!jsonObject.getString("cityname").equals(""))
			{
				if(!jsonObject.getString("stateName").equals("null") && !jsonObject.getString("stateName").equals(""))
				{
					city = jsonObject.getString("cityname")+", ";
				}else{
					city = jsonObject.getString("cityname");
				}
			}
			if(!jsonObject.getString("stateName").equals("null") && !jsonObject.getString("stateName").equals(""))
			{
				//System.out.println(jobOrderDetails.getDistrictMaster().getDistrictId()+" jobOrderDetails.getDistrictMaster().getStateId() :: "+jobOrderDetails.getDistrictMaster().getStateId().getStateName());
				
				if(!jsonObject.getString("zipCode").equals("null") && !jsonObject.getString("zipCode").equals(""))
				{
					state = jsonObject.getString("stateName")+", ";
				}
				else
				{
					state = jsonObject.getString("stateName");
				}	
			}
			if(!jsonObject.getString("zipCode").equals("null") && !jsonObject.getString("zipCode").equals(""))
			{
					zipcode = jsonObject.getString("zipCode");
			}
			
			if(districtAddress !="" || city!="" ||state!="" || zipcode!=""){
				address = districtAddress+city+state+zipcode;
			}else{
				address="";
			}
		}
	
		return address;
	}
	
	
	
	//end
	
	public String getResult()
	{
		String result="";
		List<String> locations= new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
	    Date dateWithoutTime;
		try 
		{
			dateWithoutTime = sdf.parse(sdf.format(new Date()));
			Criterion criterion1 = Restrictions.eq("status","A");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
			List<JobOrder> mapLoc = jobOrderDAO.findAllActiveJobForLocations(criterion1,criterion2,criterion3,criterion4);
			if(mapLoc!=null && mapLoc.size()>0)
			{
				for(JobOrder jOrder : mapLoc)
				{
					if(!locations.contains(jOrder.getDistrictMaster().getDistrictName()+", "+jOrder.getDistrictMaster().getStateId().getStateName()+", "+jOrder.getDistrictMaster().getStateId().getCountryMaster().getName()))
					{
						locations.add(jOrder.getDistrictMaster().getDistrictName()+", "+jOrder.getDistrictMaster().getStateId().getStateName()+", "+jOrder.getDistrictMaster().getStateId().getCountryMaster().getName());
						result = result + jOrder.getDistrictMaster().getDistrictName()+", "+jOrder.getDistrictMaster().getStateId().getStateName()+", "+jOrder.getDistrictMaster().getStateId().getCountryMaster().getName()+"||";
					}									
				}
			}
		
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
			
	}
	
	@Transactional(readOnly=true)
	public void getDBUSERByUserId(int userId)
	{
		
		try {
			Session session=jobOrderDAO.getSession();
			//List<Object[]> list=session.createSQLQuery("CALL getDBUSERByUserId(1,@firstname,@emailAddress,@password)")
			//List<Object[]> list=session.createSQLQuery("CALL getUserMaster(0)")
			List<UserMaster> list=session.createSQLQuery("CALL getUserMaster(0)")
			.addEntity(UserMaster.class)
			.list();
			/*for(Object o[]:list)
			{
				System.out.println(o[0]+"\t"+o[1]+"\t"+o[2]);
			}*/
			for(UserMaster userMaster:list)
			{
				System.out.println(userMaster.getFirstName()+"\t"+userMaster.getAuthenticationCode());
			}
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}


