package tm.services;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.master.CountryMaster;
import tm.bean.master.StateMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.utility.Utility;

public class StateAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private CountryMasterDAO countryMasterDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	
	public String[] getStateByCountry(Integer countryId,String stateModuleFlag)
	{
		System.out.println(" <<<<<<<<<< inside getStateByCountry >>>>>>>>>>>>");
		
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		List<StateMaster> lstStateMaster = new ArrayList<StateMaster>();
		StringBuffer stateOptions=	new StringBuffer();
		
		String[] dataArray = new String[5];
		TeacherPersonalInfo teacherPersonalInfo = new TeacherPersonalInfo();
		
		try
		{
			CountryMaster countryMaster = countryMasterDAO.findById(countryId, false, false);
			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
			
		    teacherPersonalInfo = teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
			
			lstStateMaster = stateMasterDAO.findActiveStateByCountryId(countryMaster);
			
			if(lstStateMaster.size()>0)
			{
				String basepath = Utility.getValueOfPropByKey("basePath");
				if(basepath.equalsIgnoreCase("https://canada-en-test.teachermatch.org/")){
					stateOptions.append("<option id='slst' value='' >Select Province</option>");
				}else{
					stateOptions.append("<option id='slst' value='' >"+Utility.getLocaleValuePropByKey("optSltSt", locale)+"</option>");
				}
				
			
				for(StateMaster st:lstStateMaster)
				{
					if(stateModuleFlag.equalsIgnoreCase("dspq"))
					{
						if(teacherPersonalInfo!=null)
						{
							if(teacherPersonalInfo.getStateId()!=null)
							{
								if(st.getStateId().equals(teacherPersonalInfo.getStateId().getStateId()))
									stateOptions.append("<option id='st__"+st.getStateId()+"' shortname='"+st.getStateShortName()+"' value='"+st.getStateId()+"' selected='selected'>"+st.getStateName()+"</option>");
								else
									stateOptions.append("<option id='st__"+st.getStateId()+"' shortname='"+st.getStateShortName()+"'  value='"+st.getStateId()+"' >"+st.getStateName()+"</option>");
							}
							else
								stateOptions.append("<option id='st__"+st.getStateId()+"' shortname='"+st.getStateShortName()+"'  value='"+st.getStateId()+"' >"+st.getStateName()+"</option>");
						}
						else
							stateOptions.append("<option id='st__"+st.getStateId()+"'  shortname='"+st.getStateShortName()+"'  value='"+st.getStateId()+"' >"+st.getStateName()+"</option>");
						
					}
					else if(stateModuleFlag.equalsIgnoreCase("portfolio"))
					{
						if(teacherPersonalInfo!=null)
						{
							if(teacherPersonalInfo.getStateId()!=null)
							{
								if(st.getStateId().equals(teacherPersonalInfo.getStateId().getStateId()))
									stateOptions.append("<option id='st"+st.getStateId()+"' shortname='"+st.getStateShortName()+"'  value='"+st.getStateId()+"' selected='selected'>"+st.getStateName()+"</option>");
								else
									stateOptions.append("<option id='st"+st.getStateId()+"' shortname='"+st.getStateShortName()+"'  value='"+st.getStateId()+"' >"+st.getStateName()+"</option>");
							}
							else
								stateOptions.append("<option id='st"+st.getStateId()+"' shortname='"+st.getStateShortName()+"'  value='"+st.getStateId()+"' >"+st.getStateName()+"</option>");
						}
						else
							stateOptions.append("<option id='st"+st.getStateId()+"' shortname='"+st.getStateShortName()+"'  value='"+st.getStateId()+"' >"+st.getStateName()+"</option>");
					}
				}
			}
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
		
		try
		{
			dataArray[0]=stateOptions.toString();
			
			if(teacherPersonalInfo!=null)
			{
				if(teacherPersonalInfo.getOtherState()!=null && !teacherPersonalInfo.getOtherState().equals(""))
					dataArray[1]=teacherPersonalInfo.getOtherState();
				else
					dataArray[1]="";
				
				if(teacherPersonalInfo.getOtherCity()!=null && !teacherPersonalInfo.getOtherCity().equals(""))
					dataArray[2]=teacherPersonalInfo.getOtherCity();
				else
					dataArray[2]="";
				
				if(teacherPersonalInfo.getCountryId()!=null)
					dataArray[3]=teacherPersonalInfo.getCountryId().getCountryId().toString();
				else
					dataArray[3]="";
				
				
				if(teacherPersonalInfo.getCityId()!=null && !teacherPersonalInfo.getCityId().getCityId().equals(""))
					dataArray[4]=teacherPersonalInfo.getCityId().getCityId().toString();
				else
					dataArray[4]="";
				
			}
			else
			{
				dataArray[1]="";
				dataArray[2]="";
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return dataArray;
	}
	
	
	public String[] getStateByCountryPresent(Integer countryId,String stateModuleFlag)
	{
		System.out.println(" <<<<<<<<<< inside getStateByCountry >>>>>>>>>>>>");
		
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		List<StateMaster> lstStateMaster = new ArrayList<StateMaster>();
		StringBuffer stateOptions=	new StringBuffer();
		
		String[] dataArray = new String[5];
		TeacherPersonalInfo teacherPersonalInfo = new TeacherPersonalInfo();
		
		try
		{
			CountryMaster countryMaster = countryMasterDAO.findById(countryId, false, false);
			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
			
		    teacherPersonalInfo = teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
			
			lstStateMaster = stateMasterDAO.findActiveStateByCountryId(countryMaster);
			
			if(lstStateMaster.size()>0)
			{
				String basepath = Utility.getValueOfPropByKey("basePath");
				if(basepath.equalsIgnoreCase("https://canada-en-test.teachermatch.org/")){
					stateOptions.append("<option id='slstPr' value='' >Select Province</option>");
				}else{
				stateOptions.append("<option id='slstPr' value='' >Select State</option>");
				}
			
				for(StateMaster st:lstStateMaster)
				{
					if(stateModuleFlag.equalsIgnoreCase("dspq"))
					{
						if(teacherPersonalInfo!=null)
						{
							if(teacherPersonalInfo.getPresentStateId()!=null)
							{
								if(st.getStateId().equals(teacherPersonalInfo.getPresentStateId().getStateId()))
									stateOptions.append("<option id='st_"+st.getStateId()+"' value='"+st.getStateId()+"' selected='selected'>"+st.getStateName()+"</option>");
								else
									stateOptions.append("<option id='st_"+st.getStateId()+"' value='"+st.getStateId()+"' >"+st.getStateName()+"</option>");
							}
							else
								stateOptions.append("<option id='st_"+st.getStateId()+"' value='"+st.getStateId()+"' >"+st.getStateName()+"</option>");
						}
						else
							stateOptions.append("<option id='st_"+st.getStateId()+"' value='"+st.getStateId()+"' >"+st.getStateName()+"</option>");
						
					}
					else if(stateModuleFlag.equalsIgnoreCase("portfolio"))
					{
						if(teacherPersonalInfo!=null)
						{
							if(teacherPersonalInfo.getPresentStateId()!=null)
							{
								if(st.getStateId().equals(teacherPersonalInfo.getPresentStateId().getStateId()))
									stateOptions.append("<option id='st"+st.getStateId()+"' value='"+st.getStateId()+"' selected='selected'>"+st.getStateName()+"</option>");
								else
									stateOptions.append("<option id='st"+st.getStateId()+"' value='"+st.getStateId()+"' >"+st.getStateName()+"</option>");
							}
							else
								stateOptions.append("<option id='st"+st.getStateId()+"' value='"+st.getStateId()+"' >"+st.getStateName()+"</option>");
						}
						else
							stateOptions.append("<option id='st"+st.getStateId()+"' value='"+st.getStateId()+"' >"+st.getStateName()+"</option>");
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		try
		{
			dataArray[0]=stateOptions.toString();
			
			if(teacherPersonalInfo!=null)
			{
				if(teacherPersonalInfo.getPersentOtherState()!=null && !teacherPersonalInfo.getPersentOtherState().equals(""))
					dataArray[1]=teacherPersonalInfo.getPersentOtherState();
				else
					dataArray[1]="";
				
				if(teacherPersonalInfo.getPersentOtherCity()!=null && !teacherPersonalInfo.getPersentOtherCity().equals(""))
					dataArray[2]=teacherPersonalInfo.getPersentOtherCity();
				else
					dataArray[2]="";
				
				if(teacherPersonalInfo.getPresentCountryId()!=null)
					dataArray[3]=teacherPersonalInfo.getPresentCountryId().getCountryId().toString();
				else
					dataArray[3]="";
				
				
				if(teacherPersonalInfo.getPresentCityId()!=null && !teacherPersonalInfo.getPresentCityId().getCityId().equals(""))
					dataArray[4]=teacherPersonalInfo.getPresentCityId().getCityId().toString();
				else
					dataArray[4]="";
				
			}
			else
			{
				dataArray[1]="";
				dataArray[2]="";
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return dataArray;
	}
	
	
	
	public String checkcandidateInvitation(Integer jobId)
	{
		System.out.println("*****************"+jobId+"*********************");
		
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		String inviteString="";
		boolean invitecandidate=true;
		try
		{
			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
			JobOrder jobOrder= jobOrderDAO.findById(jobId, false, false);
			
			if(jobOrder!=null && jobOrder.getIsInviteOnly()!=null  && jobOrder.getIsInviteOnly() && jobOrder.getHiddenJob()!=null &&  !jobOrder.getHiddenJob()){
			//  invitecandidate = jobWiseInvitedCandidatesDAO.checkCandidateInvitation(teacherDetail, jobOrder);
				List<JobForTeacher> jobForTeachers =jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail, jobOrder);
				if(jobForTeachers!=null && jobForTeachers.size()>0)
				  invitecandidate=true;
				else
				  invitecandidate=false;
			}
			if(invitecandidate)
				inviteString= "1";
			else 
				inviteString= "0";
		 
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return inviteString;
	}
	
}


