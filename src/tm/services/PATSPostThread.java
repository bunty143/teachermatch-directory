package tm.services;

import tm.bean.JobOrder;
import tm.utility.TMCommonUtil;


public class PATSPostThread extends Thread{

	private JobOrder jobOrder;
	private String locationCode;
	private String reqNo;
	private String postingNo;
	private String noOfHires;
	
	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public void setReqNo(String reqNo) {
		this.reqNo = reqNo;
	}
	
	public void setPostingNo(String postingNo) {
		this.postingNo=postingNo;
	}
	
	public void setNoOfHires(String noOfHires) {
		this.noOfHires = noOfHires;
	}
	
	public PATSPostThread() {
		super();
	}
	
	public void run()
	{
		try{
			System.out.println("==============PatsPostThread===================");
			TMCommonUtil.updatePatsPosting(jobOrder, locationCode, reqNo,postingNo,noOfHires);
			System.out.println("==============PatsPostThread===================");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
