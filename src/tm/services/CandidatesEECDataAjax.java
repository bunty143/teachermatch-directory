package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import net.sf.json.JSONArray;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.RaceMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;

import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.utility.EmailCompratorASC;
import tm.utility.EmailCompratorDESC;
import tm.utility.Utility;




public class CandidatesEECDataAjax 
{
	

	String locale = Utility.getValueOfPropByKey("locale");

	@Autowired
	private SchoolInJobOrderDAO 	schoolInJobOrderDAO;
	
	@Autowired
	private TeacherDetailDAO 	teacherDetailDAO;
	@Autowired
	private SchoolMasterDAO 	schoolMasterDAO;
	
	@Autowired
	private JobOrderDAO 	jobOrderDAO;
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	 public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	
	public String displayRecordsByEntityType(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
			schoolId,String subjectId,String certifications,int jobOrderId,String status,String noOfRow,
			String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo, String firstName,String lastName,String emailAddress)
	
	 {
		System.out.println(sortOrderType+" ::@@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		String teacherMIds=null;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			SubjectMaster subjectMaster	=	null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="lastName";
				String sortOrderNoField="lastName";
				
				boolean deafultFlag=false;
				
				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				
				List<JobForTeacher>	jobForTeacherLists = new ArrayList<JobForTeacher>();
				List<JobForTeacher>	listByFirstName = new ArrayList<JobForTeacher>();
				List<JobForTeacher>	jobJobId = new ArrayList<JobForTeacher>();
				
				boolean checkflag = false;
				
				if(entityID==2){
					if(schoolId==0){
						jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatus(districtMaster);
						if(jobForTeacherLists.size()>0)
						checkflag = true;
					}else{
						List<JobOrder> lstJobOrders =null;
						SchoolMaster  sclMaster=null;
						sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
						if(lstJobOrders.size()>0){
							jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOB(lstJobOrders);
							checkflag = true;
						}
					}
					
				}
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0){
						if(schoolId==0){
							DistrictMaster districtMaster1= districtMasterDAO.findById(districtOrSchoolId, false, false);
					        jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatus(districtMaster1);
					        if(jobForTeacherLists.size()>0)
					        	checkflag = true;
						}
						else{
							List<JobOrder> lstJobOrders =null;
							SchoolMaster  sclMaster=null;
							sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
							lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
							if(lstJobOrders.size()>0){
								jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOB(lstJobOrders);
								checkflag = true;
							}
						}
					}
					else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					}
				}
			
		  //****** job ID filter
				
				JobOrder joborder =null;
				if(jobOrderId!=0)
				{
					joborder = jobOrderDAO.findById(jobOrderId, false, false);
					if(joborder!=null)
					 {	
						jobJobId = jobForTeacherDAO.findbyJobOrder(joborder);
						if (jobJobId.size()>0){ 
							if(checkflag)
							{ 
								jobForTeacherLists.retainAll(jobJobId);
							}
							else{
								jobForTeacherLists.addAll(jobJobId);
								checkflag=true;
							}
							
						}else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						 }
					}else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					}	
				}
			
				//******* first Name , Last Name and Email filter 
				
				List<TeacherDetail> basicSearchList = new ArrayList<TeacherDetail>();
			
				List<Criterion> basicSearch = new ArrayList<Criterion>();
				boolean basicFlag = false;
				
				if(firstName!=null && !firstName.equals(""))
				{
					Criterion criterion = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
					
				if(lastName!=null && !lastName.equals(""))
				{
					Criterion criterion = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
				
				if(emailAddress!=null && !emailAddress.equals(""))
				{
					Criterion criterion = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
				if(basicFlag)
				{
					basicSearchList  = teacherDetailDAO.getTeachersByNameAndEmail(basicSearch);
					if(basicSearchList!=null && basicSearchList.size()>0)
					{
						listByFirstName = jobForTeacherDAO.findTeacherByTeacherDeatailAndJobStatus(basicSearchList);
						 if(listByFirstName!=null && listByFirstName.size()>0){
							 if(checkflag){
			   						jobForTeacherLists.retainAll(listByFirstName);
			   					}else{
			   						jobForTeacherLists.addAll(listByFirstName);
			   						checkflag=true;
			   					}
						 }
						 else{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							 }
					}
					else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					 }
				}
			
			
			     ArrayList<Integer> teacherIds = new ArrayList<Integer>();
			     ArrayList<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
				 Map<String ,String> emailDetails = new HashMap<String, String>();
				 Map<String,TeacherPersonalInfo> mapTeacherPersonalInfo = new HashMap<String, TeacherPersonalInfo>();
				 Map<String,String> mapRaces =new HashMap<String, String>();
				 List<TeacherPersonalInfo> listTeacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
				 List<TeacherPersonalInfo> lstsortedTeacherPersonInfo=null;
				 Map<String ,String> names = new HashMap<String, String>();
				 //Set<Integer> set
			if(jobForTeacherLists.size()>0)
			{	 
				for(JobForTeacher jft:jobForTeacherLists) 
				{
					
					listTeacherDetails.add(jft.getTeacherId());
					teacherIds.add(jft.getTeacherId().getTeacherId());
					emailDetails.put(jft.getTeacherId().getTeacherId().toString(),jft.getTeacherId().getEmailAddress());
					names.put(jft.getTeacherId().getTeacherId().toString(),jft.getTeacherId().getFirstName() +" "+jft.getTeacherId().getLastName());
				 }
				
				
			 //TeacherPersonalInfo
				
				
				if(sortOrder.equals("emailAddress"))
				{
				 listTeacherPersonalInfos=teacherPersonalInfoDAO.findByTeacherIdsEEC(null,teacherIds);
				 for(TeacherPersonalInfo td:listTeacherPersonalInfos) 
				 {
					 td.setEmailAddress(emailDetails.get(td.getTeacherId().toString()));
				 }
					if(sortOrderType.equals("0"))
					  Collections.sort(listTeacherPersonalInfos, new EmailCompratorASC());
					else 
				      Collections.sort(listTeacherPersonalInfos, new EmailCompratorDESC());
				}
				else
				{
				 listTeacherPersonalInfos=teacherPersonalInfoDAO.findByTeacherIdsEEC(sortOrderStrVal,teacherIds);
				}
				
				
			   
				for(TeacherPersonalInfo tpf:listTeacherPersonalInfos)
				 {
					mapTeacherPersonalInfo.put(tpf.getTeacherId().toString(),tpf);
					  if(teacherMIds==null)
						 {
							 teacherMIds = tpf.getTeacherId().toString();
						 }
						 else{
							 teacherMIds = teacherMIds +","+ tpf.getTeacherId().toString(); 
						 }
				 }
			   
				 List<RaceMaster> lstRaceMasters = raceMasterDAO.findAll();
				 for(RaceMaster rm: lstRaceMasters){
					 mapRaces.put(rm.getRaceId().toString(), rm.getRaceName());
				 }
				 
				totalRecord=listTeacherPersonalInfos.size();
					if(totalRecord<end)
						end=totalRecord;
					lstsortedTeacherPersonInfo	=	listTeacherPersonalInfos.subList(start,end);
			}	
					
			String responseText="";
			
			//mukesh 
			//tmRecords.append("<span class=\"btn-group\" ><a data-original-title='Export' id='tpexport'  rel='tooltip' data-toggle=\"dropdown\"	href=\"javascript:void(0);\"><span class='icon-cogs icon-large iconcolor'></span></a><ul class=\"dropdown-menu pull-right\"><li><a id='hrefPDF' onclick=\"downloadCandidateEEOCReport();if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;\" href=\"javascript:void(0);\">PDF&nbsp;&nbsp;&nbsp;</a></li><li><a href=\"javascript:void(0);\" onclick='generateEECExcel()'>Excel</a></li><li><a href=\"javascript:void(0);\" onclick='generateEECPrintPre()'>Print Data</a></li></ul></span>");
			
			tmRecords.append("<table  id='tblGridEEC' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCandidateName", locale),sortOrderNoField,"lastName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblEmailAddress", locale),sortOrderNoField,"emailAddress",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblRac", locale),sortOrderFieldName,"raceId",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='15%' valign='top'>"+responseText+" </th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblGend", locale),sortOrderFieldName,"genderId",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblEth", locale),sortOrderFieldName,"ethnicityId",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblEthOrig", locale),sortOrderFieldName,"ethnicOriginId",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");


			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			//tmRecords.append("<input type='text' id='teacherMIds' name='teacherMIds' value='"+1+"'>");
			
			
			
			if(lstsortedTeacherPersonInfo==null){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			 tmRecords.append("<input type='hidden' id='teacherMIds' name='teacherMIds' value=''>");
			}
			if(lstsortedTeacherPersonInfo.size()>0){
			 for(TeacherPersonalInfo td:lstsortedTeacherPersonInfo) 
			 {
				
			    tmRecords.append("<tr>");	
				
			      tmRecords.append("<td  style='text-align: left; font-size:12px;' class='net-widget-content'>"+names.get(td.getTeacherId().toString())+"\n");
			    
				tmRecords.append("</td>");
				
				
			    tmRecords.append("<td style='text-align: left;'>"+emailDetails.get(td.getTeacherId().toString())+"</td>");
				
				
				//Race
				 if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
					{
						if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
						{
							String raceName=null;
							if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getRaceId()!=null){
								String[] raceIDS=mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getRaceId().split(",");
								
							  	   for(int i=0;i<raceIDS.length;i++){
									   if(raceName==null){
											raceName=mapRaces.get(raceIDS[i]);
										}
										else{
											raceName=raceName+","+mapRaces.get(raceIDS[i]);
										}
								}
								
						    tmRecords.append("<td style='text-align: left;'>"+raceName+"</td>");
							}else
							tmRecords.append("<td style='text-align: left;'></td>");
						}
					}
					else
					{
						tmRecords.append("<td style='text-align: left;'></td>");
					}
			  //Gender	
				    if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
					{
						if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
						{
							if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getGenderId()!=null)
						    tmRecords.append("<td style='text-align: left;'>"+mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getGenderId().getGenderName()+"</td>");
							else
							tmRecords.append("<td style='text-align: left;'></td>");
						}
					}
					else
					{
						tmRecords.append("<td style='text-align: left;'></td>");
					}
				   
				 // Ethinicity
				    if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
					{
						if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
						{
							if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicityId()!=null)
						    tmRecords.append("<td style='text-align: left;'>"+mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicityId().getEthnicityName()+"</td>");
							else
							tmRecords.append("<td style='text-align: left;'></td>");
						}
					}
					else
					{
						tmRecords.append("<td style='text-align: left;'></td>");
					}
				 
				    // Ethic Origin
				    if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
					{
						if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
						{
							if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicOriginId()!=null)
						    tmRecords.append("<td style='text-align: left;'>"+mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicOriginId().getEthnicOriginName()+"</td>");
							else
							tmRecords.append("<td style='text-align: left;'></td>");
						}
					}
					else
					{
						tmRecords.append("<td style='text-align: left;'></td>");
					}
		
			  tmRecords.append("</tr>");
			}
			 
		 tmRecords.append("<input type='hidden' id='teacherMIds' name='teacherMIds' value='"+teacherMIds+"'>");
		}
			
				tmRecords.append("</tbody>");
				tmRecords.append("</table>");
				
				tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
				
		
		}catch(Exception e){}
			

	 return tmRecords.toString();
	 }
	

	
 public String displayRecordsByEntityTypeForEXCEL(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
	schoolId,String subjectId,String certifications,int jobOrderId,String status,String noOfRow,
	String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo, String firstName,String lastName,String emailAddress)
	 {
	 System.out.println(schoolId+" ::@@@@@@@@@@   AJAX EXCEL: @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		String fileName = null;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			SubjectMaster subjectMaster	=	null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			//String roleAccess=null;
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="lastName";
				String sortOrderNoField="lastName";

				
				
				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				
				List<JobForTeacher>	jobForTeacherLists = new ArrayList<JobForTeacher>();
				List<JobForTeacher>	listByFirstName = new ArrayList<JobForTeacher>();
				List<JobForTeacher>	jobJobId = new ArrayList<JobForTeacher>();
				
				boolean checkflag = false;
				
				if(entityID==2){
					if(schoolId==0){
						jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatus(districtMaster);
						if(jobForTeacherLists.size()>0)
						checkflag = true;
					}else{
						List<JobOrder> lstJobOrders =null;
						SchoolMaster  sclMaster=null;
						sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
						if(lstJobOrders.size()>0){
							jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOB(lstJobOrders);
							System.out.println("MMMMMMMM :: filter school List "+jobForTeacherLists.size());
							checkflag = true;
						}
					}
					
				}
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0){
						if(schoolId==0){
							DistrictMaster districtMaster1= districtMasterDAO.findById(districtOrSchoolId, false, false);
					        jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatus(districtMaster1);
					        if(jobForTeacherLists.size()>0)
					        	checkflag = true;
						}
						else{
							List<JobOrder> lstJobOrders =null;
							SchoolMaster  sclMaster=null;
							sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
							lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
							if(lstJobOrders.size()>0){
								jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOB(lstJobOrders);
								checkflag = true;
							}
						}
					}
					else
						jobForTeacherLists = new ArrayList<JobForTeacher>();
				}
			
//****** job ID filter
				
				JobOrder joborder =null;
				if(jobOrderId!=0)
				{
					joborder = jobOrderDAO.findById(jobOrderId, false, false);
					if(joborder!=null)
					 {	
						jobJobId = jobForTeacherDAO.findbyJobOrder(joborder);
						if (jobJobId.size()>0){ 
							if(checkflag)
							{ 
								jobForTeacherLists.retainAll(jobJobId);
							}
							else{
								jobForTeacherLists.addAll(jobJobId);
								checkflag=true;
							}
							
						}else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						 }
					}else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					}	
				}
			
				//******* first Name , Last Name and Email filter 
				
				List<TeacherDetail> basicSearchList = new ArrayList<TeacherDetail>();
			
				List<Criterion> basicSearch = new ArrayList<Criterion>();
				boolean basicFlag = false;
				
				if(firstName!=null && !firstName.equals(""))
				{
					Criterion criterion = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
					
				if(lastName!=null && !lastName.equals(""))
				{
					Criterion criterion = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
				
				if(emailAddress!=null && !emailAddress.equals(""))
				{
					Criterion criterion = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
				if(basicFlag)
				{
					basicSearchList  = teacherDetailDAO.getTeachersByNameAndEmail(basicSearch);
					if(basicSearchList!=null && basicSearchList.size()>0)
					{
						listByFirstName = jobForTeacherDAO.findTeacherByTeacherDeatailAndJobStatus(basicSearchList);
						 if(listByFirstName!=null && listByFirstName.size()>0){
							 if(checkflag){
			   						jobForTeacherLists.retainAll(listByFirstName);
			   					}else{
			   						jobForTeacherLists.addAll(listByFirstName);
			   						checkflag=true;
			   					}
						 }
						 else{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							 }
					}
					else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					 }
				}
				
			
			     ArrayList<Integer> teacherIds = new ArrayList<Integer>();
			     ArrayList<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
				 Map<String ,String> emailDetails = new HashMap<String, String>();
				 Map<String,TeacherPersonalInfo> mapTeacherPersonalInfo = new HashMap<String, TeacherPersonalInfo>();
				 Map<String,String> mapRaces =new HashMap<String, String>();
				 List<TeacherPersonalInfo> listTeacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
				 List<TeacherPersonalInfo> lstsortedTeacherPersonInfo=null;
				 Map<String ,String> names = new HashMap<String, String>();
			if(jobForTeacherLists.size()>0)
			{	 
				for(JobForTeacher jft:jobForTeacherLists) 
				{
					//System.out.println("TeacherIDDDD  :: "+jft.getTeacherId().getTeacherId());
					jft.getJobId().getJobTitle();
					listTeacherDetails.add(jft.getTeacherId());
					teacherIds.add(jft.getTeacherId().getTeacherId());
					names.put(jft.getTeacherId().getTeacherId().toString(),jft.getTeacherId().getFirstName() +" "+jft.getTeacherId().getLastName());
					emailDetails.put(jft.getTeacherId().getTeacherId().toString(),jft.getTeacherId().getEmailAddress());
				 }
				
				
				
			 //TeacherPersonalInfo
				if(sortOrder.equals("emailAddress"))
				{
				 listTeacherPersonalInfos=teacherPersonalInfoDAO.findByTeacherIdsEEC(null,teacherIds);
				 for(TeacherPersonalInfo td:listTeacherPersonalInfos) 
				 {
					 td.setEmailAddress(emailDetails.get(td.getTeacherId().toString()));
				 }
					if(sortOrderType.equals("0"))
					  Collections.sort(listTeacherPersonalInfos, new EmailCompratorASC());
					else 
				      Collections.sort(listTeacherPersonalInfos, new EmailCompratorDESC());
				}
				else
				{
				listTeacherPersonalInfos=teacherPersonalInfoDAO.findByTeacherIdsEEC(sortOrderStrVal,teacherIds);
				}
			
				
			   
				for(TeacherPersonalInfo tpf:listTeacherPersonalInfos)
				 {
					mapTeacherPersonalInfo.put(tpf.getTeacherId().toString(),tpf);
				 }
			   
				 List<RaceMaster> lstRaceMasters = raceMasterDAO.findAll();
				 for(RaceMaster rm: lstRaceMasters){
					 mapRaces.put(rm.getRaceId().toString(), rm.getRaceName());
				 }
			
						
			}	
			//  Excel   Exporting	
			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			//String basePath = request.getRealPath("/")+"/candidate";
			String basePath = request.getSession().getServletContext().getRealPath ("/")+"/eeocreport";
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
			fileName ="eeocreport"+time+".xls";

			
			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);

			file = new File(basePath+"/"+fileName);

			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));

			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;

			WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
			workbook.createSheet(Utility.getLocaleValuePropByKey("headEEOCRet", locale), 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);

			// Create create a bold font with unterlines
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			header.setBackground(Colour.GRAY_25);

			// Write a few headers
			excelSheet.mergeCells(0, 0, 5, 1);
			Label label;
			label = new Label(0, 0, Utility.getLocaleValuePropByKey("headEEOCRet", locale), timesBoldUnderline);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 3, 5, 3);
			label = new Label(0, 3, "");
			excelSheet.addCell(label);
			excelSheet.getSettings().setDefaultColumnWidth(18);
			
			int k=4;
			int col=1;
			label = new Label(0, k, Utility.getLocaleValuePropByKey("lblCandidateName", locale),header); 
			excelSheet.addCell(label);
			label = new Label(1, k, Utility.getLocaleValuePropByKey("lblEmailAddress", locale),header); 
			excelSheet.addCell(label);
			/*label = new Label(++col, k, "Job Title",header); 
			excelSheet.addCell(label);*/
			label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblRac", locale),header); 
			excelSheet.addCell(label);
			label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblGend", locale),header); 
			excelSheet.addCell(label);
			label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblEth", locale),header); 
			excelSheet.addCell(label);
			label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblEthOrig", locale),header); 
			excelSheet.addCell(label);
			 
			
			k=k+1;
			if(listTeacherPersonalInfos.size()==0)
			{	
				excelSheet.mergeCells(0, k, 8, k);
				label = new Label(0, k, Utility.getLocaleValuePropByKey("lblNoRecord", locale)); 
				excelSheet.addCell(label);
			}
			
			
				
			
			if(listTeacherPersonalInfos.size()>0){
			 for(TeacherPersonalInfo td:listTeacherPersonalInfos) 
			 {
				// System.out.println("@@ :: ");
				 col=1;
		
				String name=names.get(td.getTeacherId().toString());
				label = new Label(0, k, name); 
				excelSheet.addCell(label);
				
			
				if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
				{
					label = new Label(1, k, emailDetails.get(td.getTeacherId().toString())); 
					excelSheet.addCell(label);
				}
			
				
				//Race
				 if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
					{
						if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
						{
							String raceName=null;
							if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getRaceId()!=null){
								String[] raceIDS=mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getRaceId().split(",");
								
							  	   for(int i=0;i<raceIDS.length;i++){
									   if(raceName==null){
											raceName=mapRaces.get(raceIDS[i]);
										}
										else{
											raceName=raceName+","+mapRaces.get(raceIDS[i]);
										}
								}
								
						    //tmRecords.append("<td style='text-align: left;'>"+raceName+"</td>");
						    label = new Label(++col, k, raceName); 
							excelSheet.addCell(label);
							}else{
								label = new Label(++col, k, ""); 
								excelSheet.addCell(label);
							}
						}
					}
					else
					{
						label = new Label(++col, k, ""); 
						excelSheet.addCell(label);
					}
			  //Gender	
				    if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
					{
						if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
						{
							if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getGenderId()!=null){
						   // tmRecords.append("<td style='text-align: left;'>"+mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getGenderId().getGenderName()+"</td>");
						    label = new Label(++col, k, mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getGenderId().getGenderName()); 
							excelSheet.addCell(label);
							}else{
								label = new Label(++col, k, ""); 
								excelSheet.addCell(label);
							}
						}
					}
					else
					{
						label = new Label(++col, k,""); 
						excelSheet.addCell(label);
					}
				   
				 // Ethinicity
				    if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
					{
						if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
						{
							if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicityId()!=null)
							{
						    label = new Label(++col, k, mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicityId().getEthnicityName()); 
							excelSheet.addCell(label);
							}else
							{
								label = new Label(++col, k, ""); 
								excelSheet.addCell(label);
							}
						}
					}
					else
					{
						label = new Label(++col, k, ""); 
						excelSheet.addCell(label);
					}
				 
				    // Ethic Origin
				    if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
					{
						if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
						{
							if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicOriginId()!=null){
						    label = new Label(++col, k, mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicOriginId().getEthnicOriginName()); 
							excelSheet.addCell(label);
							}else
							{
							label = new Label(++col, k, ""); 
							excelSheet.addCell(label);
							}
						}
					}
					else
					{
						label = new Label(++col, k, ""); 
						excelSheet.addCell(label);
					}
			
			  ++k;
			 }
		}
		
		workbook.write();
		workbook.close();
		}catch(Exception e){e.printStackTrace();}

	 return fileName;
	 }

public String  downloadCandidateEEOCReport(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
		schoolId,String subjectId,String certifications,int jobOrderId,String status,String noOfRow,
		String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo, String firstName,String lastName,String emailAddress)
        {
		
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}


	try
	{	
		Font font9 = null;
		Font font9bold = null;
		Font font11 = null;
		Font font11bold = null;

		try {
			//tahoma = BaseFont.createFont("c:\\font\\tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			font9 = new Font(tahoma, 9);
			font9bold = new Font(tahoma, 9, Font.BOLD);
			font11 = new Font(tahoma, 11);
			font11bold = new Font(tahoma, 11,Font.BOLD);


		} 
		catch (DocumentException e1) 
		{
			e1.printStackTrace();
		} 
		catch (IOException e1) 
		{
			e1.printStackTrace();
		}


		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			userMaster = (UserMaster)session.getAttribute("userMaster");

		int userId = userMaster.getUserId();

		

		String time = String.valueOf(System.currentTimeMillis()).substring(6);
		String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
		String fileName =time+"Report.pdf";

		File file = new File(basePath);
		if(!file.exists())
			file.mkdirs();

		Utility.deleteAllFileFromDir(basePath);
		

		generateCandidatePDReport( resultFlag, JobOrderType,districtOrSchoolId, 
				schoolId,subjectId,certifications,jobOrderId,status,noOfRow,
				pageNo,sortOrder,sortOrderType,disJobReqNo, firstName,lastName,emailAddress, basePath+"/"+fileName,context.getServletContext().getRealPath("/"));
		return "user/"+userId+"/"+fileName;
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return "ABV";
   }
public boolean generateCandidatePDReport(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
		schoolId,String subjectId,String certifications,int jobOrderId,String status,String noOfRow,
		String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo, String firstName,String lastName,String emailAddress,String path,String realPath)
       {
	
	int cellSize = 0;
	Document document=null;
	FileOutputStream fos = null;
	PdfWriter writer = null;
	Paragraph footerpara = null;
	HeaderFooter headerFooter = null;
	
		
		Font font8 = null;
		Font font8Green = null;
		Font font8bold = null;
		Font font9 = null;
		Font font9bold = null;
		Font font10 = null;
		
		Font font10_10=null;
		
		
		Font font10bold = null;
		Font font11 = null;
		Font font11bold = null;
		Font font20bold = null;
		Font font11b   =null;
		Color bluecolor =null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			SubjectMaster subjectMaster	=	null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			//String roleAccess=null;
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="lastName";
				String sortOrderNoField="lastName";

			
				
				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				
				List<JobForTeacher>	jobForTeacherLists = new ArrayList<JobForTeacher>();
				List<JobForTeacher>	listByFirstName = new ArrayList<JobForTeacher>();
				List<JobForTeacher>	jobJobId = new ArrayList<JobForTeacher>();
				
				boolean checkflag = false;
				
				if(entityID==2){
					if(schoolId==0){
						jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatus(districtMaster);
						if(jobForTeacherLists.size()>0)
						checkflag = true;
					}else{
						List<JobOrder> lstJobOrders =null;
						SchoolMaster  sclMaster=null;
						sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
						if(lstJobOrders.size()>0){
							jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOB(lstJobOrders);
							checkflag = true;
						}
					}
					
				}
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0){
						if(schoolId==0){
							DistrictMaster districtMaster1= districtMasterDAO.findById(districtOrSchoolId, false, false);
					        jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatus(districtMaster1);
					        if(jobForTeacherLists.size()>0)
					        	checkflag = true;
						}
						else{
							List<JobOrder> lstJobOrders =null;
							SchoolMaster  sclMaster=null;
							sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
							lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
							if(lstJobOrders.size()>0){
								jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOB(lstJobOrders);
								checkflag = true;
							}
						}
					}
					else
						jobForTeacherLists = new ArrayList<JobForTeacher>();
				}
			
//****** job ID filter
				
				JobOrder joborder =null;
				if(jobOrderId!=0)
				{
					joborder = jobOrderDAO.findById(jobOrderId, false, false);
					if(joborder!=null)
					 {	
						jobJobId = jobForTeacherDAO.findbyJobOrder(joborder);
						if (jobJobId.size()>0){ 
							if(checkflag)
							{ 
								System.out.println("jobJobId.size() :: "+jobJobId.size());
								jobForTeacherLists.retainAll(jobJobId);
							}
							else{
								jobForTeacherLists.addAll(jobJobId);
								checkflag=true;
							}
							
						}else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						 }
					}else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					}	
				}
			
				//******* first Name , Last Name and Email filter 
				
				List<TeacherDetail> basicSearchList = new ArrayList<TeacherDetail>();
			
				List<Criterion> basicSearch = new ArrayList<Criterion>();
				boolean basicFlag = false;
				
				if(firstName!=null && !firstName.equals(""))
				{
					Criterion criterion = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
					
				if(lastName!=null && !lastName.equals(""))
				{
					Criterion criterion = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
				
				if(emailAddress!=null && !emailAddress.equals(""))
				{
					Criterion criterion = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);
					basicSearch.add(criterion);
					basicFlag = true;
				}
				if(basicFlag)
				{
					basicSearchList  = teacherDetailDAO.getTeachersByNameAndEmail(basicSearch);
					if(basicSearchList!=null && basicSearchList.size()>0)
					{
						listByFirstName = jobForTeacherDAO.findTeacherByTeacherDeatailAndJobStatus(basicSearchList);
						 if(listByFirstName!=null && listByFirstName.size()>0){
							 if(checkflag){
			   						jobForTeacherLists.retainAll(listByFirstName);
			   					}else{
			   						jobForTeacherLists.addAll(listByFirstName);
			   						checkflag=true;
			   					}
						 }
						 else{
								jobForTeacherLists = new ArrayList<JobForTeacher>();
							 }
					}
					else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					 }
				}
				
			     ArrayList<Integer> teacherIds = new ArrayList<Integer>();
			     ArrayList<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
				 Map<String ,String> emailDetails = new HashMap<String, String>();
				 Map<String,TeacherPersonalInfo> mapTeacherPersonalInfo = new HashMap<String, TeacherPersonalInfo>();
				 Map<String,String> mapRaces =new HashMap<String, String>();
				 List<TeacherPersonalInfo> listTeacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
				 List<TeacherPersonalInfo> lstsortedTeacherPersonInfo=null;
				 Map<String ,String> names = new HashMap<String, String>();
			if(jobForTeacherLists.size()>0)
			{	 
				for(JobForTeacher jft:jobForTeacherLists) 
				{
					//System.out.println("TeacherIDDDD  :: "+jft.getTeacherId().getTeacherId());
					jft.getJobId().getJobTitle();
					listTeacherDetails.add(jft.getTeacherId());
					teacherIds.add(jft.getTeacherId().getTeacherId());
					names.put(jft.getTeacherId().getTeacherId().toString(),jft.getTeacherId().getFirstName() +" "+jft.getTeacherId().getLastName());
					emailDetails.put(jft.getTeacherId().getTeacherId().toString(),jft.getTeacherId().getEmailAddress());
				 }
				
			
				
			 //TeacherPersonalInfo
				if(sortOrder.equals("emailAddress"))
				{
				 listTeacherPersonalInfos=teacherPersonalInfoDAO.findByTeacherIdsEEC(null,teacherIds);
				 for(TeacherPersonalInfo td:listTeacherPersonalInfos) 
				 {
					 td.setEmailAddress(emailDetails.get(td.getTeacherId().toString()));
				 }
					if(sortOrderType.equals("0"))
					  Collections.sort(listTeacherPersonalInfos, new EmailCompratorASC());
					else 
				      Collections.sort(listTeacherPersonalInfos, new EmailCompratorDESC());
				}
				else
				{
				listTeacherPersonalInfos=teacherPersonalInfoDAO.findByTeacherIdsEEC(sortOrderStrVal,teacherIds);
				}
				
				
				
			   
				for(TeacherPersonalInfo tpf:listTeacherPersonalInfos)
				 {
					mapTeacherPersonalInfo.put(tpf.getTeacherId().toString(),tpf);
				 }
			   
				 List<RaceMaster> lstRaceMasters = raceMasterDAO.findAll();
				 for(RaceMaster rm: lstRaceMasters){
					 mapRaces.put(rm.getRaceId().toString(), rm.getRaceName());
				 }
					
			}
	
	
//#################################################################################	
	
			String fontPath = realPath;
		try {
			
			BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			font8 = new Font(tahoma, 8);

			font8Green = new Font(tahoma, 8);
			font8Green.setColor(Color.BLUE);
			font8bold = new Font(tahoma, 8, Font.NORMAL);


			font9 = new Font(tahoma, 9);
			font9bold = new Font(tahoma, 9, Font.BOLD);
			font10 = new Font(tahoma, 10);
			
			font10_10 = new Font(tahoma, 10);
			font10_10.setColor(Color.white);
			font10bold = new Font(tahoma, 10, Font.BOLD);
			font11 = new Font(tahoma, 11);
			font11bold = new Font(tahoma, 11,Font.BOLD);
			bluecolor =  new Color(0,122,180); 
			
			//Color bluecolor =  new Color(0,122,180); 
			
			font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
			//font20bold.setColor(Color.BLUE);
			font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


		} 
		catch (DocumentException e1) 
		{
			e1.printStackTrace();
		} 
		catch (IOException e1) 
		{
			e1.printStackTrace();
		}
		
		document = new Document(PageSize.A4,10f,10f,10f,10f);		
		document.addAuthor("TeacherMatch");
		document.addCreator("TeacherMatch Inc.");
		document.addSubject(Utility.getLocaleValuePropByKey("headEEOCRet", locale));
		document.addCreationDate();
		document.addTitle(Utility.getLocaleValuePropByKey("headEEOCRet", locale));

		fos = new FileOutputStream(path);
		PdfWriter.getInstance(document, fos);
		/*
		if(entityID==2){
		footerpara = new Paragraph("Created by  "+districtMaster.getDistrictName()+" : Created on - "+new Date(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 6,Font.BOLDITALIC,Color.black));
		}else if(entityID==1){
			footerpara = new Paragraph("Created by TeacherMatch: Created on - "+new Date(),FontFactory.getFont(FontFactory.TIMES_ROMAN, 6,Font.BOLDITALIC,Color.black));
		}
		headerFooter = new HeaderFooter(footerpara,false);
		headerFooter.setAlignment(HeaderFooter.ALIGN_RIGHT);
		headerFooter.setBorder(0);
		document.setFooter(headerFooter);*/
		document.open();
		
		PdfPTable mainTable = new PdfPTable(1);
		mainTable.setWidthPercentage(90);

		Paragraph [] para = null;
		PdfPCell [] cell = null;


		para = new Paragraph[3];
		cell = new PdfPCell[3];
		para[0] = new Paragraph(" ",font20bold);
		cell[0]= new PdfPCell(para[0]);
		cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell[0].setBorder(0);
		mainTable.addCell(cell[0]);
		
		//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
		Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
		logo.scalePercent(75);

		//para[1] = new Paragraph(" ",font20bold);
		cell[1]= new PdfPCell(logo);
		cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
		cell[1].setBorder(0);
		mainTable.addCell(cell[1]);

		document.add(new Phrase("\n"));
		document.add(new Phrase("\n"));
		document.add(new Phrase("\n"));
		
		para[2] = new Paragraph("",font20bold);
		cell[2]= new PdfPCell(para[2]);
		cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
		cell[2].setBorder(0);
		mainTable.addCell(cell[2]);

		document.add(mainTable);
		
		document.add(new Phrase("\n"));
		document.add(new Phrase("\n"));
		

		float[] tblwidthz={.15f};
		
		mainTable = new PdfPTable(tblwidthz);
		mainTable.setWidthPercentage(100);
		para = new Paragraph[1];
		cell = new PdfPCell[1];

		
		para[0] = new Paragraph(Utility.getLocaleValuePropByKey("headEEOCRet", locale),font20bold);
		cell[0]= new PdfPCell(para[0]);
		cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
		cell[0].setBorder(0);
		mainTable.addCell(cell[0]);
		document.add(mainTable);

		document.add(new Phrase("\n"));
		document.add(new Phrase("\n"));
		
		
        float[] tblwidths={.15f,.20f};
		
		mainTable = new PdfPTable(tblwidths);
		mainTable.setWidthPercentage(100);
		para = new Paragraph[2];
		cell = new PdfPCell[2];

		String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
		
		para[0] = new Paragraph( Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
		cell[0]= new PdfPCell(para[0]);
		cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
		cell[0].setBorder(0);
		mainTable.addCell(cell[0]);
		
		para[1] = new Paragraph(""+ Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
		cell[1]= new PdfPCell(para[1]);
		cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell[1].setBorder(0);
		mainTable.addCell(cell[1]);
		
		document.add(mainTable);
		
		
		
		document.add(new Phrase("\n"));
		document.add(new Phrase("\n"));
		document.add(new Phrase("\n"));


		float[] tblwidth={.15f,.20f,.15f,.10f,.10f,.10f};
		
		mainTable = new PdfPTable(tblwidth);
		mainTable.setWidthPercentage(100);
		para = new Paragraph[6];
		cell = new PdfPCell[6];
// header
		para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblCandidateName", locale),font10_10);
		cell[0]= new PdfPCell(para[0]);
		cell[0].setBackgroundColor(bluecolor);
		cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
		//cell[0].setBorder(1);
		mainTable.addCell(cell[0]);
		
		para[1] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblEmailAddress", locale),font10_10);
		cell[1]= new PdfPCell(para[1]);
		cell[1].setBackgroundColor(bluecolor);
		cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
		//cell[1].setBorder(1);
		mainTable.addCell(cell[1]);
		
		para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblRac", locale),font10_10);
		cell[2]= new PdfPCell(para[2]);
		cell[2].setBackgroundColor(bluecolor);
		cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
	//	cell[2].setBorder(1);
		mainTable.addCell(cell[2]);

		para[3] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblGend", locale),font10_10);
		cell[3]= new PdfPCell(para[3]);
		cell[3].setBackgroundColor(bluecolor);
		cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
		//cell[3].setBorder(1);
		mainTable.addCell(cell[3]);
		
		para[4] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblEth", locale),font10_10);
		cell[4]= new PdfPCell(para[4]);
		cell[4].setBackgroundColor(bluecolor);
		cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
	//	cell[4].setBorder(1);
		mainTable.addCell(cell[4]);
		
		para[5] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblEthOrig", locale),font10_10);
		cell[5]= new PdfPCell(para[5]);
		cell[5].setBackgroundColor(bluecolor);
		cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
		//cell[5].setBorder(1);
		mainTable.addCell(cell[5]);
		
		document.add(mainTable);
		
		if(listTeacherPersonalInfos.size()==0){
			    float[] tblwidth11={.10f};
				
				 mainTable = new PdfPTable(tblwidth11);
				 mainTable.setWidthPercentage(100);
				 para = new Paragraph[1];
				 cell = new PdfPCell[1];
			
				 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgNorecordfound", locale),font8bold);
				 cell[0]= new PdfPCell(para[0]);
				 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
				 //cell[index].setBorder(1);
				 mainTable.addCell(cell[0]);
			
			document.add(mainTable);
		}
		if(listTeacherPersonalInfos.size()>0){
			 for(TeacherPersonalInfo td:listTeacherPersonalInfos) 
			 {
				
				 int index=0;
				 float[] tblwidth1={.15f,.20f,.15f,.10f,.10f,.10f};
					
				mainTable = new PdfPTable(tblwidth1);
				mainTable.setWidthPercentage(100);
				 para = new Paragraph[6];
				 cell = new PdfPCell[6];
				 
				 
				 //Candidate Name
				// String name=td.getFirstName()+" "+td.getLastName();
				 
				 para[index] = new Paragraph(names.get(td.getTeacherId().toString()),font8bold);
				 cell[index]= new PdfPCell(para[index]);
				 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
				 //cell[index].setBorder(1);
				 mainTable.addCell(cell[index]);
				 index++;
				//Email Address
				if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
				{
					
					 para[index] = new Paragraph(""+emailDetails.get(td.getTeacherId().toString()),font8bold);
					 cell[index]= new PdfPCell(para[index]);
					 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
					// cell[index].setBorder(1);
					 mainTable.addCell(cell[index]);
					 index++;
				}
				
			
				//Race
				 if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
					{
						if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
						{
							String raceName=null;
							if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getRaceId()!=null){
								String[] raceIDS=mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getRaceId().split(",");
								
							  	   for(int i=0;i<raceIDS.length;i++){
									   if(raceName==null){
											raceName=mapRaces.get(raceIDS[i]);
										}
										else{
											raceName=raceName+","+mapRaces.get(raceIDS[i]);
										}
								}
								
							  	 para[index] = new Paragraph(""+raceName,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								// cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
							
							
							}else{
								 para[index] = new Paragraph(""+"",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								// cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
							}
						}
					}
					else
					{ para[index] = new Paragraph(""+"",font8bold);
					 cell[index]= new PdfPCell(para[index]);
					 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
					// cell[index].setBorder(1);
					 mainTable.addCell(cell[index]);
					 index++;
					}
			  //Gender	
				    if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
					{
						if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
						{
							if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getGenderId()!=null){
								 para[index] = new Paragraph(""+mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getGenderId().getGenderName(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								// cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
								
							}else{
								 para[index] = new Paragraph(""+"",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								// cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
							}
						}
					}
					else
					{
						 para[index] = new Paragraph(""+"",font8bold);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						// cell[index].setBorder(1);
						 mainTable.addCell(cell[index]);
						 index++;
					}
				   
				 // Ethinicity
				    if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
					{
						if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
						{
							if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicityId()!=null)
							{
								 para[index] = new Paragraph(""+mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicityId().getEthnicityName(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								// cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
							
							}else
							{
								 para[index] = new Paragraph(""+"",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								// cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
							}
						}
					}
					else
					{
						 para[index] = new Paragraph(""+"",font8bold);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 //cell[index].setBorder(1);
						 mainTable.addCell(cell[index]);
						 index++;
					}
				 
				    // Ethic Origin
				    if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
					{
						if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
						{
							if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicOriginId()!=null){
								 para[index] = new Paragraph(""+mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicOriginId().getEthnicOriginName(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								// cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
							
							}else
							{
								para[index] = new Paragraph(""+"",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								// cell[index].setBorder(1);
								 mainTable.addCell(cell[index]);
								 index++;
							
							}
						}
					}
					else
					{
						para[index] = new Paragraph(""+"",font8bold);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						// cell[index].setBorder(1);
						 mainTable.addCell(cell[index]);
						 index++;
					
					}
			
		document.add(mainTable);
			}
		
		
		}
	
		
	
	}catch(Exception e){e.printStackTrace();}
	finally
	{
		if(document != null && document.isOpen())
			document.close();
		if(writer!=null){
			writer.flush();
			writer.close();
		}
	}
	return true;
   }



public String displayRecordsByEntityTypePrintPre(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
		schoolId,String subjectId,String certifications,int jobOrderId,String status,String noOfRow,
		String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo, String firstName,String lastName,String emailAddress)

 {
	System.out.println(sortOrderType+" ::@@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	StringBuffer tmRecords =	new StringBuffer();
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		SubjectMaster subjectMaster	=	null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		
		//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
		//-- get no of record in grid,
		//-- set start and end position

			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------
		 /** set default sorting fieldName **/
			String sortOrderFieldName="lastName";
			String sortOrderNoField="lastName";
			System.out.println("sortOrderFieldName :: "+sortOrderFieldName);
		
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
		
		//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
			
			List<JobForTeacher>	jobForTeacherLists = new ArrayList<JobForTeacher>();
			List<JobForTeacher>	listByFirstName = new ArrayList<JobForTeacher>();
			List<JobForTeacher>	jobJobId = new ArrayList<JobForTeacher>();
			
			boolean checkflag = false;
			
			if(entityID==2){
				if(schoolId==0){
					jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatus(districtMaster);
					if(jobForTeacherLists.size()>0)
					checkflag = true;
				}else{
					List<JobOrder> lstJobOrders =null;
					SchoolMaster  sclMaster=null;
					sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
					lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
					if(lstJobOrders.size()>0){
						jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOB(lstJobOrders);
						checkflag = true;
					}
				}
				
			}
			else if(entityID==1) 
			{
				if(districtOrSchoolId!=0){
					if(schoolId==0){
						DistrictMaster districtMaster1= districtMasterDAO.findById(districtOrSchoolId, false, false);
				        jobForTeacherLists  =  jobForTeacherDAO.findTeacersByDistrictStatus(districtMaster1);
				        if(jobForTeacherLists.size()>0)
				        	checkflag = true;
					}
					else{
						List<JobOrder> lstJobOrders =null;
						SchoolMaster  sclMaster=null;
						sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						lstJobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(sclMaster);
						if(lstJobOrders.size()>0){
							jobForTeacherLists = jobForTeacherDAO.getJobForTeacherByJOB(lstJobOrders);
							checkflag = true;
						}
					}
				}
				else{
					jobForTeacherLists = new ArrayList<JobForTeacher>();
				}
			}
		
	  //****** job ID filter
			
			JobOrder joborder =null;
			if(jobOrderId!=0)
			{
				System.out.println(" jobOrderId ::  "+jobOrderId);
				joborder = jobOrderDAO.findById(jobOrderId, false, false);
				if(joborder!=null)
				 {	
					jobJobId = jobForTeacherDAO.findbyJobOrder(joborder);
					if (jobJobId.size()>0){ 
						if(checkflag)
						{ 
							jobForTeacherLists.retainAll(jobJobId);
						}
						else{
							jobForTeacherLists.addAll(jobJobId);
							checkflag=true;
						}
						
					}else{
						jobForTeacherLists = new ArrayList<JobForTeacher>();
					 }
				}else{
					jobForTeacherLists = new ArrayList<JobForTeacher>();
				}	
			}
		
			//******* first Name , Last Name and Email filter 
			
			List<TeacherDetail> basicSearchList = new ArrayList<TeacherDetail>();
		
			List<Criterion> basicSearch = new ArrayList<Criterion>();
			boolean basicFlag = false;
			
			if(firstName!=null && !firstName.equals(""))
			{
				Criterion criterion = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
				basicSearch.add(criterion);
				basicFlag = true;
			}
				
			if(lastName!=null && !lastName.equals(""))
			{
				Criterion criterion = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
				basicSearch.add(criterion);
				basicFlag = true;
			}
			
			if(emailAddress!=null && !emailAddress.equals(""))
			{
				Criterion criterion = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);
				basicSearch.add(criterion);
				basicFlag = true;
			}
			if(basicFlag)
			{
				basicSearchList  = teacherDetailDAO.getTeachersByNameAndEmail(basicSearch);
				if(basicSearchList!=null && basicSearchList.size()>0)
				{
					listByFirstName = jobForTeacherDAO.findTeacherByTeacherDeatailAndJobStatus(basicSearchList);
					 if(listByFirstName!=null && listByFirstName.size()>0){
						 if(checkflag){
		   						jobForTeacherLists.retainAll(listByFirstName);
		   					}else{
		   						jobForTeacherLists.addAll(listByFirstName);
		   						checkflag=true;
		   					}
					 }
					 else{
							jobForTeacherLists = new ArrayList<JobForTeacher>();
						 }
				}
				else{
					jobForTeacherLists = new ArrayList<JobForTeacher>();
				 }
			}
		System.out.println("finallllllllll JFTTTT :: "+jobForTeacherLists.size());	
			
		
		     ArrayList<Integer> teacherIds = new ArrayList<Integer>();
		     ArrayList<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
			 Map<String ,String> emailDetails = new HashMap<String, String>();
			 Map<String,TeacherPersonalInfo> mapTeacherPersonalInfo = new HashMap<String, TeacherPersonalInfo>();
			 Map<String,String> mapRaces =new HashMap<String, String>();
			 List<TeacherPersonalInfo> listTeacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
			 List<TeacherPersonalInfo> lstsortedTeacherPersonInfo=null;
			 Map<String ,String> names = new HashMap<String, String>();
			 
		if(jobForTeacherLists.size()>0)
		{	 
			for(JobForTeacher jft:jobForTeacherLists) 
			{
				//System.out.println("TeacherIDDDD  :: "+jft.getTeacherId().getTeacherId());
				jft.getJobId().getJobTitle();
				listTeacherDetails.add(jft.getTeacherId());
				teacherIds.add(jft.getTeacherId().getTeacherId());
				names.put(jft.getTeacherId().getTeacherId().toString(),jft.getTeacherId().getFirstName() +" "+jft.getTeacherId().getLastName());
				emailDetails.put(jft.getTeacherId().getTeacherId().toString(),jft.getTeacherId().getEmailAddress());
			 }
			
			
		 //TeacherPersonalInfo
			
			
			if(sortOrder.equals("emailAddress"))
			{
			 listTeacherPersonalInfos=teacherPersonalInfoDAO.findByTeacherIdsEEC(null,teacherIds);
			 for(TeacherPersonalInfo td:listTeacherPersonalInfos) 
			 {
				 td.setEmailAddress(emailDetails.get(td.getTeacherId().toString()));
			 }
				if(sortOrderType.equals("0"))
				  Collections.sort(listTeacherPersonalInfos, new EmailCompratorASC());
				else 
			      Collections.sort(listTeacherPersonalInfos, new EmailCompratorDESC());
			}
			else
			{
			listTeacherPersonalInfos=teacherPersonalInfoDAO.findByTeacherIdsEEC(sortOrderStrVal,teacherIds);
			}
			
			System.out.println(">>>>>>>>>>>>>>>>>>> size :: "+listTeacherPersonalInfos.size());
		   
			for(TeacherPersonalInfo tpf:listTeacherPersonalInfos)
			 {
				mapTeacherPersonalInfo.put(tpf.getTeacherId().toString(),tpf);
			 }
		   
			 List<RaceMaster> lstRaceMasters = raceMasterDAO.findAll();
			 for(RaceMaster rm: lstRaceMasters){
				 mapRaces.put(rm.getRaceId().toString(), rm.getRaceName());
			 }
			 
		
		}	
				
				System.out.println("listTeacherPersonalInfos :: "+listTeacherPersonalInfos.size()+"  lstsortedTeacherPersonInfo :: "+lstsortedTeacherPersonInfo);	
		
		String responseText="";
		
		//mukesh 
		tmRecords.append("<div style='text-align: center; font-size: 25px; text-decoration: underline; font-weight:bold;'>"+Utility.getLocaleValuePropByKey("headEEOCRet", locale)+"</div><br/>");
		
		tmRecords.append("<div style='width:100%'>");
			tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+ Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
			tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
			tmRecords.append("<br/><br/>");
		tmRecords.append("</div>");
		tmRecords.append("<table  id='tblGridEECPrint' width='100%' border='0'>");
		tmRecords.append("<thead class='bg'>");
		tmRecords.append("<tr>");
		tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblCandidateName", locale)+"</th>");

		tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblEmailAddress", locale)+"</th>");
		
		tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblRac", locale)+"</th>");
		
		tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblGend", locale)+"</th>");

		tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblEth", locale)+"</th>");

		tmRecords.append("<th style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblEthOrig", locale)+"</th>");


		tmRecords.append("</tr>");
		tmRecords.append("</thead>");
		tmRecords.append("<tbody>");
		
		if(listTeacherPersonalInfos==null || listTeacherPersonalInfos.size()==0){
     		tmRecords.append("<tr><td style='font-size:12px;' >"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
		}
		if(listTeacherPersonalInfos.size()>0){
		 for(TeacherPersonalInfo td:listTeacherPersonalInfos) 
		 {
		    tmRecords.append("<tr>");	
			
			tmRecords.append("<td  style='font-size:12px;'>"+names.get(td.getTeacherId().toString()));
			tmRecords.append("</td>");
			
			tmRecords.append("<td style='font-size:12px;'>"+emailDetails.get(td.getTeacherId().toString())+"</td>");
			
			//Race
			 if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
				{
					if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
					{
						String raceName=null;
						if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getRaceId()!=null){
							String[] raceIDS=mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getRaceId().split(",");
							
						  	   for(int i=0;i<raceIDS.length;i++){
								   if(raceName==null){
										raceName=mapRaces.get(raceIDS[i]);
									}
									else{
										raceName=raceName+","+mapRaces.get(raceIDS[i]);
									}
							}
							
					    tmRecords.append("<td style='font-size:12px;'>"+raceName+"</td>");
						}else
						tmRecords.append("<td style='font-size:12px; '></td>");
					}
				}
				else
				{
					tmRecords.append("<td style='font-size:12px; '></td>");
				}
			 
		  //Gender	
			    if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
				{
					if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
					{
						if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getGenderId()!=null)
					    tmRecords.append("<td style='font-size:12px;'>"+mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getGenderId().getGenderName()+"</td>");
						else
						tmRecords.append("<td style='font-size:12px; '></td>");
					}
				}
				else
				{
					tmRecords.append("<td style='font-size:12px;'></td>");
				}
			   
			 // Ethinicity
			    if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
				{
					if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
					{
						if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicityId()!=null)
					    tmRecords.append("<td style='font-size:12px;'>"+mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicityId().getEthnicityName()+"</td>");
						else
						tmRecords.append("<td style='font-size:12px;'></td>");
					}
				}
				else
				{
					tmRecords.append("<td style='font-size:12px; '></td>");
				}
			 
			    // Ethic Origin
			    if(mapTeacherPersonalInfo.get(td.getTeacherId().toString())!=null)
				{
					if(td.getTeacherId().toString().equals(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getTeacherId().toString()))
					{
						if(mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicOriginId()!=null)
					    tmRecords.append("<td style='font-size:12px;'>"+mapTeacherPersonalInfo.get(td.getTeacherId().toString()).getEthnicOriginId().getEthnicOriginName()+"</td>");
						else
						tmRecords.append("<td style='font-size:12px; '></td>");
					}
				}
				else
				{
					tmRecords.append("<td style='font-size:12px; '></td>");
				}
			
		 // }
		  tmRecords.append("</tr>");
		}
	}
		

			tmRecords.append("</tbody>");
			tmRecords.append("</table>");
			
	
	}catch(Exception e){}
	
 System.out.println("####################### END #########################");

 return tmRecords.toString();
 }

public JSONArray displayRecordsByGender(String teacherIdstr)
 {
	System.out.println(  "@@@@@@@@@@@@@ Gender  @@@@@@@@@@@@@ :: ");
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	JSONArray gendercategories = new JSONArray();
	try{
			//System.out.println("teacherIdstr :: "+teacherIdstr);
			List<TeacherPersonalInfo> listTeacherPersonalInfos=new ArrayList<TeacherPersonalInfo>();
			String idList[];
			TreeSet<Integer> teachers =new TreeSet<Integer>();
			if(!teacherIdstr.equals("")){
				idList = teacherIdstr.split(",");
				for(String id : idList){
					teachers.add(Integer.parseInt(id));
				}
			}
		if(teachers.size()>0)
		 listTeacherPersonalInfos=teacherPersonalInfoDAO.findBYTeachersIds(teachers);
			
		int malecount		=	0;
		int femalecount		=	0;
		int declinecount	=	0;
		int noresponsecount	=	0;
			for(TeacherPersonalInfo tpf : listTeacherPersonalInfos){
			   if(tpf.getGenderId()!= null)
			   {
				    if(tpf.getGenderId().getGenderId()==0){
				    	noresponsecount++;
				    }

				    if(tpf.getGenderId().getGenderId()==1){
				    	malecount++;
				    }
				    if(tpf.getGenderId().getGenderId()==2){
				    	femalecount++;
				    }
				    if(tpf.getGenderId().getGenderId()==3){
				    	declinecount++;
				    }
				   
			   }else{
				   noresponsecount++;
			   }
				
			}
	
			
	List<TeacherPersonalInfo> allTeacherPersonalInfos  = teacherPersonalInfoDAO.findAll();
	
	int overallmalecount		=	0;
	int overallfemalecount		=	0;
	int overalldeclinecount	    =	0;
	int overallnoresponsecount	=	0;
		for(TeacherPersonalInfo tpf : allTeacherPersonalInfos){
		   if(tpf.getGenderId()!= null)
		   {
			    if(tpf.getGenderId().getGenderId()==0){
			    	overallnoresponsecount++;
			    }

			    if(tpf.getGenderId().getGenderId()==1){
			    	overallmalecount++;
			    }
			    if(tpf.getGenderId().getGenderId()==2){
			    	overallfemalecount++;
			    }
			    if(tpf.getGenderId().getGenderId()==3){
			    	overalldeclinecount++;
			    }
			   
		   }else{
			   overallnoresponsecount++;
		   }
			
		}
	

//calculation 
	
float malecountprcts				=	((float)malecount/(float)listTeacherPersonalInfos.size())*100;
float femalecountprcts			    =	((float)femalecount/(float)listTeacherPersonalInfos.size())*100;
float declinecountprcts			    =	((float)declinecount/(float)listTeacherPersonalInfos.size())*100;
float noresponsecountprcts		    =	((float)noresponsecount/(float)listTeacherPersonalInfos.size())*100;


float overallmalecountprcts		    =	((float)overallmalecount/(float)allTeacherPersonalInfos.size())*100;
float overallfemalecountprcts		=	((float)overallfemalecount/(float)allTeacherPersonalInfos.size())*100;
float overalldeclinecountprcts	   =	((float)overalldeclinecount/(float)allTeacherPersonalInfos.size())*100;
float overallnoresponsecountprcts	=	((float)overallnoresponsecount/(float)allTeacherPersonalInfos.size())*100;
	
   
	gendercategories.add((double)Math.round(malecountprcts*100)/100);
	gendercategories.add((double)Math.round(femalecountprcts*100)/100);
	gendercategories.add((double)Math.round(declinecountprcts*100)/100);
	gendercategories.add((double)Math.round(noresponsecountprcts*100)/100);
	
	gendercategories.add((double)Math.round(overallmalecountprcts*100)/100);
	gendercategories.add((double)Math.round(overallfemalecountprcts*100)/100);
	gendercategories.add((double)Math.round(overalldeclinecountprcts*100)/100);
	gendercategories.add((double)Math.round(overallnoresponsecountprcts*100)/100);
	

	//System.out.println(" data :: "+gendercategories.toString());
	
	
	}catch(Exception e){}
	

 return gendercategories;
 }


public JSONArray displayRecordsByEthinicity(String teacherIdstr)
   
 {
	System.out.println("@@@@@@@@@@@@@@@ Ethinicity Ajax @@@@@@@@@@@@@@@@@@@@@");
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	JSONArray gendercategories = new JSONArray();
	try{
		//System.out.println("teacherIdstr :: "+teacherIdstr);
		List<TeacherPersonalInfo> listTeacherPersonalInfos=new ArrayList<TeacherPersonalInfo>();
		String idList[];
		TreeSet<Integer> teachers =new TreeSet<Integer>();
		if(!teacherIdstr.equals("")){
			idList = teacherIdstr.split(",");
			//System.out.println(">>>>>>>>>>>>>>>>> :: "+idList.length);
			for(String id : idList){
				teachers.add(Integer.parseInt(id));
			}
		}
	if(teachers.size()>0)
	 listTeacherPersonalInfos=teacherPersonalInfoDAO.findBYTeachersIds(teachers);
				
		int hisponiccount   		=	0;
		int nonhisponiccount		=	0;
		int declinecount	        =	0;
		int noresponsecount			=   0;
	
			for(TeacherPersonalInfo tpf : listTeacherPersonalInfos){
			   if(tpf.getEthnicityId()!= null)
			   {
				    if(tpf.getEthnicityId().getEthnicityId()==1){
				    	hisponiccount++;
				    }

				    if(tpf.getEthnicityId().getEthnicityId()==2){
				    	nonhisponiccount++;
				    }
				    if(tpf.getEthnicityId().getEthnicityId()==5){
				    	declinecount++;
				    }
				    
			   }
			   else{
				   noresponsecount++; 
			   }
				
			}
		int totalRecordethinicity  =  hisponiccount+nonhisponiccount+declinecount+noresponsecount;
		
	List<TeacherPersonalInfo> allTeacherPersonalInfos  = teacherPersonalInfoDAO.findAll();
	
	int overallhisponiccount		=	0;
	int overallnonhisponiccount		=	0;
	int overalldeclinecount	        =	0;
	int overallnoresponsecount		=   0;
	
		for(TeacherPersonalInfo tpf : allTeacherPersonalInfos){
		   if(tpf.getEthnicityId()!= null)
		   {
			    if(tpf.getEthnicityId().getEthnicityId()==1){
			    	overallhisponiccount++;
			    }
			    if(tpf.getEthnicityId().getEthnicityId()==2){
			    	overallnonhisponiccount++;
			    }
			    if(tpf.getEthnicityId().getEthnicityId()==5){
			    	overalldeclinecount++;
			    }
		   }
		   else{
			   overallnoresponsecount++;
		   }
			
		}
	int totaloverallethinicity = overallhisponiccount+overallnonhisponiccount+overalldeclinecount+overallnoresponsecount;

//calculation 
	
float hisponiccountprcts				=	((float)hisponiccount/(float)totalRecordethinicity)*100;
float nonhisponiccountprcts			    =	((float)nonhisponiccount/(float)totalRecordethinicity)*100;
float declinecountprcts		            =	((float)declinecount/(float)totalRecordethinicity)*100;
float noresponsecountprcts		        =	((float)noresponsecount/(float)totalRecordethinicity)*100;

//System.out.println("hisponiccountprcts :: "+hisponiccountprcts +" nonhisponiccountprcts :: "+nonhisponiccountprcts+" declinecountprctsv :: "+declinecountprcts+" noresponsecountprcts "+noresponsecountprcts);
float overallhisponiccountprcts		    =	((float)overallhisponiccount/(float)totaloverallethinicity)*100;
float overallnonhisponiccountprcts		=	((float)overallnonhisponiccount/(float)totaloverallethinicity)*100;
float overalldeclinecountprcts	        =	((float)overalldeclinecount/(float)totaloverallethinicity)*100;
float overallnoresponsecountprcts	    =	((float)overallnoresponsecount/(float)totaloverallethinicity)*100;
	

	gendercategories.add((double)Math.round(hisponiccountprcts*100)/100);
	gendercategories.add((double)Math.round(nonhisponiccountprcts*100)/100);
	gendercategories.add((double)Math.round(declinecountprcts*100)/100);
	gendercategories.add((double)Math.round(noresponsecountprcts*100)/100);
	
	
	
	gendercategories.add((double)Math.round(overallhisponiccountprcts*100)/100);
	gendercategories.add((double)Math.round(overallnonhisponiccountprcts*100)/100);
	gendercategories.add((double)Math.round(overalldeclinecountprcts*100)/100);
	gendercategories.add((double)Math.round(overallnoresponsecountprcts*100)/100);
	
	
	
	}catch(Exception e){}
	

 return gendercategories;
 }



public JSONArray displayRecordsByEthinicOrigin(String teacherIdstr)
 {
	System.out.println("@@@@@@@@@@@@@@ EthinicOrigin Ajax@@@@@@@@@@@@@@");
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	JSONArray ethinicorigincategories = new JSONArray();
	try{
		//System.out.println("teacherIdstr :: "+teacherIdstr);
		List<TeacherPersonalInfo> listTeacherPersonalInfos=new ArrayList<TeacherPersonalInfo>();
		String idList[];
		TreeSet<Integer> teachers =new TreeSet<Integer>();
		if(!teacherIdstr.equals("")){
			idList = teacherIdstr.split(",");
			for(String id : idList){
				teachers.add(Integer.parseInt(id));
			}
		}
	if(teachers.size()>0)
	 listTeacherPersonalInfos=teacherPersonalInfoDAO.findBYTeachersIds(teachers);
				
		
		int whiteNhispanic   		=	0;
		int blackNhisponic		    =	0;
		int hispanic   		        =	0;
		int asianpasificIselander	=	0;
		int americanIndian   		=	0;
		int nativehawaiian		    =	0;
		int hispanicwhiteonly   	=	0;
		int hisponicallother		=	0;
		int declinecount	        =	0;
		int noresopnsecount			=	0;
	
			for(TeacherPersonalInfo tpf : listTeacherPersonalInfos){
			   if(tpf.getEthnicOriginId()!= null)
			   {
				    if(tpf.getEthnicOriginId().getEthnicOriginId()==1){
				    	whiteNhispanic++;
				    }
					if(tpf.getEthnicOriginId().getEthnicOriginId()==2){
						blackNhisponic++;		    	
					}
					if(tpf.getEthnicOriginId().getEthnicOriginId()==3){
						hispanic++;
					}
					if(tpf.getEthnicOriginId().getEthnicOriginId()==4){
						asianpasificIselander++;
					}
					if(tpf.getEthnicOriginId().getEthnicOriginId()==5){
						americanIndian++;
					}
					if(tpf.getEthnicOriginId().getEthnicOriginId()==6){
						nativehawaiian++;
					}
					if(tpf.getEthnicOriginId().getEthnicOriginId()==7){
						hispanicwhiteonly++;
					}
					if(tpf.getEthnicOriginId().getEthnicOriginId()==8){
						hisponicallother++;
					}
					if(tpf.getEthnicOriginId().getEthnicOriginId()==9){
						declinecount++;
					}
			   }
			   else
				   noresopnsecount++;
			}
		
		int totalRecordethinicorigin  =  whiteNhispanic+blackNhisponic+hispanic+asianpasificIselander+americanIndian+nativehawaiian+hispanicwhiteonly+hisponicallother+declinecount+noresopnsecount;
	
		//System.out.println("@@@@@@@@ totalRecordethinicorigin @@@ :: "+totalRecordethinicorigin);
		//calculation 
		float whiteNhispanicRate				=	((float)whiteNhispanic/(float)totalRecordethinicorigin)*100;
		float blackNhisponicRate			    =	((float)blackNhisponic/(float)totalRecordethinicorigin)*100;
		float hispanicRate						=	((float)hispanic/(float)totalRecordethinicorigin)*100;
		float asianpasificIselanderRate		    =	((float)asianpasificIselander/(float)totalRecordethinicorigin)*100;
		float americanIndianRate				=	((float)americanIndian/(float)totalRecordethinicorigin)*100;
		float nativehawaiianRate			    =	((float)nativehawaiian/(float)totalRecordethinicorigin)*100;
		float hispanicwhiteonlyRate				=	((float)hispanicwhiteonly/(float)totalRecordethinicorigin)*100;
		float hisponicallotherRate			    =	((float)hisponicallother/(float)totalRecordethinicorigin)*100;
		float declinecountprcts		            =	((float)declinecount/(float)totalRecordethinicorigin)*100;
		float noresopnsecountprcts		        =	((float)noresopnsecount/(float)totalRecordethinicorigin)*100;
		
		ethinicorigincategories.add((double)Math.round(whiteNhispanicRate*100)/100);
		ethinicorigincategories.add((double)Math.round(blackNhisponicRate*100)/100);
		ethinicorigincategories.add((double)Math.round(hispanicRate*100)/100);
		
		ethinicorigincategories.add((double)Math.round(asianpasificIselanderRate*100)/100);
		ethinicorigincategories.add((double)Math.round(americanIndianRate*100)/100);
		ethinicorigincategories.add((double)Math.round(nativehawaiianRate*100)/100);
		
		ethinicorigincategories.add((double)Math.round(hispanicwhiteonlyRate*100)/100);
		ethinicorigincategories.add((double)Math.round(hisponicallotherRate*100)/100);
		ethinicorigincategories.add((double)Math.round(declinecountprcts*100)/100);
		
		ethinicorigincategories.add((double)Math.round(noresopnsecountprcts*100)/100);
		
		
	
	
	List<TeacherPersonalInfo> allTeacherPersonalInfos  = teacherPersonalInfoDAO.findAll();
	
		
	int whiteNhispanicAll   		=	0;
	int blackNhisponicAll		    =	0;
	int hispanicAll   		        =	0;
	int asianpasificIselanderAll	=	0;
	int americanIndianAll   		=	0;
	int nativehawaiianAll		    =	0;
	int hispanicwhiteonlyAll   	    =	0;
	int hisponicallotherAll		    =	0;
	int declinecountAll	            =	0;
	int noresponsecountAll	        =	0;
    
	
		for(TeacherPersonalInfo tpf : allTeacherPersonalInfos)
		{
		   if(tpf.getEthnicOriginId()!= null)
		   {
			  
			   if(tpf.getEthnicOriginId().getEthnicOriginId()==1){
			    	whiteNhispanicAll++;
			    }
				if(tpf.getEthnicOriginId().getEthnicOriginId()==2){
					blackNhisponicAll++;		    	
				}
				if(tpf.getEthnicOriginId().getEthnicOriginId()==3){
					hispanicAll++;
				}
				if(tpf.getEthnicOriginId().getEthnicOriginId()==4){
					asianpasificIselanderAll++;
				}
				if(tpf.getEthnicOriginId().getEthnicOriginId()==5){
					americanIndianAll++;
				}
				if(tpf.getEthnicOriginId().getEthnicOriginId()==6){
					nativehawaiianAll++;
				}
				if(tpf.getEthnicOriginId().getEthnicOriginId()==7){
					hispanicwhiteonlyAll++;
				}
					if(tpf.getEthnicOriginId().getEthnicOriginId()==8){
					hisponicallotherAll++;
				}
					if(tpf.getEthnicOriginId().getEthnicOriginId()==9){
					declinecountAll++;
				}
		   }
		   else
			   noresponsecountAll++;
			
		}
	int totalRecordethinicoriginAll  =  whiteNhispanicAll+blackNhisponicAll+hispanicAll+asianpasificIselanderAll+americanIndianAll+nativehawaiianAll+hispanicwhiteonlyAll+hisponicallotherAll+declinecountAll+noresponsecountAll;



		float whiteNhispanicAllRate		    =	((float)whiteNhispanicAll/(float)totalRecordethinicoriginAll)*100;
		float blackNhisponicAllRate		    =	((float)blackNhisponicAll/(float)totalRecordethinicoriginAll)*100;
		float hispanicAllRate		        =	((float)hispanicAll/(float)totalRecordethinicoriginAll)*100;
		float asianpasificIselanderAllRate	=	((float)asianpasificIselanderAll/(float)totalRecordethinicoriginAll)*100;
		float americanIndianAllRate		    =	((float)americanIndianAll/(float)totalRecordethinicoriginAll)*100;
		float nativehawaiianAllRate		    =	((float)nativehawaiianAll/(float)totalRecordethinicoriginAll)*100;
		float hispanicwhiteonlyAllRate	    =	((float)hispanicwhiteonlyAll/(float)totalRecordethinicoriginAll)*100;
		float hisponicallotherAllRate		=	((float)hisponicallotherAll/(float)totalRecordethinicoriginAll)*100;
		float declinecountAllRate		    =	((float)declinecountAll/(float)totalRecordethinicoriginAll)*100;
		
		float noresponsecountAllRate		=	((float)noresponsecountAll/(float)totalRecordethinicoriginAll)*100;
		

	ethinicorigincategories.add((double)Math.round(whiteNhispanicAllRate*100)/100);
	ethinicorigincategories.add((double)Math.round(blackNhisponicAllRate*100)/100);
	ethinicorigincategories.add((double)Math.round(hispanicAllRate*100)/100);
	
	ethinicorigincategories.add((double)Math.round(asianpasificIselanderAllRate*100)/100);
	ethinicorigincategories.add((double)Math.round(americanIndianAllRate*100)/100);
	ethinicorigincategories.add((double)Math.round(nativehawaiianAllRate*100)/100);
	
	ethinicorigincategories.add((double)Math.round(hispanicwhiteonlyAllRate*100)/100);
	ethinicorigincategories.add((double)Math.round(hisponicallotherAllRate*100)/100);
	ethinicorigincategories.add((double)Math.round(declinecountAllRate*100)/100);
	
	ethinicorigincategories.add((double)Math.round(noresponsecountAllRate*100)/100);
	
	
	
	}catch(Exception e){}
	
 return ethinicorigincategories;
 }


public JSONArray displayRecordsByRaces(String teacherIdstr)
 {
	System.out.println("@@@@@@@@@@@@@@ Races Ajax @@@@@@@@@@@@@@@@@");
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	JSONArray racecategories = new JSONArray();
	try{
		//System.out.println("teacherIdstr :: "+teacherIdstr);
		List<TeacherPersonalInfo> listTeacherPersonalInfos=new ArrayList<TeacherPersonalInfo>();
		String idList[];
		TreeSet<Integer> teachers =new TreeSet<Integer>();
		if(!teacherIdstr.equals("")){
			idList = teacherIdstr.split(",");
			for(String id : idList){
				teachers.add(Integer.parseInt(id));
			}
		}
	if(teachers.size()>0)
	 listTeacherPersonalInfos=teacherPersonalInfoDAO.findBYTeachersIds(teachers);
	
		int noresponse      =   0;
		int black   		=	0;
		int asian			=	0;
		int american 	    =	0;
		int hispanic		=	0;
		int white 			=   0;
		int decline			=	0;
		int nativeHawaiian  =   0; 
	
			for(TeacherPersonalInfo tpf : listTeacherPersonalInfos){
			   if(tpf.getRaceId()!= null)
			   {
				  String []raceIDS  =   tpf.getRaceId().split(",");
				   for(int i=0;i<raceIDS.length;i++)
				    {
					   int raceId = Integer.parseInt(raceIDS[i]);
					   if(raceId==0){
						   noresponse++;
					   }
						if(raceId==1){
							black++;					   
						}
						if(raceId==2){
							asian++; 
						}
						if(raceId==3){
							american++;
						}
						if(raceId==4){
							hispanic++;   
						}
						if(raceId==5){
							white++;   
						}
						if(raceId==6){
							decline++;   
						}
						if(raceId==7){
					   nativeHawaiian++;   
						}
				    }
				}
			   else{
				   noresponse++;
			   }
			}
		int totalRaces  =  noresponse+black+asian+american+hispanic+white+decline+nativeHawaiian;
		/*System.out.println("Total Races :: "+totalRaces);
	    System.out.println(noresponse+" :: "+black+" :: "+asian+" :: "+american+" :: "+hispanic+" :: "+white+" :: "+decline+" :: "+nativeHawaiian);
	*/
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	    
	    float noresponseRate				=	((float)noresponse/(float)totalRaces)*100;
		float blackRate			    		=	((float)black/(float)totalRaces)*100;
		float asianRate						=	((float)asian/(float)totalRaces)*100;
		float americanRate		   			=	((float)american/(float)totalRaces)*100;
		float hispanicRate			   	    =	((float)hispanic/(float)totalRaces)*100;
		float whiteRate						=	((float)white/(float)totalRaces)*100;
		float declineRate					=	((float)decline/(float)totalRaces)*100;
		float nativeHawaiianRate			=	((float)nativeHawaiian/(float)totalRaces)*100;
		
		racecategories.add((double)Math.round(noresponseRate*100)/100);
		racecategories.add((double)Math.round(blackRate*100)/100);
		racecategories.add((double)Math.round(asianRate*100)/100);
		racecategories.add((double)Math.round(americanRate*100)/100);
		racecategories.add((double)Math.round(hispanicRate*100)/100);
		racecategories.add((double)Math.round(whiteRate*100)/100);
		racecategories.add((double)Math.round(declineRate*100)/100);
		racecategories.add((double)Math.round(nativeHawaiianRate*100)/100);
		
	 //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   
	
	
	List<TeacherPersonalInfo> allTeacherPersonalInfos  = teacherPersonalInfoDAO.findAll();
	
	int noresponseAll        =   0;
	int blackAll  		     =	 0;
	int asianAll			 =	 0;
	int americanAll 	     =	 0;
	int hispanicAll		     =	 0;
	int whiteAll 			 =   0;
	int declineAll			 =	 0;
	int nativeHawaiianAll    =   0; 
	
		for(TeacherPersonalInfo tpf : allTeacherPersonalInfos)
		{
			if(tpf.getRaceId()!= null)
			   {
				  String []raceIDS  =   tpf.getRaceId().split(",");
				   for(int i=0;i<raceIDS.length;i++)
				    {
					   int raceId = Integer.parseInt(raceIDS[i]);
					   if(raceId==0){
						   noresponseAll++;
					   }
						if(raceId==1){
							blackAll++;					   
						}
						if(raceId==2){
							asianAll++; 
						}
						if(raceId==3){
							americanAll++;
						}
						if(raceId==4){
							hispanicAll++;   
						}
						if(raceId==5){
							whiteAll++;   
						}
						if(raceId==6){
							declineAll++;   
						}
						if(raceId==7){
					     nativeHawaiianAll++;   
						}
				    }
				}
			else{
				noresponseAll++;
			}
		}
		int totalAllRaces  =  noresponseAll+blackAll+asianAll+americanAll+hispanicAll+whiteAll+declineAll+nativeHawaiianAll;
		/*System.out.println("Total All Races :: "+totalAllRaces);
	System.out.println(noresponseAll+" :: "+blackAll+" :: "+asianAll+" :: "+americanAll+" :: "+hispanicAll+" :: "+whiteAll+" :: "+declineAll+" :: "+nativeHawaiianAll);
	*/

	
	    float noresponseRateAll				    =	((float)noresponseAll/(float)totalAllRaces)*100;
		float blackRateAll			    		=	((float)blackAll/(float)totalAllRaces)*100;
		float asianRateAll						=	((float)asianAll/(float)totalAllRaces)*100;
		float americanRateAll		   			=	((float)americanAll/(float)totalAllRaces)*100;
		float hispanicRateAll			   	    =	((float)hispanicAll/(float)totalAllRaces)*100;
		float whiteRateAll						=	((float)whiteAll/(float)totalAllRaces)*100;
		float declineRateAll					=	((float)declineAll/(float)totalAllRaces)*100;
		float nativeHawaiianRateAll			    =	((float)nativeHawaiianAll/(float)totalAllRaces)*100;
		
		racecategories.add((double)Math.round(noresponseRateAll*100)/100);
		racecategories.add((double)Math.round(blackRateAll*100)/100);
		racecategories.add((double)Math.round(asianRateAll*100)/100);
		racecategories.add((double)Math.round(americanRateAll*100)/100);
		racecategories.add((double)Math.round(hispanicRateAll*100)/100);
		racecategories.add((double)Math.round(whiteRateAll*100)/100);
		racecategories.add((double)Math.round(declineRateAll*100)/100);
		racecategories.add((double)Math.round(nativeHawaiianRateAll*100)/100);
	
	
	}catch(Exception e){}
	

 return racecategories;
 }




}


