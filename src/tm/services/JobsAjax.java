package tm.services;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobCertification;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.Jobrequisitionnumberstemp;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.CertificationTypeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.JobRequisitionNumbersTempDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

public class JobsAjax
{	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	@Autowired
	private JobRequisitionNumbersTempDAO jobRequisitionNumbersTempDAO;
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	
	@SuppressWarnings("unchecked")
	public String fetchCandidateListOfJobApplied(Integer jobId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		String fileName = null;
		try {
			JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			StatusMaster statusmaster = WorkThreadServlet.statusIdMap.get(8);
			List<JobForTeacher> jobForTeacher=new ArrayList<JobForTeacher>();
			Criterion criterion=Restrictions.eq("jobId", jobOrder);
			Criterion criterion1=Restrictions.ne("status",statusmaster);
			jobForTeacher=jobForTeacherDAO.findByCriteria(criterion,criterion1);
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = request.getSession().getServletContext().getRealPath("/")+ "/candidatelistAppiledforjob";
			
			fileName = "CandidateListforJob" + jobId+ ".xls";

			File file = new File(basePath);
			if (!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);

			file = new File(basePath + "/" + fileName);

			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));

			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;

			WritableWorkbook workbook = Workbook.createWorkbook(file,wbSettings);
			workbook.createSheet("CandidateList", 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);

			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			header.setBackground(Colour.GRAY_25);

			// Write a few headers
			excelSheet.mergeCells(0, 0, 2, 1);
			Label label;
			label = new Label(0, 0, "Candidate List Applied For Job "+jobId, timesBoldUnderline);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 3, 2, 3);
			label = new Label(0, 3, "");
			excelSheet.addCell(label);
			excelSheet.getSettings().setDefaultColumnWidth(18);

			int k = 4;
			int col = 1;
			label = new Label(0, k, "FIRST NAME", header);
			excelSheet.addCell(label);
			label = new Label(1, k, "LAST NAME", header);
			excelSheet.addCell(label);
			label = new Label(++col, k, "EMAIL ADDRESS", header);
			excelSheet.addCell(label);
			k = k+1;
		if (jobForTeacher.size() > 0) {
			for (JobForTeacher jobForTeacher2 : jobForTeacher) {
					TeacherDetail teacherDetail=new TeacherDetail();
					teacherDetail=jobForTeacher2.getTeacherId();
					col = 1;

					label = new Label(0, k, teacherDetail.getFirstName());
					excelSheet.addCell(label);

					label = new Label(1, k, teacherDetail.getLastName());
					excelSheet.addCell(label);

					label = new Label(++col, k, teacherDetail.getEmailAddress());
					excelSheet.addCell(label);
					k++;
					
				}
			}else {
				excelSheet.mergeCells(0, 5, 2, 1);
				label = new Label(0, 5, "No records found");
				excelSheet.addCell(label);
			}
		
			workbook.write();
			workbook.close();
		} catch (Exception e) {
		}
		return fileName;
	}
	
	
	
	public String displayDistrictRecord(int districtId,String status,int jobId,int schoolId,String certType,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{			
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		if(session == null || session.getAttribute("userMaster")==null) 
		{
			return "redirect:quest.do";
		}
		StringBuffer tmRecords =	new StringBuffer();
		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start =((pgNo-1)*noOfRowInPage);
		int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		System.out.println(noOfRowInPage+"::"+pgNo+"::"+start+"::"+end+"::"+totalRecord);
		String sortOrderFieldName="jobId";
		String sortOrderNoField="jobId";

		boolean deafultFlag=false;
		Order  sortOrderStrVal=null;
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
				deafultFlag=false;
			}else{
				deafultFlag=true;
			}			
		}
		String sortOrderTypeVal="0";
		sortOrderStrVal=Order.asc(sortOrderFieldName);
		if(sortOrderType!=null){
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("1")){
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
		}
		if(sortOrderType.equals("")){
			sortOrderStrVal=Order.desc(sortOrderFieldName);
		}	





		try
		{    

			tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			String responseText="";

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobId", locale),sortOrderNoField,"jobId",sortOrderTypeVal,Integer.valueOf(pageNo));
			tmRecords.append("<th valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTitle", locale),sortOrderFieldName,"jobTitle",sortOrderTypeVal,Integer.valueOf(pageNo));
			tmRecords.append("<th valign='top'>"+responseText+"</th>");			

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblStatus", locale),sortOrderFieldName,"status",sortOrderTypeVal,Integer.valueOf(pageNo));
			tmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblPostStDate", locale),sortOrderFieldName,"jobStartDate",sortOrderTypeVal,Integer.valueOf(pageNo));
			tmRecords.append("<th valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblPostEnDate", locale),sortOrderFieldName,"jobEndDate",sortOrderTypeVal,Integer.valueOf(pageNo));
			tmRecords.append("<th valign='top'>"+responseText+"</th>");

			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblApplicants", locale)+" <a href='#' id='iconpophover1' class='net-header-text ' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgTotalNumberApplicants", locale)+"'><span class='icon-question-sign'></span></a></th>");
			tmRecords.append("<script type='text/javascript'>$('#iconpophover1').tooltip();</script>");


			tmRecords.append(" <th   valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");

			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			//System.out.println("status=="+status+"  jobId=="+jobId+"  schoolId=="+schoolId+"  certType=="+certType);
			UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");
			List<JobOrder> jobOrders=new ArrayList<JobOrder>();
			DistrictMaster districtMaster=districtMasterDAO.findByDistrictId(userMaster.getDistrictId().getDistrictId()+"");	
			List<Integer> jobs = new ArrayList<Integer>();
			List<JobOrder> schoolJList	  =	 new ArrayList<JobOrder>();
			List<CertificationTypeMaster> certificationTypeMaster=null;
			List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
			Criterion statusFilter=null;
			Criterion jobFilter=null;
			Criterion schoolFilter=null;
			if(districtMaster!=null && districtMaster!=null)
			{			

				Criterion criterion1 =	Restrictions.eq("districtMaster",districtMaster);
				if(!status.equalsIgnoreCase(""))
				{
					statusFilter=Restrictions.eq("status",status);
				}
				if(jobId!=0)
				{
					jobs.add(jobId);
					if(jobs.size()>0)
					{
						jobFilter	= Restrictions.in("jobId",jobs);						
					}
				}			
				if(statusFilter==null && jobFilter==null)
				{
					System.out.println("all null");					
					jobOrders=jobOrderDAO.findByCriteria(sortOrderStrVal,criterion1);
				}
				else if(statusFilter!=null && jobFilter!=null)
				{
					System.out.println("according status and job id");
					jobOrders=jobOrderDAO.findByCriteria(sortOrderStrVal,criterion1,statusFilter,jobFilter);
				}
				else if(statusFilter!=null && jobFilter==null)
				{
					System.out.println("according status");
					jobOrders=jobOrderDAO.findByCriteria(sortOrderStrVal,criterion1,statusFilter);
				}
				else if(statusFilter==null && jobFilter!=null)
				{
					System.out.println("according status");
					jobOrders=jobOrderDAO.findByCriteria(sortOrderStrVal,criterion1,jobFilter);
				}


				boolean schoolFlag = false;				
				if(schoolId!=0){
					SchoolMaster  sclMaster=null;				
					sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);			
					schoolJList=schoolInJobOrderDAO.findAllJobBySchool(sclMaster);	
					//System.out.println("schoolJList===="+schoolJList.size());
					schoolFlag = true;

				}			
				if(schoolFlag)
					jobOrders.retainAll(schoolJList);
				else
					jobOrders.addAll(schoolJList);	


				boolean certificateFlag = false;		
				if(!certType.equals("0") && !certType.equals("")){						
					certJobOrderList=jobCertificationDAO.findCertificationByJob(certType);
					certificateFlag=true;				   									
					if(certificateFlag)
						jobOrders.retainAll(certJobOrderList);
					else
						jobOrders.addAll(certJobOrderList);	
				}			

			}

			if(jobOrders.size()>0)
			{
				totalRecord =jobOrders.size();
				if(totalRecord<end)
					end=totalRecord;
			}		
			Map<Integer,String> map = new HashMap<Integer, String>();
			if(jobOrders.size()>0)
				map = jobForTeacherDAO.countApplicantsByJobOrdersAllll(jobOrders);

			//totalRecord=jobOrders.size();
			System.out.println("total job==="+jobOrders.size());
			if(jobOrders.size()==0)
				tmRecords.append("<tr><td colspan='10' align='center'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			List<JobOrder>	subjobOrders=null;
			if(jobOrders.size()>0)
			{
				subjobOrders=jobOrders.subList(start,end);	
				for(JobOrder jobOrder:subjobOrders)
				{
					tmRecords.append("<tr>");
					tmRecords.append("<td>"+jobOrder.getJobId()+"</td>");
					tmRecords.append("<td>"+jobOrder.getJobTitle()+"</td>");
					tmRecords.append("<td>");
					if(jobOrder.getStatus().equalsIgnoreCase("A"))
					{
						tmRecords.append(Utility.getLocaleValuePropByKey("optAct", locale));
					}
					else
					{
						tmRecords.append(Utility.getLocaleValuePropByKey("optInActiv", locale));
					}
					tmRecords.append("</td>" );
					
					if(Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobStartDate()).equals("Dec 25, 2099"))
						tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("lblUntilfilled", locale)+"</td>");
					else
						tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobStartDate())+"</td>");


					if(Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobEndDate()).equals("Dec 25, 2099"))
						tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("lblUntilfilled", locale)+"</td>");
					else
						tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobEndDate())+"</td>");

					String appliedStr = null;
					appliedStr = map.get(jobOrder.getJobId());
					if(appliedStr!=null){
						tmRecords.append("<td>"+appliedStr.split("##")[0]+"/"+appliedStr.split("##")[1]+"</td>");			;
					}else
					{
						tmRecords.append("<td>0/0</td>");				
					}


					tmRecords.append("<td nowrap><a data-original-title='"+Utility.getLocaleValuePropByKey("msgClickEditjob", locale)+"' class='editjob' rel='tooltip' href='editJobs.do?JobOrderType="+jobOrder.getCreatedForEntity()+"&JobId="+jobOrder.getJobId()+"&districtId="+jobOrder.getDistrictMaster().getDistrictId().toString()+"'><i class='fa fa-pencil-square-o fa-lg' /></a>");
					tmRecords.append(" | ");           	
					if(jobOrder.getStatus().equalsIgnoreCase("A"))
					{
						tmRecords.append("<a href='javascript:void(0);' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgClickDeactivateJob", locale)+"' class='deactivate' rel='tooltip'  onclick=\"return activateDeactivateJob("+userMaster.getEntityType()+","+jobOrder.getJobId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>");
						tmRecords.append(" | "); 
					}
					else
					{
						tmRecords.append("<a href='javascript:void(0);' data-original-title='"+Utility.getLocaleValuePropByKey("msgClickActivateJob", locale)+"' class='activate' rel='tooltip' onclick=\"return activateDeactivateJob("+userMaster.getEntityType()+","+jobOrder.getJobId()+",'A')\"><i class='fa fa-check fa-lg' /></a>");
						tmRecords.append(" | "); 
					}   
					tmRecords.append("<a href='javascript:void(0);' data-original-title='"+Utility.getLocaleValuePropByKey("msgClickForCandidateList", locale)+"' class='activate' rel='tooltip' onclick=\"return getCandidateListForAppliedJob("+jobOrder.getJobId()+")\"><span class='icon-file-excel iconcolor'></span></a>");
				}
			}
			tmRecords.append("<script>$('.deactivate').tooltip();</script>");
			tmRecords.append("<script>$('.activate').tooltip();</script>");
			tmRecords.append("<script>$('.editjob').tooltip();</script>");
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForManageJobOrdersAjax(request,totalRecord,noOfRow, pageNo));


		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tmRecords.toString();
	}


	/**
	 * 
	 * @param jobId
	 * @param status
	 * @return
	 */
	@Transactional(readOnly=false)
	public boolean activateDeactivateJob(int jobId,String status)
	{
		System.out.println(jobId+":::"+status);
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null || session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		try{
			JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
			jobOrder.setStatus(status);
			jobOrderDAO.updatePersistent(jobOrder);
			try{
				String logType="";
				if(status.equalsIgnoreCase("A")){
					logType="Activate Job Order";
				}else{
					logType="Deactivate Job Order";
				}
				//userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
			}catch(Exception e){
				e.printStackTrace();
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}	

/*	public String getJobCategories()
	{
		System.out.println("--------Getting Active Job Categories-------");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || (session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}		
		StringBuffer sb	=	new StringBuffer();	  
		List <JobCategoryMaster> jobCategoryMasters	= new ArrayList<JobCategoryMaster>();		
		Criterion criterion		= 	Restrictions.eq("status","A");
		jobCategoryMasters = jobCategoryMasterDAO.findByCriteria(criterion);
		if(jobCategoryMasters.size()>0)
		{	
			sb.append("<select id='jobCategoryId'>");		
			sb.append("<option value='0'>Select Job Category Name</option>");
			for(JobCategoryMaster categoryMaster : jobCategoryMasters)
			{
				sb.append("<option value='"+categoryMaster.getJobCategoryId()+"'>"+categoryMaster.getJobCategoryName()+"</option>");
			}
			sb.append("</select>");
		}
		return sb.toString();
	}*/

	public String  displayCertification(int jobOrderId,int JobOrderType){
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		try{
			UserMaster userMaster = null;
			SchoolMaster schoolMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			/*try{
				if(JobOrderType==2){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"editJobs.do",1);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"editJobs.do",2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}*/
			JobOrder jobOrder =jobOrderDAO.findById(jobOrderId,false,false);
			List<JobCertification> lstJobCertification= null;
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			lstJobCertification = jobCertificationDAO.findByCriteria(criterion1);		
			int i=1;
			for (JobCertification jobCertification : lstJobCertification) 
			{
				if(userMaster.getEntityType()==3 && JobOrderType==2){
					tmRecords.append("<div style='margin-top:-20px;' class=\"\" id=\"CertificationGrid"+i+"\" nowrap><input type=\"hidden\" name=\"certificateTypeIds\" id=\"certificateTypeId"+i+"\" value=\""+jobCertification.getJobCertificationId()+"\" >" +
							"<label certtypeidcl=\""+jobCertification.getCertificateTypeMaster().getCertTypeId()+"\" name=\"certificateTypeText\">"+jobCertification.getCertificateTypeMaster().getCertType()+"</label></div>" );
					tmRecords.append("<a href=\"javascript:void(0);\" onclick=\"deleteCertification("+jobCertification.getJobCertificationId()+");\"><img width=\"15\" height=\"15\"  class=\"can\" src=\"images/can-icon.png\"></a>" );
				}else{
					tmRecords.append("<div style='margin-top:-20px;' class=\"\" id=\"CertificationGrid"+i+"\" nowrap>" +
							"<label certtypeidcl=\""+jobCertification.getCertificateTypeMaster().getCertTypeId()+"\" name=\"certificateTypeText\">"+jobCertification.getCertificateTypeMaster().getCertType()+" ");
				//	if(roleAccess.indexOf("|3|")!=-1){
						tmRecords.append("<a href=\"javascript:void(0);\" onclick=\"deleteCertification("+jobCertification.getJobCertificationId()+");\"><img width=\"15\" height=\"15\"  class=\"can\" src=\"images/can-icon.png\"></a>" );
				//	}
					tmRecords.append("</label></div></br>");
				}
				i++;
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tmRecords.toString();
	}

	public String saveJobEequisitionNumbersTemp(String arrReqNumAndSchoolId)
	{	
		
		StringBuffer buffer=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}		
		String tempId=session.getId();		
		SchoolMaster schoolMaster=null;
		try {	
			if(arrReqNumAndSchoolId!="" && arrReqNumAndSchoolId.length()>0)
			{
				String sNewReqTextDetails[]=arrReqNumAndSchoolId.split("::");
				Jobrequisitionnumberstemp jrqn = new Jobrequisitionnumberstemp();
				Jobrequisitionnumberstemp jobrequisitionnumberstemp = new Jobrequisitionnumberstemp();			
				schoolMaster = schoolMasterDAO.findById(Long.parseLong(sNewReqTextDetails[0]), false, false);					
				jrqn.setSchoolMaster(schoolMaster);					
				jrqn.setStatus(0);
				jrqn.setNoOfSchoolExpHires(Integer.valueOf(sNewReqTextDetails[1]));
				jrqn.setTempId(tempId);
				System.out.println("Integer.valueOf(sNewReqTextDetails[1])===="+Integer.valueOf(sNewReqTextDetails[1]));
				if(Integer.valueOf(sNewReqTextDetails[2])==1)
				{
					jobrequisitionnumberstemp=jobRequisitionNumbersTempDAO.findBySchoolMasterAndTempId(schoolMaster, tempId);				
					jobrequisitionnumberstemp.setNoOfSchoolExpHires(Integer.parseInt(sNewReqTextDetails[1]));				
					jobRequisitionNumbersTempDAO.updatePersistent(jobrequisitionnumberstemp);
					System.out.println("update complete");
				}
				else
				{
					System.out.println("insert school");
					jobRequisitionNumbersTempDAO.makePersistent(jrqn);	
					System.out.println("save complete");
				}						
			}		
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return "0";
		}
		return "1";
	}

	public String  displaySchoolTemp(int JobOrderType)
	{
		System.out.println("calling displaySchoolTemp");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		String tempId=session.getId();		
		StringBuffer tmRecords =	new StringBuffer();
		try{

			UserMaster userMaster = null;
			SchoolMaster schoolMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}			
			Criterion criterion1 = Restrictions.eq("tempId", tempId);			
			Map<SchoolMaster, Integer> mapTemp=new HashMap<SchoolMaster, Integer>();
			Map<SchoolMaster, Integer> mapTempReqCount=new HashMap<SchoolMaster, Integer>();		
			List<Jobrequisitionnumberstemp> jobrequisitionnumberstemps=new ArrayList<Jobrequisitionnumberstemp>();
			jobrequisitionnumberstemps=jobRequisitionNumbersTempDAO.findByCriteria(criterion1);		  
			if(jobrequisitionnumberstemps.size() > 0)
			{
				tmRecords.append("<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>"+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAddedOn", locale)+"</th><th> "+Utility.getLocaleValuePropByKey("lblOfExpHi", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th></tr></thead><tbody>");
			}
			if(jobrequisitionnumberstemps.size() > 0)
			{
				for (Jobrequisitionnumberstemp schoolMaster2 : jobrequisitionnumberstemps) 
				{	
					SchoolMaster schoolname=schoolMasterDAO.findById(schoolMaster2.getSchoolMaster().getSchoolId(), false, false);
					tmRecords.append("<tr>" );
					tmRecords.append("<td>"+schoolname.getSchoolName()+"</td>");
					tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</td>");
					tmRecords.append("<td>"+schoolMaster2.getNoOfSchoolExpHires()+"</td>");							
					tmRecords.append("<td nowrap><a href='javascript:void(0);' onclick='return editSchoolInJobOrderTemp("+schoolMaster2.getSchoolMaster().getSchoolId()+")'><span class='fa-pencil-square-o  icon-large  iconcolor'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;");
					tmRecords.append("<a href='javascript:void(0);' onclick='return deleteSchoolInJobOrderTemp("+schoolMaster2.getSchoolMaster().getSchoolId()+")'><span class='fa-trash-o  icon-large  iconcolor'></span></a></td>");			
					tmRecords.append("<tr>");
				}
			}
			tmRecords.append("</table>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tmRecords.toString();
	}

@Transactional(readOnly=false)
	public String addEditJobOrder(int jobCategoryId,int jobId,
			String exitURL,String certificationType,int JobOrderType, int districtOrSchooHiddenlId,
			String  jobTitle,String  jobDescription,int  noOfExpHires,String  jobStartDate,
			String  jobEndDate,int  allSchoolGradeDistrictVal,String jobType,int subjectId) 
	{

		System.out.println("========================== addEditJobOrder ==================================");
		System.out.println("JobcategoryId :"+jobCategoryId);
		System.out.println("jobId :"+jobId);
		System.out.println("exitURL :"+exitURL);
		System.out.println("certificationType :"+certificationType);
		System.out.println("JobOrderType :"+JobOrderType);
		System.out.println("districtOrSchooHiddenlId :"+districtOrSchooHiddenlId);
		System.out.println("jobTitle :"+jobTitle);
		System.out.println("jobDescription :"+jobDescription);
		System.out.println("noOfExpHires :"+noOfExpHires);
		System.out.println("jobStartDate :"+jobStartDate);
		System.out.println("jobEndDate :"+jobEndDate);
		System.out.println("allSchoolGradeDistrictVal :"+allSchoolGradeDistrictVal);
		System.out.println("jobType :"+jobType);
		System.out.println("subjectId :"+subjectId);
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String JobPostingURL="";
		JobOrder jobOrder=null;
		String returnjobTitle="";
		List<SchoolInJobOrder> existSchoolsList= null; //For exist Schools List
		int returnJobId=0;
		try{
			UserMaster userMaster = null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			SubjectMaster subjectMaster = null;
			int entityID=0;
			int createdBy=0;		

			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				entityID=userMaster.getEntityType();
				createdBy=userMaster.getUserId();
			}
			if(districtOrSchooHiddenlId!=0){
				districtMaster =districtMasterDAO.findById(districtOrSchooHiddenlId, false, false);
			}
			
			if(subjectId!=0)
			{
				subjectMaster = subjectMasterDAO.findById(subjectId, false, false);
			}

			if(jobId==0){
				jobOrder=new JobOrder();
				jobOrder.setIsPoolJob(0);
				jobOrder.setNotificationToschool(true);
			}else{
				jobOrder = jobOrderDAO.findById(jobId, false, false);
				returnJobId=jobId;				
				//Get Exist Schools
				//existSchoolsList = schoolInJobOrderDAO.findJobOrder(jobOrder);	
			}
			jobOrder.setDistrictMaster(districtMaster);			
			if(jobId==0)
			{			
				boolean approvalBeforeGoLive=districtMaster.getApprovalBeforeGoLive()==null?false:districtMaster.getApprovalBeforeGoLive();			
				if(approvalBeforeGoLive)
				{
					jobOrder.setApprovalBeforeGoLive(0);
				}
				else
				{
					jobOrder.setApprovalBeforeGoLive(1);
				}
			}            

			if(allSchoolGradeDistrictVal==1)
			{
				System.out.println("privilege to district");
				jobOrder.setWritePrivilegeToSchool(false);
			}
			else if(allSchoolGradeDistrictVal==2)
			{
				System.out.println("privilege to school");
				jobOrder.setWritePrivilegeToSchool(true);				
			}

			jobOrder.setIsExpHireNotEqualToReqNo(false);

			if(jobTitle!=null)
				jobOrder.setJobTitle(jobTitle);

			if(jobStartDate!=null)
				jobOrder.setJobStartDate(Utility.getCurrentDateFormart(jobStartDate));

			if(jobEndDate!=null)
				jobOrder.setJobEndDate(Utility.getCurrentDateFormart(jobEndDate));

			if(jobDescription!=null)
				jobOrder.setJobDescription(jobDescription);

			jobOrder.setJobType(jobType);			
			if(JobOrderType==2){					
				jobOrder.setIsPortfolioNeeded(false);				
			}
			
			if(allSchoolGradeDistrictVal==1 && JobOrderType==2){
				jobOrder.setAllSchoolsInDistrict(2);
				jobOrder.setAllGrades(2);
				jobOrder.setPkOffered(2);
				jobOrder.setKgOffered(2);
				jobOrder.setG01Offered(2);
				jobOrder.setG02Offered(2);
				jobOrder.setG03Offered(2);
				jobOrder.setG04Offered(2);
				jobOrder.setG05Offered(2);
				jobOrder.setG06Offered(2);
				jobOrder.setG07Offered(2);
				jobOrder.setG08Offered(2);
				jobOrder.setG09Offered(2);
				jobOrder.setG10Offered(2);
				jobOrder.setG11Offered(2);
				jobOrder.setG12Offered(2);
				jobOrder.setSelectedSchoolsInDistrict(2);
				jobOrder.setNoSchoolAttach(1);

				if(noOfExpHires!=0){
					jobOrder.setNoOfExpHires(noOfExpHires);
				}	
			}
			if((allSchoolGradeDistrictVal==2 && JobOrderType==2) || JobOrderType==3){
				jobOrder.setAllGrades(2);
				jobOrder.setAllSchoolsInDistrict(2);
				jobOrder.setAllGrades(2);
				jobOrder.setPkOffered(2);
				jobOrder.setKgOffered(2);
				jobOrder.setG01Offered(2);
				jobOrder.setG02Offered(2);
				jobOrder.setG03Offered(2);
				jobOrder.setG04Offered(2);
				jobOrder.setG05Offered(2);
				jobOrder.setG06Offered(2);
				jobOrder.setG07Offered(2);
				jobOrder.setG08Offered(2);
				jobOrder.setG09Offered(2);
				jobOrder.setG10Offered(2);
				jobOrder.setG11Offered(2);
				jobOrder.setG12Offered(2);
				jobOrder.setSelectedSchoolsInDistrict(1);
				jobOrder.setNoSchoolAttach(2);
				jobOrder.setNoOfExpHires(null);
			}			

			jobOrder.setExitURL(exitURL);
			jobOrder.setFlagForURL(1);
			jobOrder.setFlagForMessage(2);
			jobOrder.setCreatedBy(createdBy);
			jobOrder.setCreatedByEntity(entityID);
			jobOrder.setSendAutoVVILink(false);
			jobOrder.setJobStatus("O");
			jobOrder.setStatus("A");
			jobOrder.setIsJobAssessment(false);
			jobOrder.setJobAssessmentStatus(0);
			jobOrder.setIsInviteOnly(false); 
			jobOrder.setOfferVirtualVideoInterview(false);
			if(noOfExpHires!=0){
				jobOrder.setNoOfExpHires(noOfExpHires);
			}
			jobOrder.setCreatedForEntity(JobOrderType);
			
			try{
				JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				jobOrder.setJobCategoryMaster(jobCategoryMaster);
				//SubjectMaster subjectMaster=null;				
				jobOrder.setSubjectMaster(subjectMaster);
			}catch (Exception e) {				
				//e.printStackTrace();
				JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(1, false, false);
				jobOrder.setJobCategoryMaster(jobCategoryMaster);
			}
			String tempId = session.getId();
			List<Jobrequisitionnumberstemp> jobrequisitionnumberstemps = jobRequisitionNumbersTempDAO.findByTempIdList(tempId);
			if(jobId==0){
				if(JobOrderType==2){					
					jobOrder.setIsPortfolioNeeded(false);				
				}			
				if(noOfExpHires!=0){
					jobOrder.setNoOfExpHires(noOfExpHires);
				}
				
				jobOrderDAO.makePersistent(jobOrder);
				System.out.println("==========JOB ORDER PERSIST========");
				returnJobId=jobOrder.getJobId();
				System.out.println("return job Id : "+returnJobId);
				returnjobTitle = jobOrder.getJobTitle();				
				if(returnJobId > 0 && allSchoolGradeDistrictVal==2)
				{		
					System.out.println("allSchoolGradeDistrictVal : "+allSchoolGradeDistrictVal);
					
					if(jobrequisitionnumberstemps.size()>0)
					{						
						for(Jobrequisitionnumberstemp jobrequisitionnumberstemp : jobrequisitionnumberstemps)
						{			
							int count = 0;
							SchoolInJobOrder schoolInJobOrder = new SchoolInJobOrder();	
							schoolInJobOrder.setJobId(jobOrder);
							schoolInJobOrder.setCreatedDateTime(new Date());
							schoolInJobOrder.setSchoolId(jobrequisitionnumberstemp.getSchoolMaster());
							schoolInJobOrder.setNoOfSchoolExpHires(jobrequisitionnumberstemp.getNoOfSchoolExpHires());
							System.out.println("==========school in job order persist==========");
							schoolInJobOrderDAO.makePersistent(schoolInJobOrder);							
							jobRequisitionNumbersTempDAO.makeTransient(jobrequisitionnumberstemp);
							System.out.println("========== data release from jobrequisitionnumberstemp ==========");
						}					
					}					
				}
			}else{	
				System.out.println("=========job update=========");
				jobOrderDAO.updatePersistent(jobOrder);				
				if(jobrequisitionnumberstemps.size()>0)
				{
					int count = 0;
					SchoolMaster schoolMaster_temp=null;
					for(Jobrequisitionnumberstemp jobrequisitionnumberstemp : jobrequisitionnumberstemps)
					{	
						
						SchoolInJobOrder schoolInJobOrder2 = new SchoolInJobOrder();
						schoolMaster_temp=jobrequisitionnumberstemp.getSchoolMaster();						
						Criterion criterion = Restrictions.eq("jobId", jobOrder);
						Criterion criterion2 = Restrictions.eq("schoolId", schoolMaster_temp);				
						List<SchoolInJobOrder> schoolInJobOrders = schoolInJobOrderDAO.findByCriteria(criterion, criterion2);						
						if(schoolInJobOrders!=null && schoolInJobOrders.size()>0)
						{
							schoolInJobOrder2 = schoolInJobOrders.get(0);
							schoolInJobOrder2.setSchoolId(schoolMaster_temp);							
							schoolInJobOrder2.setNoOfSchoolExpHires(jobrequisitionnumberstemp.getNoOfSchoolExpHires());
							System.out.println("==========SCHOOL IN JOB ORDER UPDATE========");
							schoolInJobOrderDAO.updatePersistent(schoolInJobOrder2);
						}
						else
						{
							SchoolInJobOrder schoolInJobOrder = new SchoolInJobOrder();	
							schoolInJobOrder.setJobId(jobOrder);							
							schoolInJobOrder.setCreatedDateTime(new Date());
							schoolInJobOrder.setSchoolId(schoolMaster_temp);
							System.out.println("School Id : "+schoolMaster_temp.getSchoolId());
							schoolInJobOrder.setNoOfSchoolExpHires(jobrequisitionnumberstemp.getNoOfSchoolExpHires());
							System.out.println("==========SCHOOL IN JOB ORDER PERSIST========");
							schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
							System.out.println("==========SCHOOL IN JOB ORDER PERSIST COMPLETE========");
						}
						jobRequisitionNumbersTempDAO.makeTransient(jobrequisitionnumberstemp);
						System.out.println("========== data release from jobrequisitionnumberstemp ==========");
						count++;
					}					
				}		
			}			

			

			/*if(allSchoolGradeDistrictVal==2 && JobOrderType==2){
				deleteSchoolInJobOrder(jobOrder.getJobId(),0);
			}*/
			if(certificationType!="" && !certificationType.equalsIgnoreCase("")){
				try{
					System.out.println("=========certification =========");
					String certificationTypes[]=certificationType.split(",");
					for(int i=0;i<certificationTypes.length;i++){
						JobCertification jobCertification= new JobCertification();
						//String certificationTypeVal=certificationTypes[i].replace("<|>",",");
						String certificationTypeVal=certificationTypes[i];
						CertificateTypeMaster certificateTypeMaster = new CertificateTypeMaster();
						certificateTypeMaster.setCertTypeId(Integer.parseInt(certificationTypeVal));
						jobCertification.setCertificateTypeMaster(certificateTypeMaster);
						//jobCertification.setCertType(certificationTypeVal);
						jobCertification.setJobId(jobOrder);
						System.out.println("==========Job Certification PERSIST========");
						jobCertificationDAO.makePersistent(jobCertification);						
						System.out.println("==========Job Certification PERSIST END========");
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			if(returnJobId!=0){
				JobPostingURL=Utility.getShortURL(Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+returnJobId);
			}

		}catch(Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		System.out.println("Successfully inserted");
		return JobPostingURL+"||"+jobOrder.getJobId();	
	}




	public boolean deleteSchoolInJobOrder(int jobId,int schoolId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			List<SchoolInJobOrder> lstschoolInJobOrder=null; 
			JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			lstschoolInJobOrder =schoolInJobOrderDAO.findByCriteria(criterion1);
			SchoolMaster schoolMaster = schoolMasterDAO.findById(Long.parseLong(""+schoolId), false, false);

			Criterion criterion2 = Restrictions.eq("schoolMaster",schoolMaster);
			Criterion criterion3 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion4 = Restrictions.eq("status",1);
			Criterion criterion5 = Restrictions.and(criterion2, criterion3);
			Criterion criterion6 = Restrictions.and(criterion4, criterion5);

			List<JobRequisitionNumbers> lstcheckJRQNisAnyHired = new ArrayList<JobRequisitionNumbers>();
			lstcheckJRQNisAnyHired = jobRequisitionNumbersDAO.findByCriteria(criterion5,criterion6);

			if(lstcheckJRQNisAnyHired.size()>0)
				return false;

			List<JobRequisitionNumbers> lstJobRequisitionNumbers = new ArrayList<JobRequisitionNumbers>();
			lstJobRequisitionNumbers = jobRequisitionNumbersDAO.findByCriteria(criterion2,criterion3);

			List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>();

			for(JobRequisitionNumbers jbrn : lstJobRequisitionNumbers)
			{
				lstDistrictRequisitionNumbers.add(jbrn.getDistrictRequisitionNumbers());
				jobRequisitionNumbersDAO.makeTransient(jbrn);
			}
			for(DistrictRequisitionNumbers drqn: lstDistrictRequisitionNumbers)
			{
				drqn.setIsUsed(false);
				districtRequisitionNumbersDAO.makePersistent(drqn);
			}
			for(SchoolInJobOrder schoolInJobOrder: lstschoolInJobOrder){
				if(schoolId==0){
					schoolInJobOrderDAO.makeTransient(schoolInJobOrder);
				}else{
					if(schoolInJobOrder.getSchoolId().getSchoolId()==schoolId){
						schoolInJobOrderDAO.makeTransient(schoolInJobOrder);
					}
				}
			}

			try
			{
				// Sending Email to Attach Schools 
				if(jobId!=0)
				{
					//List<SchoolMaster> schoolList = new ArrayList<SchoolMaster>();
					if(schoolId!=0)
					{
						List<UserMaster> schoolAdminList = userMasterDAO.getSchoolAdmin(schoolMaster);

						String mailContent ="" ;

						if(schoolAdminList!=null && schoolAdminList.size()>0)
						{
							for(UserMaster saObj:schoolAdminList)
							{
								System.out.println("  Send Email to School Admin-========== for delete school ==>>>>>>>>>>>============"+saObj.getEmailAddress()+" schoolId "+saObj.getSchoolId().getSchoolName());

								DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
								dsmt.setEmailerService(emailerService);
								dsmt.setMailfrom("ashish.kumar@netsutra.com");
								//dsmt.setMailto("ashish.kumar@netsutra.com");
								dsmt.setMailto(saObj.getEmailAddress());
								dsmt.setMailsubject(saObj.getSchoolId().getSchoolName()+" "+Utility.getLocaleValuePropByKey("msgHasBeenRemovedfromthe", locale)+" "+jobOrder.getJobTitle()+" "+Utility.getLocaleValuePropByKey("lblJobOdr", locale)+"");
								mailContent = MailText.emailToSchoolAdminsForDeleteSchool(request,saObj, jobOrder.getDistrictMaster(), jobOrder.getJobTitle());
								dsmt.setMailcontent(mailContent);
								dsmt.start();

								System.out.println("mailContent ::--for delete school -----> "+mailContent);
							}
						}
						else
						{
							System.out.println(" school is not in user master table.");
						}
					}
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}

		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public String editSchoolInJobOrder(int jobId,long schoolId)
	{
		StringBuffer sb=new StringBuffer();
		try{
			JobOrder jobOrder=jobOrderDAO.findById(jobId,false, false);
			Criterion  criterion1=Restrictions.eq("jobOrder",jobOrder);
			if(schoolId!=0){
				SchoolMaster schoolMaster=schoolMasterDAO.findById(schoolId,false, false);
				Criterion  criterion2=Restrictions.eq("schoolId",schoolMaster);
				
				criterion1=Restrictions.eq("jobId",jobOrder);
				List<SchoolInJobOrder>list=schoolInJobOrderDAO.findByCriteria(criterion1,criterion2);
				SchoolInJobOrder schoolInJobOrders=list.get(0);
				
				sb.append(schoolMaster.getSchoolId()+"!!"+schoolMaster.getSchoolName()+"##");
				sb.append(schoolInJobOrders.getNoOfSchoolExpHires()+"###");
				criterion1=Restrictions.eq("jobOrder",jobOrder);
				criterion2=Restrictions.eq("schoolMaster",schoolMaster);
				
			/*	List<JobRequisitionNumbers>jobRequisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(criterion1,criterion2);
				for(JobRequisitionNumbers jb:jobRequisitionNumbers){
					Integer districtreqId=jb.getDistrictRequisitionNumbers().getDistrictRequisitionId();
					String districtreqNumber=jb.getDistrictRequisitionNumbers().getRequisitionNumber();
					if(jb.getStatus()==0)
						sb.append("<option value='"+districtreqId+"'>"+districtreqNumber+"</option>");
					else
						sb.append("<option value='"+districtreqId+"'"+"disabled='disabled'>"+districtreqNumber+"</option>");
				}*/
			}else{
				List<SchoolInJobOrder>list=schoolInJobOrderDAO.findByCriteria(Restrictions.eq("jobId",jobOrder));
				/*List<JobRequisitionNumbers>jobRequisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(criterion1);
				for(JobRequisitionNumbers jb:jobRequisitionNumbers){
					Integer districtreqId=jb.getDistrictRequisitionNumbers().getDistrictRequisitionId();
					String districtreqNumber=jb.getDistrictRequisitionNumbers().getRequisitionNumber();
					if(jb.getStatus()==0)
						sb.append("<option value='"+districtreqId+"'>"+districtreqNumber+"</option>");
					else
						sb.append("<option value='"+districtreqId+"'"+"disabled='disabled'>"+districtreqNumber+"</option>");
					
					DistrictRequisitionNumbers dd=jb.getDistrictRequisitionNumbers();
					dd.setIsUsed(false);
					districtRequisitionNumbersDAO.makePersistent(dd);
					jobRequisitionNumbersDAO.makeTransient(jb);
				}*/
				for(SchoolInJobOrder sch:list){
					schoolInJobOrderDAO.makeTransient(sch);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
	
	public String  displaySchool(int jobOrderId,int JobOrderType){
		/* ========  For Session time Out Error =========*/
		//System.out.println("calling displaySchool");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		try{

			UserMaster userMaster = null;
			SchoolMaster schoolMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				/*if(JobOrderType==2){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
				}*/
			}catch(Exception e){
				e.printStackTrace();
			}

			JobOrder jobOrder =jobOrderDAO.findById(jobOrderId,false,false);
			List<SchoolInJobOrder> listSchoolInJobOrder	= schoolInJobOrderDAO.findJobOrder(jobOrder);

			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			tmRecords.append("<input type='hidden' id='avlSchoolInJobOrder' value='"+listSchoolInJobOrder.size()+"'>");
			if(listSchoolInJobOrder.size()!=0){
				//if(jobOrder.getDistrictMaster().getIsReqNoRequired())
					tmRecords.append("<table id=\"schoolTable\" width=\"80%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>"+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAddedOn", locale)+"</th><th> "+Utility.getLocaleValuePropByKey("lblOfExpHi", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th></tr></thead><tbody>");
				//else
					//tmRecords.append("<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>School Name</th><th>Added on</th><th> # of Expected Hire(s)</th><th>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th></tr></thead><tbody>");
			}
			int noOfRecordCheck = 0;
			
			
			/*================= Checking If Record Not Found ======================*/
			List<SchoolMaster> schoolMasters= new ArrayList<SchoolMaster>();
			for (SchoolInJobOrder schoolJobOrder : listSchoolInJobOrder){
				schoolMasters.add(schoolJobOrder.getSchoolId());
			}
			Map<Long,List<JobRequisitionNumbers>> jobReqMap= new HashMap<Long, List<JobRequisitionNumbers>>();
			List<JobRequisitionNumbers> jobRequisitionNumbers = jobRequisitionNumbersDAO.findJobRequisitionNumbersBySchoolInJobOrder(jobOrder,schoolMasters);
			//System.out.println("jobRequisitionNumbers::::::::::::::>>>"+jobRequisitionNumbers.size());
			try{
				for (JobRequisitionNumbers jobReqObj : jobRequisitionNumbers) {
					Long schoolId=jobReqObj.getSchoolMaster().getSchoolId();
					List<JobRequisitionNumbers> tAlist = jobReqMap.get(schoolId);
					if(tAlist==null){
						List<JobRequisitionNumbers> jobs = new ArrayList<JobRequisitionNumbers>();
						jobs.add(jobReqObj);
						jobReqMap.put(schoolId, jobs);
					}else{
						tAlist.add(jobReqObj);
						jobReqMap.put(schoolId, tAlist);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			//System.out.println("jobReqMap::::::::::::::>>>"+jobReqMap.size());
			List<TeacherAssessmentStatus> teacherAssessmentStatusList=teacherAssessmentStatusDAO.findHireStatusByJobAndSchools(jobOrder,schoolMasters);
			Map<Long,List<TeacherAssessmentStatus>> assMap= new HashMap<Long, List<TeacherAssessmentStatus>>();
			try{
				for (TeacherAssessmentStatus assObj : teacherAssessmentStatusList) {
					Long schoolId=assObj.getUpdatedBy().getSchoolId().getSchoolId();
					List<TeacherAssessmentStatus> tAlist = assMap.get(schoolId);
					if(tAlist==null){
						List<TeacherAssessmentStatus> assList = new ArrayList<TeacherAssessmentStatus>();
						assList.add(assObj);
						assMap.put(schoolId, assList);
					}else{
						tAlist.add(assObj);
						assMap.put(schoolId, tAlist);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			String location = "";
			for (SchoolInJobOrder schoolJobOrder : listSchoolInJobOrder){
					if(schoolJobOrder.getSchoolId().getLocationCode()!=null && schoolJobOrder.getSchoolId().getLocationCode()!="")
						location = " ("+schoolJobOrder.getSchoolId().getLocationCode()+")";
		
				noOfRecordCheck++;
				int countOfReqNo =0;
				try{
					if(jobReqMap.get(schoolJobOrder.getSchoolId().getSchoolId())!=null){
						countOfReqNo = jobReqMap.get(schoolJobOrder.getSchoolId().getSchoolId()).size();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				tmRecords.append("<tr id='"+schoolJobOrder.getSchoolId().getSchoolId()+"'>" );
				tmRecords.append("<td nowrap >"+schoolJobOrder.getSchoolId().getSchoolName()+location+"</td>");
				tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(schoolJobOrder.getCreatedDateTime())+"</td>");
				tmRecords.append("<td>"+schoolJobOrder.getNoOfSchoolExpHires()+"</td>");
				//if(jobOrder.getDistrictMaster().getIsReqNoRequired())
					/*tmRecords.append("<td>"+countOfReqNo+"</td>");*/
				int val=0;
				try{
					if(assMap.get(schoolJobOrder.getSchoolId().getSchoolId())!=null){
						val=assMap.get(schoolJobOrder.getSchoolId().getSchoolId()).size();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(val==0){
					if(userMaster.getEntityType()==3 && JobOrderType==2){
						tmRecords.append("<td nowrap>&nbsp;</td>");
					}else{
						/*if(roleAccess.indexOf("|3|")!=-1)
						{
							String sReqNumbers="";
							for(JobRequisitionNumbers pojo :jobRequisitionNumbers)
							{
								sReqNumbers=sReqNumbers+pojo.getDistrictRequisitionNumbers().getDistrictRequisitionId()+"#";
							}
							if(sReqNumbers.length() >1)
							{
								sReqNumbers=sReqNumbers.substring(0,sReqNumbers.length()-1);
							}
							//System.out.println("Edit sReqNumbers "+sReqNumbers);
							
							tmRecords.append("<td nowrap><a href='javascript:void(0);' onclick='return editSchoolInJobOrder("+schoolJobOrder.getJobId().getJobId()+","+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-pencil-square-o  icon-large  iconcolor'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;");
							tmRecords.append("<a href='javascript:void(0);' onclick='return deleteSchoolInJobOrder("+schoolJobOrder.getJobId().getJobId()+","+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-trash-o  icon-large  iconcolor'></span></a></td>");
							
							//tmRecords.append("<td style='display:none;'>"+sReqNumbers+"</td>");
							tmRecords.append("<td style='display:none;'>"+schoolJobOrder.getSchoolId().getSchoolId()+"</td>");
							
						}
						else
						{
							tmRecords.append("<td nowrap>&nbsp;</td>");
						}*/
					}
					tmRecords.append("<td nowrap><a href='javascript:void(0);' onclick='return editSchoolInJobOrder("+schoolJobOrder.getJobId().getJobId()+","+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-pencil-square-o  icon-large  iconcolor'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;");
					tmRecords.append("<a href='javascript:void(0);' onclick='return deleteSchoolInJobOrder("+schoolJobOrder.getJobId().getJobId()+","+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-trash-o  icon-large  iconcolor'></span></a></td>");
					tmRecords.append("<td style='display:none;'>"+schoolJobOrder.getSchoolId().getSchoolId()+"</td>");
				}else{
					tmRecords.append("<td nowrap>&nbsp;</td>");
				}
			}
			if(listSchoolInJobOrder.size()!=0)
				tmRecords.append("</table>");
			
			boolean checkSchoolFlag=false;
			if(listSchoolInJobOrder.size()==0)
				 checkSchoolFlag=true;
			tmRecords.append("@@@@"+checkSchoolFlag);
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
	 
		return tmRecords.toString();
	}
	
	
	public String editSchoolInJobOrderTemp(long schoolId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		String sReqText="",sSource="";
		StringBuffer sb=new StringBuffer();
		try{
			
			String tempId=session.getId();
			
			SchoolMaster schoolMaster = schoolMasterDAO.findById(Long.parseLong(""+schoolId), false, false);
			
			Criterion criterion1 = Restrictions.eq("schoolMaster",schoolMaster);
			Criterion criterion2 = Restrictions.eq("tempId",tempId);
			
			List<Jobrequisitionnumberstemp> jobrequisitionnumberstemps=new ArrayList<Jobrequisitionnumberstemp>();
			jobrequisitionnumberstemps=jobRequisitionNumbersTempDAO.findByCriteria(criterion1,criterion2);
			
			
			
			int iNoOfSchoolExpHires=0;
			if(jobrequisitionnumberstemps.size() > 0)
			{
				iNoOfSchoolExpHires=jobrequisitionnumberstemps.get(0).getNoOfSchoolExpHires();
			}
				sb.append(schoolMaster.getSchoolId()+"!!"+schoolMaster.getSchoolName()+"##"+iNoOfSchoolExpHires+"###");
				for(Jobrequisitionnumberstemp jbtemp:jobrequisitionnumberstemps)
				{
					if(jbtemp.getDistrictRequisitionNumbers()!=null)
					{
						Integer districtreqId=jbtemp.getDistrictRequisitionNumbers().getDistrictRequisitionId();
						String districtreqNumber=jbtemp.getDistrictRequisitionNumbers().getRequisitionNumber();
						sb.append("<option value='"+districtreqId+"'>"+districtreqNumber+"</option>");
						sSource="2";
					}
					else if(jbtemp.getReqText()!=null && !jbtemp.getReqText().equalsIgnoreCase(""))
					{
						sReqText=sReqText+","+jbtemp.getReqText();
						sSource="1";
					}
						
				}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(sReqText!=null && !sReqText.equalsIgnoreCase("") && sReqText.length()>1)
		{
			sReqText=sReqText.substring(1);
			sb.append(sReqText);
			
		}
		sb.append("=="+sSource);
		return sb.toString();
	}
	
	public boolean deleteSchoolInJobOrderTemp(int schoolId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			
			String tempId=session.getId();
			
			SchoolMaster schoolMaster = schoolMasterDAO.findById(Long.parseLong(""+schoolId), false, false);
			
			Criterion criterion1 = Restrictions.eq("schoolMaster",schoolMaster);
			Criterion criterion2 = Restrictions.eq("tempId",tempId);
			
			List<Jobrequisitionnumberstemp> jobrequisitionnumberstemps=new ArrayList<Jobrequisitionnumberstemp>();
			jobrequisitionnumberstemps=jobRequisitionNumbersTempDAO.findByCriteria(criterion1,criterion2);
			
			for(Jobrequisitionnumberstemp jobrequisitionnumberstemp:jobrequisitionnumberstemps)
			{
				jobRequisitionNumbersTempDAO.makeTransient(jobrequisitionnumberstemp);
				
			}
			

		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}


