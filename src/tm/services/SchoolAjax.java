package tm.services;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NavigableSet;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictAttachment;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolKeyContact;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SchoolNotes;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.TeacherDetailDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictAttachmentDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.SchoolKeyContactDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SchoolNotesDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.IPAddressUtility;
import tm.utility.ImageResize;
import tm.utility.Utility;


public class SchoolAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}
		
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private SchoolNotesDAO schoolNotesDAO;
	public void setSchoolNotesDAO(SchoolNotesDAO schoolNotesDAO) 
	{
		this.schoolNotesDAO = schoolNotesDAO;
	}
	
	@Autowired
	private UserMasterDAO usermasterdao;
	public void setUsermasterdao(UserMasterDAO usermasterdao) 
	{
		this.usermasterdao = usermasterdao;
	}
	
	@Autowired
	private  TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) 
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	
	@Autowired
	private  RoleMasterDAO roleMasterDAO;
	public void setRoleMasterDAO(RoleMasterDAO roleMasterDAO) 
	{
		this.roleMasterDAO = roleMasterDAO;
	}
	
	@Autowired
	private SchoolKeyContactDAO schoolKeyContactDAO;
	public void setSchoolKeyContactDAO(SchoolKeyContactDAO schoolKeyContactDAO) 
	{
		this.schoolKeyContactDAO = schoolKeyContactDAO;
	}
	
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
 	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) 
 	{
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}
 	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}

	@Autowired
	private DistrictAttachmentDAO districtAttachmentDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;

	/*** sekhar ***/
	// Get School Records as per Entity.
	public String displayRecordsByEntityType(boolean resultFlag,int entityID,int schoolId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer tmRecords =	new StringBuffer();
		try{
			//-- get no of record in grid,
			//-- set start and end position
			//String locale = Utility.getValueOfPropByKey("locale");
			String lblSchoolName = Utility.getLocaleValuePropByKey("tableheaderSchool", locale);
			String lblDistrictName = Utility.getLocaleValuePropByKey("lblDistrictName", locale);
			String lblFinalDecisionMaker = Utility.getLocaleValuePropByKey("lblFinalDecisionMaker", locale);
			String lblActions = Utility.getLocaleValuePropByKey("lblActions", locale);
			String lblOfTeachers = Utility.getLocaleValuePropByKey("lblOfTeachers", locale);
			String lblOfStudents = Utility.getLocaleValuePropByKey("lblOfStudents", locale);
			String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
			String lblEdit  = Utility.getLocaleValuePropByKey("lblEdit", locale);
			String lblView = Utility.getLocaleValuePropByKey("lblView", locale);
			String lblDeactivate = Utility.getLocaleValuePropByKey("lblDeactivate", locale);
			String lblActivate = Utility.getLocaleValuePropByKey("lblActivate", locale);
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord = 0;
			//------------------------------------
			
			System.out.println("start"+start);
			System.out.println("end"+end);

			UserMaster userMaster = null;
			Integer entityIDLogin=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return false;
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				entityIDLogin=userMaster.getEntityType();
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,11,"manageschool.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<DistrictSchools> distSchools		=	districtSchoolsDAO.findSchoolIdAllList();
			List<SchoolMaster> schoolMaster	  =	null;
			/** set default sorting fieldName **/
			String sortOrderFieldName="schoolName";
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/

			Criterion criterionDSchool= Restrictions.in("schoolId",distSchools);
			if(entityID!=0 && entityIDLogin==1 && schoolId==0 && resultFlag){
				//schoolMaster =schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterionDSchool);
				totaRecord = schoolMasterDAO.getRowCount(criterionDSchool);
				schoolMaster = schoolMasterDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionDSchool);
				System.out.println("::::::::::::1st Condition");

			}
			else if(entityIDLogin==2 && schoolId==0){
				DistrictMaster districtMaster = districtMasterDAO.findById(userMaster.getDistrictId().getDistrictId(), false, false);
				Criterion criterion2= Restrictions.eq("districtId",districtMaster);
				//schoolMaster =schoolMasterDAO.findByCriteria(Order.asc("schoolName"),criterion2,criterionDSchool);
				totaRecord = schoolMasterDAO.getRowCount(criterion2,criterionDSchool);
				schoolMaster = schoolMasterDAO.findBySchool(sortOrderStrVal,start,noOfRowInPage,criterion2,criterionDSchool);
			}
			else if(entityIDLogin==2){
				Criterion criterion1= Restrictions.eq("schoolId", (long)schoolId);
				DistrictMaster districtMaster = districtMasterDAO.findById(userMaster.getDistrictId().getDistrictId(), false, false);
				
				Criterion criterion2= Restrictions.eq("districtId",districtMaster);
				//schoolMaster =schoolMasterDAO.findByCriteria(Order.asc("schoolName"),criterion1,criterion2,criterionDSchool);
				totaRecord = schoolMasterDAO.getRowCount(criterion1,criterion2,criterionDSchool);
				schoolMaster = schoolMasterDAO.findBySchool(sortOrderStrVal,start,noOfRowInPage,criterion1,criterion2,criterionDSchool);
				
			}
			else if(entityID==2 && entityIDLogin==1 && schoolId!=0)
			{
				DistrictMaster districtMaster = districtMasterDAO.findById(schoolId, false, false);
				Criterion criterion2= Restrictions.eq("districtId",districtMaster);
				//schoolMaster =schoolMasterDAO.findByCriteria(Order.asc("schoolName"),criterion2,criterionDSchool);
				totaRecord = schoolMasterDAO.getRowCount(criterion2,criterionDSchool);
				schoolMaster = schoolMasterDAO.findBySchool(sortOrderStrVal,start,noOfRowInPage,criterion2,criterionDSchool);
				
			}
			else if(entityID==3 && entityIDLogin==1 && schoolId!=0)
			{
				Criterion criterion1= Restrictions.eq("schoolId", (long)schoolId);
				//schoolMaster =schoolMasterDAO.findByCriteria(Order.asc("schoolName"),criterion1,criterionDSchool);
				totaRecord = schoolMasterDAO.getRowCount(criterion1,criterionDSchool);
				schoolMaster = schoolMasterDAO.findBySchool(sortOrderStrVal,start,noOfRowInPage,criterion1,criterionDSchool);
			}
			else
			{
				Criterion criterion= Restrictions.eq("schoolId", (long)schoolId);
				//schoolMaster =schoolMasterDAO.findByCriteria(Order.asc("schoolName"),criterion,criterionDSchool);
				totaRecord = schoolMasterDAO.getRowCount(criterion,criterionDSchool);
				schoolMaster = schoolMasterDAO.findBySchool(sortOrderStrVal,start,noOfRowInPage,criterion,criterionDSchool);
			}
			
			tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			String responseText="";

			//tmRecords.append("<th width='18%'>School Name</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblSchoolName,sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='18%' valign='top'>"+responseText+"</th>");
			
			//tmRecords.append("<th width='10%'>Final Decision Maker</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblFinalDecisionMaker,sortOrderFieldName,"dmName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='10%' valign='top'>"+responseText+"</th>");
			
			//tmRecords.append("<th width='10%'># of Teachers</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblOfTeachers,sortOrderFieldName,"noOfTeachers",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='10%' valign='top'>"+responseText+"</th>");
			
			//tmRecords.append("<th width='10%'># of Students</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblOfStudents,sortOrderFieldName,"noOfStudents",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='10%' valign='top'>"+responseText+"</th>");
			
			//tmRecords.append("<th  width='18%'>District</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblDistrictName,sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='18%' valign='top'>"+responseText+"</th>");
			
			tmRecords.append(" <th  width='15%'>"+lblActions+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(resultFlag){
				if(schoolMaster.size()==0)
					tmRecords.append("<tr><td colspan='8' align='center'>"+lblNoRecord+"</td></tr>" );
				
				for (SchoolMaster schoolMasterDetails : schoolMaster) 
				{
					tmRecords.append("<tr>" );
					tmRecords.append("<td nowrap >"+schoolMasterDetails.getSchoolName()+"</td>");
					tmRecords.append("<td>"+schoolMasterDetails.getDmName()+"</td>");
					if(schoolMasterDetails.getNoOfTeachers()!=null)
					{
					tmRecords.append("<td>"+schoolMasterDetails.getNoOfTeachers()+"</td>");
					}
					else
					{
						tmRecords.append("<td></td>");	
					}
					if(schoolMasterDetails.getNoOfStudents()!=null)
					{
					tmRecords.append("<td>"+schoolMasterDetails.getNoOfStudents()+"");
					}
					else
					{
						tmRecords.append("<td></td>");
					}
					tmRecords.append("<td>"+schoolMasterDetails.getDistrictId().getDistrictName()+"</td>");
					
					boolean pipeFlag=false;
					if(roleAccess.indexOf("|2|")!=-1){
						tmRecords.append("<td nowrap><a href='editschool.do?&schoolId="+Utility.encryptNo(Integer.parseInt(schoolMasterDetails.getSchoolId()+""))+"'>"+lblEdit+"</a>");
						pipeFlag=true;
					}else if(roleAccess.indexOf("|4|")!=-1){
						tmRecords.append("<td nowrap><a href='editschool.do?&schoolId="+Utility.encryptNo(Integer.parseInt(schoolMasterDetails.getSchoolId()+""))+"'>"+lblView+"</a>");
						pipeFlag=true;
					}
					if(roleAccess.indexOf("|7|")!=-1 && (entityIDLogin==1 || entityIDLogin==2)){
						if(pipeFlag)tmRecords.append(" | ");
						if(schoolMasterDetails.getStatus().equalsIgnoreCase("A")){
							tmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateSchool("+entityID+","+schoolMasterDetails.getSchoolId()+",'I')\">"+lblDeactivate+"");
						}else{
							tmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateSchool("+entityID+","+schoolMasterDetails.getSchoolId()+",'A')\">"+lblActivate+"");
						}
					}
					tmRecords.append("</td>");
				}
			}else{
				tmRecords.append("<tr><td colspan='6'>"+lblNoRecord+"</td></tr>" );
			}
			tmRecords.append("</table>");
			System.out.println("totaRecord"+totaRecord);
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totaRecord,noOfRow, pageNo));

		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tmRecords.toString();
	}
	public String showFile(String districtORSchoool,int districtORSchooolId,String docFileName)
	{
		System.out.println(" showFile");
		
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		String path="";
		String source="";
		String target="";
		try 
		{
			if(districtORSchoool.equalsIgnoreCase("district")){
				source = Utility.getValueOfPropByKey("districtRootPath")+districtORSchooolId+"/"+docFileName;
				target = context.getServletContext().getRealPath("/")+"/"+"/district/"+districtORSchooolId+"/";
			}else{
				source = Utility.getValueOfPropByKey("schoolRootPath")+districtORSchooolId+"/"+docFileName;
				target = context.getServletContext().getRealPath("/")+"/"+"/school/"+districtORSchooolId+"/";
			}
		    File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        
	        FileUtils.copyFile(sourceFile, targetFile);
	        
	        /* @Start
	         * @Ashish Kumar
	         * Description :: Get File Extension and Resize Image Before display.
	         */
	        String ext = null;
	        String s = sourceFile.getName();
	        int i = s.lastIndexOf('.');

	        if (i > 0 &&  i < s.length() - 1) {
	            ext = s.substring(i+1).toLowerCase();
	            System.out.println(" ext of file :: "+ext);
	        }
	        
	        if(ext.equals("jpeg") || ext.equals("png") || ext.equals("gif") || ext.equals("jpg")){
	        	ImageResize.resizeImage(targetDir+"/"+sourceFile.getName());
	        }
	        /* @End
	         * @Ashish Kumar
	         * Description :: Get File Extension and Resize Image Before display.
	         */
	        
	        
	        if(districtORSchoool.equalsIgnoreCase("district")){
	        	  path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+districtORSchooolId+"/"+docFileName; 	
	        }else{
	        	  path = Utility.getValueOfPropByKey("contextBasePath")+"/school/"+districtORSchooolId+"/"+docFileName;
	        }
		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
		}
		
		return path;
	}
	/*** sekhar ***/
	// Get School Records as per School Name.
	public List<SchoolMaster> getFieldOfSchoolList(String fieldOfStudy)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<SchoolMaster> fieldOfSchoolList = null;
		try 
		{
			Criterion criterion = Restrictions.like("schoolName", fieldOfStudy,MatchMode.ANYWHERE);
			if(fieldOfStudy.trim()!=null && fieldOfStudy.trim().length()>0){
				fieldOfSchoolList = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion);
			}else{
				fieldOfSchoolList = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return fieldOfSchoolList;
	}
	/* ===============      Activate Deactivate School        =========================*/
	public boolean activateDeactivateSchool(Long schoolId,String status)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
	    	userMaster=(UserMaster)session.getAttribute("userMaster");
	    }
		try{
			schoolMasterDAO.activateDeactivateSchool(schoolId, status);
			SchoolMaster schooolMaster=schoolMasterDAO.findById(schoolId, false,false);
			
			//--------Rahul Tyagi:19/11/2014------------
			String messageSubject="";
			String messageSend="";
			SimpleDateFormat dformat=new SimpleDateFormat("HH:MM:SS");
			String schoolname=schooolMaster.getSchoolName();
			Date d=new Date();
			String to[]={"rclark@teachermatch.org","krogers@teachermatch.org"};
			if(status.equals("I"))
			{
				messageSubject=Utility.getLocaleValuePropByKey("msgDistrictSchoolDeactivated", locale);
				messageSend=""+Utility.getLocaleValuePropByKey("msgDearTMAdmin", locale)+",<br><br>" +schoolname+
						" "+Utility.getLocaleValuePropByKey("msgWasDeactivatedby", locale)+" " + userMaster.getFirstName()+" "+userMaster.getLastName()
						+" "+Utility.getLocaleValuePropByKey("msgAt3", locale)+" "+dformat.format(d)+". "+Utility.getLocaleValuePropByKey("msgPleaseVerifyWith", locale)+" "+userMaster.getFirstName()+" "+userMaster.getLastName()+
						"("+userMaster.getEmailAddress()+") "+
						Utility.getLocaleValuePropByKey("msgIntendedAction", locale);
				
			}
			
				
			if(status.equals("A"))
			{
				messageSubject=Utility.getLocaleValuePropByKey("msgDistrictSchoolActivated", locale);
				messageSend=""+Utility.getLocaleValuePropByKey("msgDearTMAdmin", locale)+",<br><br>" +schoolname+
						" "+Utility.getLocaleValuePropByKey("msgWasActivatedBy", locale)+" " + userMaster.getFirstName()+" "+userMaster.getLastName()
						+" "+Utility.getLocaleValuePropByKey("msgAt3", locale)+" "+dformat.format(d)+". "+Utility.getLocaleValuePropByKey("msgPleaseVerifyWith", locale)+" "+userMaster.getFirstName()+" "+userMaster.getLastName()+
						"("+userMaster.getEmailAddress()+") "+
						Utility.getLocaleValuePropByKey("msgIntendedAction", locale);
				
			}
					
			System.out.println(messageSend);
			
			Properties  properties = new Properties();  
			try {
				InputStream inputStream = new Utility().getClass().getClassLoader().getResourceAsStream("smtpmail.properties");
				properties.load(inputStream);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		String content="";
			try {
				
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom(properties.getProperty("smtphost.noreplyusername"));
				dsmt.setMailto(properties.getProperty("smtphost.tmsysadmin"));
				dsmt.setMailsubject(messageSubject);
				content=MailText.messageForDefaultFont(messageSend, userMaster);					
				dsmt.setMailcontent(content);
				
				System.out.println("content     "+content);
				try {
					dsmt.start();	
				} catch (Exception e) {}
				
				
				
			} catch (Exception e) {
				e.printStackTrace();
			
			}
			
			
			//-----------END--=--------------------------
			
			try{
				String logType="";
				SchoolMaster schoolMaster = schoolMasterDAO.findById(schoolId, false, false);
				if(status.equalsIgnoreCase("A")){
					logType="Activate School";
				}else{
					logType="Deactivate School";
				}
				/*if(schoolMaster!=null)
					userMaster.setSchoolId(schoolMaster);*/
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
			}catch(Exception e){
				e.printStackTrace();
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
			return true;
		}
		System.out.println("complete");
		return true;
	}
	/*** sekhar ***/
	// Get District Records as per District Name.
	public List<DistrictMaster> getFieldOfDistrictList(String DistrictName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		try{
			
			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			Criterion criterion1 = Restrictions.eq("status", "A");
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);
				
				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);
				
				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
				
			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25,criterion1);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return districtMasterList;
	}
	/*** sekhar ***/
	// Get School Records as per School Name and DistrictId.
	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
	{
		//System.out.println("getFieldOfSchoolList:::::::::::::::::::");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
		List<DistrictSchools> fieldOfSchoolList1 = null;
		List<DistrictSchools> fieldOfSchoolList2 = null;
		try 
		{
			Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
			if(SchoolName.length()>2){
				if(districtIdForSchool==0){
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
						//Collections.sort(fieldOfSchoolList1,SchoolMaster.compSchoolMaster);
						Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
						//Collections.sort(fieldOfSchoolList2,SchoolMaster.compSchoolMaster);
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					}else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
						//Collections.sort(fieldOfSchoolList1,SchoolMaster.compSchoolMaster);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}else{
					DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
					Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
					
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
						Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
						
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					}else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		//Collections.sort(schoolMasterList,SchoolMaster.compSchoolMaster);
		return schoolMasterList;
	}
	/* @Author: Gagan 
	 * @Discription: It is used to displayNotesGrid in editschool Page in Account information  .
	 */
	public String displayNotesGrid(int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			//-- get no of record in grid,
			//-- set start and end position
			

			//String locale = Utility.getValueOfPropByKey("locale");
			String lblNotes = Utility.getLocaleValuePropByKey("lblNotes", locale);
			String lblAddedOn = Utility.getLocaleValuePropByKey("lblAddedOn", locale);
			String lblAddedBy = Utility.getLocaleValuePropByKey("lblAddedBy", locale);
			String lblNoNotes = Utility.getLocaleValuePropByKey("lblNoNotes", locale);
			String lblRemove = Utility.getLocaleValuePropByKey("lblRemove", locale);
			String lblActions = Utility.getLocaleValuePropByKey("lblActions", locale);
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,16,"editschool.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			SortedMap map = new TreeMap();
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"note";
			String sortOrderNoField		=	"note";
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("note") && !sortOrder.equals("addedBy")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("note"))
				 {
					 sortOrderNoField="note";
				 }
				 if(sortOrder.equals("addedBy"))
				 {
					 sortOrderNoField="addedBy";
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			
			List<SchoolNotes> schoolNotes			=	null;
			SchoolMaster schoolMaster 	=	schoolMasterDAO.findById(Long.parseLong(schoolId+""), false, false);
			Criterion criterionSchool 	= 	Restrictions.eq("schoolMaster",schoolMaster);
			schoolNotes					=	schoolNotesDAO.findByCriteria(Order.asc("note"),criterionSchool);
			
			List<SchoolNotes> sortedlstSchoolNotes		=	new ArrayList<SchoolNotes>();
			
			SortedMap<String,SchoolNotes>	sortedMap 	= 	new TreeMap<String,SchoolNotes>();
			if(sortOrderNoField.equals("note"))
			{
				sortOrderFieldName	=	"note";
			}
			if(sortOrderNoField.equals("addedBy"))
			{
				sortOrderFieldName	=	"addedBy";
			}
			int mapFlag=2;
			for (SchoolNotes schoolNote : schoolNotes){
				String orderFieldName=""+schoolNote.getNote().replaceAll("\\<[^>]*>","");
				if(sortOrderFieldName.equals("note")){
					orderFieldName=schoolNote.getNote().replaceAll("\\<[^>]*>","").toUpperCase()+"||"+schoolNote.getNotesId();
					sortedMap.put(orderFieldName+"||",schoolNote);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("addedBy")){
					orderFieldName=schoolNote.getCreatedBy().getFirstName()+" "+schoolNote.getCreatedBy().getLastName()+"||"+schoolNote.getNotesId();
					sortedMap.put(orderFieldName+"||",schoolNote);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstSchoolNotes.add((SchoolNotes) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstSchoolNotes.add((SchoolNotes) sortedMap.get(key));
				}
			}else{
				sortedlstSchoolNotes=schoolNotes;
			}
			
			totalRecord =sortedlstSchoolNotes.size();

			if(totalRecord<end)
				end=totalRecord;
			List<SchoolNotes> lstsortedSchoolNotes		=	sortedlstSchoolNotes.subList(start,end);
			
			dmRecords.append("<table id='schoolNotesTable' border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblNotes,sortOrderFieldName,"note",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblAddedOn,sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='25%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblAddedBy,sortOrderFieldName,"addedBy",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			dmRecords.append("<th >"+lblActions+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(schoolNotes.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+lblNoNotes+"</td></tr>" );

			for (SchoolNotes schoolNotesDetail : lstsortedSchoolNotes) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+schoolNotesDetail.getNote()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(schoolNotesDetail.getCreatedDateTime())+"</td>");
				dmRecords.append("<td>"+schoolNotesDetail.getCreatedBy().getFirstName()+" "+schoolNotesDetail.getCreatedBy().getLastName()+"</td>");
				dmRecords.append("<td>");
				if(roleAccess.indexOf("|3|")!=-1){
						dmRecords.append("<a href='javascript:void(0);' onclick='return deleteNotes("+schoolNotesDetail.getNotesId()+")'>"+lblRemove+"</a>");
				}else{
					dmRecords.append("&nbsp;");
				}
				
				dmRecords.append("</td>");
			}
			dmRecords.append("</table>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	
	/* @Author: Gagan 
	 * @Discription: It is used to save Notes in editschool Page in Account information  .
	 */
	/* ===============      save Key Contact        =========================*/
	public int saveNotes(String note,Integer schoolId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		
		SchoolMaster schoolMaster				=	null;
		UserMaster userMaster					=	null;
		SchoolNotes schoolNotes					=	new SchoolNotes();
		try
		{
			
			schoolMaster						=	schoolMasterDAO.findById(Long.parseLong(""+schoolId), false, false);
			schoolNotes.setSchoolMaster(schoolMaster);
			schoolNotes.setNote(note);
			
			userMaster							=	usermasterdao.findById(userSession.getUserId(), false, false);
			schoolNotes.setCreatedBy(userMaster);
			schoolNotes.setCreatedDateTime(new Date());
			
			schoolNotesDAO.makePersistent(schoolNotes);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 2;
		}

		return 1;
	}
	
	
	/* @Author: Gagan 
	 * @Discription: It is used to delete Notes in editschool Page   .
	 */
	@Transactional(readOnly=false)
	public boolean deleteNotes(Integer notesId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try
		{
			SchoolNotes schoolNotes					=	null;
			schoolNotes								=	schoolNotesDAO.findById(notesId, false, false);
			
			schoolNotesDAO.makeTransient(schoolNotes);
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	/*======== School's User Functionality ============*/
	/*======== It will add school's user (Administrator and Analyst) in usermaster Table and check duplicate user email address in teacherdetail and usermaster table ============*/
	public int saveSchoolAdministratorOrAnalyst(int schoolId,String emailAddress,String firstName,String lastName,int salutation,int entitytype,String title,String phoneNumber,String mobileNumber,String autCode,int roleId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer tmRecords =	new StringBuffer();
		try{
			int userId									=	0;
			int entityType 								=	3;
			SchoolMaster schoolMaster					=	null;
			HeadQuarterMaster headQuarterMaster=null;
			/*======= Check For Unique User Email address ==========*/
			List<UserMaster> dupUserEmail				=	usermasterdao.checkDuplicateUserEmail(userId,emailAddress);
			int size 									=	dupUserEmail.size();
			if(size>0)
				return 3;/*=== it return 3 if find duplicate email address from usermaster Table=======*/
			
			List<TeacherDetail> dupteacherEmail			=	teacherDetailDAO.findByEmail(emailAddress);
			int sizeTeacherList 						=	dupteacherEmail.size();
			if(sizeTeacherList>0)
				return 4;/*=== it return 4 if find duplicate email address from teacherdetail Table=======*/
			
			int verificationCode=(int) Math.round(Math.random() * 2000000);
			
			UserMaster userSession									=	(UserMaster) session.getAttribute("userMaster");
			UserMaster userMaster									=	new UserMaster();
			DistrictMaster districtMaster							=	null;
			
			userMaster.setEntityType(entityType);
			Utility utility											=	new Utility();
			String authenticationCode								=	utility.getUniqueCodebyId((userId+1));
			if(userId!=0)
			{	
				userMaster.setUserId(userId);
				UserMaster userPwd		=	usermasterdao.findById(userId, false, false);
				userMaster.setPassword(userPwd.getPassword());
				userMaster.setAuthenticationCode(userPwd.getAuthenticationCode());
			}
			else
			{
				userMaster.setAuthenticationCode(authenticationCode);
				userMaster.setPassword(MD5Encryption.toMD5("teachermatch"));
		    }
			if(schoolId!=0)
			{
				schoolMaster									=	schoolMasterDAO.findById(Long.parseLong(""+schoolId), false, false);
				
				districtMaster									=	districtMasterDAO.findById(schoolMaster.getDistrictId().getDistrictId(), false, false);
			}
			if(districtMaster!=null && districtMaster.getHeadQuarterMaster()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
			{
				headQuarterMaster=headQuarterMasterDAO.findById(2, false, false);
				if(headQuarterMaster!=null)
				userMaster.setHeadQuarterMaster(headQuarterMaster);
			}	
			
			userMaster.setTitle(title);
			//userMaster.setDistrictId(schoolMaster.getDistrictId().getDistrictId());
			userMaster.setSchoolId(schoolMaster);
			userMaster.setDistrictId(districtMaster);
			userMaster.setFirstName(firstName);
			userMaster.setLastName(lastName);
			userMaster.setSalutation(salutation);
			userMaster.setMobileNumber(mobileNumber);
			userMaster.setPhoneNumber(phoneNumber);
			userMaster.setEmailAddress(emailAddress);
			RoleMaster roleMaster = roleMasterDAO.findById(roleId, false, false);			
			userMaster.setRoleId(roleMaster);
			
			userMaster.setStatus("A");
			userMaster.setVerificationCode(""+verificationCode);
			userMaster.setForgetCounter(0);
			userMaster.setIsQuestCandidate(false);
			userMaster.setCreatedDateTime(new Date());
		
			usermasterdao.makePersistent(userMaster);
			emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), Utility.getLocaleValuePropByKey("msgIAddedYouUser", locale),MailText.createUserPwdMailToOtUser(request,userMaster));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return 2;
		}
		return 1;
	}
	
	
	/* @Author: Gagan 
	 * @Discription: It is used to display school Administrator grid in editschool Page in Contact information  .
	 */
	public String displaySchoolAdministratorGrid(int schoolId,int roleId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		boolean showEditModeUser=true;
		try{
			int roleIdForAccess=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleIdForAccess=userMaster.getRoleId().getRoleId();
				}
				if(userMaster.getEntityType()==3){
					if(userMaster.getDistrictId().getSaAddUser()==null || !userMaster.getDistrictId().getSaAddUser()){
						showEditModeUser=false;
					}
				}
			}
			String roleAccess		=	null;
			int tooltipCounter		=	0;
			String actDeactivateUser=	null;
			if(roleId==3)
			{
				actDeactivateUser	=	"actDeactivateUserAdministrator";
			}
			if(roleId==6)
			{
				actDeactivateUser	=	"actDeactivateUserAnalyst";
			}
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleIdForAccess,15,"editschool.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			SchoolMaster schoolMaster								=	null;
			schoolMaster											=	schoolMasterDAO.findById(Long.parseLong(""+schoolId), false, false);
			
			RoleMaster roleMaster									=	null;
			roleMaster												=	roleMasterDAO.findById(roleId, false, false);
			
			Criterion criterion										=	Restrictions.eq("schoolId",schoolMaster);
			Criterion criterion1									=	Restrictions.eq("roleId",roleMaster);
			List<UserMaster> userMaster								=	usermasterdao.findByCriteria(criterion,criterion1);
			if(userMaster.size()<1)
			{
				if(roleId==3)
				{
					dmRecords.append("<div class='col-sm-3 col-md-3'>"+Utility.getLocaleValuePropByKey("msgNoAdministratorFound", locale)+"</div>");
				}
				if(roleId==6)
				{
					dmRecords.append("<div class='col-sm-3 col-md-3'>"+Utility.getLocaleValuePropByKey("msgNoAnalystFound", locale)+"</div>");
				}
			}
			for (UserMaster userMasterDetail : userMaster) 
			{
				if(userMasterDetail.getStatus().equalsIgnoreCase("A"))
				{
					tooltipCounter++;
					dmRecords.append("<div class='col-sm-3 col-md-3'>"+userMasterDetail.getFirstName()+" "+userMasterDetail.getLastName()+"  ");
					if(roleAccess.indexOf("|3|")!=-1){	
						if(showEditModeUser)
						dmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblDeactivate", locale)+"' rel='tooltip' id='"+actDeactivateUser+""+tooltipCounter+"' href='javascript:void(0);' onclick=\"return activateDeactivateSchoolAdministratorOrAnalyst("+userMasterDetail.getRoleId().getRoleId()+","+userMasterDetail.getUserId()+",'I');\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a>");
					}else
					{
						dmRecords.append("&nbsp;");
					}
					dmRecords.append("</div>");
				}
				else
				{
					tooltipCounter++;
					dmRecords.append("<div class='col-sm-3 col-md-3'>"+userMasterDetail.getFirstName()+" "+userMasterDetail.getLastName()+"  ");
					if(roleAccess.indexOf("|3|")!=-1){	
						if(showEditModeUser)
						dmRecords.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblActivate", locale)+"' rel='tooltip' id='"+actDeactivateUser+""+tooltipCounter+"' href='javascript:void(0);' onclick=\"return activateDeactivateSchoolAdministratorOrAnalyst("+userMasterDetail.getRoleId().getRoleId()+","+userMasterDetail.getUserId()+",'A');\"><img width='15' height='15' class='can' src='images/option02.png' alt=''></a>");
					}else
					{
						dmRecords.append("&nbsp;");
					}
					dmRecords.append("</div>");
				}
				
			}
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	
	/* @Author: Gagan 
	 * @Discription: It is used to save Key Contact in editschool Page in Contact information  .
	 */
	/* ===============      save Key Contact        =========================*/
	public int saveKeyContact(int updateUser,Integer keyContactId,Long schoolId,Integer keyContactTypeId,String keyContactFirstName,String keyContactLastName,String keyContactEmailAddress,String keyContactPhoneNumber,String keyContactTitle,String keyContactIdVal)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		SchoolKeyContact schoolKeyContact	=	new SchoolKeyContact();
		int emailCheck=0;
		int keyContactTypeIdcheck=0; 
		int flagVal=0;
		if(keyContactId	!=	null){	
			flagVal=1;
		}	
		SchoolMaster schoolMaster=null;
		if(schoolId!=0)
		{
			schoolMaster =	schoolMasterDAO.findById(Long.parseLong(""+schoolId), false, false);
		}
		emailCheck=schoolKeyContactDAO.checkEmail(flagVal,keyContactId,keyContactEmailAddress,schoolMaster);
		try
		{
			ContactTypeMaster contactType  = contactTypeMasterDAO.findById(keyContactTypeId, false, false); 		
			keyContactTypeIdcheck=schoolKeyContactDAO.checkkeyContactType(contactType,schoolMaster,keyContactEmailAddress);//shriram
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return 2;
		}
		System.out.println(emailCheck);
		System.out.println(keyContactTypeIdcheck);
		System.out.println(keyContactIdVal);
		if(emailCheck>=1 && keyContactTypeIdcheck>=1 && keyContactIdVal==null){
			return 4;
		}
		String prevEmail="";
		int userId=0;
		try
		{
			if(keyContactId	!=	null)
			{	
				prevEmail=schoolKeyContactDAO.findEmail(keyContactId);
				userId = usermasterdao.getUserId(prevEmail);
				schoolKeyContact.setKeyContactId(keyContactId);
			}
			schoolKeyContact.setSchoolId(schoolId);
			
			ContactTypeMaster contactTypeMaster		=	contactTypeMasterDAO.findById(keyContactTypeId, false, false);
			
			schoolKeyContact.setKeyContactTypeId(contactTypeMaster);
			schoolKeyContact.setKeyContactFirstName(keyContactFirstName);
			schoolKeyContact.setKeyContactLastName(keyContactLastName);
			schoolKeyContact.setKeyContactEmailAddress(keyContactEmailAddress);
			schoolKeyContact.setKeyContactPhoneNumber(keyContactPhoneNumber);
			schoolKeyContact.setKeyContactTitle(keyContactTitle);
			
			/*======= Check For Unique User Email address ==========*/
			List<UserMaster> dupUserEmail				=	usermasterdao.checkDuplicateUserEmail(userId,keyContactEmailAddress);
			int size 									=	dupUserEmail.size();
			
			List<TeacherDetail> dupteacherEmail			=	teacherDetailDAO.findByEmail(keyContactEmailAddress);
			int sizeTeacherList 						=	dupteacherEmail.size();
			
			if(sizeTeacherList>0)
				return 3;
			
			
			UserMaster master=null;
			int insertusermaster=0;
			if((size==0 && sizeTeacherList==0 && updateUser==2) || (userId==0 && size==0 && sizeTeacherList==0 && updateUser==1)){
				int verificationCode=(int) Math.round(Math.random() * 2000000);
				UserMaster userMaster									=	new UserMaster();
				userMaster.setEntityType(3);
				Utility utility											=	new Utility();
				String authenticationCode								=	utility.getUniqueCodebyId((1));
				userMaster.setPassword(MD5Encryption.toMD5("teachermatch"));
				userMaster.setAuthenticationCode(authenticationCode);
				DistrictMaster districtMaster=null;
				
				if(schoolId!=0)
				{
					schoolMaster =	schoolMasterDAO.findById(Long.parseLong(""+schoolId), false, false);
					districtMaster = districtMasterDAO.findById(schoolMaster.getDistrictId().getDistrictId(), false, false);
				}
				userMaster.setTitle(keyContactTitle);
				userMaster.setDistrictId(districtMaster);
				userMaster.setSchoolId(schoolMaster);
				userMaster.setFirstName(keyContactFirstName);
				userMaster.setLastName(keyContactLastName);
				userMaster.setPhoneNumber(keyContactPhoneNumber);
				userMaster.setEmailAddress(keyContactEmailAddress);
				RoleMaster roleMaster = roleMasterDAO.findById(3, false, false);			
				userMaster.setRoleId(roleMaster);
				userMaster.setStatus("A");
				userMaster.setVerificationCode(""+verificationCode);
				userMaster.setForgetCounter(0);
				userMaster.setIsQuestCandidate(false);
				userMaster.setCreatedDateTime(new Date());			
				usermasterdao.makePersistent(userMaster);
				master=userMaster;
				insertusermaster=1;
				emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), Utility.getLocaleValuePropByKey("msgIAddedYouUser", locale),MailText.createUserPwdMailToOtUser(request,userMaster));
			}else if((size!=0 || sizeTeacherList!=0)&& updateUser==1){
				return 3;
			}else if(size==0 && sizeTeacherList==0 && updateUser==1){
				SchoolKeyContact contact=schoolKeyContactDAO.findById(keyContactId, false, false);
				master=new UserMaster();
				master.setUserId(contact.getUserMaster().getUserId());
				schoolKeyContact.setUserMaster(master);
				schoolKeyContactDAO.makePersistent(schoolKeyContact);
				if(updateUser==1){
					UserMaster userMaster =	usermasterdao.findById(userId, false, false);
					userMaster.setTitle(keyContactTitle);
					userMaster.setFirstName(keyContactFirstName);
					userMaster.setLastName(keyContactLastName);
					userMaster.setPhoneNumber(keyContactPhoneNumber);
					userMaster.setEmailAddress(keyContactEmailAddress);
					userMaster.setForgetCounter(0);
					
					if(userMaster.getSchoolId().equals(schoolMaster))
						usermasterdao.makePersistent(userMaster);
				}
			}			
			if(updateUser==2){				
				if(insertusermaster==0)
				{	
					master=new UserMaster();
					int userid=usermasterdao.getUserId(keyContactEmailAddress);				
					master.setUserId(userid);					
				}
				schoolKeyContact.setUserMaster(master);
				schoolKeyContactDAO.makePersistent(schoolKeyContact);
				
			}
			
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 2;
		}

		return 1;
	}
	
	
	/* @Author: Gagan 
	 * @Discription: It is used to display Key Contact grid in editschool Page in Contact information  .
	 */
	public String displaySchoolKeyContactGrid(long schoolId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			//String locale = Utility.getValueOfPropByKey("locale");
			String lblActions = Utility.getLocaleValuePropByKey("lblActions", locale);
			String lblEmail  = Utility.getLocaleValuePropByKey("lblEmail", locale);
			String lblTitle  = Utility.getLocaleValuePropByKey("lblTitle", locale);
			String lblEdit  = Utility.getLocaleValuePropByKey("lblEdit", locale);
			String lblContactType  = Utility.getLocaleValuePropByKey("lblContactType", locale);
			String lblContactName  = Utility.getLocaleValuePropByKey("lblContactName", locale);
			String lblPhone  = Utility.getLocaleValuePropByKey("lblPhone", locale);
			String lblNoContact  = Utility.getLocaleValuePropByKey("lblNoContact", locale);
			String lblDelete   = Utility.getLocaleValuePropByKey("lblDelete", locale);
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,14,"editschool.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<SchoolKeyContact> schoolKeyContact	  	=	null;
			Criterion criterion1 						=	Restrictions.eq("schoolId",schoolId);
			schoolKeyContact = schoolKeyContactDAO.findByCriteria(Order.asc("keyContactFirstName"),criterion1);
			dmRecords.append("<table border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			dmRecords.append("<th width='20%'>"+lblContactType+"</th>");
			dmRecords.append("<th width='20%'>"+lblContactName+"</th>");
			dmRecords.append("<th width='15%'>"+lblEmail+"</th>");
			dmRecords.append("<th width='15%'>"+lblPhone+"</th>");
			dmRecords.append("<th width='15%'>"+lblTitle+"</th>");
			dmRecords.append("<th width='15%'>"+lblActions+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(schoolKeyContact.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+lblNoContact+"</td></tr>" );

			for (SchoolKeyContact schoolKeyContactDetail : schoolKeyContact) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+schoolKeyContactDetail.getKeyContactTypeId().getContactType()+"</td>");
				dmRecords.append("<td>"+schoolKeyContactDetail.getKeyContactFirstName()+" "+schoolKeyContactDetail.getKeyContactLastName()+"</td>");
				dmRecords.append("<td>"+schoolKeyContactDetail.getKeyContactEmailAddress()+"</td>");
				dmRecords.append("<td>"+schoolKeyContactDetail.getKeyContactPhoneNumber()+"</td>");
				dmRecords.append("<td>"+schoolKeyContactDetail.getKeyContactTitle()+"</td>");
				dmRecords.append("<td>");
				if(roleAccess.indexOf("|2|")!=-1){
					dmRecords.append("<a href='javascript:void(0);' onclick='return beforeEditKeyContact("+schoolKeyContactDetail.getKeyContactId()+")'>"+lblEdit+"</a>");
				}else{
					dmRecords.append("&nbsp;");
				}
				if(roleAccess.indexOf("|3|")!=-1){
					dmRecords.append(" | <a href='javascript:void(0);' onclick='return deleteKeyContact("+schoolKeyContactDetail.getKeyContactId()+")'>"+lblDelete+"</a>");
				}
				dmRecords.append("</td>");
			}
			dmRecords.append("</table>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	/* @Author: Gagan 
	 * @Discription: It is used to Edit Key Contact Functionality  .
	 */
	/* ===============    Edit Key Contact Functionality   =========================*/
	@Transactional(readOnly=false)
	public SchoolKeyContact getKeyContactsBykeyContactId(int keyContactId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		SchoolKeyContact schoolKeyContact	=	null;
		try
		{
			schoolKeyContact	=	schoolKeyContactDAO.findById(keyContactId, false, false);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			//return null;
		}
		return schoolKeyContact;
	}
	
	/* @Author: Gagan 
	 * @Discription: It is used to delete key contact   .
	 */
	@Transactional(readOnly=false)
	public boolean deleteKeyContact(Integer keyContactId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
			SchoolKeyContact schoolKeyContact	=	null;
			schoolKeyContact						=	schoolKeyContactDAO.findById(keyContactId, false, false);
			
			schoolKeyContactDAO.makeTransient(schoolKeyContact);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/* @Author: Gagan 
	 * @Discription: It is used to Match pasword of Schools Decision Maker Field Editable in editschool Page in Contact information  .
	 */
	public SchoolMaster matchPwdOfSchoolDm(Long schoolId,String cnfmPwd)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			SchoolMaster schoolMaster 					=	schoolMasterDAO.findById(schoolId, false, false);
			if(schoolMaster.getDmPassword().equals(MD5Encryption.toMD5(cnfmPwd)))
			{
				return schoolMaster;
			}
			else
			{
				return null;
			}
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * 	Purpose		:	Display District Attachment Grid
	 *  By			:	Hanzala Subhani
	 *  Dated		:	31 Oct 2014
	 * 
	 * 
	 * */
	public String displayDistAttachmentGrid(int districtId,Long schoolId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{	
		
		System.out.println(districtId+":::::::::::::::::::::::::::::::::"+schoolId);
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		
		try{
			//-- get no of record in grid,
			//-- set start and end position
			//String locale = Utility.getValueOfPropByKey("locale");
			String lblSource = Utility.getLocaleValuePropByKey("lblSource", locale);
			String lblAddedOn = Utility.getLocaleValuePropByKey("lblAddedOn", locale);
			String lblAddedBy = Utility.getLocaleValuePropByKey("lblAddedBy", locale);
			String lblNoNotes = Utility.getLocaleValuePropByKey("lblNoNotes", locale);
			String lblEdit  = Utility.getLocaleValuePropByKey("lblEdit", locale);
			String lblDelete   = Utility.getLocaleValuePropByKey("lblDelete", locale);
			String lblActions = Utility.getLocaleValuePropByKey("lblActions", locale);
			
			
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,10,"editdistrict.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			SortedMap map = new TreeMap();
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"districtAttachment";
			String sortOrderNoField		=	"districtAttachment";
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("districtAttachment") && !sortOrder.equals("addedBy")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("districtAttachment"))
				 {
					 sortOrderNoField="districtAttachment";
				 }
				 if(sortOrder.equals("addedBy"))
				 {
					 sortOrderNoField="addedBy";
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			
			DistrictMaster districtMaster 	=	districtMasterDAO.findById(districtId, false, false);
			SchoolMaster schoolMaster		=	schoolMasterDAO.findById(schoolId, false, false);
			Criterion criterionDAttach 		= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterionSAttach 		= 	Restrictions.eq("schoolMaster",schoolMaster);
			
			List<DistrictAttachment> districtAttachment	 =	null;
			districtAttachment	=	districtAttachmentDAO.findByCriteria(sortOrderStrVal,criterionSAttach,criterionDAttach);
			List<DistrictAttachment> sortedlstdistrictAttach  =	 new ArrayList<DistrictAttachment>();

			SortedMap<String,DistrictAttachment>	sortedMap = new TreeMap<String,DistrictAttachment>();
			if(sortOrderNoField.equals("districtAttachment"))
			{
				sortOrderFieldName	=	"districtAttachment";
			}
			if(sortOrderNoField.equals("addedBy"))
			{
				sortOrderFieldName	=	"addedBy";
			}
			int mapFlag=2;
			for (DistrictAttachment distAttach : districtAttachment){
				String orderFieldName=""+distAttach.getDistrictAttachment().replaceAll("\\<[^>]*>","");
				if(sortOrderFieldName.equals("districtAttachment")){
					orderFieldName=distAttach.getDistrictAttachment().replaceAll("\\<[^>]*>","").toUpperCase()+"||"+distAttach.getDistrictattachmentId();
					sortedMap.put(orderFieldName+"||",distAttach);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("addedBy")){
					orderFieldName=distAttach.getCreatedBy().getFirstName()+" "+distAttach.getCreatedBy().getLastName()+"||"+distAttach.getDistrictattachmentId();
					sortedMap.put(orderFieldName+"||",distAttach);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet(); 
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstdistrictAttach.add((DistrictAttachment) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstdistrictAttach.add((DistrictAttachment) sortedMap.get(key));
				}
			}else{
				sortedlstdistrictAttach=districtAttachment;
			}

			totalRecord =sortedlstdistrictAttach.size();

			if(totalRecord<end)
				end=totalRecord;
			//List<DistrictAttachment> lstsortedlstdistrictNotes	=	sortedlstdistrictAttach.subList(start,end);
			List<DistrictAttachment> lstsortedlstdistrictAttach		=	sortedlstdistrictAttach.subList(start,end);
			
			dmRecords.append("<table id='distAttachmentTable' border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblSource,sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblAddedOn,sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='25%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblAddedBy,sortOrderFieldName,"addedBy",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			dmRecords.append("<th >"+lblActions+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(districtAttachment.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+lblNoNotes+"</td></tr>" );
			System.out.println("No of Records "+districtAttachment.size());

			for (DistrictAttachment districtAttachDetail : lstsortedlstdistrictAttach) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+districtAttachDetail.getDistrictAttachment()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(districtAttachDetail.getCreatedDateTime())+"</td>");
				dmRecords.append("<td>"+districtAttachDetail.getCreatedBy().getFirstName()+" "+districtAttachDetail.getCreatedBy().getLastName()+"</td>");
				dmRecords.append("<td>");
				dmRecords.append("<a href='javascript:void(0);' onclick='return editDistrictAttachment("+districtAttachDetail.getDistrictattachmentId()+")'>"+lblEdit+"</a>");
				dmRecords.append(" || ");
				dmRecords.append("<a href='javascript:void(0);' onclick='return deleteDistrictAttachment("+districtAttachDetail.getDistrictattachmentId()+")'>"+lblDelete+"</a>");
				dmRecords.append("</td>");
			}
			dmRecords.append("</table>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	} 
	
	
	public int saveDistrictAttachment(String districtId,Long schoolId,String fileName)
	{
		System.out.println(districtId+":::::::	saveDistrictAttachment	:::::::"+fileName);
		
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		
		DistrictMaster  districtMaster			=	null;
		SchoolMaster	schoolMaster			=	null;
		DistrictAttachment districtAttachment	=	new	DistrictAttachment();
		
		try{
			districtMaster	=	districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			schoolMaster	=	schoolMasterDAO.findById(schoolId, false, false);
			districtAttachment.setDistrictMaster(districtMaster);
			districtAttachment.setSchoolMaster(schoolMaster);
			districtAttachment.setCreatedBy(userSession);
			districtAttachment.setDistrictAttachment(fileName);
			districtAttachmentDAO.makePersistent(districtAttachment);
		} catch(Exception e){
			e.printStackTrace();
			return 2;
		}
		return 1;
	}
	
	public String vieDistrictFile(String districtattachmentId) {
		  System.out.println("::::::::::  vieDistrictFile  ::::::::::");
		  String filePath = "";
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) {
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			String path = "";
			try {
				
				DistrictAttachment districtAttachment	=	null;
				districtAttachment		=	districtAttachmentDAO.findById(Integer.parseInt(districtattachmentId), false, false);
				Integer districtId		=	districtAttachment.getDistrictMaster().getDistrictId();
				Long schoolId			=	districtAttachment.getSchoolMaster().getSchoolId();
				String fileName			=	districtAttachment.getDistrictAttachment();
				
				filePath = schoolId+"/SchoolAttachmentFile";
				String source 	= 	Utility.getValueOfPropByKey("schoolRootPath")+filePath+"/"+fileName;
				String target 	= 	context.getServletContext().getRealPath("/")+"/teacher/"+filePath+"/";
				File sourceFile = 	new File(source);
				File targetDir 	= 	new File(target);
				if(!targetDir.exists())
					targetDir.mkdirs();
				File targetFile = new File(targetDir+"/"+sourceFile.getName());
				FileUtils.copyFile(sourceFile, targetFile);
				path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+filePath+"/"+fileName;
				System.out.println("::::::::::::::::::::::::::::"+path);
			} catch (Exception e) {
				e.printStackTrace();
				path="";
			}
			return path;
		 }

public int editDistrictAttachment(String districtId,Long schoolId,String districtattachmentId,String fileName)
	{
		System.out.println(districtId+":::::	editDistrictAttachment	:::::::"+fileName);
		
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");

		try{ System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"+fileName);
			DistrictAttachment districtAttachmentList = districtAttachmentDAO.findById(Integer.parseInt(districtattachmentId), false, false);
			String fileNameDel	  =	districtAttachmentList.getDistrictAttachment();

			districtAttachmentList.setDistrictAttachment(fileName);

			File file = new File(Utility.getValueOfPropByKey("schoolRootPath")+schoolId+"/SchoolAttachmentFile/"+fileNameDel);
			if(file.exists()){
				if(file.delete()){
					System.out.println(file.getName()+ " deleted");
					districtAttachmentDAO.makePersistent(districtAttachmentList);
				}
			}
			
		} catch(Exception e){
			e.printStackTrace();
			return 2;
		}
		return 1;
	}
	
	@Transactional(readOnly=false)
	public boolean deleteDistrictAttachment(Integer districtattachmentId)
	{
		System.out.println("::::::::::::::::"+districtattachmentId);
		
		//File path = new File(Utility.getValueOfPropByKey("districtRootPath")+districtId+"/DistAttachmentFile");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try
		{
			DistrictAttachment districtAttachment	=	null;
			districtAttachment						=	districtAttachmentDAO.findById(districtattachmentId, false, false);
			Integer districtId						=	districtAttachment.getDistrictMaster().getDistrictId();
			Long schoolId							=	districtAttachment.getSchoolMaster().getSchoolId();
			String fileName							=	districtAttachment.getDistrictAttachment();
			File file = new File(Utility.getValueOfPropByKey("schoolRootPath")+schoolId+"/SchoolAttachmentFile/"+fileName);
			if(file.exists()){
				if(file.delete()){
					System.out.println(file.getName()+ " deleted");
					districtAttachmentDAO.makeTransient(districtAttachment);
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	
}


