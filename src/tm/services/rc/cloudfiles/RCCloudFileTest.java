package tm.services.rc.cloudfiles;

import java.io.IOException;

public class RCCloudFileTest {

	public static void main(String[] args) 
	{
		RCCloudFileServices createContainer = new RCCloudFileServices(RCConfig.RC_CLOUDFILE_USERNAME, RCConfig.RC_CLOUDFILE_APIKEY);
	      try {
	         //Create Container
	    	  //System.out.println(createContainer.createContaineronRC());
	    	  
	    	  //get Container
	    	  //createContainer.getContainer();
	    	  //System.out.println(createContainer.getContainer());
	    	  
	    	  //listContainers
	    	  //createContainer.listContainers();
	    	  //System.out.println();
	    	  
	    	  //uploadFileOnRC
	    	  //System.out.println(createContainer.uploadFileOnRC("E:\\test_files\\", "fme-effective-communication.pdf", "Shiva/Ganesha/"));
	    	  
	    	  //
	    	  //System.out.println();
	    	  //System.out.println(createContainer.deleteObjects("teacher/1/Desert.jpg"));
	    	  
	    	  System.out.println("RC_CLOUDFILE_USERNAME "+RCConfig.RC_CLOUDFILE_USERNAME);
	    	  System.out.println("RC_CLOUDFILE_APIKEY "+RCConfig.RC_CLOUDFILE_APIKEY);
	    	  System.out.println("RC_CLOUDFILE_CONTAINER "+RCConfig.RC_CLOUDFILE_CONTAINER);
	    	  
	    	  System.out.println("RC_CLOUDFILE_WRITE "+RCConfig.RC_CLOUDFILE_WRITE);
	    	  System.out.println("RC_CLOUDFILE_READ "+RCConfig.RC_CLOUDFILE_READ);
	    	  
	      }
	      catch (Exception e) {
	         e.printStackTrace();
	      }
	      finally 
	      {
	         try {
				createContainer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	      }
	}

}
