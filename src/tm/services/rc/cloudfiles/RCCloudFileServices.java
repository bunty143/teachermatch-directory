package tm.services.rc.cloudfiles;

import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.jclouds.ContextBuilder;
import org.jclouds.io.Payload;
import org.jclouds.io.Payloads;
import org.jclouds.openstack.swift.v1.domain.Container;
import org.jclouds.openstack.swift.v1.domain.SwiftObject;
import org.jclouds.openstack.swift.v1.features.ObjectApi;
import org.jclouds.openstack.swift.v1.options.CreateContainerOptions;
import org.jclouds.rackspace.cloudfiles.v1.CloudFilesApi;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.ByteSource;
import com.google.common.io.ByteStreams;
import com.google.common.io.Closeables;
import com.google.common.io.Files;

public class RCCloudFileServices implements Closeable 
{
	private final CloudFilesApi cloudFiles;

	public RCCloudFileServices(String username, String apiKey){
		cloudFiles = ContextBuilder.newBuilder(RCConfig.RC_CLOUDFILE_PROVIDER).credentials(username, apiKey).buildApi(CloudFilesApi.class);
	}
	
	 public void close() throws IOException {
	      Closeables.close(cloudFiles, true);
	   }
	 
	 public boolean createContaineronRC() 
	   {
	      System.out.format("Create Container%n");
	      CreateContainerOptions options = CreateContainerOptions.Builder.metadata(ImmutableMap.of("key1", "value1", "key2", "value2"));
	      boolean isCreated=cloudFiles.getContainerApi(RCConfig.RC_CLOUDFILE_REGION_ORD).create(RCConfig.RC_CLOUDFILE_CONTAINER, options);
	      System.out.println("createContainer "+isCreated);
	      System.out.format("  %s%n", RCConfig.RC_CLOUDFILE_CONTAINER);
	      return isCreated;
	   }
	 
	 public void getContainer() {
	      System.out.format("Get Container%n");
	      Container container = cloudFiles.getContainerApi(RCConfig.RC_CLOUDFILE_REGION_ORD).get(RCConfig.RC_CLOUDFILE_CONTAINER);
	      System.out.println("container "+container);
	      System.out.format("  %s%n", container);
	   }
	 
	 public void listContainers() {
	      System.out.format("List Containers%n");

	      List<Container> containers = cloudFiles.getContainerApi(RCConfig.RC_CLOUDFILE_REGION_ORD).list().toList();
	      for (Container container: containers) {
	         System.out.format("  %s%n", container);
	      }
	   }
	 
	 /**
	  * 
	  * @param sSourcePath
	  * @param sFileName
	  * @param sTargetUploadPath
	  * @return
	  * @throws IOException
	  * @author Ramesh Kumar Bhartiya
	  * Date Apr 10, 2015
	  */
	 public String uploadFileOnRC(String sRC_CLOUDFILE_REGION, String sRC_CLOUDFILE_CONTAINER,String sSourcePath,String sFileName,String sTargetUploadPath) throws IOException {
	      System.out.format("Upload Object From File on RackSpace %n");
	      String sUploadFileName="";
	      /*System.out.println("RCConfig.RC_CLOUDFILE_WRITE "+RCConfig.RC_CLOUDFILE_WRITE);
	      System.out.println("RC_CLOUDFILE_READ "+RCConfig.RC_CLOUDFILE_READ);
	      System.out.println("sSourcePath "+sSourcePath);
	      System.out.println("sFileName "+sFileName);
	      System.out.println("sTargetUploadPath "+sTargetUploadPath);
	      System.out.println("sRC_CLOUDFILE_REGION "+sRC_CLOUDFILE_REGION);
	      System.out.println("sRC_CLOUDFILE_CONTAINER "+sRC_CLOUDFILE_CONTAINER);*/
	      
	      if(RCConfig.RC_CLOUDFILE_WRITE)
	      {
	    	  File objFile = new File(sSourcePath, sFileName);
		      ByteSource source = Files.asByteSource(objFile);
		      Payload payload = Payloads.newByteSourcePayload(source);
		      try {
		         sUploadFileName=cloudFiles.getObjectApi(sRC_CLOUDFILE_REGION, sRC_CLOUDFILE_CONTAINER).put(sTargetUploadPath+""+sFileName, payload);
		      } finally {
		    	  return sUploadFileName;
		    	  //largeFile.delete();
		      } 
	      }
	      else
	      {
	    	  System.out.println("Do not have permission to write file on Rackspace Cloud File ... Please RCConfig.RC_CLOUDFILE_WRITE ");
	    	  return sUploadFileName;
	      }
	      
	   }
	 
	 
	 public SwiftObject getObject(String sFileName) {
	      System.out.format("Get Object%n");

	      ObjectApi objectApi = cloudFiles.getObjectApi(RCConfig.RC_CLOUDFILE_REGION_ORD, RCConfig.RC_CLOUDFILE_CONTAINER);
	      SwiftObject swiftObject = objectApi.get(sFileName);

	      System.out.format("  %s%n", swiftObject);

	      return swiftObject;
	   }

	 public String writeObject(SwiftObject swiftObject,String sFileName,String sSourcePath,String sTargetPath) throws IOException {
	      System.out.format("Write Object RC %n");
	      String sSavedFileName="";
	      InputStream inputStream = swiftObject.getPayload().openStream();
	      
	      String slashType = (sFileName.lastIndexOf("/") > 0) ? "/" : "/";
		  int startIndex = sFileName.lastIndexOf(slashType);
	      String extention=sFileName.substring(sFileName.lastIndexOf("."),sFileName.length());
	      System.out.println("extention "+extention);
	      String sFileNameWithOutExt = sFileName.substring(startIndex + 1, sFileName.lastIndexOf("."));
	      System.out.println("sFileNameWithOutExt "+sFileNameWithOutExt +" sTargetPath "+sTargetPath);
	      File FileDirectory=new File(sTargetPath); 
	      File file = File.createTempFile(sFileNameWithOutExt,extention,FileDirectory);
	      
	      BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));

	      try {
	         ByteStreams.copy(inputStream, outputStream);
	      }
	      finally {
	         inputStream.close();
	         outputStream.close();
	      }

	      System.out.format("  %s%n", file.getAbsolutePath());
	      sSavedFileName=file.getName();
	      return sSavedFileName;
	   }
	 
	 public boolean deleteObjects(String sDeleteFileName) {
		 System.out.println(" delete Objects RC "+sDeleteFileName);
		 ObjectApi objectApi = cloudFiles.getObjectApi(RCConfig.RC_CLOUDFILE_REGION_ORD, RCConfig.RC_CLOUDFILE_CONTAINER);
		 SwiftObject swiftObject = objectApi.get(sDeleteFileName);
	     System.out.format("  %s%n", swiftObject);
	     objectApi.delete(swiftObject.getName());
		 
	     SwiftObject isDeletedSwiftObject =getObject(sDeleteFileName);
	     if(isDeletedSwiftObject==null)
	     {
	    	 System.out.println("Deleted "+sDeleteFileName);
	    	 return true;
	     }
	     else
	     {
	    	 return false;
	     }
	   }
}
