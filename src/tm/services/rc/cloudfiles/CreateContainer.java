package tm.services.rc.cloudfiles;

import java.io.Closeable;
import java.io.IOException;
import org.jclouds.ContextBuilder;
import org.jclouds.openstack.swift.v1.options.CreateContainerOptions;
import org.jclouds.rackspace.cloudfiles.v1.CloudFilesApi;
import tm.services.rc.cloudfiles.RCConfig;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Closeables;

public class CreateContainer implements Closeable {
   private final CloudFilesApi cloudFiles;

   public static void main(String[] args) throws IOException {
      CreateContainer createContainer = new CreateContainer(RCConfig.RC_CLOUDFILE_USERNAME, RCConfig.RC_CLOUDFILE_APIKEY);

      try {
         createContainer.createContainer();
      }
      catch (Exception e) {
         e.printStackTrace();
      }
      finally {
         createContainer.close();
      }
   }

   public CreateContainer(String username, String apiKey) 
   {
      cloudFiles = ContextBuilder.newBuilder(RCConfig.RC_CLOUDFILE_PROVIDER).credentials(username, apiKey).buildApi(CloudFilesApi.class);
   }

   public boolean createContainer() 
   {
      System.out.format("Create Container%n");
      CreateContainerOptions options = CreateContainerOptions.Builder.metadata(ImmutableMap.of("key1", "value1", "key2", "value2"));
      boolean isCreated=cloudFiles.getContainerApi(RCConfig.RC_CLOUDFILE_REGION_ORD).create(RCConfig.RC_CLOUDFILE_CONTAINER, options);
      System.out.println("createContainer "+isCreated);
      System.out.format("  %s%n", RCConfig.RC_CLOUDFILE_CONTAINER);
      return isCreated;
   }

   /**
    * Always close your service when you're done with it.
    */
   public void close() throws IOException {
      Closeables.close(cloudFiles, true);
   }
}
