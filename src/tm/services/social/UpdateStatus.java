/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tm.services.social;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import tm.bean.master.TwitterConfig;
import tm.utility.Utility;
import twitter4j.AccountSettings;
import twitter4j.DirectMessage;
import twitter4j.Paging;
import twitter4j.Relationship;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;


public final class UpdateStatus {
    /**
     * Usage: java twitter4j.examples.tweets.UpdateStatus [text]
     *
     * @param args message
     */
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
    public boolean tweet(String message,String pin,TwitterConfig twitterConfig, String baseUrl)
    {
    	try {
    		/*
    		 * oauth.consumerKey=LhApOLvMoCL5ahlrCFq7gg
			   oauth.consumerSecret=kfFXmJi0DfTrZFqo4noTyVXE50K6g5kq6VpIDCQkPb4
               oauth.accessToken=364861233-pxiZzlU3vqDd4SZ8WSkyd4jlSFybVqmv9Y7fpw8d
               oauth.accessTokenSecret=ocrB8nnOm6YSu7eYdeLc5GxhmMTwUVqUXRMv0IplSdA
    		 */
    		String oauthConsumerKey = null;
    		String oauthConsumerSecret = null;
    		String oauthAccessToken = null;
    		String oauthAccessTokenSecret = null;
    		
    		try {
				oauthConsumerKey = Utility.decodeBase64(twitterConfig.getConsumerKey());
				oauthConsumerSecret = Utility.decodeBase64(twitterConfig.getConsumerSecret());
				oauthAccessToken = Utility.decodeBase64(twitterConfig.getAccessToken());
				oauthAccessTokenSecret = Utility.decodeBase64(twitterConfig.getAccessTokenSecret());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
    		ConfigurationBuilder cb = new ConfigurationBuilder();
    		cb.setUseSSL(true);
    		cb.setDebugEnabled(true)
    		  .setOAuthConsumerKey(oauthConsumerKey)
    		  .setOAuthConsumerSecret(oauthConsumerSecret)
    		  .setOAuthAccessToken(oauthAccessToken)
    		  .setOAuthAccessTokenSecret(oauthAccessTokenSecret);
    		TwitterFactory tf = new TwitterFactory(cb.build());
    		Twitter twitter = tf.getInstance();
    		
    		
           // with properties file
    		//Twitter twitter = new TwitterFactory().getInstance();

            
            try {
                // get request token.
                // this will throw IllegalStateException if access token is already available
                RequestToken requestToken = twitter.getOAuthRequestToken();
                System.out.println("Got request token.");
                System.out.println("Request token: " + requestToken.getToken());
                System.out.println("Request token secret: " + requestToken.getTokenSecret());
                
                AccessToken accessToken = null;

                while (null == accessToken) {
                    System.out.println("Open the following URL and grant access to your account:");
                    System.out.println(requestToken.getAuthorizationURL());
                    System.out.print("Enter the PIN(if available) and hit enter after you granted access.[PIN]:");
                   
                    try {
                        if (pin.length() > 0) {
                            accessToken = twitter.getOAuthAccessToken(requestToken, pin);
                        } else {
                            accessToken = twitter.getOAuthAccessToken(requestToken);
                        }
                    } catch (TwitterException te) {
                        if (401 == te.getStatusCode()) {
                            System.out.println("Unable to get the access token.");
                        } else {
                            //te.printStackTrace();
                        }
                    }
                }
                System.out.println("Got access token.");
                System.out.println("Access token: " + accessToken.getToken());
                System.out.println("Access token secret: " + accessToken.getTokenSecret());
            } catch (IllegalStateException ie) {
                // access token is already available, or consumer key/secret is not set.
                if (!twitter.getAuthorization().isEnabled()) {
                    System.out.println("OAuth consumer key/secret is not set.");
                    return false;
                }
            }
            //Status status = twitter.updateStatus(message);
         
            File twitterImageFile = new File(baseUrl);
            StatusUpdate statusUpdate = new StatusUpdate(message);
            statusUpdate.setMedia(twitterImageFile);
            Status status = twitter.updateStatus(statusUpdate);
            
            System.out.println("Successfully updated the status to [" + status.getText() + "].");
            return true;
        } catch (TwitterException te) {
            te.printStackTrace();
            //System.out.println("Failed to get timeline: " + te.getMessage());
        	System.out.println("Failed to get timeline: ");
        	return false;
        } catch (Exception e) {
			e.printStackTrace();
		}
		return false; 
    	
    }
    
 public static void main(String[] args) {
       
	 UpdateStatus us=new UpdateStatus();
	 TwitterConfig twitterConfig = new TwitterConfig();
	/* twitterConfig.setConsumerKey(Utility.encodeInBase64("LhApOLvMoCL5ahlrCFq7gg"));
	 twitterConfig.setConsumerSecret(Utility.encodeInBase64("kfFXmJi0DfTrZFqo4noTyVXE50K6g5kq6VpIDCQkPb4"));
	 twitterConfig.setAccessToken(Utility.encodeInBase64("364861233-pxiZzlU3vqDd4SZ8WSkyd4jlSFybVqmv9Y7fpw8d"));
	 twitterConfig.setAccessTokenSecret(Utility.encodeInBase64("ocrB8nnOm6YSu7eYdeLc5GxhmMTwUVqUXRMv0IplSdA"));*/
	 
	/* twitterConfig.setConsumerKey(Utility.encodeInBase64("rWbh7CSqjEjHpilI8XHXTA"));
	 twitterConfig.setConsumerSecret(Utility.encodeInBase64("x6YAckZeJtervfUR0r2MMbwGeAmJjWzOOaD5ldYNM2I"));
	 twitterConfig.setAccessToken(Utility.encodeInBase64("1292794286-kCxSvJIUHWa53MwsicGXazeHg51ea1g06QkmJl4"));
	 twitterConfig.setAccessTokenSecret(Utility.encodeInBase64("ECnO4hM9jf0o6DHnjOzi4iMGfPfTnPveJCkh83RMJ5Y"));*/
	 
	 //us.tweet("Hi I am hiring","",twitterConfig);
	 
/*	 try {
         Twitter twitter = new TwitterFactory().getInstance();
         Relationship relationship = twitter.showFriendship("testmysite3", "vishwanathJava");
         System.out.println("isSourceBlockingTarget: " + relationship.isSourceBlockingTarget());
         System.out.println("isSourceFollowedByTarget: " + relationship.isSourceFollowedByTarget());
         System.out.println("isSourceFollowingByTarget: " + relationship.isSourceFollowingTarget());
         System.out.println("isSourceNotificationsEnabled: " + relationship.isSourceNotificationsEnabled());
         System.exit(0);
     } catch (TwitterException te) {
         te.printStackTrace();
         System.out.println("Failed to show friendship: " + te.getMessage());
         System.exit(-1);
     }*/
	 
	/* Twitter twitter = new TwitterFactory().getInstance();
     try {
    	 
    	 //("Usage: java twitter4j.examples.directmessage.SendDirectMessage [recipient screen name] [message]"
        // DirectMessage message = twitter.sendDirectMessage("facebookville", "Hi, facebookville");
    	 DirectMessage message = twitter.sendDirectMessage("vishwanathJava", "Hi, vishwanath how r u?");
         System.out.println("Direct message successfully sent to " + message.getRecipientScreenName());
         System.exit(0);
     } catch (TwitterException te) {
         te.printStackTrace();
         System.out.println("Failed to send a direct message: " + te.getMessage());
         System.exit(-1);
     }*/
	 
	 
	 /*try {
         Twitter twitter = new TwitterFactory().getInstance();
         twitter.createFriendship("vishwanathJava");
         System.out.println("Successfully followed [" + "vishwanathJava" + "].");
         System.exit(0);
     } catch (TwitterException te) {
         te.printStackTrace();
         System.out.println("Failed to follow: " + te.getMessage());
         System.exit(-1);
     }*/
     
	 /*try {
         Twitter twitter = new TwitterFactory().getInstance();
         User user = twitter.showUser("vishwanathJava");
         if (user.getStatus() != null) {
             System.out.println("@" + user.getScreenName() + " - " + user.getStatus().getText());
         } else {
             // the user is protected
             System.out.println("@" + user.getScreenName());
         }
         System.exit(0);
     } catch (TwitterException te) {
         te.printStackTrace();
         System.out.println("Failed to delete status: " + te.getMessage());
         System.exit(-1);
     }*/
     
    }
    
}
