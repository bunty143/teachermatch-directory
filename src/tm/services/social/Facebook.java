package tm.services.social;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Iterator;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import tm.utility.Utility;

import java.io.*;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
/* @Author: Vishwanath Kumar
 * @Discription: 
 */
public class Facebook {

	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	private static String readAll(Reader rd) throws IOException 
	{
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	public static String sendRequestToURL(String urlTOcall){
		String url=urlTOcall;
		InputStream is = null;
		String responseText;
		try 
		{
			is = new URL(url).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			responseText = readAll(rd);
			System.out.println(responseText);

		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally 
		{
			try {
				if(is!=null)
					is.close();
				is=null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return responseText;

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	public static String encodeText(String str)
	{
		try {
			str=URLEncoder.encode(str,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return str;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	public static boolean postOnReceiverWall(String accessToken,String fbId,String sender,String msg,String baseUrl,String jobUrl)
	{


		String message=msg;
		String name="TeacherMatch";
		//fbId="me";
		String url = "https://graph.facebook.com/?batch=" +
		""+encodeText("[{ 'method':'POST', 'relative_url':'"+fbId+"/feed', 'body':'message="+message+"" +
				"&name="+name+"&picture="+baseUrl+"" +
		"&link="+jobUrl+"' }]")+
		"&access_token="+accessToken+"&method=post";

		System.out.println(url);
		try {
			sendRequestToURL(url);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	public static String renewAccess_token(String accessToken,String client_id,String client_secret)
	{

		//client_id="253531061354196";
		//client_secret="f88445c4bf05d2dc3bc8eba0ac77a4ff";

		if(accessToken.equals("") || client_id.equals("") || client_secret.equals(""))
			return "";

		//https://graph.facebook.com/oauth/access_token?client_id=253531061354196&client_secret=f88445c4bf05d2dc3bc8eba0ac77a4ff&grant_type=fb_exchange_token&fb_exchange_token=AAADmlczZAZCtQBAKbZA81BAVqMJZBeqfQDtsgFgoWFs1P3xCjQPcKBZBi2CspLyNosZB5BA6hQn9Kt0qdGAtXjvRHJwBBPliyvADAoUcoRZBQZDZD
		String url = "https://graph.facebook.com/oauth/access_token?client_id="+client_id+"&client_secret="+client_secret+"&grant_type=fb_exchange_token&fb_exchange_token="+accessToken;
		System.out.println(url);
		try {
			String responseText=sendRequestToURL(url);
			if(responseText!=null && responseText.contains("access_token"))
			{
				return responseText.replace("access_token=","");
			}
			return "";
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	public static String getFacebookPages(String accessToken)
	{

		if(accessToken.equals(""))
			return "";

		//https://graph.facebook.com/me/accounts?access_token=AAAEbpsG8twUBAOY3XQAwVluYLIPTg721jmb5FdV01TZCFi9bCHkuOgD0NY4P9hbo38om181rZAlfflBlnFt2A158QrV5sVrlbNZBI0KzgZDZD
		String url = "https://graph.facebook.com/me/accounts?type=page&access_token="+accessToken;
		System.out.println(url);
		try {
			String responseText=sendRequestToURL(url);
			//System.out.println("responseText : "+responseText);
			JSONObject json = (JSONObject) JSONSerializer.toJSON(responseText);
			System.out.println("json: "+json);
			JSONArray data = json.getJSONArray("data");
			System.out.println("size: "+data.size());
			if(data.size()==0)
				return "1";
			
			Iterator<JSONObject> itr1 = data.iterator();
			JSONObject innerData = null;
			String name = null;
			String id = null;
			String access_token = null;
			JSONObject returnData = new JSONObject();
			JSONArray jsArr = new JSONArray(); 
			while (itr1.hasNext()) 
			{
				innerData = itr1.next();
				name = innerData.getString("name");
				id = innerData.getString("id");
				access_token = innerData.getString("access_token");
				returnData.put("name", name);
				returnData.put("id",id);
				returnData.put("access_token",access_token);
				
				jsArr.add(returnData);
				//System.out.println("name: "+name);
				//System.out.println("id: "+id);
				//System.out.println("=================================================");
							
			}
			System.out.println("reurnData: "+jsArr);
			return jsArr.toString();
			
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	/*https://graph.facebook.com/me/accounts?access_token=AAAEbpsG8twUBAOY3XQAwVluYLIPTg721jmb5FdV01TZCFi9bCHkuOgD0NY4P9hbo38om181rZAlfflBlnFt2A158QrV5sVrlbNZBI0KzgZDZD

		https://graph.facebook.com/me/accounts?type=page&access_token=AAAEbpsG8twUBAOY3XQAwVluYLIPTg721jmb5FdV01TZCFi9bCHkuOgD0NY4P9hbo38om181rZAlfflBlnFt2A158QrV5sVrlbNZBI0KzgZDZD

		https://graph.facebook.com/search?q=platform&type=page&access_token=AAAEbpsG8twUBAOY3XQAwVluYLIPTg721jmb5FdV01TZCFi9bCHkuOgD0NY4P9hbo38om181rZAlfflBlnFt2A158QrV5sVrlbNZBI0KzgZDZD
		*/
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	public static String getLinkedInAccess_token(String code,String client_id,String client_secret,String redirectURL)
	{

		if(code.equals("") || client_id.equals("") || client_secret.equals(""))
			return "";

		//https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code="+code+"&state=DCEEFWF45453sdffef424&redirect_uri=http://localhost:8080/teachermatch/linkedInsettings.do&client_id=ni8imi0nbdpz&client_secret=VDsIUsRq47AfxzAB
		String url = "https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code="+code+"&state=DCEEFWF45453sdffef424&redirect_uri="+redirectURL+"&client_id="+client_id+"&client_secret="+client_secret;
		System.out.println(url);
		try {
			String responseText=sendRequestToURL(url);
			/*if(responseText!=null && responseText.contains("access_token"))
			{
				return responseText.replace("access_token=","");
			}*/
			return responseText;
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	public static String shareOnLinkedIn(String access_token,String req)
	{
		String retrunValue = "";
		String strURL="https://api.linkedin.com/v1/people/~/shares?oauth2_access_token="+access_token;
		//String strXMLFilename = "c://output.xml";
		//java.io.File input = new java.io.File(strXMLFilename);
		PostMethod post = new PostMethod(strURL);
		try {

			//post.setRequestEntity(new InputStreamRequestEntity(new FileInputStream(input), input.length()));
			/*StringRequestEntity requestEntity = new StringRequestEntity(
				    "",
				    "application/json",
				    "UTF-8");*/
			/*String req="<share><comment>I am hiring Now </comment>"+
			" <content>"+
			"  <title>I am hiring Now</title>"+
			//" <description>Leverage the Share API to maximize engagement on user-generated content on LinkedIn</description>"+
			" <submitted-url>http://demo.teachermatch.org</submitted-url>"+
			"<submitted-image-url>http://demo.teachermatch.org/images/testnew.png</submitted-image-url>"+ 
			"</content>"+
			"<visibility>"+ 
			"<code>anyone</code>"+ 
			"</visibility>"+
			"</share>";*/

			StringRequestEntity requestEntity = new StringRequestEntity(req,"application/xml","UTF-8");

			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
			HttpClient httpclient = new HttpClient();

			int result = httpclient.executeMethod(post);            
			System.out.println("Response status code: " + result);            
			//System.out.println("Response body: ");
			//System.out.println(post.getResponseBodyAsString());    
			retrunValue = post.getResponseBodyAsString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			post.releaseConnection();
		}

		return retrunValue;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	public static boolean postOnReceiverPage(String accessToken,String fbId,String sender,String msg,String baseUrl)
	{

		//https://graph.facebook.com/561575777209806/feed?access_token=AAAEbpsG8twUBAKl81kugTfmgkZBiachrFcD13LZAlCoos0gZCiGvqvkmgq0Cwpy5BQb8GrtIlSzA6ZBIW82NLLLZBJitGda6DhzRi8wQ9rOV9r87rr21j&message=uuuuhhhuuu...
		String message=msg;
		String name="TeacherMatch";
		//fbId="me";
		String url = "https://graph.facebook.com/?batch=" +
		""+encodeText("[{ 'method':'POST', 'relative_url':'"+fbId+"/feed', 'body':'message="+message+"" +
				"&name="+name+"&picture="+baseUrl+"/images/Logo with Beta300.png" +
		"&link="+baseUrl+"' }]")+
		"&access_token="+accessToken+"&method=post";

		System.out.println(url);
		try {
			sendRequestToURL(url);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	public static String postJSON(String access_token,String req)
	{
		String retrunValue = "";
		String strURL="http://dim.herokuapp.com/scrape";
		//String strXMLFilename = "c://output.xml";
		//java.io.File input = new java.io.File(strXMLFilename);
		PostMethod post = new PostMethod(strURL);
		try {

			req ="{\"mpn\":[\"DH361FGK\",\"DH361UGK\",\"DH363FGK\",\"DH361UDK\",\"DH361URK\"]}";
			StringRequestEntity requestEntity = new StringRequestEntity(req,"application/json","UTF-8");
			System.out.println(req);
			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "text/json; charset=ISO-8859-1");//
			HttpClient httpclient = new HttpClient();

			int result = httpclient.executeMethod(post);            
			System.out.println("Response status code: " + result);            
			//System.out.println("Response body: ");
			System.out.println(post.getURI());    
			retrunValue = post.getResponseBodyAsString();
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			post.releaseConnection();
		}

		return retrunValue;
	}
	public static void main(String[] args) {
		//https://graph.facebook.com/oauth/access_token?client_id=253531061354196&client_secret=f88445c4bf05d2dc3bc8eba0ac77a4ff&grant_type=client_credentials
		//postOnReceiverWall("AAADmlczZAZCtQBAKbZA81BAVqMJZBeqfQDtsgFgoWFs1P3xCjQPcKBZBi2CspLyNosZB5BA6hQn9Kt0qdGAtXjvRHJwBBPliyvADAoUcoRZBQZDZD","me","sender","We are hiring \\n in US \\n http://demo.teachermatch.org");
		//AAAEbpsG8twUBAM4itQoTZAZCZBKU25bPoeDfZCPtR7WdIUDlzFZCtvRBsQ9QyQTbi9S4bs4c8Sa8Y1UFCGXvZBT28tSw3gHVSCWrODWJG19KjftE93f6Fm
		//QUFBQ2ZkeUtJVnFnQkFIZ1NFNk9yWkFMMFFvWDV4M0t5RmFpZUxPSmk2UFpCVEs4cUh0M1RYWkF1
		//postOnReceiverPage("AAAEbpsG8twUBAIrjyGUuc9XY7NqRpAK9piqBSdTZC56getWAOpyWnlD22XCdbw34kHiKojmFCJCZAvNzYZCeJZBYaGoJPcRKegcFoaD7JocG6828PvKO","129577153895739","sender","We are hiring \\n in US \\n http://demo.teachermatch.org","https://platform.teachermatch.org");
		//postOnReceiverPage("AAAEbpsG8twUBAIrjyGUuc9XY7NqRpAK9piqBSdTZC56getWAOpyWnlD22XCdbw34kHiKojmFCJCZAvNzYZCeJZBYaGoJPcRKegcFoaD7JocG6828PvKO","547691618586677","sender","We are hiring \\n in US \\n http://demo.teachermatch.org","https://platform.teachermatch.org");
		//postOnReceiverPage("AAAEbpsG8twUBAKl81kugTfmgkZBiachrFcD13LZAlCoos0gZCiGvqvkmgq0Cwpy5BQb8GrtIlSzA6ZBIW82NLLLZBJitGda6DhzRi8wQ9rOV9r87rr21j","100005463761344","sender","We are hiring \\n in US \\n http://demo.teachermatch.org","https://platform.teachermatch.org");
		//renewAccess_token("AAACEdEose0cBAGlWNEGqJsBnYuEIzErYnnzEBMnugGzBEpxSpjuCPMVjaGu7ORoHeovgnkPCl0zoXTFxuh8RZA9x9I18o1XMt4wGnFhZCZBPowVrgmx","","");
		
		//String basepath = Utility.getValueOfPropByKey("basePath");
		//System.out.println("basepath: "+basepath);
		
		//postOnReceiverPage("BAAEbpsG8twUBAGnk98kWr4exKHiY7PEXcKZBs6nUTEeyUYJCHkY5cNEfdR2tr3PuUKP48mHleJxs1PZC1EKep9vId518sbuy1vmkFrccqjZAYjigKyPINfhcWvATKqrETomZCorwZAMqEEHRlvmtAHLUR4uZBZA0OVqit9ZA8uHL9byOE0Tn7uXEZAYxkAjsVWhNbxmtZCJOWVLT4aGrfBnR97i7a2Khi9bJmmzdZBhVXTDEwZDZD","446415492102334","sender","We are hiring \\n in US \\n http://demo.teachermatch.org","https://platform.teachermatch.org");
		//postOnReceiverPage("BAAEbpsG8twUBABZCkJEnCjmbuE9kJMYj60SU2be5a1UGAbiodzoJbeAIEFHZA51agok7FIPljvEcBkclzUjiK0KXzjGfPdKYtVZARJF1B08lcPlXhcMplHpmOKyttZAhGMjP8OUpkaq83ZAqWIijsgnAC6ZCKm9Nb1m18ZCf4FuT44YZCKNeDgGnPZBiZChIHzSnviKgJ7wBTgmcr7ubXXf2KZALebloeDA78gyoAMebyglUwZDZD","362330727219997","sender","We are hiring \\n in US \\n http://demo.teachermatch.org","https://platform.teachermatch.org");
		//postOnReceiverPage("CAAEbpsG8twUBADyg123iunmzTlobPZAIEKMeNfFLYDQGbFnyFndwk81iytPnMRmkS2BvvrSvaXxnZCwhurooSZCvIaSD2hTojx0khusfKmzmBvqXm8mjW5aTKBGYUNsC1PZAFpwIvnAePVWKeSeMF7tovsV2b6h51klvaWYRZBAZDZD","362330727219997","sender","We are hiring \\n in US \\n http://demo.teachermatch.org","https://platform.teachermatch.org");
		//https://graph.facebook.com/446415492102334/feed?access_token=BAAEbpsG8twUBAGnk98kWr4exKHiY7PEXcKZBs6nUTEeyUYJCHkY5cNEfdR2tr3PuUKP48mHleJxs1PZC1EKep9vId518sbuy1vmkFrccqjZAYjigKyPINfhcWvATKqrETomZCorwZAMqEEHRlvmtAHLUR4uZBZA0OVqit9ZA8uHL9byOE0Tn7uXEZAYxkAjsVWhNbxmtZCJOWVLT4aGrfBnR97i7a2Khi9bJmmzdZBhVXTDEwZDZD
		//https://graph.facebook.com/<user_id>/accounts?access_token=<access_token>
			//This will list all the pages the user has access to and will provide the access tokens for each. You can then use those tokens to post as the page.
		//QUFBQ2ZkeUtJVnFnQkFIZ1NFNk9yWkFMMFFvWDV4M0t5RmFpZUxPSmk2UFpCVEs4cUh0M1RYWkF1
		//postOnReceiverPage("QUFBQ2ZkeUtJVnFnQkFIZ1NFNk9yWkFMMFFvWDV4M0t5RmFpZUxPSmk2UFpCVEs4cUh0M1RYWkF1","100005463761344","sender","We are hiring \\n in US \\n http://platform.teachermatch.org","https://platform.teachermatch.org");
		//String json ="products={'mpn': ['DH361FGK', 'DH361UGK', 'DH363FGK', 'DH361UDK', 'DH361URK'] }";
		//System.out.println(postJSON("",json));
		
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	public static String httpPost(String urlStr, String[] paramName,
			String[] paramVal) throws Exception {
		URL url = new URL(urlStr);
		HttpURLConnection conn =
			(HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setUseCaches(false);
		conn.setAllowUserInteraction(false);
		conn.setRequestProperty("Content-Type",
		"application/x-www-form-urlencoded");

		// Create the form content
		OutputStream out = conn.getOutputStream();
		Writer writer = new OutputStreamWriter(out, "UTF-8");
		for (int i = 0; i < paramName.length; i++) {
			writer.write(paramName[i]);
			writer.write("=");
			writer.write(URLEncoder.encode(paramVal[i], "UTF-8"));
			writer.write("&");
		}

		writer.close();
		out.close();

		if (conn.getResponseCode() != 200) {
			throw new IOException(conn.getResponseMessage());
		}

		// Buffer the result into a string
		BufferedReader rd = new BufferedReader(
				new InputStreamReader(conn.getInputStream()));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = rd.readLine()) != null) {
			sb.append(line);
		}
		rd.close();

		conn.disconnect();
		return sb.toString();
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	public static String httpGet(String urlStr) throws IOException {
		URL url = new URL(urlStr);
		HttpURLConnection conn =
			(HttpURLConnection) url.openConnection();

		if (conn.getResponseCode() != 200) {
			throw new IOException(conn.getResponseMessage());
		}

		// Buffer the result into a string
		BufferedReader rd = new BufferedReader(
				new InputStreamReader(conn.getInputStream()));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = rd.readLine()) != null) {
			sb.append(line);
		}
		rd.close();

		conn.disconnect();
		return sb.toString();
	}
}
