package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.api.PaginationAndSortingAPI;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherProfileVisitHistory;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherProfileVisitHistoryDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.DistrictSpecificPortfolioAnswersDAO;
import tm.services.quartz.FTPConnectAndLogin;
import tm.utility.DistrictNameCompratorASC;
import tm.utility.DistrictNameCompratorDESC;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
public class NobleStDspqReportAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private DistrictSpecificPortfolioAnswersDAO districtSpecificPortfolioAnswersDAO;
	
	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	
	public String displayRecordsByEntityType(String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	 {
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int sortingcheck=1;
			try{
				
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				//System.out.println("schoolMaster="+schoolMaster+"districtMaster="+districtMaster+"entityID="+entityID);
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position


				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				//------------------------------------
			 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"teacherId";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List lstNobleDSPQ =new ArrayList();
				
				if(entityID==2||entityID==3)
				{
					lstNobleDSPQ =districtSpecificPortfolioAnswersDAO.nobleStDSPQ(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
				  	totalRecord = lstNobleDSPQ.size();
				    System.out.println("totallllllllllll========="+totalRecord);
			    	 
				}
				else if(entityID==1) 
				{
					System.out.println("indside admin ");
					
					lstNobleDSPQ =districtSpecificPortfolioAnswersDAO.nobleStDSPQ(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
				  	totalRecord = lstNobleDSPQ.size();
				   	System.out.println("totallllllllllll========="+totalRecord);
					
				
				}
				
				List<String[]> finallstNobleDSPQ =new ArrayList<String[]>();
			     if(totalRecord<end)
						end=totalRecord;
			     finallstNobleDSPQ=lstNobleDSPQ.subList(start,end);
							
				String responseText="";
				
				tmRecords.append("<table  id='tblGridNobleStDSPQ' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				
				
				responseText=PaginationAndSorting.responseSortingLink("Internal Teacher ID",sortOrderFieldName,"teacherId",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("School Name",sortOrderFieldName,"schoolname",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Question Id",sortOrderFieldName,"questionid",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Question",sortOrderFieldName,"question",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
								
				responseText=PaginationAndSorting.responseSortingLink("Answer",sortOrderFieldName,"answer",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink("Write In Answer",sortOrderFieldName,"wianswer",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("DSPQ Score",sortOrderFieldName,"dspqscore",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Score By",sortOrderFieldName,"scoreby",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Score Date",sortOrderFieldName,"scoredata",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Answer Date",sortOrderFieldName,"answerdate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink("Answer Order for Multiple",sortOrderFieldName,"answerorder",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
						
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				if(finallstNobleDSPQ.size()==0){
					 tmRecords.append("<tr><td colspan='9' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
					}
				
				String sta="";
				  if(finallstNobleDSPQ.size()>0){
	                   for (Iterator it = finallstNobleDSPQ.iterator(); it.hasNext();)
	                   {
	                	   String[] row = (String[]) it.next(); 
	                	   
	                	   String  myVal1="";
	                	   String  myVal2="";
	                	  
	                	        tmRecords.append("<tr>");	
	                	        
	                            myVal1 = (row[0]==null)? " " : row[0].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
	                            
			                    
			                    myVal1 = (row[1]==null)? " " : row[1].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[2]==null)||(row[2].toString().length()==0))? " " : row[2].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    
			                    myVal1 = (row[3]==null)? " " : row[3].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[4]==null)? " " : row[4].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = (row[5]==null)? " " : row[5].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[6]==null)||(row[6].toString().length()==0))? " " : row[6].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                    myVal1 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString();
			                    tmRecords.append("<td>"+myVal1+"</td>");
			                    
			                  		                    
			                    tmRecords.append("</tr>");
	               }               
	              }
				
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			

			}catch(Exception e){
				e.printStackTrace();
			}
			
		 return tmRecords.toString();
	  }
		
		
  	public String displayRecordsByEntityTypePrintPreview(String noOfRow,String pageNo,String sortOrder,String sortOrderType)
	{
		System.out.println("inside Print Preview");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
		
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"teacherId";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List lstNobleDSPQ =new ArrayList();
				
				
				if(entityID==2||entityID==3)
				{
					lstNobleDSPQ =districtSpecificPortfolioAnswersDAO.nobleStDSPQ(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
				  	totalRecord = lstNobleDSPQ.size();
				    	System.out.println("totallllllllllll========="+totalRecord);
			    	 
				}
				else if(entityID==1) 
				{
					System.out.println("indside admin ");
					
					lstNobleDSPQ =districtSpecificPortfolioAnswersDAO.nobleStDSPQ(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
				  	totalRecord = lstNobleDSPQ.size();
				   	System.out.println("totallllllllllll========="+totalRecord);
					
				
				}
										
					List<String[]> finallstNobleDSPQ =new ArrayList<String[]>();
				     if(totalRecord<end)
							end=totalRecord;
				     finallstNobleDSPQ=lstNobleDSPQ;
				
			

	        tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>Noble St Application</div><br/>");
			
		    	tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+"Created by: "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>Date of Print: "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			    tmRecords.append("</div>");
			
			
			tmRecords.append("<table  id='tblGridNoblePrint' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Internal Teacher ID</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>School Name</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Question Id</th>");
    
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Question</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Answer</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Write In Answer</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>DSPQ Score</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Score By</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Score Date</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Answer Date</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>Answer Order for Multiple</th>");
			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(finallstNobleDSPQ.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			}
		String sta="";
			if(finallstNobleDSPQ.size()>0){
				 for (Iterator it = finallstNobleDSPQ.iterator(); it.hasNext();) 					            
					{
					String[] row = (String[]) it.next();
					
					 String myVal1="";
                     String myVal2="";
					  
					 tmRecords.append("<tr>");
					 
					 myVal1 = (row[0]==null)? "0" : row[0].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
					 
						 myVal1 = (row[1]==null)? " " : row[1].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
						 myVal1 = (row[2]==null)? "0" : row[2].toString();
							tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
														
							 myVal1 = (row[3]==null)? " " : row[3].toString();
								tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
						 myVal1 = ((row[4]==null)||(row[4].toString().length()==0))? "N/A" : row[4].toString();
			      		tmRecords.append("<td style='font-size:12px;'>"+ myVal1+"</td>");	
			      		
			      		 myVal1 = (row[5]==null)? " " : row[5].toString();
						tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
						
						 myVal1 = ((row[6]==null)||(row[6].toString().length()==0))? "0" : row[6].toString();
							tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
							
							 myVal1 = ((row[7]==null)||(row[7].toString().length()==0))? " " : row[7].toString();
								tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
								
								 myVal1 = ((row[8]==null)||(row[8].toString().length()==0))? " " : row[8].toString();
									tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
									
									 myVal1 = ((row[9]==null)||(row[9].toString().length()==0))? " " : row[9].toString();
										tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
										
										 myVal1 = ((row[10]==null)||(row[10].toString().length()==0))? " " : row[10].toString();
											tmRecords.append("<td style='font-size:12px;'>"+myVal1+"</td>");
													
					   tmRecords.append("</tr>");
			   }
			}
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
  }
	 public String displayNobleDspqCSV(String noOfRow,String pageNo,String sortOrder,String sortOrderType){
		 String basePath = null;
			
			String fileName = null;
	    	 WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =new StringBuffer();
			int sortingcheck=1;
			try{
				
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				//System.out.println("schoolMaster="+schoolMaster+"districtMaster="+districtMaster+"entityID="+entityID);
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position


				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				
				//------------------------------------
			 /** set default sorting fieldName **/
				String  sortOrderStrVal		=	null;
				String sortOrderFieldName	=	"teacherId";
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
				}
				
			/**end set dynamic sorting fieldName **/
                   List lstNobleDSPQ =new ArrayList();
				
                   if(entityID==2||entityID==3)
   				{
   					lstNobleDSPQ =districtSpecificPortfolioAnswersDAO.nobleStDSPQ(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
   				  	totalRecord = lstNobleDSPQ.size();
   				    	System.out.println("totallllllllllll========="+totalRecord);
   			    	 
   				}
   				else if(entityID==1) 
   				{
   					System.out.println("indside admin ");
   					
   					lstNobleDSPQ =districtSpecificPortfolioAnswersDAO.nobleStDSPQ(sortingcheck,sortOrderStrVal,start, noOfRowInPage,true,sortOrderFieldName);
   				  	totalRecord = lstNobleDSPQ.size();
   				   	System.out.println("totallllllllllll========="+totalRecord);
   					
   				}
				
				List<String[]> finallstNobleDSPQ =new ArrayList<String[]>();
			     if(totalRecord<end)
						end=totalRecord;
			     finallstNobleDSPQ=lstNobleDSPQ.subList(start,end);
			
			 
		   //Excel SmartFusion Exporting	

			     String time = String.valueOf(System.currentTimeMillis()).substring(6);
				
					 basePath = request.getSession().getServletContext().getRealPath ("/")+"/NobleStDSPQ";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="noblestdspq"+time+".csv";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);
                    System.out.println("fileeeeeeeeeeee====="+file);
			 FileWriter writer = new FileWriter(file);
			 
		   //Delimiter used in CSV file
			final String COMMA_DELIMITER = ",";
			final String NEW_LINE_SEPARATOR = "\n";
			
			StringBuilder csvFileData = new StringBuilder();
			 
			csvFileData.append("Internal_Teacher_ID");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("School_Name");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Question_ID");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Questions");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Answer");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Write_In_Answer");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("DSPQ_Score");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Scored_By");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Scored_Date");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Answer_Date");
			csvFileData.append(COMMA_DELIMITER);
			 
			csvFileData.append("Order_of_Answers_for_Multiple_Opeions");
			
			if(lstNobleDSPQ!=null)
				{
				 if(lstNobleDSPQ.size()>0){
				 totalRecord =  lstNobleDSPQ.size();
				 }
				
				 System.out.println("totalRecord: "+totalRecord+" end: "+end+" start: "+start+" sortOrderFieldName: "+sortOrderFieldName);
				 if(totalRecord>end){
					 end =totalRecord;
					 finallstNobleDSPQ =lstNobleDSPQ.subList(start, end);
				}else{
					 end =totalRecord;
					 finallstNobleDSPQ =lstNobleDSPQ.subList(start, end);
				}
				
				
				if(finallstNobleDSPQ.size()==0){
					csvFileData.append(NEW_LINE_SEPARATOR);
					csvFileData.append("No Rcords Founds");
				}
			
				 if(finallstNobleDSPQ.size()>0){
					// Integer count=1;
	                   for (Iterator it = finallstNobleDSPQ.iterator(); it.hasNext();)
	                   {
	                	   String[] row = (String[]) it.next(); 

						String  myVal1="";
						
						 myVal1 = ((row[0]==null)||(row[0].toString().length()==0))? "0" : row[0].toString();
						 csvFileData.append(NEW_LINE_SEPARATOR);
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						  myVal1 = ((row[1]==null)||((row[1].toString().length()==0)))? "" : row[1].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = ((row[2]==null)||((row[2].toString().length()==0)))? "0" : row[2].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = ((row[3]==null)||((row[3].toString().length()==0)))? "" : row[3].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = ((row[4]==null)||((row[4].toString().length()==0)))? "" : row[4].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = ((row[5]==null)||((row[5].toString().length()==0)))? "" : row[5].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = ((row[6]==null)||((row[6].toString().length()==0)))? "0" : row[6].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = ((row[7]==null)||((row[7].toString().length()==0)))? "" : row[7].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = ((row[8]==null)||((row[8].toString().length()==0)))? "" : row[8].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = ((row[9]==null)||((row[9].toString().length()==0)))? "" : row[9].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
						 myVal1 = ((row[10]==null)||((row[10].toString().length()==0)))? "" : row[10].toString();
						 csvFileData.append("\""+myVal1+"\"");
						 csvFileData.append(COMMA_DELIMITER);
						 
												
						//count++;
					}
				}
			}else{
				
				csvFileData.append(NEW_LINE_SEPARATOR);
				csvFileData.append("No Rcords Founds");
					
			}
				  writer.append(csvFileData);
				  writer.flush();
				  writer.close();
				  
			}catch (Exception e) {
				e.printStackTrace();
			}
		  return basePath+"/"+fileName;
	  }	
	
	/*****************************/
	 public String displayNobleDspqCSVRun(DistrictSpecificPortfolioAnswersDAO districtSpecificPortfolioAnswersDAO,ServletContext servletContext){
		 
		 System.out.println("Method call from schudler DSPA: "+districtSpecificPortfolioAnswersDAO+" servletContext :"+servletContext);
		 String basePath = null;
		String fileName = null;
		try{
		int	sortingcheck=1;
		String	sortOrderStrVal="asc";
		int	start=0;
		int noOfRow=50;
		boolean	report=true;
		String	sortColomnName="teacherId";
		
			/**end set dynamic sorting fieldName **/
                   List lstNobleDSPQ =new ArrayList();
				
                   System.out.println("DistrictSpecificPortfolioAnswersDAO : "+districtSpecificPortfolioAnswersDAO);
					lstNobleDSPQ =districtSpecificPortfolioAnswersDAO.nobleStDSPQ(sortingcheck, sortOrderStrVal, start, noOfRow, report, sortColomnName);
			  
			    	List<String[]> finallstNobleDSPQ =new ArrayList<String[]>();
			   
			       finallstNobleDSPQ=lstNobleDSPQ;//.subList(start,end);
			System.out.println("finallstNobleDSPQ===="+finallstNobleDSPQ.size());
			    
			 String time = String.valueOf(System.currentTimeMillis()).substring(6);
			 /***************************creating directory inside  ***rootPath*******************************************/	
			    FileOutputStream outStream = null;
			    fileName ="noblestDSPQ.csv";
			    String dspqDir = Utility.getValueOfPropByKey("rootPath"); 
				System.out.println("Dspq Dir : "+dspqDir);	
				
				String sourceDirName = dspqDir+"//"+"NobleDSPQ"; 
				String targetFileName = sourceDirName+"/"+fileName; ;
				
				File fileDspqDir = new File(sourceDirName);
				
				File sourceFile= new File(targetFileName);
				System.out.println("sourceFile========"+sourceFile);
				System.out.println("dspqDir.exists() before : "+fileDspqDir.exists());
				
				if(!fileDspqDir.exists())
				{
					fileDspqDir.mkdir();
					outStream = new FileOutputStream(sourceFile);
					System.out.println("directory created succesffully");
				}
				if(sourceFile.exists())
				{
					outStream = new FileOutputStream(targetFileName);
					System.out.println("sourceFile.exists()writing Noble DSPQ report : "+sourceFile.exists());
				}
				
		/********************************************************************/	
			 
			 Utility.deleteAllFileFromDir(sourceDirName);
			// file = new File(basePath+"/"+fileName);
			 
			 FileWriter writer = new FileWriter(sourceFile);
			 
		   //Delimiter used in CSV file
			 final String COMMA_DELIMITER = ",";
				final String NEW_LINE_SEPARATOR = "\n";
				
				StringBuilder csvFileData = new StringBuilder();
				 
				csvFileData.append("Internal_Teacher_ID");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("School_Name");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Question_ID");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Questions");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Answer");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Write_In_Answer");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("DSPQ_Score");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Scored_By");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Scored_Date");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Answer_Date");
				csvFileData.append(COMMA_DELIMITER);
				 
				csvFileData.append("Order_of_Answers_for_Multiple_Opeions");
					 	

				if(lstNobleDSPQ!=null)
				{
							
				if(finallstNobleDSPQ.size()==0){
					csvFileData.append(NEW_LINE_SEPARATOR);
					csvFileData.append("No Rcords Founds");
				}
			
				 if(finallstNobleDSPQ.size()>0){
					// Integer count=1;
	                   for (Iterator it = finallstNobleDSPQ.iterator(); it.hasNext();)
	                   {
	                	   String[] row = (String[]) it.next(); 

	                	   String  myVal1="";
							
							 myVal1 = ((row[0]==null)||(row[0].toString().length()==0))? "0" : row[0].toString();
							 csvFileData.append(NEW_LINE_SEPARATOR);
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							  myVal1 = ((row[1]==null)||((row[1].toString().length()==0)))? " " : row[1].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = ((row[2]==null)||((row[2].toString().length()==0)))? "0" : row[2].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = ((row[3]==null)||((row[3].toString().length()==0)))? " " : row[3].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = ((row[4]==null)||((row[4].toString().length()==0)))? " " : row[4].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = ((row[5]==null)||((row[5].toString().length()==0)))? " " : row[5].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = ((row[6]==null)||((row[6].toString().length()==0)))? "0" : row[6].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = ((row[7]==null)||((row[7].toString().length()==0)))? " " : row[7].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = ((row[8]==null)||((row[8].toString().length()==0)))? " " : row[8].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = ((row[9]==null)||((row[9].toString().length()==0)))? " " : row[9].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
							 
							 myVal1 = ((row[10]==null)||((row[10].toString().length()==0)))? " " : row[10].toString();
							 csvFileData.append("\""+myVal1+"\"");
							 csvFileData.append(COMMA_DELIMITER);
						
						
						//count++;
					}
				}
			}else{
				
				csvFileData.append(NEW_LINE_SEPARATOR);
				csvFileData.append("No Rcords Founds");
					
			}
				  writer.append(csvFileData);
				  writer.flush();
				  writer.close();
				  
				  writeNobleReportONServer(servletContext);
				  
			}catch (Exception e) {
				e.printStackTrace();
			}
		  return basePath+"/"+fileName;
	  }	
	 /*****************************/
	
	 
	 public void writeNobleReportONServer(ServletContext servletContext){
		  System.out.println("************writing Noble DSPQ  on server inside ******************* servletContext: "+servletContext);
		    try{
		     String path="";
		   
		     String source = Utility.getValueOfPropByKey("rootPath")+"NobleDSPQ/noblestDSPQ.csv";
			 System.out.println("source Path "+source);

			 String target = servletContext.getRealPath("/")+"/"+"/NobleDSPQ/";
			 System.out.println("Real target Path "+target);

			File sourceFile = new File(source);
			File targetDir = new File(target);
			
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());

			try {
				FileUtils.copyFile(sourceFile, targetFile);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			path = Utility.getValueOfPropByKey("contextBasePath")+"/NobleDSPQ/noblestDSPQ.csv";
			System.out.println("NobleApplication contextBasePath "+path);
		  
			
			FTPConnectAndLogin.dropFileOnFTPServerForNobleDspq(target+"noblestDSPQ.csv");
			
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
			
		}
	 
}


