package tm.services;

import gui.ava.html.image.generator.HtmlImageGenerator;

import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.io.File;
import java.util.Random;

import tm.utility.Utility;

public class PDServices {

	
	public String getRular(Integer teacherId,String basePath,String urlRootPath)
	{
		basePath=basePath+"/temp";
		File file = new File(basePath);
		if(!file.exists())
			file.mkdirs();
		
		//Utility.deleteAllFileFromDir(basePath);

		
		HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
		StringBuffer sb=new StringBuffer("");
	

		sb.append("<table width='400px;' style=\"font-family:'Tahoma'\">");
		sb.append("<tr>");
		sb.append("<td  align='left' style='height: 10px;width : 20px;'>");
		sb.append("0%");
		//sb.append("|0%");
		sb.append("</td>");
		sb.append("<td  align='right' style='height: 10px;width : 80px;'>");
		sb.append("	25%");
		//sb.append("25%|");
		sb.append("</td>");
		sb.append("<td  align='right' style='height: 10px;width : 100px;'>");
		sb.append("50%");
		//sb.append("50%|");
		sb.append("</td>");
		sb.append("<td  align='right' style='height: 10px;width : 100px;'>");
		sb.append("75%");
		//sb.append("75%|");
		sb.append("</td>");
		sb.append("<td  align='right' style='height: 10px;width : 100px;'>");
		sb.append("100%");
		//sb.append("100%|");
		sb.append("</td>");
		sb.append("</table>");
		
/*		sb.append("<table width='200px;' style=\"font-family:'Tahoma'\">");
		sb.append("<tr>");
		sb.append("<td  align='left' style='height: 10px;width : 10px;'>");
		sb.append("0%");
		//sb.append("|0%");
		sb.append("</td>");
		sb.append("<td  align='right' style='height: 10px;width : 40px;'>");
		sb.append("	25%");
		//sb.append("25%|");
		sb.append("</td>");
		sb.append("<td  align='right' style='height: 10px;width : 50px;'>");
		sb.append("50%");
		//sb.append("50%|");
		sb.append("</td>");
		sb.append("<td  align='right' style='height: 10px;width : 50px;'>");
		sb.append("75%");
		//sb.append("75%|");
		sb.append("</td>");
		sb.append("<td  align='right' style='height: 10px;width : 50px;'>");
		sb.append("100%");
		//sb.append("100%|");
		sb.append("</td>");
		sb.append("</table>");*/
		
		imageGenerator.loadHtml(sb.toString());
		imageGenerator.setOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		//System.out.println(imageGenerator.getDefaultSize());
		Dimension dimension = new Dimension(1000, 800);
		imageGenerator.setSize(dimension);
		//imageGenerator.setOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		String time = "rular_"+String.valueOf(System.currentTimeMillis()).substring(6);
		imageGenerator.saveAsImage(basePath+"/"+time+".png");
		
		return time+".png";
	}
	public String getContents(Integer teacherId,String basePath,int teacherPer,String displayName,String urlRootPath)
	{
		basePath=basePath+"/temp";
		File file = new File(basePath);
		if(!file.exists())
			file.mkdirs();
		
		
		HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
		StringBuffer sb=new StringBuffer("");
	
		sb.append("<table width='900px;' style=\"font-size:10px;font-family:'Tahoma'\">");
		sb.append("<tr>");
		sb.append("<td  style='align:left;width:770;font-size:24px;'>");
		sb.append("<b>"+displayName+"</b>");
		sb.append("</td>");
		
		sb.append("<td style='align:right;width:130;font-size:24px;'>");
		sb.append("<b>"+teacherPer+"%<b>");
		sb.append("</td></tr>");
		sb.append("</table>");
		
		imageGenerator.loadHtml(sb.toString());
		imageGenerator.setOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		//System.out.println(imageGenerator.getDefaultSize());
		Dimension dimension = new Dimension(1000, 800);
		imageGenerator.setSize(dimension);
		//imageGenerator.setOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		String time = "content_"+String.valueOf(System.currentTimeMillis()).substring(6);
		imageGenerator.saveAsImage(basePath+"/"+time+".png");
		
		return time+".png";
		
		
		/*basePath=basePath+"/temp";
		File file = new File(basePath);
		if(!file.exists())
			file.mkdirs();
		
		
		HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
		StringBuffer sb=new StringBuffer("");
	
		sb.append("<table width='300px;' style=\"font-size:12px;font-family:'Tahoma'\">");
		sb.append("<tr>");
		sb.append("<td  style='align:left;width:255;font-size:11px;'>");
		sb.append("<b>"+displayName+"</b>");
		sb.append("</td>");
		
		sb.append("<td style='align:right;width:45;font-size:11px;'>");
		sb.append("<b>"+teacherPer+"%<b>");
		sb.append("</td></tr>");
		sb.append("</table>");
		
		imageGenerator.loadHtml(sb.toString());
		//System.out.println(imageGenerator.getDefaultSize());
		Dimension dimension = new Dimension(1000, 800);
		imageGenerator.setSize(dimension);
		imageGenerator.setOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		String time = "content_"+String.valueOf(System.currentTimeMillis()).substring(6);
		imageGenerator.saveAsImage(basePath+"/"+time+".png");
		
		return time+".png";*/
		
/*		basePath=basePath+"/temp";
		File file = new File(basePath);
		if(!file.exists())
			file.mkdirs();
		
		
		HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
		StringBuffer sb=new StringBuffer("");
	
		sb.append("<table width='260px;' style=\"font-size:9px;font-family:'Tahoma'\">");
		sb.append("<tr>");
		sb.append("<td  style='align:left;width:220;'>");
		sb.append("<b>"+displayName+"</b>");
		sb.append("</td>");
		
		sb.append("<td style='align:right;width:40;'>");
		sb.append("<b>"+teacherPer+"%<b>");
		sb.append("</td></tr>");
		sb.append("</table>");
		
		imageGenerator.loadHtml(sb.toString());
		imageGenerator.setOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		//System.out.println(imageGenerator.getDefaultSize());
		Dimension dimension = new Dimension(1000, 800);
		imageGenerator.setSize(dimension);
		//imageGenerator.setOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		String time = "content_"+String.valueOf(System.currentTimeMillis()).substring(6);
		imageGenerator.saveAsImage(basePath+"/"+time+".png");
		
		return time+".png";*/
	}
	public String getProgressGridCompetencyWise(Integer teacherId,String basePath,int teacherPer,int totalPercentile,String displayName,String urlRootPath)
	{
		basePath=basePath+"/temp";
		File file = new File(basePath);
		if(!file.exists())
			file.mkdirs();
		
		//System.out.println("TTTT totalPercentile:  "+totalPercentile);
		//System.out.println("TTTT teacherPer:  "+teacherPer);
		/*totalPercentile=50;
		teacherPer=10;*/
		HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
		StringBuffer sb=new StringBuffer("");
	
		sb.append("<table cellspacing=0 cellpadding=0 BORDERCOLOR=blue style='border: 1px outset blue;' width='400px;'>");
		sb.append("<tr>");
		String bg="#0191C8";
		if(teacherPer==0)
			bg="#C0C0C0";
		sb.append("<td style='background-color:"+bg+"; width: "+getPercentage(teacherPer,400)+"px; height: 15px;'>");
		if(totalPercentile<=teacherPer)
		{
			//sb.append(totalPercentile*4);
			sb.append("<table cellspacing=0 cellpadding=0> <tr><td style='width: "+((totalPercentile*4)-10)+"px; height: 15px;'>&nbsp;</td><td style='padding-top:2px;width: 15px;'><span style='color:red;'>&#x25B2;</span></td></tr></table>");
			
		}else
			sb.append("&nbsp;");
		sb.append("</td>");
		sb.append("<td style='background-color: #C0C0C0; width: "+getPercentage((100-teacherPer),400)+"px; height: 15px;'>");
		if(totalPercentile>teacherPer)
		{	
			//sb.append((totalPercentile*4)-(teacherPer*4));
			sb.append("<table cellspacing=0 cellpadding=0> <tr><td style='width: "+((totalPercentile*4)-(teacherPer*4)-10)+"px; height: 15px;'>&nbsp;</td><td style='padding-top:2px;'><span style='color:red;'>&#x25B2;</span></td></tr></table>");
		}else
		    sb.append("&nbsp;");
		
		//System.out.println("getSpace: "+getSpace(totalPercentile,390,urlRootPath));
		sb.append("</td>");
		sb.append("</tr>");
		sb.append("</table>");
		
		imageGenerator.loadHtml(sb.toString());
		imageGenerator.setOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		//System.out.println(imageGenerator.getDefaultSize());
		Dimension dimension = new Dimension(1000, 800);
		imageGenerator.setSize(dimension);
		//imageGenerator.setOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		String time = "comp_"+String.valueOf(System.currentTimeMillis()).substring(6);
		imageGenerator.saveAsImage(basePath+"/"+time+".png");
		
		return time+".png";
	}

	public static int getPercentage(int a,int b)
	{
		return (b*a)/100;
	}
	
	public static String getSpace(int noSp,int b,String urlRootPath)
	{
		int count=0;
		String str = "";
		noSp = getPercentage(noSp, b);
		//System.out.println(noSp);
		
		int no50=noSp/50;
		
		//System.out.println(no50);
		
		for(int i=0;i<no50;i++)
			{
				str+="<img SRC='"+urlRootPath+"images/spacer50px.png' />";
				count=count+50;
			}
		
		int re50=noSp%50;
		
		//System.out.println(re50);
		int no10=0;
		int re10=0;
		int no5=0;
		int re5=0;
		int no2=0;
		int re2=0;
		int no1=0;
		int re1=0;
		if(re50<50)
		{
			no10 = re50/10;
			for(int i=0;i<no10;i++)
				{
					str+="<img SRC='"+urlRootPath+"images/spacer10px.png' />";
					count=count+10;
				}
			
			re10 = re50%10;
			
			if(re10<10)
			{
				no5 = re10/5;
				for(int i=0;i<no5;i++)
					{
						str+="<img SRC='"+urlRootPath+"images/spacer5px.png' />";
						count=count+5;
					}
				
				re5 = re10%5;
				
				if(re5<5)
				{
					no2 = re5/2;
					for(int i=0;i<no2;i++)
						{
							str+="<img SRC='"+urlRootPath+"images/spacer2px.png' />";
							count=count+2;
						}
					
					re2 = re5%2;
					
					if(re2<2)
					{
						no1 = re2/1;
						for(int i=0;i<no1;i++)
							{
								str+="<img SRC='"+urlRootPath+"images/spacer1px.png' />";
								count=count+1;
							}
						
						re1 = re1%2;
					}
				}
				
			}
			
		}
		
		//System.out.println("11111: "+str);
		System.out.println("vishwanath: "+count);
		
		return str;
	}
}
