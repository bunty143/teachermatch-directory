package tm.services;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobCategoryTransaction;
import tm.bean.assessment.AssessmentGroupDetails;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.i4.I4QuestionSets;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.ReferenceCheckQuestionSet;
import tm.bean.master.ReferenceQuestionSets;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobCategoryTransactionDAO;
import tm.dao.assessment.AssessmentGroupDetailsDAO;
import tm.dao.districtassessment.DistrictAssessmentDetailDAO;
import tm.dao.i4.I4QuestionSetsDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificRefChkQuestionsDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.QqQuestionSetsDAO;
import tm.dao.master.ReferenceCheckQuestionSetDAO;
import tm.dao.master.ReferenceQuestionSetsDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class JobCategoryAjax
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	String 	lblJobCategoryName=Utility.getLocaleValuePropByKey("lblJobCategoryName", locale);
	String 	optDistrict=Utility.getLocaleValuePropByKey("optDistrict", locale);
	String 	lblEPIReq=Utility.getLocaleValuePropByKey("lblEPIReq", locale);
	String 	lblFullTimeTeachers=Utility.getLocaleValuePropByKey("lblFullTimeTeachers", locale);
	String 	lblJSIAttachment=Utility.getLocaleValuePropByKey("lblJSIAttachment", locale);
	String 	lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	String 	lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	String 	lblNoJobCategoryfound=Utility.getLocaleValuePropByKey("lblNoJobCategoryfound", locale);
	String 	toolClickHereSeeAssedoc=Utility.getLocaleValuePropByKey("toolClickHereSeeAssedoc", locale);
	String 	optAct=Utility.getLocaleValuePropByKey("optAct", locale);
	String 	lblInActiv=Utility.getLocaleValuePropByKey("lblInActiv", locale);
	String 	lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	String 	lblActivate=Utility.getLocaleValuePropByKey("lblActivate", locale);
	String 	lblDeactivate=Utility.getLocaleValuePropByKey("lblDeactivate", locale);
	String 	lblPleaseSelectQQSet=Utility.getLocaleValuePropByKey("lblPleaseSelectQQSet", locale);
	String 	optStrJobCat=Utility.getLocaleValuePropByKey("optStrJobCat", locale);
	String 	optStrEpiGroups=Utility.getLocaleValuePropByKey("optStrEpiGroups", locale);
	
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(
			JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	@Autowired 
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}	
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private I4QuestionSetsDAO i4QuestionSetsDAO;

	@Autowired
	private DistrictAssessmentDetailDAO districtAssessmentDetailDAO;
	
	@Autowired
	private DistrictSpecificRefChkQuestionsDAO districtSpecificRefChkQuestionsDAO;
	
	@Autowired
	private ReferenceCheckQuestionSetDAO referenceCheckQuestionSetDAO;
	
	@Autowired
	private ReferenceQuestionSetsDAO referenceQuestionSetsDAO;
	@Autowired
	private QqQuestionSetsDAO qqQuestionSetsDAO;
	
	@Autowired 
	private HqJobCategoryAjax hqJobCategoryAjax;
	
	@Autowired
	private AssessmentGroupDetailsDAO assessmentGroupDetailsDAO;
	
	@Autowired
	private JobCategoryTransactionDAO jobCategoryTransactionDAO;
	
	public String displayJobCategories(Boolean result,Integer searchEntityType,Integer entityType,Integer searchId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------

			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}

			List<JobCategoryMaster> jobCategoryMasters	  	=	null;
			List<JobCategoryMaster> jobCategoryMastersAll	  	=	null;
			String sortOrderFieldName	=	"jobCategoryName";
			String sortOrderNoField		=	"jobCategoryName";

			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("districtMaster")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("districtMaster"))
				{
					sortOrderNoField="districtMaster";
				}
			}

			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}

			List<JobCategoryMaster>sortedJobCatList=new ArrayList<JobCategoryMaster>();
			Criterion criterion=null , hqCriterian = null , brCriterian = null;
			hqCriterian = Restrictions.isNull("headQuarterMaster");
			brCriterian = Restrictions.isNull("branchMaster");
			DistrictMaster districtMaster=null;
			if(userMaster.getEntityType()==2){
				districtMaster=userMaster.getDistrictId();
				criterion=Restrictions.eq("districtMaster", districtMaster);
				jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,hqCriterian,brCriterian);
				jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion,hqCriterian,brCriterian);
				totalRecord=jobCategoryMastersAll.size();
			}else{
				if(result){
					System.out.println("searchId :"+searchId);
					if(searchId!=0){
						districtMaster=districtMasterDAO.findById(searchId, false, false);
						criterion=Restrictions.eq("districtMaster", districtMaster); 
						jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,hqCriterian,brCriterian);
					}else{
						criterion=Restrictions.isNotNull("districtMaster"); 
						jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,hqCriterian,brCriterian);
					}
					jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion,hqCriterian,brCriterian);
					totalRecord=jobCategoryMastersAll.size();
				}else{
					if(searchEntityType==2){
						districtMaster=districtMasterDAO.findById(searchId, false, false);
						criterion=Restrictions.eq("districtMaster", districtMaster); 
						jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion,hqCriterian,brCriterian);
						jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion,hqCriterian,brCriterian);
					}else{
						jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,hqCriterian,brCriterian);
						jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,hqCriterian,brCriterian);
					}	
					totalRecord=jobCategoryMastersAll.size();
				}
			}

			SortedMap<String,JobCategoryMaster>	sortedMap = new TreeMap<String,JobCategoryMaster>();
			if(sortOrderNoField.equals("districtMaster"))
			{
				sortOrderFieldName	=	"districtMaster";
			}

			int mapFlag=2;
			for (JobCategoryMaster jbc : jobCategoryMastersAll){
				String orderFieldName=null;
				if(jbc.getDistrictMaster()!=null){
					orderFieldName=jbc.getDistrictMaster().getDistrictName();
					if(sortOrderFieldName.equals("districtMaster")){
						orderFieldName=jbc.getDistrictMaster().getDistrictName()+"||"+jbc.getJobCategoryId();
						sortedMap.put(orderFieldName+"||",jbc);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}else{
					if(sortOrderFieldName.equals("districtMaster")){
						orderFieldName="0||"+jbc.getJobCategoryId();
						sortedMap.put(orderFieldName+"||",jbc);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedJobCatList.add((JobCategoryMaster) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedJobCatList.add((JobCategoryMaster) sortedMap.get(key));
				}
			}else{
				sortedJobCatList=jobCategoryMasters;
			}

			List<JobCategoryMaster>sortedJBCList=new ArrayList<JobCategoryMaster>();

			if(totalRecord<end)
				end=totalRecord;

			if(mapFlag!=2)
				sortedJBCList=sortedJobCatList.subList(start,end);
			else
				sortedJBCList=sortedJobCatList;

			//List<JobCategoryMaster> lstsortedjobCategory		=	jobCategoryMasters.subList(start,end);
			dmRecords.append("<table  id='jobCategoryTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblJobCategoryName,sortOrderFieldName,"jobCategoryName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='55%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(optDistrict,sortOrderFieldName,"districtMaster",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblEPIReq,sortOrderFieldName,"baseStatus",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblFullTimeTeachers,sortOrderFieldName,"epiForFullTimeTeachers",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblJSIAttachment,sortOrderFieldName,"assessmentDocument",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");

			dmRecords.append("<th width='15%' valign='top'>"+lblAct+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			//System.out.println(" jobCategoryMasters :   "+jobCategoryMasters.size()+" lstsortedjobCategory "+lstsortedjobCategory);

			if(sortedJobCatList!=null && sortedJobCatList.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+lblNoJobCategoryfound+"</td></tr>" );
			for (JobCategoryMaster jc : sortedJBCList) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+jc.getJobCategoryName()+"</td>");
				if(jc.getDistrictMaster()!=null){
					dmRecords.append("<td>"+jc.getDistrictMaster().getDistrictName()+"</td>");
				}else{
					dmRecords.append("<td></td>");
				}

				String bStatus=jc.getBaseStatus().toString();
				dmRecords.append("<td>"+Character.toUpperCase(bStatus.charAt(0)) + bStatus.substring(1)+"</td>");
				
				String fullTimeTeacher=jc.getEpiForFullTimeTeachers().toString();
				dmRecords.append("<td>"+Character.toUpperCase(fullTimeTeacher.charAt(0)) + fullTimeTeacher.substring(1)+"</td>");
				
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				dmRecords.append("<td style='text-align:center;'>");
				dmRecords.append(jc.getAssessmentDocument()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='"+toolClickHereSeeAssedoc+"'  id='jsi"+jc.getJobCategoryId()+"' onclick=\"downloadJsi('"+jc.getJobCategoryId()+"','jsi"+jc.getJobCategoryId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
				dmRecords.append("<script>$('#jsi"+jc.getJobCategoryId()+"').tooltip();</script>");
				dmRecords.append("</td>");

				dmRecords.append("<td>");
				if(jc.getStatus().equalsIgnoreCase("A"))
					dmRecords.append(optAct);
				else
					dmRecords.append(lblInActiv);
				dmRecords.append("</td>");


				dmRecords.append("<td>");
				dmRecords.append("<a href='javascript:void(0);'title='Edit' onclick='return editJobCategory("+jc.getJobCategoryId()+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>&nbsp;|&nbsp;");
				dmRecords.append("<a href='javascript:void(0);' onclick='return showHistory("+jc.getJobCategoryId()+")'><i class='fa fa-history fa-lg'/></a>&nbsp;|&nbsp;");
				
				if(jc.getStatus().equalsIgnoreCase("A"))
					dmRecords.append("<a href='javascript:void(0);'title='Deactivate' onclick=\"return activateDeactivateJobCategory("+jc.getJobCategoryId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>");
				else
					dmRecords.append("<a href='javascript:void(0);' title='Activate' onclick=\"return activateDeactivateJobCategory("+jc.getJobCategoryId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>");
				dmRecords.append("</td>");

				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}

	public String saveJobCategory(Integer jobCategoryId,String jobCategoryName,Integer districtId,boolean baseStatus,boolean epiForFullTimeTeachers,String assessmentDocument,boolean offerDistrictSpecificItems, boolean offerQualificationItems,Integer offerJSI,boolean offerPortfolioNeeded,boolean offerVVIForInternal,boolean offerVirtualVideoInterview,String quesId,String maxScoreForVVI,boolean sendAutoVVILink,String slctStatusID,String timePerQues,String vviExpDays,boolean offerAssessmentInviteOnly,String assessmentIds,String questionSetVal,boolean imepicheck2,boolean imofferPortfolioNeeded,boolean imofferDistrictSpecificItems,boolean imofferQualificationItems,Integer imofferJSI, boolean schoolSelection,Integer qqId,Integer parentJobCategoryId,Integer qqIdOnboard,Boolean approvalBeforeGoLive,Integer noOfApprovalNeeded, Boolean buildApprovalGroup,boolean attachDSPQFromJC,boolean attachSLCFromJC,boolean approvalByPredefinedGroups,Integer assessmentGroupId,Boolean jobCompletedVVILink,Integer autoRejectScore)
	{
		System.out.println("::::::::::::::::::::::::::::::: saveJobCategory ::::::::::::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster 			= 	null;
		String ipAddress=null;
		boolean isNew =false;
		ipAddress= IPAddressUtility.getIpAddress(request);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		} else {
			userMaster = (UserMaster)session.getAttribute("userMaster");
		}
		try
		{
			//System.out.println(" assessmentDocument  :: "+assessmentDocument);
			DistrictMaster districtMaster=null;
			JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();

			if(districtId!=null){
				districtMaster=districtMasterDAO.findById(districtId, false, false); 
				jobCategoryMaster.setDistrictMaster(districtMaster);
			}
			if(assessmentDocument!=null && !assessmentDocument.equals(""))
				jobCategoryMaster.setAssessmentDocument(assessmentDocument);
			else
				jobCategoryMaster.setAssessmentDocument(null);

			jobCategoryMaster.setJobCategoryName(jobCategoryName);
			jobCategoryMaster.setBaseStatus(baseStatus);
			jobCategoryMaster.setEpiForFullTimeTeachers(epiForFullTimeTeachers);
			jobCategoryMaster.setAutoRejectScore(autoRejectScore);
			jobCategoryMaster.setStatus("A");
			jobCategoryMaster.setOfferDistrictSpecificItems(offerDistrictSpecificItems);
			jobCategoryMaster.setOfferQualificationItems(offerQualificationItems);
			jobCategoryMaster.setOfferJSI(offerJSI);
			jobCategoryMaster.setSchoolSelection(schoolSelection);
			jobCategoryMaster.setOfferPortfolioNeeded(offerPortfolioNeeded);
			if(assessmentGroupId!=null && assessmentGroupId>0)
				jobCategoryMaster.setAssessmentGroupId(assessmentGroupId);
			else
				jobCategoryMaster.setAssessmentGroupId(null);
			/****add by ankit***/
			
			 if(parentJobCategoryId!=null && !parentJobCategoryId.equals(0))
			 {
				 JobCategoryMaster parentCategory = jobCategoryMasterDAO.findById(parentJobCategoryId, false, false);
                 jobCategoryMaster.setParentJobCategoryId(parentCategory);
			 }
			
			jobCategoryMaster.setEpiForIMCandidates(imepicheck2);
			jobCategoryMaster.setPortfolioForIMCandidates(imofferPortfolioNeeded);
			jobCategoryMaster.setDistrictSpecificItemsForIMCandidates(imofferDistrictSpecificItems);
			jobCategoryMaster.setQualificationItemsForIMCandidates(imofferQualificationItems);
			jobCategoryMaster.setJsiForIMCandidates(imofferJSI);
			
			
			/****add end by ankit***/
			/*For VVI*/
			try
			{
				StatusMaster statusMasterForVVI=new StatusMaster();
				SecondaryStatus secondaryStatusForVVI=new SecondaryStatus();
				Integer statusIdForSchoolPrivilegeForVVI=null;
				Integer secondaryStatusIdForSchoolPrivilegeForVVI=null;
				I4QuestionSets i4QuestionSets = null;
				
				jobCategoryMaster.setOfferVVIForInternalCandidates(offerVVIForInternal);
				if(offerVirtualVideoInterview)
				{
					if(quesId!=null && !quesId.equals(""))
						i4QuestionSets = i4QuestionSetsDAO.findById(Integer.parseInt(quesId), false, false);
					
					if(offerVirtualVideoInterview)
						jobCategoryMaster.setOfferVirtualVideoInterview(offerVirtualVideoInterview);
					else
						jobCategoryMaster.setOfferVirtualVideoInterview(false);
					
					jobCategoryMaster.setI4QuestionSets(i4QuestionSets);
					
					
					if(maxScoreForVVI!=null && !maxScoreForVVI.equals(""))
						jobCategoryMaster.setMaxScoreForVVI(Integer.parseInt(maxScoreForVVI));
					
					if(sendAutoVVILink)
						jobCategoryMaster.setSendAutoVVILink(sendAutoVVILink);
					else
						jobCategoryMaster.setSendAutoVVILink(false);
					
					if(timePerQues!=null && !timePerQues.equals(""))
						jobCategoryMaster.setTimeAllowedPerQuestion(Integer.parseInt(timePerQues));
					else
						jobCategoryMaster.setTimeAllowedPerQuestion(null);
					
					if(vviExpDays!=null && !vviExpDays.equals(""))
						jobCategoryMaster.setVVIExpiresInDays(Integer.parseInt(vviExpDays));
					else
						jobCategoryMaster.setVVIExpiresInDays(null);
				
					if(sendAutoVVILink==true)
					{
						if(jobCompletedVVILink==null){
							jobCategoryMaster.setJobCompletedVVILink(null);
							jobCategoryMaster.setStatusIdForAutoVVILink(null);
							jobCategoryMaster.setSecondaryStatusIdForAutoVVILink(null);
						}else if(jobCompletedVVILink==false){
							
							jobCategoryMaster.setJobCompletedVVILink(false);
							if(slctStatusID.contains("SSID_"))
							{
								System.out.println("01");
								
								secondaryStatusIdForSchoolPrivilegeForVVI=new Integer(slctStatusID.substring(5));
								secondaryStatusForVVI=secondaryStatusDAO.findById(secondaryStatusIdForSchoolPrivilegeForVVI, false, false);
								if(secondaryStatusIdForSchoolPrivilegeForVVI!=null)
								{
									jobCategoryMaster.setSecondaryStatusIdForAutoVVILink(secondaryStatusIdForSchoolPrivilegeForVVI);
								}
								jobCategoryMaster.setStatusIdForAutoVVILink(null);
							}
							else
							{
								System.out.println("02");
								
								System.out.println(" slctStatusID :: "+slctStatusID);
								
								statusIdForSchoolPrivilegeForVVI=new Integer(slctStatusID);
								statusMasterForVVI=WorkThreadServlet.statusIdMap.get(statusIdForSchoolPrivilegeForVVI);
								
								if(statusIdForSchoolPrivilegeForVVI!=null)
								{
									jobCategoryMaster.setStatusIdForAutoVVILink(statusIdForSchoolPrivilegeForVVI);
								}
								jobCategoryMaster.setSecondaryStatusIdForAutoVVILink(null);
							}
						}else if(jobCompletedVVILink==true){
							jobCategoryMaster.setJobCompletedVVILink(true);
							jobCategoryMaster.setStatusIdForAutoVVILink(null);
							jobCategoryMaster.setSecondaryStatusIdForAutoVVILink(null);
						}
							
					}
					else
					{
						jobCategoryMaster.setJobCompletedVVILink(null);
						jobCategoryMaster.setStatusIdForAutoVVILink(null);
						jobCategoryMaster.setSecondaryStatusIdForAutoVVILink(null);
					}
				}
				else
				{
					jobCategoryMaster.setOfferVirtualVideoInterview(false);
					jobCategoryMaster.setSendAutoVVILink(false);
					jobCategoryMaster.setMaxScoreForVVI(null);
					/*jobCategoryMaster.setStatusMaster(null);
					jobCategoryMaster.setSecondaryStatus(null);*/
					jobCategoryMaster.setStatusIdForAutoVVILink(null);
					jobCategoryMaster.setSecondaryStatusIdForAutoVVILink(null);
					jobCategoryMaster.setI4QuestionSets(null);
					jobCategoryMaster.setTimeAllowedPerQuestion(null);
					jobCategoryMaster.setVVIExpiresInDays(null);
				}
				
				
				
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			//For District Assessment
			try
			{
				
				if(offerAssessmentInviteOnly)
				{
					if(assessmentIds!=null && !assessmentIds.equals(""))
					{
							jobCategoryMaster.setOfferAssessmentInviteOnly(offerAssessmentInviteOnly);
							jobCategoryMaster.setDistrictAssessmentId(assessmentIds);
					}
				}
				else
				{
					jobCategoryMaster.setOfferAssessmentInviteOnly(false);
					jobCategoryMaster.setDistrictAssessmentId(null);
				}
			}catch(Exception e)
			{e.printStackTrace();}
			
			
			
			boolean duplicate=false;
			if(jobCategoryId!=null){
				jobCategoryMaster.setJobCategoryId(jobCategoryId);
				duplicate=jobCategoryMasterDAO.checkDuplicateJobCategory(jobCategoryMaster,jobCategoryName,districtMaster);
			}else{
				duplicate=jobCategoryMasterDAO.checkDuplicateJobCategory(null,jobCategoryName,districtMaster);
			}
			/*
			 *		Add Reference Check
			 *		By		:	Hanzala Subhani
			 *		Dated	:	25 Feb 2015
			 *
			 */
			Date currentDate	=	new Date();
			if(questionSetVal!="" && !questionSetVal.equalsIgnoreCase("")){
				ReferenceQuestionSets referenceQuestionSets = referenceQuestionSetsDAO.findById(Integer.parseInt(questionSetVal), false, false);
				List<ReferenceCheckQuestionSet> referenceCheckQuestionSetList = null;
				referenceCheckQuestionSetList	=	referenceCheckQuestionSetDAO.findByQuestionAndJobCategory(districtMaster,jobCategoryMaster);

				ReferenceCheckQuestionSet referenceCheckQuestionSet =  new ReferenceCheckQuestionSet();
                  if(referenceCheckQuestionSetList.size() == 0 || referenceCheckQuestionSetList == null){
                	  // Insert Record
                	  try{
	                  		  referenceCheckQuestionSet.setReferenceQuestionSets(referenceQuestionSets);
	                  		  referenceCheckQuestionSet.setJobCategoryMaster(jobCategoryMaster);
	                          referenceCheckQuestionSet.setDistrictMaster(districtMaster);
	                          referenceCheckQuestionSet.setUserMaster(userMaster);
	                          referenceCheckQuestionSet.setCreatedDateTime(currentDate);
	                          referenceCheckQuestionSetDAO.makePersistent(referenceCheckQuestionSet);
                      } catch(Exception exception){
                          exception.printStackTrace();
                      }
                  } else {
                	  if(referenceCheckQuestionSetList!=null)
                		  try{
                			  referenceCheckQuestionSet = referenceCheckQuestionSetDAO.findById(referenceCheckQuestionSetList.get(0).getReferenceQuestionId(), false, false);
                			  referenceCheckQuestionSet.setReferenceQuestionSets(referenceQuestionSets);
                			  referenceCheckQuestionSetDAO.makePersistent(referenceCheckQuestionSet);
                		  } catch(Exception exception){
                			  exception.printStackTrace();
                		  }
                  	}
				}
			
			/*	End Reference Check */
			
			/* start of QQusetion Set */
			try{
			if(qqId!=null && !qqId.equals(0)){				
				QqQuestionSets questionSets = new QqQuestionSets();
				questionSets.setID(qqId);
				jobCategoryMaster.setQuestionSets(questionSets);
			 }else{
				 jobCategoryMaster.setQuestionSets(null); 
			 }
			if(qqIdOnboard!=null && !qqIdOnboard.equals(0)){				
				QqQuestionSets questionSets = new QqQuestionSets();
				questionSets.setID(qqIdOnboard);
				jobCategoryMaster.setQuestionSetsForOnboarding(questionSets);
			 }else{
				 jobCategoryMaster.setQuestionSetsForOnboarding(null); 
			 }
			
			}catch(Exception e){e.printStackTrace();}
			/* End of QQusetion Set */
			try
			{
				jobCategoryMaster.setAttachDSPQFromJC(attachDSPQFromJC);
				jobCategoryMaster.setAttachSLCFromJC(attachSLCFromJC);
				jobCategoryMaster.setApprovalByPredefinedGroups(approvalByPredefinedGroups);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			jobCategoryMaster.setApprovalBeforeGoLive(approvalBeforeGoLive);
			jobCategoryMaster.setNoOfApprovalNeeded(noOfApprovalNeeded);
			jobCategoryMaster.setBuildApprovalGroup(buildApprovalGroup);
			
			if(!duplicate){
				System.out.println("******"+jobCategoryMaster.getJobCategoryId());
				if(jobCategoryMaster!=null){
					if(jobCategoryMaster.getJobCategoryId()==null || jobCategoryMaster.getJobCategoryId()==0)
						isNew = true;
					else  isNew = false;
				}
//				isNew= ( && ( jobCategoryMaster.getJobCategoryId()==null)) ? true :false;
				if(isNew){
				jobCategoryMasterDAO.makePersistent(jobCategoryMaster);
				hqJobCategoryAjax.saveToJobcategoryMasterHistory(jobCategoryMaster, userMaster, ipAddress,isNew,questionSetVal);
				}
				else{
					hqJobCategoryAjax.saveToJobcategoryMasterHistory(jobCategoryMaster, userMaster, ipAddress,isNew,questionSetVal);
					jobCategoryMasterDAO.makePersistent(jobCategoryMaster);
				}
				return ""+jobCategoryMaster.getJobCategoryId();
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "true";
	}
	
	public boolean activateDeactivateJobCategory(Integer jobcategoryId,String status)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		String ipAddress=null;
		boolean isNew = false;
		ipAddress= IPAddressUtility.getIpAddress(request);
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else{
			userMaster = (UserMaster)session.getAttribute("userMaster");
		}
		try{
			JobCategoryMaster jobCategoryMaster=jobCategoryMasterDAO.findById(jobcategoryId, false, false);
			jobCategoryMaster.setStatus(status);
			hqJobCategoryAjax.saveToJobcategoryMasterHistory(jobCategoryMaster, userMaster, ipAddress, isNew, "nochange")	;		
			jobCategoryMasterDAO.makePersistent(jobCategoryMaster);
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Transactional(readOnly=false)
	public List<Object> getJobCategoryId(Integer jobCategoryId)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		List<Object> jobCategoryObject=null;
		String p="I";
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();

		try{
			if(jobCategoryId!=null){
				jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				if(jobCategoryMaster!=null &&  jobCategoryMaster.getAssessmentDocument()!=null){
					session.setAttribute("previousJSIFileName", jobCategoryMaster.getAssessmentDocument());	
				}
				
				// **************************************************************************************
				
				List<JobCategoryTransaction> jobCategoryTransactionList=new ArrayList<JobCategoryTransaction>();
/*				jobCategoryTransactionList = jobCategoryTransactionDAO.findjobCategoryList(jobCategoryMaster.getJobCategoryId());
				jobCategoryTransactionList=jobCategoryTransactionDAO.findjobCategoryList(jobCategoryMaster.getJobCategoryId());*/
				jobCategoryTransactionList = jobCategoryTransactionDAO.getRecordBasedOnJobCategory(jobCategoryMaster.getJobCategoryId());
				
				for(JobCategoryTransaction re: jobCategoryTransactionList){
					if(re.getPortfolioStatus().equalsIgnoreCase("A")){
						p="A";
					}
				}
				jobCategoryObject=new ArrayList<Object>();
				jobCategoryObject.add(p);
				jobCategoryObject.add(jobCategoryMaster);
				
				// **************************************************************************************
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return jobCategoryObject;
	}


	/* ====== Gagan : This method is used for  download Jsi =======*/
	@Transactional(readOnly=false)
	public String downloadJsi(int jobCategoryId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String path="";
		try 
		{
			String source="";
			String target="";
			JobCategoryMaster jc=new JobCategoryMaster();
			if(jobCategoryId!=0)
				jc=jobCategoryMasterDAO.findById(jobCategoryId, false, false);

			String fileName= jc.getAssessmentDocument();
			source = Utility.getValueOfPropByKey("districtRootPath")+jc.getDistrictMaster().getDistrictId()+"/jobcategory/"+jc.getAssessmentDocument();
			target = context.getServletContext().getRealPath("/")+"/"+"/district/"+jc.getDistrictMaster().getDistrictId()+"/";

			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());

			FileUtils.copyFile(sourceFile, targetFile);
			path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+jc.getDistrictMaster().getDistrictId()+"/"+jc.getAssessmentDocument(); 	

			//if(districtORSchoool.equalsIgnoreCase("district")){
			//path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+districtId+"/"+docFileName; 	
			/* }else{
	        	  path = Utility.getValueOfPropByKey("contextBasePath")+"/school/"+districtORSchooolId+"/"+docFileName;
	        }*/
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;

	}
	
	/* ====== Gagan : This method is used for  download Jsi =======*/
	public String removeJsi(int jobCategoryId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try 
		{
			JobCategoryMaster jc=null;
			if(jobCategoryId!=0)
			{
				jc=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				SecondaryStatus  secondaryStatus = 	secondaryStatusDAO.findSecondaryStatusByDistrict(jc.getDistrictMaster(),jc);
				if(secondaryStatus!=null)
					return "3";
			}

			if(jc!=null ){

				File file = new File(Utility.getValueOfPropByKey("districtRootPath")+jc.getDistrictMaster().getDistrictId()+"/jobcategory/"+jc.getAssessmentDocument());
				//System.out.println("Remove Jsi 1111 "+file.exists()+"\n"+file.getPath());
				if(file.exists()){
					if(file.delete()){
						System.out.println(file.getName()+" deleted");
					}else{
						System.out.println("Delete operation for Jsi is failed.");
						return "2";
					}
					jc.setAssessmentDocument(null);
					jobCategoryMasterDAO.makePersistent(jc);
					return "1";
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return "2";
		}
		return "2";
	}
 // For VVI
	public String getStatusList(String districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		StringBuffer status=	new StringBuffer();
		
		List<StatusMaster> statusMasterList = null;
		List<StatusMaster> lstStatusMaster = null;
		
		try
		{
			
			Map<Integer,SecondaryStatus> mapSStatus = new HashMap<Integer, SecondaryStatus>();
			
			DistrictMaster districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			List<SecondaryStatus> lstSecStatus=	secondaryStatusDAO.findSecondaryStatusOnlyStatus(districtMaster);
			List<SecondaryStatus> lstsecondaryStatus=  secondaryStatusDAO.findSecondaryStatusOnly(districtMaster);
			
			
			if(lstSecStatus!=null)
			for(SecondaryStatus secStatus : lstSecStatus){
				mapSStatus.put(secStatus.getStatusMaster().getStatusId(),secStatus);
			}
			

			SecondaryStatus secondaryStatus=null;

			String[] statuss = {"hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
			lstStatusMaster	=	statusMasterDAO.findStatusByStatusByShortNames(statuss);
			
			statusMasterList = new ArrayList<StatusMaster>();
			
			for(StatusMaster sm: lstStatusMaster){
				if(sm.getBanchmarkStatus()==1){
					secondaryStatus=mapSStatus.get(sm.getStatusId());
					if(secondaryStatus!=null){
						sm.setStatus(secondaryStatus.getSecondaryStatusName());
					}
				}
				statusMasterList.add(sm);
			}
			
			status.append("<select class='form-control' id='slctStatusID'>");
			//status.append("<option id='' value='' ></option>");
			
			if(statusMasterList!=null && statusMasterList.size()>0)
			{
				for(StatusMaster sm:statusMasterList)
				{
					status.append("<option value='"+sm.getStatusId()+"' >"+sm.getStatus()+"</option>");
				}
			}
			
			if(lstsecondaryStatus!=null && lstsecondaryStatus.size()>0)
			{
				for(SecondaryStatus ssm:lstsecondaryStatus)
				{
					status.append("<option value='SSID_"+ssm.getSecondaryStatusId()+"' >"+ssm.getSecondaryStatusName()+"</option>");
				}
			}
			
			
			status.append("</select>");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	
		return status.toString();
	}
	
	@Transactional(readOnly=false)
	public DistrictMaster getVVIDefaultFields(Integer districtId)
	{
		System.out.println(" ====== getVVIDefaultFields =======");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		DistrictMaster districtMaster	=	new DistrictMaster();
		try{
			if(districtId!=null){
				districtMaster = districtMasterDAO.findById(districtId, false, false);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtMaster;
	}
	
	public String displayAllDistrictAssessment(String districtId,Integer jobcategoryId)
	{
		System.out.println(" =================== displayAllDistrictAssessment ================== ");
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		StringBuffer sbAttachSchool=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		boolean selectedDAS = false;
		DistrictMaster districtMaster  = null;
		JobCategoryMaster jobCategoryMaster = null;
		try
		{
			districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			if(districtMaster!=null)
			{
				if(jobcategoryId!=null && !jobcategoryId.equals(""))
					jobCategoryMaster = jobCategoryMasterDAO.findById(jobcategoryId, false, false);
				
				//Job category Wise Data
				if(jobCategoryMaster!=null)
				{
					List<DistrictAssessmentDetail> AlldistrictAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
					List<DistrictAssessmentDetail> selecteddistAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
					
					AlldistrictAssessmentDetails = districtAssessmentDetailDAO.findByDistrict(districtMaster);
					
					
					String daIds = jobCategoryMaster.getDistrictAssessmentId();
					
					
					String arr[]=null;
					List multiDAIds = new ArrayList();
					
					if(daIds!=null && !daIds.equals(""))
					{
						if(daIds!="" && daIds.length() > 0)
						{
							arr=daIds.split("#");
						}
						
						if(arr!=null && arr.length > 0)
						{
							for(int i=0;i<arr.length;i++)
							{
								multiDAIds.add(Integer.parseInt((arr[i])));	
							}
						}
					}
					
					selecteddistAssessmentDetails = districtAssessmentDetailDAO.findByDistAssessmentIds(multiDAIds);
								
					AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails);
					//Unselected DA List
					sb.append("<select multiple id=\"lstDistrictAdmins\" name=\"lstDistrictAdmins\" class=\"form-control\" style=\"height: 150px;\" >");
					if(AlldistrictAssessmentDetails.size()>0)
					{
						for (DistrictAssessmentDetail selectedDASMT : AlldistrictAssessmentDetails) {
							sb.append("<option value="+selectedDASMT.getDistrictAssessmentId()+">"+selectedDASMT.getDistrictAssessmentName()+"</option>");
						}
				//		selectedDAS = true;
					}
					sb.append("</select>");
					
					//Selected DA List
					sbAttachSchool.append("<select multiple class=\"form-control\" id=\"attachedDAList\" name=\"attachedDAList\" style=\"height: 150px;\">");
					if(selecteddistAssessmentDetails.size()>0)
					{
						for (DistrictAssessmentDetail selectdDASMT : selecteddistAssessmentDetails) {
							sbAttachSchool.append("<option value="+selectdDASMT.getDistrictAssessmentId()+">"+selectdDASMT.getDistrictAssessmentName()+"</option>");
						}
						System.out.println("01");
						selectedDAS = true;
					}
					sbAttachSchool.append("</select>");
				}else
				{
					List<DistrictAssessmentDetail> AlldistrictAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
					List<DistrictAssessmentDetail> selecteddistAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
					
					AlldistrictAssessmentDetails = districtAssessmentDetailDAO.findByDistrict(districtMaster);
					
					
				    String daIds = districtMaster.getDistrictAssessmentId();
					
					
					String arr[]=null;
					List multiDAIds = new ArrayList();
					
					if(daIds!=null && !daIds.equals(""))
					{
						if(daIds!="" && daIds.length() > 0)
						{
							arr=daIds.split("#");
						}
						
						if(arr!=null && arr.length > 0)
						{
							for(int i=0;i<arr.length;i++)
							{
								multiDAIds.add(Integer.parseInt((arr[i])));	
							}
						}
					}
					
					selecteddistAssessmentDetails = districtAssessmentDetailDAO.findByDistAssessmentIds(multiDAIds);
					
					AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails);
					//Unselected DA List
					sb.append("<select multiple id=\"lstDistrictAdmins\" name=\"lstDistrictAdmins\" class=\"form-control\" style=\"height: 150px;\" >");
					if(AlldistrictAssessmentDetails.size()>0)
					{
						for (DistrictAssessmentDetail selectedDASMT : AlldistrictAssessmentDetails) {
							sb.append("<option value="+selectedDASMT.getDistrictAssessmentId()+">"+selectedDASMT.getDistrictAssessmentName()+"</option>");
						}
				//		selectedDAS = true;
					}
					sb.append("</select>");
					
					//Selected DA List
					sbAttachSchool.append("<select multiple class=\"form-control\" id=\"attachedDAList\" name=\"attachedDAList\" style=\"height: 150px;\">");
					if(selecteddistAssessmentDetails.size()>0)
					{
						for (DistrictAssessmentDetail selectdDASMT : selecteddistAssessmentDetails) {
							sbAttachSchool.append("<option value="+selectdDASMT.getDistrictAssessmentId()+">"+selectdDASMT.getDistrictAssessmentName()+"</option>");
						}
						selectedDAS = true;	
					}
					sbAttachSchool.append("</select>");
				}
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return sb.toString()+"||"+sbAttachSchool.toString()+"||"+selectedDAS;
	}
	
	/*	
	 * 		Get Reference Check List
	 * 		Date	:	24 Feb 2015
	 * 		By		:	Hanzala Subhani
	 * 
	 */
	
	public String getReferenceByDistrictList(int districtId, int jobCategoryId){
		StringBuffer sb = new StringBuffer();
		DistrictMaster districtMaster							=	null;
		List<ReferenceCheckQuestionSet> refChkQuestionSet		= 	null;
		List<ReferenceQuestionSets>	referenceQuestionSetsList 	= 	null;
		JobCategoryMaster jobCategoryMaster						=	null;
		List<Integer> ques = new ArrayList<Integer>();
		Integer counter = 1; 
		Integer questionId = null;
		try 
		{
			System.out.println("District ID::::::::::"+districtId);
			if(""+districtId!="")
			districtMaster			=	districtMasterDAO.findById(districtId, false, false);
			if(jobCategoryId!=0)
			jobCategoryMaster		=	jobCategoryMasterDAO.findById(jobCategoryId, false, false);
			refChkQuestionSet 		= 	referenceCheckQuestionSetDAO.getReferenceByDistrictAndJobCategory(districtMaster, jobCategoryMaster);
			System.out.println("districtMaster    status     ::::::::"+districtMaster.getStatus());
			if(refChkQuestionSet.size()>0 && refChkQuestionSet !=null && refChkQuestionSet.get(0) != null)
				questionId	=	refChkQuestionSet.get(0).getReferenceQuestionSets().getID();
				referenceQuestionSetsList	=	referenceQuestionSetsDAO.findByDistrictActive(districtMaster);
				sb.append("<select id='avlbList' name='avlbList' class='span4'>");
				sb.append("<option value=''>Please Select Question Set</option>");
				if(referenceQuestionSetsList!=null && referenceQuestionSetsList.size() > 0)
					for(ReferenceQuestionSets refChkRecord : referenceQuestionSetsList)
						if(refChkQuestionSet!=null && refChkQuestionSet.size() > 0 && refChkQuestionSet.get(0) != null){
							if(questionId.equals(refChkRecord.getID())){
								sb.append("<option selected value='"+refChkRecord.getID()+"'>"+refChkRecord.getQuestionSetText()+"</option>");
							} else {
								sb.append("<option value='"+refChkRecord.getID()+"'>"+refChkRecord.getQuestionSetText()+"</option>");
							}
						} else {
							sb.append("<option value='"+refChkRecord.getID()+"'>"+refChkRecord.getQuestionSetText()+"</option>");
						}
						// sb.append("<option value='"+refChkRecord.getID()+"'>"+refChkRecord.getQuestionSetText()+"</option>");
			  sb.append("</select>");
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public String getQQuestionSetByDistrictAndJobCategoryList(Integer districtId, Integer jobCategoryId){
		System.out.println(" ******* getQQuestionSetByDistrictAndJobCategoryList**** districtId: "+districtId+" jobCategoryId : "+jobCategoryId);
		
        WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		StringBuffer sb = new StringBuffer();
        DistrictMaster districtMaster = null;
        JobCategoryMaster jobCategoryMaster = null;
        
        List<QqQuestionSets> districtSpecificQQList = null;
		
	 try{
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		if(districtId!=null){			
		districtMaster = districtMasterDAO.findById(districtId, false, false);
	    Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
		districtSpecificQQList = qqQuestionSetsDAO.findByCriteria(criterion1);
	    }else{
		districtSpecificQQList = new ArrayList<QqQuestionSets>();
	    }
        
		if(jobCategoryId!=null){
			 jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
		}
		
		//sb.append("<select id='qqAvlbList' name='qqAvlbList' class='span4'>");
		sb.append("<option value=''>"+lblPleaseSelectQQSet+"</option>");
		
		if(districtSpecificQQList!=null)
	    {
			Integer selectOpt=0;
			try {
				if(jobCategoryMaster!=null && jobCategoryMaster.getQuestionSets()!=null){
					selectOpt= jobCategoryMaster.getQuestionSets().getID();
				}else{
					selectOpt= districtMaster.getQqQuestionSets().getID();
				}
			} catch (Exception e) {}
		  for(QqQuestionSets list: districtSpecificQQList)
	       {
			  if(list.getID().equals(selectOpt))
	    		  sb.append("<option selected value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
			  else
				  sb.append("<option value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
	       }
	     }
		//sb.append("</select>");
		
	 }catch(Exception e){
        	 e.printStackTrace();
         }
		return sb.toString();
	}
	
	public String getJobCategoryByDistrictId(int districtId,int jobSubCate)
	{
		StringBuffer sb	=	new StringBuffer();
		try {
			DistrictMaster districtMaster	=	districtMasterDAO.findById(districtId, false, false);
			/*=== values to dropdown from jobCategoryMaster Table ===*/
			List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
			//sb.append("<select id='parentJobCategoryId' name='parentJobCategoryId' class='form-control'>");
			JobCategoryMaster jobSubCategory = null;
			
			if(jobSubCate>0)
				jobSubCategory = jobCategoryMasterDAO.findById(jobSubCate, false, false);
			
			sb.append("<option value='0'>"+optStrJobCat+"</option>");
			if(jobCategoryMasterlst!=null && jobCategoryMasterlst.size()>0){
				for(JobCategoryMaster jb: jobCategoryMasterlst)
				{
					if(jb.getParentJobCategoryId()==null){
						if(jobSubCategory!=null && jobSubCategory.getParentJobCategoryId()!=null && jb.getJobCategoryId().equals(jobSubCategory.getParentJobCategoryId().getJobCategoryId()))
							sb.append("<option value='"+jb.getJobCategoryId()+"' selected>"+jb.getJobCategoryName()+"</option>");
						else
							sb.append("<option value='"+jb.getJobCategoryId()+"'>"+jb.getJobCategoryName()+"</option>");
					}
				}
			}
		} catch (Exception e) 
		{
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	public String getQQSetForOnboardingByDistrictAndJobCategoryList(Integer districtId, Integer jobCategoryId){
		
        WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictMaster districtMaster=null;
		StringBuffer sb = new StringBuffer();
        JobCategoryMaster jobCategoryMaster = null;
        
        List<QqQuestionSets> districtSpecificQQList = null;
		
	 try{
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}	
		if(districtId!=null){			
		districtMaster = districtMasterDAO.findById(districtId, false, false);
	    Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
		districtSpecificQQList = qqQuestionSetsDAO.findByCriteria(criterion1);
	    }else{
		districtSpecificQQList = new ArrayList<QqQuestionSets>();
	    }
        System.out.println("Size :::::::::::::::"+districtSpecificQQList.size());
		if(jobCategoryId!=null){
			 jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
		}
     	sb.append("<option value=''>Please Select QQ Set to Job Category</option>");
		
		if(districtSpecificQQList!=null)
	    {
			Integer selectOpt=0;
			try {
				if(jobCategoryMaster!=null && jobCategoryMaster.getQuestionSetsForOnboarding()!=null){
					selectOpt= jobCategoryMaster.getQuestionSetsForOnboarding().getID();
				}else{
					selectOpt= districtMaster.getQqQuestionSets().getID();
				}
			} catch (Exception e) {}
		  for(QqQuestionSets list: districtSpecificQQList)
	       {
			  System.out.println("Status :::::::"+list.getStatus());
			  if(list.getID().equals(selectOpt))
	    		  sb.append("<option selected value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
			  else
				  sb.append("<option value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
	       }
	     }
		//sb.append("</select>");
	 }catch(Exception e){
        	 e.printStackTrace();
         }
		return sb.toString();
	}
	
	public String getEpiGroups()
	{
		System.out.println("::::::::::::::::: JobCategoryAjax : getEpiGroups :::::::::::::::::");
		StringBuffer sb	= new StringBuffer();
		try{
			List<AssessmentGroupDetails> assessmentGroupDetailList = null;
			assessmentGroupDetailList = assessmentGroupDetailsDAO.getAllAssessmentGroupDetailsList(1, 1);
			sb.append("<option value='0'>"+optStrEpiGroups+"</option>");
			if(assessmentGroupDetailList!=null && assessmentGroupDetailList.size()>0){
				for(AssessmentGroupDetails assessmentGroupDetails : assessmentGroupDetailList)
				{
					if(assessmentGroupDetails!=null && assessmentGroupDetails.getAssessmentGroupName()!=null && assessmentGroupDetails.getAssessmentGroupName().equalsIgnoreCase("Standard EPI"))
						sb.append("<option value='"+assessmentGroupDetails.getAssessmentGroupId()+"' selected>"+assessmentGroupDetails.getAssessmentGroupName()+"</option>");
					else
						sb.append("<option value='"+assessmentGroupDetails.getAssessmentGroupId()+"'>"+assessmentGroupDetails.getAssessmentGroupName()+"</option>");
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
}
