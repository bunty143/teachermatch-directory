package tm.services;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictApprovalGroups;
import tm.bean.DistrictWiseApprovalGroup;
import tm.bean.JobApprovalHistory;
import tm.bean.JobApprovalProcess;
import tm.bean.MailToCandidateOnImport;
import tm.bean.TeacherDetail;
import tm.bean.UserLoginHistory;
import tm.bean.cgreport.DistrictMaxFitScore;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.HqBranchesDistricts;
import tm.bean.master.CityMaster;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictAttachment;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictNotes;
import tm.bean.master.DistrictSchools;
import tm.bean.master.FutureDistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.menu.MenuMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictApprovalGroupsDAO;
import tm.dao.DistrictWiseApprovalGroupDAO;
import tm.dao.JobApprovalHistoryDAO;
import tm.dao.JobApprovalProcessDAO;
import tm.dao.MailToCandidateOnImportDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.cgreport.DistrictMaxFitScoreDAO;
import tm.dao.districtassessment.DistrictAssessmentDetailDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.hqbranchesmaster.HqBranchesDistrictsDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictAttachmentDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictNotesDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.QqQuestionSetsDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.menu.MenuMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.quartz.NewJobAlertServices;
import tm.utility.IPAddressUtility;
import tm.utility.ImageResize;
import tm.utility.Utility;


public class DistrictAjax 
{
	
	 static String locale = Utility.getValueOfPropByKey("locale");
     String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String optDistrict=Utility.getLocaleValuePropByKey("optDistrict", locale);
	 String optSchool=Utility.getLocaleValuePropByKey("tableheaderSchool", locale);
	 String hdAttachment=Utility.getLocaleValuePropByKey("hdAttachment", locale);
	 String lblCreatedBy=Utility.getLocaleValuePropByKey("lblCreatedBy", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String msgNoNotefound1=Utility.getLocaleValuePropByKey("msgNoNotefound1", locale);
	 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	 String lnkDlt=Utility.getLocaleValuePropByKey("lnkDlt", locale);
	 String lblDeactivate=Utility.getLocaleValuePropByKey("lblDeactivate", locale);
	 String lblActivate=Utility.getLocaleValuePropByKey("lblActivate", locale);
	 String optHeadQuarter=Utility.getLocaleValuePropByKey("optHeadQuarter", locale);
	 String optBranch=Utility.getLocaleValuePropByKey("optBranch", locale);
	 String lbllDocName=Utility.getLocaleValuePropByKey("lbllDocName", locale);
	 
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	@Autowired
	private QqQuestionSetsDAO qqQuestionSetsDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private JobApprovalProcessDAO jobApprovalProcessDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
	{
		this.districtMasterDAO = districtMasterDAO;
	}
	/* @Author: Gagan */
	@Autowired
	private StateMasterDAO stateMasterDAO; 
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) 
	{
		this.stateMasterDAO = stateMasterDAO;
	}
	@Autowired
	private CityMasterDAO cityMasterDAO; 
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO) 
	{
		this.cityMasterDAO = cityMasterDAO;
	}
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) 
	{
		this.districtKeyContactDAO = districtKeyContactDAO;
	}
	
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
 	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) 
 	{
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}
 	@Autowired
 	private DistrictNotesDAO districtNotesDAO;
 	public DistrictNotesDAO getDistrictNotesDAO() 
 	{
		return districtNotesDAO;
	}
	public void setDistrictNotesDAO(DistrictNotesDAO districtNotesDAO)
	{
		this.districtNotesDAO = districtNotesDAO;
	}
	@Autowired
	private UserMasterDAO usermasterdao;
	public void setUsermasterdao(UserMasterDAO usermasterdao) 
	{
		this.usermasterdao = usermasterdao;
	}
	
	@Autowired
	private  TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) 
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	
	@Autowired
	private  RoleMasterDAO roleMasterDAO;
	public void setRoleMasterDAO(RoleMasterDAO roleMasterDAO) 
	{
		this.roleMasterDAO = roleMasterDAO;
	}
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}

	@Autowired
	private MailToCandidateOnImportDAO mailToCandidateOnImportDAO;
	public void setMailToCandidateOnImportDAO(
			MailToCandidateOnImportDAO mailToCandidateOnImportDAO) {
		this.mailToCandidateOnImportDAO = mailToCandidateOnImportDAO;
	}
	
	@Autowired
	private DistrictMaxFitScoreDAO districtMaxFitScoreDAO;
	public void setDistrictMaxFitScoreDAO(
			DistrictMaxFitScoreDAO districtMaxFitScoreDAO) {
		this.districtMaxFitScoreDAO = districtMaxFitScoreDAO;
	}
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	
	@Autowired
	private DistrictApprovalGroupsDAO districtApprovalGroupsDAO;
	public void setDistrictApprovalGroupsDAO(DistrictApprovalGroupsDAO districtApprovalGroupsDAO) {
		this.districtApprovalGroupsDAO = districtApprovalGroupsDAO;
	}

	
	@Autowired
	private DistrictAttachmentDAO districtAttachmentDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private DistrictAssessmentDetailDAO districtAssessmentDetailDAO;
	
	@Autowired
	private JobApprovalHistoryDAO jobApprovalHistoryDAO;
	
	@Autowired
	private DistrictWiseApprovalGroupDAO districtWiseApprovalGroupDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	@Autowired
	private HqBranchesDistrictsDAO hqBranchesDistrictsDAO;
	
	@Autowired
	private MenuMasterDAO menuMasterDAO;

	
	public String showFile(String districtORSchoool,int districtORSchooolId,String docFileName)
	{
		System.out.println(" Inside District Ajax showfile() :: ");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		String path="";
		String source="";
		String target="";
		try 
		{
			if(districtORSchoool.equalsIgnoreCase("district")){
				source = Utility.getValueOfPropByKey("districtRootPath")+districtORSchooolId+"/"+docFileName;
				target = context.getServletContext().getRealPath("/")+"/"+"/district/"+districtORSchooolId+"/";
			}else{
				source = Utility.getValueOfPropByKey("schoolRootPath")+districtORSchooolId+"/"+docFileName;
				target = context.getServletContext().getRealPath("/")+"/"+"/school/"+districtORSchooolId+"/";
			}
		    File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        System.out.println(" Inside District Ajax showfile() :: sourceFile.getName() ::"+sourceFile.getName());
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        FileUtils.copyFile(sourceFile, targetFile);
	        
	        String ext = null;
	       
	        
	        /* @Start
	         * @Ashish Kumar
	         * Description :: Get File Extension and Resize Image Before display.
	         */
	        String s = sourceFile.getName();
	        int i = s.lastIndexOf('.');

	        if (i > 0 &&  i < s.length() - 1) {
	            ext = s.substring(i+1).toLowerCase();
	            System.out.println(" ext of file :: "+ext);
	        }
	        
	        if(ext.equals("jpeg") || ext.equals("png") || ext.equals("gif") || ext.equals("jpg")){
	        	ImageResize.resizeImage(targetDir+"/"+sourceFile.getName());
	        }
	        /* @End
	         * @Ashish Kumar
	         * Description :: Get File Extension and Resize Image Before display.
	         */
	        
	        
	        if(districtORSchoool.equalsIgnoreCase("district")){
	        	  path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+districtORSchooolId+"/"+docFileName; 	
	        	  System.out.println(" district path :: "+path);
	        	  
	        }else{
	        	  path = Utility.getValueOfPropByKey("contextBasePath")+"/school/"+districtORSchooolId+"/"+docFileName;
	        }
		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
		}
		
		return path;
	}
	
	public String SearchDistrictRecords(int entityID,String DistrictName,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer tmRecords =	new StringBuffer();
		try{
			
			//-- get no of record in grid,
			//-- set start and end position
			//String locale = Utility.getValueOfPropByKey("locale");
			String lblActions = Utility.getLocaleValuePropByKey("lblActions", locale);
            String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
            String lblDeactivate = Utility.getLocaleValuePropByKey("lblDeactivate", locale);
            String lblActivate = Utility.getLocaleValuePropByKey("lblActivate", locale);
            String lblView = Utility.getLocaleValuePropByKey("lblView", locale);
            String lblEdit = Utility.getLocaleValuePropByKey("lblEdit", locale);
            String lblDistrictName  = Utility.getLocaleValuePropByKey("lblDistrictName", locale);
            String lblState   = Utility.getLocaleValuePropByKey("lblState", locale);
            String lblFinalDecisionMaker   = Utility.getLocaleValuePropByKey("lblFinalDecisionMaker", locale);
            String lblOfTeachers   = Utility.getLocaleValuePropByKey("lblOfTeachers", locale);
            String lblOfStudents   = Utility.getLocaleValuePropByKey("lblOfStudents", locale);
            String lblOfSchools   = Utility.getLocaleValuePropByKey("lblOfSchools", locale);
            String lblCreatedOn   = Utility.getLocaleValuePropByKey("lblCreatedOn", locale);
            String lblStatus    = Utility.getLocaleValuePropByKey("lblStatus", locale);
            String lblActive    = Utility.getLocaleValuePropByKey("lblActive", locale);
            String lblInactive    = Utility.getLocaleValuePropByKey("lblInactive", locale);
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord 		=	0;
			//------------------------------------

			
			UserMaster userMaster = null;
			int districtId =0;
			int entityIDLogin=0;
			int roleId=0;

			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getDistrictId()!=null){
					
					districtId =userMaster.getDistrictId().getDistrictId();
				}
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				entityIDLogin=userMaster.getEntityType();
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,5,"managedistrict.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(districtId!=0 && entityIDLogin==2){
				entityID=entityIDLogin;
			}
		
			List<DistrictMaster> districtMaster	  =	null;
			
			/** set default sorting fieldName **/
			String sortOrderFieldName="districtName";
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				sortOrderFieldName=sortOrder;
			}
			String sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
			if(sortOrderType!=null)
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("1")){
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}

			/**End ------------------------------------**/
			
			if(entityID==1 || districtId==0){
					if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
						Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
						totaRecord			= districtMasterDAO.getRowCountWithSort(sortOrderStrVal,criterion);
						districtMaster  	= districtMasterDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion);
						//****************** Added by deepak  *******************************
						for(DistrictMaster dt: districtMaster){
						if(dt.getDistrictName().equalsIgnoreCase(DistrictName.trim())){
							session.setAttribute("districtMasterId", dt.getDistrictId());
							session.setAttribute("districtMasterName", dt.getDistrictName());
							break;
						}
						}
						//**************************************************************
					}else{
						session.setAttribute("districtMasterId", 0);
						session.setAttribute("districtMasterName", "");
						totaRecord			= districtMasterDAO.getRowCountWithSort(sortOrderStrVal);
						districtMaster  	= districtMasterDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage);
					}
			}else{
				Criterion criterion= Restrictions.eq("districtId", districtId);
				totaRecord			= districtMasterDAO.getRowCountWithSort(sortOrderStrVal,criterion);
				districtMaster  	= districtMasterDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion);
			}
				
			tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblDistrictName,sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='250' valign='top'>"+responseText+"</th>");
		
			responseText=PaginationAndSorting.responseSortingLink(lblState,sortOrderFieldName,"stateId",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblFinalDecisionMaker,sortOrderFieldName,"dmName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblOfTeachers,sortOrderFieldName,"totalNoOfTeachers",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='20' valign='top'> "+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblOfStudents,sortOrderFieldName,"totalNoOfStudents",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='20' valign='top'> "+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblOfSchools,sortOrderFieldName,"totalNoOfSchools",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  width='20' valign='top'> "+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblCreatedOn,sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='12%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			tmRecords.append(" <th width='50' valign='top'>"+responseText+"</th>");
			tmRecords.append(" <th  width='15%' valign='top'>"+lblActions+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(districtMaster.size()==0)
				tmRecords.append("<tr><td colspan='8' align='center'>"+lblNoRecord+"</td></tr>" );
			
			System.out.println("No of Records "+districtMaster.size());
			
			for (DistrictMaster districtMasterDetails : districtMaster) 
			{
				
				tmRecords.append("<tr>");
				tmRecords.append("<td>"+districtMasterDetails.getDistrictName());
				if(districtMasterDetails.getSelfServicePortfolioStatus()!=null && districtMasterDetails.getSelfServicePortfolioStatus().equals(true))
					tmRecords.append("&nbsp;<a class='selfServiceTTips' data-original-title='Self Service ON' target='_blank' rel='tooltip' href='./manageportfolio.do?districtId="+districtMasterDetails.getDistrictId()+"'><span class='fa-cog icon-large iconcolorRed'></span></a>");
				else
					tmRecords.append("&nbsp;<a class='selfServiceTTips' data-original-title='No Self Service' target='_blank' rel='tooltip' href='javascript:void(0);'><span class='fa-cog icon-large iconcolorhover'></span></a>");
				
				tmRecords.append("</td>");
				tmRecords.append("<td>"+districtMasterDetails.getStateId().getStateName()+"</td>");
				tmRecords.append("<td>"+districtMasterDetails.getDmName()+"</td>");
				tmRecords.append("<td>"+districtMasterDetails.getTotalNoOfTeachers()+"</td>");
				tmRecords.append("<td>"+districtMasterDetails.getTotalNoOfStudents()+"</td>");
				tmRecords.append("<td>"+districtMasterDetails.getTotalNoOfSchools()+"</td>");
				tmRecords.append("<td nowrap>"+Utility.convertDateAndTimeToUSformatOnlyDate(districtMasterDetails.getCreatedDateTime())+"</td>");
				tmRecords.append("<td>");
				if(districtMasterDetails.getStatus().equalsIgnoreCase("A"))
					tmRecords.append(lblActive);
				else
					tmRecords.append(lblInactive);
				tmRecords.append("</td>");
				tmRecords.append("<td nowrap>");
				
				
					boolean pipeFlag=false;
					if(roleAccess.indexOf("|2|")!=-1){
						tmRecords.append("<a title='Edit' href='editdistrict.do?&distId="+districtMasterDetails.getDistrictId()+"'><i class='fa fa-pencil-square-o fa-lg'></i></a>");
						pipeFlag=true;
					}else if(roleAccess.indexOf("|4|")!=-1){
						tmRecords.append("<a title='Edit' href='editdistrict.do?&distId="+districtMasterDetails.getDistrictId()+"'><i class='fa fa-pencil-square-o fa-lg'></i></a>");
						pipeFlag=true;
					} 
						
					if(roleAccess.indexOf("|7|")!=-1 && entityIDLogin==1){
						if(pipeFlag)tmRecords.append(" | ");
						if(districtMasterDetails.getStatus().equalsIgnoreCase("A"))
							tmRecords.append("<a title='Deactivate' href='javascript:void(0);' onclick=\"return activateDeactivateDistrict("+districtMasterDetails.getDistrictId()+",'I')\"><i class='fa fa-times fa-lg'></i></a>");
						else
							tmRecords.append("<a title='Activate' href='javascript:void(0);' onclick=\"return activateDeactivateDistrict("+districtMasterDetails.getDistrictId()+",'A')\"><i class='fa fa-check fa-lg'></i></a>");
					}else{
						tmRecords.append("&nbsp;");
					}			
				tmRecords.append("</td>");
				tmRecords.append("</tr>");
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForManageJobOrdersAjaxDSPQ(request,totaRecord,noOfRow, pageNo));
			//System.out.println(tmRecords.toString());
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tmRecords.toString();
	}	
	public List<DistrictMaster> getFieldOfDistrictList(String DistrictName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		try{
			
			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion);
				
				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2);
				
				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
				
			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return districtMasterList;
	}
	public List<SchoolMaster> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<SchoolMaster> schoolMasterList =  new ArrayList<SchoolMaster>();
		List<SchoolMaster> fieldOfSchoolList1 = null;
		List<SchoolMaster> fieldOfSchoolList2 = null;
		try 
		{
			Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
			
			if(districtIdForSchool==0){
				List schoolIds = new ArrayList();
				schoolIds =	districtSchoolsDAO.findSchoolIdAllList();
				Criterion criterionSchoolIds = Restrictions.in("schoolId",schoolIds);
				if(schoolIds.size()>0){
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterionSchoolIds);
						
						Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion2,criterionSchoolIds);
						
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<SchoolMaster> setSchool = new LinkedHashSet<SchoolMaster>(schoolMasterList);
						schoolMasterList = new ArrayList<SchoolMaster>(new LinkedHashSet<SchoolMaster>(setSchool));
					}else{
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterionSchoolIds);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
			}else{
				DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
				Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
				
				if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
					fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(null, 0, 10, criterion,criterion2);
					Collections.sort(fieldOfSchoolList1,SchoolMaster.compSchoolMaster);
					Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
					fieldOfSchoolList2 = schoolMasterDAO.findWithLimit(null, 0, 10,criterion3,criterion2);
					Collections.sort(fieldOfSchoolList2,SchoolMaster.compSchoolMaster);
					schoolMasterList.addAll(fieldOfSchoolList1);
					schoolMasterList.addAll(fieldOfSchoolList2);
					Set<SchoolMaster> setSchool = new LinkedHashSet<SchoolMaster>(schoolMasterList);
					schoolMasterList = new ArrayList<SchoolMaster>(new LinkedHashSet<SchoolMaster>(setSchool));
				}else{
					fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(null, 0, 10,criterion2);
					Collections.sort(fieldOfSchoolList1,SchoolMaster.compSchoolMaster);
					schoolMasterList.addAll(fieldOfSchoolList1);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return schoolMasterList;
	}
	/* ===============      Activate Deactivate District        =========================*/
	//@Transactional(readOnly=false)
	public boolean activateDeactivateDistrict(int DistrictId,String status)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
	    	userMaster=(UserMaster)session.getAttribute("userMaster");
	    }
		try{
			DistrictMaster districtMaster = districtMasterDAO.findById(DistrictId, false, false);
			districtMaster.setStatus(status);
			districtMasterDAO.makePersistent(districtMaster);
			//--------------------------
			
			
			String messageSubject="";
			String messageSend="";
			SimpleDateFormat dformat=new SimpleDateFormat("HH:MM:SS");
			Date d=new Date();
		
			String distname=districtMaster.getDistrictName();		
			
			if(status.equals("I"))
			{
				messageSubject=Utility.getLocaleValuePropByKey("msgDistrictSchoolDeactivated", locale);
				messageSend=Utility.getLocaleValuePropByKey("msgDearTMAdmin", locale)+",<br><br>" +distname+
				Utility.getLocaleValuePropByKey("msgWasDeactivatedby", locale) + userMaster.getFirstName()+" "+userMaster.getLastName()
						+ Utility.getLocaleValuePropByKey("msgAt3", locale) +dformat.format(d)+"."+Utility.getLocaleValuePropByKey("msgPleaseVerifyWith", locale)+userMaster.getFirstName()+" "+userMaster.getLastName()+
						"("+userMaster.getEmailAddress()+") "+
						Utility.getLocaleValuePropByKey("msgIntendedAction", locale);
			}
			if(status.equals("A"))
				{
				messageSubject= Utility.getLocaleValuePropByKey("msgDistrictSchoolActivated", locale);
				messageSend=Utility.getLocaleValuePropByKey("msgDearTMAdmin", locale)+",<br><br>" +distname+
				Utility.getLocaleValuePropByKey("msgWasActivatedBy", locale) + userMaster.getFirstName()+" "+userMaster.getLastName()
						+Utility.getLocaleValuePropByKey("msgAt3", locale)+dformat.format(d)+"."+Utility.getLocaleValuePropByKey("msgPleaseVerifyWith", locale) +userMaster.getFirstName()+" "+userMaster.getLastName()+
						"("+userMaster.getEmailAddress()+") "+
						Utility.getLocaleValuePropByKey("msgIntendedAction", locale);
				}
			System.out.println(messageSend);
				String content="";
				
				Properties  properties = new Properties();  
				try {
					InputStream inputStream = new Utility().getClass().getClassLoader().getResourceAsStream("smtpmail.properties");
					properties.load(inputStream);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			try {				
				
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom(properties.getProperty("smtphost.noreplyusername"));
				dsmt.setMailto(properties.getProperty("smtphost.tmsysadmin"));
				dsmt.setMailsubject(messageSubject);
				content=MailText.messageForDefaultFont(messageSend, userMaster);					
				dsmt.setMailcontent(content);
				
				System.out.println("content     "+content);
				try {
					dsmt.start();	
				} catch (Exception e) {}
			} catch (Exception e) {
				e.printStackTrace();
			}
			//--------------------------
			try{
				UserLoginHistory userLoginHistory = new UserLoginHistory();
				userLoginHistory.setUserMaster(userMaster);
				userLoginHistory.setSessionId(session.getId());
				userLoginHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
				userLoginHistory.setLogTime(new Date());
				if(districtMaster!=null)
					userLoginHistory.setDistrictId(districtMaster);
				if(status.equalsIgnoreCase("A")){
					userLoginHistory.setLogType("Activate District");
				}else{
					userLoginHistory.setLogType("Deactivate District");
				}
				userLoginHistoryDAO.makePersistent(userLoginHistory);
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}catch (Exception e) 
		{
			e.printStackTrace();
			return true;
		}
		System.out.println("complete");
		return true;
	}
	/* @Author: Gagan 
	 * @Discription: It is used to display  City List By State .
	 */	
	/*=================== Get City List By State ================*/
	public String getCityListByState(Long stateId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<CityMaster> lstCityMaster = null;
		StateMaster stateMaster		 			=	stateMasterDAO.findById(stateId, false, false);
		StringBuffer cityOptions 				=	new StringBuffer();
		try{
			lstCityMaster 						=	cityMasterDAO.findCityByState(stateMaster);
			cityOptions.append("<select class='span3 ' name='cityId' id='cityId'>" );
				for (CityMaster lstCityMasterDetail : lstCityMaster) 
				{
					cityOptions.append("<option id='"+lstCityMasterDetail.getCityId()+"' value='"+lstCityMasterDetail.getCityId()+"' >"+lstCityMasterDetail.getCityName()+"</option>");
				}
				cityOptions.append("</select>");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return cityOptions.toString();
	}
	
	/* @Author: Gagan 
	 * @Discription: It is used to save Key Contact in editdistrict Page in Contact information  .
	 */
	/* ===============      save Key Contact        =========================*/
	public int saveKeyContact(int updateUser, Integer keyContactId,Integer districtId,Integer keyContactTypeId,String keyContactFirstName,String keyContactLastName,String keyContactEmailAddress,String keyContactPhoneNumber,String keyContactTitle,String keyContactIdVal)
	{
		System.out.println(":$$$$$$::::::::::updateUser"+updateUser);
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		DistrictKeyContact districtKeyContact	=	new DistrictKeyContact();
		int emailCheck=0;
		int keyContactTypeIdcheck=0;
		int flagVal=0;
		HeadQuarterMaster headQuarterMaster=null;
		if(keyContactId	!=	null){	
			flagVal=1;
		}
		DistrictMaster districtMaster=null;
		if(districtId!=0)
		{
			districtMaster=districtMasterDAO.findById(districtId, false, false);
		}
	//	emailCheck=districtKeyContactDAO.checkEmail(flagVal,keyContactId,keyContactEmailAddress,districtMaster);
		
		
		
		try
		{
			ContactTypeMaster contactType  = contactTypeMasterDAO.findById(keyContactTypeId, false, false); 		
			keyContactTypeIdcheck=districtKeyContactDAO.checkkeyContactTypeContact(contactType,districtMaster,keyContactEmailAddress,keyContactId);
			emailCheck=districtKeyContactDAO.checkEmailContact(flagVal,contactType,keyContactEmailAddress,districtMaster,keyContactId);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return 2;
		}
		if(emailCheck>=1 && keyContactTypeIdcheck>=1  && keyContactIdVal==null){
			return 4;
		}
		else if(emailCheck>=1 && keyContactTypeIdcheck>=1 && keyContactIdVal!=null){
			return 4;
		}
		//Ajay Jain
		
		int userId=0;
		try
		{
			if(keyContactId	!=	null){				
				userId = usermasterdao.getUserId(keyContactEmailAddress);
				districtKeyContact.setKeyContactId(keyContactId);
			}
			else{				
				userId = usermasterdao.getUserId(keyContactEmailAddress);
			}			
			districtKeyContact.setDistrictId(districtId);
			ContactTypeMaster contactTypeMaster	=	contactTypeMasterDAO.findById(keyContactTypeId, false, false);
		
			districtKeyContact.setKeyContactTypeId(contactTypeMaster);
			districtKeyContact.setKeyContactFirstName(keyContactFirstName);
			districtKeyContact.setKeyContactLastName(keyContactLastName);
			districtKeyContact.setKeyContactEmailAddress(keyContactEmailAddress);
			districtKeyContact.setKeyContactPhoneNumber(keyContactPhoneNumber);
			districtKeyContact.setKeyContactTitle(keyContactTitle);
			
			/*======= Check For Unique User Email address ==========*/
		//	List<UserMaster> dupUserEmail =	usermasterdao.checkDuplicateUserEmailForKeyContact(userId,keyContactEmailAddress);
			
			int foundUserId = usermasterdao.getUserId(keyContactEmailAddress);
			int size =	foundUserId;
			
			List<TeacherDetail> dupteacherEmail	= teacherDetailDAO.findByEmail(keyContactEmailAddress);
			int sizeTeacherList = dupteacherEmail.size();		
			if(sizeTeacherList>0)
				return 3;
		
			UserMaster master=null;
			int insertusermaster=0;			
			if(foundUserId==0 && ((size==0 && sizeTeacherList==0 && updateUser==2) || (userId==0 && size==0 && sizeTeacherList==0 && updateUser==1))){
				int verificationCode=(int) Math.round(Math.random() * 2000000);
				UserMaster userSession = (UserMaster) session.getAttribute("userMaster");
				UserMaster userMaster =	new UserMaster();
				userMaster.setEntityType(2);
				Utility utility	=	new Utility();
				String authenticationCode	=	utility.getUniqueCodebyId((1));
				userMaster.setPassword(MD5Encryption.toMD5("teachermatch"));
				userMaster.setAuthenticationCode(authenticationCode);			
				if(districtId!=0)
				{
					districtMaster =	districtMasterDAO.findById(districtId, false, false);
				}
				if(districtMaster!=null && districtMaster.getHeadQuarterMaster()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
				{
					headQuarterMaster=headQuarterMasterDAO.findById(2, false, false);
					if(headQuarterMaster!=null)
					userMaster.setHeadQuarterMaster(headQuarterMaster);
				}				
				userMaster.setTitle(keyContactTitle);
				userMaster.setDistrictId(districtMaster);
				userMaster.setFirstName(keyContactFirstName);
				userMaster.setLastName(keyContactLastName);
				userMaster.setPhoneNumber(keyContactPhoneNumber);
				userMaster.setEmailAddress(keyContactEmailAddress);
				RoleMaster roleMaster = roleMasterDAO.findById(2, false, false);			
				userMaster.setRoleId(roleMaster);
				userMaster.setStatus("A");
				userMaster.setVerificationCode(""+verificationCode);
				userMaster.setForgetCounter(0);
				userMaster.setIsQuestCandidate(false);
				userMaster.setCreatedDateTime(new Date());			   
				usermasterdao.makePersistent(userMaster);
				master=userMaster;
				insertusermaster=1;
				emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), Utility.getLocaleValuePropByKey("msgAddyou", locale),MailText.createUserPwdMailToOtUser(request,userMaster));
			
			}else if((keyContactTypeIdcheck!=0 || emailCheck!=0 || sizeTeacherList!=0)&& updateUser==1){	
				return 3;
			}else if(size>0 && sizeTeacherList==0 && updateUser==1){	
			   DistrictKeyContact districtKeyContact2 = districtKeyContactDAO.findById(keyContactId, false, false);			
			  if(districtKeyContact2!=null){
			   master=new UserMaster(); 
			   if(districtKeyContact2.getUserMaster()!=null){
				   master.setUserId(districtKeyContact2.getUserMaster().getUserId());
				   districtKeyContact.setUserMaster(master);
			   }	   
				   districtKeyContactDAO.makePersistent(districtKeyContact);
			   
			  }
				if(updateUser==1 && foundUserId==0){
					UserMaster userMaster =	usermasterdao.findById(userId, false, false);
					if(userMaster!=null){
					userMaster.setTitle(keyContactTitle);
					userMaster.setFirstName(keyContactFirstName);
					userMaster.setLastName(keyContactLastName);
					userMaster.setPhoneNumber(keyContactPhoneNumber);
					userMaster.setEmailAddress(keyContactEmailAddress);
					userMaster.setForgetCounter(0);
					
					if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().equals(districtMaster))	
					{
						usermasterdao.makePersistent(userMaster);						
					}			
					}		
				}
			}
			if(updateUser==2){				
				if(insertusermaster==0)
				{	
					master=new UserMaster();
					int userid=usermasterdao.getUserId(keyContactEmailAddress);				
					master.setUserId(userid);					
				}
				districtKeyContact.setUserMaster(master);
				districtKeyContactDAO.makePersistent(districtKeyContact);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 2;
		}
        
		return 1;
	}
	
	
	/* @Author: Gagan 
	 * @Discription: It is used to display Key Contact grid in editdistrict Page in Contact information  .
	 */
	public String displayKeyContactGrid(int districtId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			
			//String locale = Utility.getValueOfPropByKey("locale");
			String lblActions = Utility.getLocaleValuePropByKey("lblActions", locale);
			String lblEmail  = Utility.getLocaleValuePropByKey("lblEmail", locale);
			String lblTitle  = Utility.getLocaleValuePropByKey("lblTitle", locale);
			String lblEdit  = Utility.getLocaleValuePropByKey("lblEdit", locale);
			String lblContactType  = Utility.getLocaleValuePropByKey("lblContactType", locale);
			String lblContactName  = Utility.getLocaleValuePropByKey("lblContactName", locale);
			String lblPhone  = Utility.getLocaleValuePropByKey("lblPhone", locale);
			String lblNoContact  = Utility.getLocaleValuePropByKey("lblNoContact", locale);
			String lblDelete   = Utility.getLocaleValuePropByKey("lblDelete", locale);
			
			
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,8,"editdistrict.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}			
			Criterion criterion1 = Restrictions.eq("districtId",districtId);
			List<DistrictKeyContact> districtKeyContact	  	=	null;
		    districtKeyContact = districtKeyContactDAO.findByCriteria(Order.asc("keyContactFirstName"),criterion1);
		
			//dmRecords.append("<div id='keyContactTable' class='span12' ><br/>");
			dmRecords.append("<table border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			dmRecords.append("<th width='20%'>"+lblContactType+"</th>");
			dmRecords.append("<th width='20%'>"+lblContactName+"</th>");
			dmRecords.append("<th width='15%'>"+lblEmail+"</th>");
			dmRecords.append("<th width='15%'>"+lblPhone+"</th>");
			dmRecords.append("<th width='15%'>"+lblTitle+"</th>");
			dmRecords.append("<th width='15%'>"+lblActions+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(districtKeyContact.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+lblNoContact+"</td></tr>" );

			for (DistrictKeyContact districtKeyContactDetail : districtKeyContact) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactTypeId().getContactType()+"</td>");
				dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactFirstName()+" "+districtKeyContactDetail.getKeyContactLastName()+"</td>");
				dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactEmailAddress()+"</td>");
				dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactPhoneNumber()+"</td>");
				dmRecords.append("<td>"+districtKeyContactDetail.getKeyContactTitle()+"</td>");
				dmRecords.append("<td>");
				if(roleAccess.indexOf("|2|")!=-1){
					dmRecords.append("<a href='javascript:void(0);' title='Edit' onclick='return beforeEditKeyContact("+districtKeyContactDetail.getKeyContactId()+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>");
				}else{
					dmRecords.append("&nbsp;");
				}
				if(roleAccess.indexOf("|3|")!=-1){
					dmRecords.append("| <a href='javascript:void(0);' title='Delete' onclick='return deleteKeyContact("+districtKeyContactDetail.getKeyContactId()+")'><i class='fa fa-trash-o' fa-lg></i></a>");
				}
				dmRecords.append("</td>");
			}
			dmRecords.append("</table>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	/* @Author: Gagan 
	 * @Discription: It is used to Edit Key Contact Functionality  .
	 */
	/* ===============    Edit Key Contact Functionality   =========================*/
	@Transactional(readOnly=false)
	public DistrictKeyContact getKeyContactsBykeyContactId(int keyContactId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		DistrictKeyContact districtKeyContact	=	null;
		try
		{
			districtKeyContact	=	districtKeyContactDAO.findById(keyContactId, false, false);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			//return null;
		}
		return districtKeyContact;
	}
	
	/* @Author: Gagan 
	 * @Discription: It is used to delete key contact   .
	 */
	@Transactional(readOnly=false)
	public boolean deleteKeyContact(Integer keyContactId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
			DistrictKeyContact districtKeyContact	=	null;
			districtKeyContact						=	districtKeyContactDAO.findById(keyContactId, false, false);
			
			districtKeyContactDAO.makeTransient(districtKeyContact);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/* @Author: Gagan 
	 * @Discription: It is used to displayNotesGrid in editdistrict Page in Account information  .
	 */
	public String displayNotesGrid(int districtId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		
		try{
			//-- get no of record in grid,
			//-- set start and end position
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,10,"editdistrict.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			SortedMap map = new TreeMap();
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"note";
			String sortOrderNoField		=	"note";
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("note") && !sortOrder.equals("addedBy")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("note"))
				 {
					 sortOrderNoField="note";
				 }
				 if(sortOrder.equals("addedBy"))
				 {
					 sortOrderNoField="addedBy";
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			
			DistrictMaster districtMaster =	districtMasterDAO.findById(districtId, false, false);
			Criterion criterionDSchool = Restrictions.eq("districtMaster",districtMaster);
			List<DistrictNotes> districtNotes	  	=	null;
			districtNotes = districtNotesDAO.findByCriteria(sortOrderStrVal,criterionDSchool);
			
			
			List<DistrictNotes> sortedlstdistrictNotes		=	new ArrayList<DistrictNotes>();
			
			SortedMap<String,DistrictNotes>	sortedMap = new TreeMap<String,DistrictNotes>();
			if(sortOrderNoField.equals("note"))
			{
				sortOrderFieldName	=	"note";
			}
			if(sortOrderNoField.equals("addedBy"))
			{
				sortOrderFieldName	=	"addedBy";
			}
			int mapFlag=2;
			for (DistrictNotes distNote : districtNotes){
				String orderFieldName=""+distNote.getNote().replaceAll("\\<[^>]*>","");
				if(sortOrderFieldName.equals("note")){
					orderFieldName=distNote.getNote().replaceAll("\\<[^>]*>","").toUpperCase()+"||"+distNote.getNotesId();
					sortedMap.put(orderFieldName+"||",distNote);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("addedBy")){
					orderFieldName=distNote.getCreatedBy().getFirstName()+" "+distNote.getCreatedBy().getLastName()+"||"+distNote.getNotesId();
					sortedMap.put(orderFieldName+"||",distNote);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstdistrictNotes.add((DistrictNotes) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstdistrictNotes.add((DistrictNotes) sortedMap.get(key));
				}
			}else{
				sortedlstdistrictNotes=districtNotes;
			}
			
			totalRecord =sortedlstdistrictNotes.size();

			if(totalRecord<end)
				end=totalRecord;
			List<DistrictNotes> lstsortedlstdistrictNotes		=	sortedlstdistrictNotes.subList(start,end);
			
			
			dmRecords.append("<table id='notesTable' border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("Notes",sortOrderFieldName,"note",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Added On",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='25%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Added By",sortOrderFieldName,"addedBy",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			dmRecords.append("<th >"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(districtNotes.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoNotes", locale)+"</td></tr>" );
			System.out.println("No of Records "+districtNotes.size());

			for (DistrictNotes districtNotesDetail : lstsortedlstdistrictNotes) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+districtNotesDetail.getNote()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(districtNotesDetail.getCreatedDateTime())+"</td>");
				dmRecords.append("<td>"+districtNotesDetail.getCreatedBy().getFirstName()+" "+districtNotesDetail.getCreatedBy().getLastName()+"</td>");
				dmRecords.append("<td>");
				if(roleAccess.indexOf("|3|")!=-1){
					dmRecords.append("<a href='javascript:void(0);' onclick='return deleteNotes("+districtNotesDetail.getNotesId()+")'>Remove</a>");
				}else{
					dmRecords.append("&nbsp;");
				}
				dmRecords.append("</td>");
			}
			dmRecords.append("</table>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	
	/* @Author: Gagan 
	 * @Discription: It is used to save Notes in editdistrict Page in Account information  .
	 */
	/* ===============      save Key Contact        =========================*/
	public int saveNotes(String note,Integer districtId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		
		DistrictMaster districtMaster			=	null;
		UserMaster userMaster					=	null;
		DistrictNotes districtNotes				=	new	DistrictNotes();
		try
		{
			districtMaster						=	districtMasterDAO.findById(districtId, false, false);
			districtNotes.setDistrictMaster(districtMaster);
			districtNotes.setNote(note);
			
			userMaster							=	usermasterdao.findById(userSession.getUserId(), false, false);
			districtNotes.setCreatedBy(userMaster);
			districtNotes.setCreatedDateTime(new Date());
			
			districtNotesDAO.makePersistent(districtNotes);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 2;
		}

		return 1;
	}
	
	
	/* @Author: Gagan 
	 * @Discription: It is used to delete Notes   .
	 */
	@Transactional(readOnly=false)
	public boolean deleteNotes(Integer notesId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try
		{
			DistrictNotes districtNotes			=	null;
			districtNotes						=	districtNotesDAO.findById(notesId, false, false);
			
			districtNotesDAO.makeTransient(districtNotes);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/*======== District's User Functionality ============*/
	/*======== It will add district's user (Administrator and Analyst) in usermaster Table and check duplicate user email address in teacherdetail and usermaster table ============*/
	public int saveDistrictAdministratorOrAnalyst(int districtId,String keyContactTypeId,String emailAddress,String firstName,String lastName,int salutation,int entitytype,String title,String phoneNumber,String mobileNumber,String authCode,int roleId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer tmRecords =	new StringBuffer();
		try{
			int userId									=	0;
			int entityType 								=	2;
			DistrictMaster districtMaster					=	null;
			HeadQuarterMaster headQuarterMaster=null;
			/*======= Check For Unique User Email address ==========*/
			List<UserMaster> dupUserEmail				=	usermasterdao.checkDuplicateUserEmail(userId,emailAddress);
			int size 									=	dupUserEmail.size();
			if(size>0)
				return 3;/*=== it return 3 if find duplicate email address from usermaster Table=======*/
			
			List<TeacherDetail> dupteacherEmail			=	teacherDetailDAO.findByEmail(emailAddress);
			int sizeTeacherList 						=	dupteacherEmail.size();
			if(sizeTeacherList>0)
				return 4;/*=== it return 4 if find duplicate email address from teacherdetail Table=======*/
			
			int verificationCode=(int) Math.round(Math.random() * 2000000);
			
			UserMaster userSession									=	(UserMaster) session.getAttribute("userMaster");
			UserMaster userMaster									=	new UserMaster();
			
			userMaster.setEntityType(entityType);
			Utility utility											=	new Utility();
			String authenticationCode								=	utility.getUniqueCodebyId((userId+1));
			if(userId!=0)
			{	
				userMaster.setUserId(userId);
				UserMaster userPwd		=	usermasterdao.findById(userId, false, false);
				userMaster.setPassword(userPwd.getPassword());
				userMaster.setAuthenticationCode(userPwd.getAuthenticationCode());
			}
			else
			{
				userMaster.setPassword(MD5Encryption.toMD5("teachermatch"));
				userMaster.setAuthenticationCode(authenticationCode);
		    }
			if(districtId!=0)
			{
				districtMaster =districtMasterDAO.findById(districtId, false, false);
				
			}
			if(districtMaster!=null && districtMaster.getHeadQuarterMaster()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
			{
				headQuarterMaster=headQuarterMasterDAO.findById(2, false, false);
				if(headQuarterMaster!=null)
				userMaster.setHeadQuarterMaster(headQuarterMaster);
			}	
			userMaster.setTitle(title);
			userMaster.setDistrictId(districtMaster);
			userMaster.setFirstName(firstName);
			userMaster.setLastName(lastName);
			userMaster.setSalutation(salutation);
			userMaster.setMobileNumber(mobileNumber);
			userMaster.setPhoneNumber(phoneNumber);
			userMaster.setEmailAddress(emailAddress);
			RoleMaster roleMaster = roleMasterDAO.findById(roleId, false, false);			
			userMaster.setRoleId(roleMaster);
			userMaster.setStatus("A");
			userMaster.setVerificationCode(""+verificationCode);
			userMaster.setForgetCounter(0);
			userMaster.setIsQuestCandidate(false);
			userMaster.setCreatedDateTime(new Date());
		
			usermasterdao.makePersistent(userMaster);
			if(keyContactTypeId!=null){
				if(!keyContactTypeId.trim().equals(""))
					saveAdministratorOrAnalystDistrictKeyContact(districtId, keyContactTypeId, firstName, lastName, emailAddress, phoneNumber, title, userMaster);
			}
			emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), Utility.getLocaleValuePropByKey("msgAddyou", locale) ,MailText.createUserPwdMailToOtUser(request,userMaster));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return 2;
		}
		return 1;
	}
	
	
	/* @Author: Gagan 
	 * @Discription: It is used to display district Administrator grid in editdistrict Page in Contact information  .
	 */
	public String displayDistrictAdministratorGrid(int districtId,int roleId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*
			  HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("user") == null) {
				return "false";
			}*/
			int roleIdForAccess		=	0;
			int tooltipCounter		=	0;
			String actDeactivateUser=	null;
			UserMaster	userSession=null;
			if(roleId==2)
			{
				actDeactivateUser	=	"actDeactivateUserAdministrator";
			}
			if(roleId==5)
			{
				actDeactivateUser	=	"actDeactivateUserAnalyst";
			}
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userSession=	(UserMaster) session.getAttribute("userMaster");
				if(userSession.getRoleId().getRoleId()!=null){
					roleIdForAccess=userSession.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleIdForAccess,9,"editdistrict.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			DistrictMaster districtMaster							=	null;
			districtMaster											=	districtMasterDAO.findById(districtId, false, false);
			
			RoleMaster roleMaster									=	null;
			roleMaster												=	roleMasterDAO.findById(roleId, false, false);
			
			Criterion criterion										=	Restrictions.eq("districtId",districtMaster);
			Criterion criterion1									=	Restrictions.eq("roleId",roleMaster);
			List<UserMaster> userMasterList								=	usermasterdao.findByCriteria(criterion,criterion1);
			boolean showEditModeUser=true;
			DistrictKeyContact userKeyContact=null;
			if(userMasterList.size()<1)
			{
				if(roleId==2)
				{
					dmRecords.append("<div class='col-sm-3 col-md-3'>"+Utility.getLocaleValuePropByKey("msgNoAdministratorFound", locale)+"</div>");
				}
				if(roleId==5)
				{
					dmRecords.append("<div class='col-sm-3 col-md-3'>"+Utility.getLocaleValuePropByKey("msgNoAnalystFound", locale)+"</div>");
				}
			}
			if(userMasterList.size()>0){
				try{
					List<DistrictKeyContact>	districtKeyContactList=districtKeyContactDAO.findByContactType(userSession, "Manage Users");
					if(districtKeyContactList!=null && districtKeyContactList.size()>0){
						userKeyContact=districtKeyContactList.get(0);
					}
					if(userSession.getEntityType()==2){
						if(userKeyContact==null){
							showEditModeUser=false;
						}
						if(userSession.getRoleId().getRoleId()==7){
							showEditModeUser=true;
						}
					} 
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
			for (UserMaster userMasterDetail : userMasterList) 
			{
				//tooltipCounter++;
				if(userMasterDetail.getStatus().equalsIgnoreCase("A"))
				{
					tooltipCounter++;
					dmRecords.append("<div class='col-sm-3 col-md-3'>"+userMasterDetail.getFirstName()+" "+userMasterDetail.getLastName());
					if(roleAccess.indexOf("|3|")!=-1){
						if(showEditModeUser)
						dmRecords.append("  <a data-original-title='"+Utility.getLocaleValuePropByKey("lblDeactivate", locale)+"' rel='tooltip' id='"+actDeactivateUser+""+tooltipCounter+"' href='javascript:void(0);' onclick=\"return activateDeactivateDistrictAdministratorOrAnalyst("+userMasterDetail.getRoleId().getRoleId()+","+userMasterDetail.getUserId()+",'I');\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a>");
					}
					dmRecords.append("</div>");
				}
				else
				{
					tooltipCounter++;
					dmRecords.append("<div class='col-sm-3 col-md-3'>"+userMasterDetail.getFirstName()+" "+userMasterDetail.getLastName());
					if(roleAccess.indexOf("|3|")!=-1){	
						if(showEditModeUser)
						dmRecords.append("  <a data-original-title='"+Utility.getLocaleValuePropByKey("lblActivate", locale)+"' rel='tooltip' id='"+actDeactivateUser+""+tooltipCounter+"' href='javascript:void(0);' onclick=\"return activateDeactivateDistrictAdministratorOrAnalyst("+userMasterDetail.getRoleId().getRoleId()+","+userMasterDetail.getUserId()+",'A');\"><img width='15' height='15' class='can' src='images/option02.png' alt=''></a>");
					}
					dmRecords.append("</div>");
				}
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	
	/* @Author: Gagan 
	 * @Discription: It is used to displayDistrictSchoolsGrid in editdistrict Page in Account information  .
	 */
	public String displayDistrictSchoolsGrid(int districtId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,10,"editdistrict.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<DistrictSchools> districtSchools	=	null;
			
			SortedMap map = new TreeMap();
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("schoolName")&& !sortOrder.equals("addedBy")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("schoolName"))
				 {
					 sortOrderNoField="schoolName";
				 }
				 if(sortOrder.equals("addedBy"))
				 {
					 sortOrderNoField="addedBy";
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			
			DistrictMaster districtMaster =	districtMasterDAO.findById(districtId, false, false);
			Criterion criterionDSchool = Restrictions.eq("districtMaster",districtMaster);
			districtSchools 						=	districtSchoolsDAO.findByCriteria(sortOrderStrVal,criterionDSchool);
			
			
			List<DistrictSchools> sortedlstDistrictSchools		=	new ArrayList<DistrictSchools>();
			
			SortedMap<String,DistrictSchools>	sortedMap = new TreeMap<String,DistrictSchools>();
			if(sortOrderNoField.equals("schoolName"))
			{
				sortOrderFieldName	=	"schoolName";
			}
			if(sortOrderNoField.equals("addedBy"))
			{
				sortOrderFieldName	=	"addedBy";
			}
			int mapFlag=2;
			for (DistrictSchools distSchool : districtSchools){
				String orderFieldName=""+distSchool.getCreatedDateTime();
				if(sortOrderFieldName.equals("schoolName")){
					orderFieldName=distSchool.getSchoolMaster().getSchoolName()+"||"+distSchool.getDistrictSchoolId();
					sortedMap.put(orderFieldName+"||",distSchool);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("addedBy")){
					orderFieldName=distSchool.getCreatedBy().getFirstName()+" "+distSchool.getCreatedBy().getLastName()+"||"+distSchool.getDistrictSchoolId();
					sortedMap.put(orderFieldName+"||",distSchool);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstDistrictSchools.add((DistrictSchools) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstDistrictSchools.add((DistrictSchools) sortedMap.get(key));
				}
			}else{
				sortedlstDistrictSchools=districtSchools;
			}
			
			totalRecord =sortedlstDistrictSchools.size();

			if(totalRecord<end)
				end=totalRecord;
			List<DistrictSchools> lstsortedlstDistrictSchools		=	sortedlstDistrictSchools.subList(start,end);
			
			dmRecords.append("<table id='districtSchoolsTable' border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSchoolName", locale),sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblAddedOn", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='25%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblAddedBy", locale),sortOrderFieldName,"addedBy",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
			
			dmRecords.append("<th width='15%'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(districtSchools.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNoSchoolFound", locale)+"</td></tr>" );
			System.out.println(""+Utility.getLocaleValuePropByKey("msgNoOfRecords", locale)+"  "+districtSchools.size()+" lstsortedlstDistrictSchools "+lstsortedlstDistrictSchools.size());

			for (DistrictSchools districtSchoolsDetail : lstsortedlstDistrictSchools) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+districtSchoolsDetail.getSchoolMaster().getSchoolName()+"</td>");
				dmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(districtSchoolsDetail.getCreatedDateTime())+"</td>");
				dmRecords.append("<td>"+districtSchoolsDetail.getCreatedBy().getFirstName()+" "+districtSchoolsDetail.getCreatedBy().getLastName()+"</td>");
				dmRecords.append("<td>");
				if(roleAccess.indexOf("|3|")!=-1){
					dmRecords.append("<a href='javascript:void(0);' onclick='return deleteDistrictSchools("+districtSchoolsDetail.getDistrictSchoolId()+")'>Remove</a>");
				}else{
					dmRecords.append("&nbsp;");
				}
				dmRecords.append("</td>");
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForDistriceAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	
	/* @Author: Gagan 
	 * @Discription: It is used to save DistrictSchools in editdistrict Page in Account information  .
	 */
	/* ===============      save Key Contact        =========================*/
	public int saveDistrictSchools(Integer districtId,Integer schoolId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		
		DistrictMaster districtMaster			=	null;
		SchoolMaster schoolMaster				=	null;
		UserMaster userMaster					=	null;
		//DistrictNotes districtNotes				=	new	DistrictNotes();
		DistrictSchools districtSchools			= 	new DistrictSchools();
		try
		{

			
			
			districtMaster						=	districtMasterDAO.findById(districtId, false, false);
			schoolMaster						=	schoolMasterDAO.findById(Long.parseLong(""+schoolId), false, false);
			userMaster							=	usermasterdao.findById(userSession.getUserId(), false, false);
			Criterion criterion										=	Restrictions.eq("schoolMaster",schoolMaster);
			List<DistrictSchools> dupdistSchools		=	districtSchoolsDAO.findByCriteria(criterion);
			/*======= Check For Unique School in district School Table  ==========*/
			int size 									=	dupdistSchools.size();
			
			if(size>0)
				return 3;/*=== it return 3 if find dupdistSchools from  Table=======*/
			
			districtSchools.setDistrictMaster(districtMaster);
			districtSchools.setSchoolMaster(schoolMaster);
			districtSchools.setSchoolName(schoolMaster.getSchoolName());
			districtSchools.setCreatedBy(userMaster);
			districtSchools.setCreatedDateTime(new Date());
			
			districtSchoolsDAO.makePersistent(districtSchools);
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 2;
		}

		return 1;
	}
	
	
	/* @Author: Gagan 
	 * @Discription: It is used to delete DistrictSchools   .
	 */
	@Transactional(readOnly=false)
	public boolean deleteDistrictSchools(Integer districtSchoolId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try
		{
			DistrictNotes districtNotes			=	null;
			DistrictSchools districtSchools		=	null;
			//districtNotes						=	districtNotesDAO.findById(notesId, false, false);
			districtSchools						=	districtSchoolsDAO.findById(districtSchoolId, false, false);
			
			districtSchoolsDAO.makeTransient(districtSchools);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/* @Author: Gagan 
	 * @Discription: It is used to Match pasword of Decision Maker Field Editable in editdistrict Page in Contact information  .
	 */
	public DistrictMaster matchPwdOfDm(int districtId,String cnfmPwd)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
			DistrictMaster districtMaster 					=	districtMasterDAO.findById(districtId, false, false);
			if(districtMaster.getDmPassword().equals(MD5Encryption.toMD5(cnfmPwd)))
			{
				return districtMaster;
			}
			else
			{
				return null;
			}
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		//return districtMaster;
		return null;
	}
	
	/* @Author: Gagan 
	 * @Discription: It is used to get Email Format for Candidate On Import   . 
	 */
	public MailToCandidateOnImport getEmailFormat(int districtId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try{
			DistrictMaster districtMaster 		=	districtMasterDAO.findById(districtId, false, false);
			Criterion criterion1				=	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2				=	Restrictions.eq("defaultFlag", 0);
			
			List<MailToCandidateOnImport> lstMailToCandidateOnImport	=	mailToCandidateOnImportDAO.findByCriteria(criterion1);
			
			if(lstMailToCandidateOnImport.size()>0)
			{
				return lstMailToCandidateOnImport.get(0);
				
			}
			else
			{
				lstMailToCandidateOnImport	=	mailToCandidateOnImportDAO.findByCriteria(criterion2);
				MailToCandidateOnImport mailToCandidateOnImport	=	new	MailToCandidateOnImport(); 
				
				String fromAddress 			=	Utility.getLocaleValuePropByKey("msgTeacherMatchAdmin", locale); 
				String subjectLine 			=	Utility.getLocaleValuePropByKey("msgTeacherMatchEPI1", locale);
				String mailBody 			=	defaultEmailFormatOnImport(districtMaster.getDistrictName());

				mailToCandidateOnImport.setFromAddress(fromAddress);
				mailToCandidateOnImport.setSubjectLine(subjectLine);
				mailToCandidateOnImport.setMailBody(mailBody);

				return mailToCandidateOnImport;
				//return lstMailToCandidateOnImport.get(0);
			}
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		//return districtMaster;
		return null;
	}
	
	
	/* @Author: Gagan 
	 * @Discription: It is used to get Email Format for Candidate On Import   . 
	 */
	public int saveEmailFormat(int districtId,String msgFrom,String msgSubject, String msgSpt)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		try{
			DistrictMaster districtMaster 		=	districtMasterDAO.findById(districtId, false, false);
			UserMaster userMaster				=	usermasterdao.findById(userSession.getUserId(), false, false);
			
			Criterion criterion1				=	Restrictions.eq("districtMaster", districtMaster);
			
			List<MailToCandidateOnImport> lstMailToCandidateOnImport	=	mailToCandidateOnImportDAO.findByCriteria(criterion1);
			
			MailToCandidateOnImport mailToCandidateOnImport		=	new MailToCandidateOnImport();
			
			if(lstMailToCandidateOnImport.size()>0)
			{
				mailToCandidateOnImport.setMailId(lstMailToCandidateOnImport.get(0).getMailId());
			}
			
			mailToCandidateOnImport.setFromAddress(msgFrom);
			mailToCandidateOnImport.setSubjectLine(msgSubject);
			mailToCandidateOnImport.setMailBody(msgSpt);
			mailToCandidateOnImport.setDistrictMaster(districtMaster);
			mailToCandidateOnImport.setUserMaster(userMaster);
			mailToCandidateOnImport.setCreatedDateTime(new Date());			
			
			mailToCandidateOnImportDAO.makePersistent(mailToCandidateOnImport);
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		//return districtMaster;
		return 1;
	}
	
	public static String defaultEmailFormatOnImport(String districtName)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		String mailCssFontSize = "'font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'";
		String mailFooterCssFontSize = "'font-family: Century Gothic,Open Sans, sans-serif;font-size: 12px;'";
		StringBuffer sb = new StringBuffer();
			try 
			{
				sb.append("<table>");
				String loginUrl  = Utility.getBaseURL(request)+"signin.do";
				
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append(""+Utility.getLocaleValuePropByKey("lblDear", locale)+" < Teacher First Name >, <br/><br/>"+Utility.getLocaleValuePropByKey("msgDistricAjax1", locale)+" <br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append(Utility.getLocaleValuePropByKey("msgDistricAjax2", locale));
					sb.append("</td>");
					sb.append("</tr>");
					
					
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("<ul><li>"+Utility.getLocaleValuePropByKey("msgDistricAjax3", locale)+"</br></li><li>"+Utility.getLocaleValuePropByKey("msgDistricAjax4", locale)+"</br></li><li>Do not close your browser or hit the \"back\" button on your browser.</br></li><li>You are not able to skip questions.</li><li>By taking the EPI, you agree to TeacherMatch's Terms of Use.</li></ul>");
					sb.append("</td>");
					sb.append("</tr>");
					
						sb.append("<tr>");
						sb.append("<td style="+mailCssFontSize+">");
						sb.append(""+Utility.getLocaleValuePropByKey("msgDistricAjax5", locale)+"<br/><br/>");
						sb.append("</td>");
						sb.append("</tr>");
						
						
						sb.append("<tr>");
						sb.append("<td style="+mailCssFontSize+">");
						sb.append("<a href='"+loginUrl+"'>"+loginUrl+"</a><br/> "+Utility.getLocaleValuePropByKey("lblEmail", locale)+": < Teacher Email ><br/>"+Utility.getLocaleValuePropByKey("lblPass", locale)+": < Teacher Password ><br/><br/>");
						sb.append("</td>");
						sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append(""+Utility.getLocaleValuePropByKey("msgDistricAjax6", locale)+"<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append(""+Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal11", locale)+",<br/>"+Utility.getLocaleValuePropByKey("msgDistricAjax7", locale)+"<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append(""+Utility.getLocaleValuePropByKey("msgDistricAjax8", locale)+" <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("</table>");
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	/* @Author: Gagan 
	 * @Discription: It is used to saveBanchmarkStatus to districtmaxfitscore Table
	 */
	public int saveBanchmarkStatus(int districtId,String arrStatusId, String arrStatusSliderValue,String arrSecStatusId,String arrSecStatusSliderValue)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try
		{
			DistrictMaster districtMaster 		=	districtMasterDAO.findById(districtId, false, false);
			Criterion criterion2				=	Restrictions.eq("districtMaster", districtMaster);
			List<DistrictMaxFitScore> lstDistrictMaxFitScore	=	districtMaxFitScoreDAO.findByCriteria(criterion2);
			
			String statusId[]=arrStatusId.split(",");
			String statusSliderValue[]=arrStatusSliderValue.split(",");
			
			String statusSecId[]=arrSecStatusId.split(",");
			String statusSecSliderValue[]=arrSecStatusSliderValue.split(",");
			
			List statusIdList = new ArrayList();
			for(int i=0;i<statusId.length;i++)
			{
				statusIdList.add(Integer.parseInt(statusId[i]));	
			}
			
			List statusSecIdList = new ArrayList();
			if(!statusSecId[0].equals(""))
			for(int i=0;i<statusSecId.length;i++)
			{
				statusSecIdList.add(Integer.parseInt(statusSecId[i]));	
			}
			
			List<StatusMaster> lstStatusMasterByArrStatusId	=	statusMasterDAO.findStatusByStatusIdList(statusIdList);	
			/**** Session dunamic ****/
       	 	SessionFactory sessionFactory		=	districtMaxFitScoreDAO.getSessionFactory();
			StatelessSession statelesSsession 	= sessionFactory.openStatelessSession();
			Transaction txOpen =statelesSsession.beginTransaction();
			
			DistrictMaxFitScore districtMaxFitScore	=	new DistrictMaxFitScore();
			if(lstDistrictMaxFitScore.size()==0)
			{
				
				for(int i=0;i<statusId.length;i++)
				{
					//jobforteacherlistteacherIdList.add(Long.parseLong(arr[i]));	
					StatusMaster statusMaster	=	getStatusMaster(lstStatusMasterByArrStatusId,Integer.parseInt(statusId[i]));
					districtMaxFitScore.setDistrictMaster(districtMaster);
					districtMaxFitScore.setStatusMaster(statusMaster);
					districtMaxFitScore.setMaxFitScore(Double.parseDouble(statusSliderValue[i]));
					districtMaxFitScore.setStatus("A");
					districtMaxFitScore.setCreatedDateTime(new Date());
					districtMaxFitScore.setSecondaryStatus(null);
					statelesSsession.insert(districtMaxFitScore);
				}
				if(!statusSecId[0].equals("")){
					List<SecondaryStatus> lstSecStatusMasterByArrStatusId	=	secondaryStatusDAO.findSecStatusByStatusIdList(statusSecIdList);
					for(int i=0;i<statusSecId.length;i++)
					{
						SecondaryStatus secondaryStatus	=	getSecStatusMaster(lstSecStatusMasterByArrStatusId,Integer.parseInt(statusSecId[i]));
						districtMaxFitScore.setDistrictMaster(districtMaster);
						districtMaxFitScore.setStatusMaster(null);
						districtMaxFitScore.setSecondaryStatus(secondaryStatus);
						districtMaxFitScore.setMaxFitScore(Double.parseDouble(statusSecSliderValue[i]));
						districtMaxFitScore.setStatus("A");
						districtMaxFitScore.setCreatedDateTime(new Date());
						statelesSsession.insert(districtMaxFitScore);
					}
				}
			}
			else
			{
				for(int i=0;i<statusId.length;i++)
				{
					try{
						System.out.println("districtMaxFitScore statusId:::"+statusId[i]);
						districtMaxFitScore	=	getDistrictMaxFitScore(lstDistrictMaxFitScore,Integer.parseInt(statusId[i]),districtMaster);
						if(districtMaxFitScore!=null){
							System.out.println("districtMaxFitScore::::"+districtMaxFitScore.getFitScoreId());
							districtMaxFitScore.setFitScoreId(districtMaxFitScore.getFitScoreId());
							districtMaxFitScore.setMaxFitScore(Double.parseDouble(statusSliderValue[i]));
							statelesSsession.update(districtMaxFitScore);
						}else{
							System.out.println("districtMaxFitScore is null");
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				if(!statusSecId[0].equals(""))
				for(int i=0;i<statusSecId.length;i++)
				{
					districtMaxFitScore	=	getDistrictMaxSecFitScore(lstDistrictMaxFitScore,Integer.parseInt(statusSecId[i]),districtMaster);
					if(districtMaxFitScore!=null){
						districtMaxFitScore.setFitScoreId(districtMaxFitScore.getFitScoreId());
						districtMaxFitScore.setMaxFitScore(Double.parseDouble(statusSecSliderValue[i]));
						statelesSsession.update(districtMaxFitScore);
					}else
					{
						districtMaxFitScore =new DistrictMaxFitScore();
						SecondaryStatus secondaryStatus	=	secondaryStatusDAO.findById(Integer.parseInt(statusSecId[i]), false, false);
						districtMaxFitScore.setDistrictMaster(districtMaster);
						districtMaxFitScore.setStatusMaster(null);
						districtMaxFitScore.setSecondaryStatus(secondaryStatus);
						districtMaxFitScore.setMaxFitScore(Double.parseDouble(statusSecSliderValue[i]));
						districtMaxFitScore.setStatus("A");
						districtMaxFitScore.setCreatedDateTime(new Date());
						statelesSsession.insert(districtMaxFitScore);
					}
				}
			}
			txOpen.commit();
	       	statelesSsession.close();
	       	return 1;
			
		}catch (Exception e) 
		{
			e.printStackTrace();
			return 0;
		}
		//return districtMaster;
	}
	
	/*============ Gagan : get Status master =============================*/
	public StatusMaster getStatusMaster(List<StatusMaster> lstStatusMaster ,int statusId)
	{	
		StatusMaster status= null;
		try 
		{   if(lstStatusMaster!=null)
			for(StatusMaster statusMaster: lstStatusMaster){
				if(statusMaster.getStatusId().equals(statusId))
				{
					status=statusMaster;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(status==null)
			return null;
		else
			return status;
	}
	public SecondaryStatus getSecStatusMaster(List<SecondaryStatus> lstSecondaryStatus ,int secStatusId)
	{	
		SecondaryStatus status= null;
		try 
		{   if(lstSecondaryStatus!=null)
			for(SecondaryStatus secondaryStatus: lstSecondaryStatus){
				if(secondaryStatus.getSecondaryStatusId().equals(secStatusId))
				{
					status=secondaryStatus;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(status==null)
			return null;
		else
			return status;
	}
	/*============ Gagan : get Status master =============================*/
	public DistrictMaxFitScore getDistrictMaxFitScore(List<DistrictMaxFitScore> lstDistrictMaxFitScore ,int statusId, DistrictMaster districtMaster)
	{	
		
		DistrictMaxFitScore districtMaxFitScore= null;	
		try 
		{   if(lstDistrictMaxFitScore!=null)
			for(DistrictMaxFitScore dmfs: lstDistrictMaxFitScore){
				if(dmfs.getStatusMaster()!=null){
					if(dmfs.getStatusMaster().getStatusId().equals(statusId))
						districtMaxFitScore=dmfs;
				}
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(districtMaxFitScore==null){
			return null;
		}	
		else
			return districtMaxFitScore;
	}
	
	public DistrictMaxFitScore getDistrictMaxSecFitScore(List<DistrictMaxFitScore> lstDistrictMaxFitScore ,int statusSecId, DistrictMaster districtMaster)
	{	
		
		DistrictMaxFitScore districtMaxFitScore= null;	
		try 
		{   if(lstDistrictMaxFitScore!=null)
			for(DistrictMaxFitScore dmfs: lstDistrictMaxFitScore){
				if(dmfs.getSecondaryStatus()!=null){
					if(dmfs.getSecondaryStatus().getSecondaryStatusId().equals(statusSecId))
						districtMaxFitScore=dmfs;
				}
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(districtMaxFitScore==null){
			return null;
		}	
		else
			return districtMaxFitScore;
	}
	public boolean saveDistrictPrevilege(Integer districtId,int tmDefaultValue,int tmdisplaycommunication,int dpointValue,String accessDPoints,Integer qqSetId){
		try{
			System.out.println("tmDefaultValue ========>>>> "+tmDefaultValue);
			System.out.println("dpointValue >>>>    "+ dpointValue);
			System.out.println("accessDPoints >>>>   "+ accessDPoints);
			System.out.println("qqSetId : "+qqSetId);
			
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			if(tmDefaultValue==1)
				districtMaster.setDisplayTMDefaultJobCategory(false);
			else
				districtMaster.setDisplayTMDefaultJobCategory(true);
			
			if(tmdisplaycommunication==0)
				districtMaster.setDisplayCommunication(true);
			else
				districtMaster.setDisplayCommunication(false);
			
			if(dpointValue==1)
				districtMaster.setSetAssociatedStatusToSetDPoints(true);
			else
				districtMaster.setSetAssociatedStatusToSetDPoints(false);
			
			if(dpointValue==1){
				districtMaster.setAccessDPoints(accessDPoints);
			}else{
				districtMaster.setAccessDPoints(null);
			}
			
			districtMasterDAO.makePersistent(districtMaster);
		}catch(Exception exception){
			exception.printStackTrace();
		}
		return false;
	}
	
	public void saveQuestionInstruction(String Qins,Integer districtId)
	{
		try{
		DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
		if(districtMaster!=null){
			districtMaster.setTextForDistrictSpecificQuestions(Qins);
		}
		districtMasterDAO.makePersistent(districtMaster);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getQuestionInstruction(Integer districtId)
	{
		try{
		DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
		if(districtMaster!=null){
			return districtMaster.getTextForDistrictSpecificQuestions();
		}else{
			return null;
		}
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public boolean deleteQuestionInstruction(Integer districtId)
	{
		try{
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			if(districtMaster!=null){
				districtMaster.setTextForDistrictSpecificQuestions(null);
				districtMasterDAO.makePersistent(districtMaster);
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public String displayDistAttachmentGrid(int headQuarterId,int branchId,int districtId,Long schoolId,String docName,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		// System.out.println("dISPLAYING DOCUMENTS RECORDS");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer dmRecords =	new StringBuffer();
		System.out.println(" districtId :: "+districtId);
		System.out.println(" headQuarterId :: "+headQuarterId);
		System.out.println(" branchId :: "+branchId);
		try{
			//-- get no of record in grid,
			//-- set start and end position
			System.out.println("----"+noOfRow);
			if(noOfRow==null || noOfRow.equals(""))
			{
				noOfRow="10";
			}
			System.out.println("----"+noOfRow);	
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
		
			int roleId=0;
			int entityid=0;
			HeadQuarterMaster headQuarterMaster = null;
			BranchMaster branchMaster = null;
			DistrictMaster districtMaster = null;
			SchoolMaster schoolMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				entityid=userMaster.getEntityType();
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				
				if(userMaster.getSchoolId()!=null && userMaster.getSchoolId().getSchoolId()!=null && userMaster.getRoleId().getRoleId().equals(3)){
					schoolMaster = userMaster.getSchoolId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,10,"editdistrict.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			String sortOrderFieldName	=	"districtId";
			String sortOrderNoField		=	"districtId";
			Order  sortOrderStrVal		=	null;
			
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) ){
					 sortOrderFieldName	=	sortOrder;
					 sortOrderNoField	=	sortOrder;
				 }
				 if(sortOrder.equals("districtAttachment")) {
					 sortOrderNoField="districtAttachment";
				 }
				 
				 if(sortOrder.equals("districtId")) {
					 sortOrderNoField="districtId";
				 }
				 
				 if(sortOrder.equals("schoolId")) {
					 sortOrderNoField="schoolId";
				 }

				 if(sortOrder.equals("createdBy")) {
					 sortOrderNoField="createdBy";
				 }
			}
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			} else {
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			
			/*System.out.println("sortOrderTypeVal::::::::"+sortOrderTypeVal);
			System.out.println("sortOrderFieldName::::::::"+sortOrderFieldName);*/
			try {
				if(headQuarterId!=0 && branchId==0)
					headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false,false);
				else if(headQuarterId!=0 && branchId!=0){
					branchMaster = branchMasterDAO.findById(branchId, false, false);
					headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false,false);
				}
				else if(districtId!=0)
					districtMaster = districtMasterDAO.findById(districtId, false, false);
				else if(schoolId!=0){
					schoolMaster = schoolMasterDAO.findById(schoolId, false, false);
				}
			}
			catch (Exception e) {}

			List<DistrictAttachment> districtAttachment	 =	null;
			Criterion criterionDAttach  = null;
			Criterion criterionSAttach 	= null;
			Criterion criteriondAttachent = null;
			Criterion criteriondAttachment = null;
			//Integer districtAttachmentCount = 0;
			if(entityid==1) {
				if(districtMaster!=null && schoolMaster!=null && docName!="") {
					criterionDAttach 		= 	Restrictions.eq("districtMaster",districtMaster);
					criterionSAttach 		= 	Restrictions.eq("schoolMaster",schoolMaster);
					Criterion criterionDAttachment 	= 	Restrictions.like("districtAttachment",docName.trim(),MatchMode.ANYWHERE);
					districtAttachment				=	districtAttachmentDAO.findByCriteria(criterionDAttach,criterionSAttach,criterionDAttachment);

				} else if(districtMaster!=null && schoolMaster!=null) {
					criterionDAttach 		= 	Restrictions.eq("districtMaster",districtMaster);			
					criterionSAttach 		= 	Restrictions.eq("schoolMaster",schoolMaster);
					districtAttachment				=	districtAttachmentDAO.findByCriteria(criterionDAttach,criterionSAttach);

				} else if(districtMaster!=null && docName!="") {
					criterionDAttach 		= 	Restrictions.eq("districtMaster",districtMaster);
					criteriondAttachment 	= 	Restrictions.like("districtAttachment",docName.trim(),MatchMode.ANYWHERE);
					districtAttachment				=	districtAttachmentDAO.findByCriteria(criterionDAttach,criteriondAttachment);

				} else if(districtMaster!=null) {
					criterionDAttach 		= 	Restrictions.eq("districtMaster",districtMaster);
					districtAttachment				=	districtAttachmentDAO.findByCriteria(criterionDAttach);

				} else if(districtMaster==null && schoolMaster==null && docName!="") {
					criteriondAttachment 		= 	Restrictions.like("districtAttachment",docName.trim(),MatchMode.ANYWHERE);
					districtAttachment					=	districtAttachmentDAO.findByCriteria(criteriondAttachment);
				} else {
					districtAttachment	=	districtAttachmentDAO.findAll();
				}
			} else if(entityid==2) {
				if(schoolMaster!=null && docName!="") {
					criterionDAttach 		= 	Restrictions.eq("districtMaster",districtMaster);
					criterionSAttach 		= 	Restrictions.eq("schoolMaster",schoolMaster);
					criteriondAttachent 	= 	Restrictions.like("districtAttachment",docName.trim(),MatchMode.ANYWHERE);
					districtAttachment				=	districtAttachmentDAO.findByCriteria(criterionDAttach,criterionSAttach,criteriondAttachent);

				} else if(schoolMaster!=null && docName=="") {
					criterionDAttach 		= 	Restrictions.eq("districtMaster",districtMaster);
					criterionSAttach 		= 	Restrictions.eq("schoolMaster",schoolMaster);
					districtAttachment				=	districtAttachmentDAO.findByCriteria(criterionDAttach,criterionSAttach);

				} else if(schoolMaster==null && docName!="") {
					criterionDAttach 		= 	Restrictions.eq("districtMaster",districtMaster);
					criteriondAttachent 	= 	Restrictions.like("districtAttachment",docName.trim(),MatchMode.ANYWHERE);
					districtAttachment				=	districtAttachmentDAO.findByCriteria(criterionDAttach,criteriondAttachent);

				} else {
					criterionDAttach 		= 	Restrictions.eq("districtMaster",districtMaster);
					districtAttachment				=	districtAttachmentDAO.findByCriteria(criterionDAttach);

				}

			} else if(entityid==3) {
				System.out.println("::::::::::::::::::::::::::::::::"+districtMaster.getDistrictId());
				criterionDAttach 		= 	Restrictions.eq("districtMaster",districtMaster);
				criterionSAttach 		= 	Restrictions.eq("schoolMaster",schoolMaster);

				if(docName!=null) {
					criteriondAttachent 	= 	Restrictions.like("districtAttachment",docName.trim(),MatchMode.ANYWHERE);
					districtAttachment				=	districtAttachmentDAO.findByCriteria(criterionDAttach,criterionSAttach,criteriondAttachent);

				} else {
					districtAttachment	=	districtAttachmentDAO.findByCriteria(criterionDAttach,criterionSAttach);
				}
			}
			else if(entityid==5) {
				System.out.println(":::::::::: Headquarter :::::::::::");
					criterionDAttach 		= 	Restrictions.eq("headQuarterMaster",headQuarterMaster);
					if(branchId!=0)
						criterionSAttach 		= 	Restrictions.eq("branchMaster",branchMaster);
				
				if(docName!=null && docName.length()>0) {
					criteriondAttachent 	= 	Restrictions.like("districtAttachment",docName.trim(),MatchMode.ANYWHERE);
					if(branchId!=0 && docName!=null)
						districtAttachment	=	districtAttachmentDAO.findByCriteria(criterionDAttach,criterionSAttach,criteriondAttachent);
					else
						districtAttachment	=	districtAttachmentDAO.findByCriteria(criterionDAttach,criteriondAttachent);
				}
				else{	
					if(branchId!=0)
						districtAttachment	=	districtAttachmentDAO.findByCriteria(criterionDAttach,criterionSAttach);
					else
						districtAttachment	=	districtAttachmentDAO.findByCriteria(criterionDAttach);
				}

				
				/*if(docName!=null) {
					criteriondAttachent 	= 	Restrictions.like("districtAttachment",docName.trim(),MatchMode.ANYWHERE);
					districtAttachment				=	districtAttachmentDAO.findByCriteria(criterionDAttach,criteriondAttachent);

				} else {
					districtAttachment	=	districtAttachmentDAO.findByCriteria(criterionDAttach,criterionSAttach);
				}*/
			}
			else if(entityid==6) {
				System.out.println(":::::::::::::::: Branch ::::::::::::::::");
				//criterionDAttach 		= 	Restrictions.eq("headQuarterMaster",headQuarterMaster);
				criterionSAttach 		= 	Restrictions.eq("branchMaster",branchMaster);
				if(docName!=null) {
					criteriondAttachent 	= 	Restrictions.like("districtAttachment",docName.trim(),MatchMode.ANYWHERE);
					districtAttachment				=	districtAttachmentDAO.findByCriteria(criterionSAttach,criteriondAttachent);

				} else {
					districtAttachment	=	districtAttachmentDAO.findByCriteria(criterionSAttach);
				}
			}
			
			List<DistrictAttachment> sortedlstdistrictAttach  =	 new ArrayList<DistrictAttachment>();

			SortedMap<String,DistrictAttachment>	sortedMap = new TreeMap<String,DistrictAttachment>();
			if(sortOrderNoField.equals("districtAttachment")) {
				sortOrderFieldName	=	"districtAttachment";
			}
			if(sortOrderNoField.equals("districtId")) {
				sortOrderFieldName	=	"districtId";
			}

			if(sortOrderNoField.equals("schoolId")) {
				sortOrderFieldName	=	"schoolId";
			}

			if(sortOrderNoField.equals("createdBy")) {
				sortOrderFieldName	=	"createdBy";
			}

			int mapFlag=2;
			String orderFieldName = "";
			for (DistrictAttachment distAttach : districtAttachment){
				if(distAttach.getDistrictMaster()!=null){
				orderFieldName=""+distAttach.getDistrictMaster().getDistrictId();
				if(sortOrderFieldName.equals("districtId")){
					orderFieldName=distAttach.getDistrictMaster().getDistrictId()+" "+distAttach.getCreatedBy().getLastName()+"||"+distAttach.getDistrictattachmentId();
					sortedMap.put(orderFieldName+"||",distAttach);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					} else {
						mapFlag=1;
					}
				}
				}

				if(distAttach.getSchoolMaster() != null){
					orderFieldName=""+distAttach.getSchoolMaster().getSchoolId();
					if(sortOrderFieldName.equals("schoolId")){
						System.out.println(":::::::::: schoolId ::::::::::::");
						orderFieldName=distAttach.getSchoolMaster().getSchoolId()+" "+distAttach.getCreatedBy().getLastName()+"||"+distAttach.getDistrictattachmentId();
						sortedMap.put(orderFieldName+"||",distAttach);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						} else {
							mapFlag=1;
						}
					}
				} else if(distAttach.getSchoolMaster() == null){
					if(sortOrderFieldName.equals("schoolId")){
						System.out.println(":::::::::: schoolId222 ::::::::::::");
						orderFieldName="0||"+distAttach.getDistrictattachmentId();
						sortedMap.put(orderFieldName+"||",distAttach);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}
				orderFieldName=""+distAttach.getDistrictAttachment().replaceAll("\\<[^>]*>","");
				if(sortOrderFieldName.equals("districtAttachment")){
					System.out.println(":::::::::: districtAttachment ::::::::::::");
					orderFieldName=distAttach.getDistrictAttachment().replaceAll("\\<[^>]*>","").toUpperCase()+"||"+distAttach.getDistrictattachmentId();
					sortedMap.put(orderFieldName+"||",distAttach);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}

				if(sortOrderFieldName.equals("createdBy")){
					System.out.println(":::::::::: createdBy ::::::::::::");
					orderFieldName=distAttach.getCreatedBy().getFirstName()+" "+distAttach.getCreatedBy().getLastName()+"||"+distAttach.getDistrictattachmentId();
					sortedMap.put(orderFieldName+"||",distAttach);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					} else {
						mapFlag=1;
					}
				}
			}
			if(mapFlag==1) {
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet(); 
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstdistrictAttach.add((DistrictAttachment) sortedMap.get(key));
				} 
			} else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstdistrictAttach.add((DistrictAttachment) sortedMap.get(key));
				}
			} else {
				sortedlstdistrictAttach=districtAttachment;
			}

			totalRecord =sortedlstdistrictAttach.size();

			if(totalRecord<end)
				end=totalRecord;
			List<DistrictAttachment> lstsortedlstdistrictAttach		=	sortedlstdistrictAttach.subList(start,end);
			dmRecords.append("<table id='distAttachmentTable' border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			if(entityid==5 || entityid==6)
				responseText=PaginationAndSorting.responseSortingLink(optHeadQuarter,sortOrderFieldName,"headQuarterId",sortOrderTypeVal,pgNo);
			else
				responseText=PaginationAndSorting.responseSortingLink(optDistrict,sortOrderFieldName,"districtId",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			if(entityid!=5 && entityid!=6){
			responseText=PaginationAndSorting.responseSortingLink(optSchool,sortOrderFieldName,"schoolId",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='25%' valign='top'>"+responseText+"</th>");
			}
			else{
				responseText=PaginationAndSorting.responseSortingLink(optBranch,sortOrderFieldName,"branchId",sortOrderTypeVal,pgNo);
				dmRecords.append("<th width='25%' valign='top'>"+responseText+"</th>");
			}
			if(Utility.getLocaleValuePropByKey("kellyupdatecodelive", locale).equals("yes") || (entityid!=5 && entityid!=6))
			{
				responseText=PaginationAndSorting.responseSortingLink(lbllDocName, sortOrderFieldName, "documentName", sortOrderTypeVal, pgNo);
				dmRecords.append("<th width='25%' valign='top'>"+responseText+"</th>");	
				
			}
			responseText=PaginationAndSorting.responseSortingLink(hdAttachment,sortOrderFieldName,"districtAttachment",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblCreatedBy,sortOrderFieldName,"createdBy",sortOrderTypeVal,pgNo);

			dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			dmRecords.append("<th >"+lblAct+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(districtAttachment.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+msgNoNotefound1+"</td></tr>" );
			
			String distname ="";
			for (DistrictAttachment districtAttachDetail : lstsortedlstdistrictAttach) {
				String attachmentname=districtAttachDetail.getDistrictAttachment();
				if(districtAttachDetail.getDistrictMaster()!=null)
				distname = districtAttachDetail.getDistrictMaster().getDistrictName();
				else if(districtAttachDetail.getHeadQuarterMaster()!=null)
					distname = districtAttachDetail.getHeadQuarterMaster().getHeadQuarterName();
		        int attachmentid=districtAttachDetail.getDistrictattachmentId();

		        dmRecords.append("<tr>");

				dmRecords.append("<td>"+distname+"</td>");
				
				String schoolname="";
				if(entityid!=5 && entityid!=6){
					try{
						if(!districtAttachDetail.getSchoolMaster().equals(null)) {
							schoolname	=	districtAttachDetail.getSchoolMaster().getSchoolName();
							schoolId	=	districtAttachDetail.getSchoolMaster().getSchoolId();
						}
					}catch (Exception ew) {
						schoolname="";
					}
				}
				else
				{
					try {
						if(districtAttachDetail.getHeadQuarterMaster()!=null && districtAttachDetail.getBranchMaster()!=null)
							schoolname = districtAttachDetail.getBranchMaster().getBranchName();
					} catch (Exception e) {
						schoolname="";
					}
				}
				dmRecords.append("<td>"+schoolname+"</td>");
				String documentName="";
				if(Utility.getLocaleValuePropByKey("kellyupdatecodelive", locale).equals("yes") || (entityid!=5 && entityid!=6))
				{
					if(districtAttachDetail.getDocumentName()!=null && districtAttachDetail.getDocumentName()!="")
						documentName = districtAttachDetail.getDocumentName();
					dmRecords.append("<td>"+documentName+"</td>");
				}				
				dmRecords.append("<td>");
				if(Utility.getLocaleValuePropByKey("kellyupdatecodelive", locale).equals("yes") || (entityid!=5 && entityid!=6))
				{
					dmRecords.append("<a href='javascript:void(0);' onclick='return vieDistrictFile("+attachmentid+")' style='padding-left: 25px;'><i class='icon-download-alt icon-large iconcolorBlue'></i></a>");
				}
				else
				{
					dmRecords.append("<a href='javascript:void(0);' onclick='return vieDistrictFile("+attachmentid+")'>"+attachmentname+"</a>");
				}
				dmRecords.append("</td>");
				
				String createdby=districtAttachDetail.getCreatedBy().getFirstName()+" "+districtAttachDetail.getCreatedBy().getLastName();
				dmRecords.append("<td>"+createdby+"</td>");
				
				dmRecords.append("<td>");
				int entity=districtAttachDetail.getCreatedBy().getEntityType();
				if(roleId!=12 && roleId !=13){
				dmRecords.append("<a href='javascript:void(0);' title='Edit' onclick='return editDistrictAttachment("+attachmentid+","+entity+")'> <i class='fa fa-pencil-square-o fa-lg'></i></a>");

				dmRecords.append("|");
				dmRecords.append("<a href='javascript:void(0);' title='Delete' onclick='return deleteDistrictAttachment("+districtAttachDetail.getDistrictattachmentId()+")'><i class='fa fa-trash-o' fa-lg></i></a>");
				dmRecords.append("|");
				String status="";
				if(districtAttachDetail.getStatus().equals("A")) {
					status=lblDeactivate;
					dmRecords.append("<a href='javascript:void(0);' title='Deactivate' onclick='return upDateStatus("+districtAttachDetail.getDistrictattachmentId()+")'><i class='fa fa-times fa-lg'></i></a>");
				}
				if(districtAttachDetail.getStatus().equals("I")) {
					status=lblActivate;
					dmRecords.append("<a href='javascript:void(0);' title='Activate' onclick='return upDateStatus("+districtAttachDetail.getDistrictattachmentId()+")'><i class='fa fa-check fa-lg'></i></a>");
				}
				//dmRecords.append("<a href='javascript:void(0);' title='Update' onclick='return upDateStatus("+districtAttachDetail.getDistrictattachmentId()+")'><i class='fa fa-times fa-lg'></i></a>");
				}
				dmRecords.append("</td>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForDistriceAjax(request,totalRecord,noOfRow, pageNo));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dmRecords.toString();
	}

	/* ========  Search Distt Attachments =========*/	
	
	public String searchDistrictAttachment(int districtId,Long schoolIds,String docname,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		
		try{
			//-- get no of record in grid,
			//-- set start and end position
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			
			int roleId=0;
			SchoolMaster schoolMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				UserMaster	userMaster=	(UserMaster) session.getAttribute("userMaster");
				
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
					System.out.println(roleId);
				}
				
				if(userMaster.getSchoolId()!=null && userMaster.getSchoolId().getSchoolId()!=null && userMaster.getRoleId().getRoleId().equals(3)){
					schoolMaster = userMaster.getSchoolId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,10,"editdistrict.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			SortedMap map = new TreeMap();
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"districtAttachment";
			String sortOrderNoField		=	"districtAttachment";
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				
				
		
				
				
				
				
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("districtAttachment") && !sortOrder.equals("addedBy")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("districtAttachment"))
				 {
					 sortOrderNoField="districtAttachment";
				 }
				 if(sortOrder.equals("addedBy"))
				 {
					 sortOrderNoField="addedBy";
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			
			DistrictMaster districtMaster 	=	districtMasterDAO.findById(districtId, false, false);
			
			List<DistrictAttachment> districtAttachment	 =	null;
			System.out.println("-------Searching Documents------");
			if(String.valueOf(districtId)==null)
			{
				System.out.println("Distt id is"+districtId);
				districtAttachment	=	districtAttachmentDAO.findAll();
			}	
			else
			{
				System.out.println("Distt id is"+districtId);
				Criterion criterionDAttach 		= 	Restrictions.eq("districtMaster",districtMaster);
				
				if(String.valueOf(schoolIds)!=null){
				System.out.println("sccool id is"+schoolIds);
					Criterion criterionSAttach 		= 	Restrictions.eq("schoolMaster",schoolMaster);
					districtAttachment	=	districtAttachmentDAO.findByCriteria(sortOrderStrVal,criterionDAttach,criterionSAttach);
				}else{
					System.out.println("xxxxxxxx");
					districtAttachment	=	districtAttachmentDAO.findByCriteria(sortOrderStrVal,criterionDAttach);
				}
				
			
			
			}
			List<DistrictAttachment> sortedlstdistrictAttach  =	 new ArrayList<DistrictAttachment>();
			
			SortedMap<String,DistrictAttachment>	sortedMap = new TreeMap<String,DistrictAttachment>();
			if(sortOrderNoField.equals("districtAttachment"))
			{
				sortOrderFieldName	=	"districtAttachment";
			}
			if(sortOrderNoField.equals("addedBy"))
			{
				sortOrderFieldName	=	"addedBy";
			}
			int mapFlag=2;
			for (DistrictAttachment distAttach : districtAttachment){
				String orderFieldName=""+distAttach.getDistrictAttachment().replaceAll("\\<[^>]*>","");
				if(sortOrderFieldName.equals("districtAttachment")){
					orderFieldName=distAttach.getDistrictAttachment().replaceAll("\\<[^>]*>","").toUpperCase()+"||"+distAttach.getDistrictattachmentId();
					sortedMap.put(orderFieldName+"||",distAttach);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("addedBy")){
					orderFieldName=distAttach.getCreatedBy().getFirstName()+" "+distAttach.getCreatedBy().getLastName()+"||"+distAttach.getDistrictattachmentId();
					sortedMap.put(orderFieldName+"||",distAttach);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet(); 
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstdistrictAttach.add((DistrictAttachment) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstdistrictAttach.add((DistrictAttachment) sortedMap.get(key));
				}
			}else{
				sortedlstdistrictAttach=districtAttachment;
			}

			totalRecord =sortedlstdistrictAttach.size();

			if(totalRecord<end)
				end=totalRecord;
			//List<DistrictAttachment> lstsortedlstdistrictNotes	=	sortedlstdistrictAttach.subList(start,end);
			List<DistrictAttachment> lstsortedlstdistrictAttach		=	sortedlstdistrictAttach.subList(start,end);
			
			dmRecords.append("<table id='distAttachmentTable' border='0' class='table table-bordered table-striped' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("District",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='40%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("School",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='25%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Attachment",sortOrderFieldName,"addedBy",sortOrderTypeVal,pgNo);
		
			dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink("Created BY",sortOrderFieldName,"createddBy",sortOrderTypeVal,pgNo);
			
			dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			
			
			dmRecords.append("<th >"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(districtAttachment.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoNotes", locale)+"</td></tr>" );
			System.out.println("No of Records "+districtAttachment.size());

			for (DistrictAttachment districtAttachDetail : lstsortedlstdistrictAttach) 
			{
				String attachmentname=districtAttachDetail.getDistrictAttachment();
				String distname=districtAttachDetail.getDistrictMaster().getDistrictName();
				int distid=districtAttachDetail.getDistrictMaster().getDistrictId();
		        int attachmentid=districtAttachDetail.getDistrictattachmentId();
				
				
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+distname+"</td>");
				
				Long schoolId=0l;
				String schoolname="";
				try{
				System.out.println("id is"+districtAttachDetail.getSchoolMaster());
				if(!districtAttachDetail.getSchoolMaster().equals(null))
				{
					
					System.out.println("idddds");
					schoolname=districtAttachDetail.getSchoolMaster().getSchoolName();
				schoolId=districtAttachDetail.getSchoolMaster().getSchoolId();
				}
				}catch (Exception ew) {
					schoolname="";
				}
				
				dmRecords.append("<td>"+schoolname+"</td>");
				dmRecords.append("<td>");
				dmRecords.append("<a href='javascript:void(0);' onclick='return vieDistrictFile("+attachmentid+")'>"+attachmentname+"</a>");
				dmRecords.append("</td>");
				String createdby=districtAttachDetail.getCreatedBy().getFirstName()+" "+districtAttachDetail.getCreatedBy().getLastName();
				dmRecords.append("<td>"+createdby+"</td>");
				
				dmRecords.append("<td>");
				int entity=districtAttachDetail.getCreatedBy().getEntityType();
				
				
				dmRecords.append("<a href='javascript:void(0);' title='Edit' onclick='return editDistrictAttachment("+attachmentid+","+entity+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>");
				
				dmRecords.append("|");
				dmRecords.append("<a href='javascript:void(0);' title='Delete' onclick='return deleteDistrictAttachment("+districtAttachDetail.getDistrictattachmentId()+")'> <i class='fa fa-trash-o' fa-lg></i></a>");
				dmRecords.append("|");
				String status="";
				if(districtAttachDetail.getStatus().equals("A"))
				{
					status="Deactivate";
					dmRecords.append("<a href='javascript:void(0);' title='Deactivate' onclick='return upDateStatus("+districtAttachDetail.getDistrictattachmentId()+")'><i class='fa fa-times fa-lg'></i></a>");
				}
				if(districtAttachDetail.getStatus().equals("I"))
				{
					status="Activate";
					dmRecords.append("<a href='javascript:void(0);' title='Activate' onclick='return upDateStatus("+districtAttachDetail.getDistrictattachmentId()+")'><i class='fa fa-check fa-lg'></i></a>");
				}
				
				
				//dmRecords.append("<a href='javascript:void(0);' onclick='return upDateStatus("+districtAttachDetail.getDistrictattachmentId()+")'>"+status+"</a>");
				
				
				dmRecords.append("</td>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	
	
	
	
	
	
	
	
	/* ========  Search Distt Attachments =========*/
	
	
	
	
	
	
	public int saveDistrictAttachment(int headQuarterId,int branchId,String docName,Integer districtId,String fileName,Long schoolId)
	{
		System.out.println(districtId+"::::::::::::"+fileName);
		
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		
		HeadQuarterMaster headQuarterMaster = null;
		BranchMaster branchMaster = null;
		DistrictMaster districtMaster	=	null;
		SchoolMaster schoolMaster       =   null;
		DistrictAttachment districtAttachment	=	new	DistrictAttachment();
		
		try{
			System.out.println("schoolId   "+schoolId+"    districtId  "+districtId);
			if(headQuarterId!=0)
				headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
			if(branchId!=0)
				branchMaster = branchMasterDAO.findById(branchId, false, false);
			if(districtId!=null && districtId!=0)
				districtMaster	=	districtMasterDAO.findById(districtId, false, false);
			if(schoolId!=null && !schoolId.equals("")){
				schoolMaster=	schoolMasterDAO.findById(schoolId,false,false);
				districtAttachment.setSchoolMaster(schoolMaster);
			}
			districtAttachment.setHeadQuarterMaster(headQuarterMaster);
			districtAttachment.setBranchMaster(branchMaster);
			districtAttachment.setDistrictMaster(districtMaster);
			districtAttachment.setDocumentName(docName);
			districtAttachment.setCreatedBy(userSession);
			districtAttachment.setStatus("A");
			districtAttachment.setDistrictAttachment(fileName);
			districtAttachment.setCreatedDateTime(new Date());
			districtAttachmentDAO.makePersistent(districtAttachment);
		} catch(Exception e){
			e.printStackTrace();
			return 2;
		}
		return 1;
	}
	
	/*
	 * 	Delete District Attachment By Hanzala Subhani
	 * */
	
	@Transactional(readOnly=false)
	public boolean deleteDistrictAttachment(Integer districtattachmentId)
	{
		System.out.println("::::::::::::::::"+districtattachmentId);
		
		//File path = new File(Utility.getValueOfPropByKey("districtRootPath")+districtId+"/DistAttachmentFile");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		Integer id = 0;
		File file = null;
		try
		{
			DistrictAttachment districtAttachment	=	null;
			districtAttachment	=	districtAttachmentDAO.findById(districtattachmentId, false, false);
			String fileName		=	districtAttachment.getDistrictAttachment();
			if(districtAttachment.getDistrictMaster()!=null){
				id		=	districtAttachment.getDistrictMaster().getDistrictId();
				file = new File(Utility.getValueOfPropByKey("districtRootPath")+id+"/DistAttachmentFile/"+fileName);
			}
			else if(districtAttachment.getHeadQuarterMaster()!=null && districtAttachment.getBranchMaster()==null){
				id		=	districtAttachment.getHeadQuarterMaster().getHeadQuarterId();
				file = new File(Utility.getValueOfPropByKey("headQuarterRootPath")+id+"/HQAttachmentFile/"+fileName);
			}
			else if(districtAttachment.getBranchMaster()!=null){
				id = districtAttachment.getBranchMaster().getBranchId();
				file = new File(Utility.getValueOfPropByKey("branchRootPath")+id+"/BranchAttachmentFile/"+fileName);
			}

			if(file.exists()){
				if(file.delete()){
					System.out.println(file.getName()+ " deleted");
					districtAttachmentDAO.makeTransient(districtAttachment);
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public int editDistrictAttachment(String headQuarterId,String branchId,String districtId,String schoolIds,String districtattachmentId,String fileName)
	{
		System.out.println(districtId+":::::	editDistrictAttachment	:::::::"+fileName);
		
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		DistrictMaster dr1 = null;
		HeadQuarterMaster headQuarterMaster = null;
		BranchMaster branchMaster = null;
		File file = null;
		int hqbrFlag = 0;
		try{ System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		
			DistrictAttachment districtAttachmentList = districtAttachmentDAO.findById(Integer.parseInt(districtattachmentId), false, false);
			String fileNameDel	  =	districtAttachmentList.getDistrictAttachment();
			if(districtAttachmentList.getDistrictMaster()!=null)
				dr1	= districtAttachmentList.getDistrictMaster();
			else if(districtAttachmentList.getHeadQuarterMaster()!=null && districtAttachmentList.getBranchMaster()==null)
			{
				headQuarterMaster = districtAttachmentList.getHeadQuarterMaster();
				file = new File(Utility.getValueOfPropByKey("headQuarterRootPath")+headQuarterMaster.getHeadQuarterId()+"/HQAttachmentFile/"+fileNameDel);
			}
			else if(districtAttachmentList.getBranchMaster()!=null){
				branchMaster = districtAttachmentList.getBranchMaster();
				file = new File(Utility.getValueOfPropByKey("branchRootPath")+branchMaster.getBranchId()+"/BranchAttachmentFile/"+fileNameDel);
			}
			if(districtId!=null && districtId.length()>0){
				DistrictMaster dr=districtMasterDAO.findById(Integer.parseInt(districtId), false,false);
				districtAttachmentList.setDistrictMaster(dr);
				System.out.println(dr.getDistrictName()+"---------------------"+dr1.getDistrictName());
				file = new File(Utility.getValueOfPropByKey("districtRootPath")+dr1.getDistrictId()+"/DistAttachmentFile/"+fileNameDel);
			}
			else if(headQuarterId!=null && headQuarterId.length()>0 && branchId.length()==0){
				HeadQuarterMaster hqMaster = headQuarterMasterDAO.findById(Integer.parseInt(headQuarterId), false, false);
				districtAttachmentList.setHeadQuarterMaster(hqMaster);
				districtAttachmentList.setBranchMaster(null);
			}
			else if(branchId!=null && branchId.length()>0){
				BranchMaster bMaster = branchMasterDAO.findById(Integer.parseInt(branchId), false, false);
				districtAttachmentList.setBranchMaster(bMaster);
			}
			 
			if(schoolIds.length()>0)
			{
			SchoolMaster sr=schoolMasterDAO.findById(Long.parseLong(schoolIds),false,false);	
			districtAttachmentList.setSchoolMaster(sr);
			
			}else
			{
				districtAttachmentList.setSchoolMaster(null);
			}
			
			districtAttachmentList.setDistrictAttachment(fileName);
			System.out.println(file.getName()+"-------"+fileName);
			
			if(file.exists() && !file.getName().equals(fileName)){
				if(file.delete()){
					System.out.println(file.getName()+ " deleted");
					districtAttachmentDAO.makePersistent(districtAttachmentList);
				}
			}
			else{
				districtAttachmentDAO.makePersistent(districtAttachmentList);
			}
			
		} catch(Exception e){
			e.printStackTrace();
			return 2;
		}
		return 1;
	}
	
	public String vieDistrictFile(String districtattachmentId) {
		  System.out.println("::::::::::  vieDistrictFile  ::::::::::");
		  String filePath = "";
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) {
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			String fileName	= "";
			String path = "";
			Integer id = 0;
			try {
				
				DistrictAttachment districtAttachment	=	null;
				if(districtattachmentId!=null && districtattachmentId.length()>0){
					districtAttachment		=	districtAttachmentDAO.findById(Integer.parseInt(districtattachmentId), false, false);
				
					fileName		=	districtAttachment.getDistrictAttachment();
				}
				String source="";
				String target="";
				if(districtAttachment!=null){
				 
				 if(districtAttachment.getHeadQuarterMaster()!=null && districtAttachment.getBranchMaster()==null){
						id=districtAttachment.getHeadQuarterMaster().getHeadQuarterId(); 
						filePath = id+"/HQAttachmentFile";
						source 	= 	Utility.getValueOfPropByKey("headQuarterRootPath")+filePath+"/"+fileName;
						 target 	= 	context.getServletContext().getRealPath("/")+"/headquarter/"+filePath+"/";
						 path = Utility.getValueOfPropByKey("contextBasePath")+"/headquarter/"+filePath+"/"+fileName;
					}
					else if(districtAttachment.getBranchMaster()!=null){
						id=districtAttachment.getBranchMaster().getBranchId(); 
						filePath = id+"/BranchAttachmentFile";
						source 	= 	Utility.getValueOfPropByKey("branchRootPath")+filePath+"/"+fileName;
						 target 	= 	context.getServletContext().getRealPath("/")+"/branch/"+filePath+"/";
						 path = Utility.getValueOfPropByKey("contextBasePath")+"/branch/"+filePath+"/"+fileName;
					}
					else if(districtAttachment.getDistrictMaster()!=null){
						id=districtAttachment.getDistrictMaster().getDistrictId(); 
						filePath = id+"/DistAttachmentFile";  
						source 	= 	Utility.getValueOfPropByKey("districtRootPath")+filePath+"/"+fileName;
						 target 	= 	context.getServletContext().getRealPath("/")+"/district/"+filePath+"/";
						 path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+filePath+"/"+fileName;
					}
				 	File sourceFile = 	new File(source);
					File targetDir 	= 	new File(target);
					if(!targetDir.exists())
						targetDir.mkdirs();
					File targetFile = new File(targetDir+"/"+sourceFile.getName());
					FileUtils.copyFile(sourceFile, targetFile);
				}
				//path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+filePath+"/"+fileName;
			} catch (Exception e) {
				e.printStackTrace();
				path="";
			}
			return path;
		 }
	
	
	
	
	public boolean updateDistrictAttachmentStatus(Integer districtattachmentId)	
	{
		
	System.out.println("::::::::::::::::"+districtattachmentId);
		
		//File path = new File(Utility.getValueOfPropByKey("districtRootPath")+districtId+"/DistAttachmentFile");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		try{
		DistrictAttachment distattachment=districtAttachmentDAO.findById(districtattachmentId, false, false);
		
		String stat=distattachment.getStatus();
		System.out.println("stat is"+stat);
		if(stat.equals("A"))
		{
			distattachment.setStatus("I");
		}
		if(stat.equals("I"))
		{
			distattachment.setStatus("A");
		}
		
		districtAttachmentDAO.makePersistent(distattachment);
		}catch (Exception e) {
		System.out.println(e);
		}
		
		return true;
	}
		
	
	public String getAttachmentBYId(Integer id)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		String distId = "";
		String distname = "";
		int hqbrFlag = 0;
		try{
		DistrictAttachment distattachment=districtAttachmentDAO.findById(id, false, false);
		
		String attname=distattachment.getDistrictAttachment();
		if(distattachment.getHeadQuarterMaster()!=null && distattachment.getBranchMaster()==null){
			distId=String.valueOf(distattachment.getHeadQuarterMaster().getHeadQuarterId());
			distname=distattachment.getHeadQuarterMaster().getHeadQuarterName();
			hqbrFlag = 1;
		}
		else if(distattachment.getBranchMaster()!=null){
			distId=String.valueOf(distattachment.getBranchMaster().getBranchId());
			distname=distattachment.getBranchMaster().getBranchName();
			hqbrFlag = 2;
		}
		else{
			distId=String.valueOf(distattachment.getDistrictMaster().getDistrictId());
			distname=distattachment.getDistrictMaster().getDistrictName();
			hqbrFlag = 0;
		}
		String schoolname="";
		String schooolId="";
		try{
		if(!distattachment.getSchoolMaster().equals(null))
		{
			schoolname=distattachment.getSchoolMaster().getSchoolName();
			schooolId=String.valueOf(distattachment.getSchoolMaster().getSchoolId());
		}}catch (Exception e) {
		
		}
		StringBuffer bfr=new StringBuffer();
		bfr.append(attname+"::::");
		bfr.append(distId+"::::");
		bfr.append(distname+"::::");
		bfr.append(schooolId+"::::");
		bfr.append(schoolname+"::::");
		bfr.append(hqbrFlag);
	 return bfr.toString();	
		
		}catch(Exception e)
		{
			System.out.println(e);
		return "nill";	
		}
		}
	
	public String displayAllDistrictAssessment(String districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		StringBuffer sbAttachSchool=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		boolean selectedDAS = false;
		try
		{
			DistrictMaster districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			
			List<DistrictAssessmentDetail> AlldistrictAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
			List<DistrictAssessmentDetail> selecteddistAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
			
			AlldistrictAssessmentDetails = districtAssessmentDetailDAO.findByDistrict(districtMaster);
			
			
			String daIds = districtMaster.getDistrictAssessmentId();
			
			
		//	JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(Integer.parseInt(jobCategoryId), false, false);
		//	SecondaryStatus secondaryStatus = secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId), false, false);
			
		//	List<UserMaster> allActiveUserList = userMasterDAO.getActiveDAUserByDistrict(districtMaster);
		//	List<JobCategoryWiseStatusPrivilege> jcwpsList = new ArrayList<JobCategoryWiseStatusPrivilege>();
			
		//	jcwpsList = jobCategoryWiseStatusPrivilegeDAO.getFilterStatus(districtMaster, jobCategoryMaster, secondaryStatus);

			//jcwpsList = jobCategoryWiseStatusPrivilegeDAO.getMaxJSIValueList(districtMaster, jobCategoryMaster, secondaryStatus, statusMaster);
			
		//	System.out.println(" >>>>>>>> jcwpsList <<<<<<<< "+jcwpsList.size());
			
	//		String daIds = jcwpsList.get(0).getEmailNotificationToSelectedDistrictAdmins();
			
	//		System.out.println(" jcwpsList.get(0).getEmailNotificationToSelectedDistrictAdmins() "+jcwpsList.get(0).getEmailNotificationToSelectedDistrictAdmins());
	//		System.out.println(" jcwpsList.get(0).getJobCategoryStatusPrivilegeId() "+jcwpsList.get(0).getJobCategoryStatusPrivilegeId());
			
			String arr[]=null;
			List multiDAIds = new ArrayList();
			
			if(daIds!=null && !daIds.equals(""))
			{
				if(daIds!="" && daIds.length() > 0)
				{
					arr=daIds.split("#");
				}
				
				if(arr!=null && arr.length > 0)
				{
					for(int i=0;i<arr.length;i++)
					{
						multiDAIds.add(Integer.parseInt((arr[i])));	
					}
				}
			}
			
			selecteddistAssessmentDetails = districtAssessmentDetailDAO.findByDistAssessmentIds(multiDAIds);
			
			System.out.println(" selecteddistAssessmentDetails :: "+selecteddistAssessmentDetails.size());
			
			
			AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails);
			
			
			System.out.println(" AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails) :: "+AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails));
			
			
			System.out.println(" AlldistrictAssessmentDetails :: "+AlldistrictAssessmentDetails.size());
			
			
			//Unselected DA List
			sb.append("<select multiple id=\"lstDistrictAdmins\" name=\"lstDistrictAdmins\" class=\"form-control\" style=\"height: 150px;\" >");
			if(AlldistrictAssessmentDetails.size()>0)
			{
				for (DistrictAssessmentDetail selectedDASMT : AlldistrictAssessmentDetails) {
					sb.append("<option value="+selectedDASMT.getDistrictAssessmentId()+">"+selectedDASMT.getDistrictAssessmentName()+"</option>");
				}
			}
			sb.append("</select>");
			
			//Selected DA List
			sbAttachSchool.append("<select multiple class=\"form-control\" id=\"attachedDAList\" name=\"attachedDAList\" style=\"height: 150px;\">");
			if(selecteddistAssessmentDetails.size()>0)
			{
				for (DistrictAssessmentDetail selectdDASMT : selecteddistAssessmentDetails) {
					sbAttachSchool.append("<option value="+selectdDASMT.getDistrictAssessmentId()+">"+selectdDASMT.getDistrictAssessmentName()+"</option>");
				}
				selectedDAS = true;
			}
			sbAttachSchool.append("</select>");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		System.out.println(" sb.toString() :: "+sb.toString());
		System.out.println(" sbAttachSchool.toString() :: "+sbAttachSchool.toString());
		System.out.println(" selectedDAS :: "+selectedDAS);

		return sb.toString()+"||"+sbAttachSchool.toString();
	}
	
	public String addApprovalGroup(Integer districtId, String groupName, Integer noOfApproval, String keyContactIds, boolean isRestrict)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
						
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		if(districtId!=null && groupName!=null && !groupName.equals(""))
		{
			try
			{
				DistrictMaster districtMaster=null;
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null)
					if(userMaster.getDistrictId().getDistrictId().equals(districtId))
						districtMaster = userMaster.getDistrictId();
				
				if(districtMaster==null)
					districtMaster = districtMasterDAO.findById(districtId, false, false);
				
				if(isRestrict)
				{
					List<DistrictWiseApprovalGroup> districtWiseApprovalGroupES = districtWiseApprovalGroupDAO.findGroupsByDistrictAndShortName(districtMaster,"ES");
					
					if(districtWiseApprovalGroupES!=null && districtWiseApprovalGroupES.size()>0)
						if(districtWiseApprovalGroupES.get(0).getGroupName()!=null)
							if(districtWiseApprovalGroupES.get(0).getGroupName().equalsIgnoreCase(groupName))
								return "EsExist";
					
					List<DistrictWiseApprovalGroup> districtWiseApprovalGroupSA = districtWiseApprovalGroupDAO.findGroupsByDistrictAndShortName(districtMaster,"SA");
					
					if(districtWiseApprovalGroupSA!=null && districtWiseApprovalGroupSA.size()>0){
						if(districtWiseApprovalGroupSA.get(0).getGroupName()!=null){
							if(districtWiseApprovalGroupSA.get(0).getGroupName().equalsIgnoreCase(groupName)){
								
								String groupMembers=null;
								if(keyContactIds!=null && !keyContactIds.equals(""))
									groupMembers = keyContactIds;
								
								List<DistrictApprovalGroups> districtApprovalGroupsList = districtApprovalGroupsDAO.findGroupsByDistrictAndName(districtMaster, groupName);
								
								if(districtApprovalGroupsList!=null && districtApprovalGroupsList.size()>0){
									DistrictApprovalGroups districtApprovalGroups = districtApprovalGroupsList.get(0);
									districtApprovalGroups.setGroupMembers(groupMembers);
									districtApprovalGroups.setStatus("A");
								
									districtApprovalGroupsDAO.makePersistent(districtApprovalGroups);
									
									return "";
								}
							}
						}
					}
					
					boolean isGroupExist = districtApprovalGroupsDAO.isGroupExistByDistrict(districtId, groupName);
					if(!isGroupExist)
					{
						String groupMembers=null;
						if(keyContactIds!=null && !keyContactIds.equals(""))
							groupMembers = keyContactIds;
											
						DistrictApprovalGroups districtApprovalGroups = new DistrictApprovalGroups();
						districtApprovalGroups.setApprovalGroupCreatedDateTime(new Date());
						districtApprovalGroups.setGroupName(groupName);
						districtApprovalGroups.setDistrictId(districtMaster);
						districtApprovalGroups.setGroupMembers(groupMembers);
						districtApprovalGroups.setJobCategoryId(null);
						districtApprovalGroups.setJobId(null);
						districtApprovalGroups.setStatus("A");
						districtApprovalGroups.setNoOfApprovals(noOfApproval);
						
						districtApprovalGroupsDAO.makePersistent(districtApprovalGroups);
						
						return "";
					}
					else
						return "GroupExist";
				}
				else
				{
					boolean isGroupExist = districtApprovalGroupsDAO.isGroupExistByDistrict(districtId, groupName);
					if(!isGroupExist)
					{
						String groupMembers=null;
						if(keyContactIds!=null && !keyContactIds.equals(""))
							groupMembers = keyContactIds;
											
						DistrictApprovalGroups districtApprovalGroups = new DistrictApprovalGroups();
						districtApprovalGroups.setApprovalGroupCreatedDateTime(new Date());
						districtApprovalGroups.setGroupName(groupName);
						districtApprovalGroups.setDistrictId(districtMaster);
						districtApprovalGroups.setGroupMembers(groupMembers);
						districtApprovalGroups.setJobCategoryId(null);
						districtApprovalGroups.setJobId(null);
						districtApprovalGroups.setStatus("A");
						districtApprovalGroups.setNoOfApprovals(noOfApproval);
						
						districtApprovalGroupsDAO.makePersistent(districtApprovalGroups);
						
						return "";
					}
					else
						return "GroupExist";
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return "";
	}
	
	public String addMemberIntoApprovalGroup(Integer approvalGroupId, String keyContactIds)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		String str="";
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		if(approvalGroupId!=null && keyContactIds!=null && !keyContactIds.equals(""))
		{
			try
			{
				DistrictApprovalGroups districtApprovalGroups = districtApprovalGroupsDAO.findById(approvalGroupId, false, false);
				
				if(districtApprovalGroups!=null)
				{
					String groupMembers = (districtApprovalGroups.getGroupMembers()==null || districtApprovalGroups.getGroupMembers().equals(""))? "#" : districtApprovalGroups.getGroupMembers();
					System.out.println("groupMembers:- "+groupMembers);
					
					if(!keyContactIds.contains("Remove"))
					{
						String members[] = keyContactIds.split("#");
						keyContactIds="";
						for(String mem : members)
						{
							System.out.println("Mem:- "+mem+", and conatins:- "+(groupMembers.contains("#"+mem+"#") ));
							if(!mem.equals("") && !groupMembers.contains("#"+mem+"#"))
							{
								keyContactIds = keyContactIds +mem+"#";
								System.out.println("keyContactIds:- "+keyContactIds);
							}
						}
						if(!keyContactIds.equals(""))
							groupMembers = groupMembers+keyContactIds;
					}
					else
					{
						System.out.println("inside Remove ::::::::::");
						groupMembers=keyContactIds.replace("Remove", "");
						if(!groupMembers.startsWith("#"))
							groupMembers = "#"+groupMembers;
						if(!groupMembers.endsWith("#"))
							groupMembers = groupMembers+"#";
					}
					
					System.out.println("keyContactIds:- "+keyContactIds);
					System.out.println("groupMembers:- "+groupMembers);
					districtApprovalGroups.setGroupMembers(groupMembers);
					
					districtApprovalGroupsDAO.makePersistent(districtApprovalGroups);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return str;
	}
	
	public String removeMemberFromApprovalGroup(Integer approvalGroupId, Integer keyContactId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
						
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		if(approvalGroupId!=null && keyContactId!=null)
		{
			try
			{
				DistrictApprovalGroups districtApprovalGroups = districtApprovalGroupsDAO.findById(approvalGroupId, false, false);
				
				if(districtApprovalGroups!=null)
				{
					if(districtApprovalGroups.getGroupMembers()!=null || districtApprovalGroups.getGroupMembers().contains("#"+keyContactId+"#") )
					{
						String groupMembers = districtApprovalGroups.getGroupMembers();
						groupMembers = groupMembers.replace( "#"+keyContactId+"#", "#");
						
						districtApprovalGroups.setGroupMembers(groupMembers);
						
						SessionFactory sessionFactory = districtApprovalGroupsDAO.getSessionFactory();
						StatelessSession statelesSsession 	= sessionFactory.openStatelessSession();
						Transaction txOpen =statelesSsession.beginTransaction();
						statelesSsession.update(districtApprovalGroups);
						txOpen.commit();
				       	statelesSsession.close();
				    }
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return "";
	}
	
	public String displayApprovalGroupsAndMembers(Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb = new StringBuffer();
				
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		if(districtId!=null)
		{
			try
			{
				DistrictMaster districtMaster=null;
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null)
					if(userMaster.getDistrictId().getDistrictId().equals(districtId))
						districtMaster = userMaster.getDistrictId();
				
				if(districtMaster==null)
					districtMaster = districtMasterDAO.findById(districtId, false, false);
					
				List<DistrictApprovalGroups> districtApprovalGroupList = districtApprovalGroupsDAO.findAllDistrictApprovalGroupsByDistrict(districtMaster);
				if(districtApprovalGroupList!=null && districtApprovalGroupList.size()>0)
				{
					
					Set<Integer> membersId = new TreeSet<Integer>();
					for(DistrictApprovalGroups districtApprovalGroups :districtApprovalGroupList)
					{
						String[] membersName = (districtApprovalGroups.getGroupMembers()!=null)? districtApprovalGroups.getGroupMembers().split("#") : null;
						if(membersName!=null && membersName.length >0)
							for(String member : membersName)
								if(!member.equals(""))
									membersId.add(Integer.parseInt(member));
					}
					
					Map<String, DistrictKeyContact> membersMap = new HashMap<String, DistrictKeyContact>();
					if(membersId!=null && membersId.size()>0)
					{
						List<DistrictKeyContact> membersList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", membersId));
						for(DistrictKeyContact contact : membersList)
							membersMap.put(""+contact.getKeyContactId(), contact);
					}
					
					membersId=null;
					for(DistrictApprovalGroups districtApprovalGroups :districtApprovalGroupList)
					{
						String groupName = districtApprovalGroups.getGroupName();
						String[] membersIds = (districtApprovalGroups.getGroupMembers()!=null)? districtApprovalGroups.getGroupMembers().split("#") : null;
						
						sb.append("<div class='row mt10'>");
						sb.append("<label> "+groupName+"&nbsp<a href='javascript:void(0);' data-toggle='tooltip' title='"+Utility.getLocaleValuePropByKey("msgRemoveGroup", locale)+"' onclick='removeGroupConfirmation("+districtApprovalGroups.getDistrictApprovalGroupsId()+")' ><img width='15' height='15' class='can' src='images/can-icon.png'></a> </label>");
						
						sb.append("<div style='margin-top: -6px;'>");
						
						int i=0;
						if(membersIds!=null && membersIds.length >0)
							for(String memberId : membersIds)
								if(!memberId.equals(""))
									if(membersMap.get(memberId)!=null)
									sb.append("<div class='col-sm-3 col-md-3'>"+membersMap.get(memberId).getKeyContactFirstName()+" "+membersMap.get(memberId).getKeyContactLastName()+"&nbsp<a href='javascript:void(0);' data-toggle='tooltip' title='"+Utility.getLocaleValuePropByKey("msgRemoveMember", locale)+"' onclick='removeMemberConfirmation("+districtApprovalGroups.getDistrictApprovalGroupsId()+","+membersMap.get(memberId).getKeyContactId()+")' ><img width='15' height='15' class='can' src='images/can-icon.png'></a></div>");
									
						sb.append("</div>");
						sb.append("</div>");
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return sb.toString();
	}
	
	public JSONObject getApprovalGroupsAndMembersList(Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		JSONObject approvalGroupAndMemberList = new JSONObject();
		JSONObject  groupList = new JSONObject();
		JSONObject  memberList = new JSONObject();
				
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		if(districtId!=null)
		{
			try
			{
				DistrictMaster districtMaster=null;
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null)
					if(userMaster.getDistrictId().getDistrictId().equals(districtId))
						districtMaster = userMaster.getDistrictId();
				
				if(districtMaster==null)
					districtMaster = districtMasterDAO.findById(districtId, false, false);
					
				List<DistrictApprovalGroups> districtApprovalGroupList = districtApprovalGroupsDAO.findAllDistrictApprovalGroupsByDistrict(districtMaster);
				List<DistrictKeyContact> districtKeyContactsList = districtKeyContactDAO.findByContactType(districtMaster, "Job Approval");
				
				Set<String> membersExist = new TreeSet<String>();
				
				//Adding the group name into groupList
				if(districtApprovalGroupList!=null && districtApprovalGroupList.size()>0)
				{
					for(DistrictApprovalGroups districtApprovalGroup :districtApprovalGroupList)
					{
						groupList.put(districtApprovalGroup.getDistrictApprovalGroupsId(), districtApprovalGroup.getGroupName());
						if(districtApprovalGroup.getGroupMembers()!=null)
							for(String member : districtApprovalGroup.getGroupMembers().split("#"))
								membersExist.add(member);
					}
				}
				
				//Adding the members name into membersList
				if(districtKeyContactsList!=null && districtKeyContactsList.size()>0)
				{
					for(DistrictKeyContact districtKeyContact :districtKeyContactsList)
					{
						//if(!membersExist.contains(""+districtKeyContact.getKeyContactId()))
							memberList.put(districtKeyContact.getKeyContactId(), districtKeyContact.getKeyContactFirstName()+" "+districtKeyContact.getKeyContactLastName());
					}
				}
				
				approvalGroupAndMemberList.put("groups", groupList);
				approvalGroupAndMemberList.put("members", memberList);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return approvalGroupAndMemberList;
	}
	
	public JSONObject getApprovalGroupsInfo(Integer districtId, Integer jobCategoryId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		JSONObject approvalGroupAndMemberInfo = new JSONObject();
		Map<String, String>  groupAndMemberList = new LinkedHashMap<String, String>();	//key-> GroupId_GroupName, Value->Member01Id_Member01Name,Member02Id_Member02Name... 
		List<String> memberList = new ArrayList<String>();	//Member01Id_Member01Name,Member02Id_Member02Name...
						
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		boolean flag=false;
		System.out.println("getApprovalGroupsInfo:- districtId:- "+districtId);
		if(districtId!=null)
		{
			try
			{
				DistrictMaster districtMaster=null;
				JobCategoryMaster jobCategoryMaster=null;
				JobCategoryMaster jobCategoryMaster1=null;
				
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null)
				{
					districtMaster = districtMasterDAO.findById(userMaster.getDistrictId().getDistrictId(), false, false);
				}
				
				if(districtMaster==null)
					districtMaster = districtMasterDAO.findById(districtId, false, false);
				if(jobCategoryMaster==null && jobCategoryId!=-1 && jobCategoryId!=null)
					jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				jobCategoryMaster1 = jobCategoryMaster;
				
					
				List<DistrictApprovalGroups> districtApprovalGroupList = new ArrayList<DistrictApprovalGroups>();
				if(jobCategoryMaster!=null)
				{
					districtApprovalGroupList = districtApprovalGroupsDAO.findAllDistrictApprovalGroupsByJobCategory(jobCategoryMaster.getDistrictMaster(), jobCategoryMaster);
					if(districtApprovalGroupList==null || districtApprovalGroupList.size()==0)
					{
						jobCategoryMaster=null;
						flag=true;
					}
				}
				
				System.out.println("districtApprovalGroupList(Using jobCategory):- \n"+districtApprovalGroupList);
				
				if(districtApprovalGroupList==null || districtApprovalGroupList.size()==0)
					districtApprovalGroupList = districtApprovalGroupsDAO.findAllDistrictApprovalGroupsByDistrict(districtMaster);
				
				System.out.println("districtApprovalGroupList:- \n"+districtApprovalGroupList);
				List<DistrictKeyContact> districtKeyContactsList = districtKeyContactDAO.findByContactType(districtMaster, "Job Approval");
				
				Map<Integer, String> membersName = new LinkedHashMap<Integer, String>();
				if(districtKeyContactsList!=null && districtKeyContactsList.size()>0)
					for(DistrictKeyContact districtKeyContact :districtKeyContactsList)
					{
						membersName.put(districtKeyContact.getKeyContactId(), districtKeyContact.getKeyContactFirstName()+" "+districtKeyContact.getKeyContactLastName());
						memberList.add(districtKeyContact.getKeyContactId()+"_"+districtKeyContact.getKeyContactFirstName()+" "+districtKeyContact.getKeyContactLastName());
					}
				
				
				if(districtApprovalGroupList!=null && districtApprovalGroupList.size()>0)
				{
					for(DistrictApprovalGroups districtApprovalGroup :districtApprovalGroupList)
					{
						String key= districtApprovalGroup.getDistrictApprovalGroupsId()+"_"+districtApprovalGroup.getGroupName();
						String value="";
						if(districtApprovalGroup.getGroupMembers()!=null)
						{
							for(String memberId : districtApprovalGroup.getGroupMembers().split("#"))
							{
								if(!memberId.equals(""))
								{
									if(value.equals(""))
										value= memberId+"_"+membersName.get(Integer.parseInt(memberId));
									else
										value=value+","+memberId+"_"+membersName.get(Integer.parseInt(memberId));
									
									//memberList.remove(memberId+"_"+membersName.get(Integer.parseInt(memberId)));
								}
							}
						}
						groupAndMemberList.put(key, value);
					}
				}
				
				String noOfApprovalNeeded="";
				String buildApprovalGroup="";
				String approvalBeforeGoLive="";
				
				if(jobCategoryMaster!=null)
				{
					System.out.println("Get Element by jobCategory:- "+jobCategoryMaster.getJobCategoryId()+", "+jobCategoryMaster.getJobCategoryName());
					noOfApprovalNeeded = (jobCategoryMaster.getNoOfApprovalNeeded()!=null)? ""+jobCategoryMaster.getNoOfApprovalNeeded() : "0";
					buildApprovalGroup = (jobCategoryMaster.getBuildApprovalGroup()!=null)? ""+jobCategoryMaster.getBuildApprovalGroup() : "false";
					approvalBeforeGoLive = (jobCategoryMaster.getApprovalBeforeGoLive()!=null)? ""+jobCategoryMaster.getApprovalBeforeGoLive() : "false";
				}
				else
				if(districtMaster!=null)
				{
					System.out.println("Get Element by districtMaster:- "+districtMaster);
					noOfApprovalNeeded = (districtMaster.getNoOfApprovalNeeded()!=null)? ""+districtMaster.getNoOfApprovalNeeded() : "0";
					buildApprovalGroup = (districtMaster.getBuildApprovalGroup()!=null)? ""+districtMaster.getBuildApprovalGroup() : "false";
					if(jobCategoryMaster1!=null)
						approvalBeforeGoLive = (jobCategoryMaster1.getApprovalBeforeGoLive()!=null)? ""+jobCategoryMaster1.getApprovalBeforeGoLive() : "false";
					else
						approvalBeforeGoLive = (districtMaster.getApprovalBeforeGoLive()!=null)? ""+districtMaster.getApprovalBeforeGoLive() : "false";
				}
				
				
				approvalGroupAndMemberInfo.put("groupAndMemberList", groupAndMemberList);
				approvalGroupAndMemberInfo.put("memberList", memberList);
				approvalGroupAndMemberInfo.put("noOfApprovalNeeded", noOfApprovalNeeded);
				approvalGroupAndMemberInfo.put("buildApprovalGroup", buildApprovalGroup);
				approvalGroupAndMemberInfo.put("approvalBeforeGoLive", approvalBeforeGoLive);
				
				System.out.println("noOfApprovalNeeded:- "+noOfApprovalNeeded);
				System.out.println("buildApprovalGroup:- "+buildApprovalGroup);
				System.out.println("approvalBeforeGoLive:- "+approvalBeforeGoLive);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		System.out.println("groupAndMemberList:- "+groupAndMemberList);
		System.out.println("memberList:- "+memberList);
		
		return approvalGroupAndMemberInfo;
	}
	
	public String setActiveApprovalGroup(String districtId, Integer noOfActiveGroup)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
						
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		if(districtId!=null && noOfActiveGroup!=null)
		{
			try
			{
				DistrictMaster districtMaster = districtMasterDAO.findByDistrictId(districtId);
				System.out.println("districtMaster:- "+districtMaster);
				if(districtMaster!=null)
				{
					List<DistrictApprovalGroups> districtApprovalGroupsList = districtApprovalGroupsDAO.findAllDistrictApprovalGroupsByDistrict(districtMaster);
					//List<DistrictApprovalGroups> districtApprovalGroupsListNew = new ArrayList<DistrictApprovalGroups>(districtApprovalGroupsList.size());
					
					for(DistrictApprovalGroups approvalGroups : districtApprovalGroupsList)
						approvalGroups.setStatus("A");	//Change status I to A
					
					System.out.println("districtApprovalGroupsList:- "+districtApprovalGroupsList.size());
					int length = (noOfActiveGroup>districtApprovalGroupsList.size())? districtApprovalGroupsList.size() : noOfActiveGroup;
					
					System.out.println("length:- "+length+",noOfActiveGroup:-  "+noOfActiveGroup);
					for(int i=0; i<length;i++)
						districtApprovalGroupsList.get(i).setStatus("A");
					
					
					SessionFactory sessionFactory = districtApprovalGroupsDAO.getSessionFactory();
					StatelessSession statelesSsession 	= sessionFactory.openStatelessSession();
					Transaction txOpen =statelesSsession.beginTransaction();
					for(DistrictApprovalGroups approvalGroups : districtApprovalGroupsList)
						statelesSsession.update( approvalGroups );
					txOpen.commit();
					statelesSsession.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return "";
	}
	
	public String addOrUpdateDistrictApprovalGroupJobCategorywise(Integer districtId, Integer jobCategoryId, Integer noOfApprovals, String groupInfo)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb = new StringBuffer();
				
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		System.out.println("districtId:- "+districtId);
		if(districtId!=null && noOfApprovals!=null && groupInfo!=null && !groupInfo.equals(""))
		{
			try
			{
				Map<String, String> approvalGroupMap = new LinkedHashMap<String, String>();
				System.out.println("groupInfo:- "+groupInfo);
				for(String val : groupInfo.split("#"))	//#1_#32_1_3_4_#54_10_#newGroup459_12_19_#
				{
					if(!val.equals(""))			//1_ , 32_1_3_4_ , 54_10_
					{
						System.out.println("Val:- "+val);
						
						String groupId = val.substring(0, val.indexOf("_"));
						String groupMember="";
						String[] groupMembers = (val.substring(val.indexOf("_"))).split("_");
						
						if(groupMembers.length>0)
						for(int i=0;i<groupMembers.length;i++)
						{
							try
							{
								if(i==0)
									groupMember="#";
								Integer.parseInt(groupMembers[i]);
								if(!groupMember.contains("#"+groupMembers[i]+"#"))
									groupMember = groupMember+groupMembers[i]+"#";
							}
							catch(Exception e){}
						}
						
						if(groupMember.equals("#"))
							groupMember = "";
						
						System.out.println("group Id:- "+groupId+", groupMember:- "+groupMember);
						approvalGroupMap.put(groupId, groupMember);
					}
				}
				
				JobCategoryMaster jobCategoryMaster =null;
				Map<String, DistrictApprovalGroups> districtApprovalGroupListJobCategorywiseMap= new LinkedHashMap<String, DistrictApprovalGroups>();
				Map<String, DistrictApprovalGroups> districtApprovalGroupListDistrictwiseMap = new LinkedHashMap<String, DistrictApprovalGroups>();
				
				if(jobCategoryId!=null)
					jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				
				if (jobCategoryMaster!=null && jobCategoryMaster.getDistrictMaster()!=null)
				{
					List<DistrictApprovalGroups> districtApprovalGroupListJobCategorywise = districtApprovalGroupsDAO.findAllDistrictApprovalGroupsByJobCategory(jobCategoryMaster.getDistrictMaster(), jobCategoryMaster);
					List<DistrictApprovalGroups> districtApprovalGroupListDistrictwise = districtApprovalGroupsDAO.findAllDistrictApprovalGroupsByDistrict(jobCategoryMaster.getDistrictMaster());
					
					for(DistrictApprovalGroups dag: districtApprovalGroupListJobCategorywise)
						districtApprovalGroupListJobCategorywiseMap.put(""+dag.getDistrictApprovalGroupsId(), dag);
					
					for(DistrictApprovalGroups dag: districtApprovalGroupListDistrictwise)
						districtApprovalGroupListDistrictwiseMap.put(""+dag.getDistrictApprovalGroupsId(), dag);
				}
				
				SessionFactory sessionFactory		=	districtApprovalGroupsDAO.getSessionFactory();
				StatelessSession statelesSsession 	= sessionFactory.openStatelessSession();
				Transaction txOpen =statelesSsession.beginTransaction();
				
				System.out.println("approvalGroupMap:- "+approvalGroupMap);
				System.out.println("districtApprovalGroupListJobCategorywiseMap:- "+districtApprovalGroupListJobCategorywiseMap);
				System.out.println("districtApprovalGroupListDistrictwiseMap:- "+districtApprovalGroupListDistrictwiseMap);
				
				for(Map.Entry<String, String> entry : approvalGroupMap.entrySet())
				{
					String groupId = entry.getKey();
					String groupMember = entry.getValue();
					
					boolean isNewGroup=false;
					
					try{Integer.parseInt(groupId);}
					catch(Exception e){isNewGroup=true;}
					
					System.out.println("groupId:- "+groupId+" is new group :- "+isNewGroup);
					
					if(isNewGroup)
					{
						DistrictApprovalGroups districtApprovalGroups = new DistrictApprovalGroups();
						districtApprovalGroups.setApprovalGroupCreatedDateTime(new Date());
						districtApprovalGroups.setDistrictId(jobCategoryMaster.getDistrictMaster());
						districtApprovalGroups.setGroupMembers(groupMember);
						districtApprovalGroups.setGroupName(groupId.replaceAll("-", " "));	//If Group is new then group id is group name
						districtApprovalGroups.setJobCategoryId(jobCategoryMaster);
						districtApprovalGroups.setJobId(null);
						districtApprovalGroups.setNoOfApprovals(noOfApprovals);
						districtApprovalGroups.setStatus("A");
						
						statelesSsession.insert(districtApprovalGroups);
						System.out.println("insert groupId:- "+groupId);
					}
					else
					if(districtApprovalGroupListDistrictwiseMap.containsKey(groupId))
					{
						DistrictApprovalGroups districtApprovalGroups = districtApprovalGroupListDistrictwiseMap.get(groupId);
						districtApprovalGroups.setApprovalGroupCreatedDateTime(new Date());
						districtApprovalGroups.setDistrictId(jobCategoryMaster.getDistrictMaster());
						districtApprovalGroups.setGroupMembers(groupMember);
						districtApprovalGroups.setJobCategoryId(jobCategoryMaster);
						districtApprovalGroups.setJobId(null);
						districtApprovalGroups.setNoOfApprovals(noOfApprovals);
						districtApprovalGroups.setStatus("A");
						
						statelesSsession.insert(districtApprovalGroups);
						System.out.println("insert groupId:- "+groupId);
					}
					else
					if(districtApprovalGroupListJobCategorywiseMap.containsKey(groupId))
					{
						DistrictApprovalGroups districtApprovalGroups = districtApprovalGroupListJobCategorywiseMap.remove(groupId);
						districtApprovalGroups.setGroupMembers(groupMember);
						statelesSsession.update(districtApprovalGroups);
						System.out.println("update groupId:- "+groupId);
					}
				}
				
				txOpen.commit();
				statelesSsession.close();
				
				if(districtApprovalGroupListJobCategorywiseMap!=null && districtApprovalGroupListJobCategorywiseMap.size()>0)
				{
					List<DistrictApprovalGroups> districtApprovalGroupsList = new ArrayList<DistrictApprovalGroups>();
					for(Map.Entry<String, DistrictApprovalGroups> entry : districtApprovalGroupListJobCategorywiseMap.entrySet())
					{
						districtApprovalGroupsList.add(entry.getValue());
						
						/*String groupId = entry.getKey();
						DistrictApprovalGroups districtApprovalGroups = districtApprovalGroupListJobCategorywiseMap.get(groupId);
						
						Integer groupIdInInt = null;
						try{groupIdInInt = Integer.parseInt(groupId);}
						catch(Exception e){}
						
						if(districtApprovalGroups!=null)
							System.out.println("delete group Id:- "+groupIdInInt+", is group deleted:- "+removeApprovalGroupByGroupId(groupIdInInt));
						*/
					}
					
					if(districtApprovalGroupsList!=null && districtApprovalGroupsList.size()>0)
					{
						List<JobApprovalHistory> jobApprovalHistoryList =  jobApprovalHistoryDAO.findByCriteria(Restrictions.in("districtApprovalGroups", districtApprovalGroupsList));
						
						System.out.println("jobApprovalHistoryList:- "+jobApprovalHistoryList.size());
						
						//update the history
						sessionFactory		=	jobApprovalHistoryDAO.getSessionFactory();
						statelesSsession 	= sessionFactory.openStatelessSession();
						txOpen =statelesSsession.beginTransaction();
						for(JobApprovalHistory jah: jobApprovalHistoryList)
						{
							System.out.println("history is updated..");
							jah.setDistrictApprovalGroups(null);
							statelesSsession.update(jah);
						}
						txOpen.commit();
						statelesSsession.close();
						
						//Now delete the district Approval group
						sessionFactory		=	districtApprovalGroupsDAO.getSessionFactory();
						statelesSsession 	= sessionFactory.openStatelessSession();
						txOpen =statelesSsession.beginTransaction();
						for(DistrictApprovalGroups dag: districtApprovalGroupsList)
						{
							System.out.println("District Approval Group is deleted");
							statelesSsession.delete(dag);
						}
						txOpen.commit();
						statelesSsession.close();
					}
					
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return "";
	}
	
	public boolean removeApprovalGroupByGroupId(Integer groupId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
						
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		
		if(groupId!=null)
		{
			DistrictApprovalGroups districtApprovalGroups = districtApprovalGroupsDAO.findById(groupId, false, false);
			if(districtApprovalGroups.getDistrictId()!=null){
				List<DistrictWiseApprovalGroup> districtWiseApprovalGroupSA = districtWiseApprovalGroupDAO.findGroupsByDistrictAndShortName(districtApprovalGroups.getDistrictId(),"SA");
				System.out.println("districtWiseApprovalGroupSA.size() : "+districtWiseApprovalGroupSA.size());
				if(districtWiseApprovalGroupSA!=null && districtWiseApprovalGroupSA.size()>0){
					if(districtWiseApprovalGroupSA.get(0).getGroupName()!=null){
						if(districtWiseApprovalGroupSA.get(0).getGroupName().equalsIgnoreCase(districtApprovalGroups.getGroupName())){
							districtApprovalGroups.setGroupMembers("");
							districtApprovalGroups.setStatus("I");
							districtApprovalGroupsDAO.makePersistent(districtApprovalGroups);
							return true;
						}
					}	
				}
				List<DistrictWiseApprovalGroup> districtWiseApprovalGroupES = districtWiseApprovalGroupDAO.findGroupsByDistrictAndShortName(districtApprovalGroups.getDistrictId(),"ES");
				System.out.println("districtWiseApprovalGroupES.size() : "+districtWiseApprovalGroupES.size());
				if(districtWiseApprovalGroupES!=null && districtWiseApprovalGroupES.size()>0){
					if(districtWiseApprovalGroupES.get(0).getGroupName()!=null){
						if(districtWiseApprovalGroupES.get(0).getGroupName().equalsIgnoreCase(districtApprovalGroups.getGroupName())){
							districtApprovalGroups.setGroupMembers("");
							districtApprovalGroups.setStatus("I");
							districtApprovalGroupsDAO.makePersistent(districtApprovalGroups);
							return true;
						}
					}	
				}
			}
			if(jobApprovalHistoryDAO.removeHistoryByGroupId(districtApprovalGroups)){
				boolean flagForDeleteApprovalGroup=true;
				Criterion criterion=Restrictions.like("jobApprovalGroups","%|"+districtApprovalGroups.getDistrictApprovalGroupsId()+"|%");
				List<JobApprovalProcess> jobApprovalProcessList=	jobApprovalProcessDAO.findByCriteria(criterion);
				if(jobApprovalProcessList!=null && jobApprovalProcessList.size()>0){
					flagForDeleteApprovalGroup=false;
				}
				if(flagForDeleteApprovalGroup)
				if(districtApprovalGroupsDAO.removeApprovalGroupByGroupId(districtApprovalGroups.getDistrictApprovalGroupsId()))
					return true;
				}
		}
		return false;
	}
	
	public List<DistrictMaster> getActiveFieldOfDistrictList(String DistrictName)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		List<HqBranchesDistricts> hqBranchesDistricts = null;
		try{
			
			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			Criterion criterionActive = Restrictions.eq("status", "A");
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterionActive);
				
				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterionActive);
				
				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
				
			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
			
			if(districtMasterList!=null && districtMasterList.size()>0){
				hqBranchesDistricts = hqBranchesDistrictsDAO.findAll();
				List<DistrictMaster> districtMasters = null;
				if(hqBranchesDistricts!=null && hqBranchesDistricts.size()>0){
					List<Integer> distIntIds = new ArrayList<Integer>();
					for(HqBranchesDistricts hqbrDist : hqBranchesDistricts){
						if(hqbrDist.getDistrictId()!=null && hqbrDist.getDistrictId().length()>0){
							String dIds = hqbrDist.getDistrictId();
							if(dIds.contains(","))
							{
								String[] districtIds = dIds.split(",");
								for(String s : districtIds)
								{
									distIntIds.add(Integer.parseInt(s));
								}
							}
							else if(dIds!=null && dIds.length()>0)
							{
								distIntIds.add(Integer.parseInt(dIds));
							}
						}						
					}
					if(distIntIds!=null && distIntIds.size()>0){
						try {
							Criterion criterionDists = Restrictions.in("districtId", distIntIds);					
							districtMasters = districtMasterDAO.findByCriteria(criterionDists);
							
							System.out.println(districtMasterList.removeAll(districtMasters));

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return districtMasterList;
	}
	
	public String getQQInManageDistrict(Integer districtId){
		
		System.out.println("****************************getQQInManageDistrict******************************"+districtId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
	    StringBuffer returnList = new StringBuffer();
	 try{
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		 List<QqQuestionSets> lstQqQuestionSets = null;
		 DistrictMaster districtMaster = null;
		
		if(districtId!=null){			
			districtMaster = districtMasterDAO.findById(districtId, false, false);
			lstQqQuestionSets = qqQuestionSetsDAO.findByDistrictActive(districtMaster);
			}else{
				lstQqQuestionSets = new ArrayList<QqQuestionSets>();
		    }
		 
		returnList.append("<select id='qqAvlDistList' name='qqAvlDistList' class='span4'>");
		returnList.append("<option value='0'>"+Utility.getLocaleValuePropByKey("lblPleaseSelectQQSet", locale)+"</option>");
		
		if(lstQqQuestionSets!=null){
			Integer selectOpt=0;
			if(districtMaster!=null && districtMaster.getQqQuestionSets()!=null){
				selectOpt= districtMaster.getQqQuestionSets().getID();
			}
			for(QqQuestionSets list : lstQqQuestionSets){				
				if(list.getID().equals(selectOpt))
					returnList.append("<option selected value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
				  else
					  returnList.append("<option value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
				
			 }
			returnList.append("</select>");
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
		
		return returnList.toString();
}
	
public String getQQForOnboardingInManageDistrict(Integer districtId){
		
		System.out.println("****************************getQQForOnboardingInManageDistrict******************************"+districtId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
	    StringBuffer returnList = new StringBuffer();
	 try{
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		 List<QqQuestionSets> lstQqQuestionSets = null;
		 DistrictMaster districtMaster = null;
		
		if(districtId!=null){			
			districtMaster = districtMasterDAO.findById(districtId, false, false);
			lstQqQuestionSets = qqQuestionSetsDAO.findByDistrictActive(districtMaster);
			}else{
				lstQqQuestionSets = new ArrayList<QqQuestionSets>();
		    }
		 
		returnList.append("<select id='qqAvlDistListForOnboard' name='qqAvlDistListForOnboard' class='span4'>");
		returnList.append("<option value='0'>"+Utility.getLocaleValuePropByKey("listPleaseSelectQQSetForonboarding", locale)+"</option>");
		
		if(lstQqQuestionSets!=null){
		 	Integer selectOpt=0;
			if(districtMaster!=null && districtMaster.getQqQuestionSetsForOnboarding()!=null){
				selectOpt= districtMaster.getQqQuestionSetsForOnboarding().getID();
			}
			for(QqQuestionSets list : lstQqQuestionSets){				
				if(list.getID().equals(selectOpt))
					returnList.append("<option selected value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
				  else
					  returnList.append("<option value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
				
			 }
			returnList.append("</select>");
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
		
		return returnList.toString();
}
	
	public List<DistrictMaster> getActiveDistrictListByState(String DistrictName , Long stateId)
	{
		/* ========  For Session time Out Error =========*/
		System.out.println("StateId::"+stateId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
	    }
		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		StateMaster stateMaster	=	stateMasterDAO.findById(stateId, false, false);
		try{
			
			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			Criterion criterionActive = Restrictions.eq("status", "A");
			Criterion criterionState = Restrictions.eq("stateId", stateMaster);
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterionActive,criterionState);
				
				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterionActive,criterionState);
				
				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
				
			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return districtMasterList;
	}
	public JSONObject getActiveDistrictUserList(Integer districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		JSONObject activeDistrictUserList = new JSONObject();
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		
		if(districtId!=null)
		{
			try
			{
				DistrictMaster districtMaster=null;
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null)
					if(userMaster.getDistrictId().getDistrictId().equals(districtId))
						districtMaster = userMaster.getDistrictId();
				
				if(districtMaster==null)
					districtMaster = districtMasterDAO.findById(districtId, false, false);
				
				JSONObject activeDistrictQQUserList = new JSONObject();
				JSONObject activeDistrictNotQQUserList = new JSONObject();
				
				
				List<UserMaster> userMasterList = usermasterdao.getActiveDAUserByDistrict(districtMaster);
				HashMap<Integer, UserMaster> userMasterMap = new HashMap<Integer, UserMaster>();
				for(UserMaster master : userMasterList)
					userMasterMap.put(master.getUserId(), master);
				
				List<Integer> qqUserIds = new ArrayList<Integer>();
				if(districtMaster.getSendNotificationOnNegativeQQ()!=null)
					for(String id : districtMaster.getSendNotificationOnNegativeQQ().split("#"))
						if(!id.equals(""))
							qqUserIds.add(Integer.parseInt(id));
				
				for(Integer id : qqUserIds)
				{
					UserMaster master = userMasterMap.remove(id);
					activeDistrictQQUserList.put(master.getUserId(), master.getFirstName()+" "+master.getLastName());
				}
				
				for(Map.Entry<Integer, UserMaster> entry : userMasterMap.entrySet())
					activeDistrictNotQQUserList.put(entry.getValue().getUserId(), entry.getValue().getFirstName()+" "+entry.getValue().getLastName());
				
				activeDistrictUserList.put("activeDistrictQQUserList", activeDistrictQQUserList);
				activeDistrictUserList.put("activeDistrictNotQQUserList", activeDistrictNotQQUserList);
				
				return activeDistrictUserList;
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return activeDistrictUserList;
	}
	
	public String ActivateOrDeactiveManageEventMenu(Boolean sACreateEvent, String districtId)
	{
		System.out.println("::::::::::::::::::::::::: inside ActivateOrDeactiveManageEventMenu ::::::::::::::::::::::::::::");
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		Boolean flag = sACreateEvent==null? false : sACreateEvent;
		try{Integer.parseInt(districtId);}catch(Exception e){districtId=null;}
		
		System.out.println("flag:- "+flag+", districtId:- "+districtId);
		if(districtId!=null)
		{
			try
			{
				MenuMaster parentMenuMaster=null;
				MenuMaster eventsMenuMaster=null;
				
				//getting the ParentMenu Id
				try
				{
					Criterion criterion1 = Restrictions.eq("menuName","Manage");
					Criterion criterion2 = Restrictions.isNull("parentMenuId");
					Criterion criterion3 = Restrictions.isNull("districtId");
					Criterion criterion4 = Restrictions.isNull("subMenuId");
					Criterion criterion5 = Restrictions.eq("status","A");

					List<MenuMaster> parentMenuMasterList = menuMasterDAO.findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
					if(parentMenuMasterList!=null && parentMenuMasterList.size()>0)
						parentMenuMaster = parentMenuMasterList.get(0);
					
					//for(MenuMaster menuMaster : parentMenuMasterList)
					//	System.out.println("parentMenuMasterList:- "+menuMaster.getMenuName()+", "+menuMaster.getMenuId()+", "+menuMaster.getDistrictId());
				}
				catch(Exception e){e.printStackTrace();}
				
				//getting the EventMenu Id
				try
				{
					if(parentMenuMaster!=null)
					{
						System.out.println("parentMenuMaster:- "+parentMenuMaster.getMenuName()+", "+parentMenuMaster.getMenuId()+", "+parentMenuMaster.getDistrictId());
						
						Criterion criterion1 = Restrictions.eq("menuName","Events");
						Criterion criterion2 = Restrictions.eq("parentMenuId",parentMenuMaster);
						Criterion criterion3 = Restrictions.isNull("subMenuId");
						Criterion criterion4 = Restrictions.isNotNull("districtId");
						Criterion criterion5 = Restrictions.eq("status","A");
						
						List<MenuMaster> menuMasterList = menuMasterDAO.findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
						if(menuMasterList!=null && menuMasterList.size()>0)
							eventsMenuMaster = menuMasterList.get(0);
						
						//for(MenuMaster menuMaster : menuMasterList)
						//	System.out.println("menuMasterList:- "+menuMaster.getMenuName()+", "+menuMaster.getMenuId()+", "+menuMaster.getDistrictId());
					}
				}
				catch(Exception e){e.printStackTrace();}
				
				if(eventsMenuMaster!=null)
				{
					System.out.println("eventsMenuMaster:- "+eventsMenuMaster.getMenuName()+", "+eventsMenuMaster.getMenuId()+", "+eventsMenuMaster.getDistrictId());
					String districtIds = eventsMenuMaster.getDistrictId().trim();
					
					if( !districtIds.endsWith("|") )
						districtIds = districtIds+"|";
					
					System.out.println("Going to add district, districtIds.contains(|districtId|):- "+districtIds.contains("|"+districtId+"|"));
					if(flag)	//Add district
					{
						if(!districtIds.contains("|"+districtId+"|"))	//district is not available in List
						{
							districtIds = districtIds+districtId+"|";
							System.out.println("district Addedd Successfully");
						}
					}
					else	//remove district
					{
						districtIds = districtIds.replace("|"+districtId+"|", "|");
						System.out.println("district Removed Successfully");
					}
					
					eventsMenuMaster.setDistrictId(districtIds);
					menuMasterDAO.makePersistent(eventsMenuMaster);
				}
			}
			catch(Exception e){e.printStackTrace();}
		}
		return "";
	}
	
	public int saveAdministratorOrAnalystDistrictKeyContact(Integer districtId,String keyContactTypeIdSet,String keyContactFirstName,String keyContactLastName,String keyContactEmailAddress,String keyContactPhoneNumber,String keyContactTitle,UserMaster userMaster)
	{
		String keyContactTypeId[]= 	keyContactTypeIdSet.split("##");
		ArrayList<Integer> idList=new ArrayList<Integer>();
		 for(String id:keyContactTypeId){
			 if(id!=null && !id.equals(""))
			 idList.add(Integer.parseInt(id));
		 }
		List<ContactTypeMaster> keyContactTypeList=null;
		 SessionFactory sessionFactory=districtKeyContactDAO.getSessionFactory();
	       StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	       Transaction txOpen =statelesSsession.beginTransaction();
			 keyContactTypeList	=	contactTypeMasterDAO.findByContactTypeId(idList);
			 if(keyContactTypeList!=null && keyContactTypeList.size()>0)
			 for(ContactTypeMaster contactTypeMaster: keyContactTypeList){
				 	DistrictKeyContact districtKeyContact	=	new DistrictKeyContact();
				 	districtKeyContact.setDistrictId(districtId);
				 	districtKeyContact.setKeyContactTypeId(contactTypeMaster);
				 	districtKeyContact.setKeyContactFirstName(keyContactFirstName);
				 	districtKeyContact.setKeyContactLastName(keyContactLastName);
				 	districtKeyContact.setKeyContactEmailAddress(keyContactEmailAddress);
				 	districtKeyContact.setKeyContactPhoneNumber(keyContactPhoneNumber);
				 	districtKeyContact.setKeyContactTitle(keyContactTitle);
				 	districtKeyContact.setUserMaster(userMaster);
					statelesSsession.insert(districtKeyContact);
			 }
			 txOpen.commit();
//		DistrictKeyContact districtKeyContact	=	new DistrictKeyContact();
//		int emailCheck=0;
//		int keyContactTypeIdcheck=0;
//		int flagVal=0;
//		DistrictMaster districtMaster=null;
//		if(districtId!=0)
//		{
//			districtMaster=districtMasterDAO.findById(districtId, false, false);
//		}
//		try
//		{
//			
//			ContactTypeMaster contactType  = contactTypeMasterDAO.findById(keyContactTypeId, false, false); 	
//			
//			keyContactTypeIdcheck=districtKeyContactDAO.checkkeyContactTypeContact(contactType,districtMaster,keyContactEmailAddress,null);
//			emailCheck=districtKeyContactDAO.checkEmailContact(flagVal,contactType,keyContactEmailAddress,districtMaster,null);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//			return 2;
//		}
//		
//		if(emailCheck==0&&keyContactTypeIdcheck==0)
//		{
//			districtKeyContact.setDistrictId(districtId);
//			ContactTypeMaster contactTypeMaster	=	contactTypeMasterDAO.findById(keyContactTypeId, false, false);
//			districtKeyContact.setKeyContactTypeId(contactTypeMaster);
//			districtKeyContact.setKeyContactFirstName(keyContactFirstName);
//			districtKeyContact.setKeyContactLastName(keyContactLastName);
//			districtKeyContact.setKeyContactEmailAddress(keyContactEmailAddress);
//			districtKeyContact.setKeyContactPhoneNumber(keyContactPhoneNumber);
//			districtKeyContact.setKeyContactTitle(keyContactTitle);
//			districtKeyContact.setUserMaster(userMaster);
//			districtKeyContactDAO.makePersistent(districtKeyContact);
//		}
		return 1;
	}
	
	public String searchDistrictsByDistrictName(int entityID,String DistrictName,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer tmRecords =	new StringBuffer();
		try{
			
			//-- get no of record in grid,
			//-- set start and end position
			//String locale = Utility.getValueOfPropByKey("locale");
			String lblActions = Utility.getLocaleValuePropByKey("lblActions", locale);
            String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
            String lblDeactivate = Utility.getLocaleValuePropByKey("lblDeactivate", locale);
            String lblActivate = Utility.getLocaleValuePropByKey("lblActivate", locale);
            String lblView = Utility.getLocaleValuePropByKey("lblView", locale);
            //String lblEdit = Utility.getLocaleValuePropByKey("lblEdit", locale);
            String lblDistrictName  = Utility.getLocaleValuePropByKey("lblDistrictName", locale);
            String lblState   = Utility.getLocaleValuePropByKey("lblState", locale);
            String lblFinalDecisionMaker   = Utility.getLocaleValuePropByKey("lblFinalDecisionMaker", locale);
            String lblOfTeachers   = Utility.getLocaleValuePropByKey("lblOfTeachers", locale);
            String lblOfStudents   = Utility.getLocaleValuePropByKey("lblOfStudents", locale);
            String lblOfSchools   = Utility.getLocaleValuePropByKey("lblOfSchools", locale);
            String lblCreatedOn   = Utility.getLocaleValuePropByKey("lblCreatedOn", locale);
            String lblStatus    = Utility.getLocaleValuePropByKey("lblStatus", locale);
            //String lblActive    = Utility.getLocaleValuePropByKey("lblActive", locale);
            String lblInactive    = Utility.getLocaleValuePropByKey("lblInactive", locale);
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord 		=	0;
			//------------------------------------

			
			UserMaster userMaster = null;
			int districtId =0;
			int entityIDLogin=0;
			int roleId=0;

			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getDistrictId()!=null){
					
					districtId =userMaster.getDistrictId().getDistrictId();
				}
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				entityIDLogin=userMaster.getEntityType();
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,5,"managedistrict.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(districtId!=0 && entityIDLogin==2){
				entityID=entityIDLogin;
			}
		
			//List<DistrictMaster> districtMaster	  =	null;
			
			/** set default sorting fieldName **/
			String sortOrderFieldName="districtName";
			
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				sortOrderFieldName=sortOrder;
			}
			String sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
			if(sortOrderType!=null)
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("1")){
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}

			/**End ------------------------------------**/
			
			/*if(entityID==1 || districtId==0){
					if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
						Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
						totaRecord			= districtMasterDAO.getRowCountWithSort(sortOrderStrVal,criterion);
						districtMaster  	= districtMasterDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion);
						//****************** Added by deepak  *******************************
						for(DistrictMaster dt: districtMaster){
						if(dt.getDistrictName().equalsIgnoreCase(DistrictName.trim())){
							session.setAttribute("districtMasterId", dt.getDistrictId());
							session.setAttribute("districtMasterName", dt.getDistrictName());
							break;
						}
						}
						//**************************************************************
					}else{
						session.setAttribute("districtMasterId", 0);
						session.setAttribute("districtMasterName", "");
						totaRecord			= districtMasterDAO.getRowCountWithSort(sortOrderStrVal);
						districtMaster  	= districtMasterDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage);
					}
			}*/
			
				/*Criterion criterion= Restrictions.eq("districtId", districtId);
				totaRecord			= districtMasterDAO.getRowCountWithSort(sortOrderStrVal,criterion);
				districtMaster  	= districtMasterDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion);*/
			//int districtId=0;
				JSONArray[] gridJsonRecord = new JSONArray[2];
				gridJsonRecord  	= districtMasterDAO.activeNewDistrictDetails(sortOrderStrVal,start,noOfRowInPage, DistrictName);
			
				
			tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblDistrictName,sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='250' valign='top'>"+responseText+"</th>");
		
			responseText=PaginationAndSorting.responseSortingLink(lblState,sortOrderFieldName,"stateId",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblFinalDecisionMaker,sortOrderFieldName,"dmName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='14%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblOfTeachers,sortOrderFieldName,"totalNoOfTeachers",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='20' valign='top'> "+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblOfStudents,sortOrderFieldName,"totalNoOfStudents",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='20' valign='top'> "+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblOfSchools,sortOrderFieldName,"totalNoOfSchools",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  width='20' valign='top'> "+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblCreatedOn,sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			tmRecords.append("<th width='12%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			tmRecords.append(" <th width='50' valign='top'>"+responseText+"</th>");
			tmRecords.append(" <th  width='15%' valign='top'>"+lblActions+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(gridJsonRecord[0].size()==0)
				tmRecords.append("<tr><td colspan='8' align='center'>"+lblNoRecord+"</td></tr>" );
			
			System.out.println("No of Records "+gridJsonRecord[0].size());
			String hiredDate = "N/A";
			if(gridJsonRecord[0].size()>0){
				System.out.println("gridJsonRecord[0]    "+gridJsonRecord[0].size());
				for (int i = 0; i < gridJsonRecord[0].size(); i++) {
					
					JSONObject jsonrec = new JSONObject();
					jsonrec = gridJsonRecord[0].getJSONObject(i);
					
					tmRecords.append("<tr>");
					tmRecords.append("<td>"+jsonrec.get("districtName")+"</td>");
					tmRecords.append("<td>"+jsonrec.get("stateName")+"</td>");
					tmRecords.append("<td>"+jsonrec.get("dmName")+"</td>");
					tmRecords.append("<td>"+jsonrec.get("totalNoOfTeachers")+"</td>");
					tmRecords.append("<td>"+jsonrec.get("totalNoOfStudents")+"</td>");
					tmRecords.append("<td>"+jsonrec.get("totalNoOfSchools")+"</td>");
					
					if(!jsonrec.get("createdDateTime").toString().equalsIgnoreCase("N/A") && jsonrec.get("createdDateTime")!=null)
						hiredDate = new SimpleDateFormat("MMM dd, yyyy").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(jsonrec.get("createdDateTime").toString()));
					
					tmRecords.append("<td nowrap>"+hiredDate+"</td>");
					tmRecords.append("<td>");
				 
					tmRecords.append(lblInactive);
				 
					tmRecords.append("</td>");
					tmRecords.append("<td nowrap>");

					tmRecords.append("<a title='Activate' href='javascript:void(0);' onclick=\"return activateNewDistrict("+jsonrec.get("districtId")+")\"><i class='fa fa-check fa-lg'></i></a>");
			
					tmRecords.append("</td>");
					tmRecords.append("</tr>");
				}
				tmRecords.append("</table>");
				totaRecord = gridJsonRecord[1].getJSONObject(0).getInt("totalCount");
				if(totaRecord<end)
					end=totaRecord;
				tmRecords.append(PaginationAndSorting.getPaginationStringForManageJobOrdersAjaxDSPQ(request,totaRecord,noOfRow, pageNo));
			
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tmRecords.toString();
	}
 public List<FutureDistrictMaster>	getFutureDistrictListByDistrictName(String districtName){
	 List<FutureDistrictMaster> futureDistrictMasterList=new ArrayList<FutureDistrictMaster>();
	try{
		futureDistrictMasterList=	districtMasterDAO.getFutureDistrictListByDistrictName(districtName);
		
	}catch(Exception e){
		e.printStackTrace();
	}
	 return futureDistrictMasterList;
}
	
 public boolean activateNewDistrict(int districtId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		boolean updateFlag = false;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
	    	userMaster=(UserMaster)session.getAttribute("userMaster");
	    }
		try{
			/*DistrictMaster districtMaster = districtMasterDAO.findById(DistrictId, false, false);
			districtMasterDAO.makePersistent(districtMaster);*/
			//--------------------------
			
			updateFlag = districtMasterDAO.updateActiveNewDistrict(districtMasterDAO, districtId);
			
			 

			
		}catch (Exception e) 
		{
			e.printStackTrace();
			return true;
		}
		System.out.println("complete");
		return true;
	}	
 
}
