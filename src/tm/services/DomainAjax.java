package tm.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DomainMaster;
import tm.bean.user.UserMaster;
import tm.dao.master.DomainMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.utility.Utility;

public class DomainAjax
{
	 String locale = Utility.getValueOfPropByKey("locale");
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lblDomName=Utility.getLocaleValuePropByKey("lblDomName", locale);
	 String headPDRep=Utility.getLocaleValuePropByKey("headPDRep", locale);
	 String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String lblNoDomain=Utility.getLocaleValuePropByKey("lblNoDomain", locale);
	 String lblYes=Utility.getLocaleValuePropByKey("lblYes", locale);
	 String lblNo=Utility.getLocaleValuePropByKey("lblNo", locale);
	 String optAct=Utility.getLocaleValuePropByKey("optAct", locale);
	 String optInActiv=Utility.getLocaleValuePropByKey("optInActiv", locale);
	 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	 String lnkV=Utility.getLocaleValuePropByKey("lnkV", locale);
	 String lblDeactivate=Utility.getLocaleValuePropByKey("lblDeactivate", locale);
	 String lblActivate=Utility.getLocaleValuePropByKey("lblActivate", locale);
	 
	 
	@Autowired
	private DomainMasterDAO domainmasterdao;

	public void setDomainmasterdao(DomainMasterDAO domainmasterdao)
	{
		this.domainmasterdao = domainmasterdao;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	/* @Author: Gagan */
	public String displayDomainRecords(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totaRecord		= 	0;
			//------------------------------------
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,17,"domain.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			List<DomainMaster> domainMaster	  	=	null;
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"domainName";
			/*====== Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			/*=============== End ======================*/
			//domainMaster = domainmasterdao.findByCriteria(Order.asc("domainName"));
			totaRecord = domainmasterdao.getRowCountWithSort(sortOrderStrVal);
			domainMaster = domainmasterdao.findWithLimit(sortOrderStrVal,0,100);
			dmRecords.append("<table  id='domainTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			//dmRecords.append("<th width='55%'>Domain Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblDomName,sortOrderFieldName,"domainName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='55%' valign='top'>"+responseText+"</th>");
			
			//dmRecords.append("<th width='15%'>PD Report</th>");
			responseText=PaginationAndSorting.responseSortingLink(headPDRep,sortOrderFieldName,"displayInPDReport",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
			
			//dmRecords.append("<th width='15%'>Status</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='15%' valign='top'>"+responseText+"</th>");
			
			dmRecords.append("<th width='15%'>"+lblAct+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(domainMaster.size()==0)
				dmRecords.append("<tr><td colspan='6'>"+lblNoDomain+"</td></tr>" );
			System.out.println("No of Records "+domainMaster.size());

			for (DomainMaster domainMasterDetail : domainMaster) 
			{
				dmRecords.append("<tr>" );
				dmRecords.append("<td>"+domainMasterDetail.getDomainName()+"</td>");
				dmRecords.append("<td>");
				if(domainMasterDetail.getDisplayInPDReport()	==	1)
					dmRecords.append(lblYes);
				else
					dmRecords.append(lblNo);
				dmRecords.append("</td>");
				dmRecords.append("<td>");
				if(domainMasterDetail.getStatus().equalsIgnoreCase("A"))
					dmRecords.append(optAct);
				else
					dmRecords.append(optInActiv);
				dmRecords.append("</td>");
				dmRecords.append("<td>");
				boolean pipeFlag=false;
				if(roleAccess.indexOf("|2|")!=-1){
					dmRecords.append("<a href='javascript:void(0);' onclick='return editDomain("+domainMasterDetail.getDomainId()+")'>"+lblEdit+"</a>");
					pipeFlag=true;
				}else if(roleAccess.indexOf("|4|")!=-1){
					dmRecords.append("<a href='javascript:void(0);' onclick='return editDomain("+domainMasterDetail.getDomainId()+")'>"+lnkV+"</a>");
					pipeFlag=true;
				}
				if(roleAccess.indexOf("|7|")!=-1){
					if(pipeFlag)dmRecords.append(" | ");
					if(domainMasterDetail.getStatus().equalsIgnoreCase("A"))
						dmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateDomain("+domainMasterDetail.getDomainId()+",'I')\">"+lblDeactivate+"");
					else
						dmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateDomain("+domainMasterDetail.getDomainId()+",'A')\">"+lblActivate+"");
				}else{
					dmRecords.append("&nbsp;");
				}
				dmRecords.append("</td>");
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			//System.out.println(dmRecords.toString());
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	/* ===============      saveDomain        =========================*/
	public int saveDomain(Integer domainId,String domainName,Short PDReportValue,String domainStatus,Double multiplier,String domainUId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		DomainMaster domainMaster=new DomainMaster();
		try
		{
			domainMaster.setDomainName(domainName);
			domainMaster.setMultiplier(multiplier);
			domainMaster.setStatus(domainStatus);
			if(domainUId!=null && !domainUId.equals(""))
				domainUId = domainUId.toUpperCase();
			domainMaster.setDomainUId(domainUId);
			domainMaster.setDisplayInPDReport(PDReportValue);

			if(domainId	!=	null)
			{	
				domainMaster.setDomainId(domainId);
			}
			/*======= Check For Unic Domain ==========*/
			List<DomainMaster> DupdomainMaster=domainmasterdao.checkDuplicateDomainName(domainName,domainId);
			List<DomainMaster> domainMasterList = null;
			if(domainId==null)
				domainMasterList = domainmasterdao.findDomainsByUId(domainUId);
			
			int size = DupdomainMaster.size();
			if(size>0)
				return 3;
			if(domainMasterList!=null && domainMasterList.size()>0)
				return 4;//if duplicate domainUId
			
			domainmasterdao.makePersistent(domainMaster);
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 2;
		}

		return 1;
	}
	/* ===============      Activate Deactivate Domain        =========================*/
	@Transactional(readOnly=false)
	public boolean activateDeactivateDomain(int domainId,String status)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		try{
			DomainMaster domainMaster	=	domainmasterdao.findById(domainId, false, false);
			domainMaster.setStatus(status);
			domainmasterdao.makePersistent(domainMaster);
			
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/* ===============    Edit Domain Functionality        =========================*/
	@Transactional(readOnly=false)
	public DomainMaster getDomainById(int domainId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		DomainMaster domainMaster =null;
		try
		{
			domainMaster	=	domainmasterdao.findById(domainId, false, false);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		return domainMaster;
	}
}
