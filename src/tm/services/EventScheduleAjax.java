package tm.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.CandidateEventDetails;
import tm.bean.EventDetails;
import tm.bean.EventFacilitatorsList;
import tm.bean.EventParticipantsList;
import tm.bean.EventSchedule;
import tm.bean.SlotSelection;
import tm.bean.TeacherDetail;
import tm.bean.master.TimeZoneMaster;
import tm.bean.user.UserMaster;
import tm.dao.CandidateEventDetailsDAO;
import tm.dao.EventDetailsDAO;
import tm.dao.EventParticipantsListDAO;
import tm.dao.EventScheduleDAO;
import tm.dao.SlotSelectionDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.TimeZoneMasterDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class EventScheduleAjax {
	
	       String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	public CandidateEventDetailsDAO candidateEventDetailsDAO; 
	
	@Autowired
	public TimeZoneMasterDAO timeZoneMasterDAO; 
	
	
	@Autowired
	public EventDetailsDAO eventDetailsDAO; 
	public void setEventDetailsDAO(EventDetailsDAO eventDetailsDAO) {
		this.eventDetailsDAO = eventDetailsDAO;
	}
	@Autowired
	public EventScheduleDAO eventScheduleDAO;
	public void setEventScheduleDAO(EventScheduleDAO eventScheduleDAO) {
		this.eventScheduleDAO = eventScheduleDAO;
	}
	@Autowired
	public SlotSelectionDAO slotSelectionDAO;
	public void SlotSelectionDAO(SlotSelectionDAO slotSelectionDAO) {
		this.slotSelectionDAO = slotSelectionDAO;
	}
	
	@Autowired
	private EventParticipantsListDAO eventParticipantsListDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	 
public String saveSchedule(String scheduleId,String eventId,String timezone,String eventStartDateArr,String starthrArr,String starttimeformArr,String locationArr,String validDays,String endhrArr,String endtimeformArr, Boolean singlebookingonly,Integer noOfCandidate)
{
	System.out.println(" ============saveSchedule============= "+locationArr);
	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userSession = (UserMaster) session.getAttribute("userMaster");
		
		try
		{
			
			EventSchedule eventSchedule = null; 
			EventDetails eventDetails = eventDetailsDAO.findById(Integer.parseInt(eventId), false, false);
			
			if(eventDetails.getEventTypeId().getEventTypeId()==1)
			{
				if(scheduleId!=null && !scheduleId.equals(""))
					eventSchedule = eventScheduleDAO.findById(Integer.parseInt(scheduleId), false, false);
				
				System.out.println(" eventSchedule "+eventSchedule);
				
				
				if(eventSchedule==null)
					eventSchedule = new EventSchedule();
				
				System.out.println(" validDays>>>>>>> "+validDays);
				
				if(validDays!=null && !validDays.equals(""))
					eventSchedule.setValidity(Integer.parseInt(validDays));
				else
					eventSchedule.setValidity(0);
				eventSchedule.setEventDetails(eventDetails);
				eventSchedule.setCreatedDateTime(new Date());
				eventSchedule.setCreatedBY(userSession);
				eventSchedule.setStatus("A");
				eventScheduleDAO.makePersistent(eventSchedule);
				
			}
			else
			{
				System.out.println(" eventStartDateArr...."+eventStartDateArr+".........");
				
				String[] eScheduleIdArr = scheduleId.split(",");
				String[] eStartDateArr = eventStartDateArr.split(",");
				String[] estarthrArr = starthrArr.split(","); 
				String[] estarttimeformArr = starttimeformArr.split(",");
				String[] eendhrArr = endhrArr.split(","); 
				String[] eendtimeformArr = endtimeformArr.split(",");
				String[] elocationArr = locationArr.split("##");

				TimeZoneMaster timeZoneMaster = null;
				
				if(timezone!=null && !timezone.equals(""))
					timeZoneMaster = timeZoneMasterDAO.findById(Integer.parseInt(timezone), false, false);
				
			//	System.out.println("eStartDateArr.length::"+eStart"+Utility.getLocaleValuePropByKey("msgDate", locale)+"Arr.length+" eStartDateArr[0]"+eStartDateArr[0]+"..........");
				
				/*if(!eStartDateArr[0].equals(""))
				{*/
					for(int i=0;i<eStartDateArr.length;i++)
					{
						if(eStartDateArr[i]!=null && !eStartDateArr[i].equals("") && estarthrArr[i]!=null && !estarthrArr[i].equals("") && estarttimeformArr[i]!=null && !estarttimeformArr[i].equals("") && eendhrArr[i]!=null && !eendhrArr[i].equals("") && eendtimeformArr[i]!=null && !eendtimeformArr[i].equals(""))
						{
							if(eScheduleIdArr.length>i)
							{
								eventSchedule = eventScheduleDAO.findById(Integer.parseInt(eScheduleIdArr[i]), false, false);
							}
							else	
								eventSchedule = new EventSchedule();
							
							if(singlebookingonly!=null){
								eventSchedule.setSinglebookingonly(singlebookingonly);
							}else{
								eventSchedule.setSinglebookingonly(null);
							}
							
							//noOfCandidate
							if(noOfCandidate!=null){
								eventSchedule.setNoOfCandidatePerSlot(noOfCandidate);
							}else{
								eventSchedule.setNoOfCandidatePerSlot(null);
							}
							eventSchedule.setEventDetails(eventDetails);
							eventSchedule.setCreatedDateTime(new Date());
							eventSchedule.setEventDateTime(Utility.getCurrentDateFormart(eStartDateArr[i]));
							eventSchedule.setEventStartTime(estarthrArr[i]);
							eventSchedule.setEventStartTimeFormat(estarttimeformArr[i]);
							eventSchedule.setEventEndTime(eendhrArr[i]);
							eventSchedule.setEventEndTimeFormat(eendtimeformArr[i]);
							
							if(!elocationArr[i].equals("null") && !elocationArr[i].equals(""))
								eventSchedule.setLocation(elocationArr[i]);
							else
								eventSchedule.setLocation(null);
							
							eventSchedule.setStatus("A");
							eventSchedule.setCreatedBY(userSession);
							eventSchedule.setValidity(0);
							eventSchedule.setTimeZoneMaster(timeZoneMaster);
							
							eventScheduleDAO.makePersistent(eventSchedule);
						}
					}
				}
				
			/*}*/
			
		}catch(Exception e){e.printStackTrace();}
		
		
		
		/*try {
			EventDetails eventDetails = eventDetailsDAO.findById(Integer
					.parseInt(eventId), false, false);
			SimpleDateFormat formats = new SimpleDateFormat("MM-dd-yyyy");
			Date startDate = formats.parse(eventStartDate);
			Date endDate = formats.parse(eventEndDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(startDate);
			long diff = endDate.getTime() - startDate.getTime();
			diff = diff / (1000 * 60 * 60 * 24);
			System.out.println(diff);
			String days[] = selecteddays.split(",");
			List seldays = (List) Arrays.asList(days);

			// System.out.println(">>>>>>>>>>>>>>>>>>>timeZoneId :: "+timeZoneId);
			TimeZoneMaster timeZoneMaster = null;
			if (timeZoneId != 0 && timeZoneId != null)
				timeZoneMaster = timeZoneMasterDAO.findById(timeZoneId, false,
						false);

			for (int i = 0; i <= diff; i++) {
				EventSchedule eventSchedule = null;
				;
				if (scheduleId != null && !scheduleId.equals("")) {
					eventSchedule = eventScheduleDAO.findById(Integer
							.parseInt(scheduleId), false, false);
				} else {
					eventSchedule = new EventSchedule();
				}
				eventSchedule.setEventDetails(eventDetails);
				eventSchedule.setEventDateTime(startDate);
				if (eventDetails.getEventTypeId().getEventTypeId() == 1) {
					eventSchedule.setValidity(validity);
					eventSchedule.setEventStartTime("");
					eventSchedule.setEventStartTimeFormat("");
					eventSchedule.setEventEndTime("");
					eventSchedule.setEventEndTimeFormat("");
					eventSchedule.setLocation("");
				} else {
					eventSchedule.setEventStartTime(starthr + ":" + startmin);
					eventSchedule.setEventStartTimeFormat(starttimeform);
					eventSchedule.setEventEndTime(endhr + ":" + endmin);
					eventSchedule.setEventEndTimeFormat(endtimeform);
					eventSchedule.setLocation(location);
					eventSchedule.setTimeZoneMaster(timeZoneMaster);
				}

				eventSchedule.setStatus("A");
				eventSchedule.setCreatedBY(userSession);
				eventSchedule.setCreatedDateTime(new Date());
				int getDay = cal.get(Calendar.DAY_OF_WEEK);
				System.out.println(startDate + " " + getDay);
				if (seldays.size() == 1
						|| seldays.contains(String.valueOf(getDay))) {
					eventScheduleDAO.makePersistent(eventSchedule);
					scheduleId = null;
				}
				cal.add(Calendar.DATE, 1);
				startDate = cal.getTime();
			}
		} catch (Exception e) {
			System.out.println(e);
		}*/

return "success";	
}

public String[] geteventSchedule(int eventId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	
	String[] arr = new String[2];
	
	StringBuffer sb	=	new StringBuffer();
	StringBuffer sb1	=	null; 
	StringBuffer sb2	=	null; 
	StringBuffer sb3	=	null; 
	StringBuffer sb4	=	null; 
	StringBuffer sb5	=	null; 
	StringBuffer sb6	=	null; 
	StringBuffer sb7	=	null; 
	
	List<EventSchedule> eventScheduleList = new ArrayList<EventSchedule>();
	
	try
	{
		EventDetails eventDetails = eventDetailsDAO.findById(eventId, false, false);
		
		List<TimeZoneMaster>   listTimeZoneMasters=timeZoneMasterDAO.getAllTimeZone();
		
		String chkDateAndTime = eventDetails.getEventSchedulelId().getEventScheduleName().toLowerCase().trim();
		int chkDATFlag = 0;
		
		
		if(eventDetails.getEventTypeId().getEventTypeId()!=1)
		{
			if(chkDateAndTime.equals(("Fixed Date and Time").toLowerCase().trim()))
				chkDATFlag = 1;
			else if(chkDateAndTime.equals(("Multiple Fixed Date and Time").toLowerCase().trim()))
				chkDATFlag = 2;
			else
				chkDATFlag = 3;
		}
		
		
		if(eventDetails!=null)
		{
			eventScheduleList = eventScheduleDAO.findByEventDetail(eventDetails);
			
			//Map<EventId"##"scheduleId,True>
			Map<String, Boolean> mapOfSlotSelections=new HashMap<String, Boolean>();
			//Map<EventId,True>
			Map<String, Boolean> mapOfRegParticipant=new HashMap<String, Boolean>();
			
			//Logic for candidate icon grean/blue
			if(eventScheduleList!=null && eventScheduleList.size()>0)
			{
				Criterion criterion = Restrictions.eq("eventDetails",eventDetails);
				List<SlotSelection> listSlotSelections= new ArrayList<SlotSelection>();
				List<CandidateEventDetails> listCandidateEventDetails=new ArrayList<CandidateEventDetails>();
				
				listSlotSelections=slotSelectionDAO.findByEventScheduleList(eventScheduleList);
				listCandidateEventDetails=candidateEventDetailsDAO.findByCriteria(criterion);
				
				if(listSlotSelections!=null && listSlotSelections.size()>0){
					for(SlotSelection ss : listSlotSelections){
						mapOfSlotSelections.put(ss.getEventSchedule().getEventScheduleId()+"##"+ss.getEventSchedule().getEventDetails().getEventId(),true);
					}
				}
				
				if(listCandidateEventDetails!=null && listCandidateEventDetails.size()>0){
					for(CandidateEventDetails ced : listCandidateEventDetails){
						mapOfRegParticipant.put(ced.getEventDetails().getEventId()+"", true);
					}
				}
			}
			
		//	if(eventDetails.getEventTypeId().getEventTypeId()==2)
		//	{
			int countRecord = eventScheduleList.size();
			String timezone = "";
			String eventStartDate = "";
			String starthr = "";
			String startmin = "";
			String starttimeform = "";
			String location = "";
			String eventEndDate = "";
			String endhr = "";
			String endtmin = "";
			String endtimeform = "";
			String eventScheduleId = "";
			StringBuffer singlebookingData=new StringBuffer(); 
			
			if(eventScheduleList.size()>0)
			{
				arr[1] = eventScheduleList.size()+"";
				
				
				sb7	=	new StringBuffer();
				//Selected Zone
				 sb7.append("<select id='timezoneforevent' name='timezoneforevent' class='form-control help-inline' >");
				 sb7.append("<option value='' selected='selected'></option>");
				 	for(TimeZoneMaster iZoneMaster:listTimeZoneMasters)
				 	{	
				 		if(eventScheduleList.get(0).getTimeZoneMaster().getTimeZoneId().equals(iZoneMaster.getTimeZoneId()))
				 			sb7.append("<option value='"+iZoneMaster.getTimeZoneId()+"' selected='selected'>"+iZoneMaster.getTimeZoneName()+" ("+iZoneMaster.getTimeZoneShortName()+")</option>");
				 		else
				 			sb7.append("<option value='"+iZoneMaster.getTimeZoneId()+"'>"+iZoneMaster.getTimeZoneName()+" ("+iZoneMaster.getTimeZoneShortName()+")</option>");
				 	}
				 sb7.append("</select>");
				
				 if(chkDATFlag==3){
					 singlebookingData.append("<div class='col-sm-6 col-md-6' style='margin-top: 20px;' id='singlebookingonlyDiv'>");
					 singlebookingData.append("<label class='checkbox inline'>");
						
					 if(eventScheduleList.get(0).getSinglebookingonly()!=null && eventScheduleList.get(0).getSinglebookingonly()){
							singlebookingData.append("<input type='checkbox' id='singlebookingonly' name='singlebookingonly' checked onchange='checkforSingleBooking();' />");
						}else if(eventScheduleList.get(0).getSinglebookingonly()!=null && !eventScheduleList.get(0).getSinglebookingonly()){
							singlebookingData.append("<input type='checkbox' id='singlebookingonly' name='singlebookingonly' onchange='checkforSingleBooking();' />");
						}
					 
						singlebookingData.append(Utility.getLocaleValuePropByKey("msgEventSchedAjax1", locale)); 
					singlebookingData.append("</label>");
					singlebookingData.append("</div>"); 
				 
				 String hide1="";
				 String noofCadidate="";
				 if(eventScheduleList.get(0).getSinglebookingonly()!=null && eventScheduleList.get(0).getSinglebookingonly()){
					 hide1="hide";
				 }
				 if(eventScheduleList.get(0).getNoOfCandidatePerSlot()!=null && eventScheduleList.get(0).getNoOfCandidatePerSlot()!=0){
					 noofCadidate=eventScheduleList.get(0).getNoOfCandidatePerSlot()+"";
				 }
				 
				 singlebookingData.append("<div class='col-sm-2 col-md-2 "+hide1+"' id='noofcandidateDiv'>");
				    singlebookingData.append("<label'>No of Candidate(s)</label><a href='#' id='noofcandidate' rel='tooltip' data-original-title='For an interview where there are more candidates during one time slot, please indicate that number here.'><img width='15' height='15' alt='' src='images/qua-icon.png'></a>");
				       singlebookingData.append("<input type='text' onkeypress='return checkForInt(event)' class='help-inline form-control' id='noofcandidateperslot' name='noofcandidateperslot' maxlength='8' value='"+noofCadidate+"' />");
				    singlebookingData.append("</label>");
				 singlebookingData.append("</div>");
				 
				 }
				 
				 timezone = sb7.toString();
				 
				//Start Row
					sb.append("<div class='row'>");
						sb.append("<div class='col-sm-12 col-md-12'>");
							sb.append("<div class='col-sm-4 col-md-4'>");
								sb.append("<label>"+Utility.getLocaleValuePropByKey("lblTimeZone", locale)+"<span class='required'>*</span></label>");
								sb.append(timezone);
							sb.append("</div>");
							if(chkDATFlag==3)//adding single slot check
								sb.append(singlebookingData.toString());
						sb.append("</div>");
					sb.append("</div>");
					//End Row
				 
				
				for(int i=0;i<eventScheduleList.size();i++)
				{
					//String[] selectedStartTimeFormat = eventScheduleList.get(i).getEventStartTimeFormat().split(":");
					
					String selectedStartHr  =  eventScheduleList.get(i).getEventStartTime();
				//	String selectedStartMin = ""+selectedStartTimeFormat[1];
					String selectedStartFmt = eventScheduleList.get(i).getEventStartTimeFormat();
					
				//	String[] selectedEndTimeFormat = eventScheduleList.get(i).getEventEndTimeFormat().split(":");
					
					String selectedEndHr  = eventScheduleList.get(i).getEventEndTime();
				//	String selectedEndMin = ""+selectedEndTimeFormat[1];
					String selectedEndFmt = eventScheduleList.get(i).getEventEndTimeFormat();
					
					String selectedLocation = ""; 
					
					if(eventScheduleList.get(i).getLocation()!=null)
						selectedLocation = eventScheduleList.get(i).getLocation();
					else
						selectedLocation = "";
					
					if(i==0)
					{
						System.out.println("11111111111111111111111111");
						
						sb1	=	new StringBuffer();
						sb2	=	new StringBuffer();
						sb3	=	new StringBuffer();
						sb4	=	new StringBuffer();
						sb5	=	new StringBuffer();
						sb6	=	new StringBuffer();
						
						
						 //Selected start HR
						 sb1.append("<select class='form-control help-inline' id='starthr' name='starthr'>");
						 	sb1.append("<option value='' selected='selected'>"+Utility.getLocaleValuePropByKey("lbloptionHH", locale)+"</option>");
						 	for(int m=1; m<=12;m++){
								String min=m+"";
								if(m!=10 && m!=11 && m!=12){
									min="0"+min;
								}
						 		for(int s=0; s<60;s=s+15){
						 			String sec=s+"";
						 			if(s==0){
						 				 sec="00";
						 			}
						 			if(!(m==0 && s==0)){
							 			if(selectedStartHr.equalsIgnoreCase(""+min+":"+sec+"")){
							 				sb1.append("<option selected='selected' value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
							 			}else{
							 				sb1.append("<option value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
							 			}
						 			}
						 			//if(m==12)break;
						 		}
						 	}
						
						 	sb1.append("</select>");
						 ////////////////
						 
						 //Selected start Time Format
						 sb3.append("<select class='form-control help-inline' id='starttimeform' name='startminform' class='span3'>");
						// sb3.append("<option value='' selected='selected'>AM/PM</option>");
						 if(selectedStartFmt.equals("AM"))
							 sb3.append("<option value='AM' selected='selected' >AM</option>");
						 else
							 sb3.append("<option value='AM'>AM</option>");
						 
						 if(selectedStartFmt.equals("PM"))
							 sb3.append("<option value='PM' selected='selected'>PM</option>");
						 else
							 sb3.append("<option value='PM'>PM</option>");
						 sb3.append("</select>");
						 /////////////////////
						 
							sb4.append("<select class='form-control help-inline' id='endhr' name='endhr' class='span3'>");
							 		sb4.append("<option value='' selected='selected'>"+Utility.getLocaleValuePropByKey("lbloptionHH", locale)+"</option>");
							 		for(int m=1; m<=12;m++){
										String min=m+"";
										if(m!=10 && m!=11 && m!=12){
											min="0"+min;
										}
								 		for(int s=0; s<60;s=s+15){
								 			String sec=s+"";
								 			if(s==0){
								 				 sec="00";
								 			}
								 			if(!(m==0 && s==0)){
									 			if(selectedEndHr.equalsIgnoreCase(""+min+":"+sec+"")){
									 				sb4.append("<option selected='selected' value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
									 			}else{
									 				sb4.append("<option value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
									 			}
								 			}
								 			//if(m==12)break;
								 		}
								 	}
							 	sb4.append("</select>");
						 
					 
							//Selected end Time Format
							 sb6.append("<select class='form-control help-inline' id='endtimeform' name='endtimeform' class='span3'>");
							// sb6.append("<option value='' selected='selected'>AM/PM</option>");
							 if(selectedEndFmt.equals("AM"))
								 sb6.append("<option value='AM' selected='selected' >AM</option>");
							 else
								 sb6.append("<option value='AM'>AM</option>");
							 
							 if(selectedEndFmt.equals("PM"))
								 sb6.append("<option value='PM' selected='selected'>PM</option>");
							 else
								 sb6.append("<option value='PM'>PM</option>");
							 sb6.append("</select>");
							 /////////////////////
						 
						 
						 
						 eventScheduleId = "<input type='hidden' id='eventScheduleId' name='eventScheduleId' value='"+eventScheduleList.get(i).getEventScheduleId()+"' >";
						 location = "<input class='form-control help-inline' type='text' id='locationfirst' name='locationfirst' maxlength='100' value='"+selectedLocation+"' />";
						 eventStartDate = "<input type='text' id='eventStartDate' name='eventStartDate' maxlength='0' class='form-control' value='"+Utility.getCalenderDateFormart(eventScheduleList.get(i).getEventDateTime()+"")+"' style='width:148px;' onfocus='call(this);'>";
						 eventEndDate = "";//"<input type='text' id='eventEndDate' name='eventEndDate' maxlength='0' class='form-control' value='"+eventScheduleList.get(i).getEventEndTime()+"'/>";
						 
						 System.out.println(" starthr >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+starthr);
						 
						 System.out.println(" endhr >>>>>>>>>>>>>>> "+endhr);
						 
						 starthr = sb1.toString();
						 startmin = sb2.toString();
						 starttimeform = sb3.toString();
						 endhr = sb4.toString();
						 endtmin = sb5.toString();
						 endtimeform =  sb6.toString();
						 
						 	/*sb.append("<script type='text/javascript'>var cal = Calendar.setup({ onSelect: function(cal) { cal.hide()}, showTime: true});");
							sb.append("cal.manageFields('eventStartDate', 'eventStartDate', '%m-%d-%Y');");
							sb.append("cal.manageFields('eventEndDate', 'eventEndDate', '%m-%d-%Y');");
							sb.append("</script>");*/
							
						 
					}
					else
					{
						System.out.println("222222222222222222");
						
						sb1	=	new StringBuffer();
						sb2	=	new StringBuffer();
						sb3	=	new StringBuffer();
						sb4	=	new StringBuffer();
						sb5	=	new StringBuffer();
						sb6	=	new StringBuffer();
						sb7	=	new StringBuffer();
						 
						/*eventStartDate = "";
						starthr = "";
						startmin = "";
						starttimeform = "";
						location = "";
						eventEndDate = "";
						endhr = "";
						endtmin = "";
						endtimeform = "";
						eventScheduleId = "";*/
						
						//Selected Zone
						/* sb7.append("<select id='timezone' name='timezone' class='form-control' >");
						 sb7.append("<option value='' selected='selected'></option>");
						 	for(TimeZoneMaster iZoneMaster:listTimeZoneMasters)
						 	{	System.out.println(eventScheduleList.get(i).getTimeZoneMaster().getTimeZoneId().equals(iZoneMaster.getTimeZoneId()));
						 		if(eventScheduleList.get(i).getTimeZoneMaster().getTimeZoneId().equals(iZoneMaster.getTimeZoneId()))
						 			sb7.append("<option value='"+iZoneMaster.getTimeZoneId()+"' selected='selected'>"+iZoneMaster.getTimeZoneShortName()+"</option>");
						 		else
						 			sb7.append("<option value='"+iZoneMaster.getTimeZoneId()+"'>"+iZoneMaster.getTimeZoneShortName()+"</option>");
						 	}
						 sb7.append("</select>");*/
						 
						 //Selected start HR
						
						
						
						
						 sb1.append("<select class='form-control help-inline' id='starthr"+i+"' name='starthr"+i+"' class='span3'>");
						 	sb1.append("<option value='' selected='selected'>"+Utility.getLocaleValuePropByKey("lbloptionHH", locale)+"</option>");
						 	for(int m=1; m<=12;m++){
								String min=m+"";
								if(m!=10 && m!=11 && m!=12){
									min="0"+min;
								}
						 		for(int s=0; s<60;s=s+15){
						 			String sec=s+"";
						 			if(s==0){
						 				 sec="00";
						 			}
						 			if(!(m==0 && s==0)){
							 			if(selectedStartHr.equalsIgnoreCase(""+min+":"+sec+"")){
							 				sb1.append("<option selected='selected' value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
							 			}else{
							 				sb1.append("<option value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
							 			}
						 			}
						 			//if(m==12)break;
						 		}
						 	}
						 sb1.append("</select>");
						 ////////////////
						 
						 //Selected start Time Format
						 sb3.append("<select class='form-control help-inline' id='starttimeform"+i+"' name='startminform"+i+"' class='span3'>");
						// sb3.append("<option value='' selected='selected'>AM/PM</option>");
						 if(selectedStartFmt.equals("AM"))
							 sb3.append("<option value='AM' selected='selected' >AM</option>");
						 else
							 sb3.append("<option value='AM'>AM</option>");
						 
						 if(selectedStartFmt.equals("PM"))
							 sb3.append("<option value='PM' selected='selected'>PM</option>");
						 else
							 sb3.append("<option value='PM'>PM</option>");
						 sb3.append("</select>");
						 /////////////////////
						 
						 
						 sb4.append("<select class='form-control help-inline' id='endhr"+i+"' name='endhr"+i+"' class='span3'>");
					 		sb4.append("<option value='' selected='selected'>"+Utility.getLocaleValuePropByKey("lbloptionHH", locale)+"</option>");
					 		for(int m=1; m<=12;m++){
								String min=m+"";
								if(m!=10 && m!=11 && m!=12){
									min="0"+min;
								}
						 		for(int s=0; s<60;s=s+15){
						 			String sec=s+"";
						 			if(s==0){
						 				 sec="00";
						 			}
						 			if(!(m==0 && s==0)){
							 			if(selectedEndHr.equalsIgnoreCase(""+min+":"+sec+"")){
							 				sb4.append("<option selected='selected' value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
							 			}else{
							 				sb4.append("<option value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
							 			}
						 			}
						 			//if(m==12)break;
						 		}
						 	}
						 sb4.append("</select>");
						 
						 
						//Selected end Time Format
						 sb6.append("<select class='form-control help-inline' id='endtimeform"+i+"' name='endtimeform"+i+"' class='span3'>");
						// sb6.append("<option value='' selected='selected'>AM/PM</option>");
						 if(selectedEndFmt.equals("AM"))
							 sb6.append("<option value='AM' selected='selected' >AM</option>");
						 else
							 sb6.append("<option value='AM'>AM</option>");
						 
						 if(selectedEndFmt.equals("PM"))
							 sb6.append("<option value='PM' selected='selected'>PM</option>");
						 else
							 sb6.append("<option value='PM'>PM</option>");
						 sb6.append("</select>");
						 /////////////////////
						 
						 
						 //timezone = sb7.toString();
						 eventScheduleId = "<input type='hidden' id='eventScheduleId"+i+"' name='eventScheduleId"+i+"' value='"+eventScheduleList.get(i).getEventScheduleId()+"' >";
						 location = "<input class='form-control help-inline' type='text' id='location"+i+"' name='location"+i+"' maxlength='100' value='"+selectedLocation+"' />";
						 eventStartDate = "<input type='text' id='eventStartDate"+i+"' name='eventStartDate"+i+"' maxlength='0' class='form-control' value='"+Utility.getCalenderDateFormart(eventScheduleList.get(i).getEventDateTime()+"")+"' style='width:148px;' onfocus='call(this);'>";
						 eventEndDate = "<input type='text' id='eventEndDate"+i+"' name='eventEndDate"+i+"' maxlength='0' class='form-control' value='"+eventScheduleList.get(i).getEventEndTime()+"' onfocus='call(this);'/>";
						 starthr = sb1.toString();
						 
						 startmin = sb2.toString();
						 starttimeform = sb3.toString();
						 endhr = sb4.toString();
						// endtmin = sb5.toString();
						 endtimeform =  sb6.toString();
						 
						 
						 System.out.println(" endtimeform "+endtimeform);
						 
						 	/*sb.append("<script type='text/javascript'>var cal = Calendar.setup({ onSelect: function(cal) { cal.hide()}, showTime: true});");
							sb.append("cal.manageFields('eventStartDate"+i+"', 'eventStartDate"+i+"', '%m-%d-%Y');");
							sb.append("cal.manageFields('eventEndDate"+i+"', 'eventEndDate"+i+"', '%m-%d-%Y');");
							sb.append("</script>");*/
					}
					//Start Row
					/*sb.append("<div class='row'>");
						sb.append("<div class='col-sm-10 col-md-10'>");
							sb.append("<div class='col-sm-3 col-md-3'>");
								sb.append("<label>Time Zone<span class='required'>*</span></label>");
								sb.append(timezone);
							sb.append("</div>");
						sb.append("</div>");
					sb.append("</div>");*/
					//End Row
					
					//Start Row
					sb.append("<div class='row' style='padding-left: 16px;'>");
						sb.append("<div class='col-sm-16 col-md-16'>");
							sb.append("<div class='col-sm-2 col-md-2'>");
								sb.append("<label>"+Utility.getLocaleValuePropByKey("msgDate", locale)+"<span class='required'>*</span></label>"+eventScheduleId);
								sb.append(eventStartDate);
							sb.append("</div>");
							
							sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
								sb.append("<label>"+Utility.getLocaleValuePropByKey("lblStartTime", locale)+"<span class='required'>*</span></label>");
								sb.append(starthr);
							sb.append("</div>");
						
							sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
								sb.append("<label>AM/PM<span class='required'>*</span></label>");
								sb.append(starttimeform);
							sb.append("</div>");
							
							sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
								sb.append("<label> "+Utility.getLocaleValuePropByKey("lblEndTime", locale)+"<span class='required'>*</span></label>");
								sb.append(endhr);
							sb.append("</div>");
					
							sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
								sb.append("<label>AM/PM<span class='required'>*</span></label>");
								sb.append(endtimeform);
							sb.append("</div>");

							sb.append("<div class='col-sm-3 col-md-3 left10' id='loctext' style='width: 297px;'>");
								sb.append("<label>"+Utility.getLocaleValuePropByKey("msgEventSchedAjax2", locale)+"</label>");
								sb.append(location);
							sb.append("</div>");
							
						sb.append("<div class='col-sm-1 col-md-1 top5 left0' style='margin-top: 34px;padding-left:10px;padding-right:0px;'>");
							  sb.append("<label></label>");
						
						//to see candidate
							  sb.append("<a href='javascript:void(0)' id='viewConToolTip"+i+"' rel='tooltip' data-original-title='View Candidate' onclick=\"viewCandidates("+eventScheduleList.get(i).getEventScheduleId()+")\">"); 
								  if(mapOfSlotSelections.get(eventScheduleList.get(i).getEventScheduleId()+"##"+eventScheduleList.get(i).getEventDetails().getEventId())!=null && mapOfSlotSelections.get(eventScheduleList.get(i).getEventScheduleId()+"##"+eventScheduleList.get(i).getEventDetails().getEventId())){
									  sb.append("<img src='images/option06_green.png' style='width:21px; height:21px;'>");
									  sb.append("<script>");
									  if(i==0)
									  {
										  sb.append("$('#eventStartDate').prop('disabled',true);" +
											  		"$('#starthr').prop('disabled',true);" +
											  		"$('#starttimeform').prop('disabled',true);" +
											  		"$('#endhr').prop('disabled',true);" +
											  		"$('#endtimeform').prop('disabled',true);" +
											  		"$('#locationfirst').prop('disabled',true);");
									  }
									  else
									  {
										  sb.append("$('#eventStartDate"+i+"').prop('disabled',true);" +
										  			"$('#starthr"+i+"').prop('disabled',true);" +
										  			"$('#starttimeform"+i+"').prop('disabled',true);" +
										  			"$('#endhr"+i+"').prop('disabled',true);" +
										  			"$('#endtimeform"+i+"').prop('disabled',true);" +
										  			"$('#location"+i+"').prop('disabled',true);");
									  }
									  sb.append("</script>");
								  }else if(mapOfRegParticipant.get(eventScheduleList.get(i).getEventDetails().getEventId()+"")!=null && mapOfRegParticipant.get(eventScheduleList.get(i).getEventDetails().getEventId()+"")){
									  sb.append("<img src='images/option06_green.png' style='width:21px; height:21px;'>");
									  sb.append("<script>");
									  if(i==0)
									  {
										  sb.append("$('#eventStartDate').prop('disabled',true);" +
											  		"$('#starthr').prop('disabled',true);" +
											  		"$('#starttimeform').prop('disabled',true);" +
											  		"$('#endhr').prop('disabled',true);" +
											  		"$('#endtimeform').prop('disabled',true);" +
											  		"$('#locationfirst').prop('disabled',true);");
									  }
									  else
									  {
										  sb.append("$('#eventStartDate"+i+"').prop('disabled',true);" +
										  			"$('#starthr"+i+"').prop('disabled',true);" +
										  			"$('#starttimeform"+i+"').prop('disabled',true);" +
										  			"$('#endhr"+i+"').prop('disabled',true);" +
										  			"$('#endtimeform"+i+"').prop('disabled',true);" +
										  			"$('#location"+i+"').prop('disabled',true);");
									  }
									  sb.append("</script>");
								  }else{
									  sb.append("<img src='images/option06.png' style='width:21px; height:21px;'>"); 
								  }
							  sb.append("</a>");
							  sb.append("<script>$('#viewConToolTip"+i+"').tooltip();</script>");
							  
							//delete schedule
							  if(mapOfSlotSelections.get(eventScheduleList.get(i).getEventScheduleId()+"##"+eventScheduleList.get(i).getEventDetails().getEventId())!=null && mapOfSlotSelections.get(eventScheduleList.get(i).getEventScheduleId()+"##"+eventScheduleList.get(i).getEventDetails().getEventId())){
							  }else if(mapOfRegParticipant.get(eventScheduleList.get(i).getEventDetails().getEventId()+"")!=null && mapOfRegParticipant.get(eventScheduleList.get(i).getEventDetails().getEventId()+"")){
							  }else{
								  sb.append("&nbsp;<a href='javascript:void(0)' id='deleteConToolTip"+i+"' rel='tooltip' data-original-title='Remove slot' onclick=\"deleteSchedulePopUp("+eventScheduleList.get(i).getEventScheduleId()+")\">");
								    sb.append("<img src='images/red_cancel.png' style='width:15px; height:15px;'>"); 
								  sb.append("</a>");
								  sb.append("<script>$('#deleteConToolTip"+i+"').tooltip();</script>");
							  }
							  //Add candidate on Slot
							  if(chkDATFlag==3){
								  sb.append("&nbsp;<a href='javascript:void(0)' id='addConToolTip"+i+"' rel='tooltip' data-original-title='Add participant(s)' onclick=\"addCandidateSchedulePopUp("+eventScheduleList.get(i).getEventScheduleId()+")\">");
								    sb.append("<i class='fa fa-users icon-large'></i>"); 
								  sb.append("</a>");
								  sb.append("<script>$('#addConToolTip"+i+"').tooltip();</script>");
							  }
							  
							sb.append("</div>"); 
							
						sb.append("</div>");
					sb.append("</div>");
					//End Row

					if(chkDATFlag==1)
					{
						arr[1] = 1+"";
						break;
					}
						 
					
				}
			}
			else
			{
				//This UI works, When event have no schedule

				if(chkDATFlag==1 || chkDATFlag==2)
				{
					//Start Row
					sb.append("<div class='row'>");
						sb.append("<div class='col-sm-10 col-md-10'>");
							sb.append("<div class='col-sm-5 col-md-5'>");
								sb.append("<label>"+Utility.getLocaleValuePropByKey("lblTimeZone", locale)+"<span class='required'>*</span></label>");
								sb.append("<select id='timezoneforevent' name='timezoneforevent' class='form-control help-inline' >");
									sb.append("<option value='' selected='selected'></option>");
									for(TimeZoneMaster iZoneMaster:listTimeZoneMasters)
								 	{	
								 			sb.append("<option value='"+iZoneMaster.getTimeZoneId()+"'>"+iZoneMaster.getTimeZoneName()+" ("+iZoneMaster.getTimeZoneShortName()+")</option>");
								 	}
								sb.append("</select>");
							sb.append("</div>");
						sb.append("</div>");
					sb.append("</div>");
					//End Row
					
					//Start Row
					sb.append("<div class='row' style='padding-left: 16px;'>");
						sb.append("<div class='col-sm-16 col-md-16'>");
							sb.append("<div class='col-sm-2 col-md-2'>");
								sb.append("<label>"+Utility.getLocaleValuePropByKey("msgDate", locale)+"<span class='required'>*</span></label><input type='hidden' id='eventScheduleId' name='eventScheduleId' value='' >");
								sb.append("<input type='text' id='eventStartDate' name='eventStartDate' maxlength='0' class='form-control ' value='' style='width:148px;'>");
							sb.append("</div>");
							
							sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
								sb.append("<label>"+Utility.getLocaleValuePropByKey("lblStartTime", locale)+"<span class='required'>*</span></label>");
								sb.append("<select class='form-control help-inline' id='starthr' name='starthr'>");
								sb.append("<option value='' selected='selected'>HH:MM</option>");
								for(int m=1; m<=12;m++){
									String min=m+"";
									if(m!=10 && m!=11 && m!=12){
										min="0"+min;
									}
							 		for(int s=0; s<60;s=s+15){
							 			String sec=s+"";
							 			if(s==0){
							 				 sec="00";
							 			}
							 			if(!(m==0 && s==0)){
							 				sb.append("<option value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
							 			}
								 		//if(m==12)break;
							 		}
							 	}
							sb.append("</select>");
							sb.append("</div>");
						
							sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
								sb.append("<label>AM/PM<span class='required'>*</span></label>");
								sb.append("<select class='form-control help-inline' id='starttimeform' name='startminform' class='span3'>");
									// sb.append("<option value='' selected='selected'>AM/PM</option>");
								 	 sb.append("<option value='AM'   selected='selected'>AM</option>");
								 	 sb.append("<option value='PM'>PM</option>");
								 sb.append("</select>");
							sb.append("</div>");
							
							
							sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
							sb.append("<label>"+Utility.getLocaleValuePropByKey("lblEndTime", locale)+"<span class='required'>*</span></label>");
							sb.append("<select class='form-control help-inline' id='endhr' name='endhr' class='span3'>");
							sb.append("<option value='' selected='selected'>HH:MM</option>");
							for(int m=1; m<=12;m++){
								String min=m+"";
								if(m!=10 && m!=11 && m!=12){
									min="0"+min;
								}
						 		for(int s=0; s<60;s=s+15){
						 			String sec=s+"";
						 			if(s==0){
						 				 sec="00";
						 			}
						 			if(!(m==0 && s==0)){
						 				sb.append("<option value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
						 			}
						 			//if(m==12)break;
						 		}
						 	}
							 sb.append("</select>");
							sb.append("</div>");
					
							sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
								sb.append("<label>AM/PM<span class='required'>*</span></label>");
								sb.append("<select class='form-control help-inline' id='endtimeform' name='endtimeform' class='span3'>");
									// sb.append("<option value='' selected='selected'>AM/PM</option>");
								 	 sb.append("<option value='AM'   selected='selected'>AM</option>");
								 	 sb.append("<option value='PM'>PM</option>");
								 sb.append("</select>");
							sb.append("</div>");
					
							sb.append("<div class='col-sm-4 col-md-4 left10'  style='width: 378px;'>");
								sb.append("<label>"+Utility.getLocaleValuePropByKey("msgEventSchedAjax2", locale)+"</label>");
								sb.append("<input class='form-control help-inline' type='text' id='locationfirst' name='locationfirst' maxlength='100' value='' />");
							sb.append("</div>");
						sb.append("</div>");
					sb.append("</div>");
					//End Row
					
					sb.append("<script type='text/javascript'>var cal = Calendar.setup({ onSelect: function(cal) { cal.hide()}, showTime: true});");
					sb.append("cal.manageFields('eventStartDate', 'eventStartDate', '%m-%d-%Y');");
					sb.append("cal.manageFields('eventEndDate', 'eventEndDate', '%m-%d-%Y');");
					sb.append("</script>");
					
					arr[1] = 1+"";
				}
				else if(chkDATFlag==3)
				{
					int	DivCount = 0;
					
					
							//Start Row
							sb.append("<div class='row'>");
								sb.append("<div class='col-sm-12 col-md-12'>");
									sb.append("<div class='col-sm-4 col-md-4'>");
										sb.append("<label>"+Utility.getLocaleValuePropByKey("lblTimeZone", locale)+"<span class='required'>*</span></label>");
										sb.append("<select id='timezoneforevent' name='timezoneforevent' class='form-control help-inline' >");
											sb.append("<option value='' selected='selected'></option>");
											for(TimeZoneMaster iZoneMaster:listTimeZoneMasters)
										 	{	
										 			sb.append("<option value='"+iZoneMaster.getTimeZoneId()+"'>"+iZoneMaster.getTimeZoneName()+" ("+iZoneMaster.getTimeZoneShortName()+")</option>");
										 	}
										sb.append("</select>");
									sb.append("</div>");
								  
									sb.append("<div class='col-sm-6 col-md-6' style='margin-top: 20px;' id='singlebookingonlyDiv'>");
									sb.append("<label class='checkbox inline'>");
										sb.append("<input type='checkbox' id='singlebookingonly' name='singlebookingonly' onchange='checkforSingleBooking();'/>");
										sb.append(Utility.getLocaleValuePropByKey("msgEventSchedAjax1", locale)); 
									sb.append("</label>");
								  sb.append("</div>");
								  sb.append("<div class='col-sm-2 col-md-2' id='noofcandidateDiv'>");
									sb.append("<label'>No. of Candidate(s)</label><a href='#' id='noofcandidate' rel='tooltip' data-original-title='For an interview where there are more candidates during one time slot, please indicate that number here.'><img width='15' height='15' alt='' src='images/qua-icon.png'></a>");
										sb.append("<input type='text' onkeypress='return checkForInt(event)' class='help-inline form-control' id='noofcandidateperslot' name='noofcandidateperslot' maxlength='8' />");
									sb.append("</label>");
								  sb.append("</div>");
									
								sb.append("</div>");
							sb.append("</div>");
							//End Row
						for(int i=0;i<5;i++)
						{
								   DivCount=i;
								   
								   if(i==0)
								   {
										 //Start Row
											sb.append("<div class='row' style='padding-left: 16px;'>");
											sb.append("<div class='col-sm-16 col-md-16'>");
												sb.append("<div class='col-sm-2 col-md-2'>");
													sb.append("<label>"+Utility.getLocaleValuePropByKey("msgDate", locale)+"<span class='required'>*</span></label><input type='hidden' id='eventScheduleId' name='eventScheduleId' value='' >");
													sb.append("<input type='text' id='eventStartDate' name='eventStartDate' maxlength='0' class='form-control' value='' style='width:148px;'>");
												sb.append("</div>");
												
												sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
													sb.append("<label>"+Utility.getLocaleValuePropByKey("lblStartTime", locale)+"<span class='required'>*</span></label>");
													sb.append("<select class='form-control help-inline' id='starthr' name='starthr' class='span3'>");
														sb.append("<option value='' selected='selected'>HH:MM</option>");
														for(int m=1; m<=12;m++){
															String min=m+"";
															if(m!=10 && m!=11 && m!=12){
																min="0"+min;
															}
													 		for(int s=0; s<60;s=s+15){
													 			String sec=s+"";
													 			if(s==0){
													 				 sec="00";
													 			}
													 			if(!(m==0 && s==0)){
													 				sb.append("<option value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
													 			}
													 			//if(m==12)break;
													 		}
													 	}
													 sb.append("</select>");
												sb.append("</div>");
											
												sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
													sb.append("<label>AM/PM<span class='required'>*</span></label>");
													sb.append("<select class='form-control help-inline' id='starttimeform' name='startminform' class='span3'>");
														 //sb.append("<option value='' selected='selected'>AM/PM</option>");
													 	 sb.append("<option value='AM'   selected='selected'>AM</option>");
													 	 sb.append("<option value='PM'>PM</option>");
													 sb.append("</select>");
												sb.append("</div>");
												
												sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
												sb.append("<label>"+Utility.getLocaleValuePropByKey("lblEndTime", locale)+"<span class='required'>*</span></label>");
												sb.append("<select class='form-control help-inline' id='endhr' name='endhr' class='span3'>");
												sb.append("<option value='' selected='selected'>HH:MM</option>");
												for(int m=1; m<=12;m++){
													String min=m+"";
													if(m!=10 && m!=11 && m!=12){
														min="0"+min;
													}
											 		for(int s=0; s<60;s=s+15){
											 			String sec=s+"";
											 			if(s==0){
											 				 sec="00";
											 			}
											 			if(!(m==0 && s==0)){
											 				sb.append("<option value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
											 			}
											 			//if(m==12)break;
											 		}
											 	}
												 sb.append("</select>");
												sb.append("</div>");

												sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
													sb.append("<label>AM/PM<span class='required'>*</span></label>");
													sb.append("<select class='form-control help-inline' id='endtimeform' name='endtimeform' class='span3'>");
														// sb.append("<option value='' selected='selected'>AM/PM</option>");
														 sb.append("<option value='AM'   selected='selected'>AM</option>");
														 sb.append("<option value='PM'>PM</option>");
													 sb.append("</select>");
												sb.append("</div>");

										
												sb.append("<div class='col-sm-4 col-md-4 left10'  style='width: 378px;'>");
													sb.append("<label>"+Utility.getLocaleValuePropByKey("msgEventSchedAjax2", locale)+"</label>");
													sb.append("<input class='form-control help-inline' type='text' id='locationfirst' name='locationfirst' maxlength='100' value='' />");
												sb.append("</div>");
											sb.append("</div>");
										sb.append("</div>");
										//End Row
										
										sb.append("<script type='text/javascript'>var cal = Calendar.setup({ onSelect: function(cal) { cal.hide()}, showTime: true});");
										sb.append("cal.manageFields('eventStartDate', 'eventStartDate', '%m-%d-%Y');");
									//	sb.append("cal.manageFields('eventEndDate"+DivCount+"', 'eventEndDate"+DivCount+"', '%m-%d-%Y');");
										sb.append("</script>");
								   }
								   else
								   {
									 //Start Row
										sb.append("<div class='row' style='padding-left: 16px;'>");
										sb.append("<div class='col-sm-16 col-md-16'>");
											sb.append("<div class='col-sm-2 col-md-2'>");
												sb.append("<label>"+Utility.getLocaleValuePropByKey("msgDate", locale)+"<span class='required'>*</span></label><input type='hidden' id='eventScheduleId"+DivCount+"' name='eventScheduleId"+DivCount+"' value='' >");
												sb.append("<input type='text' id='eventStartDate"+DivCount+"' name='eventStartDate"+DivCount+"' maxlength='0' class='form-control' value='' style='width:148px;'>");
											sb.append("</div>");
											
											sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
												sb.append("<label>"+Utility.getLocaleValuePropByKey("lblStartTime", locale)+"<span class='required'>*</span></label>");
												sb.append("<select class='form-control help-inline' id='starthr"+DivCount+"' name='starthr"+DivCount+"' class='span3'>");
													sb.append("<option value='' selected='selected'>HH:MM</option>");
													for(int m=1; m<=12;m++){
														String min=m+"";
														if(m!=10 && m!=11 && m!=12){
															min="0"+min;
														}
												 		for(int s=0; s<60;s=s+15){
												 			String sec=s+"";
												 			if(s==0){
												 				 sec="00";
												 			}
												 			if(!(m==0 && s==0)){
												 				sb.append("<option value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
												 			}
												 			//if(m==12)break;
												 		}
												 	}
												 sb.append("</select>");
											sb.append("</div>");
										
											sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
												sb.append("<label>AM/PM<span class='required'>*</span></label>");
												sb.append("<select class='form-control help-inline' id='starttimeform"+DivCount+"' name='startminform"+DivCount+"' class='span3'>");
													// sb.append("<option value='' selected='selected'>AM/PM</option>");
												 	 sb.append("<option value='AM'   selected='selected'>AM</option>");
												 	 sb.append("<option value='PM'>PM</option>");
												 sb.append("</select>");
											sb.append("</div>");
											
											
											sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
											sb.append("<label>"+Utility.getLocaleValuePropByKey("lblEndTime", locale)+"<span class='required'>*</span></label>");
											sb.append("<select class='form-control help-inline' id='endhr"+DivCount+"' name='endhr"+DivCount+"' class='span3'>");
											sb.append("<option value='' selected='selected'>HH:MM</option>");
											for(int m=1; m<=12;m++){
												String min=m+"";
												if(m!=10 && m!=11 && m!=12){
													min="0"+min;
												}
										 		for(int s=0; s<60;s=s+15){
										 			String sec=s+"";
										 			if(s==0){
										 				 sec="00";
										 			}
										 			if(!(m==0 && s==0)){
										 				sb.append("<option value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
										 			}
										 			//if(m==12)break;
										 		}
										 	}
											 sb.append("</select>");
											sb.append("</div>");

											sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
												sb.append("<label>AM/PM<span class='required'>*</span></label>");
												sb.append("<select class='form-control help-inline' id='endtimeform"+DivCount+"' name='endtimeform"+DivCount+"' class='span3'>");
													// sb.append("<option value='' selected='selected'>AM/PM</option>");
													 sb.append("<option value='AM'   selected='selected'>AM</option>");
													 sb.append("<option value='PM'>PM</option>");
												 sb.append("</select>");
											sb.append("</div>");

									
											sb.append("<div class='col-sm-4 col-md-4 left10'  style='width: 378px;'>");
												sb.append("<label>"+Utility.getLocaleValuePropByKey("msgEventSchedAjax2", locale)+"</label>");
												sb.append("<input class='form-control help-inline' type='text' id='location"+DivCount+"' name='location"+DivCount+"' maxlength='100' value='' />");
											sb.append("</div>");
										sb.append("</div>");
									sb.append("</div>");
									//End Row
									
									sb.append("<script type='text/javascript'>var cal = Calendar.setup({ onSelect: function(cal) { cal.hide()}, showTime: true});");
									sb.append("cal.manageFields('eventStartDate"+DivCount+"', 'eventStartDate"+DivCount+"', '%m-%d-%Y');");
								//	sb.append("cal.manageFields('eventEndDate"+DivCount+"', 'eventEndDate"+DivCount+"', '%m-%d-%Y');");
									sb.append("</script>");
								   }
						}
						arr[1] = 5+"";
				}
				
			}
		}
		
		
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	
	arr[0] = sb.toString();
	
	
	return arr;	
}
public boolean deleteEventScheduleById(int scheduleId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	try{
		EventSchedule eventSchedule						=	eventScheduleDAO.findById(scheduleId, false, false);
		if(eventSchedule!=null)
		  eventScheduleDAO.makeTransient(eventSchedule);
		
	}catch (Exception e) {
		e.printStackTrace();
		return false;
	}
	return true;	
}

public String getScheduleById(int scheduleId)
{
	StringBuffer bfr=new StringBuffer();
	WebContext context;
	EventSchedule eventSchedule	=	null;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	try{
		eventSchedule						=	eventScheduleDAO.findById(scheduleId, false, false);
		EventDetails eventDetails=eventSchedule.getEventDetails();
		Date stdate=eventSchedule.getEventDateTime();
		SimpleDateFormat formats=new SimpleDateFormat("MM-dd-yyyy");
		bfr.append(formats.format(stdate)+"||||");
		
		if(eventDetails.getEventTypeId().getEventTypeId()!=1){
		String startTime=eventSchedule.getEventStartTime();
		String startTimearray[]=startTime.split(":");
		String endTime=eventSchedule.getEventEndTime();
		String endTimearray[]=endTime.split(":");
		bfr.append(startTimearray[0]+"||||");
		bfr.append(startTimearray[1]+"||||");
		bfr.append(eventSchedule.getEventStartTimeFormat()+"||||");
		bfr.append(endTimearray[0]+"||||");
		bfr.append(endTimearray[1]+"||||");
		bfr.append(eventSchedule.getEventEndTimeFormat()+"||||");
		bfr.append(eventSchedule.getLocation()+"||||");
		
		if(eventSchedule.getTimeZoneMaster()!=null)
		  bfr.append(eventSchedule.getTimeZoneMaster().getTimeZoneId());
		else
			bfr.append("");	
		}
		else
		{
			bfr.append(eventSchedule.getValidity());	
		}	
		
	    
	}catch (Exception e) {
		e.printStackTrace();
		
	}
	return bfr.toString();		
}


public String checkScheduleByEventId(Integer eventId)
{
	System.out.println(">>>>>>>>>>>>>> :: "+eventId);
	StringBuffer bfr= new StringBuffer();
	int check=0;
	try{
		EventDetails eventDetail = eventDetailsDAO.findById(eventId, false, false);
		Criterion criterion1 = Restrictions.eq("eventDetails",eventDetail); 
		List<EventSchedule> eventSchedule = eventScheduleDAO.findByCriteria(criterion1);
		if(eventSchedule!=null && eventSchedule.size()>0)
		{
			check=1;
			bfr.append(check+"@@@");
			bfr.append(eventSchedule.get(0).getValidity());
			bfr.append("@@@"+eventSchedule.get(0).getEventScheduleId());
		}
		else
		{
			check = 0;
			bfr.append(check+"@@@");
			bfr.append("non");
		}
		
	}catch (Exception e) {
		e.printStackTrace();
	}
	return bfr.toString();
}

public String saveVirtualVideoSchedule(Integer scheduleId,Integer eventId, String location,int validity)
{
	System.out.println(eventId +" ::  eventId SSSSSSSSSSSSSSSSSSSSSSS  :: " +scheduleId);	
	    WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		try
		{
		    EventDetails eventDetails=eventDetailsDAO.findById(eventId,false,false);
		    
			EventSchedule eventSchedule=null;
			if(scheduleId!=null && scheduleId!=0)
			{
				eventSchedule=eventScheduleDAO.findById(scheduleId, false, false);	
			}else{
			eventSchedule=new EventSchedule();	
			}
			eventSchedule.setEventDetails(eventDetails);
			
			eventSchedule.setValidity(validity);
			eventSchedule.setEventStartTime("");
			eventSchedule.setEventStartTimeFormat("");
			eventSchedule.setEventEndTime("");
			eventSchedule.setEventEndTimeFormat("");
			eventSchedule.setLocation("");
			eventSchedule.setStatus("A");
			eventSchedule.setCreatedBY(userSession);
			eventSchedule.setCreatedDateTime(new Date());
			eventScheduleDAO.makePersistent(eventSchedule);
		
		}catch(Exception e)
			{
				e.printStackTrace();
			}
		return "success";	
}

public String getCandidateSlotBySccheduleId(int scheduleId,String noOfRow,String pageNo,String sortOrder,String sortOrderType)
{
	StringBuffer dmRecords =	new StringBuffer();
	WebContext context;
	EventSchedule eventSchedule	=	new EventSchedule();
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	boolean slotflag=false;
	List<EventSchedule> listeEventSchedules=new ArrayList<EventSchedule>();
	List<SlotSelection> lstSlotSelections=new ArrayList<SlotSelection>();
	List<CandidateEventDetails> listCandidateEventDetails= new ArrayList<CandidateEventDetails>(); 
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	try{
	    //-- get no of record in grid,
	    //-- set start and end position

		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start =((pgNo-1)*noOfRowInPage);
		int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		int totalRecords=0;
		//------------------------------------
	    /** set default sorting fieldName **/
		String sortOrderFieldName="lastName";
		String sortOrderNoField="lastName";

		/**Start set dynamic sorting fieldName **/
			
		Order  sortOrderStrVal=null;
		if(sortOrder!=null){
			if(!sortOrder.equals("") && sortOrder!=null){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
			}
		}

		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && sortOrderType!=null){
			if(sortOrderType.equals("0")){
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}else{
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
		
		eventSchedule	=	eventScheduleDAO.findById(scheduleId, false, false);
		EventDetails eventDetails=eventSchedule.getEventDetails();
		
		listeEventSchedules=eventScheduleDAO.findByEventDetail(eventDetails);
		
		if(eventDetails.getEventTypeId().getEventTypeName().equals("Networking") || eventDetails.getEventTypeId().getEventTypeName().equals("Job Fair")){
			slotflag=false;
		}else{
			if(listeEventSchedules!=null && listeEventSchedules.size()>1){
				slotflag=true;
			}else{
				slotflag=false;
			}
		}
		
		if(slotflag){
			System.out.println("eventSchedule.getEventScheduleId() :: "+eventSchedule.getEventScheduleId());
			lstSlotSelections=slotSelectionDAO.findByEventScheduleList(eventSchedule,start,end,sortOrderStrVal);
			totalRecords=slotSelectionDAO.countByEventScheduleList(eventSchedule,start,end,sortOrderStrVal);
		}else{
			System.out.println("eventDetails :: "+eventDetails.getEventId());
			listCandidateEventDetails= candidateEventDetailsDAO.findByEventDetail(eventDetails,start,end,sortOrderStrVal);
			totalRecords=candidateEventDetailsDAO.getRowEventDetail(eventDetails,start,end,sortOrderStrVal);
		}
		
		totalRecord =totalRecords;
		if(totalRecord<end)
			end=totalRecord;
		
		//header
		String responseText="";
		dmRecords.append("<table id='candidateSlotsGrid' border='0' class='table table-bordered table-striped' >");
		dmRecords.append("<thead class='bg'>");
		dmRecords.append("<tr>");
		
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblFirstName", locale),sortOrderNoField,"firstName",sortOrderTypeVal,pgNo);
		dmRecords.append("<th  valign='top'>"+responseText+"</th>");

		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblLastName", locale),sortOrderNoField,"lastName",sortOrderTypeVal,pgNo);
		dmRecords.append("<th  valign='top'>"+responseText+"</th>");
		
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblEmailAddress", locale),sortOrderNoField,"emailAddress",sortOrderTypeVal,pgNo);
		dmRecords.append("<th  valign='top'>"+responseText+"</th>");
		
		dmRecords.append("</thead>");
		dmRecords.append("<tbody>");
		if(slotflag)
		{
			if(lstSlotSelections!=null && lstSlotSelections.size()==0)
				dmRecords.append("<tr><td colspan='3' align='center'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			if(lstSlotSelections!=null && lstSlotSelections.size()>0)
			for(SlotSelection sltsel:lstSlotSelections)
			{
				TeacherDetail teacherdetail=sltsel.getTeacherDetail();
				dmRecords.append("<tr>");	
					dmRecords.append("<td>"+teacherdetail.getFirstName()+"</td>");
					dmRecords.append("<td>"+teacherdetail.getLastName()+"</td>");
					dmRecords.append("<td>"+teacherdetail.getEmailAddress()+"</td>");
				dmRecords.append("</tr>");	
			}
		}else{
			if(listCandidateEventDetails!=null && listCandidateEventDetails.size()==0)
				dmRecords.append("<tr><td colspan='3' align='center'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			
			if(listCandidateEventDetails!=null && listCandidateEventDetails.size()>0)
			for(CandidateEventDetails ced:listCandidateEventDetails)
			{
				dmRecords.append("<tr>");	
					dmRecords.append("<td>"+ced.getFirstName()+"</td>");
					dmRecords.append("<td>"+ced.getLastName()+"</td>");
					dmRecords.append("<td>"+ced.getEmailAddress()+"</td>");
				dmRecords.append("</tr>");	
			}
		}
		dmRecords.append("</tbody>");
		dmRecords.append("</table>");	
		dmRecords.append(PaginationAndSorting.getPaginationStringForParticipantsAjax(request,totalRecord,noOfRow, pageNo,"pageSize","690","participantsGrid"));
	}catch (Exception e) {
		e.printStackTrace();
		
	}
	return dmRecords.toString();			
}

//TimeZoneMasterDAO

//************ TimeZone ************
public String getTimeZone()
{
	StringBuffer sb	=	new StringBuffer();
	try 
	{
		List<TimeZoneMaster>   listTimeZoneMasters=timeZoneMasterDAO.getAllTimeZone();
		if(listTimeZoneMasters.size()>0)
		{	
			sb.append("<select id=\"timezone\" id=\"timezone\">");
	    		if(listTimeZoneMasters.size()>1)
			     sb.append("<option value=''>Select Time Zone</option>");
	    		
	    		for(TimeZoneMaster tzm : listTimeZoneMasters)
	    		{
	    			sb.append("<option  value='"+tzm.getTimeZoneId()+"'>"+tzm.getTimeZoneShortName()+"</option>");
	    		}
	         sb.append("</select>");
		}
	} 
	catch (Exception e){
		e.printStackTrace();
	}
	return sb.toString();	
}

public String createAddScheduleDiv(Integer eventId,Integer DivCount)
{
	System.out.println(" ============createAddScheduleDiv ================");
	StringBuffer dmRecords =	new StringBuffer();
	WebContext context;
	EventSchedule eventSchedule	=	new EventSchedule();
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	
	StringBuffer sb	=	new StringBuffer();
	
	try
	{
		EventDetails eventDetails = eventDetailsDAO.findById(eventId, false, false);
		
		String chkDateAndTime = eventDetails.getEventSchedulelId().getEventScheduleName().toLowerCase().trim();
		int chkDATFlag = 0;
		
		
		if(eventDetails.getEventTypeId().getEventTypeId()!=1)
		{
			if(chkDateAndTime.equals(("Fixed Date and Time").toLowerCase().trim()))
				chkDATFlag = 1;
			else if(chkDateAndTime.equals(("Multiple Fixed Date and Time").toLowerCase().trim()))
				chkDATFlag = 2;
			else
				chkDATFlag = 3;
		}
		
	//	List<TimeZoneMaster>   listTimeZoneMasters=timeZoneMasterDAO.getAllTimeZone();
		
		if(eventDetails!=null)
		{
			//Start Row
			/*sb.append("<div class='row'>");
				sb.append("<div class='col-sm-10 col-md-10'>");
					sb.append("<div class='col-sm-3 col-md-3'>");
						sb.append("<label>Time Zone<span class='required'>*</span></label>");
						sb.append("<select id='timezone' name='timezone' class='form-control' >");
							sb.append("<option value='' selected='selected'></option>");
							for(TimeZoneMaster iZoneMaster:listTimeZoneMasters)
						 	{	
						 			sb.append("<option value='"+iZoneMaster.getTimeZoneId()+"'>"+iZoneMaster.getTimeZoneShortName()+"</option>");
						 	}
						sb.append("</select>");
					sb.append("</div>");
				sb.append("</div>");
			sb.append("</div>");*/
			//End Row
			
			if(chkDATFlag>1)
			{
				/*if(chkDATFlag==3)
					DivCount++;*/
				
				//Start Row
				sb.append("<div class='row' style='padding-left: 16px;'>");
					sb.append("<div class='col-sm-16 col-md-16'>");
						sb.append("<div class='col-sm-2 col-md-2'>");
							sb.append("<label>"+Utility.getLocaleValuePropByKey("msgDate", locale)+"<span class='required'>*</span></label><input type='hidden' id='eventScheduleId"+DivCount+"' name='eventScheduleId"+DivCount+"' value='' >");
							sb.append("<input type='text' id='eventStartDate"+DivCount+"' name='eventStartDate"+DivCount+"' maxlength='0' class='form-control' value='' style='width:148px;'>");
						sb.append("</div>");
						
						sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
							sb.append("<label>"+Utility.getLocaleValuePropByKey("lblStartTime", locale)+"<span class='required'>*</span></label>");
							sb.append("<select class='form-control help-inline' id='starthr"+DivCount+"' name='starthr"+DivCount+"' class='span3'>");
							sb.append("<option value='' selected='selected'>HH:MM</option>");
							for(int m=1; m<=12;m++){
								String min=m+"";
								if(m!=10 && m!=11 && m!=12){
									min="0"+min;
								}
						 		for(int s=0; s<60;s=s+15){
						 			String sec=s+"";
						 			if(s==0){
						 				 sec="00";
						 			}
						 			if(!(m==0 && s==0)){
						 				sb.append("<option value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
						 			}
						 			//if(m==12)break;
						 		}
						 	}
							 sb.append("</select>");
						sb.append("</div>");
					
						sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
							sb.append("<label>AM/PM<span class='required'>*</span></label>");
							sb.append("<select class='form-control help-inline' id='starttimeform"+DivCount+"' name='startminform"+DivCount+"' class='span3'>");
								// sb.append("<option value='' selected='selected'>AM/PM</option>");
							 	 sb.append("<option value='AM'   selected='selected'>AM</option>");
							 	 sb.append("<option value='PM'>PM</option>");
							 sb.append("</select>");
						sb.append("</div>");
				
						
						sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
						sb.append("<label>"+Utility.getLocaleValuePropByKey("lblEndTime", locale)+"<span class='required'>*</span></label>");
						sb.append("<select class='form-control help-inline' id='endhr"+DivCount+"' name='endhr"+DivCount+"' class='span3'>");
						sb.append("<option value='' selected='selected'>HH:MM</option>");
						for(int m=1; m<=12;m++){
							String min=m+"";
							if(m!=10 && m!=11 && m!=12){
								min="0"+min;
							}
					 		for(int s=0; s<60;s=s+15){
					 			String sec=s+"";
					 			if(s==0){
					 				 sec="00";
					 			}
					 			if(!(m==0 && s==0)){
					 				sb.append("<option value='"+min+":"+sec+"'>"+min+":"+sec+"</option>");
					 			}
					 			//if(m==12)break;
					 		}
					 	}
						 sb.append("</select>");
						sb.append("</div>");

						sb.append("<div class='col-sm-1 col-md-1 left20' style='width: 85px;padding-right: 0px;padding-left: 0px;'>");
							sb.append("<label>AM/PM<span class='required'>*</span></label>");
							sb.append("<select class='form-control help-inline' id='endtimeform"+DivCount+"' name='endtimeform"+DivCount+"' class='span3'>");
								// sb.append("<option value='' selected='selected'>AM/PM</option>");
								 sb.append("<option value='AM'  selected='selected'>AM</option>");
								 sb.append("<option value='PM'>PM</option>");
							 sb.append("</select>");
						sb.append("</div>");
						
						sb.append("<div class='col-sm-4 col-md-4 left10'  style='width: 378px;'>");
							sb.append("<label>"+Utility.getLocaleValuePropByKey("msgEventSchedAjax2", locale)+"</label>");
							sb.append("<input class='form-control help-inline' type='text' id='location"+DivCount+"' name='location"+DivCount+"' maxlength='100' value='' />");
						sb.append("</div>");
					sb.append("</div>");
				sb.append("</div>");
				//End Row
				
				sb.append("<script type='text/javascript'>var cal = Calendar.setup({ onSelect: function(cal) { cal.hide()}, showTime: true});");
				sb.append("cal.manageFields('eventStartDate"+DivCount+"', 'eventStartDate"+DivCount+"', '%m-%d-%Y');");
			//	sb.append("cal.manageFields('eventEndDate"+DivCount+"', 'eventEndDate"+DivCount+"', '%m-%d-%Y');");
				sb.append("</script>");
			}
		}
		
		
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	
	
	return sb.toString();
}

/*
public String createScheduleSlots(Integer eventId,Integer DivCount)
{
	System.out.println(" ============createAddScheduleDiv ================");
	StringBuffer dmRecords =	new StringBuffer();
	WebContext context;
	EventSchedule eventSchedule	=	new EventSchedule();
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	
	StringBuffer sb	=	new StringBuffer();
	
	try
	{
		EventDetails eventDetails = eventDetailsDAO.findById(eventId, false, false);
		
		String chkDateAndTime = eventDetails.getEventSchedulelId().getEventScheduleName().toLowerCase().trim();
		int chkDATFlag = 0;
		
		
		if(eventDetails.getEventTypeId().getEventTypeId()!=1)
		{
			if(chkDateAndTime.equals(("Fixed Date and Time").toLowerCase().trim()))
				chkDATFlag = 1;
			else if(chkDateAndTime.equals(("Multiple Fixed Date and Time").toLowerCase().trim()))
				chkDATFlag = 2;
			else
				chkDATFlag = 3;
		}
		
	//	List<TimeZoneMaster>   listTimeZoneMasters=timeZoneMasterDAO.getAllTimeZone();
		
		if(eventDetails!=null)
		{
			if(chkDATFlag==3)
			{
				for(int i=1;i<6;i++)
				{
							DivCount++;
							//Start Row
							sb.append("<div class='row' style='padding-left: 16px;'>");
							sb.append("<div class='col-sm-16 col-md-16'>");
								sb.append("<div class='col-sm-2 col-md-2'>");
									sb.append("<label>Date<span class='required'>*</span></label><input type='hidden' id='eventScheduleId"+DivCount+"' name='eventScheduleId"+DivCount+"' value='' >");
									sb.append("<input type='text' id='eventStartDate"+DivCount+"' name='eventStartDate"+DivCount+"' maxlength='0' class='form-control' value=''>");
								sb.append("</div>");
								
								sb.append("<div class='col-sm-1 col-md-1 left25' style='width: 95px;padding-right: 0px;padding-left: 0px;'>");
									sb.append("<label>Time<span class='required'>*</span></label>");
									sb.append("<select class='form-control ' id='starthr"+DivCount+"' name='starthr"+DivCount+"' class='span3'>");
										sb.append("<option value='' selected='selected'>HH:MM</option>");
										sb.append("<option value='8:15'>8:15</option>");
										sb.append("<option value='8:30'>8:30</option>");
										sb.append("<option value='8:45'>8:45</option>");
										sb.append("<option value='9:00'>9:00</option>");
									 sb.append("</select>");
								sb.append("</div>");
							
								sb.append("<div class='col-sm-1 col-md-1 left25' style='width: 70px;padding-right: 0px;padding-left: 0px;'>");
									sb.append("<label>AM/PM<span class='required'>*</span></label>");
									sb.append("<select class='form-control ' id='starttimeform"+DivCount+"' name='startminform"+DivCount+"' class='span3'>");
										 sb.append("<option value='' selected='selected'>AM/PM</option>");
									 	 sb.append("<option value='AM'>AM</option>");
									 	 sb.append("<option value='PM'>PM</option>");
									 sb.append("</select>");
								sb.append("</div>");
						
								sb.append("<div class='col-sm-7 col-md-7 left25' >");
									sb.append("<label>Location/Phone #/URL</label>");
									sb.append("<input class='form-control' type='text' id='location"+DivCount+"' name='location"+DivCount+"' maxlength='100' value='' />");
								sb.append("</div>");
							sb.append("</div>");
						sb.append("</div>");
						//End Row
						
						sb.append("<script type='text/javascript'>var cal = Calendar.setup({ onSelect: function(cal) { cal.hide()}, showTime: true});");
						sb.append("cal.manageFields('eventStartDate"+DivCount+"', 'eventStartDate"+DivCount+"', '%m-%d-%Y');");
					//	sb.append("cal.manageFields('eventEndDate"+DivCount+"', 'eventEndDate"+DivCount+"', '%m-%d-%Y');");
						sb.append("</script>");
					}
				}
			}
		
		
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	
	
	return sb.toString();
}*/

public String[] getVVIDays(Integer eventId)
{
	EventDetails eventDetails = null;
	String[] data = new String[2];
	try
	{
		eventDetails = eventDetailsDAO.findById(eventId, false, false);
		List<EventSchedule> eventSchedule = eventScheduleDAO.findByEventDetail(eventDetails);
		
		if(eventDetails.getDistrictMaster().getVVIExpiresInDays()!=null && !eventDetails.getDistrictMaster().getVVIExpiresInDays().equals("") && !eventDetails.getDistrictMaster().getVVIExpiresInDays().equals("0"))
			data[0] = eventDetails.getDistrictMaster().getVVIExpiresInDays()+""; 
		
		if(eventSchedule!=null && eventSchedule.size()>0)
		{
			if(eventSchedule.get(0).getValidity()!=null && eventSchedule.get(0).getValidity()>0)
			{
				data[0] = eventSchedule.get(0).getValidity()+"";
			}
			else if(eventDetails.getDistrictMaster().getVVIExpiresInDays()!=null && !eventDetails.getDistrictMaster().getVVIExpiresInDays().equals("") && !eventDetails.getDistrictMaster().getVVIExpiresInDays().equals("0"))
				data[0] = eventDetails.getDistrictMaster().getVVIExpiresInDays()+""; 
				
			data[1] = eventSchedule.get(0).getEventScheduleId()+"";
		}	
		else
			data[1]="";
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	return data;
}

 public String getCandidateByEvent(Integer scheduleID)
  {
	StringBuffer tmData=new StringBuffer();
	try
	{
		EventSchedule	eventSchedule = eventScheduleDAO.findById(scheduleID, false, false);
		EventDetails eventDetails=eventSchedule.getEventDetails();
		
		List<EventParticipantsList> listEventParticipants=eventParticipantsListDAO.findByEventDetails(eventDetails);
		///List<SlotSelection> listSlotSelections=slotSelectionDAO.findByEventScheduleId(eventSchedule);
		tmData.append("<select id='participantId' name='participantId' class='form-control help-inline'>");
			tmData.append("<option value='0'>Select Participant</option>");
		if(listEventParticipants!=null && listEventParticipants.size()>0){
			for(EventParticipantsList participant : listEventParticipants){
				String fullName=participant.getParticipantFirstName().trim()+" "+participant.getParticipantLastName().trim()+"("+participant.getParticipantEmailAddress().trim()+")";
				tmData.append("<option value='"+participant.getEventParticipantId()+"'>"+fullName+"</option>");
			}
		}
		tmData.append("</select>");
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	return tmData.toString();
 }
 
 //@SuppressWarnings("null")
public int addCandidateToSlot(Integer scheduleID,Integer participantId)
 {
	System.out.println(">>>>>>>>> scheduleID :: "+scheduleID +"participantId :: "+participantId);
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	//UserMaster userMaster =	null;
	int returnvalue=100;
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	
	try
	{
		TeacherDetail teacherDetail=null;
		boolean isNew=false;
		EventSchedule	eventSchedule = eventScheduleDAO.findById(scheduleID, false, false);
		EventDetails eventDetails=eventSchedule.getEventDetails();
		EventParticipantsList eventParticipants=eventParticipantsListDAO.findById(participantId, false, false);
		List<TeacherDetail> listTeacherDetail=teacherDetailDAO.findByEmail(eventParticipants.getParticipantEmailAddress());
		if(listTeacherDetail!=null && listTeacherDetail.size()>0){
			teacherDetail=listTeacherDetail.get(0);
		}
		boolean unlockedSlotFlag=true;
		List<SlotSelection> listSlotSelections=slotSelectionDAO.fidSlotByCandidateAndEvent(eventSchedule,teacherDetail);
		
		if(listSlotSelections!=null && listSlotSelections.size()>0){
			returnvalue=0;
		}else{
			if(eventSchedule.getSinglebookingonly()!=null && eventSchedule.getSinglebookingonly()){
				   listSlotSelections =  slotSelectionDAO.findByEventScheduleId(eventSchedule);
				   if(listSlotSelections!=null && listSlotSelections.size()>0){
					   unlockedSlotFlag=false;
				   }
			   }else{
				   if(eventSchedule.getNoOfCandidatePerSlot()!=null && eventSchedule.getNoOfCandidatePerSlot()>0){
					   listSlotSelections =  slotSelectionDAO.findByEventScheduleId(eventSchedule);
					   if(listSlotSelections!=null && listSlotSelections.size()>0)
					   {
						   if(listSlotSelections.size()>=eventSchedule.getNoOfCandidatePerSlot()){
							   unlockedSlotFlag=false;
						   }
					   }
				   }
			   }
			  if(unlockedSlotFlag){
				  if(teacherDetail!=null){
						//teacherDetail=listTeacherDetail.get(0);
					}else{
						teacherDetail=new TeacherDetail();
						isNew=true;
						int authorizationkey=(int) Math.round(Math.random() * 2000000);
						
						//password to be send 
						String uniquepass=Utility.randomString(8);
						
						String firstName=eventParticipants.getParticipantFirstName()==null?"":eventParticipants.getParticipantFirstName();
						String lastName=eventParticipants.getParticipantLastName()==null?"":eventParticipants.getParticipantLastName();
						String emailAddress=eventParticipants.getParticipantEmailAddress()==null?"":eventParticipants.getParticipantEmailAddress();
						
						teacherDetail.setFirstName(firstName);
					    teacherDetail.setLastName(lastName);
					    teacherDetail.setEmailAddress(emailAddress);
						teacherDetail.setUserType("N");
						teacherDetail.setPassword(MD5Encryption.toMD5(uniquepass));	
						teacherDetail.setFbUser(false);
						teacherDetail.setQuestCandidate(0);
						teacherDetail.setAuthenticationCode(""+authorizationkey);
						teacherDetail.setVerificationCode(""+authorizationkey);
						teacherDetail.setVerificationStatus(1);
						teacherDetail.setIsPortfolioNeeded(true);
						teacherDetail.setNoOfLogin(0);
						teacherDetail.setStatus("A");
						teacherDetail.setSendOpportunity(false);
						teacherDetail.setIsResearchTeacher(false);
						teacherDetail.setForgetCounter(0);
						teacherDetail.setCreatedDateTime(new Date());
						teacherDetail.setIpAddress(IPAddressUtility.getIpAddress(request));
						teacherDetail.setInternalTransferCandidate(false);
						teacherDetailDAO.makePersistent(teacherDetail);
					}
				  
					SlotSelection slotSelection=new SlotSelection();
					slotSelection.setTeacherDetail(teacherDetail);
					slotSelection.setEventSchedule(eventSchedule);
					slotSelectionDAO.makePersistent(slotSelection);	
					
					//Send Mail to candidate
					String districtName="";
					if(eventDetails.getDistrictMaster()!=null)
					   districtName=eventDetails.getDistrictMaster().getDistrictName();
					else if(eventDetails.getBranchMaster()!=null)
						districtName=eventDetails.getBranchMaster().getBranchName();
					else if(eventDetails.getHeadQuarterMaster()!=null)
						districtName=eventDetails.getHeadQuarterMaster().getHeadQuarterName();
					
					String mailid="";			
					String content="";		
					String calFileName="";
					//Create ICS File 			
					String eventSubject=eventDetails.getEventName();				
					String desc=eventDetails.getDescription();	
					
					SlotSelectionAjax slAjax=new SlotSelectionAjax();
					
					calFileName= slAjax.createIcalFile(eventSubject, desc,eventSchedule);	
					String location=eventSchedule.getLocation();
					String location1="";
					String location2="";
					if(location!=null && location!=""){
						location1=" at "+eventSchedule.getLocation();
						location2="<br>"+Utility.getLocaleValuePropByKey("lblLocti", locale)+": "+eventSchedule.getLocation();
					}
					SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
					SimpleDateFormat formday=new SimpleDateFormat("EEEE");
					String dayofweek=formday.format(eventSchedule.getEventDateTime());			
					String slotTime=eventSchedule.getEventStartTime()+" "+eventSchedule.getEventStartTimeFormat()+Utility.getLocaleValuePropByKey("lblTo", locale)+ " "+eventSchedule.getEventEndTime()+" "+eventSchedule.getEventEndTimeFormat()+" "+eventSchedule.getTimeZoneMaster().getTimeZoneShortName();
					String slotDate=dayofweek+", "+dateFormat.format(eventSchedule.getEventDateTime()).toString();	
					String eventName=eventDetails.getEventName();
					String participantsName=teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
					String eventSubjectforParticipants=eventName +": "+ slotTime+" "+Utility.getLocaleValuePropByKey("msgon3", locale) +" "+slotDate+" "+location1 ;
					String msgToParticipants="<p>"+Utility.getLocaleValuePropByKey("lblDear", locale)+" "+participantsName+",</p> <p>You are invited in event "+eventName+". "+Utility.getLocaleValuePropByKey("msgDetailsRreBelow", locale)+":<br><br>"+Utility.getLocaleValuePropByKey("lblDate", locale)+":  "+slotDate+"<br>"+Utility.getLocaleValuePropByKey("msgTime3", locale)+":"+slotTime+location2+" "+"<br></p>"+"<br><p>For more information contact "+districtName+"</p><br>"+" <p>"+Utility.getLocaleValuePropByKey("msgRegards", locale)+" "+"<br>"+districtName+"</p>";			
					
					mailid=teacherDetail.getEmailAddress();				
					content=msgToParticipants;
					System.out.println(">>>>>>>>>>>>>>>>>>>> :: "+content);
					emailerService.sendMailWithAttachments(mailid, eventSubjectforParticipants, "noreply@teachermatch.net", content,calFileName.toString());
					
			  }else{
				  returnvalue=1;
			  }
			
		}
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	return returnvalue;
}
 
 
}