package tm.services;

import javax.servlet.http.HttpServletRequest;

import tm.utility.Utility;

public class PaginationAndSorting 
{
	static String locale = Utility.getValueOfPropByKey("locale");
	
	public static String getPaginationString(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{		
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:970px;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		return sb.toString();
	}
	//By alok (create method to change the width)
	public static String getPaginationStringForManageJobOrdersAjax(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{	//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:945px;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		return sb.toString();
	}
	public static String getPaginationStringForManageDSPQAjax(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{	//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:851px;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		return sb.toString();
	}
	/*
	 * Author : Deepak 
	 * Purpose : Pagination style according to new data Grid
	 */
	public static String getPaginationStringForManageDSPQAjaxUsingDiv(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{	//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;	
		noOfPage=(totalRow+noOfRow-1)/noOfRow;		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow){
			recordTo=totalRow;
		}else{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize){
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		sb.append("<div class='row' style='padding:7px 0px 5px 5px;'>");
		if(totalRow>minPgSize){
			sb.append("<div class='row' style='padding:7px 0px 5px 5px;'>");
			sb.append("<div class='col-sm-3 col-md-3 pleft25'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</div>");			
			sb.append("<div class='col-sm-2 col-md-2'>");
			if(pageNo!=1){
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			sb.append("</div>");
			sb.append("<div class='col-sm-4 col-md-4'>");
			if(noOfPage>5 && pageNo>3){
				if(pageNo+2>noOfPage){
					start=noOfPage-4;
					end=noOfPage;
				}else{
					start=pageNo-2;
					end=pageNo+2;
				}
			}else{
				if(noOfPage<5){
					start=1;
					end=noOfPage;
				}else{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++){
				if(k==pageNo){
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}else{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1)){
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			sb.append("</div>");
			sb.append("<div class='col-sm-3 col-md-3 pleft30'>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPaging('');\" >");
			for(String no:gridPageSize){
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</div>");
			sb.append("</div>");
			sb.append("</div>");
		}
		sb.append("</div>");
		return sb.toString();
	}
	/*                     End                                      */
	public static String getPaginationStringForPanelAjax(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{	//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:945px;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		return sb.toString();
	}
	
	//******************** Adding By Deepak *******************************
	public static String getPaginationStringForPanelAjaxDSPQ(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{	//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:862px;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		return sb.toString();
	}
	
	
	public static String getPaginationStringForManageJobOrdersAjaxDSPQ(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{	//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:862px;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		return sb.toString();
	}
	
	
	
	public static String getPaginationStringForPanelAjaxForDSPQ(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{	//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:862px;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		return sb.toString();
	}
	
	//************************************** End ********************************************************
	public static String getPaginationStringForJobboard(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{	//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='padding:5px;width:100%;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			//sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='3%' nowrap align='center'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");			
			
		//	sb.append("<td width='1%'  nowrap>");
	
			
			//sb.append("</td>");
			sb.append("<td  width='8%'  align='center' nowrap>");
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingForJobboard('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingForJobboard('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPagingForJobboard('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingForJobboard('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingForJobboard('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='2%' align='center'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPagingForJobboard('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		return sb.toString();
	}
	public static String getPaginationStringForDistriceAjax(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{	//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:920px;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		return sb.toString();
	}

	//*********************** Adding By Deepak ****************************************************
	public static String getPaginationStringForDistriceAjaxForDSPQ(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{	//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:862px;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		return sb.toString();
	}
	//*********************************************************************************************
	public static String getPaginationStringForTeacherInfoAjax(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{		
		System.out.println("use by candidate pools.");
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:940px;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		return sb.toString();
	}
	public static String responseSortingLink(String fieldLabel, String sortOrderFieldName,String fieldName,String sortOrderTypeVal,int pgNo){
		StringBuffer sbResponseText = new StringBuffer("");
		try{
				sbResponseText.append("<span style=''>");
				if( sortOrderTypeVal.equals("1") && sortOrderFieldName.equals(fieldName)){
					sbResponseText.append("<table border='0px'style='width: auto;'>");
					sbResponseText.append("<tr>");
					sbResponseText.append("<td valign='top'>"+fieldLabel+"</td>");
					sbResponseText.append("<td valign='top'><div style='margin-bottom: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></div></td>");
					sbResponseText.append("</table>");
				}else if( sortOrderTypeVal.equals("0") && sortOrderFieldName.equals(fieldName)){

					sbResponseText.append("<table border='0px'style='width: auto;'>");
					sbResponseText.append("<tr>");
					sbResponseText.append("<td valign='top'>"+fieldLabel+"</td>");
					sbResponseText.append("<td valign='top'><div style='margin-top: 0px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></div></td>");
					sbResponseText.append("</table>");
				}else{
					sbResponseText.append("<table border='0px'style='width: auto;'>");
					sbResponseText.append("<tr>");
					sbResponseText.append("<td valign='top'rowspan='2'>"+fieldLabel+"</td>");
					sbResponseText.append("<td valign='top'><div style='margin-bottom: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></div></td>");
					sbResponseText.append("</tr>");
					sbResponseText.append("<tr>");
					sbResponseText.append("<td valign='top'><div style='margin-top: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></div></td>");
					sbResponseText.append("</tr>");
					sbResponseText.append("</table>");
				}


			sbResponseText.append("</span>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sbResponseText+"";
	}
	public static String responseSortingMLink(String fieldLabel, String sortOrderFieldName,String fieldName,String sortOrderTypeVal,int pgNo){
		StringBuffer sbResponseText = new StringBuffer("");
		try{
			sbResponseText.append("<span style=''>");
			if( sortOrderTypeVal.equals("1") && sortOrderFieldName.equals(fieldName)){
				sbResponseText.append("<table border='0px'style='width: auto;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' rowspan='2' style='text-align:left;'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:12px;padding-left:4px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' style='width:500px;padding-top:2px;'>&nbsp;</td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");	
			}else if( sortOrderTypeVal.equals("0") && sortOrderFieldName.equals(fieldName)){
				sbResponseText.append("<table border='0px'style='width: auto;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' rowspan='2' style='text-align:left;'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:12px;padding-left:4px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' style='width:500px;padding-top:2px;'>&nbsp;</td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");	
			}else{
				sbResponseText.append("<table border='0px' style='width: auto;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' rowspan='2'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:12px;padding-left:4px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' style='width:500px;padding-top:2px;padding-left:4px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");	
			}


			sbResponseText.append("</span>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sbResponseText+"";
	}
	
	
	public static String responseSortingMLink_1(String fieldLabel, String sortOrderFieldName,String fieldName,String sortOrderTypeVal,int pgNo){
		StringBuffer sbResponseText = new StringBuffer("");
		try{
			sbResponseText.append("<span style=''>");
			if( sortOrderTypeVal.equals("1") && sortOrderFieldName.equals(fieldName)){
				sbResponseText.append("<table border='0px'style='width: auto;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' rowspan='2' style='text-align:left;vertical-align:middle;height:25px;padding-left:4px;'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:13px;padding-left:4px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:12px;padding-left:4px;'></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");	
			}else if( sortOrderTypeVal.equals("0") && sortOrderFieldName.equals(fieldName)){
				sbResponseText.append("<table border='0px'style='width: auto;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' rowspan='2' style='text-align:left;vertical-align:middle;height:25px;padding-left:4px;'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:13px;padding-left:4px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:12px;padding-left:4px;'></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");	
			}else{
				sbResponseText.append("<table border='0px' style='width: auto;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' rowspan='2' style='text-align:left;vertical-align:middle;height:25px;padding-left:4px;'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:13px;padding-left:4px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' style='width:500px;vertical-align:middle;height:12px;padding-left:4px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");	
			}


			sbResponseText.append("</span>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sbResponseText+"";
	}
	public static String responseSortingMLink_2_TP(String fieldLabel, String sortOrderFieldName,String fieldName,String sortOrderTypeVal,int pgNo){
		StringBuffer sbResponseText = new StringBuffer("");
		try{
			sbResponseText.append("<span style=''>");
			if( sortOrderTypeVal.equals("1") && sortOrderFieldName.equals(fieldName)){
				sbResponseText.append("<table border='0px' style='border-collapse:collapse;width: auto;height:25px !important;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' rowspan='2' style='text-align:left;vertical-align:middle;height:25px !important;padding-left:4px;'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='bottom' style='width:500px;vertical-align:bottom;height:13px !important;padding:0px;padding-top:4px;padding-left:2px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:12px !important;padding:2px;padding-left:4px;'></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");	
			}else if( sortOrderTypeVal.equals("0") && sortOrderFieldName.equals(fieldName)){
				sbResponseText.append("<table border='0px' style='border-collapse:collapse;width: auto;height:25px !important;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' rowspan='2' style='text-align:left;vertical-align:middle;height:25px !important;padding-left:4px;'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='bottom' style='width:500px;vertical-align:bottom;height:13px !important;padding:0px;padding-top:4px;padding-left:2px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:12px !important;padding:2px;padding-left:4px;'></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");	
			}else{
				sbResponseText.append("<table border='0px' style='border-collapse:collapse;width: auto;height:25px !important;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' rowspan='2' style='text-align:left;vertical-align:middle;height:25px !important;padding-left:4px;'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='bottom' style='width:500px;vertical-align:bottom;height:13px !important;padding:2px;padding-left:2px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top' style='width:500px;vertical-align:top;height:12px !important;padding:2px;padding-left:2px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");	
			}


			sbResponseText.append("</span>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sbResponseText+"";
	}
	
	public static String responseSortingProfile(String fieldLabel, String sortOrderFieldName,String fieldName,String sortOrderTypeVal,int pgNo){
		StringBuffer sbResponseText = new StringBuffer("");
		try{
			sbResponseText.append("<span style=''>");
			if( sortOrderTypeVal.equals("1") && sortOrderFieldName.equals(fieldName)){
				
				//System.out.println("111111111111111111111");
				sbResponseText.append("<table border='0px'style='width: auto; margin-top: -9px;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top'><div style='margin-bottom: 0px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></div></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");	
			}else if( sortOrderTypeVal.equals("0") && sortOrderFieldName.equals(fieldName)){
				
				//System.out.println("222222222222222222222222");
				sbResponseText.append("<table border='0px'style='width: auto; margin-top: -9px;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top'><div style='margin-top: 0px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></div></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");	
			}else{
				//System.out.println("333333333333333333333333");
			
				sbResponseText.append("<table border='0px'style='width: auto;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top'rowspan='2'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top'><div style='margin-bottom: 0px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></div></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top'><div style='margin-top: -3px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSorting('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></div></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");
				
			}


			sbResponseText.append("</span>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sbResponseText+"";
	}
	
	
	public static String getPaginationDoubleString(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				//sb.append("<<");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				//sb.append("<");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			//
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				//sb.append(">");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				//sb.append(">>");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize1' id='pageSize1' style='width: 60px;height:28px; color: #555555;' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		
		//System.out.println(sb.toString());
		return sb.toString();
	}
	public static String getPaginationTrippleGrid(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				//sb.append("<<");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				//sb.append("<");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			//
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				//sb.append(">");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				//sb.append(">>");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize3' id='pageSize3' style='width: 60px;height:28px; color: #555555;' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		
		//System.out.println(sb.toString());
		return sb.toString();
	}
	public static String getPaginationTrippleGridNew(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:670px;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				//sb.append("<<");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				//sb.append("<");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			//
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				//sb.append(">");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				//sb.append(">>");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize3' id='pageSize3' style='width: 60px;height:28px; color: #555555;' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		
		//System.out.println(sb.toString());
		return sb.toString();
	}
	
	
	
	public static String getPaginationTrippleGrid2(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:700px;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				//sb.append("<<");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				//sb.append("<");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			//
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				//sb.append(">");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				//sb.append(">>");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize3' id='pageSize3' style='width: 60px;height:28px; color: #555555;' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		
		//System.out.println(sb.toString());
		return sb.toString();
	}
	
	public static String getPaginationNoDivGrid(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				//sb.append("<<");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				//sb.append("<");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			//
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				//sb.append(">");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				//sb.append(">>");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='pageSize3' id='pageSize3' style='width: 60px;height:28px; color: #555555;' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
		}
		
		//System.out.println(sb.toString());
		return sb.toString();
	}
	public static String getPagination_pageSizeVariableName(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr,String pageSizeVariableName,String width)
	{		
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{			
			sb.append("<div class='net-widget-footer net-corner-bottom'>");
			sb.append("<div class=''>");
			sb.append("<table width='"+width+"' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				//sb.append("<<");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				//sb.append("<");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			//
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				//sb.append(">");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				//sb.append(">>");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append("Records Per Page ");
			sb.append("<select name='"+pageSizeVariableName+"' id='"+pageSizeVariableName+"' style='width: 60px;height:28px; color: #555555;' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		
		//System.out.println(sb.toString());
		return sb.toString();
	}
	
	
	
	public static String getPaginationPaging(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr,String pagingName)
	{
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		
		String stylecss="vertical-align: middle;padding-top:5px;font-weight: bold;";
		
		if(totalRow>minPgSize)
		{
			sb.append("<div class='net-widget-footer net-corner-bottom divwidth'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left' style='"+stylecss+"'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap  style='"+stylecss+"'>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap  style='"+stylecss+"'>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			//
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap  style='"+stylecss+"'>");
			sb.append("Records Per Page ");
			sb.append("<select name='"+pagingName+"' id='"+pagingName+"' style='width: 60px;height:28px; color: #555555;' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		
		return sb.toString();
	}
	public static String responseLink(String fieldLabel){
		StringBuffer sbResponseText = new StringBuffer("");
		try{
				sbResponseText.append("<span style=''>");
					sbResponseText.append("<table border='0px'style='width: auto;'>");
					sbResponseText.append("<tr>");
					sbResponseText.append("<td valign='center' rowspan='2' height='30'>"+fieldLabel+"</td>");
					sbResponseText.append("</tr>");
					sbResponseText.append("</table>");
			sbResponseText.append("</span>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sbResponseText+"";
	}
	
	
 public static String getPaginationForZoneSchool(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				//sb.append("<<");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				//sb.append("<");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			//
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				//sb.append(">");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				//sb.append(">>");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append(Utility.getLocaleValuePropByKey("msgRecordsPerPage", locale));
			sb.append("<select name='pageSizeZoneSchool' id='pageSizeZoneSchool' style='width: 60px;height:28px; color: #555555;' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		
		//System.out.println(sb.toString());
		return sb.toString();
	}
	
 public static String getPaginationDoubleStringPnr(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingPnr('1') \">");
				//sb.append("<<");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingPnr('"+(pageNo-1)+"') \" >");
				//sb.append("<");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			//
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPagingPnr('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingPnr('"+(pageNo+1)+"') \"  >");
				//sb.append(">");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingPnr('"+(noOfPage)+"') \"   >");
				//sb.append(">>");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append(Utility.getLocaleValuePropByKey("msgRecordsPerPage", locale));
			sb.append("<select name='pageSize1' id='pageSize1' style='width: 60px;height:28px; color: #555555;' onchange=\"getPagingPnr('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		
		//System.out.println(sb.toString());
		return sb.toString();
	}
 
 public static String responseSortingLinkPnr(String fieldLabel, String sortOrderFieldName,String fieldName,String sortOrderTypeVal,int pgNo){
		StringBuffer sbResponseText = new StringBuffer("");
		try{
				sbResponseText.append("<span style=''>");
				if( sortOrderTypeVal.equals("1") && sortOrderFieldName.equals(fieldName)){
					sbResponseText.append("<table border='0px'style='width: auto;'>");
					sbResponseText.append("<tr>");
					sbResponseText.append("<td valign='top'>"+fieldLabel+"</td>");
					sbResponseText.append("<td valign='top'><div style='margin-bottom: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSortingPnr('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></div></td>");
					sbResponseText.append("</table>");
				}else if( sortOrderTypeVal.equals("0") && sortOrderFieldName.equals(fieldName)){

					sbResponseText.append("<table border='0px'style='width: auto;'>");
					sbResponseText.append("<tr>");
					sbResponseText.append("<td valign='top'>"+fieldLabel+"</td>");
					sbResponseText.append("<td valign='top'><div style='margin-top: 0px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSortingPnr('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></div></td>");
					sbResponseText.append("</table>");
				}else{
					sbResponseText.append("<table border='0px'style='width: auto;'>");
					sbResponseText.append("<tr>");
					sbResponseText.append("<td valign='top'rowspan='2'>"+fieldLabel+"</td>");
					sbResponseText.append("<td valign='top'><div style='margin-bottom: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSortingPnr('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></div></td>");
					sbResponseText.append("</tr>");
					sbResponseText.append("<tr>");
					sbResponseText.append("<td valign='top'><div style='margin-top: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSortingPnr('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></div></td>");
					sbResponseText.append("</tr>");
					sbResponseText.append("</table>");
				}


			sbResponseText.append("</span>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sbResponseText+"";
	}
 
 public static String getPaginationStringCommon(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{		
		//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");
		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//
		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;
		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
			sb.append("<div class='net-widget-footer  net-corner-bottom'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");
			
			
			sb.append("<td width='1%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append(Utility.getLocaleValuePropByKey("msgRecordsPerPage", locale));
			sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPaging('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		return sb.toString();
	}
 
 public static String getPaginationStringForFacilitatorAjax(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
	{	//--Pagination and Sorting
		StringBuffer sb = new StringBuffer("");		
		int pageNo = Integer.parseInt(pageNoStr);
		int k;
		//
		int noOfPage=0;
		int totalRow=totalRecords;
		int noOfRow=Integer.parseInt(""+noOfRowInPage);
		int start;
		int end;
		//		
		noOfPage=(totalRow+noOfRow-1)/noOfRow;		
		int recordFrom=(noOfRow*(pageNo-1))+1;
		int recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
		int minPgSize = Integer.parseInt(gridPageSize[0]);
		for(String no:gridPageSize)
		{
			if(Integer.parseInt(no)<minPgSize)
				minPgSize=Integer.parseInt(no);
		}
		if(totalRow>minPgSize)
		{
				
			sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:945px;'>");
			sb.append("<div class=''>");
			sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
			sb.append("<tr>");
			sb.append("<td width='1%' ></td>");
			
			sb.append("<td width='28%' nowrap align='left'>");
			sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
			sb.append("</td>");			
			
			sb.append("<td width='15%'  nowrap>");
	
			if(pageNo!=1)
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingforFacilitator('1') \">");
				sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingforFacilitator('"+(pageNo-1)+"') \" >");
				sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
				sb.append("</A>");
			}
			sb.append("</td>");
			sb.append("<td  width='25%'  align='left' nowrap>");
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb.append("<B>");		
					sb.append(""+k+"&nbsp;</B>");
				}
				else
				{
					sb.append("<A class='pagingLink' HREF=\"javascript:getPagingforFacilitator('"+k+"') \" >");
					sb.append(k+"&nbsp;");
					sb.append("</A>");
				}
			}
			if(pageNo<(k-1))
			{
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingforFacilitator('"+(pageNo+1)+"') \"  >");
				sb.append("&nbsp;|&nbsp;&nbsp;"+Utility.getLocaleValuePropByKey("btnNext", locale)+"&gt;>&nbsp;&nbsp;");
				sb.append("</A>");
	
				sb.append("<A class='pagingLink' HREF=\"javascript:getPagingforFacilitator('"+(noOfPage)+"') \"   >");
				sb.append("|&nbsp;&nbsp;&nbsp;Last");
				sb.append("</A>");	
			}
			
			
			sb.append("</td>");
			sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
			sb.append(Utility.getLocaleValuePropByKey("msgRecordsPerPage", locale));
			sb.append("<select name='pageSize20' id='pageSize20' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPagingforFacilitator('');\" >");
			
			for(String no:gridPageSize)
			{
				sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
			}		
			sb.append("</select>");
			sb.append("</td>");
			sb.append("<td  width='1%'></td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
			sb.append("</div>");	
		}
		return sb.toString();
	}
 
 public static String responseSortingLinkCommonMul(String fieldLabel, String sortOrderFieldName,String fieldName,String sortOrderTypeVal,int pgNo,String gridName){
     StringBuffer sbResponseText = new StringBuffer("");
     try{
                 sbResponseText.append("<span style=''>");
                 if( sortOrderTypeVal.equals("1") && sortOrderFieldName.equals(fieldName)){
                       sbResponseText.append("<table border='0px'style='width: auto;'>");
                       sbResponseText.append("<tr>");
                       sbResponseText.append("<td valign='top'>"+fieldLabel+"</td>");
                       sbResponseText.append("<td valign='top'><div style='margin-bottom: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSortingCommonMul('"+(pgNo)+"','"+fieldName+"',0,'"+gridName+"')\"><span class=\"dscorder\"></span></a></div></td>");
                       sbResponseText.append("</table>");
                 }else if( sortOrderTypeVal.equals("0") && sortOrderFieldName.equals(fieldName)){

                       sbResponseText.append("<table border='0px'style='width: auto;'>");
                       sbResponseText.append("<tr>");
                       sbResponseText.append("<td valign='top'>"+fieldLabel+"</td>");
                       sbResponseText.append("<td valign='top'><div style='margin-top: 0px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSortingCommonMul('"+(pgNo)+"','"+fieldName+"',1,'"+gridName+"')\"><span class=\"ascorder\"></span></a></div></td>");
                       sbResponseText.append("</table>");
                 }else{
                       sbResponseText.append("<table border='0px'style='width: auto;'>");
                       sbResponseText.append("<tr>");
                       sbResponseText.append("<td valign='top'rowspan='2'>"+fieldLabel+"</td>");
                       sbResponseText.append("<td valign='top'><div style='margin-bottom: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSortingCommonMul('"+(pgNo)+"','"+fieldName+"',0,'"+gridName+"')\"><span class=\"dscorder\"></span></a></div></td>");
                       sbResponseText.append("</tr>");
                       sbResponseText.append("<tr>");
                       sbResponseText.append("<td valign='top'><div style='margin-top: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSortingCommonMul('"+(pgNo)+"','"+fieldName+"',1,'"+gridName+"')\"><span class=\"ascorder\"></span></a></div></td>");
                       sbResponseText.append("</tr>");
                       sbResponseText.append("</table>");
                 }


           sbResponseText.append("</span>");
     }catch(Exception e){
           e.printStackTrace();
     }
     return sbResponseText+"";
}

public static String getPaginationStringForParticipantsAjax(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr,String pageSizeVariableName,String width,String gridName)
{           
//--Pagination and Sorting
StringBuffer sb = new StringBuffer("");         
int pageNo = Integer.parseInt(pageNoStr);
int k;
//
int noOfPage=0;
int totalRow=totalRecords;
int noOfRow=Integer.parseInt(""+noOfRowInPage);
int start;
int end;
//

noOfPage=(totalRow+noOfRow-1)/noOfRow;

int recordFrom=(noOfRow*(pageNo-1))+1;
int recordTo;
if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
{
     recordTo=totalRow;
}
else
{
     recordTo=(noOfRow*(pageNo-1))+noOfRow;
}
String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
int minPgSize = Integer.parseInt(gridPageSize[0]);
for(String no:gridPageSize)
{
     if(Integer.parseInt(no)<minPgSize)
           minPgSize=Integer.parseInt(no);
}
if(totalRow>minPgSize)
{                 
     sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:"+width+"px;'>");
     sb.append("<div class=''>");
     sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
     sb.append("<tr>");
     sb.append("<td width='1%' ></td>");
     
     sb.append("<td width='28%' nowrap align='left'>");
     sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
     sb.append("</td>");
     
     
     sb.append("<td width='15%'  nowrap>");

     if(pageNo!=1)
     {
           sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1','"+gridName+"') \">");
           //sb.append("<<");
           sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
           sb.append("</A>");
           
           sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"','"+gridName+"') \" >");
           //sb.append("<");
           sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
           sb.append("</A>");
     
     }
     sb.append("</td>");
     sb.append("<td  width='25%'  align='left' nowrap>");
     if(noOfPage>5 && pageNo>3)
     {
           if(pageNo+2>noOfPage)
           {
                 start=noOfPage-4;
                 end=noOfPage;
           }
           else
           {
                 start=pageNo-2;
                 end=pageNo+2;
           }
     }
     else
     {
           
           if(noOfPage<5)
           {
                 start=1;
                 end=noOfPage;
           }
           else
           {
                 start=1;
                 end=5;
           }
     }
     //
     for(k=start;k<=end;k++)
     {
           if(k==pageNo)
           {
                 sb.append("<B>");       
                 sb.append(""+k+"&nbsp;</B>");
           }
           else
           {
                 sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"','"+gridName+"') \" >");
                 sb.append(k+"&nbsp;");
                 sb.append("</A>");
           }
     }
     if(pageNo<(k-1))
     {

           sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"','"+gridName+"') \"  >");
           //sb.append(">");
           sb.append("&nbsp;|&nbsp;&nbsp;"+Utility.getLocaleValuePropByKey("btnNext", locale)+"&gt;>&nbsp;&nbsp;");
           sb.append("</A>");

           sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"','"+gridName+"') \"   >");
           //sb.append(">>");
           sb.append("|&nbsp;&nbsp;&nbsp;Last");
           sb.append("</A>");      
     }
     
     
     sb.append("</td>");
     sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
     sb.append(Utility.getLocaleValuePropByKey("msgRecordsPerPage", locale));
     sb.append("<select name='"+pageSizeVariableName+"' id='"+pageSizeVariableName+"' style='width: 60px;height:28px; color: #555555;' onchange=\"getPaging('','"+gridName+"');\" >");
     
     for(String no:gridPageSize)
     {
           sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
     }           
     sb.append("</select>");
     sb.append("</td>");
     sb.append("<td  width='1%'></td>");
     sb.append("</tr>");
     sb.append("</table>");
     sb.append("</div>");
     sb.append("</div>");    
}

//System.out.println(sb.toString());
return sb.toString();
}

//shadab start pagination for elasticsearch changes only getPaging to getESPaging
public static String getESPaginationStringForPanelAjax(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
{	//--Pagination and Sorting
	StringBuffer sb = new StringBuffer("");		
	int pageNo = Integer.parseInt(pageNoStr);
	int k;
	//
	int noOfPage=0;
	int totalRow=totalRecords;
	int noOfRow=Integer.parseInt(""+noOfRowInPage);
	int start;
	int end;
	//		
	noOfPage=(totalRow+noOfRow-1)/noOfRow;		
	int recordFrom=(noOfRow*(pageNo-1))+1;
	int recordTo;
	if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
	{
		recordTo=totalRow;
	}
	else
	{
		recordTo=(noOfRow*(pageNo-1))+noOfRow;
	}
	String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
	int minPgSize = Integer.parseInt(gridPageSize[0]);
	for(String no:gridPageSize)
	{
		if(Integer.parseInt(no)<minPgSize)
			minPgSize=Integer.parseInt(no);
	}
	if(minPgSize <= 10)
		minPgSize=0;
	if(totalRow>minPgSize)
	{
			
		sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:945px;'>");
		sb.append("<div class=''>");
		sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
		sb.append("<tr>");
		sb.append("<td width='1%' ></td>");
		
		sb.append("<td width='28%' nowrap align='left'>");
		sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
		sb.append("</td>");			
		
		sb.append("<td width='15%'  nowrap>");

		if(pageNo!=1)
		{
			sb.append("<A class='pagingLink' HREF=\"javascript:getESPaging('1') \">");
			sb.append("First&nbsp;&nbsp;|");
			sb.append("</A>");
			sb.append("<A class='pagingLink' HREF=\"javascript:getESPaging('"+(pageNo-1)+"') \" >");
			sb.append("&nbsp;&nbsp;<&lt;Previous&nbsp;&nbsp;|");
			sb.append("</A>");
		}
		sb.append("</td>");
		sb.append("<td  width='25%'  align='left' nowrap>");
		if(noOfPage>5 && pageNo>3)
		{
			if(pageNo+2>noOfPage)
			{
				start=noOfPage-4;
				end=noOfPage;
			}
			else
			{
				start=pageNo-2;
				end=pageNo+2;
			}
		}
		else
		{
			
			if(noOfPage<5)
			{
				start=1;
				end=noOfPage;
			}
			else
			{
				start=1;
				end=5;
			}
		}
		for(k=start;k<=end;k++)
		{
			if(k==pageNo)
			{
				sb.append("<B>");		
				sb.append(""+k+"&nbsp;</B>");
			}
			else
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getESPaging('"+k+"') \" >");
				sb.append(k+"&nbsp;");
				sb.append("</A>");
			}
		}
		if(pageNo<(k-1))
		{

			sb.append("<A class='pagingLink' HREF=\"javascript:getESPaging('"+(pageNo+1)+"') \"  >");
			sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
			sb.append("</A>");

			sb.append("<A class='pagingLink' HREF=\"javascript:getESPaging('"+(noOfPage)+"') \"   >");
			sb.append("|&nbsp;&nbsp;&nbsp;Last");
			sb.append("</A>");	
		}
		
		
		sb.append("</td>");
		sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
		sb.append("Records Per Page ");
		sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getESPaging('');\" >");
		
		for(String no:gridPageSize)
		{
			sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
		}		
		sb.append("</select>");
		sb.append("</td>");
		sb.append("<td  width='1%'></td>");
		sb.append("</tr>");
		sb.append("</table>");
		sb.append("</div>");
		sb.append("</div>");	
	}
	return sb.toString();
}
public static String responseSortingLinkES(String fieldLabel, String sortOrderFieldName,String fieldName,String sortOrderTypeVal,int pgNo){
	StringBuffer sbResponseText = new StringBuffer("");
	try{
			sbResponseText.append("<span style=''>");
			if( sortOrderTypeVal.equals("1") && sortOrderFieldName.equals(fieldName)){
				sbResponseText.append("<table border='0px'style='width: auto;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top'><div style='margin-bottom: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getESPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></div></td>");
				sbResponseText.append("</table>");
			}else if( sortOrderTypeVal.equals("0") && sortOrderFieldName.equals(fieldName)){

				sbResponseText.append("<table border='0px'style='width: auto;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top'><div style='margin-top: 0px;'>&nbsp;<a class='net-header-text' href=\"javascript:getESPagingAndSorting('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></div></td>");
				sbResponseText.append("</table>");
			}else{
				sbResponseText.append("<table border='0px'style='width: auto;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top'rowspan='2'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top'><div style='margin-bottom: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getESPagingAndSorting('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></div></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top'><div style='margin-top: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getESPagingAndSorting('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></div></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");
			}


		sbResponseText.append("</span>");
	}catch(Exception e){
		e.printStackTrace();
	}
	return sbResponseText+"";
}


public static String getESPaginationStringForJobboard(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
{	//--Pagination and Sorting
	StringBuffer sb = new StringBuffer("");		
	int pageNo = Integer.parseInt(pageNoStr);
	int k;
	//
	int noOfPage=0;
	int totalRow=totalRecords;
	int noOfRow=Integer.parseInt(""+noOfRowInPage);
	int start;
	int end;
	//		
	noOfPage=(totalRow+noOfRow-1)/noOfRow;		
	int recordFrom=(noOfRow*(pageNo-1))+1;
	int recordTo;
	if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
	{
		recordTo=totalRow;
	}
	else
	{
		recordTo=(noOfRow*(pageNo-1))+noOfRow;
	}
	String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
	int minPgSize = Integer.parseInt(gridPageSize[0]);
	for(String no:gridPageSize)
	{
		if(Integer.parseInt(no)<minPgSize)
			minPgSize=Integer.parseInt(no);
	}
	if(totalRow>minPgSize)
	{				
		sb.append("<div class='net-widget-footer  net-corner-bottom' style='padding:5px;width:100%;'>");
		sb.append("<div class=''>");
		sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
		sb.append("<tr>");
		//sb.append("<td width='1%' ></td>");
		
		sb.append("<td width='3%' nowrap align='center'>");
		sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
		sb.append("</td>");			
		
	//	sb.append("<td width='1%'  nowrap>");

		
		//sb.append("</td>");
		sb.append("<td  width='8%'  align='center' nowrap>");
		if(pageNo!=1)
		{
			sb.append("<A class='pagingLink' HREF=\"javascript:getESPagingForJobboard('1') \">");
			sb.append("First&nbsp;&nbsp;|");
			sb.append("</A>");
			sb.append("<A class='pagingLink' HREF=\"javascript:getESPagingForJobboard('"+(pageNo-1)+"') \" >");
			sb.append("&nbsp;&nbsp;<&lt;Previous&nbsp;&nbsp;|");
			sb.append("</A>");
		}
		if(noOfPage>5 && pageNo>3)
		{
			if(pageNo+2>noOfPage)
			{
				start=noOfPage-4;
				end=noOfPage;
			}
			else
			{
				start=pageNo-2;
				end=pageNo+2;
			}
		}
		else
		{
			
			if(noOfPage<5)
			{
				start=1;
				end=noOfPage;
			}
			else
			{
				start=1;
				end=5;
			}
		}
		for(k=start;k<=end;k++)
		{
			if(k==pageNo)
			{
				sb.append("<B>");		
				sb.append(""+k+"&nbsp;</B>");
			}
			else
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getESPagingForJobboard('"+k+"') \" >");
				sb.append(k+"&nbsp;");
				sb.append("</A>");
			}
		}
		if(pageNo<(k-1))
		{

			sb.append("<A class='pagingLink' HREF=\"javascript:getESPagingForJobboard('"+(pageNo+1)+"') \"  >");
			sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
			sb.append("</A>");

			sb.append("<A class='pagingLink' HREF=\"javascript:getESPagingForJobboard('"+(noOfPage)+"') \"   >");
			sb.append("|&nbsp;&nbsp;&nbsp;Last");
			sb.append("</A>");	
		}
		
		
		sb.append("</td>");
		sb.append("<td  width='2%' align='center'  class='pagingBoldText' nowrap>");
		sb.append("Records Per Page ");
		sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getESPagingForJobboard('');\" >");
		
		for(String no:gridPageSize)
		{
			sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
		}		
		sb.append("</select>");
		sb.append("</td>");
		sb.append("</tr>");
		sb.append("</table>");
		sb.append("</div>");
		sb.append("</div>");	
	}
	return sb.toString();
}
//shadab end
public static String getPaginationForKellyOnboarding(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
{		
	System.out.println("use by candidate pools.");
	//--Pagination and Sorting
	StringBuffer sb = new StringBuffer("");
	
	int pageNo = Integer.parseInt(pageNoStr);
	int k;
	//
	int noOfPage=0;
	int totalRow=totalRecords;
	int noOfRow=Integer.parseInt(""+noOfRowInPage);
	int start;
	int end;
	//
	
	noOfPage=(totalRow+noOfRow-1)/noOfRow;
	
	int recordFrom=(noOfRow*(pageNo-1))+1;
	int recordTo;
	if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
	{
		recordTo=totalRow;
	}
	else
	{
		recordTo=(noOfRow*(pageNo-1))+noOfRow;
	}
	String[] gridPageSize = {"10","20","50","100","500"};
	int minPgSize = 0;
	for(String no:gridPageSize)
	{
		if(Integer.parseInt(no)<minPgSize)
			minPgSize=Integer.parseInt(no);
	}
	if(totalRow>minPgSize)
	{
			
		sb.append("<div class='net-widget-footer  net-corner-bottom' style='width:1100px;'>");
		sb.append("<div class=''>");
		sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
		sb.append("<tr>");
		sb.append("<td width='1%' ></td>");
		
		sb.append("<td width='28%' nowrap align='left'>");
		sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Record(s)");
		sb.append("</td>");
		
		
		sb.append("<td width='15%'  nowrap>");

		if(pageNo!=1)
		{
			sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
			sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
			sb.append("</A>");
			sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
			sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
			sb.append("</A>");
		}
		sb.append("</td>");
		sb.append("<td  width='25%'  align='left' nowrap>");
		if(noOfPage>5 && pageNo>3)
		{
			if(pageNo+2>noOfPage)
			{
				start=noOfPage-4;
				end=noOfPage;
			}
			else
			{
				start=pageNo-2;
				end=pageNo+2;
			}
		}
		else
		{
			
			if(noOfPage<5)
			{
				start=1;
				end=noOfPage;
			}
			else
			{
				start=1;
				end=5;
			}
		}
		for(k=start;k<=end;k++)
		{
			if(k==pageNo)
			{
				sb.append("<B>");		
				sb.append(""+k+"&nbsp;</B>");
			}
			else
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
				sb.append(k+"&nbsp;");
				sb.append("</A>");
			}
		}
		if(pageNo<(k-1))
		{

			sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
			sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
			sb.append("</A>");

			sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
			sb.append("|&nbsp;&nbsp;&nbsp;Last");
			sb.append("</A>");	
		}
		
		
		sb.append("</td>");
		sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
		sb.append("Records Per Page ");
		sb.append("<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\"getPaging('');\" >");
		
		for(String no:gridPageSize)
		{
			sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
		}		
		sb.append("</select>");
		sb.append("</td>");
		sb.append("<td  width='1%'></td>");
		sb.append("</tr>");
		sb.append("</table>");
		sb.append("</div>");
		sb.append("</div>");	
	}
	return sb.toString();
}

public static String responseSortingMLinkSSPF(String fieldLabel, String sortOrderFieldName,String fieldName,String sortOrderTypeVal,int pgNo){
	StringBuffer sbResponseText = new StringBuffer("");
	try{
		sbResponseText.append("<span style=''>");
		if( sortOrderTypeVal.equals("1") && sortOrderFieldName.equals(fieldName)){
			sbResponseText.append("<table border='0px'style='width: auto;'>");
			sbResponseText.append("<tr>");
			sbResponseText.append("<td valign='top' rowspan='2' style='text-align:left;vertical-align:middle;height:25px;padding-left:4px;'>"+fieldLabel+"</td>");
			sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:13px;padding-left:4px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSortingSSPF('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></td>");
			sbResponseText.append("</tr>");
			sbResponseText.append("<tr>");
			sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:12px;padding-left:4px;'></td>");
			sbResponseText.append("</tr>");
			sbResponseText.append("</table>");	
		}else if( sortOrderTypeVal.equals("0") && sortOrderFieldName.equals(fieldName)){
			sbResponseText.append("<table border='0px'style='width: auto;'>");
			sbResponseText.append("<tr>");
			sbResponseText.append("<td valign='top' rowspan='2' style='text-align:left;vertical-align:middle;height:25px;padding-left:4px;'>"+fieldLabel+"</td>");
			sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:13px;padding-left:4px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSortingSSPF('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></td>");
			sbResponseText.append("</tr>");
			sbResponseText.append("<tr>");
			sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:12px;padding-left:4px;'></td>");
			sbResponseText.append("</tr>");
			sbResponseText.append("</table>");	
		}else{
			sbResponseText.append("<table border='0px' style='width: auto;'>");
			sbResponseText.append("<tr>");
			sbResponseText.append("<td valign='top' rowspan='2' style='text-align:left;vertical-align:middle;height:25px;padding-left:4px;'>"+fieldLabel+"</td>");
			sbResponseText.append("<td valign='top' style='width:500px;vertical-align:bottom;height:13px;padding-left:4px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSortingSSPF('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></td>");
			sbResponseText.append("</tr>");
			sbResponseText.append("<tr>");
			sbResponseText.append("<td valign='top' style='width:500px;vertical-align:middle;height:12px;padding-left:4px;'><a   class='net-header-text'  href=\"javascript:getPagingAndSortingSSPF('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></td>");
			sbResponseText.append("</tr>");
			sbResponseText.append("</table>");	
		}
		sbResponseText.append("</span>");
	}catch(Exception e){
		e.printStackTrace();
	}
	return sbResponseText+"";
}
public static String responseSortingLinkSSPF(String fieldLabel, String sortOrderFieldName,String fieldName,String sortOrderTypeVal,int pgNo){
	StringBuffer sbResponseText = new StringBuffer("");
	try{
			sbResponseText.append("<span style=''>");
			if( sortOrderTypeVal.equals("1") && sortOrderFieldName.equals(fieldName)){
				sbResponseText.append("<table border='0px'style='width: auto;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top'><div style='margin-bottom: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSortingSSPF('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></div></td>");
				sbResponseText.append("</table>");
			}else if( sortOrderTypeVal.equals("0") && sortOrderFieldName.equals(fieldName)){

				sbResponseText.append("<table border='0px'style='width: auto;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top'><div style='margin-top: 0px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSortingSSPF('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></div></td>");
				sbResponseText.append("</table>");
			}else{
				sbResponseText.append("<table border='0px'style='width: auto;'>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top'rowspan='2'>"+fieldLabel+"</td>");
				sbResponseText.append("<td valign='top'><div style='margin-bottom: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSortingSSPF('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></div></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("<tr>");
				sbResponseText.append("<td valign='top'><div style='margin-top: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:getPagingAndSortingSSPF('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></div></td>");
				sbResponseText.append("</tr>");
				sbResponseText.append("</table>");
			}


		sbResponseText.append("</span>");
	}catch(Exception e){
		e.printStackTrace();
	}
	return sbResponseText+"";
}
public static String getPaginationDoubleStringSSPF(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
{
	//--Pagination and Sorting
	StringBuffer sb = new StringBuffer("");
	
	int pageNo = Integer.parseInt(pageNoStr);
	int k;
	//
	int noOfPage=0;
	int totalRow=totalRecords;
	int noOfRow=Integer.parseInt(""+noOfRowInPage);
	int start;
	int end;
	//
	
	noOfPage=(totalRow+noOfRow-1)/noOfRow;
	
	int recordFrom=(noOfRow*(pageNo-1))+1;
	int recordTo;
	if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
	{
		recordTo=totalRow;
	}
	else
	{
		recordTo=(noOfRow*(pageNo-1))+noOfRow;
	}
	String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
	int minPgSize = Integer.parseInt(gridPageSize[0]);
	for(String no:gridPageSize)
	{
		if(Integer.parseInt(no)<minPgSize)
			minPgSize=Integer.parseInt(no);
	}
	if(totalRow>minPgSize)
	{
			
		sb.append("<div class='net-widget-footer  net-corner-bottom'>");
		sb.append("<div class=''>");
		sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
		sb.append("<tr>");
		sb.append("<td width='1%' ></td>");
		
		sb.append("<td width='28%' nowrap align='left'>");
		sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
		sb.append("</td>");
		
		
		sb.append("<td width='15%'  nowrap>");

		if(pageNo!=1)
		{
			sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
			//sb.append("<<");
			sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
			sb.append("</A>");
			
			sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
			//sb.append("<");
			sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
			sb.append("</A>");
		
		}
		sb.append("</td>");
		sb.append("<td  width='25%'  align='left' nowrap>");
		if(noOfPage>5 && pageNo>3)
		{
			if(pageNo+2>noOfPage)
			{
				start=noOfPage-4;
				end=noOfPage;
			}
			else
			{
				start=pageNo-2;
				end=pageNo+2;
			}
		}
		else
		{
			
			if(noOfPage<5)
			{
				start=1;
				end=noOfPage;
			}
			else
			{
				start=1;
				end=5;
			}
		}
		//
		for(k=start;k<=end;k++)
		{
			if(k==pageNo)
			{
				sb.append("<B>");		
				sb.append(""+k+"&nbsp;</B>");
			}
			else
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
				sb.append(k+"&nbsp;");
				sb.append("</A>");
			}
		}
		if(pageNo<(k-1))
		{

			sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
			//sb.append(">");
			sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
			sb.append("</A>");

			sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
			//sb.append(">>");
			sb.append("|&nbsp;&nbsp;&nbsp;Last");
			sb.append("</A>");	
		}
		
		
		sb.append("</td>");
		sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
		sb.append("Records Per Page ");
		sb.append("<select name='pageSize1' id='pageSize1' style='width: 60px;height:28px; color: #555555;' onchange=\"getPaging('');\" >");
		
		for(String no:gridPageSize)
		{
			sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
		}		
		sb.append("</select>");
		sb.append("</td>");
		sb.append("<td  width='1%'></td>");
		sb.append("</tr>");
		sb.append("</table>");
		sb.append("</div>");
		sb.append("</div>");	
	}
	
	//System.out.println(sb.toString());
	return sb.toString();
}
public static String getPaginationTrippleGridSSPF(HttpServletRequest request, int totalRecords, String noOfRowInPage, String pageNoStr)
{
	//--Pagination and Sorting
	StringBuffer sb = new StringBuffer("");
	
	int pageNo = Integer.parseInt(pageNoStr);
	int k;
	//
	int noOfPage=0;
	int totalRow=totalRecords;
	int noOfRow=Integer.parseInt(""+noOfRowInPage);
	int start;
	int end;
	//
	
	noOfPage=(totalRow+noOfRow-1)/noOfRow;
	
	int recordFrom=(noOfRow*(pageNo-1))+1;
	int recordTo;
	if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
	{
		recordTo=totalRow;
	}
	else
	{
		recordTo=(noOfRow*(pageNo-1))+noOfRow;
	}
	String[] gridPageSize = Utility.getValueOfPropByKey("gridPageSize").split(",");
	int minPgSize = Integer.parseInt(gridPageSize[0]);
	for(String no:gridPageSize)
	{
		if(Integer.parseInt(no)<minPgSize)
			minPgSize=Integer.parseInt(no);
	}
	if(totalRow>minPgSize)
	{
			
		sb.append("<div class='net-widget-footer  net-corner-bottom'>");
		sb.append("<div class=''>");
		sb.append("<table width='100%' cellspacing='0px' cellpadding='0px' >");
		sb.append("<tr>");
		sb.append("<td width='1%' ></td>");
		
		sb.append("<td width='28%' nowrap align='left'>");
		sb.append(""+recordFrom+" - "+recordTo+" of "+totalRow+" Records");
		sb.append("</td>");
		
		
		sb.append("<td width='15%'  nowrap>");

		if(pageNo!=1)
		{
			sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('1') \">");
			//sb.append("<<");
			sb.append(Utility.getLocaleValuePropByKey("msgFirst3", locale)+"&nbsp;&nbsp;|");
			sb.append("</A>");
			
			sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo-1)+"') \" >");
			//sb.append("<");
			sb.append("&nbsp;&nbsp;<&lt;"+Utility.getLocaleValuePropByKey("msgPrevious3", locale)+"&nbsp;&nbsp;|");
			sb.append("</A>");
		
		}
		sb.append("</td>");
		sb.append("<td  width='25%'  align='left' nowrap>");
		if(noOfPage>5 && pageNo>3)
		{
			if(pageNo+2>noOfPage)
			{
				start=noOfPage-4;
				end=noOfPage;
			}
			else
			{
				start=pageNo-2;
				end=pageNo+2;
			}
		}
		else
		{
			
			if(noOfPage<5)
			{
				start=1;
				end=noOfPage;
			}
			else
			{
				start=1;
				end=5;
			}
		}
		//
		for(k=start;k<=end;k++)
		{
			if(k==pageNo)
			{
				sb.append("<B>");		
				sb.append(""+k+"&nbsp;</B>");
			}
			else
			{
				sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+k+"') \" >");
				sb.append(k+"&nbsp;");
				sb.append("</A>");
			}
		}
		if(pageNo<(k-1))
		{

			sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(pageNo+1)+"') \"  >");
			//sb.append(">");
			sb.append("&nbsp;|&nbsp;&nbsp;Next&gt;>&nbsp;&nbsp;");
			sb.append("</A>");

			sb.append("<A class='pagingLink' HREF=\"javascript:getPaging('"+(noOfPage)+"') \"   >");
			//sb.append(">>");
			sb.append("|&nbsp;&nbsp;&nbsp;Last");
			sb.append("</A>");	
		}
		
		
		sb.append("</td>");
		sb.append("<td  width='29%' align='right'  class='pagingBoldText' nowrap>");
		sb.append("Records Per Page ");
		sb.append("<select name='pageSize3' id='pageSize3' style='width: 60px;height:28px; color: #555555;' onchange=\"getPaging('');\" >");
		
		for(String no:gridPageSize)
		{
			sb.append("<option value='"+no+"' "+(noOfRow==Integer.parseInt(no)?"selected":"")+" >"+no+"</option>");
		}		
		sb.append("</select>");
		sb.append("</td>");
		sb.append("<td  width='1%'></td>");
		sb.append("</tr>");
		sb.append("</table>");
		sb.append("</div>");
		sb.append("</div>");	
	}
	
	//System.out.println(sb.toString());
	return sb.toString();
}
}


