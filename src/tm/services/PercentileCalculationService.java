package tm.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


public class PercentileCalculationService 
{
	
	private Map<String,Map> mapPercentileData = null;
	public  void setPercentile(double[] ar)
	{
		mapPercentileData = new LinkedHashMap<String, Map>();
		int fcheck =0;
		double uniqueData=0;
		double temp; 
		List<String> freqList = new ArrayList<String>(); 
		int noOfRecord = ar.length;
		
		
		
		Arrays.sort(ar);
		if(ar!=null && ar.length!=0)
		{
			temp = ar[0];
			for(int i=0;i<ar.length;i++)
			{
				if(ar[i]==temp)
				{
					fcheck++;
					uniqueData=ar[i];
				}
				else
				{
					temp=ar[i];
					freqList.add(uniqueData+"##"+fcheck);
					fcheck=1;
				}
				uniqueData=ar[i];
				
				if(ar.length==(i+1))
				{
					freqList.add(uniqueData+"##"+fcheck);
				}
				
			}
					
			/*for(double d:ar)
			{
				System.out.print(d+", ");
			}*/
			
			
			
			Map<String,String> record = null;
			double x;
			double f;
			double cf=0.0;
			double fby2;
			double cf_fby2;
			double cf_fby2byn;
			
			for(int i=0; i<freqList.size(); i++)
			{
				record = new HashMap<String, String>();
				x  = new Double(freqList.get(i).split("##")[0]);
				f  = new Integer(freqList.get(i).split("##")[1]);
				cf = cf+f;
				fby2 = f/2;			
				cf_fby2 = cf-fby2;
				cf_fby2byn = (cf-fby2)/noOfRecord;
				
				record.put("x",""+x);
				record.put("f",""+f);
				record.put("cf",""+cf);
				record.put("fby2", ""+fby2);
				record.put("cf_fby2", ""+cf_fby2);
				record.put("cf_fby2byn", ""+cf_fby2byn);
				record.put("pr", ""+cf_fby2byn*100);
				mapPercentileData.put(""+x, record);
			}
		}
		//System.out.println("mapPercentileData"+mapPercentileData);
	}
	
	
	
	public  double getF(double val)
	{
		double f = 0.0;
		try 
		{	
			Map<String,String> record  =(Map<String,String>) mapPercentileData.get(""+val);
			f =new Double(record.get("f"));		
		} 
		catch (Exception e) 
		{
			
		}
		return f;
	}
	public  double getCF(double val)
	{
		double cf = 0.0;
		try 
		{
			Map<String,String> record  =(Map<String,String>) mapPercentileData.get(""+val);
			cf =new Double(record.get("cf"));
		} 
		catch (Exception e) 
		{
			
		}
		return cf;
	}
	public  double getFby2(double val)
	{
		double fby2 = 0.0;
		try 
		{		
			Map<String,String> record  =(Map<String,String>) mapPercentileData.get(""+val);
			fby2 =new Double(record.get("fby2"));
		} 
		catch (Exception e) 
		{
			
		}
		return fby2;
	}
	public  double getCF_Fby3(double val)
	{
		double cf_fby2 = 0.0;
		try 
		{
			Map<String,String> record  =(Map<String,String>) mapPercentileData.get(""+val);
			cf_fby2 =new Double(record.get("cf_fby2"));
		}
		catch (Exception e) 
		{
			
		}
		return cf_fby2;	
	}
	public  double getCF_Fby2byn(double val)
	{
		double f = 0.0;
		try 
		{
			Map<String,String> record  =(Map<String,String>) mapPercentileData.get(""+val);
			f =new Double(record.get("cf_fby2byn"));
		} 
		catch (Exception e) 
		{
			
		}
		return f;
	}
	public  double getPr(double val)
	{
		double f = 0.0;
		try 
		{
			Map<String,String> record  =(Map<String,String>) mapPercentileData.get(""+val);
			f =new Double(record.get("pr"));
		}
		catch (Exception e) 
		{
			
		}
		return f;
	}
	
	public static void main(String[] args)
	{
		try 
		{
			//double[] ar = {14,19,22,25,45,54,65,12,4,45,32,4,5,55,55,55,65,55,4};
			//double[] ar = {24.00,24.00,27.00,27.00,27.00,28.00,29.00,29.00,30.00,30.00,31.00,31.00,31.00,31.00,32.00,32.00,32.00,32.00,32.00,32.00,32.00,32.00,32.00,33.00,33.00,33.00,33.00,34.00,38.00,39.00,42.00,42.00,14.00,19.00,22.00,24.00,25.00,26.00,27.00,28.00,29.00,30.00,31.00,32.00,33.00,34.00,35.00,38.00,39.00,42.00};
			//double[] ar = {140,190,202,265,450,540,654,120,400,450,320,400,50,550,550,550,650,550,400,1000};
			double[] ar = {160,160,90,60};
			
			PercentileCalculationService calculation = new PercentileCalculationService();
			
			calculation.setPercentile(ar);
			
			
		} 
		catch (Exception e) 
		{
			
		}
	}
	
	@Override
	public String toString() 
	{
		return mapPercentileData.toString();
	}
}
