package tm.services.charts;


import java.awt.*;
import java.io.File;

import org.jfree.chart.*;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.*;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.*;
import org.jfree.data.function.Function2D;
import org.jfree.data.function.NormalDistributionFunction2D;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYDifferenceRenderer;

public class ChartService {

	public String createPieChart(String[] value,int[] values,String path,int labelMarginLeft)
	{
		//System.out.println("createPieChart ------------------------------------ ");
		try{
			
			DefaultPieDataset pieDataset = new DefaultPieDataset();
			int i = 0;
			for(int d : values)
			{
				pieDataset.setValue(value[i]+" ("+d+"%)", new Integer(d));
				i++;
			}
			
			JFreeChart chart = ChartFactory.createPieChart("", pieDataset, true,false,true);
			
			chart.getLegend().setHorizontalAlignment(HorizontalAlignment.CENTER); 
			//chart.getLegend().setVerticalAlignment(VerticalAlignment.CENTER);
			//chart.getLegend().setWidth(100); 
			//chart.getLegend().setMargin(0, 30, 0, 0);
			chart.getLegend().setMargin(0,labelMarginLeft, 0, 0); 
			chart.getLegend().setFrame(BlockBorder.NONE);
			// chart.setBackgroundPaint(new Color(222, 222, 255));
			
			PiePlot plot = (PiePlot) chart.getPlot();
			plot.setBackgroundPaint(Color.white);
		    plot.setLabelFont(new Font("Tahoma", Font.PLAIN, 12));
		    plot.setNoDataMessage("No data available");
		    plot.setCircular(false);
		    plot.setLabelGap(0.02);
		    
		    //plot.setInteriorGap(0.0);
	        plot.setLabelGenerator(null);
	        // for border 
	        plot.setOutlineStroke(null);
	        //plot.setLabelBackgroundPaint(null);
	        //plot.setLabelShadowPaint(null);
			//chart.getPlot().setForegroundAlpha(0.5f);
			
			// ChartFactory.createPieChar
			//ChartUtilities.saveChartAsPNG(new File("C:\\pie3d.png"), chart, 400, 300);
			//System.out.println("Path: "+path);
			//ChartUtilities.saveChartAsPNG(new File(path), chart, 400, 300);
			//ChartUtilities.saveChartAsPNG(new File(path), chart, 380, 280);
	        ChartUtilities.saveChartAsPNG(new File(path), chart, 320, 320);
			//ChartUtilities.saveChartAsPNG(new File(path), chart, 700, 600);
		}catch (Exception e) {
			System.out.println(e);
		}

		return null;
	}
	
	public String createNormalCurveChart(String path,int labelMarginLeft)
	{
		//System.out.println("createPieChart ------------------------------------ ");
		try{
			
		/*	Function2D normal = new NormalDistributionFunction2D(0.0, 1.0);
	        XYDataset dataset = DatasetUtilities.sampleFunction2D(normal, -5.0, 5.0, 100, "Normal Curve");
	        final JFreeChart chart = ChartFactory.createXYLineChart(
	            //"XY Series Demo",
	        		"Normal Curve",
	            "X", 
	            "Y", 
	            dataset,
	            PlotOrientation.VERTICAL,
	            true,
	            true,
	            false
	        );
			chart.getLegend().setHorizontalAlignment(HorizontalAlignment.CENTER); 
			//chart.getLegend().setWidth(100); 
			//chart.getLegend().setMargin(0, 30, 0, 0);
			chart.getLegend().setMargin(0,labelMarginLeft, 0, 0); 
			chart.getLegend().setFrame(BlockBorder.NONE);
			// chart.setBackgroundPaint(new Color(222, 222, 255));
			
			XYPlot plot = (XYPlot) chart.getPlot();
			plot.setBackgroundPaint(Color.white);
		    plot.setNoDataMessage("No data available");
	        // for border 
	        plot.setOutlineStroke(null);*/
	     /////////////////////////////////////////////////////
			Function2D normal = new NormalDistributionFunction2D(0.0, 1.0);
			XYDataset dataset = DatasetUtilities.sampleFunction2D(normal, -4, 4, 100,
	                "Normal");

	        XYSeries fLine = new XYSeries("fLine");
	        fLine.add(-4, 0);
	        fLine.add(4, 0);
	        ((XYSeriesCollection) dataset).addSeries(fLine);

	        NumberAxis xAxis = new NumberAxis(null);
	        NumberAxis yAxis = new NumberAxis(null);
	        XYDifferenceRenderer renderer = new XYDifferenceRenderer();
	        xAxis.setRange(-5, 5);
	        XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);

	        final JFreeChart chart = new JFreeChart(plot);
	        chart.removeLegend();

	        
			
		/////////////////////////////////////////////////////
	        ChartUtilities.saveChartAsPNG(new File(path), chart, 320, 320);
		}catch (Exception e) {
			System.out.println(e);
		}

		return null;
	}
	
	public static void main(String[] args) {
		ChartService chartService = new ChartService();
		chartService.createNormalCurveChart("E://normalchart.jpg", 150);
	}

}
