package tm.services;

/* @Author: Sekhar 
 * @Discription: It is used to Teacher Upload
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MailToCandidateOnImport;
import tm.bean.MessageToTeacher;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherMailSend;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.TeacherUploadTemp;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.ExclusivePeriodMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.MailToCandidateOnImportDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.TeacherUploadTempDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.master.ExclusivePeriodMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class TeacherUploadTempAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	// **********  Adding by deepak ***********
	@Autowired
	private TeacherUploadTempDAO teacheruploadtempdao;
	public void setTeacherDetailDAO(TeacherUploadTempDAO teacherUploadTempDAO)
	{
		this.teacheruploadtempdao = teacherUploadTempDAO;
	}
	//****************************************
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO)
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@Autowired
	TeacherNormScoreDAO teacherNormScoreDAO;
	public void setTeacherNormScoreDAO(TeacherNormScoreDAO teacherNormScoreDAO) {
		this.teacherNormScoreDAO = teacherNormScoreDAO;
	}

	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	@Autowired
	private TeacherUploadTempDAO teacherUploadTempDAO;
	public void setTeacherUploadTempDAO(TeacherUploadTempDAO teacherUploadTempDAO)
	{
		this.teacherUploadTempDAO = teacherUploadTempDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) 
	{
		this.userMasterDAO = userMasterDAO;
	}
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) {
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}

	@Autowired
	private MailToCandidateOnImportDAO mailToCandidateOnImportDAO;
	public void setMailToCandidateOnImportDAO(
			MailToCandidateOnImportDAO mailToCandidateOnImportDAO) {
		this.mailToCandidateOnImportDAO = mailToCandidateOnImportDAO;
	}
	
	@Autowired
	private ExclusivePeriodMasterDAO exclusivePeriodMasterDAO;
	public void setExclusivePeriodMasterDAO(ExclusivePeriodMasterDAO exclusivePeriodMasterDAO)
	{
		this.exclusivePeriodMasterDAO = exclusivePeriodMasterDAO;
	}
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	private Vector vectorDataExcelXLSX = new Vector();
	public String displayTempTeacherRecords(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			System.out.println("calling displayTempJobRecords :::::::::::::;;;");
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			System.out.println("start"+start);
			System.out.println("end"+end);
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,47,"importcandidatedetails.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			/***** Sorting by  Stating ( Sekhar ) ******/
			List<TeacherUploadTemp> teacherUploadTemplst	  =	null;
			
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"teachertempid";
	
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				 {
					 sortOrderFieldName		=	sortOrder;
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion = Restrictions.eq("sessionid",session.getId());
			totalRecord = teacherUploadTempDAO.getRowCountTempTeacher(criterion);
			teacherUploadTemplst = teacherUploadTempDAO.findByTempTeacher(sortOrderStrVal,start,noOfRowInPage,criterion);
			
			
			/***** Sorting by Temp Teacher  End ******/
	
			dmRecords.append("<table  id='tempTeacherTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("Date Applied",sortOrderFieldName,"date_applied",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Job Id",sortOrderFieldName,"job_id",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("User First Name",sortOrderFieldName,"user_first_name",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("User Last Name",sortOrderFieldName,"user_last_name",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("User Email",sortOrderFieldName,"user_email",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Errors",sortOrderFieldName,"errortext",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(teacherUploadTemplst.size()==0)
				dmRecords.append("<tr><td colspan='6' align='center'>"+Utility.getLocaleValuePropByKey("lblNoCandidate", locale)+"</td></tr>" );
			System.out.println(Utility.getLocaleValuePropByKey("msgNoOfRecords", locale)+teacherUploadTemplst.size());

			for (TeacherUploadTemp teacherUploadTemp : teacherUploadTemplst){
				String jobId=teacherUploadTemp.getJob_id();
				String fName=teacherUploadTemp.getUser_first_name();
				String lName=teacherUploadTemp.getUser_last_name();
				String email=teacherUploadTemp.getUser_email();
				String rowCss="";
				
				if(!teacherUploadTemp.getErrortext().equalsIgnoreCase("")){
					rowCss="style=\"background-color:#FF0000;\"";
				}
				
				dmRecords.append("<tr >" );
				dmRecords.append("<td "+rowCss+">"+teacherUploadTemp.getDate_applied()+"</td>");
				dmRecords.append("<td "+rowCss+">"+jobId+"</td>");
				dmRecords.append("<td "+rowCss+">"+fName+"</td>");
				dmRecords.append("<td "+rowCss+">"+lName+"</td>");
				dmRecords.append("<td "+rowCss+">"+email+"</td>");
				dmRecords.append("<td "+rowCss+">"+teacherUploadTemp.getErrortext()+"</td>");
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	public TeacherDetail findByTeacherDetailObj(List<TeacherDetail> currentTeacherDetailList,List<TeacherDetail> teacherDetailList,int teacherId)
	{
		TeacherDetail TeacherDetailObj = null;
		boolean teacherFlag=false;
		try 
		{
			for(TeacherDetail teacherDetail : teacherDetailList){
				if(teacherDetail.getTeacherId()==teacherId){
					TeacherDetailObj=teacherDetail;
					teacherFlag=true;
				}
			}
			if(teacherFlag==false){
				for(TeacherDetail teacherDetail: currentTeacherDetailList){
					if(teacherDetail.getTeacherId()==teacherId)
						TeacherDetailObj=teacherDetail;
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(TeacherDetailObj==null)
			return null;
		else
			return TeacherDetailObj;	
	}
	public boolean findPortfolioNeededByApiJobId(Map<Integer,JobOrder> jobOrderList,String apiJobId)
	{	
		try{
		     for(int i=0;i<jobOrderList.size();i++){
		    	 String apiId=jobOrderList.get(i).getApiJobId();
		    	if(apiId!=null)
		    	 if(apiId.equals(apiJobId)){
		    		 if(jobOrderList.get(i).getCreatedForEntity()==2){
		    			 return jobOrderList.get(i).getDistrictMaster().getIsPortfolioNeeded();
					 }else{
						 return jobOrderList.get(i).getSchool().get(0).getIsPortfolioNeeded(); 
					 }
		    	 }
		     }
		     return true;
		}catch(Exception e){
			return true;
		}
	}
	/*============ Gagan : get Exclusive Period Functionality =============================*/
	public int getExclusivePeriodByApiJobId(Map<Integer,JobOrder> jobOrderList,String apiJobId, List<ExclusivePeriodMaster> lstExclusivePeriodMaster )
	{	
		int x=0;
		try{
		     for(int i=0;i<jobOrderList.size();i++){
		    	 String apiId=jobOrderList.get(i).getApiJobId();
		    	if(apiId!=null)
		    	 if(apiId.equals(apiJobId))
		    	 {
		    		 if(jobOrderList.get(i).getDistrictMaster().getExclusivePeriod()==null || jobOrderList.get(i).getDistrictMaster().getExclusivePeriod()==0)
		 			{
		 				System.out.println(" district Exclusive period is NuLL --- or 0 ------");
		 				ExclusivePeriodMaster exclusivePeriodMaster = lstExclusivePeriodMaster.get(0);
		 				//districtMaster.setExclusivePeriod(exclusivePeriodMaster.getExclPeriod());
		 				x= lstExclusivePeriodMaster.get(0).getExclPeriod();
		 			}
		    		 else
		    		 {
		    			 System.out.println("\n\n\n  Gagan : Get Exclusive Period is Not Null ");
		    			 x= jobOrderList.get(i).getDistrictMaster().getExclusivePeriod();
		    		 }
		    	 }
		     }
		}catch(Exception e){
			return 0;
		}
		return x;
	}
	
	/*============ Gagan : get Dynamic Email  MailToCandidateOnImport Functionality =============================*/
	public MailToCandidateOnImport getEmailFormatByDistrictId(DistrictMaster districtMaster, List<MailToCandidateOnImport> lstMailToCandidateOnImport,int headQuarterId)
	{	
		MailToCandidateOnImport mailToCandidateOnImport	=	null;
		try
		{
			for(MailToCandidateOnImport lst :lstMailToCandidateOnImport)
            {
                  if(districtMaster!=null && lst.getDistrictMaster()!=null  && districtMaster.getDistrictId().equals(lst.getDistrictMaster().getDistrictId()))
                  {
                        mailToCandidateOnImport=lst;
                  }
                  else if(headQuarterId!=0  && lst.getHeadQuarterId().equals(headQuarterId))
                  {
                        mailToCandidateOnImport=lst;
                  }
            
            }

		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return mailToCandidateOnImport;
	}

	public int saveTeacher()
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		System.out.println("::::::::saveTeacher now:::::::::");
		int returnVal=0;
        try{
        	Criterion criterion1 = Restrictions.eq("sessionid",session.getId());
        	Criterion criterion2 = Restrictions.eq("errortext","");
        	List <TeacherUploadTemp> teacherUploadTemplst=teacherUploadTempDAO.findByCriteria(criterion1,criterion2);
        	
        	//****************** Adding By Deepak    ******************************* 	
        	System.out.println("teacherUploadTemplst       ::::::::: "+teacherUploadTemplst.size());
        	Integer checkflag=teacherUploadTemplst.size();
        	 	if(checkflag==0)
        	 	{
        	 		returnVal=1;
        	 	}
        	 	else
        	 	{
        	 //******************   End    ******************************************
        	 List emails = new ArrayList();
        	 List jobs = new ArrayList();
        	 List<String> inviteTempjobList=new ArrayList<String>();
        	 List<JobOrder> listJobOrders = new ArrayList<JobOrder>(); 
        	 Map<String,JobOrder> mapAllJobs = new HashMap<String, JobOrder>();
        	 List<TeacherNormScore> teacherNormScore= new ArrayList<TeacherNormScore>();
        	 List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobs= new ArrayList<TeacherStatusHistoryForJob>();	
        	 Map<Integer,Boolean> historyMap = new HashMap<Integer, Boolean>();
        	 Map<Integer,Integer> normScoreMap = new HashMap<Integer, Integer>();
	         
        	 for(int e=0; e<teacherUploadTemplst.size(); e++) {
	            		emails.add(teacherUploadTemplst.get(e).getUser_email());
	            		jobs.add(teacherUploadTemplst.get(e).getJob_id());
	            		String jobId=teacherUploadTemplst.get(e).getJob_id();
	            		if(jobId!=null && jobId.length()>0)
	            		inviteTempjobList.add(jobId);
			 }
        	 if(inviteTempjobList!=null && inviteTempjobList.size()>0){
        		 listJobOrders = jobOrderDAO.findJobOrderListByApiJobIdListAndHeadOrDistrict(userMaster.getHeadQuarterMaster(), userMaster.getDistrictId(), inviteTempjobList);

        	 }
        	 if(listJobOrders!=null && listJobOrders.size()>0){
        		 for(JobOrder jobOrder :listJobOrders){
        			 mapAllJobs.put(jobOrder.getJobId()+"", jobOrder);
        		 }
        	 }
        	 List <TeacherDetail> teacherUploadTempList=teacherDetailDAO.findAllTeacherDetails(emails);
        	 try
 			{
 				teacherNormScore=teacherNormScoreDAO.findTeacersNormScoresList(teacherUploadTempList);
 				teacherStatusHistoryForJobs=teacherStatusHistoryForJobDAO.findStatusAndSecStatusByTeacherList(teacherUploadTempList);
 				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : teacherStatusHistoryForJobs) {
 					if(teacherStatusHistoryForJob.getStatusMaster()!=null && (teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("widrw")|| teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("hird")|| teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("rem")|| teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("dcln")))
 					historyMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(), true);
 					else
 						historyMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(), false);
 				}
 				for (TeacherNormScore teacherNormScore1 : teacherNormScore) {
 					if(teacherNormScore1.getTeacherNormScore()!=null)
 					normScoreMap.put(teacherNormScore1.getTeacherDetail().getTeacherId(), teacherNormScore1.getTeacherNormScore());
				}
 			}catch(Exception e){
 				e.printStackTrace();
 			}
        	 
        	 List teacherIdList = new ArrayList();
	         for(int t=0; t<teacherUploadTempList.size(); t++) {
	        	 teacherIdList.add(teacherUploadTempList.get(t));
			 }
	         List<TeacherPortfolioStatus> teacherPortfolioStatusList= new ArrayList<TeacherPortfolioStatus>();
	         
	         if(teacherIdList.size()>0)
	        	 teacherPortfolioStatusList=teacherPortfolioStatusDAO.findAllTeacherPortfolioStatus(teacherIdList);
	        
	         List<JobForTeacher> jobForTeacherList=new ArrayList<JobForTeacher>();
	         if(teacherIdList.size()>0)
	        	 jobForTeacherList=jobForTeacherDAO.findJobByFilterTeacher(teacherIdList);
	         
	         List<TeacherAssessmentStatus> teacherAssessmentStatusList=new ArrayList<TeacherAssessmentStatus>();
	         
	         if(teacherIdList.size()>0)
	        	 teacherAssessmentStatusList=teacherAssessmentStatusDAO.findByAllTeacherAssessmentStatus(teacherIdList);
	         
	         List<AssessmentJobRelation> lstAssessmentJobRelation =assessmentJobRelationDAO.findByAllAssessmentJobRelation();
	         
	         List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
        	 Map<Integer,JobOrder> jobOrderList = jobOrderDAO.findByAllJobsWithDistrictOrHead(jobs,userMaster.getDistrictId(),userMaster.getHeadQuarterMaster());//findByAllActiveAndInactiveJobs(jobs); 
        	 System.out.println("jobOrderList   :::::::::::>>>"+jobOrderList.values());
        	 
        	//********************* Adding By Deepak    ***************************
	         Integer headQuarterId=(Integer)session.getAttribute("Hid");
	         Integer branchId=(Integer)session.getAttribute("Bid");
	         Integer distid=(Integer)session.getAttribute("Did");
	        //System.out.println("distid   ::::::::=== "+distid);
	         List districtId=null;
        	 if(distid!=null)
        	 {
        		 districtId= new ArrayList();
	         for(int e=0; e<jobOrderList.size(); e++) {
	        	 districtId.add(jobOrderList.get(e).getDistrictMaster());
			 }
        	 }
	         
	         //**************************************************************************
	         List<MailToCandidateOnImport> lstMailToCandidateOnImport	=	mailToCandidateOnImportDAO.getDistrictByDistrictIdList(districtId,headQuarterId,branchId);
        	 System.out.println("==== lstMailToCandidateOnImport  ===="+lstMailToCandidateOnImport.size());
        	 
        	 List<TeacherDetail> currentTeacherDetaillst =new ArrayList<TeacherDetail>();
        	 List<TeacherDetail> newTeacherList = new  ArrayList<TeacherDetail>();
        	 List<TeacherMailSend> teacherMailSendList =new ArrayList<TeacherMailSend>();
        	 /* ========== Gagan : Exclusive Period Functionality ================*/
        	 List<ExclusivePeriodMaster> lstExclusivePeriodMaster = exclusivePeriodMasterDAO.findAll();
        	 
        	 /**** Session dunamic ****/
        	 SessionFactory sessionFactory=teacherDetailDAO.getSessionFactory();
			 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
        	 Transaction txOpen =statelesSsession.beginTransaction();
        	 
        	TeacherDetail teacherDetail;
        	boolean isPortfolioNeeded=true,oneTime=false;
        	int i=0;
        	List<TeacherDetail> existingTeacherDetailList = new ArrayList<TeacherDetail>();
        	JobOrder jobOrder = null;
        	String importType = "N";
        	for(TeacherUploadTemp teacherUploadTemp : teacherUploadTemplst){
        		String jobId = teacherUploadTemp.getJob_id();	
        			boolean newUser=false;
	        		if(oneTime==false){
	        			if(!findPortfolioNeededByApiJobId(jobOrderList,jobId))
	        			isPortfolioNeeded=false;
	        			oneTime=true;
	    			}
	        		int teacherId =0;
	        		if(emailTeacherExistForSave(currentTeacherDetaillst,teacherUploadTempList,teacherUploadTemp.getUser_email())!=0){
	        			teacherId=emailTeacherExistForSave(currentTeacherDetaillst,teacherUploadTempList,teacherUploadTemp.getUser_email());
	        		}
	        		System.out.println("teacherId:::::"+teacherId);
	        		String rPassword=Utility.randomString(8);//"158RJXH0";
	        		int exclusivePeriod =getExclusivePeriodByApiJobId(jobOrderList,jobId,lstExclusivePeriodMaster);
	        		BranchMaster branchMaster=null;
        			if(teacherUploadTemp.getExist_teacherId()==0 && teacherId==0){
        				if(mapAllJobs.get(jobId)!=null){
        					JobOrder job=mapAllJobs.get(jobId);
        					try{
	        					if(job!=null && job.getBranchMaster()!=null){
	        						if(job.getJobCategoryMaster()!=null && job.getJobCategoryMaster().getPreHireSmartPractices()!=null && job.getJobCategoryMaster().getPreHireSmartPractices()){
	        							branchMaster=job.getBranchMaster();
	        						}
	        					}
        					}catch(Exception e){
        						e.printStackTrace();
        					}
        					if(job.getIsInviteOnly()!=null && job.getIsInviteOnly()){
        						isPortfolioNeeded=true;
        						importType = "H";
        					}
        				}
        				String password=MD5Encryption.toMD5(rPassword);//MD5Encryption.toMD5("123");//
						System.out.println("Create New Teacher ::::::::::::::::::::::"+isPortfolioNeeded);
						int authorizationkey=(int) Math.round(Math.random() * 2000000);
						teacherDetail= new TeacherDetail();
						teacherDetail.setEmailAddress(teacherUploadTemp.getUser_email());
						teacherDetail.setFirstName(teacherUploadTemp.getUser_first_name());
						teacherDetail.setLastName(teacherUploadTemp.getUser_last_name());
						teacherDetail.setPassword(password);
						teacherDetail.setUserType("R");
						teacherDetail.setExclusivePeriod(exclusivePeriod);
						teacherDetail.setQuestCandidate(0);
						teacherDetail.setFbUser(false);
						teacherDetail.setAuthenticationCode(""+authorizationkey);
						teacherDetail.setVerificationCode(""+authorizationkey);
						teacherDetail.setVerificationStatus(1);				
						teacherDetail.setNoOfLogin(0);
						teacherDetail.setStatus("A");
						teacherDetail.setSendOpportunity(false);
						teacherDetail.setCreatedDateTime(new Date());
						teacherDetail.setIsPortfolioNeeded(isPortfolioNeeded);
						teacherDetail.setForgetCounter(0);
						teacherDetail.setIsResearchTeacher(false);
						teacherDetail.setInternalTransferCandidate(false);
						if(branchMaster!=null){
							teacherDetail.setSmartPracticesCandidate(1);
							teacherDetail.setAssociatedBranchId(branchMaster);
						}
						//teacherDetailDAO.makePersistent(teacherDetail);
						statelesSsession.insert(teacherDetail);                                   //Insert record here in TeacherDetail
						newTeacherList.add(teacherDetail);
						currentTeacherDetaillst.add(teacherDetail);
						newUser=true;
						teacherDetail.setPassword(rPassword);
						System.out.println("i:::::"+i);
						i++;
        			}else{
        				boolean checkInvite=false;
        				if(mapAllJobs.get(jobId)!=null){
        					JobOrder job=mapAllJobs.get(jobId);
        					if(job.getIsInviteOnly()!=null && job.getIsInviteOnly()){
        						isPortfolioNeeded=true;
        						checkInvite=true;
        						importType = "H";
        					}
        				}
        				teacherDetail= findByTeacherDetailObj(currentTeacherDetaillst,teacherUploadTempList,teacherId);
        				if(checkInvite){
        					if(teacherDetail.getIsPortfolioNeeded()==null || teacherDetail.getIsPortfolioNeeded()==false)
        					{
	        					teacherDetail.setIsPortfolioNeeded(isPortfolioNeeded);
	        					statelesSsession.update(teacherDetail);
        					}
        				}
        				//existingTeacherDetailList.add(teacherDetail);
        			}
        			existingTeacherDetailList.add(teacherDetail);
        			Date dateApplied=null;
        			try{
        				dateApplied=Utility.getCurrentDateFormart(teacherUploadTemp.getDate_applied().replaceAll("/","-"));
        			}catch(Exception e){}
        			
        			jobOrder=saveJobForTeacher(statelesSsession,jobOrderList,lstAssessmentJobRelation,teacherAssessmentStatusList,jobForTeacherList,teacherPortfolioStatusList,statusMasterList,jobId,dateApplied,teacherDetail,historyMap,normScoreMap);
					if(headQuarterId==null){
						headQuarterId=0;
					}
        			MailToCandidateOnImport mailToCandidateOnImport =	getEmailFormatByDistrictId(jobOrder.getDistrictMaster(),lstMailToCandidateOnImport,headQuarterId);
        			String schoolDistrictName="";
        			if(jobOrder.getHeadQuarterMaster()!=null){
        				schoolDistrictName=jobOrder.getHeadQuarterMaster().getHeadQuarterName();
        			}else{
        				schoolDistrictName=jobOrder.getDistrictMaster().getDistrictName();	
        			}
        			 //getSchoolDistrictName(jobOrder);
					
        			
        			String jobTitle=jobOrder.getJobTitle();
					String epiStatus=epiStatus(teacherAssessmentStatusList,teacherDetail);
					String to=teacherDetail.getEmailAddress();
					System.out.println("epiStatus:::::"+epiStatus);
					//Set temporary base URL
					teacherDetail.setIdentityVerificationPicture(Utility.getBaseURL(request));//"https://platform.teachermatch.org:443/"
					TeacherMailSend teacherMailSend= new TeacherMailSend();
					teacherMailSend.setEpiStatus(epiStatus);
					teacherMailSend.setUserMaster(userMaster);
					teacherMailSend.setMailTo(to);
					teacherMailSend.setTeacherDetail(teacherDetail);
					teacherMailSend.setNewUser(newUser);
					teacherMailSend.setSchoolDistrictName(schoolDistrictName);
					teacherMailSend.setJobTitle(jobTitle);
					teacherMailSendList.add(teacherMailSend);
					teacherMailSend.setMailToCandidateOnImport(mailToCandidateOnImport);
					
					////////////////////////////////
					if(jobOrder.getHeadQuarterMaster()==null  || (jobOrder.getIsInviteOnly()==null || (jobOrder.getIsInviteOnly()!=null && !jobOrder.getIsInviteOnly()))){
						MessageToTeacher  messageToTeacher= new MessageToTeacher();
						messageToTeacher.setTeacherId(teacherDetail);
						messageToTeacher.setJobId(jobOrder);
						messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
						messageToTeacher.setSenderId(userMaster);
						messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
						messageToTeacher.setEntityType(userMaster.getEntityType());
						messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
						System.out.println(" importType ::::>>>>>>> "+importType);
						messageToTeacher.setImportType(importType);


						String districtName=null;
						if(schoolDistrictName.equalsIgnoreCase("Charlotte-Mecklenburg Schools")){
							districtName="CMS";
						}else{
							districtName=schoolDistrictName;
						}
						boolean mailStatus=false;
						if(epiStatus.equalsIgnoreCase("")){
							messageToTeacher.setMessageSubject(Utility.getLocaleValuePropByKey("msgNextStepforYour", locale)+districtName+Utility.getLocaleValuePropByKey("msgTeacherApplication", locale));
							mailStatus=true;
						}else if(epiStatus.equals("icomp")){
							messageToTeacher.setMessageSubject(Utility.getLocaleValuePropByKey("msgNextStepforYour", locale)+districtName+Utility.getLocaleValuePropByKey("msgTeacherApplication", locale));
							mailStatus=true;
						}
						if(mailStatus && newUser){ //new user only
							if(teacherMailSend.getMailToCandidateOnImport()!=null)
							{
								messageToTeacher.setMessageSend(MailText.mailToTeacherOnImport("http://bit.ly/1B4Vww5",teacherDetail,teacherMailSend.getMailToCandidateOnImport()));
							}
							else
							{
								if(districtName.equalsIgnoreCase("CMS")){
									messageToTeacher.setMessageSend(MailText.mailToTeacherForEpiOrNoRecord("http://bit.ly/1az1fhe",userMaster,teacherDetail,jobTitle,newUser,0,schoolDistrictName));
								}else{
									messageToTeacher.setMessageSend(MailText.mailToTeacherForEpiOrNoRecord("http://bit.ly/1B4Vww5",userMaster,teacherDetail,jobTitle,newUser,0,schoolDistrictName));
								}
							}
							statelesSsession.insert(messageToTeacher);
						}
					}
					/////////////////////////////////////
        	}
	       
	       	try{
		       	if(jobOrder.getIsInviteOnly()!=null && jobOrder.getIsInviteOnly()) // Invitation Only
		       	{
		       		String distrcitSchoolName="";
			       	for (TeacherDetail teacherDetail2 : existingTeacherDetailList) {
			       		if(jobOrder.getHeadQuarterMaster()!=null){
			       			distrcitSchoolName=jobOrder.getHeadQuarterMaster().getHeadQuarterName();
	        			}else{
	        				distrcitSchoolName=jobOrder.getDistrictMaster().getDistrictName();	
	        			}
			       		DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("admin@netsutra.com");
						dsmt.setMailto(teacherDetail2.getEmailAddress());
						dsmt.setMailsubject(Utility.getLocaleValuePropByKey("msgInvitedApplyJob", locale)+" "+distrcitSchoolName);
						if(jobOrder.getHeadQuarterMaster()!=null){
							dsmt.setMailcontent(MailText.sendInviteForInviteOnlyCandidatesMailTextForKelly(null,jobOrder,teacherDetail2,newTeacherList));
						}else{
						  dsmt.setMailcontent(MailText.sendInviteForInviteOnlyCandidatesMailText(jobOrder,teacherDetail2));
						}					
						try {
							dsmt.start();	
						} catch (Exception e) {}
						
						/* Insert mail in log By Sekhar  */
						try{
							MessageToTeacher  messageToTeacher= new MessageToTeacher();
							messageToTeacher.setTeacherId(teacherDetail2);
							messageToTeacher.setJobId(jobOrder);
							messageToTeacher.setTeacherEmailAddress(teacherDetail2.getEmailAddress());
							messageToTeacher.setMessageSubject(Utility.getLocaleValuePropByKey("msgInvitedApplyJob", locale)+" "+distrcitSchoolName);
							if(jobOrder.getHeadQuarterMaster()!=null){
								messageToTeacher.setMessageSend(MailText.sendInviteForInviteOnlyCandidatesMailTextForKelly(null,jobOrder,teacherDetail2,newTeacherList));
							}else{
								messageToTeacher.setMessageSend(MailText.sendInviteForInviteOnlyCandidatesMailText(jobOrder,teacherDetail2));
							}		
							messageToTeacher.setSenderId(userMaster);
							messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
							messageToTeacher.setEntityType(userMaster.getEntityType());
							messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
							messageToTeacher.setImportType(importType);
							statelesSsession.insert(messageToTeacher);
						}catch(Exception e){
							e.printStackTrace();
						}
						/* End mail in log by Sekhar */
					}
		       	}
	       	}catch(Exception e){
	       		e.printStackTrace();
	       	}
	    	txOpen.commit();
	       	statelesSsession.close();
	       	try{
	       		if(jobOrder.getHeadQuarterMaster()==null  || (jobOrder.getIsInviteOnly()==null || (jobOrder.getIsInviteOnly()!=null && !jobOrder.getIsInviteOnly())))
		       	   mailSendByThread(teacherMailSendList);
	        	deleteXlsAndXlsxFile(request,session.getId());
	       		teacherUploadTempDAO.deleteAllTeacherTemp(session.getId());
	       	}catch(Exception e){
	       		e.printStackTrace();
	       	}
         }                                                       //new Add for check flag
		}catch (Exception e){
			System.out.println("Error >"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			return returnVal;
		}
     
	}
	public String getSchoolDistrictName(JobOrder jobOrder){
		String schoolDistrictName="";
		if(jobOrder.getCreatedForEntity()==2)
		{
			schoolDistrictName			=	jobOrder.getDistrictMaster().getDistrictName()==null?"":jobOrder.getDistrictMaster().getDistrictName();
		}
		if(jobOrder.getCreatedForEntity()==3)
		{
			List<SchoolMaster> schoolList=jobOrder.getSchool();
			for(SchoolMaster schoolDetails:schoolList)
			{
				schoolDistrictName		=	schoolDetails.getSchoolName()==null?"":schoolDetails.getSchoolName();
			}
		}
		return schoolDistrictName;
	}
	public String epiStatus(List<TeacherAssessmentStatus> teacherAssessmentStatusList,TeacherDetail teacherDetail)
	{
		String status="";
		try {
				if(teacherAssessmentStatusList!=null)
				for(TeacherAssessmentStatus teacherAssessmentStatus: teacherAssessmentStatusList){
					if(teacherAssessmentStatus.getTeacherDetail().equals(teacherDetail)){
						status=teacherAssessmentStatus.getStatusMaster().getStatusShortName();
					}
				}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;

	}
	public void mailSendByThread(List<TeacherMailSend> teacherMailSendList){
		MailSend mst =new MailSend(teacherMailSendList);
		Thread currentThread = new Thread(mst);
		currentThread.start(); 
	}
	public class MailSend  extends Thread{
		
		List<TeacherMailSend> teacherMailSendListTrdVal=new ArrayList<TeacherMailSend>();
		public MailSend(List<TeacherMailSend> teacherMailSendList){
			teacherMailSendListTrdVal=teacherMailSendList;
		 }
		 public void run() {
				try {
					Thread.sleep(1000);
					System.out.println("::Trd size::"+teacherMailSendListTrdVal.size());
					int mailCounter=0;
					for(TeacherMailSend teacherMailSend :teacherMailSendListTrdVal){
						//Thread.sleep(1000);
						int mailTypeTrdVal=0;
						UserMaster userSessionTrdVal=null;
						String toTrdVal="";
						String subjectTrdVal="";
						TeacherDetail teacherDetailTrdVal=null;
						String jobTitleTrdVal="";
						boolean newUserTrdVal=false;
						String schoolDistrictNameTrdVal="";
						String epiStatus="";
						if(teacherMailSend.getEpiStatus()!=null)
							epiStatus=teacherMailSend.getEpiStatus();
						
						if(teacherMailSend.getUserMaster()!=null)
							userSessionTrdVal=teacherMailSend.getUserMaster();
						
						if(teacherMailSend.getMailTo()!=null)
							toTrdVal=teacherMailSend.getMailTo();
						
						if(teacherMailSend.getTeacherDetail()!=null)
							teacherDetailTrdVal=teacherMailSend.getTeacherDetail();
						
						if(teacherMailSend.getJobTitle()!=null)
							jobTitleTrdVal=teacherMailSend.getJobTitle();
						
						newUserTrdVal=teacherMailSend.isNewUser();
						if(teacherMailSend.getSchoolDistrictName()!=null)
							schoolDistrictNameTrdVal=teacherMailSend.getSchoolDistrictName();
						
						String districtName=null;
						
						if(schoolDistrictNameTrdVal.equalsIgnoreCase("Charlotte-Mecklenburg Schools")){
							districtName="CMS";
						}else{
							districtName=schoolDistrictNameTrdVal;
						}
						boolean mailStatus=false;
						if(epiStatus.equalsIgnoreCase("")){
							mailTypeTrdVal=0;
							//String actionMsg = epiStatus.equals("")?"Start":"Resume";
							subjectTrdVal=Utility.getLocaleValuePropByKey("msgNextStepforYour", locale)+districtName+Utility.getLocaleValuePropByKey("msgTeacherApplication", locale);
							 mailStatus=true;
						}else if(epiStatus.equals("icomp")){
							//String actionMsg = epiStatus.equals("")?"Start":"Resume";
							mailTypeTrdVal=0;
							subjectTrdVal=Utility.getLocaleValuePropByKey("msgNextStepforYour", locale)+districtName+Utility.getLocaleValuePropByKey("msgTeacherApplication", locale);
							mailStatus=true;
						}/*else if(epiStatus.equals("comp")){
							mailTypeTrdVal=1;
							subjectTrdVal="Confirmation of CMS Job Application";
							mailStatus=true;
						}*/
						if(mailStatus && newUserTrdVal){ //new user only
							if(teacherMailSend.getMailToCandidateOnImport()!=null)
							{
								System.out.println(" getMailToCandidateOnImport Template Available");
								//emailerService.sendMailAsHTMLTextToClient(teacherMailSend.getMailToCandidateOnImport().getFromAddress(),toTrdVal,teacherMailSend.getMailToCandidateOnImport().getSubjectLine(),MailText.mailToTeacherOnImport("http://bit.ly/1B4Vww5",teacherDetailTrdVal,teacherMailSend.getMailToCandidateOnImport()),"nocms");
								emailerService.sendMailAsHTMLTextToClient(teacherMailSend.getMailToCandidateOnImport().getFromAddress(),toTrdVal,teacherMailSend.getMailToCandidateOnImport().getSubjectLine(),MailText.mailToTeacherOnImport("http://bit.ly/1B4Vww5",teacherDetailTrdVal,teacherMailSend.getMailToCandidateOnImport()),"nocms");
								System.out.println(" Dynamic FromAddress "+teacherMailSend.getMailToCandidateOnImport().getFromAddress()+" To Address "+toTrdVal+" body "+teacherMailSend.getMailToCandidateOnImport());
							}
							else
							{
								if(districtName.equalsIgnoreCase("CMS")){
									System.out.println(" ELSe if  CMS  ");
									emailerService.sendMailAsHTMLText(toTrdVal,subjectTrdVal,MailText.mailToTeacherForEpiOrNoRecord("http://bit.ly/1az1fhe",userSessionTrdVal,teacherDetailTrdVal,jobTitleTrdVal,newUserTrdVal,mailTypeTrdVal,schoolDistrictNameTrdVal),"cms");
								}else{
									System.out.println(" ELSe New User Mail Body Template is availablr for district in database. ");
									emailerService.sendMailAsHTMLTextToClient(districtName+" Job Application",toTrdVal,subjectTrdVal,MailText.mailToTeacherForEpiOrNoRecord("http://bit.ly/1B4Vww5",userSessionTrdVal,teacherDetailTrdVal,jobTitleTrdVal,newUserTrdVal,mailTypeTrdVal,schoolDistrictNameTrdVal),"nocms");
								}
								mailCounter++;
								System.out.println("Mail sent No : "+mailCounter+" : "+toTrdVal);
							}
						}
					}
				}catch (NullPointerException en) {
					en.printStackTrace();
				}catch (InterruptedException e) {
					e.printStackTrace();
				}
		  }
	}
	public StatusMaster findStatusByShortName(List <StatusMaster> statusMasterList,String statusShortName)
	{
		StatusMaster status= null;
		try 
		{   if(statusMasterList!=null)
			for(StatusMaster statusMaster: statusMasterList){
				if(statusMaster.getStatusShortName().equalsIgnoreCase(statusShortName)){
					status=statusMaster;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(status==null)
			return null;
		else
			return status;
	}
	public TeacherPortfolioStatus findPortfolioStatusByTeacher(List<TeacherPortfolioStatus> teacherPortfolioStatusList,TeacherDetail teacherDetail)
	{
		TeacherPortfolioStatus teacherPortStatus= null;
		try 
		{
			if(teacherPortfolioStatusList!=null)
			for(TeacherPortfolioStatus teacherPortfolioStatus: teacherPortfolioStatusList){
				if(teacherPortfolioStatus.getTeacherId().equals(teacherDetail)){
					teacherPortStatus=teacherPortfolioStatus;
				}
			}	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(teacherPortStatus==null)
			return null;
		else
			return teacherPortStatus;
	}
	public JobForTeacher findJobByTeacherAndJob(List<JobForTeacher> jobForTeacherList,TeacherDetail teacherDetail, JobOrder jobOrder)
	{
		JobForTeacher jobFTeacher= null;
		try 
		{
			if(jobForTeacherList!=null)
			for(JobForTeacher jobForTeacher: jobForTeacherList){
				if(jobForTeacher.getTeacherId().equals(teacherDetail) && jobForTeacher.getJobId().equals(jobOrder)){
					jobFTeacher=jobForTeacher;
				}
			}	
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(jobFTeacher==null)
			return null;
		else
			return jobFTeacher;	
	}
	public TeacherAssessmentStatus findAssessmentStatusByTeacherForBase(List<TeacherAssessmentStatus> teacherAssessmentStatusList,TeacherDetail teacherDetail)
	{
		TeacherAssessmentStatus teacherAssessmntStatus = null;
		try 
		{	if(teacherAssessmentStatusList!=null)
			for(TeacherAssessmentStatus teacherAssessmentStatus: teacherAssessmentStatusList){
				if(teacherAssessmentStatus.getTeacherDetail().equals(teacherDetail)){
					teacherAssessmntStatus=teacherAssessmentStatus;
				}
			}	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(teacherAssessmntStatus==null)
			return null;
		else
			return teacherAssessmntStatus;
	}
	public List<TeacherAssessmentStatus> findTASByTeacherAndAssessment(List<TeacherAssessmentStatus> teacherAssessmentStatusLst,TeacherDetail teacherDetail, AssessmentDetail assessmentDetail)
	{
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = new ArrayList<TeacherAssessmentStatus>();
		try 
		{
			if(teacherAssessmentStatusLst!=null)
			for(TeacherAssessmentStatus teacherAssessmentStatus: teacherAssessmentStatusLst){
				if(teacherAssessmentStatus.getTeacherDetail().equals(teacherDetail) && teacherAssessmentStatus.getAssessmentDetail().equals(assessmentDetail)){
					lstTeacherAssessmentStatus.add(teacherAssessmentStatus);
				}
			}	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherAssessmentStatus;
	}
	public List<TeacherAssessmentStatus> findAssessmentTaken(List<TeacherAssessmentStatus> teacherAssessmentStatusLst,TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			if(teacherAssessmentStatusLst!=null)
			for(TeacherAssessmentStatus teacherAssessmentStatus: teacherAssessmentStatusLst){
				JobOrder jobOrderObj=teacherAssessmentStatus.getJobOrder();
				if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0){
					
					if(teacherDetail==null){
						if(jobOrderObj==null){
							teacherAssessmentStatusList.add(teacherAssessmentStatus);
						}
					}
					else{
						if(teacherAssessmentStatus.getTeacherDetail().equals(teacherDetail) && jobOrderObj==null){
							teacherAssessmentStatusList.add(teacherAssessmentStatus);
						}
					}
				}else{
					if(teacherDetail==null){
						if(jobOrderObj.equals(jobOrder)){
							teacherAssessmentStatusList.add(teacherAssessmentStatus);
						}
					}
					else{
						if(teacherAssessmentStatus.getTeacherDetail().equals(teacherDetail) && jobOrderObj.equals(jobOrder)){
							teacherAssessmentStatusList.add(teacherAssessmentStatus);
						}
					}
					
				}
			}	
		}catch (Exception e){
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	public List<AssessmentJobRelation> findRelationByJobOrder(Map<Integer,JobOrder> jobOrderList,List<AssessmentJobRelation> lstAssessmentJobRelation,JobOrder jobOrder) 
	{
		List<AssessmentJobRelation> assessmentJobRelations = new ArrayList<AssessmentJobRelation>();
		try {
			if(lstAssessmentJobRelation!=null)
			for(AssessmentJobRelation assessmentJobRelation: lstAssessmentJobRelation){
				if(assessmentJobRelation.getJobId().equals(jobOrder) && assessmentJobRelation.getStatus().equalsIgnoreCase("A")){
					for(int i=0;i<jobOrderList.size();i++){
						if(jobOrderList.get(i).equals(jobOrder) && jobOrderList.get(i).getIsJobAssessment() && jobOrderList.get(i).getStatus().equalsIgnoreCase("A") && jobOrderList.get(i).getJobStatus().equalsIgnoreCase("o")){
							if(Utility.isDateValidGtLt(getJobFormatDate(jobOrderList.get(i).getJobStartDate()),getJobFormatDate(new Date()), 0, 1)!=1 && Utility.isDateValidGtLt(getJobFormatDate(jobOrderList.get(i).getJobEndDate()),getJobFormatDate(new Date()), 1, 1)!=1){
								assessmentJobRelations.add(assessmentJobRelation);
							}
						}
						
					}
					
				}
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return assessmentJobRelations;
	}
	public static String getJobFormatDate(Date dat)
	{
		try 
		{
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			return sdf.format(dat);
		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
			return "";
		}
	}
	public JobOrder findJobByApiJobId(Map<Integer,JobOrder> jobOrderList,String apiJobId,Integer districtid1,Integer headQuarterId1,Integer branchId1)
	{
		JobOrder jobOrderObj = new JobOrder();
		try 
		{	if(jobOrderList!=null)
			for(int i=0;i<jobOrderList.size();i++){
				if(jobOrderList.get(i).getApiJobId()!=null)
				if(jobOrderList.get(i).getApiJobId().equals(apiJobId)){    //New By Deepak   if(jobOrderList.get(i).getApiJobId().equals(apiJobId) && jobOrderList.get(i).getDistrictMaster().getDistrictId()==districtid1)
					jobOrderObj=jobOrderList.get(i);
				}
			}	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(jobOrderObj==null)
			return null;
		else
			return jobOrderObj;
	}
	public JobOrder saveJobForTeacher(StatelessSession statelesSsession,Map<Integer,JobOrder> jobOrderList,List<AssessmentJobRelation> assessmentJobRelationLst,List<TeacherAssessmentStatus> teacherAssessmentStatusLst,List<JobForTeacher> jobForTeacherList,List<TeacherPortfolioStatus> teacherPortfolioStatusList,List <StatusMaster> statusMasterList,String jobId,Date createddDate,TeacherDetail teacherDetail,Map<Integer,Boolean> historyMap,Map<Integer,Integer> normScoreMap)
	{
		System.out.println("::::::::::::saveJobForTeacher::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
			String coverLetter ="",redirectedURL="";
			Integer districtid1=(Integer) session.getAttribute("Did");
			//******** Adding By Deepak   ****************************
			Integer  headQuarterId1=(Integer) session.getAttribute("Hid");
			Integer branchId1=(Integer) session.getAttribute("Bid");
			//********* End   ********************************
			
			//System.out.println("districtid1:::::::::::  "+districtid1+"       headQuarterId1::::::"+headQuarterId1+"  branchId1:::"+branchId1+"    jobId::::"+jobId);
			JobOrder jobOrder=null;			
		    jobOrder = findJobByApiJobId(jobOrderList,jobId,districtid1,headQuarterId1,branchId1);
			System.out.println(jobOrder.getJobId());
			//System.out.println("jobForTeacherList.size()::::"+jobForTeacherList.size()+"   ,teacherDetail    ::::"+teacherDetail.getTeacherId()+" ,jobOrder :::"+jobOrder.getApiJobId());
			JobForTeacher jobForTeacher =findJobByTeacherAndJob(jobForTeacherList,teacherDetail, jobOrder);
			if(jobForTeacher==null)
			{
				TeacherPortfolioStatus teacherPortfolioStatus = findPortfolioStatusByTeacher(teacherPortfolioStatusList,teacherDetail);
				jobForTeacher = new JobForTeacher();
				jobForTeacher.setTeacherId(teacherDetail);
				jobForTeacher.setJobId(jobOrder);
				if(jobOrder.getDistrictMaster()!=null)
				jobForTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
				jobForTeacher.setCoverLetter(coverLetter);
				jobForTeacher.setCreatedDateTime(new Date());
				
				if(createddDate!=null){
					jobForTeacher.setCreatedDateTime(createddDate);
				}
				StatusMaster statusMaster = null;
				statusMaster = findStatusByShortName(statusMasterList,"icomp");
				if(jobOrder.getHeadQuarterMaster()==null && jobOrder.getIsInviteOnly()!=null && jobOrder.getIsInviteOnly()){
					//statusMaster = findStatusByShortName(statusMasterList,"icomp");
				}else{
					AssessmentDetail assessmentDetail = null;
					TeacherAssessmentStatus teacherAssessmentStatus = findAssessmentStatusByTeacherForBase(teacherAssessmentStatusLst,jobForTeacher.getTeacherId());
					System.out.println(" job Category :: "+jobOrder.getJobCategoryMaster());
					if(!jobOrder.getJobCategoryMaster().getBaseStatus()){
						if(jobOrder.getIsJobAssessment()){
							List<AssessmentJobRelation> lstAssessmentJobRelation = findRelationByJobOrder(jobOrderList,assessmentJobRelationLst,jobOrder);
							if(lstAssessmentJobRelation.size()>0)
							{
								AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
								assessmentDetail = assessmentJobRelation.getAssessmentId();
								List<TeacherAssessmentStatus> lstTeacherAssessmentStatus =findTASByTeacherAndAssessment(teacherAssessmentStatusLst,teacherDetail, assessmentDetail);
								
								if(lstTeacherAssessmentStatus.size()>0)
								{						
									teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
									/*teacherAssessmentStatus.setTeacherAssessmentStatusId(null);
									teacherAssessmentStatus.setJobOrder(jobOrder);
									teacherAssessmentStatus.setCreatedDateTime(new Date());
									statelesSsession.insert(teacherAssessmentStatus);*/
									statusMaster = teacherAssessmentStatus.getStatusMaster();
								}
								else
								{
									statusMaster = findStatusByShortName(statusMasterList,"icomp");
								}					
							}
							else
							{
								statusMaster = findStatusByShortName(statusMasterList,"icomp");
							}
						}
						else{
							statusMaster = findStatusByShortName(statusMasterList,"comp"); 
						}
					}
					else if(jobOrder.getIsJobAssessment()){
						List<AssessmentJobRelation> lstAssessmentJobRelation = findRelationByJobOrder(jobOrderList,assessmentJobRelationLst,jobOrder);;
						if(lstAssessmentJobRelation.size()>0)
						{
							AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
							assessmentDetail = assessmentJobRelation.getAssessmentId();
							List<TeacherAssessmentStatus> lstTeacherAssessmentStatus =findTASByTeacherAndAssessment(teacherAssessmentStatusLst,teacherDetail, assessmentDetail);
							
							if(lstTeacherAssessmentStatus.size()>0)
							{						
								teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
								/*teacherAssessmentStatus.setTeacherAssessmentStatusId(null);
								teacherAssessmentStatus.setJobOrder(jobOrder);
								teacherAssessmentStatus.setCreatedDateTime(new Date());
								statelesSsession.insert(teacherAssessmentStatus);*/
								statusMaster = teacherAssessmentStatus.getStatusMaster();
							}
							else
							{
								statusMaster = findStatusByShortName(statusMasterList,"icomp");
							}					
						}
						else
						{
							statusMaster = findStatusByShortName(statusMasterList,"icomp");
						}
					}else{
						List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
						JobOrder jOrder = new JobOrder();
						jOrder.setJobId(0);
						teacherAssessmentStatusList =findAssessmentTaken(teacherAssessmentStatusLst,jobForTeacher.getTeacherId(),jOrder);
						if(teacherAssessmentStatusList.size()>0){
							teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
							statusMaster = teacherAssessmentStatus.getStatusMaster();
						}else{
							statusMaster = findStatusByShortName(statusMasterList,"icomp");
						}
					}
				}
				
				jobForTeacher.setIsAffilated(0);
				jobForTeacher.setRedirectedFromURL(redirectedURL);
				jobForTeacher.setStatus(statusMaster);
				jobForTeacher.setStatusMaster(statusMaster);
				if(statusMaster!=null){
					jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
				}
				try {
					commonService.autoReject(jobOrder, teacherDetail, jobForTeacher,historyMap,normScoreMap);
				} catch (Exception e) {
				}
				statelesSsession.insert(jobForTeacher);
			}
		return jobOrder;
	}
	public boolean emailExist(Map<String,String> jobIdEmailMap,List<UserMaster> userMasterlst,String emailAddress,String sessionId,String job_id){
		boolean userFlag=false,teacherFlag=false;
		for(UserMaster userMaster :userMasterlst){
			if(userMaster.getEmailAddress().equalsIgnoreCase(emailAddress)){
				userFlag=true;
			}
		}
		for(Map.Entry<String, String> entry : jobIdEmailMap.entrySet()){
			String mJobIds[]=null;
			String mJobId=null;
			try{
				mJobIds=entry.getKey().split("\\|");
				mJobId=mJobIds[1];
			}catch(Exception e){}
			
			String mEmail=entry.getValue();
			if(!job_id.equalsIgnoreCase("")){
				if(mJobId!=null && mEmail!=null)
				if(mJobId.equalsIgnoreCase(job_id) && mEmail.equalsIgnoreCase(emailAddress)){
					teacherFlag=true;
				}
			}else{
				if(mEmail.equalsIgnoreCase(emailAddress)){
					teacherFlag=true;	
				}
			}
		}
		if(teacherFlag || userFlag)
		{
			return true;
		}else{
			return false;
		}
	}
	public int emailTeacherExistForSave(List<TeacherDetail> currentTeacherDetaillst,List<TeacherDetail> lstTeacherDetails,String emailAddress){
		int teacherId=0;
		for(TeacherDetail teacherDetail: lstTeacherDetails){
			if(teacherDetail.getEmailAddress().equalsIgnoreCase(emailAddress)){
				teacherId=teacherDetail.getTeacherId();
				return teacherId;
			}
			
		}
		if(teacherId==0)
		for(TeacherDetail teacherDetail: currentTeacherDetaillst){
			if(teacherDetail.getEmailAddress().equalsIgnoreCase(emailAddress)){
				teacherId=teacherDetail.getTeacherId();
				return teacherId;
			}
			
		}
		return teacherId;
	}
	public int emailTeacherExist(List<TeacherDetail> lstTeacherDetails,String emailAddress){
		int teacherId=0;
		for(TeacherDetail teacherDetail: lstTeacherDetails){
			if(teacherDetail.getEmailAddress().equalsIgnoreCase(emailAddress)){
				teacherId=teacherDetail.getTeacherId();
				return teacherId;
			}
			
		}
		return teacherId;
	}
	public JobOrder apiJobIdExist(Map<Integer,JobOrder> jobOrderList,String apiJobId,Integer districtId){
		for(int i=0;i<jobOrderList.size();i++){
			if(jobOrderList.get(i).getApiJobId()!=null){
				if(jobOrderList.get(i).getApiJobId().equalsIgnoreCase(apiJobId)&&jobOrderList.get(i).getDistrictMaster().getDistrictId()==districtId){
					return jobOrderList.get(i);
				}
			}
		}
		return null;
	}
	public void deleteXlsAndXlsxFile(HttpServletRequest request,String sessionId){
		List<TeacherUploadTemp> teacherUploadTemplst= teacherUploadTempDAO.findByAllTempTeacher();
		String root = request.getRealPath("/")+"/uploads/";
		for(TeacherUploadTemp teacherUploadTemp : teacherUploadTemplst){
			String filePath="";
			File file=null;
			try{
				filePath=root+teacherUploadTemp.getSessionid()+".xlsx";
				file = new File(filePath);
				file.delete();
			}catch(Exception e){}
			try{
				filePath=root+teacherUploadTemp.getSessionid()+".xls";
				file = new File(filePath);
				file.delete();
			}catch(Exception e){}
		}
		
		String filePath="";
		File file=null;
		try{
			filePath=root+sessionId+".xlsx";
			file = new File(filePath);
			file.delete();
		}catch(Exception e){}
		try{
			filePath=root+sessionId+".xls";
			file = new File(filePath);
			file.delete();
		}catch(Exception e){}
	}
	public int deleteTempTeacher(String sessionId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		System.out.println(":::::::::::::::::deleteTempTeacher:::::::::::::::::"+sessionId);
		try{
			teacherUploadTempDAO.deleteTeacherTemp(sessionId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
		
	public String saveTeacherTemp(String fileName,String sessionId,Integer invitejobOnlyId ,Integer districtId,Integer headQuarterId,Integer branchId)
	{
		/* ========  For Session time Out Error =========*/
		boolean inviteflag=false,applyJobFlag=false;
		if(invitejobOnlyId!=null && invitejobOnlyId!=0)
			inviteflag=true;
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		System.out.println("::::::::saveTeacherTemp:::::::::");
		session.setAttribute("Did",districtId);
		session.setAttribute("Hid",headQuarterId);
		session.setAttribute("Bid",branchId);
		System.out.println("headQuarterId    :::"+headQuarterId+", branchId    ::::"+branchId+",  districtId ::::::::::::::"+districtId);
		String returnVal="";
        try{
        	 String root = request.getRealPath("/")+"/uploads/";
        	 String filePath=root+fileName;
        	 System.out.println("filePath :"+filePath);
        	 if(fileName.contains(".xlsx")){
        		 vectorDataExcelXLSX = readDataExcelXLSX(filePath);
        	 }else{
        		 vectorDataExcelXLSX = readDataExcelXLS(filePath);
        	 }
        	 System.out.println("::::::::get value of XLSX and XLS:::::::::");
			 int date_applied_count=11;
			 int job_id_count=11;
			 int user_first_name_count=11;
			 int user_last_name_count=11;
			 int user_email_count=11;
			 DistrictMaster districtMaster = null;
			 			 
			 UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");
			 HeadQuarterMaster headQuarterMaster=userMaster.getHeadQuarterMaster();
			 if(headQuarterMaster==null){
				 districtMaster = new DistrictMaster();
				 districtMaster.setDistrictId(districtId);
			 }else{
				 districtMaster=null;	 
			 }
			 Map<String,JobOrder> jobOrderList = new HashMap<String, JobOrder>();		 
			 List<String> jobApiJobIdList = new ArrayList<String>();
			 List<UserMaster> userMasterlst = userMasterDAO.findByAllUserMaster();
			 List emails = new ArrayList();
			 int user_email_count1=11;
			 int job_id_count1=11;
			 Map<String,String> emailExistCheck = new HashMap<String, String>();
			 SessionFactory sessionFactory=teacherUploadTempDAO.getSessionFactory();
			 StatelessSession sessionHiber = sessionFactory.openStatelessSession();
			
        	 Transaction txOpen =sessionHiber.beginTransaction();
			 for(int em=0; em<vectorDataExcelXLSX.size(); em++) {
	            Vector vectorCellEachRowDataGetEmail = (Vector) vectorDataExcelXLSX.get(em);
	            String email="",jobid="";
	            for(int e=0; e<vectorCellEachRowDataGetEmail.size(); e++) {
	            	if(vectorCellEachRowDataGetEmail.get(e).toString().equalsIgnoreCase("user_email")){
	            		user_email_count1=e;
	            	}
	            	if(vectorCellEachRowDataGetEmail.get(e).toString().equalsIgnoreCase("job_id")){
	            		job_id_count1=e;
	            	}
	            	if(job_id_count1==e)
	            		jobid=vectorCellEachRowDataGetEmail.get(e).toString().trim();
	            	
	            	if(user_email_count1==e)
	            		email=vectorCellEachRowDataGetEmail.get(e).toString().trim();
	            }
	            if(em!=0){
	            	emails.add(email);
	            	if(inviteflag){
	            		jobApiJobIdList.add(invitejobOnlyId+"");
	            	}else{
	            		jobApiJobIdList.add(jobid);
	            	}
	            	
	            }
			 }
			 //********** Adding by deepak ***************************************
			 
			 String tempdata[]=new String[vectorDataExcelXLSX.size()];
			 int jobid_count=1;
			 for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
				 
	             Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
	            	 if(vectorCellEachRowData.get(0).toString().equalsIgnoreCase("job_id")){
		            		jobid_count=0;
		            	}
		            	if(jobid_count==0)
		            	{
	            		tempdata[i]=vectorCellEachRowData.get(0).toString().trim();
			           }
		            	if(inviteflag)
		            		tempdata[i]=invitejobOnlyId+"";
			   } 
			 
			 List <TeacherDetail> teacherUploadTempList=teacherDetailDAO.findAllTeacherDetails(emails);
			// System.out.println("teacherUploadTempList ::::::::::::"+teacherUploadTempList.size());
			 //if(teacherUploadTempList.size()>0)
			// {
			 Map<String,Boolean> map =emailTeacherExistInTeacherDetail(teacherUploadTempList,tempdata,userMaster);
/*			 for(Map.Entry<String,Boolean> entry:map.entrySet())
				 System.out.println("entry===key==="+entry.getKey()+"   value======="+entry.getValue());*/
			 //}	 
			//********************************  End   *******************************************
			 
			 System.out.println("jobApiJobIdList:::::::::"+jobApiJobIdList.size());
			 
			// System.out.println("districtMaster ==================:"+districtMaster);
			 List<JobOrder> jobOrders = jobOrderDAO.findJobOrderListByApiJobIdListAndHeadOrDistrict(headQuarterMaster, districtMaster, jobApiJobIdList);
			 if(jobOrders!=null && jobOrders.size()>0)
			 {
				 for(JobOrder jo : jobOrders)
				 {
					 jobOrderList.put(jo.getApiJobId(), jo);
				 }
			 }
			List <TeacherUploadTemp> teachertemplst=teacheruploadtempdao.findByAllTempTeacherEmail(emails);
			 System.out.println("::::::::Start Inserting:::::::::");
			 TeacherUploadTemp teacherUploadTemp= new TeacherUploadTemp();
			 for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
	             Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
	             String date_applied="";
				 String job_id="";
				 String user_first_name="";
				 String user_last_name="";
				 String user_email="";
				 
	            for(int j=0; j<vectorCellEachRowData.size(); j++) {
	            	
	            	try{
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("date_applied")){
		            		date_applied_count=j;
		            	}
		            	if(date_applied_count==j)
		            		date_applied=vectorCellEachRowData.get(j).toString().trim();
		            //************************ inviteflag ********************	
		            	
		            	if(inviteflag){
			            		job_id_count=j;			            				            	
			            		job_id=invitejobOnlyId+"";
		            	}else{
		            		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("job_id")){
			            		job_id_count=j;
			            	}
			            	if(job_id_count==j)
			            		job_id=vectorCellEachRowData.get(j).toString().trim();
		            	}
	                //************************************************
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("user_first_name")){
		            		user_first_name_count=j;
		            	}
		            	if(user_first_name_count==j)
		            		 user_first_name=vectorCellEachRowData.get(j).toString().trim();
		            		
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("user_last_name")){
		            		user_last_name_count=j;
		            	}
		            	if(user_last_name_count==j)
		            		user_last_name=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("user_email")){
		            		user_email_count=j;
		            	}
		            	if(user_email_count==j)
		            		user_email=vectorCellEachRowData.get(j).toString().trim();
	            	}catch(Exception e){}
	            	
	            }
	          //*********************** Adding By Deepak **************************
	            System.out.println(user_email+"##"+job_id); 
	           
	            // boolean isAlreadyApplyThisJob=map.get(user_email+"##"+job_id)==null?false:(map.get(user_email+"##"+job_id)?true:false);
	            boolean isAlreadyApplyThisJob=false;
	            if(map.get(user_email.toLowerCase().trim()+"##"+job_id)!=null && map.get(user_email.toLowerCase().trim()+"##"+job_id))
	            	isAlreadyApplyThisJob=true;
	            System.out.println("isAlreadyApplyThisJob================"+isAlreadyApplyThisJob);
	            //****************** End *****************************************  	
	            
	            if(i!=0){
	            	boolean jobFlag=false,multiErrorFlag=false;
		             if(returnVal.equals("1") && (!job_id.equalsIgnoreCase("") || !user_first_name.equalsIgnoreCase("") || !user_last_name.equalsIgnoreCase("") || !user_email.equalsIgnoreCase(""))){
	            	 	teacherUploadTemp= new TeacherUploadTemp();
	            	 	try{
	            	 		teacherUploadTemp.setDate_applied(monthReplace(date_applied));
	            	 	}catch(Exception e){}
	            	 	String errorText=""; boolean errorFlag=false;
	            	 	teacherUploadTemp.setExist_teacherId(0);
	            	 	
	            	 	if(!date_applied.equalsIgnoreCase("")){
	            	 		if(Utility.isDateValid(teacherUploadTemp.getDate_applied(),1)==0){
	            	 			errorText+=Utility.getLocaleValuePropByKey("msgDateAppliedCurrentdate", locale);
		            	 		errorFlag=true;
	            	 		}else if(Utility.isDateValid(teacherUploadTemp.getDate_applied(),1)==2){
	            	 			errorText+=Utility.getLocaleValuePropByKey("msgDateAppliedValid", locale);
		            	 		errorFlag=true;
	            	 		}
	            	 	}
	            	 	if(job_id.equalsIgnoreCase("")||job_id.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		jobFlag=true;
	            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobIdNotAvailable", locale);
	            	 		errorFlag=true;
	            	 	}else{

	            	 		//JobOrder jobOrder=apiJobIdExist(jobOrderList,job_id,districtId);
	            	 		 String apiJobId=String.valueOf(job_id);
	            	 		 //List<JobOrder> jobOrder = jobOrderDAO.getJOByDID_APIJID(districtMaster, apiJobId);
	            	 		 JobOrder jobOrd = jobOrderList.get(apiJobId);
	            	 		 //********************* Adding by deepak ************************************
	            	 		 if(headQuarterId!=null && branchId ==null)
	            	 		 {
	            	 			 try{
	            	 			 if((jobOrd==null) ||((!jobOrd.getApiJobId().equalsIgnoreCase(apiJobId))|| (!jobOrd.getHeadQuarterMaster().getHeadQuarterId().equals(headQuarterId))))
	            	 			 {
	            	 				if(errorFlag){
			            	 			errorText+=" </br>";
			            	 			
			            	 		}
		            	 			jobFlag=true;
		            	 			multiErrorFlag=true;
		            	 			errorText+=Utility.getLocaleValuePropByKey("msgJobHqIdNotExist", locale);
			            	 		errorFlag=true; 
	            	 			 }
	            	 			 }catch(Exception e){
	            	 				if(errorFlag){
			            	 			errorText+=" </br>";
			            	 			
			            	 		}
		            	 			jobFlag=true;
		            	 			multiErrorFlag=true;
		            	 			errorText+=Utility.getLocaleValuePropByKey("msgJobHqIdNotExist", locale);
			            	 		errorFlag=true; 
	            	 				 e.printStackTrace();
	            	 			 }
	            	 		 
	            	 		 }
	            	 		 if(branchId!=null)
	            	 		 {
	            	 			 if((jobOrd==null) || (!jobOrd.getApiJobId().equalsIgnoreCase(apiJobId)) ||((!jobOrd.getBranchMaster().getBranchId().equals(branchId)) || (!jobOrd.getHeadQuarterMaster().getHeadQuarterId().equals(headQuarterId))))
	            	 			 {
	            	 				if(errorFlag){
			            	 			errorText+=" </br>";
			            	 			
			            	 		}
		            	 			jobFlag=true;
		            	 			multiErrorFlag=true;
		            	 			errorText+=Utility.getLocaleValuePropByKey("msgJobBrIdNotExist", locale);
			            	 		errorFlag=true; 
	            	 			 }
	            	 		 
	            	 		 }//******************** End  **************************************************
	            	 		 if((jobOrd==null) ||(!jobOrd.getApiJobId().equalsIgnoreCase(apiJobId))){
	            	 			if(errorFlag){
		            	 			errorText+=" </br>";
		            	 			
		            	 		}
	            	 			jobFlag=true;
	            	 			multiErrorFlag=true;
	            	 			errorText+=Utility.getLocaleValuePropByKey("msgJobIdNotExist", locale);
		            	 		errorFlag=true;
	            	 		}
	            	 	}
	            	 	if(user_first_name.equalsIgnoreCase("")||user_first_name.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+=Utility.getLocaleValuePropByKey("msgUserFirstNameNotAvail", locale);
	            	 		errorFlag=true;
	            	 	}
	            	 	if(user_last_name.equalsIgnoreCase("")||user_last_name.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br> ";	
	            	 		}
	            	 		errorText+=Utility.getLocaleValuePropByKey("msgUserLastNameNotAvail", locale);
	            	 		errorFlag=true;
	            	 	}
	            	 	if(user_email.equalsIgnoreCase("")||user_email.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+=Utility.getLocaleValuePropByKey("msgUserEmailNotAvailable", locale);
	            	 	}else if(!Utility.validEmailForTeacher(user_email)){
            	 			if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+=Utility.getLocaleValuePropByKey("msgUserEmailNotValid", locale);
	            	 	}else if(emailExist(emailExistCheck,userMasterlst,user_email,session.getId(),job_id) && jobFlag==false){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+=Utility.getLocaleValuePropByKey("msgUserEmailAlreadyRegt", locale);
	            	 	}
	            	 	else if(multiErrorFlag)
	            	 	{
	            	 		multiErrorFlag=false;
	            	 	}
	            	 	/*else if(emailTeacherExistInTemp(teachertemplst,user_email,job_id))             //***** Adding By Deepak for check email from Teacheruploadtemp ********
                        {
                        	if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="User Email has already Applied.";	
                        } */   
	            	 	else if(isAlreadyApplyThisJob)                                                          // * Check Duplicate Mail from TeacherDetail *****************// * Check Duplicate Mail from TeacherDetail *****************           
                        {
	            	 		System.out.println("isAlreadyApplyThisJob     ::::::::::::::"+isAlreadyApplyThisJob);
                        	if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="User already Applied the Job.";	
                        }                                                                                //************ End *****************
	            	 	else if(emailTeacherExist(teacherUploadTempList,user_email)!=0){
	            	 		teacherUploadTemp.setExist_teacherId(emailTeacherExist(teacherUploadTempList,user_email));
	            	 	}
	            	 	emailExistCheck.put(i+"|"+job_id,user_email);
	            	 	teacherUploadTemp.setJob_id(job_id);
	            	 	teacherUploadTemp.setUser_first_name(user_first_name);
	            	 	teacherUploadTemp.setUser_last_name(user_last_name);
	            	 	teacherUploadTemp.setUser_email(user_email);
	            	 	teacherUploadTemp.setSessionid(sessionId);
	            	 	
	            	 	if(errorText!=null)
	            	 	teacherUploadTemp.setErrortext(errorText);
	            	 	sessionHiber.insert(teacherUploadTemp);
		             }
	            }else{
	            	 if(job_id_count!=11 && user_first_name_count!=11 && user_last_name_count!=11 && user_email_count!=11){
	            		 returnVal="1"; 
	            		 teacherUploadTempDAO.deleteTeacherTemp(sessionId);
	            	 }else{
	            		 returnVal="";
	            		 boolean fieldVal=false;
		            	 if(job_id_count==11){
		            		 returnVal+=" job_id";
		            		 fieldVal=true;
		            	 }
		            	 if(user_first_name_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" user_first_name";
		            	 }
		            	 if(user_last_name_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" user_last_name";
		            	 }
		            	 if(user_email_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" user_email";
		            	 }
	            	 }
	            }
		     }
			 txOpen.commit();
     	 	 sessionHiber.close();
     	 	System.out.println("Data has been inserted successfully.");
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return returnVal;
	}
	public static Vector readDataExcelXLS(String fileName) throws IOException {

		System.out.println(":::::::::::::::::readDataExcel XLS::::::::::::");
		Vector vectorData = new Vector();
		
		try{
			InputStream ExcelFileToRead = new FileInputStream(fileName);
			HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);
	 
			HSSFSheet sheet=wb.getSheetAt(0);
			HSSFRow row; 
			HSSFCell cell;
	 
			Iterator rows = sheet.rowIterator();
			int date_applied_count=11;
	        int job_id_count=11;
	        int user_first_name_count=11;
	        int user_last_name_count=11;
	        int user_email_count=11;
	        String indexCheck="";
			 
			while (rows.hasNext()){
				row=(HSSFRow) rows.next();
				Iterator cells = row.cellIterator();
				Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false,dateFlag=false;
				String indexPrev="";
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
				while (cells.hasNext())
				{
					cell=(HSSFCell) cells.next();
					cIndex=cell.getColumnIndex();
					if(cell.toString().equalsIgnoreCase("date_applied")){
						date_applied_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(date_applied_count==cIndex){
						cellFlag=true;
						dateFlag=true;
					}	
					  
					if(cell.toString().equalsIgnoreCase("job_id")){
						job_id_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(job_id_count==cIndex){
						cellFlag=true;
					}
					
					if(cell.toString().equalsIgnoreCase("user_first_name")){
						user_first_name_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(user_first_name_count==cIndex){
						cellFlag=true;
					}
					
					
					if(cell.toString().equalsIgnoreCase("user_last_name")){
						user_last_name_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(user_last_name_count==cIndex){
						cellFlag=true;
					}
					
					
					if(cell.toString().equalsIgnoreCase("user_email")){
						user_email_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(user_email_count==cIndex){
						cellFlag=true;
					}
					if(cellFlag){
						if(!dateFlag){
	            			try{
	            				if(cell.getStringCellValue()!=null && cell.getStringCellValue().trim()!=null && !cell.getStringCellValue().trim().equalsIgnoreCase("")){
	            					mapCell.put(Integer.valueOf(cIndex),cell.getStringCellValue()+"");
	            				}
	            			}catch(Exception e){
	            				mapCell.put(Integer.valueOf(cIndex),new Double(cell.getNumericCellValue()).longValue()+"");
	            			}
	            		}else{
	            			mapCell.put(Integer.valueOf(cIndex),cell+"");
	            		}
					}
					dateFlag=false;
		        	cellFlag=false;
					
				}
				try{
				  if(mapCell.size()>0){
					  vectorCellEachRowData=cellValuePopulate(mapCell);
					  vectorData.addElement(vectorCellEachRowData);
				  }
               }catch(Exception e){
				   e.printStackTrace();
               }
			}
		}catch(Exception e){}
		return vectorData;
    }
	public static Vector readDataExcelXLSX(String fileName) {
		System.out.println(":::::::::::::::::readDataExcel XLSX ::::::::::::");
        Vector vectorData = new Vector();
        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            XSSFWorkbook xssfWorkBook = new XSSFWorkbook(fileInputStream);
            // Read data at sheet 0
            XSSFSheet xssfSheet = xssfWorkBook.getSheetAt(0);
            Iterator rowIteration = xssfSheet.rowIterator();
            int date_applied_count=11;
            int job_id_count=11;
            int user_first_name_count=11;
            int user_last_name_count=11;
            int user_email_count=11;
            // Looping every row at sheet 0
            String indexCheck="";
            while (rowIteration.hasNext()) {
                XSSFRow xssfRow = (XSSFRow) rowIteration.next();
                Iterator cellIteration = xssfRow.cellIterator();
                Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false,dateFlag=false;
				String indexPrev="";
                // Looping every cell in each row at sheet 0
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
                while (cellIteration.hasNext()){
                    XSSFCell xssfCell = (XSSFCell) cellIteration.next();
                    cIndex=xssfCell.getColumnIndex();
                    
                    if(xssfCell.toString().equalsIgnoreCase("date_applied")){
	            		date_applied_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(date_applied_count==cIndex){
	            		cellFlag=true;
	            		dateFlag=true;
	            	}	
	            	  
	            	if(xssfCell.toString().equalsIgnoreCase("job_id")){
	            		job_id_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(job_id_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("user_first_name")){
	            		user_first_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(user_first_name_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("user_last_name")){
	            		user_last_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(user_last_name_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("user_email")){
	            		user_email_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(user_email_count==cIndex){
	            		cellFlag=true;
	            	}
	            	if(cellFlag){
	            		if(!dateFlag){
	            			try{
	            				if(xssfCell.getStringCellValue()!=null && xssfCell.getStringCellValue().trim()!=null && !xssfCell.getStringCellValue().trim().equalsIgnoreCase("")){
	            					mapCell.put(Integer.valueOf(cIndex),xssfCell.getStringCellValue()+"");
	            				}
	            			}catch(Exception e){
	            				mapCell.put(Integer.valueOf(cIndex),xssfCell.getRawValue()+"");
	            			}
	            		}else{
	            			mapCell.put(Integer.valueOf(cIndex),xssfCell+"");
	            		}
	            	}
	            	dateFlag=false;
	            	cellFlag=false;
                }
              
                try{
    				  if(mapCell.size()>0){
	                	vectorCellEachRowData=cellValuePopulate(mapCell);
	                	vectorData.addElement(vectorCellEachRowData);
	                  }
                }catch(Exception e){
                	e.printStackTrace();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
         
        return vectorData;
    }
	public static Vector cellValuePopulate(Map<Integer,String> mapCell){
		 Vector vectorCellEachRowData = new Vector();
		 Map<Integer,String> mapCellTemp = new TreeMap<Integer, String>();
		 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false;
		 for(Map.Entry<Integer, String> entry : mapCell.entrySet()){
			int key=entry.getKey();
			String cellValue=null;
			if(entry.getValue()!=null)
				cellValue=entry.getValue().trim();
			
			if(key==0){
				mapCellTemp.put(key, cellValue);
				flag0=true;
			}
			if(key==1){
				flag1=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==2){
				flag2=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==3){
				flag3=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==4){
				flag4=true;
				mapCellTemp.put(key, cellValue);
			}
		 }
		 if(flag0==false){
			 mapCellTemp.put(0, "");
		 }
		 if(flag1==false){
			 mapCellTemp.put(1, "");
		 }
		 if(flag2==false){
			 mapCellTemp.put(2, "");
		 }
		 if(flag3==false){
			 mapCellTemp.put(3, "");
		 }
		 if(flag4==false){
			 mapCellTemp.put(4, "");
		 }
		 for(Map.Entry<Integer, String> entry : mapCellTemp.entrySet()){
			 vectorCellEachRowData.addElement(entry.getValue());
		 }
		 return vectorCellEachRowData;
	}
	
	public static String monthReplace(String date){
		String currentDate="";
		try{
			
		   if(date.contains("Jan")){
			   currentDate=date.replaceAll("Jan","01");
		   }
		   if(date.contains("Feb")){
			   currentDate=date.replaceAll("Feb","02");
		   }
		   if(date.contains("Mar")){
			   currentDate=date.replaceAll("Mar","03");
		   }
		   if(date.contains("Apr")){
			   currentDate=date.replaceAll("Apr","04");
		   }
		   if(date.contains("May")){
			   currentDate=date.replaceAll("May","05");
		   }
		   if(date.contains("Jun")){
			   currentDate=date.replaceAll("Jun","06");
		   }
		   if(date.contains("Jul")){
			   currentDate=date.replaceAll("Jul","07");
		   }
		   if(date.contains("Aug")){
			   currentDate=date.replaceAll("Aug","08");
		   }
		   if(date.contains("Sep")){
			   currentDate=date.replaceAll("Sep","09");
		   }
		   if(date.contains("Oct")){
			   currentDate=date.replaceAll("Oct","10");
		   }
		   if(date.contains("Nov")){
			   currentDate=date.replaceAll("Nov","11");
		   }
		   if(date.contains("Dec")){
			   currentDate=date.replaceAll("Dec","12");
		   }
		   String uDate=currentDate.replaceAll("-","/");
		   if(Utility.isDateValid(uDate,0)==2){
			   currentDate=date; 
		   }else{
			   try{
				   String dateSplit[]=uDate.split("/");
				   currentDate=dateSplit[1]+"/"+dateSplit[0]+"/"+dateSplit[2];
			   }catch(Exception e){
				   currentDate=date; 
			   }
		   }
		}catch(Exception e){
			currentDate=date;
		}
		   return currentDate;
	 }
	 
	 	
	//************* Adding By Deepak  *******************
	public boolean emailTeacherExistInTemp(List<TeacherUploadTemp> userTemplst,String email,String job_id)
	{
		boolean emailFlag=false;
		for(TeacherUploadTemp userlist :userTemplst ){
			System.out.println(userlist.getJob_id());
			if(userlist.getUser_email().equalsIgnoreCase(email) || userlist.getJob_id().equalsIgnoreCase(job_id)){
				emailFlag=true;
			}
		}
		if(emailFlag)
		{
			return true;
		}
		else
		{
			return false;	
		}
		
	}
	
	public Map<String,Boolean> emailTeacherExistInTeacherDetail(List <TeacherDetail> userlst,String []ApiId , UserMaster userMaster)
	{
		Map<String,Boolean> mapJFT=new LinkedHashMap<String,Boolean>();
		try{
			int counter=0;
			System.out.println("userlst.size():::::::::::"+userlst.size());
			List<String> styrTypeList= new ArrayList<String>();
			if(ApiId!=null && ApiId.length>0){
				System.out.println("  ApiId.length :: "+ ApiId.length);
				styrTypeList =  Arrays.asList(ApiId);
			}
			for(TeacherDetail td:userlst){
				mapJFT.put(td.getEmailAddress()+"##"+ApiId[counter], false);
				counter++;
			}
			
			List<JobOrder> lstJobOrder = jobOrderDAO.findJobOrderListByApiJobIdListAndHeadOrDistrict(userMaster.getHeadQuarterMaster(), userMaster.getDistrictId(), styrTypeList);

			System.out.println(" lstJobOrder :: "+lstJobOrder.size());
			List<JobForTeacher> lstJFT=new ArrayList<JobForTeacher>();
			if(userlst.size()>0)
			{
				try{
					 lstJFT=jobForTeacherDAO.findByCriteria(Restrictions.in("teacherId",userlst),Restrictions.in("jobId",lstJobOrder));
					}catch (Exception e) {
					  e.printStackTrace();
					}
			
			for(JobForTeacher jft:lstJFT){
				Boolean isAddInMap=mapJFT.get(jft.getTeacherId().getEmailAddress().toLowerCase().trim()+"##"+jft.getJobId().getApiJobId());
				if(isAddInMap!=null && !isAddInMap)
				mapJFT.put(jft.getTeacherId().getEmailAddress().toLowerCase().trim()+"##"+jft.getJobId().getApiJobId(), true);
			}
			}
			else
			{
				mapJFT.put("000000@0000.000"+"##"+0, false);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return mapJFT;	
	}
	
	//**************************************  End    ************************************
}

