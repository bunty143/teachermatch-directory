package tm.services;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.EventDetails;
import tm.bean.EventFacilitatorsList;
import tm.bean.FacilitatorUploadTemp;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.EventDetailsDAO;
import tm.dao.EventFacilitatorsListDAO;
import tm.dao.FacilitatorUploadTempDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.Utility;

public class ManageFacilitatorsAjax {
	
String locale = Utility.getValueOfPropByKey("locale");	
@Autowired
public UserMasterDAO userMasterDAO;
public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
	this.userMasterDAO = userMasterDAO;
}
@Autowired
public DistrictMasterDAO districtMasterDAO;
public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
{
this.districtMasterDAO = districtMasterDAO;
}
@Autowired
public EventDetailsDAO eventDetailsDAO;
public void setEventDetailsDAO(EventDetailsDAO eventDetailsDAO) {
	this.eventDetailsDAO = eventDetailsDAO;
}
@Autowired
public EventFacilitatorsListDAO eventFacilitatorsListDAO;
public void setEventFacilitatorsListDAO(
		EventFacilitatorsListDAO eventFacilitatorsListDAO) {
	this.eventFacilitatorsListDAO = eventFacilitatorsListDAO;
}
@Autowired
public SchoolMasterDAO schoolMasterDAO;
public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
	this.schoolMasterDAO = schoolMasterDAO;
}
@Autowired
public FacilitatorUploadTempDAO facilitatorUploadTempDAO;
public void setFacilitatorUploadTempDAO(
		FacilitatorUploadTempDAO facilitatorUploadTempDAO) {
	this.facilitatorUploadTempDAO = facilitatorUploadTempDAO;
}
@Autowired
public RoleMasterDAO roleMasterDAO;
public void setRoleMasterDAO(RoleMasterDAO roleMasterDAO) {
	this.roleMasterDAO = roleMasterDAO;
}

public String getUserByDistrict(int eventId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,String firstName,String lastName,
		String emailAddress)
{  WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	StringBuffer dmRecords =	new StringBuffer();
	
	try{
		System.out.println("----"+noOfRow);	
		
		int noOfRowInPage	=	Integer.parseInt(noOfRow);
		int pgNo			= 	Integer.parseInt(pageNo);
		int start 			= 	((pgNo-1)*noOfRowInPage);
		int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord 	=	0;
		//------------------------------------
	
		String sortOrderFieldName	=	"roleId";
		String sortOrderNoField		=	"roleId";
		Order  sortOrderStrVal		=	null;
		
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null) ){
				 sortOrderFieldName	=	sortOrder;
				 sortOrderNoField	=	sortOrder;
			 }
			 if(sortOrder.equals("firstName")) {
				 sortOrderNoField="firstName";
			 }
			 
			 if(sortOrder.equals("lastName")) {
				 sortOrderNoField="lastName";
			 }
			 if(sortOrder.equals("emailAddress")) {
				 sortOrderNoField="emailAddress";
			 }
			 if(sortOrder.equals("schoolId")) {
				 sortOrderNoField="schoolId";
			 }
			 
		}
		if(!sortOrder.equals("") && !sortOrder.equals(null))
		{
			sortOrderFieldName		=	sortOrder;
		}
		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null))
		{
			if(sortOrderType.equals("0"))
			{
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else
			{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		} else {
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
		
		List<Criterion> basicSearch = new ArrayList<Criterion>();
		
		if(firstName!=null && !firstName.equals(""))
		{
			Criterion criterion = Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE);
			basicSearch.add(criterion);
		}
			
		if(lastName!=null && !lastName.equals(""))
		{
			Criterion criterion = Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE);
			basicSearch.add(criterion);
		}

		if(emailAddress!=null && !emailAddress.equals(""))
		{
			Criterion criterion = Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE);
			basicSearch.add(criterion);
		}
		
		
		
		List<UserMaster> userMaster	 =	new ArrayList<UserMaster>();
		EventDetails eventDetails=eventDetailsDAO.findById(eventId,false, false);
		DistrictMaster districtMaster=eventDetails.getDistrictMaster();
		
		Criterion crt=Restrictions.eq("districtId", districtMaster);
		
		//totalRecord=userMasterDAO.getRowCount(crt);
		
		userMaster=userMasterDAO.getUsersByDistrict(districtMaster,basicSearch);
		
		
		List<UserMaster> sortedeventdetails  =	 new ArrayList<UserMaster>();

		SortedMap<String,UserMaster>	sortedMap = new TreeMap<String,UserMaster>();
		if(sortOrderNoField.equals("roleId")) {
			sortOrderFieldName	=	"roleId";
		}
		if(sortOrderNoField.equals("firstName")) {
			sortOrderFieldName	=	"firstName";
		}

		if(sortOrderNoField.equals("lastName")) {
			sortOrderFieldName	=	"lastName";
		}
		if(sortOrderNoField.equals("emailAddress")) {
			sortOrderFieldName	=	"emailAddress";
		}
		if(sortOrderNoField.equals("schoolId")) {
			sortOrderFieldName	=	"schoolId";
		}
		
			int mapFlag=2;
		String orderFieldName = "";
		for (UserMaster usrdtl : userMaster)
		{
		orderFieldName=""+usrdtl.getRoleId().getRoleName();
			if(sortOrderFieldName.equals("roleId")){
				orderFieldName=usrdtl.getRoleId().getRoleName()+"||"+usrdtl.getFirstName()+"||"+usrdtl.getLastName();
				sortedMap.put(orderFieldName+"||",usrdtl);
				if(sortOrderTypeVal.equals("0")){
					mapFlag=0;
				} else {
					mapFlag=1;
				}
			}

			orderFieldName=""+usrdtl.getFirstName();
			if(sortOrderFieldName.equals("firstName")){
				orderFieldName=""+usrdtl.getFirstName()+"||"+usrdtl.getLastName();
				sortedMap.put(orderFieldName+"||",usrdtl);
				if(sortOrderTypeVal.equals("0")){
					mapFlag=0;
				}else{
					mapFlag=1;
				}
			}

			orderFieldName=""+usrdtl.getLastName();
			if(sortOrderFieldName.equals("lastName")){
			orderFieldName=usrdtl.getLastName()+"||"+usrdtl.getFirstName();
				sortedMap.put(orderFieldName+"||",usrdtl);
				if(sortOrderTypeVal.equals("0")){
					mapFlag=0;
				}else{
					mapFlag=1;
				}
			}
				orderFieldName=""+usrdtl.getEmailAddress();
				if(sortOrderFieldName.equals("emailAddress")){
				orderFieldName=usrdtl.getEmailAddress()+"||"+usrdtl.getFirstName()+"||"+usrdtl.getLastName();
					sortedMap.put(orderFieldName+"||",usrdtl);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}	
									
			}
		if(usrdtl.getSchoolId()!=null){
				orderFieldName=""+usrdtl.getSchoolId().getSchoolName();
				if(sortOrderFieldName.equals("schoolId")){
				orderFieldName=usrdtl.getSchoolId().getSchoolName()+"||"+usrdtl.getEmailAddress()+"||"+usrdtl.getFirstName()+"||"+usrdtl.getLastName();
					sortedMap.put(orderFieldName+"||",usrdtl);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}		
				}}
				
		}
		if(mapFlag==1) {
			NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet(); 
			for (Iterator iter=navig.iterator();iter.hasNext();) {  
				Object key = iter.next(); 
				sortedeventdetails.add((UserMaster) sortedMap.get(key));
			} 
		} else if(mapFlag==0){
			Iterator iterator = sortedMap.keySet().iterator();
			while (iterator.hasNext()) {
				Object key = iterator.next();
				sortedeventdetails.add((UserMaster) sortedMap.get(key));
			}
		} else {
			sortedeventdetails=userMaster;
		}

		totalRecord =sortedeventdetails.size();		
		if(totalRecord<end)
		end=totalRecord;
        List<UserMaster> lstsortedeventDetails		=	sortedeventdetails.subList(start,end);
		
        dmRecords.append("<table id='userTable' border='0' class='table table-bordered table-striped' >");
		dmRecords.append("<thead class='bg'>");
		dmRecords.append("<tr>");
		String responseText="";
		dmRecords.append("<th></th>");
		responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblRole", locale),sortOrderFieldName,"roleId",sortOrderTypeVal,pgNo,"userGrid");
		dmRecords.append("<th width='19%' valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblFname", locale),sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo,"userGrid");
		dmRecords.append("<th width='19%' valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblLname", locale),sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo,"userGrid");
		dmRecords.append("<th width='19%' valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblEmailAddress", locale),sortOrderFieldName,"emailAddress",sortOrderTypeVal,pgNo,"userGrid");
		dmRecords.append("<th width='19%' valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblSchoolName", locale),sortOrderFieldName,"schoolId",sortOrderTypeVal,pgNo,"userGrid");
		dmRecords.append("<th width='19%' valign='top'>"+responseText+"</th>");
		dmRecords.append("</tr>");
		dmRecords.append("</thead>");
		/*================= Checking If Record Not Found ======================*/
		if(userMaster.size()==0)
			dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNoNotefound1", locale)+"</td></tr>" );

		for (UserMaster userMaster2 :lstsortedeventDetails) {
			dmRecords.append("<tr>");
			dmRecords.append("<td>");
			dmRecords.append("<input type='checkbox' name='"+userMaster2.getUserId()+"' value='' onclick='saveFacilitatorBySelect("+userMaster2.getUserId()+")'/>");
			dmRecords.append("</td>");
			dmRecords.append("<td>"+userMaster2.getRoleId().getRoleName()+"</td>");
			dmRecords.append("<td>"+userMaster2.getFirstName()+"</td>");
			dmRecords.append("<td>"+userMaster2.getLastName()+"</td>");
			dmRecords.append("<td>"+userMaster2.getEmailAddress()+"</td>");
			if(userMaster2.getSchoolId()!=null){
			dmRecords.append("<td>"+userMaster2.getSchoolId().getSchoolName()+"</td>");
			}else
			{
				dmRecords.append("<td></td>");	
			}
			}
		dmRecords.append("</table>");
		dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
    	} catch (Exception e) {
	   	e.printStackTrace();
	     }
	return dmRecords.toString();		
}

public String saveFacilitatorByUserId(int userId,int eventId)
{
	String data="success";
	boolean errorMsgForFacilitator=true;
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
	try
	{
	UserMaster userMaster=userMasterDAO.findById(userId, false,false);
	EventDetails eventDetails=eventDetailsDAO.findById(eventId, false,false);
	
	Criterion critevents=Restrictions.eq("eventDetails",eventDetails);
	Criterion criterion=Restrictions.eq("facilitatorEmailAddress", userMaster.getEmailAddress());
	List<EventFacilitatorsList> facilist=eventFacilitatorsListDAO.findByCriteria(criterion,critevents);
	
	
	//List<EventFacilitatorsList> eventFacilitatorsListForMapping=eventFacilitatorsListDAO.findByCriteria(critevents);
	
	if(facilist.size()!=0)
	{
		data=Utility.getLocaleValuePropByKey("msgFacilitatorAlreadyExist", locale);	
		errorMsgForFacilitator=false;
	}else
	  {
		/*if(eventDetails.getEventFormatId().getEventFormatName().equalsIgnoreCase("One to One") || eventDetails.getEventFormatId().getEventFormatName().equalsIgnoreCase("One to Many")){
			if(eventFacilitatorsListForMapping!=null && eventFacilitatorsListForMapping.size()>0){
				data="Only One Facilitator can be added for this event.";
				errorMsgForFacilitator=false;
			}
		}*/
		
		if(errorMsgForFacilitator){
			EventFacilitatorsList eventFacilitatorsList=new EventFacilitatorsList();
			System.out.println(eventDetails.getEventId());
			eventFacilitatorsList.setEventDetails(eventDetails);
			eventFacilitatorsList.setDistrictMaster(eventDetails.getDistrictMaster());
			
			eventFacilitatorsList.setHeadQuarterMaster(eventDetails.getHeadQuarterMaster());
			eventFacilitatorsList.setBranchMaster(userMaster.getBranchMaster());
			
			 
			eventFacilitatorsList.setFacilitatorFirstName(userMaster.getFirstName());
			eventFacilitatorsList.setFacilitatorLastName(userMaster.getLastName());
			eventFacilitatorsList.setFacilitatorEmailAddress(userMaster.getEmailAddress());
			eventFacilitatorsList.setSchoolId(userMaster.getSchoolId());
			eventFacilitatorsList.setInvitationEmailSent(0);
			eventFacilitatorsList.setStatus("A");
			eventFacilitatorsList.setCreatedBY(userSession);
			eventFacilitatorsList.setCreatedDateTime(new Date());
			eventFacilitatorsListDAO.makePersistent(eventFacilitatorsList);
		}
		
	  }
	
	}catch(Exception e)
	{
		e.printStackTrace();
	}
return data;	
}
public String getFacilitatorGrid(int eventId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
{
	/*WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	StringBuffer dmRecords =	new StringBuffer();
	
	try{
		System.out.println("----"+noOfRow);	
		
		int noOfRowInPage	=	Integer.parseInt(noOfRow);
		int pgNo			= 	Integer.parseInt(pageNo);
		int start 			= 	((pgNo-1)*noOfRowInPage);
		System.out.println(start);
		int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord 	=	0;
		//------------------------------------
	
		String sortOrderFieldName	=	"eventFacilitatorId";
		String sortOrderNoField		=	"eventFacilitatorId";
		Order  sortOrderStrVal		=	null;
		
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null) ){
				 sortOrderFieldName	=	sortOrder;
				 sortOrderNoField	=	sortOrder;
			 }
			 if(sortOrder.equals("facilitatorFirstName")) {
				 sortOrderNoField="facilitatorFirstName";
			 }
			 
			 if(sortOrder.equals("facilitatorLastName")) {
				 sortOrderNoField="facilitatorLastName";
			 }
			 if(sortOrder.equals("facilitatorEmailAddress")) {
				 sortOrderNoField="facilitatorEmailAddress";
			 }
			 if(sortOrder.equals("schoolId")) {
				 sortOrderNoField="schoolId";
			 }
			 
		}
		if(!sortOrder.equals("") && !sortOrder.equals(null))
		{
			sortOrderFieldName		=	sortOrder;
		}
		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null))
		{
			if(sortOrderType.equals("0"))
			{
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else
			{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		} else {
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
		
		
		List<EventFacilitatorsList> eventfacilitatorList	 =	null;
		EventDetails eventDetails=eventDetailsDAO.findById(eventId,false, false);
		Criterion crt=Restrictions.eq("eventDetails", eventDetails);
		totalRecord=eventFacilitatorsListDAO.getRowCount(crt);
		eventfacilitatorList=eventFacilitatorsListDAO.findByCriteria(crt);
		List<EventFacilitatorsList> sortedeventdetails  =	 new ArrayList<EventFacilitatorsList>();

		SortedMap<String,EventFacilitatorsList>	sortedMap = new TreeMap<String,EventFacilitatorsList>();
		if(sortOrderNoField.equals("eventFacilitatorId")) {
			sortOrderFieldName	=	"eventFacilitatorId";
		}
		if(sortOrderNoField.equals("facilitatorFirstName")) {
			sortOrderFieldName	=	"facilitatorFirstName";
		}

		if(sortOrderNoField.equals("facilitatorLastName")) {
			sortOrderFieldName	=	"facilitatorLastName";
		}
		if(sortOrderNoField.equals("facilitatorEmailAddress")) {
			sortOrderFieldName	=	"facilitatorEmailAddress";
		}
		if(sortOrderNoField.equals("schoolId")) {
			sortOrderFieldName	=	"facilitatorEmailAddress";
		}
		
			int mapFlag=2;
		String orderFieldName = "";
		for (EventFacilitatorsList usrdtl : eventfacilitatorList)
		{
		orderFieldName=""+usrdtl.getEventFacilitatorId();
			if(sortOrderFieldName.equals("eventFacilitatorId")){
				orderFieldName=usrdtl.getEventFacilitatorId()+"||"+usrdtl.getFacilitatorFirstName()+"||"+usrdtl.getFacilitatorLastName();
				sortedMap.put(orderFieldName+"||",usrdtl);
				if(sortOrderTypeVal.equals("0")){
					mapFlag=0;
				} else {
					mapFlag=1;
				}
			}

			orderFieldName=""+usrdtl.getFacilitatorFirstName();
			if(sortOrderFieldName.equals("facilitatorFirstName")){
				orderFieldName=""+usrdtl.getFacilitatorFirstName()+"||"+usrdtl.getFacilitatorLastName();
				sortedMap.put(orderFieldName+"||",usrdtl);
				if(sortOrderTypeVal.equals("0")){
					mapFlag=0;
				}else{
					mapFlag=1;
				}
			}

			orderFieldName=""+usrdtl.getFacilitatorLastName();
			if(sortOrderFieldName.equals("facilitatorLastName")){
			orderFieldName=usrdtl.getFacilitatorLastName()+"||"+usrdtl.getFacilitatorFirstName();
				sortedMap.put(orderFieldName+"||",usrdtl);
				if(sortOrderTypeVal.equals("0")){
					mapFlag=0;
				}else{
					mapFlag=1;
				}
			}
				orderFieldName=""+usrdtl.getFacilitatorEmailAddress();
				if(sortOrderFieldName.equals("facilitatorEmailAddress")){
				orderFieldName=usrdtl.getFacilitatorEmailAddress()+"||"+usrdtl.getFacilitatorFirstName()+"||"+usrdtl.getFacilitatorLastName();
					sortedMap.put(orderFieldName+"||",usrdtl);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}	
									
			}
		if(usrdtl.getSchoolId()!=null){
				orderFieldName=""+usrdtl.getSchoolId().getSchoolName();
				if(sortOrderFieldName.equals("schoolId")){
				orderFieldName=usrdtl.getSchoolId().getSchoolName()+"||"+usrdtl.getFacilitatorFirstName()+"||"+usrdtl.getFacilitatorLastName()+"||"+usrdtl.getFacilitatorEmailAddress();
					sortedMap.put(orderFieldName+"||",usrdtl);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}		
				}}
				
		}
		if(mapFlag==1) {
			NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet(); 
			for (Iterator iter=navig.iterator();iter.hasNext();) {  
				Object key = iter.next(); 
				sortedeventdetails.add((EventFacilitatorsList) sortedMap.get(key));
			} 
		} else if(mapFlag==0){
			Iterator iterator = sortedMap.keySet().iterator();
			while (iterator.hasNext()) {
				Object key = iterator.next();
				sortedeventdetails.add((EventFacilitatorsList) sortedMap.get(key));
			}
		} else {
			sortedeventdetails=eventfacilitatorList;
		}

		totalRecord =sortedeventdetails.size();		
		if(totalRecord<end)
		end=totalRecord;
        List<EventFacilitatorsList> lstsortedeventDetails		=	sortedeventdetails.subList(start,end);
		
        dmRecords.append("<table id='facilitatorTable' border='0' class='table table-bordered table-striped' >");
		dmRecords.append("<thead class='bg'>");
		dmRecords.append("<tr>");
		String responseText="";
		
		responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblFname", locale),sortOrderFieldName,"facilitatorFirstName",sortOrderTypeVal,pgNo,"factgrid");
		dmRecords.append("<th width='19%' valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblLname", locale),sortOrderFieldName,"facilitatorLastName",sortOrderTypeVal,pgNo,"factgrid");
		dmRecords.append("<th width='19%' valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblEmailAddress", locale),sortOrderFieldName,"facilitatorEmailAddress",sortOrderTypeVal,pgNo,"factgrid");
		dmRecords.append("<th width='19%' valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLinkCommonMul(Utility.getLocaleValuePropByKey("lblSchoolName", locale),sortOrderFieldName,"schoolId",sortOrderTypeVal,pgNo,"factgrid");
		dmRecords.append("<th width='19%' valign='top'>"+responseText+"</th>");
		dmRecords.append("<th>Action</th>");
		dmRecords.append("</tr>");
		dmRecords.append("</thead>");
		================= Checking If Record Not Found ======================
		if(eventfacilitatorList.size()==0)
			dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );

		for (EventFacilitatorsList eventdtllist :lstsortedeventDetails) {
			dmRecords.append("<tr>");
			dmRecords.append("<td>"+eventdtllist.getFacilitatorFirstName()+"</td>");
			dmRecords.append("<td>"+eventdtllist.getFacilitatorLastName()+"</td>");
			dmRecords.append("<td>"+eventdtllist.getFacilitatorEmailAddress()+"</td>");
			if(eventdtllist.getSchoolId()!=null){
			dmRecords.append("<td>"+eventdtllist.getSchoolId().getSchoolName()+"</td>");
			}else
			{
				dmRecords.append("<td></td>");	
			}
			dmRecords.append("<td>");
			dmRecords.append("<a href='javascript:void(0);' onclick='return deleteFacilitator("+eventdtllist.getEventFacilitatorId()+")'>Remove</a>");
			dmRecords.append("</td>");
			}
		dmRecords.append("</table>");
		dmRecords.append(PaginationAndSorting. getPaginationStringForFacilitatorAjax(request,totalRecord,noOfRow, pageNo));
    	} catch (Exception e) {
	   	e.printStackTrace();
	     }
	return dmRecords.toString();	*/	
	
	return null;
}
public boolean removeFacilitator(int facilitatorId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	try{
		EventFacilitatorsList eventFacilitatorsList=	null;
		eventFacilitatorsList						=	eventFacilitatorsListDAO.findById(facilitatorId, false, false);
		eventFacilitatorsListDAO.makeTransient(eventFacilitatorsList);
	}catch (Exception e) {
		e.printStackTrace();
		return false;
	}
	return true;
}
private Vector vectorDataExcelXLSX = new Vector();
public String saveFacilitatorTemp(String fileName,String sessionId,int eventId)
{
	/* ========  For Session time Out Error =========*/
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
	String returnVal="";
    try{
    	 String root = request.getRealPath("/")+"/facilitatoruploads/";
    	 String filePath=root+fileName;
    	 System.out.println("filePath :"+filePath);
    	 if(fileName.contains(".xlsx")){
    		 vectorDataExcelXLSX = readDataExcelXLSX(filePath);
    	 }else{
    		 vectorDataExcelXLSX = readDataExcelXLS(filePath);
    	 }
   
	  int count=0;
		 int firstnameCcount=0;
		 int lastnameCcount=0;
		 int emailaddressCcount=0;
		
		 
		     System.out.println(vectorDataExcelXLSX.size());
		 
		 if(vectorDataExcelXLSX.size()>0)
		 {
			    Vector vectorCellEachColumn = (Vector) vectorDataExcelXLSX.get(0);
			   	 
	          	 
		for(int j=0;j<vectorCellEachColumn.size();j++)	 
		{
			String columnname=vectorCellEachColumn.get(j).toString().trim();
			
			if(columnname.equalsIgnoreCase("facilitatorFirstName"))
			{
				firstnameCcount=j;
				count++;
			}
			if(columnname.equalsIgnoreCase("facilitatorLastName"))
			{
				lastnameCcount=j;
				count++;
			}
			
			if(columnname.equalsIgnoreCase("facilitatorEmailAddress"))
			{
				emailaddressCcount=j;
				count++;
			}
		}
		} 
		     
		
		 if(count>=3)
		 {
		 returnVal="data inserted";
		 EventDetails eventDetails=eventDetailsDAO.findById(eventId,false,false);
      	 Criterion critevents=Restrictions.eq("eventDetails",eventDetails);
      	 List<EventFacilitatorsList> eventfaclist=eventFacilitatorsListDAO.findByCriteria(critevents);
	    
      	 List<String> facemail=new ArrayList<String>();
      	 
      	 for(EventFacilitatorsList evnfaclst:eventfaclist)
      	 {
      		facemail.add(evnfaclst.getFacilitatorEmailAddress()); 
      	 } 
		 
		     for(int i=1; i<vectorDataExcelXLSX.size(); i++) {
             Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
             String facilitatorFirstName="",facilitatorLastName="",facilitatorEmailAddress="",
             errorText="";
          
            facilitatorFirstName=vectorCellEachRowData.get(firstnameCcount).toString().trim();
          	facilitatorLastName=vectorCellEachRowData.get(lastnameCcount).toString().trim();
          	facilitatorEmailAddress=vectorCellEachRowData.get(emailaddressCcount).toString().trim();
          	System.out.println(facilitatorFirstName+" "+facilitatorLastName+" "+facilitatorEmailAddress);
          
          	
          	int unfillcol=0; 
          	
          	if(facilitatorFirstName.equalsIgnoreCase(""))
          	{
          		unfillcol++;
          		errorText+=" </br>";
          		errorText+=Utility.getLocaleValuePropByKey("msgFirstNameblank", locale);
          	}
          	
        	
          	if(facilitatorLastName.equalsIgnoreCase(""))
          	{
          		unfillcol++;
          		errorText+=" </br>";
          		errorText+=Utility.getLocaleValuePropByKey("msgLastNameblank", locale);
          	}
          	
        	
          	
          	
          	 String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
          	 Boolean b = facilitatorEmailAddress.matches(EMAIL_REGEX);
          	         	 
          	 
          	if(facilitatorEmailAddress.equalsIgnoreCase(""))
          	{
          		unfillcol++;
          		errorText+=" </br>";
          		errorText+=Utility.getLocaleValuePropByKey("msgEmailAddressCanblank", locale);
          	}
          	 
          	else if(!b)
          	{
          		errorText+=" </br>";
          		errorText+=Utility.getLocaleValuePropByKey("msgEmailAddressnotvalid", locale); 
          	}	 
          	 
          	else if(facemail.contains(facilitatorEmailAddress))
          	{
          		errorText+=" </br>";
          		errorText+=Utility.getLocaleValuePropByKey("msgEmailAddressAlreadyExists", locale); 	
          	}	
          	if(unfillcol!=3){
          	FacilitatorUploadTemp facilitatorUploadTemp=new FacilitatorUploadTemp();
          	facilitatorUploadTemp.setFirstName(facilitatorFirstName);
          	facilitatorUploadTemp.setLastName(facilitatorLastName);
          	facilitatorUploadTemp.setEmailId(facilitatorEmailAddress);
          	System.out.println(sessionId);
          	facilitatorUploadTemp.setSessionId(sessionId);
          	facilitatorUploadTemp.setErrorText(errorText);
          	facilitatorUploadTempDAO.makePersistent(facilitatorUploadTemp);
          	}
            	
	     }
		 }
 	 	System.out.println("Data has been inserted successfully.");
	}catch (Exception e){
		e.printStackTrace();
	}
	
	return returnVal;
}
public static Vector readDataExcelXLS(String fileName) throws IOException {

	System.out.println(":::::::::::::::::readDataExcel XLS::::::::::::");
	Vector vectorData = new Vector();
	
	try{
		InputStream ExcelFileToRead = new FileInputStream(fileName);
		HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);
 
		HSSFSheet sheet=wb.getSheetAt(0);
		HSSFRow row; 
		HSSFCell cell;
 
		Iterator rows = sheet.rowIterator();
	   	int facilitatorFirstName_count=11;
    	int facilitatorLastName_count=11;
    	int facilitatorEmailAddress_count=11;
    	int schoolId_count=11;
    	int invitationEmailSent_count=11;
    	
       String indexCheck="";
		 
		while (rows.hasNext()){
			row=(HSSFRow) rows.next();
			Iterator cells = row.cellIterator();
			Vector vectorCellEachRowData = new Vector();
			int cIndex=0; 
			boolean cellFlag=false;
			String indexPrev="";
			Map<Integer,String> mapCell = new TreeMap<Integer, String>();
			while (cells.hasNext())
			{
				cell=(HSSFCell) cells.next();
				cIndex=cell.getColumnIndex();
							  
				
				
				if(cell.toString().equalsIgnoreCase("facilitatorFirstName")){
					facilitatorFirstName_count=cIndex;
					indexCheck+="||"+cIndex+"||";
				}
				if(facilitatorFirstName_count==cIndex){
					cellFlag=true;
				}
				
				
				if(cell.toString().equalsIgnoreCase("facilitatorLastName")){
					facilitatorLastName_count=cIndex;
					indexCheck+="||"+cIndex+"||";
				}
				if(facilitatorLastName_count==cIndex){
					cellFlag=true;
				}
				
			   if(cell.toString().equalsIgnoreCase("facilitatorEmailAddress")){
					facilitatorEmailAddress_count=cIndex;
					indexCheck+="||"+cIndex+"||";
				}
				if(facilitatorEmailAddress_count==cIndex){
					cellFlag=true;
				}
				
				 if(cell.toString().equalsIgnoreCase("schoolId")){
					 schoolId_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(schoolId_count==cIndex){
						cellFlag=true;
					}
					
					 if(cell.toString().equalsIgnoreCase("invitationEmailSent")){
						 invitationEmailSent_count=cIndex;
							indexCheck+="||"+cIndex+"||";
						}
						if(invitationEmailSent_count==cIndex){
							cellFlag=true;
						}
					
				if(cellFlag){
					
            			mapCell.put(Integer.valueOf(cIndex),cell+"");
            		}
				}
				cellFlag=false;
		
		    vectorCellEachRowData=cellValuePopulate(mapCell);
			vectorData.addElement(vectorCellEachRowData);
		}
	}catch(Exception e){}
	return vectorData;
}
public static Vector readDataExcelXLSX(String fileName) {
	System.out.println(":::::::::::::::::readDataExcel XLSX ::::::::::::");
    Vector vectorData = new Vector();
    try {
        FileInputStream fileInputStream = new FileInputStream(fileName);
        XSSFWorkbook xssfWorkBook = new XSSFWorkbook(fileInputStream);
        // Read data at sheet 0
        XSSFSheet xssfSheet = xssfWorkBook.getSheetAt(0);
        Iterator rowIteration = xssfSheet.rowIterator();
       	
    	int facilitatorFirstName_count=11;
    	int facilitatorLastName_count=11;
    	int facilitatorEmailAddress_count=11;
    	int schoolId_count=11;
    	int invitationEmailSent_count=11;
    	
        // Looping every row at sheet 0
        String indexCheck="";
        while (rowIteration.hasNext()) {
            XSSFRow xssfRow = (XSSFRow) rowIteration.next();
            Iterator cellIteration = xssfRow.cellIterator();
            Vector vectorCellEachRowData = new Vector();
			int cIndex=0; 
			boolean cellFlag=false;
			String indexPrev="";
            // Looping every cell in each row at sheet 0
			Map<Integer,String> mapCell = new TreeMap<Integer, String>();
            while (cellIteration.hasNext()){
                XSSFCell xssfCell = (XSSFCell) cellIteration.next();
                cIndex=xssfCell.getColumnIndex();
                
                   	
            	if(xssfCell.toString().equalsIgnoreCase("facilitatorFirstName")){
            		facilitatorFirstName_count=cIndex;
            		indexCheck+="||"+cIndex+"||";
            	}
            	if(facilitatorFirstName_count==cIndex){
            		cellFlag=true;
            	}
            	
            	
            	if(xssfCell.toString().equalsIgnoreCase("facilitatorLastName")){
            		facilitatorLastName_count=cIndex;
            		indexCheck+="||"+cIndex+"||";
            	}
            	if(facilitatorLastName_count==cIndex){
            		cellFlag=true;
            	}
            	
            	
            	if(xssfCell.toString().equalsIgnoreCase("facilitatorEmailAddress")){
            		facilitatorEmailAddress_count=cIndex;
            		indexCheck+="||"+cIndex+"||";
            	}
            	if(facilitatorEmailAddress_count==cIndex){
            		cellFlag=true;
            	}
            	
            	if(xssfCell.toString().equalsIgnoreCase("schoolId")){
            		schoolId_count=cIndex;
            		indexCheck+="||"+cIndex+"||";
            	}
            	if(schoolId_count==cIndex){
            		cellFlag=true;
            	}
            	
            	if(xssfCell.toString().equalsIgnoreCase("invitationEmailSent")){
            		invitationEmailSent_count=cIndex;
            		indexCheck+="||"+cIndex+"||";
            	}
            	if(invitationEmailSent_count==cIndex){
            		cellFlag=true;
            	}
             	
            	if(cellFlag){
            	
            			mapCell.put(Integer.valueOf(cIndex),xssfCell+"");
            		
            	}
            	  	cellFlag=false;
            }
            vectorCellEachRowData=cellValuePopulate(mapCell);
            vectorData.addElement(vectorCellEachRowData);
        }
    } catch (Exception ex) {
        ex.printStackTrace();
    }
     
    return vectorData;
}
public static Vector cellValuePopulate(Map<Integer,String> mapCell){
	 Vector vectorCellEachRowData = new Vector();
	 Map<Integer,String> mapCellTemp = new TreeMap<Integer, String>();
	 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false;
	 for(Map.Entry<Integer, String> entry : mapCell.entrySet()){
		int key=entry.getKey();
		String cellValue=null;
		if(entry.getValue()!=null)
			cellValue=entry.getValue().trim();
		
		if(key==0){
			mapCellTemp.put(key, cellValue);
			flag0=true;
		}
		if(key==1){
			flag1=true;
			mapCellTemp.put(key, cellValue);
		}
		if(key==2){
			flag2=true;
			mapCellTemp.put(key, cellValue);
		}
		if(key==3){
			flag3=true;
			mapCellTemp.put(key, cellValue);
		}
		if(key==4){
			flag4=true;
			mapCellTemp.put(key, cellValue);
		}
	 }
	 if(flag0==false){
		 mapCellTemp.put(0, "");
	 }
	 if(flag1==false){
		 mapCellTemp.put(1, "");
	 }
	 if(flag2==false){
		 mapCellTemp.put(2, "");
	 }
	 if(flag3==false){
		 mapCellTemp.put(3, "");
	 }
	 if(flag4==false){
		 mapCellTemp.put(4, "");
	 }
	 for(Map.Entry<Integer, String> entry : mapCellTemp.entrySet()){
		 vectorCellEachRowData.addElement(entry.getValue());
	 }
	 return vectorCellEachRowData;
}
public String saveFacilitator(int eventId,String facfirstName,String faclasttName,String facemailAddress)
{
	String data="success";
	boolean errorMsgForFacilitator=true;
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");	
	try
	{
	EventFacilitatorsList eventFacilitatorsList=new EventFacilitatorsList();
	Criterion crtemail=Restrictions.eq("emailAddress", facemailAddress);
	List<UserMaster> userMasterList=userMasterDAO.findByCriteria(crtemail);
	
	EventDetails eventDetails=eventDetailsDAO.findById(eventId, false, false);
	Criterion evncrt=Restrictions.eq("eventDetails",eventDetails);
	/*List<EventFacilitatorsList> eventFacilitatorsListForMapping=eventFacilitatorsListDAO.findByCriteria(evncrt);
	if(eventDetails.getEventFormatId().getEventFormatName().equalsIgnoreCase("One to One") || eventDetails.getEventFormatId().getEventFormatName().equalsIgnoreCase("One to Many")){
		if(eventFacilitatorsListForMapping!=null && eventFacilitatorsListForMapping.size()>0){
			data="Only one Facilitator can be added for this event.";
			errorMsgForFacilitator=false;
		}
	}*/
	//if(errorMsgForFacilitator)
	if(userMasterList.size()==0 && errorMsgForFacilitator)
	{
		UserMaster umr=new UserMaster();
		RoleMaster roleMaster= new RoleMaster();
		if(eventDetails.getHeadQuarterMaster()!=null){
			umr.setEntityType(6);
			roleMaster.setRoleId(11);
		}else{
			umr.setEntityType(2);
			roleMaster.setRoleId(9);
		}
	
		//umr.setDistrictId(districtMaster);
		int authorizationkey=(int) Math.round(Math.random() * 2000000);
		umr.setFirstName(facfirstName);
		umr.setLastName(facfirstName);				
		umr.setEmailAddress(facemailAddress);			
		umr.setPassword(MD5Encryption.toMD5("1"));	
		
		//RoleMaster roleMaster = roleMasterDAO.findById(9, false, false);	
		umr.setRoleId(roleMaster);
		umr.setAuthenticationCode(""+authorizationkey);
		umr.setVerificationCode(""+authorizationkey);
		umr.setStatus("I");
		umr.setForgetCounter(0);
		umr.setIsExternal(true);
		umr.setIsQuestCandidate(false);
		umr.setCreatedDateTime(new Date());							
		userMasterDAO.makePersistent(umr);		
	}
	else
	{
		UserMaster umr=	userMasterList.get(0);
		if(umr.getSchoolId()!=null)
		{
			eventFacilitatorsList.setSchoolId(umr.getSchoolId());	
		}	
	}		
	
	Criterion crtfacemail=Restrictions.eq("facilitatorEmailAddress", facemailAddress);

    List<EventFacilitatorsList> evntfactlist=eventFacilitatorsListDAO.findByCriteria(evncrt,crtfacemail);
	
    if(evntfactlist!=null &&  evntfactlist.size()>0){
    	 data=Utility.getLocaleValuePropByKey("msgEmailAddressAlreadyExists", locale);
   	     errorMsgForFacilitator=false;
    	/*if(eventDetails.getEventFormatId().getEventFormatName().equalsIgnoreCase("One to One") || eventDetails.getEventFormatId().getEventFormatName().equalsIgnoreCase("One to Many")){
			if(eventFacilitatorsListForMapping!=null && eventFacilitatorsListForMapping.size()==1){
				data="Only One Facilitator can be added for this event.";
				errorMsgForFacilitator=false;
			}
		}*/
		
		/*DistrictMaster districtMaste=eventDetails.getDistrictMaster();
		eventFacilitatorsList.setEventDetails(eventDetails);
		eventFacilitatorsList.setDistrictMaster(districtMaste);
		eventFacilitatorsList.setFacilitatorFirstName(facfirstName);
		eventFacilitatorsList.setFacilitatorLastName(faclasttName);
		eventFacilitatorsList.setFacilitatorEmailAddress(facemailAddress);
		eventFacilitatorsList.setStatus("A");
		eventFacilitatorsList.setCreatedBY(userSession);
		eventFacilitatorsList.setCreatedDateTime(new Date());
		eventFacilitatorsListDAO.makePersistent(eventFacilitatorsList);*/
	}	
	if(errorMsgForFacilitator){
		DistrictMaster districtMaste=eventDetails.getDistrictMaster();
		eventFacilitatorsList.setEventDetails(eventDetails);
		eventFacilitatorsList.setDistrictMaster(districtMaste);
		
		eventFacilitatorsList.setHeadQuarterMaster(eventDetails.getHeadQuarterMaster());
		
		eventFacilitatorsList.setFacilitatorFirstName(facfirstName);
		eventFacilitatorsList.setFacilitatorLastName(faclasttName);
		eventFacilitatorsList.setFacilitatorEmailAddress(facemailAddress);
		eventFacilitatorsList.setStatus("A");
		eventFacilitatorsList.setCreatedBY(userSession);
		eventFacilitatorsList.setCreatedDateTime(new Date());
		eventFacilitatorsListDAO.makePersistent(eventFacilitatorsList);
	}
	
}catch(Exception e)
{
System.out.println(e);	
}
return data;	
}

public String viewFacilitatorExcelFile(String fileName) {
	StringBuffer temptable=new StringBuffer();
	WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try {
			temptable.append("<table id='temptable'>");
			String root = request.getRealPath("/")+"/facilitatoruploads/";
	    	 String filePath=root+fileName;
	    	 System.out.println("filePath :"+filePath);
	    	 if(fileName.contains(".xlsx")){
	    		 vectorDataExcelXLSX = readDataExcelXLSX(filePath);
	    	 }else{
	    		 vectorDataExcelXLSX = readDataExcelXLS(filePath);
	    	 }
	    	   Vector vectorCellEachColumn = (Vector) vectorDataExcelXLSX.get(0);
	    	   temptable.append("<table id='facilitatorTable' border='0' class='table table-bordered table-striped' >");
	    	   temptable.append("<thead class='bg'>");
	    	   temptable.append("<tr>");
   		
	    	   for(int j=0;j<vectorCellEachColumn.size();j++) 
	              {
	    			temptable.append("<th>"+vectorCellEachColumn.get(j).toString().trim()+"</th>");
	    			
	              } 
	    	   temptable.append("</tr>");
	    	   temptable.append("</thead>");
	    	  for(int i=1; i<vectorDataExcelXLSX.size(); i++) {
	    		  temptable.append("<tr>");
	              Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
	              for(int j=0;j<vectorCellEachRowData.size();j++) 
	              {
	            	  temptable.append("<td>"+vectorCellEachRowData.get(j).toString().trim()+"</td>");  
	              } 
	              temptable.append("</tr>");
	 		 } 	 
	    	  temptable.append("</table>");
		}catch(Exception e)
		{
		e.printStackTrace();	
		}
		return temptable.toString();
		}
public String getTempFacilitatorList()
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	StringBuffer dmRecords =	new StringBuffer();
	
	try{
		Criterion 	criterion=Restrictions.eq("sessionId", session.getId())	;
		
		List<FacilitatorUploadTemp> facilitatorUploadTemp=facilitatorUploadTempDAO.findByCriteria(criterion);
		
        dmRecords.append("<table id='facilitatorTable' border='0' class='table table-bordered table-striped'>");
		dmRecords.append("<thead class='bg'>");
		dmRecords.append("<tr>");
		dmRecords.append("<th width='19%' valign='top'>"+Utility.getLocaleValuePropByKey("lblFname", locale)+"</th>");
		dmRecords.append("<th width='19%' valign='top'>"+Utility.getLocaleValuePropByKey("lblLname", locale)+"</th>");
		dmRecords.append("<th width='19%' valign='top'>"+Utility.getLocaleValuePropByKey("lblEmailAddress", locale)+"</th>");
		dmRecords.append("<th width='19%' valign='top'>"+Utility.getLocaleValuePropByKey("msgErrorText", locale)+"</th>");
		dmRecords.append("</tr>");
		dmRecords.append("</thead>");
		/*================= Checking If Record Not Found ======================*/
		if(facilitatorUploadTemp.size()==0)
			dmRecords.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNoNotefound1", locale)+"</td></tr>" );

		for (FacilitatorUploadTemp eventdtllist :facilitatorUploadTemp) {
			dmRecords.append("<tr>");
			String rowCss="";
			
			if(!eventdtllist.getErrorText().equalsIgnoreCase("")){
				rowCss="style=\"background-color:#FF0000;\"";
			}
			
			dmRecords.append("<td "+rowCss+">"+eventdtllist.getFirstName()+"</td>");
			dmRecords.append("<td "+rowCss+">"+eventdtllist.getLastName()+"</td>");
			dmRecords.append("<td "+rowCss+">"+eventdtllist.getEmailId()+"</td>");
			dmRecords.append("<td "+rowCss+">"+eventdtllist.getErrorText()+"</td>");
			dmRecords.append("</tr>");
		
		}
		dmRecords.append("</table>");
		} catch (Exception e){
	   	e.printStackTrace();
	     }
	return dmRecords.toString();			
}
public String saveFacilitatorByExcel(int eventId)
{
	System.out.println("inserting facilitator");
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");	
	
	try
	{
		System.out.println("eventId"+eventId);
		Criterion 	criterion=Restrictions.eq("sessionId", session.getId())	;
		Criterion 	criterion1=Restrictions.eq("errorText","");
		List<FacilitatorUploadTemp> facilitatorUploadTemp=facilitatorUploadTempDAO.findByCriteria(criterion,criterion1);
		EventDetails eventDetails=eventDetailsDAO.findById(eventId, false,false);
		
		//userwithAssociatedSchool
		List<String> emailIds =new  ArrayList<String>();
		Map<String,UserMaster> userwithAssociatedSchoolOrBranch =new HashMap<String, UserMaster>();
		for(FacilitatorUploadTemp facilitatorUpltmp : facilitatorUploadTemp){
			emailIds.add(facilitatorUpltmp.getEmailId());
		 }
		
		Criterion crtemail=Restrictions.in("emailAddress", emailIds);
		List<UserMaster> userMasterList=userMasterDAO.findByCriteria(crtemail);
		if(userMasterList!=null && userMasterList.size()>0){
			for( UserMaster um :userMasterList){
				if(um.getSchoolId()!=null)
					userwithAssociatedSchoolOrBranch.put(um.getEmailAddress(), um);
			}
		}
		int counter=0;
		for(FacilitatorUploadTemp facilitatorUpltmp : facilitatorUploadTemp)
		{
		/*Criterion crtemail=Restrictions.eq("emailAddress", facilitatorUpltmp.getEmailId());
		   List<UserMaster> userMasterList=userMasterDAO.findByCriteria(crtemail);*/
		
		EventFacilitatorsList eventFacilitatorsList=new EventFacilitatorsList();
		//System.out.println(userMasterList.size());
		if(userwithAssociatedSchoolOrBranch.get(facilitatorUpltmp.getEmailId())==null)
		{
			
			UserMaster umr=new UserMaster();
			RoleMaster roleMaster=new RoleMaster();
			if(eventDetails.getHeadQuarterMaster()!=null){
				umr.setEntityType(6);
				roleMaster.setRoleId(11);
			}else{
				umr.setEntityType(5);
				roleMaster.setRoleId(9);
			}
			
			umr.setFirstName(facilitatorUpltmp.getFirstName());
			umr.setLastName(facilitatorUpltmp.getLastName());
			umr.setEmailAddress(facilitatorUpltmp.getLastName());
			umr.setPassword("1");
			//RoleMaster roleMaster = roleMasterDAO.findById(9, false, false);	
			umr.setRoleId(roleMaster);
			int authorizationkey=(int) Math.round(Math.random() * 2000000);
			umr.setAuthenticationCode(""+authorizationkey);
			umr.setStatus("I");
			umr.setForgetCounter(0);
			umr.setIsQuestCandidate(false);
			userMasterDAO.makePersistent(umr);
		}
		else
		{
			
			eventFacilitatorsList.setSchoolId(userwithAssociatedSchoolOrBranch.get(facilitatorUpltmp.getEmailId()).getSchoolId());
			eventFacilitatorsList.setBranchMaster(userwithAssociatedSchoolOrBranch.get(facilitatorUpltmp.getEmailId()).getBranchMaster());
			/*UserMaster umr=	userMasterList.get(0);
			if(umr.getSchoolId()!=null)
			{
				eventFacilitatorsList.setSchoolId(umr.getSchoolId());	
			}*/	
		}	
			eventFacilitatorsList.setEventDetails(eventDetails);
			eventFacilitatorsList.setFacilitatorFirstName(facilitatorUpltmp.getFirstName());
			eventFacilitatorsList.setFacilitatorLastName(facilitatorUpltmp.getLastName());
			eventFacilitatorsList.setFacilitatorEmailAddress(facilitatorUpltmp.getEmailId());
			eventFacilitatorsList.setDistrictMaster(eventDetails.getDistrictMaster());
			
			eventFacilitatorsList.setHeadQuarterMaster(eventDetails.getHeadQuarterMaster());
			
			eventFacilitatorsList.setStatus("A");
			eventFacilitatorsList.setCreatedBY(userSession);
			eventFacilitatorsList.setCreatedDateTime(new Date());
			eventFacilitatorsListDAO.makePersistent(eventFacilitatorsList);
		}
		
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	return "success";
}

public String truncateFacilitatorTemp()
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	try{
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	Criterion crt=Restrictions.eq("sessionId", session.getId());
	List<FacilitatorUploadTemp> facilitatorUploadTemplist=facilitatorUploadTempDAO.findByCriteria(crt);
	for(FacilitatorUploadTemp facilitatorUploadTemp:facilitatorUploadTemplist)
	{
		facilitatorUploadTempDAO.makeTransient(facilitatorUploadTemp);
	}}catch(Exception e)
	{
		System.out.println(e);
	}
	
	
	return "success";
}


public List<UserMaster> getFieldOfUserList(int eventId,String seachTxt)
{
	/* ========  For Session time Out Error =========*/
	System.out.println(eventId+" @@@@@@@  "+seachTxt);
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	DistrictMaster districtMaster =null;
	HeadQuarterMaster headQuarterMaster=null;
	BranchMaster branchMaster=null;
	
	List<UserMaster> userMasterList = new ArrayList<UserMaster>(); 

	EventDetails eventDetails =eventDetailsDAO.findById(eventId, false,false);
	
	districtMaster=eventDetails.getDistrictMaster();
	headQuarterMaster=eventDetails.getHeadQuarterMaster();
	branchMaster =eventDetails.getBranchMaster();
	
	Criterion criterionDis =Restrictions.eq("districtId", districtMaster);
	
	Criterion criterionHQ=Restrictions.eq("headQuarterMaster",headQuarterMaster);
	Criterion  criterionBA=Restrictions.eq("branchMaster", branchMaster);
	
	List<UserMaster> fieldOfUserList1 = null;
	List<UserMaster> fieldOfUserList2 = null;
	
	try{
		
		if(seachTxt.trim()!=null && !seachTxt.trim().equals("")){
			Criterion crFirstName =null;
            Criterion crLstName =null;
            Criterion crEmail =null;
            
            Criterion crFirstName1 =null;
            Criterion crLstName1 =null;
            Criterion crEmail1 =null;
            Criterion finalCriteria =null;
            Criterion finalCriteria1 =null;
			crFirstName = Restrictions.like("firstName", seachTxt.trim(),MatchMode.ANYWHERE);
            crLstName = Restrictions.like("lastName", seachTxt.trim(),MatchMode.ANYWHERE);
            crEmail = Restrictions.like("emailAddress", seachTxt.trim(),MatchMode.ANYWHERE);
            finalCriteria =Restrictions.or(crFirstName, crLstName);
            
            crFirstName1 = Restrictions.ilike("firstName","% "+seachTxt.trim()+"%" );
            crLstName1 = Restrictions.like("lastName","% "+seachTxt.trim()+"%");
            crEmail1 = Restrictions.like("emailAddress","% "+seachTxt.trim()+"%");
            
            
            Criterion criterionZA=Restrictions.or(finalCriteria, crEmail);
            
            
            finalCriteria1 =Restrictions.or(crFirstName1, crLstName1);
            
            Criterion criterionZB=Restrictions.or(finalCriteria1, crEmail1);
            if(districtMaster!=null){
            	  fieldOfUserList1=userMasterDAO.findWithLimit(Order.asc("firstName"),0,25,criterionZA,criterionDis);
                  fieldOfUserList2=userMasterDAO.findWithLimit(Order.asc("firstName"),0,25,criterionZB,criterionDis);
            }else{
            	if(branchMaster!=null){
            		fieldOfUserList1=userMasterDAO.findWithLimit(Order.asc("firstName"),0,25,criterionZA,criterionBA,criterionHQ);
                    fieldOfUserList2=userMasterDAO.findWithLimit(Order.asc("firstName"),0,25,criterionZB,criterionBA,criterionHQ);
            	}else{
            		fieldOfUserList1=userMasterDAO.findWithLimit(Order.asc("firstName"),0,25,criterionZA,criterionHQ);
                    fieldOfUserList2=userMasterDAO.findWithLimit(Order.asc("firstName"),0,25,criterionZB,criterionHQ);
            	}
            }
          
            userMasterList.addAll(fieldOfUserList1);
            userMasterList.addAll(fieldOfUserList2);
            Set<UserMaster> setUser = new LinkedHashSet<UserMaster>(userMasterList);
            userMasterList = new ArrayList<UserMaster>(new LinkedHashSet<UserMaster>(setUser));
        }		
		
	
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}
	return userMasterList;
}


public String getFacilitatorGridNew(int eventId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	StringBuffer dmRecords =	new StringBuffer();
	
	try{
		
		
		List<EventFacilitatorsList> eventfacilitatorList	 =	null;
		EventDetails eventDetails=eventDetailsDAO.findById(eventId,false, false);
		Criterion crt=Restrictions.eq("eventDetails", eventDetails);
		
		Order order=Order.asc("facilitatorFirstName");
		eventfacilitatorList=eventFacilitatorsListDAO.findByCriteria(order,crt);
		int cnt = 1;
		
		if(eventfacilitatorList.size()==0)
			dmRecords.append("<div class='col-sm-3 col-md-3'>"+Utility.getLocaleValuePropByKey("msgNoFacilitatorFound", locale)+"</div>");
		if(eventfacilitatorList!=null &&eventfacilitatorList.size()>0)
		for (EventFacilitatorsList eventdtllist :eventfacilitatorList) {
			
			dmRecords.append("<div class='col-sm-4 col-md-4' style='margin-bottom:10px;'>"+eventdtllist.getFacilitatorFirstName()+" "+eventdtllist.getFacilitatorLastName()+"<br>("+eventdtllist.getFacilitatorEmailAddress()+")");
			
			dmRecords.append(" <a data-original-title='"+Utility.getLocaleValuePropByKey("lnkRemo", locale)+"' rel='tooltip' id='rmId"+cnt+"' href='javascript:void(0);' onclick=\"return deleteFacilitator("+eventdtllist.getEventFacilitatorId()+");\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a>");
				//dmRecords.append(" <a data-original-title='Remove' rel='tooltip' id='rmId"+cnt+"' href='javascript:void(0);' onclick=\"return deleteFacilitator("+eventdtllist.getEventFacilitatorId()+");\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a>");
			
			dmRecords.append("</div>");
			
			/*dmRecords.append("<tr>");
			dmRecords.append("<td>"+eventdtllist.getFacilitatorFirstName()+"</td>");
			dmRecords.append("<td>"+eventdtllist.getFacilitatorLastName()+"</td>");
			dmRecords.append("<td>"+eventdtllist.getFacilitatorEmailAddress()+"</td>");
			if(eventdtllist.getSchoolId()!=null){
			dmRecords.append("<td>"+eventdtllist.getSchoolId().getSchoolName()+"</td>");
			}
			dmRecords.append("<td>");
			dmRecords.append("<a href='javascript:void(0);' onclick='return deleteFacilitator("+eventdtllist.getEventFacilitatorId()+")'>Remove</a>");
			dmRecords.append("</td>");
			*/
			cnt++;	
		}
		dmRecords.append("####"+cnt);
    	} catch (Exception e) {
	   	e.printStackTrace();
	     }
	return dmRecords.toString();		
}


}