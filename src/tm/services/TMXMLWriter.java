package tm.services;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.utility.Utility;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class TMXMLWriter {

	public Document createNormalCurveDocument(List lstdata,int norscore,TeacherDetail teacherDetail,List<TeacherNormScore> tnsList)
	{
		Document doc = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			doc = builder.newDocument();

			//Map<Integer,Integer> map = new HashMap<Integer, Integer>();
			String teacherdecile = "";
			Integer frequency = 0;
			Map<String,Integer> map = new HashMap<String, Integer>();
			for(Object oo: lstdata){
				Object obj[] = (Object[])oo;
				System.out.println(obj[0] + "  " + obj[1]+" "+obj[2]);
				/*Integer ii =(int)Double.parseDouble(String.valueOf(obj[2]));
				map.put(ii, Integer.parseInt(String.valueOf(obj[1])));*/
				map.put(String.valueOf(obj[2]), Integer.parseInt(String.valueOf(obj[1])));
				/*if(String.valueOf(obj[3]).equals("1"))
				{
					teacherdecile = String.valueOf(obj[2]);
					frequency = Integer.parseInt(String.valueOf(obj[1]));
				}*/
			}
			//System.out.println(frequency+" teacherdecile: "+teacherdecile);
			Element results = doc.createElement("chart");
			results.setAttribute("canvasbgColor","FFFFFFF");
			results.setAttribute("canvasbgAlpha","90");
			results.setAttribute("canvasBgAngle", "0");
			results.setAttribute("canvasBgAlpha", "50");
			results.setAttribute("showValues", "0");
			results.setAttribute("bgColor", "999999,FFFFFF");
			results.setAttribute("bgAlpha", "50");
			results.setAttribute("showBorder", "0");
			//labelDisplay='NONE'
			results.setAttribute("labelDisplay", "NONE");

			Element categories = doc.createElement("categories");

			Element category = null;
			for(int i=0;i<=100;i++)
			{
				category = doc.createElement("category");
				category.setAttribute("label",""+i);
				if(!(i==11 || i==22 || i==33 || i==44 || i==55 || i==66 || i==77 || i==88 || i==100))
					category.setAttribute("showLabel","0");
				categories.appendChild(category);
			}

			results.appendChild(categories);

			Element dataset = doc.createElement("dataset");
			dataset.setAttribute("seriesName",teacherDetail.getFirstName()+" "+teacherDetail.getLastName());
			Element set = null;
			int j=2;
			String decileColor = null;
			Integer mapVal = null;
			/* Norm Score wise

			for(int i=0;i<=100;i++)
			{
				set = doc.createElement("set");
				mapVal = map.get(i);
				if(mapVal!=null)
					set.setAttribute("value",""+i);
				else
					set.setAttribute("value","");

				decileColor=Utility.getValueOfPropByKey("decile"+j);
				if((i==11 || i==22 || i==33 || i==44 || i==55 || i==66 || i==77 || i==88 || i==100))
					j++;

				set.setAttribute("color",decileColor);
				dataset.appendChild(set);
			}*/

			for(int i=0;i<=100;i++)
			{
				decileColor=Utility.getValueOfPropByKey("decile"+j);
				set = doc.createElement("set");
				mapVal = map.get(decileColor);
				if(mapVal!=null)
					set.setAttribute("value",""+mapVal);
				else
					set.setAttribute("value","");


				if((i==11 || i==22 || i==33 || i==44 || i==55 || i==66 || i==77 || i==88 || i==100))
					j++;

				set.setAttribute("color",decileColor);
				dataset.appendChild(set);
			}
			results.appendChild(dataset);

			Element dataset1 = doc.createElement("dataset");
			dataset1.setAttribute("seriesName","Norm Score: "+norscore);
			dataset1.setAttribute("showValues","1");
			dataset1.setAttribute("drawAnchors","1");
			dataset1.setAttribute("anchorRadius","5");
			dataset1.setAttribute("anchorSides","4");
			dataset1.setAttribute("anchorBorderColor","FF0000");
			dataset1.setAttribute("anchorAlpha","100");
			dataset1.setAttribute("alpha","0.1");
			dataset1.setAttribute("plotBorderAlpha","0");

			Element set1 = null;
			int k=2;boolean flag=true;
			/*for(int i=0;i<=100;i++)
			{
				decileColor=Utility.getValueOfPropByKey("decile"+k);
				set1 = doc.createElement("set");
				if(decileColor.equalsIgnoreCase(teacherdecile) && flag)
					{
						set1.setAttribute("value",""+frequency);
						flag=false;
					}
				else
					set1.setAttribute("value","");

				if((i==11 || i==22 || i==33 || i==44 || i==55 || i==66 || i==77 || i==88 || i==100))
					k++;

				dataset1.appendChild(set1);
			}*/
			int percentile = 0 ;
			if(tnsList.size()>0)
			{
				percentile = (int)Math.round(tnsList.get(0).getPercentile());
				frequency = map.get(tnsList.get(0).getDecileColor());
			}

			for(int i=0;i<=100;i++)
			{
				set1 = doc.createElement("set");
				if(percentile==i)
				{
					set1.setAttribute("value",""+frequency);
					flag=false;
				}
				else
					set1.setAttribute("value","");

				dataset1.appendChild(set1);
			}
			results.appendChild(dataset1);

			doc.appendChild(results);
			System.out.println(doc.getTextContent());

		} catch (DOMException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		return doc;
	}
	
	public Document createNormalDistributionDocument(List lstdata,List percentileList)
	{
		Document doc = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			doc = builder.newDocument();

			Map<Integer,Integer> percentileMap = new HashMap<Integer, Integer>();
			Integer frequency = 0;
			Map<String,Integer> map = new HashMap<String, Integer>();
			for(Object oo: lstdata){
				Object obj[] = (Object[])oo;
				//System.out.println(obj[0] + "  " + obj[1]+" "+obj[2]);
				map.put(String.valueOf(obj[2]), Integer.parseInt(String.valueOf(obj[1])));
			}
			
			for(Object oo: percentileList){
				Object obj[] = (Object[])oo;
				//System.out.println(obj[0] + "  " + obj[1]+" "+obj[2]);
				Integer ii =(int)Double.parseDouble(String.valueOf(obj[3]));
				percentileMap.put(ii, Integer.parseInt(String.valueOf(obj[1])));
			}
			
			Element results = doc.createElement("chart");
			results.setAttribute("canvasbgColor","FFFFFFF");
			results.setAttribute("canvasbgAlpha","90");
			results.setAttribute("canvasBgAngle", "0");
			results.setAttribute("canvasBgAlpha", "50");
			results.setAttribute("showValues", "0");
			results.setAttribute("bgColor", "999999,FFFFFF");
			results.setAttribute("bgAlpha", "50");
			results.setAttribute("showBorder", "0");
			results.setAttribute("labelDisplay", "NONE");

			Element categories = doc.createElement("categories");

			Element category = null;
			for(int i=0;i<=100;i++)
			{
				category = doc.createElement("category");
				category.setAttribute("label",""+i);
				if(!(i==11 || i==22 || i==33 || i==44 || i==55 || i==66 || i==77 || i==88 || i==100))
					category.setAttribute("showLabel","0");
				categories.appendChild(category);
			}

			results.appendChild(categories);
			Element dataset = doc.createElement("dataset");
			dataset.setAttribute("seriesName","National Norm");
			Element set = null;
			int j=2;
			String decileColor = null;
			Integer mapVal = null;
			
			for(int i=0;i<=100;i++)
			{
				decileColor=Utility.getValueOfPropByKey("decile"+j);
				set = doc.createElement("set");
				mapVal = map.get(decileColor);
				if(mapVal!=null && mapVal!=0)
					set.setAttribute("value",""+mapVal);
				else
					set.setAttribute("value","");


				if((i==11 || i==22 || i==33 || i==44 || i==55 || i==66 || i==77 || i==88 || i==100))
					j++;

				set.setAttribute("color",decileColor);
				dataset.appendChild(set);
			}
			results.appendChild(dataset);

			Element dataset1 = doc.createElement("dataset");
			dataset1.setAttribute("seriesName","Candidates");
			dataset1.setAttribute("showValues","1");
			dataset1.setAttribute("drawAnchors","1");
			dataset1.setAttribute("anchorRadius","5");
			dataset1.setAttribute("anchorSides","4");
			dataset1.setAttribute("anchorBorderColor","FF0000");
			dataset1.setAttribute("anchorAlpha","100");
			dataset1.setAttribute("alpha","0.1");
			dataset1.setAttribute("plotBorderAlpha","0");

			Element set1 = null;
			
			//System.out.println("percentileMap.size(): "+percentileMap.size());
			for(int i=0;i<=100;i++)
			{
				set1 = doc.createElement("set");
				percentileMap.get(i);
				frequency = percentileMap.get(i);
				//System.out.println(i+" frequency: "+frequency);
				if(frequency!=null)
				{
					set1.setAttribute("value",""+frequency);
				}
				else
					set1.setAttribute("value","");

				dataset1.appendChild(set1);
			}
			results.appendChild(dataset1);

			doc.appendChild(results);
			//System.out.println(doc.getTextContent());

		} catch (DOMException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		return doc;
	}

	public String serialize(Document doc) {
		try {
			StringWriter writer = new StringWriter();
			OutputFormat format = new OutputFormat();
			format.setIndenting(true);

			XMLSerializer serializer = new XMLSerializer(writer, format);
			serializer.serialize(doc);

			serializer = null;
			format = null;
			writer.flush();
			String result =writer.getBuffer().toString();
			writer.close();
			writer = null;
			System.gc();

			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
