package tm.services.hqbranches;

/* @Author: Sekhar 
 * @Discription: It is used to Teacher Upload
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.JmsException;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.LinkToKsnTalentsTemp;
import tm.bean.hqbranchesmaster.StatusNodeColorHistory;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.mq.MQEvent;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.MailToCandidateOnImportDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.hqbranchesmaster.LinkToKsnTalentsTempDAO;
import tm.dao.hqbranchesmaster.StatusNodeColorHistoryDAO;
import tm.dao.master.ExclusivePeriodMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.mq.MQEventDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.CommonService;
import tm.services.EmailerService;
import tm.services.PaginationAndSorting;
import tm.services.district.ManageStatusAjax;
import tm.utility.Utility;

public class LinkToKsnTalentsTempAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	private static final Logger logger = Logger.getLogger(LinkToKsnTalentsTempAjax.class.getName());
	//****************************************
	@Autowired
	private StatusNodeColorHistoryDAO statusNodeColorHistoryDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO)
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	@Autowired
	private LinkToKsnTalentsTempDAO linkToKsnTalentsTempDAO;
	public void setLinkToKsnTalentsTempDAO(LinkToKsnTalentsTempDAO linkToKsnTalentsTempDAO)
	{
		this.linkToKsnTalentsTempDAO = linkToKsnTalentsTempDAO;
	}
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) 
	{
		this.userMasterDAO = userMasterDAO;
	}
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) {
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}

	@Autowired
	private MailToCandidateOnImportDAO mailToCandidateOnImportDAO;
	public void setMailToCandidateOnImportDAO(
			MailToCandidateOnImportDAO mailToCandidateOnImportDAO) {
		this.mailToCandidateOnImportDAO = mailToCandidateOnImportDAO;
	}
	
	@Autowired
	private ExclusivePeriodMasterDAO exclusivePeriodMasterDAO;
	public void setExclusivePeriodMasterDAO(ExclusivePeriodMasterDAO exclusivePeriodMasterDAO)
	{
		this.exclusivePeriodMasterDAO = exclusivePeriodMasterDAO;
	}
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private MQEventDAO mqEventDAO;
	@Autowired
	private ManageStatusAjax manageStatmusAjax;
	
	private Vector vectorDataExcelXLSX = new Vector();
	public String displayTempTalentRecords(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		
		System.out.println("::::::::::::::displayTempTalentRecords::::::::::::::");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			System.out.println("start"+start);
			System.out.println("end"+end);
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			/***** Sorting by  Stating ( Sekhar ) ******/
			List<LinkToKsnTalentsTemp> linkToKsnTalentsTemplst	  =	null;
			
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"talenttempid";
	
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null))
				 {
					 sortOrderFieldName		=	sortOrder;
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion = Restrictions.eq("sessionid",session.getId());
			totalRecord = linkToKsnTalentsTempDAO.getRowCountTempTalent(criterion);
			linkToKsnTalentsTemplst = linkToKsnTalentsTempDAO.findByTempTalent(sortOrderStrVal,start,noOfRowInPage,criterion);
			
			
			/***** Sorting by Temp Talent  End ******/
	
			dmRecords.append("<table  id='tempTalentTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			
			
			responseText=PaginationAndSorting.responseSortingLink("Talent First Name",sortOrderFieldName,"talent_first_name",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Talent Last Name",sortOrderFieldName,"talent_last_name",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Talent Email",sortOrderFieldName,"talent_email",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Job Id",sortOrderFieldName,"jobId",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			
			responseText=PaginationAndSorting.responseSortingLink("KSN ID",sortOrderFieldName,"ksnId",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("TM KSN ID",sortOrderFieldName,"tmksnId",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Created DateTime",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Errors",sortOrderFieldName,"errortext",sortOrderTypeVal,pgNo);
			dmRecords.append("<th  valign='top'>"+responseText+"</th>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(linkToKsnTalentsTemplst.size()==0)
				dmRecords.append("<tr><td colspan='6' align='center'>"+Utility.getLocaleValuePropByKey("lblNoCandidate", locale)+"</td></tr>" );
			System.out.println(Utility.getLocaleValuePropByKey("msgNoOfRecords", locale)+linkToKsnTalentsTemplst.size());

			for (LinkToKsnTalentsTemp linkToKsnTalentsTemp : linkToKsnTalentsTemplst){
				String jobId=linkToKsnTalentsTemp.getJobId();
				String fName=linkToKsnTalentsTemp.getTalent_first_name();
				String lName=linkToKsnTalentsTemp.getTalent_last_name();
				String email=linkToKsnTalentsTemp.getTalent_email();
				String rowCss="";
				
				if(!linkToKsnTalentsTemp.getErrortext().equalsIgnoreCase("")){
					rowCss="style=\"background-color:#FF0000;\"";
				}
				
				dmRecords.append("<tr >" );
				dmRecords.append("<td "+rowCss+">"+fName+"</td>");
				dmRecords.append("<td "+rowCss+">"+lName+"</td>");
				dmRecords.append("<td "+rowCss+">"+email+"</td>");
				dmRecords.append("<td "+rowCss+">"+jobId+"</td>");
				dmRecords.append("<td "+rowCss+">"+linkToKsnTalentsTemp.getKsnId()+"</td>");
				dmRecords.append("<td "+rowCss+">"+linkToKsnTalentsTemp.getTmksnId()+"</td>");
				dmRecords.append("<td "+rowCss+">"+linkToKsnTalentsTemp.getCreatedDateTime()+"</td>");
				dmRecords.append("<td "+rowCss+">"+linkToKsnTalentsTemp.getErrortext()+"</td>");
				dmRecords.append("</tr>");
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}

	public int saveTalent()
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		System.out.println("::::::::saveTalent now:::::::::");
		int returnVal=0;
        try{
        	Criterion criterion1 = Restrictions.eq("sessionid",session.getId());
        	Criterion criterion2 = Restrictions.eq("errortext","");
        	List <LinkToKsnTalentsTemp> linkToKsnTalentsTemplst=linkToKsnTalentsTempDAO.findByCriteria(criterion1,criterion2);
        	
        	//****************** Adding By Deepak    ******************************* 	
        	System.out.println("linkToKsnTalentsTemplst       ::::::::: "+linkToKsnTalentsTemplst.size());
        	Integer checkflag=linkToKsnTalentsTemplst.size();
        	 	if(checkflag==0)
        	 	{
        	 		returnVal=1;
        	 	}
        	 	else
        	 	{
        	 //******************   End    ******************************************
        	 List emails = new ArrayList();
        	 List jobs = new ArrayList();
        	 List<String> inviteTempjobList = new ArrayList<String>();
        	 List<JobOrder> listJobOrders = new ArrayList<JobOrder>(); 
        	 Map<String,JobOrder> mapAllJobs = new HashMap<String, JobOrder>();
        	 Map<String,String> ksnByTalentID=new HashMap<String,String>();
        	 for(int e=0; e<linkToKsnTalentsTemplst.size(); e++) {
	            		emails.add(linkToKsnTalentsTemplst.get(e).getTalent_email());
	            		jobs.add(linkToKsnTalentsTemplst.get(e).getJobId());
	            		String jobId=linkToKsnTalentsTemplst.get(e).getJobId();
	            		if(jobId!=null && jobId.length()>0)
	            		inviteTempjobList.add(jobId);
	            		ksnByTalentID.put(linkToKsnTalentsTemplst.get(e).getTalentId(), linkToKsnTalentsTemplst.get(e).getKsnId());
			 }
        	 if(inviteTempjobList!=null && inviteTempjobList.size()>0){
        		 listJobOrders=jobOrderDAO.findJobOrderListByApiJobIdList(userMaster.getDistrictId(),inviteTempjobList);
        	 }
        	 if(listJobOrders!=null && listJobOrders.size()>0){
        		 for(JobOrder jobOrder :listJobOrders){
        			 mapAllJobs.put(jobOrder.getJobId()+"", jobOrder);
        		 }
        	 }
        	 System.out.println("emails:::::::::::::::::::::::::::::::::::::::::"+emails.size());
        	 List <TeacherDetail> linkToKsnTalentsTempList=teacherDetailDAO.findAllTeacherDetails(emails);
        	 System.out.println("linkToKsnTalentsTempList:::::::::::::::::::::::::::::::::::::::::"+linkToKsnTalentsTempList.size());
        	 List teacherIdList = new ArrayList();
	         for(int t=0; t<linkToKsnTalentsTempList.size(); t++) {
	        	 teacherIdList.add(linkToKsnTalentsTempList.get(t));
			 }
	         System.out.println("teacherIdList::::::::::::"+teacherIdList.size());
	         
	         
				Map<String,JobCategoryMaster> mapForjobcat=new HashMap<String, JobCategoryMaster>();
				Map<Integer,JobForTeacher> jftbtTalentID=new HashMap<Integer, JobForTeacher>();
				Map<Integer,SecondaryStatus> secStatusMap=new HashMap<Integer, SecondaryStatus>();
				List<TeacherDetail> finalTeachers=new ArrayList<TeacherDetail>();
				List<JobCategoryMaster> alljobCategories = new ArrayList<JobCategoryMaster>(); // list for all parent jobcategories from jobforteachers
				List<JobForTeacher> jftByTalentId=jobForTeacherDAO.getjftByTalentId(teacherIdList);
				
				System.out.println("jftByTalentId:::::::::::"+jftByTalentId.size());
				
				if(jftByTalentId!=null&&jftByTalentId.size()>0){
					for(JobForTeacher jft:jftByTalentId){
						if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null)
							alljobCategories.add(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId());
						    mapForjobcat.put(jft.getTeacherId().getTeacherId()+"##"+jft.getJobId().getJobId(), jft.getJobId().getJobCategoryMaster().getParentJobCategoryId());
					}
				}
				System.out.println("alljobCategories:::::::::::"+alljobCategories.size());
				   List<SecondaryStatus> secondaryStatuses =null;
				   if(alljobCategories!=null && alljobCategories.size()>0)
					   secondaryStatuses = secondaryStatusDAO.findByNameAndJobCats_OP(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale),alljobCategories);
				   
				   
				   System.out.println("secondaryStatuses::::::::"+secondaryStatuses.size());
				   
				   if(secondaryStatuses!=null && secondaryStatuses.size()>0){
					   for(SecondaryStatus sec : secondaryStatuses){
						   if(sec.getJobCategoryMaster()!=null && sec.getStatusMaster()==null){
							   secStatusMap.put(sec.getJobCategoryMaster().getJobCategoryId(), sec);// for secondry status
						   }
						   if(sec.getJobCategoryMaster()!=null && sec.getStatusMaster()!=null){
							   secStatusMap.put(sec.getJobCategoryMaster().getJobCategoryId(), sec); // for statusmaster...
						   }
					   }
				   }//end of if
				   StatusMaster statusMaster=null;
				   SecondaryStatus secondryStatus=null;
				   JobForTeacher finaljftobj=null;
				 for(JobForTeacher jftobj:jftByTalentId){					 
					  if(jftobj.getJobId().getHeadQuarterMaster()!=null && jftobj.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
						  jftbtTalentID.put(jftobj.getTeacherId().getTeacherId(), jftobj);
						   if(secStatusMap.get(jftobj.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId())!=null){							  
							   secondryStatus = secStatusMap.get(jftobj.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
							   if(secondryStatus.getStatusMaster()==null && secondryStatus.getSecondaryStatus()!=null)
								   secondryStatus = secStatusMap.get(jftobj.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
							   else
								   statusMaster = secondryStatus.getStatusMaster();							   
						   }
					   }//end main if					 
				 } 
				 
				 ///////////////////////////////////////////////////////////////////////////
					SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor)mqEventDAO.getSessionFactory();
					ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
					Connection connection = connectionProvider.getConnection();
					ApplicationContext APPLICATIONCONTEXT = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
					
					Map<TeacherDetail,List<StatusNodeColorHistory>> teacherListByTeacherObj=new HashMap<TeacherDetail,List<StatusNodeColorHistory>>();
		    		List<StatusNodeColorHistory> list =statusNodeColorHistoryDAO.getStatusColorHistoryByTeacherList(teacherIdList);
		    		for(StatusNodeColorHistory snch:list){
		    			if(teacherListByTeacherObj.get(snch.getTeacherDetail())!=null){
		    				List<StatusNodeColorHistory> snchListForTeacher=teacherListByTeacherObj.get(snch.getTeacherDetail());
		    				snchListForTeacher.add(snch);
		    				teacherListByTeacherObj.put(snch.getTeacherDetail(), snchListForTeacher);
		    			}else{
		    				List<StatusNodeColorHistory> snchListForTeacher=new ArrayList<StatusNodeColorHistory>();
		    				snchListForTeacher.add(snch);
		    				teacherListByTeacherObj.put(snch.getTeacherDetail(), snchListForTeacher);
		    			}
		    		}
		    		Map<Integer,MQEvent> mQEventMAP=new HashMap<Integer,MQEvent>();
		    		String LinkToKsn = Utility.getLocaleValuePropByKey("lblLinkToKsn", locale);
					List<MQEvent> mqEventList=mqEventDAO.findMQEventByTeacherList_Op(teacherIdList,LinkToKsn);
					for (MQEvent mqEvent : mqEventList) {
						mQEventMAP.put(mqEvent.getTeacherdetail().getTeacherId(), mqEvent);
					}
					
					System.out.println("jftbtTalentID:::::::::::::::::>>>"+jftbtTalentID.size());
				 ///////////////////////////////////////////////////////////////////////////
        	for(LinkToKsnTalentsTemp linkToKsnTalentsTemp : linkToKsnTalentsTemplst){
        		 finaljftobj=jftbtTalentID.get(Integer.parseInt(linkToKsnTalentsTemp.getTalentId()));
        		String jobId = linkToKsnTalentsTemp.getJobId();	
	        	System.out.println("teacherId::>>>>:::"+linkToKsnTalentsTemp.getTalentId());
	        	System.out.println("jobId:::::"+jobId);
	        	System.out.println(" "+ statusMaster+" "+secondryStatus+" "+finaljftobj+" "+userMaster+" "+""+" ");
	        	System.out.println(ksnByTalentID.get(linkToKsnTalentsTemp.getTalentId()));
	        	try {
	        		manageStatmusAjax.messageQueueForOnboardingBulk(true,statusMaster,secondryStatus,finaljftobj,userMaster,"",ksnByTalentID.get(linkToKsnTalentsTemp.getTalentId()),connection,teacherListByTeacherObj,mQEventMAP,request.getRemoteAddr(),APPLICATIONCONTEXT);
				} catch (Exception e) {
					e.printStackTrace();
				} 
        	}
        	
          	try{
	        	deleteXlsAndXlsxFile(request,session.getId());
	        	linkToKsnTalentsTempDAO.deleteAllTalentsTemp(session.getId());
	       	}catch(Exception e){
	       		e.printStackTrace();
	       	}
	       	
         }                                                       //new Add for check flag
		}catch (Exception e){
			System.out.println("Error >"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			return returnVal;
		}
     
	}
	
	public void deleteXlsAndXlsxFile(HttpServletRequest request,String sessionId){
		List<LinkToKsnTalentsTemp> linkToKsnTalentsTemplst= linkToKsnTalentsTempDAO.findByAllTempTalent();
		String root = request.getRealPath("/")+"/uploads/";
		for(LinkToKsnTalentsTemp linkToKsnTalentsTemp : linkToKsnTalentsTemplst){
			String filePath="";
			File file=null;
			try{
				filePath=root+linkToKsnTalentsTemp.getSessionid()+".xlsx";
				file = new File(filePath);
				file.delete();
			}catch(Exception e){}
			try{
				filePath=root+linkToKsnTalentsTemp.getSessionid()+".xls";
				file = new File(filePath);
				file.delete();
			}catch(Exception e){}
		}
		
		String filePath="";
		File file=null;
		try{
			filePath=root+sessionId+".xlsx";
			file = new File(filePath);
			file.delete();
		}catch(Exception e){}
		try{
			filePath=root+sessionId+".xls";
			file = new File(filePath);
			file.delete();
		}catch(Exception e){}
	}
	public int deleteTempTalent(String sessionId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		System.out.println(":::::::::::::::::deleteTempTalent:::::::::::::::::"+sessionId);
		try{
			linkToKsnTalentsTempDAO.deleteTalentTemp(sessionId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
		
	public String saveTalentTemp(String fileName,String sessionId,Integer headQuarterId,Integer branchId)
	{
		/* ========  For Session time Out Error =========*/
		boolean inviteflag=false,applyJobFlag=false;
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		System.out.println("::::::::saveTeacherTemp:::::::::");
		session.setAttribute("Hid",headQuarterId);
		session.setAttribute("Bid",branchId);
		System.out.println("headQuarterId    :::"+headQuarterId+", branchId    ::::"+branchId+",   ::::::::::::::");
		String returnVal="";
        try{
        	 String root = request.getRealPath("/")+"/uploads/";
        	 String filePath=root+fileName;
        	 System.out.println("filePath :"+filePath);
        	 if(fileName.contains(".xlsx")){
        		 vectorDataExcelXLSX = readDataExcelXLSX(filePath);
        	 }else{
        		 vectorDataExcelXLSX = readDataExcelXLS(filePath);
        	 }
        	 System.out.println("::::::::get value of XLSX and XLS:::::::::"+vectorDataExcelXLSX.size());
			 int createdDateTime_count=11;
			 int jobId_count=11;
			 int talent_first_name_count=11;
			 int talent_last_name_count=11;
			 int talent_email_count=11;
			 int ksnId_count=11;
			 int tmksnId_count=11;
			 			 
			 UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");
			 List<UserMaster> userMasterlst = userMasterDAO.findByAllUserMaster();
			 List talentList = new ArrayList();
			 List<String> talentIdList=new ArrayList<String>();
			 int talentId_count1=11;
			 int talentId_count=11;
			 Map<String,String> emailExistCheck = new HashMap<String, String>();
			 SessionFactory sessionFactory=linkToKsnTalentsTempDAO.getSessionFactory();
			 StatelessSession sessionHiber = sessionFactory.openStatelessSession();
			
        	 Transaction txOpen =sessionHiber.beginTransaction();
			 for(int em=0; em<vectorDataExcelXLSX.size(); em++) {
	            Vector vectorCellEachRowDataGetEmail = (Vector) vectorDataExcelXLSX.get(em);
	            String talentId="",jobid="";
	            for(int e=0; e<vectorCellEachRowDataGetEmail.size(); e++) {
	            	if(vectorCellEachRowDataGetEmail.get(e).toString().equalsIgnoreCase("talentId")){
	            		talentId_count1=e;
	            		System.out.println("------------------------------------------------");
	            	}
	            	if(talentId_count1==e)
	            		talentId=vectorCellEachRowDataGetEmail.get(e).toString().trim();
	            }
	            if(em!=0){
	            	System.out.println("talentId::::::::::::"+talentId);
	            	try{
	            		if(talentId!=null && !talentId.equals(""))
	            			talentList.add(Integer.parseInt(talentId));
	            	}catch(Exception e){
	            		e.printStackTrace();
	            	}
	            	talentIdList.add(talentId);
	            }
			 }
			 
			 String tempdata[]=new String[vectorDataExcelXLSX.size()];
			 int jobid_count=1;
			 for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
				 
	            Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
        	 	if(vectorCellEachRowData.get(0).toString().equalsIgnoreCase("jobId")){
            		jobid_count=0;
            	}
            	if(jobid_count==0)
            	{
            		tempdata[i]=vectorCellEachRowData.get(0).toString().trim();
	            }
			   } 
			 
			List <TeacherDetail> linkToKsnTalentsTempList=new ArrayList<TeacherDetail>();
			List <LinkToKsnTalentsTemp> talenttemplst=new ArrayList<LinkToKsnTalentsTemp>();
			if(talentList.size()>0){
				linkToKsnTalentsTempList=teacherDetailDAO.findIcompTeacherDetails(talentList);
				if(linkToKsnTalentsTempList==null){
					linkToKsnTalentsTempList=new ArrayList<TeacherDetail>();
				}
				talenttemplst=linkToKsnTalentsTempDAO.findByAllTempTalentId(talentIdList);
			}
			System.out.println("talentList::::::::::::::"+talentList.size());
			System.out.println("linkToKsnTalentsTempList::::::::::"+linkToKsnTalentsTempList.size());
			System.out.println("::::::::Start Inserting::::::talenttemplst====:::"+talenttemplst.size());
			
			System.out.println("vectorDataExcelXLSX.size():::::::::::"+vectorDataExcelXLSX.size());
			LinkToKsnTalentsTemp linkToKsnTalentsTemp= new LinkToKsnTalentsTemp() ;
			for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
	             Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
	             System.out.println("vectorCellEachRowData:::::::::::::::::::"+vectorCellEachRowData.size());
	             String createdDateTime="";
				 String jobId="";
				 String talent_first_name="";
				 String talent_last_name="";
				 String talent_email="";
				 String ksnId="";
				 String tmksnId="";
				 String talentId="";
	            for(int j=0; j<vectorCellEachRowData.size(); j++) {
	            	
	            	System.out.println("vectorCellEachRowData.get(j).toString()::::::::::::"+vectorCellEachRowData.get(j).toString());
	            	
	            	try{
	            		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("talentId")){
		            		talentId_count=j;
		            	}
		            	if(talentId_count==j)
		            		talentId=vectorCellEachRowData.get(j).toString().trim();
		            	
	            		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("talent_first_name")){
		            		talent_first_name_count=j;
		            	}
		            	if(talent_first_name_count==j)
		            		 talent_first_name=vectorCellEachRowData.get(j).toString().trim();
		            		
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("talent_last_name")){
		            		talent_last_name_count=j;
		            	}
		            	if(talent_last_name_count==j)
		            		talent_last_name=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("talent_email")){
		            		talent_email_count=j;
		            	}
		            	if(talent_email_count==j)
		            		talent_email=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("jobId")){
		            		jobId_count=j;
		            	}
		            	if(jobId_count==j)
		            		jobId=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("ksnId")){
		            		ksnId_count=j;
		            	}
		            	if(ksnId_count==j)
		            		ksnId=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("tmksnId")){
		            		tmksnId_count=j;
		            	}
		            	if(tmksnId_count==j)
		            		tmksnId=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("createdDateTime")){
		            		createdDateTime_count=j;
		            	}
		            	if(createdDateTime_count==j)
		            		createdDateTime=vectorCellEachRowData.get(j).toString().trim();
		            
	            	}catch(Exception e){}
	            	
	            }
	            System.out.println("ksnId::::::::::::::::::::::::::::::::::::::::::::::::"+ksnId);
	            if(i!=0){
	            	boolean jobFlag=false,multiErrorFlag=false;
		             if(returnVal.equals("1") && (!jobId.equalsIgnoreCase("") || !talent_first_name.equalsIgnoreCase("") || !talent_last_name.equalsIgnoreCase("") || !talent_email.equalsIgnoreCase("")|| !ksnId.equalsIgnoreCase("")|| !createdDateTime.equalsIgnoreCase(""))){
	            	 	linkToKsnTalentsTemp= new LinkToKsnTalentsTemp() ;
	            	 	try{
	            	 		linkToKsnTalentsTemp.setCreatedDateTime(createdDateTime);
	            	 	}catch(Exception e){}
	            	 	String errorText=""; boolean errorFlag=false;
	            	 	
	            	 	if(talentId.equalsIgnoreCase("")||talentId.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="talentId is not available.";
	            	 		errorFlag=true;
	            	 	}
	            	 	if(talent_first_name.equalsIgnoreCase("")||talent_first_name.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+=Utility.getLocaleValuePropByKey("msgUserFirstNameNotAvail", locale);
	            	 		errorFlag=true;
	            	 	}
	            	 	if(talent_last_name.equalsIgnoreCase("")||talent_last_name.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br> ";	
	            	 		}
	            	 		errorText+=Utility.getLocaleValuePropByKey("msgUserLastNameNotAvail", locale);
	            	 		errorFlag=true;
	            	 	}
	            	 	if(talent_email.equalsIgnoreCase("")||talent_email.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+=Utility.getLocaleValuePropByKey("msgUserEmailNotAvailable", locale);
	            	 	}else if(!Utility.validEmailForTeacher(talent_email)){
            	 			if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+=Utility.getLocaleValuePropByKey("msgUserEmailNotValid", locale);
	            	 	}/*else if(emailExist(emailExistCheck,userMasterlst,talent_email,session.getId(),jobId) && jobFlag==false){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+=Utility.getLocaleValuePropByKey("msgUserEmailAlreadyRegt", locale);
	            	 	}*/
	            	 	if(jobId.equalsIgnoreCase("")||jobId.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		jobFlag=true;
	            	 		errorText+=Utility.getLocaleValuePropByKey("msgJobIdNotAvailable", locale);
	            	 		errorFlag=true;
	            	 	}
	            	 	if(ksnId.equalsIgnoreCase("")||ksnId.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="ksnId is not available.";
	            	 		errorFlag=true;
	            	 	}
	            	 	/*if(tmksnId.equalsIgnoreCase("")||tmksnId.equalsIgnoreCase(" ")){
	            	 		if(errorFlag){
	            	 			errorText+=" </br>";	
	            	 		}
	            	 		errorText+="tmksnId is not available.";
	            	 		errorFlag=true;
	            	 	}*/
	            	 	
	            	 	if(!createdDateTime.equalsIgnoreCase("")){
	            	 		if(Utility.isDateValid(linkToKsnTalentsTemp.getCreatedDateTime(),1)==0){
	            	 			errorText+=Utility.getLocaleValuePropByKey("msgDateAppliedCurrentdate", locale);
		            	 		errorFlag=true;
	            	 		}
	            	 	}
	            	 	
	            	 	if(multiErrorFlag)
	            	 	{
	            	 		multiErrorFlag=false;
	            	 	}
	            	 	
	            	 	emailExistCheck.put(i+"|"+jobId,talent_email);
	            	 	linkToKsnTalentsTemp.setTalentId(talentId);
	            	 	linkToKsnTalentsTemp.setJobId(jobId);
	            	 	linkToKsnTalentsTemp.setTalent_first_name(talent_first_name);
	            	 	linkToKsnTalentsTemp.setTalent_last_name(talent_last_name);
	            	 	linkToKsnTalentsTemp.setTalent_email(talent_email);
	            	 	linkToKsnTalentsTemp.setSessionid(sessionId);
	            	 	linkToKsnTalentsTemp.setKsnId(ksnId);
	            	 	linkToKsnTalentsTemp.setTmksnId(tmksnId);
	            	 	
	            	 	if(errorText!=null)
	            	 	linkToKsnTalentsTemp.setErrortext(errorText);
	            	 	sessionHiber.insert(linkToKsnTalentsTemp);
		             }
	            }else{
	            	 if(jobId_count!=11 && talent_first_name_count!=11 && talent_last_name_count!=11 && talent_email_count!=11 && createdDateTime_count!=11 && ksnId_count!=11){
	            		 returnVal="1"; 
	            		 linkToKsnTalentsTempDAO.deleteTalentTemp(sessionId);
	            	 }else{
	            		 returnVal="";
	            		 boolean fieldVal=false;
		            	 if(talent_first_name_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" talent_first_name";
		            	 }
		            	 if(talent_last_name_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" talent_last_name";
		            	 }
		            	 if(talent_email_count==11){
		            		 if(fieldVal){
		            			 returnVal+=",";
		            		 }
		            		 fieldVal=true;
		            		 returnVal+=" talent_email";
		            	 }
		            	 if(jobId_count==11){
		            		 returnVal+=" jobId";
		            		 fieldVal=true;
		            	 }
		            	 if(ksnId_count==11){
		            		 returnVal+=" ksnId";
		            		 fieldVal=true;
		            	 }
		            	 if(tmksnId_count==11){
		            		 returnVal+=" tmksnId";
		            		 fieldVal=true;
		            	 }
		            	 if(createdDateTime_count==11){
		            		 returnVal+=" createdDateTime";
		            		 fieldVal=true;
		            	 }
	            	 }
	            }
		     }
			 txOpen.commit();
     	 	 sessionHiber.close();
     	 	System.out.println("Data has been inserted successfully.");
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return returnVal;
	}
	public static Vector readDataExcelXLS(String fileName) throws IOException {

		System.out.println(":::::::::::::::::readDataExcel XLS>>>>::::::::::::");
		Vector vectorData = new Vector();
		
		try{
			InputStream ExcelFileToRead = new FileInputStream(fileName);
			HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);
	 
			HSSFSheet sheet=wb.getSheetAt(0);
			HSSFRow row; 
			HSSFCell cell;
	 
			Iterator rows = sheet.rowIterator();
			int talentId_count=11;
			int createdDateTime_count=11;
	        int jobId_count=11;
	        int talent_first_name_count=11;
	        int talent_last_name_count=11;
	        int talent_email_count=11;
	        int ksnId_count=11;
	        int tmksnId_count=11;
	        String indexCheck="";
			 
			while (rows.hasNext()){
				row=(HSSFRow) rows.next();
				Iterator cells = row.cellIterator();
				Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false,dateFlag=false;
				String indexPrev="";
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
				int ii=0;
				while (cells.hasNext())
				{
					
					System.out.println("iiiiii>>>"+ii++);
					cell=(HSSFCell) cells.next();
					cIndex=cell.getColumnIndex();
					
					System.out.println("cell.toString()::::::::::::::::>>>>>>>>>>>>>>>:::::::::>>>"+cell.toString());
					
					if(cell.toString().equalsIgnoreCase("talentId")){
						talentId_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(talentId_count==cIndex){
						cellFlag=true;
					}
					
					if(cell.toString().equalsIgnoreCase("talent_first_name")){
						talent_first_name_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(talent_first_name_count==cIndex){
						cellFlag=true;
					}
					
					
					if(cell.toString().equalsIgnoreCase("talent_last_name")){
						talent_last_name_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(talent_last_name_count==cIndex){
						cellFlag=true;
					}
					
					
					if(cell.toString().equalsIgnoreCase("talent_email")){
						talent_email_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(talent_email_count==cIndex){
						cellFlag=true;
					}
					if(cell.toString().equalsIgnoreCase("jobId")){
						jobId_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(jobId_count==cIndex){
						cellFlag=true;
					}
					if(cell.toString().equalsIgnoreCase("ksnId")){
						ksnId_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(ksnId_count==cIndex){
						cellFlag=true;
					}
					
					if(cell.toString().equalsIgnoreCase("tmksnId")){
						tmksnId_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(tmksnId_count==cIndex){
						cellFlag=true;
					}
					
					if(cell.toString().equalsIgnoreCase("createdDateTime")){
						createdDateTime_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(createdDateTime_count==cIndex){
						cellFlag=true;
						dateFlag=true;
					}	
					System.out.println("cellFlag::::::::::::::::>>>>>>>>>>>>>>>:::::::::>>>"+cellFlag);
					if(cellFlag){
						if(!dateFlag){
							System.out.println("--------------------"+cIndex);
	            			try{
	            				mapCell.put(Integer.valueOf(cIndex),cell.getStringCellValue()+"");
	            				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
	            			}catch(Exception e){
	            				mapCell.put(Integer.valueOf(cIndex),new Double(cell.getNumericCellValue()).longValue()+"");
	            				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 2");
	            			}
	            		}else{
	            			System.out.println("-------------------- Else:::::::"+cIndex);
	            			mapCell.put(Integer.valueOf(cIndex),cell+"");
	            		}
					}
					dateFlag=false;
		        	cellFlag=false;
					
				}
			    vectorCellEachRowData=cellValuePopulate(mapCell);
				vectorData.addElement(vectorCellEachRowData);
			}
		}catch(Exception e){}
		return vectorData;
    }
	public static Vector readDataExcelXLSX(String fileName) {
		System.out.println(":::::::::::::::::readDataExcel XLSX ::::::::::::");
        Vector vectorData = new Vector();
        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            XSSFWorkbook xssfWorkBook = new XSSFWorkbook(fileInputStream);
            // Read data at sheet 0
            XSSFSheet xssfSheet = xssfWorkBook.getSheetAt(0);
            Iterator rowIteration = xssfSheet.rowIterator();
            int createdDateTime_count=11;
            int jobId_count=11;
            int talent_first_name_count=11;
            int talent_last_name_count=11;
            int talent_email_count=11;
            int ksnId_count=11;
            int tmksnId_count=11;
            int talentId_count=11;
            // Looping every row at sheet 0
            String indexCheck="";
            while (rowIteration.hasNext()) {
                XSSFRow xssfRow = (XSSFRow) rowIteration.next();
                Iterator cellIteration = xssfRow.cellIterator();
                Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false,dateFlag=false;
				String indexPrev="";
                // Looping every cell in each row at sheet 0
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
                while (cellIteration.hasNext()){
                    XSSFCell xssfCell = (XSSFCell) cellIteration.next();
                    cIndex=xssfCell.getColumnIndex();
                    
                    if(xssfCell.toString().equalsIgnoreCase("talentId")){
	            		talentId_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(talentId_count==cIndex){
	            		cellFlag=true;
	            	}
					
	            	if(xssfCell.toString().equalsIgnoreCase("talent_first_name")){
	            		talent_first_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(talent_first_name_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("talent_last_name")){
	            		talent_last_name_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(talent_last_name_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("talent_email")){
	            		talent_email_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(talent_email_count==cIndex){
	            		cellFlag=true;
	            	}
	            	if(xssfCell.toString().equalsIgnoreCase("jobId")){
	            		jobId_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(jobId_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("ksnId")){
	            		ksnId_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(ksnId_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("tmksnId")){
	            		tmksnId_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(tmksnId_count==cIndex){
	            		cellFlag=true;
	            	}
	            	
	            	if(xssfCell.toString().equalsIgnoreCase("createdDateTime")){
                    	createdDateTime_count=cIndex;
	            		indexCheck+="||"+cIndex+"||";
	            	}
	            	if(createdDateTime_count==cIndex){
	            		cellFlag=true;
	            		dateFlag=true;
	            	}	
	            	  
	            	System.out.println("xssfCell.toString() :::"+xssfCell.toString());
		            	
	            	
	            	if(cellFlag){
	            		if(!dateFlag){
	            			try{
	            				mapCell.put(Integer.valueOf(cIndex),xssfCell.getStringCellValue()+"");
	            			}catch(Exception e){
	            				mapCell.put(Integer.valueOf(cIndex),xssfCell.getRawValue()+"");
	            			}
	            		}else{
	            			mapCell.put(Integer.valueOf(cIndex),xssfCell+"");
	            		}
	            	}
	            	dateFlag=false;
	            	cellFlag=false;
                }
                vectorCellEachRowData=cellValuePopulate(mapCell);
                vectorData.addElement(vectorCellEachRowData);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
         
        return vectorData;
    }
	public static Vector cellValuePopulate(Map<Integer,String> mapCell){
		 Vector vectorCellEachRowData = new Vector();
		 Map<Integer,String> mapCellTemp = new TreeMap<Integer, String>();
		 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false;
		 for(Map.Entry<Integer, String> entry : mapCell.entrySet()){
			int key=entry.getKey();
			String cellValue=null;
			if(entry.getValue()!=null)
				cellValue=entry.getValue().trim();
			
			if(key==0){
				mapCellTemp.put(key, cellValue);
				flag0=true;
			}
			if(key==1){
				flag1=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==2){
				flag2=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==3){
				flag3=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==4){
				flag4=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==5){
				flag5=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==6){
				flag6=true;
				mapCellTemp.put(key, cellValue);
			}
			if(key==7){
				flag7=true;
				mapCellTemp.put(key, cellValue);
			}
		 }
		 if(flag0==false){
			 mapCellTemp.put(0, "");
		 }
		 if(flag1==false){
			 mapCellTemp.put(1, "");
		 }
		 if(flag2==false){
			 mapCellTemp.put(2, "");
		 }
		 if(flag3==false){
			 mapCellTemp.put(3, "");
		 }
		 if(flag4==false){
			 mapCellTemp.put(4, "");
		 }
		 if(flag5==false){
			 mapCellTemp.put(5, "");
		 }
		 if(flag6==false){
			 mapCellTemp.put(6, "");
		 }
		 if(flag7==false){
			 mapCellTemp.put(7, "");
		 }
		 for(Map.Entry<Integer, String> entry : mapCellTemp.entrySet()){
			 vectorCellEachRowData.addElement(entry.getValue());
		 }
		 return vectorCellEachRowData;
	}
	
	public static String monthReplace(String date){
		String currentDate="";
		try{
			
		   if(date.contains("Jan")){
			   currentDate=date.replaceAll("Jan","01");
		   }
		   if(date.contains("Feb")){
			   currentDate=date.replaceAll("Feb","02");
		   }
		   if(date.contains("Mar")){
			   currentDate=date.replaceAll("Mar","03");
		   }
		   if(date.contains("Apr")){
			   currentDate=date.replaceAll("Apr","04");
		   }
		   if(date.contains("May")){
			   currentDate=date.replaceAll("May","05");
		   }
		   if(date.contains("Jun")){
			   currentDate=date.replaceAll("Jun","06");
		   }
		   if(date.contains("Jul")){
			   currentDate=date.replaceAll("Jul","07");
		   }
		   if(date.contains("Aug")){
			   currentDate=date.replaceAll("Aug","08");
		   }
		   if(date.contains("Sep")){
			   currentDate=date.replaceAll("Sep","09");
		   }
		   if(date.contains("Oct")){
			   currentDate=date.replaceAll("Oct","10");
		   }
		   if(date.contains("Nov")){
			   currentDate=date.replaceAll("Nov","11");
		   }
		   if(date.contains("Dec")){
			   currentDate=date.replaceAll("Dec","12");
		   }
		   String uDate=currentDate.replaceAll("-","/");
		   if(Utility.isDateValid(uDate,0)==2){
			   currentDate=date; 
		   }else{
			   try{
				   String dateSplit[]=uDate.split("/");
				   currentDate=dateSplit[1]+"/"+dateSplit[0]+"/"+dateSplit[2];
			   }catch(Exception e){
				   currentDate=date; 
			   }
		   }
		}catch(Exception e){
			currentDate=date;
		}
		   return currentDate;
	 }
	public String disconnectQueue(String teacherId,String ksnId){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		int mqEventId = 0;
		String result = "";
			try {
				UserMaster userMaster = null;
				TeacherDetail teacherDetail = null;
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				{
					throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
				}
				else{
						userMaster = (UserMaster) session.getAttribute("userMaster");
				}
				boolean isAlphaNumeric = false;
				
				try{
					Long.parseLong(ksnId);
				}catch(Exception ex){
					isAlphaNumeric = true;
				}
				
				if(ksnId.trim().length()==0)
					return "error###&#149; Please enter KSN ID";
				/*else if(ksnId.trim().length()<10 || ksnId.trim().length()>10)
					return "error###&#149; Please enter 10 digit KSN ID";*/
				else if(isAlphaNumeric){
					return "error###&#149; Please enter KSN ID in digits only";
				}
				
				try{
					List<TeacherDetail> lstteacherList = teacherDetailDAO.findTeacherByKSN(BigInteger.valueOf(Long.valueOf(ksnId)));
					if(lstteacherList!=null && lstteacherList.size()>0)
						return "error###&#149; This KSN ID is already exists";
				}catch(Exception ex){
					ex.printStackTrace();
				}
				
				   Map<Object, Object> mapData = new HashMap<Object, Object>();
				   mapData.put("userId",userMaster.getUserId());
				   String TMTransactionID="TM-"+(Utility.randomString(8)+Utility.getDateTime());
				   mapData.put("TMTransactionID", TMTransactionID);
				   mapData.put("KSNID", ksnId);
				   mapData.put("teacherId", teacherId);
				   if(userMaster.getBranchMaster()!=null)
					   mapData.put("branchCode", userMaster.getBranchMaster().getBranchCode());
				   
				   if(teacherId!=null && teacherId.length()>0)
					   teacherDetail = teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);
				   
				   SecondaryStatus secondaryStatus = null;
				   JobForTeacher jobForTeacherObj = null;
				   JobOrder jobOrder = null;
				   List<JobForTeacher> kellyAppliedJobs = new ArrayList<JobForTeacher>();
				   List<JobCategoryMaster> jobCategories = new ArrayList<JobCategoryMaster>();
				   Map<Integer,Integer> jftCatMap = new HashMap<Integer, Integer>();
				   Map<Integer,JobOrder> jobMap = new HashMap<Integer, JobOrder>();
				   Map<Integer,JobForTeacher> jobForTeacherMap = new HashMap<Integer, JobForTeacher>();
				   Map<Integer,SecondaryStatus> secStatusMap = new HashMap<Integer, SecondaryStatus>();
				   Boolean jobCodeExists =false;
				   try {
					   kellyAppliedJobs = jobForTeacherDAO.findJobByTeacherInDesc(teacherDetail);
					   Integer maxVal = 0;
					   if(kellyAppliedJobs!=null && kellyAppliedJobs.size()>0){
						   for(JobForTeacher jft : kellyAppliedJobs){
							   if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
								   jobCategories.add(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId());
							   }
						   }
					   }
					   
					   List<SecondaryStatus> secondaryStatuses =null;
					   if(jobCategories!=null && jobCategories.size()>0)
						   secondaryStatuses = secondaryStatusDAO.findByNameAndJobCats(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale),jobCategories);
					   if(secondaryStatuses!=null && secondaryStatuses.size()>0){
						   for(SecondaryStatus sec : secondaryStatuses){
							   if(sec.getJobCategoryMaster()!=null){
								   secStatusMap.put(sec.getJobCategoryMaster().getJobCategoryId(), sec);
							   }
						   }
					   }
					   
					   if(kellyAppliedJobs!=null && kellyAppliedJobs.size()>0){
						   for(JobForTeacher jft : kellyAppliedJobs){
							   jobForTeacherObj = jft;
							   jobOrder = jft.getJobId();
							   if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
								   if(secStatusMap.get(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId())!=null){
									   jobOrder = jft.getJobId();
									   jobCodeExists = true;
									   jobForTeacherObj = jft;
									   secondaryStatus = secStatusMap.get(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
									   break;
								   }
							   }
						   }
					   }
				   }catch (Exception e1) {
					e1.printStackTrace();
				}
				   
				   jobMap.clear();
				   jftCatMap.clear();
				   jobForTeacherMap.clear();
				   secStatusMap.clear();
				   
				   StatusMaster statusMaster = null;
				   
				   List<MQEvent> mqEventObj=mqEventDAO.findEventByEventType(teacherDetail, Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));///findNodeStatusByTeacherJobStatus(teacherDetail,null,null,secondaryStatus);
					boolean linkToKsnFlag=true;
					if(mqEventObj!=null && mqEventObj.size()>0){
						if(mqEventObj.get(0).getStatus()!=null && mqEventObj.get(0).getStatus().equalsIgnoreCase("C")){
							linkToKsnFlag=false;
						}
					}
					
					logger.info(" linkToKsnFlag ::: "+linkToKsnFlag);
					
				   MQEvent mqEventData=null;
					int counter=0;
					if(linkToKsnFlag){
					//	messageQueue(statusMaster,secondaryStatus,jobForTeacherObj,userMaster,"",ksnId);
						logger.info("Time Start :::::::::::::::::::::::::::::::: "+new Date());
						
					}
			  } catch (BeansException e) {
			   e.printStackTrace();
			  } catch (JmsException e) {
			   e.printStackTrace();
			  } catch (Exception e) {
			   e.printStackTrace();
			  }
			return result;
	}
	
	//nadeem
	public String excelLinktoKSN(){
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<----  excelLinktoKSN() ---- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		boolean resultFlag=false;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
			try {
				UserMaster userMaster = null;
				TeacherDetail teacherDetail = null;
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				{
					throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
				}
				else{
						userMaster = (UserMaster) session.getAttribute("userMaster");
				}
				// get all teacherID and kSN ID from temp table.....
				
				List<TeacherDetail> allTalentDetail=new ArrayList<TeacherDetail>();
				//List<Integer> allTalentID=new ArrayList<Integer>();
				Map<Integer,String> ksnByTalentID=new HashMap<Integer,String>(); // map to store ksnid according to talentID
				Map<String,JobCategoryMaster> mapForjobcat=new HashMap<String, JobCategoryMaster>();
				Map<TeacherDetail,JobForTeacher> jftbtTalentID=new HashMap<TeacherDetail, JobForTeacher>();
				Map<Integer,SecondaryStatus> secStatusMap=new HashMap<Integer, SecondaryStatus>();
				List<TeacherDetail> finalTeachers=new ArrayList<TeacherDetail>();
				List<JobCategoryMaster> alljobCategories = new ArrayList<JobCategoryMaster>(); // list for all parent jobcategories from jobforteachers
			    List <LinkToKsnTalentsTemp> allData=	linkToKsnTalentsTempDAO.findAllTempTalent();	
				for(LinkToKsnTalentsTemp data:allData){
					TeacherDetail teachdtl=new TeacherDetail();
					teachdtl.setTeacherId(Integer.parseInt(data.getTalentId()));
					allTalentDetail.add(teachdtl);	
					boolean atleastOneAlpha = data.getKsnId().matches(".*[a-zA-Z]+.*");
					if(!atleastOneAlpha)
						ksnByTalentID.put(Integer.parseInt(data.getTalentId()),data.getKsnId());
					else{
						LinkToKsnTalentsTemp tempLinktoksn=data;
						tempLinktoksn.setTalentId(data.getTalentId());
						tempLinktoksn.setTalent_first_name(data.getTalent_first_name());
						tempLinktoksn.setTalent_last_name(data.getTalent_last_name());
						tempLinktoksn.setTalent_email(data.getTalent_email());
						tempLinktoksn.setJobId(data.getJobId());
						tempLinktoksn.setKsnId(data.getKsnId());
						tempLinktoksn.setCreatedDateTime(data.getCreatedDateTime());
						tempLinktoksn.setErrortext("Invalid KSNID");
						tempLinktoksn.setSessionid(data.getSessionid());
						tempLinktoksn.setDate(new Date());
						linkToKsnTalentsTempDAO.updatePersistent(tempLinktoksn);
					}
					//allTalentID.add(Integer.parseInt(data.getTalentId())); // for talent IDS only integer val
				}
				List<JobForTeacher> jftByTalentId=jobForTeacherDAO.getjftByTalentId(allTalentDetail);
				if(jftByTalentId!=null&&jftByTalentId.size()>0){
					for(JobForTeacher jft:jftByTalentId){
						if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null)
							alljobCategories.add(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId());
						    mapForjobcat.put(jft.getTeacherId().getTeacherId()+"##"+jft.getJobId().getJobId(), jft.getJobId().getJobCategoryMaster().getParentJobCategoryId());
					}
				}
				   List<SecondaryStatus> secondaryStatuses =null;
				   if(alljobCategories!=null && alljobCategories.size()>0)
					   secondaryStatuses = secondaryStatusDAO.findByNameAndJobCats(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale),alljobCategories);
				   if(secondaryStatuses!=null && secondaryStatuses.size()>0){
					   for(SecondaryStatus sec : secondaryStatuses){
						   if(sec.getJobCategoryMaster()!=null && sec.getStatusMaster()==null){
							   secStatusMap.put(sec.getJobCategoryMaster().getJobCategoryId(), sec);// for secondry status
						   }
						   if(sec.getJobCategoryMaster()!=null && sec.getStatusMaster()!=null){
							   secStatusMap.put(sec.getJobCategoryMaster().getJobCategoryId(), sec); // for statusmaster...
						   }
					   }
				   }//end of if
				   StatusMaster statusMaster=null;
				   SecondaryStatus secondryStatus=null;
				   JobForTeacher finaljftobj=null;
				 for(JobForTeacher jftobj:jftByTalentId){					 
					  if(jftobj.getJobId().getHeadQuarterMaster()!=null && jftobj.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
						  jftbtTalentID.put(jftobj.getTeacherId(), jftobj);
						   if(secStatusMap.get(jftobj.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId())!=null){							  
							   secondryStatus = secStatusMap.get(jftobj.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
							   if(secondryStatus.getStatusMaster()==null && secondryStatus.getSecondaryStatus()!=null)
								   secondryStatus = secStatusMap.get(jftobj.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
							   else
								   statusMaster = secondryStatus.getStatusMaster();							   
						   }
					   }//end main if					 
				 } 
				 Set<TeacherDetail> keysets=jftbtTalentID.keySet();
				 for(TeacherDetail tdetail:keysets){							 
					 finaljftobj=jftbtTalentID.get(tdetail);
					 if(finaljftobj!=null && ksnByTalentID.get(finaljftobj.getTeacherId().getTeacherId())!=null){
						 resultFlag = manageStatmusAjax.messageQueue(statusMaster,secondryStatus,finaljftobj,userMaster,"",ksnByTalentID.get(finaljftobj.getTeacherId().getTeacherId()));
						 List<LinkToKsnTalentsTemp> successTeacher=linkToKsnTalentsTempDAO.findByTalentId(finaljftobj.getTeacherId().getTeacherId().toString());
						 LinkToKsnTalentsTemp tempLinktoksnsucc=successTeacher.get(0);
						 tempLinktoksnsucc.setTalentId(finaljftobj.getTeacherId().getTeacherId().toString());
						 tempLinktoksnsucc.setTalent_first_name(finaljftobj.getTeacherId().getFirstName());
						 tempLinktoksnsucc.setTalent_last_name(finaljftobj.getTeacherId().getLastName());
						 tempLinktoksnsucc.setTalent_email(finaljftobj.getTeacherId().getEmailAddress());
						 tempLinktoksnsucc.setJobId(finaljftobj.getJobId().getJobId().toString());
						 tempLinktoksnsucc.setKsnId(ksnByTalentID.get(finaljftobj.getTeacherId().getTeacherId()));
						 tempLinktoksnsucc.setCreatedDateTime("");
						 tempLinktoksnsucc.setErrortext("Success");
						 tempLinktoksnsucc.setSessionid("");						
						 tempLinktoksnsucc.setDate(new Date());
						 linkToKsnTalentsTempDAO.updatePersistent(tempLinktoksnsucc);
					 }
				 }
				 System.out.println("end excelupload link to ksn method.................."+jftbtTalentID.size());
			}
			catch(Exception e){
				e.printStackTrace();
			}			
		return "";
	}
}

