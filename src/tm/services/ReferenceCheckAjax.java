package tm.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherElectronicReferencesHistory;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificRefChkAnswers;
import tm.bean.master.DistrictSpecificRefChkOptions;
import tm.bean.master.DistrictSpecificRefChkQuestions;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.ReferenceCheckFitScore;
import tm.bean.master.ReferenceCheckQuestionSet;
import tm.bean.master.ReferenceQuestionSetQuestions;
import tm.bean.master.ReferenceQuestionSets;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherElectronicReferencesHistoryDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.DistrictSpecificRefChkAnswersDAO;
import tm.dao.master.DistrictSpecificRefChkQuestionsDAO;
import tm.dao.master.ReferenceCheckFitScoreDAO;
import tm.dao.master.ReferenceCheckQuestionSetDAO;
import tm.dao.master.ReferenceQuestionSetQuestionsDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.TeacherAnswerDetailsForDistrictSpecificQuestionsDAO;
import tm.dao.user.UserMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

public class ReferenceCheckAjax {

	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	
	@Autowired
	private TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO;
	
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private DistrictSpecificRefChkAnswersDAO districtSpecificRefChkAnswersDAO;
	
	@Autowired
	private DistrictSpecificRefChkQuestionsDAO districtSpecificRefChkQuestionsDAO;
	
	@Autowired
	private TeacherElectronicReferencesHistoryDAO teacherElectronicReferencesHistoryDAO;
	
	@Autowired
	private ReferenceCheckFitScoreDAO referenceCheckFitScoreDAO;
	
	@Autowired
	private ReferenceCheckQuestionSetDAO referenceCheckQuestionSetDAO;
	
	@Autowired
	private ReferenceQuestionSetQuestionsDAO referenceQuestionSetQuestionsDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	HeadQuarterMasterDAO headQuarterMasterDAO;
@Transactional(readOnly=false)
public String getDistrictSpecificQuestion(Integer referenceId,Integer headQuarterId, Integer districtId,Integer jobId,Integer teacherId,String isAffilated){
	System.out.println("*****:::::::::::::::Assessment getDistrictSpecificQuestion:::::::::::::::::::;; ");
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	/*HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}*/
	
	/*UserMaster userMaster = null;
	if (session == null || session.getAttribute("userMaster") == null) {
		return "false";
	}
	else
	{
		userMaster=(UserMaster)session.getAttribute("userMaster");
	}*/
	
	TeacherElectronicReferences teacherElectronicReferences = null;
	JobOrder jobOrder 				= 	null;
	DistrictMaster districtMaster 	=	null;
	List<ReferenceCheckQuestionSet> referenceCheckQuestionSetList = new ArrayList<ReferenceCheckQuestionSet>();
	List<ReferenceQuestionSetQuestions> referenceQuestionSetQuestionsList = new ArrayList<ReferenceQuestionSetQuestions>();
	
	StringBuffer sb =new StringBuffer();
	try{
		TeacherDetail teacherDetail = null;
		HeadQuarterMaster headQuarterMaster =null;
		teacherElectronicReferences = teacherElectronicReferencesDAO.findById(referenceId, false, false);
		teacherDetail	=	teacherDetailDAO.findById(teacherId, false, false);
		if(districtId!=null && districtId!=0)
		districtMaster	=	districtMasterDAO.findById(districtId, false, false);
		
		if(headQuarterId!=null && headQuarterId !=0)
			headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
		
		List<JobOrder> jobOrders = null;
			try{
				if(jobId!=0){
					jobOrder						=	jobOrderDAO.findById(jobId, false, false);
					if(districtMaster!=null)
						referenceCheckQuestionSetList	=	referenceCheckQuestionSetDAO.findByQuestionAndJobCategory(districtMaster,jobOrder.getJobCategoryMaster());
					else if(headQuarterMaster!=null)
						referenceCheckQuestionSetList	=	referenceCheckQuestionSetDAO.findByQuestionAndJobCategoryByHQ(headQuarterMaster,jobOrder.getJobCategoryMaster());
					
					
					if(referenceCheckQuestionSetList!=null && referenceCheckQuestionSetList.size()>0){
						referenceQuestionSetQuestionsList	=	referenceQuestionSetQuestionsDAO.findByQuestionSet(referenceCheckQuestionSetList.get(0).getReferenceQuestionSets());		
					}
					
				}
			} catch (Exception exception){
				//exception.printStackTrace();
			}

			List<DistrictSpecificRefChkAnswers> lastList = new ArrayList<DistrictSpecificRefChkAnswers>();
			
			lastList = districtSpecificRefChkAnswersDAO.findTeacherAnswersByDistrict(teacherDetail);

			Map<Integer, DistrictSpecificRefChkAnswers> map = new HashMap<Integer, DistrictSpecificRefChkAnswers>();

			for(DistrictSpecificRefChkAnswers teacherAnswerDetailsForDistrictSpecificQuestion : lastList)
				map.put(teacherAnswerDetailsForDistrictSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionId(), teacherAnswerDetailsForDistrictSpecificQuestion);

			DistrictSpecificRefChkAnswers teacherAnswerDetailsForDistrictSpecificQuestion = null;
			List<DistrictSpecificRefChkOptions> questionOptionsList = null;
			String questionInstruction = null;
			String shortName = "";
			//int totalQuestions = districtSpecificQuestionList.size();
			int totalQuestions = referenceQuestionSetQuestionsList.size();
			int cnt = 0;
			int topMaxScore = 0;
			int interval 	= 10;
			int dslider 	= 0;
			int svalue 		= 0;
			int scoreProvided = 0;
			
			/* Section for User Declaration */
			if(totalQuestions>0){
				sb.append("<tr>");
				sb.append("<td width='90%'><h3 style='margin-left: 12px;'>");
				if(districtId!=null && districtId==4218990)
				{
					sb.append(Utility.getLocaleValuePropByKey("msgTakingTheTimeComplete", locale) +
							Utility.getLocaleValuePropByKey("msgSchoolDistrictPhiladelphia", locale));
				}
				else
				{
					String sDistrictName="";
					
					if(districtMaster!=null){
					if(!districtMaster.getDistrictName().equals(""))
							sDistrictName=districtMaster.getDisplayName();
						if(districtMaster.getDisplayName() == null || districtMaster.getDisplayName() == "")
							sDistrictName=districtMaster.getDistrictName();
						
					}
					else{
						if(headQuarterMaster!=null){
							if(!headQuarterMaster.getHeadQuarterName().equals(""))
								sDistrictName=headQuarterMaster.getHeadQuarterName();
							 
						}
					}
					sb.append(Utility.getLocaleValuePropByKey("msgTakingTheTimeComplete", locale)+sDistrictName+".");					  
				}
				
				sb.append("<h3></td>");
				sb.append("</tr>");
				
				if(districtMaster!=null && districtMaster.getTextForReferenceInstruction()!="" && districtMaster.getTextForReferenceInstruction()!=null){
					sb.append("<tr>");
					sb.append("<td width='90%'><h3 style='margin-left: 12px;'>");
					sb.append(Utility.getUTFToHTML(districtMaster.getTextForReferenceInstruction()));			
					sb.append("</h3></td>");
					sb.append("</tr>");
				}
				
				sb.append("<tr>");
				sb.append("<td>");
				sb.append("<table width='100%'>");
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<h3 style='margin-left: 12px;'><B>"+Utility.getLocaleValuePropByKey("msgYourContactInformation", locale)+"</B></h3>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<div class=''>");
					sb.append("<div class='col-sm-4 col-md-4'>"+Utility.getLocaleValuePropByKey("lblFname", locale)+":<span class=\"required\">*</span></div>");
					sb.append("<div class='col-sm-3 col-md-3'>");
					sb.append("<input type='text' id='firstName' name='firstName' maxlength='50' value='"+teacherElectronicReferences.getFirstName()+"' class='help-inline form-control'>");
					sb.append("</div>");
				sb.append("</div>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<div class=''>");
					sb.append("<div class='col-sm-4 col-md-4'>"+Utility.getLocaleValuePropByKey("lblLname", locale)+":<span class=\"required\">*</span></div>");
					sb.append("<div class='col-sm-3 col-md-3'>");
					sb.append("<input type='text' id='lastName' name='lastName' maxlength='50' value='"+teacherElectronicReferences.getLastName()+"' class='help-inline form-control'>");
					sb.append("</div>");
				sb.append("</div>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<div class=''>");
					sb.append("<div class='col-sm-4 col-md-4'>"+Utility.getLocaleValuePropByKey("msgCurrentTitle", locale)+":</div>");
					sb.append("<div class='col-sm-3 col-md-3'>");
					sb.append("<input type='text' id='designation' name='designation' maxlength='50' value='"+teacherElectronicReferences.getDesignation()+"' class='help-inline form-control'>");
					sb.append("</div>");
				sb.append("</div>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<div class=''>");
					sb.append("<div class='col-sm-4 col-md-4'>"+Utility.getLocaleValuePropByKey("msgCurrentCompanyOrg", locale)+":</div>");
					sb.append("<div class='col-sm-3 col-md-3'>");
					sb.append("<input type='text' id='organization' name='organization' maxlength='50' value='"+teacherElectronicReferences.getOrganization()+"' class='help-inline form-control'>");
					sb.append("</div>");
				sb.append("</div>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<div class=''>");
					sb.append("<div class='col-sm-4 col-md-4'>"+Utility.getLocaleValuePropByKey("msgPrimaryPhone", locale)+":</div>");
					sb.append("<div class='col-sm-3 col-md-3'>");
					sb.append("<input type='text' id='contactnumber' name='contactnumber' maxlength='50' value='"+teacherElectronicReferences.getContactnumber()+"' class='help-inline form-control'>");
					sb.append("</div>");
				sb.append("</div>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<div class=''>");
					sb.append("<div class='col-sm-4 col-md-4'>"+Utility.getLocaleValuePropByKey("msgSecondaryPhone", locale)+":</div>");
					sb.append("<div class='col-sm-3 col-md-3'>");
					sb.append("<input type='text' id='secondaryPhone' name='secondaryPhone' maxlength='50' class='help-inline form-control'>");
					sb.append("</div>");
				sb.append("</div>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<div class=''>");
					sb.append("<div class='col-sm-4 col-md-4'>"+Utility.getLocaleValuePropByKey("msgEmail3", locale)+":<span class=\"required\">*</span></div>");
					sb.append("<div class='col-sm-3 col-md-3'>");
					sb.append("<input type='text' id='email' name='email' value='"+teacherElectronicReferences.getEmail()+"' maxlength='50' class='help-inline form-control'>");
					sb.append("</div>");
				sb.append("</div>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<div class=''>");
					sb.append("<div class='col-sm-4 col-md-4'>"+Utility.getLocaleValuePropByKey("msgRelationshipCandidate", locale)+":</div>");
					sb.append("<div class='col-sm-3 col-md-3'>");
					sb.append("<input type='text' id='relationship' name='relationship' maxlength='50' class='help-inline form-control'>");
					sb.append("</div>");
				sb.append("</div>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<div class=''>");
					sb.append("<div class='col-sm-4 col-md-4'>"+Utility.getLocaleValuePropByKey("msgLengthOfTime", locale)+":</div>");
					sb.append("<div class='col-sm-3 col-md-3'>");
					sb.append("<input type='text' id='length' name='length' maxlength='50' class='help-inline form-control'>");
					sb.append("</div>");
				sb.append("</div>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<div class=''>");
					sb.append("<div class='col-sm-4 col-md-4'>"+Utility.getLocaleValuePropByKey("msgCandidatePeerGroup", locale)+":</div>");
					sb.append("<div class='col-sm-3 col-md-3'>");
					sb.append("<input type='text' id='reference' name='reference' maxlength='50' class='help-inline form-control'>");
					sb.append("</div>");
				sb.append("</div>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<div class=''>");
					sb.append("<div class='col-sm-4 col-md-4'>"+Utility.getLocaleValuePropByKey("msgMayHiringManager", locale)+":</div>");
					sb.append("<div class='col-sm-3 col-md-3'>");
					sb.append("<input type='text' id='manager' name='manager' maxlength='50' class='help-inline form-control'>");
					sb.append("</div>");
				sb.append("</div>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</table>");
				sb.append("</td>");
				sb.append("</tr>");
				
				if(districtId!=null && districtId==4218990)
				{
				sb.append("<tr>");
				sb.append("<td>");
				sb.append("<table width='100%'>");
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<h3><B>"+Utility.getLocaleValuePropByKey("msgCandidateEvaluation", locale)+"</B></h3>");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append(Utility.getLocaleValuePropByKey("msgEvaluateCandidate", locale) +
						Utility.getLocaleValuePropByKey("msgApplicableRelevant", locale)); 
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("Rankings:"); 
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style='border:0px;background-color: transparent;'>");
				sb.append("<UL>");
				sb.append("<LI>"+Utility.getLocaleValuePropByKey("msgExceptionalTop5", locale)+"</LI>");
				sb.append("<LI>"+Utility.getLocaleValuePropByKey("msgAboveAverageTop25", locale)+"</LI>");
				sb.append("<LI>"+Utility.getLocaleValuePropByKey("msgAverageTop50", locale)+"</LI>");
				sb.append("<LI>"+Utility.getLocaleValuePropByKey("msgNeedsImprovementBottom50", locale)+"</LI>");
				sb.append("<LI>"+Utility.getLocaleValuePropByKey("msgNoBasisforJudgment", locale)+"</LI>");
				sb.append("<UL>");
				sb.append("</td>");
				sb.append("</tr>");
				

				sb.append("</table>");
				sb.append("</td>");
				sb.append("</tr>");
				}
				
				//for (DistrictSpecificRefChkQuestions districtSpecificQuestion : districtSpecificQuestionList)
				for (ReferenceQuestionSetQuestions districtSpecificQuestion : referenceQuestionSetQuestionsList)
					{
						shortName = districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionTypeMaster().getQuestionTypeShortName();

						sb.append("<tr>");
						sb.append("<td width='90%' style='padding-left: 18px;'>");
						sb.append("<div><b>Question "+(++cnt)+" of "+totalQuestions+"</b></div>");

						questionInstruction = districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionInstructions()==null?"":districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionInstructions();
						if(!questionInstruction.equals(""))
							sb.append(""+Utility.getUTFToHTML(questionInstruction)+"");

						sb.append("<div id='Q"+cnt+"question'>"+Utility.getUTFToHTML(districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestion())+"</div>");

						questionOptionsList = districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionOptions();
						teacherAnswerDetailsForDistrictSpecificQuestion = map.get(districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionId());

						String checked = "";
						Integer optId = 0;
						String insertedText = "";
						String selectedOptions = "";
						String yesChecked="";
						String noChecked="";
						if(teacherAnswerDetailsForDistrictSpecificQuestion!=null)
						{
							if(shortName.equalsIgnoreCase(teacherAnswerDetailsForDistrictSpecificQuestion.getQuestionType()))
							{
								if(teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions()!=null || (headQuarterMaster!=null && headQuarterMaster.getHeadQuarterId()==2))
									insertedText = teacherAnswerDetailsForDistrictSpecificQuestion.getInsertedText();
							    
								if(headQuarterMaster!=null && headQuarterMaster.getHeadQuarterId()==2)
								{
								checked=teacherAnswerDetailsForDistrictSpecificQuestion.getQuestionOption();
								System.out.println("checked::::::::::::::::::"+checked);
								if(checked!=null && checked!="")
								{									
									if(checked.trim().equalsIgnoreCase("yes"))
									{								
										yesChecked="checked";
									}
									else if(checked.trim().equalsIgnoreCase("no"))
									{
										noChecked="checked";
									}
								}
								}
								
							}
							
						}
						
						try{
							if(districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionMaxScore() !=0 && districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionMaxScore() != null)
							{
								topMaxScore		=	districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionMaxScore();
								if(topMaxScore > 0){
									dslider = 1;
								} else {
									dslider = 0;
								}

								if(topMaxScore%10==0)
									interval=10;
								else if(topMaxScore%5==0)
									interval=5;
								else if(topMaxScore%3==0)
									interval=3;
								else if(topMaxScore%2==0)
									interval=2;
								/*sb.append("<div>");
								sb.append("</div>");*/
								sb.append("<table>");
								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'>");
								sb.append("<B style='vertical-align: top;'>"+Utility.getLocaleValuePropByKey("msgNoBasisforJudgment", locale)+" :</B>");
								//sb.append("<input type='radio' id='MaxScore"+cnt+"' name='MaxScore"+cnt+"' value=\"1\" onclick=\"showHideScoreDiv("+cnt+")\" />");
								sb.append("<input type='checkbox' id='MaxScore"+cnt+"' name='MaxScore"+cnt+"' value=\"1\" onclick=\"showHideScoreDiv("+cnt+")\" style='margin-left: 5px;margin-top: 2px;'/>");
								sb.append("</div></td></tr>");
								sb.append("</table>");	
							
								sb.append("<table id='MaxScoreDiv"+cnt+"' border=0><tr><td style='vertical-align:top;height:33px;padding-left:8px;border:0px;background-color: transparent;'><label><B>Score</B></label></td>");
								sb.append("<td style='padding:5px;vertical-align:bottom;height:33px;border:0px;background-color: transparent;'>&nbsp;");
								sb.append("<iframe id='Q"+cnt+"ifrmStatusNote' src='slideract.do?name=statusNoteFrm&amp;tickInterval="+interval+"&amp;max="+topMaxScore+"&amp;swidth=536&amp;dslider="+dslider+"&amp;svalue="+scoreProvided+"' scrolling='no' frameborder='0' style='border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:580px;margin-top:-11px;'></iframe>");						
								sb.append("</td>");
								sb.append("<td valign=top style='padding-top:4px; padding-left: 17px;border:0px;background-color: transparent;'><input type='hidden' name='Q"+cnt+"dsliderchk' id='Q"+cnt+"dsliderchk'  value="+dslider+">");
								sb.append("</td></tr>");
								sb.append("</table>");
							}
							
						} catch(Exception exception){
							
						}
						
						
						if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel"))
						{
							int  count=0;
							String checkedNC="";
							sb.append("<table>");
							for (DistrictSpecificRefChkOptions questionOptions : questionOptionsList) {
								
								if(headQuarterMaster!=null && headQuarterMaster.getHeadQuarterId()==2)
								{		
									if(count==0)
									{
											if(yesChecked.equalsIgnoreCase("checked"))
											{
												checkedNC="checked";										        	
										   }
									}else if(count==1)
									{
										if(noChecked.equalsIgnoreCase("checked"))
										{								
											checkedNC="checked";									        	
									   }										
									}
								
									sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'>" +
											  "<input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checkedNC+"/></div>" +
											  "<input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
											  "<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
									sb.append("</td></tr>");
									count++;
									checkedNC="";
									
								}
								else
								{
								
								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'>" +
										  "<input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
										  "<input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										  "<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
							    }
							}	
							sb.append("</table>");
							yesChecked="";
							noChecked="";
						}
						
						if(shortName.equalsIgnoreCase("sloet"))
						{
							sb.append("<table>");
							for (DistrictSpecificRefChkOptions questionOptions : questionOptionsList) {
								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'>" +
										  "<input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
										  "<input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										  "<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
							}
							sb.append("</table>");
							
							if(districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionExplanation()!=null)
								sb.append("<div>"+Utility.getUTFToHTML(districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionExplanation())+"</div>");

							sb.append("<BR><B>"+Utility.getLocaleValuePropByKey("msgAdditionalComments", locale)+":</B>&nbsp;&nbsp;<span style=''><textarea  name='Q"+cnt+"opt' id='Q"+cnt+"optet' class='form-control' maxlength='5000' rows='4' style='width:98%;margin-top:5px;'></textarea><span><BR>");
							/*sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							sb.append("document.getElementById('Q"+cnt+"optet').focus();");
							sb.append("</script>");*/
						}
						
						if(shortName.equalsIgnoreCase("et"))
						{
							int  count=0;
							String checkedNC="";
						
						sb.append("<table>");
						for (DistrictSpecificRefChkOptions questionOptions : questionOptionsList) {
							if(optId.equals(questionOptions.getOptionId()))
								checked = "checked";
							else
								checked = "";
							
							if(headQuarterMaster!=null && headQuarterMaster.getHeadQuarterId()==2)
							{		
								if(count==0)
								{
										if(yesChecked.equalsIgnoreCase("checked"))
										{
											checkedNC="checked";										        	
									   }
								}else if(count==1)
								{
									if(noChecked.equalsIgnoreCase("checked"))
									{								
										checkedNC="checked";									        	
								   }										
								}									
								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checkedNC+" /></div>" +
										" <input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
								count++;
								checkedNC="";									
							}
							else
							{									
								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
										" <input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");									
							}								
						}
						sb.append("</table>");

						if(districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionExplanation()!=null)
							sb.append("<div>"+Utility.getUTFToHTML(districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionExplanation())+"</div>");

						sb.append("<BR><B>"+Utility.getLocaleValuePropByKey("msgAdditionalComments", locale)+":</B>&nbsp;&nbsp;<span style=''><textarea  name='Q"+cnt+"opt' id='Q"+cnt+"optet' class='form-control' maxlength='5000' rows='4' style='width:98%;margin-top:5px;'>"+insertedText+"</textarea><span><BR>");
						/*sb.append("<script type=\"text/javascript\" language=\"javascript\">");
						sb.append("document.getElementById('Q"+cnt+"optet').focus();");
						sb.append("</script>");*/
						yesChecked="";
						noChecked="";
						}
						if(shortName.equalsIgnoreCase("ml"))
						{
							sb.append("<BR>&nbsp;&nbsp;<textarea name='Q"+cnt+"opt' id='Q"+cnt+"opt' class='form-control' maxlength='5000' rows='4' style='width:98%;margin-top:5px;'>"+insertedText+"</textarea><BR>");					
							/*sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							sb.append("document.getElementById('Q"+cnt+"opt').focus();");
							sb.append("</script>");*/
						}
						
						if(shortName.equalsIgnoreCase("SLD"))
						{
							sb.append("<BR><B>"+Utility.getLocaleValuePropByKey("msgAdditionalComments", locale)+":</B>&nbsp;&nbsp;<textarea name='Q"+cnt+"opt' id='Q"+cnt+"opt' class='form-control' maxlength='5000' rows='4' style='width:98%;margin-top:5px;'>"+insertedText+"</textarea><BR>");					
							/*sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							sb.append("document.getElementById('Q"+cnt+"opt').focus();");
							sb.append("</script>");*/
						}
						if(shortName.equalsIgnoreCase("mlsel")){
							if(teacherAnswerDetailsForDistrictSpecificQuestion!=null && teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions()!=null){
								selectedOptions = teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions()==null?"":teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions();
							}
							
							String[] multiCounts = selectedOptions.split("\\|");
							int multiCount=1;
							sb.append("<table>");
							
							String ansId="";
							for (DistrictSpecificRefChkOptions questionOptions : questionOptionsList) {
								try{
									ansId="";
									for(int i=0;i<multiCounts.length;i++){
										try{ansId =multiCounts[i];}catch(Exception e){}
										if(ansId!=null && !ansId.equals("") && questionOptions.getOptionId()==Integer.parseInt(ansId)){
											if(ansId!=null && !ansId.equals("")){
												ansId="checked";
											}
											break;
										}
									}
								}catch(Exception e){
									e.printStackTrace();
								}

								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+questionOptions.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+questionOptions.getQuestionOption()+"");
								sb.append("</td></tr>");
								multiCount++;

							}
							sb.append("</table >");

						}
						
						if(shortName.equalsIgnoreCase("mloet")){
							if(teacherAnswerDetailsForDistrictSpecificQuestion!=null && teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions()!=null){
								selectedOptions = teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions()==null?"":teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions();
							}
							String[] multiCounts = selectedOptions.split("\\|");
							int multiCount=1;
							
							sb.append("<table>");
							
							String ansId="";
							for (DistrictSpecificRefChkOptions teacherQuestionOption : questionOptionsList) {
								try{
									ansId="";
									for(int i=0;i<multiCounts.length;i++){
										try{ansId =multiCounts[i];}catch(Exception e){}
										if(ansId!=null && !ansId.equals("") && teacherQuestionOption.getOptionId()==Integer.parseInt(ansId)){
											if(ansId!=null && !ansId.equals("")){
												ansId="checked";
											}
											break;
										}
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' id='multiSelect"+multiCount+"' name='multiSelect"+cnt+"' class='span1' maxlength='1' value='"+teacherQuestionOption.getOptionId()+"'></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
								sb.append("</td></tr>");
								multiCount++;
								
							}
							sb.append("</table >");
							String questionInstructions="";
							try{
								if(districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionInstructions()!=null){
									questionInstructions="&nbsp;&nbsp;"+districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionInstructions();	
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							sb.append(questionInstructions+"</br><B>"+Utility.getLocaleValuePropByKey("msgAdditionalComments", locale)+":</B>&nbsp;&nbsp;<span style=''><textarea  name='Q"+cnt+"optmloet' id='Q"+cnt+"optmloet' class='form-control' maxlength='5000' rows='4' style='width:98%;margin-top:5px;'></textarea><span><BR>");
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							sb.append("</script>");
						}
						if(shortName.equalsIgnoreCase("it"))
						{
							sb.append("<table >");
							for (DistrictSpecificRefChkOptions teacherQuestionOption : questionOptionsList) {
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='optS' value='"+teacherQuestionOption.getOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherQuestionOption.getQuestionOption()+"'> ");
								sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'  /></td></tr>");
							}
							sb.append("</table>");
						}

						sb.append("<input type='hidden' id='Q"+cnt+"questionId' value='"+districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionId()+"'/>");
						sb.append("<input type='hidden' id='Q"+cnt+"questionTypeId' value='"+districtSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
						sb.append("<input type='hidden' id='Q"+cnt+"questionTypeShortName' value='"+shortName+"'/>" );
						sb.append("</td>");
						sb.append("</tr>");
						insertedText = "";
					}
				//sb.append("@##@"+cnt+"@##@"+jobOrder.getDistrictMaster().getTextForDistrictSpecificQuestions());
				sb.append("<input type='hidden' id='totalQuestions' value='"+cnt+"'/>" );
				sb.append("@##@"+cnt);
			} else {
				sb.append("<tr>");
				sb.append("<td width='90%'><h3>");
				sb.append(Utility.getLocaleValuePropByKey("msgTakingTheTimeComplete", locale) +
						Utility.getLocaleValuePropByKey("msgSchoolDistrictPhiladelphia", locale));
				sb.append("<h3></td>");
				sb.append("</tr>");
			}
			
		//////////////////////	
	}catch (Exception e) {
		e.printStackTrace();
	}
	return sb.toString();
  }
	

@Transactional(readOnly=false)
public String setDistrictQuestions(Integer referenceId,Integer headQuarterId, Integer districtId,Integer jobId, Integer teacherId,DistrictSpecificRefChkAnswers[] answerDetailsForDistrictSpecificQuestions,String[] arrUserInfo)
	{
		System.out.println("::::::::: setDistrictQuestions Reference Check ::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		
		TeacherElectronicReferences teacherElectronicReferences = null;
		HeadQuarterMaster headQuarterMaster = null;						// @ Anurag 18 aug
		DistrictMaster districtMaster 	= 	null;
		TeacherDetail teacherDetail		=	null;
		JobOrder jobOrder				=	null;
		teacherElectronicReferences 	= 	teacherElectronicReferencesDAO.findById(referenceId, false, false);
		
		if(districtId!=null && districtId !=0)
		 districtMaster					=	districtMasterDAO.findById(districtId, false, false);
		else if(headQuarterId!=null && headQuarterId !=0)
			headQuarterMaster =  headQuarterMasterDAO.findById(headQuarterId, false, false);
		teacherDetail					=	teacherDetailDAO.findById(teacherId, false, false);
		List<DistrictSpecificRefChkAnswers> districtSpecificRefChkAnswersNCDPI=new ArrayList<DistrictSpecificRefChkAnswers>();
		Map<String, DistrictSpecificRefChkAnswers> answerMap=new HashMap<String, DistrictSpecificRefChkAnswers>();

		try {
			
			int n=1;
			districtSpecificRefChkAnswersNCDPI=districtSpecificRefChkAnswersDAO.recordByTeacherIdAndRef(teacherElectronicReferences.getElerefAutoId(),teacherDetail);
			if(districtSpecificRefChkAnswersNCDPI!=null && districtSpecificRefChkAnswersNCDPI.size()>0)
			{
				for( DistrictSpecificRefChkAnswers answerFromTable:districtSpecificRefChkAnswersNCDPI)
					{							
						answerMap.put(answerFromTable.getElerefAutoId()+"_"+answerFromTable.getTeacherDetail().getTeacherId()+n, answerFromTable);
						n++;
					}
			}	
		
			int inValidCount = 0;
			try{
				if(jobId != null){
					jobOrder = jobOrderDAO.findById(jobId, false, false);
				}
			} catch(Exception exception) {
				exception.printStackTrace();
			}
			String firstName		=	"";
			String lastName			=	"";
			String email			=	"";
			String designation		=	"";
			String organization		=	"";
			String contactnumber	=	"";
			String secondaryPhone	=	"";
			String relationship		=	"";
			String length			=	"";
			String reference		=	"";
			String manager			=	"";
			String userId			=	"";

			try{
				System.out.println("***************  user Info *****&******"+arrUserInfo);
					firstName		=	arrUserInfo[0];
					lastName		=	arrUserInfo[1];
					email			=	arrUserInfo[2];
					designation		=	arrUserInfo[3];
					organization	=	arrUserInfo[4];
					contactnumber	=	arrUserInfo[5];
					secondaryPhone	=	arrUserInfo[6];
					relationship	=	arrUserInfo[7];
					length			=	arrUserInfo[8];
					reference		=	arrUserInfo[9];
					manager			=	arrUserInfo[10];
					try{
					if(arrUserInfo.length>11)	
					userId			=	arrUserInfo[11];
					}catch(ArrayIndexOutOfBoundsException e){e.printStackTrace();}
					System.out.println("userId======================="+userId);

			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			/*
			 * 		Purpose		:	Get List For Ref Question
			 * 
			 */

			List<DistrictSpecificRefChkQuestions> districtSpecificQuestionList =null;
			Map<Integer,DistrictSpecificRefChkQuestions> refQuesMap	= new HashMap<Integer, DistrictSpecificRefChkQuestions>();
			try{
		 
					districtSpecificQuestionList	=	districtSpecificRefChkQuestionsDAO.getDistrictRefChkQuestionBYDistrict(districtMaster,headQuarterMaster);
			} catch (Exception exception){
				exception.printStackTrace();
			}
			
			if(districtSpecificQuestionList!=null){
				try{
					
					for(DistrictSpecificRefChkQuestions refChkRecord : districtSpecificQuestionList){

						refQuesMap.put(refChkRecord.getQuestionId(), refChkRecord);
					}

				} catch(Exception exception){
					exception.printStackTrace();
				}
			}
			UserMaster userMaster=null;
			if(userId!=null && !userId.trim().equalsIgnoreCase("")){
				userMaster=userMasterDAO.findById(Integer.parseInt(userId),false,false);
			}
			Map<Integer,Integer> map = new HashMap<Integer, Integer>();
			List<Integer> answers = new ArrayList<Integer>();
			Integer ansId 			= 	null;
			Integer totalMaxMarks	=	0;
			Integer	totalMinMarks	=	0;
			Integer	questionId		=	null;
			Integer numberOfAttamtQuestion	=	0;
            int nn=1;
			
			SessionFactory sessionFactory=districtSpecificRefChkAnswersDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	 	    Transaction txOpen =statelesSsession.beginTransaction();
	 	    txOpen =statelesSsession.beginTransaction();
			
			for (DistrictSpecificRefChkAnswers answerDetailsForDistrictSpecificQuestion : answerDetailsForDistrictSpecificQuestions) {
				Integer	maxMarks	=	0;
				Integer	minMarks	=	0;
				questionId		=	answerDetailsForDistrictSpecificQuestion.getDistrictSpecificRefChkQuestions().getQuestionId();
				
				/*
				 * 	Get Question Map Values
				 * 
				 */
				
				DistrictSpecificRefChkQuestions	questionRecord = null;
				try {
					if(answerDetailsForDistrictSpecificQuestion.getQuestionMaxScore() != null){
						questionRecord	=	refQuesMap.get(questionId);
						if(questionRecord.getQuestionMaxScore()!=null)
							maxMarks		=	questionRecord.getQuestionMaxScore();
						
						minMarks		=	answerDetailsForDistrictSpecificQuestion.getQuestionMaxScore();
						totalMaxMarks	=	totalMaxMarks+maxMarks;
						
						if(!minMarks.equals(999))
							totalMinMarks	=	totalMinMarks+minMarks;
						
						numberOfAttamtQuestion	=	numberOfAttamtQuestion+1;
					}
					
				} catch(Exception exception) {
					exception.printStackTrace();
				}
				
				DistrictSpecificRefChkAnswers answerDetailsForDistrictSpecificQuestion1=null;				
				String kepMap=referenceId+"_"+teacherId+nn;
							
				if(answerMap.containsKey(kepMap))
				{					
					answerDetailsForDistrictSpecificQuestion1 = answerMap.get(kepMap);
             	}
				
				
				if(answerDetailsForDistrictSpecificQuestion1==null)
				{
				
			answerDetailsForDistrictSpecificQuestion.setTeacherDetail(teacherDetail);
			answerDetailsForDistrictSpecificQuestion.setElerefAutoId(referenceId);
			if(jobOrder != null){
				answerDetailsForDistrictSpecificQuestion.setJobOrder(jobOrder);
			}
			answerDetailsForDistrictSpecificQuestion.setDistrictMaster(districtMaster);
			answerDetailsForDistrictSpecificQuestion.setHeadQuarterMasterId(headQuarterId);
			answerDetailsForDistrictSpecificQuestion.setDistrictSpecificRefChkQuestions(answerDetailsForDistrictSpecificQuestion.getDistrictSpecificRefChkQuestions());
			answerDetailsForDistrictSpecificQuestion.setQuestion(answerDetailsForDistrictSpecificQuestion.getQuestion());
			answerDetailsForDistrictSpecificQuestion.setQuestionTypeMaster(answerDetailsForDistrictSpecificQuestion.getQuestionTypeMaster());
			if(answerDetailsForDistrictSpecificQuestion.getQuestionMaxScore() != null){
				answerDetailsForDistrictSpecificQuestion.setQuestionMaxScore(answerDetailsForDistrictSpecificQuestion.getQuestionMaxScore());
			}
			if(answerDetailsForDistrictSpecificQuestion.getQuestionOption()!=null){
				answerDetailsForDistrictSpecificQuestion.setQuestionOption(answerDetailsForDistrictSpecificQuestion.getQuestionOption());
			}
			answerDetailsForDistrictSpecificQuestion.setSelectedOptions(answerDetailsForDistrictSpecificQuestion.getSelectedOptions());
			answerDetailsForDistrictSpecificQuestion.setQuestionOption(answerDetailsForDistrictSpecificQuestion.getQuestionOption());
			answerDetailsForDistrictSpecificQuestion.setInsertedText(answerDetailsForDistrictSpecificQuestion.getInsertedText());
			answerDetailsForDistrictSpecificQuestion.setIsActive(true);
			answerDetailsForDistrictSpecificQuestion.setCreatedDateTime(new Date());
			answerDetailsForDistrictSpecificQuestion.setAnsweredBy(userMaster);
			statelesSsession.insert(answerDetailsForDistrictSpecificQuestion);
			//districtSpecificRefChkAnswersDAO.makePersistent(answerDetailsForDistrictSpecificQuestion);
			answers.add(answerDetailsForDistrictSpecificQuestion.getAnswerId());
			}
			else
			{ 
				System.out.println("inside update");
				answerDetailsForDistrictSpecificQuestion.setAnswerId(answerMap.get(referenceId+"_"+teacherId+nn).getAnswerId());
				answerDetailsForDistrictSpecificQuestion.setTeacherDetail(teacherDetail);
				answerDetailsForDistrictSpecificQuestion.setElerefAutoId(referenceId);
				if(jobOrder != null){
					answerDetailsForDistrictSpecificQuestion.setJobOrder(jobOrder);
				}
				answerDetailsForDistrictSpecificQuestion.setDistrictMaster(districtMaster);
				answerDetailsForDistrictSpecificQuestion.setHeadQuarterMasterId(headQuarterId);
				answerDetailsForDistrictSpecificQuestion.setDistrictSpecificRefChkQuestions(answerDetailsForDistrictSpecificQuestion.getDistrictSpecificRefChkQuestions());
				answerDetailsForDistrictSpecificQuestion.setQuestion(answerDetailsForDistrictSpecificQuestion.getQuestion());
				answerDetailsForDistrictSpecificQuestion.setQuestionTypeMaster(answerDetailsForDistrictSpecificQuestion.getQuestionTypeMaster());
				if(answerDetailsForDistrictSpecificQuestion.getQuestionMaxScore() != null){
					answerDetailsForDistrictSpecificQuestion.setQuestionMaxScore(answerDetailsForDistrictSpecificQuestion.getQuestionMaxScore());
				}
				if(answerDetailsForDistrictSpecificQuestion.getQuestionOption()!=null){
					answerDetailsForDistrictSpecificQuestion.setQuestionOption(answerDetailsForDistrictSpecificQuestion.getQuestionOption());
				}
				answerDetailsForDistrictSpecificQuestion.setSelectedOptions(answerDetailsForDistrictSpecificQuestion.getSelectedOptions());
				answerDetailsForDistrictSpecificQuestion.setQuestionOption(answerDetailsForDistrictSpecificQuestion.getQuestionOption());
				answerDetailsForDistrictSpecificQuestion.setInsertedText(answerDetailsForDistrictSpecificQuestion.getInsertedText());
				answerDetailsForDistrictSpecificQuestion.setIsActive(true);
				answerDetailsForDistrictSpecificQuestion.setCreatedDateTime(new Date());
				answerDetailsForDistrictSpecificQuestion.setAnsweredBy(userMaster);
				statelesSsession.update(answerDetailsForDistrictSpecificQuestion);
				//districtSpecificRefChkAnswersDAO.makePersistent(answerDetailsForDistrictSpecificQuestion);
				answers.add(answerDetailsForDistrictSpecificQuestion.getAnswerId());
				 nn++;	
		}
		}
		txOpen.commit();
  
			// Update Reference Check History Table
			List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistory = null;
			teacherElectronicReferencesHistory		=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefIdAndDist(teacherElectronicReferences,headQuarterMaster, districtMaster, teacherDetail);

			if(teacherElectronicReferencesHistory!=null && teacherElectronicReferencesHistory.size()>0){
				TeacherElectronicReferencesHistory updateRecord = teacherElectronicReferencesHistoryDAO.findById(teacherElectronicReferencesHistory.get(0).getElerefHAutoId(), false, false);
				updateRecord.setContactStatus(1);
				updateRecord.setEmail(email);
				teacherElectronicReferencesHistoryDAO.makePersistent(updateRecord);
			}else{
				if(!userId.trim().equalsIgnoreCase("")){
					System.out.println(":::::::::::::::: Record Save Successfully ::::::::::::::::");
					TeacherElectronicReferencesHistory createRecord = new  TeacherElectronicReferencesHistory();
					createRecord.setTeacherElectronicReferences(teacherElectronicReferences);
					createRecord.setDistrictMaster(districtMaster);
					createRecord.setHeadQuarterMaster(headQuarterMaster);
					if(jobOrder!=null){
						createRecord.setJobOrder(jobOrder);
					}
					createRecord.setTeacherDetail(teacherElectronicReferences.getTeacherDetail());
					//createRecord.setEmail(teacherElectronicReferences.getEmail());
					createRecord.setContactStatus(1);
					createRecord.setReplyStatus(0);
					createRecord.setEmail(email);
					createRecord.setCreatedDateTime(new Date());
					teacherElectronicReferencesHistoryDAO.makePersistent(createRecord);
				}
			}
			
			/* Update Reference Table */
			
			try{
				teacherElectronicReferences.setFirstName(firstName);
				teacherElectronicReferences.setLastName(lastName);
				teacherElectronicReferences.setEmail(email);
				teacherElectronicReferences.setDesignation(designation);
				teacherElectronicReferences.setOrganization(organization);
				teacherElectronicReferences.setContactnumber(contactnumber);
				teacherElectronicReferencesDAO.makePersistent(teacherElectronicReferences);
				
			} catch(Exception exception){
				exception.printStackTrace();
			}

			/* End Update */
			
			/*
			 * 		Purpose	:	Manipulation for ReferenceCheckFitScore
			 * 		Date	:	03 Jan 2015
			 *		By		:	Hanzala Subhani 
			 * 
			 */
			
			ReferenceCheckFitScore referenceCheckFitScore = new ReferenceCheckFitScore();
			
			List<ReferenceCheckFitScore> referenceCheckFitScoreList = new ArrayList<ReferenceCheckFitScore>(); 
			referenceCheckFitScoreList	=	referenceCheckFitScoreDAO.getRecordsByDistAndTeacherId(headQuarterId,  districtMaster, teacherDetail);
			
			if(referenceCheckFitScoreList.size() == 0){
				
				referenceCheckFitScore.setHeadQuarterId(headQuarterId);
				referenceCheckFitScore.setDistrictMaster(districtMaster);
				referenceCheckFitScore.setTeacherDetail(teacherDetail);
				//referenceCheckFitScore.setJobCategoryMaster(jobCategoryMaster);
				referenceCheckFitScore.setMinScore(totalMinMarks);
				referenceCheckFitScore.setMaxScore(totalMaxMarks);
				referenceCheckFitScoreDAO.makePersistent(referenceCheckFitScore);

			} else {

				try{
					Integer referenceFScoreId	=	referenceCheckFitScoreList.get(0).getReferenceFScoreId();
					referenceCheckFitScore 		=	referenceCheckFitScoreDAO.findById(referenceFScoreId, false, false);

					if(numberOfAttamtQuestion > 0){
						Integer maxFitScore	=	referenceCheckFitScore.getMaxScore();
						Integer minFitScore	=	referenceCheckFitScore.getMinScore();
						totalMaxMarks		= 	(maxFitScore+totalMaxMarks)/2;
						totalMinMarks		=	(minFitScore+totalMinMarks)/2;
						referenceCheckFitScore.setMaxScore(totalMaxMarks);
						referenceCheckFitScore.setMinScore(totalMinMarks);
						referenceCheckFitScoreDAO.makePersistent(referenceCheckFitScore);
					}
				} catch (Exception exception){
					exception.printStackTrace();
				}
			}
			/*
			 *  End Fit Score
			 */
			
			boolean validFlag = false;
			if(inValidCount>0)
				validFlag=false;
			else
				validFlag=true;
		
			return "1";
		
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
   }
	
	@Transactional(readOnly=false)
	public void sendMail(JobForTeacher jobForTeacher)
	{
		try
		{
			boolean startExecution=false;
			
			try
			{
				if(jobForTeacher!=null && jobForTeacher.getJobId()!=null && jobForTeacher.getJobId().getDistrictMaster()!=null )
				{System.out.println("--------------------------------------------");
					System.out.println(jobForTeacher.getStatus().getStatusId()+"\t"+jobForTeacher.getJobId());
					//int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
	               // StatusMaster statusMasterMain = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
	                //System.out.println("statusMasterMain=="+statusMasterMain.getStatus());
				//	String status=candidateGridAjax.getIncompleteInfoHtml(jft.getJobForTeacherId().intValue());
					//System.out.println(status);
					//if(jobForTeacher.getStatus()!=null &&  statusMasterMain!=null && !statusMasterMain.getStatus().equalsIgnoreCase("Completed"))
					{
						if(jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusId()!=null && jobForTeacher.getStatus().getStatusId().equals(new Integer(4)))
						{
							List<JobOrder> list=jobOrderDAO.findJobWithPoolConditionByJobId(jobForTeacher.getJobId().getJobId());
							if(list.size()==0)
							{
								StatusMaster statusMaster=WorkThreadServlet.statusIdMap.get(new Integer(3));
								jobForTeacher.setStatus(statusMaster);
								if(jobForTeacher.getStatusMaster()!=null)
									jobForTeacher.setStatusMaster(statusMaster);
								
								if(statusMaster!=null){
									jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
								}
								//jobForTeacherDAO.makePersistent(jobForTeacher);
							}
						}
					}
					
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
				
			
			
			
			if(jobForTeacher!=null && jobForTeacher.getJobId()!=null && jobForTeacher.getJobId().getDistrictMaster()!=null && jobForTeacher.getJobId().getDistrictMaster().getSendReferenceOnJobComplete()!=null)
			{
				if(jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusId()!=null && jobForTeacher.getStatus().getStatusId().equals(4))
				{
					if(jobForTeacher.getJobId().getDistrictMaster().getSendReferenceOnJobComplete().equals(1))
						startExecution = jobForTeacher.isStatusChange();
					
					jobForTeacher.setJobCompleteDate(new Date());
				}
			}
			else if(jobForTeacher!=null && jobForTeacher.getJobId()!=null && jobForTeacher.getJobId().getHeadQuarterMaster()!=null){
				startExecution = jobForTeacher.isStatusChange();				
				jobForTeacher.setJobCompleteDate(new Date());
			}
			HeadQuarterMaster headQuarterMaster = null;
			DistrictMaster districtMaster=null;
			JobCategoryMaster jobCategoryMaster=null;
			if(startExecution)
			{
				List<ReferenceCheckQuestionSet> questionSetsList=null;	//contains the questionSetID
				ReferenceQuestionSets questionSetId=null;
				List<ReferenceQuestionSetQuestions> noOfQuestionsInQuestionSet=null;
				List<TeacherElectronicReferences> referenceList=null;
				List<TeacherElectronicReferencesHistory> historyList=null;
				
				if(jobForTeacher.getJobId()!=null)	//getting the districtMaster and JobCategory
				{
					if(jobForTeacher.getJobId().getDistrictMaster()!=null)
						districtMaster=jobForTeacher.getJobId().getDistrictMaster();
					else if(jobForTeacher.getJobId().getHeadQuarterMaster()!=null)
						headQuarterMaster = jobForTeacher.getJobId().getHeadQuarterMaster();
					
					if(jobForTeacher.getJobId().getJobCategoryMaster()!=null)
						jobCategoryMaster=jobForTeacher.getJobId().getJobCategoryMaster();
				}
				
				if(districtMaster!=null && jobCategoryMaster!=null)	//getting the List of QuestionSet
					questionSetsList = referenceCheckQuestionSetDAO.getReferenceByDistrictAndJobCategory(districtMaster, jobCategoryMaster);
				else if(headQuarterMaster!=null && jobCategoryMaster!=null)
					questionSetsList = referenceCheckQuestionSetDAO.findByQuestionAndJobCategoryByHQ(headQuarterMaster, jobCategoryMaster);
				
				
				if(questionSetsList!=null && questionSetsList.size()>0)	//getting the no of questions in that QuestionSet
				{
					questionSetId = questionSetsList.get(0).getReferenceQuestionSets();
					noOfQuestionsInQuestionSet = referenceQuestionSetQuestionsDAO.findByQuesSetId(questionSetId, districtMaster , headQuarterMaster);
				}
				
				if(noOfQuestionsInQuestionSet!=null && noOfQuestionsInQuestionSet.size()>0)//If Questions are available in QuestionSet then find the reference List of Teacher and also find the historyList 
				{
					referenceList = teacherElectronicReferencesDAO.findActiveContactedReferencesByTeacher(jobForTeacher.getTeacherId());
					if(referenceList!= null && referenceList.size()>0){
						if(districtMaster!=null)
							historyList = teacherElectronicReferencesHistoryDAO.findByCriteria(Restrictions.and(Restrictions.in("teacherElectronicReferences",referenceList), Restrictions.eq("districtMaster", districtMaster)) );
						else if(headQuarterMaster!=null)
							historyList = teacherElectronicReferencesHistoryDAO.findByCriteria(Restrictions.and(Restrictions.in("teacherElectronicReferences",referenceList), Restrictions.eq("headQuarterMaster", headQuarterMaster)) );
						historyList = (historyList==null)? new ArrayList<TeacherElectronicReferencesHistory>() : historyList;
					}
				}
				
				HttpServletRequest request=null;
				if(WebContextFactory.get()!=null)
					request = WebContextFactory.get().getHttpServletRequest();
				else
					request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
				
				//generating the base url for sending the mail.
				String path = request.getContextPath();
				int portNo = request.getServerPort();
				String showPortNo = null;
				if(portNo==80)
					showPortNo="";
				else
					showPortNo="";
				String baseUrl = request.getScheme()+"://"+request.getServerName()+""+showPortNo+path+"/";
				
				//send the mail If teacher has some references and don't send mail to reference which is available in history List
				if(referenceList!=null && referenceList.size()>0)
				{
					Integer jobCategoryId = questionSetsList.get(0).getJobCategoryMaster().getJobCategoryId();
					for(TeacherElectronicReferences reference : referenceList)
					{
						boolean sendEmail=true;
						boolean createHistory=true;
						for(TeacherElectronicReferencesHistory history : historyList)	//checking that reference is available in history or not
						{
							if( reference.getElerefAutoId().equals( history.getTeacherElectronicReferences().getElerefAutoId() ))	//checking that reference is exist in history 
							{
								//if(history.getJobOrder().getJobCategoryMaster().getJobCategoryId().equals(jobCategoryId))	//checking that history has same jobcategory as QuestionSet has. 
								//{
									createHistory=false;	//If available in history then dont't create the history
									sendEmail=false;
									break;
								//}
							}
						}
						System.out.println("createHistory:- "+createHistory+", sendEmail:- "+sendEmail+", EmailId:- "+reference.getEmail());
						
						if(sendEmail)	//Send the mail to reference.
						{
							String eId		=	Utility.encryptNo(reference.getElerefAutoId());
							String dId = "";
							String hId = "";
							String urlString = "";
							String jId		=	Utility.encryptNo(jobForTeacher.getJobId().getJobId());
							String tId		=	Utility.encryptNo(reference.getTeacherDetail().getTeacherId());
							if(districtMaster!=null){
								dId		=	Utility.encryptNo(jobForTeacher.getJobId().getDistrictMaster().getDistrictId());
								urlString 		= 	"eId="+eId+"&dId="+dId+"&hId=&jId="+jId+"&tId="+tId+"";
							}
							else if(headQuarterMaster!=null){
								hId		=	Utility.encryptNo(jobForTeacher.getJobId().getHeadQuarterMaster().getHeadQuarterId());
								urlString 		= 	"eId="+eId+"&dId=&hId="+hId+"&jId="+jId+"&tId="+tId+"";
							}
							
							System.out.println(" urlString :::: "+urlString);
							
							String mainURLEncode	=	baseUrl+"referencecheck.do?p="+Utility.encodeInBase64(urlString);
							
							String mailContent = MailText.ReferenceCheckMail(reference, jobForTeacher.getJobId(), reference.getTeacherDetail(), districtMaster, mainURLEncode);
							System.out.println("::::::: >>>>>>>> ::::::::: "+mailContent);
							emailerService.sendMailAsHTMLText(reference.getEmail(),"Reference Mail", mailContent);
						}
						
						if(createHistory) //After sending the mail just create the history
						{
							TeacherElectronicReferencesHistory teacherElectronicReferencesHistory = new  TeacherElectronicReferencesHistory();
							teacherElectronicReferencesHistory.setTeacherElectronicReferences(reference);
							teacherElectronicReferencesHistory.setDistrictMaster(districtMaster);
							if(jobForTeacher.getJobId()!=null)
								teacherElectronicReferencesHistory.setJobOrder(jobForTeacher.getJobId());
							if(jobForTeacher!=null)
								teacherElectronicReferencesHistory.setHeadQuarterMaster(jobForTeacher.getJobId().getHeadQuarterMaster());
							teacherElectronicReferencesHistory.setTeacherDetail(reference.getTeacherDetail());
							teacherElectronicReferencesHistory.setEmail(reference.getEmail());
							teacherElectronicReferencesHistory.setContactStatus(0);
							teacherElectronicReferencesHistory.setReplyStatus(0);
							teacherElectronicReferencesHistory.setCreatedDateTime(new Date());
							teacherElectronicReferencesHistoryDAO.makePersistent(teacherElectronicReferencesHistory);
						}
					}
				}//End of reference List Check
			}//End of JobForTeacherList Check
		}	//End of try
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}