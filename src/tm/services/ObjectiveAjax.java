package tm.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.CityMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.ObjectiveMaster;
import tm.bean.master.CompetencyMaster;
import tm.bean.user.UserMaster;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.ObjectiveMasterDAO;
import tm.dao.master.CompetencyMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.utility.Utility;

public class ObjectiveAjax 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	//String locale = Utility.getValueOfPropByKey("locale");
	
	  String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	  String lblDomName=Utility.getLocaleValuePropByKey("lblDomName", locale);
	  String lblCompNm=Utility.getLocaleValuePropByKey("lblCompNm", locale);
	  String lblObjectiveName1=Utility.getLocaleValuePropByKey("lblObjectiveName1", locale);
	  String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
      String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
      String lblNoObjectivefound1=Utility.getLocaleValuePropByKey("lblNoObjectivefound1", locale);
      String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
      String lnkV=Utility.getLocaleValuePropByKey("lnkV", locale);
      String optAct=Utility.getLocaleValuePropByKey("optAct", locale);
      String optInActiv=Utility.getLocaleValuePropByKey("optInActiv", locale);
      String lblDeactivate=Utility.getLocaleValuePropByKey("lblDeactivate", locale);
      String lblActivate=Utility.getLocaleValuePropByKey("lblActivate", locale);
      String lblSelectCompetency1=Utility.getLocaleValuePropByKey("lblSelectCompetency1", locale);

      
	  
	@Autowired
	private ObjectiveMasterDAO objectiveMasterDAO;
	
	public void setObjectiveMasterDAO(ObjectiveMasterDAO objectiveMasterDAO) 
	{
		this.objectiveMasterDAO = objectiveMasterDAO;
	}
	
	@Autowired
	private CompetencyMasterDAO competencyMasterDAO;
	public void setCompetencyMasterDAO(CompetencyMasterDAO competencyMasterDAO) {
		this.competencyMasterDAO = competencyMasterDAO;
	}
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) {
		this.domainMasterDAO = domainMasterDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	public String DisplayObjectiveRecords(String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		StringBuffer dmRecords =	new StringBuffer();
		try{
			System.out.println("calling DisplayObjectiveRecords ");
			//-- get no of record in grid,
			//-- set start and end position
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	=	0;
			//------------------------------------
			
			System.out.println("start"+start);
			System.out.println("end"+end);
			
			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,19,"objective.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			/***** Sorting by Domain Name,Competency Name and Objective Name Stating ( Sekhar ) ******/
			SortedMap map = new TreeMap();
			List<ObjectiveMaster> lstObjectiveMaster	  	=	null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"objectiveName";
			String sortOrderNoField		=	"objectiveName";
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("domainName")&& !sortOrder.equals("competencyName")){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("domainName"))
				 {
					 sortOrderNoField="domainName";
				 }
				 if(sortOrder.equals("competencyName"))
				 {
					 sortOrderNoField="competencyName";
				 }
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			List<ObjectiveMaster> reslstObjectiveMaster			=	new ArrayList<ObjectiveMaster>();
			lstObjectiveMaster  								= 	objectiveMasterDAO.findByCriteria(sortOrderStrVal);
			List<ObjectiveMaster> sortedlstObjectiveMaster		=	new ArrayList<ObjectiveMaster>();
			
			SortedMap<String,ObjectiveMaster>	sortedMap = new TreeMap<String,ObjectiveMaster>();
			if(sortOrderNoField.equals("domainName"))
			{
				sortOrderFieldName	=	"domainName";
			}
			if(sortOrderNoField.equals("competencyName"))
			{
				sortOrderFieldName	=	"competencyName";
			}
			int mapFlag=2;
			for (ObjectiveMaster objectiveMaster : lstObjectiveMaster){
				String orderFieldName=objectiveMaster.getObjectiveName();
				if(sortOrderFieldName.equals("domainName")){
					orderFieldName=objectiveMaster.getCompetencyMaster().getDomainMaster().getDomainName()+"||"+objectiveMaster.getObjectiveId();
					sortedMap.put(orderFieldName+"||",objectiveMaster);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("competencyName")){
					orderFieldName=objectiveMaster.getCompetencyMaster().getCompetencyName()+"||"+objectiveMaster.getObjectiveId();
					sortedMap.put(orderFieldName+"||",objectiveMaster);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstObjectiveMaster.add((ObjectiveMaster) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstObjectiveMaster.add((ObjectiveMaster) sortedMap.get(key));
				}
			}else{
				sortedlstObjectiveMaster=lstObjectiveMaster;
			}
			
			totalRecord =sortedlstObjectiveMaster.size();

			if(totalRecord<end)
				end=totalRecord;
			List<ObjectiveMaster> lstsortedObjectiveMaster		=	sortedlstObjectiveMaster.subList(start,end);
			
			
			/***** Sorting by Domain Name,Competency Name and Objective Name  End ******/
	
			dmRecords.append("<table  id='objectiveTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			//dmRecords.append("<th width='26%'>Domain Name</th>");
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblDomName,sortOrderFieldName,"domainName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='26%' valign='top'>"+responseText+"</th>");
			
			//dmRecords.append("<th width='27%'>Competency Name</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblCompNm,sortOrderFieldName,"competencyName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='27%' valign='top'>"+responseText+"</th>");
			
			//dmRecords.append("<th width='27%'>Objective Name</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblObjectiveName1,sortOrderFieldName,"objectiveName",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='27%' valign='top'>"+responseText+"</th>");
			
			//dmRecords.append("<th width='7%'>Status</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			dmRecords.append("<th width='7%' valign='top'>"+responseText+"</th>");
			
			dmRecords.append("<th width='13%'>"+lblAct+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			/*================= Checking If Record Not Found ======================*/
			if(lstsortedObjectiveMaster.size()==0)
				dmRecords.append("<tr><td colspan='6' align='center'>"+lblNoObjectivefound1+"</td></tr>" );
			System.out.println("No of Records "+lstsortedObjectiveMaster.size());

			for (ObjectiveMaster objectiveMasterDetail : lstsortedObjectiveMaster) 
			{
				if(objectiveMasterDetail.getCompetencyMaster().getStatus().equalsIgnoreCase("A"))
				{
					if(objectiveMasterDetail.getCompetencyMaster().getDomainMaster().getStatus().equalsIgnoreCase("A"))
					{
						dmRecords.append("<tr>" );
						dmRecords.append("<td>"+objectiveMasterDetail.getCompetencyMaster().getDomainMaster().getDomainName()+"</td>");
						dmRecords.append("<td>"+objectiveMasterDetail.getCompetencyMaster().getCompetencyName()+"</td>");
						dmRecords.append("<td>"+objectiveMasterDetail.getObjectiveName()+"</td>");
						dmRecords.append("<td>");
						if(objectiveMasterDetail.getStatus().equalsIgnoreCase("A"))
							dmRecords.append(optAct);
						else
							dmRecords.append(optInActiv);
						dmRecords.append("</td>");
						dmRecords.append("<td>");
						
						boolean pipeFlag=false;
						if(roleAccess.indexOf("|2|")!=-1){
							dmRecords.append("<a href='javascript:void(0);' onclick='return editObjective("+objectiveMasterDetail.getObjectiveId()+")'>"+lblEdit+"</a>");
							pipeFlag=true;
						}else if(roleAccess.indexOf("|4|")!=-1){
							dmRecords.append("<a href='javascript:void(0);' onclick='return editObjective("+objectiveMasterDetail.getObjectiveId()+")'>"+lnkV+"</a>");
							pipeFlag=true;
						}
						if(roleAccess.indexOf("|7|")!=-1){
							if(pipeFlag)dmRecords.append(" | ");
							if(objectiveMasterDetail.getStatus().equalsIgnoreCase("A"))
								dmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateObjective("+objectiveMasterDetail.getObjectiveId()+",'I')\">"+lblDeactivate+"");
							else
								dmRecords.append("<a href='javascript:void(0);' onclick=\"return activateDeactivateObjective("+objectiveMasterDetail.getObjectiveId()+",'A')\">"+lblActivate+"");
						}else{
							dmRecords.append("&nbsp;");
						}
						dmRecords.append("</td>");
						dmRecords.append("</tr>");
					}
				}
			}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			//System.out.println(dmRecords.toString());
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	/* =================      saveObjective        =========================*/
	public int saveObjective(Integer domainId,Integer competencyId,Integer objectiveId,String objectiveName,String objectiveStatus,String objectiveUId )
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		ObjectiveMaster objectiveMaster	=	new ObjectiveMaster();
		try
		{
			if(objectiveId	!=	null)
			{	
				//objectiveMaster.setObjectiveId(objectiveId);
				objectiveMaster		=	objectiveMasterDAO.findById(objectiveId, false, false);
			}
			else
			{
				objectiveMaster.setObjectiveStrength("");
				objectiveMaster.setObjectiveOpportunity("");
			}
			System.out.println("domainId "+domainId+" competencyId "+competencyId+" objectiveId "+objectiveId+" objectiveName "+objectiveName+" objectiveStatus "+objectiveStatus);
			CompetencyMaster competencyMaster = competencyMasterDAO.findById(competencyId, false, false);
			/*========= It will set competency Id ==========*/
			objectiveMaster.setCompetencyMaster(competencyMaster);
			if(objectiveUId!=null && !objectiveUId.equals(""))
				objectiveUId = objectiveUId.toUpperCase();
			objectiveMaster.setObjectiveUId(objectiveUId);
			objectiveMaster.setObjectiveName(objectiveName);
			objectiveMaster.setStatus(objectiveStatus);

			/*================ Check For Unique Objective =======================*/
			List<ObjectiveMaster> dupobjectiveMaster	=	objectiveMasterDAO.checkDuplicateObjectiveName(competencyMaster,objectiveName,objectiveId);
			int size 									=	dupobjectiveMaster.size();
			if(size>0)
				return 3;
			
			List<ObjectiveMaster> objectiveMasterList = null;
			if(competencyMaster!=null && objectiveId==null)
				objectiveMasterList = objectiveMasterDAO.findObjectivesByCompetencyAndUId(competencyMaster, objectiveUId);
			if(objectiveMasterList!=null && objectiveMasterList.size()>0)
				return 4;//if duplicate objectiveUId
			
			objectiveMasterDAO.makePersistent(objectiveMaster);
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 2;
		}

		return 1;
	}
	/* ===============      Activate Deactivate Objective        =========================*/
	@Transactional(readOnly=false)
	public boolean activateDeactivateObjective(int competencyId,String status)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }

		try
		{
			ObjectiveMaster objectiveMaster				=	objectiveMasterDAO.findById(competencyId, false, false);
			objectiveMaster.setStatus(status);
			objectiveMasterDAO.makePersistent(objectiveMaster);
			
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/* ===============    Edit Objective Functionality        =========================*/
	@Transactional(readOnly=false)
	public ObjectiveMaster getObjectiveById(int objectiveId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		
		ObjectiveMaster objectiveMaster =	null;
		try
		{
			objectiveMaster	= objectiveMasterDAO.findById(objectiveId, false, false);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		return objectiveMaster;
	}
	
	public String getCompetenciesByDomainId(int domainId,int competencyId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		List<CompetencyMaster> competencyMasters = null;
		StringBuffer cpOptions 				=	new StringBuffer();
		try{
			DomainMaster domainMaster 		= 	domainMasterDAO.findById(domainId, false, false);
			competencyMasters = competencyMasterDAO.getCompetencesByDomain(domainMaster);
			System.out.println("/*========= competencyMasters List  ==========*/"+competencyMasters+" competencyMasters Size \n"+competencyMasters.size());
			System.out.println("No of Records "+competencyMasters.size());

			cpOptions.append("<select class='form-control ' name='competencyMaster' id='competencyMaster'>" );
			cpOptions.append("<option value='0'>"+lblSelectCompetency1+"</option>");
				for (CompetencyMaster competencyMastersDetail : competencyMasters) 
				{
					if(competencyMastersDetail.getCompetencyId().equals(competencyId))
					{
						cpOptions.append("<option id='"+competencyMastersDetail.getCompetencyId()+"' value='"+competencyMastersDetail.getCompetencyId()+"'selected='selected' >"+competencyMastersDetail.getCompetencyName()+"</option>");
					}
					else
					{
						cpOptions.append("<option id='"+competencyMastersDetail.getCompetencyId()+"' value='"+competencyMastersDetail.getCompetencyId()+"' >"+competencyMastersDetail.getCompetencyName()+"</option>");
					}
				}
			cpOptions.append("</select>");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return cpOptions.toString();
	}
}
