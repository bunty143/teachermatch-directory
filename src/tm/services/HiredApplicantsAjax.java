package tm.services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.utility.DistrictNameCompratorASC;
import tm.utility.DistrictNameCompratorDESC;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
public class HiredApplicantsAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;

	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;

	@Autowired
	private DistrictMasterDAO 	districtMasterDAO;

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	
	 public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
		{
			/* ========  For Session time Out Error =========*/
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
			List<DistrictSchools> fieldOfSchoolList1 = null;
			List<DistrictSchools> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
				if(SchoolName.length()>0){
					if(districtIdForSchool==0){
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
							Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}else{
						DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
						Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
						
						if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
							Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
							fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
							
							schoolMasterList.addAll(fieldOfSchoolList1);
							schoolMasterList.addAll(fieldOfSchoolList2);
							Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
							schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
						}else{
							fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
							schoolMasterList.addAll(fieldOfSchoolList1);
						}
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return schoolMasterList;
			
		}
	/**********************************SWADESH *******************************************/
	public String displayRecordsByEntityType(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate)
	 {
			System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			int sortingcheck=1;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				/******************SWADESH********************/
				Date hiredStartdate=null;
				Date hiredEndDtae=null;
				boolean epiDateFlag=false;
				List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
				try{
					if(!startDate.equals("")){
						hiredStartdate=Utility.getCurrentDateFormart(startDate);
						epiDateFlag=true;
					}
					if(!endDate.equals("")){
						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(Utility.getCurrentDateFormart(endDate));
						cal2.add(Calendar.HOUR,23);
						cal2.add(Calendar.MINUTE,59);
						cal2.add(Calendar.SECOND,59);
						hiredEndDtae=cal2.getTime();
						epiDateFlag=true;
					}
					
				}catch(Exception e){
					e.printStackTrace();
				}
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				//-- get no of record in grid,
				//-- set start and end position

					int noOfRowInPage = Integer.parseInt(noOfRow);
					int pgNo = Integer.parseInt(pageNo);
					int start =((pgNo-1)*noOfRowInPage);
					int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
					int totalRecord = 0;
					//------------------------------------
				 /** set default sorting fieldName **/
					String sortOrderFieldName="lastName";
					String sortOrderNoField="lastName";

					if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("lastName"))
						sortingcheck=1;
					else if(sortOrder.equalsIgnoreCase("jobTitle"))
						sortingcheck=2;
					else  if(sortOrder.equalsIgnoreCase("requisitionNumber")){
						sortingcheck=3;
					}else
						sortingcheck=4;
					
					/**Start set dynamic sorting fieldName **/
					
					Order  sortOrderStrVal=null;

					if(sortOrder!=null){
						if(!sortOrder.equals("") && !sortOrder.equals(null)){
							sortOrderFieldName=sortOrder;
							sortOrderNoField=sortOrder;
						}
					}

					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
						if(sortOrderType.equals("0")){
							sortOrderStrVal=Order.asc(sortOrderFieldName);
						}else{
							sortOrderTypeVal="1";
							sortOrderStrVal=Order.desc(sortOrderFieldName);
						}
					}else{
						sortOrderTypeVal="0";
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<JobForTeacher> lstJobForTeachers =new ArrayList<JobForTeacher>();
					
					if(entityID==2)
					{
					 lstJobForTeachers = jobForTeacherDAO.hiredCandidateListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,hiredStartdate,hiredEndDtae); 
					}
					else if(entityID==1) 
					{
						if(districtOrSchoolId!=0)
							 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
						lstJobForTeachers = jobForTeacherDAO.hiredCandidateListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,hiredStartdate,hiredEndDtae);	 
					}
				
					System.out.println("=====================lstForTeachers.size() :: "+lstJobForTeachers.size());
					
				 List<TeacherDetail> listTeacherDetails= new ArrayList<TeacherDetail>();
				 List<JobOrder> lstJobOrderss=new ArrayList<JobOrder>();
				 
				 List<TeacherNormScore> listTeacherNormScores = new  ArrayList<TeacherNormScore>();
		     	 Map<Integer,TeacherNormScore> mapNormScores =new HashMap<Integer, TeacherNormScore>();
			     Map<String,DistrictRequisitionNumbers> mapDistrictRequisitionNumbers =new HashMap<String, DistrictRequisitionNumbers>();
				 Map<String,JobRequisitionNumbers> mapJobRequisitionNumbers= new HashMap<String, JobRequisitionNumbers>();
			     if(lstJobForTeachers!=null && lstJobForTeachers.size()>0)
				 {
			    	 List<String>reqList =new ArrayList<String>();
			    	 
					 for(JobForTeacher jft :  lstJobForTeachers)
					 {
					  listTeacherDetails.add(jft.getTeacherId());
					  lstJobOrderss.add(jft.getJobId());
					  if(jft.getRequisitionNumber()!=null)
						 reqList.add(jft.getRequisitionNumber());
					 }
					 
					 listTeacherNormScores = teacherNormScoreDAO.findTeacersNormScoresList(listTeacherDetails);
					 for(TeacherNormScore tns:listTeacherNormScores)
				 	 {
					  mapNormScores.put(tns.getTeacherDetail().getTeacherId(),tns);
				 	 }
					 
					 List<DistrictRequisitionNumbers> districtRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>();
					 List<DistrictRequisitionNumbers> tempRequisitionNumbers     = new ArrayList<DistrictRequisitionNumbers>(); 
					 districtRequisitionNumbers=districtRequisitionNumbersDAO.getRequisitionNumbersWithAndWithoutDistrict(reqList,districtMaster);
					 List<String> duplicateKeySet =new ArrayList<String>();
					 for(DistrictRequisitionNumbers distRequisitionNumbers : districtRequisitionNumbers)
					   {
						 if(mapDistrictRequisitionNumbers.get(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber())==null)
						 {
							 mapDistrictRequisitionNumbers.put(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber(), distRequisitionNumbers);	 
						 }
						 else
						 {
							duplicateKeySet.add(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber());
							tempRequisitionNumbers.add(mapDistrictRequisitionNumbers.get(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber()));
							tempRequisitionNumbers.add(distRequisitionNumbers);
							//mapDistrictRequisitionNumbers.remove(requisitionNumbers.getDistrictMaster().getDistrictId()+"#"+requisitionNumbers.getRequisitionNumber());
						 }
					  }
					 for(String dkey : duplicateKeySet){
						 mapDistrictRequisitionNumbers.remove(dkey);
					 }
					 List<JobRequisitionNumbers> lstJobRequisitionNumbers = new ArrayList<JobRequisitionNumbers>();
					 
				     if(tempRequisitionNumbers!=null && tempRequisitionNumbers.size()>0){
					   lstJobRequisitionNumbers=jobRequisitionNumbersDAO.findJobRequsition(tempRequisitionNumbers);
				     }
				     for(JobRequisitionNumbers jrNumber : lstJobRequisitionNumbers){
					   mapJobRequisitionNumbers.put(jrNumber.getDistrictRequisitionNumbers().getDistrictMaster().getDistrictId()+"#"+jrNumber.getDistrictRequisitionNumbers().getRequisitionNumber()+"#"+jrNumber.getJobOrder().getJobId(), jrNumber);
				     }
				   
				 }
				 
			     if(schoolId!=0)
				 {
			    	 List<JobForTeacher> filterSchoolJobForTeachers= new ArrayList<JobForTeacher>();
					 for(JobForTeacher jft : lstJobForTeachers)
					 {
						 if(jft.getSchoolMaster()!=null)
						  if(jft.getSchoolMaster().getSchoolId()==schoolId){
							  filterSchoolJobForTeachers.add(jft);
						 }
					 } 
					 lstJobForTeachers.clear();	 
					 lstJobForTeachers.addAll(filterSchoolJobForTeachers);
				}	 
				
				 List<JobForTeacher> finalJobForTeachers =new ArrayList<JobForTeacher>(); 
			
				// Manual sorting logic start 
				// Norm Score Sorting     
				 if(sortOrder.equals("normScore"))
				 {
					for(JobForTeacher jft : lstJobForTeachers){
						if(mapNormScores.get(jft.getTeacherId().getTeacherId()) != null){
							jft.setNormScore(Double.valueOf(mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+""));
						}else{
							jft.setNormScore(-0.0);
						}
					}
					 if(sortOrderType.equals("0"))
						 Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorNormScore);
					 else 
						 Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorNormScoreDesc);
			 	}  
				 
				// schoolName
				 if(sortOrder.equals("schoolName"))
				 {
					 for(JobForTeacher jft : lstJobForTeachers){
						 if(jft.getSchoolMaster()!=null){
						      jft.setDistName(jft.getSchoolMaster().getSchoolName());
							}else{
								jft.setDistName("");
							 }
					 }
					 if(sortOrderType.equals("0"))
						  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
					 else 
					      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
				 }
				 // Teacher Title
				 if(sortingcheck==1)
				 {
					 for(JobForTeacher jft : lstJobForTeachers){
						 jft.setDistName(jft.getTeacherId().getLastName());
					 }
					 if(sortOrderType.equals("0"))
						  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
					 else 
					      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
				 }
				// JobTitle Name
				 if(sortOrder.equals("jobTitle"))
				 {
					 for(JobForTeacher jft : lstJobForTeachers){
						 jft.setDistName(jft.getJobId().getJobTitle());
					 }
					 if(sortOrderType.equals("0"))
						  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
					 else 
					      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
				 }
				 
				 /****BY DATE********SWADESH*************************************/
				 if(sortOrder.equals("byDate"))
				 {
					 for(JobForTeacher jft : lstJobForTeachers){
						 jft.setHiredByDate(jft.getHiredByDate());
					 }
					 if(sortOrderType.equals("0"))
						  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
					 else 
					      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
				 }
				 
				 /*************************************************/
				// Region Name
				 if(sortOrder.equals("regionName"))
				 {
					 for(JobForTeacher jft : lstJobForTeachers){
						 if(jft.getSchoolMaster()!=null){
							 if(jft.getSchoolMaster().getDivision()!=null)
						       jft.setDistName(jft.getSchoolMaster().getDivision());
							 else
								 jft.setDistName("");
							}else{
								jft.setDistName("");
							 }
					 }
					 if(sortOrderType.equals("0"))
						  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
					 else 
					      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
				 }
				 
				 if(sortOrder.equals("ftpt"))
				 {
					 for(JobForTeacher jft : lstJobForTeachers){
					 if(jft.getRequisitionNumber()!=null)
					  {
						 if(mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber())!=null)
						  {
							String posType;
							String originalPosType=mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()).getPosType();
							if(originalPosType.equalsIgnoreCase("P"))
								posType="Part Time";
							else
								posType="Full Time";
							jft.setDistName(posType);
						  }
					  else if(mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId())!=null) {
						  String posType;
							String originalPosType=mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId()).getDistrictRequisitionNumbers().getPosType();
							if(originalPosType.equalsIgnoreCase("P"))
								posType="Part Time";
							else
								posType="Full Time";
							jft.setDistName(posType);
					  }else{
						  jft.setDistName("");
						}
				  }
				 else{
					 jft.setDistName("");
					}
					 }
					 if(sortOrderType.equals("0"))
						  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
					 else 
					      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC()); 
				 }
				 
				 
			   totalRecord=lstJobForTeachers.size();
				if(totalRecord<end)
					end=totalRecord;
				finalJobForTeachers	=	lstJobForTeachers.subList(start,end);
				
				
				String responseText="";
				
				tmRecords.append("<table  id='tblGridHired' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblApplicantName", locale),sortOrderNoField,"lastName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
        
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderNoField,"jobTitle",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblEPINormScore", locale),sortOrderNoField,"normScore",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDateHired", locale),sortOrderNoField,"byDate",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgPositionNumber", locale),sortOrderNoField,"requisitionNumber",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgHiredforSchool", locale),sortOrderNoField,"schoolName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgRegionName", locale),sortOrderNoField,"regionName",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgFT/PT", locale),sortOrderFieldName,"ftpt",sortOrderTypeVal,pgNo);
				tmRecords.append("<th valign='top'>"+responseText+"</th>");
				
			
				tmRecords.append("</tr>");
				tmRecords.append("</thead>");
				tmRecords.append("<tbody>");
				
				if(finalJobForTeachers.size()==0){
				 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
				}
			
				if(finalJobForTeachers.size()>0){
				 for(JobForTeacher jft:finalJobForTeachers) 
				 {
				  tmRecords.append("<tr>");	
					
					tmRecords.append("<td>"+jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")"+"</td>");
					tmRecords.append("<td>"+jft.getJobId().getJobTitle()+"</td>");
					
					//Norm Score Sorting
					if(mapNormScores.get(jft.getTeacherId().getTeacherId())!=null){
					 tmRecords.append("<td>"+mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+"</td>");
					}else{
						tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					}
					
					////HIERED BY DATE @SWADESH 
					if(jft.getHiredByDate()!=null)
					{
					tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getHiredByDate())+"</td>");
					}else{
						tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					}
					
					//for position Number
					
					 if(jft.getRequisitionNumber()!=null)
					 {
					  tmRecords.append("<td>"+jft.getRequisitionNumber()+"</td>");
					 }else{
						tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					  }
					
					//for School
						if(jft.getSchoolMaster()!=null){
							String schoolAndLocCode="";
							if(jft.getSchoolMaster().getLocationCode()!=null && jft.getSchoolMaster().getLocationCode().length()>0){
								schoolAndLocCode+="("+jft.getSchoolMaster().getLocationCode()+")&nbsp;";
							}
							schoolAndLocCode+=jft.getSchoolMaster().getSchoolName();
					       tmRecords.append("<td>"+schoolAndLocCode+"</td>");
					       if(jft.getSchoolMaster().getDivision()!=null)
					    	   tmRecords.append("<td>"+jft.getSchoolMaster().getDivision()+"</td>");
					       else
					    	   tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
						}else{
						  tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
						  tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
						 }
						
					//for FT/PT
					
						if(jft.getRequisitionNumber()!=null)
						  {
							 if(mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber())!=null)
							  {
								String posType;
								String originalPosType=mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()).getPosType();
								if(originalPosType.equalsIgnoreCase("P"))
									posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
								else
									posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
								
								tmRecords.append("<td>"+posType+"</td>");
							  }
							  else if(mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId())!=null) {
								  String posType;
									String originalPosType=mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId()).getDistrictRequisitionNumbers().getPosType();
									if(originalPosType.equalsIgnoreCase("P"))
										posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
									else
										posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
									tmRecords.append("<td>"+posType+"</td>");
							  }else{
								  tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
								}
						  }
						 else{
							 tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
							}
					}
					
				  tmRecords.append("</tr>");
				 }
		

			tmRecords.append("</tbody>");
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));

			}catch(Exception e){
				e.printStackTrace();
			}
			
		 return tmRecords.toString();
	  }
		
	public String displayRecordsByEntityTypeEXL(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = null;
		int sortingcheck=1;
		try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
		}
		
		/******************SWADESH********************/
		Date hiredStartdate=null;
		Date hiredEndDtae=null;
		boolean epiDateFlag=false;
		try{
			if(!startDate.equals("")){
				hiredStartdate=Utility.getCurrentDateFormart(startDate);
				epiDateFlag=true;
			}
			if(!endDate.equals("")){
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(Utility.getCurrentDateFormart(endDate));
				cal2.add(Calendar.HOUR,23);
				cal2.add(Calendar.MINUTE,59);
				cal2.add(Calendar.SECOND,59);
				hiredEndDtae=cal2.getTime();
				epiDateFlag=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
		 /** set default sorting fieldName **/
			String sortOrderFieldName="lastName";
			String sortOrderNoField="lastName";

			if(sortOrder.equalsIgnoreCase("") || sortOrder.equalsIgnoreCase("lastName"))
				sortingcheck=1;
			else if(sortOrder.equalsIgnoreCase("jobTitle"))
				sortingcheck=2;
			else  if(sortOrder.equalsIgnoreCase("requisitionNumber")){
				sortingcheck=3;
			}else
				sortingcheck=4;
			
			/**Start set dynamic sorting fieldName **/
			
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
		
		//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
			List<JobForTeacher> lstJobForTeachers =new ArrayList<JobForTeacher>();
			
			if(entityID==2)
			{
			 lstJobForTeachers = jobForTeacherDAO.hiredCandidateListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,hiredStartdate,hiredEndDtae); 
			}
			else if(entityID==1) 
			{
				if(districtOrSchoolId!=0)
					 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
				lstJobForTeachers = jobForTeacherDAO.hiredCandidateListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,hiredStartdate,hiredEndDtae);	 
			}
		
			System.out.println("=====================lstForTeachers.size() :: "+lstJobForTeachers.size());
			
		 List<TeacherDetail> listTeacherDetails= new ArrayList<TeacherDetail>();
		 List<JobOrder> lstJobOrderss=new ArrayList<JobOrder>();
		 
		 List<TeacherNormScore> listTeacherNormScores = new  ArrayList<TeacherNormScore>();
     	 Map<Integer,TeacherNormScore> mapNormScores =new HashMap<Integer, TeacherNormScore>();
	     Map<String,DistrictRequisitionNumbers> mapDistrictRequisitionNumbers =new HashMap<String, DistrictRequisitionNumbers>();
		 Map<String,JobRequisitionNumbers> mapJobRequisitionNumbers= new HashMap<String, JobRequisitionNumbers>();
	     if(lstJobForTeachers!=null && lstJobForTeachers.size()>0)
		 {
	    	 List<String>reqList =new ArrayList<String>();
	    	 
			 for(JobForTeacher jft :  lstJobForTeachers)
			 {
			  listTeacherDetails.add(jft.getTeacherId());
			  lstJobOrderss.add(jft.getJobId());
			  if(jft.getRequisitionNumber()!=null)
				 reqList.add(jft.getRequisitionNumber());
			 }
			 
			 listTeacherNormScores = teacherNormScoreDAO.findTeacersNormScoresList(listTeacherDetails);
			 for(TeacherNormScore tns:listTeacherNormScores)
		 	 {
			  mapNormScores.put(tns.getTeacherDetail().getTeacherId(),tns);
		 	 }
			 
			 List<DistrictRequisitionNumbers> districtRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>();
			 List<DistrictRequisitionNumbers> tempRequisitionNumbers     = new ArrayList<DistrictRequisitionNumbers>(); 
			 districtRequisitionNumbers=districtRequisitionNumbersDAO.getRequisitionNumbersWithAndWithoutDistrict(reqList,districtMaster);
			 List<String> duplicateKeySet =new ArrayList<String>();
			 for(DistrictRequisitionNumbers distRequisitionNumbers : districtRequisitionNumbers)
			   {
				 if(mapDistrictRequisitionNumbers.get(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber())==null)
				 {
					 mapDistrictRequisitionNumbers.put(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber(), distRequisitionNumbers);	 
				 }
				 else
				 {
					duplicateKeySet.add(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber());
					tempRequisitionNumbers.add(mapDistrictRequisitionNumbers.get(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber()));
					tempRequisitionNumbers.add(distRequisitionNumbers);
					//mapDistrictRequisitionNumbers.remove(requisitionNumbers.getDistrictMaster().getDistrictId()+"#"+requisitionNumbers.getRequisitionNumber());
				 }
			  }
			 for(String dkey : duplicateKeySet){
				 mapDistrictRequisitionNumbers.remove(dkey);
			 }
			 List<JobRequisitionNumbers> lstJobRequisitionNumbers = new ArrayList<JobRequisitionNumbers>();
			 
		     if(tempRequisitionNumbers!=null && tempRequisitionNumbers.size()>0){
			   lstJobRequisitionNumbers=jobRequisitionNumbersDAO.findJobRequsition(tempRequisitionNumbers);
		     }
		     for(JobRequisitionNumbers jrNumber : lstJobRequisitionNumbers){
			   mapJobRequisitionNumbers.put(jrNumber.getDistrictRequisitionNumbers().getDistrictMaster().getDistrictId()+"#"+jrNumber.getDistrictRequisitionNumbers().getRequisitionNumber()+"#"+jrNumber.getJobOrder().getJobId(), jrNumber);
		     }
		   
		 }
		 
	     if(schoolId!=0)
		  {
	    	 List<JobForTeacher> filterSchoolJobForTeachers= new ArrayList<JobForTeacher>();
			 for(JobForTeacher jft : lstJobForTeachers)
			 {
				 if(jft.getSchoolMaster()!=null)
				  if(jft.getSchoolMaster().getSchoolId()==schoolId){
					  filterSchoolJobForTeachers.add(jft);
				 }
			 } 
			 lstJobForTeachers.clear();	 
			 lstJobForTeachers.addAll(filterSchoolJobForTeachers);
		 }	 
		
		// Manual sorting logic start 
		// Norm Score Sorting     
		 if(sortOrder.equals("normScore"))
		 {
			for(JobForTeacher jft : lstJobForTeachers){
				if(mapNormScores.get(jft.getTeacherId().getTeacherId()) != null){
					jft.setNormScore(Double.valueOf(mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+""));
				}else{
					jft.setNormScore(-0.0);
				}
			}
			 if(sortOrderType.equals("0"))
				 Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorNormScore);
			 else 
				 Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorNormScoreDesc);
	 	}  
		 
		// schoolName
		 if(sortOrder.equals("schoolName"))
		 {
			 for(JobForTeacher jft : lstJobForTeachers){
				 if(jft.getSchoolMaster()!=null){
				      jft.setDistName(jft.getSchoolMaster().getSchoolName());
					}else{
						jft.setDistName("");
					 }
			 }
			 if(sortOrderType.equals("0"))
				  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
			 else 
			      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
		 }
		// Teacher Title
		 if(sortingcheck==1)
		 {
			 for(JobForTeacher jft : lstJobForTeachers){
				 jft.setDistName(jft.getTeacherId().getLastName());
			 }
			 if(sortOrderType.equals("0"))
				  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
			 else 
			      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
		 }
		// JobTitle Name
		 if(sortOrder.equals("jobTitle"))
		 {
			 for(JobForTeacher jft : lstJobForTeachers){
				 jft.setDistName(jft.getJobId().getJobTitle());
			 }
			 if(sortOrderType.equals("0"))
				  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
			 else 
			      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
		 }
		// Region Name
		 if(sortOrder.equals("regionName"))
		 {
			 for(JobForTeacher jft : lstJobForTeachers){
				 if(jft.getSchoolMaster()!=null){
					 if(jft.getSchoolMaster().getDivision()!=null)
						 jft.setDistName(jft.getSchoolMaster().getDivision());
					 else
						 jft.setDistName("");
					}else{
						jft.setDistName("");
					 }
			 }
			 if(sortOrderType.equals("0"))
				  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
			 else 
			      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
		 }
		 
		 if(sortOrder.equals("ftpt"))
		 {
			 for(JobForTeacher jft : lstJobForTeachers){
			 if(jft.getRequisitionNumber()!=null)
			  {
				 if(mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber())!=null)
				  {
					String posType;
					String originalPosType=mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()).getPosType();
					if(originalPosType.equalsIgnoreCase("P"))
						posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
					else
						posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
					jft.setDistName(posType);
				  }
			  else if(mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId())!=null) {
				  String posType;
					String originalPosType=mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId()).getDistrictRequisitionNumbers().getPosType();
					if(originalPosType.equalsIgnoreCase("P"))
						posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
					else
						posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
					jft.setDistName(posType);
			  }else{
				  jft.setDistName("");
				}
		  }
		 else{
			 jft.setDistName("");
			}
			 }
			 if(sortOrderType.equals("0"))
				  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
			 else 
			      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC()); 
		 }


		 
		 
			 //  Excel   Exporting	
					
					String time = String.valueOf(System.currentTimeMillis()).substring(6);
					//String basePath = request.getRealPath("/")+"/candidate";
					String basePath = request.getSession().getServletContext().getRealPath ("/")+"/hiredapplicants";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="hiredapplicants"+time+".xls";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);

					WorkbookSettings wbSettings = new WorkbookSettings();

					wbSettings.setLocale(new Locale("en", "EN"));

					WritableCellFormat timesBoldUnderline;
					WritableCellFormat header;
					WritableCellFormat headerBold;
					WritableCellFormat times;

					WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
					workbook.createSheet("hiredapplicants", 0);
					WritableSheet excelSheet = workbook.getSheet(0);

					WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
					// Define the cell format
					times = new WritableCellFormat(times10pt);
					// Lets automatically wrap the cells
					times.setWrap(true);

					// Create create a bold font with unterlines
					WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
					WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

					timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
					timesBoldUnderline.setAlignment(Alignment.CENTRE);
					// Lets automatically wrap the cells
					timesBoldUnderline.setWrap(true);

					header = new WritableCellFormat(times10ptBoldUnderline);
					headerBold = new WritableCellFormat(times10ptBoldUnderline);
					CellView cv = new CellView();
					cv.setFormat(times);
					cv.setFormat(timesBoldUnderline);
					cv.setAutosize(true);

					header.setBackground(Colour.GRAY_25);

					// Write a few headers
					excelSheet.mergeCells(0, 0, 6, 1);
					Label label;
					label = new Label(0, 0, "Applicants Hired", timesBoldUnderline);
					excelSheet.addCell(label);
					excelSheet.mergeCells(0, 3, 6, 3);
					label = new Label(0, 3, "");
					excelSheet.addCell(label);
					excelSheet.getSettings().setDefaultColumnWidth(18);
					
					int k=4;
					int col=1;
					label = new Label(0, k, Utility.getLocaleValuePropByKey("lblApplicantName", locale),header); 
					excelSheet.addCell(label);
					label = new Label(1, k, Utility.getLocaleValuePropByKey("lblJoTil", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblEPINormScore", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblDateHired", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgPositionNumber", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgHiredforSchool", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgRegionName", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgFT/PT", locale),header); 
					excelSheet.addCell(label);
					 
					
					k=k+1;
					if(lstJobForTeachers.size()==0)
					{	
						excelSheet.mergeCells(0, k, 9, k);
						label = new Label(0, k, Utility.getLocaleValuePropByKey("msgNorecordfound", locale)); 
						excelSheet.addCell(label);
					}
					
			if(lstJobForTeachers.size()>0){
			 for(JobForTeacher jft :lstJobForTeachers) 
			 {
				col=1;
			
			    label = new Label(0, k, jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")"); 
				excelSheet.addCell(label);
				
				
				label = new Label(1, k, jft.getJobId().getJobTitle()); 
				excelSheet.addCell(label);
				
				//Norm Score Sorting
				if(mapNormScores.get(jft.getTeacherId().getTeacherId())!=null){
					label = new Label(++col, k, mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+""); 
					excelSheet.addCell(label);
				}else{
					label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("optN/A", locale)+""); 
					excelSheet.addCell(label);
				}
				//HIRED BY DATE @SWADESH
				if(jft.getHiredByDate()!=null)
				 {
					 label = new Label(++col, k,Utility.convertDateAndTimeToUSformatOnlyDate(jft.getHiredByDate()).toString()); 
						excelSheet.addCell(label);
				 }else{
					 label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("optN/A", locale)+""); 
						excelSheet.addCell(label);
				  }
				//for position Number
				 if(jft.getRequisitionNumber()!=null)
				 {
					 label = new Label(++col, k,jft.getRequisitionNumber()); 
						excelSheet.addCell(label);
				 }else{
					 label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("optN/A", locale)+""); 
						excelSheet.addCell(label);
				  }
				//for School
				if(jft.getSchoolMaster()!=null){
					String schoolAndLocCode="";
					if(jft.getSchoolMaster().getLocationCode()!=null && jft.getSchoolMaster().getLocationCode().length()>0){
						schoolAndLocCode="("+jft.getSchoolMaster().getLocationCode()+")";
					}
					schoolAndLocCode+=jft.getSchoolMaster().getSchoolName();
					label = new Label(++col, k, schoolAndLocCode); 
					excelSheet.addCell(label);
					
					String zoneName="";
					if(jft.getSchoolMaster().getDivision()!=null)
						zoneName=jft.getSchoolMaster().getDivision();
					else
						zoneName =""+Utility.getLocaleValuePropByKey("optN/A", locale)+"";
						
					label = new Label(++col, k,zoneName); 
					excelSheet.addCell(label);
				}else{
					label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("optN/A", locale)+""); 
					excelSheet.addCell(label);
					label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("optN/A", locale)+""); 
					excelSheet.addCell(label);
				 }	
					
				//for FT/PT
				
					if(jft.getRequisitionNumber()!=null)
					  {
						 if(mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber())!=null)
						  {
							String posType;
							String originalPosType=mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()).getPosType();
							if(originalPosType.equalsIgnoreCase("P"))
								posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
							else
								posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
							
							label = new Label(++col, k, posType); 
							excelSheet.addCell(label);
						  }
						  else if(mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId())!=null) {
							  String posType;
								String originalPosType=mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId()).getDistrictRequisitionNumbers().getPosType();
								if(originalPosType.equalsIgnoreCase("P"))
									posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
								else
									posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
								label = new Label(++col, k, posType); 
								excelSheet.addCell(label);  
						  }else{
							  label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("optN/A", locale)+""); 
								excelSheet.addCell(label);
							}
					  }
					 else{
						 label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("optN/A", locale)+""); 
							excelSheet.addCell(label);
						}
				
				 k++;
			 }
			}
	
			workbook.write();
			workbook.close();
		}catch(Exception e){}
		
	 return fileName;
  }
	
  
	
	public String displayRecordsByEntityTypePDF(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport( districtOrSchoolId, schoolId, noOfRow, pageNo, sortOrder, sortOrderType,basePath+"/"+fileName,context.getServletContext().getRealPath("/"),startDate,endDate);
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}

	public boolean generateCandidatePDReport(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String path,String realPath,String startDate,String endDate)
	{
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		
			
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			int sortingcheck=1;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				/******************SWADESH********************/
				Date hiredStartdate=null;
				Date hiredEndDtae=null;
				boolean epiDateFlag=false;
				List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
				try{
					if(!startDate.equals("")){
						hiredStartdate=Utility.getCurrentDateFormart(startDate);
						epiDateFlag=true;
					}
					if(!endDate.equals("")){
						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(Utility.getCurrentDateFormart(endDate));
						cal2.add(Calendar.HOUR,23);
						cal2.add(Calendar.MINUTE,59);
						cal2.add(Calendar.SECOND,59);
						hiredEndDtae=cal2.getTime();
						epiDateFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				 /** set default sorting fieldName **/
					String sortOrderFieldName="lastName";
					String sortOrderNoField="lastName";

					if(sortOrder.equalsIgnoreCase("") || sortOrder.equalsIgnoreCase("lastName"))
						sortingcheck=1;
					else if(sortOrder.equalsIgnoreCase("jobTitle"))
						sortingcheck=2;
					else  if(sortOrder.equalsIgnoreCase("requisitionNumber")){
						sortingcheck=3;
					}else
						sortingcheck=4;
					
					/**Start set dynamic sorting fieldName **/
					
					Order  sortOrderStrVal=null;

					if(sortOrder!=null){
						if(!sortOrder.equals("") && !sortOrder.equals(null)){
							sortOrderFieldName=sortOrder;
							sortOrderNoField=sortOrder;
						}
					}

					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
						if(sortOrderType.equals("0")){
							sortOrderStrVal=Order.asc(sortOrderFieldName);
						}else{
							sortOrderTypeVal="1";
							sortOrderStrVal=Order.desc(sortOrderFieldName);
						}
					}else{
						sortOrderTypeVal="0";
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					List<JobForTeacher> lstJobForTeachers =new ArrayList<JobForTeacher>();
					
					if(entityID==2)
					{
					 lstJobForTeachers = jobForTeacherDAO.hiredCandidateListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,hiredStartdate,hiredEndDtae); 
					}
					else if(entityID==1) 
					{
						if(districtOrSchoolId!=0)
							 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
						lstJobForTeachers = jobForTeacherDAO.hiredCandidateListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,hiredStartdate,hiredEndDtae);	 
					}
				
					System.out.println("=====================lstForTeachers.size() :: "+lstJobForTeachers.size());
					
				 List<TeacherDetail> listTeacherDetails= new ArrayList<TeacherDetail>();
				 List<JobOrder> lstJobOrderss=new ArrayList<JobOrder>();
				 
				 List<TeacherNormScore> listTeacherNormScores = new  ArrayList<TeacherNormScore>();
		     	 Map<Integer,TeacherNormScore> mapNormScores =new HashMap<Integer, TeacherNormScore>();
			     Map<String,DistrictRequisitionNumbers> mapDistrictRequisitionNumbers =new HashMap<String, DistrictRequisitionNumbers>();
				 Map<String,JobRequisitionNumbers> mapJobRequisitionNumbers= new HashMap<String, JobRequisitionNumbers>();
			     if(lstJobForTeachers!=null && lstJobForTeachers.size()>0)
				 {
			    	 List<String>reqList =new ArrayList<String>();
			    	 
					 for(JobForTeacher jft :  lstJobForTeachers)
					 {
					  listTeacherDetails.add(jft.getTeacherId());
					  lstJobOrderss.add(jft.getJobId());
					  if(jft.getRequisitionNumber()!=null)
						 reqList.add(jft.getRequisitionNumber());
					 }
					 
					 listTeacherNormScores = teacherNormScoreDAO.findTeacersNormScoresList(listTeacherDetails);
					 for(TeacherNormScore tns:listTeacherNormScores)
				 	 {
					  mapNormScores.put(tns.getTeacherDetail().getTeacherId(),tns);
				 	 }
					 
					 List<DistrictRequisitionNumbers> districtRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>();
					 List<DistrictRequisitionNumbers> tempRequisitionNumbers     = new ArrayList<DistrictRequisitionNumbers>(); 
					 districtRequisitionNumbers=districtRequisitionNumbersDAO.getRequisitionNumbersWithAndWithoutDistrict(reqList,districtMaster);
					 List<String> duplicateKeySet =new ArrayList<String>();
					 for(DistrictRequisitionNumbers distRequisitionNumbers : districtRequisitionNumbers)
					   {
						 if(mapDistrictRequisitionNumbers.get(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber())==null)
						 {
							 mapDistrictRequisitionNumbers.put(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber(), distRequisitionNumbers);	 
						 }
						 else
						 {
							duplicateKeySet.add(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber());
							tempRequisitionNumbers.add(mapDistrictRequisitionNumbers.get(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber()));
							tempRequisitionNumbers.add(distRequisitionNumbers);
							//mapDistrictRequisitionNumbers.remove(requisitionNumbers.getDistrictMaster().getDistrictId()+"#"+requisitionNumbers.getRequisitionNumber());
						 }
					  }
					 for(String dkey : duplicateKeySet){
						 mapDistrictRequisitionNumbers.remove(dkey);
					 }
					 List<JobRequisitionNumbers> lstJobRequisitionNumbers = new ArrayList<JobRequisitionNumbers>();
					 
				     if(tempRequisitionNumbers!=null && tempRequisitionNumbers.size()>0){
					   lstJobRequisitionNumbers=jobRequisitionNumbersDAO.findJobRequsition(tempRequisitionNumbers);
				     }
				     for(JobRequisitionNumbers jrNumber : lstJobRequisitionNumbers){
					   mapJobRequisitionNumbers.put(jrNumber.getDistrictRequisitionNumbers().getDistrictMaster().getDistrictId()+"#"+jrNumber.getDistrictRequisitionNumbers().getRequisitionNumber()+"#"+jrNumber.getJobOrder().getJobId(), jrNumber);
				     }
				   
				 }
				 
			     if(schoolId!=0)
				  {
			    	 List<JobForTeacher> filterSchoolJobForTeachers= new ArrayList<JobForTeacher>();
					 for(JobForTeacher jft : lstJobForTeachers)
					 {
						 if(jft.getSchoolMaster()!=null)
						  if(jft.getSchoolMaster().getSchoolId()==schoolId){
							  filterSchoolJobForTeachers.add(jft);
						 }
					 } 
					 lstJobForTeachers.clear();	 
					 lstJobForTeachers.addAll(filterSchoolJobForTeachers);
				 }	 
				
				// Manual sorting logic start 
				// Norm Score Sorting     
				 if(sortOrder.equals("normScore"))
				 {
					for(JobForTeacher jft : lstJobForTeachers){
						if(mapNormScores.get(jft.getTeacherId().getTeacherId()) != null){
							jft.setNormScore(Double.valueOf(mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+""));
						}else{
							jft.setNormScore(-0.0);
						}
					}
					 if(sortOrderType.equals("0"))
						 Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorNormScore);
					 else 
						 Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorNormScoreDesc);
			 	}  
				 
				// schoolName
				 if(sortOrder.equals("schoolName"))
				 {
					 for(JobForTeacher jft : lstJobForTeachers){
						 if(jft.getSchoolMaster()!=null){
						      jft.setDistName(jft.getSchoolMaster().getSchoolName());
							}else{
								jft.setDistName("");
							 }
					 }
					 if(sortOrderType.equals("0"))
						  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
					 else 
					      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
				 }
				// Teacher Title
				 if(sortingcheck==1)
				 {
					 for(JobForTeacher jft : lstJobForTeachers){
						 jft.setDistName(jft.getTeacherId().getLastName());
					 }
					 if(sortOrderType.equals("0"))
						  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
					 else 
					      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
				 }
				// JobTitle Name
				 if(sortOrder.equals("jobTitle"))
				 {
					 for(JobForTeacher jft : lstJobForTeachers){
						 jft.setDistName(jft.getJobId().getJobTitle());
					 }
					 if(sortOrderType.equals("0"))
						  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
					 else 
					      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
				 }
				// Region Name
				 if(sortOrder.equals("regionName"))
				 {
					 for(JobForTeacher jft : lstJobForTeachers){
						 if(jft.getSchoolMaster()!=null){
							 if(jft.getSchoolMaster().getDivision()!=null)
								 jft.setDistName(jft.getSchoolMaster().getDivision());
							  else
								 jft.setDistName("");
							}else{
								jft.setDistName("");
							 }
					 }
					 if(sortOrderType.equals("0"))
						  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
					 else 
					      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
				 }
				 
				 if(sortOrder.equals("ftpt"))
				 {
					 for(JobForTeacher jft : lstJobForTeachers){
					 if(jft.getRequisitionNumber()!=null)
					  {
						 if(mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber())!=null)
						  {
							String posType;
							String originalPosType=mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()).getPosType();
							if(originalPosType.equalsIgnoreCase("P"))
								posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
							else
								posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
							jft.setDistName(posType);
						  }
					  else if(mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId())!=null) {
						  String posType;
							String originalPosType=mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId()).getDistrictRequisitionNumbers().getPosType();
							if(originalPosType.equalsIgnoreCase("P"))
								posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
							else
								posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
							jft.setDistName(posType);
					  }else{
						  jft.setDistName("");
						}
				  }
				 else{
					 jft.setDistName("");
					}
					 }
					 if(sortOrderType.equals("0"))
						  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
					 else 
					      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC()); 
				 }


				 
			
			 // System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
				 String fontPath = realPath;
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 8, Font.NORMAL);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 10);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							//Color bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject(Utility.getLocaleValuePropByKey("headAppHir", locale));
						document.addCreationDate();
						document.addTitle(Utility.getLocaleValuePropByKey("headAppHir", locale));

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;


						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						//para[1] = new Paragraph(" ",font20bold);
						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						

						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];

						
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("headAppHir", locale),font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						
						
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						document.add(new Phrase("\n"));


						float[] tblwidth={.22f,.22f,.08f,.08f,.08f,.24f,.09f,.06f};
						
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[8];
						cell = new PdfPCell[8];
				// header
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblApplicantName", locale),font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJoTil", locale),font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblEPINormScore", locale),font10_10);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[2]);
						
						//Hired by date Swadesh
						para[3] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblDateHired", locale),font10_10);
						cell[3]= new PdfPCell(para[3]);
						cell[3].setBackgroundColor(bluecolor);
						cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[3]);

						para[4] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgPositionNumber", locale),font10_10);
						cell[4]= new PdfPCell(para[4]);
						cell[4].setBackgroundColor(bluecolor);
						cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[4]);
						
						para[5] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgHiredforSchool", locale),font10_10);
						cell[5]= new PdfPCell(para[5]);
						cell[5].setBackgroundColor(bluecolor);
						cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[5]);
						
						para[6] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgRegionName", locale),font10_10);
						cell[6]= new PdfPCell(para[6]);
						cell[6].setBackgroundColor(bluecolor);
						cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[6]);
						
						para[7] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgFT/PT", locale),font10_10);
						cell[7]= new PdfPCell(para[7]);
						cell[7].setBackgroundColor(bluecolor);
						cell[7].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[7]);
						
						document.add(mainTable);
						
						if(lstJobForTeachers.size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgNorecordfound", locale),font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
				     
						if(lstJobForTeachers.size()>0){
							 for(JobForTeacher jft :lstJobForTeachers) 
							 {
							  int index=0;
							  float[] tblwidth1={.22f,.22f,.08f,.08f,.08f,.24f,.09f,.06f};
									
							  mainTable = new PdfPTable(tblwidth1);
							  mainTable.setWidthPercentage(100);
							  para = new Paragraph[8];
							  cell = new PdfPCell[8];

							 para[index] = new Paragraph(""+jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")",font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
							 
							 para[index] = new Paragraph(""+jft.getJobId().getJobTitle(),font8bold);
							 cell[index]= new PdfPCell(para[index]);
							 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							 mainTable.addCell(cell[index]);
							 index++;
								
								//Norm Score Sorting
							if(mapNormScores.get(jft.getTeacherId().getTeacherId())!=null){
								para[index] = new Paragraph(""+mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore(),font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
							}else{
								para[index] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("optN/A", locale)+"",font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
							}
							
							//for hired by date SWADESH
							if(jft.getHiredByDate()!=null){
								para[index] = new Paragraph(""+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getHiredByDate()),font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
							}else{
								para[index] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("optN/A", locale)+"",font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
							}
								
							//for position Number
							 if(jft.getRequisitionNumber()!=null)
							 {
								 para[index] = new Paragraph(""+jft.getRequisitionNumber(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							 }else{
								 para[index] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("optN/A", locale)+"",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							  }
							//for School
								if(jft.getSchoolMaster()!=null){
									String schoolAndLocCode="";
									if(jft.getSchoolMaster().getLocationCode()!=null && jft.getSchoolMaster().getLocationCode().length()>0){
										schoolAndLocCode="("+jft.getSchoolMaster().getLocationCode()+") ";
									}
									schoolAndLocCode+=jft.getSchoolMaster().getSchoolName();
									para[index] = new Paragraph(""+schoolAndLocCode,font8bold);
									cell[index]= new PdfPCell(para[index]);
									cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									mainTable.addCell(cell[index]);
									index++;
									
									String zoneName="";
									if(jft.getSchoolMaster().getDivision()!=null)
										zoneName=jft.getSchoolMaster().getDivision();
									else
										zoneName =""+Utility.getLocaleValuePropByKey("optN/A", locale)+"";
									
									para[index] = new Paragraph(""+zoneName,font8bold);
									cell[index]= new PdfPCell(para[index]);
									cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									mainTable.addCell(cell[index]);
									index++;
								}else{
									para[index] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("optN/A", locale)+"",font8bold);
									cell[index]= new PdfPCell(para[index]);
									cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									mainTable.addCell(cell[index]);
									index++;
									para[index] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("optN/A", locale)+"",font8bold);
									cell[index]= new PdfPCell(para[index]);
									cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									mainTable.addCell(cell[index]);
									index++;
								 }	
							
						//for FT/PT
							 if(jft.getRequisitionNumber()!=null)
							  {
								 if(mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber())!=null)
								  {
									String posType;
									String originalPosType=mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()).getPosType();
									if(originalPosType.equalsIgnoreCase("P"))
										posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
									else
										posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
									
									para[index] = new Paragraph(""+posType,font8bold);
									cell[index]= new PdfPCell(para[index]);
									cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									mainTable.addCell(cell[index]);
									index++;
								  }
								  else if(mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId())!=null) {
									  String posType;
										String originalPosType=mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId()).getDistrictRequisitionNumbers().getPosType();
										if(originalPosType.equalsIgnoreCase("P"))
											posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
										else
											posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
										para[index] = new Paragraph(""+posType,font8bold);
										cell[index]= new PdfPCell(para[index]);
										cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
										mainTable.addCell(cell[index]);
										index++;  
								  }else{
									  para[index] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("optN/A", locale)+"",font8bold);
									  cell[index]= new PdfPCell(para[index]);
									  cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									  mainTable.addCell(cell[index]);
									  index++;
									}
							  }
							 else{
								 para[index] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("optN/A", locale)+"",font8bold);
								  cell[index]= new PdfPCell(para[index]);
								  cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								  mainTable.addCell(cell[index]);
								  index++;
								}
						document.add(mainTable);
				   }
				}
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
			return true;
	}
	
	
	public String displayRecordsByEntityTypePrintPreview(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			
			/******************SWADESH********************/
			Date hiredStartdate=null;
			Date hiredEndDtae=null;
			boolean epiDateFlag=false;
			List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			try{
				if(!startDate.equals("")){
					hiredStartdate=Utility.getCurrentDateFormart(startDate);
					epiDateFlag=true;
				}
				if(!endDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(endDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					hiredEndDtae=cal2.getTime();
					epiDateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="lastName";
				String sortOrderNoField="lastName";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("lastName"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("jobTitle"))
					sortingcheck=2;
				else  if(sortOrder.equalsIgnoreCase("requisitionNumber")){
					sortingcheck=3;
				}else
					sortingcheck=4;
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<JobForTeacher> lstJobForTeachers =new ArrayList<JobForTeacher>();
				
				if(entityID==2)
				{
				 lstJobForTeachers = jobForTeacherDAO.hiredCandidateListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,hiredStartdate,hiredEndDtae); 
				}
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0)
						 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					lstJobForTeachers = jobForTeacherDAO.hiredCandidateListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,hiredStartdate,hiredEndDtae);	 
				}
			
				System.out.println("=====================lstForTeachers.size() :: "+lstJobForTeachers.size());
				
			 List<TeacherDetail> listTeacherDetails= new ArrayList<TeacherDetail>();
			 List<JobOrder> lstJobOrderss=new ArrayList<JobOrder>();
			 
			 List<TeacherNormScore> listTeacherNormScores = new  ArrayList<TeacherNormScore>();
	     	 Map<Integer,TeacherNormScore> mapNormScores =new HashMap<Integer, TeacherNormScore>();
		     Map<String,DistrictRequisitionNumbers> mapDistrictRequisitionNumbers =new HashMap<String, DistrictRequisitionNumbers>();
			 Map<String,JobRequisitionNumbers> mapJobRequisitionNumbers= new HashMap<String, JobRequisitionNumbers>();
		     if(lstJobForTeachers!=null && lstJobForTeachers.size()>0)
			 {
		    	 List<String>reqList =new ArrayList<String>();
		    	 
				 for(JobForTeacher jft :  lstJobForTeachers)
				 {
				  listTeacherDetails.add(jft.getTeacherId());
				  lstJobOrderss.add(jft.getJobId());
				  if(jft.getRequisitionNumber()!=null)
					 reqList.add(jft.getRequisitionNumber());
				 }
				 
				 listTeacherNormScores = teacherNormScoreDAO.findTeacersNormScoresList(listTeacherDetails);
				 for(TeacherNormScore tns:listTeacherNormScores)
			 	 {
				  mapNormScores.put(tns.getTeacherDetail().getTeacherId(),tns);
			 	 }
				 
				 List<DistrictRequisitionNumbers> districtRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>();
				 List<DistrictRequisitionNumbers> tempRequisitionNumbers     = new ArrayList<DistrictRequisitionNumbers>(); 
				 districtRequisitionNumbers=districtRequisitionNumbersDAO.getRequisitionNumbersWithAndWithoutDistrict(reqList,districtMaster);
				 List<String> duplicateKeySet =new ArrayList<String>();
				 for(DistrictRequisitionNumbers distRequisitionNumbers : districtRequisitionNumbers)
				   {
					 if(mapDistrictRequisitionNumbers.get(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber())==null)
					 {
						 mapDistrictRequisitionNumbers.put(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber(), distRequisitionNumbers);	 
					 }
					 else
					 {
						duplicateKeySet.add(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber());
						tempRequisitionNumbers.add(mapDistrictRequisitionNumbers.get(distRequisitionNumbers.getDistrictMaster().getDistrictId()+"#"+distRequisitionNumbers.getRequisitionNumber()));
						tempRequisitionNumbers.add(distRequisitionNumbers);
						//mapDistrictRequisitionNumbers.remove(requisitionNumbers.getDistrictMaster().getDistrictId()+"#"+requisitionNumbers.getRequisitionNumber());
					 }
				  }
				 for(String dkey : duplicateKeySet){
					 mapDistrictRequisitionNumbers.remove(dkey);
				 }
				 List<JobRequisitionNumbers> lstJobRequisitionNumbers = new ArrayList<JobRequisitionNumbers>();
				 
			     if(tempRequisitionNumbers!=null && tempRequisitionNumbers.size()>0){
				   lstJobRequisitionNumbers=jobRequisitionNumbersDAO.findJobRequsition(tempRequisitionNumbers);
			     }
			     for(JobRequisitionNumbers jrNumber : lstJobRequisitionNumbers){
				   mapJobRequisitionNumbers.put(jrNumber.getDistrictRequisitionNumbers().getDistrictMaster().getDistrictId()+"#"+jrNumber.getDistrictRequisitionNumbers().getRequisitionNumber()+"#"+jrNumber.getJobOrder().getJobId(), jrNumber);
			     }
			   
			 }
			 
		     if(schoolId!=0)
			 {
		    	 List<JobForTeacher> filterSchoolJobForTeachers= new ArrayList<JobForTeacher>();
				 for(JobForTeacher jft : lstJobForTeachers)
				 {
					 if(jft.getSchoolMaster()!=null)
					  if(jft.getSchoolMaster().getSchoolId()==schoolId){
						  filterSchoolJobForTeachers.add(jft);
					 }
				 } 
				 lstJobForTeachers.clear();	 
				 lstJobForTeachers.addAll(filterSchoolJobForTeachers);
			}	 
			
			 List<JobForTeacher> finalJobForTeachers =new ArrayList<JobForTeacher>(); 
		
			// Manual sorting logic start 
			// Norm Score Sorting     
			 if(sortOrder.equals("normScore"))
			 {
				for(JobForTeacher jft : lstJobForTeachers){
					if(mapNormScores.get(jft.getTeacherId().getTeacherId()) != null){
						jft.setNormScore(Double.valueOf(mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+""));
					}else{
						jft.setNormScore(-0.0);
					}
				}
				 if(sortOrderType.equals("0"))
					 Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorNormScore);
				 else 
					 Collections.sort(lstJobForTeachers, JobForTeacher.jobForTeacherComparatorNormScoreDesc);
		 	}  
			 
			// schoolName
			 if(sortOrder.equals("schoolName"))
			 {
				 for(JobForTeacher jft : lstJobForTeachers){
					 if(jft.getSchoolMaster()!=null){
					      jft.setDistName(jft.getSchoolMaster().getSchoolName());
						}else{
							jft.setDistName("");
						 }
				 }
				 if(sortOrderType.equals("0"))
					  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
				 else 
				      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
			 }
			// Teacher Title
			 if(sortingcheck==1)
			 {
				 for(JobForTeacher jft : lstJobForTeachers){
					 jft.setDistName(jft.getTeacherId().getLastName());
				 }
				 if(sortOrderType.equals("0"))
					  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
				 else 
				      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
			 }
			// JobTitle Name
			 if(sortOrder.equals("jobTitle"))
			 {
				 for(JobForTeacher jft : lstJobForTeachers){
					 jft.setDistName(jft.getJobId().getJobTitle());
				 }
				 if(sortOrderType.equals("0"))
					  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
				 else 
				      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
			 }
			// Region Name
			 if(sortOrder.equals("regionName"))
			 {
				 for(JobForTeacher jft : lstJobForTeachers){
					 if(jft.getSchoolMaster()!=null){
						 if(jft.getSchoolMaster().getDivision()!=null)
							 jft.setDistName(jft.getSchoolMaster().getDivision());
					     else
							jft.setDistName("");
						}else{
							jft.setDistName("");
						 }
				 }
				 if(sortOrderType.equals("0"))
					  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
				 else 
				      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC());
			 }
			 
			 if(sortOrder.equals("ftpt"))
			 {
				 for(JobForTeacher jft : lstJobForTeachers){
				 if(jft.getRequisitionNumber()!=null)
				  {
					 if(mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber())!=null)
					  {
						String posType;
						String originalPosType=mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()).getPosType();
						if(originalPosType.equalsIgnoreCase("P"))
							posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
						else
							posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
						jft.setDistName(posType);
					  }
				  else if(mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId())!=null) {
					  String posType;
						String originalPosType=mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId()).getDistrictRequisitionNumbers().getPosType();
						if(originalPosType.equalsIgnoreCase("P"))
							posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
						else
							posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
						jft.setDistName(posType);
				  }else{
					  jft.setDistName("");
					}
			  }
			 else{
				 jft.setDistName("");
				}
				 }
				 if(sortOrderType.equals("0"))
					  Collections.sort(lstJobForTeachers, new DistrictNameCompratorASC());
				 else 
				      Collections.sort(lstJobForTeachers, new DistrictNameCompratorDESC()); 
			 }

	        tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>"+Utility.getLocaleValuePropByKey("headAppHir", locale)+"</div><br/>");
			
			tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");
			
			
			tmRecords.append("<table  id='tblGridHired' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblApplicantName", locale)+"</th>");
    
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJoTil", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblEPINormScore", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblDateHired", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgPositionNumber", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgHiredforSchool", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgRegionName", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgFT/PT", locale)+"</th>");
			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			
			if(lstJobForTeachers.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			}
		
			if(lstJobForTeachers.size()>0){
			 for(JobForTeacher jft:lstJobForTeachers) 
			 {
				   tmRecords.append("<tr>");	
					
					tmRecords.append("<td style='font-size:11px;'>"+jft.getTeacherId().getFirstName()+" "+jft.getTeacherId().getLastName()+"\n"+"("+jft.getTeacherId().getEmailAddress()+")"+"</td>");
					tmRecords.append("<td style='font-size:11px;'>"+jft.getJobId().getJobTitle()+"</td>");
					
					//Norm Score Sorting
					if(mapNormScores.get(jft.getTeacherId().getTeacherId())!=null){
					 tmRecords.append("<td style='font-size:11px;'>"+mapNormScores.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+"</td>");
					}else{
						tmRecords.append("<td style='font-size:11px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					}
					
					//Hired By Date swadesh
					
					if(jft.getHiredByDate()!=null)
					 {
					  tmRecords.append("<td style='font-size:11px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getHiredByDate())+"</td>");
					 }else{
						tmRecords.append("<td style='font-size:11px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					  }
					
					//for position Number
					
					 if(jft.getRequisitionNumber()!=null)
					 {
					  tmRecords.append("<td style='font-size:11px;'>"+jft.getRequisitionNumber()+"</td>");
					 }else{
						tmRecords.append("<td style='font-size:11px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
					  }
					
					//for School
						if(jft.getSchoolMaster()!=null){
							String schoolAndLocCode="";
							if(jft.getSchoolMaster().getLocationCode()!=null && jft.getSchoolMaster().getLocationCode().length()>0 ){
								schoolAndLocCode="("+jft.getSchoolMaster().getLocationCode()+") &nbsp;";
							}
							schoolAndLocCode+=jft.getSchoolMaster().getSchoolName();
					       tmRecords.append("<td style='font-size:11px;'>"+schoolAndLocCode+"</td>");
					       
					       String zoneName="";
							if(jft.getSchoolMaster().getDivision()!=null)
								zoneName=jft.getSchoolMaster().getDivision();
							else
								zoneName =""+Utility.getLocaleValuePropByKey("optN/A", locale)+"";
					       tmRecords.append("<td style='font-size:11px;'>"+zoneName+"</td>");
					       
						}else{
						   tmRecords.append("<td style='font-size:11px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
						   tmRecords.append("<td style='font-size:11px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
						 }	
					//for FT/PT
					
						if(jft.getRequisitionNumber()!=null)
						  { if(mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber())!=null)
						  {
								String posType;
								String originalPosType=mapDistrictRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()).getPosType();
								if(originalPosType.equalsIgnoreCase("P"))
									posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
								else
									posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
								
								tmRecords.append("<td style='font-size:11px;'>"+posType+"</td>");
							  }
							  else if(mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId())!=null) {
								  String posType;
									String originalPosType=mapJobRequisitionNumbers.get(jft.getJobId().getDistrictMaster().getDistrictId()+"#"+jft.getRequisitionNumber()+"#"+jft.getJobId().getJobId()).getDistrictRequisitionNumbers().getPosType();
									if(originalPosType.equalsIgnoreCase("P"))
										posType=Utility.getLocaleValuePropByKey("optPartTime", locale);
									else
										posType=Utility.getLocaleValuePropByKey("optFullTime", locale);
									tmRecords.append("<td style='font-size:11px;'>"+posType+"</td>");
							  }else{
								  tmRecords.append("<td style='font-size:11px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
								}
						  }
						 else{
							 tmRecords.append("<td style='font-size:11px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
							}
			   }
			}
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
  }

	public String displayRecordsByEntityType_Op(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate)
	 {
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			/******************SWADESH********************/
			Date hiredStartdate=null;
			Date hiredEndDtae=null;
			boolean epiDateFlag=false;
			List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			try{
				if(!startDate.equals("")){
					hiredStartdate=Utility.getCurrentDateFormart(startDate);
					epiDateFlag=true;
				}
				if(!endDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(endDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					hiredEndDtae=cal2.getTime();
					epiDateFlag=true;
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			//-- get no of record in grid,
			//-- set start and end position

			//System.out.println("noOfRowInPage   "+noOfRowInPage);
				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="lastName";
				String sortOrderNoField="lastName";

				if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("lastName"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("jobTitle"))
					sortingcheck=2;
				else  if(sortOrder.equalsIgnoreCase("requisitionNumber")){
					sortingcheck=3;
				}else
					sortingcheck=4;
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;

				 String query="";
					if(sortOrder!=null){
						if(!sortOrder.equals("") && !sortOrder.equals(null)){
							sortOrderFieldName=sortOrder;
							sortOrderNoField=sortOrder;
						}
					}

					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
						if(sortOrderType.equals("0")){
							sortOrderStrVal=Order.asc(sortOrderFieldName);
							query=""+sortOrderFieldName+" asc ";
						}else{
							sortOrderTypeVal="1";
							sortOrderStrVal=Order.desc(sortOrderFieldName);
							query=""+sortOrderFieldName+" desc ";
						}
					}else{
						sortOrderTypeVal="0";
						sortOrderStrVal=Order.asc(sortOrderFieldName);
						query=""+sortOrderFieldName+" desc ";
					}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				List<JobForTeacher> lstJobForTeachers =new ArrayList<JobForTeacher>();
				JSONArray[] gridJsonRecord = new JSONArray[2];					
				if(entityID==2)
				{
				 //lstJobForTeachers = jobForTeacherDAO.hiredCandidateListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,hiredStartdate,hiredEndDtae);
				 gridJsonRecord = jobForTeacherDAO.getHiredGrid(schoolId, districtMaster,sortingcheck,query,hiredStartdate,hiredEndDtae,start,end,noOfRow); 
				}
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0)
						 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
						gridJsonRecord = jobForTeacherDAO.getHiredGrid(schoolId, districtMaster,sortingcheck,query,hiredStartdate,hiredEndDtae,start,end,noOfRow);
					//lstJobForTeachers = jobForTeacherDAO.hiredCandidateListWithAndWithoutDistrict(districtMaster,sortingcheck,sortOrderStrVal,hiredStartdate,hiredEndDtae);	 
				}
					
			
			String responseText="";
			
			tmRecords.append("<table  id='tblGridHired' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblApplicantName", locale),sortOrderNoField,"lastName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
 
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderNoField,"jobTitle",sortOrderTypeVal,pgNo);
			tmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblEPINormScore", locale),sortOrderNoField,"normScore",sortOrderTypeVal,pgNo);
			tmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblDateHired", locale),sortOrderNoField,"hiredByDate",sortOrderTypeVal,pgNo);
			tmRecords.append("<th valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgPositionNumber", locale),sortOrderNoField,"requisitionNumber",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgHiredforSchool", locale),sortOrderNoField,"schoolName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgRegionName", locale),sortOrderNoField,"division",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgFT/PT", locale),sortOrderFieldName,"postType",sortOrderTypeVal,pgNo);
			tmRecords.append("<th valign='top'>"+responseText+"</th>");
			
		
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			String hiredDate = "N/A";
			if(gridJsonRecord[0].size()==0){
			 tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			}
		
			if(gridJsonRecord[0].size()>0){
				
				System.out.println("gridJsonRecord[0]    "+gridJsonRecord[0].size());
				for (int i = 0; i < gridJsonRecord[0].size(); i++) {
				
					JSONObject jsonrec = new JSONObject();
					jsonrec = gridJsonRecord[0].getJSONObject(i);
					tmRecords.append("<tr>");
					tmRecords.append("<td>"+jsonrec.get("teacherName")+"\n"+"("+jsonrec.get("emailAddress")+")"+"</td>");
					tmRecords.append("<td>"+jsonrec.get("jobTitle")+"</td>");
					tmRecords.append("<td>"+jsonrec.get("normScore")+"</td>");

					if(!jsonrec.get("hiredByDate").toString().equalsIgnoreCase("N/A"))
						hiredDate = new SimpleDateFormat("MMM dd, yyyy").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(jsonrec.get("hiredByDate").toString()));
					
					tmRecords.append("<td>"+hiredDate+"</td>");
					tmRecords.append("<td>"+jsonrec.get("requisitionNumber")+"</td>");
					tmRecords.append("<td>");
					if(!jsonrec.get("locationCode").toString().equalsIgnoreCase("N/A"))
						tmRecords.append("("+jsonrec.get("locationCode")+") ");
					
					tmRecords.append(jsonrec.get("schoolName")+"</td>");
					tmRecords.append("<td>"+jsonrec.get("division")+"</td>");
					tmRecords.append("<td>"+jsonrec.get("postType")+"</td>");
					tmRecords.append("</tr>");
				}				
			  	
			 }
	

		tmRecords.append("</tbody>");
		tmRecords.append("</table>");
		JSONObject jsonCount = new JSONObject();
		jsonCount = gridJsonRecord[1].getJSONObject(0);
		totalRecord = jsonCount.getInt("totalCount");
		
		
		if((gridJsonRecord[0].size() >= Integer.valueOf(noOfRow)) || totalRecord > Integer.valueOf(noOfRow))				
		tmRecords.append(PaginationAndSorting.getPaginationStringForTeacherInfoAjax(request,totalRecord,noOfRow, pageNo));
		

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
}

	public String displayRecordsByEntityTypePDF_OP(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{	
			String fontPath = request.getRealPath("/");
			BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

			UserMaster userMaster = null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else
				userMaster = (UserMaster)session.getAttribute("userMaster");

			int userId = userMaster.getUserId();

			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
			String fileName =time+"Report.pdf";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			System.out.println("user/"+userId+"/"+fileName);

			generateCandidatePDReport_OP( districtOrSchoolId, schoolId, noOfRow, pageNo, sortOrder, sortOrderType,basePath+"/"+fileName,context.getServletContext().getRealPath("/"),startDate,endDate);
			return "user/"+userId+"/"+fileName;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "ABV";
	}

	public boolean generateCandidatePDReport_OP(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String path,String realPath,String startDate,String endDate)
	{
		int cellSize = 0;
		Document document=null;
		FileOutputStream fos = null;
		PdfWriter writer = null;
		Paragraph footerpara = null;
		HeaderFooter headerFooter = null;
		
			
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			int sortingcheck=1;
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
				}
				
				/******************SWADESH********************/
				Date hiredStartdate=null;
				Date hiredEndDtae=null;
				boolean epiDateFlag=false;
				List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
				try{
					if(!startDate.equals("")){
						hiredStartdate=Utility.getCurrentDateFormart(startDate);
						epiDateFlag=true;
					}
					if(!endDate.equals("")){
						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(Utility.getCurrentDateFormart(endDate));
						cal2.add(Calendar.HOUR,23);
						cal2.add(Calendar.MINUTE,59);
						cal2.add(Calendar.SECOND,59);
						hiredEndDtae=cal2.getTime();
						epiDateFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
				noOfRow = "0";
				int start =0;
				int end =0 ;
				int totalRecord = 0;
				//------------------------------------
				 /** set default sorting fieldName **/
					String sortOrderFieldName="lastName";
					String sortOrderNoField="lastName";

					if(sortOrder.equalsIgnoreCase("") || sortOrder.equalsIgnoreCase("lastName"))
						sortingcheck=1;
					else if(sortOrder.equalsIgnoreCase("jobTitle"))
						sortingcheck=2;
					else  if(sortOrder.equalsIgnoreCase("requisitionNumber")){
						sortingcheck=3;
					}else
						sortingcheck=4;
					
					/**Start set dynamic sorting fieldName **/
					
					Order  sortOrderStrVal=null;
					 String query="";
						if(sortOrder!=null){
							if(!sortOrder.equals("") && !sortOrder.equals(null)){
								sortOrderFieldName=sortOrder;
								sortOrderNoField=sortOrder;
							}
						}

						String sortOrderTypeVal="0";
						if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
							if(sortOrderType.equals("0")){
								sortOrderStrVal=Order.asc(sortOrderFieldName);
								query=""+sortOrderFieldName+" asc ";
							}else{
								sortOrderTypeVal="1";
								sortOrderStrVal=Order.desc(sortOrderFieldName);
								query=""+sortOrderFieldName+" desc ";
							}
						}else{
							sortOrderTypeVal="0";
							sortOrderStrVal=Order.asc(sortOrderFieldName);
							query=""+sortOrderFieldName+" asc ";
						}
				
				//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
					JSONArray[] gridJsonRecord = new JSONArray[2];
					if(entityID==2)
					{
						gridJsonRecord = jobForTeacherDAO.getHiredGrid(schoolId, districtMaster,sortingcheck,query,hiredStartdate,hiredEndDtae,start,end,noOfRow);
					}
					else if(entityID==1) 
					{
						if(districtOrSchoolId!=0)
							 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
						gridJsonRecord = jobForTeacherDAO.getHiredGrid(schoolId, districtMaster,sortingcheck,query,hiredStartdate,hiredEndDtae,start,end,noOfRow);
					}
				
				 String fontPath = realPath;
				     try {
							
							BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
							BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
							font8 = new Font(tahoma, 8);

							font8Green = new Font(tahoma, 8);
							font8Green.setColor(Color.BLUE);
							font8bold = new Font(tahoma, 8, Font.NORMAL);


							font9 = new Font(tahoma, 9);
							font9bold = new Font(tahoma, 9, Font.BOLD);
							font10 = new Font(tahoma, 10);
							
							font10_10 = new Font(tahoma, 10);
							font10_10.setColor(Color.white);
							font10bold = new Font(tahoma, 10, Font.BOLD);
							font11 = new Font(tahoma, 11);
							font11bold = new Font(tahoma, 11,Font.BOLD);
							bluecolor =  new Color(0,122,180); 
							
							//Color bluecolor =  new Color(0,122,180); 
							
							font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
							//font20bold.setColor(Color.BLUE);
							font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


						} 
						catch (DocumentException e1) 
						{
							e1.printStackTrace();
						} 
						catch (IOException e1) 
						{
							e1.printStackTrace();
						}
						
						document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
						document.addAuthor("TeacherMatch");
						document.addCreator("TeacherMatch Inc.");
						document.addSubject(Utility.getLocaleValuePropByKey("headAppHir", locale));
						document.addCreationDate();
						document.addTitle(Utility.getLocaleValuePropByKey("headAppHir", locale));

						fos = new FileOutputStream(path);
						PdfWriter.getInstance(document, fos);
					
						document.open();
						
						PdfPTable mainTable = new PdfPTable(1);
						mainTable.setWidthPercentage(90);

						Paragraph [] para = null;
						PdfPCell [] cell = null;


						para = new Paragraph[3];
						cell = new PdfPCell[3];
						para[0] = new Paragraph(" ",font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
						Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
						logo.scalePercent(75);

						//para[1] = new Paragraph(" ",font20bold);
						cell[1]= new PdfPCell(logo);
						cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);

						document.add(new Phrase("\n"));
						
						
						para[2] = new Paragraph("",font20bold);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[2].setBorder(0);
						mainTable.addCell(cell[2]);

						document.add(mainTable);
						
						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						

						float[] tblwidthz={.15f};
						
						mainTable = new PdfPTable(tblwidthz);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[1];
						cell = new PdfPCell[1];

						
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("headAppHir", locale),font20bold);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						document.add(mainTable);

						document.add(new Phrase("\n"));
						//document.add(new Phrase("\n"));
						
						
				        float[] tblwidths={.15f,.20f};
						
						mainTable = new PdfPTable(tblwidths);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[2];
						cell = new PdfPCell[2];

						String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
						
						para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						cell[0].setBorder(0);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell[1].setBorder(0);
						mainTable.addCell(cell[1]);
						
						document.add(mainTable);
						
						document.add(new Phrase("\n"));


						float[] tblwidth={.22f,.22f,.08f,.08f,.08f,.24f,.09f,.06f};
						
						mainTable = new PdfPTable(tblwidth);
						mainTable.setWidthPercentage(100);
						para = new Paragraph[8];
						cell = new PdfPCell[8];
				// header
						para[0] = new Paragraph(Utility.getLocaleValuePropByKey("lblApplicantName", locale),font10_10);
						cell[0]= new PdfPCell(para[0]);
						cell[0].setBackgroundColor(bluecolor);
						cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[0]);
						
						para[1] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJoTil", locale),font10_10);
						cell[1]= new PdfPCell(para[1]);
						cell[1].setBackgroundColor(bluecolor);
						cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[1]);
						
						para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblEPINormScore", locale),font10_10);
						cell[2]= new PdfPCell(para[2]);
						cell[2].setBackgroundColor(bluecolor);
						cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[2]);
						
						//Hired by date Swadesh
						para[3] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblDateHired", locale),font10_10);
						cell[3]= new PdfPCell(para[3]);
						cell[3].setBackgroundColor(bluecolor);
						cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[3]);

						para[4] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgPositionNumber", locale),font10_10);
						cell[4]= new PdfPCell(para[4]);
						cell[4].setBackgroundColor(bluecolor);
						cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[4]);
						
						para[5] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgHiredforSchool", locale),font10_10);
						cell[5]= new PdfPCell(para[5]);
						cell[5].setBackgroundColor(bluecolor);
						cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[5]);
						
						para[6] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgRegionName", locale),font10_10);
						cell[6]= new PdfPCell(para[6]);
						cell[6].setBackgroundColor(bluecolor);
						cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[6]);
						
						para[7] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgFT/PT", locale),font10_10);
						cell[7]= new PdfPCell(para[7]);
						cell[7].setBackgroundColor(bluecolor);
						cell[7].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[7]);
						
						document.add(mainTable);
						
						if(gridJsonRecord[0].size()==0){
							    float[] tblwidth11={.10f};
								
								 mainTable = new PdfPTable(tblwidth11);
								 mainTable.setWidthPercentage(100);
								 para = new Paragraph[1];
								 cell = new PdfPCell[1];
							
								 para[0] = new Paragraph(Utility.getLocaleValuePropByKey("msgNorecordfound", locale),font8bold);
								 cell[0]= new PdfPCell(para[0]);
								 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[0]);
							
							document.add(mainTable);
						}
						String hiredDate = "N/A";
						
						if(gridJsonRecord[0].size()>0){
							System.out.println("gridJsonRecord[0]    "+gridJsonRecord[0].size());
							for (int i = 0; i < gridJsonRecord[0].size(); i++) {
								String finalSchool = "";
								JSONObject jsonrec = new JSONObject();
								jsonrec = gridJsonRecord[0].getJSONObject(i);
					    	   
								int index=0;
								float[] tblwidth1={.22f,.22f,.08f,.08f,.08f,.24f,.09f,.06f};
									
								mainTable = new PdfPTable(tblwidth1);
								mainTable.setWidthPercentage(100);
								para = new Paragraph[8];
								cell = new PdfPCell[8];

								para[index] = new Paragraph(jsonrec.get("teacherName")+"\n"+"("+jsonrec.get("emailAddress")+")",font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
							 
								para[index] = new Paragraph(""+jsonrec.get("jobTitle"),font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
								
								para[index] = new Paragraph(""+jsonrec.get("normScore"),font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
							
								if(!jsonrec.get("hiredByDate").toString().equalsIgnoreCase("N/A"))
									hiredDate = new SimpleDateFormat("MMM dd, yyyy").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(jsonrec.get("hiredByDate").toString()));
								
								para[index] = new Paragraph(""+hiredDate,font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
								
								para[index] = new Paragraph(""+jsonrec.get("requisitionNumber"),font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
							 
								if(!jsonrec.get("locationCode").toString().equalsIgnoreCase("N/A"))
									finalSchool = "("+jsonrec.get("locationCode")+") ";
								
								para[index] = new Paragraph(finalSchool+jsonrec.get("schoolName"),font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
	
								para[index] = new Paragraph(""+jsonrec.get("division"),font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
									
								para[index] = new Paragraph(""+jsonrec.get("postType"),font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
						
								document.add(mainTable);
							}
				}
			}catch(Exception e){e.printStackTrace();}
			finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
			return true;
	}
	
	public String displayRecordsByEntityTypeEXL_OP(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileName = null;
		int sortingcheck=1;
		try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
		}
		
		/******************SWADESH********************/
		Date hiredStartdate=null;
		Date hiredEndDtae=null;
		boolean epiDateFlag=false;
		try{
			if(!startDate.equals("")){
				hiredStartdate=Utility.getCurrentDateFormart(startDate);
				epiDateFlag=true;
			}
			if(!endDate.equals("")){
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(Utility.getCurrentDateFormart(endDate));
				cal2.add(Calendar.HOUR,23);
				cal2.add(Calendar.MINUTE,59);
				cal2.add(Calendar.SECOND,59);
				hiredEndDtae=cal2.getTime();
				epiDateFlag=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
		noOfRow = "0";
		int start =0;
		int end =0 ;
		int totalRecord = 0;
		//------------------------------------
		 /** set default sorting fieldName **/
			String sortOrderFieldName="lastName";
			String sortOrderNoField="lastName";

			if(sortOrder.equalsIgnoreCase("") || sortOrder.equalsIgnoreCase("lastName"))
				sortingcheck=1;
			else if(sortOrder.equalsIgnoreCase("jobTitle"))
				sortingcheck=2;
			else  if(sortOrder.equalsIgnoreCase("requisitionNumber")){
				sortingcheck=3;
			}else
				sortingcheck=4;
			
			/**Start set dynamic sorting fieldName **/
			
			Order  sortOrderStrVal=null;
			 String query="";
				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
						query=""+sortOrderFieldName+" asc ";
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
						query=""+sortOrderFieldName+" desc ";
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
					query=""+sortOrderFieldName+" asc ";
				}
		
		//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
			JSONArray[] gridJsonRecord = new JSONArray[2];
			
			if(entityID==2)
			{
				gridJsonRecord = jobForTeacherDAO.getHiredGrid(schoolId, districtMaster,sortingcheck,query,hiredStartdate,hiredEndDtae,start,end,noOfRow); 
			}
			else if(entityID==1) 
			{
				if(districtOrSchoolId!=0)
					 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
				gridJsonRecord = jobForTeacherDAO.getHiredGrid(schoolId, districtMaster,sortingcheck,query,hiredStartdate,hiredEndDtae,start,end,noOfRow);	 
			}
		 
			 //  Excel   Exporting	
					
					String time = String.valueOf(System.currentTimeMillis()).substring(6);
					//String basePath = request.getRealPath("/")+"/candidate";
					String basePath = request.getSession().getServletContext().getRealPath ("/")+"/hiredapplicants";
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
					fileName ="hiredapplicants"+time+".xls";

					
					File file = new File(basePath);
					if(!file.exists())
						file.mkdirs();

					Utility.deleteAllFileFromDir(basePath);

					file = new File(basePath+"/"+fileName);

					WorkbookSettings wbSettings = new WorkbookSettings();

					wbSettings.setLocale(new Locale("en", "EN"));

					WritableCellFormat timesBoldUnderline;
					WritableCellFormat header;
					WritableCellFormat headerBold;
					WritableCellFormat times;

					WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
					workbook.createSheet("hiredapplicants", 0);
					WritableSheet excelSheet = workbook.getSheet(0);

					WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
					// Define the cell format
					times = new WritableCellFormat(times10pt);
					// Lets automatically wrap the cells
					times.setWrap(true);

					// Create create a bold font with unterlines
					WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
					WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

					timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
					timesBoldUnderline.setAlignment(Alignment.CENTRE);
					// Lets automatically wrap the cells
					timesBoldUnderline.setWrap(true);

					header = new WritableCellFormat(times10ptBoldUnderline);
					headerBold = new WritableCellFormat(times10ptBoldUnderline);
					CellView cv = new CellView();
					cv.setFormat(times);
					cv.setFormat(timesBoldUnderline);
					cv.setAutosize(true);

					header.setBackground(Colour.GRAY_25);

					// Write a few headers
					excelSheet.mergeCells(0, 0, 6, 1);
					Label label;
					label = new Label(0, 0, "Applicants Hired", timesBoldUnderline);
					excelSheet.addCell(label);
					excelSheet.mergeCells(0, 3, 6, 3);
					label = new Label(0, 3, "");
					excelSheet.addCell(label);
					excelSheet.getSettings().setDefaultColumnWidth(18);
					
					int k=4;
					int col=1;
					label = new Label(0, k, Utility.getLocaleValuePropByKey("lblApplicantName", locale),header); 
					excelSheet.addCell(label);
					label = new Label(1, k, Utility.getLocaleValuePropByKey("lblJoTil", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblEPINormScore", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblDateHired", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgPositionNumber", locale),header); 
					excelSheet.addCell(label);
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgHiredforSchool", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgRegionName", locale),header); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgFT/PT", locale),header); 
					excelSheet.addCell(label);
					 
					
					k=k+1;
					if(gridJsonRecord[0].size()==0){	
						excelSheet.mergeCells(0, k, 9, k);
						label = new Label(0, k, Utility.getLocaleValuePropByKey("msgNorecordfound", locale)); 
						excelSheet.addCell(label);
					}

					String hiredDate = "N/A";
					
					if(gridJsonRecord[0].size()>0){
						System.out.println("gridJsonRecord[0]    "+gridJsonRecord[0].size());
						for (int i = 0; i < gridJsonRecord[0].size(); i++) {
							String finalSchool = "";
							JSONObject jsonrec = new JSONObject();
							jsonrec = gridJsonRecord[0].getJSONObject(i);
							col=1;
			
						    label = new Label(0, k, jsonrec.get("teacherName")+"\n"+"("+jsonrec.get("emailAddress")+")");
							excelSheet.addCell(label);
							
							label = new Label(1, k, jsonrec.get("jobTitle")+""); 
							excelSheet.addCell(label);
							
							label = new Label(++col, k, jsonrec.get("normScore")+""); 
							excelSheet.addCell(label);
							
							if(!jsonrec.get("hiredByDate").toString().equalsIgnoreCase("N/A"))
								hiredDate = new SimpleDateFormat("MMM dd, yyyy").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(jsonrec.get("hiredByDate").toString()));
							
							label = new Label(++col, k, hiredDate); 
							excelSheet.addCell(label);
							
							label = new Label(++col, k, jsonrec.get("requisitionNumber")+""); 
							excelSheet.addCell(label);
							
							if(!jsonrec.get("locationCode").toString().equalsIgnoreCase("N/A"))
								finalSchool = "("+jsonrec.get("locationCode")+") ";
							
							label = new Label(++col, k, ""+finalSchool+jsonrec.get("schoolName")); 
							excelSheet.addCell(label);
							
							label = new Label(++col, k, jsonrec.get("division")+""); 
							excelSheet.addCell(label);
							
							label = new Label(++col, k, jsonrec.get("postType")+""); 
							excelSheet.addCell(label);
							
							k++;
						 }
					}
			
			workbook.write();
			workbook.close();
		}catch(Exception e){}
	 return fileName;
 }
	
	public String displayRecordsByEntityTypePrintPreview_OP(int districtOrSchoolId,int schoolId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,String startDate,String endDate)
	{
		System.out.println(schoolId+" :: schoolId @@@@@@@@@@ "+sortOrder+"  AJAX  @@@@@@@@@@@@@ :: "+districtOrSchoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		int sortingcheck=1;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
			}
			
			
			/******************SWADESH********************/
			Date hiredStartdate=null;
			Date hiredEndDtae=null;
			boolean epiDateFlag=false;
			List<TeacherDetail> epiDateTeacherdetaDetailList = new ArrayList<TeacherDetail>();
			try{
				if(!startDate.equals("")){
					hiredStartdate=Utility.getCurrentDateFormart(startDate);
					epiDateFlag=true;
				}
				if(!endDate.equals("")){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(Utility.getCurrentDateFormart(endDate));
					cal2.add(Calendar.HOUR,23);
					cal2.add(Calendar.MINUTE,59);
					cal2.add(Calendar.SECOND,59);
					hiredEndDtae=cal2.getTime();
					epiDateFlag=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			//@@@@@@@@@@@@@@@@@@@@ start @@@@@@@@@@@@@@@@@@@@@@@@@@@
			noOfRow = "0";
			int start =0;
			int end =0 ;
			int totalRecord = 0;
			//------------------------------------
			 /** set default sorting fieldName **/
				String sortOrderFieldName="lastName";
				String sortOrderNoField="lastName";

				if(sortOrder.equalsIgnoreCase("") || sortOrder.equalsIgnoreCase("lastName"))
					sortingcheck=1;
				else if(sortOrder.equalsIgnoreCase("jobTitle"))
					sortingcheck=2;
				else  if(sortOrder.equalsIgnoreCase("requisitionNumber")){
					sortingcheck=3;
				}else
					sortingcheck=4;
				
				/**Start set dynamic sorting fieldName **/
				
				Order  sortOrderStrVal=null;
				 String query="";
					if(sortOrder!=null){
						if(!sortOrder.equals("") && !sortOrder.equals(null)){
							sortOrderFieldName=sortOrder;
							sortOrderNoField=sortOrder;
						}
					}

					String sortOrderTypeVal="0";
					if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
						if(sortOrderType.equals("0")){
							sortOrderStrVal=Order.asc(sortOrderFieldName);
							query=""+sortOrderFieldName+" asc ";
						}else{
							sortOrderTypeVal="1";
							sortOrderStrVal=Order.desc(sortOrderFieldName);
							query=""+sortOrderFieldName+" desc ";
						}
					}else{
						sortOrderTypeVal="0";
						sortOrderStrVal=Order.asc(sortOrderFieldName);
						query=""+sortOrderFieldName+" asc ";
					}
			
			//@@@@@@@@@@@@@@@@@@@@@ end  @@@@@@@@@@@@@@@@@@@@@@@@@
				JSONArray[] gridJsonRecord = new JSONArray[2];
				
				if(entityID==2)
				{
					gridJsonRecord = jobForTeacherDAO.getHiredGrid(schoolId, districtMaster,sortingcheck,query,hiredStartdate,hiredEndDtae,start,end,noOfRow); 
				}
				else if(entityID==1) 
				{
					if(districtOrSchoolId!=0)
						 districtMaster= districtMasterDAO.findById(districtOrSchoolId, false, false);
					gridJsonRecord = jobForTeacherDAO.getHiredGrid(schoolId, districtMaster,sortingcheck,query,hiredStartdate,hiredEndDtae,start,end,noOfRow);	 
				}
			


	        tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>"+Utility.getLocaleValuePropByKey("headAppHir", locale)+"</div><br/>");
			
			tmRecords.append("<div style='width:100%'>");
				tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
				tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
				tmRecords.append("<br/><br/>");
			tmRecords.append("</div>");
			
			
			tmRecords.append("<table  id='tblGridHired' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			
			
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblApplicantName", locale)+"</th>");
   
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJoTil", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblEPINormScore", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblDateHired", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgPositionNumber", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgHiredforSchool", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgRegionName", locale)+"</th>");
			
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("msgFT/PT", locale)+"</th>");
			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			tmRecords.append("<tbody>");
			String hiredDate = "N/A";
			if(gridJsonRecord[0].size()==0){
				tmRecords.append("<tr><td colspan='10' align='center' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			}
		
			if(gridJsonRecord[0].size()>0){
				System.out.println("gridJsonRecord[0]    "+gridJsonRecord[0].size());
				for (int i = 0; i < gridJsonRecord[0].size(); i++) {
				
				JSONObject jsonrec = new JSONObject();
				jsonrec = gridJsonRecord[0].getJSONObject(i);
				   tmRecords.append("<tr>");	
					tmRecords.append("<td style='font-size:11px;'>"+jsonrec.get("teacherName")+"\n"+"("+jsonrec.get("emailAddress")+")</td>");
					tmRecords.append("<td style='font-size:11px;'>"+jsonrec.get("jobTitle")+"</td>");
					
					tmRecords.append("<td style='font-size:11px;'>"+jsonrec.get("normScore")+"</td>");
					
					if(!jsonrec.get("hiredByDate").toString().equalsIgnoreCase("N/A"))
						hiredDate = new SimpleDateFormat("MMM dd, yyyy").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(jsonrec.get("hiredByDate").toString()));
					
					  tmRecords.append("<td style='font-size:11px;'>"+hiredDate+"</td>");

					  tmRecords.append("<td style='font-size:11px;'>"+jsonrec.get("requisitionNumber")+"</td>");
					
					  tmRecords.append("<td style='font-size:11px;'>");
					  if(!jsonrec.get("locationCode").toString().equalsIgnoreCase("N/A"))
							tmRecords.append("("+jsonrec.get("locationCode")+") ");
					  tmRecords.append(jsonrec.get("schoolName")+"</td>");
					tmRecords.append("<td style='font-size:11px;'>"+jsonrec.get("division")+"</td>");
					tmRecords.append("<td style='font-size:11px;'>"+jsonrec.get("postType")+"</td>");
					tmRecords.append("</tr>");	 
			   }
			}
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 return tmRecords.toString();
 }
}


