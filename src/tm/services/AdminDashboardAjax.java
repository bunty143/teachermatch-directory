package tm.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DashboardEmailsToTeacher;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageFromDashboard;
import tm.bean.RemovedTeacherMailDetails;
import tm.bean.TeacherAssessmentAttempt;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherEPIChangedStatusLog;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.DashboardEmailsToTeacherDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.MessageFromDashboardDAO;
import tm.dao.TeacherAssessmentAttemptDAO;
import tm.dao.TeacherAssessmentDetailDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherEPIChagedStatusLogDAO;
import tm.dao.TeacherStrikeLogDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.master.StatusMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.HTML2Text;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class AdminDashboardAjax {
	
	
	String locale = Utility.getValueOfPropByKey("locale");
	 	  
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) {
		this.teacherDetailDAO = teacherDetailDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO;
	public void setTeacherAssessmentAttemptDAO(TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO) {
		this.teacherAssessmentAttemptDAO = teacherAssessmentAttemptDAO;
	}

	@Autowired
	private TeacherStrikeLogDAO teacherStrikeLogDAO;
	public void setTeacherStrikeLogDAO(TeacherStrikeLogDAO teacherStrikeLogDAO) {
		this.teacherStrikeLogDAO = teacherStrikeLogDAO;
	}

	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) {
		this.emailerService = emailerService;
	}

	@Autowired
	private MessageFromDashboardDAO messageFromDashboardDAO;
	public void setMessageFromDashboardDAO(MessageFromDashboardDAO messageFromDashboardDAO) {
		this.messageFromDashboardDAO = messageFromDashboardDAO;
	}

	@Autowired
	private DashboardEmailsToTeacherDAO dashboardEmailsToTeacherDAO;
	public void setDashboardEmailsToTeacherDAO(DashboardEmailsToTeacherDAO dashboardEmailsToTeacherDAO) {
		this.dashboardEmailsToTeacherDAO = dashboardEmailsToTeacherDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private TeacherEPIChagedStatusLogDAO teacherEPIChagedStatusLogDAO;
	public void setTeacherEPIChagedStatusLogDAO(TeacherEPIChagedStatusLogDAO teacherEPIChagedStatusLogDAO) {
		this.teacherEPIChagedStatusLogDAO = teacherEPIChagedStatusLogDAO;
	}
	@Autowired
	private TeacherAssessmentDetailDAO teacherAssessmentDetailDAO;
	public void setTeacherAssessmentDetailDAO(TeacherAssessmentDetailDAO teacherAssessmentDetailDAO) {
		this.teacherAssessmentDetailDAO = teacherAssessmentDetailDAO;
	}
	
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;

	private static final Logger logger = Logger.getLogger(AdminDashboardAjax.class.getName());

	public String getYesterdayApplicants()
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		StringBuffer sb = new StringBuffer();

		Calendar cal = Calendar.getInstance();

		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Date dateWithoutTime = cal.getTime();
		System.out.println("dateWithoutTime: "+dateWithoutTime);

		Calendar cal1 = Calendar.getInstance();
		cal1.add(Calendar.DATE, -1);
		cal1.set(Calendar.HOUR_OF_DAY, 0);
		cal1.set(Calendar.MINUTE, 0);
		cal1.set(Calendar.SECOND, 0);
		cal1.set(Calendar.MILLISECOND, 0);

		System.out.println("Yesterday's date = "+ cal1.getTime());

		Criterion criterion1 = Restrictions.le("createdDateTime",dateWithoutTime);
		Criterion criterion2 = Restrictions.ge("createdDateTime",cal1.getTime());

		List<TeacherDetail> teacherDetails = teacherDetailDAO.findByCriteria(criterion1,criterion2);
		System.out.println("teacherDetails: "+teacherDetails.size());

		List<TeacherDetail> nativeTeacherDetails = new ArrayList<TeacherDetail>();
		List<TeacherDetail> referralTeacherDetails = new ArrayList<TeacherDetail>();
		for (TeacherDetail teacherDetail : teacherDetails) {
			if(teacherDetail.getUserType().equalsIgnoreCase("N"))
				nativeTeacherDetails.add(teacherDetail);
			else
				referralTeacherDetails.add(teacherDetail);
		}

		List<TeacherDetail> nativeAuthTeacherDetails = new ArrayList<TeacherDetail>();
		List<TeacherDetail> nativeUnAuthTeacherDetails = new ArrayList<TeacherDetail>();

		for (TeacherDetail teacherDetail : nativeTeacherDetails) {
			if(teacherDetail.getVerificationStatus()==1)
				nativeAuthTeacherDetails.add(teacherDetail);
			else
				nativeUnAuthTeacherDetails.add(teacherDetail);
		}

		/////referral
		List<TeacherDetail> referralAuthTeacherDetails = new ArrayList<TeacherDetail>();
		List<TeacherDetail> referralUnAuthTeacherDetails = new ArrayList<TeacherDetail>();

		for (TeacherDetail teacherDetail : referralTeacherDetails) {
			if(teacherDetail.getVerificationStatus()==1)
				referralAuthTeacherDetails.add(teacherDetail);
			else
				referralUnAuthTeacherDetails.add(teacherDetail);
		}

		List<TeacherDetail> teacherDetailsOnlyAuth = new ArrayList<TeacherDetail>();

		teacherDetailsOnlyAuth.addAll(nativeAuthTeacherDetails);
		teacherDetailsOnlyAuth.addAll(referralAuthTeacherDetails);


		List<TeacherAssessmentStatus> teacherAssessmentStatus = new ArrayList<TeacherAssessmentStatus>();
		// base status
		//List<TeacherAssessmentStatus> teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetailsOnlyAuth);
		if(teacherDetailsOnlyAuth.size()>0)
			teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachersYesterday(teacherDetailsOnlyAuth);

		List<TeacherDetail> teacherDetailsBaseCompletedNative = new ArrayList<TeacherDetail>();
		List<TeacherDetail> teacherDetailsBaseInCompletedNative = new ArrayList<TeacherDetail>();
		List<TeacherDetail> teacherDetailsBaseViolatedNative = new ArrayList<TeacherDetail>();

		List<TeacherDetail> teacherDetailsBaseCompletedReferral = new ArrayList<TeacherDetail>();
		List<TeacherDetail> teacherDetailsBaseInCompletedReferral = new ArrayList<TeacherDetail>();
		List<TeacherDetail> teacherDetailsBaseViolatedReferral = new ArrayList<TeacherDetail>();
		//System.out.println("teacherAssessmentStatus : "+teacherAssessmentStatus.size());

		for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {

			if(teacherAssessmentStatus2.getTeacherDetail().getUserType().equalsIgnoreCase("N"))
			{
				if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
					teacherDetailsBaseCompletedNative.add(teacherAssessmentStatus2.getTeacherDetail());
				else if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
					teacherDetailsBaseViolatedNative.add(teacherAssessmentStatus2.getTeacherDetail());
				else if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
					teacherDetailsBaseInCompletedNative.add(teacherAssessmentStatus2.getTeacherDetail());
				else
					teacherDetailsBaseInCompletedNative.add(teacherAssessmentStatus2.getTeacherDetail());
			}else
			{
				if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
					teacherDetailsBaseCompletedReferral.add(teacherAssessmentStatus2.getTeacherDetail());
				else if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
					teacherDetailsBaseViolatedReferral.add(teacherAssessmentStatus2.getTeacherDetail());
				else if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
					teacherDetailsBaseInCompletedReferral.add(teacherAssessmentStatus2.getTeacherDetail());
				else
					teacherDetailsBaseInCompletedReferral.add(teacherAssessmentStatus2.getTeacherDetail());
			}
		}
		/*	System.out.println("teacherDetailsBaseCompletedNative: "+teacherDetailsBaseCompletedNative.size());
		System.out.println("teacherDetailsBaseInCompletedNative: "+teacherDetailsBaseInCompletedNative.size());
		System.out.println("teacherDetailsBaseViolatedNative: "+teacherDetailsBaseViolatedNative.size());

		System.out.println("teacherDetailsBaseCompletedReferal: "+teacherDetailsBaseCompletedReferral.size());
		System.out.println("teacherDetailsBaseInCompletedReferal: "+teacherDetailsBaseInCompletedReferral.size());
		System.out.println("teacherDetailsBaseViolatedReferal: "+teacherDetailsBaseViolatedNative.size());*/

		List<TeacherDetail> baseCompletedTeaches = new ArrayList<TeacherDetail>();
		baseCompletedTeaches.addAll(teacherDetailsBaseCompletedNative);
		baseCompletedTeaches.addAll(teacherDetailsBaseCompletedReferral);
		//System.out.println("baseCompletedTeaches: "+baseCompletedTeaches.size());

		List<TeacherDetail> appliedJobTeachers = new ArrayList<TeacherDetail>();
		List<TeacherDetail> appliedJobTeachersNative = new ArrayList<TeacherDetail>();
		List<TeacherDetail> appliedJobTeachersReferral = new ArrayList<TeacherDetail>();
		/// job applied 
		if(baseCompletedTeaches.size()>0)
		{
			appliedJobTeachers = jobForTeacherDAO.getJobAppliedByTeachersYesterday(baseCompletedTeaches);
			//System.out.println("appliedJobTeachers: "+appliedJobTeachers.size());

			for (TeacherDetail teacherDetail : appliedJobTeachers) {
				if(teacherDetail.getUserType().equalsIgnoreCase("N"))
					appliedJobTeachersNative.add(teacherDetail);
				else
					appliedJobTeachersReferral.add(teacherDetail);
			}
		}
		//System.out.println("appliedJobTeachersNative: "+appliedJobTeachersNative.size());
		//System.out.println("appliedJobTeachersReferral: "+appliedJobTeachersReferral.size());
		/// job applied
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblTotalRegistered", locale)+" = "+teacherDetails.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblNative", locale)+" = "+nativeTeacherDetails.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblAuthenticated", locale)+" = "+nativeAuthTeacherDetails.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("sltEpiComp", locale)+" = "+teacherDetailsBaseCompletedNative.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblAppliedForJob", locale)+" = "+appliedJobTeachersNative.size());
		sb.append("</li>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblNotAppliedForJobs", locale)+" = "+(teacherDetailsBaseCompletedNative.size()-appliedJobTeachersNative.size()));
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("<li style=\"color: red;\">");
		if((nativeAuthTeacherDetails.size()-(teacherDetailsBaseCompletedNative.size()+teacherDetailsBaseViolatedNative.size()))>0)
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPIIncompleted", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('y','n',1);\">"+(nativeAuthTeacherDetails.size()-(teacherDetailsBaseCompletedNative.size()+teacherDetailsBaseViolatedNative.size()))+"</a>");
		else
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPIIncompleted", locale)+" = "+(nativeAuthTeacherDetails.size()-(teacherDetailsBaseCompletedNative.size()+teacherDetailsBaseViolatedNative.size()))+"");
		sb.append("</li>");
		sb.append("<li style=\"color: red;\">");
		//sb.append("EPI Timed Out = "+teacherDetailsBaseViolatedNative.size());
		if(teacherDetailsBaseViolatedNative.size()>0)
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPITimedOut", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('y','n',2);\">"+teacherDetailsBaseViolatedNative.size()+"</a>");
		else
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPITimedOut", locale)+" = "+teacherDetailsBaseViolatedNative.size());
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("<li style=\"color: red;\">");
		if(nativeUnAuthTeacherDetails.size()>0)
			sb.append(""+Utility.getLocaleValuePropByKey("lblUnauthenticated  ", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('y','n',0);\">"+nativeUnAuthTeacherDetails.size()+"</a>");
		else
			sb.append(""+Utility.getLocaleValuePropByKey("lblUnauthenticated  ", locale)+" = "+nativeUnAuthTeacherDetails.size()+"");
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblReferral ", locale)+" = "+referralTeacherDetails.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblAuthenticated", locale)+" = "+referralAuthTeacherDetails.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("sltEpiComp", locale)+" = "+teacherDetailsBaseCompletedReferral.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblAppliedForJob", locale)+" = "+appliedJobTeachersReferral.size());
		sb.append("</li>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblNotAppliedForJobs", locale)+" = "+(teacherDetailsBaseCompletedReferral.size()-appliedJobTeachersReferral.size()));
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("<li style=\"color: red;\">");
		if((referralAuthTeacherDetails.size()-(teacherDetailsBaseCompletedReferral.size()+teacherDetailsBaseViolatedReferral.size()))>0)
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPIIncompleted", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('y','r',1);\">"+(referralAuthTeacherDetails.size()-(teacherDetailsBaseCompletedReferral.size()+teacherDetailsBaseViolatedReferral.size()))+"</a>");
		else
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPIIncompleted", locale)+" = "+(referralAuthTeacherDetails.size()-(teacherDetailsBaseCompletedReferral.size()+teacherDetailsBaseViolatedReferral.size()))+"");
		sb.append("</li>");
		sb.append("<li style=\"color: red;\">");
		if(teacherDetailsBaseViolatedReferral.size()>0)
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPITimedOut", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('y','r',2);\">"+teacherDetailsBaseViolatedReferral.size()+"</a>");
		else
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPITimedOut", locale)+" = "+teacherDetailsBaseViolatedReferral.size());
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("<li style=\"color: red;\">");
		if(referralUnAuthTeacherDetails.size()>0)
			sb.append(""+Utility.getLocaleValuePropByKey("lblUnauthenticated  ", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('y','r',0);\">"+referralUnAuthTeacherDetails.size()+"</a>");
		else
			sb.append(""+Utility.getLocaleValuePropByKey("lblUnauthenticated  ", locale)+" = "+referralUnAuthTeacherDetails.size()+"");
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("</ul>");


		return sb.toString();

	}

	public String getTotalsOfDateApplicants()
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		StringBuffer sb = new StringBuffer();

		JSONObject jsonRec = teacherDetailDAO.getAdminTeacherList("");
		
		
		/*List<TeacherDetail> teacherDetails = teacherDetailDAO.findAll();
		System.out.println("teacherDetails: "+teacherDetails.size());

		List<TeacherDetail> nativeTeacherDetails = new ArrayList<TeacherDetail>();
		List<TeacherDetail> referralTeacherDetails = new ArrayList<TeacherDetail>();
		for (TeacherDetail teacherDetail : teacherDetails) {
			if(teacherDetail.getUserType().equalsIgnoreCase("N"))
				nativeTeacherDetails.add(teacherDetail);
			else
				referralTeacherDetails.add(teacherDetail);
		}

		List<TeacherDetail> nativeAuthTeacherDetails = new ArrayList<TeacherDetail>();
		List<TeacherDetail> nativeUnAuthTeacherDetails = new ArrayList<TeacherDetail>();

		for (TeacherDetail teacherDetail : nativeTeacherDetails) {
			if(teacherDetail.getVerificationStatus()==1)
				nativeAuthTeacherDetails.add(teacherDetail);
			else
				nativeUnAuthTeacherDetails.add(teacherDetail);
		}

		/////referral
		List<TeacherDetail> referralAuthTeacherDetails = new ArrayList<TeacherDetail>();
		List<TeacherDetail> referralUnAuthTeacherDetails = new ArrayList<TeacherDetail>();

		for (TeacherDetail teacherDetail : referralTeacherDetails) {
			if(teacherDetail.getVerificationStatus()==1)
				referralAuthTeacherDetails.add(teacherDetail);
			else
				referralUnAuthTeacherDetails.add(teacherDetail);
		}

		List<TeacherDetail> teacherDetailsOnlyAuth = new ArrayList<TeacherDetail>();

		teacherDetailsOnlyAuth.addAll(nativeAuthTeacherDetails);
		teacherDetailsOnlyAuth.addAll(referralAuthTeacherDetails);

		// base status
		List<TeacherAssessmentStatus> teacherAssessmentStatus = new ArrayList<TeacherAssessmentStatus>();
		if(teacherDetailsOnlyAuth.size()>0)
			teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetailsOnlyAuth);
		//List<TeacherAssessmentStatus> teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTotalTeachers();

		List<TeacherDetail> teacherDetailsBaseCompletedNative = new ArrayList<TeacherDetail>();
		List<TeacherDetail> teacherDetailsBaseInCompletedNative = new ArrayList<TeacherDetail>();
		List<TeacherDetail> teacherDetailsBaseViolatedNative = new ArrayList<TeacherDetail>();

		List<TeacherDetail> teacherDetailsBaseCompletedReferral = new ArrayList<TeacherDetail>();
		List<TeacherDetail> teacherDetailsBaseInCompletedReferral = new ArrayList<TeacherDetail>();
		List<TeacherDetail> teacherDetailsBaseViolatedReferral = new ArrayList<TeacherDetail>();
		//System.out.println("teacherAssessmentStatus : "+teacherAssessmentStatus.size());
		for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {

			if(teacherAssessmentStatus2.getTeacherDetail().getUserType().equalsIgnoreCase("N"))
			{
				if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
					teacherDetailsBaseCompletedNative.add(teacherAssessmentStatus2.getTeacherDetail());
				else if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
					teacherDetailsBaseViolatedNative.add(teacherAssessmentStatus2.getTeacherDetail());
				else if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
					teacherDetailsBaseInCompletedNative.add(teacherAssessmentStatus2.getTeacherDetail());
				else
					teacherDetailsBaseInCompletedNative.add(teacherAssessmentStatus2.getTeacherDetail());
			}else
			{
				if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
					teacherDetailsBaseCompletedReferral.add(teacherAssessmentStatus2.getTeacherDetail());
				else if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
					teacherDetailsBaseViolatedReferral.add(teacherAssessmentStatus2.getTeacherDetail());
				else if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
					teacherDetailsBaseInCompletedReferral.add(teacherAssessmentStatus2.getTeacherDetail());
				else
					teacherDetailsBaseInCompletedReferral.add(teacherAssessmentStatus2.getTeacherDetail());
			}
		}
		System.out.println("teacherDetailsBaseCompletedNative: "+teacherDetailsBaseCompletedNative.size());
		System.out.println("teacherDetailsBaseInCompletedNative: "+teacherDetailsBaseInCompletedNative.size());
		System.out.println("teacherDetailsBaseViolatedNative: "+teacherDetailsBaseViolatedNative.size());

		System.out.println("teacherDetailsBaseCompleted: "+teacherDetailsBaseCompletedReferral.size());
		System.out.println("teacherDetailsBaseInCompleted: "+teacherDetailsBaseInCompletedReferral.size());
		System.out.println("teacherDetailsBaseViolated: "+teacherDetailsBaseViolatedNative.size());

		List<TeacherDetail> baseCompletedTeaches = new ArrayList<TeacherDetail>();
		baseCompletedTeaches.addAll(teacherDetailsBaseCompletedNative);
		baseCompletedTeaches.addAll(teacherDetailsBaseCompletedReferral);
		//System.out.println("baseCompletedTeaches: "+baseCompletedTeaches.size());

		List<TeacherDetail> appliedJobTeachers = new ArrayList<TeacherDetail>();
		List<TeacherDetail> appliedJobTeachersNative = new ArrayList<TeacherDetail>();
		List<TeacherDetail> appliedJobTeachersReferral = new ArrayList<TeacherDetail>();

		if(baseCompletedTeaches.size()>0)
		{
			appliedJobTeachers = jobForTeacherDAO.getJobAppliedByTeachers(baseCompletedTeaches);
			//System.out.println("appliedJobTeachers: "+appliedJobTeachers.size());
			for (TeacherDetail teacherDetail : appliedJobTeachers) {
				if(teacherDetail.getUserType().equalsIgnoreCase("N"))
					appliedJobTeachersNative.add(teacherDetail);
				else
					appliedJobTeachersReferral.add(teacherDetail);
			}
		}
		//System.out.println("appliedJobTeachersNative: "+appliedJobTeachersNative.size());
		//System.out.println("appliedJobTeachersReferral: "+appliedJobTeachersReferral.size());
		//////////////////////////////////////////////////////////////////////////


		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblTotalRegistered", locale)+" = "+teacherDetails.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblNative", locale)+"   = "+nativeTeacherDetails.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblAuthenticated  ", locale)+" = "+nativeAuthTeacherDetails.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("sltEpiComp", locale)+" = "+teacherDetailsBaseCompletedNative.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblAppliedForJob", locale)+" = "+appliedJobTeachersNative.size());
		sb.append("</li>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblNotAppliedForJobs", locale)+" = "+(teacherDetailsBaseCompletedNative.size()-appliedJobTeachersNative.size()));
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("<li style=\"color: red;\">");
		if((nativeAuthTeacherDetails.size()-(teacherDetailsBaseCompletedNative.size()+teacherDetailsBaseViolatedNative.size()))>0)
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPIIncompleted", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('t','n',1);\">"+(nativeAuthTeacherDetails.size()-(teacherDetailsBaseCompletedNative.size()+teacherDetailsBaseViolatedNative.size()))+"</a>");
		else
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPIIncompleted", locale)+" = "+(nativeAuthTeacherDetails.size()-(teacherDetailsBaseCompletedNative.size()+teacherDetailsBaseViolatedNative.size()))+"");
		sb.append("</li>");
		sb.append("<li style=\"color: red;\">");
		if(teacherDetailsBaseViolatedNative.size()>0)
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPITimedOut", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('t','n',2);\">"+teacherDetailsBaseViolatedNative.size()+"</a>");
		else
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPITimedOut", locale)+" = "+teacherDetailsBaseViolatedNative.size());
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("<li style=\"color: red;\">");
		if(nativeUnAuthTeacherDetails.size()>0)
			sb.append(""+Utility.getLocaleValuePropByKey("lblUnauthenticated  ", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('t','n',0);\">"+nativeUnAuthTeacherDetails.size()+"</a>");
		else
			sb.append(""+Utility.getLocaleValuePropByKey("lblUnauthenticated  ", locale)+" = "+nativeUnAuthTeacherDetails.size()+"");
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblReferral ", locale)+" = "+referralTeacherDetails.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblAuthenticated  ", locale)+" = "+referralAuthTeacherDetails.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("sltEpiComp", locale)+" = "+teacherDetailsBaseCompletedReferral.size());
		sb.append("<ul>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblAppliedForJob", locale)+" = "+appliedJobTeachersReferral.size());
		sb.append("</li>");
		sb.append("<li>");
		sb.append(""+Utility.getLocaleValuePropByKey("lblNotAppliedForJobs", locale)+" = "+(teacherDetailsBaseCompletedReferral.size()-appliedJobTeachersReferral.size()));
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("<li style=\"color: red;\">");
		if((referralAuthTeacherDetails.size()-(teacherDetailsBaseCompletedReferral.size()+teacherDetailsBaseViolatedReferral.size()))>0)
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPIIncompleted", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('t','r',1);\">"+(referralAuthTeacherDetails.size()-(teacherDetailsBaseCompletedReferral.size()+teacherDetailsBaseViolatedReferral.size()))+"</a>");
		else
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPIIncompleted", locale)+" = "+(referralAuthTeacherDetails.size()-(teacherDetailsBaseCompletedReferral.size()+teacherDetailsBaseViolatedReferral.size()))+"");
		sb.append("</li>");
		sb.append("<li style=\"color: red;\">");
		if(teacherDetailsBaseViolatedReferral.size()>0)
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPITimedOut", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('t','r',2);\">"+teacherDetailsBaseViolatedReferral.size()+"</a>");
		else
			sb.append(""+Utility.getLocaleValuePropByKey("lblEPITimedOut", locale)+" = "+teacherDetailsBaseViolatedReferral.size());
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("<li style=\"color: red;\">");
		if(referralUnAuthTeacherDetails.size()>0)
			sb.append(""+Utility.getLocaleValuePropByKey("lblUnauthenticated  ", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('t','r',0);\">"+referralUnAuthTeacherDetails.size()+"</a>");
		else
			sb.append(""+Utility.getLocaleValuePropByKey("lblUnauthenticated  ", locale)+" = "+referralUnAuthTeacherDetails.size()+"");
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("</ul>");
		sb.append("</li>");
		sb.append("</ul>");*/

		try {
			sb.append("<ul>");
			sb.append("<li>");
			sb.append(""+Utility.getLocaleValuePropByKey("lblTotalRegistered", locale)+" = "+jsonRec.getString("total"));
			sb.append("<ul>");
			sb.append("<li>");
			sb.append(""+Utility.getLocaleValuePropByKey("lblNative", locale)+"   = "+jsonRec.getString("native_teacher"));
			sb.append("<ul>");
			sb.append("<li>");
			sb.append(""+Utility.getLocaleValuePropByKey("lblAuthenticated  ", locale)+" = "+jsonRec.getString("authentication_native"));
			sb.append("<ul>");
			sb.append("<li>");
			sb.append(""+Utility.getLocaleValuePropByKey("sltEpiComp", locale)+" = "+jsonRec.getString("emp_comp_native"));
			sb.append("<ul>");
			sb.append("<li>");
			sb.append(""+Utility.getLocaleValuePropByKey("lblAppliedForJob", locale)+" = "+jsonRec.getString("emp_comp_native_apply"));
			sb.append("</li>");
			sb.append("<li>");
			sb.append(""+Utility.getLocaleValuePropByKey("lblNotAppliedForJobs", locale)+" = "+jsonRec.getString("no_apply_anyJob_native"));
			sb.append("</li>");
			sb.append("</ul>");
			sb.append("</li>");
			sb.append("<li style=\"color: red;\">");
			int epiInCompleted = jsonRec.getString("time_out_native")==null?0:jsonRec.getInt("time_out_native");
			if(epiInCompleted>0)
				sb.append(""+Utility.getLocaleValuePropByKey("lblEPIIncompleted", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('t','n',1);\">"+jsonRec.getString("time_out_native")+"</a>");
			else
				sb.append(""+Utility.getLocaleValuePropByKey("lblEPIIncompleted", locale)+" = "+jsonRec.getString("time_out_native")+"");
			sb.append("</li>");
			sb.append("<li style=\"color: red;\">");
			int epiTimedOut = jsonRec.getString("emp_Incomp_native")==null?0:jsonRec.getInt("emp_Incomp_native");
			if(epiTimedOut>0)
				sb.append(""+Utility.getLocaleValuePropByKey("lblEPITimedOut", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('t','n',2);\">"+jsonRec.getString("emp_Incomp_native")+"</a>");
			else
				sb.append(""+Utility.getLocaleValuePropByKey("lblEPITimedOut", locale)+" = "+jsonRec.getString("emp_Incomp_native"));
			sb.append("</li>");
			sb.append("</ul>");
			sb.append("</li>");
			sb.append("<li style=\"color: red;\">");
			int unAuthenticated = jsonRec.getString("unauthentication_native")==null?0:jsonRec.getInt("unauthentication_native");
			if(unAuthenticated>0)
				sb.append(""+Utility.getLocaleValuePropByKey("lblUnauthenticated  ", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('t','n',0);\">"+jsonRec.getString("unauthentication_native")+"</a>");
			else
				sb.append(""+Utility.getLocaleValuePropByKey("lblUnauthenticated  ", locale)+" = "+jsonRec.getString("unauthentication_native")+"");
			sb.append("</li>");
			sb.append("</ul>");
			sb.append("</li>");
			sb.append("<li>");
			sb.append(""+Utility.getLocaleValuePropByKey("lblReferral ", locale)+" = "+jsonRec.getString("referal_teacher"));
			sb.append("<ul>");
			sb.append("<li>");
			sb.append(""+Utility.getLocaleValuePropByKey("lblAuthenticated  ", locale)+" = "+jsonRec.getString("authentication_referal"));///////////////////
			sb.append("<ul>");
			sb.append("<li>");
			sb.append(""+Utility.getLocaleValuePropByKey("sltEpiComp", locale)+" = "+jsonRec.getString("emp_comp_referal"));
			sb.append("<ul>");
			sb.append("<li>");
			sb.append(""+Utility.getLocaleValuePropByKey("lblAppliedForJob", locale)+" = "+jsonRec.getString("emp_comp_referal_apply"));
			sb.append("</li>");
			sb.append("<li>");
			sb.append(""+Utility.getLocaleValuePropByKey("lblNotAppliedForJobs", locale)+" = "+jsonRec.getString("no_apply_anyJob_referal"));
			sb.append("</li>");
			sb.append("</ul>");
			sb.append("</li>");
			sb.append("<li style=\"color: red;\">");
			
			int epiIncompetedReferal = jsonRec.getString("EPI_Incomp_erferal")==null?0:jsonRec.getInt("EPI_Incomp_erferal");
			if(epiIncompetedReferal>0)
				sb.append(""+Utility.getLocaleValuePropByKey("lblEPIIncompleted", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('t','r',1);\">"+jsonRec.getString("EPI_Incomp_erferal")+"</a>");
			else
				sb.append(""+Utility.getLocaleValuePropByKey("lblEPIIncompleted", locale)+" = "+jsonRec.getString("EPI_Incomp_erferal")+"");
			sb.append("</li>");
			sb.append("<li style=\"color: red;\">");
			int epiTimedOutReferal = jsonRec.getString("emp_Incomp_referal")==null?0:jsonRec.getInt("emp_Incomp_referal");
			if(epiTimedOutReferal>0)
				sb.append(""+Utility.getLocaleValuePropByKey("lblEPITimedOut", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('t','r',2);\">"+jsonRec.getString("emp_Incomp_referal")+"</a>");
			else
				sb.append(""+Utility.getLocaleValuePropByKey("lblEPITimedOut", locale)+" = "+jsonRec.getString("emp_Incomp_referal"));
			sb.append("</li>");
			sb.append("</ul>");
			sb.append("</li>");
			sb.append("<li style=\"color: red;\">");
			int unAuthenticatedReferal = jsonRec.getString("unauthentication_referal")==null?0:jsonRec.getInt("unauthentication_referal");
			if(unAuthenticatedReferal>0)
				sb.append(""+Utility.getLocaleValuePropByKey("lblUnauthenticated  ", locale)+" = <a href='javascript:void(0);' onclick=\"getCandidates('t','r',0);\">"+jsonRec.getString("unauthentication_referal")+"</a>");
			else
				sb.append(""+Utility.getLocaleValuePropByKey("lblUnauthenticated  ", locale)+" = "+jsonRec.getString("unauthentication_referal")+"");
			sb.append("</li>");
			sb.append("</ul>");
			sb.append("</li>");
			sb.append("</ul>");
			sb.append("</li>");
			sb.append("</ul>");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();

	}
	public String getApplicants(String firstName,String lastName, String emailAddress,String p,String q,int r,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		int s = r;
		r = (r==2?1:r);

		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start = ((pgNo-1)*noOfRowInPage);
		int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;

		SortedMap map = new TreeMap();
		List<TeacherDetail> lstTeacherDetail	  	=	null;
		/** set default sorting fieldName **/
		/*String sortOrderFieldName	=	"emailAddress";
		String sortOrderNoField		=	"emailAddress";*/
		String sortOrderFieldName	=	"firstName";
		String sortOrderNoField		=	"firstName";
		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal		=	null;
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null) ){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
			}
			if(sortOrderNoField.equals("firstName"))
			{
				sortOrderFieldName	=	"firstName";
			}
		}

		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null))
		{
			if(sortOrderType.equals("0"))
			{
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else
			{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}
		else
		{
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
		/**End ------------------------------------**/

		//Criterion criterion1 = Restrictions.eq("userType",q);
		//Criterion criterion2 = Restrictions.eq("verificationStatus",r);
		List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
		List<TeacherAssessmentdetail> teacherAssessmentdetails = new ArrayList<TeacherAssessmentdetail>();
		List<TeacherDetail> teacherDetailsIncompleted = new ArrayList<TeacherDetail>();
		List<TeacherDetail> teacherDetailsViolated = new ArrayList<TeacherDetail>();

		if(p.equalsIgnoreCase("y"))
		{
			/*Calendar cal = Calendar.getInstance();

			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			Date dateWithoutTime = cal.getTime();
			System.out.println("dateWithoutTime: "+dateWithoutTime);

			Calendar cal1 = Calendar.getInstance();
			cal1.add(Calendar.DATE, -1);
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);

			System.out.println("Yesterday's date = "+ cal1.getTime());

			Criterion criterion3 = Restrictions.le("createdDateTime",dateWithoutTime);
			Criterion criterion4 = Restrictions.ge("createdDateTime",cal1.getTime());*/
			if(r==0)
			{
				//teacherDetails = teacherDetailDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4);
				teacherDetails	=	teacherDetailDAO.getApplicantsByEPI(firstName, lastName, emailAddress, sortOrderStrVal,p, q, r);



			}else
			{
				//teacherDetails = teacherDetailDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4);
				teacherDetails	=	teacherDetailDAO.getApplicantsByEPI(firstName, lastName, emailAddress, sortOrderStrVal,p, q, r);

				List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
				if(teacherDetails.size()>0)
				{

					System.out.println("vv teacherDetails: "+teacherDetails.size());
					//yesterday
					//teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetails);
					teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTakenByTeachersYesterday(teacherDetails);
					System.out.println("teacherAssessmentStatusList : "+teacherAssessmentStatusList.size());
					if(s==2) //violated
					{
						for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatusList) {
							if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
							{
								teacherDetailsViolated.add(teacherAssessmentStatus2.getTeacherDetail());
								teacherAssessmentdetails.add(teacherAssessmentStatus2.getTeacherAssessmentdetail());
							}
						}
						teacherDetails.retainAll(teacherDetailsViolated);
						teacherDetailsIncompleted = teacherDetailsViolated;

					}else{

						for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatusList) {
							if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") || teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
							{
								teacherDetails.remove(teacherAssessmentStatus2.getTeacherDetail());
								//System.out.println("td : "+teacherAssessmentStatus2.getTeacherDetail()+" "+teacherAssessmentStatus2.getTeacherDetail().getTeacherId());
								//System.out.println("List : "+teacherDetails);
							}
							else
							{
								teacherAssessmentdetails.add(teacherAssessmentStatus2.getTeacherAssessmentdetail());
								teacherDetailsIncompleted.add(teacherAssessmentStatus2.getTeacherDetail());
							}
						}
					}

				}

			}
			System.out.println("teacherDetails: "+teacherDetails.size());
		}else // total 
		{
			if(r==0)
			{
				//teacherDetails = teacherDetailDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2);
				teacherDetails	=	teacherDetailDAO.getApplicantsByEPI(firstName, lastName, emailAddress, sortOrderStrVal,p, q, r);
			}else
			{
				//teacherDetails = teacherDetailDAO.findByCriteria(sortOrderStrVal,criterion1,criterion2);
				teacherDetails	=	teacherDetailDAO.getApplicantsByEPI(firstName, lastName, emailAddress, sortOrderStrVal,p, q, r);
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
				if(teacherDetails.size()>0)
				{
					System.out.println("vv teacherDetails: "+teacherDetails.size());
					teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetails);

					if(s==2) //violated
					{
						for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatusList) {
							if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
							{
								teacherDetailsViolated.add(teacherAssessmentStatus2.getTeacherDetail());
								teacherAssessmentdetails.add(teacherAssessmentStatus2.getTeacherAssessmentdetail());
							}
						}
						teacherDetails.retainAll(teacherDetailsViolated);
						teacherDetailsIncompleted = teacherDetailsViolated;
					}else
					{
						for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatusList) {
							if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") || teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
								teacherDetails.remove(teacherAssessmentStatus2.getTeacherDetail());
							else
							{
								teacherAssessmentdetails.add(teacherAssessmentStatus2.getTeacherAssessmentdetail());
								teacherDetailsIncompleted.add(teacherAssessmentStatus2.getTeacherDetail());
							}
						}
					}

				}
			}
			System.out.println("ss teacherDetails: "+teacherDetails.size());
		}


		List<TeacherDetail> sortedlstTeacherDetail		=	new ArrayList<TeacherDetail>();

		SortedMap<String,TeacherDetail>	sortedMap = new TreeMap<String,TeacherDetail>();
		if(sortOrderNoField.equals("firstName"))
		{
			sortOrderFieldName	=	"firstName";
		}

		int mapFlag=2;
		for (TeacherDetail teacherDetail : teacherDetails){
			String orderFieldName=teacherDetail.getEmailAddress();


		}
		if(mapFlag==1){
			NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
			for (Iterator iter=navig.iterator();iter.hasNext();) {  
				Object key = iter.next(); 
				sortedlstTeacherDetail.add((TeacherDetail) sortedMap.get(key));
			} 
		}else if(mapFlag==0){
			Iterator iterator = sortedMap.keySet().iterator();
			while (iterator.hasNext()) {
				Object key = iterator.next();
				sortedlstTeacherDetail.add((TeacherDetail) sortedMap.get(key));
			}
		}else{
			sortedlstTeacherDetail=teacherDetails;
		}

		totalRecord =sortedlstTeacherDetail.size();

		if(totalRecord<end)
			end=totalRecord;
		List<TeacherDetail> lstsortedTeacherDetail		=	sortedlstTeacherDetail.subList(start,end);

		Map<Integer,Integer> assessmentAttempsMap = new HashMap<Integer, Integer>();
		Map<Integer,Integer> assessmentStrikeMap = new HashMap<Integer, Integer>();
		Map<Integer,Integer> dashboardEmailsToTeacherMap = new HashMap<Integer, Integer>();

		//DashboardEmailsToTeacher
		if(teacherDetailsIncompleted.size()>0 && r==1)
		{
			//assessmentAttempsMap = teacherAssessmentAttemptDAO.findAssessmentsResponsesForBase(teacherDetailsIncompleted);
			assessmentAttempsMap = teacherAssessmentAttemptDAO.findAssessmentsResponsesForBase(lstsortedTeacherDetail);
			//System.out.println("ass: "+assessmentAttempsMap.size());
			if(teacherAssessmentdetails.size()>0)
			{
				//assessmentStrikeMap = teacherStrikeLogDAO.findAssessmentsStrikesForBase(teacherDetailsIncompleted,teacherAssessmentdetails);
				assessmentStrikeMap = teacherStrikeLogDAO.findAssessmentsStrikesForBase(lstsortedTeacherDetail,teacherAssessmentdetails);
				//System.out.println("ass: "+assessmentStrikeMap.size());
			}
		}

		//System.out.println(" ========= lstsortedTeacherDetail ========== "+lstsortedTeacherDetail.size());

		//dashboardEmailsToTeacherMap = dashboardEmailsToTeacherDAO.findMailSentToTeachers(teacherDetails);
		if(lstsortedTeacherDetail.size()>0)
			dashboardEmailsToTeacherMap = dashboardEmailsToTeacherDAO.findMailSentToTeachers(lstsortedTeacherDetail);

		//System.out.println("LLLLLLLL assessmentAttempsMap : "+assessmentAttempsMap.get(36));
		//System.out.println("LLLLLLLL assessmentStrikeMap : "+assessmentStrikeMap.get(36));

		StringBuffer sb = new StringBuffer();
		String responseText = "";
		sb.append("<table  id='tblGrid' width='100%' border='0'>");
		sb.append("<thead class='bg'>");
		sb.append("<tr>");
		sb.append("<th valign='top'>&nbsp;</th>");
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblNm", locale),sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo);
		sb.append("<th valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblEmail1", locale),sortOrderFieldName,"emailAddress",sortOrderTypeVal,pgNo);
		sb.append("<th valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblRegisterdOn", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
		sb.append("<th valign='top'>"+responseText+"</th>");
		responseText=Utility.getLocaleValuePropByKey("lblEPIofAttempt", locale);
		sb.append("<th valign='top'>"+responseText+"</th>");
		//responseText="Send A Message";
		//sb.append("<th valign='top'>"+responseText+"</th>");
		responseText="# "+Utility.getLocaleValuePropByKey("lblofMessagesSent", locale);
		sb.append("<th valign='top'>"+responseText+"</th>");
		if(s==2)
			sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
		sb.append("</tr>");
		sb.append("</thead>");
		if(lstsortedTeacherDetail.size()==0)
			sb.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
		int epiAttempts = 0;
		int cnt=0;
		int mailSent = 0;
		Integer atmp = 0;
		Integer strk = 0;
		//for (TeacherDetail teacherDetail : teacherDetails) {
		for (TeacherDetail teacherDetail : lstsortedTeacherDetail) {
			//te+=teacherDetail.getTeacherId()+",";
			cnt++;
			sb.append("<tr>");
			sb.append("<td><input type='checkbox' class='case' name='case' value='"+teacherDetail.getTeacherId()+"'> </td>");
			sb.append("<td>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"</td>");
			sb.append("<td>"+teacherDetail.getEmailAddress()+"</td>");
			sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(teacherDetail.getCreatedDateTime())+"</td>");
			if(r==1)
			{
				//epiAttempts=assessmentAttempsMap.get(teacherDetail.getTeacherId())==null?0:assessmentAttempsMap.get(teacherDetail.getTeacherId());
				//epiAttempts+=assessmentStrikeMap.get(teacherDetail.getTeacherId())==null?0:assessmentStrikeMap.get(teacherDetail.getTeacherId());
				atmp=assessmentAttempsMap.get(teacherDetail.getTeacherId());
				epiAttempts=atmp==null?0:atmp;
				strk=assessmentStrikeMap.get(teacherDetail.getTeacherId());
				epiAttempts+=strk==null?0:strk;

				if(s==2)
					epiAttempts=epiAttempts<3?epiAttempts:3;
				else
					epiAttempts=epiAttempts<2?epiAttempts:2;
			}
			sb.append("<td> "+epiAttempts+"</td>");
			mailSent = dashboardEmailsToTeacherMap.get(teacherDetail.getTeacherId())==null?0:dashboardEmailsToTeacherMap.get(teacherDetail.getTeacherId());
			//System.out.println("          = Mail Sent  "+mailSent);
			sb.append("<td> "+mailSent+"</td>");
			if(s==2)
				sb.append("<td>&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("titleMakeEPIIncomplete", locale)+"' rel='tooltip' id='tpIncomplete"+cnt+"' onclick=\"makeEPIIncomplete("+teacherDetail.getTeacherId()+")\" href='javascript:void(0);'><img src='images/option08.png' width='24px'></a></td>");

			sb.append("</tr>");
		}
		sb.append("</table >");
		sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
		//System.out.println("te: "+te);

		return sb.toString();
	}

	public String sendMessagesToApplicants(List<Integer> teacherIds)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		System.out.println("teacherDetails.size() : "+teacherIds.size());
		Criterion criterion1 = Restrictions.in("teacherId",teacherIds);

		try {
			String defaultMessage = "";
			String subject = "";
			List<MessageFromDashboard> messageFromDashboardList = messageFromDashboardDAO.findByCriteria();
			if(messageFromDashboardList.size()>0)
			{
				defaultMessage = messageFromDashboardList.get(0).getMessage();
				subject = messageFromDashboardList.get(0).getSubject();
			}

			String mailText = null;
			List<TeacherDetail> teacherDetails = teacherDetailDAO.findByCriteria(criterion1);
			for (TeacherDetail teacherDetail : teacherDetails) {

				mailText = MailText.getTeacherMailText(request,teacherDetail,defaultMessage);

				DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
				dashboardEmailsToTeacher.setCreatedDateTime(new Date());
				dashboardEmailsToTeacher.setEmailAddress(teacherDetail.getEmailAddress());
				dashboardEmailsToTeacher.setSubject(subject);
				dashboardEmailsToTeacher.setMailText(mailText);
				dashboardEmailsToTeacher.setEmailType("Manual");
				dashboardEmailsToTeacher.setTeacherDetail(teacherDetail);
				dashboardEmailsToTeacherDAO.makePersistent(dashboardEmailsToTeacher);

				emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), subject,mailText);
				System.out.println(teacherDetail.getEmailAddress());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "1";
	}
	public MessageFromDashboard getDefaultMsg(String teacherStatus)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		try {
			if(teacherStatus==null)
			{
				teacherStatus = "DashBoradMail";
				Criterion criterion = Restrictions.eq("teacherStatus",teacherStatus);
				List<MessageFromDashboard> messageFromDashboardList = messageFromDashboardDAO.findByCriteria(criterion);
				if(messageFromDashboardList.size()>0)
					return messageFromDashboardList.get(0);
			}else
			{
				Criterion criterion = Restrictions.eq("teacherStatus",teacherStatus);
				List<MessageFromDashboard> messageFromDashboardList = messageFromDashboardDAO.findByCriteria(criterion);
				if(messageFromDashboardList.size()>0)
					return messageFromDashboardList.get(0);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new MessageFromDashboard();
	}
	public String setDefaultMsg(Integer messageId,String subject,String message,String teacherStatus)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
		MessageFromDashboard messageFromDashboard = new MessageFromDashboard();
		try {

			if(messageId>0)
				messageFromDashboard.setMessageId(messageId);

			messageFromDashboard.setCreatedDateTime(new Date());
			messageFromDashboard.setUserMaster(userMaster);
			messageFromDashboard.setMessage(message);
			messageFromDashboard.setSubject(Utility.trim(subject));
			messageFromDashboard.setTeacherStatus(teacherStatus);
			messageFromDashboardDAO.makePersistent(messageFromDashboard);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ""+messageFromDashboard.getMessageId();
	}
	public String sendNotificationMessagesToApplicants(List<Integer> teacherIds,String teacherStatus)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		System.out.println("teacherDetails.size() : "+teacherIds.size());
		Criterion criterion1 = Restrictions.in("teacherId",teacherIds);
		Criterion criterion2 = Restrictions.eq("teacherStatus",teacherStatus);

		try {
			String defaultMessage = "";
			String subject = "";
			List<MessageFromDashboard> messageFromDashboardList = messageFromDashboardDAO.findByCriteria(criterion2);
			if(messageFromDashboardList.size()>0)
			{
				defaultMessage = messageFromDashboardList.get(0).getMessage();
				subject = messageFromDashboardList.get(0).getSubject();
			}

			SessionFactory sessionFactory =  teacherDetailDAO.getSessionFactory();
			StatelessSession session1 = sessionFactory.openStatelessSession();

			String mailText = null;
			List<TeacherDetail> teacherDetails = teacherDetailDAO.findByCriteria(criterion1);
			for (TeacherDetail teacherDetail : teacherDetails) {

				//mailText = MailText.getTeacherMailText(request,teacherDetail,defaultMessage);
				mailText = defaultMessage;

				DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
				dashboardEmailsToTeacher.setCreatedDateTime(new Date());
				dashboardEmailsToTeacher.setEmailAddress(teacherDetail.getEmailAddress());
				dashboardEmailsToTeacher.setSubject(subject);
				dashboardEmailsToTeacher.setMailText(mailText);
				dashboardEmailsToTeacher.setEmailType("Manual");
				dashboardEmailsToTeacher.setTeacherDetail(teacherDetail);
				dashboardEmailsToTeacher.setTeacherStatus(teacherStatus);
				session1.insert(dashboardEmailsToTeacher);
				//dashboardEmailsToTeacherDAO.makePersistent(dashboardEmailsToTeacher);

				emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), subject,mailText);
				//emailerService.sendMailAsHTMLText("vishwanath@netsutra.com", subject,mailText);
				System.out.println("email: "+teacherDetail.getEmailAddress());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "1";
	}
	@Transactional(readOnly=false)
	public String makeEPIIncomplete(TeacherDetail teacherDetail,String resetMessage)
	{	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userMaster = (UserMaster)session.getAttribute("userMaster"); 
		try {
			TeacherAssessmentStatus teacherAssessmentStatus = null;
			TeacherAssessmentdetail teacherAssessmentdetail = null;
			AssessmentDetail assessmentDetail = null;

			JobOrder jobOrder = new JobOrder();
			jobOrder.setJobId(0);
			List<TeacherAssessmentStatus> teacherAssessmentStatuList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail, jobOrder);
			if(teacherAssessmentStatuList.size()>0)
			{
				teacherAssessmentStatus = teacherAssessmentStatuList.get(0);
				teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();
				assessmentDetail = teacherAssessmentStatus.getAssessmentDetail();
				teacherDetail = teacherAssessmentStatus.getTeacherDetail();

				// Delete strike 
				Session session2 =  teacherStrikeLogDAO.getSession();

				String hql = "delete from teacherstrikelog where teacherAssessmentId = :teacherAssessmentId";
				Query query = session2.createQuery(hql);
				query.setInteger("teacherAssessmentId",teacherAssessmentdetail.getTeacherAssessmentId());
				int rowCount = query.executeUpdate();
				System.out.println("Rows affected: " + rowCount);

				//List<TeacherAssessmentAttempt> teacherAssessmentAttempts = teacherAssessmentAttemptDAO.findNoOfAttempts(assessmentDetail, teacherDetail, teacherAssessmentdetail.getAssessmentTakenCount());
				List<TeacherAssessmentAttempt> teacherAssessmentAttempts = teacherAssessmentAttemptDAO.findNoOfAttempts(assessmentDetail, teacherDetail, teacherAssessmentdetail);
				System.out.println("teacherAssessmentAttempts.size() : "+teacherAssessmentAttempts.size());
				TeacherAssessmentAttempt teacherAssessmentAttempt = teacherAssessmentAttempts.get(0);
				//System.out.println(teacherAssessmentAttempt);
				if(teacherAssessmentAttempts.size()>1)
				{
					teacherAssessmentAttempt = teacherAssessmentAttempts.get(0);

					teacherAssessmentAttempts.remove(teacherAssessmentAttempt);

					for (TeacherAssessmentAttempt teacherAssessmentAttempt1 : teacherAssessmentAttempts) {
						System.out.println(teacherAssessmentAttempt1.getAttemptId());
						teacherAssessmentAttemptDAO.makeTransient(teacherAssessmentAttempt1);
					}
				}

				//System.out.println("dd: "+teacherAssessmentAttempt);

				Integer assessmentSessionTime = teacherAssessmentdetail.getAssessmentSessionTime();
				if(assessmentSessionTime <= teacherAssessmentAttempt.getAssessmentSessionTime())
					teacherAssessmentAttempt.setAssessmentSessionTime(0);

				teacherAssessmentAttempt.setIsForced(false);
				//System.out.println("attempt: "+teacherAssessmentAttempt.getAttemptId());
				teacherAssessmentAttemptDAO.makePersistent(teacherAssessmentAttempt);

				StatusMaster statusMasterIncomp = WorkThreadServlet.statusMap.get("icomp");
				teacherAssessmentStatus.setStatusMaster(statusMasterIncomp);
				teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);

				//////////////////////////////////////////////////////////////////

				StatusMaster statusMasterVlt = WorkThreadServlet.statusMap.get("vlt");
				StatusMaster statusMasterComp = WorkThreadServlet.statusMap.get("comp");
				List<JobForTeacher> lstJobForTeacherVlt = null;
				List<JobForTeacher> lstJobForTeacherComp = null;

				lstJobForTeacherVlt  = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail, statusMasterVlt);
				//System.out.println("lstJobForTeacherVlt.size() : "+lstJobForTeacherVlt.size());
				lstJobForTeacherComp  = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail, statusMasterComp);

				if(lstJobForTeacherVlt.size()>0)
				{
					for (JobForTeacher jobForTeacher : lstJobForTeacherVlt) {
						jobOrder = jobForTeacher.getJobId();
						//if(jobOrder.getIsJobAssessment()==false && jobOrder.getJobCategoryMaster().getBaseStatus()==true)
						if(jobOrder.getJobCategoryMaster().getBaseStatus()==true)//**##**
						{
							jobForTeacher.setStatus(statusMasterIncomp);
							jobForTeacher.setStatusMaster(statusMasterIncomp);
							if(statusMasterIncomp!=null){
								jobForTeacher.setApplicationStatus(statusMasterIncomp.getStatusId());
							}
							jobForTeacherDAO.makePersistent(jobForTeacher);
						}
					}
				}


				if(lstJobForTeacherComp.size()>0)
				{
					for (JobForTeacher jobForTeacher : lstJobForTeacherComp) {
						jobOrder = jobForTeacher.getJobId();						
						if(jobOrder.getJobCategoryMaster().getBaseStatus()==true)
						{
							jobForTeacher.setStatus(statusMasterIncomp);
							jobForTeacher.setStatusMaster(statusMasterIncomp);
							if(statusMasterIncomp!=null){
								jobForTeacher.setApplicationStatus(statusMasterIncomp.getStatusId());
							}
							jobForTeacherDAO.makePersistent(jobForTeacher);
						}
					}
				}


				//System.out.println("teacherAssessmentStatus : "+teacherAssessmentStatus);
				TeacherEPIChangedStatusLog teacherEPIChangedStatusLog  = new TeacherEPIChangedStatusLog();
				teacherEPIChangedStatusLog.setUserMaster(userMaster);
				teacherEPIChangedStatusLog.setTeacherDetail(teacherDetail);
				teacherEPIChangedStatusLog.setAssessmentDetail(assessmentDetail);
				teacherEPIChangedStatusLog.setAssessmentName(assessmentDetail.getAssessmentName());
				teacherEPIChangedStatusLog.setAssessmentType(assessmentDetail.getAssessmentType());
				teacherEPIChangedStatusLog.setTeacherAssessmentStatus(teacherAssessmentStatus);
				teacherEPIChangedStatusLog.setResetMessage(resetMessage);
				teacherEPIChangedStatusLog.setCreatedDateTime(new Date());
				teacherEPIChangedStatusLog.setIpAddress(IPAddressUtility.getIpAddress(request));

				teacherEPIChagedStatusLogDAO.makePersistent(teacherEPIChangedStatusLog);

				teacherAssessmentdetail.setIsDone(false);
				teacherAssessmentDetailDAO.makePersistent(teacherAssessmentdetail);

				String mailText = MailText.getTeacherEPIResetMailText(request,teacherDetail);
				String subject = Utility.getLocaleValuePropByKey("msgEPIReset", locale);
				DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
				dashboardEmailsToTeacher.setCreatedDateTime(new Date());
				dashboardEmailsToTeacher.setEmailAddress(teacherDetail.getEmailAddress());
				dashboardEmailsToTeacher.setSubject(subject);
				dashboardEmailsToTeacher.setMailText(mailText);
				dashboardEmailsToTeacher.setEmailType("Manual");
				dashboardEmailsToTeacher.setTeacherDetail(teacherDetail);
				dashboardEmailsToTeacherDAO.makePersistent(dashboardEmailsToTeacher);

				emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),subject,mailText);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "1";
	}

	public String getCandidates(String fromDate,String toDate,int statusId,String firstName,String lastName, String emailAddress,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		int r = 0;
		int s = r;
		r = (r==2?1:r);

		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start = ((pgNo-1)*noOfRowInPage);
		int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;

		SortedMap map = new TreeMap();
		List<TeacherDetail> lstTeacherDetail	  	=	null;
		/** set default sorting fieldName **/
		//String sortOrderFieldName	=	"emailAddress";
		//String sortOrderNoField		=	"emailAddress";
		String sortOrderFieldName	=	"firstName";
		String sortOrderNoField		=	"firstName";
		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal		=	null;
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null)){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
			}
			if(sortOrderNoField.equals("firstName"))
			{
				sortOrderFieldName	=	"firstName";
			}
		}

		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null))
		{
			if(sortOrderType.equals("0"))
			{
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else
			{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}
		else
		{
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
		/**End ------------------------------------**/

		List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
		List<TeacherAssessmentdetail> teacherAssessmentdetails = new ArrayList<TeacherAssessmentdetail>();

		if(statusId>0)
			r = statusId==1?0:1;
		else
			r = 2;

		System.out.println("rrrrrrrrrrrr1 "+r);
		if(r==0)
		{
			teacherDetails	=	teacherDetailDAO.getCadidatesByEPI(fromDate, toDate, statusId,firstName, lastName, emailAddress, sortOrderStrVal,r);
			System.out.println("teacherDetails "+teacherDetails.size());

		}else
		{
			teacherDetails	=	teacherDetailDAO.getCadidatesByEPI(fromDate, toDate, statusId,firstName, lastName, emailAddress, sortOrderStrVal,r);

			//System.out.println("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv statusId: "+statusId);
			List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
			if(teacherDetails.size()>0)
			{
				System.out.println("vv teacherDetails: "+teacherDetails.size());

				if(statusId==3)
				{
					StatusMaster statusMaster = WorkThreadServlet.statusMap.get("icomp");
					teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTakenByTeachersByStauts(teacherDetails,statusMaster);

					teacherDetails.clear();
					for(TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatusList) {
						teacherDetails.add(teacherAssessmentStatus2.getTeacherDetail());
						teacherAssessmentdetails.add(teacherAssessmentStatus2.getTeacherAssessmentdetail());
					}
				}else if(statusId==4)
				{
					StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
					teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTakenByTeachersByStauts(teacherDetails,statusMaster);

					teacherDetails.clear();
					for(TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatusList) {
						teacherDetails.add(teacherAssessmentStatus2.getTeacherDetail());
						teacherAssessmentdetails.add(teacherAssessmentStatus2.getTeacherAssessmentdetail());
					}
				}else{
					// EPI not started
					teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetails);
					List<TeacherDetail> teacherDetailsEPI = new ArrayList<TeacherDetail>();
					for(TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatusList) {
						teacherDetailsEPI.add(teacherAssessmentStatus2.getTeacherDetail());
					}

					teacherDetails.removeAll(teacherDetailsEPI);
				}
			}
		}
		System.out.println("ss teacherDetails: "+teacherDetails.size());

		List<TeacherDetail> sortedlstTeacherDetail		=	new ArrayList<TeacherDetail>();

		SortedMap<String,TeacherDetail>	sortedMap = new TreeMap<String,TeacherDetail>();
		if(sortOrderNoField.equals("firstName"))
		{
			sortOrderFieldName	=	"firstName";
		}

		int mapFlag=2;
		for (TeacherDetail teacherDetail : teacherDetails){
			String orderFieldName=teacherDetail.getEmailAddress();
		}
		if(mapFlag==1){
			NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
			for (Iterator iter=navig.iterator();iter.hasNext();) {  
				Object key = iter.next(); 
				sortedlstTeacherDetail.add((TeacherDetail) sortedMap.get(key));
			} 
		}else if(mapFlag==0){
			Iterator iterator = sortedMap.keySet().iterator();
			while (iterator.hasNext()) {
				Object key = iterator.next();
				sortedlstTeacherDetail.add((TeacherDetail) sortedMap.get(key));
			}
		}else{
			sortedlstTeacherDetail=teacherDetails;
		}

		totalRecord = sortedlstTeacherDetail.size();

		if(totalRecord<end)
			end=totalRecord;
		List<TeacherDetail> lstsortedTeacherDetail = sortedlstTeacherDetail.subList(start,end);

		Map<Integer,Integer> assessmentAttempsMap = new HashMap<Integer, Integer>();
		Map<Integer,Integer> assessmentStrikeMap = new HashMap<Integer, Integer>();
		Map<Integer,Integer> dashboardEmailsToTeacherMap = new HashMap<Integer, Integer>();


		//DashboardEmailsToTeacher
		if(statusId>2)
		{
			//assessmentAttempsMap = teacherAssessmentAttemptDAO.findAssessmentsResponsesForBase(teacherDetailsIncompleted);
			assessmentAttempsMap = teacherAssessmentAttemptDAO.findAssessmentsResponsesForBase(lstsortedTeacherDetail);
			//System.out.println("ass: "+assessmentAttempsMap.size());
			if(teacherAssessmentdetails.size()>0)
			{
				//assessmentStrikeMap = teacherStrikeLogDAO.findAssessmentsStrikesForBase(teacherDetailsIncompleted,teacherAssessmentdetails);
				assessmentStrikeMap = teacherStrikeLogDAO.findAssessmentsStrikesForBase(lstsortedTeacherDetail,teacherAssessmentdetails);
				//System.out.println("ass: "+assessmentStrikeMap.size());
			}
		}
		System.out.println("///////////////////////////////////////////////////////////////");
		//System.out.println(" ========= lstsortedTeacherDetail ========== "+lstsortedTeacherDetail.size());

		if(lstsortedTeacherDetail.size()>0)
			dashboardEmailsToTeacherMap = dashboardEmailsToTeacherDAO.findMailSentToTeachers(lstsortedTeacherDetail);

		StringBuffer sb = new StringBuffer();
		String responseText = "";
		sb.append("<table  id='tblGrid' width='100%' border='0'>");
		sb.append("<thead class='bg'>");
		sb.append("<tr>");
		sb.append("<th valign='top'>&nbsp;</th>");
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblNm", locale),sortOrderFieldName,"firstName",sortOrderTypeVal,pgNo);
		sb.append("<th valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblEmail1", locale),sortOrderFieldName,"emailAddress",sortOrderTypeVal,pgNo);
		sb.append("<th valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblRegisterdOn", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
		sb.append("<th valign='top'>"+responseText+"</th>");
		responseText=Utility.getLocaleValuePropByKey("lblEPIofAttempt", locale);
		sb.append("<th valign='top'>"+responseText+"</th>");
		responseText="# "+Utility.getLocaleValuePropByKey("lblofMessagesSent", locale);
		sb.append("<th valign='top'>"+responseText+"</th>");
		sb.append("</tr>");
		sb.append("</thead>");
		if(lstsortedTeacherDetail.size()==0)
			sb.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
		int epiAttempts = 0;
		int cnt=0;
		int mailSent = 0;
		Integer atmp=0;
		Integer strk=0; 
		for (TeacherDetail teacherDetail : lstsortedTeacherDetail) {
			//te+=teacherDetail.getTeacherId()+",";
			cnt++;
			sb.append("<tr>");
			sb.append("<td><input type='checkbox' class='case' name='case' value='"+teacherDetail.getTeacherId()+"'> </td>");
			sb.append("<td>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"</td>");
			sb.append("<td>"+teacherDetail.getEmailAddress()+"</td>");
			sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(teacherDetail.getCreatedDateTime())+"</td>");

			if(r==1)
			{
				atmp=assessmentAttempsMap.get(teacherDetail.getTeacherId());
				epiAttempts=atmp==null?0:atmp;
				strk=assessmentStrikeMap.get(teacherDetail.getTeacherId());
				epiAttempts+=strk==null?0:strk;

				if(s==2)
					epiAttempts=epiAttempts<3?epiAttempts:3;
				else
					epiAttempts=epiAttempts<2?epiAttempts:2;
			}
			sb.append("<td> "+epiAttempts+"</td>");
			mailSent = dashboardEmailsToTeacherMap.get(teacherDetail.getTeacherId())==null?0:dashboardEmailsToTeacherMap.get(teacherDetail.getTeacherId());
			//System.out.println("          = Mail Sent  "+mailSent);
			if(mailSent==0)
				sb.append("<td> "+mailSent+"</td>");
			else
				sb.append("<td> <a href='candidatesmaildetail.do?o="+teacherDetail.getTeacherId()+"'>"+mailSent+"</a></td>");
			sb.append("</tr>");
		}
		sb.append("</table >");
		sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));

		return sb.toString();

	}

	@Transactional(readOnly=false)
	public String runScheduler()
	{

		int conversionMarketingMaildays = Integer.parseInt(Utility.getValueOfPropByKey("conversionMarketingMaildays"));

		try {
			String[] teacherStatus  = {"NotAuthenticated","EPINotStarted","EPINotCompleted","EPICompleted"};
			List<MessageFromDashboard>  messageFromDashboards = messageFromDashboardDAO.getMailFormates(teacherStatus);

			String NotAuthenticatedSubject = null;
			String NotAuthenticatedMailText = null;
			String EPINotStartedSubject = null;
			String EPINotStartedMailText = null;
			String EPINotCompletedSubject = null;
			String EPINotCompletedMailText = null;
			String EPICompletedSubject = null;
			String EPICompletedMailText = null;
			String tStatus = null;
			for (MessageFromDashboard messageFromDashboard : messageFromDashboards) {

				tStatus = messageFromDashboard.getTeacherStatus();

				if(tStatus.equalsIgnoreCase("NotAuthenticated"))
				{
					NotAuthenticatedSubject = messageFromDashboard.getSubject();
					NotAuthenticatedMailText = messageFromDashboard.getMessage();
				}else if(tStatus.equalsIgnoreCase("EPINotStarted"))
				{
					EPINotStartedSubject = messageFromDashboard.getSubject();
					EPINotStartedMailText = messageFromDashboard.getMessage();
				}else if(tStatus.equalsIgnoreCase("EPINotCompleted"))
				{
					EPINotCompletedSubject = messageFromDashboard.getSubject();
					EPINotCompletedMailText = messageFromDashboard.getMessage();
				}else if(tStatus.equalsIgnoreCase("EPICompleted"))
				{	
					EPICompletedSubject = messageFromDashboard.getSubject();
					EPICompletedMailText = messageFromDashboard.getMessage();
				}

			}
			String mailTo = "vishwanath@netsutra.com";
			Date startDate = null;
			Date endDate = new Date();

			List<TeacherDetail> teacherRemoved = new ArrayList<TeacherDetail>();
			List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
			teacherDetails = teacherDetailDAO.findAllTeacherStatusWisePortfolioNeeded("A",0,true);
			System.out.println("teacherDetails.size() : "+teacherDetails.size());

			Map<TeacherDetail, DashboardEmailsToTeacher> teacherEmailMap = new HashMap<TeacherDetail, DashboardEmailsToTeacher>();

			teacherEmailMap = dashboardEmailsToTeacherDAO.findMailSentToTeachersNotAuthenticated(teacherDetails);
			System.out.println("kkkkkkkkkkkkkkkkkk "+teacherEmailMap.size());
			////////////////////////////////////////////////////////////////////////////////////

			for (Map.Entry entry: teacherEmailMap.entrySet()) {

				DashboardEmailsToTeacher det = (DashboardEmailsToTeacher)entry.getValue();
				//teacherDetail = det.getTeacherDetail();
				startDate = det.getCreatedDateTime();
				int difInDays = (int) ((endDate.getTime() - startDate.getTime())/(1000*60*60*24));
				System.out.println("llllllllllll "+difInDays);
				if(difInDays>=conversionMarketingMaildays)
					teacherRemoved.add(det.getTeacherDetail());
			}
			//////////// now remove //////////////////////////////

			if(teacherRemoved.size()>0)
			{
				List<DashboardEmailsToTeacher> dashboardEmailsToTeachers = dashboardEmailsToTeacherDAO.getMailsByTeachers(teacherRemoved);

				SessionFactory sessionFactory =  teacherDetailDAO.getSessionFactory();
				StatelessSession session = sessionFactory.openStatelessSession();

				for(DashboardEmailsToTeacher dashboardEmailsToTeacher: dashboardEmailsToTeachers){
					RemovedTeacherMailDetails removedTeacherMailDetail = new RemovedTeacherMailDetails();
					removedTeacherMailDetail.setTeacherFirstName(dashboardEmailsToTeacher.getTeacherDetail().getFirstName());
					removedTeacherMailDetail.setTeacherLastName(dashboardEmailsToTeacher.getTeacherDetail().getLastName());
					removedTeacherMailDetail.setCreatedDateTime(dashboardEmailsToTeacher.getCreatedDateTime());
					removedTeacherMailDetail.setEmailAddress(dashboardEmailsToTeacher.getEmailAddress());
					removedTeacherMailDetail.setSubject(dashboardEmailsToTeacher.getSubject());
					removedTeacherMailDetail.setMailText(dashboardEmailsToTeacher.getMailText());
					removedTeacherMailDetail.setEmailType(dashboardEmailsToTeacher.getEmailType());
					removedTeacherMailDetail.setTeacherStatus(dashboardEmailsToTeacher.getTeacherStatus());
					removedTeacherMailDetail.setRemovalDateTime(new Date());
					session.insert(removedTeacherMailDetail);
				}

				//Deletion from dashboardEmailsToTeacher
				dashboardEmailsToTeacherDAO.deleteFromDashboradEmailsToTeacher(teacherRemoved);
				System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				//Deletion from TeacherDetails
				//teacherDetailDAO.deleteFromTeacherDetails(teacherRemoved);
				for (TeacherDetail teacherDetail : teacherRemoved) {
					teacherDetailDAO.makeTransient(teacherDetail);
				}
			}
			/////////////////////////////////////////////////////////////////////////////////
			System.out.println("teacherRemoved: "+teacherRemoved.size());

			System.out.println("Then : "+teacherDetails.size());
			if(teacherEmailMap.size()>0)
			{
				System.out.println("removed: "+teacherDetails.removeAll(teacherRemoved));
			}
			System.out.println("Now : "+teacherDetails.size());


			teacherEmailMap = new HashMap<TeacherDetail, DashboardEmailsToTeacher>(); 
			if(teacherDetails.size()>0)
				teacherEmailMap = dashboardEmailsToTeacherDAO.getMailsByTypeAndStatusWithoutDateRange(teacherDetails, "Automatic","NotAuthenticated");

			System.out.println("teacherEmailMap  : "+teacherEmailMap.size());	 
			SessionFactory sessionFactory1 =  teacherDetailDAO.getSessionFactory();
			StatelessSession session1 = sessionFactory1.openStatelessSession();
			for (TeacherDetail teacherDetail : teacherDetails) {
				if(teacherEmailMap.get(teacherDetail)==null)
				{
					//String mailText = MailText.getTeacherFirstMailText(teacherDetail);
					//String subject = "Please authenticate your account";
					DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
					dashboardEmailsToTeacher.setCreatedDateTime(new Date());
					dashboardEmailsToTeacher.setEmailAddress(teacherDetail.getEmailAddress());
					dashboardEmailsToTeacher.setSubject(NotAuthenticatedSubject);
					dashboardEmailsToTeacher.setMailText(NotAuthenticatedMailText);
					dashboardEmailsToTeacher.setEmailType("Automatic");
					dashboardEmailsToTeacher.setTeacherStatus("NotAuthenticated");
					dashboardEmailsToTeacher.setTeacherDetail(teacherDetail);
					session1.insert(dashboardEmailsToTeacher);
					emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),NotAuthenticatedSubject,NotAuthenticatedMailText);
				}
			}


			TeacherDetail teacherDetail = null;

			for (Map.Entry entry: teacherEmailMap.entrySet()) {

				DashboardEmailsToTeacher det = (DashboardEmailsToTeacher)entry.getValue();
				teacherDetail = det.getTeacherDetail();
				startDate = det.getCreatedDateTime();
				int difInDays = (int) ((endDate.getTime() - startDate.getTime())/(1000*60*60*24));
				// System.out.println("difInDays: "+difInDays);

				if(difInDays>=conversionMarketingMaildays)
				{
					System.out.println("difInDays : "+difInDays);
					//String mailText = MailText.getTeacherFirstMailText(teacherDetail);
					//String subject = "Please authenticate your account";
					//dashboardEmailsToTeacherDAO.saveDashboardMail(teacherDetail, NotAuthenticatedMailText, NotAuthenticatedMailText, "Automatic", "NotAuthenticated");
					DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
					dashboardEmailsToTeacher.setCreatedDateTime(new Date());
					dashboardEmailsToTeacher.setEmailAddress(teacherDetail.getEmailAddress());
					dashboardEmailsToTeacher.setSubject(NotAuthenticatedSubject);
					dashboardEmailsToTeacher.setMailText(NotAuthenticatedMailText);
					dashboardEmailsToTeacher.setEmailType("Automatic");
					dashboardEmailsToTeacher.setTeacherStatus("NotAuthenticated");
					dashboardEmailsToTeacher.setTeacherDetail(teacherDetail);
					session1.insert(dashboardEmailsToTeacher);
					emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),NotAuthenticatedSubject,NotAuthenticatedMailText);
				}
			}

			/////////////////////////// Process two (authenticated teachers) /////////////////////////////////////////////////
			teacherDetails = teacherDetailDAO.findAllTeacherStatusWisePortfolioNeeded("A",1,true);
			System.out.println("teacherDetails.size() : "+teacherDetails.size());

			// base status
			List<TeacherAssessmentStatus> teacherAssessmentStatus = new ArrayList<TeacherAssessmentStatus>();
			if(teacherDetails.size()>0)
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetails);

			System.out.println("teacherAssessmentStatus: "+teacherAssessmentStatus.size());

			List<TeacherDetail> teacherDetailscompleted = new ArrayList<TeacherDetail>();
			List<TeacherDetail> teacherDetailsViolated = new ArrayList<TeacherDetail>();
			List<TeacherDetail> teacherDetailsIncompleted = new ArrayList<TeacherDetail>();
			String statusShortName = null;
			for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {
				statusShortName = teacherAssessmentStatus2.getStatusMaster().getStatusShortName();
				teacherDetail = teacherAssessmentStatus2.getTeacherDetail();
				if(statusShortName.equalsIgnoreCase("vlt"))
				{
					teacherDetailsViolated.add(teacherDetail);
				}else if(statusShortName.equalsIgnoreCase("comp")){
					teacherDetailscompleted.add(teacherDetail);
				}else
					teacherDetailsIncompleted.add(teacherDetail);
			}
			System.out.println("teacherDetailscompleted.size(): "+teacherDetailsViolated.size());
			System.out.println("teacherDetailscompleted.size(): "+teacherDetailscompleted.size());
			teacherDetails.removeAll(teacherDetailsViolated);
			teacherDetails.removeAll(teacherDetailscompleted);
			teacherDetails.removeAll(teacherDetailsIncompleted);
			System.out.println("teacherDetailsINcompleted.size(): "+teacherDetails.size());

			//////////////////////////////// authenticated but epi not started //////////////////
			if(teacherDetails.size()>0)
			{
				teacherEmailMap = new HashMap<TeacherDetail, DashboardEmailsToTeacher>();
				if(teacherDetails.size()>0)
					teacherEmailMap = dashboardEmailsToTeacherDAO.getMailsByTypeAndStatusWithoutDateRange(teacherDetails, "Automatic","EPINotStarted");

				SessionFactory sessionFactory =  teacherDetailDAO.getSessionFactory();
				StatelessSession session = sessionFactory.openStatelessSession();
				//Transaction tx = session.beginTransaction();
				System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
				for (TeacherDetail teacherDetail1 : teacherDetails) {
					if(teacherEmailMap.get(teacherDetail1)==null)
					{
						//String mailText = MailText.getTeacherFirstMailTextEPIRequest(teacherDetail1);
						//String subject = "Please start your EPI";
						//System.out.println("dddddd "+teacherDetail1.getTeacherId());
						DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
						dashboardEmailsToTeacher.setCreatedDateTime(new Date());
						dashboardEmailsToTeacher.setEmailAddress(teacherDetail1.getEmailAddress());
						dashboardEmailsToTeacher.setSubject(EPINotStartedSubject);
						dashboardEmailsToTeacher.setMailText(EPINotStartedMailText);
						dashboardEmailsToTeacher.setEmailType("Automatic");
						dashboardEmailsToTeacher.setTeacherStatus("EPINotStarted");
						dashboardEmailsToTeacher.setTeacherDetail(teacherDetail1);
						session.insert(dashboardEmailsToTeacher);

						emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),EPINotStartedSubject,EPINotStartedMailText);

					}
				}
				System.out.println("DONE::::::::::: ");
				//tx.commit();

				for (Map.Entry entry: teacherEmailMap.entrySet()) {

					DashboardEmailsToTeacher det = (DashboardEmailsToTeacher)entry.getValue();
					teacherDetail = det.getTeacherDetail();
					startDate = det.getCreatedDateTime();
					int difInDays = (int) ((endDate.getTime() - startDate.getTime())/(1000*60*60*24));
					if(difInDays>=15)
					{
						//tring mailText = MailText.getTeacherFirstMailTextEPIRequest(teacherDetail);
						//String subject = "Please start your EPI";

						DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
						dashboardEmailsToTeacher.setCreatedDateTime(new Date());
						dashboardEmailsToTeacher.setEmailAddress(teacherDetail.getEmailAddress());
						dashboardEmailsToTeacher.setSubject(EPINotStartedSubject);
						dashboardEmailsToTeacher.setMailText(EPINotStartedMailText);
						dashboardEmailsToTeacher.setEmailType("Automatic");
						dashboardEmailsToTeacher.setTeacherStatus("EPINotStarted");
						dashboardEmailsToTeacher.setTeacherDetail(teacherDetail);
						session.insert(dashboardEmailsToTeacher);

						emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),EPINotStartedSubject,EPINotStartedMailText);
					}
				}
			}


			////////////////////////////////////////////// authenticated but epi not completed 
			if(teacherDetailsIncompleted.size()>0)
			{
				teacherEmailMap = new HashMap<TeacherDetail, DashboardEmailsToTeacher>();
				teacherEmailMap = dashboardEmailsToTeacherDAO.getMailsByTypeAndStatus(teacherDetailsIncompleted, "","EPINotCompleted");

				SessionFactory sessionFactory =  teacherDetailDAO.getSessionFactory();
				StatelessSession session = sessionFactory.openStatelessSession();
				//Transaction tx = session.beginTransaction();
				for (TeacherDetail teacherDetail1 : teacherDetailsIncompleted) {
					if(teacherEmailMap.get(teacherDetail1)==null)
					{
						//String mailText = MailText.getTeacherFirstMailTextEPIRequest(teacherDetail1);
						//String subject = "Please complete your EPI";
						DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
						dashboardEmailsToTeacher.setCreatedDateTime(new Date());
						dashboardEmailsToTeacher.setEmailAddress(teacherDetail1.getEmailAddress());
						dashboardEmailsToTeacher.setSubject(EPINotCompletedSubject);
						dashboardEmailsToTeacher.setMailText(EPINotCompletedMailText);
						dashboardEmailsToTeacher.setEmailType("Automatic");
						dashboardEmailsToTeacher.setTeacherStatus("EPINotCompleted");
						dashboardEmailsToTeacher.setTeacherDetail(teacherDetail1);
						session.insert(dashboardEmailsToTeacher);

						emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),EPINotCompletedSubject,EPINotCompletedMailText);
					}
				}

				System.out.println("DONE::::::::::: ");
				//tx.commit();

				for (Map.Entry entry: teacherEmailMap.entrySet()) {

					DashboardEmailsToTeacher det = (DashboardEmailsToTeacher)entry.getValue();
					teacherDetail = det.getTeacherDetail();
					startDate = det.getCreatedDateTime();
					int difInDays = (int) ((endDate.getTime() - startDate.getTime())/(1000*60*60*24));
					if(difInDays>=conversionMarketingMaildays)
					{
						//String mailText = MailText.getTeacherFirstMailTextEPIRequest(teacherDetail);
						//String subject = "Please complete your EPI";
						DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
						dashboardEmailsToTeacher.setCreatedDateTime(new Date());
						dashboardEmailsToTeacher.setEmailAddress(teacherDetail.getEmailAddress());
						dashboardEmailsToTeacher.setSubject(EPINotCompletedSubject);
						dashboardEmailsToTeacher.setMailText(EPINotCompletedMailText);
						dashboardEmailsToTeacher.setEmailType("Automatic");
						dashboardEmailsToTeacher.setTeacherStatus("EPINotCompleted");
						dashboardEmailsToTeacher.setTeacherDetail(teacherDetail);
						session.insert(dashboardEmailsToTeacher);
						emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),EPINotCompletedSubject,EPINotCompletedMailText);
					}
				}
			}

			//////////////////////////////////////////////////

			// process four Completed EPI to Applied for Jobs 
			if(teacherDetailscompleted.size()>0)
			{
				teacherEmailMap = new HashMap<TeacherDetail, DashboardEmailsToTeacher>();
				if(teacherDetailscompleted.size()>0)
					teacherEmailMap = dashboardEmailsToTeacherDAO.getMailsByTypeAndStatus(teacherDetailscompleted, "Automatic","EPICompleted");

				SessionFactory sessionFactory =  teacherDetailDAO.getSessionFactory();
				StatelessSession session = sessionFactory.openStatelessSession();
				//Transaction tx = session.beginTransaction();
				System.out.println("Pppppppppppppppppppppppppp");
				for (TeacherDetail teacherDetail1 : teacherDetailscompleted) {
					if(teacherEmailMap.get(teacherDetail1)==null)
					{
						//String mailText = MailText.getTeacherFirstMailTextEPICompleted(teacherDetail1);
						//String subject = "Apply Job on TeacherMatch";
						DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
						dashboardEmailsToTeacher.setCreatedDateTime(new Date());
						dashboardEmailsToTeacher.setEmailAddress(teacherDetail1.getEmailAddress());
						dashboardEmailsToTeacher.setSubject(EPICompletedSubject);
						dashboardEmailsToTeacher.setMailText(EPICompletedMailText);
						dashboardEmailsToTeacher.setEmailType("Automatic");
						dashboardEmailsToTeacher.setTeacherStatus("EPICompleted");
						dashboardEmailsToTeacher.setTeacherDetail(teacherDetail1);
						session.insert(dashboardEmailsToTeacher);

						emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),EPICompletedSubject,EPICompletedMailText);

					}
				}
				System.out.println("DONE11::::::::::: ");
				//tx.commit();

				session = sessionFactory.openStatelessSession();

				for(Map.Entry entry: teacherEmailMap.entrySet()) {

					DashboardEmailsToTeacher det = (DashboardEmailsToTeacher)entry.getValue();
					teacherDetail = det.getTeacherDetail();
					startDate = det.getCreatedDateTime();
					int difInDays = (int) ((endDate.getTime() - startDate.getTime())/(1000*60*60*24));
					if(difInDays>=conversionMarketingMaildays)
					{
						//String mailText = MailText.getTeacherFirstMailTextEPICompleted(teacherDetail);
						//String subject = "Apply Job on TeacherMatch";
						//System.out.println("dddddd "+teacherDetail1.getTeacherId());
						DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
						dashboardEmailsToTeacher.setCreatedDateTime(new Date());
						dashboardEmailsToTeacher.setEmailAddress(teacherDetail.getEmailAddress());
						dashboardEmailsToTeacher.setSubject(EPICompletedSubject);
						dashboardEmailsToTeacher.setMailText(EPICompletedMailText);
						dashboardEmailsToTeacher.setEmailType("Automatic");
						dashboardEmailsToTeacher.setTeacherStatus("EPICompleted");
						dashboardEmailsToTeacher.setTeacherDetail(teacherDetail);
						session.insert(dashboardEmailsToTeacher);

						emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),EPICompletedSubject,EPICompletedMailText);
					}
				}

				System.out.println("DONE44::::::::::: ");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public String getCandidatesMails(TeacherDetail teacherDetail,String subject,String teacherStatus,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{

		//System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start = ((pgNo-1)*noOfRowInPage);
		int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;

		SortedMap map = new TreeMap();
		List<DashboardEmailsToTeacher> dashboardEmailsToTeachers	 =	null;
		/** set default sorting fieldName **/

		String sortOrderFieldName	=	"createdDateTime";
		String sortOrderNoField		=	"createdDateTime";
		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal		=	null;
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null) ){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
			}
			if(sortOrderNoField.equals("createdDateTime"))
			{
				sortOrderFieldName	=	"createdDateTime";
			}
		}

		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null))
		{
			if(sortOrderType.equals("0"))
			{
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else
			{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}
		else
		{
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}

		/**End ------------------------------------**/

		Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);

		dashboardEmailsToTeachers = dashboardEmailsToTeacherDAO.findByCriteria(sortOrderStrVal,criterion1);

		List<DashboardEmailsToTeacher> sortedlstdashboardEmailsToTeachers		=	new ArrayList<DashboardEmailsToTeacher>();

		SortedMap<String,DashboardEmailsToTeacher>	sortedMap = new TreeMap<String,DashboardEmailsToTeacher>();
		if(sortOrderNoField.equals("createdDateTime"))
		{
			sortOrderFieldName	=	"createdDateTime";
		}

		int mapFlag=2;


		if(mapFlag==1){
			NavigableSet<String> navig = ((TreeMap)sortedMap).descendingKeySet();  
			for (Iterator iter=navig.iterator();iter.hasNext();) {  
				Object key = iter.next(); 
				sortedlstdashboardEmailsToTeachers.add((DashboardEmailsToTeacher) sortedMap.get(key));
			} 
		}else if(mapFlag==0){
			Iterator iterator = sortedMap.keySet().iterator();
			while (iterator.hasNext()) {
				Object key = iterator.next();
				sortedlstdashboardEmailsToTeachers.add((DashboardEmailsToTeacher) sortedMap.get(key));
			}
		}else{
			sortedlstdashboardEmailsToTeachers=dashboardEmailsToTeachers;
		}

		totalRecord = sortedlstdashboardEmailsToTeachers.size();

		if(totalRecord<end)
			end=totalRecord;
		List<DashboardEmailsToTeacher> lstsorteddashboardEmailsToTeacher  =	sortedlstdashboardEmailsToTeachers.subList(start,end);


		StringBuffer sb = new StringBuffer();
		String responseText = "";
		sb.append("<table  id='tblGrid' width='100%' border='0'>");
		sb.append("<thead class='bg'>");
		sb.append("<tr>");
		responseText=PaginationAndSorting.responseSortingLink("Status",sortOrderFieldName,"teacherStatus",sortOrderTypeVal,pgNo);
		sb.append("<th valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLink("Email Type",sortOrderFieldName,"emailType",sortOrderTypeVal,pgNo);
		sb.append("<th valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLink("Email to",sortOrderFieldName,"emailAddress",sortOrderTypeVal,pgNo);
		sb.append("<th valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLink("Subject",sortOrderFieldName,"subject",sortOrderTypeVal,pgNo);
		sb.append("<th valign='top'>"+responseText+"</th>");
		responseText="Mail Text";
		sb.append("<th valign='top'>"+responseText+"</th>");
		responseText=PaginationAndSorting.responseSortingLink("Sent Date",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
		sb.append("<th valign='top'>"+responseText+"</th>");

		sb.append("</tr>");
		sb.append("</thead>");


		if(lstsorteddashboardEmailsToTeacher.size()==0)
			sb.append("<tr><td colspan='6'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );

		HTML2Text h2t = new HTML2Text();
		for (DashboardEmailsToTeacher dashboardEmailsToTeacher : lstsorteddashboardEmailsToTeacher) {
			sb.append("<tr>");
			sb.append("<td>"+(dashboardEmailsToTeacher.getTeacherStatus()==null?"":dashboardEmailsToTeacher.getTeacherStatus())+"</td>");
			sb.append("<td>"+(dashboardEmailsToTeacher.getEmailType()==null?"":dashboardEmailsToTeacher.getEmailType())+"</td>");
			sb.append("<td>"+dashboardEmailsToTeacher.getEmailAddress()+"</td>");
			sb.append("<td>"+(dashboardEmailsToTeacher.getSubject()==null?"":dashboardEmailsToTeacher.getSubject())+"</td>");
			if(dashboardEmailsToTeacher.getMailText()==null)
				sb.append("<td></td>");
			else{
				String text = "";
				try {text = h2t.convert(dashboardEmailsToTeacher.getMailText());}catch (Exception e) {}
				sb.append("<td>"+Utility.LimitCharacter(text, 100, "...") +" <a href='javascript:void(0)' onclick='getMailMsg("+dashboardEmailsToTeacher.getMailId()+")'>View More</a></td>");
			}
			sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(dashboardEmailsToTeacher.getCreatedDateTime())+" </td>");
			sb.append("</tr>");
		}
		sb.append("</table >");
		sb.append(PaginationAndSorting.getPaginationString(request,totalRecord,noOfRow, pageNo));
		//System.out.println("te: "+te);

		return sb.toString();
	}

	public DashboardEmailsToTeacher getMailMsg(Integer mailId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		try {
			Criterion criterion = Restrictions.eq("mailId",mailId);
			List<DashboardEmailsToTeacher> dashboardEmailsToTeacherList = dashboardEmailsToTeacherDAO.findByCriteria(criterion);
			if(dashboardEmailsToTeacherList.size()>0)
				return dashboardEmailsToTeacherList.get(0);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new DashboardEmailsToTeacher();
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param teacherDetail
	 * @param spResetMsg
	 * @return {@link String}
	 */
	@Transactional(readOnly=false)
	public String makeSpIncomplete(TeacherDetail teacherDetail, String spResetMsg)
	{
		System.out.println(":::::::::::::: AdminDashboardAjax : makeSpIncomplete ::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userMaster = (UserMaster)session.getAttribute("userMaster"); 
		try {
			TeacherAssessmentStatus teacherAssessmentStatus = null;
			TeacherAssessmentdetail teacherAssessmentdetail = null;
			AssessmentDetail assessmentDetail = null;

			JobOrder jobOrder = new JobOrder();
			jobOrder.setJobId(-3);
			List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail, jobOrder, 3, null);
			if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0)
			{
				teacherAssessmentStatus = teacherAssessmentStatusList.get(teacherAssessmentStatusList.size()-1);
				teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();
				assessmentDetail = teacherAssessmentStatus.getAssessmentDetail();
				teacherDetail = teacherAssessmentStatus.getTeacherDetail();
				System.out.println("teacherAssessmentStatus.getTeacherAssessmentStatusId() : "+teacherAssessmentStatus.getTeacherAssessmentStatusId());
				
				//Delete strike 
				Session session2 =  teacherStrikeLogDAO.getSession();
				String hql = "delete from teacherstrikelog where teacherAssessmentId = :teacherAssessmentId";
				Query query = session2.createQuery(hql);
				query.setInteger("teacherAssessmentId",teacherAssessmentdetail.getTeacherAssessmentId());
				int rowCount = query.executeUpdate();
				System.out.println("teacherstrikelog rows affected: " + rowCount);

				List<TeacherAssessmentAttempt> teacherAssessmentAttemptList = teacherAssessmentAttemptDAO.findNoOfAttempts(assessmentDetail, teacherDetail, teacherAssessmentdetail.getAssessmentTakenCount());
				System.out.println("teacherAssessmentAttemptList.size() : "+teacherAssessmentAttemptList.size());
				TeacherAssessmentAttempt teacherAssessmentAttempt = teacherAssessmentAttemptList.get(0);

				if(teacherAssessmentAttemptList.size()>1)
				{
					teacherAssessmentAttempt = teacherAssessmentAttemptList.get(0);
					teacherAssessmentAttemptList.remove(teacherAssessmentAttempt);
					for (TeacherAssessmentAttempt teacherAssessmentAttempt1 : teacherAssessmentAttemptList) {
						System.out.println("makeTransient teacherAssessmentAttempt1.getAttemptId() : "+teacherAssessmentAttempt1.getAttemptId());
						teacherAssessmentAttemptDAO.makeTransient(teacherAssessmentAttempt1);
					}
				}

				Integer assessmentSessionTime = teacherAssessmentdetail.getAssessmentSessionTime();
				if(assessmentSessionTime <= teacherAssessmentAttempt.getAssessmentSessionTime())
					teacherAssessmentAttempt.setAssessmentSessionTime(0);

				teacherAssessmentAttempt.setIsForced(false);
				teacherAssessmentAttemptDAO.makePersistent(teacherAssessmentAttempt);

				StatusMaster statusMasterIncomp = WorkThreadServlet.statusMap.get("icomp");
				teacherAssessmentStatus.setStatusMaster(statusMasterIncomp);
				teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);

				//////////////////////////////////////////////////////////////////

				TeacherEPIChangedStatusLog teacherEPIChangedStatusLog  = new TeacherEPIChangedStatusLog();
				teacherEPIChangedStatusLog.setUserMaster(userMaster);
				teacherEPIChangedStatusLog.setTeacherDetail(teacherDetail);
				teacherEPIChangedStatusLog.setAssessmentDetail(assessmentDetail);
				teacherEPIChangedStatusLog.setAssessmentName(assessmentDetail.getAssessmentName());
				teacherEPIChangedStatusLog.setTeacherAssessmentStatus(teacherAssessmentStatus);
				if(spResetMsg!=null)
					teacherEPIChangedStatusLog.setResetMessage(spResetMsg);
				teacherEPIChangedStatusLog.setCreatedDateTime(new Date());
				teacherEPIChangedStatusLog.setIpAddress(IPAddressUtility.getIpAddress(request));

				teacherEPIChagedStatusLogDAO.makePersistent(teacherEPIChangedStatusLog);

				teacherAssessmentdetail.setIsDone(false);
				teacherAssessmentDetailDAO.makePersistent(teacherAssessmentdetail);

				String mailText = MailText.getTeacherSpResetMailText(request,teacherDetail);
				String subject = Utility.getLocaleValuePropByKey("msgSpReset", locale);
				DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
				dashboardEmailsToTeacher.setCreatedDateTime(new Date());
				dashboardEmailsToTeacher.setEmailAddress(teacherDetail.getEmailAddress());
				dashboardEmailsToTeacher.setSubject(subject);
				dashboardEmailsToTeacher.setMailText(mailText);
				dashboardEmailsToTeacher.setEmailType("Manual");
				dashboardEmailsToTeacher.setTeacherDetail(teacherDetail);
				dashboardEmailsToTeacherDAO.makePersistent(dashboardEmailsToTeacher);

				emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),subject,mailText);
				
				return teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "1";
	}
	
	/**
	 * It makes timed out assessment to incomplete assessment.
	 * @author Amit Chaudhary 
	 * @param teacherDetail
	 * @param assessmentType
	 * @param teacherAssessmentStatusId
	 * @param resetMsg
	 * @return Teacher First Name and Teacher Last Name as {@link String}
	 */
	@Transactional(readOnly=false)
	public String makeAssessmentIncomplete(TeacherDetail teacherDetail, Integer assessmentType, Integer teacherAssessmentStatusId, String resetMsg)
	{
		System.out.println(":::::::::::::::::::::::::: AdminDashboardAjax : makeAssessmentIncomplete ::::::::::::::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userMaster = (UserMaster)session.getAttribute("userMaster"); 
		try{
			List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
			TeacherAssessmentStatus teacherAssessmentStatus = null;
			TeacherAssessmentdetail teacherAssessmentdetail = null;
			AssessmentDetail assessmentDetail = null;
			JobOrder jobOrder = new JobOrder();
			
			System.out.println("teacherAssessmentStatusId : "+teacherAssessmentStatusId);
			teacherAssessmentStatusList = teacherAssessmentStatusDAO.findTASByTeacherAndTAS(teacherDetail, teacherAssessmentStatusId);
			
			if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0){
				teacherAssessmentStatus = teacherAssessmentStatusList.get((teacherAssessmentStatusList.size()-1));
				teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();
				assessmentDetail = teacherAssessmentStatus.getAssessmentDetail();
				teacherDetail = teacherAssessmentStatus.getTeacherDetail();

				//delete strike
				Session session2 =  teacherStrikeLogDAO.getSession();
				String hql = "delete from teacherstrikelog where teacherAssessmentId = :teacherAssessmentId";
				Query query = session2.createQuery(hql);
				query.setInteger("teacherAssessmentId",teacherAssessmentdetail.getTeacherAssessmentId());
				int rowCount = query.executeUpdate();
				System.out.println("Rows deleted by hql from teacherstrikelog for teacherAssessmentId : "+teacherAssessmentdetail.getTeacherAssessmentId()+" : no of rows deleted : "+rowCount);

				List<TeacherAssessmentAttempt> teacherAssessmentAttemptList = teacherAssessmentAttemptDAO.findNoOfAttempts(assessmentDetail, teacherDetail, teacherAssessmentdetail.getAssessmentTakenCount());
				TeacherAssessmentAttempt teacherAssessmentAttempt = teacherAssessmentAttemptList.get(0);
				
				//delete attempts
				if(teacherAssessmentAttemptList.size()>1){
					teacherAssessmentAttempt = teacherAssessmentAttemptList.get(0);
					teacherAssessmentAttemptList.remove(teacherAssessmentAttempt);
					for (TeacherAssessmentAttempt teacherAssessmentAttempt1 : teacherAssessmentAttemptList) {
						System.out.println("makeTransient : teacherAssessmentAttempt1.getAttemptId() : "+teacherAssessmentAttempt1.getAttemptId());
						teacherAssessmentAttemptDAO.makeTransient(teacherAssessmentAttempt1);
					}
				}

				Integer assessmentSessionTime = teacherAssessmentdetail.getAssessmentSessionTime();
				if(assessmentSessionTime!=null && assessmentSessionTime <= teacherAssessmentAttempt.getAssessmentSessionTime())
					teacherAssessmentAttempt.setAssessmentSessionTime(0);

				teacherAssessmentAttempt.setIsForced(false);
				teacherAssessmentAttemptDAO.makePersistent(teacherAssessmentAttempt);

				StatusMaster statusMasterIncomp = WorkThreadServlet.statusMap.get("icomp");
				teacherAssessmentStatus.setStatusMaster(statusMasterIncomp);
				teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);

				StatusMaster statusMasterVlt = WorkThreadServlet.statusMap.get("vlt");
				StatusMaster statusMasterComp = WorkThreadServlet.statusMap.get("comp");
				List<JobForTeacher> jobForTeacherList = null;
				List<JobForTeacher> jobForTeacherListVlt = null;
				List<JobForTeacher> jobForTeacherListComp = null;

				if(assessmentType==1){//for EPI
					jobForTeacherListVlt  = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail, statusMasterVlt);
					jobForTeacherListComp = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail, statusMasterComp);

					if(jobForTeacherListVlt.size()>0){
						for (JobForTeacher jobForTeacher : jobForTeacherListVlt) {
							jobOrder = jobForTeacher.getJobId();
							if(jobOrder.getJobCategoryMaster().getBaseStatus()==true){
								jobForTeacher.setStatus(statusMasterIncomp);
								jobForTeacher.setStatusMaster(statusMasterIncomp);
								if(statusMasterIncomp!=null){
									jobForTeacher.setApplicationStatus(statusMasterIncomp.getStatusId());
								}
								jobForTeacherDAO.makePersistent(jobForTeacher);
							}
						}
					}
					if(jobForTeacherListComp.size()>0){
						for (JobForTeacher jobForTeacher : jobForTeacherListComp){
							jobOrder = jobForTeacher.getJobId();
							if(jobOrder.getJobCategoryMaster().getBaseStatus()==true){
								jobForTeacher.setStatus(statusMasterIncomp);
								jobForTeacher.setStatusMaster(statusMasterIncomp);
								if(statusMasterIncomp!=null){
									jobForTeacher.setApplicationStatus(statusMasterIncomp.getStatusId());
								}
								jobForTeacherDAO.makePersistent(jobForTeacher);
							}
						}
					}
				}else if(assessmentType==2){//for JSI
					List<JobOrder> jobOrderList = assessmentJobRelationDAO.findJobsByAssessment(assessmentDetail);
					if(jobOrderList!=null && jobOrderList.size()>0){
						System.out.println("for JSI reset : jobOrderList.size() : "+jobOrderList.size());
						jobForTeacherList = jobForTeacherDAO.findJobByTeacherAndSatusFormJobIds(teacherDetail, jobOrderList);
						System.out.println("for JSI reset : jobForTeacherList.size() : "+jobForTeacherList.size());
					}
					
					if(jobForTeacherList!=null && jobForTeacherList.size()>0){
						for(JobForTeacher jobForTeacher : jobForTeacherList){
							jobForTeacher.setStatus(statusMasterIncomp);
							jobForTeacher.setStatusMaster(statusMasterIncomp);
							if(statusMasterIncomp!=null){
								jobForTeacher.setApplicationStatus(statusMasterIncomp.getStatusId());
							}
							jobForTeacherDAO.makePersistent(jobForTeacher);
						}
					}
				}

				TeacherEPIChangedStatusLog teacherEPIChangedStatusLog  = new TeacherEPIChangedStatusLog();
				teacherEPIChangedStatusLog.setUserMaster(userMaster);
				teacherEPIChangedStatusLog.setTeacherDetail(teacherDetail);
				teacherEPIChangedStatusLog.setAssessmentDetail(assessmentDetail);
				teacherEPIChangedStatusLog.setAssessmentName(assessmentDetail.getAssessmentName());
				teacherEPIChangedStatusLog.setAssessmentType(assessmentDetail.getAssessmentType());
				teacherEPIChangedStatusLog.setTeacherAssessmentStatus(teacherAssessmentStatus);
				if(resetMsg!=null)
					teacherEPIChangedStatusLog.setResetMessage(resetMsg);
				teacherEPIChangedStatusLog.setCreatedDateTime(new Date());
				teacherEPIChangedStatusLog.setIpAddress(IPAddressUtility.getIpAddress(request));
				teacherEPIChagedStatusLogDAO.makePersistent(teacherEPIChangedStatusLog);

				teacherAssessmentdetail.setIsDone(false);
				teacherAssessmentDetailDAO.makePersistent(teacherAssessmentdetail);

				//send mail to teacher
				String inventoryName = "";
				String mailText = "";
				String subject = "";
				if(assessmentType==1){
					inventoryName = "EPI (Base Inventory)";
					mailText = MailText.getAssessmentResetMailText(request,teacherDetail,inventoryName);
					subject = Utility.getLocaleValuePropByKey("msgEPIReset", locale);
				}else if(assessmentType==2){
					inventoryName = "Job Specific Inventory";
					mailText = MailText.getAssessmentResetMailText(request,teacherDetail,inventoryName);
					subject = Utility.getLocaleValuePropByKey("msgJsiReset", locale);
				}else if(assessmentType==3){
					inventoryName = "Smart Practices Inventory";
					mailText = MailText.getAssessmentResetMailText(request,teacherDetail,inventoryName);
					subject = Utility.getLocaleValuePropByKey("msgSpReset", locale);
				}else if(assessmentType==4){
					inventoryName = "Instructional Proficiency Inventory";
					mailText = MailText.getAssessmentResetMailText(request,teacherDetail,inventoryName);
					subject = Utility.getLocaleValuePropByKey("msgIpiReset", locale);
				}
				DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
				dashboardEmailsToTeacher.setCreatedDateTime(new Date());
				dashboardEmailsToTeacher.setEmailAddress(teacherDetail.getEmailAddress());
				dashboardEmailsToTeacher.setSubject(subject);
				dashboardEmailsToTeacher.setMailText(mailText);
				dashboardEmailsToTeacher.setEmailType("Manual");
				dashboardEmailsToTeacher.setTeacherDetail(teacherDetail);
				dashboardEmailsToTeacherDAO.makePersistent(dashboardEmailsToTeacher);

				emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(),subject,mailText);
				
				return teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return "1";
	}
}
