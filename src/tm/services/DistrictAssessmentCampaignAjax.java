package tm.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.DistrictAssessmentQuestionOptions;
import tm.bean.districtassessment.DistrictAssessmentQuestions;
import tm.bean.districtassessment.DistrictAssessmentSection;
import tm.bean.districtassessment.TeacherDistrictAssessmentAnswerDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentAttempt;
import tm.bean.districtassessment.TeacherDistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentOption;
import tm.bean.districtassessment.TeacherDistrictAssessmentQuestion;
import tm.bean.districtassessment.TeacherDistrictAssessmentStatus;
import tm.bean.districtassessment.TeacherDistrictSectionDetail;
import tm.bean.districtassessment.TeacherDistrictStrikeLog;
import tm.bean.master.QuestionTypeMaster;
import tm.bean.master.StatusMaster;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.districtassessment.DistrictAssessmentDetailDAO;
import tm.dao.districtassessment.DistrictAssessmentQuestionsDAO;
import tm.dao.districtassessment.DistrictAssessmentSectionDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentAnswerDetailDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentAttemptDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentDetailDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentOptionDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentQuestionDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentStatusDAO;
import tm.dao.districtassessment.TeacherDistrictSectionDetailDAO;
import tm.dao.districtassessment.TeacherDistrictStrikeLogDAO;
import tm.dao.master.StatusMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;
public class DistrictAssessmentCampaignAjax {

	String locale = Utility.getValueOfPropByKey("locale");

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}


	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;

	@Autowired
	private TeacherDistrictAssessmentStatusDAO teacherDistrictAssessmentStatusDAO;

	@Autowired
	private DistrictAssessmentDetailDAO districtAssessmentDetailDAO;

	@Autowired
	private DistrictAssessmentQuestionsDAO districtAssessmentQuestionsDAO;


	@Autowired
	private TeacherDistrictAssessmentAnswerDetailDAO teacherDistrictAssessmentAnswerDetailDAO;

	@Autowired
	private TeacherDistrictAssessmentAttemptDAO teacherDistrictAssessmentAttemptDAO;

	@Autowired
	private TeacherDistrictAssessmentDetailDAO teacherDistrictAssessmentDetailDAO;

	@Autowired
	private TeacherDistrictAssessmentQuestionDAO teacherDistrictAssessmentQuestionDAO;

	@Autowired
	private TeacherDistrictAssessmentOptionDAO teacherDistrictAssessmentOptionDAO;

	@Autowired
	private TeacherDistrictStrikeLogDAO teacherDistrictStrikeLogDAO;

	@Autowired
	private DistrictAssessmentSectionDAO districtAssessmentSectionDAO;

	@Autowired
	private TeacherDistrictSectionDetailDAO teacherDistrictSectionDetailDAO;

	/* @Author: Vishwanath Kumar
	 * @Discription: check Inventory.
	 */
	public DistrictAssessmentDetail checkDistrictInventory(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictAssessmentDetail districtAssessmentDetail)
	{
		System.out.println(":::::::::::::::Assessment checkInventory:::::::::::::::::::>>> ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		//teacherDetail = teacherDetailDAO.findById(teacherDetail.getTeacherId(), false, false);
		int newJobId = jobOrder.getJobId();

		if(jobOrder.getJobId()!=null && jobOrder.getJobId()>0)
			jobOrder = jobOrderDAO.findById(newJobId, false, false);

		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList = null;
		TeacherDistrictAssessmentStatus teacherDistrictAssessmentStatus = null;
		teacherDistrictAssessmentStatusList=teacherDistrictAssessmentStatusDAO.findDistrictAssessmentTakenByTeacher(teacherDetail,districtAssessmentDetail);

		/////////////////////////////////////
		int oldJobId = 0;
		if(teacherDistrictAssessmentStatusList!=null && teacherDistrictAssessmentStatusList.size()!=0)
		{
			teacherDistrictAssessmentStatus = teacherDistrictAssessmentStatusList.get(0);
			districtAssessmentDetail = teacherDistrictAssessmentStatus.getDistrictAssessmentDetail();
			String statusShortName=teacherDistrictAssessmentStatus.getStatusMaster().getStatusShortName();

			if(statusShortName.equalsIgnoreCase("comp"))
			{
				districtAssessmentDetail.setDistrictAssessmentName(null);
				districtAssessmentDetail.setStatus("3");
				return districtAssessmentDetail;
			}else if(statusShortName.equalsIgnoreCase("exp"))
			{
				districtAssessmentDetail.setDistrictAssessmentName(null);
				districtAssessmentDetail.setStatus("4");
				return districtAssessmentDetail;
			}
			else if(statusShortName.equalsIgnoreCase("vlt"))
			{
				districtAssessmentDetail.setDistrictAssessmentName(null);
				districtAssessmentDetail.setStatus("5");
				return districtAssessmentDetail;
			}
			else if(statusShortName.equalsIgnoreCase("icomp"))
			{
				List<TeacherDistrictAssessmentAttempt> teacherDistrictAssessmentAttempts = null;
				teacherDistrictAssessmentAttempts = teacherDistrictAssessmentAttemptDAO.findNoOfAttempts(districtAssessmentDetail,teacherDetail);
				int attempts = 0;
				for (TeacherDistrictAssessmentAttempt teacherDistrictAssessmentAttempt1 : teacherDistrictAssessmentAttempts) {
					if(!teacherDistrictAssessmentAttempt1.getIsForced())
						attempts++;
				}
				////////////////////////

				int strikeLogs=0; 
				try{
					if(teacherDistrictAssessmentAttempts.size()>0)
					{
						List<TeacherDistrictAssessmentDetail> teacherDistrictAssessmentDetails = null;
						teacherDistrictAssessmentDetails = teacherDistrictAssessmentDetailDAO.getTeacherDistrictAssessmentdetails(teacherDistrictAssessmentAttempts.get(0).getDistrictAssessmentDetail(), teacherDetail);
						List<TeacherDistrictStrikeLog> teacherDistrictStrikeLog = null;
						teacherDistrictStrikeLog = teacherDistrictStrikeLogDAO.getTotalStrikesByTeacher(teacherDetail, teacherDistrictAssessmentDetails.get(0));
						strikeLogs=teacherDistrictStrikeLog.size();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				int attemptsStrike=attempts+strikeLogs;

				if(attemptsStrike==3)
				{
					//StatusMaster statusMaster = WorkThreadServlet.statusMap.get("vlt");
					TeacherDistrictAssessmentDetail teacherDistrictAssessmentdetail = teacherDistrictAssessmentStatus.getTeacherDistrictAssessmentDetail();

					List<TeacherDistrictAssessmentStatus> lstTeacherDistrictAssessmentStatus = null;
					lstTeacherDistrictAssessmentStatus =teacherDistrictAssessmentStatusDAO.findDistrictAssessmentStatusByTeacherAssessmentdetail(teacherDistrictAssessmentdetail);
					teacherDistrictAssessmentStatus=lstTeacherDistrictAssessmentStatus.get(0);

					//List<JobOrder> jobs = new ArrayList<JobOrder>();

					if(lstTeacherDistrictAssessmentStatus.size()!=0)
						if(teacherDistrictAssessmentStatus!=null)
						{
							for(TeacherDistrictAssessmentStatus tdas: lstTeacherDistrictAssessmentStatus)
							{

								teacherDistrictAssessmentStatusDAO.makePersistent(tdas);
							}
						}
				}

				districtAssessmentDetail.setStatus(districtAssessmentDetail.getStatus()+"#"+attemptsStrike+"#"+oldJobId);

				return districtAssessmentDetail;
			}

		}
		System.out.println("[[[[[[]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]");

		try {
			System.out.println("istrictAssessmentDetail.getDistrictAssessmentId():: "+districtAssessmentDetail.getDistrictAssessmentId());
			districtAssessmentDetail = districtAssessmentDetailDAO.findAssessmentDetail(districtAssessmentDetail.getDistrictAssessmentId());
			System.out.println("districtAssessmentDetail:: "+districtAssessmentDetail);
			if(districtAssessmentDetail!=null)
			{
				if(districtAssessmentDetail.getStatus().equalsIgnoreCase("A"))
				{
					List<DistrictAssessmentQuestions> districtAssessmentQuestions = districtAssessmentQuestionsDAO.getDistrictAssessmentQuestions(districtAssessmentDetail);
					if(districtAssessmentQuestions.size()>0)
					{
						return districtAssessmentDetail;
					}
					else
					{
						districtAssessmentDetail.getDistrictAssessmentDescription();
						districtAssessmentDetail.setDistrictAssessmentName(null);
						districtAssessmentDetail.setStatus("2"); 
						return districtAssessmentDetail;
					}
				}
				else{
					districtAssessmentDetail.setDistrictAssessmentName(null);
					districtAssessmentDetail.setStatus("2");// JSI not active
					return districtAssessmentDetail;
				}
			}else
			{
				districtAssessmentDetail = new DistrictAssessmentDetail();
				districtAssessmentDetail.setDistrictAssessmentName(null);
				districtAssessmentDetail.setStatus("8");//Does not exist
				return districtAssessmentDetail;
			}



		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to load Assessment Questions.
	 */
	@Transactional(readOnly=false)
	public String loadDistrictAssessmentQuestions(DistrictAssessmentDetail districtAssessmentDetail,JobOrder jobOrder,TeacherDetail teacherDetail)
	{
		System.out.println(":::::::::::::::District Assessment loadAssessmentQuestions:::::::::::::::::::;; ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		Integer jId = 0;
		if(jobOrder!=null && jobOrder.getJobId()==0)
			jId = null;

		List<TeacherDistrictAssessmentStatus> teacherDisrictAssessmentStatusList =null;
		try{

			String ipAddress = IPAddressUtility.getIpAddress(request);
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
			//teacherDetail = teacherDetailDAO.findById(teacherDetail.getTeacherId(), false, false);

			Boolean questionRandomization = districtAssessmentDetail.getQuestionRandomization();
			Boolean optionRandomization = districtAssessmentDetail.getOptionRandomization();
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);

			teacherDisrictAssessmentStatusList=teacherDistrictAssessmentStatusDAO.findByCriteria(criterion,criterion1);

			if(teacherDisrictAssessmentStatusList.size()==0)
			{
				try {

					/////////////////////////////////////////////////////////////////////////////////
					TeacherDistrictAssessmentDetail teacherDistrictAssessmentdetail = new TeacherDistrictAssessmentDetail();
					teacherDistrictAssessmentdetail.setDistrictAssessmentDetail(districtAssessmentDetail);
					teacherDistrictAssessmentdetail.setDistrictAssessmentDescription(districtAssessmentDetail.getDistrictAssessmentDescription());
					teacherDistrictAssessmentdetail.setDistrictAssessmentName(districtAssessmentDetail.getDistrictAssessmentName());
					teacherDistrictAssessmentdetail.setStatus(districtAssessmentDetail.getStatus());
					teacherDistrictAssessmentdetail.setCreatedDateTime(new Date());
					teacherDistrictAssessmentdetail.setDistrictMaster(districtAssessmentDetail.getDistrictMaster());
					teacherDistrictAssessmentdetail.setIpaddress(ipAddress);
					teacherDistrictAssessmentdetail.setOptionRandomization(districtAssessmentDetail.getOptionRandomization());
					teacherDistrictAssessmentdetail.setQuestionRandomization(districtAssessmentDetail.getQuestionRandomization());
					teacherDistrictAssessmentdetail.setSchoolMaster(districtAssessmentDetail.getSchoolMaster());
					teacherDistrictAssessmentdetail.setTeacherDetail(teacherDetail);
					teacherDistrictAssessmentdetail.setUserMaster(districtAssessmentDetail.getUserMaster());
					teacherDistrictAssessmentdetail.setIsDone(false);

					teacherDistrictAssessmentdetail.setQuestionSessionTime(districtAssessmentDetail.getQuestionSessionTime());
					teacherDistrictAssessmentdetail.setAssessmentSessionTime(districtAssessmentDetail.getAssessmentSessionTime());


					teacherDistrictAssessmentDetailDAO.makePersistent(teacherDistrictAssessmentdetail);

					List<DistrictAssessmentSection> districtAssessmentSectionsList = null;
					if(questionRandomization!=null && questionRandomization==true)
						districtAssessmentSectionsList = districtAssessmentSectionDAO.getRNDSectionsByDistrictAssessmentId(districtAssessmentDetail);
					else
						districtAssessmentSectionsList = districtAssessmentSectionDAO.getSectionsByDistrictAssessmentId(districtAssessmentDetail);

					if(districtAssessmentSectionsList.size()!=0)
					{

						for (DistrictAssessmentSection districtAssessmentSection : districtAssessmentSectionsList) 
						{

							TeacherDistrictSectionDetail teacherDistrictSectionDetail = new TeacherDistrictSectionDetail(); 

							teacherDistrictSectionDetail.setTeacherDistrictAssessmentdetail(teacherDistrictAssessmentdetail);
							teacherDistrictSectionDetail.setCreatedDateTime(new Date());
							teacherDistrictSectionDetail.setIpAddress(ipAddress);
							teacherDistrictSectionDetail.setSectionDescription(districtAssessmentSection.getSectionDescription());
							teacherDistrictSectionDetail.setSectionInstructions(districtAssessmentSection.getSectionInstructions());
							teacherDistrictSectionDetail.setSectionName(districtAssessmentSection.getSectionName());
							teacherDistrictSectionDetail.setStatus(districtAssessmentSection.getStatus());
							teacherDistrictSectionDetail.setUserMaster(districtAssessmentSection.getUserMaster());
							teacherDistrictSectionDetail.setTeacherDetail(teacherDetail);

							teacherDistrictSectionDetailDAO.makePersistent(teacherDistrictSectionDetail);

							List<DistrictAssessmentQuestions> districtAssessmentQuestions = new ArrayList<DistrictAssessmentQuestions>();
							districtAssessmentQuestions = districtAssessmentQuestionsDAO.getDistrictAssessmentQuestions(districtAssessmentDetail,districtAssessmentSection);
							if(districtAssessmentQuestions.size()>0 && questionRandomization!=null && questionRandomization==true)
								Collections.shuffle(districtAssessmentQuestions);


							List<DistrictAssessmentQuestionOptions> districtAssessmentQuestionOptions = null;
							if(districtAssessmentQuestions.size()!=0)
							{

								for (DistrictAssessmentQuestions districtAssessmentQuestion : districtAssessmentQuestions) 
								{

									TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQuestion = new TeacherDistrictAssessmentQuestion();
									teacherDistrictAssessmentQuestion.setTeacherDistrictAssessmentDetail(teacherDistrictAssessmentdetail);
									teacherDistrictAssessmentQuestion.setTeacherDistrictSectionDetail(teacherDistrictSectionDetail);
									teacherDistrictAssessmentQuestion.setQuestion(districtAssessmentQuestion.getQuestion());
									teacherDistrictAssessmentQuestion.setQuestionInstruction(districtAssessmentQuestion.getQuestionInstruction());
									teacherDistrictAssessmentQuestion.setQuestionTypeMaster(districtAssessmentQuestion.getQuestionTypeMaster());
									teacherDistrictAssessmentQuestion.setQuestionWeightage(districtAssessmentQuestion.getQuestionWeightage());
									teacherDistrictAssessmentQuestion.setQuestionImage(districtAssessmentQuestion.getQuestionImage());
									if(districtAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName().equals("itq") && districtAssessmentQuestion.getQuestionImage()!=null && !districtAssessmentQuestion.getQuestionImage().equals(""))
									{
										try {
											ImageCopyThread ict = new ImageCopyThread();
											String fileName= districtAssessmentQuestion.getQuestionImage().trim();
											ict.setFileName(fileName);


											String source = Utility.getValueOfPropByKey("ques_imagesRootPath")+""+fileName;
											//System.out.println("source:: "+source);
											String target = Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/questions";
											//System.out.println("target:: "+target);
											ict.setSourceFile(source);
											ict.setTargetFile(target);

											/*File sourceFile = new File(source);
											File targetDir = new File(target);
											if(!targetDir.exists())
												targetDir.mkdirs();

											System.out.println("ourceFile.getName():::::::::::: "+sourceFile.getName());
											File targetFile = new File(targetDir+"/"+sourceFile.getName());

											FileUtils.copyFile(sourceFile, targetFile);*/

											ict.start();

										} catch (Exception e) {
											e.printStackTrace();
										}
									}
									teacherDistrictAssessmentQuestion.setMaxMarks(districtAssessmentQuestion.getMaxMarks());
									teacherDistrictAssessmentQuestion.setTeacherDetail(teacherDetail);
									teacherDistrictAssessmentQuestion.setIsAttempted(false);
									teacherDistrictAssessmentQuestion.setCreatedDateTime(new Date());
									teacherDistrictAssessmentQuestionDAO.makePersistent(teacherDistrictAssessmentQuestion);
									QuestionTypeMaster questionTypeMaster= districtAssessmentQuestion.getQuestionTypeMaster();
									// Likert Scale check
									districtAssessmentQuestionOptions=districtAssessmentQuestion.getDistrictAssessmentQuestionOptions();
									if(optionRandomization!=null && optionRandomization==true && !questionTypeMaster.getQuestionTypeShortName().equalsIgnoreCase("lkts"))
										Collections.shuffle(districtAssessmentQuestionOptions);

									if(districtAssessmentQuestionOptions.size()!=0)
									{
										for (DistrictAssessmentQuestionOptions districtAssessmentQuestionOption : districtAssessmentQuestionOptions) {
											//System.out.println("LL "+questionOption.getQuestionOption());
											TeacherDistrictAssessmentOption teacherDistrictAssessmentOption = new TeacherDistrictAssessmentOption();

											teacherDistrictAssessmentOption.setTeacherDetail(teacherDetail);
											teacherDistrictAssessmentOption.setTeacherDistrictAssessmentQuestion(teacherDistrictAssessmentQuestion);
											teacherDistrictAssessmentOption.setQuestionOption(districtAssessmentQuestionOption.getQuestionOption());
											teacherDistrictAssessmentOption.setRank(districtAssessmentQuestionOption.getRank());
											teacherDistrictAssessmentOption.setScore(districtAssessmentQuestionOption.getScore());
											teacherDistrictAssessmentOption.setCreatedDateTime(new Date());
											teacherDistrictAssessmentOptionDAO.makePersistent(teacherDistrictAssessmentOption);
										}
									}

								}

							}
						}
					}


					////////////////////// 

					// teacherassessmentattempt
					TeacherDistrictAssessmentAttempt teacherDistrictAssessmentAttempt = new TeacherDistrictAssessmentAttempt();
					teacherDistrictAssessmentAttempt.setDistrictAssessmentDetail(districtAssessmentDetail);
					teacherDistrictAssessmentAttempt.setIpAddress(ipAddress);
					teacherDistrictAssessmentAttempt.setJobOrder(jobOrder);

					teacherDistrictAssessmentAttempt.setTeacherDetail(teacherDetail);
					teacherDistrictAssessmentAttempt.setDistrictAssessmentSessionTime(0);
					teacherDistrictAssessmentAttempt.setDistrictAssessmentStartTime(new Date());
					teacherDistrictAssessmentAttempt.setIsForced(false);
					teacherDistrictAssessmentAttemptDAO.makePersistent(teacherDistrictAssessmentAttempt);

					/////// TeacherAssessmentStatus
					TeacherDistrictAssessmentStatus teacherDistrictAssessmentStatus = new TeacherDistrictAssessmentStatus();
					teacherDistrictAssessmentStatus.setDistrictAssessmentDetail(districtAssessmentDetail);
					teacherDistrictAssessmentStatus.setTeacherDetail(teacherDetail);
					teacherDistrictAssessmentStatus.setTeacherDistrictAssessmentDetail(teacherDistrictAssessmentdetail);
					teacherDistrictAssessmentStatus.setCreatedDateTime(new Date());
					StatusMaster statusMaster=WorkThreadServlet.statusMap.get("icomp");
					teacherDistrictAssessmentStatus.setStatusMaster(statusMaster);
					teacherDistrictAssessmentStatus.setJobOrder(jobOrder);
					teacherDistrictAssessmentStatus.setMarksObtained(0.0);
					teacherDistrictAssessmentStatus.setMarksObtained(0.0);
					
					teacherDistrictAssessmentStatusDAO.makePersistent(teacherDistrictAssessmentStatus);

				} catch (Exception e) {
					e.printStackTrace();
				}


			}else
			{
				return teacherDisrictAssessmentStatusList.get(0).getStatusMaster().getStatusShortName();
			}


		}catch (Exception e) {
			e.printStackTrace();
		}
		return "1";
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment Section Questions.
	 */
	/*@Transactional(readOnly=false)
	public String getDistrictAssessmentSectionCampaignQuestions(DistrictAssessmentDetail districtAssessmentDetail,TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail,TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQues,TeacherDistrictSectionDetail teacherDistrictSectionDetail,TeacherDistrictAssessmentAnswerDetail[] teacherDistrictAssessmentAnswerDetails,int attemptId,int newJobId,TeacherDetail teacherDetail)
	{
		//System.out.println("::::::::::::::::getAssessmentSectionCampaignQuestions::::::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		StringBuffer sb =new StringBuffer();
		try{


			try{
				System.out.print("DistrictAssesment:"+new Date()+", T:"+teacherDetail.getTeacherId() +", E:"+teacherDetail.getEmailAddress());
			}catch(Exception e){
				e.printStackTrace();
			}

			if(attemptId!=0)
			{
				teacherDistrictAssessmentAttemptDAO.updateDistrictAssessmentAttemptSessionTime(attemptId,false);
			}
			Double marksObtained = 0.0;
			Double totalMarks = 0.0;
			if(teacherDistrictAssessmentQues!=null && teacherDistrictAssessmentQues.getTeacherDistrictAssessmentQuestionId()!=null)
			{

				TeacherDistrictAssessmentAnswerDetail teacherDistrictAssessmentAnswerDetail;
				if(teacherDistrictAssessmentAnswerDetails!=null && teacherDistrictAssessmentAnswerDetails.length>0)
				{
					teacherDistrictAssessmentAnswerDetail = teacherDistrictAssessmentAnswerDetails[0];
					//teacherDistrictAssessmentAnswerDetail.setJobOrder(null);
					teacherDistrictAssessmentAnswerDetail.setDistrictAssessmentDetail(districtAssessmentDetail);
					teacherDistrictAssessmentAnswerDetail.setTeacherDistrictAssessmentDetail(teacherDistrictAssessmentDetail);
					teacherDistrictAssessmentAnswerDetail.setTeacherDistrictAssessmentQuestion(teacherDistrictAssessmentQues);
					teacherDistrictAssessmentAnswerDetail.setCreatedDateTime(new Date());
					teacherDistrictAssessmentAnswerDetail.setTeacherDetail(teacherDetail);
					teacherDistrictAssessmentAnswerDetailDAO.makePersistent(teacherDistrictAssessmentAnswerDetail);
					if(teacherDistrictAssessmentAnswerDetail.getTotalScore()!=null)
					{
						List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList = teacherDistrictAssessmentStatusDAO.findDistrictAssessmentStatusByTeacher(teacherDetail,teacherDistrictAssessmentDetail);
						if(teacherDistrictAssessmentStatusList!=null)
						{
							TeacherDistrictAssessmentStatus teacherDistrictAssessmentStatus= teacherDistrictAssessmentStatusList.get(0);
							if(teacherDistrictAssessmentStatus.getMarksObtained()!=null)
							{
								marksObtained = teacherDistrictAssessmentStatus.getMarksObtained() +teacherDistrictAssessmentAnswerDetail.getTotalScore();
								teacherDistrictAssessmentStatus.setMarksObtained(marksObtained);
							}else
							{
								marksObtained = teacherDistrictAssessmentAnswerDetail.getTotalScore();
								teacherDistrictAssessmentStatus.setMarksObtained(marksObtained);
							}

							if(teacherDistrictAssessmentStatus.getTotalMarks()!=null)
							{
								totalMarks = teacherDistrictAssessmentStatus.getTotalMarks() +teacherDistrictAssessmentAnswerDetail.getMaxMarks();
								teacherDistrictAssessmentStatus.setTotalMarks(totalMarks);
							}else
							{
								totalMarks = teacherDistrictAssessmentAnswerDetail.getMaxMarks();
								teacherDistrictAssessmentStatus.setTotalMarks(totalMarks);
							}

							teacherDistrictAssessmentStatus.setUpdatedDate(new Date());
						}
					}


				}
				/////////////////////////////////////////////////////////////////////////////////////////////
				teacherDistrictAssessmentQues=teacherDistrictAssessmentQuestionDAO.findById(teacherDistrictAssessmentQues.getTeacherDistrictAssessmentQuestionId(), false, false);
				teacherDistrictAssessmentQues.setIsAttempted(true);
				teacherDistrictAssessmentQuestionDAO.makePersistent(teacherDistrictAssessmentQues);
			}
			int teacherDistrictSectionId = 0;
			if(teacherDistrictSectionDetail!=null && teacherDistrictSectionDetail.getTeacherDistrictSectionId()!=null)
			{
				teacherDistrictSectionId = teacherDistrictSectionDetail.getTeacherDistrictSectionId();
			}
			List<TeacherDistrictAssessmentQuestion> teacherDistrictAssessmentQuestions =null;
			Criterion criterion = Restrictions.eq("teacherDistrictAssessmentDetail", teacherDistrictAssessmentDetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("isAttempted", false);
			Criterion criterion3 = Restrictions.eq("isAttempted", true);
			int totalQuestions = teacherDistrictAssessmentQuestionDAO.getRowCount(criterion,criterion1);
			int totalQuestionsAttempted = teacherDistrictAssessmentQuestionDAO.getRowCount(criterion,criterion1,criterion3);
			teacherDistrictAssessmentQuestions = teacherDistrictAssessmentQuestionDAO.findWithLimit(null,0,1,criterion,criterion1,criterion2);
			List<TeacherDistrictAssessmentOption> teacherDistrictAssessmentQuestionOptionsList = null;
			String questionInstruction = null;
			int teacherDistrictSectionIDD = 0;
			String shortName = "";
			String sectionInstructions="";
			for (TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQuestion : teacherDistrictAssessmentQuestions) 
			{
				teacherDistrictAssessmentDetail = teacherDistrictAssessmentQuestion.getTeacherDistrictAssessmentDetail();

				teacherDistrictSectionIDD=teacherDistrictAssessmentQuestion.getTeacherDistrictSectionDetail().getTeacherDistrictSectionId();
				if(teacherDistrictSectionId==0 || (teacherDistrictSectionIDD!=teacherDistrictSectionId))
				{
					sb.append("<tr>");
					sb.append("<td width='90%'>");
					sb.append("<h3>Section: "+teacherDistrictAssessmentQuestion.getTeacherDistrictSectionDetail().getSectionName()+"</h3><br>");
					sb.append("<b>Instructions for the section</b><br><br>");

					sectionInstructions = teacherDistrictAssessmentQuestion.getTeacherDistrictSectionDetail().getSectionInstructions();
					if(!sectionInstructions.equals(""))
						sb.append(Utility.getUTFToHTML(sectionInstructions)+"<br>");
					else
						sb.append("Plese click on \"Next\" to continue.<br>");

					if(teacherDistrictSectionId!=0)
						sb.append("<input type='hidden' id='teacherDistrictAssessmentQuestionId' value=''/>" );
					sb.append("<input type='hidden' id='teacherDistrictSectionId' value='"+teacherDistrictSectionIDD+"'/>" );
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("document.getElementById('sectionName').innerHTML='<b>Section: "+teacherDistrictAssessmentQuestion.getTeacherDistrictSectionDetail().getSectionName()+"</b>';");
					sb.append("document.getElementById('sectionName').style.display='none';");
					sb.append("document.getElementById('questionTimer').style.display='none';");
					sb.append("</script>");
					sb.append("</td>");
					sb.append("</tr>");

					return sb.toString();

				}
				sb.append("<tr>");
				sb.append("<td width='90%'>");

				sb.append("<div style='text-align:right;'>"+Utility.genrateHTMLspace(198)+"<b>Question "+(++totalQuestionsAttempted)+" of "+totalQuestions+"</b></div>");
				try{
					System.out.println(", Q:"+(totalQuestionsAttempted)+" of "+totalQuestions);
				}catch(Exception e){}
				questionInstruction = teacherDistrictAssessmentQuestion.getQuestionInstruction()==null?"":teacherDistrictAssessmentQuestion.getQuestionInstruction();
				if(!questionInstruction.equals(""))
					sb.append(""+Utility.getUTFToHTML(questionInstruction)+"<br/>");

				sb.append(""+Utility.getUTFToHTML(teacherDistrictAssessmentQuestion.getQuestion())+"<br/>");

				shortName=teacherDistrictAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName();
				System.out.println("shortName: "+shortName);
				System.out.println("teacherDistrictAssessmentQuestion.getQuestionTypeMaster(): "+teacherDistrictAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeId());

				if(shortName.equalsIgnoreCase("itq"))
				{
					//sb.append("<img src='showImage?image=ques_images/"+teacherDistrictAssessmentQuestion.getQuestionImage()+"'><br>");
					sb.append("<img src='showImage?image=teacher/"+teacherDetail.getTeacherId()+"/questions/"+teacherDistrictAssessmentQuestion.getQuestionImage()+"'><br>");
				}

				sb.append("<input type='hidden' name='o_maxMarks' value='"+(teacherDistrictAssessmentQuestion.getMaxMarks()==null?0:teacherDistrictAssessmentQuestion.getMaxMarks())+"'  />");

				teacherDistrictAssessmentQuestionOptionsList = teacherDistrictAssessmentQuestion.getTeacherDistrictAssessmentOptions();
				shortName=teacherDistrictAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName();

				if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel") || shortName.equalsIgnoreCase("lkts") || shortName.equalsIgnoreCase("itq"))
				{
					sb.append("<table >");
					for (TeacherDistrictAssessmentOption teacherDistrictQuestionOption : teacherDistrictAssessmentQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='opt' value='"+teacherDistrictQuestionOption.getTeacherDistrictAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;padding: 5px 3px;' >"+teacherDistrictQuestionOption.getQuestionOption()+" ");
						sb.append("<input type='hidden' name='score' value='"+(teacherDistrictQuestionOption.getScore()==null?0:teacherDistrictQuestionOption.getScore())+"'  /></td></tr>");
					}
					sb.append("</table>");
				}
				if(shortName.equalsIgnoreCase("it"))
				{
					sb.append("<table >");
					for (TeacherDistrictAssessmentOption teacherDistrictQuestionOption : teacherDistrictAssessmentQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='opt' value='"+teacherDistrictQuestionOption.getTeacherDistrictAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherDistrictQuestionOption.getQuestionOption()+"'> ");
						sb.append("<input type='hidden' name='score' value='"+(teacherDistrictQuestionOption.getScore()==null?0:teacherDistrictQuestionOption.getScore())+"'  /></td></tr>");
					}
					sb.append("</table>");
				}
				else if(shortName.equalsIgnoreCase("rt"))
				{
					int rank=1;
					sb.append("<table>");
					for (TeacherDistrictAssessmentOption teacherDistrictQuestionOption : teacherDistrictAssessmentQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' id='rnk"+rank+"' name='rank' class='span1' maxlength='1' onkeypress='return checkForIntUpto6(event)' onblur='checkUniqueRank(this);'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherDistrictQuestionOption.getQuestionOption()+"");
						sb.append("<input type='hidden' name='score' value='"+(teacherDistrictQuestionOption.getScore()==null?0:teacherDistrictQuestionOption.getScore())+"' /><input type='hidden' name='opt' value='"+teacherDistrictQuestionOption.getTeacherDistrictAssessmentOptionId()+"' />");
						sb.append("<input type='hidden' name='o_rank' value='"+(teacherDistrictQuestionOption.getRank()==null?0:teacherDistrictQuestionOption.getRank())+"' />");
						sb.append("<script type=\"text/javascript\" language=\"javascript\">");
						sb.append("document.getElementById('rnk1').focus();");
						sb.append("</script></td></tr>");
						rank++;
					}
					sb.append("</table >");
				}else if(shortName.equalsIgnoreCase("sl"))
				{
					sb.append("&nbsp;&nbsp;<input type='text' name='opt' id='opt' class='span15' maxlength='75'/><br/>");
					//sb.append(Utility.genrateHTMLspace(190)+"Max Length: 75 Characters<br/>");
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("document.getElementById('opt').focus();");
					sb.append("</script>");
				}
				else if(shortName.equalsIgnoreCase("ml"))
				{
					sb.append("&nbsp;&nbsp;<textarea name='opt' id='opt' class='form-control' maxlength='5000' rows='15' style='width:98%;margin:5px;' /></textarea><br/>");
					//sb.append(Utility.genrateHTMLspace(187)+"Max Length: 5000 Characters<br/>");
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("document.getElementById('opt').focus();");
					sb.append("</script>");
				}
				sb.append("<input type='hidden' id='teacherDistrictAssessmentQuestionId' value='"+teacherDistrictAssessmentQuestion.getTeacherDistrictAssessmentQuestionId()+"'/>" );
				sb.append("<input type='hidden' id='questionImage' value='"+teacherDistrictAssessmentQuestion.getQuestionImage()+"'/>" );
				sb.append("<input type='hidden' id='teacherDistrictSectionId' value='"+teacherDistrictSectionIDD+"'/>" );
				sb.append("<input type='hidden' id='questionWeightage' value='"+(teacherDistrictAssessmentQuestion.getQuestionWeightage()==null?0:teacherDistrictAssessmentQuestion.getQuestionWeightage())+"'/>" );
				sb.append("<input type='hidden' id='questionTypeId' value='"+teacherDistrictAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
				sb.append("<input type='hidden' id='questionTypeShortName' value='"+teacherDistrictAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName()+"'/>" );

				if(teacherDistrictAssessmentDetail.getQuestionSessionTime()!=null && teacherDistrictAssessmentDetail.getQuestionSessionTime()!=0)
				{
					sb.append("<script type=\"text/javascript\" language=\"javascript\">tmTimer("+teacherDistrictAssessmentDetail.getQuestionSessionTime()+",3);" );
					//sb.append("document.getElementById('timer').style.display='block';");
					sb.append("</script>");
				}
				sb.append("<input type='hidden' id='questionSessionTime' value='"+(teacherDistrictAssessmentDetail.getQuestionSessionTime()==null?0:teacherDistrictAssessmentDetail.getQuestionSessionTime())+"'/>" );
				sb.append("</td>");
				sb.append("</tr>");

			}
			if(teacherDistrictAssessmentQuestions.size()==0)
			{
				sb.append("<tr id='tr1'><td id='tr1'>");
				if(totalQuestions!=0)
				{
					// TeacherAssessmentStatus update after completion
					List<TeacherDistrictAssessmentStatus> lstTeacherDistrictAssessmentStatus = null;
					lstTeacherDistrictAssessmentStatus =teacherDistrictAssessmentStatusDAO.findDistrictAssessmentStatusByTeacherAssessmentdetail(teacherDistrictAssessmentDetail);

					TeacherDistrictAssessmentStatus teacherDistrictAssessmentStatus=lstTeacherDistrictAssessmentStatus.get(0);
					if(lstTeacherDistrictAssessmentStatus.size()!=0)
						if(teacherDistrictAssessmentStatus!=null)
						{
							teacherDistrictAssessmentDetail = teacherDistrictAssessmentStatus.getTeacherDistrictAssessmentDetail();

							StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");

							List<JobOrder> jobs = new ArrayList<JobOrder>();
							for(TeacherDistrictAssessmentStatus tas: lstTeacherDistrictAssessmentStatus)
							{
								jobs.add(tas.getJobOrder());

								tas.setStatusMaster(statusMaster);
								tas.setUpdatedDate(new Date());
								teacherDistrictAssessmentStatusDAO.makePersistent(tas);
							}

							//						System.out.println("done.......................");
							//sb.append("Assessment completed");
							sb.append("<br><b>Thank you, you have completed this Assessment</b>");
							sb.append("<br><br><b>Total Marks Obtained: "+marksObtained.intValue()+"</b>");
							sb.append("<br><br><button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"location.href='myassessments.do' \">Go Back to your Assessment Page</button><br><br> Or <br><br> You will be redirected to your Assessment page after 10 seconds.");//
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							sb.append("window.onbeforeunload = function() {};");
							sb.append("document.getElementById('sbmtbtn').style.display='none';");
							sb.append("setTimeout(function() {");
							sb.append("window.location.href='myassessments.do';");
							sb.append("}, 10000)");
							sb.append("</script>");

						}

					teacherDistrictAssessmentDetail.setIsDone(true);
					teacherDistrictAssessmentDetailDAO.makePersistent(teacherDistrictAssessmentDetail);

				}else
				{
					sb.append("No Questions found");
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("window.onbeforeunload = function() {};");
					sb.append("document.getElementById('mainDiv').style.display='none';");
					//sb.append("alert('No Questions found');");
					sb.append("window.location.href='myassessments.do'");

					sb.append("</script>");
				}

				sb.append("</td></tr>");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}*/

	@Transactional(readOnly=false)
	public String getDistrictAssessmentSectionCampaignQuestions(DistrictAssessmentDetail districtAssessmentDetail,TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail)
	{
		//System.out.println("::::::::::::::::getAssessmentSectionCampaignQuestions::::::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		TeacherDetail teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		StringBuffer sb =new StringBuffer();
		try{

			try{
				System.out.print("DistrictAssesment:"+new Date()+", T:"+teacherDetail.getTeacherId() +", E:"+teacherDetail.getEmailAddress());
			}catch(Exception e){
				e.printStackTrace();
			}

			Criterion criterion = Restrictions.eq("teacherDistrictAssessmentDetail", teacherDistrictAssessmentDetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);

			int totalQuestions = teacherDistrictAssessmentQuestionDAO.getRowCount(criterion,criterion1);
			List<TeacherDistrictAssessmentOption> teacherDistrictAssessmentQuestionOptionsList = null;
			List<TeacherDistrictSectionDetail> teacherDistrictSectionDetailList = null;
			teacherDistrictSectionDetailList = teacherDistrictSectionDetailDAO.getSectionsByTeacherDistrictAssessmentId(teacherDistrictAssessmentDetail);
			if(teacherDistrictSectionDetailList.size()!=0)
			{
				int i =0;
				int questionCounter = 0;
				int sectionCounter=1;
				
				sb.append("<div class='panel-group' id='accordion'>");
				for (TeacherDistrictSectionDetail teacherDistrictSectionDetail : teacherDistrictSectionDetailList) 
				{
					System.out.println("getSectionName:: "+teacherDistrictSectionDetail.getSectionName());
					//added by amit
					sb.append("<input type='hidden' name='sectionId' id='section"+(sectionCounter)+"' value='"+teacherDistrictSectionDetail.getSectionName()+"'/>" );
					
					sb.append("<div class='panel panel-default'>");

					if(i==0)
					{
						sb.append("<div class='accordion-heading'> <a href='#id"+i+"' data-parent='#accordion' data-toggle='collapse' class='accordion-toggle minus'>"+teacherDistrictSectionDetail.getSectionName()+"</a> </div>");
						sb.append("<div class='accordion-body in' id='id"+i+"' style='height:auto;'>");
					}
					else
					{
						sb.append("<div class='accordion-heading'> <a href='#id"+i+"' data-parent='#accordion' data-toggle='collapse' class='accordion-toggle plus'>"+teacherDistrictSectionDetail.getSectionName()+"</a> </div>");
						sb.append("<div class='accordion-body collapse' id='id"+i+"' style='height:auto;'>");
					}
					i++;
					sb.append("<div class='accordion-inner'>");
					//sb.append("<div class='offset1'>");
					//sb.append("<div class='row'>");
					sb.append("<div class='row' style='margin-left: 12px;margin-right: 12px;margin-bottom: -23px;'>"+teacherDistrictSectionDetail.getSectionInstructions());

					List<TeacherDistrictAssessmentQuestion> teacherDistrictAssessmentQuestionList = new ArrayList<TeacherDistrictAssessmentQuestion>();
					teacherDistrictAssessmentQuestionList = teacherDistrictAssessmentQuestionDAO.getTeacherDistrictAssessmentQuestions(teacherDistrictAssessmentDetail,teacherDistrictSectionDetail);
					String questionInstruction = "";
					String shortName = "";

					sb.append("<table width='90%' border='0' class='table table-bordered table-striped'>");
					for (TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQuestion : teacherDistrictAssessmentQuestionList) 
					{
						teacherDistrictAssessmentDetail = teacherDistrictAssessmentQuestion.getTeacherDistrictAssessmentDetail();

						sb.append("<tr>");
						sb.append("<td width='90%'>");

						sb.append("<div style='text-align:right;'>"+Utility.genrateHTMLspace(198)+"<b>Question "+(++questionCounter)+" of "+totalQuestions+"</b></div>");

						questionInstruction = teacherDistrictAssessmentQuestion.getQuestionInstruction()==null?"":teacherDistrictAssessmentQuestion.getQuestionInstruction();
						if(!questionInstruction.equals(""))
							sb.append(""+Utility.getUTFToHTML(questionInstruction)+"<br/>");

						//added by amit
						sb.append("<div id='q"+questionCounter+"validate' style='text-align:left; color:red;' class='hide'><span><b>&#42;</b> Please answer this Question.</span></div>");
						
						sb.append(""+Utility.getUTFToHTML(teacherDistrictAssessmentQuestion.getQuestion())+"<br/>");

						shortName=teacherDistrictAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName();

						if(shortName.equalsIgnoreCase("itq"))
						{
							sb.append("<img src='showImage?image=teacher/"+teacherDetail.getTeacherId()+"/questions/"+teacherDistrictAssessmentQuestion.getQuestionImage()+"'><br>");
						}
						sb.append("<input type='hidden' name='q"+questionCounter+"o_maxMarks' value='"+(teacherDistrictAssessmentQuestion.getMaxMarks()==null?0:teacherDistrictAssessmentQuestion.getMaxMarks())+"'  />");

						teacherDistrictAssessmentQuestionOptionsList = teacherDistrictAssessmentQuestion.getTeacherDistrictAssessmentOptions();
						shortName=teacherDistrictAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName();

						if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel") || shortName.equalsIgnoreCase("lkts") || shortName.equalsIgnoreCase("itq"))
						{
							sb.append("<table >");
							for (TeacherDistrictAssessmentOption teacherDistrictQuestionOption : teacherDistrictAssessmentQuestionOptionsList) {
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='q"+questionCounter+"opt' value='"+teacherDistrictQuestionOption.getTeacherDistrictAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;padding: 5px 3px;' >"+teacherDistrictQuestionOption.getQuestionOption()+" ");
								sb.append("<input type='hidden' name='q"+questionCounter+"score' value='"+(teacherDistrictQuestionOption.getScore()==null?0:teacherDistrictQuestionOption.getScore())+"'  /></td></tr>");
							}
							sb.append("</table>");
						}
						if(shortName.equalsIgnoreCase("it"))
						{
							sb.append("<table >");
							for (TeacherDistrictAssessmentOption teacherDistrictQuestionOption : teacherDistrictAssessmentQuestionOptionsList) {
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='q"+questionCounter+"opt' value='"+teacherDistrictQuestionOption.getTeacherDistrictAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherDistrictQuestionOption.getQuestionOption()+"'> ");
								sb.append("<input type='hidden' name='q"+questionCounter+"score' value='"+(teacherDistrictQuestionOption.getScore()==null?0:teacherDistrictQuestionOption.getScore())+"'  /></td></tr>");
							}
							sb.append("</table>");
						}
						else if(shortName.equalsIgnoreCase("rt"))
						{
							int rank=1;
							sb.append("<table>");
							for (TeacherDistrictAssessmentOption teacherDistrictQuestionOption : teacherDistrictAssessmentQuestionOptionsList) {
								sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' id='rnk"+rank+"' name='q"+questionCounter+"rank' class='span1' maxlength='1' onkeypress='return checkForIntUpto6(event)' onblur='checkUniqueRank(this);'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherDistrictQuestionOption.getQuestionOption()+"");
								sb.append("<input type='hidden' name='q"+questionCounter+"score' value='"+(teacherDistrictQuestionOption.getScore()==null?0:teacherDistrictQuestionOption.getScore())+"' /><input type='hidden' name='opt' value='"+teacherDistrictQuestionOption.getTeacherDistrictAssessmentOptionId()+"' />");
								sb.append("<input type='hidden' name='q"+questionCounter+"o_rank' value='"+(teacherDistrictQuestionOption.getRank()==null?0:teacherDistrictQuestionOption.getRank())+"' />");
								sb.append("</td></tr>");
								rank++;
							}	
							sb.append("</table >");

						}else if(shortName.equalsIgnoreCase("sl"))
						{
							sb.append("&nbsp;&nbsp;<input type='text' name='q"+questionCounter+"opt' id='opt' class='span15' maxlength='75'/><br/>");
							for (TeacherDistrictAssessmentOption teacherDistrictQuestionOption : teacherDistrictAssessmentQuestionOptionsList) {
								sb.append("<input type='hidden' name='q"+questionCounter+"score' value='"+(teacherDistrictQuestionOption.getScore()==null?0:teacherDistrictQuestionOption.getScore())+"'  />");
							}
						}
						else if(shortName.equalsIgnoreCase("ml"))
						{
							sb.append("&nbsp;&nbsp;<textarea name='q"+questionCounter+"opt' id='opt' class='form-control' maxlength='5000' rows='15' style='width:98%;margin:5px;' /></textarea><br/>");
							for (TeacherDistrictAssessmentOption teacherDistrictQuestionOption : teacherDistrictAssessmentQuestionOptionsList) {
								sb.append("<input type='hidden' name='q"+questionCounter+"score' value='"+(teacherDistrictQuestionOption.getScore()==null?0:teacherDistrictQuestionOption.getScore())+"'  />");
							}
						}

						sb.append("<input type='hidden' name='teacherDistrictAssessmentQuestionId' id='q"+questionCounter+"teacherDistrictAssessmentQuestionId' value='"+teacherDistrictAssessmentQuestion.getTeacherDistrictAssessmentQuestionId()+"'/>" );
						sb.append("<input type='hidden' name='questionImage' id='q"+questionCounter+"questionImage' value='"+teacherDistrictAssessmentQuestion.getQuestionImage()+"'/>" );
						//sb.append("<input type='hidden' id='teacherDistrictSectionId' value='"+teacherDistrictSectionIDD+"'/>" );
						sb.append("<input type='hidden' name='questionWeightage' id='q"+questionCounter+"questionWeightage' value='"+(teacherDistrictAssessmentQuestion.getQuestionWeightage()==null?0:teacherDistrictAssessmentQuestion.getQuestionWeightage())+"'/>" );
						sb.append("<input type='hidden' name='questionTypeId' id='q"+questionCounter+"questionTypeId' value='"+teacherDistrictAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
						sb.append("<input type='hidden' name='questionTypeShortName' id='q"+questionCounter+"questionTypeShortName' value='"+teacherDistrictAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName()+"'/>" );
						sb.append("<input type='hidden' name='questionSection' id='q"+questionCounter+"section' value='"+sectionCounter+"'/>" );
						sb.append("</td>");
						sb.append("</tr>");

					}
					sb.append("</table>");

					sb.append("</div>");
					sb.append("</div>");
					sb.append("</div>");
					sb.append("</div>");
					sectionCounter++;

				}
				sb.append("</div>");
			}

			//System.out.println("ggggggggggggggg: "+sb.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment Section Questions.
	 */
	@Transactional(readOnly=false)
	public String finalizeDistrictAssessmentQuestions(DistrictAssessmentDetail districtAssessmentDetail,TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail,TeacherDistrictAssessmentAnswerDetail[] teacherDistrictAssessmentAnswerDetails,Integer attemptId)
	{
		//System.out.println("::::::::::::::::getAssessmentSectionCampaignQuestions::::::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		TeacherDetail teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		StringBuffer sb =new StringBuffer();
		try{


			try{
				System.out.print("DistrictAssesment:"+new Date()+", T:"+teacherDetail.getTeacherId() +", E:"+teacherDetail.getEmailAddress());
			}catch(Exception e){
				e.printStackTrace();
			}
			Criterion criterion = Restrictions.eq("teacherDistrictAssessmentDetail", teacherDistrictAssessmentDetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);

			int totalQuestions = teacherDistrictAssessmentQuestionDAO.getRowCount(criterion,criterion1);
			
			if(attemptId!=0)
			{
				teacherDistrictAssessmentAttemptDAO.updateDistrictAssessmentAttemptSessionTime(attemptId,false);
			}
			List<Long> teacherDistrictAssessmentQuestionIdIds = new ArrayList<Long>();
			
			Double marksObtained = 0.0;
			Double totalMarks = 0.0;

			TeacherDistrictAssessmentAnswerDetail teacherDistrictAssessmentAnswerDetail;
			int answerCount = teacherDistrictAssessmentAnswerDetails.length;
			
			 SessionFactory sessionFactory=teacherDetailDAO.getSessionFactory();
			 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
        	 Transaction txOpen =statelesSsession.beginTransaction();
        	 
			if(teacherDistrictAssessmentAnswerDetails!=null && answerCount>0)
			{
				for(int i=0;i<answerCount;i++)
				{
					teacherDistrictAssessmentAnswerDetail = teacherDistrictAssessmentAnswerDetails[i];
					teacherDistrictAssessmentAnswerDetail.setDistrictAssessmentDetail(districtAssessmentDetail);
					teacherDistrictAssessmentAnswerDetail.setTeacherDistrictAssessmentDetail(teacherDistrictAssessmentDetail);
					teacherDistrictAssessmentAnswerDetail.setTeacherDistrictAssessmentQuestion(teacherDistrictAssessmentAnswerDetail.getTeacherDistrictAssessmentQuestion());
					teacherDistrictAssessmentAnswerDetail.setCreatedDateTime(new Date());
					teacherDistrictAssessmentAnswerDetail.setTeacherDetail(teacherDetail);
					
					/*System.out.println("teacherDistrictAssessmentAnswerDetail="+teacherDistrictAssessmentAnswerDetail.getTeacherDistrictAssessmentQuestion().getTeacherDistrictAssessmentQuestionId());
					System.out.println("teacherDistrictAssessmentAnswerDetail.getQuestionWeightage()="+teacherDistrictAssessmentAnswerDetail.getQuestionWeightage());
					System.out.println("teacherDistrictAssessmentAnswerDetail.getQuestionTypeMaster().getQuestionTypeId()="+teacherDistrictAssessmentAnswerDetail.getQuestionTypeMaster().getQuestionTypeId());
					System.out.println("teacherDistrictAssessmentAnswerDetail.getInsertedText()="+teacherDistrictAssessmentAnswerDetail.getInsertedText());
					System.out.println("teacherDistrictAssessmentAnswerDetail.getOptionScore()="+teacherDistrictAssessmentAnswerDetail.getOptionScore());
					System.out.println("teacherDistrictAssessmentAnswerDetail.getSelectedOptions()="+teacherDistrictAssessmentAnswerDetail.getSelectedOptions());*/
					System.out.println("teacherDistrictAssessmentAnswerDetail.getMaxMarks()="+teacherDistrictAssessmentAnswerDetail.getMaxMarks());
					System.out.println("teacherDistrictAssessmentAnswerDetail.getTotalScore()="+teacherDistrictAssessmentAnswerDetail.getTotalScore());
					if(teacherDistrictAssessmentAnswerDetail.getTotalScore()!=null)
						marksObtained += teacherDistrictAssessmentAnswerDetail.getTotalScore();
					if(teacherDistrictAssessmentAnswerDetail.getMaxMarks()!=null)
						totalMarks += teacherDistrictAssessmentAnswerDetail.getMaxMarks()==null?0:teacherDistrictAssessmentAnswerDetail.getMaxMarks();
					
					if(teacherDistrictAssessmentAnswerDetail.getQuestionWeightage()!= null && teacherDistrictAssessmentAnswerDetail.getQuestionWeightage()==0)
						teacherDistrictAssessmentAnswerDetail.setTotalScore(null);
					System.out.println("q"+i+" ==============================================================================");
					
					//teacherDistrictAssessmentAnswerDetailDAO.makePersistent(teacherDistrictAssessmentAnswerDetail);
					statelesSsession.insert(teacherDistrictAssessmentAnswerDetail);
					
					try {
						teacherDistrictAssessmentQuestionIdIds.add(teacherDistrictAssessmentAnswerDetail.getTeacherDistrictAssessmentQuestion().getTeacherDistrictAssessmentQuestionId());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				txOpen.commit();
		       	statelesSsession.close();
		       	
				System.out.println("marksObtained:: "+marksObtained+" totalMarks:: "+totalMarks);
				
				System.out.println("LLLLL  "+teacherDistrictAssessmentDetail.getTeacherDistrictAssessmentId());
				System.out.println("LLLLL  "+teacherDetail.getTeacherId());
				try {
					List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList = teacherDistrictAssessmentStatusDAO.findDistrictAssessmentStatusByTeacher(teacherDetail,teacherDistrictAssessmentDetail);
					if(teacherDistrictAssessmentStatusList!=null)
					{
						TeacherDistrictAssessmentStatus teacherDistrictAssessmentStatus= teacherDistrictAssessmentStatusList.get(0);
						teacherDistrictAssessmentStatus.setMarksObtained(marksObtained);
						teacherDistrictAssessmentStatus.setTotalMarks(totalMarks);
						teacherDistrictAssessmentStatus.setUpdatedDate(new Date());
						teacherDistrictAssessmentStatusDAO.makePersistent(teacherDistrictAssessmentStatus);
						if(totalQuestions==answerCount)
						{
							teacherDistrictAssessmentStatus.setStatusMaster(WorkThreadServlet.statusMap.get("comp"));
							teacherDistrictAssessmentDetail = teacherDistrictAssessmentDetailDAO.findById(teacherDistrictAssessmentDetail.getTeacherDistrictAssessmentId(), false, false);
							teacherDistrictAssessmentDetail.setIsDone(true);
							teacherDistrictAssessmentDetailDAO.makePersistent(teacherDistrictAssessmentDetail);
							try{
								teacherDistrictAssessmentStatusDAO.updatefitScore(teacherDetail, districtAssessmentDetail, marksObtained, totalMarks,0);
							}catch(Exception e){
								e.printStackTrace();
							}
							sb.append("1");
						}
						else
						{
							teacherDistrictAssessmentStatus.setStatusMaster(WorkThreadServlet.statusMap.get("vlt"));
							sb.append("0");
						}
					}
					if(teacherDistrictAssessmentQuestionIdIds.size()>0)
						teacherDistrictAssessmentQuestionDAO.updateQuestionDone(teacherDistrictAssessmentQuestionIdIds);
				} catch (Exception e) {
					e.printStackTrace();
				}
				

			}


		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	@Transactional(readOnly=false)
	public int updateAttempt(Integer attemptId)
	{
		//System.out.println(":::::::::::::::Assessment updateAttempt:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			if(attemptId!=0)
			{
				teacherDistrictAssessmentAttemptDAO.updateDistrictAssessmentAttemptSessionTime(attemptId,false);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

		return 0;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to insert TeacherAssessmentAttempt.
	 */
	@Transactional(readOnly=false)
	public int insertTeacherDistrictAssessmentAttempt(DistrictAssessmentDetail districtAssessmentDetail,JobOrder jobOrder,TeacherDetail teacherDetail)
	{
		//System.out.println(":::::::::::::::Assessment insertTeacherAssessmentAttempt:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
			String ipAddress = IPAddressUtility.getIpAddress(request);

			TeacherDistrictAssessmentAttempt teacherDistrictAssessmentAttempt = new TeacherDistrictAssessmentAttempt();
			teacherDistrictAssessmentAttempt.setDistrictAssessmentDetail(districtAssessmentDetail);
			teacherDistrictAssessmentAttempt.setIpAddress(ipAddress);
			if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0)
				teacherDistrictAssessmentAttempt.setJobOrder(null);
			else
				teacherDistrictAssessmentAttempt.setJobOrder(jobOrder);

			teacherDistrictAssessmentAttempt.setTeacherDetail(teacherDetail);
			teacherDistrictAssessmentAttempt.setDistrictAssessmentSessionTime(0);
			teacherDistrictAssessmentAttempt.setDistrictAssessmentStartTime(new Date());
			teacherDistrictAssessmentAttempt.setIsForced(false);
			teacherDistrictAssessmentAttemptDAO.makePersistent(teacherDistrictAssessmentAttempt);
			return 1;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to set TeacherAssessmentAttempt forced quit.
	 */
	@Transactional(readOnly=false)
	public int setDistrictAssessmentForced(int attemptId)
	{
		//System.out.println(":::::::::::::::Assessment setAssessmentForced:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			if(attemptId!=0)
			{
				teacherDistrictAssessmentAttemptDAO.updateDistrictAssessmentAttemptSessionTime(attemptId,true);
			}
			return 1;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	/* @Author: Vishwanath Kumar
	 * @Discription: .
	 */
	public String insertDistrictLastAttempt(JobOrder jobOrder,TeacherDetail teacherDetail,DistrictAssessmentDetail districtAssessmentDetail)
	{
		//System.out.println(":::::::::::::::Assessment insertLastAttempt:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

		List<TeacherDistrictAssessmentStatus> teacherDistrictAssessmentStatusList = null;
		TeacherDistrictAssessmentStatus teacherDistrictAssessmentStatus = null;
		teacherDistrictAssessmentStatusList=teacherDistrictAssessmentStatusDAO.findDistrictAssessmentTakenByTeacher(teacherDetail,districtAssessmentDetail);
		TeacherDistrictAssessmentDetail teacherDistrictAssessmentdetail = null;
		if(teacherDistrictAssessmentStatusList.size()!=0)
		{
			teacherDistrictAssessmentStatus = teacherDistrictAssessmentStatusList.get(0);
		}

		List<TeacherDistrictAssessmentAttempt> teacherDistrictAssessmentAttempts = null;
		//teacherDistrictAssessmentAttempts = teacherDistrictAssessmentAttemptDAO.findDistrictAssessmentAttempts(teacherDetail,jobOrder);
		teacherDistrictAssessmentAttempts = teacherDistrictAssessmentAttemptDAO.findNoOfAttempts(districtAssessmentDetail,teacherDetail);

		int attempts = 0;
		for (TeacherDistrictAssessmentAttempt teacherDistrictAssessmentAttempt1 : teacherDistrictAssessmentAttempts) {
			if(!teacherDistrictAssessmentAttempt1.getIsForced())
				attempts++;
		}
		////////////////////////

		/******** Sekhar add for count strikeLogs ( Start )*******/
		int strikeLogs=0; 
		try{
			if(teacherDistrictAssessmentAttempts.size()>0)
			{
				List<TeacherDistrictAssessmentDetail> teacherDistrictAssessmentdetails = null;
				teacherDistrictAssessmentdetails=teacherDistrictAssessmentDetailDAO.getTeacherDistrictAssessmentdetails(teacherDistrictAssessmentAttempts.get(0).getDistrictAssessmentDetail(), teacherDetail);
				List<TeacherDistrictStrikeLog> teacherDistrictStrikeLog = null;
				teacherDistrictStrikeLog = teacherDistrictStrikeLogDAO.getTotalStrikesByTeacher(teacherDetail, teacherDistrictAssessmentdetails.get(0));
				strikeLogs=teacherDistrictStrikeLog.size();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		int attemptsStrike=attempts+strikeLogs;
		/******** Sekhar add for count strikeLogs ( End )*******/

		if(attemptsStrike==3)
		{
			StatusMaster statusMaster = WorkThreadServlet.statusMap.get("vlt");
			teacherDistrictAssessmentdetail = teacherDistrictAssessmentStatus.getTeacherDistrictAssessmentDetail();

			List<TeacherDistrictAssessmentStatus> lstTeacherDistrictAssessmentStatus = null;
			lstTeacherDistrictAssessmentStatus =teacherDistrictAssessmentStatusDAO.findDistrictAssessmentStatusByTeacherAssessmentdetail(teacherDistrictAssessmentdetail);
			teacherDistrictAssessmentStatus=lstTeacherDistrictAssessmentStatus.get(0);

			List<JobOrder> jobs = new ArrayList<JobOrder>();

			if(lstTeacherDistrictAssessmentStatus.size()!=0)
				if(teacherDistrictAssessmentStatus!=null)
				{
					for(TeacherDistrictAssessmentStatus tas: lstTeacherDistrictAssessmentStatus)
					{
						tas.setStatusMaster(statusMaster);
						teacherDistrictAssessmentStatusDAO.makePersistent(tas);
					}
				}
		}

		return ""+attempts;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to finish assessment whether completed or not.
	 */
	@Transactional(readOnly=false)
	public String finishDistrictAssessment(TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail,JobOrder jobOrder,int newJobId,int sessionCheck,TeacherDetail teacherDetail)
	{

		//System.out.println(":::::::::::::::Assessment finishAssessment:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{

			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			////////////////////////////////////////////////////////////////////////////////
			StringBuffer sb =new StringBuffer();
			int jId=0;
			List<TeacherDistrictAssessmentQuestion> teacherDistrictAssessmentQuestions =null;
			Criterion criterion = Restrictions.eq("teacherDistrictAssessmentDetail", teacherDistrictAssessmentDetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("isAttempted", false);
			teacherDistrictAssessmentQuestions = teacherDistrictAssessmentQuestionDAO.findByCriteria(criterion,criterion1,criterion2);
			int unattemptedQuestions = teacherDistrictAssessmentQuestions.size();

			// TeacherAssessmentStatus update after completion
			List<TeacherDistrictAssessmentStatus> lstTeacherDistrictAssessmentStatus = null;

			lstTeacherDistrictAssessmentStatus = teacherDistrictAssessmentStatusDAO.findDistrictAssessmentStatusByTeacherAssessmentdetail(teacherDistrictAssessmentDetail);
			TeacherDistrictAssessmentStatus teacherDistrictAssessmentStatus=lstTeacherDistrictAssessmentStatus.get(0);


			if(lstTeacherDistrictAssessmentStatus.size()!=0)
				if(teacherDistrictAssessmentStatus!=null)
				{
					StatusMaster statusMaster = null;
					String appendMsg="not ";
					String status="";
					if(unattemptedQuestions==0)
					{
						status="comp";
						appendMsg="";
					}
					else
						status="vlt";

					statusMaster = WorkThreadServlet.statusMap.get(status);
					Double marksObtained = 0.0;
					List<JobOrder> jobs = new ArrayList<JobOrder>();
					for(TeacherDistrictAssessmentStatus tas: lstTeacherDistrictAssessmentStatus)
					{
						tas.setStatusMaster(statusMaster);
						teacherDistrictAssessmentStatusDAO.makePersistent(tas);
						marksObtained = tas.getMarksObtained();
					}

					boolean onDashboard = false;
					List<JobForTeacher> lstJobForTeacherIcomp = new ArrayList<JobForTeacher>();
					List<JobOrder> lstJBOrder = new ArrayList<JobOrder>();
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("window.onbeforeunload = function() {};");

					jobOrder=teacherDistrictAssessmentStatus.getJobOrder();


					String exitMessage = null;
					String exitURL = null;
					if(jobOrder!=null)
					{
						jId=jobOrder.getJobId();

						if(newJobId>0)
							jobOrder = jobOrderDAO.findById(newJobId, false, false); 

						int flagForURL = jobOrder.getFlagForURL();
						int flagForMessage  = jobOrder.getFlagForMessage();
						exitMessage = jobOrder.getExitMessage();
						exitURL = jobOrder.getExitURL();
						if(exitMessage!=null && !exitMessage.trim().equals("") && exitURL!=null && !exitURL.trim().equals(""))
						{
							onDashboard = true;
						}else if(flagForMessage==1)
						{
							onDashboard = false;
							boolean isVlt = false;
							if(status.equalsIgnoreCase("vlt"))
							{
								String msg1="";
								if(sessionCheck==1)
									msg1=Utility.getLocaleValuePropByKey("msgDistAssCamp1", locale)+" <br><br><b>"+Utility.getLocaleValuePropByKey("msgMarksObtained", locale)+": "+marksObtained+"</b>";
								else
									msg1=Utility.getLocaleValuePropByKey("msgDistAssCamp2", locale)+"<br><br><b>"+Utility.getLocaleValuePropByKey("msgMarksObtained", locale)+": "+marksObtained+"</b>";
								sb.append("document.getElementById('mainDiv').style.display='none';");
								sb.append("showInfoAndRedirect(2,'"+msg1+"','','myassessments.do?jobId="+jobOrder.getJobId()+"',"+2+");");
								isVlt = true;
							}
							if(!isVlt)
								sb.append("window.location.href='myassessments.do?jobId="+jobOrder.getJobId()+"'");

						}else if(flagForURL==1)
						{
							sb.append("document.getElementById('mainDiv').style.display='none';");
							String m="";
							if(appendMsg.equals(""))
								m="y";
							else
								m="n";
							sb.append("window.location.href='myassessments.do?jobId="+jobOrder.getJobId()+"&mf="+m+"';");

						}else
							onDashboard = true;
					}


					if(onDashboard)
					{

						String redirectURL = "myassessments.do";
						sb.append("document.getElementById('mainDiv').style.display='none';");
						String msg="";
						if(sessionCheck==1)
							msg=Utility.getLocaleValuePropByKey("msgDistAssCamp1", locale)+" <br><br><b>"+Utility.getLocaleValuePropByKey("msgMarksObtained", locale)+": "+marksObtained+"</b>";
						else
							msg=Utility.getLocaleValuePropByKey("msgDistAssCamp2", locale)+" <br><br><b>"+Utility.getLocaleValuePropByKey("msgMarksObtained", locale)+": "+marksObtained+"</b>";

						int sts = 2;
						if(jId>0)
						{
							if(appendMsg.equals(""))
							{
								msg= Utility.getLocaleValuePropByKey("msgDistAssCamp3", locale)+" <br><br><b>"+Utility.getLocaleValuePropByKey("msgMarksObtained", locale)+": "+marksObtained+"</b>";
								sts = 0;
							}
							else
								msg= Utility.getLocaleValuePropByKey("msgDistAssCamp4", locale)+" <br><br><b>"+Utility.getLocaleValuePropByKey("msgMarksObtained", locale)+": "+marksObtained+"</b>";

							redirectURL="myassessments.do";
						}

						sb.append("showInfoAndRedirect("+sts+",'"+msg+"','','"+redirectURL+"',"+2+");");
					}
					sb.append("</script>");

					teacherDistrictAssessmentDetail = teacherDistrictAssessmentDetailDAO.findById(teacherDistrictAssessmentDetail.getTeacherDistrictAssessmentId(), false, false);
					teacherDistrictAssessmentDetail.setIsDone(true);

					teacherDistrictAssessmentDetailDAO.makePersistent(teacherDistrictAssessmentDetail);
				}

			////////////////////////////////////////////////////////////////////////////////

			return sb.toString();
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to insert Teacher Assessment Strike.
	 */
	@Transactional(readOnly=false)
	public int saveDistrictStrike(TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail,TeacherDistrictAssessmentQuestion teacherDistrictAssessmentQuestion,TeacherDetail teacherDetail)
	{

		//System.out.println(":::::::::::::::Assessment saveStrike:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			String ipAddress = IPAddressUtility.getIpAddress(request);
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
			// teacherassessmentattempt
			TeacherDistrictStrikeLog teacherDistrictStrikeLog = new TeacherDistrictStrikeLog();
			teacherDistrictStrikeLog.setTeacherDetail(teacherDetail);
			teacherDistrictStrikeLog.setTeacherDistrictAssessmentdetail(teacherDistrictAssessmentDetail);
			teacherDistrictStrikeLog.setTeacherDistrictAssessmentQuestion(teacherDistrictAssessmentQuestion);
			teacherDistrictStrikeLog.setIpAddress(ipAddress);
			teacherDistrictStrikeLog.setCreatedDateTime(new Date());

			teacherDistrictStrikeLogDAO.makePersistent(teacherDistrictStrikeLog);

			return 1;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to checkAssessmentDone.
	 */
	@Transactional(readOnly=false)
	public int checkDistrictAssessmentDone(TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail,TeacherDetail teacherDetail)
	{
		System.out.println(":::::::::::::::Assessment checkAssessmentDone:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
			teacherDistrictAssessmentDetail = teacherDistrictAssessmentDetailDAO.findById(teacherDistrictAssessmentDetail.getTeacherDistrictAssessmentId(), false, false);

			if(teacherDistrictAssessmentDetail.getIsDone())
				return 1;
			else
				return 0;

		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}






}
