package tm.services;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobCategoryTransaction;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentGroupDetails;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.OnboardingDashboard;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.ReferenceCheckQuestionSet;
import tm.bean.master.ReferenceCheckQuestionSetTemp;
import tm.bean.master.ReferenceQuestionSets;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobCategoryTransactionDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentGroupDetailsDAO;
import tm.dao.districtassessment.DistrictAssessmentDetailDAO;
import tm.dao.i4.I4QuestionSetsDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DspqRouterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.QqQuestionSetsDAO;
import tm.dao.master.ReferenceCheckQuestionSetDAO;
import tm.dao.master.ReferenceCheckQuestionSetTempDAO;
import tm.dao.master.ReferenceQuestionSetsDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.onboardingcheckpoint.OnboardingDashboardDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class JobCategoryAjaxNew
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	String 	lblJobCategoryName=Utility.getLocaleValuePropByKey("lblJobCategoryName", locale);
	String 	optDistrict=Utility.getLocaleValuePropByKey("optDistrict", locale);
	String 	lblEPIReq=Utility.getLocaleValuePropByKey("lblEPIReq", locale);
	String 	lblFullTimeTeachers=Utility.getLocaleValuePropByKey("lblFullTimeTeachers", locale);
	String 	lblJSIAttachment=Utility.getLocaleValuePropByKey("lblJSIAttachment", locale);
	String 	lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	String 	lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	String 	lblNoJobCategoryfound=Utility.getLocaleValuePropByKey("lblNoJobCategoryfound", locale);
	String 	toolClickHereSeeAssedoc=Utility.getLocaleValuePropByKey("toolClickHereSeeAssedoc", locale);
	String 	optAct=Utility.getLocaleValuePropByKey("optAct", locale);
	String 	lblInActiv=Utility.getLocaleValuePropByKey("lblInActiv", locale);
	String 	lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	String 	lblActivate=Utility.getLocaleValuePropByKey("lblActivate", locale);
	String 	lblDeactivate=Utility.getLocaleValuePropByKey("lblDeactivate", locale);
	String 	lblPleaseSelectQQSet=Utility.getLocaleValuePropByKey("lblPleaseSelectQQSet", locale);
	String 	optStrJobCat=Utility.getLocaleValuePropByKey("optStrJobCat", locale);
	String 	instD=Utility.getLocaleValuePropByKey("instD", locale);
	String 	lblEPI=Utility.getLocaleValuePropByKey("lblEPI", locale);
	String 	lblPortfolio=Utility.getLocaleValuePropByKey("lblPortfolio", locale);
	String 	DSPQlblPreScreen=Utility.getLocaleValuePropByKey("DSPQlblPreScreen", locale);
	String 	msgOnBoarding=Utility.getLocaleValuePropByKey("msgOnBoarding", locale);
	String 	DSPQlblQuestions=Utility.getLocaleValuePropByKey("DSPQlblQuestions", locale);
	String 	lblMandatory=Utility.getLocaleValuePropByKey("lblMandatory", locale);
	String 	lblOptional=Utility.getLocaleValuePropByKey("lblOptional", locale);
	String 	lblNotRequired=Utility.getLocaleValuePropByKey("lblNotRequired", locale);
	String 	optInActiv=Utility.getLocaleValuePropByKey("optInActiv", locale);
	String 	lblClone=Utility.getLocaleValuePropByKey("lblClone", locale);
	String 	listPleaseSelectQuestionSet=Utility.getLocaleValuePropByKey("listPleaseSelectQuestionSet", locale);
	String 	msgPleaseSelectaSet=Utility.getLocaleValuePropByKey("msgPleaseSelectaSet", locale);
	String 	msgSelectPortfolioName=Utility.getLocaleValuePropByKey("msgSelectPortfolioName", locale);
	String 	lblPleaseSelectaEPIGroup=Utility.getLocaleValuePropByKey("lblPleaseSelectaEPIGroup", locale);
	String 	msgActiveUser=Utility.getLocaleValuePropByKey("msgActiveUser", locale);
	String 	msgInactiveUser=Utility.getLocaleValuePropByKey("msgInactiveUser", locale);
	String 	lblExternal=Utility.getLocaleValuePropByKey("lblExternal", locale);
	String 	lblInternal1=Utility.getLocaleValuePropByKey("lblInternal1", locale);
	String 	lblITransfer=Utility.getLocaleValuePropByKey("lblITransfer", locale);
	String 	msgIITransfer=Utility.getLocaleValuePropByKey("msgIITransfer", locale);
	String 	lblCandidateType=Utility.getLocaleValuePropByKey("lblCandidateType", locale);
	String 	lblYes=Utility.getLocaleValuePropByKey("lblYes", locale);
	String 	lblNo=Utility.getLocaleValuePropByKey("lblNo", locale);
	String 	msgSelectOnBoarding=Utility.getLocaleValuePropByKey("msgSelectOnBoarding", locale);
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(
			JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	@Autowired 
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}	
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private I4QuestionSetsDAO i4QuestionSetsDAO;

	@Autowired
	private DistrictAssessmentDetailDAO districtAssessmentDetailDAO;
	
	
	@Autowired
	private ReferenceCheckQuestionSetDAO referenceCheckQuestionSetDAO;
	
	@Autowired
	private ReferenceQuestionSetsDAO referenceQuestionSetsDAO;
	
	@Autowired
	private QqQuestionSetsDAO qqQuestionSetsDAO;
	
	@Autowired
	private JobCategoryTransactionDAO jobCategoryTransactionDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private AssessmentGroupDetailsDAO assessmentGroupDetailDAO;
	
	@Autowired 
	private ReferenceCheckQuestionSetTempDAO referenceCheckQuestionSetTempDAO;
	
	@Autowired
	private DspqRouterDAO dspqRouterDAO;
	
	@Autowired
	private OnboardingDashboardDAO onboardingDashboardDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	public String displayJobCategories(Boolean result,Integer searchEntityType,Integer entityType,Integer searchId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer dmRecords =	new StringBuffer();
		Map<String,JobCategoryTransaction>	jobCatMap = new TreeMap<String,JobCategoryTransaction>();
		try{
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------

			UserMaster userMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}

			List<JobCategoryMaster> jobCategoryMasters	  	=	null;
			List<JobCategoryMaster> jobCategoryMastersAll	  	=	null;
			String sortOrderFieldName	=	"jobCategoryName";
			String sortOrderNoField		=	"jobCategoryName";

			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("districtMaster")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("districtMaster"))
				{
					sortOrderNoField="districtMaster";
				}
			}

			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			List<JobCategoryMaster>sortedJobCatList=new ArrayList<JobCategoryMaster>();
			List<JobCategoryTransaction>jobCatTransList=new ArrayList<JobCategoryTransaction>();
			Criterion criterion=null; 
			DistrictMaster districtMaster=null;
			if(userMaster.getEntityType()==2){
				districtMaster=userMaster.getDistrictId();
				criterion=Restrictions.eq("districtMaster", districtMaster);
				jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion);
				jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion);
				if(jobCategoryMastersAll!=null && jobCategoryMastersAll.size()>0)
					jobCatTransList=jobCategoryTransactionDAO.findListByjobCategoryId(jobCategoryMastersAll);
				totalRecord=jobCategoryMastersAll.size();
			}else{
				if(result){
					if(searchId!=0 || searchId!=null){
						districtMaster=districtMasterDAO.findById(searchId, false, false);
						criterion=Restrictions.eq("districtMaster", districtMaster);	
						jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion);
					}else{
						criterion=Restrictions.isNotNull("districtMaster");
						jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion);
					}
					jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion);
					if(jobCategoryMastersAll!=null && jobCategoryMastersAll.size()>0)
						jobCatTransList=jobCategoryTransactionDAO.findListByjobCategoryId(jobCategoryMastersAll);
					totalRecord=jobCategoryMastersAll.size();
				}else{
					if(searchEntityType==2){
						districtMaster=districtMasterDAO.findById(searchId, false, false);
						session.setAttribute("districtMasterId", districtMaster.getDistrictId());
						session.setAttribute("districtMasterName", districtMaster.getDistrictName());
						criterion=Restrictions.eq("districtMaster", districtMaster);
						jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage,criterion);
						jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal,criterion);
						if(jobCategoryMastersAll!=null && jobCategoryMastersAll.size()>0)
							jobCatTransList=jobCategoryTransactionDAO.findListByjobCategoryId(jobCategoryMastersAll);
					}else{
						session.setAttribute("districtMasterId", 0);
						session.setAttribute("districtMasterName", "");
						jobCategoryMasters =jobCategoryMasterDAO.findJobCatList(sortOrderStrVal, start, noOfRowInPage);
						jobCategoryMastersAll=jobCategoryMasterDAO.findByCriteria(sortOrderStrVal);
						if(jobCategoryMastersAll!=null && jobCategoryMastersAll.size()>0)
							jobCatTransList=jobCategoryTransactionDAO.findListByjobCategoryId(jobCategoryMastersAll);
					}	
					totalRecord=jobCategoryMastersAll.size();
				}
			}
			SortedMap<String,JobCategoryMaster>	sortedMap = new TreeMap<String,JobCategoryMaster>();
			if(sortOrderNoField.equals("districtMaster"))
			{
				sortOrderFieldName	=	"districtMaster";
			}

			for(int i=0; i<jobCatTransList.size();i++)
			{
				jobCatMap.put(jobCatTransList.get(i).getJobCategoryId()+"#"+jobCatTransList.get(i).getCandidateType().trim(), jobCatTransList.get(i));
			}
			int mapFlag=2;
			for (JobCategoryMaster jbc : jobCategoryMastersAll){
				String orderFieldName=null;
				if(jbc.getDistrictMaster()!=null){
					orderFieldName=jbc.getDistrictMaster().getDistrictName();
					if(sortOrderFieldName.equals("districtMaster")){
						orderFieldName=jbc.getDistrictMaster().getDistrictName()+"||"+jbc.getJobCategoryId();
						sortedMap.put(orderFieldName+"||",jbc);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}else{
					if(sortOrderFieldName.equals("districtMaster")){
						orderFieldName="0||"+jbc.getJobCategoryId();
						sortedMap.put(orderFieldName+"||",jbc);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedJobCatList.add((JobCategoryMaster) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedJobCatList.add((JobCategoryMaster) sortedMap.get(key));
				}
			}else{
				sortedJobCatList=jobCategoryMasters;
			}

			List<JobCategoryMaster> sortedJBCList=new ArrayList<JobCategoryMaster>();

			if(totalRecord<end)
				end=totalRecord;

			if(mapFlag!=2)
				sortedJBCList=sortedJobCatList.subList(start,end);
			else
				sortedJBCList=sortedJobCatList;

			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink(lblJobCategoryName,sortOrderFieldName,"jobCategoryName",sortOrderTypeVal,pgNo);
			responseText=PaginationAndSorting.responseSortingLink(optDistrict,sortOrderFieldName,"districtMaster",sortOrderTypeVal,pgNo);
			if(sortedJobCatList.size()==0){
				dmRecords.append("<div class='row' style='padding:7px 0px 1px 5px;'>");
			dmRecords.append("<div class='col-sm-8 col-md-8 left5'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</div>");
			dmRecords.append("</div>");
			}else{
				String candidateType,epiValue,portfolioValue,prescreenValue,prescreenOnBoardingValue,customQuestion,manageTop;
				//int count;
				int count=0,colorCounter=0;
				String colorName="";
			dmRecords.append("<div class='row' style='padding:7px 0px 1px 5px;'>");
			for (JobCategoryMaster jc : sortedJBCList) 
			{
				if(colorCounter==0){
					colorName="white";
					colorCounter=1;
				}else{
					colorName="#dff0d8";
					colorCounter=0;
				}
				
				dmRecords.append("<div id='divId"+(count++)+"' class='row left5' style='width: 97.7%;background-color:"+colorName+";padding: 2px 0px 0px 5px;'  onmouseover='onHoverHighLight(this);' onmouseout='onOutHighLight(this);'>");
				 candidateType=epiValue=portfolioValue=prescreenValue=prescreenOnBoardingValue=customQuestion="";
				 manageTop="top0";
				 //count=0;
				if(jobCatMap.containsKey(jc.getJobCategoryId()+"#E"))
				{
				if(jobCatMap.get(jc.getJobCategoryId()+"#E")!=null && jobCatMap.get(jc.getJobCategoryId()+"#E").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#E").getEpiGroupStatus().equals("A")){
						epiValue+="<label class='mb0'> "+lblYes+"</label><br>";
						 candidateType+="<label title='"+lblExternal+"'>"+lblExternal+"</label><br>";
						 //manageTop="top0";
					}
					else{
						epiValue+="<label class='mb0'>"+lblNo+"</label><br>";
						 candidateType+="<label title='"+lblExternal+"'>"+lblExternal+"</label><br>";
						 //manageTop="top0";
					}
				}
				if(jobCatMap.get(jc.getJobCategoryId()+"#I")!=null && jobCatMap.get(jc.getJobCategoryId()+"#I").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#I").getEpiGroupStatus().equals("A")){
						epiValue+="<label class='mb0'> "+lblYes+"</label><br>";
						 candidateType+="<label title='"+lblInternal1+"'>"+lblInternal1+"</label><br>";
						 /*manageTop="top10";
						 count=1;*/
					}
					else{
						epiValue+="<label class='mb0'> "+lblNo+"</label><br>";
						 candidateType+="<label title='"+lblInternal1+"'>"+lblInternal1+"</label><br>";
						 /*manageTop="top10";
						 count=1;*/
					}
				}
				if(jobCatMap.get(jc.getJobCategoryId()+"#T")!=null && jobCatMap.get(jc.getJobCategoryId()+"#T").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#T").getEpiGroupStatus().equals("A")){
						epiValue+="<label> "+lblYes+"</label>";
						 candidateType+="<label title='"+lblITransfer+"'>"+msgIITransfer+"</label>";
						 /*if(count==0)
							 manageTop="top10";
						 else
							 manageTop="top20";*/
				
					}
					else{
						epiValue+="<label> "+lblNo+"</label>";
						 candidateType+="<label title='"+lblITransfer+"'>"+msgIITransfer+"</label>";
						 /*if(count==0)
							 manageTop="top10";
						 else
							 manageTop="top20";*/
					}
				}
					/****************** portfolio ********************************/
				if(jobCatMap.get(jc.getJobCategoryId()+"#E")!=null && jobCatMap.get(jc.getJobCategoryId()+"#E").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#E").getPortfolioStatus().equals("A"))
						portfolioValue+="<label class='mb0'> "+lblYes+"</label><br>";
					else
						portfolioValue+="<label class='mb0'>"+lblNo+"</label><br>";
				}
				if(jobCatMap.get(jc.getJobCategoryId()+"#I")!=null && jobCatMap.get(jc.getJobCategoryId()+"#I").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#I").getPortfolioStatus().equals("A"))
						portfolioValue+="<label class='mb0'> "+lblYes+"</label><br>";
					else
						portfolioValue+="<label class='mb0'>"+lblNo+"</label><br>";
				}
				if(jobCatMap.get(jc.getJobCategoryId()+"#T")!=null && jobCatMap.get(jc.getJobCategoryId()+"#T").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#T").getPortfolioStatus().equals("A"))
						portfolioValue+="<label> "+lblYes+"</label>";
					else
						portfolioValue+="<label>"+lblNo+"</label>";
					
				}
					/****************** prescreen ********************************/
				if(jobCatMap.get(jc.getJobCategoryId()+"#E")!=null && jobCatMap.get(jc.getJobCategoryId()+"#E").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#E").getPrescreenStatus().equals("A"))
						prescreenValue+="<label class='mb0'>"+lblYes+"</label><br>";
					else
						prescreenValue+="<label class='mb0'>"+lblNo+"</label><br>";
				}
				if(jobCatMap.get(jc.getJobCategoryId()+"#I")!=null && jobCatMap.get(jc.getJobCategoryId()+"#I").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#I").getPrescreenStatus().equals("A"))
						prescreenValue+="<label class='mb0'>"+lblYes+"</label><br>";
					else
						prescreenValue+="<label class='mb0'>"+lblNo+"</label><br>";
				}
				if(jobCatMap.get(jc.getJobCategoryId()+"#T")!=null && jobCatMap.get(jc.getJobCategoryId()+"#T").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#T").getPrescreenStatus().equals("A"))
						prescreenValue+="<label>"+lblYes+"</label>";
					else
						prescreenValue+="<label>"+lblNo+"</label>";
					
				}
					/****************** Onboarding ********************************/
				if(jobCatMap.get(jc.getJobCategoryId()+"#E")!=null && jobCatMap.get(jc.getJobCategoryId()+"#E").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#E").getOnBoardingStatus().equals("A"))
						prescreenOnBoardingValue+="<label class='mb0'>"+lblYes+"</label><br>";
					else
						prescreenOnBoardingValue+="<label class='mb0'>"+lblNo+"</label><br>";
				}
				if(jobCatMap.get(jc.getJobCategoryId()+"#I")!=null && jobCatMap.get(jc.getJobCategoryId()+"#I").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#I").getOnBoardingStatus().equals("A"))
						prescreenOnBoardingValue+="<label class='mb0'>"+lblYes+"</label><br>";
					else
						prescreenOnBoardingValue+="<label class='mb0'>"+lblNo+"</label><br>";
				}
				if(jobCatMap.get(jc.getJobCategoryId()+"#T")!=null && jobCatMap.get(jc.getJobCategoryId()+"#T").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#T").getOnBoardingStatus().equals("A"))
						prescreenOnBoardingValue+="<label>"+lblYes+"</label>";
					else
						prescreenOnBoardingValue+="<label>"+lblNo+"</label>";
				}
					/****************** custom Question ********************************/
				if(jobCatMap.get(jc.getJobCategoryId()+"#E")!=null && jobCatMap.get(jc.getJobCategoryId()+"#E").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#E").getCustomQuestionRequired()==1 && jobCatMap.get(jc.getJobCategoryId()+"#E").getCustomQuestionStatus().equals("A"))
						customQuestion+="<label class='mb0'>"+lblMandatory+"</label><br>";
					else if(jobCatMap.get(jc.getJobCategoryId()+"#E").getCustomQuestionRequired()==0 && jobCatMap.get(jc.getJobCategoryId()+"#E").getCustomQuestionStatus().equals("A"))
						customQuestion+="<label class='mb0'>"+lblOptional+"</label><br>";
					else 
						customQuestion+="<label class='mb0'>"+lblNotRequired+"</label><br>";
				}
				if(jobCatMap.get(jc.getJobCategoryId()+"#I")!=null && jobCatMap.get(jc.getJobCategoryId()+"#I").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#I").getCustomQuestionRequired()==1 && jobCatMap.get(jc.getJobCategoryId()+"#I").getCustomQuestionStatus().equals("A"))
						customQuestion+="<label class='mb0'>"+lblMandatory+"</label><br>";
					else if(jobCatMap.get(jc.getJobCategoryId()+"#I").getCustomQuestionRequired()==0 && jobCatMap.get(jc.getJobCategoryId()+"#I").getCustomQuestionStatus().equals("A"))
						customQuestion+="<label class='mb0'>"+lblOptional+"</label><br>";
					else 
						customQuestion+="<label class='mb0'>"+lblNotRequired+"</label><br>";
				}
				if(jobCatMap.get(jc.getJobCategoryId()+"#T")!=null && jobCatMap.get(jc.getJobCategoryId()+"#T").getStatus().equals("A")){
					if(jobCatMap.get(jc.getJobCategoryId()+"#T").getCustomQuestionRequired()==1 && jobCatMap.get(jc.getJobCategoryId()+"#T").getCustomQuestionStatus().equals("A"))
						customQuestion+="<label class='mb0'>"+lblMandatory+"</label><br>";
					else if(jobCatMap.get(jc.getJobCategoryId()+"#T").getCustomQuestionRequired()==0 && jobCatMap.get(jc.getJobCategoryId()+"#T").getCustomQuestionStatus().equals("A"))
						customQuestion+="<label class='mb0'>"+lblOptional+"</label>";
					else 
						customQuestion+="<label class='mb0'>"+lblNotRequired+"</label>";
					}
				}
				
				dmRecords.append("<div class='col-sm-2 col-md-2 pleft5 "+manageTop+"' style='width:21%;'>"+jc.getJobCategoryName()+"</div>");
				if(jc.getDistrictMaster()!=null){
					dmRecords.append("<div class='col-sm-2 col-md-2 pleftright "+manageTop+"' style='width:20%;'>"+jc.getDistrictMaster().getDistrictName()+"</div>");
				}else{
					dmRecords.append("<div class='col-sm-2 col-md-2 pleftright "+manageTop+"' style='width:20%;'></div>");
				}
				dmRecords.append("<div class='col-sm-1 col-md-1 pright0 pleft2'>"+candidateType+"</div>");
				dmRecords.append("<div class='col-sm-1 col-md-1 pleftright' style='width:4%;'>"+epiValue+"</div>");
				dmRecords.append("<div class='col-sm-1 col-md-1 pleftright' style='width:6%;'>"+portfolioValue+"</div>");
				dmRecords.append("<div class='col-sm-1 col-md-1 pleftright'>"+prescreenValue+"</div>");
				dmRecords.append("<div class='col-sm-1 col-md-1 pleftright'>"+prescreenOnBoardingValue+"</div>");
				dmRecords.append("<div class='col-sm-1 col-md-1 pleftright'>"+customQuestion+"</div>");
				if(jc.getStatus().equalsIgnoreCase("A")){
					dmRecords.append("<div class='col-sm-1 col-md-1 pleft7 pright0 "+manageTop+"' style='width:6.5%;'><label>"+optAct+"</label></div>");
				}else{
					dmRecords.append("<div class='col-sm-1 col-md-1 pleft7 pright0  "+manageTop+"' style='width:6.5%;'><label>"+lblInActiv+"</label></div>");
				}
				dmRecords.append("<div class='col-sm-1 col-md-1 pleftright  "+manageTop+"'>");
				dmRecords.append("<div style='float:left;'><a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+lblEdit+"' onclick='return editJobCategory(1,"+jc.getJobCategoryId()+")'><i class='fa fa-pencil-square-o fa-lg'></i></a>&nbsp;|&nbsp;</div>");
				if(jc.getStatus().equalsIgnoreCase("A"))
					dmRecords.append("<div style='margin-top:4px;float:left;width: 15%;'><a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+msgActiveUser+"' onclick=\"return activateDeactivateJobCategory("+jc.getJobCategoryId()+",'I')\"><i style='color:red;' class='fa fa-times fa-lg'></i></a></div>");
				else
					dmRecords.append("<div style='margin-top:4px;float:left;width: 15%;'><a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+msgInactiveUser+"' onclick=\"return activateDeactivateJobCategory("+jc.getJobCategoryId()+",'A')\"><i style='color:green;' class='fa fa-check fa-lg'></i></a></div>");
				
				dmRecords.append("<div>&nbsp;|&nbsp;<a href='javascript:void(0);' data-toggle='tooltip' class='tempToolTip' data-placement='top' title='"+lblClone+"' onclick='return editJobCategory(2,"+jc.getJobCategoryId()+")'><i class='fa fa-files-o fa-lg'></i></i></a></div>");
				dmRecords.append("</div>");
				dmRecords.append("</div>");
			  }
			dmRecords.append("</div>");
			dmRecords.append(PaginationAndSorting.getPaginationStringForManageDSPQAjaxUsingDiv(request,totalRecord,noOfRow, pageNo));
			}
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return dmRecords.toString();
	}
	@SuppressWarnings("null")
	public String saveJobCategory(Integer districtId,Integer[] candidateTypeStatus,Integer jobCategoryId,String jobCategoryName,Integer parentJobCategoryId,String externalRecord,String internalRecord,String internalTransferRecord,Integer maxScoreForVVI,Integer sendAutoVVILink,String statusDiv,Integer timePerQues,Integer vviExpDays,Integer approvalByPredefinedGroups,Integer schoolSelection,Integer approvalBeforeGoLive,Integer noOfApprovalNeeded,Integer buildApprovalGroup,Integer cloneType)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster =	null;
		Date currentDate	=	new Date();
		Boolean isNew=false;
		Boolean addEditFlag=false;
		String ipAddress = IPAddressUtility.getIpAddress(request);
		String[] candidatetpe={"E","I","T"};
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		} else {
			userMaster = (UserMaster)session.getAttribute("userMaster");
		}
		try
		{
			DistrictMaster districtMaster=null;
			JobCategoryMaster jobCategoryMasterForParentId=null;
			JobCategoryMaster jobCategoryMaster=null;
			if(districtId!=null){
				jobCategoryMaster=new JobCategoryMaster();
				districtMaster=new DistrictMaster();
				districtMaster.setDistrictId(districtId);
				if(jobCategoryId!=null && jobCategoryId!=0){
					addEditFlag=true;
					jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				}
				if(parentJobCategoryId!=null && parentJobCategoryId!=0){
					jobCategoryMasterForParentId=jobCategoryMasterDAO.findById(parentJobCategoryId, false, false);
				    jobCategoryMaster.setParentJobCategoryId(jobCategoryMasterForParentId);
				}
				if(districtMaster!=null)
				     jobCategoryMaster.setDistrictMaster(districtMaster);
			}
			if(maxScoreForVVI!=null && !maxScoreForVVI.equals("")){
				jobCategoryMaster.setMaxScoreForVVI(maxScoreForVVI);
			}
			
			if(sendAutoVVILink!=null && sendAutoVVILink!=0)
				jobCategoryMaster.setSendAutoVVILink(true);
			else{
				if(!addEditFlag)
				      jobCategoryMaster.setSendAutoVVILink(false);
		      }
			
			Integer SchoolPrivilegeForVVI=0;
			if(sendAutoVVILink!=null && sendAutoVVILink!=0)
			{
				if(statusDiv.contains("SSID_"))
				{
					SchoolPrivilegeForVVI=new Integer(statusDiv.substring(5));
					if(SchoolPrivilegeForVVI!=null)
					{
						jobCategoryMaster.setSecondaryStatusIdForAutoVVILink(SchoolPrivilegeForVVI);
					}
					jobCategoryMaster.setStatusIdForAutoVVILink(null);
				}else{
					SchoolPrivilegeForVVI=new Integer(statusDiv);
					if(SchoolPrivilegeForVVI!=null){
						jobCategoryMaster.setStatusIdForAutoVVILink(SchoolPrivilegeForVVI);
					}
					jobCategoryMaster.setSecondaryStatusIdForAutoVVILink(null);
				}
			}else{
				//jobCategoryMaster.setStatusIdForAutoVVILink(null);
				//jobCategoryMaster.setSecondaryStatusIdForAutoVVILink(null);
			}
			if(timePerQues!=null && !timePerQues.equals(""))
				jobCategoryMaster.setTimeAllowedPerQuestion(timePerQues);
			/*else
				jobCategoryMaster.setTimeAllowedPerQuestion(null);*/
			
			if(vviExpDays!=null && !vviExpDays.equals(""))
				jobCategoryMaster.setVVIExpiresInDays(vviExpDays);
			/*else
				jobCategoryMaster.setVVIExpiresInDays(null);*/
			
					if(approvalBeforeGoLive!=null && approvalBeforeGoLive!=0)
						jobCategoryMaster.setApprovalBeforeGoLive(true);
					else
						jobCategoryMaster.setApprovalBeforeGoLive(false);
					
					jobCategoryMaster.setNoOfApprovalNeeded(noOfApprovalNeeded);
					if(buildApprovalGroup!=null && buildApprovalGroup!=0)
						jobCategoryMaster.setBuildApprovalGroup(true);
					else
						jobCategoryMaster.setBuildApprovalGroup(false);
					
					if(schoolSelection==1){
						jobCategoryMaster.setSchoolSelection(true);
					}else if(schoolSelection==0){
						jobCategoryMaster.setSchoolSelection(false);
					}else{
						if(!addEditFlag)
							jobCategoryMaster.setSchoolSelection(false);
					}
					
					if(approvalByPredefinedGroups!=null && approvalByPredefinedGroups!=0)
						jobCategoryMaster.setApprovalByPredefinedGroups(true);
					else
						jobCategoryMaster.setApprovalByPredefinedGroups(false);
					
					
					// **************** Set default value in jobcategory master *******************************************
					
					boolean duplicate=false;
					if(jobCategoryId!=null && jobCategoryId!=0){
						jobCategoryMaster.setJobCategoryId(jobCategoryId);
						if(cloneType!=1)
						{
							duplicate=jobCategoryMasterDAO.checkDuplicateJobCategory(jobCategoryMaster,jobCategoryName,districtMaster);
						}
					}else{
						if(cloneType!=1)
						{
							duplicate=jobCategoryMasterDAO.checkDuplicateJobCategory(null,jobCategoryName,districtMaster);
						}
					}
					jobCategoryMaster.setJobCategoryName(jobCategoryName);
					if(jobCategoryId==0)
					{
						jobCategoryMaster.setBaseStatus(false);
						jobCategoryMaster.setEpiForFullTimeTeachers(false);
						jobCategoryMaster.setAssessmentDocument(null);
						jobCategoryMaster.setOfferDistrictSpecificItems(false);//3
						jobCategoryMaster.setOfferQualificationItems(false);//4
						jobCategoryMaster.setOfferJSI(0);
						jobCategoryMaster.setOfferPortfolioNeeded(false);
						jobCategoryMaster.setOfferVVIForInternalCandidates(false);
						jobCategoryMaster.setOfferVirtualVideoInterview(false);//2
						jobCategoryMaster.setQuestionSets(null);// 1
						jobCategoryMaster.setOfferAssessmentInviteOnly(false);
						jobCategoryMaster.setI4QuestionSets(null);
						jobCategoryMaster.setAttachDSPQFromJC(false);
						jobCategoryMaster.setAttachSLCFromJC(false);
						jobCategoryMaster.setQualificationItemsForIMCandidates(false);
						jobCategoryMaster.setPortfolioForIMCandidates(false);
						jobCategoryMaster.setJsiForIMCandidates(0);
						jobCategoryMaster.setOfferDSPQ(false);
						jobCategoryMaster.setDistrictSpecificItemsForIMCandidates(false);
						jobCategoryMaster.setEpiForIMCandidates(false);
					}
					jobCategoryMaster.setStatus("A");
					//*********************************************************************************************************

					if(!duplicate){
						if(jobCategoryMaster!=null){
							if(jobCategoryMaster.getJobCategoryId()==null || jobCategoryMaster.getJobCategoryId()==0)
								isNew = true;
							else
								isNew = false;
						}
						if(isNew){
						jobCategoryMasterDAO.makePersistent(jobCategoryMaster);
						//hqJobCategoryAjax.saveToJobcategoryMasterHistory(jobCategoryMaster, userMaster, ipAddress,isNew,questionSetVal);
						}else{
							//hqJobCategoryAjax.saveToJobcategoryMasterHistory(jobCategoryMaster, userMaster, ipAddress,isNew,questionSetVal);
							jobCategoryMasterDAO.makePersistent(jobCategoryMaster);
						}
						//************************* Job Category Transaction table *****************************************
						String[] stringObj={externalRecord,internalRecord,internalTransferRecord};
						String[][] multiDimesionalArray=new String[3][8];
						if(candidateTypeStatus[0]==1)
						{
							if(candidateTypeStatus[1]==1)
							{
								if(candidateTypeStatus[2]==1)
								{
									String[] data;
									for(int j=0;j<stringObj.length;j++)
									{
										data=null;
										data=stringObj[j].split("-");
										for(int i=1;i<data.length;i++)
											multiDimesionalArray[j][i-1]=data[i];
									}
								}else{
									String[] data;
									for(int j=0;j<stringObj.length;j++)
									{
										data=null;
										if(j==2)
											data=stringObj[j-1].split("-");
										else
											data=stringObj[j].split("-");
										for(int i=1;i<data.length;i++)
											multiDimesionalArray[j][i-1]=data[i];
									}
								}
							}else{
								if(candidateTypeStatus[2]==1)
								{
									String[] data;
									for(int j=0;j<stringObj.length;j++)
									{
										data=null;
										if(j==1)
											data=stringObj[j-1].split("-");
										else
											data=stringObj[j].split("-");
										
										for(int i=1;i<data.length;i++)
											multiDimesionalArray[j][i-1]=data[i];
									}
								}else{
									String[] data;
									for(int j=0;j<stringObj.length;j++)
									{
										data=null;
										data=stringObj[0].split("-");
										for(int i=1;i<data.length;i++)
											multiDimesionalArray[j][i-1]=data[i];
									}
								}
							}
						}
						
						Map<String,JobCategoryTransaction> typeRecord=new HashMap<String, JobCategoryTransaction>();
						if(cloneType==1)
						{
							typeRecord=jobCategoryTransactionDAO.getRecordBasedOnCandidateType(jobCategoryId);
						}						
						JobCategoryTransaction jobCategoryTransaction=null;
						SessionFactory factory = jobCategoryTransactionDAO.getSessionFactory();
					    StatelessSession statelessSession=factory.openStatelessSession();
					    Transaction transaction = statelessSession.beginTransaction();
					    String[] temp;
						String[] reTemp;
					    String[] mendOptTemp;	
						Integer[] questionSetVal = new Integer[3];
						for(int i=0;i<3;i++)
						{ 				
							temp=null;
							reTemp=null;
							mendOptTemp=null;
							jobCategoryTransaction=new JobCategoryTransaction();
							if(cloneType==1 && typeRecord.size()>0)
							{
							 jobCategoryTransaction=typeRecord.get(candidatetpe[i]);
							}
							//System.out.println("-----------------------------------------------");
							/*for(int j=0;j<8;j++)
								System.out.println("Array value  ::::::::::: "+multiDimesionalArray[i][j]);*/
							try{
							if(jobCategoryMaster!=null)
								jobCategoryTransaction.setJobCategoryId(jobCategoryMaster.getJobCategoryId());
							else if(jobCategoryId!=0)
								jobCategoryTransaction.setJobCategoryId(jobCategoryId);
							}catch (Exception e) {
								e.printStackTrace();
								//return "false";
							}
						    jobCategoryTransaction.setCandidateType(candidatetpe[i]);
							temp=multiDimesionalArray[i][0].split(":");
							reTemp=temp[1].split("\\|",2);
							jobCategoryTransaction.setPortfolioStatus(reTemp[0]);
							//System.out.println("portfolio  :::::::: "+temp[0]+" Status :  "+reTemp[0]);
							if(Utility.getIntValue(temp[0])!=0 && reTemp[0].equalsIgnoreCase("A"))
								jobCategoryTransaction.setPortfolio(Utility.getIntValue(temp[0]));
							else
								jobCategoryTransaction.setPortfolio(null);
							
							temp=multiDimesionalArray[i][1].split(":");
							//System.out.println("pre-screen  :::::::: "+temp[0]+" Status :  "+reTemp[0]);
							reTemp=temp[1].split("\\|",2);
							jobCategoryTransaction.setPrescreenStatus(reTemp[0]);
							if(Utility.getIntValue(temp[0])!=0 && reTemp[0].equalsIgnoreCase("A"))
								jobCategoryTransaction.setPrescreen(Utility.getIntValue(temp[0]));
							else
								jobCategoryTransaction.setPrescreen(null);
							
							temp=multiDimesionalArray[i][2].split(":");
							reTemp=temp[1].split("\\|",2);
							jobCategoryTransaction.setEpiGroupStatus(reTemp[0]);
							//System.out.println("epi Group  :::::::: "+temp[0]+" Status :  "+reTemp[0]);
							if(Utility.getIntValue(temp[0])!=0 && reTemp[0].equalsIgnoreCase("A"))
								jobCategoryTransaction.setEpiGroup(Utility.getIntValue(temp[0]));
							else
								jobCategoryTransaction.setEpiGroup(null);
							
							temp=multiDimesionalArray[i][3].split(":");
							reTemp=temp[1].split("\\|",2);
							jobCategoryTransaction.setCustomQuestionStatus(reTemp[0]);
							mendOptTemp=temp[0].split(";");
							//System.out.println("Custom Question  :::::::: "+mendOptTemp[0]+" Status :  "+reTemp[0]);
							if(Utility.getIntValue(mendOptTemp[0])!=0 && reTemp[0].equalsIgnoreCase("A"))
								jobCategoryTransaction.setCustomQuestion(Utility.getIntValue(mendOptTemp[0]));
							else
								jobCategoryTransaction.setCustomQuestion(null);
							jobCategoryTransaction.setCustomQuestionRequired(Utility.getIntValue(mendOptTemp[1]));
							
							temp=multiDimesionalArray[i][4].split(":");
							reTemp=temp[1].split("\\|",2);
							jobCategoryTransaction.setOnBoardingStatus(reTemp[0]);
							//System.out.println("onBoarding  :::::::: "+temp[0]+" Status :  "+reTemp[0]);
							if(Utility.getIntValue(temp[0])!=0 && reTemp[0].equalsIgnoreCase("A"))
								jobCategoryTransaction.setOnBoarding(Utility.getIntValue(temp[0]));
							else
								jobCategoryTransaction.setOnBoarding(null);
							
							temp=multiDimesionalArray[i][5].split(":");
							reTemp=temp[1].split("\\|",2);
							jobCategoryTransaction.setVviStatus(reTemp[0]);
							//System.out.println("VVI  :::::::: "+temp[0]+" Status :  "+reTemp[0]);
							if(Utility.getIntValue(temp[0])!=0 && reTemp[0].equalsIgnoreCase("A"))
								jobCategoryTransaction.setVvi(Utility.getIntValue(temp[0]));
							else
								jobCategoryTransaction.setVvi(null);
							
							temp=multiDimesionalArray[i][6].split(":");
							jobCategoryTransaction.setOnlineAssesment(temp[0]);
							reTemp=temp[1].split("\\|",2);
							jobCategoryTransaction.setOnlineAssStatus(reTemp[0]);
							temp=multiDimesionalArray[i][7].split(":");	
							reTemp=temp[1].split("\\|",2);
							jobCategoryTransaction.seteReferenceStatus(reTemp[0]);
							//System.out.println("E-Reference  :::::::: "+temp[0]+" Status :  "+reTemp[0]);
							
							if(Utility.getIntValue(temp[0])!=0 && reTemp[0].equalsIgnoreCase("A")){
								jobCategoryTransaction.seteReference(Utility.getIntValue(temp[0]));
								questionSetVal[i]=Utility.getIntValue(temp[0]);
							}else{
								jobCategoryTransaction.seteReference(null);	
								questionSetVal[i]=0;
							}						
							jobCategoryTransaction.setCreatedDate(currentDate);
							jobCategoryTransaction.setCreatedByUser(userMaster.getEntityType());
							jobCategoryTransaction.setIpAddress(ipAddress);
							if(candidateTypeStatus[i]!=0)
								jobCategoryTransaction.setStatus("A");
							else
								jobCategoryTransaction.setStatus("I");							
							
							if(cloneType==1 && typeRecord.size()>0)
								statelessSession.update(jobCategoryTransaction);
							else
								statelessSession.insert(jobCategoryTransaction);
						}
						transaction.commit();
						statelessSession.close();	
						//***************** For the reference question set **********************************************
						/*
						   Updated query 'ALTER TABLE `referencecheckquestionset` ADD `candidateType` VARCHAR( 2 ) NOT NULL AFTER `headQuarterId` ;'
						*/
						try{
					    List<ReferenceQuestionSets> referenceCheckQuestionList=new ArrayList<ReferenceQuestionSets>();
					    referenceCheckQuestionList=referenceQuestionSetsDAO.findByQuestionSetIds(questionSetVal);
					    
						if(referenceCheckQuestionList.size()>0){
			
							SessionFactory factoryRef = referenceCheckQuestionSetTempDAO.getSessionFactory();
						    StatelessSession statelessSessionRef=factoryRef.openStatelessSession();
						    Transaction transactionRef = statelessSessionRef.beginTransaction();
						    
						    List<ReferenceCheckQuestionSetTemp> referenceCheckQuestionSetList = null;
							referenceCheckQuestionSetList	=	referenceCheckQuestionSetTempDAO.findByQuestionAndJobCategory(districtMaster,jobCategoryMaster);
							ReferenceCheckQuestionSetTemp referenceCheckQuestionSet = null;
							ReferenceCheckQuestionSetTemp referenceCheckQuestionSet1 = null;
							//System.out.println("referenceCheckQuestionSetList ::::::::"+referenceCheckQuestionSetList.size());
							 if(referenceCheckQuestionSetList.size()>0)
								 referenceCheckQuestionSet1 = referenceCheckQuestionSetTempDAO.findById(referenceCheckQuestionSetList.get(0).getReferenceQuestionId(), false, false);
							
						for(int i=0;i<3;i++)
						{
							//System.out.println("E-Reference Question sets length :::"+questionSetVal.length);
						  if(questionSetVal.length!=0){
							//ReferenceQuestionSets referenceQuestionSets = referenceQuestionSetsDAO.findById(questionSetVal[i], false, false);
						      referenceCheckQuestionSet =  new ReferenceCheckQuestionSetTemp();
			                  if(referenceCheckQuestionSetList.size() == 0 || referenceCheckQuestionSetList == null){
			                	  // Insert Record
			                	  try{
			                		  for(int j=0;j<referenceCheckQuestionList.size();j++){
			                			  if(referenceCheckQuestionList.get(j).getID()==questionSetVal[i]){
			                				  referenceCheckQuestionSet.setReferenceQuestionSets(referenceCheckQuestionList.get(j));
			                				  break;
			                				  }
		                			  }
				                  		  referenceCheckQuestionSet.setCandidateType(candidatetpe[i]);
				                  		  referenceCheckQuestionSet.setJobCategoryMaster(jobCategoryMaster);
				                          referenceCheckQuestionSet.setDistrictMaster(districtMaster);
				                          referenceCheckQuestionSet.setUserMaster(userMaster);
				                          referenceCheckQuestionSet.setCreatedDateTime(currentDate);
				                          statelessSessionRef.insert(referenceCheckQuestionSet);
				                          //referenceCheckQuestionSetDAO.makePersistent(referenceCheckQuestionSet);
			                      } catch(Exception exception){
			                          exception.printStackTrace();
			                      }
			                  } else {
			                	  if(referenceCheckQuestionSetList!=null)
			                		  try{
			                			  for(int j=0;j<referenceCheckQuestionList.size();j++){
				                			  if(referenceCheckQuestionList.get(j).getID()==questionSetVal[i]){
				                				  referenceCheckQuestionSet.setReferenceQuestionSets(referenceCheckQuestionList.get(j));
				                				  break;
				                				  }
			                			  }
			                			  referenceCheckQuestionSet1.setCandidateType(candidatetpe[i]);
			                			  statelessSessionRef.insert(referenceCheckQuestionSet1);
			                			  //referenceCheckQuestionSetDAO.makePersistent(referenceCheckQuestionSet);
			                		  } catch(Exception exception){
			                			  exception.printStackTrace();
			                		  }
			                  	}
							}
						}
						transactionRef.commit();
						statelessSessionRef.close();
					}
			}catch(Exception e)
			{e.printStackTrace();}
			//***********************************************************************************************
			  return ""+jobCategoryMaster.getJobCategoryId();
					}
				}
				catch (Exception e) 
				{
					e.printStackTrace();
					//return "false";
				}

				return "true";
	}
	
	public boolean activateDeactivateJobCategory(Integer jobcategoryId,String status)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try{
			JobCategoryMaster jobCategoryMaster=jobCategoryMasterDAO.findById(jobcategoryId, false, false);
			jobCategoryMaster.setStatus(status);
			jobCategoryMasterDAO.makePersistent(jobCategoryMaster);
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	
	
	@Transactional(readOnly=false)
	public Map<String,Object> editJobCategoryId(Integer jobCategoryId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		List<JobCategoryTransaction>jobCatTransList=new ArrayList<JobCategoryTransaction>();
		Map<String,Object>  mapList=new HashMap<String, Object>();
		Map<Integer,String> VVIMap=new HashMap<Integer, String>();
		List<Integer> VVIListId=new ArrayList<Integer>();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();
		try{
			if(jobCategoryId!=null){
				jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				if(jobCategoryMaster!=null)
					jobCatTransList=jobCategoryTransactionDAO.getRecordBasedOnJobCategory(jobCategoryId);
					
				if(jobCategoryMaster!=null &&  jobCategoryMaster.getAssessmentDocument()!=null){
					session.setAttribute("previousJSIFileName", jobCategoryMaster.getAssessmentDocument());	
				}
			}
			/*                    JobCategoryMaster                */
			mapList.put("jobCategoryName", jobCategoryMaster.getJobCategoryName());
			mapList.put("jobCategoryId", jobCategoryMaster.getJobCategoryId());
			mapList.put("parentJobCategoryId", jobCategoryMaster.getParentJobCategoryId());
			if(jobCategoryMaster!=null)
			if(jobCategoryMaster.getParentJobCategoryId()!=null)
				mapList.put("parentJobCategoryId", jobCategoryMaster.getParentJobCategoryId().getJobCategoryId());
			else
				mapList.put("parentJobCategoryId",0);
			
			mapList.put("approvalByPredefinedGroups", jobCategoryMaster.getApprovalByPredefinedGroups());
			mapList.put("approvalBeforeGoLive", jobCategoryMaster.getApprovalBeforeGoLive());
			mapList.put("noOfApprovalNeeded", jobCategoryMaster.getNoOfApprovalNeeded());
			mapList.put("buildApprovalGroup", jobCategoryMaster.getBuildApprovalGroup());
			mapList.put("schoolSelection", jobCategoryMaster.getSchoolSelection());
			mapList.put("maxScoreForVVI", jobCategoryMaster.getMaxScoreForVVI());
			mapList.put("timePerQues", jobCategoryMaster.getTimeAllowedPerQuestion());
			mapList.put("sendAutoVVILink", jobCategoryMaster.getSendAutoVVILink());
			mapList.put("statusIdForAutoVVILink", jobCategoryMaster.getStatusIdForAutoVVILink());
			mapList.put("secondaryStatusIdForAutoVVILink", jobCategoryMaster.getSecondaryStatusIdForAutoVVILink());
			mapList.put("vviExpDays", jobCategoryMaster.getVVIExpiresInDays());
			mapList.put("districtid", jobCategoryMaster.getDistrictMaster().getDistrictId());
			mapList.put("districtname", jobCategoryMaster.getDistrictMaster().getDistrictName());
			
			/*                            JobCategoryTransaction                               */
			Integer count=0;
			for(JobCategoryTransaction mp:jobCatTransList)
			{
				mapList.put(count+"candidateType"+mp.getCandidateType(), mp.getCandidateType());
				mapList.put(count+"portfolio"+mp.getCandidateType(), mp.getPortfolio());
				mapList.put(count+"portfolioStatus"+mp.getCandidateType(), mp.getPortfolioStatus());
				//System.out.println(" mp.getPortfolioStatus()  ::::::: "+ mp.getPortfolioStatus());//
				mapList.put(count+"prescreen"+mp.getCandidateType(), mp.getPrescreen());
				//System.out.println("Pre screen status :::::::  "+mp.getPrescreenStatus());
				mapList.put(count+"prescreenStatus"+mp.getCandidateType(), mp.getPrescreenStatus());
				//System.out.println("  mp.getPrescreenStatus()  ::::::: "+  mp.getPrescreenStatus());//
				mapList.put(count+"epi"+mp.getCandidateType(), mp.getEpiGroup());
				mapList.put(count+"epiStatus"+mp.getCandidateType(), mp.getEpiGroupStatus());
				//System.out.println(" mp.getEpiGroupStatus()  ::::::: "+mp.getEpiGroupStatus());//
				mapList.put(count+"custom"+mp.getCandidateType(), mp.getCustomQuestion());
				mapList.put(count+"customStatus"+mp.getCandidateType(), mp.getCustomQuestionStatus());
				//System.out.println(" mp.getCustomQuestionStatus()  ::::::: "+ mp.getCustomQuestionStatus());//
				mapList.put(count+"customMandOpt"+mp.getCandidateType(), mp.getCustomQuestionRequired());
				mapList.put(count+"onboarding"+mp.getCandidateType(), mp.getOnBoarding());
				mapList.put(count+"onboardingStatus"+mp.getCandidateType(), mp.getOnBoardingStatus());
				//System.out.println(" mp.getOnBoardingStatus()  ::::::: "+ mp.getOnBoardingStatus());//
				mapList.put(count+"vvi"+mp.getCandidateType(), mp.getVvi());
				VVIListId.add(mp.getVvi());
				mapList.put(count+"vviStatus"+mp.getCandidateType(), mp.getVviStatus());
				//System.out.println(" mp.getVviStatus()  ::::::: "+ mp.getVviStatus());//
				mapList.put(count+"onlineassessment"+mp.getCandidateType(), mp.getOnlineAssesment());
				mapList.put(count+"onlineassessmentStatus"+mp.getCandidateType(), mp.getOnlineAssStatus());
				//System.out.println(" mp.getOnlineAssStatus()  ::::::: "+ mp.getOnlineAssStatus());//
				mapList.put(count+"ereference"+mp.getCandidateType(), mp.geteReference());
				mapList.put(count+"ereferenceStatus"+mp.getCandidateType(), mp.geteReferenceStatus());
				//System.out.println("mp.geteReferenceStatus()  ::::::: "+ mp.geteReferenceStatus());//
				mapList.put(count+"status"+mp.getCandidateType(), mp.getStatus());
				count++;
			}
			if(count==0){
				char canType[]={'E','I','T'};
				Object statusObj="A";
				for(int i=0;i<3;i++){
				mapList.put(i+"candidateType"+canType[i], canType[i]);
				mapList.put(i+"portfolio"+canType[i], null);
				mapList.put(i+"portfolioStatus"+canType[i], statusObj);
				mapList.put(i+"prescreen"+canType[i], null);
				mapList.put(i+"prescreenStatus"+canType[i], statusObj);
				mapList.put(i+"epi"+canType[i], null);
				mapList.put(i+"epiStatus"+canType[i], statusObj);
				mapList.put(i+"custom"+canType[i], null);
				mapList.put(i+"customStatus"+canType[i], statusObj);
				mapList.put(i+"customMandOpt"+canType[i], 1);
				mapList.put(i+"onboarding"+canType[i], null);
				mapList.put(i+"onboardingStatus"+canType[i], statusObj);
				mapList.put(i+"vvi"+canType[i], null);
				VVIListId.add(0);
				mapList.put(i+"vviStatus"+canType[i], statusObj);
				mapList.put(i+"onlineassessment"+canType[i], null);
				mapList.put(i+"onlineassessmentStatus"+canType[i], statusObj);
				mapList.put(i+"ereference"+canType[i], null);
				mapList.put(i+"ereferenceStatus"+canType[i], statusObj);
				mapList.put(i+"status"+canType[i], statusObj);
				}
			}
			VVIMap=i4QuestionSetsDAO.getQuestionSetName(VVIListId);
			Integer countValue=0;
			for(Integer id: VVIListId){
				mapList.put("vviQuesName"+countValue,VVIMap.get(id));
				countValue++;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapList;
	}

	/* ====== Gagan : This method is used for  download Jsi =======*/
	@Transactional(readOnly=false)
	public String downloadJsi(int jobCategoryId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String path="";
		try 
		{
			String source="";
			String target="";
			JobCategoryMaster jc=new JobCategoryMaster();
			if(jobCategoryId!=0)
				jc=jobCategoryMasterDAO.findById(jobCategoryId, false, false);

			String fileName= jc.getAssessmentDocument();
			source = Utility.getValueOfPropByKey("districtRootPath")+jc.getDistrictMaster().getDistrictId()+"/jobcategory/"+jc.getAssessmentDocument();
			target = context.getServletContext().getRealPath("/")+"/"+"/district/"+jc.getDistrictMaster().getDistrictId()+"/";

			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());

			FileUtils.copyFile(sourceFile, targetFile);
			path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+jc.getDistrictMaster().getDistrictId()+"/"+jc.getAssessmentDocument(); 	

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;

	}
	
	/* ====== Gagan : This method is used for  download Jsi =======*/
	public String removeJsi(int jobCategoryId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try 
		{
			JobCategoryMaster jc=null;
			if(jobCategoryId!=0)
			{
				jc=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				SecondaryStatus  secondaryStatus = 	secondaryStatusDAO.findSecondaryStatusByDistrict(jc.getDistrictMaster(),jc);
				if(secondaryStatus!=null)
					return "3";
			}

			if(jc!=null ){

				File file = new File(Utility.getValueOfPropByKey("districtRootPath")+jc.getDistrictMaster().getDistrictId()+"/jobcategory/"+jc.getAssessmentDocument());
				//System.out.println("Remove Jsi 1111 "+file.exists()+"\n"+file.getPath());
				if(file.exists()){
					if(file.delete()){
						//System.out.println(file.getName()+" deleted");
					}else{
						//System.out.println("Delete operation for Jsi is failed.");
						return "2";
					}
					jc.setAssessmentDocument(null);
					jobCategoryMasterDAO.makePersistent(jc);
					return "1";
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return "2";
		}
		return "2";
	}
 // For VVI
	public String getStatusList(String districtId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		StringBuffer status=	new StringBuffer();
		
		List<StatusMaster> statusMasterList = null;
		List<StatusMaster> lstStatusMaster = null;
		
		try
		{
			
			Map<Integer,SecondaryStatus> mapSStatus = new HashMap<Integer, SecondaryStatus>();
			
			DistrictMaster districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			List<SecondaryStatus> lstSecStatusAll=	secondaryStatusDAO.findSecondaryStatusOp(districtMaster);   
			List<SecondaryStatus> lstsecondaryStatus=  new ArrayList<SecondaryStatus>();
			
			if(lstSecStatusAll!=null)
			for(SecondaryStatus secStatus : lstSecStatusAll){
				
				if(secStatus.getStatusMaster()==null && secStatus.getStatusNodeMaster()==null && secStatus.getJobCategoryMaster()==null){
					lstsecondaryStatus.add(secStatus);
				}
				if(secStatus.getStatusMaster()!=null){
					mapSStatus.put(secStatus.getStatusMaster().getStatusId(),secStatus);
				}
			}
			

			SecondaryStatus secondaryStatus=null;

			String[] statuss = {"hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
			lstStatusMaster	=	statusMasterDAO.findStatusByStatusByShortNames(statuss);
			
			statusMasterList = new ArrayList<StatusMaster>();
			
			for(StatusMaster sm: lstStatusMaster){
				if(sm.getBanchmarkStatus()==1){
					secondaryStatus=mapSStatus.get(sm.getStatusId());
					if(secondaryStatus!=null){
						sm.setStatus(secondaryStatus.getSecondaryStatusName());
					}
				}
				statusMasterList.add(sm);
			}
			
			status.append("<select class='form-control' id='slctStatusID'>");
			//status.append("<option id='' value='' ></option>");
			
			if(statusMasterList!=null && statusMasterList.size()>0)
			{
				for(StatusMaster sm:statusMasterList)
				{
					status.append("<option value='"+sm.getStatusId()+"' >"+sm.getStatus()+"</option>");
				}
			}
			
			if(lstsecondaryStatus!=null && lstsecondaryStatus.size()>0)
			{
				for(SecondaryStatus ssm:lstsecondaryStatus)
				{
					status.append("<option value='SSID_"+ssm.getSecondaryStatusId()+"' >"+ssm.getSecondaryStatusName()+"</option>");
				}
			}
			
			
			status.append("</select>");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	
		return status.toString();
	}
	
	@Transactional(readOnly=false)
	public DistrictMaster getVVIDefaultFields(Integer districtId)
	{
		//System.out.println(" ====== getVVIDefaultFields =======");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		DistrictMaster districtMaster	=	new DistrictMaster();
		try{
			if(districtId!=null){
				districtMaster = districtMasterDAO.findById(districtId, false, false);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtMaster;
	}
	
	public String displayAllDistrictAssessment(String districtId,Integer jobcategoryId)
	{
		//System.out.println(" =================== displayAllDistrictAssessment ================== ");
		Integer selectedCounter=0;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		StringBuffer sbAttachSchool=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		boolean selectedDAS = false;
		DistrictMaster districtMaster  = null;
		JobCategoryMaster jobCategoryMaster = null;
		try
		{
			districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			if(districtMaster!=null)
			{
				if(jobcategoryId!=null && !jobcategoryId.equals(""))
					jobCategoryMaster = jobCategoryMasterDAO.findById(jobcategoryId, false, false);
				sbAttachSchool.append("<ul>");
				//Job category Wise Data
				if(jobCategoryMaster!=null)
				{
					List<DistrictAssessmentDetail> AlldistrictAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
					List<DistrictAssessmentDetail> selecteddistAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
					AlldistrictAssessmentDetails = districtAssessmentDetailDAO.findByDistrict(districtMaster);
					String daIds = jobCategoryMaster.getDistrictAssessmentId();
					
					
					String arr[]=null;
					List multiDAIds = new ArrayList();
					
					if(daIds!=null && !daIds.equals(""))
					{
						if(daIds!="" && daIds.length() > 0)
						{
							arr=daIds.split("#");
						}
						
						if(arr!=null && arr.length > 0)
						{
							for(int i=0;i<arr.length;i++)
							{
								multiDAIds.add(Integer.parseInt((arr[i])));	
							}
						}
					}
					
					selecteddistAssessmentDetails = districtAssessmentDetailDAO.findByDistAssessmentIds(multiDAIds);
								
					AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails);
					//Unselected DA List
					//sb.append("<select multiple id=\"lstDistrictAdmins\" name=\"lstDistrictAdmins\" class=\"form-control\" style=\"height: 150px;\" >");
					if(AlldistrictAssessmentDetails.size()>0)
					{
						for (DistrictAssessmentDetail selectedDASMT : AlldistrictAssessmentDetails) {
							//sb.append("<option value="+selectedDASMT.getDistrictAssessmentId()+">"+selectedDASMT.getDistrictAssessmentName()+"</option>");
							sbAttachSchool.append("<li><label class='checkbox mb0 mt0'><input style='margin-top:1px;' type='checkbox' name='asses0' onclick='selunselect0()' value='"+selectedDASMT.getDistrictAssessmentId()+"' title='"+selectedDASMT.getDistrictAssessmentName()+"' />"+selectedDASMT.getDistrictAssessmentName()+"</label></li>");
							//System.out.println("Unselected District Assessment :"+selectedDASMT.getDistrictAssessmentName());
						}
				//		selectedDAS = true;
					}
					//sb.append("</select>");
					
					//Selected DA List
					//sbAttachSchool.append("<select multiple class=\"form-control\" id=\"attachedDAList\" name=\"attachedDAList\" style=\"height: 150px;\">");
					if(selecteddistAssessmentDetails.size()>0)
					{
						for (DistrictAssessmentDetail selectdDASMT : selecteddistAssessmentDetails) {
							//sbAttachSchool.append("<option value="+selectdDASMT.getDistrictAssessmentId()+">"+selectdDASMT.getDistrictAssessmentName()+"</option>");
						    sbAttachSchool.append("<li><label class='checkbox mb0 mt0'> <input style='margin-top:1px;' type='checkbox' name='asses0' onclick='selunselect0()' value='"+selectdDASMT.getDistrictAssessmentId()+"' title='"+selectdDASMT.getDistrictAssessmentName()+"' checked/>"+selectdDASMT.getDistrictAssessmentName()+"</label></li>");
						    selectedCounter++;
						   // System.out.println("Selected District Assessment :"+selectdDASMT.getDistrictAssessmentName());
						}
						//System.out.println("01");
						selectedDAS = true;
					}
					//sbAttachSchool.append("</select>");
				}else
				{
					List<DistrictAssessmentDetail> AlldistrictAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
					List<DistrictAssessmentDetail> selecteddistAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
					
					AlldistrictAssessmentDetails = districtAssessmentDetailDAO.findByDistrict(districtMaster);
					
					
				    String daIds = districtMaster.getDistrictAssessmentId();
					
					
					String arr[]=null;
					List multiDAIds = new ArrayList();
					
					if(daIds!=null && !daIds.equals(""))
					{
						if(daIds!="" && daIds.length() > 0)
						{
							arr=daIds.split("#");
						}
						
						if(arr!=null && arr.length > 0)
						{
							for(int i=0;i<arr.length;i++)
							{
								multiDAIds.add(Integer.parseInt((arr[i])));	
							}
						}
					}
					
					selecteddistAssessmentDetails = districtAssessmentDetailDAO.findByDistAssessmentIds(multiDAIds);
					
					AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails);
					//Unselected DA List
					//sb.append("<select multiple id=\"lstDistrictAdmins\" name=\"lstDistrictAdmins\" class=\"form-control\" style=\"height: 150px;\" >");
					if(AlldistrictAssessmentDetails.size()>0)
					{
						for (DistrictAssessmentDetail selectedDASMT : AlldistrictAssessmentDetails) {
							//sb.append("<option value="+selectedDASMT.getDistrictAssessmentId()+">"+selectedDASMT.getDistrictAssessmentName()+"</option>");
							sbAttachSchool.append("<li><label class='checkbox mb0 mt0'><input style='margin-top:1px;' type='checkbox' name='asses0' onclick='selunselect0()' value='"+selectedDASMT.getDistrictAssessmentId()+"' title='"+selectedDASMT.getDistrictAssessmentName()+"' />"+selectedDASMT.getDistrictAssessmentName()+"</label></li>");
							//System.out.println("Unselected District Assessment :"+selectedDASMT.getDistrictAssessmentName());
						}
				//		selectedDAS = true;
					}
					//sb.append("</select>");
					
					//Selected DA List
					//sbAttachSchool.append("<select multiple class=\"form-control\" id=\"attachedDAList\" name=\"attachedDAList\" style=\"height: 150px;\">");
					if(selecteddistAssessmentDetails.size()>0)
					{
						for (DistrictAssessmentDetail selectdDASMT : selecteddistAssessmentDetails) {
							//sbAttachSchool.append("<option value="+selectdDASMT.getDistrictAssessmentId()+">"+selectdDASMT.getDistrictAssessmentName()+"</option>");
							sbAttachSchool.append("<li><label class='checkbox mb0 mt0'><input style='margin-top:1px;' type='checkbox' name='asses0' onclick='selunselect0()' value='"+selectdDASMT.getDistrictAssessmentId()+"' title='"+selectdDASMT.getDistrictAssessmentName()+"' checked/>"+selectdDASMT.getDistrictAssessmentName()+"</label></li>");
							selectedCounter++;
							//System.out.println("Selected District Assessment :"+selectdDASMT.getDistrictAssessmentName());
						}
						selectedDAS = true;	
					}
					//sbAttachSchool.append("</select>");
				}
				sbAttachSchool.append("</ul>");
			}
			//System.out.println("Select Counter ::::::: "+selectedCounter);
			sbAttachSchool.append("<input type='hidden' id='selectedId0' name='selectedName' value='"+selectedCounter+"'/>");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		//return sb.toString()+"||"+sbAttachSchool.toString()+"||"+selectedDAS;
		return sbAttachSchool.toString();
	}
	
	public String getReferenceByDistrictList(int districtId, int jobCategoryId){
		StringBuffer sb = new StringBuffer();
		DistrictMaster districtMaster							=	null;
		List<ReferenceCheckQuestionSet> refChkQuestionSet		= 	null;
		List<ReferenceQuestionSets>	referenceQuestionSetsList 	= 	null;
		JobCategoryMaster jobCategoryMaster						=	null;
		List<Integer> ques = new ArrayList<Integer>();
		Integer counter = 1; 
		Integer questionId = null;
		try 
		{
			//System.out.println("District ID::::::::::"+districtId);
			if(""+districtId!="")
			districtMaster			=	districtMasterDAO.findById(districtId, false, false);
			if(jobCategoryId!=0)
			jobCategoryMaster		=	jobCategoryMasterDAO.findById(jobCategoryId, false, false);
			refChkQuestionSet 		= 	referenceCheckQuestionSetDAO.getReferenceByDistrictAndJobCategory(districtMaster, jobCategoryMaster);
			if(refChkQuestionSet.size()>0 && refChkQuestionSet !=null && refChkQuestionSet.get(0) != null)
				questionId	=	refChkQuestionSet.get(0).getReferenceQuestionSets().getID();
			
				referenceQuestionSetsList	=	referenceQuestionSetsDAO.findByDistrictActive(districtMaster);
				sb.append("<select id='avlbList' name='avlbList' class='span4'>");
				sb.append("<option value=''>"+listPleaseSelectQuestionSet+"</option>");
				if(referenceQuestionSetsList!=null && referenceQuestionSetsList.size() > 0)
					for(ReferenceQuestionSets refChkRecord : referenceQuestionSetsList)
						if(refChkQuestionSet!=null && refChkQuestionSet.size() > 0 && refChkQuestionSet.get(0) != null){
							if(questionId.equals(refChkRecord.getID()) && jobCategoryId!=0){
								sb.append("<option selected value='"+refChkRecord.getID()+"'>"+refChkRecord.getQuestionSetText()+"</option>");
							} else {
								sb.append("<option value='"+refChkRecord.getID()+"'>"+refChkRecord.getQuestionSetText()+"</option>");
							}
						} else {
							sb.append("<option value='"+refChkRecord.getID()+"'>"+refChkRecord.getQuestionSetText()+"</option>");
						}
			  sb.append("</select>");
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public String getQQuestionSetByDistrictAndJobCategoryList(Integer districtId, Integer jobCategoryId){
        WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		StringBuffer sb = new StringBuffer();
        DistrictMaster districtMaster = null;
        JobCategoryMaster jobCategoryMaster = null;
        
        List<QqQuestionSets> districtSpecificQQList = null;
		
	 try{
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		if(districtId!=null){			
		districtMaster = districtMasterDAO.findById(districtId, false, false);
	    Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
		districtSpecificQQList = qqQuestionSetsDAO.findByCriteria(criterion1);
	    }else{
		districtSpecificQQList = new ArrayList<QqQuestionSets>();
	    }
        
		if(jobCategoryId!=null){
			 jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
		}
		
		//sb.append("<select id='qqAvlbList' name='qqAvlbList' class='span4'>");
		sb.append("<option value=''>"+lblPleaseSelectQQSet+"</option>");
		
		if(districtSpecificQQList!=null)
	    {
			Integer selectOpt=0;
			try {
				if(jobCategoryMaster!=null && jobCategoryMaster.getQuestionSets()!=null){
					selectOpt= jobCategoryMaster.getQuestionSets().getID();
				}else{
					selectOpt= districtMaster.getQqQuestionSets().getID();
				}
			} catch (Exception e) {}
		  for(QqQuestionSets list: districtSpecificQQList)
	       {
			  if(list.getID().equals(selectOpt))
	    		  sb.append("<option selected value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
			  else
				  sb.append("<option value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
	       }
	     }
		//sb.append("</select>");
		
	 }catch(Exception e){
        	 e.printStackTrace();
         }
		return sb.toString();
	}
	
	public String getJobCategoryByDistrictId(int districtId,int jobSubCate)
	{
		StringBuffer sb	=	new StringBuffer();
		try {
			DistrictMaster districtMaster	=	districtMasterDAO.findById(districtId, false, false);
			List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
			JobCategoryMaster jobSubCategory = null;
			if(jobSubCate>0)
				jobSubCategory = jobCategoryMasterDAO.findById(jobSubCate, false, false);
			
			sb.append("<option value='0'>"+optStrJobCat+"</option>");
			if(jobCategoryMasterlst!=null && jobCategoryMasterlst.size()>0){
				for(JobCategoryMaster jb: jobCategoryMasterlst)
				{
					if(jb.getParentJobCategoryId()==null){
						if(jobSubCategory!=null && jobSubCategory.getParentJobCategoryId()!=null && jb.getJobCategoryId().equals(jobSubCategory.getParentJobCategoryId().getJobCategoryId()))
							sb.append("<option value='"+jb.getJobCategoryId()+"' selected>"+jb.getJobCategoryName()+"</option>");
						else
							sb.append("<option value='"+jb.getJobCategoryId()+"'>"+jb.getJobCategoryName()+"</option>");
					}else{
						System.out.println("Parents job Category Id:::::: "+jb.getParentJobCategoryId());
					}
				}
			}
		} catch (Exception e) 
		{
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	public String getQQSetForOnboardingByDistrictAndJobCategoryList(Integer districtId, Integer jobCategoryId){
		
        WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		DistrictMaster districtMaster=null;
		StringBuffer sb = new StringBuffer();
        JobCategoryMaster jobCategoryMaster = null;
        char[] arr={'E','I','T'};
        List<QqQuestionSets> districtSpecificQQList = null;
		
	 try{
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}	
		if(districtId!=null){			
		districtMaster = districtMasterDAO.findById(districtId, false, false);
	    Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
		districtSpecificQQList = qqQuestionSetsDAO.findByCriteria(criterion1);
	    }else{
		districtSpecificQQList = new ArrayList<QqQuestionSets>();
	    }
		if(jobCategoryId!=null){
			 jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
		}
     	sb.append("<option value=''>"+msgPleaseSelectaSet+"</option>");
		
		if(districtSpecificQQList!=null)
	    {
			Integer selectOpt=0;
			try {
				if(jobCategoryMaster!=null && jobCategoryMaster.getQuestionSetsForOnboarding()!=null){
					selectOpt= jobCategoryMaster.getQuestionSetsForOnboarding().getID();
				}else{
					selectOpt= districtMaster.getQqQuestionSets().getID();
				}
			} catch (Exception e) {}
		  for(QqQuestionSets list: districtSpecificQQList)
	       {
			  if(list.getID().equals(selectOpt))
				  sb.append("<option selected value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
			  else
				  sb.append("<option value='"+list.getID()+"'>"+list.getQuestionSetText()+"</option>");
	       }
	     }
	 }catch(Exception e){
        	 e.printStackTrace();
         }
		return sb.toString();
	}
	/*
	 * @Deepak
	 * Portfolio name based on District id for job Category
	 * 
	 */
	public String getPortfolioNameByDistrictId(int districtId,String candidateType)
	{
		StringBuffer sb	=	new StringBuffer();
		try {
			DistrictMaster districtMaster	=	districtMasterDAO.findById(districtId, false, false);
			 List<String[]> dspqPortfolioNameList=null;
			 dspqPortfolioNameList=dspqRouterDAO.getPortfolioIdByApplicantType(districtMaster,candidateType);
				sb.append("<option value=''>"+msgSelectPortfolioName+"</option>");
			 if(dspqPortfolioNameList!=null && dspqPortfolioNameList.size()>0){
				for (Iterator it = dspqPortfolioNameList.iterator(); it.hasNext();) {
		            Object[] array = (Object[]) it.next();
							sb.append("<option value='"+array[0].toString()+"'>"+array[1].toString()+"</option>");
				}
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public String getAssessmentNameByAssessmentType(Integer assessmentType,int active)
	{
		StringBuffer sb	=	new StringBuffer();
		try {
			 List<AssessmentGroupDetails> assessmentDetailList = null;
			 assessmentDetailList=assessmentGroupDetailDAO.getAllAssessmentGroupDetailsList(assessmentType,active);
			 if(assessmentDetailList!=null && assessmentDetailList.size()>0){
				sb.append("<option value=''>"+lblPleaseSelectaEPIGroup+"</option>");
				for(AssessmentGroupDetails jb: assessmentDetailList)
					sb.append("<option value='"+jb.getAssessmentGroupId()+"'>"+jb.getAssessmentGroupName()+"</option>");
		
			}
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public String getAssessmentNameByDistrictAndType(Integer assessmentType,Integer districtId)
	{
		StringBuffer sb	=	new StringBuffer();
		try {
			 List<AssessmentDetail> assessmentDetailList = null;
			 DistrictMaster districtMaster	=	districtMasterDAO.findById(districtId, false, false);
			 assessmentDetailList=assessmentDetailDAO.findAssessmentsNameByDistrictAndType(assessmentType,districtMaster);
			 if(assessmentDetailList!=null && assessmentDetailList.size()>0){
				sb.append("<option value=''>"+msgPleaseSelectaSet+"</option>");
				for(AssessmentDetail jb: assessmentDetailList)
					sb.append("<option value='"+jb.getAssessmentId()+"'>"+jb.getAssessmentName()+"</option>");
		
			}
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
public String portfolioChangeNotification(Integer iPortfolioId, Integer iJobcategoryId,String sCandidateType,String sApplyToAllCandidateTypes,Integer districtId)
{
    WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	StringBuffer sb = new StringBuffer("");
	try
	{
		if(session == null ||session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}	
		JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();
		jobCategoryMaster.setJobCategoryId(iJobcategoryId);
		int iPortfolioIdE=0,iPortfolioIdI=0,iPortfolioIdT=0;
		Map<String,JobCategoryTransaction> mapJobCatTrans=jobCategoryTransactionDAO.getRecordBasedOnCandidateType(iJobcategoryId);
		int isAffliate=-1;
		if(sApplyToAllCandidateTypes!=null && !sApplyToAllCandidateTypes.equals(""))
		{
			isAffliate=-1;
		}
		else
		{
			if(sCandidateType!=null && sCandidateType.equalsIgnoreCase("I"))
				isAffliate=1;
			else
				isAffliate=0;
			
			if(mapJobCatTrans!=null && sCandidateType!=null && !sCandidateType.equals(""))
			{
				JobCategoryTransaction categoryTransaction=null;
				if(mapJobCatTrans.get(sCandidateType)!=null)
				{
					categoryTransaction=mapJobCatTrans.get(sCandidateType);
					if(categoryTransaction!=null && categoryTransaction.getPortfolio()!=null && iPortfolioId.equals(categoryTransaction.getPortfolio()))
					return "";
				}
			}
		}
		if(mapJobCatTrans!=null)
		{
			JobCategoryTransaction categoryTransaction=null;
			if(mapJobCatTrans.get("E")!=null)
			{
				categoryTransaction=mapJobCatTrans.get("E");
				if(categoryTransaction!=null && categoryTransaction.getPortfolio()!=null)
					iPortfolioIdE=categoryTransaction.getPortfolio();
			}
			
			if(mapJobCatTrans.get("I")!=null)
			{
				categoryTransaction=mapJobCatTrans.get("I");
				if(categoryTransaction!=null && categoryTransaction.getPortfolio()!=null)
					iPortfolioIdI=categoryTransaction.getPortfolio();
			}
			
			if(mapJobCatTrans.get("T")!=null)
			{
				categoryTransaction=mapJobCatTrans.get("T");
				if(categoryTransaction!=null && categoryTransaction.getPortfolio()!=null)
					iPortfolioIdT=categoryTransaction.getPortfolio();
			}
		}
		if(sCandidateType!=null && sCandidateType.equalsIgnoreCase("I") && iPortfolioIdI==0)
		{
			return "";
		}
		else if(sCandidateType!=null && sCandidateType.equalsIgnoreCase("T") && iPortfolioIdT==0)
		{
			return "";
		}
		if((mapJobCatTrans!=null && mapJobCatTrans.size()>0) || (iPortfolioIdE+iPortfolioIdI+iPortfolioIdT) >0 )
		{
			List<Object> lstObjects=jobForTeacherDAO.getJobStatusByJobCategory(jobCategoryMaster,isAffliate,districtId);
			if(lstObjects!=null && lstObjects.size()>0)
			{
				sb.append("<label class='mb10'>There are following job(s) which have completed or finalized node status for many candidates:-</label>");
				sb.append("<table id='portfolioChngTblJob' width='100%' border='0' >");
				sb.append("<thead class='bg'>");
		        sb.append("<tr>");
		        sb.append("<th valign='top'>Job Id</th><th valign='top'>Job Title</th><th valign='top'>No of Candidate</th>");
		        sb.append("</tr>");
		        sb.append("</thead>");
				for (Object row:lstObjects)
				{
		            Object[] arrObj = (Object[])row;
		            Integer iJobId=Utility.getIntValue(arrObj[0].toString());
		            String jobTitle=arrObj[1].toString();
		            Integer iJobCount=Utility.getIntValue(arrObj[2].toString());
		            sb.append("<tr>");
		            sb.append("<td>"+iJobId+"</td>");
		            sb.append("<td>"+jobTitle+"</td>");
		            sb.append("<td>"+iJobCount+"</td>");
		            sb.append("</tr>");
				}
				sb.append("</table>");
				
				sb.append("<DIV class='mt30'>Do you want to change portfolio?</DIV>");
			}
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
    }
	return sb.toString();
	}

/*
 * @pawan
 * onboarding name by district id
 * 
 * */
public String getOnBoardingByDistrictId(Integer districtId)
{
	StringBuffer sb=new StringBuffer();
	try
	{
		Integer districtIdP=0;
		if(districtId!=0 && districtId!=null){
			districtIdP=Integer.valueOf(districtId);
		}			
		DistrictMaster districtMaster=districtMasterDAO.findById(districtIdP, false, false);			
		List<OnboardingDashboard> onboardingDashboardList=new ArrayList<OnboardingDashboard>();
		onboardingDashboardList=onboardingDashboardDAO.findOnBoardingNameByDistrict(districtMaster);
		sb.append("<option value=''>"+msgSelectOnBoarding+"</option>");
		if(onboardingDashboardList!=null && onboardingDashboardList.size()>0)
		{
			for(OnboardingDashboard rec:onboardingDashboardList){
				sb.append("<option value='"+rec.getOnboardingId()+"'>"+rec.getOnboardingName()+"</option>");
			}
		}
	}
	catch (Exception e) 
	{
		e.printStackTrace();
	}
	return sb.toString();
}
}
