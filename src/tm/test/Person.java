package tm.test;

public class Person {

	public Person(String fname,String lname) {
		setFname(fname);
		setLname(lname);
	}

	String fname;
	String lname;
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
}
