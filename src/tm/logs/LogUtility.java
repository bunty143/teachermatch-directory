package tm.logs;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.AdminActivityDetail;
import tm.bean.JobCertification;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.user.UserMaster;
import tm.dao.AdminActivityDetailDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.master.JobWisePanelStatusDAO;
import tm.utility.Utility;

public class LogUtility {
	
	public static Map<String, Object> ConvertObjectToMap(Object obj) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
	    Method[] methods = obj.getClass().getMethods();


	    Map<String, Object> map = new LinkedHashMap<String, Object>();
	    for (Method m : methods) {
	       if (m.getName().startsWith("get") && !m.getName().startsWith("getClass")) {
	          Object value = (Object) m.invoke(obj);
	          map.put(m.getName().substring(3), (Object) value);   // here key is in 'capitalize' format means first letter is Capital.... 
	       }
	    }
	return map;
	}
	
	
	
	
	public String getDiffValueKeys(Map<String,Object> current ,Map<String,Object> previous ,List noCheckLabels ){
		String result ="";
		/*result.append("<table class='table table-hover top2' style='margin-bottom: 10px !important;'>");
		result.append("<thead style='background-color:#007AB4;'><tr><th>#</th><th>Field Name</th><th>Previous Value</th><th>Current Value</th></tr></thead>");
		*/
		int counter=0;
		String columnVsUIKey="";
		LinkedHashMap<String, String> diffMapResult = new LinkedHashMap<String, String>();
		try{
			if(current!=null && previous!=null){
				for(String key : current.keySet()){
					
					try {
					if(noCheckLabels.contains(key)) {continue;}  // for no check label
					if(current.get(key)==null || previous.get(key)==null){
						if(!(previous.get(key)==null && current.get(key)==null )){
						 
							diffMapResult.put(key, getObjValue(previous.get(key))+"|"+getObjValue(current.get(key)));
							}
					}else{
						
						if(current.get(key).hashCode()!=previous.get(key).hashCode()){
					//		System.out.println("previous:=== "+getObjValue(previous.get(key)));
					//		System.out.println("current:=== "+getObjValue(current.get(key)));
							 
							diffMapResult.put(key, getObjValue(previous.get(key))+"|"+getObjValue(current.get(key)));
						}  
						}
					}catch (Exception e) {
						System.out.println("**************************************Exception for key = "+key);
					}
				}
				
				
			}
			 
		 
		}
		catch (Exception e) {
			System.out.println("*****************************   Error while comparing two map for key******************************");
			e.printStackTrace();
		}
		
		for(String key:diffMapResult.keySet()) {
			System.out.println(key +" : "+diffMapResult.get(key));
		}
		 try {
			result= Utility.createJsonString(diffMapResult);
		} catch (IOException e) { e.printStackTrace(); }
		return result;
		
	}
	
	
	

	public String getObjValue(Object obj){
		String result ="";
	 
		if(obj!=null){
		if(obj.getClass()== java.lang.Boolean.class){
			if(obj.equals(Boolean.TRUE)) result = "Required";
			else   result = "Not required";
		}
		else {
			result+=obj;
		}
		 
		} 
		
		return result;

	}
	
	public void createLogForJobOrder(JobOrder jobOder,Boolean isNew,Integer jobId ,UserMaster user , List listJobWise ) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		System.out.println("JobOrder  =  "+jobOder);
		System.out.println("isNew	  =  "+isNew);
		System.out.println("JobId 	  =  "+jobId);
		System.out.println("listJobWise------"+listJobWise);
		List<String> noCheckLabel = new ArrayList<String>();
		noCheckLabel.add("TeacherDetail");
		noCheckLabel.add("School");
		noCheckLabel.add("jobQualificationHTML");
		noCheckLabel.add("JobDescriptionHTML");
		
		 if(isNew) {
			 logForNewJobOrder(jobOder,user);
		 }
		 else {
			 logForExistingJobOrder(jobOder,noCheckLabel,user, listJobWise);
		 }
		
		
		
	}
	
	
	private void logForExistingJobOrder(JobOrder jobOrder , List noCheckLabel,UserMaster user , List<JobWisePanelStatus> listJobWise) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		System.out.println("*************************   From logForExistingJobOrder  *****************************");
		ApplicationContext springContext=null;
		 springContext = WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		 JobOrderDAO jobOrderDAO;
		// System.out.println("********************************SprintContext#####################################"+springContext);
		 if(springContext!=null){
		 jobOrderDAO = (JobOrderDAO) springContext.getBean("jobOrderDAO"); 
		 JobWisePanelStatusDAO jobWisePanelStatusDAO = new JobWisePanelStatusDAO();
		 jobWisePanelStatusDAO = (JobWisePanelStatusDAO) springContext.getBean("jobWisePanelStatusDAO");
		
		if(jobOrder!=null && ( jobOrder.getJobId()!=null || jobOrder.getJobId()!=0)) {
			
			
			try {
				JobOrder curreJobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);
				System.out.println("current JobOrder =  " + curreJobOrder);
				Map<String,Object> currJobOrderMap =  LogUtility.ConvertObjectToMap(curreJobOrder);
				Map<String,Object> prevJobOrderMap =  LogUtility.ConvertObjectToMap(jobOrder);
				String result = new LogUtility().getDiffValueKeys(currJobOrderMap, prevJobOrderMap, noCheckLabel);
				StringBuffer sb = new StringBuffer();
				Boolean jobWiseFlag = false;
				List<JobWisePanelStatus> persistJobWise = new ArrayList<JobWisePanelStatus>();
				try {
					Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
					persistJobWise = jobWisePanelStatusDAO.findByCriteria(criterion1);
					
				} catch (Exception e) {
					
					e.printStackTrace();
				}
				
				Map<Integer, JobWisePanelStatus>jwsSecStatusMap=new HashMap<Integer, JobWisePanelStatus>();
				for(JobWisePanelStatus jws:persistJobWise){
					jwsSecStatusMap.put(jws.getSecondaryStatus().getSecondaryStatusId(), jws);
				}
				
			/* remove due to Optimization	
				for(JobWisePanelStatus currentJobWise : listJobWise){
				
			    	JobWisePanelStatus persistJobWis = jwsSecStatusMap.get(currentJobWise.getSecondaryStatus().getSecondaryStatusId());
					Map<String,Object> prevJobOrderMap1 =  LogUtility.ConvertObjectToMap(persistJobWis);
					Map<String,Object> currJobOrderMap2 =  LogUtility.ConvertObjectToMap(currentJobWise);
					String result1 = new LogUtility().getDiffValueKeys(prevJobOrderMap1, currJobOrderMap2, noCheckLabel);
					if(result1.contains("PanelStatus"))
						jobWiseFlag = true;
						
				}*/
				String lblPanelStatus = "";
				String valuePanelStatus = "";
				String panelStatus = "";
				if(jobWiseFlag){
					 
					 lblPanelStatus = "\"PanelStatus\"" ;
					 valuePanelStatus = " \"|Panel Changed\"";
					 panelStatus = lblPanelStatus+":"+valuePanelStatus;
				}	 
				
				String finalResult="";
				if(result.length()>3 && panelStatus.length()>3)
				finalResult = result.substring(0, result.length()-1)+","+panelStatus+"}";
				else if(result.length()>3 && panelStatus.length()<3)
				finalResult = result;
				else if(panelStatus.length()>3)
				finalResult = 	"{"+panelStatus+"}";
				System.out.println("resultfinal-------------"+finalResult);
				
				AdminActivityDetailDAO adminActivityDetailDAO = (AdminActivityDetailDAO)springContext.getBean("adminActivityDetailDAO");
				AdminActivityDetail adminActivityDetail = new AdminActivityDetail();
				adminActivityDetail.setActivityType("update");
				adminActivityDetail.setEntityTable("joborder");
				adminActivityDetail.setEntityType("Job Order");
				adminActivityDetail.setUserMaster(user);
				adminActivityDetail.setEntityId(jobOrder.getJobId());
				adminActivityDetail.setDescription(finalResult);
				adminActivityDetail.setCreatedDate(new Date());
				adminActivityDetail.setHeadQuarterId(jobOrder.getHeadQuarterMaster()!=null?jobOrder.getHeadQuarterMaster().getHeadQuarterId():null);
				adminActivityDetail.setBranchId(jobOrder.getBranchMaster()!=null?jobOrder.getBranchMaster().getBranchId():null);
				adminActivityDetail.setDistrictId(jobOrder.getDistrictMaster()!=null?jobOrder.getDistrictMaster().getDistrictId():null);
				
				adminActivityDetailDAO.makePersistent(adminActivityDetail)  ;
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			 
		}
		 }
		
	}

	
//sandeep	
	
	public void createCertificate(List certList , JobOrder jobOrder , UserMaster user) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		System.out.println("*************************   logForCertificate  *****************************");
		ApplicationContext springContext=null;
		 springContext = WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		 JobCertificationDAO jobCertificationDAO;
		 JobOrderDAO jobOrderDAO;
		
		 if(springContext!=null){
			 jobCertificationDAO = (JobCertificationDAO) springContext.getBean("jobCertificationDAO"); 
			 jobOrderDAO = (JobOrderDAO) springContext.getBean("jobOrderDAO"); 
		 
			List<JobCertification> lstJobCertificationLog= null;
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			lstJobCertificationLog = jobCertificationDAO.findByCriteria(criterion1);
			Boolean jobCertFlag = false;
			if(lstJobCertificationLog.size() != certList.size())
				jobCertFlag = true;
			else
				return;
			
			StringBuffer sbPrevious = new StringBuffer();
			StringBuffer sbCurrent = new StringBuffer();
			System.out.println("certList---------"+certList);
			System.out.println("lstJobCertificationLog---------"+lstJobCertificationLog);
			for(JobCertification  jobCert : (ArrayList<JobCertification>)certList){
				if(jobCert.getCertificateTypeMaster() != null){
					sbPrevious.append(jobCert.getCertificateTypeMaster().getCertType());
					sbPrevious.append(" , ");
				}	
			}
			for(JobCertification  jobCertCurrent : lstJobCertificationLog){
				if(jobCertCurrent.getCertificateTypeMaster() !=null){
					sbCurrent.append(jobCertCurrent.getCertificateTypeMaster().getCertType());
					sbPrevious.append(" , ");
				}	
			}
			
			
			String lblCert = "";
			String valueCert = "";
			String cert = "";
			if(jobCertFlag){
				 
				 lblCert = "\"Certificate\"" ;
				 valueCert = " \""+sbPrevious+"|"+sbCurrent+"\"";
				 cert = lblCert+":"+valueCert;
			}	 
			
			String finalResult = 	"{"+cert+"}";
			System.out.println("resultfinal-------------"+finalResult);
			AdminActivityDetailDAO adminActivityDetailDAO = (AdminActivityDetailDAO)springContext.getBean("adminActivityDetailDAO");
			AdminActivityDetail adminActivityDetail = new AdminActivityDetail();
			adminActivityDetail.setActivityType("update");
			adminActivityDetail.setEntityTable("joborder");
			adminActivityDetail.setEntityType("Job Order");
			adminActivityDetail.setUserMaster(user);
			adminActivityDetail.setEntityId(jobOrder.getJobId());
			adminActivityDetail.setDescription(finalResult);
			adminActivityDetail.setCreatedDate(new Date());
			adminActivityDetail.setHeadQuarterId(jobOrder.getHeadQuarterMaster()!=null?jobOrder.getHeadQuarterMaster().getHeadQuarterId():null);
			adminActivityDetail.setBranchId(jobOrder.getBranchMaster()!=null?jobOrder.getBranchMaster().getBranchId():null);
			adminActivityDetail.setDistrictId(jobOrder.getDistrictMaster()!=null?jobOrder.getDistrictMaster().getDistrictId():null);
			
			adminActivityDetailDAO.makePersistent(adminActivityDetail)  ;
			 
		}
		 }
		
	
	private void logForNewJobOrder(JobOrder jobOrder,UserMaster user) {
		System.out.println("*************************   From logForExistingJobOrder  *****************************");
		ApplicationContext springContext=null;
		 springContext = WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		 JobOrderDAO jobOrderDAO;
		
		 if(springContext!=null){
		 
		
		 if(jobOrder!=null && ( jobOrder.getJobId()!=null || jobOrder.getJobId()!=0)) {
			
			
			
			String result = "{}";
			
			System.out.println(result);
			// now save result in adminactivitydetail table
			AdminActivityDetailDAO adminActivityDetailDAO = (AdminActivityDetailDAO)springContext.getBean("adminActivityDetailDAO");
			AdminActivityDetail adminActivityDetail = new AdminActivityDetail();
			adminActivityDetail.setActivityType("create");
			adminActivityDetail.setEntityTable("joborder");
			adminActivityDetail.setEntityType("Job Order");
			adminActivityDetail.setUserMaster(user);
			adminActivityDetail.setEntityId(jobOrder.getJobId());
			adminActivityDetail.setDescription(result);
			adminActivityDetail.setCreatedDate(new Date());
			adminActivityDetail.setHeadQuarterId(jobOrder.getHeadQuarterMaster()!=null?jobOrder.getHeadQuarterMaster().getHeadQuarterId():null);
			adminActivityDetail.setBranchId(jobOrder.getBranchMaster()!=null?jobOrder.getBranchMaster().getBranchId():null);
			adminActivityDetail.setDistrictId(jobOrder.getDistrictMaster()!=null?jobOrder.getDistrictMaster().getDistrictId():null);
			
			adminActivityDetailDAO.makePersistent(adminActivityDetail);
		
	}
	
		 }
	}
	
	
	
	public void createSchoolInJobOrder(List schoolInJobOrderList , UserMaster user , JobOrder jobOrder) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		System.out.println("*************************   logForCertificate  *****************************");
		ApplicationContext springContext=null;
		 springContext = WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		 SchoolInJobOrderDAO schoolInJobOrderDAO;
		 JobOrderDAO jobOrderDAO;
		 if(springContext!=null){
			schoolInJobOrderDAO = (SchoolInJobOrderDAO) springContext.getBean("schoolInJobOrderDAO"); 
			List<SchoolInJobOrder> lstSchoolInJobOrderLog= null;
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			lstSchoolInJobOrderLog = schoolInJobOrderDAO.findByCriteria(criterion1);
			Boolean jobSchoolFlag = false;
			if(lstSchoolInJobOrderLog.size() != schoolInJobOrderList.size())
				jobSchoolFlag = true;
			else
				return;
			
			StringBuffer sbPrevious = new StringBuffer();
			StringBuffer sbCurrent = new StringBuffer();
			for(SchoolInJobOrder  jobSchool : (ArrayList<SchoolInJobOrder>)schoolInJobOrderList){
				if(jobSchool.getSchoolId() != null){
					sbPrevious.append(jobSchool.getSchoolId().getSchoolName());
					sbPrevious.append(" , ");
				}	
			}
			for(SchoolInJobOrder  jobSchoolCurrent : lstSchoolInJobOrderLog){
				if(jobSchoolCurrent.getSchoolId() !=null){
					sbCurrent.append(jobSchoolCurrent.getSchoolId().getSchoolName());
					sbPrevious.append(" , ");
				}	
			}
			
			
			String lblSchool = "";
			String valueSchool = "";
			String school = "";
			if(jobSchoolFlag){
				 
				 lblSchool = "\"School\"" ;
				 valueSchool = " \""+sbPrevious+"|"+sbCurrent+"\"";
				 school = lblSchool+":"+valueSchool;
			}	 
			
			String finalResult = 	"{"+school+"}";
			System.out.println("resultfinal-------------"+finalResult);
			
			AdminActivityDetailDAO adminActivityDetailDAO = (AdminActivityDetailDAO)springContext.getBean("adminActivityDetailDAO");
			AdminActivityDetail adminActivityDetail = new AdminActivityDetail();
			adminActivityDetail.setActivityType("update");
			adminActivityDetail.setEntityTable("joborder");
			adminActivityDetail.setEntityType("Job Order");
			adminActivityDetail.setUserMaster(user);
			adminActivityDetail.setEntityId(jobOrder.getJobId());
			adminActivityDetail.setDescription(finalResult);
			adminActivityDetail.setCreatedDate(new Date());
			adminActivityDetail.setHeadQuarterId(jobOrder.getHeadQuarterMaster()!=null?jobOrder.getHeadQuarterMaster().getHeadQuarterId():null);
			adminActivityDetail.setBranchId(jobOrder.getBranchMaster()!=null?jobOrder.getBranchMaster().getBranchId():null);
			adminActivityDetail.setDistrictId(jobOrder.getDistrictMaster()!=null?jobOrder.getDistrictMaster().getDistrictId():null);
			
			adminActivityDetailDAO.makePersistent(adminActivityDetail)  ;
			 
		}
		 }
	

}
