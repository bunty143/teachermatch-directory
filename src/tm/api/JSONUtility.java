package tm.api;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;

public class JSONUtility 
{
	public static JSONObject districtListToJson(List<DistrictMaster> list)
	{
		JSONObject jsonResponse=new JSONObject();
		JSONArray jsonArray=new JSONArray();
		for(DistrictMaster districtMaster:list)
		{
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("districtId", districtMaster.getDistrictId());
			jsonObject.put("districtName", districtMaster.getDistrictName());
			jsonArray.add(jsonObject);
		}
		jsonResponse.put("districtList", jsonArray);
		return jsonResponse;
	}
	public static JSONObject schoolListToJson(List<DistrictSchools> list)
	{
		JSONObject jsonResponse=new JSONObject();
		JSONArray jsonArray=new JSONArray();
		for(DistrictSchools districtSchools:list)
		{
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("schoolId", districtSchools.getSchoolMaster().getSchoolId());
			jsonObject.put("schoolName", districtSchools.getSchoolMaster().getSchoolName());
			jsonArray.add(jsonObject);
		}
		jsonResponse.put("schoolList", jsonArray);
		return jsonResponse;
	}
	public static JSONObject certListToJson(List<CertificateTypeMaster> list)
	{
		JSONObject jsonResponse=new JSONObject();
		JSONArray jsonArray=new JSONArray();
		for(CertificateTypeMaster certificateTypeMaster:list)
		{
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("certType", certificateTypeMaster.getCertType());
			jsonObject.put("certTypeId", certificateTypeMaster.getCertTypeId());
			jsonObject.put("stateName", certificateTypeMaster.getStateId().getStateName());
			jsonArray.add(jsonObject);
		}
		jsonResponse.put("certList", jsonArray);
		return jsonResponse;
	}
}
