package tm.api.rest;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.api.UtilityAPI;
import tm.api.services.ServiceUtility;
import tm.bean.DistrictAccountingCode;
import tm.bean.DistrictSpecificJobcode;
import tm.bean.EmployeeMaster;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.RaceMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictAccountingCodeDAO;
import tm.dao.DistrictSpecificJobcodeDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.SapCandidateDetailsDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

import com.sun.jersey.api.core.InjectParam;



@Component
@Path("/api")
public class APIServices {


	@InjectParam
	private DistrictMasterDAO districtMasterDAO;

	@InjectParam
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	@InjectParam
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	@InjectParam
	private RaceMasterDAO raceMasterDAO;
	@InjectParam
	private SapCandidateDetailsDAO sapCandidateDetailsDAO;
	@InjectParam
	private JobForTeacherDAO jobForTeacherDAO;
	@InjectParam
	private UserMasterDAO userMasterDAO;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/jobCodeUpdate")
	public Response jobCodeUpdate(String json,@Context HttpServletRequest request, @Context HttpServletResponse res) 
	{	
		System.out.println(" <<<<<<<<<<<<<<<<       jobCodeUpdate    >>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
		String returnVal = "";

		try{
			String payloadRequest = json;
			/*String payloadRequest = request.getParameter("request");
			if(payloadRequest==null)
			{
				try {
					payloadRequest = ServiceUtility.getBody(request);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}*/
			System.out.println("json:: "+json);
			
			ServletContext context = request.getSession().getServletContext();
			ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
			districtMasterDAO = (DistrictMasterDAO)context0.getBean("districtMasterDAO");
			DistrictSpecificJobcodeDAO districtSpecificJobcodeDAO = (DistrictSpecificJobcodeDAO)context0.getBean("districtSpecificJobcodeDAO");
			JobCategoryMasterDAO jobCategoryMasterDAO = (JobCategoryMasterDAO)context0.getBean("jobCategoryMasterDAO");
			HttpSession session  = request.getSession();
			Boolean responseStatus = false;
			Integer errorCode = 10001;
			String errorMsg = "Invalid Credentials.";
			String message="";

			System.out.println("payloadRequest::::::= "+payloadRequest);
			DistrictMaster districtMaster = null;
			JSONObject jsonRequest = null;
			payloadRequest = payloadRequest.replaceAll("(\r\n|\n)", "<br />");
			jsonRequest = (JSONObject) JSONSerializer.toJSON( payloadRequest ); 
			String authKey = jsonRequest.get("authKey")==null?"":UtilityAPI.parseString(jsonRequest.getString("authKey")).replace(' ', '+');


			if(authKey!=null){

				districtMaster = districtMasterDAO.validateDistrictByAuthkey(authKey);
				if(districtMaster!=null){
					session.setAttribute("apiDistrictMaster", districtMaster);
					responseStatus = true;
					System.out.println("********District Validated********"+districtMaster);
				}else{
					responseStatus = false;
					errorCode = 10001;
					errorMsg = "Invalid Credentials.";
				}
			}else{
				responseStatus = false;
				errorCode = 10001;
				errorMsg = "Invalid Credentials.";
			}
			JSONObject jsonResponse = null;
			//returnVal = payloadRequest;
			String jobDescription = "";
			String apiJobId = "";
			String jobTitle = "";
			JobCategoryMaster jobCategoryMaster = null;
			
			String action = jsonRequest.get("action")==null?"":UtilityAPI.parseString(jsonRequest.getString("action")).replace(' ', '+');
			DistrictSpecificJobcode districtSpecificJobcode = null;
			String jobFamily = "";
			String hours = "";
			String salaryAdminPlan = "";
			String step = "";
			String gradePay = "";

			if(responseStatus){

				System.out.println("Enter in try");				
				apiJobId = jsonRequest.get("jobCode")==null?"":jsonRequest.getString("jobCode").trim();
				jobTitle = jsonRequest.get("jobTitle")==null?"":jsonRequest.getString("jobTitle").trim();
				jobDescription = jsonRequest.get("jobDescription")==null?"":jsonRequest.getString("jobDescription").trim();
				jobFamily = jsonRequest.get("jobFamily")==null?"":jsonRequest.getString("jobFamily").trim();
				hours = jsonRequest.get("hours")==null?"":UtilityAPI.parseString(jsonRequest.getString("hours")).trim();
				try{
					salaryAdminPlan = jsonRequest.get("Sal_Admin_Plan")==null?"":jsonRequest.getString("Sal_Admin_Plan").trim();
					gradePay= jsonRequest.get("Grade")==null?"":jsonRequest.getString("Grade").trim();
					step  = jsonRequest.get("Step")==null?"":jsonRequest.getString("Step").trim();
				}catch(Exception e){
					e.printStackTrace();
				}
				
				
				System.out.println("apiJobId: "+apiJobId);
				System.out.println("jobTitle: "+jobTitle);
				System.out.println("jobDescription: "+jobDescription);
				System.out.println("jobFamily: "+jobFamily);
				System.out.println("hours: "+hours);
				System.out.println("salaryAdminPlan: "+salaryAdminPlan);
				System.out.println("gradePay: "+gradePay);
				System.out.println("step: "+step);
				
				jobCategoryMaster = jobCategoryMasterDAO.findJobCategoryByNameAndDistrict(jobFamily,districtMaster);
				System.out.println("jobCategoryMaster:: "+jobCategoryMaster);
				if(apiJobId!=null){
					List<DistrictSpecificJobcode> districtSpecificJobcodes = districtSpecificJobcodeDAO.findJobCategoryByJobcode(districtMaster, apiJobId);
					if(districtSpecificJobcodes!=null && districtSpecificJobcodes.size()>0)
						districtSpecificJobcode = districtSpecificJobcodes.get(0);
					System.out.println("lstDistrictRequisitionNumbers ************* size: "+districtSpecificJobcode);
				}else{
					responseStatus = false;
					errorCode = 10008;
					errorMsg = "Job Code is required";
				}

				if(apiJobId.equals("")){
					responseStatus = false;
					errorCode = 10008;
					errorMsg = "Job Code is required.";
				}
				else if(jobTitle.equals("")){
					responseStatus = false;
					errorCode = 10010;
					errorMsg = "Job Title is required.";
				}/*else if(jobFamily.equals("")){
					responseStatus = false;
					errorCode = 10019;
					errorMsg = "Job Family is required.";
				}*/
				else if(!jobFamily.equals("") && jobCategoryMaster==null){
					System.out.println("11111111111111111 ");
					responseStatus = false;
					errorCode = 10020;
					errorMsg = "Job Family does not exist.";
				}
				
				/*if(salaryAdminPlan.equals("") || salaryAdminPlan==null){
					responseStatus = false;
					errorCode = 10021;
					errorMsg = "Salary Admin Plan does not exist.";
				}
				
				if(gradePay.equals("") || gradePay==null){
					responseStatus = false;
					errorCode = 10022;
					errorMsg = "Grade Pay does not exist.";
				}
				
				if(step.equals("") || step==null){
					responseStatus = false;
					errorCode = 10022;
					errorMsg = "Step does not exist.";
				}
				*/
				System.out.println("jobCategoryMaster:: "+jobCategoryMaster);
				
				double hoursVal = 0.0;

				if(action.equalsIgnoreCase("add") || action.equalsIgnoreCase("change"))
				{
					if(districtSpecificJobcode!=null && action.equalsIgnoreCase("add"))
					{
						responseStatus = false;
						errorCode = 10010;
						errorMsg = "Duplicate JobCode.";
					}else if(districtSpecificJobcode==null && action.equalsIgnoreCase("change"))
					{
						responseStatus = false;
						errorCode = 10005;
						errorMsg = "Invalid Job Code "+apiJobId;
					}
					
					
					if(responseStatus)
					{
						if(districtSpecificJobcode==null)
						{
							districtSpecificJobcode = new DistrictSpecificJobcode();
						}

						try{ hoursVal = Double.parseDouble(hours);}catch(Exception exception){System.out.println(exception);}

						districtSpecificJobcode.setDistricMaster(districtMaster);
						districtSpecificJobcode.setHours(hoursVal);
						districtSpecificJobcode.setJobCategoryId(jobCategoryMaster);
						districtSpecificJobcode.setJobDescription(jobDescription);
						districtSpecificJobcode.setJobTitle(jobTitle);
						districtSpecificJobcode.setJobCode(apiJobId);
						districtSpecificJobcode.setSalaryAdminPlan(salaryAdminPlan);
						districtSpecificJobcode.setGradePay(gradePay);
						districtSpecificJobcode.setStep(step);
						
						if(action.equalsIgnoreCase("add"))
						{
							districtSpecificJobcode.setStatus("A");
							districtSpecificJobcode.setCreatedDateTime(new Date());
							
						}
						districtSpecificJobcode.setIpAddress(request.getRemoteAddr());
						districtSpecificJobcodeDAO.makePersistent(districtSpecificJobcode);
						errorCode = 0;
						errorMsg = "";
					}

					jsonResponse = new JSONObject();
					jsonResponse.put("status", true);
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
					jsonResponse.put("errorCode", errorCode);
					jsonResponse.put("errorMessage", errorMsg);
				}
				else if(action.equalsIgnoreCase("delete") && districtSpecificJobcode!=null)
				{
					districtSpecificJobcodeDAO.makeTransient(districtSpecificJobcode);
					
					jsonResponse = new JSONObject();
					jsonResponse.put("status", true);
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
					jsonResponse.put("errorCode", 0);
					jsonResponse.put("errorMessage", "");
				}
				else if(action.equalsIgnoreCase("delete") && districtSpecificJobcode==null)
				{
					jsonResponse = new JSONObject();
					jsonResponse.put("status", true);
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
					jsonResponse.put("errorCode", 10005);
					jsonResponse.put("errorMessage", "Invalid Job Code "+apiJobId);
				}else
				{
					jsonResponse = new JSONObject();
					jsonResponse.put("status", false);
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
					jsonResponse.put("errorCode", errorCode);
					jsonResponse.put("errorMessage", errorMsg);
				}
			}
			else{
				jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
			}	
			if(jsonResponse!=null)
				returnVal = jsonResponse.toString();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.status(200).entity(returnVal.toString()).build();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/accountingCodeUpdate")
	public Response accountingCodeUpdate(InputStream incomingData,@Context HttpServletRequest request, @Context HttpServletResponse res) 
	{	
		String returnVal = "";

		try{
			String payloadRequest = request.getParameter("request");
			if(payloadRequest==null)
			{
				try {
					payloadRequest = ServiceUtility.getBody(request);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			ServletContext context = request.getSession().getServletContext();
			ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
			DistrictAccountingCodeDAO districtAccountingCodeDAO = (DistrictAccountingCodeDAO)context0.getBean("districtAccountingCodeDAO");
			districtMasterDAO = (DistrictMasterDAO)context0.getBean("districtMasterDAO");
			HttpSession session  = request.getSession();
			Boolean responseStatus = false;
			Integer errorCode = 10001;
			String errorMsg = "Invalid Credentials.";

			System.out.println("payloadRequest::::::= "+payloadRequest);
			DistrictMaster districtMaster = null;
			JSONObject jsonRequest = null;
			jsonRequest = (JSONObject) JSONSerializer.toJSON( payloadRequest ); 
			String authKey = jsonRequest.get("authKey")==null?"":UtilityAPI.parseString(jsonRequest.getString("authKey")).replace(' ', '+');


			if(authKey!=null){

				districtMaster = districtMasterDAO.validateDistrictByAuthkey(authKey);
				if(districtMaster!=null){
					session.setAttribute("apiDistrictMaster", districtMaster);
					responseStatus = true;
					System.out.println("********District Validated********"+districtMaster);
				}else{
					responseStatus = false;
					errorCode = 10001;
					errorMsg = "Invalid Credentials.";
				}
			}else{
				responseStatus = false;
				errorCode = 10001;
				errorMsg = "Invalid Credentials.";
			}
			JSONObject jsonResponse = null;
			//returnVal = payloadRequest;
			String accountingCode = "";
			String name = "";
			
			String action = jsonRequest.get("action")==null?"":UtilityAPI.parseString(jsonRequest.getString("action")).replace(' ', '+');
			DistrictAccountingCode districtAccountingCode = null;
			

			if(responseStatus){

				System.out.println("Enter in try");				
				accountingCode = jsonRequest.get("accountingCode")==null?"":UtilityAPI.parseString(jsonRequest.getString("accountingCode")).trim();
				name = jsonRequest.get("Name")==null?"":UtilityAPI.parseString(jsonRequest.getString("Name")).trim();
				
				if(accountingCode!=null){
					districtAccountingCode = districtAccountingCodeDAO.getDistrictAccountingCode(districtMaster, accountingCode);
					System.out.println("districtAccountingCode ************* size: "+districtAccountingCode);
				}else{
					responseStatus = false;
					errorCode = 10008;
					errorMsg = "Accounting Code is required";
				}

				if(accountingCode.equals("")){
					responseStatus = false;
					errorCode = 10008;
					errorMsg = "Accounting Code is required.";
				}
				else if(name.equals("")){
					responseStatus = false;
					errorCode = 10010;
					errorMsg = "Name is required.";
				}
				if(action.equalsIgnoreCase("add") || action.equalsIgnoreCase("change"))
				{
					if(districtAccountingCode!=null && action.equalsIgnoreCase("add"))
					{
						responseStatus = false;
						errorCode = 10010;
						errorMsg = "Duplicate Accounting Code.";
					}else if(districtAccountingCode==null && action.equalsIgnoreCase("change"))
					{
						responseStatus = false;
						errorCode = 10005;
						errorMsg = "Invalid Accounting Code "+accountingCode;
					}
					
					
					if(responseStatus)
					{
						if(districtAccountingCode==null)
						{
							districtAccountingCode = new DistrictAccountingCode();
						}


						districtAccountingCode.setDistrictMaster(districtMaster);
						districtAccountingCode.setIpAddress(request.getRemoteAddr());
						districtAccountingCode.setAccountingCode(accountingCode);
						districtAccountingCode.setName(name);
						
						if(action.equalsIgnoreCase("add"))
							districtAccountingCode.setStatus("A");

						districtAccountingCodeDAO.makePersistent(districtAccountingCode);
						errorCode = 0;
						errorMsg = "";
					}

					jsonResponse = new JSONObject();
					jsonResponse.put("status", true);
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
					jsonResponse.put("errorCode", errorCode);
					jsonResponse.put("errorMessage", errorMsg);
				}
				else if(action.equalsIgnoreCase("delete") && districtAccountingCode!=null)
				{
					districtAccountingCodeDAO.makeTransient(districtAccountingCode);
					
					jsonResponse = new JSONObject();
					jsonResponse.put("status", true);
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
					jsonResponse.put("errorCode", 0);
					jsonResponse.put("errorMessage", "");
				}
				else if(action.equalsIgnoreCase("delete") && districtAccountingCode==null)
				{
					jsonResponse = new JSONObject();
					jsonResponse.put("status", true);
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
					jsonResponse.put("errorCode", 10005);
					jsonResponse.put("errorMessage", "Invalid Accounting Code "+accountingCode);
				}else
				{
					jsonResponse = new JSONObject();
					jsonResponse.put("status", false);
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
					jsonResponse.put("errorCode", errorCode);
					jsonResponse.put("errorMessage", errorMsg);
				}
			}
			else{
				jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
			}	
			if(jsonResponse!=null)
				returnVal = jsonResponse.toString();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.status(200).entity(returnVal.toString()).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/hiredCandidateQuery")
	public Response hiredCandidateQuery(InputStream incomingData,@Context HttpServletRequest request, @Context HttpServletResponse res) 
	{	
		System.out.println(":::::::::::::::::::::::::::################::::::::::hiredCandidateQuery:::::::::::::::::::");
		SessionFactory sessionFactory;
		String returnVal = "";
		try
		{
			ServletContext context = request.getSession().getServletContext();
			ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
			districtMasterDAO = (DistrictMasterDAO)context0.getBean("districtMasterDAO");
			teacherStatusHistoryForJobDAO=(TeacherStatusHistoryForJobDAO)context0.getBean("teacherStatusHistoryForJobDAO");
			teacherPersonalInfoDAO=(TeacherPersonalInfoDAO)context0.getBean("teacherPersonalInfoDAO");
			raceMasterDAO=(RaceMasterDAO)context0.getBean("raceMasterDAO");
			sapCandidateDetailsDAO = (SapCandidateDetailsDAO)context0.getBean("sapCandidateDetailsDAO");
			jobForTeacherDAO = (JobForTeacherDAO)context0.getBean("jobForTeacherDAO");
			userMasterDAO = (UserMasterDAO)context0.getBean("userMasterDAO");
			sessionFactory = sapCandidateDetailsDAO.getSessionFactory();
			StatelessSession statelessSsession = sessionFactory.openStatelessSession();
			
			ArrayList<TeacherDetail> teacherdetailList = new ArrayList<TeacherDetail>();
			ArrayList<JobOrder> jobOrdersList = new ArrayList<JobOrder>();
			
			String errorCode = "";
			String errorMsg = "";
			DistrictMaster districtMaster = null;
			String authKey=request.getParameter("authKey");
			String date=request.getParameter("date");
			Boolean credential=false;
			if(authKey!=null){

				districtMaster = districtMasterDAO.validateDistrictByAuthkey(authKey);
				if(districtMaster!=null){
					System.out.println("********District Validated********"+districtMaster);
				}else{
					credential=true;
					errorCode = "10038";
					errorMsg = "Invalid Credentials.";
				}
			}else{
				credential=true;
				errorCode = "10038";
				errorMsg = "Invalid Credentials.";
			}
			
			if(!credential && (date!=null && !date.matches("([0-9]{4})([0-9]{2})([0-9]{2})")) )
			{
				errorCode = "10038";
				errorMsg = "Invalid date format";
			}
			JSONObject jsonResponse=null;
			jsonResponse=new JSONObject();
			jsonResponse.put("status", true);
			jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
			jsonResponse.put("errorCode", errorCode);
			jsonResponse.put("errorMessage", errorMsg);
			if(date!=null && !date.matches("([0-9]{4})([0-9]{2})([0-9]{2})") || credential)
			{
				jsonResponse.put("status", false);
				System.out.println(jsonResponse);
				return Response.status(200).entity(jsonResponse.toString()).build();
			}
			if(date!=null)
			date=Utility.getDateFormat(request.getParameter("date"));
			//List<TeacherDetail> list=teacherStatusHistoryForJobDAO.getTeacherDetailByHireddDate(districtMaster,date);
			String[] statuss = new String[2]; 
			statuss[0] = "hird";
			statuss[1] = "vcomp";
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			lstStatusMasters = Utility.getStaticMasters(statuss);
			
			/*//List<SapCandidateDetails> listSentSapCandidateDetails = sapCandidateDetailsDAO.findSentSapCandidateDetails(806900);
			List<TeacherDetail> listSentTeacherDetail = new ArrayList<TeacherDetail>();
			System.out.println("listSentSapCandidateDetails===="+listSentSapCandidateDetails);
			
			for(SapCandidateDetails sp:listSentSapCandidateDetails){
				listSentTeacherDetail.add(sp.getTeacherDetail());
			}
			*/
			
			//List list = teacherStatusHistoryForJobDAO.getTeachersByHireddDate(districtMaster,date,lstStatusMasters,listSentTeacherDetail);
			List list = sapCandidateDetailsDAO.findPendingSapCandidateDetails(806900);
			List<Integer> teacherList=new ArrayList<Integer>();
			//System.out.println("ppppppppppppppppppppppppppppppp: "+list.size());
			TeacherDetail teacherDetail = null;
			if(list!=null && list.size()>0){
				for(Object oo: list){
					Object obj[] 	= (Object[])oo;
					System.out.println("llllllllllll "+obj[0].toString());
					teacherDetail 	= (TeacherDetail) obj[0];
					teacherList.add(teacherDetail.getTeacherId());
				}
			}
			List<TeacherPersonalInfo> personalInfoList=new ArrayList<TeacherPersonalInfo>();
			if(!teacherList.isEmpty())
			{
				personalInfoList=teacherPersonalInfoDAO.findByCriteria(Restrictions.in("teacherId", teacherList));
			}
			
			Map<Integer, TeacherPersonalInfo> personalInfoMap=new HashMap<Integer, TeacherPersonalInfo>();
			for(TeacherPersonalInfo personalInfo:personalInfoList)
			{
				personalInfoMap.put(personalInfo.getTeacherId(), personalInfo);
			}
			
			
			if(list!=null)
			{
				
				//Gaurav Kumar
				/*List<SapCandidateDetails> listSapCandidateDetails = sapCandidateDetailsDAO.getSapCandidateDetails(teacherList);
				if(listSapCandidateDetails!=null){
					
				}*/
				Integer districtId=806900;
				sapCandidateDetailsDAO.updateSapCandidateStatus(teacherList,806900);
				
				//End
				JSONArray candidateList=new JSONArray();
				
				for(Object oo: list){
					Object obj[] 	= (Object[])oo;
					teacherDetail 	= (TeacherDetail) obj[0];
					JobOrder jobOrder = (JobOrder)obj[1];
					System.out.println("------------------------------- "+jobOrder.getJobId());
					JSONObject candidate=new JSONObject();
					candidate.put("firstName", teacherDetail.getFirstName()==null?"":teacherDetail.getFirstName());
					candidate.put("middleName", "");
					candidate.put("lastName", teacherDetail.getLastName()==null?"":teacherDetail.getLastName());
					candidate.put("address1", personalInfoMap.get(teacherDetail.getTeacherId()).getAddressLine1()==null?"":personalInfoMap.get(teacherDetail.getTeacherId()).getAddressLine1());
					if(personalInfoMap.get(teacherDetail.getTeacherId()).getCityId()!=null)
						candidate.put("city", personalInfoMap.get(teacherDetail.getTeacherId()).getCityId().getCityName()==null?"":personalInfoMap.get(teacherDetail.getTeacherId()).getCityId().getCityName());
					else
						candidate.put("city", "");
					if(personalInfoMap.get(teacherDetail.getTeacherId()).getStateId()!=null)
						candidate.put("state", personalInfoMap.get(teacherDetail.getTeacherId()).getStateId().getStateName()==null?"":personalInfoMap.get(teacherDetail.getTeacherId()).getStateId().getStateName());
					else
						candidate.put("state", "");
					candidate.put("zipCode", personalInfoMap.get(teacherDetail.getTeacherId()).getZipCode()==null?"":personalInfoMap.get(teacherDetail.getTeacherId()).getZipCode());
					candidate.put("birthDate", personalInfoMap.get(teacherDetail.getTeacherId()).getDob()==null?"":Utility.getDateWithoutTime(personalInfoMap.get(teacherDetail.getTeacherId()).getDob()));
					candidate.put("phone", personalInfoMap.get(teacherDetail.getTeacherId()).getPhoneNumber());
					candidate.put("mobile", personalInfoMap.get(teacherDetail.getTeacherId()).getMobileNumber()==null?"":personalInfoMap.get(teacherDetail.getTeacherId()).getMobileNumber());
					candidate.put("jobId", jobOrder.getJobId());
					candidate.put("jobCode", jobOrder.getApiJobId()==null?"":jobOrder.getApiJobId());
					candidate.put("accountCode", jobOrder.getAccountCode()==null?"":jobOrder.getAccountCode());
					candidate.put("sal_admin_plan", jobOrder.getSalaryAdminPlan()==null?"":jobOrder.getSalaryAdminPlan());
					candidate.put("email_address", teacherDetail.getEmailAddress());
					
					System.out.println("::::::::::::::::::::Employee Number::::::::::::::######"+personalInfoMap.get(teacherDetail.getTeacherId()).getEmployeeNumber());
					candidate.put("empid",  personalInfoMap.get(teacherDetail.getTeacherId()).getEmployeeNumber()==null?"": personalInfoMap.get(teacherDetail.getTeacherId()).getEmployeeNumber());
					JSONArray ethniArray = new JSONArray();
					TeacherPersonalInfo teacherPersonalInfo = personalInfoMap.get(teacherDetail.getTeacherId());
					if(teacherPersonalInfo!=null)
					{
						String race = teacherPersonalInfo.getRaceId()==null?"":teacherPersonalInfo.getRaceId();
						String raceIds[] = race.split(",");
						if(raceIds.length>0)
						{
							List<Integer> races = new ArrayList<Integer>();
							for (int i = 0; i < raceIds.length; i++) {
								try{
									races.add(Integer.parseInt(raceIds[i]));
								}catch(Exception e){
									e.printStackTrace();
								}
							}
							List<RaceMaster> raceMasters = raceMasterDAO.findRacesByRaceCodeByOrder(races);
							if(raceMasters!=null && raceMasters.size()>0){
								for (RaceMaster raceMaster : raceMasters) {
									ethniArray.add(raceMaster.getRaceName());
								}
							}
						}
						
					}
					candidate.put("ethnicity", ethniArray);
					candidate.put("hours_per_day", jobOrder.getHoursPerDay()==null?"":jobOrder.getHoursPerDay());
					String jStatus = "";
					Integer jobStatus = jobOrder.getJobApplicationStatus()==null?0:jobOrder.getJobApplicationStatus(); 
					if(jobStatus==0)
						jStatus = "";
					else if(jobStatus==1)
						jStatus = "Regular";
					else if(jobStatus==2)
						jStatus = "Temporary";
					
					candidate.put("jobStatus", jStatus);
					
					candidateList.add(candidate);
					teacherdetailList.add(teacherDetail);
					jobOrdersList.add(jobOrder);
				}
				List<JobForTeacher> listJobForTeachersList = jobForTeacherDAO.getJobForTeachersByJobAndTeacher(jobOrdersList,teacherdetailList);
				UserMaster userMaster = userMasterDAO.findById(new Integer(1), false, false);
				List<StatusMaster> lstStatusMaster=WorkThreadServlet.statusMasters;
				StatusMaster statusMasterHIRD= findStatusByShortName(lstStatusMaster,"hird");
				Transaction t = statelessSsession.beginTransaction();
				for(JobForTeacher jft:listJobForTeachersList){
					System.out.println("######JobForteacher ID====="+jft.getJobForTeacherId());
					jft.setStatus(statusMasterHIRD);
					jft.setStatusMaster(statusMasterHIRD);
					jft.setUpdatedBy(userMaster);
					jft.setUpdatedByEntity(userMaster.getEntityType());		
					jft.setUpdatedDate(new Date());
					jft.setLastActivity(statusMasterHIRD.getStatus());
					jft.setLastActivityDate(new Date());
					jft.setOfferAccepted(true);
					jft.setOfferAcceptedDate(new Date());
					/*if(jft.getJobId().getRequisitionNumber()!=null && !(jft.getJobId().getRequisitionNumber()).trim().isEmpty())
					jft.setRequisitionNumber(jft.getJobId().getRequisitionNumber());
					else*/
					jft.setRequisitionNumber(null);
					
					if(jft.getJobId().getSchool()!=null && jft.getJobId().getSchool().size()>0)
					jft.setSchoolMaster(jft.getJobId().getSchool().get(0));
					statelessSsession.update(jft);

					//Insert History for teacher (TeacherStatusHistoryForJob)
					TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
					tSHJ.setTeacherDetail(teacherDetail);
					tSHJ.setJobOrder(jft.getJobId());
					tSHJ.setStatusMaster(statusMasterHIRD);
					tSHJ.setStatus("A");
					tSHJ.setCreatedDateTime(new Date());
					tSHJ.setUserMaster(userMaster);
					statelessSsession.insert(tSHJ);

					//Insert Notes for teacher (TeacherStatusNotes)
					TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
					teacherStatusNoteObj.setTeacherDetail(teacherDetail);
					teacherStatusNoteObj.setJobOrder(jft.getJobId());

					teacherStatusNoteObj.setStatusMaster(statusMasterHIRD);
					teacherStatusNoteObj.setUserMaster(userMaster);
					teacherStatusNoteObj.setDistrictId(jft.getJobId().getDistrictMaster().getDistrictId());
					teacherStatusNoteObj.setStatusNotes(null);
					teacherStatusNoteObj.setStatusNoteFileName(null);
					teacherStatusNoteObj.setEmailSentTo(1);
					//teacherStatusNoteObj.setFinalizeStatus(null);
					//teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
					statelessSsession.insert(teacherStatusNoteObj);
				
				}
				t.commit();
				statelessSsession.close();
				jsonResponse.put("candidateList", candidateList);
			}
				//System.out.println(jsonResponse);
			return	Response.status(200).entity(jsonResponse.toString()).build();
				
				
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.status(200).entity(returnVal.toString()).build();
	}
	//InputStream incomingData,@Context HttpServletRequest request
	/*@POST
	@Path("/receiveBackgroundCheckResponse")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	 public Response uploadFile(@FormDataParam("file") InputStream fileInputStream,
		            @FormDataParam("file") FormDataContentDisposition contentDispositionHeader) {
		        String filePath = "" + contentDispositionHeader.getFileName();

		        // save the file to the server

		        //saveFile(fileInputStream, filePath);

		        String output = "File saved to server location : " ;
		        return Response.status(200).entity(output).build();
	}*/
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/doNotHireUpdate")
	public Response doNotHireUpdate(String json,@Context HttpServletRequest request, @Context HttpServletResponse res) 
	{	
		String returnVal = "";

		try{
			
			String payloadRequest = json;
			ServletContext context = request.getSession().getServletContext();
			ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
			EmployeeMasterDAO employeeMasterDAO = (EmployeeMasterDAO)context0.getBean("employeeMasterDAO");
			districtMasterDAO = (DistrictMasterDAO)context0.getBean("districtMasterDAO");
			HttpSession session  = request.getSession();
			Boolean responseStatus = false;
			Integer errorCode = 10001;
			String errorMsg = "Invalid Credentials.";

			System.out.println("payloadRequest::::::= "+payloadRequest);
			DistrictMaster districtMaster = null;
			JSONObject jsonRequest = null;
			jsonRequest = (JSONObject) JSONSerializer.toJSON( payloadRequest ); 
			String authKey = jsonRequest.get("authKey")==null?"":UtilityAPI.parseString(jsonRequest.getString("authKey")).replace(' ', '+');


			if(authKey!=null){

				districtMaster = districtMasterDAO.validateDistrictByAuthkey(authKey);
				if(districtMaster!=null){
					session.setAttribute("apiDistrictMaster", districtMaster);
					responseStatus = true;
					System.out.println("********District Validated********"+districtMaster);
				}else{
					responseStatus = false;
					errorCode = 10001;
					errorMsg = "Invalid Credentials.";
				}
			}else{
				responseStatus = false;
				errorCode = 10001;
				errorMsg = "Invalid Credentials.";
			}
			JSONObject jsonResponse = null;
			//returnVal = payloadRequest;
			String EMPLID = "";
			String firstName = "";
			String lastName = "";
			String middleName = "";
			String emailAddress = "";
			String LAST_FOUR_SSN = "";
			String BIRTHDATE = "";
			String D12_NO_REHIRE = "";
			String EMP_STATUS_FLAG="";
			String fECode ="";
			EmployeeMaster employeeMaster = null;
			Date dob = null;

			if(responseStatus){

				System.out.println("Enter in try");				
				EMPLID = jsonRequest.get("EMPLID")==null?"":UtilityAPI.parseString(jsonRequest.getString("EMPLID")).trim();
				firstName = jsonRequest.get("firstName")==null?"":UtilityAPI.parseString(jsonRequest.getString("firstName")).trim();
				lastName = jsonRequest.get("lastName")==null?"":UtilityAPI.parseString(jsonRequest.getString("lastName")).trim();
				middleName = jsonRequest.get("middleName")==null?"":UtilityAPI.parseString(jsonRequest.getString("middleName")).trim();
				emailAddress = jsonRequest.get("emailAddress")==null?"":UtilityAPI.parseString(jsonRequest.getString("emailAddress")).trim();
				LAST_FOUR_SSN = jsonRequest.get("LAST_FOUR_SSN")==null?"":UtilityAPI.parseString(jsonRequest.getString("LAST_FOUR_SSN")).trim();
				BIRTHDATE = jsonRequest.get("BIRTHDATE")==null?"":UtilityAPI.parseString(jsonRequest.getString("BIRTHDATE")).trim();
				D12_NO_REHIRE = jsonRequest.get("D12_NO_REHIRE")==null?"":UtilityAPI.parseString(jsonRequest.getString("D12_NO_REHIRE")).trim();
				String action = jsonRequest.get("action")==null?"":UtilityAPI.parseString(jsonRequest.getString("action")).replace(' ', '+');
				EMP_STATUS_FLAG = jsonRequest.get("EMP_STATUS_FLAG")==null?"":UtilityAPI.parseString(jsonRequest.getString("EMP_STATUS_FLAG")).trim();
				boolean isDuplicate=false;
				
				if(action.equals("")){
					responseStatus = false;
					errorCode = 10018;
					errorMsg = "action is required.";
				 }else if(!(action.equalsIgnoreCase("add") || action.equalsIgnoreCase("change") || action.equalsIgnoreCase("delete"))){
					responseStatus = false;
					errorCode = 10019;
					errorMsg = "Invalid action";
				}
				 
				if(action.equalsIgnoreCase("add") || action.equalsIgnoreCase("change")){
	
				if(EMPLID!=null && !EMPLID.isEmpty()){
					System.out.println("District Id:"+districtMaster.getDistrictId());
					System.out.println("#########:::::::::::EMPLID"+EMPLID);
					employeeMaster = employeeMasterDAO.checkEmployeeByEmployeeCode(EMPLID,districtMaster);
					System.out.println("employeeMaster ************* size: "+employeeMaster);
				}

				if(employeeMaster==null){
					try{					
						System.out.println("BIRTHDATE"+BIRTHDATE);
						DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
						dob = (Date)formatter.parse(BIRTHDATE);
						System.out.println("################jobStartDate> "+dob);
						//dobVar = formatter.format(BIRTHDATE);
					} 
					catch (Exception e) {
						dob = null;						
					}
					employeeMaster = employeeMasterDAO.checkEmployeeSSNAndDob(LAST_FOUR_SSN,dob);
					
					if(employeeMaster!=null){
						
						isDuplicate = true;
					}
				}else{
					
					isDuplicate = true;
				}
				System.out.println("gggggggggggg: "+Utility.validateEmailAddress(emailAddress));
				
			   /*if(EMPLID.equals("")){
					responseStatus = false;
					errorCode = 10008;
					errorMsg = "EMPLID is required.";
				}
				else if(firstName.equals("")){
					responseStatus = false;
					errorCode = 10009;
					errorMsg = "firstName is required.";
				}*/
				System.out.println("isDuplicate====="+isDuplicate);
				
				if(!isDuplicate || action.equalsIgnoreCase("change")){
				
				if(firstName.equals("")){
						responseStatus = false;
						errorCode = 10009;
						errorMsg = "firstName is required.";
				}
				else if(lastName.equals("")){
					responseStatus = false;
					errorCode = 10010;
					errorMsg = "lastName is required.";
				}
				else if(emailAddress.equals("")){
					responseStatus = false;
					errorCode = 10011;
					errorMsg = "emailAddress is required.";
				}
				else if(!Utility.validateEmailAddress(emailAddress)){
					responseStatus = false;
					errorCode = 10011;
					errorMsg = "Valid emailAddress is required.";
				}//
				else if(LAST_FOUR_SSN.equals("")){
					responseStatus = false;
					errorCode = 10012;
					errorMsg = "LAST_FOUR_SSN is required.";
				}
				else if(!LAST_FOUR_SSN.matches("\\d+")){
					responseStatus = false;
					errorCode = 10012;
					errorMsg = "LAST_FOUR_SSN must be numeric.";
				}
				else if(LAST_FOUR_SSN.length()!=4){
					responseStatus = false;
					errorCode = 10012;
					errorMsg = "LAST_FOUR_SSN length must be 4.";
				}
				else if(BIRTHDATE.equals("")){
					responseStatus = false;
					errorCode = 10013;
					errorMsg = "BIRTHDATE is required.";
				}else if(D12_NO_REHIRE.equals("")){
					responseStatus = false;
					errorCode = 10014;
					errorMsg = "D12_NO_REHIRE is required.";
				}else if(!(D12_NO_REHIRE.equalsIgnoreCase("y") || D12_NO_REHIRE.equalsIgnoreCase("n"))){
					responseStatus = false;
					errorCode = 10015;
					errorMsg = "Invalid D12_NO_REHIRE.";
				}else if(EMP_STATUS_FLAG.equals("")){
					responseStatus = false;
					errorCode = 10016;
					errorMsg = "EMP_STATUS_FLAG is required.";
				}else if(!(EMP_STATUS_FLAG.equalsIgnoreCase("c") || EMP_STATUS_FLAG.equalsIgnoreCase("f"))){
					responseStatus = false;
					errorCode = 10017;
					errorMsg = "Invalid EMP_STATUS_FLAG";
				}
				System.out.println("::::::::responseStatus"+responseStatus);
				//DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			
				try{					
					System.out.println("BIRTHDATE"+BIRTHDATE);
					dob = (Date)formatter.parse(BIRTHDATE);
					System.out.println("jobStartDate> "+dob);
				} 
				catch (Exception e) {
					dob = null;						
				}
				
				if(dob==null){
					responseStatus = false;
					errorCode = 10015;
					errorMsg = "Invalid BIRTHDATE.";
				}
					
				if(responseStatus)
				{
					if(employeeMaster==null)
					{
						employeeMaster = employeeMasterDAO.checkEmployeeSSNAndDob(LAST_FOUR_SSN,dob);
						if(employeeMaster==null){
							employeeMaster = new EmployeeMaster();
							employeeMaster.setStatus("A");
						}
					}
					if(EMP_STATUS_FLAG.equalsIgnoreCase("c"))
						fECode ="1";
					
					if(EMP_STATUS_FLAG.equalsIgnoreCase("f"))
						fECode ="0";
					employeeMaster.setEmployeeCode(EMPLID);
					employeeMaster.setFirstName(firstName);
					employeeMaster.setMiddleName(middleName);
					employeeMaster.setLastName(lastName);
					employeeMaster.setEmailAddress(emailAddress);
					employeeMaster.setSSN(LAST_FOUR_SSN);
					employeeMaster.setDob(dob);
					employeeMaster.setDistrictMaster(districtMaster);
					employeeMaster.setPCCode(D12_NO_REHIRE);
					employeeMaster.setCreatedDateTime(new Date());
					employeeMaster.setFECode(fECode);
					employeeMasterDAO.makePersistent(employeeMaster);
					
					jsonResponse = new JSONObject();
					jsonResponse.put("status", true);
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
					jsonResponse.put("errorCode", "");
					jsonResponse.put("errorMessage", "");
			   }else{
					jsonResponse = new JSONObject();
					jsonResponse.put("status", true);
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
					jsonResponse.put("errorCode", errorCode);
					jsonResponse.put("errorMessage", errorMsg);
			   }
			   }else{
				    responseStatus = false;
					errorCode = 10016;
					errorMsg = "Employee already exists";
					jsonResponse = new JSONObject();
					jsonResponse.put("status", true);
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
					jsonResponse.put("errorCode", errorCode);
					jsonResponse.put("errorMessage", errorMsg);
			   }
			  }else if(action.equalsIgnoreCase("delete")){  
				  if(!EMPLID.isEmpty())
				  employeeMaster = employeeMasterDAO.checkEmployeeByEmployeeCode(EMPLID,districtMaster);
				  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
					
					try{					
						System.out.println("BIRTHDATE"+BIRTHDATE);
						dob = (Date)formatter.parse(BIRTHDATE);
						System.out.println("################jobStartDate> "+dob);
						//dobVar = formatter.format(BIRTHDATE);
					} 
					catch (Exception e) {
						dob = null;						
					}
				  if(employeeMaster==null)
					{
						employeeMaster = employeeMasterDAO.checkEmployeeSSNAndDob(LAST_FOUR_SSN,dob);
					
					}
				  if(employeeMaster!=null){
						employeeMasterDAO.makeTransient(employeeMaster);
						jsonResponse = new JSONObject();
						jsonResponse.put("status", true);
						jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
						jsonResponse.put("errorCode", 0);
						jsonResponse.put("errorMessage", "");
				  }else
					{
						jsonResponse = new JSONObject();
						jsonResponse.put("status", true);
						jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
						jsonResponse.put("errorCode", 10017);
						jsonResponse.put("errorMessage", "Invalid Employee");
					}
				}
			  else{
				  jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
		       }
			}
			else{
				jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
			}	
			if(jsonResponse!=null)
				returnVal = jsonResponse.toString();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.status(200).entity(returnVal.toString()).build();
	}
	public StatusMaster findStatusByShortName(List <StatusMaster> statusMasterList,String statusShortName)
	{
		StatusMaster status= null;
		try 
		{   if(statusMasterList!=null)
			for(StatusMaster statusMaster: statusMasterList){
				if(statusMaster.getStatusShortName().equalsIgnoreCase(statusShortName)){
					status=statusMaster;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(status==null)
			return null;
		else
			return status;
	}	
	
}