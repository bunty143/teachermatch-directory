package tm.api.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictPortfolioConfig;
import tm.bean.EpiTakenList;
import tm.bean.InternalTransferCandidates;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAffidavit;
import tm.bean.TeacherAnswerDetail;
import tm.bean.TeacherAssessmentAttempt;
import tm.bean.TeacherAssessmentOption;
import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherSectionDetail;
import tm.bean.TeacherStrikeLog;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentGroupDetails;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.assessment.AssessmentQuestions;
import tm.bean.assessment.AssessmentSections;
import tm.bean.assessment.QuestionOptions;
import tm.bean.assessment.QuestionsPool;
import tm.bean.assessment.TeacherWiseAssessmentTime;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.OptionsForDistrictSpecificQuestions;
import tm.bean.master.QuestionTypeMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.TeacherAnswerDetailsForDistrictSpecificQuestions;
import tm.bean.user.UserMaster;
import tm.dao.CandidateRawScoreDAO;
import tm.dao.EpiTakenListDAO;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAffidavitDAO;
import tm.dao.TeacherAnswerDetailDAO;
import tm.dao.TeacherAssessmentAttemptDAO;
import tm.dao.TeacherAssessmentDetailDAO;
import tm.dao.TeacherAssessmentOptionDAO;
import tm.dao.TeacherAssessmentQuestionDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherSectionDetailDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.TeacherStrikeLogDAO;
import tm.dao.assessment.AssessmentCompetencyScoreDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentDomainScoreDAO;
import tm.dao.assessment.AssessmentGroupDetailsDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.assessment.AssessmentQuestionsDAO;
import tm.dao.assessment.AssessmentSectionDAO;
import tm.dao.assessment.QuestionsPoolDAO;
import tm.dao.assessment.TeacherWiseAssessmentTimeDAO;
import tm.dao.cgreport.JobCategoryWiseStatusPrivilegeDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.SchoolKeyContactDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.TeacherAnswerDetailsForDistrictSpecificQuestionsDAO;
import tm.dao.master.UserEmailNotificationsDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.AssessmentCampaignAjax;
import tm.services.CadidateRawScoreThread;
import tm.services.CommonService;
import tm.services.EmailerService;
import tm.services.MailSendToTeacher;
import tm.services.district.PrintOnConsole;
import tm.services.report.CGReportService;
import tm.services.teacher.DistrictPortfolioConfigAjax;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.TMCommonUtil;
import tm.utility.Utility;
public class APIAssessmentCampaignAjax {

	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;

	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	public void setAssessmentDetailDAO(AssessmentDetailDAO assessmentDetailDAO) {
		this.assessmentDetailDAO = assessmentDetailDAO;
	}

	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) {
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}

	@Autowired 
	private AssessmentSectionDAO assessmentSectionDAO;
	public void setAssessmentSectionDAO(AssessmentSectionDAO assessmentSectionDAO) {
		this.assessmentSectionDAO = assessmentSectionDAO;
	}

	@Autowired
	private AssessmentQuestionsDAO assessmentQuestionsDAO;
	public void setAssessmentQuestionsDAO(AssessmentQuestionsDAO assessmentQuestionsDAO) {
		this.assessmentQuestionsDAO = assessmentQuestionsDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private TeacherAssessmentDetailDAO teacherAssessmentDetailDAO;
	public void setTeacherAssessmentDetailDAO(TeacherAssessmentDetailDAO teacherAssessmentDetailDAO) {
		this.teacherAssessmentDetailDAO = teacherAssessmentDetailDAO;
	}

	@Autowired
	private TeacherSectionDetailDAO teacherSectionDetailDAO;
	public void setTeacherSectionDetailDAO(TeacherSectionDetailDAO teacherSectionDetailDAO) {
		this.teacherSectionDetailDAO = teacherSectionDetailDAO;
	}

	@Autowired
	private TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO;
	public void setTeacherAssessmentQuestionDAO(TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO) {
		this.teacherAssessmentQuestionDAO = teacherAssessmentQuestionDAO;
	}

	@Autowired
	private TeacherAssessmentOptionDAO teacherAssessmentOptionDAO;
	public void setTeacherAssessmentOptionDAO(TeacherAssessmentOptionDAO teacherAssessmentOptionDAO) {
		this.teacherAssessmentOptionDAO = teacherAssessmentOptionDAO;
	}

	@Autowired
	private TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO;
	public void setTeacherAssessmentAttemptDAO(TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO) {
		this.teacherAssessmentAttemptDAO = teacherAssessmentAttemptDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private TeacherAnswerDetailDAO teacherAnswerDetailDAO;
	public void setTeacherAnswerDetailDAO(TeacherAnswerDetailDAO teacherAnswerDetailDAO) {
		this.teacherAnswerDetailDAO = teacherAnswerDetailDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	public void setDistrictSpecificQuestionsDAO(
			DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO) {
		this.districtSpecificQuestionsDAO = districtSpecificQuestionsDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private TeacherStrikeLogDAO teacherStrikeLogDAO;
	public void setTeacherStrikeLogDAO(TeacherStrikeLogDAO teacherStrikeLogDAO) {
		this.teacherStrikeLogDAO = teacherStrikeLogDAO;
	}

	@Autowired
	private CandidateRawScoreDAO candidateRawScoreDAO;
	public void setCandidateRawScoreDAO(CandidateRawScoreDAO candidateRawScoreDAO) {
		this.candidateRawScoreDAO = candidateRawScoreDAO;
	}

	@Autowired
	private EpiTakenListDAO epiTakenListDAO;
	public void setEpiTakenListDAO(EpiTakenListDAO epiTakenListDAO) {
		this.epiTakenListDAO = epiTakenListDAO;
	}

	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}

	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}

	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) {
		this.districtKeyContactDAO = districtKeyContactDAO;
	}

	@Autowired
	private SchoolKeyContactDAO schoolKeyContactDAO;
	public void setSchoolKeyContactDAO(SchoolKeyContactDAO schoolKeyContactDAO) {
		this.schoolKeyContactDAO = schoolKeyContactDAO;
	}

	@Autowired
	private UserEmailNotificationsDAO userEmailNotificationsDAO;
	public void setUserEmailNotificationsDAO(
			UserEmailNotificationsDAO userEmailNotificationsDAO) {
		this.userEmailNotificationsDAO = userEmailNotificationsDAO;
	}

	@Autowired
	private CommonService commonService;

	@Autowired
	private TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO;

	@Autowired
	private DistrictPortfolioConfigAjax districtPortfolioConfigAjax;

	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private TeacherWiseAssessmentTimeDAO teacherWiseAssessmentTimeDAO;
	
	@Autowired
	private QuestionsPoolDAO questionsPoolDAO;
	
	@Autowired
	private CGReportService reportService;
	
	@Autowired
	private AssessmentGroupDetailsDAO assessmentGroupDetailsDAO;
	
	@Autowired
	private AssessmentDomainScoreDAO assessmentDomainScoreDAO;
	
	@Autowired
	private AssessmentCompetencyScoreDAO assessmentCompetencyScoreDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@Autowired
	private TeacherAffidavitDAO teacherAffidavitDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO;
	
	HttpServletRequest httpServletRequest = null;
	public void setHttpServletRequest(HttpServletRequest httpServletRequest){
		this.httpServletRequest = httpServletRequest;
	}
	
	@Autowired
	private AssessmentCampaignAjax assessmentCampaignAjax;

	/* @Author: Vishwanath Kumar
	 * @Discription: check Inventory.
	 */
	public AssessmentDetail checkInventory(JobOrder jobOrder, Integer epiJobId)
	{
		//System.out.println(":::::::::::::::Assessment checkInventory:::::::::::::::::::>>> ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = null;
		HttpSession session = null;
		if(httpServletRequest!=null && httpServletRequest.getSession()!=null){
			request = httpServletRequest;
			session = request.getSession(false);
		}
		else{
			request = context.getHttpServletRequest();
			session = request.getSession(false);
		}
		
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		TeacherDetail teacherDetail = null;

		if (session == null || session.getAttribute("apiTeacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");

		int newJobId = jobOrder.getJobId();
		if(jobOrder.getJobId()!=null && jobOrder.getJobId()>0)
			jobOrder = jobOrderDAO.findById(newJobId, false, false);

		AssessmentDetail assessmentDetail = new AssessmentDetail();
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		TeacherAssessmentStatus teacherAssessmentStatus = null;
		Integer assessmentTakenCount = null;
		teacherAssessmentStatusList=teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jobOrder);
		//////////////////////////////////////
		if(teacherAssessmentStatusList.size()==0 && newJobId!=0)
		{
			List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
			if(assessmentJobRelations1.size()>0)
			{
				AssessmentJobRelation assessmentJobRelation1 = assessmentJobRelations1.get(0);
				teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTakenByTeacher(teacherDetail,assessmentJobRelation1.getAssessmentId());
			}
		}
		/////////////////////////////////////
		int oldJobId = 0;
		if(teacherAssessmentStatusList.size()!=0)
		{
			teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
			assessmentDetail = teacherAssessmentStatus.getAssessmentDetail();
			assessmentTakenCount = teacherAssessmentStatus.getAssessmentTakenCount();
			String statusShortName=teacherAssessmentStatus.getStatusMaster().getStatusShortName();
			System.out.println("teacherAssessmentStatus:: "+teacherAssessmentStatus.getTeacherAssessmentStatusId());
			System.out.println("teacherId:: "+teacherAssessmentStatus.getTeacherDetail().getTeacherId());
			System.out.println("statusShortName: "+statusShortName);
			System.out.println("assessmentTakenCount:: "+assessmentTakenCount);
			
			List<TeacherAssessmentStatus> teacherAssessmentStatusListIfexist = null;

			if(teacherAssessmentStatus.getAssessmentType()==2)
			{
				teacherAssessmentStatusListIfexist=teacherAssessmentStatusDAO.findAssessmentTakenByTeacher(teacherDetail,assessmentDetail);
				TeacherAssessmentStatus teacherAssessmentStatus1 = teacherAssessmentStatusListIfexist.get(0);
				oldJobId = teacherAssessmentStatus1.getJobOrder().getJobId();
				if(newJobId!=oldJobId)
				{
					jobOrder = teacherAssessmentStatus1.getJobOrder();
					//oldJobId = jobOrder.getJobId();
				}else
					oldJobId=0;

			}

			if(statusShortName.equalsIgnoreCase("comp"))
			{
				assessmentDetail.setAssessmentName(null);
				assessmentDetail.setStatus("3");
				return assessmentDetail;
			}else if(statusShortName.equalsIgnoreCase("exp"))
			{
				assessmentDetail.setAssessmentName(null);
				assessmentDetail.setStatus("4");
				return assessmentDetail;
			}
			else if(statusShortName.equalsIgnoreCase("vlt"))
			{
				assessmentDetail.setAssessmentName(null);
				assessmentDetail.setStatus("5");
				return assessmentDetail;
			}
			else if(statusShortName.equalsIgnoreCase("icomp"))
			{
				List<TeacherAssessmentAttempt> teacherAssessmentAttempts = null;
				teacherAssessmentAttempts = teacherAssessmentAttemptDAO.findAssessmentAttempts(teacherDetail,jobOrder);
				int attempts = 0;
				for (TeacherAssessmentAttempt teacherAssessmentAttempt1 : teacherAssessmentAttempts) {
					if(!teacherAssessmentAttempt1.getIsForced())
						attempts++;
				}
				////////////////////////

				int strikeLogs=0; 
				try{
					if(teacherAssessmentAttempts.size()>0)
					{
						List<TeacherAssessmentStatus> teacherAssessmentStatusList2 = null;
						teacherAssessmentStatusList2 = teacherAssessmentStatusDAO.getTeacherAssessmentStatus(teacherAssessmentAttempts.get(0).getAssessmentDetail(),teacherDetail);
						List<TeacherStrikeLog> teacherStrikeLog = null;
						teacherStrikeLog = teacherStrikeLogDAO.getTotalStrikesByTeacher(teacherDetail, teacherAssessmentStatusList2.get(0).getTeacherAssessmentdetail());
						strikeLogs=teacherStrikeLog.size();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				int attemptsStrike=attempts+strikeLogs;

				if(attemptsStrike==3)
				{
					StatusMaster statusMaster = WorkThreadServlet.statusMap.get("vlt");
					TeacherAssessmentdetail teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();

					List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
					lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentdetail);
					teacherAssessmentStatus=lstTeacherAssessmentStatus.get(0);

					List<JobOrder> jobs = new ArrayList<JobOrder>();

					if(lstTeacherAssessmentStatus.size()!=0)
						if(teacherAssessmentStatus!=null)
						{
							for(TeacherAssessmentStatus tas: lstTeacherAssessmentStatus)
							{
								jobs.add(tas.getJobOrder());

								tas.setStatusMaster(statusMaster);
								teacherAssessmentStatusDAO.makePersistent(tas);
							}
						}
					//teacherAssessmentStatus

					// JobForTeacher update after 3 attempts
					if(jobOrder.getJobId()!=null && jobOrder.getJobId()>0)
					{
						List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByAssessment(teacherAssessmentStatus.getAssessmentDetail());

						for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations1) {
							jobs.add(assessmentJobRelation.getJobId());
						}

						List<JobForTeacher> jobForTeachers = jobForTeacherDAO.findJobByTeacherAndSatusFormJobIds(teacherDetail, jobs);

						String jftStatus = "";
						for(JobForTeacher jft: jobForTeachers)
						{
							jftStatus = jft.getStatus().getStatusShortName();
							if(!(jftStatus.equalsIgnoreCase("hird") || jftStatus.equalsIgnoreCase("widrw") || jftStatus.equalsIgnoreCase("hide")))
							{
								if(jft.getInternalSecondaryStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(null);
									jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
								}else if(jft.getInternalStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(jft.getInternalStatus());
								}else{
									if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
										jft.setStatus(statusMaster);
										try{
											if(jft.getSecondaryStatus()!=null){
												jft.setStatusMaster(null);
											}else{
												jft.setStatusMaster(statusMaster);
											}
										}catch(Exception e){
											jft.setStatusMaster(statusMaster);
											e.printStackTrace();
										}
									}
								}
								// New Check JSI optional
								if(jft.getJobId().getJobAssessmentStatus()==1)
								jobForTeacherDAO.makePersistent(jft);
							}
						}
					}else
					{
						// base taken status 
						StatusMaster epiStatus = null;
						JobOrder jjj = new JobOrder();
						jjj.setJobId(0);
						List<TeacherAssessmentStatus> baseStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jjj);
						if(baseStatusList.size()>0)
							epiStatus = baseStatusList.get(0).getStatusMaster();

							StatusMaster stautsIcomp= WorkThreadServlet.statusMap.get("icomp");
						///////////////////////// for external user /////////////////////////
						List<JobForTeacher> lstJobForTeacherIcomp = new ArrayList<JobForTeacher>();
						List<JobOrder> lstJBOrder = new ArrayList<JobOrder>();

						List<JobForTeacher> lstJobForTeacher= null;
						lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail,WorkThreadServlet.statusMap.get("icomp"));
						for(JobForTeacher jft: lstJobForTeacher)
						{
							if(jft.getJobId().getIsJobAssessment()==false)
							{
								String jftStatus = jft.getStatus().getStatusShortName();
								if(jft.getInternalSecondaryStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(null);
									jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
								}else if(jft.getInternalStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(jft.getInternalStatus());
								}else{
									if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
										jft.setStatus(statusMaster);
										try{
											if(jft.getSecondaryStatus()!=null){
												jft.setStatusMaster(null);
											}else{
												jft.setStatusMaster(statusMaster);
											}
										}catch(Exception e){
											jft.setStatusMaster(statusMaster);
											e.printStackTrace();
										}
									}
								}
								jobForTeacherDAO.makePersistent(jft);
							}
							else if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){//**##**
								lstJobForTeacherIcomp.add(jft);
								lstJBOrder.add(jft.getJobId());
							}
						}

						TeacherAssessmentStatus tAStatus = null;
						if(lstJobForTeacherIcomp.size()>0){
							List<TeacherAssessmentStatus> lstTAStatus = teacherAssessmentStatusDAO.findByTeacherAndListJob(teacherDetail, lstJBOrder);
							Map<Integer, TeacherAssessmentStatus> mapAssessStatus = new HashMap<Integer, TeacherAssessmentStatus>();
							for(TeacherAssessmentStatus tas: lstTAStatus){
								mapAssessStatus.put(tas.getJobOrder().getJobId(), tas);
							}

							for(JobForTeacher jft: lstJobForTeacherIcomp){
								tAStatus = mapAssessStatus.get(jft.getJobId().getJobId());
								if(tAStatus!=null){
									String jftStatus = jft.getStatus().getStatusShortName();
									if(jft.getInternalSecondaryStatus()!=null){
										jft.setStatus(jft.getInternalStatus());
										jft.setStatusMaster(null);
										jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
									}else if(jft.getInternalStatus()!=null){
										jft.setStatus(jft.getInternalStatus());
										jft.setStatusMaster(jft.getInternalStatus());
									}else{
										if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
											jft.setStatus(tAStatus.getStatusMaster());
											try{
												if(jft.getSecondaryStatus()!=null){
													jft.setStatusMaster(null);
												}else{
													jft.setStatusMaster(tAStatus.getStatusMaster());
												}
											}catch(Exception e){
												jft.setStatusMaster(tAStatus.getStatusMaster());
												e.printStackTrace();
											}	
										}
									}
									// New Check JSI optional
									if(jft.getJobId().getJobAssessmentStatus()==1)
									jobForTeacherDAO.makePersistent(jft);
								}else{
			                        try{
			                        	if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){
				                            if(epiStatus!=null)
				                            {
					                             jft.setStatusMaster(epiStatus);
					                             jft.setStatus(epiStatus);
					                             jobForTeacherDAO.makePersistent(jft);
				                            }else
				                            {
				                            	 jft.setStatusMaster(stautsIcomp);
					                             jft.setStatus(stautsIcomp);
					                             jobForTeacherDAO.makePersistent(jft);
				                            	
				                            }
			                             }
			                         }catch(Exception e){
			                        	 e.printStackTrace();
			                         }
		                        }
							}

						}
						/////////////////////////////////////////////////////////////////////
					}

				}

				try{
					TeacherPortfolioStatus teacherPortfolioStatus = null;
					TeacherAffidavit teacherAffidavit = null;
					
					if(teacherDetail!=null){
						teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
						teacherAffidavit = teacherAffidavitDAO.findAffidavitByTeacher(teacherDetail);
					}
					
					teacherPortfolioStatus.setIsAffidavitCompleted(true);
					teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
					
					teacherAffidavit.setAffidavitAccepted(true);
					teacherAffidavit.setIsDone(true);
					teacherAffidavit.setCreatedDateTime(new Date());
					teacherAffidavitDAO.makePersistent(teacherAffidavit);

				}catch(Exception e){
					e.printStackTrace();
				}
				
				
				//assessmentDetail.setStatus(assessmentDetail.getStatus()+"#"+attemptsStrike+"#"+oldJobId);
				assessmentDetail.setStatus(assessmentDetail.getStatus()+"#"+attemptsStrike+"#"+assessmentTakenCount+"#"+oldJobId);

				return assessmentDetail;
			}

		}
		List<AssessmentDetail> assessmentDetails = null;
		List<AssessmentJobRelation> assessmentJobRelations = null;

		AssessmentJobRelation assessmentJobRelation = null;
		// if assessment is not taken
		try {//for base
			if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0)
			{
				Criterion criterion = Restrictions.eq("status", "A");
				Criterion criterion1 = Restrictions.eq("assessmentType", 1);
				Criterion criterion2 = Restrictions.eq("isDefault", true);
				Criterion criterion3 = null;
				
				List<AssessmentGroupDetails> assessmentGroupDetailsList = null;
				assessmentGroupDetailsList = assessmentGroupDetailsDAO.findByCriteria(criterion,criterion1,criterion2);
				if(assessmentGroupDetailsList!=null && assessmentGroupDetailsList.size()>0)
					criterion3 = Restrictions.eq("assessmentGroupDetails", assessmentGroupDetailsList.get(0)); 
				
				AssessmentGroupDetails assessmentGroupDetails = null;
				if(epiJobId!=null && epiJobId>0){
					System.out.println("******************************* 1 API ******************************* : epiJobId : "+epiJobId);
					JobOrder epiJobOrder  = jobOrderDAO.findById(epiJobId, false, false);
					if(epiJobOrder!=null){
						System.out.println("******************************* 2 API ******************************* : epiJobOrder.getJobCategoryMaster().getJobCategoryId() : "+epiJobOrder.getJobCategoryMaster().getJobCategoryId());
						Integer assessmentGroupId = null;
						assessmentGroupId = epiJobOrder.getJobCategoryMaster().getAssessmentGroupId();
						if(assessmentGroupId!=null && assessmentGroupId>0){
							System.out.println("******************************* 3 API ******************************* : assessmentGroupId : "+assessmentGroupId);
							assessmentGroupDetails = assessmentGroupDetailsDAO.findById(assessmentGroupId, false, false);
							if(assessmentGroupDetails!=null)
								assessmentDetails = assessmentDetailDAO.getAssessmentDetailListByGroup(assessmentGroupDetails);
							else
								assessmentDetails = assessmentDetailDAO.findByCriteria(criterion,criterion1,criterion3);
						}
						else
							assessmentDetails = assessmentDetailDAO.findByCriteria(criterion,criterion1,criterion3);
					}
					else
						assessmentDetails = assessmentDetailDAO.findByCriteria(criterion,criterion1,criterion3);
				}
				else
					assessmentDetails = assessmentDetailDAO.findByCriteria(criterion,criterion1,criterion3);
				
				if(assessmentDetails==null)
					assessmentDetails = assessmentDetailDAO.findByCriteria(criterion,criterion1,criterion3);
				
				System.out.println("******************************* 4 API ******************************* : assessmentDetails.size() : "+assessmentDetails.size());
				
				List<AssessmentDetail> normalAssessmentDetail = new ArrayList<AssessmentDetail>();
				List<AssessmentDetail> researchAssessmentDetail = new ArrayList<AssessmentDetail>();

				for(AssessmentDetail assessmentDetail2 : assessmentDetails)
				{
					if(assessmentDetail2.getIsResearchEPI())
						researchAssessmentDetail.add(assessmentDetail2);
					else
						normalAssessmentDetail.add(assessmentDetail2);
				}
				Map<Boolean,EpiTakenList> epiTakenMap = new HashMap<Boolean, EpiTakenList>();
				EpiTakenList epiTakenList = null;
				List<EpiTakenList> epiTakenLists = null;
				epiTakenLists = epiTakenListDAO.getEpiTakenList(assessmentGroupDetails);
				if(epiTakenLists!=null && epiTakenLists.size()>0){
					for (EpiTakenList epiTakenList2 : epiTakenLists) {
						if(epiTakenList2.getIsResearchEPI())
							epiTakenMap.put(true, epiTakenList2);
						else
							epiTakenMap.put(false, epiTakenList2);
					}
				}
				int nadSize = normalAssessmentDetail.size();
				int radSize = researchAssessmentDetail.size();
				if(teacherDetail.getIsResearchTeacher()) //Research EPI
				{
					if(radSize==1)
					{
						assessmentDetail = researchAssessmentDetail.get(0);
						return assessmentDetail;
					}else if(radSize>1)
					{
						epiTakenList = epiTakenMap.get(true);
						if(epiTakenList!=null)
						{
							int indexId = 0;
							for (AssessmentDetail assessmentDetail2 : researchAssessmentDetail) {
								if(assessmentDetail2.getAssessmentId().equals(epiTakenList.getAssessmentId()))
									break;

								indexId++;
							}

							if((indexId+1)==radSize)
								assessmentDetail = researchAssessmentDetail.get(0);
							else
								assessmentDetail = researchAssessmentDetail.get(indexId+1);
						}else
							assessmentDetail = researchAssessmentDetail.get(0);

						return assessmentDetail;
					}else
					{
						assessmentDetail.setAssessmentName(null);
						assessmentDetail.setStatus("1");
						return assessmentDetail;
					}

				}else //normal EPI
				{
					if(nadSize==1)
					{
						assessmentDetail = normalAssessmentDetail.get(0);
						return assessmentDetail;
					}else if(nadSize>1)
					{
						epiTakenList = epiTakenMap.get(false);
						if(epiTakenList!=null)
						{
							int indexId = 0;
							for (AssessmentDetail assessmentDetail2 : normalAssessmentDetail) {
								if(assessmentDetail2.getAssessmentId().equals(epiTakenList.getAssessmentId()))
									break;

								indexId++;
							}

							if((indexId+1)==nadSize)
								assessmentDetail = normalAssessmentDetail.get(0);
							else
								assessmentDetail = normalAssessmentDetail.get(indexId+1);
							
							/*Map<Integer, AssessmentDetail> nadMap = new HashMap<Integer, AssessmentDetail>();
							for (AssessmentDetail assessmentDetail2 : normalAssessmentDetail) {
								nadMap.put(assessmentDetail2.getAssessmentId(), assessmentDetail2);
							}
							if(nadMap.containsKey(epiTakenList.getAssessmentId())){
								nadMap.remove(epiTakenList.getAssessmentId());
							}
							assessmentDetail = nadMap.get(nadMap.keySet().toArray()[0]);*/
							
						}else
							assessmentDetail = normalAssessmentDetail.get(0);

						return assessmentDetail;
					}else
					{
						assessmentDetail.setAssessmentName(null);
						assessmentDetail.setStatus("1");
						return assessmentDetail;
					}

				}

			}else if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()>0)
			{
				String jobStatus = jobOrder.getStatus();
				//System.out.println("---------------- jobStatus::::: "+jobStatus);
				// checking job stauts
				/*Date dateWithoutTime = Utility.getDateWithoutTime();
				Date a = jobOrder.getJobStartDate();
				Date b =jobOrder.getJobEndDate();   // assume these are set to something

				System.out.println("=====+++====+++======++====");
				System.out.println(a.compareTo(dateWithoutTime) * dateWithoutTime.compareTo(b) > 0);
				System.out.println("=====+++====+++======++====");*/

				if(jobStatus!=null && !jobStatus.equalsIgnoreCase("A"))
				{
					//job deativated
					assessmentDetail.setAssessmentName(null);
					assessmentDetail.setStatus("6");
					return assessmentDetail;
				}else
				{
					//System.out.println("(((((((((((((((((((( "+Utility.checkDateBetweenTwoDate(jobOrder.getJobStartDate(), jobOrder.getJobEndDate()));
					if(!Utility.checkDateBetweenTwoDate(jobOrder.getJobStartDate(), jobOrder.getJobEndDate()))
					{
						//job expired
						assessmentDetail.setAssessmentName(null);
						assessmentDetail.setStatus("7");
						return assessmentDetail;
					}

				}
				assessmentJobRelations=assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
				if(assessmentJobRelations.size()!=0)
				{
					assessmentJobRelation = assessmentJobRelations.get(0);
					assessmentDetail = assessmentJobRelation.getAssessmentId();
					if(assessmentDetail.getStatus().equalsIgnoreCase("A"))
					{
						List<AssessmentQuestions> assessmentQuestions = assessmentQuestionsDAO.getAssessmentQuestions(assessmentDetail);
						if(assessmentQuestions!=null && assessmentQuestions.size()>0)
						{
							return assessmentDetail;
						}
						else
						{
							assessmentDetail.getAssessmentDescription();
							assessmentDetail.setAssessmentName(null);
							assessmentDetail.setStatus("2"); // JSI has no questions
							return assessmentDetail;
						}
					}
					else{
						assessmentDetail.setAssessmentName(null);
						assessmentDetail.setStatus("2");// JSI not active
						return assessmentDetail;
					}

				}else
				{
					assessmentDetail.setAssessmentName(null);
					assessmentDetail.setStatus("1");
					return assessmentDetail;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to load Assessment Questions.
	 */
	@Transactional(readOnly=false)
	public String loadAssessmentQuestions(AssessmentDetail assessmentDetail,JobOrder jobOrder,Integer epiJobId)
	{
		System.out.println(":::::::::::::::::::APIAssessmentCampaignAjax : loadAssessmentQuestions:::::::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		Integer jId = 0;
		if(jobOrder!=null && jobOrder.getJobId()==0)
			jId = null;
		else if(jobOrder!=null && jobOrder.getJobId()==-3)
			jId = -3;
		else if(jobOrder!=null && jobOrder.getJobId()==-4)
			jId = -4;

		TeacherDetail teacherDetail = null;
		List<TeacherAssessmentStatus> teacherAssessmentStatusList =null;
		List<TeacherAssessmentStatus> teacherAssessmentStatusList1 =null;
		try{

			String ipAddress = IPAddressUtility.getIpAddress(request);

			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,0,0,0);
			Date eDate=cal.getTime();


			if (session == null || session.getAttribute("apiTeacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");

			Boolean questionRandomization = assessmentDetail.getQuestionRandomization();
			Boolean optionRandomization = assessmentDetail.getOptionRandomization();
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			int assessmentTakenCount = 0;
			if(jId==null)//for base
			{
				teacherAssessmentStatusList=teacherAssessmentStatusDAO.findByCriteria(criterion,criterion1,criterion2);
				teacherAssessmentStatusList1=teacherAssessmentStatusDAO.findByCriteria(criterion,criterion1);
				assessmentTakenCount = teacherAssessmentStatusList1.size();
			}
			else
			{
				teacherAssessmentStatusList=teacherAssessmentStatusDAO.findByCriteria(criterion,criterion1);
				assessmentTakenCount = teacherAssessmentStatusList.size();
			}
			
			if(assessmentTakenCount==0)
				assessmentTakenCount=1;
			else if(assessmentTakenCount>0)
				assessmentTakenCount=assessmentTakenCount+1;

			if(teacherAssessmentStatusList.size()==0 || assessmentDetail.getAssessmentType()==4 || (assessmentDetail.getAssessmentType()==3 && teacherAssessmentStatusList.size()<2))
			{
				try {

					/////////////////////////////////////////////////////////////////////////////////
					TeacherAssessmentdetail teacherAssessmentdetail = new TeacherAssessmentdetail();
					teacherAssessmentdetail.setAssessmentDetail(assessmentDetail);
					teacherAssessmentdetail.setAssessmentDescription(assessmentDetail.getAssessmentDescription());
					teacherAssessmentdetail.setAssessmentName(assessmentDetail.getAssessmentName());
					teacherAssessmentdetail.setAssessmentType(assessmentDetail.getAssessmentType());
					teacherAssessmentdetail.setStatus(assessmentDetail.getStatus());
					teacherAssessmentdetail.setAssosiatedJobOrderType(assessmentDetail.getAssessmentType());
					teacherAssessmentdetail.setCreatedDateTime(new Date());
					teacherAssessmentdetail.setDistrictMaster(assessmentDetail.getDistrictMaster());
					teacherAssessmentdetail.setIpaddress(ipAddress);
					teacherAssessmentdetail.setMaxQuesToDisplay(assessmentDetail.getMaxQuesToDisplay());
					teacherAssessmentdetail.setOptionRandomization(assessmentDetail.getOptionRandomization());
					teacherAssessmentdetail.setQuestionRandomization(assessmentDetail.getQuestionRandomization());
					teacherAssessmentdetail.setSchoolMaster(assessmentDetail.getSchoolMaster());
					teacherAssessmentdetail.setTeacherDetail(teacherDetail);
					UserMaster userMaster = userMasterDAO.findById(assessmentDetail.getUserMaster().getUserId(), false, false);
					System.out.println("userMaster : "+userMaster);
					teacherAssessmentdetail.setUserMaster(userMaster);
					//teacherAssessmentdetail.setUserMaster(assessmentDetail.getUserMaster());
					teacherAssessmentdetail.setScoreLookupMaster(assessmentDetail.getScoreLookupMaster());
					teacherAssessmentdetail.setIsDone(false);
					teacherAssessmentdetail.setAssessmentTakenCount(assessmentTakenCount); 
					
					List<TeacherWiseAssessmentTime> teacherWiseAssessmentTimeList = new ArrayList<TeacherWiseAssessmentTime>();
					teacherWiseAssessmentTimeList = teacherWiseAssessmentTimeDAO.findTeacherWiseAssessmentTime(teacherDetail);
					if(teacherWiseAssessmentTimeList.size()>0) // Time reset
					{
						TeacherWiseAssessmentTime teacherWiseAssessmentTime = teacherWiseAssessmentTimeList.get(0);
						if(teacherWiseAssessmentTime.getQuestionSessionTime()!=null)
							teacherAssessmentdetail.setQuestionSessionTime(teacherWiseAssessmentTime.getQuestionSessionTime());
						else
							teacherAssessmentdetail.setQuestionSessionTime(assessmentDetail.getQuestionSessionTime());
						
						if(teacherWiseAssessmentTime.getAssessmentSessionTime()!=null)
							teacherAssessmentdetail.setAssessmentSessionTime(teacherWiseAssessmentTime.getAssessmentSessionTime());
						else
							teacherAssessmentdetail.setAssessmentSessionTime(assessmentDetail.getAssessmentSessionTime());
						
					}else
					{
						teacherAssessmentdetail.setQuestionSessionTime(assessmentDetail.getQuestionSessionTime());
						teacherAssessmentdetail.setAssessmentSessionTime(assessmentDetail.getAssessmentSessionTime());
					}
					
					teacherAssessmentDetailDAO.makePersistent(teacherAssessmentdetail);

					//System.out.println("hhhhhhhhhhhhhhhhhhhhhhhhhh "+assessmentDetail.getAssessmentId());
					////////////// section 
					List<AssessmentSections> assessmentSectionsList = null;
					if(questionRandomization!=null && questionRandomization==true)
						assessmentSectionsList = assessmentSectionDAO.getRNDSectionsByAssessmentId(assessmentDetail);
					else
						assessmentSectionsList = assessmentSectionDAO.getSectionsByAssessmentId(assessmentDetail);
					int noOfExperimentQuestions = 0;
					
					if(assessmentSectionsList.size()>0)
					{
						noOfExperimentQuestions = assessmentSectionsList.get(0).getAssessmentDetail().getNoOfExperimentQuestions()==null?0:assessmentSectionsList.get(0).getAssessmentDetail().getNoOfExperimentQuestions();
					}
					//System.out.println("assessmentSectionsList.size() : "+assessmentSectionsList.size());
					//Collections.shuffle(assessmentSectionsList);
					
					Map<Integer,List<AssessmentQuestions>> sMap = new HashMap<Integer, List<AssessmentQuestions>>();
					
					if((assessmentDetail.getAssessmentType()==1 || assessmentDetail.getAssessmentType()==3 || assessmentDetail.getAssessmentType()==4) && noOfExperimentQuestions>0)
					{
						List<AssessmentQuestions> assessmentQuestions = new ArrayList<AssessmentQuestions>();
						/*List<AssessmentQuestions> assessmentQuestionsStage2 = null;
						assessmentQuestionsStage2 = assessmentQuestionsDAO.getSectionQuestionsWithoutWeightageExp(assessmentSectionsList,noOfExperimentQuestions);
						int Stage2Size =  assessmentQuestionsStage2.size();

						System.out.println("Stage2Size:: "+Stage2Size);

						if(Stage2Size<noOfExperimentQuestions)
							assessmentQuestions = assessmentQuestionsDAO.getSectionsQuestionListWithoutWeightage(assessmentSectionsList,noOfExperimentQuestions-Stage2Size);

						System.out.println("assessmentQuestions1:: "+assessmentQuestions.size());

						assessmentQuestions.addAll(assessmentQuestionsStage2);*/

						assessmentQuestions = assessmentQuestionsDAO.getSectionsQuestionListWithoutWeightage(assessmentSectionsList,noOfExperimentQuestions);
						
						System.out.println("assessmentQuestions : ExperimentQuestions : "+assessmentQuestions.size());
						
						for (AssessmentQuestions assessmentQuestions2 : assessmentQuestions) {
							Integer secId = assessmentQuestions2.getAssessmentSections().getSectionId();
							List<AssessmentQuestions> aqList = sMap.get(secId);
							if(aqList==null)
							{
								aqList = new ArrayList<AssessmentQuestions>();
								aqList.add(assessmentQuestions2);
							}else
								aqList.add(assessmentQuestions2);
							sMap.put(secId, aqList);
						}
					}
					if(assessmentSectionsList.size()!=0)
					{
						List<Integer> questionPools = new ArrayList<Integer>();
						for (AssessmentSections assessmentSection : assessmentSectionsList) 
						{
							//System.out.println("sectionName: "+assessmentSection.getSectionName());
							TeacherSectionDetail teacherSectionDetail = new TeacherSectionDetail(); 

							teacherSectionDetail.setTeacherAssessmentdetail(teacherAssessmentdetail);
							teacherSectionDetail.setCreatedDateTime(new Date());
							teacherSectionDetail.setIpAddress(ipAddress);
							teacherSectionDetail.setSectionDescription(assessmentSection.getSectionDescription());
							teacherSectionDetail.setSectionInstructions(assessmentSection.getSectionInstructions());
							teacherSectionDetail.setSectionName(assessmentSection.getSectionName());
							teacherSectionDetail.setSectionVideoUrl(assessmentSection.getSectionVideoUrl());
							teacherSectionDetail.setStatus(assessmentSection.getStatus());
							teacherSectionDetail.setUserMaster(assessmentSection.getUserMaster());
							teacherSectionDetail.setTeacherDetail(teacherDetail);

							teacherSectionDetailDAO.makePersistent(teacherSectionDetail);

							List<AssessmentQuestions> assessmentQuestions = new ArrayList<AssessmentQuestions>();
							if(questionRandomization!=null && questionRandomization==true)
								assessmentQuestions = assessmentQuestionsDAO.getRNDSectionQuestions(assessmentSection);
							else
								assessmentQuestions = assessmentQuestionsDAO.getSectionQuestionsWithWeightage(assessmentSection);
								//assessmentQuestions = assessmentQuestionsDAO.getSectionQuestions(assessmentSection);

							//	System.out.println("assessmentQuestions.size() : "+assessmentQuestions.size());
							if(assessmentDetail.getAssessmentType()==1 || assessmentDetail.getAssessmentType()==3 || assessmentDetail.getAssessmentType()==4)
							{
								List<AssessmentQuestions> assessmentQuestionsExp = sMap.get(assessmentSection.getSectionId());
								if(assessmentQuestionsExp!=null)
									assessmentQuestions.addAll(assessmentQuestionsExp);
								
								if(questionRandomization!=null && questionRandomization==true)
									Collections.shuffle(assessmentQuestions);
							}
							
							List<QuestionOptions> questionOptions = null;
							if(assessmentQuestions.size()!=0)
							{
								QuestionsPool questionsPool = null;

								for (AssessmentQuestions assessmentQuestion : assessmentQuestions) 
								{
									questionsPool=assessmentQuestion.getQuestionsPool();
									
									questionPools.add(questionsPool.getQuestionId());
									
									TeacherAssessmentQuestion teacherAssessmentQuestion = new TeacherAssessmentQuestion();
									teacherAssessmentQuestion.setTeacherAssessmentdetail(teacherAssessmentdetail);
									teacherAssessmentQuestion.setTeacherSectionDetail(teacherSectionDetail);

									teacherAssessmentQuestion.setCompetencyMaster(questionsPool.getCompetencyMaster());
									teacherAssessmentQuestion.setCreatedDateTime(new Date());
									teacherAssessmentQuestion.setDomainMaster(questionsPool.getDomainMaster());
									teacherAssessmentQuestion.setFieldLength(questionsPool.getFieldLength());
									teacherAssessmentQuestion.setIsMandatory(assessmentQuestion.getIsMandatory());
									teacherAssessmentQuestion.setObjectiveMaster(questionsPool.getObjectiveMaster());
									//System.out.println(questionsPool.getQuestionId()+" questionsPool.getQuestionUId(): "+questionsPool.getQuestionUId());
									teacherAssessmentQuestion.setQuestionUId(questionsPool.getQuestionUId());
									teacherAssessmentQuestion.setQuestion(questionsPool.getQuestion());
									teacherAssessmentQuestion.setQuestionInstruction(questionsPool.getQuestionInstruction());
									teacherAssessmentQuestion.setQuestionPosition(assessmentQuestion.getQuestionPosition());
									teacherAssessmentQuestion.setQuestionTypeMaster(questionsPool.getQuestionTypeMaster());
									//teacherAssessmentQuestion.setQuestionWeightage(questionsPool.getQuestionWeightage());
									teacherAssessmentQuestion.setQuestionWeightage(assessmentQuestion.getQuestionWeightage());
									teacherAssessmentQuestion.setMaxMarks(questionsPool.getMaxMarks());
									teacherAssessmentQuestion.setTeacherDetail(teacherDetail);
									teacherAssessmentQuestion.setValidationType(questionsPool.getValidationType());
									teacherAssessmentQuestion.setIsAttempted(false);
									teacherAssessmentQuestionDAO.makePersistent(teacherAssessmentQuestion);
									//System.out.println(assessmentQuestion.getQuestionsPool().getQuestion());
									QuestionTypeMaster questionTypeMaster= questionsPool.getQuestionTypeMaster();
									// Likert Scale check
									questionOptions=questionsPool.getQuestionOptions();
									if(optionRandomization!=null && optionRandomization==true && !questionTypeMaster.getQuestionTypeShortName().equalsIgnoreCase("lkts"))
										Collections.shuffle(questionOptions);

									if(questionOptions.size()!=0)
									{
										for (QuestionOptions questionOption : questionOptions) {
											//System.out.println("LL "+questionOption.getQuestionOption());
											TeacherAssessmentOption teacherAssessmentOption = new TeacherAssessmentOption();

											teacherAssessmentOption.setTeacherDetail(teacherDetail);
											teacherAssessmentOption.setTeacherAssessmentQuestion(teacherAssessmentQuestion);
											teacherAssessmentOption.setQuestionOption(questionOption.getQuestionOption());
											teacherAssessmentOption.setQuestionOptionId(questionOption.getOptionId());
											teacherAssessmentOption.setQuestionOptionTag(questionOption.getQuestionOptionTag());
											teacherAssessmentOption.setRank(questionOption.getRank());
											teacherAssessmentOption.setScore(questionOption.getScore());
											teacherAssessmentOption.setCreatedDateTime(new Date());
											teacherAssessmentOptionDAO.makePersistent(teacherAssessmentOption);
										}
									}

								}
								
							}

						}
						
						/// updating questionsPool
						/*try {
							questionsPoolDAO.updateQuestionsCount(questionPools);
						} catch (Exception e) {
							e.printStackTrace();
						}*/
					}
					////////////////////// 

					// teacherassessmentattempt
					TeacherAssessmentAttempt teacherAssessmentAttempt = new TeacherAssessmentAttempt();
					teacherAssessmentAttempt.setAssessmentDetail(assessmentDetail);
					teacherAssessmentAttempt.setIpAddress(ipAddress);
					if(jobOrder!=null && jobOrder.getJobId()!=null && (jobOrder.getJobId()==0 || jobOrder.getJobId()==-3 || jobOrder.getJobId()==-4))
						teacherAssessmentAttempt.setJobOrder(null);
					else
						teacherAssessmentAttempt.setJobOrder(jobOrder);

					teacherAssessmentAttempt.setTeacherDetail(teacherDetail);
					teacherAssessmentAttempt.setAssessmentSessionTime(0);
					teacherAssessmentAttempt.setAssessmentStartTime(new Date());
					teacherAssessmentAttempt.setIsForced(false);
					teacherAssessmentAttempt.setAssessmentTakenCount(assessmentTakenCount);
					teacherAssessmentAttempt.setTeacherAssessmentdetail(teacherAssessmentdetail);
					teacherAssessmentAttemptDAO.makePersistent(teacherAssessmentAttempt);

					/////// TeacherAssessmentStatus
					TeacherAssessmentStatus teacherAssessmentStatus = new TeacherAssessmentStatus();
					teacherAssessmentStatus.setAssessmentDetail(assessmentDetail);
					teacherAssessmentStatus.setTeacherDetail(teacherDetail);
					teacherAssessmentStatus.setTeacherAssessmentdetail(teacherAssessmentdetail);
					teacherAssessmentStatus.setAssessmentType(assessmentDetail.getAssessmentType());
					teacherAssessmentStatus.setCreatedDateTime(new Date());
					StatusMaster statusMaster=WorkThreadServlet.statusMap.get("icomp");
					teacherAssessmentStatus.setStatusMaster(statusMaster);
					teacherAssessmentStatus.setAssessmentTakenCount(assessmentTakenCount);

					if(jobOrder!=null && jobOrder.getJobId()!=null && (jobOrder.getJobId()==0 || jobOrder.getJobId()==-3 || jobOrder.getJobId()==-4))
					{
						teacherAssessmentStatus.setJobOrder(null);
						teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);
					}
					else
					{
						teacherAssessmentStatus = new TeacherAssessmentStatus();
						teacherAssessmentStatus.setAssessmentDetail(assessmentDetail);
						teacherAssessmentStatus.setTeacherDetail(teacherDetail);
						teacherAssessmentStatus.setTeacherAssessmentdetail(teacherAssessmentdetail);
						teacherAssessmentStatus.setAssessmentType(assessmentDetail.getAssessmentType());
						teacherAssessmentStatus.setCreatedDateTime(new Date());
						teacherAssessmentStatus.setJobOrder(jobOrder);
						teacherAssessmentStatus.setStatusMaster(statusMaster);
						teacherAssessmentStatus.setAssessmentTakenCount(assessmentTakenCount);
						teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				//EPI taken Updation 
				//if(jobOrder.getJobId()==0 || jobOrder.getJobId()==-3 || jobOrder.getJobId()==-4)
				if(jobOrder.getJobId()==0)
				{
					Map<Boolean,EpiTakenList> epiTakenMap = new HashMap<Boolean, EpiTakenList>();
					EpiTakenList epiTakenList = null;
					List<EpiTakenList> epiTakenLists = null;
					epiTakenLists = epiTakenListDAO.getEpiTakenList(assessmentDetail.getAssessmentGroupDetails());
					if(epiTakenLists!=null && epiTakenLists.size()>0){
						for (EpiTakenList epiTakenList2 : epiTakenLists) {
							if(epiTakenList2.getIsResearchEPI())
								epiTakenMap.put(true, epiTakenList2);
							else
								epiTakenMap.put(false, epiTakenList2);
						}
					}
					
					epiTakenList = epiTakenMap.get(assessmentDetail.getIsResearchEPI());
					if(epiTakenList==null)
					{
						epiTakenList = new EpiTakenList();
						epiTakenList.setAssessmentId(assessmentDetail.getAssessmentId());
						epiTakenList.setIsResearchEPI(assessmentDetail.getIsResearchEPI());
						epiTakenList.setAssessmentGroupDetails(assessmentDetail.getAssessmentGroupDetails());
					}else
						epiTakenList.setAssessmentId(assessmentDetail.getAssessmentId());

					try {
						epiTakenListDAO.makePersistent(epiTakenList);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				//send mail to candidate on start of EPI and JSI according to district settings for EPI and JSI.
				sendMailToCandidate(assessmentDetail.getAssessmentType(),"strt",jobOrder.getJobId(),epiJobId);
			}else
			{
				return teacherAssessmentStatusList.get(0).getStatusMaster().getStatusShortName();
			}


		}catch (Exception e) {
			e.printStackTrace();
		}
		return "1";
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment Section Questions.
	 */
	@Transactional(readOnly=false)
	public String getAssessmentSectionCampaignQuestions(AssessmentDetail assessmentDetail,TeacherAssessmentdetail teacherAssessmentdetail,TeacherAssessmentQuestion teacherAssessmentQues,TeacherSectionDetail teacherSectionDetail,TeacherAnswerDetail[] teacherAnswerDetails,int attemptId,int newJobId,Integer epiJobId)
	{
		//System.out.println("::::::::::::::::getAssessmentSectionCampaignQuestions::::::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		int assessmentType=1;
		Boolean epiStandalone = session.getAttribute("epiStandalone")==null?false:(Boolean)session.getAttribute("epiStandalone");
		StringBuffer sb =new StringBuffer();
		JobOrder jobOrderForUpdate=null;
		try{
			
			try{
				if(newJobId>0)
				jobOrderForUpdate = jobOrderDAO.findById(newJobId, false, false);
			}catch(Exception e){
				e.printStackTrace();
			}
			DistrictMaster districtMaster = null;
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("apiTeacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");
			    try {
					districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
					System.out.println("districtMaster:: "+districtMaster);
					System.out.println("districtMaster:: "+districtMaster.getDistrictId());
				} catch (Exception e1) {
					districtMaster = null;
				}
			try{
				System.out.print("Assesment:"+new Date()+", T:"+teacherDetail.getTeacherId() +", E:"+teacherDetail.getEmailAddress());
			}catch(Exception e){
				e.printStackTrace();
			}

			if(attemptId!=0)
			{
				teacherAssessmentAttemptDAO.updateAssessmentAttemptSessionTime(attemptId,false);
			}

			if(teacherAssessmentQues!=null && teacherAssessmentQues.getTeacherAssessmentQuestionId()!=null)
			{

				TeacherAnswerDetail teacherAnswerDetail = teacherAnswerDetailDAO.findTADetailbyQuestion(teacherAssessmentQues);
				if(teacherAnswerDetails!=null && teacherAnswerDetails.length>0)
				{
					if(teacherAnswerDetail==null)
						teacherAnswerDetail = teacherAnswerDetails[0];
					if(teacherAnswerDetail.getJobOrder().getJobId()==null)
						teacherAnswerDetail.setJobOrder(null);
					teacherAnswerDetail.setAssessmentDetail(assessmentDetail);
					teacherAnswerDetail.setTeacherAssessmentdetail(teacherAssessmentdetail);
					teacherAnswerDetail.setTeacherAssessmentQuestion(teacherAssessmentQues);
					teacherAnswerDetail.setCreatedDateTime(new Date());
					teacherAnswerDetail.setTeacherDetail(teacherDetail);
					teacherAnswerDetailDAO.makePersistent(teacherAnswerDetail);
				}
				/////////////////////////////////////////////////////////////////////////////////////////////
				teacherAssessmentQues=teacherAssessmentQuestionDAO.findById(teacherAssessmentQues.getTeacherAssessmentQuestionId(), false, false);
				teacherAssessmentQues.setIsAttempted(true);
				teacherAssessmentQuestionDAO.makePersistent(teacherAssessmentQues);
			}
			int teacherSectionId = 0;
			if(teacherSectionDetail!=null && teacherSectionDetail.getTeacherSectionId()!=null)
			{
				teacherSectionId = teacherSectionDetail.getTeacherSectionId();
			}
			List<TeacherAssessmentQuestion> teacherAssessmentQuestions =null;
			Criterion criterion = Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("isAttempted", false);
			Criterion criterion3 = Restrictions.eq("isAttempted", true);
			int totalQuestions = teacherAssessmentQuestionDAO.getRowCount(criterion,criterion1);
			int totalQuestionsAttempted = teacherAssessmentQuestionDAO.getRowCount(criterion,criterion1,criterion3);
			teacherAssessmentQuestions = teacherAssessmentQuestionDAO.findWithLimit(null,0,1,criterion,criterion1,criterion2);
			int i=0;
			List<TeacherAssessmentOption> teacherQuestionOptionsList = null;
			String questionInstruction = null;
			int teacherSectionIDD = 0;
			String shortName = "";
			String sectionInstructions="";
			for (TeacherAssessmentQuestion teacherAssessmentQuestion : teacherAssessmentQuestions) 
			{
				teacherAssessmentdetail = teacherAssessmentQuestion.getTeacherAssessmentdetail();

				teacherSectionIDD=teacherAssessmentQuestion.getTeacherSectionDetail().getTeacherSectionId();
				if(teacherSectionId==0 || (teacherSectionIDD!=teacherSectionId))
				{
					sb.append("<tr>");
					sb.append("<td width='90%'>");
					sb.append("<h3>"+Utility.getLocaleValuePropByKey("lnkSection", locale)+": "+teacherAssessmentQuestion.getTeacherSectionDetail().getSectionName()+"</h3><br>");
					sb.append("<b>"+Utility.getLocaleValuePropByKey("msgInstructionsSection", locale)+"</b><br><br>");
					sectionInstructions = teacherAssessmentQuestion.getTeacherSectionDetail().getSectionInstructions();

					sb.append(Utility.getUTFToHTML(sectionInstructions)+"<br>");
					String sectionVideoUrl = teacherAssessmentQuestion.getTeacherSectionDetail().getSectionVideoUrl();

					if(sectionVideoUrl!=null && !sectionVideoUrl.equals(""))
						sb.append("<div align='center'>"+Utility.getYouTubeCode(sectionVideoUrl,450,350)+"</div><br><br>");

					if(teacherSectionId!=0)
						sb.append("<input type='hidden' id='teacherAssessmentQuestionId' value=''/>" );
					sb.append("<input type='hidden' id='teacherSectionId' value='"+teacherSectionIDD+"'/>" );
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("document.getElementById('sectionName').innerHTML='<b>"+Utility.getLocaleValuePropByKey("lnkSection", locale)+": "+teacherAssessmentQuestion.getTeacherSectionDetail().getSectionName()+"</b>';");
					sb.append("document.getElementById('sectionName').style.display='none';");
					sb.append("document.getElementById('questionTimer').style.display='none';");
					sb.append("</script>");
					sb.append("</td>");
					sb.append("</tr>");

					return sb.toString();

				}
				sb.append("<tr>");
				sb.append("<td width='90%'>");

				sb.append("<div style='text-align:right;'>"+Utility.genrateHTMLspace(198)+"<b>"+Utility.getLocaleValuePropByKey("lblQues", locale)+" "+(++totalQuestionsAttempted)+" of "+totalQuestions+"</b></div>");
				try{
					assessmentType=teacherAssessmentdetail.getAssessmentType();
					System.out.println(", Type:"+teacherAssessmentdetail.getAssessmentType()+", Q:"+(totalQuestionsAttempted)+" of "+totalQuestions);
				}catch(Exception e){}
				questionInstruction = teacherAssessmentQuestion.getQuestionInstruction()==null?"":teacherAssessmentQuestion.getQuestionInstruction();
				if(!questionInstruction.equals(""))
					sb.append(""+Utility.getUTFToHTML(questionInstruction)+"<br/>");

				sb.append(""+Utility.getUTFToHTML(teacherAssessmentQuestion.getQuestion())+"<br/>");
				sb.append("<input type='hidden' name='o_maxMarks' value='"+(teacherAssessmentQuestion.getMaxMarks()==null?0:teacherAssessmentQuestion.getMaxMarks())+"'  />");

				teacherQuestionOptionsList = teacherAssessmentQuestion.getTeacherAssessmentOptions();
				shortName=teacherAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName();

				if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel") || shortName.equalsIgnoreCase("lkts"))
				{
					sb.append("<table >");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;padding: 5px 3px;' >"+teacherQuestionOption.getQuestionOption()+" ");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'/>");
						sb.append("<input type='hidden' name='questionOptionId' value='"+(teacherQuestionOption.getQuestionOptionId()==null?null:teacherQuestionOption.getQuestionOptionId())+"'/>");
						sb.append("<input type='hidden' name='questionOptionTag' value='"+(teacherQuestionOption.getQuestionOptionTag()==null?null:teacherQuestionOption.getQuestionOptionTag())+"'/></td></tr>");
					}
					sb.append("</table>");
				}
				if(shortName.equalsIgnoreCase("it"))
				{
					sb.append("<table >");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherQuestionOption.getQuestionOption()+"'> ");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'/>");
						sb.append("<input type='hidden' name='questionOptionId' value='"+(teacherQuestionOption.getQuestionOptionId()==null?null:teacherQuestionOption.getQuestionOptionId())+"'/>");
						sb.append("<input type='hidden' name='questionOptionTag' value='"+(teacherQuestionOption.getQuestionOptionTag()==null?null:teacherQuestionOption.getQuestionOptionTag())+"'/></td></tr>");
					}
					sb.append("</table>");
				}
				else if(shortName.equalsIgnoreCase("rt"))
				{
					int rank=1;
					sb.append("<table>");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' id='rnk"+rank+"' name='rank' class='span1' maxlength='1' onkeypress='return checkForIntUpto6(event)' onblur='checkUniqueRank(this);'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"' /><input type='hidden' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' />");
						sb.append("<input type='hidden' name='o_rank' value='"+(teacherQuestionOption.getRank()==null?0:teacherQuestionOption.getRank())+"' />");
						sb.append("<input type='hidden' name='questionOptionId' value='"+(teacherQuestionOption.getQuestionOptionId()==null?null:teacherQuestionOption.getQuestionOptionId())+"'/>");
						sb.append("<input type='hidden' name='questionOptionTag' value='"+(teacherQuestionOption.getQuestionOptionTag()==null?null:teacherQuestionOption.getQuestionOptionTag())+"'/>");
						sb.append("<script type=\"text/javascript\" language=\"javascript\">");
						sb.append("document.getElementById('rnk1').focus();");
						sb.append("</script></td></tr>");
						rank++;
					}
					sb.append("</table >");
				}else if(shortName.equalsIgnoreCase("sl"))
				{
					sb.append("&nbsp;&nbsp;<input type='text' name='opt' id='opt' class='span15' maxlength='75'/><br/>");
					//sb.append(Utility.genrateHTMLspace(190)+"Max Length: 75 Characters<br/>");
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("document.getElementById('opt').focus();");
					sb.append("</script>");
				}
				else if(shortName.equalsIgnoreCase("ml"))
				{
					sb.append("&nbsp;&nbsp;<textarea name='opt' id='opt' class='form-control' maxlength='5000' rows='15' style='width:98%;margin:5px;' /></textarea><br/>");
					//sb.append(Utility.genrateHTMLspace(187)+"Max Length: 5000 Characters<br/>");
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("document.getElementById('opt').focus();");
					sb.append("</script>");
				}
				sb.append("<input type='hidden' id='teacherAssessmentQuestionId' value='"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"'/>" );
				sb.append("<input type='hidden' id='teacherSectionId' value='"+teacherSectionIDD+"'/>" );
				sb.append("<input type='hidden' id='questionWeightage' value='"+(teacherAssessmentQuestion.getQuestionWeightage()==null?0:teacherAssessmentQuestion.getQuestionWeightage())+"'/>" );
				sb.append("<input type='hidden' id='questionTypeId' value='"+teacherAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
				sb.append("<input type='hidden' id='questionTypeShortName' value='"+teacherAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName()+"'/>" );

				if(teacherAssessmentdetail.getQuestionSessionTime()!=null && teacherAssessmentdetail.getQuestionSessionTime()!=0)
				{
					sb.append("<script type=\"text/javascript\" language=\"javascript\">tmTimer("+teacherAssessmentdetail.getQuestionSessionTime()+",3);" );
					//sb.append("document.getElementById('timer').style.display='block';");
					sb.append("</script>");
				}
				sb.append("<input type='hidden' id='questionSessionTime' value='"+(teacherAssessmentdetail.getQuestionSessionTime()==null?0:teacherAssessmentdetail.getQuestionSessionTime())+"'/>" );
				sb.append("</td>");
				sb.append("</tr>");

			}
			if(teacherAssessmentQuestions.size()==0 && teacherAssessmentQues!=null)
			{
				sb.append("<tr id='tr1'><td id='tr1'>");
				if(totalQuestions!=0)
				{
					JobOrder jobOrder = new JobOrder();

					// TeacherAssessmentStatus update after completion
					List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
					int jId=0;
					//System.out.println("teacherId: "+teacherDetail.getTeacherId());
					//System.out.println("teacherAssessmentId: "+teacherAssessmentdetail.getTeacherAssessmentId());
					lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentdetail);

					TeacherAssessmentStatus teacherAssessmentStatus=lstTeacherAssessmentStatus.get(0);
					if(lstTeacherAssessmentStatus.size()!=0)
						if(teacherAssessmentStatus!=null)
						{
							teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();

							StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");

							List<JobOrder> jobs = new ArrayList<JobOrder>();
							for(TeacherAssessmentStatus tas: lstTeacherAssessmentStatus)
							{
								jobs.add(tas.getJobOrder());

								tas.setStatusMaster(statusMaster);
								
								if(statusMaster.getStatusShortName().equalsIgnoreCase("comp"))
									tas.setAssessmentCompletedDateTime(new Date());
								
								teacherAssessmentStatusDAO.makePersistent(tas);
							}

							//						System.out.println("done.......................");
							boolean onDashboard = false;
							TeacherAssessmentStatus teacherAssessmentStatus2 = null;
							String currentPassFailStatus = null;
							ArrayList<String> passFailStatusList = null;
							String otherPassFailStatus = null;
							//AssessmentGroupDetails assessmentGroupDetails = null;
							List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
							
							//sb.append("Assessment completed");
							List<JobForTeacher> lstJobForTeacherIcomp = new ArrayList<JobForTeacher>();
							List<JobOrder> lstJBOrder = new ArrayList<JobOrder>();
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							sb.append("window.onbeforeunload = function() {};");
							if(teacherAssessmentdetail.getAssessmentType()==1 || teacherAssessmentdetail.getAssessmentType()==3 || teacherAssessmentdetail.getAssessmentType()==4)
							{
								///////////////////////// candidate data coping ///////////////////
								String status = teacherAssessmentStatus.getStatusMaster().getStatusShortName();
								if(status.equalsIgnoreCase("comp"))
								{
									CadidateRawScoreThread crt = new CadidateRawScoreThread();
									crt.setCandidateRawScoreDAO(candidateRawScoreDAO);
									crt.setTeacherAnswerDetailDAO(teacherAnswerDetailDAO);
									crt.setTeacherAssessmentQuestionDAO(teacherAssessmentQuestionDAO);
									crt.setTeacherDetail(teacherDetail);
									crt.setReportService(reportService);
									crt.setTeacherAssessmentdetail(teacherAssessmentdetail);
									crt.setTeacherAssessmentStatus(teacherAssessmentStatus);
									crt.setDistrictMaster(districtMaster);
									crt.setBaseURL(Utility.getBaseURL(request));
									crt.setIpiReferralURL(session.getAttribute("ipiReferralURL")==null?"":(String)session.getAttribute("ipiReferralURL"));
									if(teacherAssessmentdetail.getAssessmentType()==3){
										crt.run();
										teacherAssessmentStatus2 = teacherAssessmentStatusDAO.findById(teacherAssessmentStatus.getTeacherAssessmentStatusId(), false, false);
										if(teacherAssessmentStatus2!=null)
											currentPassFailStatus = teacherAssessmentStatus2.getPass();
										if(currentPassFailStatus!=null && currentPassFailStatus=="F"){
											//assessmentGroupDetails = assessmentDetailDAO.findByCriteria(Restrictions.eq("assessmentId", assessmentDetail.getAssessmentId())).get(0).getAssessmentGroupDetails();
											teacherAssessmentStatusList = teacherAssessmentStatusDAO.findTeacherAssessmentStatusByAssessmentType(teacherDetail, teacherAssessmentStatus.getAssessmentType());
											if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0){
												teacherAssessmentStatusList.remove(teacherAssessmentStatus2);
												passFailStatusList = new ArrayList<String>();
												for(TeacherAssessmentStatus teacherAssessmentStatus3 : teacherAssessmentStatusList)
													passFailStatusList.add(teacherAssessmentStatus3.getPass());
											}
											if(passFailStatusList!=null && passFailStatusList.size()>0 && passFailStatusList.contains("F"))
												otherPassFailStatus = "F";
										}
									}
									else{
										crt.start();
									}
									if(teacherAssessmentdetail.getAssessmentType()==1){
										
										//send mail to candidate on completion of EPI according to district settings for EPI.
										sendMailToCandidate(teacherAssessmentdetail.getAssessmentType(),"comp",jobOrder.getJobId(),epiJobId);
										
										if(epiJobId!=null && epiJobId>0){
											JobOrder epiJobOrder  = jobOrderDAO.findById(epiJobId, false, false);
											if(epiJobOrder!=null){
												DistrictMaster iDistrictMaster = null;
												String epiSetting = null;
												if(epiJobOrder.getDistrictMaster()!=null){
													iDistrictMaster = epiJobOrder.getDistrictMaster();
												}
												//Send Mail to all DAs after EPI completion
												try{
													if(iDistrictMaster!=null){
														if(iDistrictMaster.getDistrictId()==7800292){
															assessmentCampaignAjax.sendMailToAllDAAfterEpiComplition(userMasterDAO,iDistrictMaster,teacherDetail,epiJobOrder,"EPI");
														}
													}
												}catch(Exception e){e.printStackTrace();};
											}
										}
									}
								}
								else if(teacherAssessmentdetail.getAssessmentType()==4 && status.equalsIgnoreCase("vlt")){
									TMCommonUtil.postIPIJSON(session.getAttribute("ipiReferralURL")==null?"":(String)session.getAttribute("ipiReferralURL"), Utility.getBaseURL(request), teacherAssessmentStatus, teacherAssessmentStatusDAO, assessmentDomainScoreDAO, assessmentCompetencyScoreDAO);
								}
								///////////////////////////////////////////////////////////////////

								if(teacherAssessmentdetail.getAssessmentType()==1){
									///////////////////////// for external user /////////////////////////
									List<JobForTeacher> lstJobForTeacher = null;
									JobForTeacher jobForTeacherObj=null;
									
									List<StatusMaster> statusList =  Utility.getStaticMasters(new String[]{"icomp","vlt"});
									//for completed 
									lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail,statusList);
									for(JobForTeacher jft: lstJobForTeacher){
										if(jft.getJobId().getIsJobAssessment()==false){
											boolean completeFlag=false;
											try{
												if(!jft.getStatus().getStatusShortName().equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
													completeFlag=true;
													jobForTeacherObj=jft;
												}
											}catch(Exception e){
												e.printStackTrace();
											}
											if(jft.getInternalSecondaryStatus()!=null){
												jft.setStatus(jft.getInternalStatus());
												jft.setStatusMaster(null);
												jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
											}else if(jft.getInternalStatus()!=null){
												jft.setStatus(jft.getInternalStatus());
												jft.setStatusMaster(jft.getInternalStatus());
											}else{
												String jftStatus = jft.getStatus().getStatusShortName();
												if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
													jft.setStatus(statusMaster);
													try{
														if(jft.getSecondaryStatus()!=null){
															jft.setStatusMaster(null);
														}else{
															jft.setStatusMaster(statusMaster);
														}
													}catch(Exception e){
														jft.setStatusMaster(statusMaster);
														e.printStackTrace();
													}
												}
											}
											jobForTeacherDAO.makePersistent(jft);
											try{
												 if(completeFlag && jobForTeacherObj!=null && jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
													System.out.println("::::::::::::::Update Status 23-::::::::::::::::");
													commonService.futureJobStatus(jobForTeacherObj.getJobId(), jobForTeacherObj.getTeacherId(),jobForTeacherObj);
												 }
											 }catch(Exception e){
												 e.printStackTrace();
											 }
										}else if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){//**##**
											lstJobForTeacherIcomp.add(jft);
											lstJBOrder.add(jft.getJobId());
										}
									}
									TeacherAssessmentStatus tAStatus = null;
									if(lstJobForTeacherIcomp.size()>0){
										List<TeacherAssessmentStatus> lstTAStatus = teacherAssessmentStatusDAO.findByTeacherAndListJob(teacherDetail, lstJBOrder);
										Map<Integer, TeacherAssessmentStatus> mapAssessStatus = new HashMap<Integer, TeacherAssessmentStatus>();
										for(TeacherAssessmentStatus tas: lstTAStatus){
											mapAssessStatus.put(tas.getJobOrder().getJobId(), tas);
										}
	
										for(JobForTeacher jft: lstJobForTeacherIcomp){
											boolean completeFlag=false;
											tAStatus = mapAssessStatus.get(jft.getJobId().getJobId());
											
											if(tAStatus==null && jft.getJobId().getJobCategoryMaster().getOfferJSI()==2)
											{
												tAStatus = new TeacherAssessmentStatus();
												tAStatus.setStatusMaster(statusMaster);
											}
											
											try{
												if(!jft.getStatus().getStatusShortName().equalsIgnoreCase("comp") && tAStatus!=null &&  tAStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
													completeFlag=true;
													jobForTeacherObj=jft;
												}
											}catch(Exception e){
												e.printStackTrace();
											}
											if(tAStatus!=null){
												if(jft.getInternalSecondaryStatus()!=null){
													jft.setStatus(jft.getInternalStatus());
													jft.setStatusMaster(null);
													jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
												}else if(jft.getInternalStatus()!=null){
													jft.setStatus(jft.getInternalStatus());
													jft.setStatusMaster(jft.getInternalStatus());
												}else{
													String jftStatus = jft.getStatus().getStatusShortName();
													if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
														jft.setStatus(tAStatus.getStatusMaster());
														try{
															if(jft.getSecondaryStatus()!=null){
																jft.setStatusMaster(null);
															}else{
																jft.setStatusMaster(tAStatus.getStatusMaster());
															}
														}catch(Exception e){
															jft.setStatusMaster(tAStatus.getStatusMaster());
															e.printStackTrace();
														}
													}
												}
												jobForTeacherDAO.makePersistent(jft);
												try{
													 if(completeFlag && jobForTeacherObj!=null && jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
														System.out.println("::::::::::::::Update Status 24-::::::::::::::::");
														commonService.futureJobStatus(jobForTeacherObj.getJobId(), jobForTeacherObj.getTeacherId(),jobForTeacherObj);
													 }
												 }catch(Exception e){
													 e.printStackTrace();
												 }
											}
										}
									}
								}
								/////////////////////////////////////////////////////////////////////
								onDashboard = true; 
							}else if(teacherAssessmentdetail.getAssessmentType()==2)
							{
								jobOrder=teacherAssessmentStatus.getJobOrder();
								String status = teacherAssessmentStatus.getStatusMaster().getStatusShortName();
								if(status.equalsIgnoreCase("comp"))
								{
									//send mail to candidate on completion of JSI according to district settings for JSI.
									sendMailToCandidate(teacherAssessmentdetail.getAssessmentType(),"comp",jobOrder.getJobId(),epiJobId);
								}
								try{
									if(jobOrderForUpdate==null){
										jobOrderForUpdate=jobOrder;
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								System.out.println("jobOrderForUpdate::::::"+jobOrderForUpdate);
								jId=jobOrder.getJobId();
								List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByAssessment(teacherAssessmentStatus.getAssessmentDetail());

								for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations1) {
									jobs.add(assessmentJobRelation.getJobId());
								}

								List<JobForTeacher> jobForTeachers = jobForTeacherDAO.findJobByTeacherAndSatusFormJobIds(teacherDetail, jobs);
								String jftStatus = "";
								boolean completeFlag=false;
								JobForTeacher jobForTeacherObj=null;
								for(JobForTeacher jft: jobForTeachers)
								{
									jftStatus = jft.getStatus().getStatusShortName();
									try{
										if(jobOrderForUpdate!=null && jobOrderForUpdate.getJobId().equals(jft.getJobId().getJobId())){
											if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
												completeFlag=true;
												jobForTeacherObj=jft;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									if(!(jftStatus.equalsIgnoreCase("hird") || jftStatus.equalsIgnoreCase("widrw") || jftStatus.equalsIgnoreCase("hide")))
									{
										if(jft.getInternalSecondaryStatus()!=null){
											jft.setStatus(jft.getInternalStatus());
											jft.setStatusMaster(null);
											jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
										}else if(jft.getInternalStatus()!=null){
											jft.setStatus(jft.getInternalStatus());
											jft.setStatusMaster(jft.getInternalStatus());
										}else{
											if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
												jft.setStatus(statusMaster);
												try{
													if(jft.getSecondaryStatus()!=null){
														jft.setStatusMaster(null);
													}else{
														jft.setStatusMaster(statusMaster);
													}
												}catch(Exception e){
													jft.setStatusMaster(statusMaster);
													e.printStackTrace();
												}
											}
										}
										// New Check JSI optional
										if(jft.getJobId().getJobAssessmentStatus()==1)
										jobForTeacherDAO.makePersistent(jft);
									}
								}
								try{
									 if(completeFlag && jobForTeacherObj!=null && jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
										commonService.futureJobStatus(jobForTeacherObj.getJobId(), jobForTeacherObj.getTeacherId(),jobForTeacherObj);
									 }
								 }catch(Exception e){
									 e.printStackTrace();
								 }
								
								String exitMessage = null;
								String exitURL = null;
								if(jobOrder!=null)
								{
									if(newJobId>0)
										jobOrder = jobOrderDAO.findById(newJobId, false, false); 

									int flagForURL = jobOrder.getFlagForURL();
									int flagForMessage  = jobOrder.getFlagForMessage();
									exitMessage = jobOrder.getExitMessage();
									exitURL = jobOrder.getExitURL();
									if(exitMessage!=null && !exitMessage.trim().equals("") && exitURL!=null && !exitURL.trim().equals(""))
									{
										onDashboard = true;
									}else if(flagForMessage==1)
									{
										onDashboard = false;
										sb.append("window.location.href='thankyoumessage.do?jobId="+jobOrder.getJobId()+"'");

									}else if(flagForURL==1)
									{
										sb.append("document.getElementById('mainDiv').style.display='none';");
										if(epiStandalone)
											sb.append("window.location.href='epiboard.do?jobId="+jobOrder.getJobId()+"';");
										else
											sb.append("window.location.href='startOrContinueEPI.do?jobId="+jobOrder.getJobId()+"';");
									}else
										onDashboard = true;
								}

							}

							if(onDashboard)
							{
								String redirectURL = "startOrContinueEPI.do";
								sb.append("document.getElementById('mainDiv').style.display='none';");
								String msg= Utility.getLocaleValuePropByKey("msgTeacherMatchBaseInventory", locale);
								if(teacherAssessmentdetail.getAssessmentType()==3){
									//redirectURL = "inventory.do";
									redirectURL = "inventorycomplete.do?assessmentType=3";
									msg= "You have completed the TeacherMatch Smart Practices.";
									if(currentPassFailStatus!=null && currentPassFailStatus=="P"){
										redirectURL = "../userdashboard.do";
										msg="Congratulations! You have passed the Smart Practices assessment. Continue to the candidate dashboard to view your certificate of completion.";

										//Auto-Status Pre-Hire Smart Practices Start
										try{
											commonService.autoStatusForSPJobs(null,teacherDetail,null);
										}catch(Exception e){
											e.printStackTrace();
										}
										//Auto-Status Pre-Hire Smart Practices End
									}
									else if(currentPassFailStatus!=null && currentPassFailStatus=="F" && otherPassFailStatus==null){
										redirectURL = "../spuserdashboard.do";
										msg="You did not pass the assessment on your first attempt. You may attempt the assessment one (1) more time. You may view the professional development as many times as you would like.";
									}
									else if(currentPassFailStatus!=null && currentPassFailStatus=="F" && otherPassFailStatus!=null && otherPassFailStatus=="F"){
										redirectURL = "../spuserdashboard.do";
										msg="Unfortunately";
									}
								}
								else if(teacherAssessmentdetail.getAssessmentType()==4){
									//redirectURL = "inventory.do";
									redirectURL = "inventorycomplete.do?assessmentType=4";
									msg="You have completed the TeacherMatch IPI.";
								}
								else if(jId>0)
								{
									//msg= "You have completed this Job Specific Inventory.";
									msg= "Thank you for completing the TeacherMatch part of the job application.";
									redirectURL="jobsofinterest.do";
								}
								//sb.append("alert('"+msg+"');");
								//sb.append("window.location.href='"+redirectURL+"'");
								//showInfoAndRedirect(sts,msg,nextmsg,url)
								if(epiStandalone)
									redirectURL="epiboard.do";
								System.out.println("\nredirectURL == "+redirectURL+"\n");
								System.out.println("msg == "+msg);
								sb.append("showInfoAndRedirect(0,'"+msg+"','','"+redirectURL+"',"+teacherAssessmentdetail.getAssessmentType()+");");
							}
							sb.append("</script>");
						}

					teacherAssessmentdetail.setIsDone(true);
					teacherAssessmentDetailDAO.makePersistent(teacherAssessmentdetail);

				}else
				{
					sb.append("No Questions found");
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("window.onbeforeunload = function() {};");
					sb.append("document.getElementById('mainDiv').style.display='none';");
					//sb.append("alert('No Questions found');");
					if(epiStandalone)
						sb.append("window.location.href='epiboard.do'");
					else
					{
						if(teacherAssessmentdetail.getAssessmentType()==3 || teacherAssessmentdetail.getAssessmentType()==4){
							sb.append("window.location.href='inventory.do'");
						}
						else
							sb.append("window.location.href='startOrContinueEPI.do'");
					}
						

					sb.append("</script>");
				}

				sb.append("</td></tr>");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to insert TeacherAssessmentAttempt.
	 */
	@Transactional(readOnly=false)
	public int insertTeacherAssessmentAttempt(AssessmentDetail assessmentDetail,JobOrder jobOrder)
	{
		System.out.println(":::::::::::::::Assessment insertTeacherAssessmentAttempt:::::::::::::::::::;; "+jobOrder.getJobId());

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			String ipAddress = IPAddressUtility.getIpAddress(request);
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("apiTeacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");

			List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
			TeacherAssessmentdetail teacherAssessmentdetail = null;
			//amit
			if(assessmentDetail!=null)
			{
				teacherAssessmentStatusList = teacherAssessmentStatusDAO.getTeacherAssessmentStatus(assessmentDetail,teacherDetail);
				if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()!=0)
					teacherAssessmentdetail = teacherAssessmentStatusList.get(0).getTeacherAssessmentdetail();
			}
			
			// teacherassessmentattempt
			TeacherAssessmentAttempt teacherAssessmentAttempt = new TeacherAssessmentAttempt();
			teacherAssessmentAttempt.setAssessmentDetail(assessmentDetail);
			teacherAssessmentAttempt.setIpAddress(ipAddress);
			if(jobOrder!=null && jobOrder.getJobId()!=null && (jobOrder.getJobId()==0 || jobOrder.getJobId()==-3 || jobOrder.getJobId()==-4))
				teacherAssessmentAttempt.setJobOrder(null);
			else
				teacherAssessmentAttempt.setJobOrder(jobOrder);

			teacherAssessmentAttempt.setTeacherDetail(teacherDetail);
			teacherAssessmentAttempt.setAssessmentSessionTime(0);
			teacherAssessmentAttempt.setAssessmentStartTime(new Date());
			teacherAssessmentAttempt.setIsForced(false);
			teacherAssessmentAttempt.setAssessmentTakenCount(teacherAssessmentdetail.getAssessmentTakenCount());
			teacherAssessmentAttempt.setTeacherAssessmentdetail(teacherAssessmentdetail);
			teacherAssessmentAttemptDAO.makePersistent(teacherAssessmentAttempt);
			return 1;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to set TeacherAssessmentAttempt forced quit.
	 */
	@Transactional(readOnly=false)
	public int setAssessmentForced(int attemptId)
	{
		//System.out.println(":::::::::::::::Assessment setAssessmentForced:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			if(attemptId!=0)
			{
				teacherAssessmentAttemptDAO.updateAssessmentAttemptSessionTime(attemptId,true);
			}
			return 1;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	/* @Author: Vishwanath Kumar
	 * @Discription: .
	 */
	public String insertLastAttempt(AssessmentDetail assessmentDetail, Integer assessmentTakenCount, JobOrder jobOrder)
	{
		System.out.println(":::::::::::::::Assessment insertLastAttempt:::::::::::::::::::;; "+assessmentDetail+" :::: "+assessmentTakenCount+" :::: "+jobOrder);

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String ipAddress = IPAddressUtility.getIpAddress(request);
		TeacherDetail teacherDetail = null;

		if (session == null || session.getAttribute("apiTeacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		TeacherAssessmentStatus teacherAssessmentStatus = null;
		//teacherAssessmentStatusList=teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jobOrder);
		teacherAssessmentStatusList=teacherAssessmentStatusDAO.findAssessmentTaken(assessmentDetail,assessmentTakenCount,teacherDetail,jobOrder);
		TeacherAssessmentdetail teacherAssessmentdetail = null;
		if(teacherAssessmentStatusList.size()!=0)
		{
			teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
		}

		List<TeacherAssessmentAttempt> teacherAssessmentAttempts = null;
		//teacherAssessmentAttempts = teacherAssessmentAttemptDAO.findAssessmentAttempts(teacherDetail,jobOrder);
		teacherAssessmentAttempts = teacherAssessmentAttemptDAO.findAssessmentAttempts(teacherDetail,assessmentDetail,assessmentTakenCount,jobOrder);

		int attempts = 0;
		for (TeacherAssessmentAttempt teacherAssessmentAttempt1 : teacherAssessmentAttempts) {
			if(!teacherAssessmentAttempt1.getIsForced())
				attempts++;
		}
		////////////////////////

		/******** Sekhar add for count strikeLogs ( Start )*******/
		int strikeLogs=0; 
		try{
			if(teacherAssessmentAttempts.size()>0)
			{
				List<TeacherAssessmentStatus> teacherAssessmentStatusList2 = null;
				teacherAssessmentStatusList2 = teacherAssessmentStatusDAO.getTeacherAssessmentStatus(teacherAssessmentAttempts.get(0).getAssessmentDetail(), teacherDetail);
				List<TeacherStrikeLog> teacherStrikeLog = null;
				teacherStrikeLog = teacherStrikeLogDAO.getTotalStrikesByTeacher(teacherDetail, teacherAssessmentStatusList2.get(0).getTeacherAssessmentdetail());
				strikeLogs=teacherStrikeLog.size();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		int attemptsStrike=attempts+strikeLogs;
		/******** Sekhar add for count strikeLogs ( End )*******/

		if(attemptsStrike==3)
		{
			StatusMaster statusMaster = WorkThreadServlet.statusMap.get("vlt");
			teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();

			List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
			lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentdetail);
			teacherAssessmentStatus=lstTeacherAssessmentStatus.get(0);

			List<JobOrder> jobs = new ArrayList<JobOrder>();

			if(lstTeacherAssessmentStatus.size()!=0)
				if(teacherAssessmentStatus!=null)
				{
					for(TeacherAssessmentStatus tas: lstTeacherAssessmentStatus)
					{
						jobs.add(tas.getJobOrder());

						tas.setStatusMaster(statusMaster);
						teacherAssessmentStatusDAO.makePersistent(tas);
					}
				}
			//teacherAssessmentStatus
			TeacherAssessmentStatus teacherAssessmentStatus2 = teacherAssessmentStatusDAO.findById(teacherAssessmentStatus.getTeacherAssessmentStatusId(), false, false);
			if(teacherAssessmentStatus2.getAssessmentType()==4 && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
				System.out.println(":::::::::: insertLastAttempt vlt postIPIJSON");
				TMCommonUtil.postIPIJSON(session.getAttribute("ipiReferralURL")==null?"":(String)session.getAttribute("ipiReferralURL"), Utility.getBaseURL(request), teacherAssessmentStatus, teacherAssessmentStatusDAO, assessmentDomainScoreDAO, assessmentCompetencyScoreDAO);
			}

			// JobForTeacher update after 3 attempts
			if(jobOrder.getJobId()!=null && jobOrder.getJobId()>0)
			{
				// New Changes 
				List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByAssessment(teacherAssessmentStatus.getAssessmentDetail());
				for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations1) {
					jobs.add(assessmentJobRelation.getJobId());
				}

				List<JobForTeacher> jobForTeachers = jobForTeacherDAO.findJobByTeacherAndSatusFormJobIds(teacherDetail, jobs);
				///////////////
				String jftStatus = "";
				for(JobForTeacher jft: jobForTeachers)
				{
					jftStatus = jft.getStatus().getStatusShortName();
					if(!(jftStatus.equalsIgnoreCase("hird") || jftStatus.equalsIgnoreCase("widrw") || jftStatus.equalsIgnoreCase("hide")))
					{
						if(jft.getInternalSecondaryStatus()!=null){
							jft.setStatus(jft.getInternalStatus());
							jft.setStatusMaster(null);
							jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
						}else if(jft.getInternalStatus()!=null){
							jft.setStatus(jft.getInternalStatus());
							jft.setStatusMaster(jft.getInternalStatus());
						}else{
							if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
								jft.setStatus(statusMaster);
								try{
									if(jft.getSecondaryStatus()!=null){
										jft.setStatusMaster(null);
									}else{
										jft.setStatusMaster(statusMaster);
									}
								}catch(Exception e){
									jft.setStatusMaster(statusMaster);
									e.printStackTrace();
								}
							}
						}
						// New Check JSI optional
						if(jft.getJobId().getJobAssessmentStatus()==1)
						jobForTeacherDAO.makePersistent(jft);
					}
				}
			}else if(teacherAssessmentStatus.getAssessmentType()==1)
			{
				// base taken status 
				StatusMaster epiStatus = null;
				JobOrder jjj = new JobOrder();
				jjj.setJobId(0);
				List<TeacherAssessmentStatus> baseStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jjj);
				if(baseStatusList.size()>0)
					epiStatus = baseStatusList.get(0).getStatusMaster();

					StatusMaster stautsIcomp= WorkThreadServlet.statusMap.get("icomp");
					
				///////////////////////// for external user /////////////////////////
				List<JobForTeacher> lstJobForTeacherIcomp = new ArrayList<JobForTeacher>();
				List<JobOrder> lstJBOrder = new ArrayList<JobOrder>();

				List<JobForTeacher> lstJobForTeacher= null;
				lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail,WorkThreadServlet.statusMap.get("icomp"));
				for(JobForTeacher jft: lstJobForTeacher)
				{
					if(jft.getJobId().getIsJobAssessment()==false)
					{
						if(jft.getInternalSecondaryStatus()!=null){
							jft.setStatus(jft.getInternalStatus());
							jft.setStatusMaster(null);
							jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
						}else if(jft.getInternalStatus()!=null){
							jft.setStatus(jft.getInternalStatus());
							jft.setStatusMaster(jft.getInternalStatus());
						}else{
							String jftStatus = jft.getStatus().getStatusShortName();
							if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
								jft.setStatus(statusMaster);
								try{
									if(jft.getSecondaryStatus()!=null){
										jft.setStatusMaster(null);
									}else{
										jft.setStatusMaster(statusMaster);
									}
								}catch(Exception e){
									jft.setStatusMaster(statusMaster);
									e.printStackTrace();
								}
							}
						}
						jobForTeacherDAO.makePersistent(jft);
					}else if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){ //**##**
						lstJobForTeacherIcomp.add(jft);
						lstJBOrder.add(jft.getJobId());
					}
				}

				TeacherAssessmentStatus tAStatus = null;
				if(lstJobForTeacherIcomp.size()>0){
					List<TeacherAssessmentStatus> lstTAStatus = teacherAssessmentStatusDAO.findByTeacherAndListJob(teacherDetail, lstJBOrder);
					Map<Integer, TeacherAssessmentStatus> mapAssessStatus = new HashMap<Integer, TeacherAssessmentStatus>();
					for(TeacherAssessmentStatus tas: lstTAStatus){
						mapAssessStatus.put(tas.getJobOrder().getJobId(), tas);
					}
					for(JobForTeacher jft: lstJobForTeacherIcomp){
						tAStatus = mapAssessStatus.get(jft.getJobId().getJobId());
						if(tAStatus!=null){
							if(jft.getInternalSecondaryStatus()!=null){
								jft.setStatus(jft.getInternalStatus());
								jft.setStatusMaster(null);
								jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
							}else if(jft.getInternalStatus()!=null){
								jft.setStatus(jft.getInternalStatus());
								jft.setStatusMaster(jft.getInternalStatus());
							}else{
								String jftStatus = jft.getStatus().getStatusShortName();
								if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
									jft.setStatus(tAStatus.getStatusMaster());
									try{
										if(jft.getSecondaryStatus()!=null){
											jft.setStatusMaster(null);
										}else{
											jft.setStatusMaster(tAStatus.getStatusMaster());
										}
									}catch(Exception e){
										jft.setStatusMaster(tAStatus.getStatusMaster());
										e.printStackTrace();
									}	
								}
							}
							// New Check JSI optional
							if(jft.getJobId().getJobAssessmentStatus()==1)
							jobForTeacherDAO.makePersistent(jft);
						}else{
	                        try{
	                        	if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){
		                            if(epiStatus!=null)
		                            {
			                             jft.setStatusMaster(epiStatus);
			                             jft.setStatus(epiStatus);
			                             jobForTeacherDAO.makePersistent(jft);
		                            }else
		                            {
		                            	 jft.setStatusMaster(stautsIcomp);
			                             jft.setStatus(stautsIcomp);
			                             jobForTeacherDAO.makePersistent(jft);
		                            	
		                            }
	                             }
	                         }catch(Exception e){
	                        	 e.printStackTrace();
	                         }
                        }
					}							
				}
				/////////////////////////////////////////////////////////////////////
			}
		}

		return ""+attempts;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to finish assessment whether completed or not.
	 */
	@Transactional(readOnly=false)
	public String finishAssessment(TeacherAssessmentdetail teacherAssessmentdetail,JobOrder jobOrder,int newJobId,int sessionCheck,Integer epiJobId)
	{
		System.out.println(":::::::::::::::APIAssessmentCampaignAjax : finishAssessment:::::::::::::::");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			Boolean epiStandalone = session.getAttribute("epiStandalone")==null?false:(Boolean)session.getAttribute("epiStandalone");

			String ipAddress = IPAddressUtility.getIpAddress(request);
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("apiTeacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");

			////////////////////////////////////////////////////////////////////////////////
			StringBuffer sb =new StringBuffer();
			int jId=0;
			List<TeacherAssessmentQuestion> teacherAssessmentQuestions =null;
			Criterion criterion = Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("isAttempted", false);
			teacherAssessmentQuestions = teacherAssessmentQuestionDAO.findByCriteria(criterion,criterion1,criterion2);
			int unattemptedQuestions = teacherAssessmentQuestions.size();

			// TeacherAssessmentStatus update after completion
			List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;

			lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentdetail);
			TeacherAssessmentStatus teacherAssessmentStatus=lstTeacherAssessmentStatus.get(0);

			int assessmentType=1;
			if(teacherAssessmentStatus!=null){
				if(teacherAssessmentStatus.getAssessmentType()!=null){
					assessmentType=teacherAssessmentStatus.getAssessmentType();
				}
			}

			if(lstTeacherAssessmentStatus.size()!=0)
				if(teacherAssessmentStatus!=null)
				{
					StatusMaster statusMaster = null;
					String appendMsg="not ";
					String status="";
					if(unattemptedQuestions==0)
					{
						status="comp";
						appendMsg="";
					}
					else
						status="vlt";

					statusMaster = WorkThreadServlet.statusMap.get(status);

					List<JobOrder> jobs = new ArrayList<JobOrder>();
					for(TeacherAssessmentStatus tas: lstTeacherAssessmentStatus)
					{
						jobs.add(tas.getJobOrder());

						tas.setStatusMaster(statusMaster);
						if(statusMaster.getStatusShortName().equalsIgnoreCase("comp"))
							tas.setAssessmentCompletedDateTime(new Date());
						teacherAssessmentStatusDAO.makePersistent(tas);
					}

					boolean onDashboard = false;
					TeacherAssessmentStatus teacherAssessmentStatus2 = null;
					String currentPassFailStatus = null;
					ArrayList<String> passFailStatusList = null;
					String otherPassFailStatus = null;
					//AssessmentGroupDetails assessmentGroupDetails = null;
					List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
					
					List<JobForTeacher> lstJobForTeacherIcomp = new ArrayList<JobForTeacher>();
					List<JobOrder> lstJBOrder = new ArrayList<JobOrder>();
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("window.onbeforeunload = function() {};");

					if(teacherAssessmentdetail.getAssessmentType()==1 || teacherAssessmentdetail.getAssessmentType()==3 || teacherAssessmentdetail.getAssessmentType()==4)
					{
						///////////////////////// candidate data coping ///////////////////
						if(status.equalsIgnoreCase("comp"))
						{
							CadidateRawScoreThread crt = new CadidateRawScoreThread();
							crt.setCandidateRawScoreDAO(candidateRawScoreDAO);
							crt.setTeacherAnswerDetailDAO(teacherAnswerDetailDAO);
							crt.setTeacherAssessmentQuestionDAO(teacherAssessmentQuestionDAO);
							crt.setTeacherDetail(teacherDetail);
							crt.setReportService(reportService);
							crt.setTeacherAssessmentdetail(teacherAssessmentdetail);
							crt.setTeacherAssessmentStatus(teacherAssessmentStatus);
							crt.setBaseURL(Utility.getBaseURL(request));
							crt.setIpiReferralURL(session.getAttribute("ipiReferralURL")==null?"":(String)session.getAttribute("ipiReferralURL"));
							if(teacherAssessmentdetail.getAssessmentType()==3){
								crt.run();
								teacherAssessmentStatus2 = teacherAssessmentStatusDAO.findById(teacherAssessmentStatus.getTeacherAssessmentStatusId(), false, false);
								if(teacherAssessmentStatus2!=null)
									currentPassFailStatus = teacherAssessmentStatus2.getPass();
								if(currentPassFailStatus!=null && currentPassFailStatus=="F"){
									//assessmentGroupDetails = assessmentDetailDAO.findByCriteria(Restrictions.eq("assessmentId", assessmentDetail.getAssessmentId())).get(0).getAssessmentGroupDetails();
									teacherAssessmentStatusList = teacherAssessmentStatusDAO.findTeacherAssessmentStatusByAssessmentType(teacherDetail, teacherAssessmentStatus.getAssessmentType());
									if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0){
										teacherAssessmentStatusList.remove(teacherAssessmentStatus2);
										passFailStatusList = new ArrayList<String>();
										for(TeacherAssessmentStatus teacherAssessmentStatus3 : teacherAssessmentStatusList)
											passFailStatusList.add(teacherAssessmentStatus3.getPass());
									}
									if(passFailStatusList!=null && passFailStatusList.size()>0 && passFailStatusList.contains("F"))
										otherPassFailStatus = "F";
								}
							}
							else{
								crt.start();
							}
							if(teacherAssessmentdetail.getAssessmentType()==1){
								//send mail to candidate on completion of EPI according to district settings for EPI.
								sendMailToCandidate(teacherAssessmentdetail.getAssessmentType(),"comp",jobOrder.getJobId(),epiJobId);
								
								if(epiJobId!=null && epiJobId>0){
									JobOrder epiJobOrder  = jobOrderDAO.findById(epiJobId, false, false);
									if(epiJobOrder!=null){
										DistrictMaster districtMaster = null;
										if(epiJobOrder.getDistrictMaster()!=null){
											districtMaster = epiJobOrder.getDistrictMaster();
										}
										//Send Mail to all DAs after EPI completion
										try{
											if(districtMaster!=null){
												if(districtMaster.getDistrictId()==7800292){
													assessmentCampaignAjax.sendMailToAllDAAfterEpiComplition(userMasterDAO,districtMaster,teacherDetail,epiJobOrder,"EPI");
												}
											}
										}catch(Exception e){e.printStackTrace();};
									}
								}
							}
						}
						else if(teacherAssessmentdetail.getAssessmentType()==1 && status.equalsIgnoreCase("vlt"))
						{
							//send mail to candidate on timed out of EPI according to district settings for EPI.
							sendMailToCandidate(teacherAssessmentdetail.getAssessmentType(),"vlt",jobOrder.getJobId(),epiJobId);
						}
						else if(teacherAssessmentdetail.getAssessmentType()==4 && status.equalsIgnoreCase("vlt")){
							TMCommonUtil.postIPIJSON(session.getAttribute("ipiReferralURL")==null?"":(String)session.getAttribute("ipiReferralURL"), Utility.getBaseURL(request), teacherAssessmentStatus, teacherAssessmentStatusDAO, assessmentDomainScoreDAO, assessmentCompetencyScoreDAO);
						}

						if(teacherAssessmentdetail.getAssessmentType()==1){
							///////////////////////// for external user /////////////////////////
							List<JobForTeacher> lstJobForTeacher= null;
							lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail,WorkThreadServlet.statusMap.get("icomp"));
							for(JobForTeacher jft: lstJobForTeacher)
							{
								if(jft.getJobId().getIsJobAssessment()==false)
								{
									if(jft.getInternalSecondaryStatus()!=null){
										jft.setStatus(jft.getInternalStatus());
										jft.setStatusMaster(null);
										jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
									}else if(jft.getInternalStatus()!=null){
										jft.setStatus(jft.getInternalStatus());
										jft.setStatusMaster(jft.getInternalStatus());
									}else{
										String jftStatus = jft.getStatus().getStatusShortName();
										if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
											jft.setStatus(statusMaster);
											try{
												if(jft.getSecondaryStatus()!=null){
													jft.setStatusMaster(null);
												}else{
													jft.setStatusMaster(statusMaster);
												}
											}catch(Exception e){
												jft.setStatusMaster(statusMaster);
												e.printStackTrace();
											}
										}
									}
									// New Check JSI optional
									if(jft.getJobId().getJobAssessmentStatus()==1)
									jobForTeacherDAO.makePersistent(jft);
								}
								else if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){ //**##**
									lstJobForTeacherIcomp.add(jft);
									lstJBOrder.add(jft.getJobId());
								}
							}
	
							TeacherAssessmentStatus tAStatus = null;
							if(lstJobForTeacherIcomp.size()>0){
								List<TeacherAssessmentStatus> lstTAStatus = teacherAssessmentStatusDAO.findByTeacherAndListJob(teacherDetail, lstJBOrder);
								Map<Integer, TeacherAssessmentStatus> mapAssessStatus = new HashMap<Integer, TeacherAssessmentStatus>();
								for(TeacherAssessmentStatus tas: lstTAStatus){
									mapAssessStatus.put(tas.getJobOrder().getJobId(), tas);
								}
	
								for(JobForTeacher jft: lstJobForTeacherIcomp){
									tAStatus = mapAssessStatus.get(jft.getJobId().getJobId());
									if(tAStatus!=null){
										if(jft.getInternalSecondaryStatus()!=null){
											jft.setStatus(jft.getInternalStatus());
											jft.setStatusMaster(null);
											jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
										}else if(jft.getInternalStatus()!=null){
											jft.setStatus(jft.getInternalStatus());
											jft.setStatusMaster(jft.getInternalStatus());
										}else{
											String jftStatus = jft.getStatus().getStatusShortName();
											if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
												jft.setStatus(tAStatus.getStatusMaster());
												try{
													if(jft.getSecondaryStatus()!=null){
														jft.setStatusMaster(null);
													}else{
														jft.setStatusMaster(tAStatus.getStatusMaster());
													}
												}catch(Exception e){
													jft.setStatusMaster(tAStatus.getStatusMaster());
													e.printStackTrace();
												}	
											}
										}
										// New Check JSI optional
										if(jft.getJobId().getJobAssessmentStatus()==1)
										jobForTeacherDAO.makePersistent(jft);
									}
								}							
							}
						}
						/////////////////////////////////////////////////////////////////////
						onDashboard = true; 
					}else if(teacherAssessmentdetail.getAssessmentType()==2)
					{
						jobOrder=teacherAssessmentStatus.getJobOrder();
						if(status.equalsIgnoreCase("comp") || status.equalsIgnoreCase("vlt"))
						{
							//send mail to candidate on completion or timed out of JSI according to district settings for JSI.
							sendMailToCandidate(teacherAssessmentdetail.getAssessmentType(),status,jobOrder.getJobId(),epiJobId);
						}
						List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByAssessment(teacherAssessmentStatus.getAssessmentDetail());

						for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations1) {
							jobs.add(assessmentJobRelation.getJobId());
						}

						List<JobForTeacher> jobForTeachers = jobForTeacherDAO.findJobByTeacherAndSatusFormJobIds(teacherDetail, jobs);

						String jftStatus = "";
						for(JobForTeacher jft: jobForTeachers)
						{
							jftStatus = jft.getStatus().getStatusShortName();
							if(!(jftStatus.equalsIgnoreCase("hird") || jftStatus.equalsIgnoreCase("widrw") || jftStatus.equalsIgnoreCase("hide")))
							{
								if(jft.getInternalSecondaryStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(null);
									jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
								}else if(jft.getInternalStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(jft.getInternalStatus());
								}else{
									if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
										jft.setStatus(statusMaster);
										try{
											if(jft.getSecondaryStatus()!=null){
												jft.setStatusMaster(null);
											}else{
												jft.setStatusMaster(statusMaster);
											}
										}catch(Exception e){
											jft.setStatusMaster(statusMaster);
											e.printStackTrace();
										}
									}
								}
								// New Check JSI optional
								if(jft.getJobId().getJobAssessmentStatus()==1)
								jobForTeacherDAO.makePersistent(jft);
							}
						}

						String exitMessage = null;
						String exitURL = null;
						if(jobOrder!=null)
						{
							jId=jobOrder.getJobId();

							if(newJobId>0)
								jobOrder = jobOrderDAO.findById(newJobId, false, false); 

							int flagForURL = jobOrder.getFlagForURL();
							int flagForMessage  = jobOrder.getFlagForMessage();
							exitMessage = jobOrder.getExitMessage();
							exitURL = jobOrder.getExitURL();
							if(exitMessage!=null && !exitMessage.trim().equals("") && exitURL!=null && !exitURL.trim().equals(""))
							{
								onDashboard = true;
							}else if(flagForMessage==1)
							{
								onDashboard = false;
								boolean isVlt = false;
								if(status.equalsIgnoreCase("vlt"))
								{
									String msg1="";
									if(sessionCheck==1)
										msg1=""+Utility.getLocaleValuePropByKey("msgAPIAssessment1", locale)+" <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a>.";
									else
										msg1=""+Utility.getLocaleValuePropByKey("msgAPIAssessment2", locale)+" <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a>.";
									sb.append("document.getElementById('mainDiv').style.display='none';");
									sb.append("showInfoAndRedirect(2,'"+msg1+"','','thankyoumessage.do?jobId="+jobOrder.getJobId()+"',"+assessmentType+");");
									isVlt = true;
								}
								if(!isVlt)
									sb.append("window.location.href='thankyoumessage.do?jobId="+jobOrder.getJobId()+"'");
							}else if(flagForURL==1)
							{
								sb.append("document.getElementById('mainDiv').style.display='none';");
								String m="";
								if(appendMsg.equals(""))
									m="y";
								else
									m="n";
								if(epiStandalone)
									sb.append("window.location.href='epiboard.do?jobId="+jobOrder.getJobId()+"&mf="+m+"';");
								else
									sb.append("window.location.href='startOrContinueEPI.do?jobId="+jobOrder.getJobId()+"&mf="+m+"';");
							}else
								onDashboard = true;
						}
					}

					if(onDashboard)
					{
						String redirectURL = "startOrContinueEPI.do";
						sb.append("document.getElementById('mainDiv').style.display='none';");
						String msg="";
						if(sessionCheck==1)
							msg=""+Utility.getLocaleValuePropByKey("msgAPIAssessment3", locale)+" <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a>.";
						else{
							//msg=""+Utility.getLocaleValuePropByKey("msgAPIAssessment4", locale)+" <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a>.";
							msg="thirdVlt";
						}

						int sts = 2;
						if(teacherAssessmentdetail.getAssessmentType()==3){
							//redirectURL = "inventory.do";
							redirectURL = "inventorycomplete.do?assessmentType=3";
							if(sessionCheck==1)
								msg="You have exceeded the Smart Practices time limit.";
							else
								msg="You have exceeded the time limit for your 3rd and final attempt and have timed out of the Smart Practices.";
							
							if(currentPassFailStatus!=null && currentPassFailStatus=="P"){
								redirectURL = "../userdashboard.do";
								msg="Congratulations! You have passed the Smart Practices assessment. Continue to the candidate dashboard to view your certificate of completion.";
								
								//Auto-Status Pre-Hire Smart Practices Start
								try{
									commonService.autoStatusForSPJobs(null,teacherDetail,null);
								}catch(Exception e){
									e.printStackTrace();
								}
							}
							else if(currentPassFailStatus!=null && currentPassFailStatus=="F" && otherPassFailStatus==null){
								redirectURL = "../spuserdashboard.do";
								msg="You did not pass the assessment on your first attempt. You may attempt the assessment one (1) more time. You may view the professional development as many times as you would like.";
							}
							else if(currentPassFailStatus!=null && currentPassFailStatus=="F" && otherPassFailStatus!=null && otherPassFailStatus=="F"){
								redirectURL = "../spuserdashboard.do";
								msg="Unfortunately";
							}
						}
						else if(teacherAssessmentdetail.getAssessmentType()==4){
							//redirectURL = "inventory.do";
							redirectURL = "inventorycomplete.do?assessmentType=4";
							if(sessionCheck==1)
								msg="You have exceeded the IPI time limit.";
							else
								msg="You have exceeded the time limit for your 3rd and final attempt and have timed out of IPI.";
						}
						else if(jId>0)
						{
							if(appendMsg.equals(""))
							{
								msg= Utility.getLocaleValuePropByKey("msgDistAssCamp3", locale);
								sts = 0;
							}
							else
								msg= Utility.getLocaleValuePropByKey("msgAPIAssessment5", locale);
							redirectURL="jobsofinterest.do";
						}

						if(epiStandalone)
							redirectURL="epiboard.do";
						System.out.println("\nredirectURL == "+redirectURL+"\n");
						System.out.println("msg == "+msg);
						sb.append("showInfoAndRedirect("+sts+",'"+msg+"','','"+redirectURL+"',"+assessmentType+");");
					}
					sb.append("</script>");

					teacherAssessmentdetail = teacherAssessmentDetailDAO.findById(teacherAssessmentdetail.getTeacherAssessmentId(), false, false);
					teacherAssessmentdetail.setIsDone(true);
					teacherAssessmentDetailDAO.makePersistent(teacherAssessmentdetail);
				}

			////////////////////////////////////////////////////////////////////////////////

			return sb.toString();
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to insert Teacher Assessment Strike.
	 */
	@Transactional(readOnly=false)
	public int saveStrike(TeacherAssessmentdetail teacherAssessmentdetail,TeacherAssessmentQuestion teacherAssessmentQuestion)
	{

		//System.out.println(":::::::::::::::Assessment saveStrike:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			String ipAddress = IPAddressUtility.getIpAddress(request);
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("apiTeacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");

			// teacherassessmentattempt
			TeacherStrikeLog teacherStrikeLog = new TeacherStrikeLog();
			teacherStrikeLog.setTeacherDetail(teacherDetail);
			teacherStrikeLog.setTeacherAssessmentdetail(teacherAssessmentdetail);
			teacherStrikeLog.setTeacherAssessmentQuestion(teacherAssessmentQuestion);
			teacherStrikeLog.setIpAddress(ipAddress);
			teacherStrikeLog.setCreatedDateTime(new Date());

			teacherStrikeLogDAO.makePersistent(teacherStrikeLog);

			return 1;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to checkAssessmentDone.
	 */
	@Transactional(readOnly=false)
	public int checkAssessmentDone(TeacherAssessmentdetail teacherAssessmentdetail)
	{
		//System.out.println(":::::::::::::::Assessment checkAssessmentDone:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("apiTeacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");

			teacherAssessmentdetail = teacherAssessmentDetailDAO.findById(teacherAssessmentdetail.getTeacherAssessmentId(), false, false);

			if(teacherAssessmentdetail.getIsDone())
				return 1;
			else
				return 0;

		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	/* @Author: Gagan 
	 * @Discription: It is used to check isAffilated check.
	 */
	@Transactional(readOnly=false)
	public int checkIsAffilated(Integer jobId,Integer isAffilated)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("apiTeacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");


			JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			List<StatusMaster> lstStatusMasters =  Utility.getStaticMasters(new String[]{"comp","icomp","vlt"});

			List<JobForTeacher> lstjobForTeacher=jobForTeacherDAO.findDistrictOrSchoolLatestAppliedJobByTeacher(teacherDetail,jobOrder.getDistrictMaster(),lstStatusMasters); 

			if(lstjobForTeacher.size()>0)
			{
				/* ========= checking Last Latest Job Applied ==  isAffilated value should be different       ===================*/
				if((lstjobForTeacher.get((lstjobForTeacher.size()-1)).getIsAffilated()-isAffilated)!=0)
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				return 0;
			}

		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		//return jobId;
		//return 0;

	}


	/* @Author: Gagan 
	 * @Discription: It is used to check isAffilated check.
	 */
	@Transactional(readOnly=false)
	public int resetjftIsAffilated(Integer jobId,Integer isAffilated)
	{
		//	System.out.println(":::::::::::::::Assessment resetjftIsAffilated:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			TeacherDetail teacherDetail = null;
			StatusMaster statusMaster = null;
			TeacherAssessmentStatus teacherAssessmentStatus = null;
			AssessmentDetail assessmentDetail = null;
			if (session == null || session.getAttribute("apiTeacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");

			TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);

			JobOrder jobOrder1=jobOrderDAO.findById(jobId, false, false);
			Map<String,StatusMaster> statusMap = new HashMap<String, StatusMaster>();
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			String[] statuss = {"comp","icomp","vlt"};
			try{
				lstStatusMasters = Utility.getStaticMasters(statuss);
				for (StatusMaster statusmaster : lstStatusMasters) {
					statusMap.put(statusmaster.getStatusShortName(), statusmaster);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}


			List<JobForTeacher> lstjobForTeacher=jobForTeacherDAO.findDistrictOrSchoolLatestAppliedJobByTeacher(teacherDetail,jobOrder1.getDistrictMaster(),lstStatusMasters); 

			/*==================================== Resetiing Status Here ==========================*/
			boolean completeFlag=false;
			if(lstjobForTeacher.size()>0)
			{
				for(JobForTeacher jb:lstjobForTeacher)
				{
					int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jb) ;
					statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jb,flagList);
					try{
						String jftStatus=jb.getStatus().getStatusShortName();
						if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
							completeFlag=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					/*----------------------------------------------------------------------------------------------------------*/
					jb.setJobForTeacherId(jb.getJobForTeacherId());
					jb.setIsAffilated(isAffilated);
					if(jb.getInternalSecondaryStatus()!=null){
						jb.setStatus(jb.getInternalStatus());
						jb.setStatusMaster(null);
						jb.setSecondaryStatus(jb.getInternalSecondaryStatus());
					}else if(jb.getInternalStatus()!=null){
						jb.setStatus(jb.getInternalStatus());
						jb.setStatusMaster(jb.getInternalStatus());
					}else{
						jb.setStatus(statusMaster);
						jb.setStatusMaster(statusMaster);
					}
					jobForTeacherDAO.makePersistent(jb);
					 try{
						 if(completeFlag && jb.getStatus()!=null && jb.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
							commonService.futureJobStatus(jb.getJobId(), teacherDetail,jb);
						 }
					 }catch(Exception e){
						 e.printStackTrace();
					 }
				}
			}
			else
			{
				return 0;
			}

		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		//return jobId;
		return 1;

	}

	// Gagan : For getting latest cover letter by Teacher
	@Transactional(readOnly=false)
	public JobForTeacher getLatestCoverLetter()
	{

		//System.out.println(":::::::::::::::Assessment getLatestCoverLetter:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			//System.out.println("calling... getLatestCoverLetter Method");
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("apiTeacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");
			JobForTeacher jFTCover		= jobForTeacherDAO.findJobByTeacherAndCoverLetter(teacherDetail);

			if(jFTCover==null)
				return null;
			else
				return jFTCover;	

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	/* @Author: Gagan 
	 * @Discription: It is used to check isAffilated check.
	 */
	@Transactional(readOnly=false)
	public String getDistrictSpecificQuestion(Integer jobId,String isAffilated)
	{
		//System.out.println("*****:::::::::::::::Assessment getDistrictSpecificQuestion:::::::::::::::::::;; ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer sb =new StringBuffer();
		try{
			List<DistrictSpecificQuestions> districtSpecificQuestionList	=	null; 
			TeacherDetail teacherDetail = null;

			if (session == null || session.getAttribute("apiTeacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");

			if(jobId!=0)
			{
				JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
				JobForTeacher jft = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
				districtSpecificQuestionList=districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(jobOrder.getDistrictMaster()); 
				List<JobOrder> jobOrders = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findLastTeacherAnswersByDistrictAndJob(teacherDetail,jobOrder);
				List<TeacherAnswerDetailsForDistrictSpecificQuestions> lastList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();

				if(jobOrders.size()>0)
					lastList = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersByDistrictAndJob(teacherDetail,jobOrders.get(0));

				Map<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions> map = new HashMap<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions>();

				for(TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion : lastList)
					map.put(teacherAnswerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId(), teacherAnswerDetailsForDistrictSpecificQuestion);

				TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion = null;
				List<OptionsForDistrictSpecificQuestions> questionOptionsList = null;
				String questionInstruction = null;
				String shortName = "";
				int totalQuestions = districtSpecificQuestionList.size();
				int cnt = 0;


				///***** Sekhar Add TranseferCandidate ******///
				DistrictMaster districtMaster=null;
				if(jobOrder.getDistrictMaster()!=null){
					districtMaster=jobOrder.getDistrictMaster();
				}
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
				boolean internalFlag=false;
				boolean getQQ=true;
				if(itcList.size()>0){
					internalFlag=true;
				}
				if(districtMaster!=null && internalFlag){
					if(districtMaster.getOfferQualificationItems()){
						getQQ=true;
					}else{
						getQQ=false;
					}
				}
				try{
					if(jft!=null){
						if(jft.getIsAffilated()!=null && jft.getIsAffilated()==1 && internalFlag==false){
							if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferQualificationItems()){
								getQQ=true;
							}else{
								getQQ=false;
							}
						}
					}else{
						if(isAffilated!=null && isAffilated.equalsIgnoreCase("1") && internalFlag==false){
							if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferQualificationItems()){
								getQQ=true;
							}else{
								getQQ=false;
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}

				if(getQQ)
					for (DistrictSpecificQuestions districtSpecificQuestion : districtSpecificQuestionList) 
					{
						shortName = districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeShortName();

						sb.append("<tr>");
						sb.append("<td width='90%'>");
						sb.append("<div><b>"+Utility.getLocaleValuePropByKey("lblQues", locale)+" "+(++cnt)+" of "+totalQuestions+"</b></div>");

						questionInstruction = districtSpecificQuestion.getQuestionInstructions()==null?"":districtSpecificQuestion.getQuestionInstructions();
						if(!questionInstruction.equals(""))
							sb.append(""+Utility.getUTFToHTML(questionInstruction)+"");

						sb.append("<div id='Q"+cnt+"question'>"+Utility.getUTFToHTML(districtSpecificQuestion.getQuestion())+"</div>");

						questionOptionsList = districtSpecificQuestion.getQuestionOptions();

						teacherAnswerDetailsForDistrictSpecificQuestion = map.get(districtSpecificQuestion.getQuestionId());

						String checked = "";
						Integer optId = 0;
						String insertedText = "";
						if(teacherAnswerDetailsForDistrictSpecificQuestion!=null)
						{
							if(shortName.equalsIgnoreCase(teacherAnswerDetailsForDistrictSpecificQuestion.getQuestionType()))
							{
								if(teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions()!=null)
									optId = Integer.parseInt(teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions());

								insertedText = teacherAnswerDetailsForDistrictSpecificQuestion.getInsertedText();
							}
						}
						if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel"))
						{
							sb.append("<table>");
							for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
								if(optId.equals(questionOptions.getOptionId()))
									checked = "checked";
								else
									checked = "";

								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
										" <input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
							}
							sb.append("</table>");
						}
						if(shortName.equalsIgnoreCase("et"))
						{
							sb.append("<table>");
							for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
								if(optId.equals(questionOptions.getOptionId()))
									checked = "checked";
								else
									checked = "";

								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
										" <input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
							}
							sb.append("</table>");

							if(districtSpecificQuestion.getQuestionExplanation()!=null)
								sb.append("<div>"+Utility.getUTFToHTML(districtSpecificQuestion.getQuestionExplanation())+"</div>");

							sb.append("&nbsp;&nbsp;<span style=''><textarea  name='Q"+cnt+"opt' id='Q"+cnt+"optet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							sb.append("document.getElementById('Q"+cnt+"optet').focus();");
							sb.append("</script>");
						}
						if(shortName.equalsIgnoreCase("ml"))
						{
							sb.append("&nbsp;&nbsp;<textarea name='Q"+cnt+"opt' id='Q"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");					
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							sb.append("document.getElementById('Q"+cnt+"opt').focus();");
							sb.append("</script>");
						}
						sb.append("<input type='hidden' id='Q"+cnt+"questionId' value='"+districtSpecificQuestion.getQuestionId()+"'/>" );
						sb.append("<input type='hidden' id='Q"+cnt+"questionTypeId' value='"+districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
						sb.append("<input type='hidden' id='Q"+cnt+"questionTypeShortName' value='"+shortName+"'/>" );

						sb.append("</td>");
						sb.append("</tr>");
						insertedText = "";
					}
				sb.append("@##@"+cnt+"@##@"+jobOrder.getDistrictMaster().getTextForDistrictSpecificQuestions());

				List<JobForTeacher> lstJobForTeacher= null;
				JobForTeacher jobForTeacher = null;
				lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail,jobOrder);
				boolean completeFlag=false;
				if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
				{
					PrintOnConsole.debugPrintln("getDistrictSpecificQuestion ... Not save in JFT");
				}else
				{
					PrintOnConsole.debugPrintln("getDistrictSpecificQuestion ... already saved in JFT");
					////////////////////Checking status to be set///////////////////////
					jobForTeacher  = lstJobForTeacher.get(0);
					StatusMaster statusMaster = null;
					if(districtSpecificQuestionList.size()>0 && jobForTeacher.getFlagForDistrictSpecificQuestions()==null)
					{
						statusMaster = WorkThreadServlet.statusMap.get("icomp");
						if(jobForTeacher.getInternalSecondaryStatus()!=null){
							jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
							jobForTeacher.setStatusMaster(null);
							jobForTeacher.setSecondaryStatus(jobForTeacher.getInternalSecondaryStatus());
						}else if(jobForTeacher.getInternalStatus()!=null){
							try{
								if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
									jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
									jobForTeacher.setStatusMaster(jobForTeacher.getInternalStatus());
								}
							}catch(Exception e){}
						}else{
							try{
								if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
									jobForTeacher.setStatus(statusMaster);
									if(jobForTeacher.getSecondaryStatus()!=null){
										jobForTeacher.setStatusMaster(null);
									}else{
										jobForTeacher.setStatusMaster(statusMaster);
									}
								}
							}catch(Exception e){}
						}
					}else
					{
						boolean baseRequired = jobOrder.getJobCategoryMaster().getBaseStatus();
						boolean isJobAssessment = jobOrder.getIsJobAssessment();
						boolean portfolioRequired = jobOrder.getIsPortfolioNeeded(); 

						/*String isAffilated = "0";
					if(jobForTeacher.getIsAffilated()!=null)
						isAffilated = ""+jobForTeacher.getIsAffilated();

					statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher, isAffilated,internalFlag,offerEPI,offerJSI,isPortfolio);*/

						int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
						statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);

						
						if(jobForTeacher.getInternalSecondaryStatus()!=null){
							jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
							jobForTeacher.setStatusMaster(null);
							jobForTeacher.setSecondaryStatus(jobForTeacher.getInternalSecondaryStatus());
							if(jobForTeacher.getInternalStatus()!=null){
								jobForTeacher.setApplicationStatus(jobForTeacher.getInternalStatus().getStatusId());
							}
						}else if(jobForTeacher.getInternalStatus()!=null){
							try{
								String jftStatus=jobForTeacher.getStatus().getStatusShortName();
								if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
									completeFlag=true;
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							try{
								if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
									jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
									jobForTeacher.setStatusMaster(jobForTeacher.getInternalStatus());
									if(jobForTeacher.getInternalStatus()!=null){
										jobForTeacher.setApplicationStatus(jobForTeacher.getInternalStatus().getStatusId());
									}
								}
							}catch(Exception e){}
						}else{
							try{
								String jftStatus=jobForTeacher.getStatus().getStatusShortName();
								if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
									completeFlag=true;
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							try{
								if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
									if(statusMaster!=null){
										jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
									}
									jobForTeacher.setStatus(statusMaster);
									if(jobForTeacher.getSecondaryStatus()!=null){
										jobForTeacher.setStatusMaster(null);
									}else{
										jobForTeacher.setStatusMaster(statusMaster);
									}
								}
							}catch(Exception e){}
						}
					}
					jobForTeacherDAO.makePersistent(jobForTeacher);
					
					 try{
						 if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
							commonService.futureJobStatus(jobOrder, teacherDetail,jobForTeacher);
						 }
					 }catch(Exception e){
						 e.printStackTrace();
					 }
				}
			}
			//////////////////////	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	@Transactional(readOnly=false)
	public String setDistrictQuestions(TeacherAnswerDetailsForDistrictSpecificQuestions[] answerDetailsForDistrictSpecificQuestions)
	{
		//System.out.println(" *** ::::::::::::::::::::setDistrictQuestions:::::::::::::::::::");
		//System.out.println(":::::::::::::::Assessment setDistrictQuestions:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		TeacherDetail teacherDetail = null;

		if (session == null || session.getAttribute("apiTeacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");

		try {

			int inValidCount = 0;
			JobOrder jobOrder = answerDetailsForDistrictSpecificQuestions[0].getJobOrder();
			jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);

			//List<TeacherAnswerDetailsForDistrictSpecificQuestions> lastList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			Map<Integer,Integer> map = new HashMap<Integer, Integer>();

			try {
				map = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersForQQByDistrict(teacherDetail,jobOrder.getDistrictMaster());
			} catch (Exception e) {
				e.printStackTrace();
			}

			List<Integer> answers = new ArrayList<Integer>();
			Integer ansId = null;
			for (TeacherAnswerDetailsForDistrictSpecificQuestions answerDetailsForDistrictSpecificQuestion : answerDetailsForDistrictSpecificQuestions) {
				answerDetailsForDistrictSpecificQuestion.setTeacherDetail(teacherDetail);
				answerDetailsForDistrictSpecificQuestion.setDistrictMaster(jobOrder.getDistrictMaster());
				answerDetailsForDistrictSpecificQuestion.setCreatedDateTime(new Date());

				ansId = map.get(answerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId());
				if(ansId!=null)
				{
					answerDetailsForDistrictSpecificQuestion.setAnswerId(ansId);
				}
				if(!answerDetailsForDistrictSpecificQuestion.getQuestionType().equalsIgnoreCase("ml"))
				{
					if(answerDetailsForDistrictSpecificQuestion.getIsValidAnswer()==false)
						inValidCount++;
				}

				answerDetailsForDistrictSpecificQuestion.setIsActive(true);
				teacherAnswerDetailsForDistrictSpecificQuestionsDAO.makePersistent(answerDetailsForDistrictSpecificQuestion);
				answers.add(answerDetailsForDistrictSpecificQuestion.getAnswerId());
			}

			if(answers.size()>0)
				teacherAnswerDetailsForDistrictSpecificQuestionsDAO.updateOtherQQ(teacherDetail,jobOrder.getDistrictMaster(),answers);

			List<JobForTeacher> lstJobForTeacher= null;
			JobForTeacher jobForTeacher = null;
			lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail,jobOrder);
			boolean validFlag = false;

			if(inValidCount>0)
				validFlag=false;
			else
				validFlag=true;

			if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
			{
				session.setAttribute("jobValid"+jobOrder.getJobId(),validFlag);
				PrintOnConsole.debugPrintln("setDistrictQuestions ... Not save in JFT");
			}else
			{
				PrintOnConsole.debugPrintln("setDistrictQuestions ... already saved in JFT");

				jobForTeacher  = lstJobForTeacher.get(0);
				jobForTeacher.setFlagForDistrictSpecificQuestions(validFlag);
				////////////////////Checking status to be set///////////////////////
				StatusMaster statusMaster = null;
				boolean baseRequired = jobOrder.getJobCategoryMaster().getBaseStatus();
				boolean isJobAssessment = jobOrder.getIsJobAssessment();
				boolean portfolioRequired = jobOrder.getIsPortfolioNeeded(); 
				boolean completeFlag=false;
				//statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher, isAffilated,internalFlag,offerEPI,offerJSI,isPortfolio);
				int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
				statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
				try{
					String jftStatus=jobForTeacher.getStatus().getStatusShortName();
					if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
						completeFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(jobForTeacher.getInternalSecondaryStatus()!=null){
					jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
					jobForTeacher.setStatusMaster(null);
					jobForTeacher.setSecondaryStatus(jobForTeacher.getInternalSecondaryStatus());
					if(jobForTeacher.getInternalStatus()!=null){
						jobForTeacher.setApplicationStatus(jobForTeacher.getInternalStatus().getStatusId());
					}
				}else if(jobForTeacher.getInternalStatus()!=null){
					jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
					jobForTeacher.setStatusMaster(jobForTeacher.getInternalStatus());
					if(jobForTeacher.getInternalStatus()!=null){
						jobForTeacher.setApplicationStatus(jobForTeacher.getInternalStatus().getStatusId());
					}
				}else{
					try{
						if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
							if(statusMaster!=null){
								jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
							}
							jobForTeacher.setStatus(statusMaster);
							if(jobForTeacher.getSecondaryStatus()!=null){
								jobForTeacher.setStatusMaster(null);
							}else{
								jobForTeacher.setStatusMaster(statusMaster);
							}
						}
					}catch(Exception e){}
				}

				PrintOnConsole.debugPrintln("try to set setIsAffilated flag in setDistrictQuestions");
				if (session != null && session.getAttribute("isCurrentEmploymentNeeded") != null) 
				{
					boolean isCurrentEmploymentNeeded=false;
					isCurrentEmploymentNeeded=(Boolean)session.getAttribute("isCurrentEmploymentNeeded");
					PrintOnConsole.debugPrintln("setDistrictQuestions isCurrentEmploymentNeeded "+isCurrentEmploymentNeeded);
					if(isCurrentEmploymentNeeded)
					{
						TeacherPersonalInfo teacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
						if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType()!=null && teacherPersonalInfo.getEmployeeType()==1)
							jobForTeacher.setIsAffilated(1);
						else
							jobForTeacher.setIsAffilated(0);
					}
					session.removeAttribute("isCurrentEmploymentNeeded");
				}

				jobForTeacherDAO.makePersistent(jobForTeacher);
				////////////////////////////////////
				 try{
					 if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
						commonService.futureJobStatus(jobOrder, teacherDetail,jobForTeacher);
					 }
				 }catch(Exception e){
					 e.printStackTrace();
				 }
			}

			lstJobForTeacher=new ArrayList<JobForTeacher>();
			lstJobForTeacher = jobForTeacherDAO.findJobAppliedByTeacherAndDistrict(teacherDetail, jobOrder.getDistrictMaster(), true);

			if(lstJobForTeacher.size()==0)
			{
				List<JobForTeacher> lstJobForTeacherUpdate = jobForTeacherDAO.findJobAppliedByTeacherAndDistrict(teacherDetail, jobOrder.getDistrictMaster(), null);
				if(lstJobForTeacherUpdate.size()>0)
				{
					for(JobForTeacher jft:lstJobForTeacherUpdate)
					{
						jft.setFlagForDistrictSpecificQuestions(validFlag);
						jobForTeacherDAO.makePersistent(jft);
					}
				}
			}else
			{
				List<JobForTeacher> lstJobForTeacherUpdate = jobForTeacherDAO.findJobAppliedByTeacherAndDistrict(teacherDetail, jobOrder.getDistrictMaster(), null);
				lstJobForTeacherUpdate.removeAll(lstJobForTeacher);
				if(lstJobForTeacherUpdate.size()>0)
				{
					for(JobForTeacher jft:lstJobForTeacherUpdate)
					{
						jft.setFlagForDistrictSpecificQuestions(validFlag);
						jobForTeacherDAO.makePersistent(jft);
					}
				}
			}

			return "1";

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	/* ====== Gagan : Save Job For teacher using ajax============ */
	@Transactional(readOnly=false)
	public int saveJobForTeacher(String jobId)
	{
		System.out.println("***:::::::::::::::Assessment saveJobForTeacher:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		TeacherDetail teacherDetail = null;

		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		if (session == null || session.getAttribute("apiTeacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");

		Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
		String jobBoardReferralURL = session.getAttribute("jobBoardReferralURL")==null?"":(String)session.getAttribute("jobBoardReferralURL");
		if(referer!=null)
		{			
			jobId = referer.get("jobId");
			String coverLetter = (String)(session.getAttribute("coverLetter")==null?"":session.getAttribute("coverLetter"));
			String isAffilated =(String)(session.getAttribute("isAffilated")==null?"0":session.getAttribute("isAffilated")); 
			String staffType =(String)(session.getAttribute("staffType")==null?"N":session.getAttribute("staffType"));
			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false); 

			JobForTeacher jobForTeacherObj = jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);

			try{
				DistrictMaster districtMaster = jobOrder.getDistrictMaster();
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
				if(itcList.size()>0){
					isAffilated="1";
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			boolean completeFlag=false;
			if(jobForTeacherObj==null)
			{
				//TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);

				JobForTeacher jobForTeacher = new JobForTeacher();
				jobForTeacher.setTeacherId(teacherDetail);
				jobForTeacher.setJobId(jobOrder);
				jobForTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
				jobForTeacher.setCoverLetter(coverLetter);
				jobForTeacher.setIsAffilated(Integer.parseInt(isAffilated));
				jobForTeacher.setCreatedDateTime(new Date());
				jobForTeacher.setStaffType(staffType);
				StatusMaster statusMaster = null;
				statusMaster = WorkThreadServlet.statusMap.get("icomp");
				int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
				statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
				TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jobForTeacher.getTeacherId());
				try{
					if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
						completeFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				try
				{
					String re = referer.get("refererURL");
					jobForTeacher.setRedirectedFromURL(re);
					jobForTeacher.setJobBoardReferralURL(jobBoardReferralURL);
					//jobForTeacher.setRedirectedFromURL("refererURL");//referer.get("refererURL")
					if(jobForTeacher.getInternalSecondaryStatus()!=null){
						jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
						jobForTeacher.setStatusMaster(null);
						jobForTeacher.setSecondaryStatus(jobForTeacher.getInternalSecondaryStatus());
					}else if(jobForTeacher.getInternalStatus()!=null){
						jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
						jobForTeacher.setStatusMaster(jobForTeacher.getInternalStatus());
					}else{
						jobForTeacher.setStatus(statusMaster);
						if(jobForTeacher.getSecondaryStatus()!=null){
							jobForTeacher.setStatusMaster(null);
						}else{
							jobForTeacher.setStatusMaster(statusMaster);
						}
					}
					if(session.getAttribute("jobValid"+jobOrder.getJobId())!=null)
					{
						if((Boolean)session.getAttribute("jobValid"+jobOrder.getJobId())==true)
							jobForTeacher.setFlagForDistrictSpecificQuestions(true);
						else
							jobForTeacher.setFlagForDistrictSpecificQuestions(false);
						session.removeAttribute("jobValid"+jobOrder.getJobId());
					}

					PrintOnConsole.debugPrintln("saveJobForTeacher ... Saved JFT");
					if (session != null && session.getAttribute("isCurrentEmploymentNeeded") != null) 
					{
						boolean isCurrentEmploymentNeeded=false;
						isCurrentEmploymentNeeded=(Boolean)session.getAttribute("isCurrentEmploymentNeeded");
						PrintOnConsole.debugPrintln("saveJobForTeacher isCurrentEmploymentNeeded "+isCurrentEmploymentNeeded);
						if(isCurrentEmploymentNeeded)
						{
							TeacherPersonalInfo teacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
							if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType()!=null && teacherPersonalInfo.getEmployeeType()==1)
								jobForTeacher.setIsAffilated(1);
							else
								jobForTeacher.setIsAffilated(0);
						}
						session.removeAttribute("isCurrentEmploymentNeeded");
					}

					jobForTeacherDAO.makePersistent(jobForTeacher);	
					 try{
						 if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
							commonService.futureJobStatus(jobOrder, teacherDetail,jobForTeacher);
						 }
					 }catch(Exception e){
						 e.printStackTrace();
					 }
					/* ======== Gagan : [ Start ] Functinality for to Send Email to all active DA\SA Admins acording to Job order, who has checked job apply CHECKBOX in notification [ Available ] checkbox ============== */
					List<UserMaster> lstusermaster	=	new ArrayList<UserMaster>();

					try{
						/* ===== Gagan : statusJobApply is used to get status short name against Apply which is used in Email sending to all active DA\SA Active Admins who has checked thier job apply CHECKBOX in notification [ Available ]======= */
						StatusMaster statusJobApply = WorkThreadServlet.statusMap.get("apl");
						String statusShortName = statusJobApply.getStatusShortName();
						boolean isSchool=false;
						if(jobOrder.getSelectedSchoolsInDistrict()!=null){
							if(jobOrder.getSelectedSchoolsInDistrict()==1){
								isSchool=true;
							}
						}
						if(jobOrder.getCreatedForEntity()==2 && isSchool==false){
							lstusermaster=userEmailNotificationsDAO.getUserByDistrict(jobOrder.getDistrictMaster(),statusShortName);
						}else if(jobOrder.getCreatedForEntity()==2 && isSchool){
							List<SchoolMaster>  lstschoolMaster= new ArrayList<SchoolMaster>();
							if(jobOrder.getSchool()!=null){
								if(jobOrder.getSchool().size()!=0){
									for(SchoolMaster sMaster : jobOrder.getSchool()){
										lstschoolMaster.add(sMaster);
									}
								}
							}
							lstusermaster=userEmailNotificationsDAO.getUserByDistrictAndSchoolList(jobOrder.getDistrictMaster(),lstschoolMaster,jobOrder,statusShortName);
						}else if(jobOrder.getCreatedForEntity()==3){
							lstusermaster=userEmailNotificationsDAO.getUserByOnlySchool(jobOrder.getSchool().get(0),statusShortName);
						}
					}catch(Exception e){
						e.printStackTrace();
					}



					TeacherAssessmentStatus _teacherAssessmentStatus =null;
					StatusMaster _statusMaster =	null;

					if(jobForTeacher.getJobId().getJobCategoryMaster().getBaseStatus())
					{
						List<TeacherAssessmentStatus> _teacherAssessmentStatusList = null;
						JobOrder _jOrder = new JobOrder();
						_jOrder.setJobId(0);
						_teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(),_jOrder);
						if(_teacherAssessmentStatusList.size()>0){
							_teacherAssessmentStatus = _teacherAssessmentStatusList.get(0);
							_statusMaster = teacherAssessmentStatus.getStatusMaster();
						}else{
							_statusMaster = WorkThreadServlet.statusMap.get("icomp");
						}
					}

					/*======= Gagan[ END ] : Check for  Base status for that job which against EPI required ==================*/

					String[] arrHrDetail = commonService.getHrDetailToTeacher(request,jobForTeacher.getJobId());
					arrHrDetail[10]	=	Utility.getBaseURL(request)+"signin.do";
					arrHrDetail[11] =	Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jobForTeacher.getJobId().getJobId()+"&JobOrderType="+jobForTeacher.getJobId().getCreatedForEntity();
					arrHrDetail[12] =	Utility.getShortURL(Utility.getBaseURL(request)+"jobsboard.do?districtId="+Utility.encryptNo(jobForTeacher.getJobId().getDistrictMaster().getDistrictId()));
					arrHrDetail[13] =	Utility.getValueOfPropByKey("basePath")+"/forgotpassword.do";
					try {
						if(_statusMaster.getStatusShortName()!=null)
							arrHrDetail[14] =	_statusMaster.getStatusShortName();
					} catch (NullPointerException e) {
						//e.printStackTrace();
					}
					String flagforEmail	="applyJob";

					MailSendToTeacher mst =new MailSendToTeacher(teacherDetail,jobOrder,request,arrHrDetail,flagforEmail,lstusermaster,emailerService,jobForTeacher);
					Thread currentThread = new Thread(mst);
					currentThread.start(); 

				}catch(Exception ex)
				{
					ex.printStackTrace();
					return 0;
				}
				//return 1;

			}
		}
		return 1;
	}

	@Transactional(readOnly=false)
	public String[] checkTeacherCriteria(JobOrder jobOrder)
	{
		//System.out.println(":::::::::::::::Assessment checkTeacherCriteria:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try {

			TeacherDetail teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");
			JobForTeacher jft = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
			String candidateType="E";
			if(jft.getIsAffilated()!=null && jft.getIsAffilated()==1)
				candidateType="I";
			boolean getQQ=false;
			boolean dspqFlag=false;
			DistrictPortfolioConfig districtPortfolioConfig = districtPortfolioConfigAjax.getPortfolioConfigByJobId(jobOrder.getJobId(),candidateType);
			if(districtPortfolioConfig!=null){
				dspqFlag = true;
			}

			try{
				jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false); 
				List<DistrictSpecificQuestions> districtSpecificQuestionList=districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(jobOrder.getDistrictMaster());
				if(districtSpecificQuestionList.size()>0)
				{
					getQQ=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			jobOrder = jft.getJobId();
			boolean isMiami = false;
			if(jobOrder.getDistrictMaster().getDistrictId().equals(1200390))
				isMiami=true;

			List<JobOrder> jobOrders = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findLastTeacherAnswersByDistrictAndJob(teacherDetail, jobOrder);
			String [] qqStr = new String[5]; 
			boolean qqFlag=false;
			if(jobOrders.size()==0)
				qqFlag = true;

			///***** Sekhar Add TranseferCandidate ******///
			DistrictMaster districtMaster=null;
			if(jobOrder.getDistrictMaster()!=null){
				districtMaster=jobOrder.getDistrictMaster();
			}
			List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
			boolean internalFlag=false;
			if(itcList.size()>0){
				internalFlag=true;
			}
			if(districtMaster!=null && internalFlag){
				if(districtMaster.getOfferDistrictSpecificItems()){
					dspqFlag=true;
				}else{
					dspqFlag=false;
				}
				if(districtMaster.getOfferQualificationItems()){
					getQQ=true;
				}else{
					getQQ=false;
				}
			}
			if(candidateType.equalsIgnoreCase("I") && internalFlag==false){
				if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferDistrictSpecificItems()){
					dspqFlag=true;
				}else{
					dspqFlag=false;
				}
				if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferQualificationItems()){
					getQQ=true;
				}else{
					getQQ=false;
				}
			}
			///////////***** Sekhar End *********//////////
			qqStr[0]=""+qqFlag;
			qqStr[1]=""+dspqFlag;
			qqStr[2]=""+jft.getIsAffilated();
			qqStr[3]=""+getQQ;
			qqStr[4]=""+isMiami;
			return qqStr;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @author Amit Chaudhary
	 * @param jobOrder
	 * @param assessmentType
	 * @param grpName
	 * @return AssessmentDetail
	 */
	public AssessmentDetail checkInventory(JobOrder jobOrder, Integer assessmentType, String grpName, boolean isUpdate, HttpServletRequest httpServletRequest)
	{
		System.out.println("\n ::::::::::::::: APIAssessmentCampaignAjax checkInventory(JobOrder jobOrder,String assessmentType,String grpName) :::::::::::::::::::\n");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = null;
		HttpSession session = null;
		if(isUpdate && httpServletRequest==null){
			request = context.getHttpServletRequest();
			session = request.getSession(false);
		}
		else{
			request = httpServletRequest;
			session = request.getSession(false);
		}
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		TeacherDetail teacherDetail = null;

		if (session == null || session.getAttribute("apiTeacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");

		System.out.println("jobOrder : "+jobOrder+" : jobOrder.getJobId() : "+jobOrder.getJobId());
		System.out.println("assessmentType : "+assessmentType+" : grpName : "+grpName);
		int jobId = 0;
		if(assessmentType==3)
			jobId=-3;
		else if(assessmentType==4)
			jobId=-4;
		
		AssessmentDetail assessmentDetail = new AssessmentDetail();
		TeacherAssessmentStatus teacherAssessmentStatus = null;
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jobOrder,assessmentType,null);
		System.out.println("teacherAssessmentStatusList.size() : "+teacherAssessmentStatusList.size());
		
		//if smart practices assessment is taken and passed.
		boolean isPass = false;
		if(assessmentType==3){
			if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0){
				for(TeacherAssessmentStatus tas : teacherAssessmentStatusList){
					if(tas.getPass()!=null && tas.getPass().equals("P"))
						isPass = true;
				}
			}
			if(isPass){
				System.out.println(" : APIAssessmentCampaignAjax : checkInventory : isPass : "+isPass);
				assessmentDetail.setAssessmentName(null);
				assessmentDetail.setStatus("6");
				return assessmentDetail;
			}
		}
		
		List<AssessmentGroupDetails> assessmentGroupDetailList = assessmentGroupDetailsDAO.getAssessmentGroupDetailsListByTypeAndName(assessmentType, grpName);
		
		AssessmentGroupDetails assessmentGroupDetails = null;
		if(assessmentGroupDetailList!=null && assessmentGroupDetailList.size()>0)
			assessmentGroupDetails = assessmentGroupDetailList.get(0);
		
		List<AssessmentDetail> assessmentDetailListByGroup = assessmentDetailDAO.getAssessmentDetailListByGroup(assessmentGroupDetails);
		
		//if assessment is not taken.
		if(teacherAssessmentStatusList.size()==0){
			if(assessmentDetailListByGroup!=null && assessmentDetailListByGroup.size()>0){
				Collections.shuffle(assessmentDetailListByGroup);
				System.out.println("assessmentDetailListByGroup.size() : "+assessmentDetailListByGroup.size()+" : assessmentDetailListByGroup.get(0).getAssessmentId() : "+assessmentDetailListByGroup.get(0).getAssessmentId());
				return assessmentDetailListByGroup.get(0);	
			}
			else{
				assessmentDetail.setAssessmentName(null);
				assessmentDetail.setStatus("1");
				return assessmentDetail;
			}
		}
		//if any of assessment is taken.
		else if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0){
			
			List<TeacherAssessmentStatus> teacherAssessmentStatusListByGroup = null;
			List<TeacherAssessmentStatus> teacherAssessmentStatusCompListByGroup = new ArrayList<TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> teacherAssessmentStatusVltListByGroup = new ArrayList<TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> teacherAssessmentStatusIcompListByGroup = new ArrayList<TeacherAssessmentStatus>();
			Integer assessmentTakenCount = null;
			teacherAssessmentStatusListByGroup = teacherAssessmentStatusDAO.findAssessmentTakenByAssessmentDetail(teacherDetail, assessmentDetailListByGroup);
			
			for(TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatusListByGroup){
				if(teacherAssessmentStatus2!=null){
					if(teacherAssessmentStatus2.getStatusMaster()!=null && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equals("comp"))
						teacherAssessmentStatusCompListByGroup.add(teacherAssessmentStatus2);
					else if(teacherAssessmentStatus2.getStatusMaster()!=null && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equals("vlt"))
						teacherAssessmentStatusVltListByGroup.add(teacherAssessmentStatus2);
					else if(teacherAssessmentStatus2.getStatusMaster()!=null && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equals("icomp"))
						teacherAssessmentStatusIcompListByGroup.add(teacherAssessmentStatus2);
				}
			}
			
			//if any of assessment is incomplete.
			if(teacherAssessmentStatusIcompListByGroup!=null && teacherAssessmentStatusIcompListByGroup.size()>0){
				teacherAssessmentStatus = teacherAssessmentStatusIcompListByGroup.get(0);
				assessmentDetail = teacherAssessmentStatus.getAssessmentDetail();
				assessmentTakenCount = teacherAssessmentStatus.getAssessmentTakenCount();
				
				int attemptsStrike=0;
				if(isUpdate){
					List<TeacherAssessmentAttempt> teacherAssessmentAttempts = null;
					teacherAssessmentAttempts = teacherAssessmentAttemptDAO.findAssessmentAttempts(teacherDetail,assessmentDetail,assessmentTakenCount,jobOrder);
					int attempts = 0;
					for (TeacherAssessmentAttempt teacherAssessmentAttempt : teacherAssessmentAttempts) {
						if(!teacherAssessmentAttempt.getIsForced())
							attempts++;
					}
	
					int strikeLogs=0;
					try{
						if(teacherAssessmentAttempts.size()>0)
						{
							List<TeacherAssessmentStatus> teacherAssessmentStatusList2 = null;
							teacherAssessmentStatusList2 = teacherAssessmentStatusDAO.getTeacherAssessmentStatus(teacherAssessmentAttempts.get(0).getAssessmentDetail(),teacherDetail);
							List<TeacherStrikeLog> teacherStrikeLog = null;
							teacherStrikeLog = teacherStrikeLogDAO.getTotalStrikesByTeacher(teacherDetail, teacherAssessmentStatusList2.get(0).getTeacherAssessmentdetail());
							strikeLogs=teacherStrikeLog.size();
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					attemptsStrike=attempts+strikeLogs;
	
					if(attemptsStrike==3)
					{
						StatusMaster statusMaster = WorkThreadServlet.statusMap.get("vlt");
						TeacherAssessmentdetail teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();
	
						List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
						lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentdetail);
						teacherAssessmentStatus=lstTeacherAssessmentStatus.get(0);
	
						if(lstTeacherAssessmentStatus.size()!=0)
							if(teacherAssessmentStatus!=null)
							{
								for(TeacherAssessmentStatus tas: lstTeacherAssessmentStatus)
								{
									tas.setStatusMaster(statusMaster);
									teacherAssessmentStatusDAO.makePersistent(tas);
								}
							}
					}
				}
				TeacherAssessmentStatus teacherAssessmentStatus2 = teacherAssessmentStatusDAO.findById(teacherAssessmentStatus.getTeacherAssessmentStatusId(), false, false);
				if(teacherAssessmentStatus2.getAssessmentType()==4 && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
					System.out.println(":::::::::: checkinventory vlt postIPIJSON");
					TMCommonUtil.postIPIJSON(session.getAttribute("ipiReferralURL")==null?"":(String)session.getAttribute("ipiReferralURL"), Utility.getBaseURL(request), teacherAssessmentStatus, teacherAssessmentStatusDAO, assessmentDomainScoreDAO, assessmentCompetencyScoreDAO);
				}
				assessmentDetail.setStatus(assessmentDetail.getStatus()+"#"+attemptsStrike+"#"+assessmentTakenCount+"#"+jobId);

				return assessmentDetail;
			}
			
			int sizeComp=0;
			int sizeVlt=0;
			if(teacherAssessmentStatusCompListByGroup!=null)
				sizeComp = teacherAssessmentStatusCompListByGroup.size();
			if(teacherAssessmentStatusVltListByGroup!=null)
				sizeVlt = teacherAssessmentStatusVltListByGroup.size();
			int sizeTotal = sizeComp+sizeVlt;
			//if two assessment are completed or timed out in a Group of SP (only for SPs).
			//if(assessmentType==3 && (teacherAssessmentStatusCompListByGroup!=null && teacherAssessmentStatusCompListByGroup.size()==2) || (teacherAssessmentStatusVltListByGroup!=null && teacherAssessmentStatusVltListByGroup.size()==2) || (teacherAssessmentStatusCompListByGroup!=null && teacherAssessmentStatusCompListByGroup.size()==1 && teacherAssessmentStatusVltListByGroup!=null && teacherAssessmentStatusVltListByGroup.size()==1)){
			if(assessmentType==3 && sizeTotal==2){
				assessmentDetail.setAssessmentName(null);
				assessmentDetail.setStatus("8");
				return assessmentDetail;
			}
			
			//if more than one assessment is taken.
			List<AssessmentDetail> assessmentDetailCompList = new ArrayList<AssessmentDetail>();
			List<AssessmentDetail> assessmentDetailVltList = new ArrayList<AssessmentDetail>();
			
			//remove comp status list.
			if(teacherAssessmentStatusCompListByGroup!=null && teacherAssessmentStatusCompListByGroup.size()>0){
				
				for(TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatusCompListByGroup)
					assessmentDetailCompList.add(teacherAssessmentStatus2.getAssessmentDetail());
				
				assessmentDetailListByGroup.removeAll(assessmentDetailCompList);
			}
			
			//remove vlt status list.
			if(teacherAssessmentStatusVltListByGroup!=null && teacherAssessmentStatusVltListByGroup.size()>0){
				
				for(TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatusVltListByGroup)
					assessmentDetailVltList.add(teacherAssessmentStatus2.getAssessmentDetail());
				
				assessmentDetailListByGroup.removeAll(assessmentDetailVltList);
			}
			
			if(assessmentDetailListByGroup!=null && assessmentDetailListByGroup.size()>0){
				Collections.shuffle(assessmentDetailListByGroup);
				System.out.println("::: assessmentDetailListByGroup.size() : "+assessmentDetailListByGroup.size()+" : assessmentDetailListByGroup.get(0).getAssessmentId() : "+assessmentDetailListByGroup.get(0).getAssessmentId());
				return assessmentDetailListByGroup.get(0);	
			}
			else if(assessmentDetailListByGroup!=null && assessmentDetailListByGroup.size()==0){
				assessmentDetailListByGroup = assessmentDetailDAO.getAssessmentDetailListByGroup(assessmentGroupDetails);
				if(assessmentDetailListByGroup!=null && assessmentDetailListByGroup.size()>0){
					Collections.shuffle(assessmentDetailListByGroup);
					System.out.println("::: ::: assessmentDetailListByGroup.size() : "+assessmentDetailListByGroup.size()+" : assessmentDetailListByGroup.get(0).getAssessmentId() : "+assessmentDetailListByGroup.get(0).getAssessmentId());
					return assessmentDetailListByGroup.get(0);	
				}
				else{
					assessmentDetail.setAssessmentName(null);
					assessmentDetail.setStatus("1");
					return assessmentDetail;
				}
			}
		}
		
		return null;
	}
	
	public void saveAffidavitFromAPI(boolean affidavit){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = null;
		HttpSession session = null;
		if(httpServletRequest!=null && httpServletRequest.getSession()!=null){
			request = httpServletRequest;
			session = request.getSession(false);
		}
		else{
			request = context.getHttpServletRequest();
			session = request.getSession(false);
		}
		
		if(session == null ||(session.getAttribute("apiTeacherDetail")==null && session.getAttribute("userMaster")==null))  
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		TeacherDetail teacherDetail = null;

		if (session == null || session.getAttribute("apiTeacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("apiTeacherDetail");
		try{
			TeacherPortfolioStatus teacherPortfolioStatus = null;
			TeacherAffidavit teacherAffidavit = null;
			
			if(teacherDetail!=null){
				teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				teacherAffidavit = teacherAffidavitDAO.findAffidavitByTeacher(teacherDetail);
				
				if(teacherPortfolioStatus==null){
					teacherPortfolioStatus = new TeacherPortfolioStatus();
					teacherPortfolioStatus.setTeacherId(teacherDetail);
					teacherPortfolioStatus.setIsAcademicsCompleted(false);
					teacherPortfolioStatus.setIsCertificationsCompleted(false);
					teacherPortfolioStatus.setIsExperiencesCompleted(false);
					teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
					teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
				}
				teacherPortfolioStatus.setIsAffidavitCompleted(true);
				
				if(teacherAffidavit==null){
					teacherAffidavit = new TeacherAffidavit();
					teacherAffidavit.setTeacherId(teacherDetail);
				}
					teacherAffidavit.setAffidavitAccepted(true);
					teacherAffidavit.setIsDone(true);
					teacherAffidavit.setCreatedDateTime(new Date());
					teacherAffidavitDAO.makePersistent(teacherAffidavit);
			}
			
			

		}catch(Exception e){
			e.printStackTrace();
		}
		

	}
	
	public void sendMailToCandidate(Integer assessmentType,String status,Integer jobId,Integer epiJobId)
	{
		System.out.println(":::::::::::::::::::APIAssessmentCampaignAjax : sendMailToCandidate:::::::::::::::::::");
		assessmentCampaignAjax.sendMailToCandidate(assessmentType, status, jobId, epiJobId);
	}
}
