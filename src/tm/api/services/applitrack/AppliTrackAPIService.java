package tm.api.services.applitrack;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.WebApplicationContextUtils;
import tm.bean.JobOrder;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherRole;
import tm.bean.master.CityMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.CurrencyMaster;
import tm.bean.master.DegreeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.EmpRoleTypeMaster;
import tm.bean.master.EthinicityMapping;
import tm.bean.master.FieldMaster;
import tm.bean.master.FieldOfStudyMaster;
import tm.bean.master.GenderMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.UniversityMaster;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherRoleDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.CurrencyMasterDAO;
import tm.dao.master.DegreeMasterDAO;
import tm.dao.master.EthinicityMappingDAO;
import tm.dao.master.FieldMasterDAO;
import tm.dao.master.FieldOfStudyMasterDAO;
import tm.dao.master.GenderMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.UniversityMasterDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;
import com.sun.jersey.api.core.InjectParam;

public class AppliTrackAPIService 
{
	
	@InjectParam
	private JobOrderDAO jobOrderDAO;
	
	@InjectParam
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@InjectParam
	private TeacherDetailDAO teacherDetailDAO;
	
	@InjectParam
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@InjectParam
	private StateMasterDAO stateMasterDAO;
	
	@InjectParam
	private CityMasterDAO cityMasterDAO;
	
	@InjectParam
	private CountryMasterDAO countryMasterDAO;
	
	@InjectParam
	private GenderMasterDAO genderMasterDAO;
	
	@InjectParam
	private CurrencyMasterDAO currencyMasterDAO;
	
	@InjectParam
	private TeacherRoleDAO teacherRoleDAO;
	
	@InjectParam
	private FieldMasterDAO fieldMasterDAO;
	
	@InjectParam
	private TeacherAcademicsDAO teacherAcademicsDAO;
	
	@InjectParam
	private DegreeMasterDAO degreeMasterDAO;
	
	@InjectParam
	private UniversityMasterDAO universityMasterDAO;
	
	@InjectParam
	private FieldOfStudyMasterDAO fieldOfStudyMasterDAO;
	
	@InjectParam
	private EthinicityMappingDAO ethinicityMappingDAO;
	
	public static String getStringValue(JSONObject jsonRequest,String sAttributeName)
	{
		String sReturnValue=""; 
		try {
			sReturnValue=jsonRequest.getString(sAttributeName);
			//System.out.println(sAttributeName +" => "+sReturnValue);
		} catch (Exception e) {e.printStackTrace();}
		finally{
			return sReturnValue;}
	}
	
	public static boolean chkString_Not_Null_And_Blank(String sInputString)
	{
		boolean bReturnValue=false; 
		try {
			if(sInputString==null)
			{
				bReturnValue=true;
			}else if(!sInputString.equals("") || !sInputString.equalsIgnoreCase("null"))
				bReturnValue=true;
			
		} catch (Exception e) {e.printStackTrace();}
		finally{
			return bReturnValue;}
	}
	
	public static String replaceDash(String sInputString)
	{
		try {
			if(sInputString!=null && !sInputString.equals(""))
				sInputString=sInputString.replace("-", "").replace("(", "").replace(")", "");
		} catch (Exception e) {e.printStackTrace();}
		finally{
			return sInputString;}
	}
	
	public static String replaceDot(String sInputString)
	{
		try {
			if(sInputString!=null && !sInputString.equals(""))
				sInputString=sInputString.replace(".", "");
		} catch (Exception e) {e.printStackTrace();}
		finally{
			//System.out.println(sInputString +" => "+sInputString);
			return sInputString;}
	}
	
	public String getUniversityName(String sInput)
	{
		String sReturnValue=sInput;
		try {
			if(sInput.lastIndexOf(" in ") >0)
				sReturnValue=sInput.substring(0, sInput.indexOf(" in "));
		} catch (Exception e) {
			sReturnValue=sInput;
		}
		//System.out.println(sInput +" => "+sReturnValue);
		return sReturnValue;
	}
	
	public long getYear(String sInput)
	{
		long sReturnValue=0;
		String sTemp="";
		try {
			sTemp=sInput.substring(3,7);
		} catch (Exception e) {
		}
		sReturnValue=Utility.getIntValue(sTemp);
		//System.out.println(sInput +" => "+sReturnValue);
		return sReturnValue;
	}
	
	public double getDoubleValue(String sInput)
	{
		double dReturnValue=0.0;
		try {
			dReturnValue=Double.parseDouble(sInput);
		} catch (Exception e) {
		}
		return dReturnValue;
	}
	
	public static boolean chkObject_Not_Null(Object obj)
	{
		boolean bReturnValue=false; 
		try {
			if(obj!=null)
				bReturnValue=true;
		} catch (Exception e) {e.printStackTrace();}
		finally{
			return bReturnValue;}
	}
	
	public static int[] getIntArryFromString(String sInputValue)
	{
		int iArr[]=new int[2];
		
		try {
			String arr[]=sInputValue.split("/");
			if(arr!=null && arr.length ==2)
			{
				iArr[0]=Utility.getIntValue(arr[0]);
				iArr[1]=Utility.getIntValue(arr[1]);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			return iArr;
		}
		
	}
	
	public static int getSalary(String sInputString)
	{
		int iReturnValue=0;
		try {
			sInputString=sInputString.replace("$", "").replace(",", "").replace(" ", "");
			if(sInputString.indexOf(".") >0)
				sInputString=sInputString.substring(0, sInputString.indexOf("."));
			if(sInputString!=null && !sInputString.equalsIgnoreCase(""))
				iReturnValue=Utility.getIntValue(sInputString);
		} catch (Exception e) {e.printStackTrace();}
		finally{
			return iReturnValue;}
	}

	@Transactional(readOnly=false)
	public JobOrder getJob(@Context HttpServletRequest request, @Context HttpServletResponse res,DistrictMaster districtMaster, String apiJobId,String auths[])
    {
		System.out.println(new Date()+ "Start calling getJob ... apiJobId "+apiJobId +" districtMaster "+districtMaster);
		
		if(districtMaster!=null)
		System.out.println(new Date()+ "Start calling getJob ... apiJobId "+apiJobId +" districtMaster "+districtMaster.getDistrictId());
		
		JobOrder jobOrder=null;
		if(apiJobId!=null && !apiJobId.equals("") && districtMaster!=null)
		{
			
			String sMethodAndParameters="JobPostings("+apiJobId+")?$format=json";
			
			ServletContext context = request.getSession().getServletContext();
			ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
			jobOrderDAO = (JobOrderDAO)context0.getBean("jobOrderDAO");
			jobCategoryMasterDAO = (JobCategoryMasterDAO)context0.getBean("jobCategoryMasterDAO");
			System.out.println(new Date()+ "Start ::::"+apiJobId+"::: districtMaster ::"+districtMaster.getDistrictId()+":::::");
			List<JobOrder> lstJobOrder=jobOrderDAO.getJOByDID_APIJID(districtMaster, apiJobId);
			System.out.println("lstJobOrder List:::"+lstJobOrder.size());
			if(lstJobOrder!=null && lstJobOrder.size()>0)
			{
				jobOrder =new JobOrder(); 
				jobOrder=lstJobOrder.get(0);
				System.out.println("::::::::::::::Inside >>::::::::::");
				//System.out.println("TM DB Job Id = "+jobOrder.getJobId());
			}
			
			////////////////////////////////////////
	    	HttpClient client = new HttpClient();
	        client.getState().setCredentials(new AuthScope(AppliTrackAPIConstant.sAPPLITRACKAPI_HOST, AppliTrackAPIConstant.sAPPLITRACKAPI_PORT, auths[3]),new UsernamePasswordCredentials(auths[0], auths[1]));
	        List authPrefs = new ArrayList(1);
	        authPrefs.add(AuthPolicy.BASIC);
	        client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
	        GetMethod httpget = new GetMethod(auths[2]+""+sMethodAndParameters);
	        try {
	            int status = client.executeMethod(httpget);
	            //System.out.println("API status "+status+" "+httpget.getStatusLine());
	           /* //System.out.println("----------- Response Data -----------------------------------");
	            //System.out.println(httpget.getResponseBodyAsString());
	            //System.out.println("------------ End Response Data -------------------------------");*/
	            if(status==200)
	            {
	            	JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( httpget.getResponseBodyAsString() ); 
	        		if(jsonRequest!=null){
	        			
	        			JSONObject jSONObject_d = null;
	        			try {
	        				jSONObject_d = jsonRequest.getJSONObject("d");
	        			} catch (Exception e) {e.printStackTrace();}
	        			
	        			String AdditionalTitle=getStringValue(jSONObject_d, "AdditionalTitle");
	        			String Category=getStringValue(jSONObject_d, "Category");
	        			String Description=getStringValue(jSONObject_d, "Description");
	        			String DateAvailable=getStringValue(jSONObject_d, "DateAvailable");
	        			
	        			String DatePosted=getStringValue(jSONObject_d, "DatePosted");
	        			Date startDate = null;
	        			if(DatePosted!=null && !DatePosted.equals(""))
	        			{
	        				//Date d = Utility.getDateFromJSONDate("Date(1378252800000)");
	        				startDate = Utility.getDateFromJSONDate(DatePosted);
	        			}

	        			String DateClosing=getStringValue(jSONObject_d, "DateClosing").trim();
	        			boolean bInsert=false;
	        			if(jobOrder==null)
	        			{
	        				bInsert=true;
	        				//System.out.println("Job Id is new  ... ");
	        				jobOrder=new JobOrder();
	        				jobOrder.setNotificationToschool(true);
	        			}
	        			else{
	        				System.out.println("Job Id is in ... "+jobOrder.getJobId());
	        			}
	        			
	        			jobOrder.setDistrictMaster(districtMaster);
						jobOrder.setJobTitle(AdditionalTitle);
						
						/*if(DateAvailable==null || DateAvailable.equalsIgnoreCase("null") || DateAvailable.equals("")  || DateAvailable.equalsIgnoreCase("Immediately"))
							jobOrder.setJobStartDate(new Date());
						else
							jobOrder.setJobStartDate(Utility.ChangeDateFormat(DateAvailable));*/
						
						if(startDate==null)
							jobOrder.setJobStartDate(new Date());
						else
							jobOrder.setJobStartDate(startDate);
						
						if(DateClosing==null || DateClosing.equalsIgnoreCase("null") || DateClosing.equals("") ||DateClosing.equalsIgnoreCase("Until Filled") || DateClosing.equalsIgnoreCase("Continuously Open") || DateClosing.equalsIgnoreCase("Open Until Filled") || DateClosing.trim().equalsIgnoreCase("Continuously"))
							jobOrder.setJobEndDate(Utility.getCurrentDateFormart("12-25-2099"));
						else
							jobOrder.setJobEndDate(Utility.ChangeDateFormat(DateClosing));
						
						JobCategoryMaster jobCategoryMaster=jobCategoryMasterDAO.findJobCategoryByNmaeAPI(auths[4], districtMaster);
						if(jobCategoryMaster==null)
						{
							jobCategoryMaster = jobCategoryMasterDAO.findById(1, false, false);
						}
						System.out.println(" Before :::");
						try{
							if(jobOrder.getDistrictMaster()==null){
								if(jobCategoryMaster.getDistrictMaster()!=null){
									jobOrder.setDistrictMaster(jobCategoryMaster.getDistrictMaster());
									System.out.println(" Inside District :::");
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						System.out.println(" After :::");
						jobOrder.setJobCategoryMaster(jobCategoryMaster);
						jobOrder.setIsJobAssessment(false);
						jobOrder.setJobAssessmentStatus(0);
						jobOrder.setJobStatus("O");
						jobOrder.setStatus("A");
						jobOrder.setJobType("");
						jobOrder.setCreatedBy(null);
						jobOrder.setJobDescription(Description);
						jobOrder.setCreatedByEntity(new Integer(2));
						jobOrder.setCreatedForEntity(new Integer(2));
						jobOrder.setNoOfExpHires(99); // Need to ask for default
						jobOrder.setApiJobId(apiJobId);
						jobOrder.setFlagForURL(1);
						jobOrder.setFlagForMessage(0);
						jobOrder.setExitURL("https://platform.teachermatch.org");
						jobOrder.setExitMessage(null);
						jobOrder.setCreatedDateTime(new Date());
						jobOrder.setIpAddress(IPAddressUtility.getIpAddress(request));
						jobOrder.setIsPortfolioNeeded(false);
						jobOrder.setIsPoolJob(0);
						jobOrder.setWritePrivilegeToSchool(false);
						jobOrder.setSelectedSchoolsInDistrict(2);
						jobOrder.setCreatedBy(1);
						jobOrder.setOfferVirtualVideoInterview(false);
						jobOrder.setVVIExpiresInDays(0);
						jobOrder.setSendAutoVVILink(false);
						jobOrder.setApprovalBeforeGoLive(1);
						jobOrder.setNoSchoolAttach(2);
						jobOrder.setAssessmentDocument(null);
						jobOrder.setAttachNewPillar(2);
						jobOrder.setAttachDefaultDistrictPillar(2);
						jobOrder.setAttachDefaultSchoolPillar(2);
						jobOrder.setIsExpHireNotEqualToReqNo(false);
						jobOrder.setNotificationToschool(true);
						jobOrder.setIsInviteOnly(true);
						jobOrder.setHiddenJob(true);
						
						try {
							if(bInsert)
								jobOrderDAO.makePersistent(jobOrder);
							else
							{
								SessionFactory factory=jobOrderDAO.getSessionFactory();
								StatelessSession statelessSession=factory.openStatelessSession();
								Transaction transaction=statelessSession.beginTransaction();
								statelessSession.update(jobOrder);
								transaction.commit();
								statelessSession.close();
							}
						} catch (Exception e) {e.printStackTrace();}
	        			
						//System.out.println("=================== jobOrder Id "+jobOrder.getJobId());
	        		}
	            }
	        }
	        catch (Exception e) {
	        	e.printStackTrace();
			}
	        finally {
	            httpget.releaseConnection();
	        }
		}
		//System.out.println("End calling getJob ...");
		return jobOrder;
		  
    }
	
	@Transactional(readOnly=false)
	public JobOrder getTeacher(@Context HttpServletRequest request, @Context HttpServletResponse response,DistrictMaster districtMaster, String AppNo,TeacherDetail teacherDetail ,String sJobAPIId,String auths[])
    {
		System.out.println(new Date()+" Start calling getTeacher ... sJobAPIId "+sJobAPIId +" AppNo "+AppNo +" teacherId "+teacherDetail.getTeacherId());
		
		JobOrder jobOrder=null;
		if(AppNo!=null && !AppNo.equals("") && districtMaster!=null)
		{
			
			String sMethodAndParameters="Applications("+AppNo+")?$format=json";
			
			ServletContext context = request.getSession().getServletContext();
			ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
			jobOrderDAO = (JobOrderDAO)context0.getBean("jobOrderDAO");
			jobCategoryMasterDAO = (JobCategoryMasterDAO)context0.getBean("jobCategoryMasterDAO");
			teacherDetailDAO = (TeacherDetailDAO)context0.getBean("teacherDetailDAO");
			teacherPersonalInfoDAO = (TeacherPersonalInfoDAO)context0.getBean("teacherPersonalInfoDAO");
			stateMasterDAO = (StateMasterDAO)context0.getBean("stateMasterDAO");
			cityMasterDAO = (CityMasterDAO)context0.getBean("cityMasterDAO");
			countryMasterDAO = (CountryMasterDAO)context0.getBean("countryMasterDAO");
			genderMasterDAO = (GenderMasterDAO)context0.getBean("genderMasterDAO");
			ethinicityMappingDAO = (EthinicityMappingDAO)context0.getBean("ethinicityMappingDAO");
			
			List<JobOrder> lstJobOrder=jobOrderDAO.getJOByDID_APIJID(districtMaster, sJobAPIId);
			
			if(lstJobOrder!=null && lstJobOrder.size()==1)
			{
				jobOrder =new JobOrder(); 
				jobOrder=lstJobOrder.get(0);
				//System.out.println("TM DB Job Id = "+jobOrder.getJobId());
			}
			
			////////////////////////////////////////
	    	HttpClient client = new HttpClient();
	        client.getState().setCredentials(new AuthScope(AppliTrackAPIConstant.sAPPLITRACKAPI_HOST, AppliTrackAPIConstant.sAPPLITRACKAPI_PORT,  auths[3]),new UsernamePasswordCredentials(auths[0], auths[1]));
	        List authPrefs = new ArrayList(1);
	        authPrefs.add(AuthPolicy.BASIC);
	        client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
	        GetMethod httpget = new GetMethod(auths[2]+""+sMethodAndParameters);
	        try {
	            int status = client.executeMethod(httpget);
	            //System.out.println("API status "+status+" "+httpget.getStatusLine());
	          /*  //System.out.println("----------- Response Teacher Data -----------------------------------");
	            //System.out.println(httpget.getResponseBodyAsString());
	            //System.out.println("------------ End Response Teacher Data -------------------------------");*/
	            if(status==200)
	            {
	            	JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( httpget.getResponseBodyAsString() ); 
	            	
	        		if(jsonRequest!=null){
	        			
	        			JSONObject jSONObject_d = null;
	        			try {
	        				jSONObject_d = jsonRequest.getJSONObject("d");
	        			} catch (Exception e) {e.printStackTrace();}
	        			
	        			String AppEmailAddress=getStringValue(jSONObject_d, "AppEmailAddress");
	        			String AppLastName=getStringValue(jSONObject_d, "AppLastName");
	        			String AppFirstName=getStringValue(jSONObject_d, "AppFirstName");
	        			String AppMiddleInitial=getStringValue(jSONObject_d, "AppMiddleInitial");
	        			
	        			String AppPermanentStreet=getStringValue(jSONObject_d, "AppPermanentStreet");
	        			String AppPermanentAptNbr=getStringValue(jSONObject_d, "AppPermanentAptNbr");
	        			String AppPermanentCity=getStringValue(jSONObject_d, "AppPermanentCity");
	        			
	        			String AppPermanentState=getStringValue(jSONObject_d, "AppPermanentState");
	        			String AppPermanentZip=getStringValue(jSONObject_d, "AppPermanentZip");
	        			String AppPermanentCountry=getStringValue(jSONObject_d, "AppPermanentCountry");
	        			
	        			String AppPermanentAreaCde=getStringValue(jSONObject_d, "AppPermanentAreaCde");
	        			String AppPermanentPhn=getStringValue(jSONObject_d, "AppPermanentPhn");
	        			
	        			String AppHomeAreaCde=getStringValue(jSONObject_d, "AppHomeAreaCde");
	        			String AppHomePhone=getStringValue(jSONObject_d, "AppHomePhone");
	        			
	        			String sPhoneNumber="",sHomePhoneNumber="";
	        			if(chkString_Not_Null_And_Blank(AppPermanentPhn))
	        			{
	        				if(chkString_Not_Null_And_Blank(AppPermanentAreaCde))
	        				{
	        					sPhoneNumber=AppPermanentAreaCde+"-"+AppPermanentPhn;
	        					sPhoneNumber=replaceDash(sPhoneNumber);
	        				}
	        			}
	        			
	        			if(chkString_Not_Null_And_Blank(AppHomePhone))
	        			{
	        				if(chkString_Not_Null_And_Blank(AppHomeAreaCde))
	        				{
	        					sHomePhoneNumber=AppHomeAreaCde+"-"+AppHomePhone;
	        					sHomePhoneNumber=replaceDash(sHomePhoneNumber);
	        				}
	        			}
	        			
	        			//************ TeacherDetails *********************
	        			//System.out.println("teacherDetail:: "+teacherDetail);
	        			
	        			teacherDetail.setFirstName(AppFirstName);
	        			teacherDetail.setLastName(AppLastName);
	        			teacherDetail.setEmailAddress(AppEmailAddress);
	        			
	        			if(chkString_Not_Null_And_Blank(sPhoneNumber))
	        				teacherDetail.setPhoneNumber(sPhoneNumber);
	        			
	        			try {
							teacherDetailDAO.makePersistent(teacherDetail);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						//************ TeacherPersonalInfo *********************
						
						StateMaster statMaster=null;
	        			if(chkString_Not_Null_And_Blank(AppPermanentState))	
	        				statMaster=stateMasterDAO.findByStateCode(AppPermanentState.trim());
	        			
	        			CityMaster cityMaster=null;
	        			CountryMaster countryMaster=null;
	        			boolean IsUSA=false;
	        			boolean IsOtherStateCity=true;
	        			//System.out.println("AppPermanentCountry:: "+AppPermanentCountry);
	        			//System.out.println("AppPermanentCountry:: "+chkString_Not_Null_And_Blank(AppPermanentCountry));
	        			if(chkString_Not_Null_And_Blank(AppPermanentCountry) && AppPermanentCountry.equalsIgnoreCase("United States of America"))
	        				IsUSA=true;
	        			
	        			if(IsUSA)
	        				countryMaster=countryMasterDAO.getCountryByShortCode("US");
	        			else
	        			{
	        				if(chkString_Not_Null_And_Blank(AppPermanentCountry))
	        					{
	        						try {
										countryMaster=countryMasterDAO.getCountryByName(AppPermanentCountry.trim());
									} catch (Exception e) {
										e.printStackTrace();
									}
	        					}
	        				if(!chkString_Not_Null_And_Blank(AppPermanentCountry) || countryMaster==null)
	        					countryMaster=countryMasterDAO.getCountryByShortCode("OTH");
	        			}
	        			
	        			if(IsUSA && chkString_Not_Null_And_Blank(AppPermanentZip))
	        			{
	        				cityMaster=cityMasterDAO.findCityByZipcode(AppPermanentZip);
	        				if(cityMaster!=null)
	        					statMaster=cityMaster.getStateId();
	        			}
	        			if(cityMaster==null)
	        				IsOtherStateCity=false;
						
						TeacherPersonalInfo teacherPersonalInfo=new TeacherPersonalInfo();
						teacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
						if(teacherPersonalInfo==null)
						{
							teacherPersonalInfo=new TeacherPersonalInfo();
							teacherPersonalInfo.setCreatedDateTime(new Date());
						}
						
						teacherPersonalInfo.setTeacherId(teacherDetail.getTeacherId());
						teacherPersonalInfo.setFirstName(AppFirstName);
						teacherPersonalInfo.setMiddleName(AppMiddleInitial);
						teacherPersonalInfo.setLastName(AppLastName);
						teacherPersonalInfo.setIsDone(true);
						
						if(chkString_Not_Null_And_Blank(sPhoneNumber))
							teacherPersonalInfo.setPhoneNumber(sPhoneNumber);
						if(chkString_Not_Null_And_Blank(sHomePhoneNumber))
							teacherPersonalInfo.setMobileNumber(sHomePhoneNumber);
						
						teacherPersonalInfo.setAddressLine1(AppPermanentStreet);
						teacherPersonalInfo.setAddressLine2(AppPermanentAptNbr);
						if(chkObject_Not_Null(cityMaster))
							teacherPersonalInfo.setCityId(cityMaster);
						
						if(IsOtherStateCity)
						{
							if(chkObject_Not_Null(statMaster))
								teacherPersonalInfo.setStateId(statMaster);
							if(chkObject_Not_Null(cityMaster))
								teacherPersonalInfo.setCityId(cityMaster);
						}
						else
						{
							teacherPersonalInfo.setOtherCity(AppPermanentCity);
							teacherPersonalInfo.setOtherState(AppPermanentState);
						}
						
						if(chkObject_Not_Null(countryMaster))
							teacherPersonalInfo.setCountryId(countryMaster);
						if(chkString_Not_Null_And_Blank(AppPermanentZip))
							teacherPersonalInfo.setZipCode(AppPermanentZip);
						
						String sSSN=getSSN(request, response, AppNo,auths);
						if(chkString_Not_Null_And_Blank(sSSN))
						{
							//System.out.println("********** true ");
							teacherPersonalInfo.setSSN(Utility.encodeInBase64(sSSN));
						}
						
						
						String sGender=getGENDER(request, response, AppNo,auths);
						//System.out.println("========== sGender :::::: "+sGender.length());
						GenderMaster genderMaster=null;
						if(chkString_Not_Null_And_Blank(sGender))
						{
							List<GenderMaster> masters=new ArrayList<GenderMaster>();
							Criterion criterionG = Restrictions.eq("genderName",sGender);
							masters=genderMasterDAO.findByCriteria(criterionG);
							if(masters!=null && masters.size() >0)
								genderMaster=masters.get(0);
							if(genderMaster==null)
							{
								genderMaster=new GenderMaster();
								genderMaster.setGenderId(0);
							}
						}
						teacherPersonalInfo.setGenderId(genderMaster);
						
						String sETHNICITY=getETHNICITY(request, response, AppNo,auths);
						//System.out.println("1 sETHNICITY "+sETHNICITY);
						if(chkString_Not_Null_And_Blank(sETHNICITY))
						{
							sETHNICITY=sETHNICITY.replace("Ethnicity:", "");
							//System.out.println("2 sETHNICITY "+sETHNICITY);
							if(chkString_Not_Null_And_Blank(sETHNICITY))
							{
								EthinicityMapping ethinicityMapping=ethinicityMappingDAO.getMappedEthinicity(sETHNICITY, districtMaster);
								if(ethinicityMapping!=null && ethinicityMapping.getEthinicityMaster()!=null)
									teacherPersonalInfo.setEthnicityId(ethinicityMapping.getEthinicityMaster());
							}
						}
						
						
						Date dateofBirth=getDateOfBirth(request, response, AppNo,auths);
						teacherPersonalInfo.setDob(dateofBirth);
						
						//getTeacherExp(request, response, AppNo,auths,sJobAPIId);
						
						try {
							teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						
						
						try {
							if(teacherDetail!=null)
								getTeacherEducation(request, response, districtMaster, AppNo, teacherDetail,auths,context);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	        			
						
						try {
							getTeacherExperience(request, response, districtMaster, AppNo, teacherDetail,auths,context,sJobAPIId);
						} catch (Exception e) {
							// TODO: handle exception
						}
	        		}
	            }
	            else
	            {
	            	System.out.println("API Error "+status +" "+httpget.getStatusLine());
	            }
	        }
	        catch (Exception e) {
	        	e.printStackTrace();
			}
	        finally {
	            httpget.releaseConnection();
	        }
		}
		//System.out.println("End calling getTeacher ...");
		return jobOrder;
		  
    }
	
	
	public static String getSSN(@Context HttpServletRequest request, @Context HttpServletResponse response,String AppNo,String auths[])
    {
		//System.out.println("Start calling getSSN ...");
		String sSSN="";
		if(AppNo!=null && !AppNo.equals(""))
		{
			
			String sMethodAndParameters="GetSSN?AppNo="+AppNo+"&$format=json";
	    	HttpClient client = new HttpClient();
	        client.getState().setCredentials(new AuthScope(AppliTrackAPIConstant.sAPPLITRACKAPI_HOST, AppliTrackAPIConstant.sAPPLITRACKAPI_PORT,  auths[3]),new UsernamePasswordCredentials(auths[0], auths[1]));
	        List authPrefs = new ArrayList(1);
	        authPrefs.add(AuthPolicy.BASIC);
	        client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
	        GetMethod httpget = new GetMethod(auths[2]+""+sMethodAndParameters);
	        try {
	            int status = client.executeMethod(httpget);
	           /* //System.out.println("----------- Response Teacher Data -----------------------------------");
	            //System.out.println(httpget.getResponseBodyAsString());
	            //System.out.println("------------ End Response Teacher Data -------------------------------");*/
	            if(status==200)
	            {
	            	JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( httpget.getResponseBodyAsString() ); 
	        		if(jsonRequest!=null){
	        			
	        			JSONObject jSONObject_d = null;
	        			try {
	        				jSONObject_d = jsonRequest.getJSONObject("d");
	        			} catch (Exception e) {e.printStackTrace();}
	        			
	        			sSSN=getStringValue(jSONObject_d, "GetSSN");
	        		}
	            }
	        }
	        catch (Exception e) {
	        	e.printStackTrace();
			}
	        finally {
	            httpget.releaseConnection();
	        }
		}
		//System.out.println("End calling getSSN ...");
		return sSSN;
    }
	
	
	public String getGENDER(@Context HttpServletRequest request, @Context HttpServletResponse response, String AppNo,String auths[])
    {
		//System.out.println("Start calling getGENDER ...");
		String sGetGENDER="";
		if(AppNo!=null && !AppNo.equals(""))
		{
			String sMethodAndParameters="GetGender?AppNo="+AppNo+"&$format=json";
			
	    	HttpClient client = new HttpClient();
	        client.getState().setCredentials(new AuthScope(AppliTrackAPIConstant.sAPPLITRACKAPI_HOST, AppliTrackAPIConstant.sAPPLITRACKAPI_PORT,  auths[3]),new UsernamePasswordCredentials(auths[0], auths[1]));
	        List authPrefs = new ArrayList(1);
	        authPrefs.add(AuthPolicy.BASIC);
	        client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
	        GetMethod httpget = new GetMethod(auths[2]+""+sMethodAndParameters);
	        try {
	            int status = client.executeMethod(httpget);
	           /* //System.out.println("----------- St Response Teacher getGENDER Data -----------------------------------");
	            //System.out.println(httpget.getResponseBodyAsString());
	            //System.out.println("------------ End Response Teacher getGENDER Data -------------------------------");*/
	            if(status==200)
	            {
	            	JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( httpget.getResponseBodyAsString() ); 
	        		if(jsonRequest!=null){
	        			
	        			JSONObject jSONObject_d = null;
	        			try {
	        				jSONObject_d = jsonRequest.getJSONObject("d");
	        			} catch (Exception e) {e.printStackTrace();}
	        			
	        			sGetGENDER=getStringValue(jSONObject_d, "GetGender");
	        		}
	            }
	        }
	        catch (Exception e) {
	        	sGetGENDER="";
	        	e.printStackTrace();
			}
	        finally {
	            httpget.releaseConnection();
	        }
		}
		//System.out.println("End calling getGENDER ...");
		return sGetGENDER;
    }
	
	
	public String getETHNICITY(@Context HttpServletRequest request, @Context HttpServletResponse response, String AppNo,String auths[])
    {
		//System.out.println("Start calling getETHNICITY ...");
		String sETHNICITY="";
		if(AppNo!=null && !AppNo.equals(""))
		{
			String sMethodAndParameters="GetEthnicity?AppNo="+AppNo+"&$format=json";
			
	    	HttpClient client = new HttpClient();
	        client.getState().setCredentials(new AuthScope(AppliTrackAPIConstant.sAPPLITRACKAPI_HOST, AppliTrackAPIConstant.sAPPLITRACKAPI_PORT,  auths[3]),new UsernamePasswordCredentials(auths[0], auths[1]));
	        List authPrefs = new ArrayList(1);
	        authPrefs.add(AuthPolicy.BASIC);
	        client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
	        GetMethod httpget = new GetMethod(auths[2]+""+sMethodAndParameters);
	        try {
	            int status = client.executeMethod(httpget);
	            //System.out.println("----------- Response Teacher Data status "+status+"-----------------------------------");
	            //System.out.println(httpget.getResponseBodyAsString());
	            //System.out.println("------------ End Response Teacher Data -------------------------------");
	            if(status==200)
	            {
	            	JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( httpget.getResponseBodyAsString() ); 
	        		if(jsonRequest!=null){
	        			
	        			JSONObject jSONObject_d = null;
	        			try {
	        				jSONObject_d = jsonRequest.getJSONObject("d");
	        			} catch (Exception e) {e.printStackTrace();}
	        			
	        			sETHNICITY=getStringValue(jSONObject_d, "GetEthnicity");
	        			
	        			
	        		}
	            }
	        }
	        catch (Exception e) {
	        	sETHNICITY="";
	        	e.printStackTrace();
			}
	        finally {
	            httpget.releaseConnection();
	        }
		}
		//System.out.println("End calling getETHNICITY ...");
		return sETHNICITY;
    }
	
	@Transactional(readOnly=false)
	public boolean getTeacherEducation(@Context HttpServletRequest request, @Context HttpServletResponse response,DistrictMaster districtMaster, String AppNo,TeacherDetail teacherDetail,String auths[],ServletContext context)
    {
		//System.out.println("Start calling getTeacherEducation ...");
		JobOrder jobOrder=null;
		if(AppNo!=null && !AppNo.equals("") && districtMaster!=null)
		{
			String sMethodAndParameters="Applications("+AppNo+")/Education?$format=json";
			
			if(context==null)
				context = request.getSession().getServletContext();
			
			ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
			teacherAcademicsDAO = (TeacherAcademicsDAO)context0.getBean("teacherAcademicsDAO");
			degreeMasterDAO = (DegreeMasterDAO)context0.getBean("degreeMasterDAO");
			universityMasterDAO = (UniversityMasterDAO)context0.getBean("universityMasterDAO");
			fieldOfStudyMasterDAO = (FieldOfStudyMasterDAO)context0.getBean("fieldOfStudyMasterDAO");
			
	    	HttpClient client = new HttpClient();
	        client.getState().setCredentials(new AuthScope(AppliTrackAPIConstant.sAPPLITRACKAPI_HOST, AppliTrackAPIConstant.sAPPLITRACKAPI_PORT,  auths[3]),new UsernamePasswordCredentials(auths[0], auths[1]));
	        List authPrefs = new ArrayList(1);
	        authPrefs.add(AuthPolicy.BASIC);
	        client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
	        GetMethod httpget = new GetMethod(auths[2]+""+sMethodAndParameters);
	        try {
	            int status = client.executeMethod(httpget);
	            /*//System.out.println("----------- St Response Teacher Education Data -----------------------------------");
	            //System.out.println(httpget.getResponseBodyAsString());
	            //System.out.println("------------ End Response Teacher Education Data -------------------------------");*/
	            if(status==200)
	            {
	            	JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( httpget.getResponseBodyAsString() ); 
	        		if(jsonRequest!=null){
	        			JSONArray jSONArray=null;
	        			try {
	        				jSONArray= (JSONArray) jsonRequest.get("d");
	        				Iterator i = jSONArray.iterator();
	        				//System.out.println("******************************************************");
	        				while (i.hasNext()) 
	        				{
	        					JSONObject innerObj = (JSONObject) i.next();
	        					String sUniversityName=getStringValue(innerObj, "EduCollegeName");
	        					sUniversityName=getUniversityName(sUniversityName);
	        					
	        					String EduDatesAttendedFrom=getStringValue(innerObj, "EduDatesAttendedFrom");
	        					long iEduDatesAttendedFrom=getYear(EduDatesAttendedFrom);
	        					
	        					String EduDatesAttendedTo=getStringValue(innerObj, "EduDatesAttendedTo");
	        					long iEduDatesAttendedTo=getYear(EduDatesAttendedTo);
	        					
	        					String EduCollegeDegree=getStringValue(innerObj, "EduCollegeDegree");
	        					EduCollegeDegree=replaceDot(EduCollegeDegree);
	        					
	        					String EduGPA=getStringValue(innerObj, "EduGPA");
	        					double dEduGPA=getDoubleValue(EduGPA);
	        					
	        					String EduCollegeMajor=getStringValue(innerObj, "EduCollegeMajor");
	        					
	        					////System.out.println("EduCollegeDegree "+EduCollegeDegree+" sUniversityName "+sUniversityName +" EduCollegeMajor "+EduCollegeMajor +" "+iEduDatesAttendedFrom+" - "+iEduDatesAttendedTo +" GPA "+dEduGPA);
	        					try 
	        					{
	        						TeacherAcademics teacherAcademics = null;
	        						
	        						DegreeMaster degreeMaster=null;
	        						if(chkString_Not_Null_And_Blank(EduCollegeDegree))
										degreeMaster=degreeMasterDAO.getDMByShortCode(EduCollegeDegree);
	        						if(degreeMaster==null)
        								degreeMaster=degreeMasterDAO.getDMByShortCode("OTH");
	        						
	        						UniversityMaster universityMaster=null;
	        						if(chkString_Not_Null_And_Blank(sUniversityName))
		        					{
	        							List<UniversityMaster> lstUniversityMaster = new ArrayList<UniversityMaster>();
	        							lstUniversityMaster=universityMasterDAO.findUniversityByName(sUniversityName);
	        							if(lstUniversityMaster!=null && lstUniversityMaster.size()>0)
	        								universityMaster=lstUniversityMaster.get(0);
	        							if(universityMaster==null)
	        							{
	        								lstUniversityMaster=universityMasterDAO.findUniversityByName("Other");
	        								if(lstUniversityMaster!=null && lstUniversityMaster.size()>0)
	        									universityMaster=lstUniversityMaster.get(0);
	        							}
		        					}
	        						FieldOfStudyMaster fieldOfStudyMaster=null;
	        						if(chkString_Not_Null_And_Blank(EduCollegeMajor))
		        					{
	        							List<FieldOfStudyMaster> lstFieldOfStudyMaster = new ArrayList<FieldOfStudyMaster>();
	        							lstFieldOfStudyMaster=fieldOfStudyMasterDAO.findFieldOfStuddyByName(EduCollegeMajor);
	        							if(lstFieldOfStudyMaster!=null && lstFieldOfStudyMaster.size()>0)
	        								fieldOfStudyMaster=lstFieldOfStudyMaster.get(0);
	        							if(fieldOfStudyMaster==null)
	        							{
	        								lstFieldOfStudyMaster=fieldOfStudyMasterDAO.findFieldOfStuddyByName("Other");
	        								if(lstFieldOfStudyMaster!=null && lstFieldOfStudyMaster.size()>0)
	        									fieldOfStudyMaster=lstFieldOfStudyMaster.get(0);
	        							}
		        					}
	        						
	        						List<TeacherAcademics> teacherAcademicList = new ArrayList<TeacherAcademics>();
	        						teacherAcademicList=teacherAcademicsDAO.ChkIdentical(teacherDetail, degreeMaster, universityMaster, fieldOfStudyMaster);
	        						if(teacherAcademicList!=null && teacherAcademicList.size()>0)
	        							teacherAcademics=teacherAcademicList.get(0);
	        						
	        						if(teacherAcademics==null)
	        						{
	        							teacherAcademics = new TeacherAcademics();
	        							teacherAcademics.setCreatedDateTime(new Date());
	        						}
	        						teacherAcademics.setTeacherId(teacherDetail);
	        						teacherAcademics.setInternational(false);
	        						teacherAcademics.setAttendedInYear(iEduDatesAttendedFrom);
	        						teacherAcademics.setLeftInYear(iEduDatesAttendedTo);
	        						if(dEduGPA >0.0)
	        							teacherAcademics.setGpaCumulative(dEduGPA);
	        						else
	        							teacherAcademics.setGpaCumulative(null);
	        						teacherAcademics.setDegreeId(degreeMaster);
	        						teacherAcademics.setUniversityId(universityMaster);
	        						teacherAcademics.setFieldId(fieldOfStudyMaster);
	        						
	        						try {
										teacherAcademicsDAO.makePersistent(teacherAcademics);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
	        					} 
	        					catch (Exception e) 
	        					{
	        						e.printStackTrace();
	        					}
	        					
	        					//
	        					
	        				}
	        				//System.out.println("******************************************************");
	        			} catch (Exception e) {e.printStackTrace();}
	        		}
	            }
	        }
	        catch (Exception e) {
	        	e.printStackTrace();
			}
	        finally {
	            httpget.releaseConnection();
	        }
		}
		
		//System.out.println("End calling getTeacherEducation ...");
		return true;
		  
    }
	
	
	@Transactional(readOnly=false)
	public boolean getTeacherExperience(@Context HttpServletRequest request, @Context HttpServletResponse response,DistrictMaster districtMaster, String AppNo,TeacherDetail teacherDetail,String auths[],ServletContext context,String sJobAPIId)
    {
		//System.out.println("Start calling getTeacherExperience ...");
		if(AppNo!=null && !AppNo.equals("") && districtMaster!=null)
		{
			
			String sMethodAndParameters="Applications("+AppNo+")/Experience?$format=json";
			
			if(context==null)
				context = request.getSession().getServletContext();
			
			ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
			currencyMasterDAO = (CurrencyMasterDAO)context0.getBean("currencyMasterDAO");
			teacherRoleDAO = (TeacherRoleDAO)context0.getBean("teacherRoleDAO");
			fieldMasterDAO = (FieldMasterDAO)context0.getBean("fieldMasterDAO");
			
			List<FieldMaster> fieldMasters=new ArrayList<FieldMaster>();
			FieldMaster fieldMaster=new FieldMaster();
			Criterion criterion1 = Restrictions.eq("fieldName","Other");
			fieldMasters=fieldMasterDAO.findByCriteria(criterion1);
			if(fieldMasters!=null && fieldMasters.size() >0)
				fieldMaster=fieldMasters.get(0);
			
			EmpRoleTypeMaster empRoleTypeMaster=new EmpRoleTypeMaster();
			empRoleTypeMaster.setEmpRoleTypeId(1);
			
			CurrencyMaster currencyMaster = currencyMasterDAO.findById(1, false, false);
			
			////////////////////////////////////////
	    	HttpClient client = new HttpClient();
	        client.getState().setCredentials(new AuthScope(AppliTrackAPIConstant.sAPPLITRACKAPI_HOST, AppliTrackAPIConstant.sAPPLITRACKAPI_PORT,  auths[3]),new UsernamePasswordCredentials(auths[0], auths[1]));
	        List authPrefs = new ArrayList(1);
	        authPrefs.add(AuthPolicy.BASIC);
	        client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
	        GetMethod httpget = new GetMethod(auths[2]+""+sMethodAndParameters);
	        try {
	            int status = client.executeMethod(httpget);
	            /*//System.out.println("----------- St Response Teacher Experience Data -----------------------------------");
	            //System.out.println(httpget.getResponseBodyAsString());
	            //System.out.println("------------ End Response Teacher Experience Data -------------------------------");*/
	            if(status==200)
	            {
	            	JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( httpget.getResponseBodyAsString() ); 
	        		if(jsonRequest!=null){
	        			JSONArray jSONArray=null;
	        			try {
	        				jSONArray= (JSONArray) jsonRequest.get("d");
	        				Iterator i = jSONArray.iterator();
	        				while (i.hasNext()) 
	        				{
	        					JSONObject innerObj = (JSONObject) i.next();
	        					
	        					String Title=getStringValue(innerObj, "Title");
	        					String DateFrom=getStringValue(innerObj, "DateFrom");
	        					String DateTo=getStringValue(innerObj, "DateTo");
	        					String LastAnnualSalary=getStringValue(innerObj, "LastAnnualSalary");
	        					String Description=getStringValue(innerObj, "Description");
	        					
	        					//String Name=getStringValue(innerObj, "Name");
	        					
	        					TeacherRole teacherRole=null;
	        					if(chkString_Not_Null_And_Blank(Title) && chkString_Not_Null_And_Blank(DateFrom))
	        					{
	        						Criterion criterion2 = Restrictions.eq("role",Title);
	        						int iFromDate[]=getIntArryFromString(DateFrom);
	        						Criterion criterion3 = Restrictions.eq("roleStartMonth",iFromDate[0]);
	        						Criterion criterion4 = Restrictions.eq("roleStartYear",iFromDate[1]+"");
	        						Criterion criterion5 = Restrictions.eq("teacherDetail",teacherDetail);
	        						if(iFromDate!=null && iFromDate.length >0)
	        						{
	        							List<TeacherRole> teacherRoles=new ArrayList<TeacherRole>();
	        							teacherRoles=teacherRoleDAO.findByCriteria(criterion2,criterion3,criterion4,criterion5);
	        							if(teacherRoles!=null && teacherRoles.size()>0)
	        								teacherRole=teacherRoles.get(0);
	        						}
	        						
	        					}
	        					
	        					
	        					if(teacherRole==null)
	        					{
	        						teacherRole=new TeacherRole();
	        						teacherRole.setCreatedDateTime(new Date());
	        						teacherRole.setPosition(0);
	        					}
	        					
	        					teacherRole.setTeacherDetail(teacherDetail);
	        					teacherRole.setCurrentlyWorking(false);
	        					teacherRole.setFieldMaster(fieldMaster);
	        					teacherRole.setEmpRoleTypeMaster(empRoleTypeMaster);
	        					
	        					if(chkString_Not_Null_And_Blank(Title))
	        						teacherRole.setRole(Title);
	        					
	        					if(chkString_Not_Null_And_Blank(DateFrom))
	        					{
	        						int iFromDate[]=getIntArryFromString(DateFrom);
	        						//System.out.println("iFromDate "+ iFromDate[0] +" :: "+iFromDate[1]);
	        						teacherRole.setRoleStartMonth(iFromDate[0]);
	        						teacherRole.setRoleStartYear(iFromDate[1]+"");
	        					}
	        					
	        					if(chkString_Not_Null_And_Blank(DateTo))
	        					{
	        						int iDateTo[]=getIntArryFromString(DateTo);
	        						//System.out.println("iDateTo "+iDateTo[0] +" :: "+iDateTo[1]);
	        						teacherRole.setRoleEndMonth(iDateTo[0]);
	        						teacherRole.setRoleEndYear(iDateTo[1]+"");
	        					}
	        					
	        					if(chkString_Not_Null_And_Blank(LastAnnualSalary))
	        					{
	        						int iLastAnnualSalary=getSalary(LastAnnualSalary);
	        						if(iLastAnnualSalary >0)
	        						{
	        							teacherRole.setAmount(iLastAnnualSalary);
	        							teacherRole.setCurrencyMaster(currencyMaster);
	        						}
	        						else
	        						{
	        							teacherRole.setAmount(null);
	        							teacherRole.setCurrencyMaster(null);
	        						}
	        					}
	        					if(chkString_Not_Null_And_Blank(Description))
	        						teacherRole.setPrimaryResp(Description);
	        					
	        					try {
									teacherRoleDAO.makePersistent(teacherRole);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
	        				}
	        			} catch (Exception e) {e.printStackTrace();}
	        		}
	            }
	        }
	        catch (Exception e) {
	        	e.printStackTrace();
			}
	        finally {
	            httpget.releaseConnection();
	        }
		}
		//System.out.println("End calling getTeacherExperience ...");
		return true;
		  
    }
	
	
	public Date getDateOfBirth(@Context HttpServletRequest request, @Context HttpServletResponse response, String AppNo,String auths[])
    {
		//System.out.println("Start calling getDateOfBirth ...");
		Date dDateOfBirth=null;
		if(AppNo!=null && !AppNo.equals(""))
		{
			String sMethodAndParameters="FieldResponses()?$filter=TraitID%20eq%20511%20and%20AppNo%20eq%20"+AppNo+"&$format=json";
			
	    	HttpClient client = new HttpClient();
	        client.getState().setCredentials(new AuthScope(AppliTrackAPIConstant.sAPPLITRACKAPI_HOST, AppliTrackAPIConstant.sAPPLITRACKAPI_PORT,  auths[3]),new UsernamePasswordCredentials(auths[0], auths[1]));
	        List authPrefs = new ArrayList(1);
	        authPrefs.add(AuthPolicy.BASIC);
	        client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
	        GetMethod httpget = new GetMethod(auths[2]+""+sMethodAndParameters);
	        try {
	            int status = client.executeMethod(httpget);
	            //System.out.println("----------- Response getDateOfBirth Data status "+status+"-----------------------------------");
	            //System.out.println(httpget.getResponseBodyAsString());
	            //System.out.println("------------ End Response getDateOfBirth Data -------------------------------");
	            if(status==200)
	            {
	            	JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( httpget.getResponseBodyAsString() ); 
	        		if(jsonRequest!=null){
	        			JSONArray jSONArray= (JSONArray) jsonRequest.get("d");
        				Iterator i = jSONArray.iterator();
        				while (i.hasNext()) 
        				{
        					JSONObject innerObj = (JSONObject) i.next();
        					String Value=getStringValue(innerObj, "Value");
        					dDateOfBirth=Utility.ChangeDateFormat(Value);
        				}
	        		}
	            }
	        }
	        catch (Exception e) {
	        	dDateOfBirth=null;
	        	e.printStackTrace();
			}
	        finally {
	            httpget.releaseConnection();
	        }
		}
		//System.out.println("End calling getDateOfBirth ...");
		return dDateOfBirth;
    }
	public Date getTeacherExp(@Context HttpServletRequest request, @Context HttpServletResponse response, String AppNo,String auths[],String sJobAPIId)
    {
		//System.out.println("Start calling getDateOfBirth ...");
		//System.out.println("Start calling getTeacher ... "+sJobAPIId);
		Date dDateOfBirth=null;
		if(AppNo!=null && !AppNo.equals(""))
		{
			//https://www.applitrack.com/CLIENTCODE/api/applitrackapi.svc/VacanciesSelected()?$filter=AppNo%20eq%201347 HTTP/1.1
			//String sMethodAndParameters="VacanciesSelected()?$filter=JobID%20eq%"+sJobAPIId+"%20andAppNo%20eq%20"+AppNo+"&$format=json";
			String sMethodAndParameters="VacanciesSelected()?$filter=AppNo%20eq%20"+AppNo+"&$format=json";
			//String sMethodAndParameters="Applications()?$filter=(AppLastName%20eq%20'konjevich')%20and%20(AppFirstName%20eq%20'tamara')&$top=1&$format=json";
			//String sMethodAndParameters="Experiences(AppNo=1347,LineNbr=2)";
			//System.out.println("sMethodAndParameters:: "+sMethodAndParameters);
			
	    	HttpClient client = new HttpClient();
	        client.getState().setCredentials(new AuthScope(AppliTrackAPIConstant.sAPPLITRACKAPI_HOST, AppliTrackAPIConstant.sAPPLITRACKAPI_PORT,  auths[3]),new UsernamePasswordCredentials(auths[0], auths[1]));
	        List authPrefs = new ArrayList(1);
	        authPrefs.add(AuthPolicy.BASIC);
	        client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
	        GetMethod httpget = new GetMethod(auths[2]+""+sMethodAndParameters);
	        try {
	            int status = client.executeMethod(httpget);
	            //System.out.println("----------- Response getTeacherExp Data status "+status+"-----------------------------------");
	            //System.out.println(httpget.getResponseBodyAsString());
	            //System.out.println("------------ End Response getTeacherExp Data -------------------------------");
	            if(status==200)
	            {
	            	JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( httpget.getResponseBodyAsString() ); 
	        		if(jsonRequest!=null){
	        			JSONArray jSONArray= (JSONArray) jsonRequest.get("d");
        				Iterator i = jSONArray.iterator();
        				while (i.hasNext()) 
        				{
        					JSONObject innerObj = (JSONObject) i.next();
        					//String Value=getStringValue(innerObj, "Value");
        					//dDateOfBirth=Utility.ChangeDateFormat(Value);
        				}
	        		}
	            }
	        }
	        catch (Exception e) {
	        	dDateOfBirth=null;
	        	e.printStackTrace();
			}
	        finally {
	            httpget.releaseConnection();
	        }
		}
		//System.out.println("End calling getDateOfBirth ...");
		return dDateOfBirth;
    }
	public static void main(String[] args) {
		
		try {
			
			String AppNo="470";
			
			//Education,Experience,Employees,FormsSent,FileBaseDocuments
			
			//String sMethodAndParameters="GetETHNICITY?AppNo="+AppNo+"&$format=json";
			String sMethodAndParameters="Applications("+AppNo+")/FileBaseDocuments?$format=json";
			
			//String sMethodAndParameters="Applications("+AppNo+")?$expand=Education,Experience,Employees,FileBaseDocuments,FormsSent,FileBaseDocuments&$format=json";
			
	    	HttpClient client = new HttpClient();
	        client.getState().setCredentials(new AuthScope(AppliTrackAPIConstant.sAPPLITRACKAPI_HOST, AppliTrackAPIConstant.sAPPLITRACKAPI_PORT, AppliTrackAPIConstant.sAPPLITRACKAPI_REALM),new UsernamePasswordCredentials(AppliTrackAPIConstant.sAPPLITRACKAPI_USERNAME, AppliTrackAPIConstant.sAPPLITRACKAPI_PASSWORD));
	        List authPrefs = new ArrayList(1);
	        authPrefs.add(AuthPolicy.BASIC);
	        client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
	        GetMethod httpget = new GetMethod(AppliTrackAPIConstant.sAPPLITRACKAPI_URL+""+sMethodAndParameters);
	        try {
	            int status = client.executeMethod(httpget);
	            //System.out.println("----------- Response Test Data status "+status+"-----------------------------------");
	            //System.out.println(httpget.getResponseBodyAsString());
	            //System.out.println("------------ End Response Data Data -------------------------------");
	            
	        }
	        catch (Exception e) {
	        	e.printStackTrace();
			}
	        finally {
	            httpget.releaseConnection();
	        }
		
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}


