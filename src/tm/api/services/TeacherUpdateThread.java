package tm.api.services;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tm.api.services.applitrack.AppliTrackAPIService;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;


public class TeacherUpdateThread extends Thread{

	
	public TeacherUpdateThread() {
		super();
	}
	private HttpServletRequest request;
	private HttpServletResponse response;
	private TeacherDetail teacherDetail;
	private DistrictMaster districtMaster;
	private String auths[];	
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	public HttpServletResponse getResponse() {
		return response;
	}
	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
	public TeacherDetail getTeacherDetail() {
		return teacherDetail;
	}
	public void setTeacherDetail(TeacherDetail teacherDetail) {
		this.teacherDetail = teacherDetail;
	}
	public DistrictMaster getDistrictMaster() {
		return districtMaster;
	}
	public void setDistrictMaster(DistrictMaster districtMaster) {
		this.districtMaster = districtMaster;
	}
	public void setAuths(String[] auths) {
		this.auths = auths;
	}
	public String[] getAuths() {
		return auths;
	}
	
	public void run()
	{
		try{
			AppliTrackAPIService appliService = new AppliTrackAPIService();
			String appNo = request.getParameter("appNo")==null?"":request.getParameter("appNo").trim();
			System.out.println("appNo:::: "+appNo);
			String apiJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
			appliService.getTeacher(request, response, districtMaster, appNo, teacherDetail, apiJobId,auths);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
