package tm.api.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tm.bean.TeacherDetail;
import tm.bean.teacher.SpInboundAPICallRecord;
import tm.bean.teacher.SpInboundAPICallRecordLog;
import tm.dao.TeacherDetailDAO;
import tm.dao.teacher.SpInboundAPICallRecordDAO;
import tm.dao.teacher.SpInboundAPICallRecordLogDAO;
import tm.utility.AESEncryption;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

@Controller
public class SPInboundAPICallRecordController 
{

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private SpInboundAPICallRecordDAO spInboundAPICallRecordDAO;
	
	@Autowired
	private SpInboundAPICallRecordLogDAO spInboundAPICallRecordLogDAO;
	
	@RequestMapping(value="/service/inventoryCallBack.do", method=RequestMethod.GET)
	public String inventoryCallBackGET(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		System.out.println("::::::::::::::::::::::::::::::: /service/inventoryCallBackGET.do :::::::::::::::::::::::::::::::");
		String id = request.getParameter("id")==null?"":request.getParameter("id").trim().replace(' ', '+');
		String sCurrentLessonNo = request.getParameter("currentLessonNo")==null?"":request.getParameter("currentLessonNo").trim();
		String sTotalLessons = request.getParameter("totalLessons")==null?"":request.getParameter("totalLessons").trim();
		String sLpm = request.getParameter("lpm")==null?"":request.getParameter("lpm").trim();
		//unit 03
		String sUnit03CurrentLessonNo = request.getParameter("unit03currentLessonNo")==null?"":request.getParameter("unit03currentLessonNo").trim();
		String sUnit03TotalLessons = request.getParameter("unit03TotalLessons")==null?"":request.getParameter("unit03TotalLessons").trim();
		String sUnit03Lpm = request.getParameter("unit03lpm")==null?"":request.getParameter("unit03lpm").trim();
		//unit 04
		String sUnit04CurrentLessonNo = request.getParameter("unit04currentLessonNo")==null?"":request.getParameter("unit04currentLessonNo").trim();
		String sUnit04TotalLessons = request.getParameter("unit04TotalLessons")==null?"":request.getParameter("unit04TotalLessons").trim();
		String sUnit04Lpm = request.getParameter("unit04lpm")==null?"":request.getParameter("unit04lpm").trim();
		
		System.out.println("id : "+id);
		System.out.println("sCurrentLessonNo : "+sCurrentLessonNo+" : sTotalLessons : "+sTotalLessons +" : sLpm : "+sLpm);
		System.out.println("sUnit03CurrentLessonNo : "+sUnit03CurrentLessonNo+" : sUnit03TotalLessons : "+sUnit03TotalLessons +" : sUnit03Lpm : "+sUnit03Lpm);
		System.out.println("sUnit04CurrentLessonNo : "+sUnit04CurrentLessonNo+" : sUnit04TotalLessons : "+sUnit04TotalLessons +" : sUnit04Lpm : "+sUnit04Lpm);
		Integer errorCode = 0;
		String errorMsg = "";
		String decryptText = null;
		try {
			if(id!=null && id!="")
			{
				decryptText = AESEncryption.decrypt(id);
			}
			else
			{
				errorCode = 10001;
				errorMsg = "Invalid Credentials.";
			}
			System.out.println("decryptText : "+decryptText);
		} catch (Exception e) {
			e.printStackTrace();
			if(e.getCause() instanceof javax.crypto.IllegalBlockSizeException){
				errorCode = 10001;
				errorMsg = "Invalid Credentials.";
			}
		}

		JSONObject jsonObject  = new JSONObject();
		if(decryptText!=null)
		{
			try {
				jsonObject = (JSONObject) JSONSerializer.toJSON(decryptText);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else
		{
			errorCode = 10001;
			errorMsg = "Invalid Credentials.";
		}
		
		String emailAddress=null;
		int teacherId=0,currentLessonNo=0,totalLessons=0,unit03currentLessonNo=0,unit03totalLessons=0,unit04currentLessonNo=0,unit04totalLessons=0;
		
		teacherId=getIntValue(jsonObject, "applicantId");
		emailAddress=getStringValue(jsonObject, "emailAddress");
		//currentLessonNo=getIntValue(jsonObject, "currentLessonNo");
		currentLessonNo=Utility.getIntValue(sCurrentLessonNo);
		//totalLessons=getIntValue(jsonObject, "totalLessons");
		totalLessons=Utility.getIntValue(sTotalLessons);
		//unit 03
		unit03currentLessonNo=Utility.getIntValue(sUnit03CurrentLessonNo);
		unit03totalLessons=Utility.getIntValue(sUnit03TotalLessons);
		//unit 04
		unit04currentLessonNo=Utility.getIntValue(sUnit04CurrentLessonNo);
		unit04totalLessons=Utility.getIntValue(sUnit04TotalLessons);
		
		String emailAddressDecode="";
		try {
			emailAddressDecode=Utility.decodeBase64(emailAddress);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
		
		if(teacherDetail!=null && emailAddressDecode!=null && !emailAddressDecode.equals(""))
		{
			if(teacherDetail.getEmailAddress().equalsIgnoreCase(emailAddressDecode))
			{
				List<SpInboundAPICallRecord> lstSPAPI = new ArrayList<SpInboundAPICallRecord>();
				lstSPAPI=spInboundAPICallRecordDAO.getDetailsByTID(teacherDetail);
				if(lstSPAPI!=null && lstSPAPI.size()>0)
				{
					SpInboundAPICallRecord apiCallRecord=lstSPAPI.get(0);
					if(apiCallRecord!=null && (apiCallRecord.getCurrentLessonNo()!=null || apiCallRecord.getUnit03currentLessonNo()!=null || apiCallRecord.getUnit04currentLessonNo()!=null) && (((currentLessonNo==0) || (currentLessonNo!=0 && currentLessonNo > apiCallRecord.getCurrentLessonNo())) || ((unit03currentLessonNo==0) || (unit03currentLessonNo!=0 && unit03currentLessonNo > apiCallRecord.getUnit03currentLessonNo())) || ((unit04currentLessonNo==0) || (unit04currentLessonNo!=0 && unit04currentLessonNo > apiCallRecord.getUnit04currentLessonNo()))))
					{
						if(currentLessonNo>0 || currentLessonNo > apiCallRecord.getCurrentLessonNo()){
							apiCallRecord.setCurrentLessonNo(currentLessonNo);
							apiCallRecord.setTotalLessons(totalLessons);
							apiCallRecord.setLpm(sLpm);
						}
						else if(sCurrentLessonNo!=null && !sCurrentLessonNo.equals("") && currentLessonNo==0){
							apiCallRecord.setCurrentLessonNo(currentLessonNo);
							apiCallRecord.setTotalLessons(totalLessons);
							apiCallRecord.setLpm(sLpm);
						}
						//unit 03
						if(unit03currentLessonNo>0 || (apiCallRecord.getUnit03currentLessonNo()!=null && !apiCallRecord.getUnit03currentLessonNo().equals("") && unit03currentLessonNo > apiCallRecord.getUnit03currentLessonNo())){
							apiCallRecord.setUnit03currentLessonNo(unit03currentLessonNo);
							apiCallRecord.setUnit03TotalLessons(unit03totalLessons);
							apiCallRecord.setUnit03lpm(sUnit03Lpm);
						}
						else if(sUnit03CurrentLessonNo!=null && !sUnit03CurrentLessonNo.equals("") && unit03currentLessonNo==0){
							apiCallRecord.setUnit03currentLessonNo(unit03currentLessonNo);
							apiCallRecord.setUnit03TotalLessons(unit03totalLessons);
							apiCallRecord.setUnit03lpm(sUnit03Lpm);
						}
						//unit 04
						if(unit04currentLessonNo>0 || (apiCallRecord.getUnit04currentLessonNo()!=null && !apiCallRecord.getUnit04currentLessonNo().equals("") && unit04currentLessonNo > apiCallRecord.getUnit04currentLessonNo())){
							apiCallRecord.setUnit04currentLessonNo(unit04currentLessonNo);
							apiCallRecord.setUnit04TotalLessons(unit04totalLessons);
							apiCallRecord.setUnit04lpm(sUnit04Lpm);
						}
						else if(sUnit04CurrentLessonNo!=null && !sUnit04CurrentLessonNo.equals("") && unit04currentLessonNo==0){
							apiCallRecord.setUnit04currentLessonNo(unit04currentLessonNo);
							apiCallRecord.setUnit04TotalLessons(unit04totalLessons);
							apiCallRecord.setUnit04lpm(sUnit04Lpm);
						}
						apiCallRecord.setLastUpdateDate(new Date());
						spInboundAPICallRecordDAO.makePersistent(apiCallRecord);
					}
					else
					{
						errorCode = 10005;
						errorMsg = "Denied";
						System.out.println("Current lesson of call back response is less than the Current lesson in DB.");
					}
					try {
						SpInboundAPICallRecordLog spInboundAPICallRecordLog = new SpInboundAPICallRecordLog();
						spInboundAPICallRecordLog.setTeacherDetail(teacherDetail);
						spInboundAPICallRecordLog.setCurrentLessonNo(currentLessonNo);
						spInboundAPICallRecordLog.setTotalLessons(totalLessons);
						spInboundAPICallRecordLog.setLpm(sLpm);
						//unit 03
						apiCallRecord.setUnit03currentLessonNo(unit03currentLessonNo);
						apiCallRecord.setUnit03TotalLessons(unit03totalLessons);
						apiCallRecord.setUnit03lpm(sUnit03Lpm);
						//unit 04
						apiCallRecord.setUnit04currentLessonNo(unit04currentLessonNo);
						apiCallRecord.setUnit04TotalLessons(unit04totalLessons);
						apiCallRecord.setUnit04lpm(sUnit04Lpm);
						spInboundAPICallRecordLog.setIPAddress(IPAddressUtility.getIpAddress(request));
						spInboundAPICallRecordLogDAO.makePersistent(spInboundAPICallRecordLog);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				else
				{
					errorCode = 10004;
					errorMsg = "Record is not found.";
				}
			}
			else
			{
				errorCode = 10003;
				errorMsg = "Wrong emailAddress.";
			}
		}
		else
		{
			errorCode = 10002;
			errorMsg = "Wrong Applicant ID or emailAddress.";
		}
		
		try 
		{
			PrintWriter pw = response.getWriter();
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			if(errorCode==0)
			{
				pw.write("Status : OK");
			}
			else if(errorCode==10005){
				pw.write("Status : "+errorMsg);
			}
			else
			{
				pw.write("errorCode "+errorCode);
				pw.write("errorMsg "+errorMsg);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getStringValue(JSONObject jsonObject,String sInputKey)
	{
		String sReturnValue="";
		try {
			if(jsonObject.get(sInputKey)!=null)
				sReturnValue = jsonObject.get(sInputKey).toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(" sInputKey "+sInputKey +" sReturnValue "+sReturnValue);
		return sReturnValue;
	}
	
	public static int getIntValue(JSONObject jsonObject,String sInputKey)
	{
		String sReturnValue="";
		try {
			if(jsonObject.get(sInputKey)!=null)
				sReturnValue = jsonObject.get(sInputKey).toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		int iReturnValue=Utility.getIntValue(sReturnValue);
		System.out.println(" sInputKey "+sInputKey +" iReturnValue "+iReturnValue);
		return iReturnValue;
	}
	
}
