package tm.api.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.api.services.ServiceUtility;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.master.StatusMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

@Controller
public class ApplicationPoolController {
	
    String locale = Utility.getValueOfPropByKey("locale");

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	

	@Autowired
	private ServiceUtility serviceUtility;
	public void setServiceUtility(ServiceUtility serviceUtility) {
		this.serviceUtility = serviceUtility;
	}
	
	
	@RequestMapping(value="/service/getApplicantView.do", method=RequestMethod.GET)
	public String doApplicatViewGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("applicationpool.do");
		try
		{	
			boolean aPoolStatus = false;
			Integer errorCode=0;
			String errorMsg="";
			HttpSession session = request.getSession();
			try {
				
				
				
				
				String apiJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
				JobOrder jobOrder = null;
				
				
				boolean isValidUser = false;
				Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
				isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
				DistrictMaster districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
				System.out.println("isValidUser>>"+isValidUser);
				if(isValidUser){
					
					UserMaster userMaster = null;					
					try {
						
						jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);
					}catch (Exception e){
						jobOrder=null;
					}
					Map<String,String> mapUserInfo = new HashMap<String, String>();
					mapUserInfo = serviceUtility.authUser(request);
					if(! new Boolean(mapUserInfo.get("isSuccess"))){
						session.setAttribute("userMasterAPI", null);
						aPoolStatus = false;
						errorCode= new Integer(mapUserInfo.get("errorCode"));
						errorMsg=mapUserInfo.get("errorMsg");
					}
					else if(apiJobId.equals("")){
						aPoolStatus = false;
						errorCode = 10004;
						errorMsg = Utility.getLocaleValuePropByKey("msgJobReq", locale);
					}
					else if(jobOrder==null){
						System.out.println("E3");
						aPoolStatus = false;
						errorCode = 10004;
						errorMsg = Utility.getLocaleValuePropByKey("msgJobNotExist", locale);
					}
					else{				
						userMaster = (UserMaster)session.getAttribute("userMasterAPI");					
						aPoolStatus = true;
					}
					
					System.out.println("usermasterEPI"+userMaster);
				}
				else{
					errorCode = new Integer(mapDistSchDetail.get("errorCode"));
					errorMsg = mapDistSchDetail.get("errorMsg");
					aPoolStatus = false;
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				aPoolStatus = false;
			}
			finally{			
			}
			
			map.addAttribute("aPoolStatus", aPoolStatus);
			map.addAttribute("errorCode", errorCode);
			map.addAttribute("errorMsg", errorMsg);
			 
			if(aPoolStatus){
				
				String apiJobId = request.getParameter("jobId")==null?"0":request.getParameter("jobId").trim();
				DistrictMaster districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
				JobOrder jobOrder =	jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);
				
				List<JobForTeacher> applicantJobForTeacher = jobForTeacherDAO.findJFTApplicantbyJobOrder(jobOrder);
			
				SortedMap map1 = new TreeMap();
				List<JobForTeacher> sortedApplicantJobForTeacher = new ArrayList<JobForTeacher>();
					
				for(int i=0; i<applicantJobForTeacher.size();i++ ){
					map1.put(applicantJobForTeacher.get(i).getTeacherId().getFirstName()+"||"+applicantJobForTeacher.get(i).getTeacherId().getLastName(),applicantJobForTeacher.get(i));
				}
				
			    Iterator iterator = map1.keySet().iterator();		    
				while (iterator.hasNext()){
					Object key = iterator.next();
					sortedApplicantJobForTeacher.add((JobForTeacher) map1.get(key));
				}
				StatusMaster statushird = WorkThreadServlet.statusMap.get("hird");
				List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
				lstStatusMasters.add(statushird);
				List<JobForTeacher> lstHiredApplicantFromJobForTeacher = jobForTeacherDAO.findJFTbyJobOrder(jobOrder,lstStatusMasters);
				if(jobOrder.getCreatedForEntity()==3){
					List<SchoolInJobOrder> lstSchoolInJobOrder = schoolInJobOrderDAO.findJobOrder(jobOrder);
					map.addAttribute("lstSchoolInJobOrder", lstSchoolInJobOrder.get(0));
				}
				
				map.addAttribute("jobOrder", jobOrder);			
				map.addAttribute("totalNoOfApplicants", applicantJobForTeacher.size());
				map.addAttribute("applicantJobForTeacher", sortedApplicantJobForTeacher);
				map.addAttribute("totalHiredApplicants", lstHiredApplicantFromJobForTeacher.size());
				map.addAttribute("lstHiredApplicant", lstHiredApplicantFromJobForTeacher);
			}			
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		System.out.println("Before call to JSP..");
		return "apool";
	}
}
