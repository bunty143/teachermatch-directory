package tm.api.controller;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Vector;

import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.csvreader.CsvWriter;

import tm.api.UtilityAPI;
import tm.bean.InternalTransferCandidates;
import tm.bean.JobCategoryTransaction;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.StatusWiseAutoEmailSend;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherExperience;
import tm.bean.TeacherLoginHistory;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherProfileVisitHistory;
import tm.bean.UserLoginHistory;
import tm.bean.assessment.AssessmentCompetencyScore;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentDomainScore;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.assessment.ScoreLookup;
import tm.bean.cgreport.JobCategoryWiseStatusPrivilege;
import tm.bean.cgreport.PercentileScoreByJob;
import tm.bean.cgreport.PercentileZscoreTscore;
import tm.bean.cgreport.RawDataForCompetency;
import tm.bean.cgreport.RawDataForDomain;
import tm.bean.cgreport.RawDataForObjective;
import tm.bean.cgreport.TmpPercentileWiseZScore;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.CurrencyMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.ObjectiveMaster;
import tm.bean.master.OptionsForDistrictSpecificQuestions;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.master.TeacherAnswerDetailsForDistrictSpecificQuestions;
import tm.bean.user.UserMaster;
import tm.dao.CandidateRawScoreDAO;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobCategoryTransactionDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.StatusWiseAutoEmailSendDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAnswerDetailDAO;
import tm.dao.TeacherAssessmentDetailDAO;
import tm.dao.TeacherAssessmentQuestionDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherLoginHistoryDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherProfileVisitHistoryDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.assessment.AssessmentCompetencyScoreDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentDomainScoreDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.assessment.ScoreLookupDAO;
import tm.dao.cgreport.JobCategoryWiseStatusPrivilegeDAO;
import tm.dao.cgreport.PercentileScoreByJobDAO;
import tm.dao.cgreport.RawDataForCompetencyDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.dao.cgreport.TmpPercentileWiseZScoreDAO;
import tm.dao.master.CompetencyMasterDAO;
import tm.dao.master.CurrencyMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.TeacherAnswerDetailsForDistrictSpecificQuestionsDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.CadidateRawScoreThread;
import tm.services.clamav.ClamAVUtil;
import tm.services.report.CGReportService;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;


@Controller
public class TestPageController {
	
	@Autowired
	private TeacherLoginHistoryDAO teacherLoginHistoryDAO;
	@Autowired
	private TeacherProfileVisitHistoryDAO teacherProfileVisitHistoryDAO;
	
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	/*public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}*/
	@Autowired
	private PercentileScoreByJobDAO percentileScoreByJobDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	
	@Autowired
	private RawDataForDomainDAO rawDataForDomainDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	@Autowired
	private ClamAVUtil clamAVUtil;
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private ScoreLookupDAO scoreLookupDAO;
	
	@Autowired
	private RawDataForCompetencyDAO rawDataForCompetencyDAO;
	
	@Autowired
	private AssessmentDomainScoreDAO assessmentDomainScoreDAO; 
	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;
	
	@Autowired
	private TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO;
	
	@Autowired
	private CGReportService reportService;
	
	List<TmpPercentileWiseZScore> tmpPercentileWiseZScoreList = null;
	List<PercentileZscoreTscore> pztList = null;
	
	@Autowired
	private TeacherAssessmentDetailDAO teacherAssessmentDetailDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private AssessmentCompetencyScoreDAO assessmentCompetencyScoreDAO;
	
	@Autowired
	private StatusWiseAutoEmailSendDAO statusWiseAutoEmailSendDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO;
	
	@Autowired
	private TeacherAnswerDetailDAO teacherAnswerDetailDAO;
	public void setTeacherAnswerDetailDAO(TeacherAnswerDetailDAO teacherAnswerDetailDAO) {
		this.teacherAnswerDetailDAO = teacherAnswerDetailDAO;
	}
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private CompetencyMasterDAO competencyMasterDAO;
	
	@Autowired
	private TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO;
	
	@Autowired
	private CurrencyMasterDAO currencyMasterDAO;
	
	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;
	
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	
	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	
	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	
	@Autowired
	private JobCategoryTransactionDAO jobCategoryTransactionDAO;
	
	@Autowired
	private TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO;
	
	@Autowired 
	private CandidateRawScoreDAO candidateRawScoreDAO;
	
	@RequestMapping(value="/service/jobordertestpage.do", method=RequestMethod.GET)
	public String doJobOrderTest(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("/service/jobordertestpage.do");
		try {
			List<JobOrder> lstJobOrder = jobOrderDAO.findWithLimit(Order.asc("createdDateTime"), 0, 10);			
			map.addAttribute("lstJobOrder", lstJobOrder);
		} catch (Exception e){
			e.printStackTrace();
		}
		finally{			
		}
		return "jobordertestpage";
	}
	
	@RequestMapping(value="/service/addjobordertest.do", method=RequestMethod.GET)
	public String doAddJobOrderTest(ModelMap map,HttpServletRequest request)
	{
		System.out.println(">>>>");
		try {
			List<JobOrder> lstJobOrder = jobOrderDAO.findWithLimit(Order.desc("createdDateTime"), 0, 100);			
			map.addAttribute("lstJobOrder", lstJobOrder);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "addjobordertest";
	}
	
	@RequestMapping(value="/service/encBase64.do", method=RequestMethod.GET)
	public String doEncBase64(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/encBase64.do");
		String strInput="";
		try {
			
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			strInput = request.getParameter("strInput")==null?"":request.getParameter("strInput");
			out.println(Utility.encodeInBase64(strInput));
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}
	
	@RequestMapping(value="/service/decBase64.do", method=RequestMethod.GET)
	public String doDecBase64(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/decBase64.do");
		String strInput="";
		try {
			
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			strInput = request.getParameter("strInput")==null?"":request.getParameter("strInput");
			out.println(Utility.decodeBase64(strInput));
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}
	@RequestMapping(value="/service/populateData.do", method=RequestMethod.GET)
	public String populateData(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/populateData.do");
		try {
			
			/*PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			List<StateMaster> stateMasters= stateMasterDAO.findAll();
			out.println("totalStates: "+stateMasters.size());
			
			PreparedStatement  pstmt=null;
			CGReportService crs = new CGReportService();
			Connection con = crs.getConnection();
			con.setAutoCommit(true);		
			//pstmt = con.prepareStatement("update certificatemaster set stateId=? where stateShortName=?");
			pstmt = con.prepareStatement("update praxismaster set stateId=? where stateCode=?");
			
			for (StateMaster stateMaster : stateMasters) {
				out.print(stateMaster.getStateShortName()+"<br>");
				pstmt.setDouble(1, stateMaster.getStateId());
				pstmt.setString(2, stateMaster.getStateShortName());
				pstmt.addBatch();
			}
			pstmt.executeBatch();*/
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}
	
	@RequestMapping(value="/service/updateQuestionCount.do", method=RequestMethod.GET)
	public String updateQuestionCount(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/updateQuestionCount.do");
		try {
						
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			CGReportService crs = new CGReportService();
			Connection con1 = crs.getConnection();
			String sql = "SELECT questionUId, COUNT(*) as cnt FROM  `teacherassessmentquestions` GROUP BY  `questionUId`";
			Statement stmt = con1.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			PreparedStatement  pstmt=null;
			Connection con = crs.getConnection();
			con.setAutoCommit(true);		
			
			pstmt = con.prepareStatement("update questionspool set questionTaken=? where questionUId=?");
			
			while(rs.next()){
				out.print(rs.getString("questionUId")+" "+rs.getInt("cnt")+"<br>");
				pstmt.setInt(1,rs.getInt("cnt"));
				pstmt.setString(2, rs.getString("questionUId"));
				pstmt.addBatch();
			}
			pstmt.executeBatch();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}
	@RequestMapping(value="/service/populateTeacherData.do", method=RequestMethod.GET)
	public String populateTeacherData(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/populateTeacherData.do");
		try {
			
			
			/*String time = String.valueOf(System.currentTimeMillis()).substring(6);
			//String basePath = request.getRealPath("/")+"/candidate";
			String basePath = request.getSession().getServletContext().getRealPath ("/")+"/candidate";
			String fileName ="candidategrid"+time+".xls";

			System.out.println(basePath+"/"+fileName);
			
			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);

			file = new File(basePath+"/"+fileName);
			
			
			*/
			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));

			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;

			//WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
			
			response.setContentType("application/vnd.ms-excel");
		    response.setHeader("Content-Disposition",    "attachment; filename=candidates.xls");

		    WritableWorkbook workbook = Workbook.createWorkbook(response.getOutputStream());
		    
			workbook.createSheet("CANDIDATE SCORE", 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);

			// Create create a bold font with unterlines
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			
			header.setBackground(Colour.GRAY_25);
			/*
			WritableImage wi = new WritableImage(0, 0, 2, 2, new File(request.getRealPath("/")+"images/Logo with Beta300.png"));
			excelSheet.addImage(wi);*/
			//WritableImage imgobj=new WritableImage(5, 7, 9, 11, new File("F:/JAVA PROJECTS/Jexcel/Sunset.png"));
			//sheet.addImage(imgobj);

			// Write a few headers
			String districtId = request.getParameter("districtId");
			if(districtId==null)
				return null;
			
			DistrictMaster districtMaster = new DistrictMaster();
			districtMaster.setDistrictId(Integer.parseInt(districtId));
			
			districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			
			excelSheet.mergeCells(0, 0, 8, 1);
			Label label;
			label = new Label(0, 0, districtMaster.getDistrictName(), timesBoldUnderline);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 2, 8, 2);
			label = new Label(0, 2, "Candidates Scores ",headerBold);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 3, 8, 3);
			label = new Label(0, 3, "");
			excelSheet.addCell(label);
			excelSheet.getSettings().setDefaultColumnWidth(18);
			
			
			
			Map<String, PercentileScoreByJob> mapJobPercentile = new HashMap<String, PercentileScoreByJob>();
			List<JobOrder> jobLst = new ArrayList<JobOrder>();
			
			jobLst = jobOrderDAO.findJobOrdersbyDistrict(districtMaster);
			System.out.println("jobLst.size(): "+jobLst.size());
			List<PercentileScoreByJob> lstPercentileScoreByJobs = percentileScoreByJobDAO.findPercentileScoreByJobList(jobLst);
			for(PercentileScoreByJob percentileScoreByJob : lstPercentileScoreByJobs){
				mapJobPercentile.put(percentileScoreByJob.getDomainMaster().getDomainId()+"##"+percentileScoreByJob.getJobOrder().getJobId()+"##"+percentileScoreByJob.getScore(), percentileScoreByJob);
			}			
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>mapJobPercentile.size(): "+mapJobPercentile.size());
			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			Map<String,RawDataForDomain> mapRawDomainScore = new HashMap<String, RawDataForDomain>();
			List<RawDataForDomain> lstRawDataForDomain = null;
			List<TeacherDetail> teacherDetails= new ArrayList<TeacherDetail>();
			
			teacherDetails = jobForTeacherDAO.findUniqueApplicantsbyJobOrders(jobLst);
			
			for(DomainMaster dm: lstDomain){				
				lstRawDataForDomain = rawDataForDomainDAO.findByDomainAndTeachers(dm, teacherDetails);
				for(RawDataForDomain rdm : lstRawDataForDomain){
					mapRawDomainScore.put(rdm.getDomainMaster().getDomainId()+"##"+rdm.getTeacherDetail().getTeacherId(), rdm);
				}
			}
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>mapRawDomainScore.size(): "+mapRawDomainScore.size());
			List<JobForTeacher> lstJobForTeacherList= null;
			lstJobForTeacherList = jobForTeacherDAO.findJFTbyJobOrders(jobLst);
			PercentileScoreByJob pScroreJob = null;
			RawDataForDomain rawDataForDomain= null;
			System.out.println("lstJobForTeacherList.size() : "+lstJobForTeacherList.size());
			int k=4;
			int col=1;
			
			label = new Label(0, k, "Candidate Id",header); 
			excelSheet.addCell(label);
			label = new Label(1, k, "Candidate Name",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Candidate Email",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Job Id",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Job Title",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Attitudinal",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Cognitive Ability",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Teaching Skills",header); 
			excelSheet.addCell(label);
			k++;
			
			String tFname = null;
			String tLname = null;
			String tEmail = null;
			System.out.println("jft:: "+lstJobForTeacherList.size());
			for(JobForTeacher jft:lstJobForTeacherList){
				TeacherDetail teacherDetail = jft.getTeacherId();
				
				if(jft.getTeacherId()!=null){
					tFname = jft.getTeacherId().getFirstName()==null?"":jft.getTeacherId().getFirstName();
					tLname = jft.getTeacherId().getLastName()==null?"":jft.getTeacherId().getLastName();
					tEmail = jft.getTeacherId().getEmailAddress()==null?"":jft.getTeacherId().getEmailAddress();
				}

				excelSheet.getSettings().setDefaultColumnWidth(18);
				col=1;
				label = new Label(0, k, ""+teacherDetail.getTeacherId()); 
				excelSheet.addCell(label);
				label = new Label(1, k, ""+tFname+" "+tLname+" "); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, tEmail); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, ""+jft.getJobId().getJobId()); 
				excelSheet.addCell(label);
				label = new Label(++col, k, ""+jft.getJobId().getJobTitle()); 
				excelSheet.addCell(label);
				
				for(DomainMaster domain: lstDomain){	
					String key = domain.getDomainId()+"##"+teacherDetail.getTeacherId();
					//System.out.println(key);
					
					rawDataForDomain =  mapRawDomainScore.get(key);
					//System.out.println(rawDataForDomain);
					if(rawDataForDomain!=null){
						key = domain.getDomainId()+"##"+jft.getJobId().getJobId()+"##"+Math.round(rawDataForDomain.getScore());						
						//System.out.println(key);
						pScroreJob = mapJobPercentile.get(key);
						
						if(pScroreJob!=null)
						{
						 //System.out.println(Math.round(pScroreJob.gettValue()));
						 label = new Label(++col, k, ""+Math.round(pScroreJob.gettValue()));  
							excelSheet.addCell(label);
						}
						else
						{
							//System.out.println("N/A");
							label = new Label(++col, k, "N/A");  
							excelSheet.addCell(label);
						}
					}else{
						//System.out.println("N/A");
						label = new Label(++col, k, "N/A");  
						excelSheet.addCell(label);
					}
				}
				++k;
			}
			
			workbook.write();
			workbook.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}
	
	@RequestMapping(value="/service/removeduplicateqq.do", method=RequestMethod.GET)
	public String removeDuplicateQQ(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/removeduplicateqq.do");
		try {
			CGReportService cgReportService = new CGReportService();
			cgReportService.removeDuplicateQQ();
			System.out.println("Removed............");
		}catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	// Ashish
	@RequestMapping(value="/doupdatesubject.do", method=RequestMethod.GET)
	public String doUpdateSubject(ModelMap map,HttpServletRequest request)
	{	
		System.out.println("====================doupdatesubject.do");
		
		SessionFactory factory=subjectMasterDAO.getSessionFactory();
		StatelessSession statelessSession=factory.openStatelessSession();
		Transaction transaction=statelessSession.beginTransaction();
		
		List<DistrictMaster> lstdisDistrictMasters11 = districtMasterDAO.findActiveDistrictMaster();
		System.out.println(" Active District Size :: "+lstdisDistrictMasters11.size());
		List<SubjectMaster> lstSubjectMast = subjectMasterDAO.findActiveSubject();
		
		for(DistrictMaster d:lstdisDistrictMasters11)
		{
			if(d.getDistrictId()!=5509600)
			{
				for(SubjectMaster sub: lstSubjectMast)
				{
					SubjectMaster subjectMaster = new SubjectMaster();
					subjectMaster.setDistrictMaster(d);
					subjectMaster.setSubjectName(sub.getSubjectName());
					subjectMaster.setMasterSubjectId(sub.getMasterSubjectId());
					subjectMaster.setStatus(sub.getStatus());
					statelessSession.insert(subjectMaster);
				}
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
		transaction.commit();
		statelessSession.close();
		
		System.out.println("+++++++++++++++++++++++++++=== DONE:");
	
		return null;
	}
	
	@Transactional(readOnly=false)
	@RequestMapping(value="/subjectInDistrict.do", method=RequestMethod.GET)
	public String SubjectInDistrict(ModelMap map,HttpServletRequest request)
	{	
		SessionFactory factory=jobOrderDAO.getSessionFactory();
		StatelessSession statelessSession=factory.openStatelessSession();
		Transaction transaction=statelessSession.beginTransaction();
		
		/*-------------------------------------------------------*/
			//List<JobOrder> jobList = jobOrderDAO.findAll();
			
			List<JobOrder> jobList = jobOrderDAO.getJobOrderWithSubject();

			System.out.println(" jobList with subjerct :: "+jobList.size());
			
			Map<String , SubjectMaster> mapTemp=new HashMap<String, SubjectMaster>();
			List<SubjectMaster> subjectMasters=new ArrayList<SubjectMaster>();
			subjectMasters=subjectMasterDAO.findAll();
			if(subjectMasters!=null && subjectMasters.size() > 0)
			{
				for(SubjectMaster subjectMaster:subjectMasters)
				{
					if(subjectMaster!=null)
					{
						String subname = subjectMaster.getSubjectName();
						DistrictMaster districtMaster = subjectMaster.getDistrictMaster();
						int iDistrictID=0;
						if(districtMaster!=null)
						{
							iDistrictID=districtMaster.getDistrictId();
						}
						String sTemp=iDistrictID+""+subname;
						if(mapTemp.get(sTemp)==null && iDistrictID > 0)
						{
							mapTemp.put(sTemp, subjectMaster);
						}
					}
				}
			}
		
			int i=0;
			int iCountTotal=0;
			iCountTotal=jobList.size();
			try {
				for(JobOrder joder:jobList)
				{
					i++;
					
						String subname = joder.getSubjectMaster().getSubjectName();
						int subId = joder.getSubjectMaster().getSubjectId();
						
						DistrictMaster districtid = joder.getDistrictMaster();
						
						int iDistrictID=0;
						if(districtid!=null)
						{
							iDistrictID=districtid.getDistrictId();
						}
						String sTemp=iDistrictID+""+subname;
						
						SubjectMaster submaster=mapTemp.get(sTemp);
						
						//SubjectMaster submaster = subjectMasterDAO.getSubjectByNameAndDistrict(districtid,subname);
						joder.setSubjectMaster(submaster);
						statelessSession.update(joder);
						System.out.println("Updating "+i+" of "+iCountTotal);
						try {
							Thread.sleep(200);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				}
				transaction.commit();
				statelessSession.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		System.out.println("+++++++++++++++++++++++++++ DONE:");
		/*-------------------------------------------------------*/
		return null;
	}
	
	@RequestMapping(value="/service/populateTeacherDataTemp.do", method=RequestMethod.GET)
	public String populateTeacherDataTemp(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/populateTeacherDataTemp.do");
		try {
			
			/*String time = String.valueOf(System.currentTimeMillis()).substring(6);
			//String basePath = request.getRealPath("/")+"/candidate";
			String basePath = request.getSession().getServletContext().getRealPath ("/")+"/candidate";
			String fileName ="candidategrid"+time+".xls";

			System.out.println(basePath+"/"+fileName);
			
			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);

			file = new File(basePath+"/"+fileName);
			
			
			*/
			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));

			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;

			//WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
			
			response.setContentType("application/vnd.ms-excel");
		    response.setHeader("Content-Disposition",    "attachment; filename=candidates.xls");

		    WritableWorkbook workbook = Workbook.createWorkbook(response.getOutputStream());
		    
			workbook.createSheet("CANDIDATE SCORE", 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);

			// Create create a bold font with unterlines
			
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);
            
			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			
			header.setBackground(Colour.GRAY_25);
			/*
			WritableImage wi = new WritableImage(0, 0, 2, 2, new File(request.getRealPath("/")+"images/Logo with Beta300.png"));
			excelSheet.addImage(wi);*/
			//WritableImage imgobj=new WritableImage(5, 7, 9, 11, new File("F:/JAVA PROJECTS/Jexcel/Sunset.png"));
			//sheet.addImage(imgobj);

			// Write a few headers
			String districtId = request.getParameter("districtId");
			if(districtId==null)
				return null;
			
			DistrictMaster districtMaster = new DistrictMaster();
			districtMaster.setDistrictId(Integer.parseInt(districtId));
			
			districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			
			excelSheet.mergeCells(0, 0, 8, 1);
			Label label;
			label = new Label(0, 0, districtMaster.getDistrictName(), timesBoldUnderline);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 2, 8, 2);
			label = new Label(0, 2, "Candidates Scores ",headerBold);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 3, 8, 3);
			label = new Label(0, 3, "");
			excelSheet.addCell(label);
			excelSheet.getSettings().setDefaultColumnWidth(18);
			
			
			
			Map<String, PercentileScoreByJob> mapJobPercentile = new HashMap<String, PercentileScoreByJob>();
			List<JobOrder> jobLst = new ArrayList<JobOrder>();
			
			jobLst = jobOrderDAO.findJobOrdersbyDistrict(districtMaster);
			System.out.println("jobLst.size(): "+jobLst.size());
			List<PercentileScoreByJob> lstPercentileScoreByJobs = percentileScoreByJobDAO.findPercentileScoreByJobList(jobLst);
			for(PercentileScoreByJob percentileScoreByJob : lstPercentileScoreByJobs){
				mapJobPercentile.put(percentileScoreByJob.getDomainMaster().getDomainId()+"##"+percentileScoreByJob.getJobOrder().getJobId()+"##"+percentileScoreByJob.getScore(), percentileScoreByJob);
			}			
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>mapJobPercentile.size(): "+mapJobPercentile.size());
			List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
			Map<String,RawDataForDomain> mapRawDomainScore = new HashMap<String, RawDataForDomain>();
			List<RawDataForDomain> lstRawDataForDomain = null;
			List<TeacherDetail> teacherDetails= new ArrayList<TeacherDetail>();
			
			//teacherDetails = jobForTeacherDAO.findUniqueApplicantsbyJobOrders(jobLst);
			String teachers ="724,761,798,900,929,940,990,1035,1048,1077,1187,1205,1226,1265,1309,1338,1371,1389,1442,1487,1601,1671,1686,1689,1707,1747,1868,1980,1996,2049,2077,2093,2157,2165,2241,2303,2318,2356,2549,2557,2561,2662,2814,2822,2849,2931,2953,2963,2965,2984,3084,3105,3297,3318,4702,4865,4867,4868,4873,4876,4877,4878,4880,4881,4885,4893,4894,4904,4905,4913,4914,4915,4916,4917,4923,4925,4928,4942,4944,4948,4956,4957,4958,4959,4961,4963,4964,4966,4967,4972,4974,4975,4976,4981,4985,4988,4990,4991,4995,4997,4998,5000,5002,5003,5004,5008,5015,5016,5019,5022,5023,5028,5029,5033,5036,5039,5045,5046,5050,5051,5052,5053,5054,5057,5058,5072,5074,5078,5080,5081,5083,5088,5093,5096,5098,5102,5110,5113,5114,5117,5118,5120,5121,5122,5123,5126,5131,5139,5142,5143,5147,5152,5153,5156,5157,5174,5177,5186,5193,5197,5206,5209,5210,5212,5217,5232,5234,5239,5251,5252,5253,5254,5263,5267,5270,5271,5272,5273,5276,5277,5278,5279,5280,5281,5282,5283,5284,5288,5291,5293,5296,5297,5298,5301,5304,5309,5310,5311,5314,5316,5317,5324,5329,5330,5345,5346,5348,5349,5356,5364,5366,5367,5373,5374,5375,5377,5378,5383,5388,5396,5402,5403,5405,5407,5416,5418,5419,5425,5436,5437,5452,5454,5455,5457,5463,5466,5471,5472,5485,5487,5488,5491,5492,5493,5494,5496,5498,5504,5505,5506,5510,5512,5513,5516,5518,5519,5520,5525,5530,5531,5533,5541,5542,5544,5545,5548,5549,5551,5559,5560,5564,5567,5570,5574,5575,5576,5577,5582,5587,5588,5594,5603,5604,5606,5613,5615,5617,5624,5629,5632,5653,5654,5656,5661,5664,5670,5673,5677,5678,5686,5694,5703,5704,5708,5709,5717,5718,5734,5737,5743,5744,5747,5748,5750,5756,5757,5758,5760,5761,5762,5769,5772,5780,5785,5786,5787,5790,5794,5795,5796,5799,5801,5802,5804,5806,5809,5812,5813,5814,5815,5816,5818,5819,5820,5821,5822,5823,5834,5836,5837,5849,5854,5860,5862,5864,5865,5867,5870,5871,5872,5873,5874,5877,5885,5889,5891,5892,5894,5897,5903,5904,5908,5909,5912,5914,5916,5920,5921,5923,5924,5925,5927,5928,5930,5931,5932,5936,5938,5940,5947,5948,5951,5956,5957,5961,5975,5979,5980,5982,5989,5990,5991,5994,5995,5997,5998,5999,6006,6007,6008,6011,6015,6016,6034,6035,6036,6043,6045,6048,6061,6062,6063,6067,6069,6070,6071,6072,6073,6074,6076,6078,6083,6084,6085,6086,6087,6088,6089,6090,6091,6095,6101,6111,6112,6118,6128,6150,6152,6153,6154,6159,6163,6164,6167,6168,6169,6170,6171,6173,6174,6176,6179,6180,6181,6183,6184,6188,6193,6194,6195,6200,6205,6210,6211,6212,6213,6218,6219,6221,6222,6232,6236,6237,6242,6245,6246,6251,6253,6256,6259,6261,6265,6266,6268,6276,6277,6282,6289,6290,6293,6294,6299,6301,6303,6304,6305,6306,6309,6317,6321,6328,6329,6332,6337,6339,6350,6359,6361,6363,6369,6371,6372,6373,6381,6383,6384,6385,6387,6388,6393,6396,6397,6399,6401,6402,6407,6408,6409,6410,6411,6415,6419,6420,6422,6423,6433,6435,6437,6439,6441,6443,6454,6456,6457,6458,6460,6464,6466,6467,6473,6474,6480,6485,6486,6488,6489,6490,6491,6492,6493,6497,6498,6501,6502,6503,6504,6507,6508,6509,6510,6513,6517,6518,6524,6525,6527,6530,6533,6539,6545,6547,6551,6553,6555,6557,6560,6561,6565,6574,6576,6577,6579,6580,6581,6587,6590,6604,6607,6609,6610,6613,6614,6616,6619,6621,6623,6627,6628,6630,6635,6636,6639,6640,6645,6648,6649,6650,6651,6652,6653,6654,6655,6656,6662,6663,6667,6669,6671,6672,6675,6676,6677,6679,6681,6683,6687,6688,6689,6690,6691,6692,6693,6695,6703,6704,6706,6707,6709,6714,6716,6717,6718,6725,6726,6731,6734,6735,6741,6744,6748,6749,6754,6755,6756,6759,6761,6773,6775,6776,6780,6781,6784,6785,6786,6819,6820,6823,6826,6827,6830,6834,6835,6840,6842,6844,6847,6850,6851,6854,6863,6867,6875,6878,6879,6885,6886,6889,6894,6895,6897,6906,6907,6908,6910,6911,6913,6914,6916,6919,6921,6922,6926,6928,6931,6934,6950,6955,6962,6977,6992,6993,6995,6996,6998,7005,7009,7010,7013,7014,7023,7026,7027,7030,7031,7032,7034,7042,7043,7045,7048,7049,7061,7064,7069,7071,7075,7080,7085,7087,7088,7089,7090,7092,7094,7096,7097,7101,7103,7111,7116,7117,7118,7120,7122,7126,7127,7129,7131,7133,7134,7137,7138,7139,7146,7148,7149,7151,7152,7153,7154,7157,7163,7164,7166,7167,7170,7174,7180,7182,7184,7186,7188,7189,7191,7192,7194,7198,7202,7204,7209,7216,7219,7221,7223,7224,7227,7228,7231,7234,7237,7239,7240,7241,7242,7245,7246,7249,7250,7299,7301,7303,7304,7306,7312,7317,7318,7321,7322,7323,7326,7328,7333,7334,7335,7352,7358,7363,7365,7366,7370,7377,7378,7382,7385,7395,7398,7409,7410,7411,7413,7415,7416,7418,7419,7421,7423,7424,7427,7432,7433,7434,7436,7437,7444,7445,7447,7450,7455,7458,7462,7465,7466,7468,7476,7477,7478,7481,7483,7502,7505,7507,7511,7514,7523,7524,7525,7526,7527,7530,7531,7532,7534,7541,7542,7543,7547,7548,7550,7551,7554,7560,7561,7563,7571,7573,7576,7577,7585,7586,7590,7595,7606,7609,7610,7612,7613,7614,7618,7619,7620,7621,7622,7626,7627,7630,7635,7636,7639,7640,7642,7650,7654,7661,7662,7667,7674,7675,7676,7679,7684,7685,7688,7690,7691,7692,7695,7696,7697,7704,7718,7737,7740,7743,7749,7792,7794,7795,7796,7797,7798,7800,7801,7803,7809,7813,7815,7826,7830,7832,7875,7876,7877,7878,7882,7886,7893,7894,7903,7907,7908,7916,7917,7922,7925,7926,7929,7930,7936,7937,7940,7946,7948,7952,7956,7960,7961,7962,7965,7966,7967,7968,7970,7971,7974,7975,7977,7980,7984,7988,7991,8002,8008,8010,8014,8015,8019,8023,8024,8025,8029,8053,8060,8064,8068,8069,8075,8076,8077,8092,8095,8097,8098,8114,8117,8121,8124,8133,8138,8147,8192,8198,8202,8203,8211,8212,8213,8218,8222,8223,8233,8234,8235,8236,8238,8245,8246,8248,8249,8254,8255,8259,8264,8265,8267,8268,8269,8280,8288,8295,8311,8312,8317,8319,8320,8345,8346,8347,8348,8357,8364,8378,8380,8382,8384,8386,8387,8388,8389,8394,8396,8397,8400,8402,8404,8405,8406,8407,8413,8415,8420,8421,8422,8425,8432,8437,8440,8442,8445,8449,8459,8464,8467,8468,8471,8488,8490,8494,8495,8497,8498,8502,8504,8508,8509,8518,8523,8524,8527,8531,8532,8535,8536,8538,8544,8545,8566,8568,8569,8578,8582,8586,8587,8593,8595,8597,8600,8616,8630,8633,8636,8640,8643,8657,8666,8670,8672,8673,8674,8676,8682,8687,8688,8691,8702,8707,8708,8718,8720,8723,8727,8740,8744,8748,8759,8762,8769,8771,8783,8785,8787,8789,8790,8796,8800,8801,8805,8806,8809,8818,8819,8822,8823,8827,8830,8836,8839,8851,8859,8860,8861,8867,8871,8872,8873,8874,8875,8884,8912,8914,8917,8918,8919,8929,8935,8942,8943,8944,8945,8948,8949,8950,8951,8954,8956,8958,8959,8966,8976,8980,8982,8983,8985,8986,8991,9002,9004,9009,9013,9016,9027,9033,9039,9049,9052,9053,9054,9055,9058,9059,9069,9078,9081,9083,9084,9088,9098,9101,9103,9115,9119,9120,9122,9132,9142,9143,9144,9146,9149,9154,9161,9162,9168,9171,9172,9175,9176,9178,9190,9193,9205,9206,9216,9217,9375,9377,9378,9379,9385,9386,9388,9392,9398,9400,9404,9405,9406,9417,9430,9437,9446,9451,9452,9454,9457,9458,9459,9478,9479,9480,9493,9498,9512,9524,9527,9529,9532,9540,9547,9549,9550,9551,9558,9575,9578,9586,9595,9598,9602,9605,9616,9621,9629,9632,9639,9642,9648,9655,9660,9686,9696,9701,9705,9710,9711,9726,9738,9749,9751,9756,9764,9769,9778,9792,9794,9799,9802,9825,9831,9837,9844,9848,9857,9858,9864,9873,9884,9886,9891,9898,9899,9900,9922,10095,10096,10102,10103,10107,10109,10111,10116,10117,10135,10140,10141,10142,10144,10157,10158,10161,10162,10163,10179,10180,10182,10191,10193,10199,10200,10202,10203,10204,10227,10231,10233,10242,10243,10246,10249,10255,10277,10278,10280,10286,10287,10288,10295,10296,10297,10299,10304,10306,10311,10312,10328,10331,10357,10359,10363,10409,10427,10432,10484,10494,10511,10529,10558,10564,10585,10617,10771,10778,10781,10784,10803,10810,10823,10824,10829,10840,10842,10859,10865,10866,10903,10908,10913,10914,10915,10925,10947,10972,10976,10986,10987,10997,11009,11014,11015,11017,11018,11031,11045,11058,11065,11073,11076,11088,11203,11216,11218,11221,11237,11238,11243,11252,11254,11257,11258,11270,11275,11280,11285,11293,11298,11300,11315,11323,11331,11336,11341,11346,11347,11354,11355,11356,11358,11365,11367,11372,11374,11375,11384,11389,11392,11482,11490,11491,11494,11509,11520,11530,11534,11538,11543,11546,11547,11549,11552,11555,11569,11578,11586,11591,11599,11600,11602,11603,11606,11626,11642,11644,11649,11655,11657,11659,11767,11798,11817,11825,11837,11840,11847,11867,11870,11877,11887,11888,11910,11913,11930,11938,11941,11949,11951,11954,11957,11959,11965,11969,11985,12000,12002,12064,12076,12082,12090,12093,12103,12105,12109,12114,12122,12133,12140,12156,12173,12184,12250,12251,12253,12255,12259,12260,12263,12289,12290,12297,12314,12315,12325,12332,12337,12345,12407,12409,12452,12453,12459,12466,12494,12503,12505,12514,12539,12541,12546,12557,12580,12589,12590,12592,12600,12613,12632,12650,12651,12660,12670,12688,12696,12700,12705,12708,12712,12753,12758,12760,12763,12764,12767,12775,12784,12800,12843,12849,12851,12871,12882,12890,12900,12943,12998,12999,13009,13021,13027,13058,13087,13090,13096,13099,13100,13104,13105,13112,13166,13172,13185,13191,13197,13211,13216,13241,13249,13254,13265,13266,13267,13270,13284,13286,13323,13333,13343,13345,13360,13366,13369,13371,13374,13379,13382,13385,13390,13747,13765,13773,13778,13793,13797,13799,13804,13813,13933,14641,15751,15773,15782,15791,15793,15795,15798,15800,15805,15806,15811,16108,16120,16716,17673,18205,19845,21731,21861";
			String techersList[] = teachers.split(",");
			System.out.println(">>>>> "+teachers);
			Integer tIds[] = new Integer[techersList.length];
			for(int i=0;i<tIds.length;i++)
			{
				tIds[i]=Integer.parseInt(techersList[i]);
			}
			teacherDetails = teacherDetailDAO.getTeachersHQL(tIds);
			
			for(DomainMaster dm: lstDomain){				
				lstRawDataForDomain = rawDataForDomainDAO.findByDomainAndTeachers(dm, teacherDetails);
				for(RawDataForDomain rdm : lstRawDataForDomain){
					mapRawDomainScore.put(rdm.getDomainMaster().getDomainId()+"##"+rdm.getTeacherDetail().getTeacherId(), rdm);
				}
			}
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>mapRawDomainScore.size(): "+mapRawDomainScore.size());
			List<JobForTeacher> lstJobForTeacherList= null;
			lstJobForTeacherList = jobForTeacherDAO.findJFTbyJobOrders(jobLst);
			PercentileScoreByJob pScroreJob = null;
			RawDataForDomain rawDataForDomain= null;
			System.out.println("lstJobForTeacherList.size() : "+lstJobForTeacherList.size());
			int k=4;
			int col=1;
			
			label = new Label(0, k, "Candidate Id",header); 
			excelSheet.addCell(label);
			label = new Label(1, k, "Candidate Name",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Candidate Email",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Job Id",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Job Title",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Attitudinal",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Cognitive Ability",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Teaching Skills",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Composite Score",header); 
			excelSheet.addCell(label);
			k++;
			
			String tFname = null;
			String tLname = null;
			String tEmail = null;
			System.out.println("jft:: "+lstJobForTeacherList.size());
			double domainTotatlScore =0;
			double tScoreJob = 0.0;
			DecimalFormat oneDForm = new DecimalFormat("###,###.0");
			for(JobForTeacher jft:lstJobForTeacherList){
				domainTotatlScore=0.0;
				tScoreJob = 0.0;
				TeacherDetail teacherDetail = jft.getTeacherId();
				
				if(jft.getTeacherId()!=null){
					tFname = jft.getTeacherId().getFirstName()==null?"":jft.getTeacherId().getFirstName();
					tLname = jft.getTeacherId().getLastName()==null?"":jft.getTeacherId().getLastName();
					tEmail = jft.getTeacherId().getEmailAddress()==null?"":jft.getTeacherId().getEmailAddress();
				}

				excelSheet.getSettings().setDefaultColumnWidth(18);
				col=1;
				label = new Label(0, k, ""+teacherDetail.getTeacherId()); 
				excelSheet.addCell(label);
				label = new Label(1, k, ""+tFname+" "+tLname+" "); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, tEmail); 
				excelSheet.addCell(label);
				
				label = new Label(++col, k, ""+jft.getJobId().getJobId()); 
				excelSheet.addCell(label);
				label = new Label(++col, k, ""+jft.getJobId().getJobTitle()); 
				excelSheet.addCell(label);
				boolean inc=true;
				for(DomainMaster domain: lstDomain){	
					String key = domain.getDomainId()+"##"+teacherDetail.getTeacherId();
					//System.out.println(key);
					
					rawDataForDomain =  mapRawDomainScore.get(key);
					
					//System.out.println(rawDataForDomain);
					if(rawDataForDomain!=null){
						key = domain.getDomainId()+"##"+jft.getJobId().getJobId()+"##"+Math.round(rawDataForDomain.getScore());						
						//System.out.println(key);
						pScroreJob = mapJobPercentile.get(key);
						
						if(pScroreJob!=null)
						{
						 //System.out.println(Math.round(pScroreJob.gettValue()));
							tScoreJob = Math.round(pScroreJob.gettValue());
							label = new Label(++col, k, ""+Math.round(pScroreJob.gettValue()));  
							excelSheet.addCell(label);
						}
						else
						{
							//System.out.println("N/A");
							label = new Label(++col, k, "N/A");  
							excelSheet.addCell(label);
							tScoreJob = 0.00;
							inc=false;
						}
					}else{
						//System.out.println("N/A");
						label = new Label(++col, k, "N/A");  
						excelSheet.addCell(label);
						inc=false;
					}
					domainTotatlScore = domainTotatlScore+(tScoreJob*domain.getMultiplier());
				}
				
				label = new Label(++col, k, ""+Math.round(Double.valueOf(oneDForm.format(domainTotatlScore))));  
				excelSheet.addCell(label);
				domainTotatlScore=0.0;
				tScoreJob = 0.0;
				
				if(inc)
				++k;
				
				
				
			}
			
			workbook.write();
			workbook.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}
	@RequestMapping(value="/service/populateTeacherHistoryData.do", method=RequestMethod.GET)
	public String populateTeacherHistoryData(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/populateTeacherHistoryData.do");
		try {
			
		
			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));

			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;

			response.setContentType("application/vnd.ms-excel");
		    response.setHeader("Content-Disposition",    "attachment; filename=candidates.xls");

		    WritableWorkbook workbook = Workbook.createWorkbook(response.getOutputStream());
		    
			workbook.createSheet("CANDIDATE LOGIN  HISTORY", 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);

			// Create create a bold font with unterlines
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			
			header.setBackground(Colour.GRAY_25);
	
			// Write a few headers
			String districtId = request.getParameter("districtId");
			if(districtId==null)
				return null;
			
			DistrictMaster districtMaster = new DistrictMaster();
			districtMaster.setDistrictId(Integer.parseInt(districtId));
			districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			
			excelSheet.mergeCells(0, 0, 8, 1);
			Label label;
			label = new Label(0, 0, districtMaster.getDistrictName(), timesBoldUnderline);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 2, 8, 2);
			label = new Label(0, 2, "CANDIDATE LOGIN  HISTORY",headerBold);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 3, 8, 3);
			label = new Label(0, 3, "");
			excelSheet.addCell(label);
			excelSheet.getSettings().setDefaultColumnWidth(18);
			
			List<JobForTeacher> jftList=jobForTeacherDAO.findTeacersByDistrict(districtMaster);
			SortedMap<Integer, TeacherDetail> mapTeacher = new  TreeMap<Integer, TeacherDetail>();
			Set<TeacherDetail> teacherList=new  HashSet<TeacherDetail>();
			for (JobForTeacher jobForTeacher : jftList) {
				teacherList.add(jobForTeacher.getTeacherId());
				mapTeacher.put(jobForTeacher.getTeacherId().getTeacherId(),jobForTeacher.getTeacherId());
			}
			List<TeacherLoginHistory> lstTLH=teacherLoginHistoryDAO.findTeachers(teacherList);
			List<TeacherProfileVisitHistory> lstTPVH =teacherProfileVisitHistoryDAO.findTeacherProfileVisitHistoryByTeachers(teacherList);
			
			System.out.println("jftList:::::"+jftList.size());
			System.out.println("lstTPVH:::::"+lstTPVH.size());
			
			Map<Integer, String> mapTLH = new HashMap<Integer, String>();
			Map<String, Integer> mapUserTeacher= new HashMap<String, Integer>();
			Map<Integer, String> mapUserTeacherCount= new HashMap<Integer, String>();
			
			if(lstTLH.size()>0){
				for (TeacherLoginHistory obj : lstTLH) {
					mapTLH.put(obj.getTeacherDetail().getTeacherId(), obj.getLoginTime()+"");
				}
			}
			
			if(jftList.size()>0)
			for (TeacherProfileVisitHistory TPVHObj :lstTPVH){
				String teacterIdUserId=TPVHObj.getTeacherDetail().getTeacherId()+"##"+TPVHObj.getUserMaster().getFirstName()+" "+TPVHObj.getUserMaster().getLastName()+"##"+TPVHObj.getUserMaster().getUserId();
				int count =0;
				try{
					count=mapUserTeacher.get(teacterIdUserId);
				}catch(Exception e){
					
				}
				if(count==0){
					mapUserTeacher.put(teacterIdUserId, 1);
				}else{
					mapUserTeacher.put(teacterIdUserId, 1+count);
				}
			}
			System.out.println("mapUserTeacher:::"+mapUserTeacher.size());
			for (Map.Entry<String, Integer> entry : mapUserTeacher.entrySet()) {
				String key=entry.getKey();
				int value = entry.getValue();
				if(key!=null){
					String[] keyArr =key.split("##");
					String userCount=null;
					
					try{
						userCount=mapUserTeacherCount.get(Integer.parseInt(keyArr[0]));
					}catch(Exception e){
						
					}
					try{
						if(userCount==null){
							mapUserTeacherCount.put(Integer.parseInt(keyArr[0]),keyArr[1]+" ("+value+")");
						}else{
							userCount+=", "+keyArr[1]+" ("+value+")";
							mapUserTeacherCount.put(Integer.parseInt(keyArr[0]),userCount);
						}
					}catch(Exception e){
						
					}
				}
			}
			int k=4;
			int col=1;
			
			label = new Label(0, k, "Candidate Id",header); 
			excelSheet.addCell(label);
			label = new Label(1, k, "Candidate Name",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Email Address",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Number of times accessing TeacherMatch",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Last time logged into TeacherMatch",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Number of jobs viewed in TeacherMatch",header); 
			excelSheet.addCell(label);
			k++;
			
			String tFname = null;
			String tLname = null;
			String tEmail = null;
			if(mapTeacher.size()>0)
				for (Map.Entry<Integer,TeacherDetail> entry : mapTeacher.entrySet()) {
					TeacherDetail teacherDetail=entry.getValue();
					if(teacherDetail!=null){
						tFname =teacherDetail.getFirstName()==null?"":teacherDetail.getFirstName();
						tLname =teacherDetail.getLastName()==null?"":teacherDetail.getLastName();
						tEmail =teacherDetail.getEmailAddress()==null?"":teacherDetail.getEmailAddress();
					}
	
					excelSheet.getSettings().setDefaultColumnWidth(18);
					col=1;
					label = new Label(0, k, ""+teacherDetail.getTeacherId()); 
					excelSheet.addCell(label);
					label = new Label(1, k, ""+tFname+" "+tLname+" "); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, tEmail); 
					excelSheet.addCell(label);
					
					String userCount=mapUserTeacherCount.get(teacherDetail.getTeacherId());
					
					label = new Label(++col, k, ""+teacherDetail.getNoOfLogin()); 
					excelSheet.addCell(label);
					String lastLoginTime=mapTLH.get(teacherDetail.getTeacherId());
					if(lastLoginTime!=null){
						label = new Label(++col, k, ""+lastLoginTime);
					}else{
						label = new Label(++col, k, "");
					}
					excelSheet.addCell(label);
					if(userCount!=null){
						label = new Label(++col, k, ""+userCount);
					}else{
						label = new Label(++col, k, "");
					}
					excelSheet.addCell(label);
					
					++k;
			}
			workbook.write();
			workbook.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}

	@RequestMapping(value="/service/populateUserHistoryData.do", method=RequestMethod.GET)
	public String populateUserHistoryData(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/populateUserHistoryData.do");
		try {
			
		
			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));

			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;

			response.setContentType("application/vnd.ms-excel");
		    response.setHeader("Content-Disposition",    "attachment; filename=candidates.xls");

		    WritableWorkbook workbook = Workbook.createWorkbook(response.getOutputStream());
		    
			workbook.createSheet("USER LOGIN  HISTORY", 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);

			// Create create a bold font with unterlines
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			
			header.setBackground(Colour.GRAY_25);
	
			// Write a few headers
			String districtId = request.getParameter("districtId");
			if(districtId==null)
				return null;
			
			DistrictMaster districtMaster = new DistrictMaster();
			districtMaster.setDistrictId(Integer.parseInt(districtId));
			districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			
			excelSheet.mergeCells(0, 0, 8, 1);
			Label label;
			label = new Label(0, 0, districtMaster.getDistrictName(), timesBoldUnderline);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 2, 8, 2);
			label = new Label(0, 2, "USER LOGIN  HISTORY",headerBold);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 3, 8, 3);
			label = new Label(0, 3, "");
			excelSheet.addCell(label);
			excelSheet.getSettings().setDefaultColumnWidth(18);
			
			List<UserLoginHistory> userLoginHistoryList =userLoginHistoryDAO.findUser(districtMaster);
			Map<Integer, UserLoginHistory> mapULH = new HashMap<Integer, UserLoginHistory>();
			Map<Integer, Integer> mapULHNoChk = new HashMap<Integer, Integer>();
			
			System.out.println("userLoginHistoryList:::"+userLoginHistoryList.size());
			int kk=0;
			if(userLoginHistoryList.size()>0)
			for (UserLoginHistory userLoginHistory : userLoginHistoryList) {
				try{
					mapULH.put(userLoginHistory.getUserMaster().getUserId(), userLoginHistory);
				}catch(Exception e){
					e.printStackTrace();
				}
				try{
					mapULHNoChk.put(userLoginHistory.getId(), userLoginHistory.getUserMaster().getUserId());
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			System.out.println("mapULH:::"+mapULH.size());
			System.out.println("mapULHNoChk:::"+mapULHNoChk.size());

			int k=4;
			int col=1;
			
			label = new Label(0, k, "User Id",header); 
			excelSheet.addCell(label);
			label = new Label(1, k, "User Name",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Email Address",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Number of times accessing TeacherMatch",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Last time logged into TeacherMatch",header); 
			excelSheet.addCell(label);
			label = new Label(++col, k,"Number of jobs viewed in TeacherMatch",header); 
			excelSheet.addCell(label);
			k++;
			
			String tFname = null;
			String tLname = null;
			String tEmail = null;
			if(mapULH.size()>0)
				for (Map.Entry<Integer, UserLoginHistory> entry : mapULH.entrySet()) {
				UserLoginHistory ulh = entry.getValue();
				if(ulh!=null){
					UserMaster userMaster = ulh.getUserMaster();
					
					if(userMaster!=null){
						tFname =userMaster.getFirstName()==null?"":userMaster.getFirstName();
						tLname =userMaster.getLastName()==null?"":userMaster.getLastName();
						tEmail =userMaster.getEmailAddress()==null?"":userMaster.getEmailAddress();
					}
	
					excelSheet.getSettings().setDefaultColumnWidth(18);
					col=1;
					label = new Label(0, k, ""+userMaster.getUserId()); 
					excelSheet.addCell(label);
					label = new Label(1, k, ""+tFname+" "+tLname+" "); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, tEmail); 
					excelSheet.addCell(label);
					
					int countLogin=getCount(mapULHNoChk,userMaster.getUserId());
					
					label = new Label(++col, k, ""+countLogin); 
					excelSheet.addCell(label);
					label = new Label(++col, k, ""+ulh.getLogTime()); 
					excelSheet.addCell(label);
					
					label = new Label(++col, k, ""); 
					excelSheet.addCell(label);
					
					++k;
				}
			}
			
			workbook.write();
			workbook.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}
	public int getCount(Map<Integer, Integer> mapULHNoChk,int userId){
		int count=0;
		if(mapULHNoChk.size()>0)
			for (Map.Entry<Integer,Integer> entry : mapULHNoChk.entrySet()) {
				  int key = entry.getKey();
				  int value = entry.getValue();
					if(value==userId){
						count++;
					}
				}
		return count;
	}
	
	
	@RequestMapping(value="/service/internaltfj.do", method=RequestMethod.GET)
	public String internalTFJ(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("Calling Controller /service/internaltfj.do");
		try 
		{
			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));

			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;

			response.setContentType("application/vnd.ms-excel");
		    response.setHeader("Content-Disposition",    "attachment; filename=internaltfj.xls");

		    WritableWorkbook workbook = Workbook.createWorkbook(response.getOutputStream());
		    
			workbook.createSheet("Internal Teacher Job Details", 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);

			// Create create a bold font with unterlines
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			header.setBackground(Colour.GRAY_25);
	
			// Write a few headers
			String districtId = request.getParameter("districtId");
			if(districtId==null)
				return null;
			
			DistrictMaster districtMaster = new DistrictMaster();
			districtMaster.setDistrictId(Integer.parseInt(districtId));
			districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			
			int iRowNo=4;
			excelSheet.mergeCells(0, 0, 4, 1);
			Label label;
			label = new Label(0, 0, districtMaster.getDistrictName(), timesBoldUnderline);
			excelSheet.addCell(label);
			
			excelSheet.mergeCells(0, 2, 4, 2);
			label = new Label(0, 2, "Internal Transfer Candidates Job Details",headerBold);
			excelSheet.addCell(label);
			
			excelSheet.mergeCells(0, 3, 4, 3);
			label = new Label(0, 3, "");
			excelSheet.addCell(label);
			
			excelSheet.getSettings().setDefaultColumnWidth(18);
			
			List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
			Map<Integer, InternalTransferCandidates> mapITD=new HashMap<Integer, InternalTransferCandidates>();
			List<JobOrder> lstjobOrders=new ArrayList<JobOrder>();
			List<JobForTeacher> lstJobForTeachers=new ArrayList<JobForTeacher>();
			
			lstjobOrders=jobOrderDAO.findJobByDistrict(districtMaster);
			
			List<InternalTransferCandidates> lstInternalTransferCandidates=new ArrayList<InternalTransferCandidates>();
			lstInternalTransferCandidates=internalTransferCandidatesDAO.getInternalCandidateDistrict(districtMaster);
			if(lstInternalTransferCandidates!=null)
			{
				if(lstInternalTransferCandidates.size() >0)
				{
					for(InternalTransferCandidates iCandidates :lstInternalTransferCandidates)
					{
						if(iCandidates!=null)
						{
							lstTeacherDetails.add(iCandidates.getTeacherDetail());
							mapITD.put(iCandidates.getTeacherDetail().getTeacherId(), iCandidates);
						}
					}
				}
			}
			
			lstJobForTeachers=jobForTeacherDAO.getTeacherIdsAndJobIds(lstTeacherDetails, lstjobOrders);
			System.out.println("lstJobForTeachers Size "+lstJobForTeachers.size());
			
			Map<Integer, String> mapJobIDsForTIds=new HashMap<Integer, String>();
			if(lstJobForTeachers!=null && lstJobForTeachers.size() > 0)
			{
				for(JobForTeacher forTeacher:lstJobForTeachers)
				{
					if(forTeacher!=null)
					{
						if(mapJobIDsForTIds.get(forTeacher.getTeacherId().getTeacherId())==null)
						{
							String sJobId=""+forTeacher.getJobId().getJobId();
							mapJobIDsForTIds.put(forTeacher.getTeacherId().getTeacherId(),sJobId);
						
						}
						else
						{
							String sJobIds=mapJobIDsForTIds.get(forTeacher.getTeacherId().getTeacherId());
							sJobIds=sJobIds+","+forTeacher.getJobId().getJobId();
							mapJobIDsForTIds.put(forTeacher.getTeacherId().getTeacherId(),sJobIds);
						}
					}
				}
			}
			
			int iCol=-1;
			
			label = new Label(++iCol,iRowNo,"Candidate Name",header); 
			excelSheet.addCell(label);
			label = new Label(++iCol,iRowNo,"Email",header); 
			excelSheet.addCell(label);
			label = new Label(++iCol,iRowNo,"Job ID(s)",header); 
			excelSheet.addCell(label);
			label = new Label(++iCol,iRowNo,"Date of Registration",header); 
			excelSheet.addCell(label);
			
			String tFname = null;
			String tLname = null;
			String tEmail = null;
			if(mapITD.size()>0)
			{
				for(Map.Entry<Integer, InternalTransferCandidates> entry : mapITD.entrySet()) 
				{
					iRowNo++;
					InternalTransferCandidates itc = entry.getValue();
					if(itc!=null)
					{
						TeacherDetail tDetail = itc.getTeacherDetail();
						if(tDetail!=null)
						{
							tFname =tDetail.getFirstName()==null?"":tDetail.getFirstName();
							tLname =tDetail.getLastName()==null?"":tDetail.getLastName();
							tEmail =tDetail.getEmailAddress()==null?"":tDetail.getEmailAddress();
						}
		
						excelSheet.getSettings().setDefaultColumnWidth(18);
						iCol=0;
						
						label = new Label(iCol, iRowNo, ""+tFname+" "+tLname+" "); 
						excelSheet.addCell(label);
						
						label = new Label(++iCol, iRowNo, tEmail); 
						excelSheet.addCell(label);
						
						String sJobIDsTemp="";
						sJobIDsTemp=mapJobIDsForTIds.get(tDetail.getTeacherId());
						
						label = new Label(++iCol, iRowNo, sJobIDsTemp); 
						excelSheet.addCell(label);
						
						label = new Label(++iCol, iRowNo, Utility.convertDateAndTimeToUSformatOnlyDate(itc.getCreatedDateTime())); 
						excelSheet.addCell(label);
					}
				}
			}
		    workbook.write();
			workbook.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}
	@RequestMapping(value="/service/updateKeyContacts.do", method=RequestMethod.GET)
	public String updateKeyContacts(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/updateKeyContacts.do");
		try {
			String req = request.getParameter("val");
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			CGReportService crs = new CGReportService();
			Connection con1 = crs.getConnection();
			String keyContact = "";
			if(req.equalsIgnoreCase("d"))
			{
				keyContact = "districtkeycontact";
			}else
			{
				keyContact = "schoolkeycontact";
			}
			String sql = "SELECT um.userId,dk.keyContactEmailAddress FROM  "+keyContact+" dk JOIN usermaster um ON dk.keyContactEmailAddress = um.emailAddress ";
			Statement stmt = con1.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			PreparedStatement  pstmt=null;
			Connection con = crs.getConnection();
			con.setAutoCommit(true);		
			
			pstmt = con.prepareStatement("update "+keyContact+" set userId=? where keyContactEmailAddress=?");
			int i=0;
			while(rs.next()){
				out.print(rs.getString("um.userId")+" "+rs.getString("dk.keyContactEmailAddress")+"<br>");
				pstmt.setInt(1,rs.getInt("um.userId"));
				pstmt.setString(2, rs.getString("dk.keyContactEmailAddress"));
				pstmt.addBatch();
				i++;
			}
			out.println("<br><br>Records: "+i);
			pstmt.executeBatch();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}

	
	@RequestMapping(value="/clamav/sacnfile.do", method=RequestMethod.GET)
	public String doClamavScanFile(HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("/clamav/sacnfile.do");
		System.out.println("education resume 54177220140803011756.doc");
		boolean bNoVirus=true;
		try {
			bNoVirus=clamAVUtil.fileScanner("/usr/share/teachermatch/teacher/41772/education resume 54177220140803011756.doc");
		} catch (Exception e){
			e.printStackTrace();
		}
		System.out.println("NoVirus "+bNoVirus);
		
		System.out.println("education resume 5.doc");
		boolean bNoVirus1=true;
		try {
			bNoVirus1=clamAVUtil.fileScanner("/usr/share/teachermatch/teacher/41772/education resume 5.doc");
		} catch (Exception e){
			e.printStackTrace();
		}
		System.out.println("1 NoVirus "+bNoVirus1);
		
		System.out.println("Reference_20140804084848.pdf");
		boolean bNoVirus2=true;
		try {
			bNoVirus2=clamAVUtil.fileScanner("/usr/share/teachermatch/teacher/41772/Reference_20140804084848.pdf");
		} catch (Exception e){
			e.printStackTrace();
		}
		System.out.println("2 NoVirus "+bNoVirus2);
		
		System.out.println("Reference_20140808092040.pdf");
		boolean bNoVirus3=true;
		try {
			bNoVirus3=clamAVUtil.fileScanner("/usr/share/teachermatch/teacher/41772/Reference_20140808092040.pdf");
		} catch (Exception e){
			e.printStackTrace();
		}
		System.out.println("3 NoVirus "+bNoVirus3);
		return null;
	}
	
	/*@RequestMapping(value="/service/updateNorm.do", method=RequestMethod.GET)
	public String updateNormscore(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/updateNorm.do");
		try {
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			CGReportService crs = new CGReportService();
			Connection con1 = crs.getConnection();
			
			List<ScoreLookup> scoreLookups = new ArrayList<ScoreLookup>();
			Map<String,ScoreLookup> scoreLookupMapDomain = new HashMap<String, ScoreLookup>();
			Map<String,ScoreLookup> scoreLookupMapCompetency = new HashMap<String, ScoreLookup>();
			scoreLookups = scoreLookupDAO.findScoreLookup(1);
			for (ScoreLookup scoreLookup : scoreLookups) {
				scoreLookupMapDomain.put(scoreLookup.getDomainMaster().getDomainId()+"###"+scoreLookup.getTotalscore(), scoreLookup);
				//System.out.println(scoreLookup.getDomainMaster().getDomainId()+"###"+scoreLookup.getTotalscore()+"  "+scoreLookup.getScorelookupId());
			}
			scoreLookups = scoreLookupDAO.findScoreLookup(2);
			for (ScoreLookup scoreLookup : scoreLookups) {
				scoreLookupMapCompetency.put(scoreLookup.getCompetencyMaster().getCompetencyId()+"###"+scoreLookup.getTotalscore(), scoreLookup);
			}
			
			SessionFactory sessionFactory =  domainMasterDAO.getSessionFactory();
			StatelessSession session1 = sessionFactory.openStatelessSession();
			
		 	List<RawDataForDomain> RawDataForDomain = rawDataForDomainDAO.findAllInCurrentYear();
		 	for (RawDataForDomain rawDataForDomain2 : RawDataForDomain) {
				AssessmentDomainScore assessmentDomainScore = new AssessmentDomainScore();
				assessmentDomainScore.setDomainMaster(rawDataForDomain2.getDomainMaster());
				assessmentDomainScore.setTeacherDetail(rawDataForDomain2.getTeacherDetail());
				ScoreLookupMaster scoreLookupMaster = new ScoreLookupMaster();
				scoreLookupMaster.setLookupId(1);
				assessmentDomainScore.setScoreLookupMaster(scoreLookupMaster);
				ScoreLookup scoreLookup = scoreLookupMapDomain.get(rawDataForDomain2.getDomainMaster().getDomainId()+"###"+rawDataForDomain2.getScore());
				if(scoreLookup!=null)
				{
					assessmentDomainScore.setPercentile(scoreLookup.getPercentile());
					
					assessmentDomainScore.setZscore(scoreLookup.getZscore());
					assessmentDomainScore.setTscore(scoreLookup.getTscore());
					assessmentDomainScore.setNcevalue(scoreLookup.getNcevalue());
					assessmentDomainScore.setNormscore(scoreLookup.getNormscore());
					assessmentDomainScore.setRitvalue(scoreLookup.getRitvalue());
					session1.insert(assessmentDomainScore);
				}
			}
		 	System.out.println("Domain wise data Done");
		 	
			StatelessSession session2 = sessionFactory.openStatelessSession();
			
		 	List<RawDataForCompetency> rawDataForCompetencies = rawDataForCompetencyDAO.findAllInCurrentYear();
		 	for (RawDataForCompetency rawDataforCompetency : rawDataForCompetencies) {
				AssessmentCompetencyScore assessmentCompetencyScore = new AssessmentCompetencyScore();
				assessmentCompetencyScore.setCompetencyMaster(rawDataforCompetency.getCompetencyMaster());
				assessmentCompetencyScore.setTeacherDetail(rawDataforCompetency.getTeacherDetail());
				ScoreLookupMaster scoreLookupMaster = new ScoreLookupMaster();
				scoreLookupMaster.setLookupId(1);
				assessmentCompetencyScore.setScoreLookupMaster(scoreLookupMaster);
				ScoreLookup scoreLookup = scoreLookupMapCompetency.get(rawDataforCompetency.getCompetencyMaster().getCompetencyId()+"###"+rawDataforCompetency.getScore());
				if(scoreLookup!=null)
				{
					assessmentCompetencyScore.setPercentile(scoreLookup.getPercentile());
					
					assessmentCompetencyScore.setZscore(scoreLookup.getZscore());
					assessmentCompetencyScore.setTscore(scoreLookup.getTscore());
					assessmentCompetencyScore.setNcevalue(scoreLookup.getNcevalue());
					assessmentCompetencyScore.setNormscore(scoreLookup.getNormscore());
					assessmentCompetencyScore.setRitvalue(scoreLookup.getRitvalue());
					session2.insert(assessmentCompetencyScore);
				}
			}
		 	System.out.println("Competency wise data Done");
		 	
		 	List assessmentDomainData = new ArrayList();
			assessmentDomainData = assessmentDomainScoreDAO.calculateCandidatesNormScore(1);
			System.out.println("lstRawData22:: "+assessmentDomainData.size());
			CGReportService cGReportService = new CGReportService();
			
			 if(tmpPercentileWiseZScoreList==null)
				{
					tmpPercentileWiseZScoreList = tmpPercentileWiseZScoreDAO.findByCriteria(Order.asc("zScore"));
					System.out.println("?????????????????????????????????????? ");
				}
				if(pztList==null)
				{
					pztList = cGReportService.populateZScoreMap(tmpPercentileWiseZScoreList);
					System.out.println("?????????????????????????????????????? ");
				}
				
				StatelessSession session3 = sessionFactory.openStatelessSession();
				
				for(Object oo: assessmentDomainData){
					Object obj[] = (Object[])oo;
					System.out.println("obj[0]:: "+obj[0]);
					System.out.println("obj[1]:: "+obj[1]);
					Double normscoreD = (Double)obj[0];
					//Integer normscore = Math.round(normscoreD);
					Integer normscore = Integer.valueOf((int) Math.round(normscoreD));
					TeacherNormScore teacherNormScore = new TeacherNormScore();
					teacherNormScore.setTeacherNormScore( normscore);
					//double percentile = ((normscore-50)*3+50);
					
					double percentile=cGReportService.getPercentile(pztList,normscore);
					 
					teacherNormScore.setPercentile(percentile);
					Object[] ob =cGReportService.getDeciles(percentile);
					teacherNormScore.setDecileColor(""+ob[0]);
					//System.out.println(ob.length+" "+(Double)ob[1]+" " +(Double)ob[2]);
					teacherNormScore.setMinDecileValue((Double)ob[1]);
					teacherNormScore.setMaxDecileValue((Double)ob[2]);
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(Integer.parseInt(String.valueOf(obj[1])));
					teacherNormScore.setTeacherDetail(teacherDetail);
					teacherNormScore.setCreatedDateTime(new Date());
					session3.insert(teacherNormScore);
				}
				
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}*/
	
	// Information is coming in this format:
	// <?xml version="1.0"?>
	// <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	//   <soap:Body>
	//    <TestDB xmlns="HR_WS">
//	      <TeacherMatchLocations>
//	        <DEPTID xmlns="">37000</DEPTID>
//	        <DESCR xmlns="">Rooney Ranch Elementary</DESCR>
//	        <JC_TM_SCHL_TYPE xmlns="">JC_TM_ELEMENTARYSCHLS</JC_TM_SCHL_TYPE>
//	        <ADDRESS1 xmlns="">2200 S. Coors St.</ADDRESS1>
//	        <ADDRESS2 xmlns=""/>
//	        <CITY xmlns="">Lakewood</CITY>
//	        <STATE xmlns="">CO</STATE>
//	        <POSTAL xmlns="">80228</POSTAL>
//	        <URL xmlns="">http://www.jeffcopublicschools.org/schools/profiles/elementary/index.php</URL>
//	      </TeacherMatchLocations>
//	    </TestDB>
//	  </soap:Body>
//	</soap:Envelope>
	@RequestMapping(value="/service/locationUpdate.do", method=RequestMethod.POST)
	public String locationUpdate(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/locationUpdate.do");
		
		String strInput="";
		
		String payloadRequest = readStringFromInput(request);

		HashMap valueFromXML = getValueMapFromXML(payloadRequest);

		try 
		{
			PrintWriter out = response.getWriter();
			
			response.setContentType("text/text");
			
			out.println(nullCheckAndPrint("authKey", valueFromXML));
			out.println(nullCheckAndPrint("action", valueFromXML));
			out.println(nullCheckAndPrint("locationCode", valueFromXML));
			out.println(nullCheckAndPrint("schoolName", valueFromXML));
			out.println(nullCheckAndPrint("schooltype", valueFromXML));
			out.println(nullCheckAndPrint("noOfTeachers", valueFromXML));
			out.println(nullCheckAndPrint("noOfStudents", valueFromXML));
			out.println(nullCheckAndPrint("schoolSite", valueFromXML));
			out.println(nullCheckAndPrint("address", valueFromXML));
			out.println(nullCheckAndPrint("city", valueFromXML));
			out.println(nullCheckAndPrint("zip", valueFromXML));
			out.println(nullCheckAndPrint("state", valueFromXML));
			out.println(nullCheckAndPrint("geography", valueFromXML));
			out.println(nullCheckAndPrint("region", valueFromXML));
			out.println(nullCheckAndPrint("phoneNumber", valueFromXML));
			out.println(nullCheckAndPrint("alternatePhone", valueFromXML));
			out.println(nullCheckAndPrint("faxNumber", valueFromXML));
			out.println(nullCheckAndPrint("website", valueFromXML));
			out.println(nullCheckAndPrint("geoZone", valueFromXML));
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{			
		}
		
		return null;
	}
	
	private String nullCheckAndPrint(String value, HashMap valueFromXML)
	{
		return value + ": " + (valueFromXML.get(value) == null ? "[]" : ("[" + valueFromXML.get(value) + "]"));
	}

	public HashMap getValueMapFromXML(String payloadRequest) 
	{
		Document doc = null;
		Element root = null;	
    	HashMap valueFromXML = new HashMap();
		
    	String [] eachLineArray = payloadRequest.split("\n");
		StringBuilder sb = new StringBuilder();

    	for (String line : eachLineArray)
    	{
			if (!(line.indexOf("<?xml") > -1))
				   sb.append(line + "\n");
    	}

		try 
		{

			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

			try 
			{
				//Using factory get an instance of document builder
				DocumentBuilder docBuilder = null;
				
				docBuilder = builderFactory.newDocumentBuilder();
				//parse using builder to get DOM representation of the XML file
				InputSource is = new InputSource();
			    is.setCharacterStream(new StringReader(sb.toString()));
				doc = docBuilder.parse(is);
				NodeList TeacherMatchLocations = doc.getElementsByTagName("TeacherMatchLocations");
				
			    if (TeacherMatchLocations.getLength() > 0) 
			    {
			        Element element = (Element)TeacherMatchLocations.item(0);
			        
			        // valueFromXML.put("authKey", element.getElementsByTagName("authKey").item(0).getTextContent());
			        valueFromXML.put("locationCode", element.getElementsByTagName("DEPTID").item(0).getTextContent());
			        valueFromXML.put("schoolName", element.getElementsByTagName("DESCR").item(0).getTextContent());
			        valueFromXML.put("schooltype", element.getElementsByTagName("JC_TM_SCHL_TYPE").item(0).getTextContent());
			        valueFromXML.put("address", element.getElementsByTagName("ADDRESS1").item(0).getTextContent());
			        valueFromXML.put("city", element.getElementsByTagName("CITY").item(0).getTextContent());
			        valueFromXML.put("zip", element.getElementsByTagName("POSTAL").item(0).getTextContent());
			        valueFromXML.put("state", element.getElementsByTagName("STATE").item(0).getTextContent());
			        valueFromXML.put("website", element.getElementsByTagName("URL").item(0).getTextContent());
			    }
			    
			}
			catch(ParserConfigurationException pce) 
			{
				pce.printStackTrace();
			}
			catch(SAXException se) 
			{
				se.printStackTrace();
			}
			catch(IOException ioe) 
			{
				ioe.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			UtilityAPI.writeXMLErrors(100011,"",null, doc, root);

		}
		
		return valueFromXML;
	}

	private String readStringFromInput(HttpServletRequest request) 
	{
		String payloadRequest = "";
		StringBuilder sb = new StringBuilder();

		try 
		{
			BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
			String line = null;
			
			while ((line = in.readLine()) != null) 
			{
				   sb.append(line);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("Error Parsing: - ");
		}
		
		payloadRequest = sb.toString();
		System.err.println("payloadRequest::= "+payloadRequest);
		return payloadRequest;
	}

	@RequestMapping(value="/service/adminUpdate.do", method=RequestMethod.GET)
	public String adminUpdate(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/adminUpdate.do");
		
		String strInput="";
		
		try 
		{
			
			PrintWriter out = response.getWriter();
			
			response.setContentType("text/text");
			strInput = request.getParameter("strInput")==null?"":request.getParameter("strInput");
			out.println(Utility.decodeBase64(strInput));
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{			
		}
		
		return null;
	}
	
	
	// Do not run on any server
	/*@RequestMapping(value="/ns/updatenormbyt.do", method=RequestMethod.GET)
	public String updateNormscoreByT(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("ns/updatenormbyt.do");
		try {
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			CGReportService crs = new CGReportService();
			Connection con1 = crs.getConnection();
			
			String sTeacherId=request.getParameter("tid");
			System.out.println("sTeacherId "+sTeacherId);
			
			List<Integer> lstTeacherIds=new ArrayList<Integer>();
			List<TeacherDetail> teacherDetails=new ArrayList<TeacherDetail>();
			if(sTeacherId!=null && !sTeacherId.equals(""))
			{
				String arr[]=sTeacherId.split(",");
				if(arr.length >0)
				{
					for(int i=0; i<arr.length; i++)
					{
						lstTeacherIds.add(Integer.parseInt(arr[i].trim()));
					}
				}
			}
			if(lstTeacherIds!=null && lstTeacherIds.size()>0)
				teacherDetails=teacherDetailDAO.findByTeacherIds(lstTeacherIds);
			
			System.out.println("teacherDetails "+teacherDetails.size());
			
			int iTeacherId=Integer.parseInt(sTeacherId);
			Integer techersList[]={iTeacherId};
			teacherDetails=teacherDetailDAO.getTeachersHQL(techersList);
			
			if(teacherDetails!=null && teacherDetails.size()>0)
			{
				System.out.println("teacherDetails "+teacherDetails.size());
				
				List<ScoreLookup> scoreLookups = new ArrayList<ScoreLookup>();
				Map<String,ScoreLookup> scoreLookupMapDomain = new HashMap<String, ScoreLookup>();
				Map<String,ScoreLookup> scoreLookupMapCompetency = new HashMap<String, ScoreLookup>();
				scoreLookups = scoreLookupDAO.findScoreLookup(1);
				for (ScoreLookup scoreLookup : scoreLookups) {
					scoreLookupMapDomain.put(scoreLookup.getDomainMaster().getDomainId()+"###"+scoreLookup.getTotalscore(), scoreLookup);
					//System.out.println(scoreLookup.getDomainMaster().getDomainId()+"###"+scoreLookup.getTotalscore()+"  "+scoreLookup.getScorelookupId());
				}
				scoreLookups = scoreLookupDAO.findScoreLookup(2);
				for (ScoreLookup scoreLookup : scoreLookups) {
					scoreLookupMapCompetency.put(scoreLookup.getCompetencyMaster().getCompetencyId()+"###"+scoreLookup.getTotalscore(), scoreLookup);
				}
				System.out.println("************* 2 ****************");
				SessionFactory sessionFactory =  domainMasterDAO.getSessionFactory();
				StatelessSession session1 = sessionFactory.openStatelessSession();
				
			 	List<RawDataForDomain> RawDataForDomain = rawDataForDomainDAO.findByTeacher(teacherDetails);
			 	System.out.println("************* 3 **************** RawDataForDomain "+RawDataForDomain.size());
			 	for (RawDataForDomain rawDataForDomain2 : RawDataForDomain) {
					AssessmentDomainScore assessmentDomainScore = new AssessmentDomainScore();
					assessmentDomainScore.setDomainMaster(rawDataForDomain2.getDomainMaster());
					assessmentDomainScore.setTeacherDetail(rawDataForDomain2.getTeacherDetail());
					assessmentDomainScore.setRawDataForDomain(rawDataForDomain2);
					assessmentDomainScore.setAssessmentDetail(rawDataForDomain2.getAssessmentDetail());
					assessmentDomainScore.setAssessmentType(rawDataForDomain2.getAssessmentType());
					assessmentDomainScore.setAssessmentTakenCount(rawDataForDomain2.getAssessmentTakenCount());
					assessmentDomainScore.setCreatedDateTime(rawDataForDomain2.getCreatedDateTime());
					ScoreLookupMaster scoreLookupMaster = new ScoreLookupMaster();
					scoreLookupMaster.setLookupId(1);
					assessmentDomainScore.setScoreLookupMaster(scoreLookupMaster);
					ScoreLookup scoreLookup = scoreLookupMapDomain.get(rawDataForDomain2.getDomainMaster().getDomainId()+"###"+rawDataForDomain2.getScore());
					if(scoreLookup!=null)
					{
						assessmentDomainScore.setPercentile(scoreLookup.getPercentile());
						
						assessmentDomainScore.setZscore(scoreLookup.getZscore());
						assessmentDomainScore.setTscore(scoreLookup.getTscore());
						assessmentDomainScore.setNcevalue(scoreLookup.getNcevalue());
						assessmentDomainScore.setNormscore(scoreLookup.getNormscore());
						assessmentDomainScore.setRitvalue(scoreLookup.getRitvalue());
						session1.insert(assessmentDomainScore);
					}
				}
			 	System.out.println("************* 4 ****************");
			 	System.out.println("Domain wise data Done");
			 	
				StatelessSession session2 = sessionFactory.openStatelessSession();
				
			 	List<RawDataForCompetency> rawDataForCompetencies = rawDataForCompetencyDAO.findAllInCurrentYearByTeacher(teacherDetails);
			 	System.out.println("************* 5 **************** rawDataForCompetencies "+rawDataForCompetencies.size());
			 	for (RawDataForCompetency rawDataforCompetency : rawDataForCompetencies) {
					AssessmentCompetencyScore assessmentCompetencyScore = new AssessmentCompetencyScore();
					assessmentCompetencyScore.setCompetencyMaster(rawDataforCompetency.getCompetencyMaster());
					assessmentCompetencyScore.setTeacherDetail(rawDataforCompetency.getTeacherDetail());
					assessmentCompetencyScore.setRawDataForCompetency(rawDataforCompetency);
					assessmentCompetencyScore.setAssessmentDetail(rawDataforCompetency.getAssessmentDetail());
					assessmentCompetencyScore.setAssessmentType(rawDataforCompetency.getAssessmentType());
					assessmentCompetencyScore.setAssessmentTakenCount(rawDataforCompetency.getAssessmentTakenCount());
					assessmentCompetencyScore.setCreatedDateTime(rawDataforCompetency.getCreatedDateTime());
					ScoreLookupMaster scoreLookupMaster = new ScoreLookupMaster();
					scoreLookupMaster.setLookupId(1);
					assessmentCompetencyScore.setScoreLookupMaster(scoreLookupMaster);
					ScoreLookup scoreLookup = scoreLookupMapCompetency.get(rawDataforCompetency.getCompetencyMaster().getCompetencyId()+"###"+rawDataforCompetency.getScore());
					if(scoreLookup!=null)
					{
						assessmentCompetencyScore.setPercentile(scoreLookup.getPercentile());
						
						assessmentCompetencyScore.setZscore(scoreLookup.getZscore());
						assessmentCompetencyScore.setTscore(scoreLookup.getTscore());
						assessmentCompetencyScore.setNcevalue(scoreLookup.getNcevalue());
						assessmentCompetencyScore.setNormscore(scoreLookup.getNormscore());
						assessmentCompetencyScore.setRitvalue(scoreLookup.getRitvalue());
						session2.insert(assessmentCompetencyScore);
					}
				}
			 	System.out.println("************* 6 ****************");
			 	System.out.println("Competency wise data Done");
			 	
			 	List assessmentDomainData = new ArrayList();
				assessmentDomainData = assessmentDomainScoreDAO.calculateCandidatesNormScore(teacherDetails);
				System.out.println("************* 7 **************** assessmentDomainData "+assessmentDomainData.size());
				System.out.println("lstRawData22:: "+assessmentDomainData.size());
				CGReportService cGReportService = new CGReportService();
				
				 if(tmpPercentileWiseZScoreList==null)
					{
						tmpPercentileWiseZScoreList = tmpPercentileWiseZScoreDAO.findByCriteria(Order.asc("zScore"));
						System.out.println("?????????????????????????????????????? ");
					}
					if(pztList==null)
					{
						pztList = cGReportService.populateZScoreMap(tmpPercentileWiseZScoreList);
						System.out.println("?????????????????????????????????????? ");
					}
					
					StatelessSession session3 = sessionFactory.openStatelessSession();
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
					Date date = null;
					
					for(Object oo: assessmentDomainData){
						Object obj[] = (Object[])oo;
						System.out.println("obj[0]:: "+obj[0]);
						System.out.println("obj[1]:: "+obj[1]);
						System.out.println("obj[1]:: "+obj[2]);
						Double normscoreD = (Double)obj[0];
						//Integer normscore = Math.round(normscoreD);
						Integer normscore = Integer.valueOf((int) Math.round(normscoreD));
						TeacherNormScore teacherNormScore = new TeacherNormScore();
						teacherNormScore.setTeacherNormScore( normscore);
						//double percentile = ((normscore-50)*3+50);
						
						double percentile=cGReportService.getPercentile(pztList,normscore);
						 
						teacherNormScore.setPercentile(percentile);
						Object[] ob =cGReportService.getDeciles(percentile);
						teacherNormScore.setDecileColor(""+ob[0]);
						//System.out.println(ob.length+" "+(Double)ob[1]+" " +(Double)ob[2]);
						teacherNormScore.setMinDecileValue((Double)ob[1]);
						teacherNormScore.setMaxDecileValue((Double)ob[2]);
						TeacherDetail teacherDetail = new TeacherDetail();
						teacherDetail.setTeacherId(Integer.parseInt(String.valueOf(obj[1])));
						teacherNormScore.setTeacherDetail(teacherDetail);
						date = format.parse(obj[2].toString());
						teacherNormScore.setCreatedDateTime(date);
						session3.insert(teacherNormScore);
					}
					System.out.println("************* 8 ****************");
			}
			else
			{
				System.out.println("No Teacher Ids ");
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
			System.out.println("************* End ****************");
		}
		return null;
	}*/

	// Do not run on any server
	/*@RequestMapping(value="/ns/updateRawData.do", method=RequestMethod.GET)
	public String updateRawDataByTeacher(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("/ns/updateRawData.do");
		try {
			String sTeacherId=request.getParameter("tid");
			String isNormScore=request.getParameter("isNormScore");
			System.out.println("sTeacherId "+sTeacherId);
			
			List<Integer> listTeacherIds=new ArrayList<Integer>();
			if(sTeacherId!=null && !sTeacherId.equals(""))
			{
				String arr[]=sTeacherId.split(",");
				if(arr.length >0)
					for(int i=0; i<arr.length; i++)
						listTeacherIds.add(Integer.parseInt(arr[i].trim()));
			}
			
			System.out.println("listTeacherIds.size() == "+listTeacherIds.size());
			
			List<TeacherDetail> teacherDetailList = teacherDetailDAO.findByCriteria(Restrictions.in("teacherId", listTeacherIds));
			
			if(teacherDetailList!=null && teacherDetailList.size()>0)
				reportService.saveRawDataOfTeacher(teacherDetailList);
			
			if(isNormScore!=null && isNormScore.equals("1"))
				updateNormscoreByT(null, request, response);
			
			System.out.println("************* Success ****************");
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
			System.out.println("************* End ****************");
		}
		return null;
	}*/
	
	
	/*@RequestMapping(value="/service/updateCandidateNorm.do", method=RequestMethod.GET)
	public String updateCandidateNormscore(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/updateNorm.do");
		try {
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			CGReportService crs = new CGReportService();
			Connection con1 = crs.getConnection();
			
			SessionFactory sessionFactory =  domainMasterDAO.getSessionFactory();
		 	List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
		 	String teachers = "383,18289,22153,26387,26484,39495,40280,41533,42768,44485,48911,49524,50439,50731,51405,52274,52287,52822,57279,57930,59939,61038,61088,62492,63507,65200,65396,65691,67289,67905,68140,68162,68877,69412,70639,71010,71580,71624,71928,71942,71998,72247,72435,74323,75520,75910,76096,76164,76723,76771,77187,77317,78035,78709,79355,79717,80692,81592,81798,81982,81987,82160,82301,82603,82606,83522,83588,83659,83750,83778,84265,84522,84578,84605,84672,84692,84758,84772,84794,84799,85012,85062,85194,85579,85621,85665,85687,85693,85723,85784,85807,86005,86006,86079,86219,86290,86293,86390,86428,86508,86520,86525,86542,86573,86612,86669,86682,86732,86773,86795,86806,86809,86849,86942,86964,87015,87016,87019,87027,87089,87100,87118,87129,87159,87171,87185,87255,87258,87262,87272,87279,87368,87408,87456,87510,87516,87518,87548,87554,87564,87565,87599,87612,87615,87618,87665,87688,87697,87709,87744,87745,87746,87747,87777,87797,87815,87819,87854,87920,87928,87933,87934,87937,87958,87984,88003,88008,88024,88026,88043,88059,88062,88085,88093,88094,88097,88117,88126,88128,88130,88132,88137,88138,88139,88141,88144,88150,88153,88155,88158,88162,88164,88174,88187,88190,88197,88199,88207,88208,88209,88215,88220,88222,88227,88229,88230,88231,88236,88255,88256,88257,88259,88265,88269,88278,88282,88283,88286,88288,88293,88295,88301,88305,88310,88312,88313,88319,88320,88325,88326,88336,88342,88350,88359,88363,88366,88367,88374,88375,88381,88384,88395,88405,88407,88409,88410,88411,88417,88423,88434,88443,88445,88455,88460,88463,88465,88467,88470,88474,88480,88506,88515,88520,88522,88525,88529,88533,88535,88544,88546,88553,88555,88556,88559,88560,88562,88564,88570,88582,88585,88594,88604,88605,88612,88615,88618,88638,88642,88644,88648,88655,88663,88664,88667,88688,88689,88697,88698,88702,88703,88704,88742,88747,88751,88753,88754,88756,88759,88763,88772,88775,88776,88777,88785,88807,88826,88827,88830,88833,88837,88841,88842,88847,88854,88858,88859,88861,88863,88865,88868,88872,88873,88875,88888,88892,88908,88925,88927,88945,88946,88951,88953,88954,88955,88964,88971,88974,88981,88982,88985";
		 	String [] teachs = teachers.split(",");
		 	for (String string : teachs) {
		 		TeacherDetail td = new TeacherDetail();
		 		td.setTeacherId(Integer.parseInt(string));
		 		teacherDetails.add(td);
			}
		 	System.out.println("ssss: "+teacherDetails.size());
		 	ScoreLookupMaster scoreLookupMaster = new ScoreLookupMaster();
		 	scoreLookupMaster.setLookupId(1);
		 	
		 	Map<Integer,TeacherNormScore> teachernorMap = new HashMap<Integer, TeacherNormScore>();
		 	List<TeacherNormScore> teacherNormscorList =  teacherNormScoreDAO.findNormScoreByTeacherList(teacherDetails);
		 	for (TeacherNormScore teacherNormScore : teacherNormscorList) {
		 		teachernorMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
			}
		 	List assessmentDomainData = new ArrayList();
			
		 	assessmentDomainData = assessmentDomainScoreDAO.calculateCandidatesNormScore(teacherDetails,scoreLookupMaster);
			
			//System.out.println("lstRawData22:: "+assessmentDomainData.size());
			CGReportService cGReportService = new CGReportService();
			
			 if(tmpPercentileWiseZScoreList==null)
				{
					tmpPercentileWiseZScoreList = tmpPercentileWiseZScoreDAO.findByCriteria(Order.asc("zScore"));
					System.out.println("?????????????????????????????????????? ");
				}
				if(pztList==null)
				{
					pztList = cGReportService.populateZScoreMap(tmpPercentileWiseZScoreList);
					System.out.println("?????????????????????????????????????? ");
				}
				
				StatelessSession session3 = sessionFactory.openStatelessSession();
				
				for(Object oo: assessmentDomainData){
					Object obj[] = (Object[])oo;
					System.out.println("obj[0]:: "+obj[0]);
					System.out.println("obj[1]:: "+obj[1]);
					Double normscoreD = (Double)obj[0];
					//Integer normscore = Math.round(normscoreD);
					Integer normscore = Integer.valueOf((int) Math.round(normscoreD));
					TeacherNormScore teacherNormScore = teachernorMap.get(Integer.parseInt(String.valueOf(obj[1])));
					System.out.println("ssssssssss: "+teacherNormScore.getTeacherNorScoreId());
					teacherNormScore.setTeacherNormScore( normscore);
					//double percentile = ((normscore-50)*3+50);
					
					double percentile=cGReportService.getPercentile(pztList,normscore);
					
					teacherNormScore.setPercentile(percentile);
					Object[] ob =cGReportService.getDeciles(percentile);
					teacherNormScore.setDecileColor(""+ob[0]);
					//System.out.println(ob.length+" "+(Double)ob[1]+" " +(Double)ob[2]);
					teacherNormScore.setMinDecileValue((Double)ob[1]);
					teacherNormScore.setMaxDecileValue((Double)ob[2]);
					//TeacherDetail teacherDetail = new TeacherDetail();
					
					//teacherNormScore.setCreatedDateTime(new Date());
					session3.update(teacherNormScore);
				}
				
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}*/
	
	@RequestMapping(value="/service/updateData.do", method=RequestMethod.GET)
	public String updateData(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("::: service/updateData.do :::");
		try {
			SessionFactory sessionFactory =  teacherNormScoreDAO.getSessionFactory();
			StatelessSession statelessSession = sessionFactory.openStatelessSession();
			Transaction transaction = statelessSession.beginTransaction();
			
		 	List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
		 	
		 	String teacher = Utility.getTeachers();
		 	
		 	String [] teachers = teacher.split(",");
		 	for (String string : teachers) {
		 		TeacherDetail teacherDetail = new TeacherDetail();
		 		teacherDetail.setTeacherId(Integer.parseInt(string));
		 		teacherDetailList.add(teacherDetail);
			}
		 	System.out.println("teacherDetailList.size() : "+teacherDetailList.size());
		 	
		 	AssessmentDetail assessmentDetail = assessmentDetailDAO.findById(110, false, false);
		 	
		 	List<TeacherNormScore> teacherNormScoreList =  teacherNormScoreDAO.findByCriteria(Restrictions.in("teacherDetail", teacherDetailList));
		 	Vector<TeacherNormScore> teacherNormScoreVector = new Vector<TeacherNormScore>(teacherNormScoreList);
		 	System.out.println("teacherNormScoreList.size() : "+teacherNormScoreList.size());
		 	
		 	List<RawDataForDomain> rawDataForDomainList = rawDataForDomainDAO.getRawDataForDomain(teacherDetailList, assessmentDetail, 1, 1);
		 	Vector<RawDataForDomain> rawDataForDomainVector = new Vector<RawDataForDomain>(rawDataForDomainList);
		 	System.out.println("rawDataForDomainList.size() : "+rawDataForDomainList.size());
		 	
		 	List<RawDataForCompetency> rawDataForCompetencyList = rawDataForCompetencyDAO.getRawDataForCompetency(teacherDetailList, assessmentDetail, 1, 1);
		 	Vector<RawDataForCompetency> rawDataForCompetencyVector = new Vector<RawDataForCompetency>(rawDataForCompetencyList);
		 	System.out.println("rawDataForCompetencyList.size() : "+rawDataForCompetencyList.size());
		 	
		 	List<TeacherAssessmentdetail> teacherAssessmentDetailList = teacherAssessmentDetailDAO.getTeacherAssessmentdetails(teacherDetailList, assessmentDetail, 1, 1);
		 	System.out.println("teacherAssessmentDetailList.size() : "+teacherAssessmentDetailList.size());
		 	
		 	List<AssessmentDomainScore> assessmentDomainScoreList = assessmentDomainScoreDAO.getAssessmentDomainScore(teacherDetailList, assessmentDetail, 1, 1);
		 	Vector<AssessmentDomainScore> assessmentDomainScoreVector = new Vector<AssessmentDomainScore>(assessmentDomainScoreList);
		 	System.out.println("assessmentDomainScoreList.size() : "+assessmentDomainScoreList.size());
		 	
		 	List<AssessmentCompetencyScore> assessmentCompetencyScoreList = assessmentCompetencyScoreDAO.getAssessmentCompetencyScore(teacherDetailList, assessmentDetail, 1, 1);
		 	Vector<AssessmentCompetencyScore> assessmentCompetencyScoreVector = new Vector<AssessmentCompetencyScore>(assessmentCompetencyScoreList);
		 	System.out.println("assessmentCompetencyScoreList.size() : "+assessmentCompetencyScoreList.size());
		 	
		 	Map<Integer, TeacherAssessmentdetail> teacherAssessmentDetailMap = new HashMap<Integer, TeacherAssessmentdetail>();
		 	Map<String, RawDataForDomain> rawDataForDomainMap = new HashMap<String, RawDataForDomain>();
		 	Map<String, RawDataForCompetency> rawDataForCompetencyMap = new HashMap<String, RawDataForCompetency>();
		 	
		 	System.out.println("Start1");
		 	for(TeacherAssessmentdetail teacherAssessmentdetail : teacherAssessmentDetailList)
		 		teacherAssessmentDetailMap.put(teacherAssessmentdetail.getTeacherDetail().getTeacherId(), teacherAssessmentdetail);
		 	
		 	System.out.println("Start2");
		 	for(RawDataForDomain rawDataForDomain : rawDataForDomainList)
		 		rawDataForDomainMap.put(rawDataForDomain.getTeacherDetail().getTeacherId()+"#"+rawDataForDomain.getDomainMaster().getDomainId(), rawDataForDomain);
		 	
		 	System.out.println("Start3");
		 	for(RawDataForCompetency rawDataForCompetency : rawDataForCompetencyList)
		 		rawDataForCompetencyMap.put(rawDataForCompetency.getTeacherDetail().getTeacherId()+"#"+rawDataForCompetency.getCompetencyMaster().getCompetencyId(), rawDataForCompetency);
		 	
		 	System.out.println("Start4");
		 	for(TeacherNormScore teacherNormScore : teacherNormScoreVector){
		 		teacherNormScore.setTeacherAssessmentdetail(teacherAssessmentDetailMap.get(teacherNormScore.getTeacherDetail().getTeacherId()));
		 		statelessSession.update(teacherNormScore);
		 	}
		 	
		 	System.out.println("Start5");
		 	for(RawDataForDomain rawDataForDomain : rawDataForDomainVector){
		 		rawDataForDomain.setTeacherAssessmentdetail(teacherAssessmentDetailMap.get(rawDataForDomain.getTeacherDetail().getTeacherId()));
		 		statelessSession.update(rawDataForDomain);
		 	}
		 	
		 	System.out.println("Start6");
		 	for(RawDataForCompetency rawDataForCompetency : rawDataForCompetencyVector){
		 		rawDataForCompetency.setTeacherAssessmentdetail(teacherAssessmentDetailMap.get(rawDataForCompetency.getTeacherDetail().getTeacherId()));
		 		statelessSession.update(rawDataForCompetency);
		 	}
		 	
		 	System.out.println("Start7");
		 	for(AssessmentDomainScore assessmentDomainScore : assessmentDomainScoreVector){
		 		assessmentDomainScore.setRawDataForDomain(rawDataForDomainMap.get(assessmentDomainScore.getTeacherDetail().getTeacherId()+"#"+assessmentDomainScore.getDomainMaster().getDomainId()));
		 		statelessSession.update(assessmentDomainScore);
		 	}
		 	
		 	System.out.println("Start8");
		 	for(AssessmentCompetencyScore assessmentCompetencyScore : assessmentCompetencyScoreVector){
		 		assessmentCompetencyScore.setRawDataForCompetency(rawDataForCompetencyMap.get(assessmentCompetencyScore.getTeacherDetail().getTeacherId()+"#"+assessmentCompetencyScore.getCompetencyMaster().getCompetencyId()));
		 		statelessSession.update(assessmentCompetencyScore);
		 	}
		 	
			transaction.commit();
			statelessSession.close();
			System.out.println("data successfully uploded");
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}
	
	
	@RequestMapping(value="/service/updateTeacherData.do", method=RequestMethod.GET)
	public String updateTeacherData(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		
		 try {
			PrintWriter out = response.getWriter();
				response.setContentType("text/html");
				
				PreparedStatement  pstmt=null;
				CGReportService crs = new CGReportService();
				Connection con = crs.getConnection();
				//con.setAutoCommit(true);
				System.out.println("Start1");
				pstmt = con.prepareStatement("update teacherassessmentdetails set assessmentTakenCount=1 where assessmentType=1 and teacherId in("+Utility.getTeachers()+")");
				pstmt.executeUpdate();
				System.out.println("Start2");
				pstmt = con.prepareStatement("update teacherassessmentattempt set assessmentTakenCount=1 where assessmentId=110 and teacherId in("+Utility.getTeachers()+")");
				pstmt.executeUpdate();
				System.out.println("Start3");
				pstmt = con.prepareStatement("update teacherassessmentstatus set assessmentTakenCount=1 where assessmentType=1 and teacherId in("+Utility.getTeachers()+")");
				pstmt.executeUpdate();
				System.out.println("Start4");
				pstmt = con.prepareStatement("update rawdatafordomain set assessmentId=110,assessmentType=1,assessmentTakenCount=1 where teacherId in("+Utility.getTeachers()+")");
				pstmt.executeUpdate();
				System.out.println("Start5");
				pstmt = con.prepareStatement("update rawdataforcompetency set assessmentId=110,assessmentType=1,assessmentTakenCount=1 where teacherId in("+Utility.getTeachers()+")");
				pstmt.executeUpdate();
				System.out.println("Start6");
				pstmt = con.prepareStatement("update assessmentdomainscore set assessmentId=110,assessmentType=1,assessmentTakenCount=1 where teacherId in("+Utility.getTeachers()+")");
				pstmt.executeUpdate();
				System.out.println("Start7");
				pstmt = con.prepareStatement("update assessmentcompetencyscore set assessmentId=110,assessmentType=1,assessmentTakenCount=1 where teacherId in("+Utility.getTeachers()+")");
				pstmt.executeUpdate();
				
				
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	@RequestMapping(value="/service/setNotifyAllFlagValue.do", method=RequestMethod.GET)
	public String setNotifyAllFlagValue(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("service/setNotifyAllFlagValue.do");
		try {
			List<StatusWiseAutoEmailSend> autoEmailSends = new ArrayList<StatusWiseAutoEmailSend>();
			List<SecondaryStatus> allSecondaryStatusList = new ArrayList<SecondaryStatus>();
			Map<String,String> aEmailSDistrictList = new HashMap<String,String>();
			Map<String,String> mapAllSSList = new HashMap<String,String>();
			Criterion c1 = Restrictions.eq("status", "A");
			Criterion c2 = Restrictions.isNotNull("secondaryStatusId");
			
			autoEmailSends = statusWiseAutoEmailSendDAO.findByCriteria(c2);
			allSecondaryStatusList = secondaryStatusDAO.findByCriteria(c1);
			
			for(StatusWiseAutoEmailSend swaes:autoEmailSends){
				aEmailSDistrictList.put(swaes.getDistrictId().getDistrictId()+"#"+swaes.getSecondaryStatusId(),swaes.getSecondaryStatusId()+"");
			}
			
			for(SecondaryStatus ss:allSecondaryStatusList){
				if(ss.getDistrictMaster()!=null)
					mapAllSSList.put(ss.getDistrictMaster().getDistrictId()+"#"+ss.getSecondaryStatusId(), ss.getSecondaryStatusName());
			}
			
			//Get district and secondary status name
			for (Map.Entry<String, String> entry : aEmailSDistrictList.entrySet())
			{
			    System.out.println(entry.getKey() + "/" + entry.getValue());
			    
			    String s[] = entry.getKey().split("#");
 			    
			    DistrictMaster districtMaster = districtMasterDAO.findByDistrictId(s[0]);
			    
			    Criterion c3 = Restrictions.eq("districtMaster", districtMaster);
			    
			    Criterion c5 = Restrictions.eq("secondaryStatusName",mapAllSSList.get(entry.getKey()));
			    
			    List<SecondaryStatus> ssListBydIDAndssName = secondaryStatusDAO.findByCriteria(c5,c3);
			    
			    for(SecondaryStatus ss:ssListBydIDAndssName){
			    	List<JobCategoryWiseStatusPrivilege> jCWSPList = jobCategoryWiseStatusPrivilegeDAO.getFilterBySecondaryStatus(districtMaster, ss);
			    	System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>> jCWSPList >>>>>>>> "+jCWSPList.size()+" ss.getSecondaryStatus() :: "+ss.getSecondaryStatusId());
			    	if(jCWSPList!=null && jCWSPList.size()>0){
			    		jCWSPList.get(0).setAutoNotifyAll(true);
				    	jobCategoryWiseStatusPrivilegeDAO.makePersistent(jCWSPList.get(0));
			    	}
			    }
			    System.out.println("------------------------------------------------------------------------------------------------");
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return null;
	}

	/**
	 * @author Amit Chaudhary
	 * @param map
	 * @param request
	 * @param response
	 * @details TPL-2515
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/service/updateByDomain.do", method=RequestMethod.GET)
	public String updateByDomain(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println(":::::::::::::::: TestPageController /service/updateByDomain.do ::::::::::::::::");
		try {
			response.setContentType("text/html");
			
			String domain = request.getParameter("domainId");
//			String tid = request.getParameter("tid");
			Integer domainId = null;
//			System.out.println("tid :: "+tid);
			System.out.println("domain :: "+domain);
			
			if(domain!=null && !domain.equals(""))
				domainId = Integer.parseInt(domain);
			
			DomainMaster domainMaster = null;
			if(domainId!=null)
				domainMaster = domainMasterDAO.findById(domainId, false, false);
			
//			List<Integer> teacherIdList = new ArrayList<Integer>();
			List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
			
//			if(tid!=null && !tid.equals("")){
//				String arr[] = tid.split(",");
//				if(arr.length >0){
//					for(int i=0; i<arr.length; i++)
//						teacherIdList.add(Integer.parseInt(arr[i].trim()));
//				}
//			}
			
			String teacher = Utility.getTeachers();
		 	
		 	String [] teachers = teacher.split(",");
		 	for (String string : teachers) {
		 		TeacherDetail teacherDetail = new TeacherDetail();
		 		teacherDetail.setTeacherId(Integer.parseInt(string));
		 		teacherDetailList.add(teacherDetail);
			}
		 	System.out.println("teacherDetailList.size() : "+teacherDetailList.size());
			
//			if(teacherIdList!=null && teacherIdList.size()>0)
//				teacherDetailList = teacherDetailDAO.findByTeacherIds(teacherIdList);
			
			if(domainMaster!=null && teacherDetailList!=null && teacherDetailList.size()>0)
			{
				System.out.println("domainMaster :: "+domainMaster);
				System.out.println("teacherDetailList.size() :: "+teacherDetailList.size());
				
				SessionFactory sessionFactory =  domainMasterDAO.getSessionFactory();
				StatelessSession statelessSession1 = sessionFactory.openStatelessSession();
				Transaction transaction = statelessSession1.beginTransaction();
				
				List teacherAnswerList = null;
				Map<Integer, Object> rawDataForDomainScoreMap = new HashMap<Integer, Object>();
				TeacherDetail teacherDetail = null;
				teacherAnswerList = teacherAnswerDetailDAO.findScoreByDomainAndTeacherList(teacherDetailList, domainMaster);
				System.out.println("************* 1 ****************");
				System.out.println("teacherAnswerList.size() : "+teacherAnswerList.size());
				for(Object object : teacherAnswerList){
					Object obj[] = (Object[])object;
					teacherDetail = (TeacherDetail) obj[0];
					rawDataForDomainScoreMap.put(teacherDetail.getTeacherId(), obj);
				}
				System.out.println("************* 2 ****************");
				System.out.println("rawDataForDomainScoreMap.size() : "+rawDataForDomainScoreMap.size());
				
				List<RawDataForDomain> rawDataForDomainList = new ArrayList<RawDataForDomain>();
				rawDataForDomainList = rawDataForDomainDAO.findByDomainAndTeacherList(domainMaster, teacherDetailList);
				System.out.println("************* 3 ****************");
				System.out.println("rawDataForDomainList.size() : "+rawDataForDomainList.size());
				for(RawDataForDomain rawDataForDomain : rawDataForDomainList){
					//System.out.println("rawDataForDomain.getTeacherDetail().getTeacherId() : "+rawDataForDomain.getTeacherDetail().getTeacherId());
					Object object[] = (Object[]) rawDataForDomainScoreMap.get(rawDataForDomain.getTeacherDetail().getTeacherId());
					if(object!=null){
						rawDataForDomain.setScore(new Double(""+object[1]));
						rawDataForDomain.setMaxMarks(new Double(""+object[2]));
					}
					statelessSession1.update(rawDataForDomain);
				}
				System.out.println("************* 4 ****************");
			 	System.out.println("rawDataForDomain data update done");
			 	
				List<ScoreLookup> scoreLookupList = new ArrayList<ScoreLookup>();
				Map<String,ScoreLookup> scoreLookupDomainMap = new HashMap<String, ScoreLookup>();
				//Map<String,ScoreLookup> scoreLookupCompetencyMap = new HashMap<String, ScoreLookup>();
				
				scoreLookupList = scoreLookupDAO.findScoreLookup(1,1);
				System.out.println("************* 5 ****************");
				System.out.println("scoreLookupList.size() D : "+scoreLookupList.size());
				for (ScoreLookup scoreLookup : scoreLookupList)
					scoreLookupDomainMap.put(scoreLookup.getDomainMaster().getDomainId()+"###"+scoreLookup.getTotalscore(), scoreLookup);
				System.out.println("************* 6 ****************");
				System.out.println("scoreLookupDomainMap.size() : "+scoreLookupDomainMap.size());
				
				/*scoreLookupList = scoreLookupDAO.findScoreLookup(2);
				System.out.println("scoreLookupList.size() C : "+scoreLookupList.size());
				for (ScoreLookup scoreLookup : scoreLookupList)
					scoreLookupCompetencyMap.put(scoreLookup.getCompetencyMaster().getCompetencyId()+"###"+scoreLookup.getTotalscore(), scoreLookup);
				System.out.println("scoreLookupCompetencyMap.size() : "+scoreLookupCompetencyMap.size());*/
				
				//StatelessSession statelessSession2 = sessionFactory.openStatelessSession();
				List<AssessmentDomainScore> assessmentDomainScoreList = new ArrayList<AssessmentDomainScore>();
				assessmentDomainScoreList = assessmentDomainScoreDAO.findByDomainAndTeacherList(domainMaster, teacherDetailList);
				System.out.println("************* 7 ****************");
				System.out.println("assessmentDomainScoreList.size() : "+assessmentDomainScoreList.size());
				for(AssessmentDomainScore assessmentDomainScore : assessmentDomainScoreList){
					Object object[] = (Object[]) rawDataForDomainScoreMap.get(assessmentDomainScore.getTeacherDetail().getTeacherId());
					if(object!=null){
						ScoreLookup scoreLookup = scoreLookupDomainMap.get(assessmentDomainScore.getDomainMaster().getDomainId()+"###"+object[1]);
						if(scoreLookup!=null)
						{
							assessmentDomainScore.setPercentile(scoreLookup.getPercentile());
							assessmentDomainScore.setZscore(scoreLookup.getZscore());
							assessmentDomainScore.setTscore(scoreLookup.getTscore());
							assessmentDomainScore.setNcevalue(scoreLookup.getNcevalue());
							assessmentDomainScore.setNormscore(scoreLookup.getNormscore());
							assessmentDomainScore.setRitvalue(scoreLookup.getRitvalue());
						}
					}
					statelessSession1.update(assessmentDomainScore);
				}
				System.out.println("************* 8 ****************");
			 	System.out.println("assessmentDomainScore data update done");
			 	
			 	//StatelessSession statelessSession3 = sessionFactory.openStatelessSession();
			 	List<TeacherNormScore> teacherNormScoreList = new ArrayList<TeacherNormScore>();
			 	teacherNormScoreList = teacherNormScoreDAO.findTeacersNormScoresList(teacherDetailList);
			 	System.out.println("************* 9 ****************");
			 	System.out.println("teacherNormScoreList.size() : "+teacherNormScoreList.size());
			 	
			 	Map<Integer, TeacherNormScore> teacherNormScoreMap = new HashMap<Integer, TeacherNormScore>();
			 	for(TeacherNormScore teacherNormScore : teacherNormScoreList)
			 		teacherNormScoreMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
			 	System.out.println("************* 10 ****************");
			 	System.out.println("teacherNormScoreMap.size() : "+teacherNormScoreMap.size());
			 	
			 	List assessmentDomainScoreDataList = new ArrayList();
			 	assessmentDomainScoreDataList = assessmentDomainScoreDAO.calculateCandidatesNormScore(teacherDetailList);
			 	System.out.println("************* 11 ****************");
			 	System.out.println("assessmentDomainScoreDataList.size() : "+assessmentDomainScoreDataList.size());
			 	
			 	CGReportService cgReportService = new CGReportService();
				
				if(tmpPercentileWiseZScoreList==null){
					tmpPercentileWiseZScoreList = tmpPercentileWiseZScoreDAO.findByCriteria(Order.asc("zScore"));
					System.out.println("************* 12 ****************");
					System.out.println("tmpPercentileWiseZScoreList.size() : "+tmpPercentileWiseZScoreList.size());
				}
				if(pztList==null){
					pztList = cgReportService.populateZScoreMap(tmpPercentileWiseZScoreList);
					System.out.println("************* 13 ****************");
					System.out.println("pztList.size() : "+pztList.size());
				}
			 	
				for(Object object : assessmentDomainScoreDataList){
					Object obj[] = (Object[])object;
					System.out.println("obj[0] normscore : "+obj[0]);
					System.out.println("obj[1] teacherId : "+obj[1]);
					Double normscoreD = (Double)obj[0];
					Integer normscore = Integer.valueOf((int) Math.round(normscoreD));
					
					TeacherNormScore teacherNormScore = teacherNormScoreMap.get(Integer.parseInt(String.valueOf(obj[1])));
					if(teacherNormScore!=null){
						teacherNormScore.setTeacherNormScore(normscore);
						double percentile = cgReportService.getPercentile(pztList,normscore);
						teacherNormScore.setPercentile(percentile);
						Object[] ob = cgReportService.getDeciles(percentile);
						teacherNormScore.setDecileColor(""+ob[0]);
						teacherNormScore.setMinDecileValue((Double)ob[1]);
						teacherNormScore.setMaxDecileValue((Double)ob[2]);
					}
					statelessSession1.update(teacherNormScore);
				}
			 	transaction.commit();
				statelessSession1.close();
				System.out.println("************* 14 ****************");
			 	System.out.println("teacherNormScore data update done");
			}
			else
			{
				System.out.println("No Domain OR No Teacher Ids ");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
			System.out.println("************* End ****************");
		}
		return null;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param map
	 * @param request
	 * @param response
	 * @details Update Smart Practices Score
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/service/updateSPScore.do", method=RequestMethod.GET)
	public String updateSPScore(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println(":::::::::::::::: TestPageController /service/updateSPScore.do ::::::::::::::::");
		try {
			response.setContentType("text/html");
			
			String domain = request.getParameter("domainId");
			String competency = request.getParameter("competencyId");
//			String tid = request.getParameter("tid");
			Integer domainId = null;
			Integer competencyId = null;
//			System.out.println("tid :: "+tid);
			System.out.println("domain :: "+domain);
			System.out.println("competency :: "+competency);
			
			if(domain!=null && !domain.equals(""))
				domainId = Integer.parseInt(domain);
			
			if(competency!=null && !competency.equals(""))
				competencyId = Integer.parseInt(competency);
			
			DomainMaster domainMaster = null;
			CompetencyMaster competencyMaster = null;
			if(domainId!=null)
				domainMaster = domainMasterDAO.findById(domainId, false, false);
			if(competencyId!=null)
				competencyMaster = competencyMasterDAO.findById(competencyId, false, false);
			
//			List<Integer> teacherIdList = new ArrayList<Integer>();
			List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
			
//			if(tid!=null && !tid.equals("")){
//				String arr[] = tid.split(",");
//				if(arr.length >0){
//					for(int i=0; i<arr.length; i++)
//						teacherIdList.add(Integer.parseInt(arr[i].trim()));
//				}
//			}
			
			String teacher = Utility.getTeachers();
		 	
		 	String [] teachers = teacher.split(",");
		 	for (String string : teachers) {
		 		TeacherDetail teacherDetail = new TeacherDetail();
		 		teacherDetail.setTeacherId(Integer.parseInt(string));
		 		teacherDetailList.add(teacherDetail);
			}
		 	System.out.println("teacherDetailList.size() : "+teacherDetailList.size());
			
//			if(teacherIdList!=null && teacherIdList.size()>0)
//				teacherDetailList = teacherDetailDAO.findByTeacherIds(teacherIdList);
			
			if(domainMaster!=null && teacherDetailList!=null && teacherDetailList.size()>0)
			{
				System.out.println("domainMaster :: "+domainMaster);
				System.out.println("teacherDetailList.size() :: "+teacherDetailList.size());
				
				AssessmentDetail assessmentDetail = assessmentDetailDAO.findById(269, false, false);
				StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.getTASByTAssAndStatus(teacherDetailList, assessmentDetail, statusMaster);
				System.out.println("************* 1 ****************");
				System.out.println("teacherAssessmentStatusList.size() : "+teacherAssessmentStatusList.size());
				List<TeacherAssessmentdetail> teacherAssessmentdetailList = new ArrayList<TeacherAssessmentdetail>();
				for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList){
					if(teacherAssessmentStatus!=null && teacherAssessmentStatus.getTeacherAssessmentdetail()!=null)
						teacherAssessmentdetailList.add(teacherAssessmentStatus.getTeacherAssessmentdetail());
				}
				System.out.println("************* 2 ****************");
				System.out.println("teacherAssessmentdetailList.size() : "+teacherAssessmentdetailList.size());
				List teacherAnswerList = null;
				Map<String, Object> rawDataForDomainScoreMap = new HashMap<String, Object>();
				TeacherDetail teacherDetail = null;
				TeacherAssessmentdetail teacherAssessmentdetail = null;
				teacherAnswerList = teacherAnswerDetailDAO.findScoreByDomainAndTAD(teacherAssessmentdetailList, domainMaster);
				System.out.println("************* 3 ****************");
				System.out.println("teacherAnswerList.size() : "+teacherAnswerList.size());
				for(Object object : teacherAnswerList){
					Object obj[] = (Object[])object;
					teacherDetail = (TeacherDetail) obj[0];
					teacherAssessmentdetail = (TeacherAssessmentdetail) obj[1];
					rawDataForDomainScoreMap.put(teacherDetail.getTeacherId()+"#"+teacherAssessmentdetail.getTeacherAssessmentId(), obj);
				}
				System.out.println("************* 4 ****************");
				System.out.println("rawDataForDomainScoreMap.size() : "+rawDataForDomainScoreMap.size());
				
				SessionFactory sessionFactory =  domainMasterDAO.getSessionFactory();
				StatelessSession statelessSession1 = sessionFactory.openStatelessSession();
				Transaction transaction = statelessSession1.beginTransaction();
				
				List<RawDataForDomain> rawDataForDomainList = new ArrayList<RawDataForDomain>();
				rawDataForDomainList = rawDataForDomainDAO.findByDomainTeacherAndTAD(domainMaster, teacherDetailList, teacherAssessmentdetailList);
				System.out.println("************* 5 ****************");
				System.out.println("rawDataForDomainList.size() : "+rawDataForDomainList.size());
				for(RawDataForDomain rawDataForDomain : rawDataForDomainList){
					//System.out.println("rawDataForDomain.getTeacherDetail().getTeacherId() : "+rawDataForDomain.getTeacherDetail().getTeacherId());
					Object object[] = (Object[]) rawDataForDomainScoreMap.get(rawDataForDomain.getTeacherDetail().getTeacherId()+"#"+rawDataForDomain.getTeacherAssessmentdetail().getTeacherAssessmentId());
					if(object!=null){
						rawDataForDomain.setScore(new Double(""+object[2]));
						rawDataForDomain.setMaxMarks(new Double(""+object[3]));
					}
					statelessSession1.update(rawDataForDomain);
				}
				System.out.println("************* 6 ****************");
			 	System.out.println("rawDataForDomain data update done");
			 	
				List<ScoreLookup> scoreLookupList = new ArrayList<ScoreLookup>();
				Map<String,ScoreLookup> scoreLookupDomainMap = new HashMap<String, ScoreLookup>();
				//Map<String,ScoreLookup> scoreLookupCompetencyMap = new HashMap<String, ScoreLookup>();
				
				scoreLookupList = scoreLookupDAO.findScoreLookup(1,4);
				System.out.println("************* 7 ****************");
				System.out.println("scoreLookupList.size() D : "+scoreLookupList.size());
				for (ScoreLookup scoreLookup : scoreLookupList)
					scoreLookupDomainMap.put(scoreLookup.getDomainMaster().getDomainId()+"###"+scoreLookup.getTotalscore(), scoreLookup);
				System.out.println("************* 8 ****************");
				System.out.println("scoreLookupDomainMap.size() : "+scoreLookupDomainMap.size());
				
				/*scoreLookupList = scoreLookupDAO.findScoreLookup(2,4);
				System.out.println("scoreLookupList.size() C : "+scoreLookupList.size());
				for (ScoreLookup scoreLookup : scoreLookupList)
					scoreLookupCompetencyMap.put(scoreLookup.getCompetencyMaster().getCompetencyId()+"###"+scoreLookup.getTotalscore(), scoreLookup);
				System.out.println("scoreLookupCompetencyMap.size() : "+scoreLookupCompetencyMap.size());*/
				
				//StatelessSession statelessSession2 = sessionFactory.openStatelessSession();
				List<AssessmentDomainScore> assessmentDomainScoreList = new ArrayList<AssessmentDomainScore>();
				assessmentDomainScoreList = assessmentDomainScoreDAO.findByDomainTeacherAndTAD(domainMaster, teacherDetailList, rawDataForDomainList);
				System.out.println("************* 9 ****************");
				System.out.println("assessmentDomainScoreList.size() : "+assessmentDomainScoreList.size());
				for(AssessmentDomainScore assessmentDomainScore : assessmentDomainScoreList){
					Object object[] = (Object[]) rawDataForDomainScoreMap.get(assessmentDomainScore.getTeacherDetail().getTeacherId()+"#"+assessmentDomainScore.getRawDataForDomain().getTeacherAssessmentdetail().getTeacherAssessmentId());
					if(object!=null){
						ScoreLookup scoreLookup = scoreLookupDomainMap.get(assessmentDomainScore.getDomainMaster().getDomainId()+"###"+object[2]);
						if(scoreLookup!=null)
						{
							assessmentDomainScore.setPercentile(scoreLookup.getPercentile());
							assessmentDomainScore.setZscore(scoreLookup.getZscore());
							assessmentDomainScore.setTscore(scoreLookup.getTscore());
							assessmentDomainScore.setNcevalue(scoreLookup.getNcevalue());
							assessmentDomainScore.setNormscore(scoreLookup.getNormscore());
							assessmentDomainScore.setRitvalue(scoreLookup.getRitvalue());
						}
					}
					statelessSession1.update(assessmentDomainScore);
				}
				System.out.println("************* 10 ****************");
			 	System.out.println("assessmentDomainScore data update done");
			 	for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList){
			 		Object object[] = (Object[]) rawDataForDomainScoreMap.get(teacherAssessmentStatus.getTeacherDetail().getTeacherId()+"#"+teacherAssessmentStatus.getTeacherAssessmentdetail().getTeacherAssessmentId());
					if(object!=null){
						ScoreLookup scoreLookup = scoreLookupDomainMap.get(domainMaster.getDomainId()+"###"+object[2]);
						if(scoreLookup!=null)
						{
							String pass = null;
							if(scoreLookup.getPass()!=null && !scoreLookup.getPass().equals("")){
								if(scoreLookup.getPass().equals("0"))
									pass="F";
								else if(scoreLookup.getPass().equals("1"))
									pass="P";
								System.out.println("teacherId : "+teacherAssessmentStatus.getTeacherDetail().getTeacherId()+" : teacherAssessmentId : "+teacherAssessmentStatus.getTeacherAssessmentdetail().getTeacherAssessmentId()+" : pass : "+pass);
								teacherAssessmentStatus.setPass(pass);
							}
						}
					}
					statelessSession1.update(teacherAssessmentStatus);
			 	}
			 	transaction.commit();
				statelessSession1.close();
			 	System.out.println("************* 11 ****************");
			 	System.out.println("teacherAssessmentStatus data update done");
			}
			else if(competencyMaster!=null && teacherDetailList!=null && teacherDetailList.size()>0)
			{
				System.out.println("competencyMaster :: "+competencyMaster);
				System.out.println("teacherDetailList.size() :: "+teacherDetailList.size());
				
				SessionFactory sessionFactory =  domainMasterDAO.getSessionFactory();
				StatelessSession statelessSession1 = sessionFactory.openStatelessSession();
				Transaction transaction = statelessSession1.beginTransaction();
				
				AssessmentDetail assessmentDetail = assessmentDetailDAO.findById(269, false, false);
				StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.getTASByTAssAndStatus(teacherDetailList, assessmentDetail, statusMaster);
				List<TeacherAssessmentdetail> teacherAssessmentdetailList = new ArrayList<TeacherAssessmentdetail>();
				for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList){
					if(teacherAssessmentStatus!=null && teacherAssessmentStatus.getTeacherAssessmentdetail()!=null)
						teacherAssessmentdetailList.add(teacherAssessmentStatus.getTeacherAssessmentdetail());
				}
				List teacherAnswerList = null;
				Map<String, Object> rawDataForCompetencyScoreMap = new HashMap<String, Object>();
				TeacherDetail teacherDetail = null;
				TeacherAssessmentdetail teacherAssessmentdetail = null;
				teacherAnswerList = teacherAnswerDetailDAO.findScoreByCompetecyAndTAD(teacherAssessmentdetailList, competencyMaster);
				System.out.println("************* 1 ****************");
				System.out.println("teacherAnswerList.size() : "+teacherAnswerList.size());
				for(Object object : teacherAnswerList){
					Object obj[] = (Object[])object;
					teacherDetail = (TeacherDetail) obj[0];
					teacherAssessmentdetail = (TeacherAssessmentdetail) obj[1];
					rawDataForCompetencyScoreMap.put(teacherDetail.getTeacherId()+"#"+teacherAssessmentdetail.getTeacherAssessmentId(), obj);
				}
				System.out.println("************* 2 ****************");
				System.out.println("rawDataForCompetencyScoreMap.size() : "+rawDataForCompetencyScoreMap.size());
				
				List<RawDataForCompetency> rawDataForCompetencyList = new ArrayList<RawDataForCompetency>();
				rawDataForCompetencyList = rawDataForCompetencyDAO.findByCompetencyTeacherAndTAD(competencyMaster, teacherDetailList, teacherAssessmentdetailList);
				System.out.println("************* 3 ****************");
				System.out.println("rawDataForCompetencyList.size() : "+rawDataForCompetencyList.size());
				for(RawDataForCompetency rawDataForCompetency : rawDataForCompetencyList){
					//System.out.println("rawDataForDomain.getTeacherDetail().getTeacherId() : "+rawDataForDomain.getTeacherDetail().getTeacherId());
					Object object[] = (Object[]) rawDataForCompetencyScoreMap.get(rawDataForCompetency.getTeacherDetail().getTeacherId()+"#"+rawDataForCompetency.getTeacherAssessmentdetail().getTeacherAssessmentId());
					if(object!=null){
						rawDataForCompetency.setScore(new Double(""+object[2]));
						rawDataForCompetency.setMaxMarks(new Double(""+object[3]));
					}
					statelessSession1.update(rawDataForCompetency);
				}
				transaction.commit();
				statelessSession1.close();
				System.out.println("************* 4 ****************");
			 	System.out.println("rawDataForCompetency data update done");
			}
			else
			{
				System.out.println("No Domain OR No Competency OR No Teacher Ids ");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
			System.out.println("************* End ****************");
		}
		return null;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param map
	 * @param request
	 * @param response
	 * @details Insert IPI Score
	 * @URL http://localhost:8080/teachermatch/service/insertIPIScore.do?tid=
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/service/insertIPIScore.do", method=RequestMethod.GET)
	public String updateIPIScore(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println(":::::::::::::::: TestPageController /service/insertIPIScore.do ::::::::::::::::");
		
		try {
			List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
			String tid = request.getParameter("tid");
			System.out.println("tid :: "+tid);
			List<Integer> teacherIdList = new ArrayList<Integer>();
			if(tid!=null && !tid.equals("")){
				teacherIdList.add(Integer.parseInt(tid.trim()));
			}
			else{
				String teacher = Utility.getTeachers();
			 	String [] teachers = teacher.split(",");
			 	for (String string : teachers) {
			 		TeacherDetail teacherDetail = new TeacherDetail();
			 		teacherDetail.setTeacherId(Integer.parseInt(string));
			 		teacherDetailList.add(teacherDetail);
				}
			}
			
			if(teacherIdList!=null && teacherIdList.size()>0)
				teacherDetailList = teacherDetailDAO.findByTeacherIds(teacherIdList);
		 	System.out.println("teacherDetailList.size() : "+teacherDetailList.size());
		 	
		 	Integer assessmentId = 255;//Platform assessmentId = 255, titankelly assessmentId = 214
		 	AssessmentDetail assessmentDetail = assessmentDetailDAO.findById(assessmentId, false, false);
		 	System.out.println("assessmentDetail.getAssessmentId() : "+assessmentDetail.getAssessmentId());
		 	
			StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
			Date assessmentCompletedDateTime = new Date();
			List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.getTASByTAssAndStatus(teacherDetailList, assessmentDetail, statusMaster);
			System.out.println("************* 1 ****************");
			System.out.println("teacherAssessmentStatusList.size() : "+teacherAssessmentStatusList.size());
			List<TeacherAssessmentdetail> teacherAssessmentdetailList = new ArrayList<TeacherAssessmentdetail>();
			for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList){
				if(teacherAssessmentStatus!=null && teacherAssessmentStatus.getTeacherAssessmentdetail()!=null)
					teacherAssessmentdetailList.add(teacherAssessmentStatus.getTeacherAssessmentdetail());
			}
			System.out.println("************* 2 ****************");
			System.out.println("teacherAssessmentdetailList.size() : "+teacherAssessmentdetailList.size());
			
			List<DomainMaster> domainMasterList = domainMasterDAO.findByCriteria(Restrictions.eq("domainId", 3));
			System.out.println("************* 3 ****************");
			System.out.println("domainMasterList.size() : "+domainMasterList.size());
			
			Integer[] competencies = {9,10,11,12};
			List<CompetencyMaster> competencyMasterList = competencyMasterDAO.findByCriteria(Restrictions.in("competencyId", competencies));
			System.out.println("************* 4 ****************");
			System.out.println("competencyMasterList.size() : "+competencyMasterList.size());
			
			if(domainMasterList!=null && domainMasterList.size()>0 && competencyMasterList!=null && competencyMasterList.size()>0 && teacherDetailList!=null && teacherDetailList.size()>0 && teacherAssessmentdetailList!=null && teacherAssessmentdetailList.size()>0)
			{
				System.out.println("************************** raw data inserting start **************************");
				if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0)
					assessmentCompletedDateTime = teacherAssessmentStatusList.get(0).getAssessmentCompletedDateTime();
				try {
					List questionIdList = new ArrayList();
					questionIdList = teacherAnswerDetailDAO.findDuplicateQuestionId(teacherDetailList, teacherAssessmentdetailList.get(0));
					for(Object object : questionIdList){
						Object obj[] = (Object[])object;
						System.out.println("obj[0] cnt : "+obj[0]);
						System.out.println("obj[1] teacherAssessmentQuestionId : "+obj[1]);
						System.out.println("obj[2] teacherId : "+obj[2]);
						Integer cnt = Integer.parseInt(""+obj[0]);
						Long teacherAssessmentQuestionId = Long.parseLong(""+obj[1]);
						Integer teacherId = Integer.parseInt(""+obj[2]);
						teacherAnswerDetailDAO.removeAnswerByTeacherIdAndQuestionId(teacherId, teacherAssessmentQuestionId, cnt);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				Map<Integer,Map<Integer, Object>> mapDomainScore = new HashMap<Integer, Map<Integer,Object>>();
				Map<Integer, Object> mapDomainScoreTotal = null;
				
				List lstAns = null;
				TeacherDetail tDetail = null;
				for(DomainMaster dm: domainMasterList){
					mapDomainScoreTotal = new HashMap<Integer, Object>();
					if(teacherAssessmentdetailList.get(0)!=null)
						lstAns = teacherAnswerDetailDAO.findSumOfAnsByDomainAndTeacherList(teacherDetailList, dm, teacherAssessmentdetailList.get(0));
					
					for(Object oo: lstAns){
						Object obj[] = (Object[])oo;
						tDetail = (TeacherDetail) obj[0];				
						mapDomainScoreTotal.put(tDetail.getTeacherId(), obj);
					}
					mapDomainScore.put(dm.getDomainId(), mapDomainScoreTotal);
				}
				
				Map<Integer,Map<Integer, Object>> mapCompetencyScore = new HashMap<Integer, Map<Integer,Object>>();
				Map<Integer, Object> mapCompetencyScoreTotal = null;
				for(CompetencyMaster competencyMaster: competencyMasterList){
					mapCompetencyScoreTotal = new HashMap<Integer, Object>();
					if(teacherAssessmentdetailList.get(0)!=null && teacherAssessmentdetailList.get(0).getAssessmentDetail()!=null && teacherAssessmentdetailList.get(0).getAssessmentType()!=null)
						lstAns = teacherAnswerDetailDAO.findSumOfAnsByComptencyAndTeacherList(teacherDetailList, competencyMaster, teacherAssessmentdetailList.get(0));
					
					for(Object oo: lstAns){
						Object obj[] = (Object[])oo;
						tDetail = (TeacherDetail) obj[0];
						mapCompetencyScoreTotal.put( tDetail.getTeacherId(), obj);
					}
					mapCompetencyScore.put(competencyMaster.getCompetencyId(), mapCompetencyScoreTotal);
				}
				
				RawDataForDomain rawdatafordomain = null;
				RawDataForCompetency rawDataForCompetency = null;
				SessionFactory sessionFactory =  domainMasterDAO.getSessionFactory();
				StatelessSession statelesssession = sessionFactory.openStatelessSession();
				Transaction tx = statelesssession.beginTransaction();
				for(TeacherDetail teacherDetail : teacherDetailList){
					for(DomainMaster dm: domainMasterList){				
						Object obj[] = (Object[]) mapDomainScore.get(dm.getDomainId()).get(teacherDetail.getTeacherId());							
						if(obj!=null){					 
							rawdatafordomain = new RawDataForDomain();
							rawdatafordomain.setTeacherDetail(teacherDetail);
							rawdatafordomain.setCreatedDateTime(assessmentCompletedDateTime);
							rawdatafordomain.setDomainMaster(dm);
							rawdatafordomain.setScore(new Double(""+obj[1]));
							rawdatafordomain.setMaxMarks(new Double(""+obj[2]));
							rawdatafordomain.setAssessmentDetail(teacherAssessmentdetailList.get(0).getAssessmentDetail());
							rawdatafordomain.setAssessmentType(teacherAssessmentdetailList.get(0).getAssessmentType());
							rawdatafordomain.setAssessmentTakenCount(teacherAssessmentdetailList.get(0).getAssessmentTakenCount());
							rawdatafordomain.setTeacherAssessmentdetail(teacherAssessmentdetailList.get(0));
							statelesssession.insert(rawdatafordomain);	
						}				
					}
					
					for(CompetencyMaster competencyMaster : competencyMasterList){
						Object obj[] = (Object[]) mapCompetencyScore.get(competencyMaster.getCompetencyId()).get(teacherDetail.getTeacherId());							
						if(obj!=null){					 
							rawDataForCompetency = new RawDataForCompetency();
							rawDataForCompetency.setTeacherDetail(teacherDetail);
							rawDataForCompetency.setCreatedDateTime(assessmentCompletedDateTime);
							rawDataForCompetency.setDomainMaster(competencyMaster.getDomainMaster());
							rawDataForCompetency.setCompetencyMaster(competencyMaster);
							rawDataForCompetency.setScore(new Double(""+obj[1]));
							rawDataForCompetency.setMaxMarks(new Double(""+obj[2]));
							rawDataForCompetency.setAssessmentDetail(teacherAssessmentdetailList.get(0).getAssessmentDetail());
							rawDataForCompetency.setAssessmentType(teacherAssessmentdetailList.get(0).getAssessmentType());
							rawDataForCompetency.setAssessmentTakenCount(teacherAssessmentdetailList.get(0).getAssessmentTakenCount());
							rawDataForCompetency.setTeacherAssessmentdetail(teacherAssessmentdetailList.get(0));
							statelesssession.insert(rawDataForCompetency);	
						}		
					}			
				}
				tx.commit();
				statelesssession.close();
				System.out.println("************************** raw data inserting end **************************");
				
				System.out.println("************************** assessment data inserting start **************************");
				Integer lookupId = teacherAssessmentdetailList.get(0).getScoreLookupMaster().getLookupId();
				System.out.println("lookupId == "+lookupId);
				List<RawDataForDomain> lstRawDataForDomains = rawDataForDomainDAO.findAllInCurrentYearByTeacher(teacherDetailList, teacherAssessmentdetailList.get(0));
				System.out.println("lstRawDataForDomains.size() == "+lstRawDataForDomains.size());
				String sql = "";
				int i=0;
				Map<Integer, RawDataForDomain> rawDataForDomainMap = new HashMap<Integer, RawDataForDomain>();
				Map<Integer, RawDataForCompetency> rawDataForCompetencyMap = new HashMap<Integer, RawDataForCompetency>();
				RawDataForDomain rawDataForDomainObj = null;
				RawDataForCompetency rawDataForCompetencyObj = null;
				int size = lstRawDataForDomains.size();
				for (RawDataForDomain rawDataForDomain : lstRawDataForDomains) {
					rawDataForDomainMap.put(rawDataForDomain.getDomainMaster().getDomainId(), rawDataForDomain);
					if(i<size-1)
						sql+="(domainId = "+rawDataForDomain.getDomainMaster().getDomainId()+" AND totalscore = "+rawDataForDomain.getScore()+" AND lookupId = "+lookupId+") OR ";
					else
						sql+="(domainId = "+rawDataForDomain.getDomainMaster().getDomainId()+" AND totalscore = "+rawDataForDomain.getScore()+" AND lookupId = "+lookupId+")";
					i++;
				}
				System.out.println("sql domain == "+sql);
				DomainMaster domainMaster = null;
				//String domainScores = " (domainId =1 AND totalscore =80 ) OR ( domainId =2 AND  totalscore =7 ) OR ( domainId =3 AND  totalscore =42) ";
				String isPassFail=null;
				List one = scoreLookupDAO.getCandidatesNormScoreDomainWise(sql);
				System.out.println("one.size() == "+one.size());
				for (Iterator iterator = one.iterator(); iterator.hasNext();) {
					Object object[] = (Object[]) iterator.next();
					//System.out.println("object:: "+object[0]);
					rawDataForDomainObj = new RawDataForDomain();
					rawDataForDomainObj = rawDataForDomainMap.get(object[0]);
					AssessmentDomainScore assessmentDomainScore = new AssessmentDomainScore();
					domainMaster = new DomainMaster();
					domainMaster.setDomainId((Integer) object[0]);
					assessmentDomainScore.setDomainMaster(domainMaster);
					assessmentDomainScore.setTeacherDetail(teacherDetailList.get(0));
					assessmentDomainScore.setPercentile((Double) object[1]);
					assessmentDomainScore.setScoreLookupMaster(teacherAssessmentdetailList.get(0).getScoreLookupMaster());
					assessmentDomainScore.setZscore((Double) object[2]);
					assessmentDomainScore.setTscore((Double) object[3]);
					assessmentDomainScore.setNcevalue((Double) object[4]);
					assessmentDomainScore.setNormscore((Double) object[5]);
					assessmentDomainScore.setRitvalue((Double) object[6]);
					isPassFail = (String) object[7];
					System.out.println("isPassFail : "+isPassFail);
					assessmentDomainScore.setAssessmentDetail(teacherAssessmentdetailList.get(0).getAssessmentDetail());
					assessmentDomainScore.setAssessmentType(teacherAssessmentdetailList.get(0).getAssessmentType());
					assessmentDomainScore.setCreatedDateTime(assessmentCompletedDateTime);
					assessmentDomainScore.setAssessmentTakenCount(teacherAssessmentdetailList.get(0).getAssessmentTakenCount());
					assessmentDomainScore.setRawDataForDomain(rawDataForDomainObj);
					assessmentDomainScoreDAO.makePersistent(assessmentDomainScore);
				}
				
				List<RawDataForCompetency> lstRawDataForCompetency = rawDataForCompetencyDAO.findAllInCurrentYearByTeacher(teacherDetailList, teacherAssessmentdetailList.get(0));
				System.out.println("lstRawDataForCompetency.size() == "+lstRawDataForCompetency.size());
				String sqlComp = "";
				int j=0;
				int size1 = lstRawDataForCompetency.size();
				for (RawDataForCompetency rawDataForCompetency2 : lstRawDataForCompetency) {
					rawDataForCompetencyMap.put(rawDataForCompetency2.getCompetencyMaster().getCompetencyId(), rawDataForCompetency2);
					if(j<size1-1)
						sqlComp+="(competencyId = "+rawDataForCompetency2.getCompetencyMaster().getCompetencyId()+" AND totalscore = "+rawDataForCompetency2.getScore()+" AND lookupId = "+lookupId+") OR ";
					else
						sqlComp+="(competencyId = "+rawDataForCompetency2.getCompetencyMaster().getCompetencyId()+" AND totalscore = "+rawDataForCompetency2.getScore()+" AND lookupId = "+lookupId+")";
					j++;
				}
				System.out.println("sqlComp == "+sqlComp);
				CompetencyMaster competencyMaster = null;
				List two = scoreLookupDAO.getCandidatesNormScoreCompetencyWise(sqlComp);
				System.out.println("two.size() == "+two.size());
				for (Iterator iterator = two.iterator(); iterator.hasNext();) {
					Object object[] = (Object[]) iterator.next();
					//System.out.println("object:: "+object[0]);
					rawDataForCompetencyObj = new RawDataForCompetency();
					rawDataForCompetencyObj = rawDataForCompetencyMap.get(object[0]);
					AssessmentCompetencyScore assessmentCompetencyScore = new AssessmentCompetencyScore();
					competencyMaster = new CompetencyMaster();
					competencyMaster.setCompetencyId((Integer) object[0]);
					assessmentCompetencyScore.setCompetencyMaster(competencyMaster);
					assessmentCompetencyScore.setTeacherDetail(teacherDetailList.get(0));
					assessmentCompetencyScore.setPercentile((Double) object[1]);
					assessmentCompetencyScore.setScoreLookupMaster(teacherAssessmentdetailList.get(0).getScoreLookupMaster());
					assessmentCompetencyScore.setZscore((Double) object[2]);
					assessmentCompetencyScore.setTscore((Double) object[3]);
					assessmentCompetencyScore.setNcevalue((Double) object[4]);
					assessmentCompetencyScore.setNormscore((Double) object[5]);
					assessmentCompetencyScore.setRitvalue((Double) object[6]);
					assessmentCompetencyScore.setAssessmentDetail(teacherAssessmentdetailList.get(0).getAssessmentDetail());
					assessmentCompetencyScore.setAssessmentType(teacherAssessmentdetailList.get(0).getAssessmentType());
					assessmentCompetencyScore.setCreatedDateTime(assessmentCompletedDateTime);
					assessmentCompetencyScore.setAssessmentTakenCount(teacherAssessmentdetailList.get(0).getAssessmentTakenCount());
					assessmentCompetencyScore.setRawDataForCompetency(rawDataForCompetencyObj);
					assessmentCompetencyScoreDAO.makePersistent(assessmentCompetencyScore);
				}
				System.out.println("************************** assessment data inserting end **************************");
			}
			else
			{
				System.out.println("No Domain OR No Competency OR No Teacher Ids ");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
			System.out.println("************* End ****************");
		}
		return null;
	}
	
	@Transactional
	@RequestMapping(value="/maildetail.do", method=RequestMethod.GET)
	public void maildetail(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		try {
		System.out.println("inside java mail imap reader");
		String host = "imap.mail.yahoo.com";
        String port = "993";
        String userName = "ankit.saini71@yahoo.co.in";
        String password = "111111";
 
        String saveDirectory = "E:/Attachment";
		   	
        Properties props = new Properties();
        props.setProperty("mail.store.protocol", "imaps");

        Session session = Session.getInstance(props, null);
        Store store = session.getStore();
        store.connect(host,userName, password);	
   
        // opens the inbox folder
        Folder folderInbox = store.getFolder("Sent");
        folderInbox.open(Folder.READ_ONLY);

        // fetches new messages from server
        Message[] arrayMessages = folderInbox.getMessages();
     int mm=0;
     String fileName="";
     
		//Criteria criteria=Restrictions.eq("entityType", 1);
		 DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
         Calendar cal = Calendar.getInstance();
         System.out.println(dateFormat.format(cal.getTime()));
     
         String n=dateFormat.format(cal.getTime());
         
         
        System.out.println("arrayMessages lenght==="+arrayMessages.length);
        for (int i = 0; i < arrayMessages.length; i++) {
            Message message = arrayMessages[i];
            Address[] fromAddress = message.getFrom();
            String from = fromAddress[0].toString();
            
            Address[] toAddress = message.getAllRecipients();
            String toMail=toAddress[0].toString();
            
            
            String subject = message.getSubject();
            Date sentDate = message.getSentDate();

            String contentType = message.getContentType();
            String messageContent = "";

            // store attachment file name, separated by comma
            String attachFiles = "";
            
            
            MessageToTeacher messageToTeacher = new MessageToTeacher();
            
            
            if (contentType.contains("multipart")) {
            	System.out.println("Attachment mail==="+mm++);
                // content may contain attachments
                Multipart multiPart = (Multipart) message.getContent();
                int numberOfParts = multiPart.getCount();
                for (int partCount = 0; partCount < numberOfParts; partCount++) {
                    MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
                    if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition()))
                     {
                    	messageContent = part.getContent().toString();
                        // this part is attachment
                         fileName = part.getFileName();
                         
                         System.out.println("------------------------------------------------------------------------------");
                         System.out.println("FileName:- "+fileName+", and it contains (.):- "+(fileName.contains(".")));
                         if(fileName.contains("."))
                         {
                        	 String firstNameWithDate = (fileName.substring(0, fileName.lastIndexOf(".")))+"_"+n;
                        	 System.out.println("firstNameWithDate:- "+firstNameWithDate);
                        	 fileName = firstNameWithDate + (fileName.substring(fileName.lastIndexOf(".")));
                        	 fileName = fileName.replaceAll(" ","_");
                        	 fileName = fileName.replaceAll(":","_");
                        	                        	
                         }
                         System.out.println("After FileName:- "+fileName);
                         System.out.println("------------------------------------------------------------------------------");
                                                  
                        if(attachFiles.equals(""))
                        	attachFiles = attachFiles + fileName;
                        else
                        	attachFiles = attachFiles+", "+ fileName;
                        part.saveFile(saveDirectory + File.separator + fileName);
                    } else {
                        // this part may be the message content
                        messageContent = part.getContent().toString();
                    }
                }
              
            } else if (contentType.contains("text/plain")|| contentType.contains("text/html"))
            {
                Object content = message.getContent();
                if (content != null) {
                    messageContent = content.toString();
                }
            }

            // print out details of each message
            System.out.println("Message #" + (i + 1) + ":");
                   
            System.out.println("Toooo===="+toMail);
            System.out.println("\t From: " + from);
            System.out.println("\t Subject: " + subject);
            System.out.println("\t Sent Date: " + sentDate);
            System.out.println("\t Message: " + messageContent);
            System.out.println("\t Attachments: " + attachFiles);
          
         
           
           
        }      
       
        // disconnect
        folderInbox.close(false);
        store.close();
    } catch (NoSuchProviderException ex) {
        System.out.println("No provider for pop3.");
        ex.printStackTrace();
    } catch (MessagingException ex) {
        System.out.println("Could not connect to the message store");
        ex.printStackTrace();
    } catch (Exception ex) {
        ex.printStackTrace();
    }
	}
	
	@RequestMapping(value="/autoattachJSI.do", method=RequestMethod.GET)
	public @ResponseBody String autoAttachJsi(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("autoattachJSI.do");
		String strInput="";
		SessionFactory factory=assessmentJobRelationDAO.getSessionFactory();
		StatelessSession statelessSession=factory.openStatelessSession();
		String JSIMessage="";
		Transaction transaction=statelessSession.beginTransaction();
		try {
			List<DistrictMaster> districtMasterList=new ArrayList<DistrictMaster>();
			List<JobCategoryMaster> lstJobcategorgmaster= new ArrayList<JobCategoryMaster>();
			List<AssessmentDetail> lstassessment=new ArrayList<AssessmentDetail>();
			List<JobOrder> lstJobOrder=new ArrayList<JobOrder>();
			List<AssessmentJobRelation> lstAssessmentJobRelation=new ArrayList<AssessmentJobRelation>();
			UserMaster userMaster=userMasterDAO.findById(1, false, false);
			//Fatch record from Jobcategory Master table on the basis of Offer JSI =1
			lstJobcategorgmaster=jobCategoryMasterDAO.findJobCategoryByOfferJSI();
			if(lstJobcategorgmaster.size()>0)
			{
				System.out.println("lstJobcategorgmaster==="+lstJobcategorgmaster.size());
				
				
				//Fatch record from JobOrder on the basis of Job category list
				lstJobOrder=jobOrderDAO.findJobOrderByJobCategoriesWithoutStatus(lstJobcategorgmaster);
				System.out.println("lstJobOrder========================"+lstJobOrder.size());
				
				for(JobCategoryMaster jobcate:lstJobcategorgmaster)
				{					
					districtMasterList.add(jobcate.getDistrictMaster());
				}	
				
				//Fatch record from assessmentDetail table on the basis of District and Job category
				lstassessment=assessmentDetailDAO.assessmentIdByJobCategory(districtMasterList,lstJobcategorgmaster);
				System.out.println("lstassessment======="+lstassessment.size());
				
				
			}
			
			Map<Integer, AssessmentDetail> assessmentDetailMap=new HashMap<Integer, AssessmentDetail>();			
			if(lstassessment.size()>0 && lstassessment!=null)
			{
				for(AssessmentDetail ad:lstassessment)
				{
					assessmentDetailMap.put(ad.getJobCategoryMaster().getJobCategoryId(), ad);
				}
				
			}
				
			//Fatch record from assessmentJobRelation table on the basis of joborder and assessment id to check duplicate records			
				lstAssessmentJobRelation=assessmentJobRelationDAO.findAssessmentJobRelationByAssessmentId(lstJobOrder,lstassessment);
				System.out.println("lstAssessmentJobRelation======================"+lstAssessmentJobRelation.size());
			
		
			Map<Integer, AssessmentJobRelation> assessmentJobRelationMap=new HashMap<Integer, AssessmentJobRelation>();						
			if(lstAssessmentJobRelation.size()>0)
			{
				for(AssessmentJobRelation ajr:lstAssessmentJobRelation)
				{
					assessmentJobRelationMap.put(ajr.getJobId().getJobId(), ajr);
				}
				
			}			
			if(lstJobOrder!=null && lstJobOrder.size()>0)
			{
				AssessmentJobRelation assessmentJobRelation=null;
				AssessmentDetail assessmentDetail=null;
				int contnull=0;
				int count=0;
				System.out.println("assessmentJobRelationMap========="+assessmentJobRelationMap.size());
				for(JobOrder jo:lstJobOrder)
				{
					assessmentJobRelation=assessmentJobRelationMap.get(jo.getJobId());									
					assessmentDetail=assessmentDetailMap.get(jo.getJobCategoryMaster().getJobCategoryId());					
					if(assessmentJobRelation==null && assessmentDetail!=null)
					{
						
						try
						{
							//System.out.println("assessmentDetail=="+assessmentDetail.getAssessmentId());
							assessmentJobRelation=new AssessmentJobRelation();
					
						assessmentJobRelation.setAssessmentId(assessmentDetail);						
						assessmentJobRelation.setJobId(jo);
						assessmentJobRelation.setCreatedBy(userMaster);
						assessmentJobRelation.setStatus("A");
						assessmentJobRelation.setCreatedDateTime(new Date());
						statelessSession.insert(assessmentJobRelation);
						contnull++;
						}catch(Exception e){e.printStackTrace();}
					}
					else
					{
						System.out.println("jo.getJobId()=="+jo.getJobId());
						System.out.println("jo.getJobCategoryMaster().getJobCategoryId()=="+jo.getJobCategoryMaster().getJobCategoryId());
						count++;
					}
					
					
				}
				System.out.println("contnull==="+contnull);
				System.out.println("cont===="+count);	
				JSIMessage="After processing "+contnull+" jobs attach with JSI and "+count+" jobs already attach with JSI or JSI not configured for these jobs. ";
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
		transaction.commit();
		statelessSession.close();
		
		return JSIMessage;
	}
		
	/**
	 * @author Amit Chaudhary
	 * @param map
	 * @param request
	 * @param response
	 * @details Insert Assessment Scores. Here "taid" value is comma seprated teacherassessmentid like 1,2,3,4,5 and maximum 5 teacherassessmentids allow at a time.  
	 * @URL http://localhost:8080/teachermatch/service/insertAssessmentScore.do?taid=?
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/service/insertAssessmentScore.do", method=RequestMethod.GET)
	public String insertAssessmentScoreByTeaherAssessmentId(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println(":::::::::::::::: TestPageController : /service/insertAssessmentScore.do ::::::::::::::::");
		
		try {
			PrintWriter output = response.getWriter();
			String taid = request.getParameter("taid");
			List<Integer> teacherAssessmentIdList = new ArrayList<Integer>();
			List<TeacherAssessmentdetail> teacherAssessmentdetailList = new ArrayList<TeacherAssessmentdetail>();
			
			System.out.println("taid :: "+taid);

			if(taid!=null && !taid.equals("")){
				String arr[] = taid.split(",");
				if(arr.length>0 && arr.length<=5){
					for(int i=0; i<arr.length; i++){
						teacherAssessmentIdList.add(Integer.parseInt(arr[i].trim()));
					}
				}else if(arr.length>5){
					output.println(":::::::::::::::: TestPageController : /service/insertAssessmentScore.do ::::::::::::::::\n");
					output.println("\n\tmaximum 5 teacherassessmentids allow at a time, no data update\n");
					output.println("\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
					return null;
				}
			}
			else{
				output.println(":::::::::::::::: TestPageController : /service/insertAssessmentScore.do ::::::::::::::::\n");
				output.println("\n\ttaid has no value that's why no data update.\n");
				output.println("\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
				return null;
			}
			/** Uncomment if you want to update data from a .txt file. 
			 else{
				String teacher = Utility.getTeachers();
			 	String [] teachers = teacher.split(",");
			 	for (String string : teachers) {
			 		TeacherAssessmentdetail teacherAssessmentdetail = new TeacherAssessmentdetail();
			 		teacherAssessmentdetail.setTeacherAssessmentId(Integer.parseInt(string));
			 		teacherAssessmentdetailList.add(teacherAssessmentdetail);
				}
			}*/
			
			if(teacherAssessmentIdList!=null && teacherAssessmentIdList.size()>0)
				teacherAssessmentdetailList = teacherAssessmentDetailDAO.findByTeacherAssessmentIds(teacherAssessmentIdList);
			
			System.out.println("************* 1 ****************");
		 	System.out.println("teacherAssessmentdetailList.size() : "+teacherAssessmentdetailList.size());
		 	
		 	List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		 	StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
		 	teacherAssessmentStatusList = teacherAssessmentStatusDAO.findByTeacherAssessmentdetails(teacherAssessmentdetailList,statusMaster);
		 	
		 	System.out.println("************* 2 ****************");
			System.out.println("teacherAssessmentStatusList.size() : "+teacherAssessmentStatusList.size());
			
			List<DomainMaster> domainMasterList = WorkThreadServlet.domainMastersPDRList;
			List<CompetencyMaster> competencyMastersList = competencyMasterDAO.getCompetenciesForPDR(true);
			SessionFactory sessionFactory =  domainMasterDAO.getSessionFactory();
		 	
			for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentStatusList){
		 		List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
		 		teacherDetailList.add(teacherAssessmentStatus.getTeacherDetail());
		 		TeacherAssessmentdetail teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();
//		 		Date assessmentCompletedDateTime = teacherAssessmentStatus.getAssessmentCompletedDateTime();
		 		
		 		PreparedStatement preparedStatement = null;
				CGReportService cgReportService = new CGReportService();
				Connection connection = cgReportService.getConnection();
				/**
					delete FROM teachernormscore WHERE teacherId=? and teacherAssessmentId=?;
					delete FROM assessmentdomainscore WHERE teacherId=? and assessmentTakenCount=? and assessmentId=?;
					delete FROM assessmentcompetencyscore WHERE teacherId=? and assessmentTakenCount=? and assessmentId=?;
					delete FROM rawdatafordomain WHERE teacherId=? and teacherAssessmentId=?;
					delete FROM rawdataforcompetency WHERE teacherId=? and teacherAssessmentId=?;
					delete FROM rawdataforobjective WHERE teacherId=? and teacherAssessmentId=?;
					delete old data if exist.
				 */
				System.out.println("delete : teachernormscore");
				preparedStatement = connection.prepareStatement("delete FROM teachernormscore WHERE teacherId="+teacherAssessmentdetail.getTeacherDetail().getTeacherId()+" and teacherAssessmentId="+teacherAssessmentdetail.getTeacherAssessmentId());
				preparedStatement.executeUpdate();
				System.out.println("delete : assessmentdomainscore");
				preparedStatement = connection.prepareStatement("delete FROM assessmentdomainscore WHERE teacherId="+teacherAssessmentdetail.getTeacherDetail().getTeacherId()+" and assessmentTakenCount="+teacherAssessmentdetail.getAssessmentTakenCount()+" and assessmentId="+teacherAssessmentdetail.getAssessmentDetail().getAssessmentId());
				preparedStatement.executeUpdate();
				System.out.println("delete : rawdatafordomain");
				preparedStatement = connection.prepareStatement("delete FROM rawdatafordomain WHERE teacherId="+teacherAssessmentdetail.getTeacherDetail().getTeacherId()+" and teacherAssessmentId="+teacherAssessmentdetail.getTeacherAssessmentId());
				preparedStatement.executeUpdate();
				System.out.println("delete : assessmentcompetencyscore");
				preparedStatement = connection.prepareStatement("delete FROM assessmentcompetencyscore WHERE teacherId="+teacherAssessmentdetail.getTeacherDetail().getTeacherId()+" and assessmentTakenCount="+teacherAssessmentdetail.getAssessmentTakenCount()+" and assessmentId="+teacherAssessmentdetail.getAssessmentDetail().getAssessmentId());
				preparedStatement.executeUpdate();
				System.out.println("delete : rawdataforcompetency");
				preparedStatement = connection.prepareStatement("delete FROM rawdataforcompetency WHERE teacherId="+teacherAssessmentdetail.getTeacherDetail().getTeacherId()+" and teacherAssessmentId="+teacherAssessmentdetail.getTeacherAssessmentId());
				preparedStatement.executeUpdate();
				System.out.println("delete : rawdataforobjective");
				preparedStatement = connection.prepareStatement("delete FROM rawdataforobjective WHERE teacherId="+teacherAssessmentdetail.getTeacherDetail().getTeacherId()+" and teacherAssessmentId="+teacherAssessmentdetail.getTeacherAssessmentId());
				preparedStatement.executeUpdate();
		 		
				if(teacherAssessmentdetail.getAssessmentType()!=1 && teacherAssessmentdetail.getAssessmentType()!=2)
					domainMasterList = teacherAssessmentQuestionDAO.findTeacherDomains(teacherAssessmentdetail);
				
				try {
					List questionIdList = new ArrayList();
					questionIdList = teacherAnswerDetailDAO.findDuplicateQuestionId(teacherDetailList, teacherAssessmentdetail);
					for(Object object : questionIdList){
						Object obj[] = (Object[])object;
						System.out.println("obj[0] cnt : "+obj[0]);
						System.out.println("obj[1] teacherAssessmentQuestionId : "+obj[1]);
						System.out.println("obj[2] teacherId : "+obj[2]);
						Integer cnt = Integer.parseInt(""+obj[0]);
						Long teacherAssessmentQuestionId = Long.parseLong(""+obj[1]);
						Integer teacherId = Integer.parseInt(""+obj[2]);
						teacherAnswerDetailDAO.removeAnswerByTeacherIdAndQuestionId(teacherId, teacherAssessmentQuestionId, cnt);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				Map<Integer,Map<Integer, Object>> mapDomainScore = new HashMap<Integer, Map<Integer,Object>>();
				Map<Integer, Object> mapDomainScoreTotal = null;
				
				List lstAns = null;
				TeacherDetail tDetail = null;
				for(DomainMaster dm: domainMasterList){
					mapDomainScoreTotal = new HashMap<Integer, Object>();
					if(teacherAssessmentdetail!=null)
						lstAns = teacherAnswerDetailDAO.findSumOfAnsByDomainAndTeacherList(teacherDetailList, dm, teacherAssessmentdetail);
					
					for(Object oo: lstAns){
						Object obj[] = (Object[])oo;
						tDetail = (TeacherDetail) obj[0];				
						mapDomainScoreTotal.put(tDetail.getTeacherId(), obj);
					}
					mapDomainScore.put(dm.getDomainId(), mapDomainScoreTotal);
				}
				
				if(teacherAssessmentdetail.getAssessmentType()!=1 && teacherAssessmentdetail.getAssessmentType()!=2)
					competencyMastersList = teacherAssessmentQuestionDAO.findTeacherCompetency(teacherAssessmentdetail);
				
				Map<Integer,Map<Integer, Object>> mapCompetencyScore = new HashMap<Integer, Map<Integer,Object>>();
				Map<Integer, Object> mapCompetencyScoreTotal = null;
				for(CompetencyMaster competencyMaster: competencyMastersList){
					mapCompetencyScoreTotal = new HashMap<Integer, Object>();
					if(teacherAssessmentdetail!=null && teacherAssessmentdetail.getAssessmentDetail()!=null && teacherAssessmentdetail.getAssessmentType()!=null)
						lstAns = teacherAnswerDetailDAO.findSumOfAnsByComptencyAndTeacherList(teacherDetailList, competencyMaster, teacherAssessmentdetail);
					
					for(Object oo: lstAns){
						Object obj[] = (Object[])oo;
						tDetail = (TeacherDetail) obj[0];
						mapCompetencyScoreTotal.put( tDetail.getTeacherId(), obj);
					}
					mapCompetencyScore.put(competencyMaster.getCompetencyId(), mapCompetencyScoreTotal);
				}
				
				List<ObjectiveMaster> objectiveMasterList = null;
				objectiveMasterList = teacherAssessmentQuestionDAO.findTeacherObjective(teacherAssessmentdetail);
				Map<Integer,Map<Integer, Object>> mapObjectiveScore = new HashMap<Integer, Map<Integer,Object>>();
				Map<Integer, Object> mapObjectiveScoreTotal = null;
				for(ObjectiveMaster objectiveMaster : objectiveMasterList){
					mapObjectiveScoreTotal = new HashMap<Integer, Object>();
					if(teacherAssessmentdetail!=null && teacherAssessmentdetail.getAssessmentDetail()!=null && teacherAssessmentdetail.getAssessmentType()!=null)
						lstAns = teacherAnswerDetailDAO.findSumOfAnsByObjectiveAndTeacherList(teacherDetailList, objectiveMaster, teacherAssessmentdetail);
					
					for(Object oo: lstAns){
						Object obj[] = (Object[])oo;
						tDetail = (TeacherDetail) obj[0];
						mapObjectiveScoreTotal.put(tDetail.getTeacherId(), obj);
					}
					mapObjectiveScore.put(objectiveMaster.getObjectiveId(), mapObjectiveScoreTotal);
				}
				
				RawDataForDomain rawdatafordomain = null;
				RawDataForCompetency rawDataForCompetency = null;
				RawDataForObjective rawDataForObjective = null;
				StatelessSession statelesssession = sessionFactory.openStatelessSession();
				Transaction tx = statelesssession.beginTransaction();
				System.out.println("************************** raw data inserting start **************************");
				for(TeacherDetail teacherDetail : teacherDetailList){
					for(DomainMaster dm: domainMasterList){				
						Object obj[] = (Object[]) mapDomainScore.get(dm.getDomainId()).get(teacherDetail.getTeacherId());							
						if(obj!=null){					 
							rawdatafordomain = new RawDataForDomain();
							rawdatafordomain.setTeacherDetail(teacherDetail);
							rawdatafordomain.setCreatedDateTime(new Date());
//							rawdatafordomain.setCreatedDateTime(assessmentCompletedDateTime);
							rawdatafordomain.setDomainMaster(dm);
							rawdatafordomain.setScore(new Double(""+obj[1]));
							rawdatafordomain.setMaxMarks(new Double(""+obj[2]));
							rawdatafordomain.setAssessmentDetail(teacherAssessmentdetail.getAssessmentDetail());
							rawdatafordomain.setAssessmentType(teacherAssessmentdetail.getAssessmentType());
							rawdatafordomain.setAssessmentTakenCount(teacherAssessmentdetail.getAssessmentTakenCount());
							rawdatafordomain.setTeacherAssessmentdetail(teacherAssessmentdetail);
							statelesssession.insert(rawdatafordomain);	
						}				
					}
					for(CompetencyMaster competencyMaster : competencyMastersList){
						Object obj[] = (Object[]) mapCompetencyScore.get(competencyMaster.getCompetencyId()).get(teacherDetail.getTeacherId());							
						if(obj!=null){					 
							rawDataForCompetency = new RawDataForCompetency();
							rawDataForCompetency.setTeacherDetail(teacherDetail);
							rawDataForCompetency.setCreatedDateTime(new Date());
//							rawDataForCompetency.setCreatedDateTime(assessmentCompletedDateTime);
							rawDataForCompetency.setDomainMaster(competencyMaster.getDomainMaster());
							rawDataForCompetency.setCompetencyMaster(competencyMaster);
							rawDataForCompetency.setScore(new Double(""+obj[1]));
							rawDataForCompetency.setMaxMarks(new Double(""+obj[2]));
							rawDataForCompetency.setAssessmentDetail(teacherAssessmentdetail.getAssessmentDetail());
							rawDataForCompetency.setAssessmentType(teacherAssessmentdetail.getAssessmentType());
							rawDataForCompetency.setAssessmentTakenCount(teacherAssessmentdetail.getAssessmentTakenCount());
							rawDataForCompetency.setTeacherAssessmentdetail(teacherAssessmentdetail);
							statelesssession.insert(rawDataForCompetency);
						}		
					}
					for(ObjectiveMaster objectiveMaster : objectiveMasterList){
						Object obj[] = (Object[]) mapObjectiveScore.get(objectiveMaster.getObjectiveId()).get(teacherDetail.getTeacherId());
						if(obj!=null){
							rawDataForObjective = new RawDataForObjective();
							rawDataForObjective.setTeacherDetail(teacherDetail);
							rawDataForObjective.setTeacherAssessmentdetail(teacherAssessmentdetail);
							rawDataForObjective.setDomainMaster(objectiveMaster.getCompetencyMaster().getDomainMaster());
							rawDataForObjective.setCompetencyMaster(objectiveMaster.getCompetencyMaster());
							rawDataForObjective.setObjectiveMaster(objectiveMaster);
							rawDataForObjective.setScore(new Double(""+obj[1]));
							rawDataForObjective.setMaxMarks(new Double(""+obj[2]));
							rawDataForObjective.setAssessmentDetail(teacherAssessmentdetail.getAssessmentDetail());
							rawDataForObjective.setAssessmentType(teacherAssessmentdetail.getAssessmentType());
							rawDataForObjective.setAssessmentTakenCount(teacherAssessmentdetail.getAssessmentTakenCount());
							rawDataForObjective.setCreatedDateTime(new Date());
//							rawDataForObjective.setCreatedDateTime(assessmentCompletedDateTime);
							statelesssession.insert(rawDataForObjective);
						}
					}
				}
				tx.commit();
				statelesssession.close();
				System.out.println("************************** raw data inserting end **************************");
				
				System.out.println("************************** assessment data inserting start **************************");
				statelesssession = sessionFactory.openStatelessSession();
				tx = statelesssession.beginTransaction();
				Integer lookupId = teacherAssessmentdetail.getScoreLookupMaster().getLookupId();
				System.out.println("lookupId == "+lookupId);
//				RawDataForDomainDAO rawDataForDomainDAO =  reportService.getRawDataForDomainDAO();
				List<RawDataForDomain> lstRawDataForDomains = rawDataForDomainDAO.findAllInCurrentYearByTeacher(teacherDetailList, teacherAssessmentdetail);
				System.out.println("lstRawDataForDomains.size() == "+lstRawDataForDomains.size());
				String sql = "";
				int i=0;
				Map<Integer, RawDataForDomain> rawDataForDomainMap = new HashMap<Integer, RawDataForDomain>();
				Map<Integer, RawDataForCompetency> rawDataForCompetencyMap = new HashMap<Integer, RawDataForCompetency>();
				RawDataForDomain rawDataForDomainObj = null;
				RawDataForCompetency rawDataForCompetencyObj = null;
				int size = lstRawDataForDomains.size();
				for (RawDataForDomain rawDataForDomain : lstRawDataForDomains) {
					rawDataForDomainMap.put(rawDataForDomain.getDomainMaster().getDomainId(), rawDataForDomain);
					if(i<size-1)
						sql+="(domainId = "+rawDataForDomain.getDomainMaster().getDomainId()+" AND totalscore = "+rawDataForDomain.getScore()+" AND lookupId = "+lookupId+") OR ";
					else
						sql+="(domainId = "+rawDataForDomain.getDomainMaster().getDomainId()+" AND totalscore = "+rawDataForDomain.getScore()+" AND lookupId = "+lookupId+")";
					i++;
				}
				System.out.println("sql domain == "+sql);
				DomainMaster domainMaster = null;
//				ScoreLookupDAO scoreLookupDAO = reportService.getScoreLookupDAO();
//				TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO = reportService.getTmpPercentileWiseZScoreDAO();
				String isPassFail=null;
				List one = scoreLookupDAO.getCandidatesNormScoreDomainWise(sql);
				System.out.println("one.size() == "+one.size());
//				AssessmentDomainScoreDAO assessmentDomainScoreDAO = reportService.getAssessmentDomainScoreDAO();
				for (Iterator iterator = one.iterator(); iterator.hasNext();) {
					Object object[] = (Object[]) iterator.next();
					rawDataForDomainObj = new RawDataForDomain();
					rawDataForDomainObj = rawDataForDomainMap.get(object[0]);
					AssessmentDomainScore assessmentDomainScore = new AssessmentDomainScore();
					domainMaster = new DomainMaster();
					domainMaster.setDomainId((Integer) object[0]);
					assessmentDomainScore.setDomainMaster(domainMaster);
					assessmentDomainScore.setTeacherDetail(teacherDetailList.get(0));
					assessmentDomainScore.setPercentile((Double) object[1]);
					assessmentDomainScore.setScoreLookupMaster(teacherAssessmentdetail.getScoreLookupMaster());
					assessmentDomainScore.setZscore((Double) object[2]);
					assessmentDomainScore.setTscore((Double) object[3]);
					assessmentDomainScore.setNcevalue((Double) object[4]);
					assessmentDomainScore.setNormscore((Double) object[5]);
					assessmentDomainScore.setRitvalue((Double) object[6]);
					isPassFail = (String) object[7];
					System.out.println("isPassFail : "+isPassFail);
					assessmentDomainScore.setAssessmentDetail(teacherAssessmentdetail.getAssessmentDetail());
					assessmentDomainScore.setAssessmentType(teacherAssessmentdetail.getAssessmentType());
					assessmentDomainScore.setCreatedDateTime(new Date());
//					assessmentDomainScore.setCreatedDateTime(assessmentCompletedDateTime);
					assessmentDomainScore.setAssessmentTakenCount(teacherAssessmentdetail.getAssessmentTakenCount());
					assessmentDomainScore.setRawDataForDomain(rawDataForDomainObj);
					statelesssession.insert(assessmentDomainScore);
				}
				tx.commit();
				statelesssession.close();
				
				statelesssession = sessionFactory.openStatelessSession();
				tx = statelesssession.beginTransaction();
				TeacherNormScore teacherNormScore = new TeacherNormScore();
				if(teacherAssessmentdetail.getAssessmentType()==1)
				{
					List assessmentDomainData = new ArrayList();
					assessmentDomainData = assessmentDomainScoreDAO.calculateCandidatesNormScore(teacherDetailList, teacherAssessmentdetail);
					System.out.println("assessmentDomainData.size() == "+assessmentDomainData.size());
					CGReportService cGReportService = new CGReportService();
					
					if(tmpPercentileWiseZScoreList==null)
					{
						tmpPercentileWiseZScoreList = tmpPercentileWiseZScoreDAO.findByCriteria(Order.asc("zScore"));
						System.out.println("tmpPercentileWiseZScoreList.size() == "+tmpPercentileWiseZScoreList.size());
					}
					if(pztList==null)
					{
						pztList = cGReportService.populateZScoreMap(tmpPercentileWiseZScoreList);
						System.out.println("pztList.size() == "+pztList.size());
					}
					
					for(Object oo: assessmentDomainData){
						Object obj[] = (Object[])oo;
						System.out.println("obj[0]:: "+obj[0]);
						System.out.println("obj[1]:: "+obj[1]);
						Double normscoreD = (Double)obj[0];
						Integer normscore = Integer.valueOf((int) Math.round(normscoreD));
						teacherNormScore.setTeacherNormScore( normscore);
						double percentile=cGReportService.getPercentile(pztList,normscore);
						teacherNormScore.setPercentile(percentile);
						Object[] ob =cGReportService.getDeciles(percentile);
						teacherNormScore.setDecileColor(""+ob[0]);
						teacherNormScore.setMinDecileValue((Double)ob[1]);
						teacherNormScore.setMaxDecileValue((Double)ob[2]);
					}
					teacherNormScore.setTeacherDetail(teacherDetailList.get(0));
					teacherNormScore.setTeacherAssessmentdetail(teacherAssessmentdetail);
					teacherNormScore.setCreatedDateTime(new Date());
//					teacherNormScore.setCreatedDateTime(assessmentCompletedDateTime);
					statelesssession.insert(teacherNormScore);
				}
				
//				RawDataForCompetencyDAO rawDataForCompetencyDAO =  reportService.getRawDataForCompetencyDAO();
				List<RawDataForCompetency> lstRawDataForCompetency = rawDataForCompetencyDAO.findAllInCurrentYearByTeacher(teacherDetailList, teacherAssessmentdetail);
				System.out.println("lstRawDataForCompetency.size() == "+lstRawDataForCompetency.size());
				String sqlComp = "";
				int j=0;
				int size1 = lstRawDataForCompetency.size();
				for (RawDataForCompetency rawDataForCompetency1 : lstRawDataForCompetency) {
					rawDataForCompetencyMap.put(rawDataForCompetency1.getCompetencyMaster().getCompetencyId(), rawDataForCompetency1);
					if(j<size1-1)
						sqlComp+="(competencyId = "+rawDataForCompetency1.getCompetencyMaster().getCompetencyId()+" AND totalscore = "+rawDataForCompetency1.getScore()+" AND lookupId = "+lookupId+") OR ";
					else
						sqlComp+="(competencyId = "+rawDataForCompetency1.getCompetencyMaster().getCompetencyId()+" AND totalscore = "+rawDataForCompetency1.getScore()+" AND lookupId = "+lookupId+")";
					j++;
				}
				System.out.println("sqlComp == "+sqlComp);
				CompetencyMaster competencyMaster = null;
				List two = scoreLookupDAO.getCandidatesNormScoreCompetencyWise(sqlComp);
				System.out.println("two.size() == "+two.size());
				for (Iterator iterator = two.iterator(); iterator.hasNext();) {
					Object object[] = (Object[]) iterator.next();
					rawDataForCompetencyObj = new RawDataForCompetency();
					rawDataForCompetencyObj = rawDataForCompetencyMap.get(object[0]);
					AssessmentCompetencyScore assessmentCompetencyScore = new AssessmentCompetencyScore();
					competencyMaster = new CompetencyMaster();
					competencyMaster.setCompetencyId((Integer) object[0]);
					assessmentCompetencyScore.setCompetencyMaster(competencyMaster);
					assessmentCompetencyScore.setTeacherDetail(teacherDetailList.get(0));
					assessmentCompetencyScore.setPercentile((Double) object[1]);
					assessmentCompetencyScore.setScoreLookupMaster(teacherAssessmentdetail.getScoreLookupMaster());
					assessmentCompetencyScore.setZscore((Double) object[2]);
					assessmentCompetencyScore.setTscore((Double) object[3]);
					assessmentCompetencyScore.setNcevalue((Double) object[4]);
					assessmentCompetencyScore.setNormscore((Double) object[5]);
					assessmentCompetencyScore.setRitvalue((Double) object[6]);
					assessmentCompetencyScore.setAssessmentDetail(teacherAssessmentdetail.getAssessmentDetail());
					assessmentCompetencyScore.setAssessmentType(teacherAssessmentdetail.getAssessmentType());
					assessmentCompetencyScore.setCreatedDateTime(new Date());
//					assessmentCompetencyScore.setCreatedDateTime(assessmentCompletedDateTime);
					assessmentCompetencyScore.setAssessmentTakenCount(teacherAssessmentdetail.getAssessmentTakenCount());
					assessmentCompetencyScore.setRawDataForCompetency(rawDataForCompetencyObj);
					statelesssession.insert(assessmentCompetencyScore);
				}
				tx.commit();
				statelesssession.close();
				System.out.println("************************** assessment data inserting end **************************");
				//check for fail and pass for SP(Smart Practices)	
				if(teacherAssessmentdetail.getAssessmentType()==3)
				{
					if(isPassFail!=null && !isPassFail.equals("")){
						if(isPassFail.equals("0"))
							isPassFail="F";
						else if(isPassFail.equals("1"))
							isPassFail="P";
					}
					else if(isPassFail.equals(""))
						isPassFail = null;
					System.out.println("isPassFail : "+isPassFail);
					teacherAssessmentStatus.setPass(isPassFail);
				}
				teacherAssessmentStatus.setCgUpdated(true);
//				TeacherAssessmentStatusDAO teacherAssessmentStatusDAO =  reportService.getTeacherAssessmentStatusDAO();
				teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);
				System.out.println("************************** status update for teacherAssessmentId : "+teacherAssessmentStatus.getTeacherAssessmentdetail().getTeacherAssessmentId()+" **************************");
				output.println("data update for teacherAssessmentId : "+teacherAssessmentStatus.getTeacherAssessmentdetail().getTeacherAssessmentId());
		 	}
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
			System.out.println("************* End ****************");
		}
		return null;
	}
	@RequestMapping(value="/service/checkDB.do", method=RequestMethod.GET)
	public String doCheckDB(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("/service/checkDB.do");
		PrintWriter pw = null; 
		try {
			pw = response.getWriter();
			List<CurrencyMaster> currencyList = currencyMasterDAO.findAllCurrencyByOrder();			
			if(currencyList.size()>0)
			{
				pw.write("<body bgcolor=\"green\" style=\"color: white;\">");
				pw.write("DB is up");
				pw.write("</body");
			}
			
		} catch (Exception e){
			pw.write("<body bgcolor=\"red\">");
			pw.write("<audio controls autoplay><source src=\"alert_red.mp3\" type=\"audio/mpeg\"> Your browser does not support the audio element.</audio></BR></BR>");
			pw.write("<font size=\"5\">DB is Down, Need to verify log !!!</BR> </BR></font>");
			pw.write("</body>");
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}
	
	
	@RequestMapping(value="/service/referencemissing.do", method=RequestMethod.GET)
	public String doReferenceMissing(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("/service/doReferenceMissing.do");
		
		PrintWriter pw = null;
		String password=request.getParameter("p");
		if(password!=null && !password.equals("") && password.equalsIgnoreCase("4G"))
		{
			try {
				pw = response.getWriter();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			pw.write("Reference File Missing Validation\n");
			System.out.println("Reference File Missing Validation");
			
			int iCounterTotal=0,iCounterMissing=0,iProcessingCounter=0;
			Criterion criterion1 = Restrictions.isNotNull("pathOfReference");
			
			List<TeacherElectronicReferences> lstERef=teacherElectronicReferencesDAO.findByCriteria(criterion1);
			if(lstERef!=null && lstERef.size()>0)
			{
				SessionFactory factory=teacherElectronicReferencesDAO.getSessionFactory();
				StatelessSession statelessSession=factory.openStatelessSession();
				Transaction transaction=statelessSession.beginTransaction();
				
				iCounterTotal=lstERef.size();
				iCounterMissing=0;
				iProcessingCounter=0;
				
				System.out.println("iCounterTotal "+iCounterTotal);
				
				String outputFile = "EReference"+Utility.getDateTime()+".csv";
				boolean alreadyExists = new File(outputFile).exists();
				
				CsvWriter csvOutput=null;	
				try {
					csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');
					if (!alreadyExists)
					{
						csvOutput.write("TeacherId");
						csvOutput.write("FirstName");
						csvOutput.write("LastName");
						csvOutput.write("EmailId");
						csvOutput.write("OldFileName");
						csvOutput.write("UpdatedRowId");
						csvOutput.endRecord();
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				String sFileName="",sFirstName="",sLastName="",sEmail="";
				Integer iTeacherId=null;
				for(TeacherElectronicReferences reference:lstERef)
				{
					iProcessingCounter++;
					System.out.println("Processing "+iProcessingCounter+" of "+iCounterTotal);
					
					try 
					{
						sFileName= reference.getPathOfReference();
						iTeacherId=reference.getTeacherDetail().getTeacherId();
						sFirstName=reference.getTeacherDetail().getFirstName();
						sLastName=reference.getTeacherDetail().getLastName();
						sEmail=reference.getTeacherDetail().getEmailAddress();
						
						if(sFileName!=null && !sFileName.equals("") && iTeacherId!=null && iTeacherId>0)
						{
							String sSaveLocation = Utility.getValueOfPropByKey("teacherRootPath")+""+iTeacherId+"/"+sFileName;
					        File fileSaveLocation = new File(sSaveLocation);
					        if(!fileSaveLocation.exists())
					        {
					        	iCounterMissing++;
					        	pw.write(iCounterMissing+" of "+iCounterTotal+" [ "+iTeacherId+","+sFirstName+","+sLastName+","+sEmail+" ] Reference File is missing in NFS ["+sSaveLocation+"] \n");
					        	System.out.println(iCounterMissing+" of "+iCounterTotal+" [ "+iTeacherId+","+sFirstName+","+sLastName+","+sEmail+" ] Reference File is missing in NFS ["+sSaveLocation+"] ");
					        	
					        	try {
					        		csvOutput.write(iTeacherId+"");
									csvOutput.write(sFirstName);
									csvOutput.write(sLastName);
									csvOutput.write(sEmail);
									csvOutput.write(sSaveLocation);
									csvOutput.write(reference.getElerefAutoId()+"");
									csvOutput.endRecord();
								} catch (Exception e) {
									e.printStackTrace();
								}
					        	
					        	transaction=statelessSession.beginTransaction();
					        	reference.setPathOfReference(null);
					        	try {
									statelessSession.update(reference);
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								try {
									if(iCounterMissing%20==0)
									{
										transaction.commit();
										transaction=statelessSession.beginTransaction();
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
					        	
					        }
						}
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
					
				}
				
				try {
					csvOutput.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try {
					transaction.commit();
					statelessSession.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
				
			}
			else
			{
				pw.write("No any Reference File In DB\n");
			}
			
			
		}
		
		return null;
	}
	
	
	
	@RequestMapping(value="/service/resumemissing.do", method=RequestMethod.GET)
	public String doResumeMissing(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("/service/doResumeMissing.do");
		
		PrintWriter pw = null;
		String password=request.getParameter("p");
		if(password!=null && !password.equals("") && password.equalsIgnoreCase("4G"))
		{
			try {
				pw = response.getWriter();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			pw.write("Resume File Missing Validation\n");
			System.out.println("Resume File Missing Validation");
			
			int iCounterTotal=0,iCounterMissing=0,iProcessingCounter=0;
			Criterion criterion1 = Restrictions.isNotNull("resume");
			List<TeacherExperience> lstExp=teacherExperienceDAO.findByCriteria(criterion1);
			if(lstExp!=null && lstExp.size()>0)
			{
				
				SessionFactory factory=teacherExperienceDAO.getSessionFactory();
				StatelessSession statelessSession=factory.openStatelessSession();
				Transaction transaction=statelessSession.beginTransaction();
				
				iCounterTotal=lstExp.size();
				iCounterMissing=0;
				iProcessingCounter=0;
				
				System.out.println("iCounterTotal "+iCounterTotal);
				
				String outputFile = "Resume"+Utility.getDateTime()+".csv";
				boolean alreadyExists = new File(outputFile).exists();
				CsvWriter csvOutput=null;	
				try {
					csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');
					if (!alreadyExists)
					{
						csvOutput.write("TeacherId");
						csvOutput.write("FirstName");
						csvOutput.write("LastName");
						csvOutput.write("EmailId");
						csvOutput.write("OldFileName");
						csvOutput.write("UpdatedRowId");
						csvOutput.endRecord();
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				String sFileName="",sFirstName="",sLastName="",sEmail="";
				Integer iTeacherId=null;
				for(TeacherExperience objExp:lstExp)
				{
					iProcessingCounter++;
					System.out.println("Processing "+iProcessingCounter+" of "+iCounterTotal);
					
					try 
					{
						sFileName= objExp.getResume();
						iTeacherId=objExp.getTeacherId().getTeacherId();
						sFirstName=objExp.getTeacherId().getFirstName();
						sLastName=objExp.getTeacherId().getLastName();
						sEmail=objExp.getTeacherId().getEmailAddress();
						
						if(sFileName!=null && !sFileName.equals("") && iTeacherId!=null && iTeacherId>0)
						{
							String sSaveLocation = Utility.getValueOfPropByKey("teacherRootPath")+""+iTeacherId+"/"+sFileName;
					        File fileSaveLocation = new File(sSaveLocation);
					        if(!fileSaveLocation.exists())
					        {
					        	iCounterMissing++;
					        	pw.write(iCounterMissing+" of "+iCounterTotal+" [ "+iTeacherId+","+sFirstName+","+sLastName+","+sEmail+" ] Resume File is missing in NFS ["+sSaveLocation+"] \n");
					        	System.out.println(iCounterMissing+" of "+iCounterTotal+" [ "+iTeacherId+","+sFirstName+","+sLastName+","+sEmail+" ] Resume File is missing in NFS ["+sSaveLocation+"] ");
					        	
					        	try {
					        		csvOutput.write(iTeacherId+"");
									csvOutput.write(sFirstName);
									csvOutput.write(sLastName);
									csvOutput.write(sEmail);
									csvOutput.write(sSaveLocation);
									csvOutput.write(objExp.getExpId()+"");
									csvOutput.endRecord();
								} catch (Exception e) {
									e.printStackTrace();
								}
								
					        	transaction=statelessSession.beginTransaction();
					        	
					        	objExp.setResume(null);
					        	try {
									statelessSession.update(objExp);
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								try {
									if(iCounterMissing%20==0)
									{
										transaction.commit();
										transaction=statelessSession.beginTransaction();
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
					        }
						}
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
					
				}
				
				try {
					csvOutput.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try {
					transaction.commit();
					statelessSession.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
			}
			else
			{
				pw.write("No any Resume File In DB\n");
			}
			
			
		}
		
		return null;
	}
	
	
	@RequestMapping(value="/service/academictranscriptmissing.do", method=RequestMethod.GET)
	public String doAcademicTranscriptMissing(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("/service/academictranscriptmissing.do");
		
		PrintWriter pw = null;
		String password=request.getParameter("p");
		if(password!=null && !password.equals("") && password.equalsIgnoreCase("4G"))
		{
			try {
				pw = response.getWriter();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			pw.write("Academic Transcript File Missing Validation\n");
			System.out.println("Academic Transcript File Missing Validation");
			
			int iCounterTotal=0,iCounterMissing=0,iProcessingCounter=0;
			Criterion criterion1 = Restrictions.isNotNull("pathOfTranscript");
			List<TeacherAcademics> lstExp=teacherAcademicsDAO.findByCriteria(criterion1);
			if(lstExp!=null && lstExp.size()>0)
			{
				
				SessionFactory factory=teacherAcademicsDAO.getSessionFactory();
				StatelessSession statelessSession=factory.openStatelessSession();
				Transaction transaction=statelessSession.beginTransaction();
				
				iCounterTotal=lstExp.size();
				iCounterMissing=0;
				iProcessingCounter=0;
				
				System.out.println("iCounterTotal "+iCounterTotal);
				
				String outputFile = "Academics"+Utility.getDateTime()+".csv";
				boolean alreadyExists = new File(outputFile).exists();
				CsvWriter csvOutput=null;	
				try {
					csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');
					if (!alreadyExists)
					{
						csvOutput.write("TeacherId");
						csvOutput.write("FirstName");
						csvOutput.write("LastName");
						csvOutput.write("EmailId");
						csvOutput.write("OldFileName");
						csvOutput.write("UpdatedRowId");
						csvOutput.endRecord();
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				String sFileName="",sFirstName="",sLastName="",sEmail="";
				Integer iTeacherId=null;
				for(TeacherAcademics objaca:lstExp)
				{
					iProcessingCounter++;
					System.out.println("Processing "+iProcessingCounter+" of "+iCounterTotal);
					
					try 
					{
						sFileName= objaca.getPathOfTranscript();
						iTeacherId=objaca.getTeacherId().getTeacherId();
						sFirstName=objaca.getTeacherId().getFirstName();
						sLastName=objaca.getTeacherId().getLastName();
						sEmail=objaca.getTeacherId().getEmailAddress();
						
						if(sFileName!=null && !sFileName.equals("") && iTeacherId!=null && iTeacherId>0)
						{
							String sSaveLocation = Utility.getValueOfPropByKey("teacherRootPath")+""+iTeacherId+"/"+sFileName;
					        File fileSaveLocation = new File(sSaveLocation);
					        if(!fileSaveLocation.exists())
					        {
					        	iCounterMissing++;
					        	pw.write(iCounterMissing+" of "+iCounterTotal+" [ "+iTeacherId+","+sFirstName+","+sLastName+","+sEmail+" ] Academic Transcript File is missing in NFS ["+sSaveLocation+"] \n");
					        	System.out.println(iCounterMissing+" of "+iCounterTotal+" [ "+iTeacherId+","+sFirstName+","+sLastName+","+sEmail+" ] Academic Transcript File is missing in NFS ["+sSaveLocation+"] ");
					        	
					        	try {
					        		csvOutput.write(iTeacherId+"");
									csvOutput.write(sFirstName);
									csvOutput.write(sLastName);
									csvOutput.write(sEmail);
									csvOutput.write(sSaveLocation);
									csvOutput.write(objaca.getAcademicId()+"");
									csvOutput.endRecord();
								} catch (Exception e) {
									e.printStackTrace();
								}
								
					        	transaction=statelessSession.beginTransaction();
					        	
					        	objaca.setPathOfTranscript(null);
					        	try {
									statelessSession.update(objaca);
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								try {
									if(iCounterMissing%20==0)
									{
										transaction.commit();
										transaction=statelessSession.beginTransaction();
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								
					        }
						}
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
					
				}
				
				try {
					csvOutput.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try {
					transaction.commit();
					statelessSession.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			else
			{
				pw.write("No any Academic Transcript File In DB\n");
			}
			
			
		}
		
		return null;
	}
	
	
	
	@RequestMapping(value="/service/certmissing.do", method=RequestMethod.GET)
	public String doCertMissing(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("/service/certmissing.do");
		
		PrintWriter pw = null;
		String password=request.getParameter("p");
		if(password!=null && !password.equals("") && password.equalsIgnoreCase("4G"))
		{
			try {
				pw = response.getWriter();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			pw.write("PathOfCertification File Missing Validation\n");
			System.out.println("PathOfCertification File Missing Validation");
			
			int iCounterTotal=0,iCounterMissing=0,iProcessingCounter=0;
			Criterion criterion1 = Restrictions.isNotNull("pathOfCertification");
			List<TeacherCertificate> lstCert=null;
			
			try {
				lstCert=teacherCertificateDAO.findByCriteria(criterion1);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			if(lstCert!=null && lstCert.size()>0)
			{
				
				SessionFactory factory=teacherCertificateDAO.getSessionFactory();
				StatelessSession statelessSession=factory.openStatelessSession();
				Transaction transaction=statelessSession.beginTransaction();
				
				iCounterTotal=lstCert.size();
				iCounterMissing=0;
				iProcessingCounter=0;
				
				System.out.println("iCounterTotal "+iCounterTotal);
				
				String outputFile = "Certificate"+Utility.getDateTime()+".csv";
				boolean alreadyExists = new File(outputFile).exists();
				CsvWriter csvOutput=null;	
				try {
					csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');
					if (!alreadyExists)
					{
						csvOutput.write("TeacherId");
						csvOutput.write("FirstName");
						csvOutput.write("LastName");
						csvOutput.write("EmailId");
						csvOutput.write("OldFileName");
						csvOutput.write("UpdatedRowId");
						csvOutput.endRecord();
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				String sFileName="",sFirstName="",sLastName="",sEmail="";
				Integer iTeacherId=null;
				for(TeacherCertificate objCert:lstCert)
				{
					iProcessingCounter++;
					System.out.println("Processing "+iProcessingCounter+" of "+iCounterTotal);
					
					try 
					{
						sFileName= objCert.getPathOfCertification();
						iTeacherId=objCert.getTeacherDetail().getTeacherId();
						sFirstName=objCert.getTeacherDetail().getFirstName();
						sLastName=objCert.getTeacherDetail().getLastName();
						sEmail=objCert.getTeacherDetail().getEmailAddress();
						if(sFileName!=null && !sFileName.equals("") && iTeacherId!=null && iTeacherId>0)
						{
							String sSaveLocation = Utility.getValueOfPropByKey("teacherRootPath")+""+iTeacherId+"/"+sFileName;
					        File fileSaveLocation = new File(sSaveLocation);
					        if(!fileSaveLocation.exists())
					        {
					        	iCounterMissing++;
					        	pw.write(iCounterMissing+" of "+iCounterTotal+" [ "+iTeacherId+","+sFirstName+","+sLastName+","+sEmail+" ] PathOfCertification File is missing in NFS ["+sSaveLocation+"] \n");
					        	System.out.println(iCounterMissing+" of "+iCounterTotal+" ["+iTeacherId+","+sFirstName+","+sLastName+","+sEmail+" ] PathOfCertification File is missing in NFS ["+sSaveLocation+"] ");
					        	
					        	
					        	try {
					        		csvOutput.write(iTeacherId+"");
									csvOutput.write(sFirstName);
									csvOutput.write(sLastName);
									csvOutput.write(sEmail);
									csvOutput.write(sSaveLocation);
									csvOutput.write(objCert.getCertId()+"");
									csvOutput.endRecord();
								} catch (Exception e) {
									e.printStackTrace();
								}
								
					        	transaction=statelessSession.beginTransaction();
					        	
					        	objCert.setPathOfCertification(null);
					        	try {
									statelessSession.update(objCert);
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								try {
									if(iCounterMissing%20==0)
									{
										transaction.commit();
										transaction=statelessSession.beginTransaction();
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								
					        }
						}
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
					
				}
				
				try {
					csvOutput.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try {
					transaction.commit();
					statelessSession.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			else
			{
				pw.write("No any PathOfCertification File In DB\n");
			}
			
			
		}
		
		return null;
	}
	
	
	@RequestMapping(value="/service/validateqq.do", method=RequestMethod.GET)
	public String doValidateQQ(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("/service/validateqq.do");
		
		PrintWriter pw = null;
		String password=request.getParameter("p");
		if(password!=null && !password.equals("") && password.equalsIgnoreCase("4G"))
		{
			try {
				pw = response.getWriter();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			pw.write("Validate QQ\n");
			System.out.println("Validate QQ");
			
			Map<String, List<TeacherAnswerDetailsForDistrictSpecificQuestions>> mapQQAnswerByTeacherAndJob=new HashMap<String, List<TeacherAnswerDetailsForDistrictSpecificQuestions>>();
			
			JobCategoryMaster jobCategoryMaster=null;
			List<JobCategoryMaster> lstjobCategoryMaster=new ArrayList<JobCategoryMaster>();
			List<JobCategoryTransaction> lstJobCategoryTransaction=jobCategoryTransactionDAO.findAll();
			System.out.println("Step 01");
			if(lstJobCategoryTransaction!=null && lstJobCategoryTransaction.size()>0)
			{
				System.out.println("lstJobCategoryTransaction size "+lstJobCategoryTransaction.size());
				for(JobCategoryTransaction transaction :lstJobCategoryTransaction)
				{
					jobCategoryMaster=new JobCategoryMaster();
					jobCategoryMaster.setJobCategoryId(transaction.getJobCategoryId());
					lstjobCategoryMaster.add(jobCategoryMaster);
				}
				System.out.println("Step 02");
				if(lstjobCategoryMaster.size()>0)
				{
					System.out.println("lstjobCategoryMaster size "+lstjobCategoryMaster.size());
					Criterion criterion1 = Restrictions.in("jobCategoryMaster",lstjobCategoryMaster);
					List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
					listjobOrder = jobOrderDAO.findByCriteria(criterion1);
					System.out.println("Step 03");
					if(listjobOrder!=null && listjobOrder.size()>0)
					{
						System.out.println("listjobOrder size "+listjobOrder.size());
						Criterion criterion2 = Restrictions.in("jobOrder",listjobOrder);
						
						/*List<Order> lstOrder=new ArrayList<Order>();
						lstOrder.add(Order.asc("teacherDetail"));
						lstOrder.add(Order.asc("jobOrder"));
						lstOrder.add(Order.asc("questionSets"));
						lstOrder.add(Order.asc("districtSpecificQuestions"));*/
						System.out.println("Step 04");
						List<TeacherAnswerDetailsForDistrictSpecificQuestions> lstTeacherAnswerDetailsForDistrictSpecificQuestions=teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findByCriteria(criterion2);
						System.out.println("Step 05");
						if(lstTeacherAnswerDetailsForDistrictSpecificQuestions!=null && lstTeacherAnswerDetailsForDistrictSpecificQuestions.size()>0)
						{
							List<TeacherAnswerDetailsForDistrictSpecificQuestions> lsTemp=null;
							System.out.println("Step 06");
							for(TeacherAnswerDetailsForDistrictSpecificQuestions questionsans :lstTeacherAnswerDetailsForDistrictSpecificQuestions)
							{
								if(questionsans!=null && questionsans.getJobOrder()!=null && questionsans.getTeacherDetail()!=null)
								{
									String sTempKey=questionsans.getTeacherDetail().getTeacherId()+"_"+questionsans.getJobOrder().getJobId();
									
									System.out.println("sTempKey "+sTempKey +questionsans.getAnswerId());
									
									if(sTempKey!=null && !sTempKey.equals(""))
									{
										if(mapQQAnswerByTeacherAndJob.get(sTempKey)==null)
										{
											lsTemp=new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
											lsTemp.add(questionsans);
											mapQQAnswerByTeacherAndJob.put(sTempKey, lsTemp);
										}
										else
										{
											lsTemp=new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
											lsTemp=mapQQAnswerByTeacherAndJob.get(sTempKey);
											lsTemp.add(questionsans);
											mapQQAnswerByTeacherAndJob.put(sTempKey, lsTemp);
										}
									}
								}
							}
							System.out.println("Step 07");
						}
						
					}
					
					
					
				}
				
				
				
			}
			
			if(mapQQAnswerByTeacherAndJob!=null && mapQQAnswerByTeacherAndJob.size()>0)
			{
				
				
				
			}
			
			
			
			// Old Code ...
			/*int iCounterTotal=0,iCounterMissing=0,iProcessingCounter=0;
			Criterion criterion1 = Restrictions.isNotNull("pathOfCertification");
			List<TeacherCertificate> lstCert=null;
			
			try {
				lstCert=teacherCertificateDAO.findByCriteria(criterion1);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			if(lstCert!=null && lstCert.size()>0)
			{
				
				SessionFactory factory=teacherCertificateDAO.getSessionFactory();
				StatelessSession statelessSession=factory.openStatelessSession();
				Transaction transaction=statelessSession.beginTransaction();
				
				iCounterTotal=lstCert.size();
				iCounterMissing=0;
				iProcessingCounter=0;
				
				System.out.println("iCounterTotal "+iCounterTotal);
				
				String outputFile = "Certificate"+Utility.getDateTime()+".csv";
				boolean alreadyExists = new File(outputFile).exists();
				CsvWriter csvOutput=null;	
				try {
					csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');
					if (!alreadyExists)
					{
						csvOutput.write("TeacherId");
						csvOutput.write("FirstName");
						csvOutput.write("LastName");
						csvOutput.write("EmailId");
						csvOutput.write("OldFileName");
						csvOutput.write("UpdatedRowId");
						csvOutput.endRecord();
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				String sFileName="",sFirstName="",sLastName="",sEmail="";
				Integer iTeacherId=null;
				for(TeacherCertificate objCert:lstCert)
				{
					iProcessingCounter++;
					System.out.println("Processing "+iProcessingCounter+" of "+iCounterTotal);
					
					try 
					{
						sFileName= objCert.getPathOfCertification();
						iTeacherId=objCert.getTeacherDetail().getTeacherId();
						sFirstName=objCert.getTeacherDetail().getFirstName();
						sLastName=objCert.getTeacherDetail().getLastName();
						sEmail=objCert.getTeacherDetail().getEmailAddress();
						if(sFileName!=null && !sFileName.equals("") && iTeacherId!=null && iTeacherId>0)
						{
							String sSaveLocation = Utility.getValueOfPropByKey("teacherRootPath")+""+iTeacherId+"/"+sFileName;
					        File fileSaveLocation = new File(sSaveLocation);
					        if(!fileSaveLocation.exists())
					        {
					        	iCounterMissing++;
					        	pw.write(iCounterMissing+" of "+iCounterTotal+" [ "+iTeacherId+","+sFirstName+","+sLastName+","+sEmail+" ] PathOfCertification File is missing in NFS ["+sSaveLocation+"] \n");
					        	System.out.println(iCounterMissing+" of "+iCounterTotal+" ["+iTeacherId+","+sFirstName+","+sLastName+","+sEmail+" ] PathOfCertification File is missing in NFS ["+sSaveLocation+"] ");
					        	
					        	
					        	try {
					        		csvOutput.write(iTeacherId+"");
									csvOutput.write(sFirstName);
									csvOutput.write(sLastName);
									csvOutput.write(sEmail);
									csvOutput.write(sSaveLocation);
									csvOutput.write(objCert.getCertId()+"");
									csvOutput.endRecord();
								} catch (Exception e) {
									e.printStackTrace();
								}
								
					        	transaction=statelessSession.beginTransaction();
					        	
					        	objCert.setPathOfCertification(null);
					        	try {
									statelessSession.update(objCert);
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								try {
									if(iCounterMissing%20==0)
									{
										transaction.commit();
										transaction=statelessSession.beginTransaction();
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								
					        }
						}
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
					
				}
				
				try {
					csvOutput.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try {
					transaction.commit();
					statelessSession.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			else
			{
				pw.write("No any PathOfCertification File In DB\n");
			}*/
			
			
		}
		
		return null;
	}
	
	
	@RequestMapping(value="/service/validateqqbyd.do", method=RequestMethod.GET)
	public String doValidateQQByDistrict(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("/service/validateqqbyd.do");
		
		PrintWriter pw = null;
		String password=request.getParameter("p");
		String d=request.getParameter("d");
		if(password!=null && !password.equals("") && password.equalsIgnoreCase("4G"))
		{
			try {
				pw = response.getWriter();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			pw.write("Validate QQ\n");
			System.out.println("Validate QQ");
			
			Map<String, List<TeacherAnswerDetailsForDistrictSpecificQuestions>> mapQQAnswerByTeacherAndJob=new HashMap<String, List<TeacherAnswerDetailsForDistrictSpecificQuestions>>();
			Integer iDistrictId=Utility.getIntValue(d);
			System.out.println("iDistrictId "+iDistrictId);
			if(iDistrictId >0)
			{
				//System.out.println("Step 01");
				DistrictMaster districtMaster=districtMasterDAO.findById(iDistrictId, false, false);
				//System.out.println("Step 02");
				if(districtMaster!=null && districtMaster.getSelfServicePortfolioStatus()!=null && districtMaster.getSelfServicePortfolioStatus().equals(true))
				{
					Criterion criterion11 = Restrictions.eq("districtMaster",districtMaster);
					//System.out.println("Step 03");
					List<TeacherAnswerDetailsForDistrictSpecificQuestions> lstTeacherAnswerDetailsForDistrictSpecificQuestions=teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findByCriteria(criterion11);
					//System.out.println("Step 04");
					if(lstTeacherAnswerDetailsForDistrictSpecificQuestions!=null && lstTeacherAnswerDetailsForDistrictSpecificQuestions.size()>0)
					{
						List<TeacherAnswerDetailsForDistrictSpecificQuestions> lsTemp=null;
						//System.out.println("Step 05");
						for(TeacherAnswerDetailsForDistrictSpecificQuestions questionsans :lstTeacherAnswerDetailsForDistrictSpecificQuestions)
						{
							if(questionsans!=null && questionsans.getJobOrder()!=null && questionsans.getTeacherDetail()!=null)
							{
								String sTempKey=questionsans.getTeacherDetail().getTeacherId()+"_"+questionsans.getJobOrder().getJobId();
								
								//System.out.println("sTempKey "+sTempKey +" AnswerId "+questionsans.getAnswerId());
								
								if(sTempKey!=null && !sTempKey.equals(""))
								{
									if(mapQQAnswerByTeacherAndJob.get(sTempKey)==null)
									{
										lsTemp=new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
										lsTemp.add(questionsans);
										mapQQAnswerByTeacherAndJob.put(sTempKey, lsTemp);
									}
									else
									{
										lsTemp=new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
										lsTemp=mapQQAnswerByTeacherAndJob.get(sTempKey);
										lsTemp.add(questionsans);
										mapQQAnswerByTeacherAndJob.put(sTempKey, lsTemp);
									}
								}
							}
						}
						//System.out.println("Step 06");
					}
				}
			}
			
			if(mapQQAnswerByTeacherAndJob!=null && mapQQAnswerByTeacherAndJob.size()>0)
			{
				
				for (Map.Entry<String, List<TeacherAnswerDetailsForDistrictSpecificQuestions>> entry : mapQQAnswerByTeacherAndJob.entrySet()) 
				{
				    //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
				    
					//System.out.println("Key = " + entry.getKey());
				    Integer iTeacherId=null;
				    Integer iJobId=null;
					String sKey=entry.getKey();
					if(sKey!=null && sKey.contains("_"))
					{
						String arrTemp[]=sKey.split("_");
						//System.out.println("arrTemp "+arrTemp.length);
						
						if(arrTemp!=null && arrTemp.length==2)
						{
							iTeacherId=Utility.getIntValue(arrTemp[0]);
							iJobId=Utility.getIntValue(arrTemp[1]);
							//System.out.println("iTeacherId "+iTeacherId +" iJobId "+iJobId);
							
							List<TeacherAnswerDetailsForDistrictSpecificQuestions> questions=entry.getValue();
							if(questions!=null)
							{
								List<OptionsForDistrictSpecificQuestions> questionOptionsTemp=null;
								for(TeacherAnswerDetailsForDistrictSpecificQuestions qans:questions)
								{
									Map<String, Boolean> mapOpts=new HashMap<String, Boolean>();
									questionOptionsTemp=qans.getDistrictSpecificQuestions().getQuestionOptions();
									if(questionOptionsTemp!=null && questionOptionsTemp.size()>0)
									{
										mapOpts=new HashMap<String, Boolean>();
										//System.out.println("questionOptionsTemp "+questionOptionsTemp.size());
										for(OptionsForDistrictSpecificQuestions questions2:questionOptionsTemp)
										{
											//System.out.println("questions2 "+questions2.getValidOption() +" "+questions2.getDistrictSpecificQuestions().getQuestionId());
											mapOpts.put(questions2.getOptionId()+"", questions2.getValidOption());
										}
									}
									
									if(qans.getQuestionType()!=null && qans.getQuestionType().equalsIgnoreCase("et")) //Radio Button With Explanation Field
									{
										
										if(mapOpts!=null && mapOpts.get(qans.getSelectedOptions())!=null)
										{
											boolean bValidOpt=mapOpts.get(qans.getSelectedOptions());
											if(bValidOpt)
											{
												if(qans.getIsValidAnswer().equals(false))
												{
													System.out.println("iTeacherId "+iTeacherId +" iJobId "+iJobId +" AnswerId "+qans.getAnswerId()+" == 1");
												}
											}
											else
											{
												if(qans.getIsValidAnswer().equals(true))
												{
													System.out.println("iTeacherId "+iTeacherId +" iJobId "+iJobId +" AnswerId "+qans.getAnswerId()+" == 0");
												}
											}
										}
										
									}
									else if(qans.getQuestionType()!=null && qans.getQuestionType().equalsIgnoreCase("tf")) //Radio Buttons Pair
									{
										if(mapOpts!=null && mapOpts.get(qans.getSelectedOptions())!=null)
										{
											boolean bValidOpt=mapOpts.get(qans.getSelectedOptions());
											if(bValidOpt)
											{
												if(qans.getIsValidAnswer().equals(false))
												{
													System.out.println("iTeacherId "+iTeacherId +" iJobId "+iJobId +" AnswerId "+qans.getAnswerId()+" == 1");
												}
											}
											else
											{
												if(qans.getIsValidAnswer().equals(true))
												{
													System.out.println("iTeacherId "+iTeacherId +" iJobId "+iJobId +" AnswerId "+qans.getAnswerId()+" == 0");
												}
											}
										}
									}
									else if(qans.getQuestionType()!=null && qans.getQuestionType().equalsIgnoreCase("slsel")) //Multi-Choice Single Selection
									{
										if(mapOpts!=null && mapOpts.get(qans.getSelectedOptions())!=null)
										{
											boolean bValidOpt=mapOpts.get(qans.getSelectedOptions());
											if(bValidOpt)
											{
												if(qans.getIsValidAnswer().equals(false))
												{
													System.out.println("iTeacherId "+iTeacherId +" iJobId "+iJobId +" AnswerId "+qans.getAnswerId()+" == 1");
												}
											}
											else
											{
												if(qans.getIsValidAnswer().equals(true))
												{
													System.out.println("iTeacherId "+iTeacherId +" iJobId "+iJobId +" AnswerId "+qans.getAnswerId()+" == 0");
												}
											}
										}
									}
									else if(qans.getQuestionType()!=null && qans.getQuestionType().equalsIgnoreCase("mlsel")) //Multi-Choice Multiple Selections
									{
										/*if(mapOpts!=null && mapOpts.get(qans.getSelectedOptions())!=null)
										{
											boolean bValidOpt=mapOpts.get(qans.getSelectedOptions());
											if(bValidOpt)
											{
												if(qans.getIsValidAnswer().equals(false))
												{
													System.out.println("iTeacherId "+iTeacherId +" iJobId "+iJobId +" AnswerId "+qans.getAnswerId()+" == 1");
												}
											}
											else
											{
												if(qans.getIsValidAnswer().equals(true))
												{
													System.out.println("iTeacherId "+iTeacherId +" iJobId "+iJobId +" AnswerId "+qans.getAnswerId()+" == 0");
												}
											}
										}*/
									}
									else if(qans.getQuestionType()!=null && qans.getQuestionType().equalsIgnoreCase("ml")) //Multi Line Text
									{
										
									}
									
								}
							}
						}
						
					}
					
					
				}
				
			}
			
		}
		
		return null;
	}
	
	@RequestMapping(value="/service/addassessmentscore.do", method=RequestMethod.GET)
	public String doUpdateMissing(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("TestPageController : /service/addassessmentscore.do");
		
		PrintWriter pw = null;
		String password=request.getParameter("p");
		if(password!=null && !password.equals("") && password.equalsIgnoreCase("4G"))
		{
			try {
				pw = response.getWriter();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			pw.write("addassessmentscore.do\n");
			System.out.println("addassessmentscore.do");
			String tAId=request.getParameter("tAId");
			Integer teacherAssessmentId = Integer.parseInt(tAId);
			TeacherAssessmentdetail teacherAssessmentDetail = new TeacherAssessmentdetail();
			teacherAssessmentDetail.setTeacherAssessmentId(teacherAssessmentId);
			System.out.println("teacherAssessmentId : "+teacherAssessmentId);
			List<TeacherNormScore> teacherNormScores = teacherNormScoreDAO.findNormScoreByTeacherByList(teacherAssessmentDetail);
			if(teacherNormScores!=null && teacherNormScores.size()==0)
			{
				System.out.println("teacherNormScores.size() : "+teacherNormScores.size());
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentDetail);
				System.out.println("teacherAssessmentStatusList.size() : "+teacherAssessmentStatusList.size());
				TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
				System.out.println("teacherAssessmentStatus : "+teacherAssessmentStatus);
				String status = teacherAssessmentStatus.getStatusMaster().getStatusShortName();
				System.out.println("status : "+status);
				if(status.equalsIgnoreCase("comp"))
				{
					System.out.println("status :: "+status);
					CadidateRawScoreThread crt = new CadidateRawScoreThread();
					crt.setCandidateRawScoreDAO(candidateRawScoreDAO);
					crt.setTeacherAnswerDetailDAO(teacherAnswerDetailDAO);
					crt.setTeacherAssessmentQuestionDAO(teacherAssessmentQuestionDAO);
					crt.setTeacherDetail(teacherAssessmentStatus.getTeacherDetail());
					crt.setReportService(reportService);
					crt.setTeacherAssessmentdetail(teacherAssessmentStatus.getTeacherAssessmentdetail());
					crt.setTeacherAssessmentStatus(teacherAssessmentStatus);
					crt.start();
					System.out.println("CadidateRawScoreThread start");
				}
				System.out.println("addassessmentscore.do");
			}
		}
		return null;
	}
	
	@RequestMapping(value="/service/addnormscore.do", method=RequestMethod.GET)
	public String doAddnormScore(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("/service/addassessmentscore.do");
		
		PrintWriter pw = null;
		String password=request.getParameter("p");
		if(password!=null && !password.equals("") && password.equalsIgnoreCase("4G"))
		{
			try {
				pw = response.getWriter();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			pw.write("addassessmentscore.do\n");
			System.out.println("addassessmentscore.do");
			String tAId=request.getParameter("tAId");
			Integer teacherAssessmentId = Integer.parseInt(tAId);
			TeacherAssessmentdetail teacherAssessmentDetail = new TeacherAssessmentdetail();
			teacherAssessmentDetail.setTeacherAssessmentId(teacherAssessmentId);
			
			List<TeacherNormScore> teacherNormScores = teacherNormScoreDAO.findNormScoreByTeacherByList(teacherAssessmentDetail);
			TeacherNormScore teacherNormScore = new TeacherNormScore();
			if(teacherNormScores!=null && teacherNormScores.size()>0)
			{
				teacherNormScore = teacherNormScores.get(0);
			}
			List<TeacherAssessmentStatus> teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentDetail);
			TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
			String status = teacherAssessmentStatus.getStatusMaster().getStatusShortName();
			if(status.equalsIgnoreCase("comp"))
			{
				TeacherAssessmentdetail teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();
				if(teacherAssessmentdetail.getAssessmentType()==1)
				{
					List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
					teacherDetails.add(teacherAssessmentdetail.getTeacherDetail());
					
					List assessmentDomainData = new ArrayList();
					assessmentDomainData = assessmentDomainScoreDAO.calculateCandidatesNormScore(teacherDetails, teacherAssessmentdetail);
					System.out.println("assessmentDomainData.size() == "+assessmentDomainData.size());
					CGReportService cGReportService = new CGReportService();
					
					if(tmpPercentileWiseZScoreList==null)
					{
						tmpPercentileWiseZScoreList = tmpPercentileWiseZScoreDAO.findByCriteria(Order.asc("zScore"));
						System.out.println("tmpPercentileWiseZScoreList.size() == "+tmpPercentileWiseZScoreList.size());
					}
					if(pztList==null)
					{
						pztList = cGReportService.populateZScoreMap(tmpPercentileWiseZScoreList);
						System.out.println("pztList.size() == "+pztList.size());
					}
					List<RawDataForDomain> rawDataForDomains =  rawDataForDomainDAO.findAllInCurrentYearByTeacher(teacherDetails, teacherAssessmentdetail);
					
					Date assessmentDateTime = null;
					
					if(rawDataForDomains!=null && rawDataForDomains.size()>0)
					{
						assessmentDateTime = rawDataForDomains.get(0).getAssessmentDateTime();
					}
					//TeacherNormScoreDAO teacherNormScoreDAO = reportService.getTeacherNormScoreDAO();
					for(Object oo: assessmentDomainData){
						Object obj[] = (Object[])oo;
						Double normscoreD = (Double)obj[0];
						//Integer normscore = Math.round(normscoreD);
						Integer normscore = Integer.valueOf((int) Math.round(normscoreD));
						teacherNormScore.setTeacherNormScore( normscore);
						//double percentile = ((normscore-50)*3+50);
						double percentile=cGReportService.getPercentile(pztList,normscore);
						teacherNormScore.setPercentile(percentile);
						Object[] ob =cGReportService.getDeciles(percentile);
						teacherNormScore.setDecileColor(""+ob[0]);
						teacherNormScore.setMinDecileValue((Double)ob[1]);
						teacherNormScore.setMaxDecileValue((Double)ob[2]);
					}
					teacherNormScore.setTeacherDetail(teacherAssessmentdetail.getTeacherDetail());
					teacherNormScore.setTeacherAssessmentdetail(teacherAssessmentdetail);
					if(teacherNormScore.getTeacherNorScoreId()==null)
					{
						teacherNormScore.setCreatedDateTime(new Date());
						teacherNormScore.setAssessmentDateTime(assessmentDateTime);
						teacherNormScore.setUpdateDateTime(new Date());
					}
					
					try {
						teacherNormScoreDAO.makePersistent(teacherNormScore);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
				System.out.println("addassessmentscore.do");
			
		}
		return null;
	}
}
