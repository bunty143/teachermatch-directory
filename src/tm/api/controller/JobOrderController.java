package tm.api.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;

import tm.api.UtilityAPI;
import tm.api.services.ServiceUtility;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.DistrictSpecificJobCategoryMaster;
import tm.bean.JobCertification;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoMapping;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.RaceMaster;
import tm.bean.master.RegionMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SchoolTypeMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.DistrictSpecificJobCategoryMasterDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.GeoMappingDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.RegionMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SchoolStandardMasterDAO;
import tm.dao.master.SchoolTypeMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

@Controller
public class JobOrderController {

	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}

	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	public void setJobCertificationDAO(JobCertificationDAO jobCertificationDAO) {
		this.jobCertificationDAO = jobCertificationDAO;
	}

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}

	@Autowired
	private SchoolTypeMasterDAO schoolTypeMasterDAO;
	public void setSchoolTypeMasterDAO(SchoolTypeMasterDAO schoolTypeMasterDAO) {
		this.schoolTypeMasterDAO = schoolTypeMasterDAO;
	}
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
    public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}

    @Autowired
    private GeoMappingDAO geoMappingDAO;
    public void setGeoMappingDAO(GeoMappingDAO geoMappingDAO) {
		this.geoMappingDAO = geoMappingDAO;
	}

    @Autowired
    private RegionMasterDAO regionMasterDAO;
    public void setRegionMasterDAO(RegionMasterDAO regionMasterDAO) {
		this.regionMasterDAO = regionMasterDAO;
	}
    @Autowired
    private GeoZoneMasterDAO geoZoneMasterDAO;
 	public void setGeoZoneMasterDAO(GeoZoneMasterDAO geoZoneMasterDAO) {
		this.geoZoneMasterDAO = geoZoneMasterDAO;
	}
 	@Autowired 
 	private SchoolStandardMasterDAO schoolStandardMasterDAO;
 	public void setStandardMasterDAO(SchoolStandardMasterDAO schoolStandardMasterDAO) {
		this.schoolStandardMasterDAO = schoolStandardMasterDAO;
	}

 	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;

	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;

	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;

	@Autowired
	private RaceMasterDAO raceMasterDAO;

	@Autowired
	private DistrictSpecificJobCategoryMasterDAO districtSpecificJobCategoryMasterDAO;
	
	@Autowired
	private TeacherStatusNotesDAO teacherStatusNotesDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@RequestMapping(value="/service/json/addJobOrder.do", method=RequestMethod.GET)
	public String doAddJobOrderByJSONGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		return doAddJobOrderByJSONPOST(map, request, response);
	}

	@RequestMapping(value="/service/json/addJobOrder.do", method=RequestMethod.POST)
	public String doAddJobOrderByJSONPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		try {			
			PrintWriter out =null;
			out = response.getWriter();
			System.out.println("Enter in addJobOrder.do JSON");

			JSONObject jsonResponse = new JSONObject();

			response.setContentType("application/json");
			HttpSession session  = request.getSession();
			SchoolMaster schoolMaster = null;
			DistrictMaster districtMaster = null;
			JobOrder jobOrder = null;
			JobCategoryMaster jobCategoryMaster = null;			

			//JSONObject jsonRequest = new JSONObject();

			System.out.println("request.getParameter(request)"+request.getParameter("request"));

			String apiJobId = "";
			String jobTitle = "";
			String postingStartDate = "";
			String postingEndDate = "";
			String jobCategory = "";
			String hiringAuthority = "";
			String expectedHires = "";
			String exitURL = "";
			String exitMessage = "";				
			Date jobStartDate = null;
			Date jobEndDate = null;


			Boolean responseStatus = false;
			Integer errorCode = 10001;
			String errorMsg = "Invalid Credentials.";
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

			boolean schoolErExist = false;
			String schErrorText = "";

			try {
				JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( request.getParameter("request") ); 
				JSONArray jsonArrayCertification = new JSONArray();
				JSONArray jsonArraySchool = new JSONArray();

				String authKey = jsonRequest.get("authKey")==null?"":UtilityAPI.parseString(jsonRequest.getString("authKey")).replace(' ', '+');
				String orgType = jsonRequest.get("orgType")==null?"":UtilityAPI.parseString(jsonRequest.getString("orgType")).trim();
				String userEmail = jsonRequest.get("userEmail")==null?"":UtilityAPI.parseString(jsonRequest.getString("userEmail")).replace(' ', '+');

				if(!userEmail.trim().equals("")){
					userEmail = Utility.decodeBase64(userEmail);
				}
				UserMaster userMaster = null;
				List<UserMaster> lstUMaster = userMasterDAO.findByEmail(userEmail); 
				if(lstUMaster!=null && lstUMaster.size()>0){
					userMaster=lstUMaster.get(0);
				}

				if(orgType.equalsIgnoreCase("D")){
					districtMaster =  districtMasterDAO.validateDistrictByAuthkey(authKey);				
					if(districtMaster!=null){
						session.setAttribute("apiDistrictMaster", districtMaster);						
						responseStatus = true;
					}
				}
				else if(orgType.equalsIgnoreCase("S")){
					schoolMaster = schoolMasterDAO.validateSchoolByAuthkey(authKey);					
					System.out.println("SchoolMaster IN"+schoolMaster);
					if(schoolMaster!=null){
						districtMaster =  schoolMaster.getDistrictId();
						session.setAttribute("apiSchoolMaster", schoolMaster);
						session.setAttribute("apiDistrictMaster", schoolMaster.getDistrictId());
						responseStatus = true;
					}
				}
				else{
					responseStatus = false;	
					errorCode=10001;
					errorMsg="Invalid Credentials.";
				}

				if(responseStatus){
					if(userEmail.equals("")){
						responseStatus=false;
						errorCode = 10002;
						errorMsg = "User email address is required.";
					}
					else if(!UtilityAPI.validEmail(userEmail)){
						responseStatus=false;
						errorCode = 10003;
						errorMsg = "Invalid User email address.";
					}	
					else if(userMaster==null){
						responseStatus=false;
						errorCode = 10004;
						errorMsg = "User does not exist.";
					}
					else if(!userMaster.getStatus().equals("A")){
						responseStatus=false;
						errorCode = 10009;
						errorMsg = "Candidate is inactivated.";
					}
					else if(userMaster.getDistrictId()==null){
						System.out.println("Block 1");
						responseStatus=false;
						errorCode = 10010;
						errorMsg = "User does not belong to district/school.";
					}
					else if(!userMaster.getDistrictId().equals(districtMaster)){
						System.out.println("Block 2");
						System.out.println(">"+userMaster.getDistrictId());
						System.out.println(">"+districtMaster.getDistrictId());
						responseStatus=false;
						errorCode = 10010;
						errorMsg = "User does not belong to district/school.";
					}			
					else{
						session.setAttribute("userMasterAPI", userMaster);
						responseStatus=true;
					}
				}

				if(responseStatus){						
					System.out.println("Enter in try");				
					apiJobId = jsonRequest.get("jobId")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobId")).trim();
					jobTitle = jsonRequest.get("jobTitle")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobTitle")).trim();
					postingStartDate = jsonRequest.get("postingStartDate")==null?"":UtilityAPI.parseString(jsonRequest.getString("postingStartDate")).trim(); 
					postingEndDate = jsonRequest.get("postingEndDate")==null?"":UtilityAPI.parseString(jsonRequest.getString("postingEndDate")).trim(); 
					jobCategory = jsonRequest.get("jobCategory")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobCategory")).trim();
					expectedHires = jsonRequest.get("expectedHires")==null?"":UtilityAPI.parseString(jsonRequest.getString("expectedHires")).trim();
					try{
						Integer.parseInt(expectedHires);
					}catch (Exception e) {
						//e.printStackTrace();
						expectedHires="";
					}
					hiringAuthority = jsonRequest.get("hiringAuthority")==null?"":UtilityAPI.parseString(jsonRequest.getString("hiringAuthority")).trim(); 
					exitURL = jsonRequest.get("exitURL")==null?"":UtilityAPI.parseString(jsonRequest.getString("exitURL")).trim(); 
					exitMessage = jsonRequest.get("exitMessage")==null?"":UtilityAPI.parseString(jsonRequest.getString("exitMessage")).trim();
					jobCategoryMaster = jobCategoryMasterDAO.findJobCategoryByNmae(jobCategory);
					jsonArrayCertification = new JSONArray();
					System.out.println("><><><>");
					try {
						jsonArrayCertification = jsonRequest.getJSONArray("certification");						
					} catch (Exception e) {
						jsonArrayCertification = null;						
					}

					try {						
						jsonArraySchool = jsonRequest.getJSONArray("attachedSchools");
					} catch (Exception e) {						
						jsonArraySchool = null;
					}

					Map<String,SchoolMaster> mapSchoolMaster = new HashMap<String, SchoolMaster>();
					if(jsonArraySchool!=null && jsonArraySchool.size()>0){
						SchoolMaster scm = null;
						JSONObject jsonScm = null;			
						for (int i = 0; i < jsonArraySchool.size(); i++){
							jsonScm = jsonArraySchool.getJSONObject(i);
							scm = schoolMasterDAO.findBySchoolName(jsonScm.getString("schoolName"),districtMaster);
							if(scm!=null){
								mapSchoolMaster.put(scm.getSchoolName(), scm);
							}
							else{
								schoolErExist = true;
								schErrorText = schErrorText+jsonScm.getString("schoolName")+", ";
							}
						}
						if(schoolErExist){
							schErrorText = schErrorText.substring(0,schErrorText.length()-3);
							schErrorText = schErrorText + " is not exist in the district.";
						}
					}
					try{					
						System.out.println("postingStartDate"+postingStartDate);
						jobStartDate = (Date)formatter.parse(postingStartDate);
						System.out.println("jobStartDate> "+jobStartDate);

					} 
					catch (Exception e) {
						jobStartDate = null;						
					}

					try{
						System.out.println("jobEndDate"+jobEndDate);
						jobEndDate = (Date)formatter.parse(postingEndDate);
						System.out.println("jobEndDate> "+jobEndDate);
					} 
					catch (Exception e) {						
						jobEndDate = null;
					}
					jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);
					System.out.println(""+jobCategory);

					if(apiJobId.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Job Id is required.";
					}
					else if(jobTitle.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Job Title is required.";
					}					
					else if(postingStartDate.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Posting Start Date is required.";
					}
					else if(postingEndDate.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Posting End Date is required.";
					}
					else if(jobStartDate==null){
						responseStatus = false;
						errorCode = 10003;
						errorMsg = "Invalid jobStartDate date.";
					}
					else if(jobEndDate==null){
						responseStatus = false;
						errorCode = 10003;
						errorMsg = "Invalid jobEndDate date.";
					}
					else if(jobEndDate.compareTo(new Date())<=0){
						responseStatus = false;
						errorCode = 10016;
						errorMsg = "Posting End Date should be greater than current date.";
					}
					else if(jobEndDate.compareTo(jobStartDate)<0){
						responseStatus = false;
						errorCode = 10016;
						errorMsg = "Posting End Date should be greater than Posting Start Date.";
					}
					else if(jobCategory.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Job Category is required.";
					}
					else if(jobCategoryMaster==null){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Job Category does not exist.";
					}
					else if(hiringAuthority.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Hiring Authority is required.";
					}										
					else if((schoolMaster!=null) && (!hiringAuthority.equalsIgnoreCase("S"))){
						responseStatus = false;
						errorCode = 10014;
						errorMsg = "Hiring Authority should be S.";
					}
					else if((hiringAuthority.equals("D")) && (jsonArraySchool!=null && jsonArraySchool.size()>0)){
						responseStatus = false;
						errorCode = 10014;
						errorMsg = "Hiring Authority should be S.";
					}
					else if((hiringAuthority.equals("S") && (jsonArraySchool==null || jsonArraySchool.size()==0))){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Attached Schools is required.";
					}
					else if(schoolErExist){
						responseStatus = false;
						errorCode = 10005;
						errorMsg = schErrorText;					
					}
					else if(hiringAuthority.equals("D") && expectedHires.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Expected Hires is required.";
					}					
					else if(jobOrder!=null){
						responseStatus = false;
						errorCode = 10005;
						errorMsg = "Job Id "+apiJobId+" is already exist.";
					}					
					else{

						System.out.println("jobCategoryMaster"+jobCategoryMaster);					
						jobOrder = new JobOrder();
						jobOrder.setDistrictMaster(districtMaster);
						jobOrder.setJobTitle(jobTitle);
						jobOrder.setJobStartDate(jobStartDate);
						jobOrder.setJobEndDate(jobEndDate);
						jobOrder.setJobCategoryMaster(jobCategoryMaster);
						jobOrder.setIsJobAssessment(false);
						jobOrder.setJobStatus("O");
						jobOrder.setStatus("A");
						jobOrder.setCreatedBy(null);
						if(schoolMaster!=null){
							jobOrder.setCreatedByEntity(new Integer(3));
						}
						else{
							jobOrder.setCreatedByEntity(new Integer(2));
						}
						if(hiringAuthority.equals("S")){
							jobOrder.setCreatedForEntity(new Integer(3));						
						}
						else{
							jobOrder.setCreatedForEntity(new Integer(2));
							jobOrder.setNoOfExpHires(new Integer(expectedHires));
						}
						jobOrder.setApiJobId(apiJobId);
						jobOrder.setExitURL(exitURL);
						jobOrder.setExitMessage(exitMessage);
						jobOrder.setCreatedDateTime(new Date());
						jobOrder.setIpAddress(IPAddressUtility.getIpAddress(request));
						jobOrder.setNotificationToschool(true);
						jobOrder.setIsPortfolioNeeded(false);
						jobOrderDAO.makePersistent(jobOrder);

						System.out.println("apiJobId"+apiJobId);
						System.out.println("jobTitle"+jobTitle);
						System.out.println("postingStartDate"+postingStartDate);
						System.out.println("postingEndDate"+postingEndDate);

						JSONObject jsonCert = null;
						JobCertification jobCertification = null;

						if(jsonArrayCertification!=null)
							for (int i = 0; i < jsonArrayCertification.size(); i++) {
								jsonCert =  jsonArrayCertification.getJSONObject(i);
								jobCertification = new JobCertification();
								jobCertification.setJobId(jobOrder);
								jobCertification.setCertType(""+jsonCert.get("certificationType"));
								jobCertificationDAO.makePersistent(jobCertification);
							}

						SchoolMaster scm = null;
						SchoolInJobOrder schoolInJobOrder = null;
						JSONObject jsonScm = null;		
						if(jsonArraySchool!=null)
							for (int i = 0; i < jsonArraySchool.size(); i++){
								jsonScm = jsonArraySchool.getJSONObject(i);
								//scm = schoolMasterDAO.findBySchoolName(jsonScm.getString("schoolName"));
								scm = mapSchoolMaster.get(jsonScm.getString("schoolName"));
								schoolInJobOrder = new SchoolInJobOrder();
								schoolInJobOrder.setJobId(jobOrder);
								schoolInJobOrder.setSchoolId(scm);
								schoolInJobOrder.setNoOfSchoolExpHires(new Integer(jsonScm.getString("expectedHires")));
								schoolInJobOrder.setCreatedDateTime(new Date());
								schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
							}
						responseStatus = true;
						try{
							if(jobOrder!=null)
							jobOrderDAO.totalNoOfHires(jobOrder);
						}catch (Exception e) {
							e.printStackTrace();
						}
						
					}
				}				
				if(responseStatus){
					jsonResponse = new JSONObject();
					jsonResponse.put("status", true);
					jsonResponse.put("jobId", jobOrder.getApiJobId());
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());					
				}
				else{
					jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
				}

			} 
			catch (Exception e) {
				e.printStackTrace();
				jsonResponse = UtilityAPI.writeJSONErrors(10017, "Server Error.", e);

			}
			//out.print(">>>>"+jsonRequest);
			out.print(jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;	
	}
	@RequestMapping(value="/service/json/addHiredCandidate.do", method=RequestMethod.GET)
	public String updateHiredCandidateByJSONGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("================================= addHiredCandidate.do");
		String retrunValue = "";
		//String strURL="http://localhost:8080/teachermatch/service/json/addHiredCandidate.do";
		String strURL="http://localhost:8080/teachermatch/service/json/addHiredCandidate.do";
		PostMethod post = new PostMethod(strURL);
		try {

			String req="<share><comment>I am hiring Now </comment>"+
			" <content>"+
			" <title>I am hiring Now</title>"+
			//" <description>Leverage the Share API to maximize engagement on user-generated content on LinkedIn</description>"+
			" <submitted-url>http://demo.teachermatch.org</submitted-url>"+
			"<submitted-image-url>http://demo.teachermatch.org/images/testnew.png</submitted-image-url>"+ 
			"</content>"+
			"<visibility>"+ 
			"<code>anyone</code>"+ 
			"</visibility>"+
			"</share>";
			JobForTeacher jobForTeacher = jobForTeacherDAO.findById(new Long(1), false, false);
			TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
			JobOrder jobOrder = jobForTeacher.getJobId();
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false,false);
			System.out.println("teacherDetail.getTeacherId()::: "+teacherDetail.getTeacherId());
			System.out.println("jobOrder::: "+jobOrder.getJobId());
			//StringRequestEntity requestEntity = new StringRequestEntity(req,"application/xml","UTF-8");
			String candidateRace = "";
			String raceIds[] = new String[10];
			if(teacherPersonalInfo.getRaceId()!=null)
			{
				raceIds = teacherPersonalInfo.getRaceId().split(",");
			}
			if(raceIds.length>0)
			{
				List<Integer> races = new ArrayList<Integer>();
				for (int i = 0; i < raceIds.length; i++) {
					races.add(Integer.parseInt(raceIds[i]));
				}
				List<RaceMaster> raceMasters = raceMasterDAO.findRacesByRaceCodeByOrder(races);
				for (RaceMaster raceMaster : raceMasters) {
					candidateRace+=raceMaster.getRaceName()+",";
				}

			}
			String employeeType = "";
			if(teacherPersonalInfo.getEmployeeType()!=null)
			{
				if(teacherPersonalInfo.getEmployeeType()==1)
					employeeType = "Instructional";
				else
					employeeType = "Non-Instructional";
			}

			req="{ 'LastName': '"+teacherPersonalInfo.getLastName()+"'," +
			"    'FirstName': '"+teacherPersonalInfo.getFirstName()+"'," +
			"    'MiddleName': '"+teacherPersonalInfo.getMiddleName()+"'," +
			"    'StreetAddress': '"+teacherPersonalInfo.getAddressLine1()+"'," +
			"    'City': '"+teacherPersonalInfo.getCityId().getCityName()+"', " +
			"    'State': '"+teacherPersonalInfo.getStateId().getStateShortName()+"'," +
			"    'Postal': '"+teacherPersonalInfo.getZipCode()+"'," +
			"    'HomePhone': '"+teacherPersonalInfo.getPhoneNumber()+"'," +
			"    'EmailAddr': '"+Utility.encodeInBase64(teacherPersonalInfo.getEmailAddress())+"'," +
			"    'LoginName': '"+Utility.encodeInBase64(teacherPersonalInfo.getEmailAddress())+"'," +
			//"    'Password': 'bm90Y3JlYXRpdmU='," +
			"    'DOB': '"+teacherPersonalInfo.getDob()+"'," +
			"    'AppSSN': '"+teacherPersonalInfo.getSSN()+"'," +
			//"    'EmergencyContact': 'Henry Mitchell'," +
			//"    'EmergencyPhone': '3156641442'," +
			"    'Gender': '"+teacherPersonalInfo.getGenderId().getGenderName()+"'," +
			"    'DOE': '"+teacherPersonalInfo.getEthnicOriginId().getEthnicOriginName()+"'," +
			"    'Race': '"+candidateRace+"'," +
			//"    'TotalYrsOfExpETPay': '10'," +
			//"    'YrsVerifiedGenEmpPay': '1'," +
			//"    'TotalYrsOfExpMilExpPay': '5'," +
			"    'EmployeeType': '"+employeeType+"'," +
			"    'PopstingNumber': '"+jobForTeacher.getRequisitionNumber()+"'," +
			"    'JobTitle': '"+jobOrder.getJobTitle()+"'," +
			//"    'JobTitleCode': '3201'," +
			"    'JobCode' : '"+jobOrder.getApiJobId()+"'," +
			"    'DatePosted': '"+jobOrder.getJobStartDate()+"'," +
			"    'DateClosed': '"+jobOrder.getJobEndDate()+"'," +
			//"    'CostCenter' : '0106'," +
			//"    'CostCenterName': 'Huntington PreK-8 School'," +
			//"    'PafType': 'New Hire'," +
			//"    'EmpStatus': 'Full Time'," +
			//"    'FTE': '1.0'," +
			//"    'FundSource': 'A'," +
			//"    'ServiceDt': '2013-09-01'," +
			//"    'StartDate': '2013-09-01'," +
			//"    'VacancyReasonET': 'Anticipated Vacancy'," +
			//"    'PrevOccupant': 'Kay Frizzell'," +
			//"    'RankPayLvl': 'Class Level 4'," +
			"    'TotalYrsOfExpET': '"+teacherPersonalInfo.getExpectedSalary()+"'," +

			"}";
			System.out.println("req:: "+req);
			StringRequestEntity requestEntity = new StringRequestEntity(req,"application/json","UTF-8");

			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
			HttpClient httpclient = new HttpClient();

			int result = httpclient.executeMethod(post);            
			System.out.println("Response status code: " + result);            
			//System.out.println("Response body: ");
			//System.out.println(post.getResponseBodyAsString());    
			retrunValue = post.getResponseBodyAsString();

		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			post.releaseConnection();
		}

		return null;
	}
	@RequestMapping(value="/service/json/addHiredCandidate.do", method=RequestMethod.POST)
	public String updateHiredCandidateByJSONPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("================================= addHiredCandidate.do");
		System.out.println("request:::: "+request.getParameter("request"));
		String payloadRequest = request.getParameter("request");
		if(payloadRequest==null)
		{
			try {
				payloadRequest = ServiceUtility.getBody(request);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("payloadRequest::= "+payloadRequest);
		return payloadRequest;
	}
	@RequestMapping(value="/service/xml/addJobOrder.do", method=RequestMethod.GET)
	public String doAddJobOrderByXMLGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		return doAddJobOrderByXMLPOST(map, request, response);
	}

	@RequestMapping(value="/service/xml/addJobOrder.do", method=RequestMethod.POST)
	public String doAddJobOrderByXMLPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{		
		try {
			PrintWriter out =null;
			out = response.getWriter();
			System.out.println("Enter in addJobOrder.do XML.");

			response.setContentType("text/xml");
			HttpSession session  = request.getSession();
			SchoolMaster schoolMaster = null;
			DistrictMaster districtMaster = null;
			JobOrder jobOrder = null;
			JobCategoryMaster jobCategoryMaster = null;

			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = null;
			Document doc = null;
			Element root = null;


			docBuilder = builderFactory.newDocumentBuilder();
			doc = docBuilder.newDocument();
			root = doc.createElement("teachermatch");
			doc.appendChild(root);			

			System.out.println("request.getParameter(request)"+request.getParameter("request"));				



			//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

			String apiJobId = "";
			String jobTitle = "";
			String postingStartDate = "";
			String postingEndDate = "";
			String jobCategory = "";
			String hiringAuthority = "";
			String expectedHires = "";
			String exitURL = "";
			String exitMessage = "";				
			Date jobStartDate = null;
			Date jobEndDate = null;

			Boolean responseStatus = false;
			Integer errorCode = 10001;
			String errorMsg = "Invalid Credentials.";
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

			boolean schoolErExist = false;
			String schErrorText = "";

			try {						
				System.out.println("Enter in try.");	
				DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilderReq = dbfac.newDocumentBuilder();

				String reqString = request.getParameter("request");
				Document docReq = docBuilderReq.parse(new InputSource(new StringReader(reqString)));
				docReq.getDocumentElement().normalize();


				String authKey = docReq.getElementsByTagName("authKey").item(0).getTextContent().replace(' ', '+');
				String orgType = docReq.getElementsByTagName("orgType").item(0).getTextContent();
				String userEmail = docReq.getElementsByTagName("userEmail").item(0).getTextContent().replace(' ', '+');;

				if(!userEmail.trim().equals("")){
					userEmail = Utility.decodeBase64(userEmail);
				}
				UserMaster userMaster = null;
				List<UserMaster> lstUMaster = userMasterDAO.findByEmail(userEmail); 
				if(lstUMaster!=null && lstUMaster.size()>0){
					userMaster=lstUMaster.get(0);
				}

				if(orgType.equalsIgnoreCase("D")){
					districtMaster =  districtMasterDAO.validateDistrictByAuthkey(authKey);				
					if(districtMaster!=null){
						session.setAttribute("apiDistrictMaster", districtMaster);						
						responseStatus = true;
					}
				}
				else if(orgType.equalsIgnoreCase("S")){
					schoolMaster = schoolMasterDAO.validateSchoolByAuthkey(authKey);					
					System.out.println("SchoolMaster IN"+schoolMaster);
					if(schoolMaster!=null){
						districtMaster =  schoolMaster.getDistrictId();
						session.setAttribute("apiSchoolMaster", schoolMaster);
						session.setAttribute("apiDistrictMaster", schoolMaster.getDistrictId());
						responseStatus = true;
					}
				}
				else{
					responseStatus = false;	
					errorCode=10001;
					errorMsg="Invalid Credentials.";
				}

				if(responseStatus){
					if(userEmail.equals("")){
						responseStatus=false;
						errorCode = 10002;
						errorMsg = "User email address is required.";
					}
					else if(!UtilityAPI.validEmail(userEmail)){
						responseStatus=false;
						errorCode = 10003;
						errorMsg = "Invalid User email address.";
					}	
					else if(userMaster==null){
						responseStatus=false;
						errorCode = 10004;
						errorMsg = "User does not exist.";
					}
					else if(!userMaster.getStatus().equals("A")){
						responseStatus=false;
						errorCode = 10009;
						errorMsg = "Candidate is inactivated.";
					}
					else if(userMaster.getDistrictId()==null){
						responseStatus=false;
						errorCode = 10010;
						errorMsg = "User does not belong to district/school.";
					}
					else if(!userMaster.getDistrictId().equals(districtMaster)){				
						responseStatus=false;
						errorCode = 10010;
						errorMsg = "User does not belong to district/school.";
					}			
					else{
						session.setAttribute("userMasterAPI", userMaster);
						responseStatus=true;
					}
				}

				if(responseStatus){	
					apiJobId = docReq.getElementsByTagName("jobId").item(0)==null?"":docReq.getElementsByTagName("jobId").item(0).getTextContent();;
					jobTitle = docReq.getElementsByTagName("jobTitle").item(0)==null?"":docReq.getElementsByTagName("jobTitle").item(0).getTextContent();;
					postingStartDate = docReq.getElementsByTagName("postingStartDate").item(0)==null?"":docReq.getElementsByTagName("postingStartDate").item(0).getTextContent();; 
					postingEndDate = docReq.getElementsByTagName("postingEndDate").item(0)==null?"":docReq.getElementsByTagName("postingEndDate").item(0).getTextContent();; 
					jobCategory = docReq.getElementsByTagName("jobCategory").item(0)==null?"":docReq.getElementsByTagName("jobCategory").item(0).getTextContent();;
					expectedHires = docReq.getElementsByTagName("expectedHires").item(0)==null?"":docReq.getElementsByTagName("expectedHires").item(0).getTextContent();;
					hiringAuthority = docReq.getElementsByTagName("hiringAuthority").item(0)==null?"":docReq.getElementsByTagName("hiringAuthority").item(0).getTextContent();; 
					exitURL = docReq.getElementsByTagName("exitURL").item(0)==null?"":docReq.getElementsByTagName("exitURL").item(0).getTextContent();; 
					exitMessage = docReq.getElementsByTagName("exitMessage").item(0)==null?"":docReq.getElementsByTagName("exitMessage").item(0).getTextContent();;
					jobCategoryMaster = jobCategoryMasterDAO.findJobCategoryByNmae(jobCategory);
					NodeList nListAttachedSchools = docReq.getElementsByTagName("attachedSchools");
					NodeList nListCertification = docReq.getElementsByTagName("certification");



					Map<String,SchoolMaster> mapSchoolMaster = new HashMap<String, SchoolMaster>();

					if(nListAttachedSchools!=null && nListAttachedSchools.getLength()>0){
						SchoolMaster scm = null;							

						for (int i = 0; i < nListAttachedSchools.getLength(); i++){						
							Node nNode = nListAttachedSchools.item(i);
							if (nNode.getNodeType() == Node.ELEMENT_NODE){ 
								Element eElement = (Element) nNode;     
								scm = schoolMasterDAO.findBySchoolName(eElement.getElementsByTagName("schoolName").item(0).getTextContent(),districtMaster);
								if(scm!=null){
									mapSchoolMaster.put(scm.getSchoolName(), scm);
								}
								else{
									schoolErExist = true;
									schErrorText = schErrorText + eElement.getElementsByTagName("schoolName").item(0).getTextContent()+", ";
								}
							}							
						}
						System.out.println("nListAttachedSchools"+nListAttachedSchools.getLength());
						if(schoolErExist){
							schErrorText = schErrorText.substring(0,schErrorText.length()-3);
							schErrorText = schErrorText + " is not exist in the district.";
						}
					}
					try{					
						System.out.println("postingStartDate"+postingStartDate);
						jobStartDate = (Date)formatter.parse(postingStartDate);
						System.out.println("jobStartDate> "+jobStartDate);						
					} 
					catch (Exception e) {
						jobStartDate = null;						
					}

					try{
						System.out.println("jobEndDate"+jobEndDate);
						jobEndDate = (Date)formatter.parse(postingEndDate);
						System.out.println("jobEndDate> "+jobEndDate);
					} 
					catch (Exception e) {						
						jobEndDate = null;
					}
					jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);


					if(apiJobId.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Job Id is required.";
					}
					else if(jobTitle.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Job Title is required.";
					}					
					else if(postingStartDate.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Posting Start Date is required.";
					}
					else if(postingEndDate.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Posting End Date is required.";
					}
					else if(jobStartDate==null){
						responseStatus = false;
						errorCode = 10003;
						errorMsg = "Invalid jobStartDate date.";
					}
					else if(jobEndDate==null){
						responseStatus = false;
						errorCode = 10003;
						errorMsg = "Invalid jobEndDate date.";
					}
					else if(jobEndDate.compareTo(new Date())<=0){
						responseStatus = false;
						errorCode = 10016;
						errorMsg = "Posting End Date should be greater current date.";
					}
					else if(jobEndDate.compareTo(jobStartDate)<0){
						responseStatus = false;
						errorCode = 10016;
						errorMsg = "Posting End Date should be greater than Posting Start Date.";
					}
					else if(jobCategory.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Job Category is required.";
					}
					else if(jobCategoryMaster==null){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Job Category does not exist.";
					}
					else if(hiringAuthority.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Hiring Authority is required.";
					}										
					else if((schoolMaster!=null) && (!hiringAuthority.equalsIgnoreCase("S"))){
						responseStatus = false;
						errorCode = 10014;
						errorMsg = "Hiring Authority should be S.";
					}
					else if((hiringAuthority.equals("D")) && (nListAttachedSchools!=null && nListAttachedSchools.getLength()>0)){
						responseStatus = false;
						errorCode = 10014;
						errorMsg = "Hiring Authority should be S.";
					}
					else if((hiringAuthority.equals("S") && (nListAttachedSchools==null || nListAttachedSchools.getLength()==0))){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Attached Schools is required.";
					}
					else if(schoolErExist){
						responseStatus = false;
						errorCode = 10005;
						errorMsg = schErrorText;					
					}
					else if(hiringAuthority.equals("D") && expectedHires.equals("")){
						responseStatus = false;
						errorCode = 10002;
						errorMsg = "Expected Hires is required.";
					}					
					else if(jobOrder!=null){
						responseStatus = false;
						errorCode = 10005;
						errorMsg = "Job Id "+apiJobId+" is already exist.";
					}							
					else{
						jobCategoryMaster = jobCategoryMasterDAO.findJobCategoryByNmae(jobCategory);					
						System.out.println("jobCategoryMaster"+jobCategoryMaster);					
						jobOrder = new JobOrder();
						jobOrder.setDistrictMaster(districtMaster);
						jobOrder.setJobTitle(jobTitle);
						jobOrder.setJobStartDate(jobStartDate);
						jobOrder.setJobEndDate(jobEndDate);
						jobOrder.setJobCategoryMaster(jobCategoryMaster);
						jobOrder.setIsJobAssessment(false);
						jobOrder.setJobStatus("O");
						jobOrder.setStatus("A");
						jobOrder.setCreatedBy(null);
						if(schoolMaster!=null){
							jobOrder.setCreatedByEntity(new Integer(3));
						}
						else{
							jobOrder.setCreatedByEntity(new Integer(2));
						}
						if(hiringAuthority.equals("S")){
							jobOrder.setCreatedForEntity(new Integer(3));						
						}
						else{
							jobOrder.setCreatedForEntity(new Integer(2));
							jobOrder.setNoOfExpHires(new Integer(expectedHires));
						}
						jobOrder.setApiJobId(apiJobId);
						jobOrder.setExitURL(exitURL);
						jobOrder.setExitMessage(exitMessage);
						jobOrder.setCreatedDateTime(new Date());
						jobOrder.setIpAddress(IPAddressUtility.getIpAddress(request));
						jobOrder.setIsPortfolioNeeded(false);
						jobOrder.setNotificationToschool(true);
						jobOrderDAO.makePersistent(jobOrder);

						System.out.println("apiJobId"+apiJobId);
						System.out.println("jobTitle"+jobTitle);
						System.out.println("postingStartDate"+postingStartDate);
						System.out.println("postingEndDate"+postingEndDate);

						JobCertification jobCertification = null;

						for (int i = 0; i < nListCertification.getLength(); i++) {
							System.out.println(">>>>>>>");
							Node nNode = nListCertification.item(i);
							if (nNode.getNodeType() == Node.ELEMENT_NODE){ 
								Element eElement = (Element) nNode;     
								jobCertification = new JobCertification();
								jobCertification.setJobId(jobOrder);
								jobCertification.setCertType(""+eElement.getElementsByTagName("certificationType").item(0).getTextContent());
								jobCertificationDAO.makePersistent(jobCertification);
							}
						}
						SchoolMaster scm = null;
						SchoolInJobOrder schoolInJobOrder = null;

						for (int i = 0; i < nListAttachedSchools.getLength(); i++){			
							System.out.println("_______");
							Node nNode = nListAttachedSchools.item(i);
							if (nNode.getNodeType() == Node.ELEMENT_NODE){ 
								Element eElement = (Element) nNode;     
								//scm = schoolMasterDAO.findBySchoolName(eElement.getElementsByTagName("schoolName").item(0).getTextContent());
								scm = mapSchoolMaster.get(eElement.getElementsByTagName("schoolName").item(0).getTextContent());
								schoolInJobOrder = new SchoolInJobOrder();
								schoolInJobOrder.setJobId(jobOrder);
								schoolInJobOrder.setSchoolId(scm);
								schoolInJobOrder.setNoOfSchoolExpHires(new Integer(eElement.getElementsByTagName("expectedHires").item(0).getTextContent()));
								schoolInJobOrder.setCreatedDateTime(new Date());
								schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
							}
						}
						responseStatus = true;
						
						try{
							if(jobOrder!=null)
							jobOrderDAO.totalNoOfHires(jobOrder);
						}catch (Exception e) {
							e.printStackTrace();
						}
					}
				}

				if(responseStatus){
					Element eleStatus = doc.createElement("status");
					root.appendChild(eleStatus);
					Text textStatus = doc.createTextNode("true");
					eleStatus.appendChild(textStatus);

					Element eleJbStatus = doc.createElement("jobId");
					root.appendChild(eleJbStatus);
					Text jbStatusResult = doc.createTextNode(""+jobOrder.getApiJobId());
					eleJbStatus.appendChild(jbStatusResult);

					Element timeStamp = doc.createElement("timeStamp");
					root.appendChild(timeStamp);
					Text timeStampResult = doc.createTextNode(UtilityAPI.getCurrentTimeStamp());
					timeStamp.appendChild(timeStampResult);					
				}
				else{
					UtilityAPI.writeXMLErrors(errorCode, errorMsg, null, doc, root);
				}

			} 
			catch (Exception e) {
				System.out.println(">>>>"+e.getMessage());
				UtilityAPI.writeXMLErrors(10017, "Server Error.", e, doc, root);
				e.printStackTrace();
			}
			//out.print(">>>>"+jsonRequest);

			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();

			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
			out.println(sw.toString());
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;	
	}

	@RequestMapping(value="/service/json/addPosition.do", method=RequestMethod.GET)
	public String doAddPositionByJSONGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		return doAddPositionByJSONPOST(map, request, response);
	}

	@RequestMapping(value="/service/json/addPosition.do", method=RequestMethod.POST)
	public String doAddPositionByJSONPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		try {			
			PrintWriter out =null;
			out = response.getWriter();
			System.out.println("Enter in addPosition.do JSON");
			JSONObject jsonResponse = new JSONObject();

			response.setContentType("application/json");
			HttpSession session  = request.getSession();
			DistrictMaster districtMaster = null;
			JobOrder jobOrder = null;
			JobCategoryMaster jobCategoryMaster = null;			

			//JSONObject jsonRequest = new JSONObject();

			System.out.println("request.getParameter(request)"+request.getParameter("request"));

			String apiJobId = "";
			String jobTitle = "";
			String postingStartDate = "";
			String postingEndDate = "";
			String jobCategory = "";
			String hiringAuthority = "";
			String expectedHires = "";
			String exitURL = "";
			String exitMessage = "";				
			Date jobStartDate = null;
			Date jobEndDate = null;
			Boolean responseStatus = false;
			Integer errorCode = 10001;
			String errorMsg = "Invalid Credentials.";
			String message="";
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			SchoolMaster schoolMaster = null;
			boolean schoolErExist = false;
			String schErrorText = "";
			boolean certErExist = false;
			String certErrorText = "";
			districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
			System.out.println("dm:: "+districtMaster);
			try{
				String payloadRequest = request.getParameter("request");
				if(payloadRequest==null)
				{
					try {
						payloadRequest = ServiceUtility.getBody(request);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				System.out.println("payloadRequest::::::= "+payloadRequest);

				JSONObject jsonRequest = null;
				jsonRequest = (JSONObject) JSONSerializer.toJSON( payloadRequest ); 
				String authKey = jsonRequest.get("authKey")==null?"":UtilityAPI.parseString(jsonRequest.getString("authKey")).replace(' ', '+');
				String orgType = jsonRequest.get("orgType")==null?"":UtilityAPI.parseString(jsonRequest.getString("orgType")).trim();
				String userEmail = jsonRequest.get("userEmail")==null?"":UtilityAPI.parseString(jsonRequest.getString("userEmail")).replace(' ', '+');
				JSONArray jsonArrayCertification = new JSONArray();
				JSONArray jsonArraySchool = new JSONArray();

				if(!userEmail.trim().equals("")){
					userEmail = Utility.decodeBase64(userEmail);
				}
				UserMaster userMaster = null;
				List<UserMaster> lstUMaster = userMasterDAO.findByEmail(userEmail); 
				if(lstUMaster!=null && lstUMaster.size()>0){
					userMaster=lstUMaster.get(0);
				}
				boolean positionCheckFlag = true;
				if(orgType.equalsIgnoreCase("D")){
					districtMaster =  districtMasterDAO.validateDistrictByAuthkey(authKey);				
					if(districtMaster!=null){
						session.setAttribute("apiDistrictMaster", districtMaster);						
						responseStatus = true;
					}
				}
				else if(orgType.equalsIgnoreCase("S")){
					schoolMaster = schoolMasterDAO.validateSchoolByAuthkey(authKey);					
					System.out.println("SchoolMaster IN"+schoolMaster);
					if(schoolMaster!=null){
						districtMaster =  schoolMaster.getDistrictId();
						session.setAttribute("apiSchoolMaster", schoolMaster);
						session.setAttribute("apiDistrictMaster", schoolMaster.getDistrictId());
						responseStatus = true;
					}
				}
				else{
					responseStatus = false;	
					errorCode=10001;
					errorMsg="Invalid Credentials.";
				}
				System.out.println("districtMaster: "+districtMaster);


				if(responseStatus){
					if(userEmail.equals("")){
						responseStatus=false;
						errorCode = 10002;
						errorMsg = "User email address is required.";
					}
					else if(!UtilityAPI.validEmail(userEmail)){
						responseStatus=false;
						errorCode = 10003;
						errorMsg = "Invalid User email address.";
					}	
					else if(userMaster==null){
						responseStatus=false;
						errorCode = 10004;
						errorMsg = "User does not exist.";
					}
					else if(!userMaster.getStatus().equals("A")){
						responseStatus=false;
						errorCode = 10006;
						errorMsg = "Candidate is inactivated.";
					}
					else if(userMaster.getDistrictId()==null){
						System.out.println("Block 1");
						responseStatus=false;
						errorCode = 10007;
						errorMsg = "User does not belong to district/school.";
					}
					else if(!userMaster.getDistrictId().equals(districtMaster)){
						System.out.println("Block 2");
						System.out.println(">"+userMaster.getDistrictId());
						System.out.println(">"+districtMaster.getDistrictId());
						responseStatus=false;
						errorCode = 10007;
						errorMsg = "User does not belong to district/school.";
					}			
					else{
						session.setAttribute("userMasterAPI", userMaster);
						responseStatus=true;
					}
				}
				boolean isJSINeeded = false;
				String jobDescription = "";
				if(responseStatus){
					System.out.println("Enter in try");				
					apiJobId = jsonRequest.get("jobCode")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobCode")).trim();
					jobTitle = jsonRequest.get("jobTitle")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobTitle")).trim();
					//jobDescription = StringEscapeUtils.unescapeHtml(jsonRequest.get("jobDescription")==null?"":jsonRequest.getString("jobDescription").trim());
					jobDescription = jsonRequest.get("jobDescription")==null?"":jsonRequest.getString("jobDescription").trim();
					String jobType = jsonRequest.get("jobType")==null?"F":UtilityAPI.parseString(jsonRequest.getString("jobType")).trim();
					postingStartDate = jsonRequest.get("jobStartDate")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobStartDate")).trim(); 
					postingEndDate = jsonRequest.get("jobEndDate")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobEndDate")).trim(); 
					jobCategory = jsonRequest.get("jobCategory")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobCategory")).trim();
					expectedHires = jsonRequest.get("expectedHires")==null?"":UtilityAPI.parseString(jsonRequest.getString("expectedHires")).trim();					
					hiringAuthority = jsonRequest.get("hiringAuthority")==null?"":UtilityAPI.parseString(jsonRequest.getString("hiringAuthority")).trim(); 
					exitURL = jsonRequest.get("exitURL")==null?"":UtilityAPI.parseString(jsonRequest.getString("exitURL")).trim(); 
					exitMessage = jsonRequest.get("exitMessage")==null?"":UtilityAPI.parseString(jsonRequest.getString("exitMessage")).trim();
					String isJSI = jsonRequest.get("isJSINeeded")==null?"false":UtilityAPI.parseString(jsonRequest.getString("isJSINeeded")).trim();
					try {
						isJSINeeded=Boolean.valueOf(isJSI);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					
					if(districtMaster.getDistrictId()==3628590)
					{
						String jobCode = apiJobId;
						String [] jobCodeSplit = apiJobId.split("-");
						if(jobCodeSplit.length>0)
						{
							jobCode = jobCodeSplit[0];
						}
						
						System.out.println("OOOOOOOOOOOOOOOOOOOOOOOOOO "+jobCode);
						DistrictSpecificJobCategoryMaster districtSpecificJobCategoryMaster = districtSpecificJobCategoryMasterDAO.checkJobCategoryByJobCode(jobCode,districtMaster);
						System.out.println("districtSpecificJobCategoryMaster::: "+districtSpecificJobCategoryMaster);
						if(districtSpecificJobCategoryMaster!=null)
							jobCategoryMaster = districtSpecificJobCategoryMaster.getJobCategoryMaster();
						
							//System.out.println("OOOOOOOOOOOOOOOOOOOOOOOOOO "+jobCategory);
							//System.out.println("JobTitle::::::::::::::: "+jobTitle);
							/*DistrictSpecificJobCategoryMaster districtSpecificJobCategoryMaster = districtSpecificJobCategoryMasterDAO.checkJobCategoryByJobCode(jobTitle,districtMaster);
							System.out.println("districtSpecificJobCategoryMaster::: "+districtSpecificJobCategoryMaster);
							if(districtSpecificJobCategoryMaster!=null)
								jobCategoryMaster = districtSpecificJobCategoryMaster.getJobCategoryMaster();*/
						
						
						positionCheckFlag = false;
					}else
						jobCategoryMaster = jobCategoryMasterDAO.findJobCategoryByNmae(jobCategory);
					
					

					JSONArray jsonArraypositionNo = new JSONArray();
					JSONArray jsonArrayPostingNo = new JSONArray();
					int positions = 0;
					Integer postingNoSize = 0;
					try {
						jsonArraypositionNo = jsonRequest.getJSONArray("positionNo");	
						jsonArrayPostingNo  = jsonRequest.getJSONArray("postingNo");
						if(jsonArraypositionNo!=null)
							positions = jsonArraypositionNo.size();
						if(jsonArrayPostingNo!=null)
							postingNoSize = jsonArrayPostingNo.size();
						System.out.println("postingNoSize : "+postingNoSize+" jsonArrayPostingNo :"+jsonArrayPostingNo);	
					} catch (Exception e) {
						jsonArraypositionNo = null;
						jsonArrayPostingNo = null;
					}
					List<String> positionNos = new ArrayList<String>();
					List<String> postingNos = new ArrayList<String>();
					boolean positionNoFlag = false;
					String positionNoError ="";

					try {						
						jsonArraySchool = jsonRequest.getJSONArray("attachedSchools");
					} catch (Exception e) {						
						jsonArraySchool = null;
					}
					boolean schoolRequisionNotEqual = false;
					String schoolRequisionNotEqualError = "";
					Map<String,SchoolMaster> mapSchoolMaster = new HashMap<String, SchoolMaster>();
					boolean jobWithSchool =false;
					//if(jsonArraySchool!=null && jsonArraySchool.size()>0 && hiringAuthority.equalsIgnoreCase("S")){
					if(jsonArraySchool!=null && jsonArraySchool.size()>0){
						jobWithSchool =true;
						SchoolMaster scm = null;
						JSONObject jsonScm = null;			
						for (int i = 0; i < jsonArraySchool.size(); i++){
							jsonScm = jsonArraySchool.getJSONObject(i);
							scm = schoolMasterDAO.findBySchoolName(jsonScm.getString("schoolName"),districtMaster);
							if(scm!=null){
								mapSchoolMaster.put(scm.getSchoolName(), scm);
								try{
									JSONArray jsonArraypositionNoSchool = new JSONArray();
									jsonArraypositionNoSchool = jsonScm.getJSONArray("positionNo");
									int schoolPositionNo = jsonArraypositionNoSchool.size();
									
									String expHrd = "0";
									try{
										expHrd = jsonScm.get("expectedHires")==null?"":UtilityAPI.parseString(jsonScm.getString("expectedHires")).trim();
									}catch(Exception e){expHrd="0";}
									if(expHrd.equals("") || expHrd.equalsIgnoreCase("0"))
									{
										schoolRequisionNotEqual = true;
										schoolRequisionNotEqualError = "expectedHires for school "+schoolRequisionNotEqualError+jsonScm.getString("schoolName")+" is required,  ";

									}else{

										if(!positionCheckFlag)
										{
											if(schoolPositionNo!=0 && schoolPositionNo!=Integer.parseInt(expHrd))
											{
												schoolRequisionNotEqual = true;
												schoolRequisionNotEqualError = "PositionNo. count does not match as expectedHires for school "+schoolRequisionNotEqualError+jsonScm.getString("schoolName")+",  ";
											}
										}else
										{
											if(schoolPositionNo!=Integer.parseInt(expHrd))
											{
												schoolRequisionNotEqual = true;
												schoolRequisionNotEqualError = "PositionNo. count does not match as expectedHires for school "+schoolRequisionNotEqualError+jsonScm.getString("schoolName")+",  ";
											}
										}
									}

									for (int j = 0; j < schoolPositionNo; j++){
										String position = jsonArraypositionNoSchool.getString(j);
										if(position!=null)
											position = position.replace("?", "");
										positionNos.add(position);
									}
								}catch (Exception e) {
									e.printStackTrace();
								}
							}
							else{
								schoolErExist = true;
								schErrorText = schErrorText+jsonScm.getString("schoolName")+", ";
							}
						}
						if(schoolErExist){
							schErrorText = schErrorText.substring(0,schErrorText.length()-3);
							schErrorText = schErrorText + " is not exist in the district.";
						}
						if(schoolRequisionNotEqual){
							schoolRequisionNotEqualError = schoolRequisionNotEqualError.substring(0,schoolRequisionNotEqualError.length()-3);
							schoolRequisionNotEqualError = schoolRequisionNotEqualError + ".";
						}
					}

					if(jsonArraypositionNo!=null && positions>0){
						for (int i = 0; i < jsonArraypositionNo.size(); i++){
							String position = jsonArraypositionNo.getString(i).trim();
							if(position!=null)
								position = position.replace("?", "");
							positionNos.add(position);
						}
						if(positionNos.size()>0)
						{
							List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = districtRequisitionNumbersDAO.getDistrictRequisitionNumbersBYReqNo(positionNos,districtMaster);
							if(lstDistrictRequisitionNumbers.size()>0)
							{
								positionNoFlag = true;
								for (DistrictRequisitionNumbers districtRequisitionNumbers : lstDistrictRequisitionNumbers) {
									positionNoError+=districtRequisitionNumbers.getRequisitionNumber()+", ";
								}
							}
						}
						if(jsonArrayPostingNo!=null && postingNoSize>0){
							for(int k = 0; k<jsonArrayPostingNo.size(); k++){
							String postingNo = jsonArrayPostingNo.getString(k).trim();
								if(postingNoSize!=null){
									postingNo = postingNo.replace("?", ""); 
								}
								postingNos.add(postingNo);
								
							}
						}
					}else if(hiringAuthority.equalsIgnoreCase("S") || jobWithSchool ) {
						if(positionNos.size()>0)
						{
							List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = districtRequisitionNumbersDAO.getDistrictRequisitionNumbersBYReqNo(positionNos,districtMaster);
							System.out.println("lstDistrictRequisitionNumbers:: "+lstDistrictRequisitionNumbers.size());
							if(lstDistrictRequisitionNumbers.size()>0)
							{
								positionNoFlag = true;
								for(DistrictRequisitionNumbers districtRequisitionNumbers : lstDistrictRequisitionNumbers) {
									positionNoError+=districtRequisitionNumbers.getRequisitionNumber()+", ";
								}
							}
						}
					}

					if(positionNoFlag){
						positionNoError = positionNoError.substring(0,positionNoError.length()-2);
						positionNoError = "Duplicate position nos. "+positionNoError + ".";
					}

					jsonArrayCertification = new JSONArray();
					System.out.println("><><><>");
					try {
						jsonArrayCertification = jsonRequest.getJSONArray("certification");						
					} catch (Exception e) {
						jsonArrayCertification = null;						
					}
					Map<String,CertificateTypeMaster> mapCertificateTypeMaster = new HashMap<String, CertificateTypeMaster>();
					if(jsonArrayCertification!=null && jsonArrayCertification.size()>0){
						CertificateTypeMaster ctm = null;
						JSONObject jsonCtm = null;			
						for (int i = 0; i < jsonArrayCertification.size(); i++){
							jsonCtm = jsonArrayCertification.getJSONObject(i);
							ctm = certificateTypeMasterDAO.findCertificationTypeByCertTypeCode(jsonCtm.getString("certificationType"));
							if(ctm!=null){
								mapCertificateTypeMaster.put(jsonCtm.getString("certificationType"), ctm);
							}
							else{
								certErExist = true;
								certErrorText = certErrorText+jsonCtm.getString("certificationType")+", ";
							}
						}
						if(certErExist){
							certErrorText = certErrorText.substring(0,certErrorText.length()-3);
							certErrorText = "Certification "+certErrorText + " does't exist.";
						}
					}

					System.out.println("jobCode: "+apiJobId);
					System.out.println("jobTitle: "+jobTitle);
					System.out.println("jobDescription: "+jobDescription);
					System.out.println("jobType: "+jobType);
					System.out.println("jobEndDate: "+postingStartDate);
					System.out.println("jobEndDate: "+postingEndDate);
					System.out.println("jobCategory: "+jobCategory);
					System.out.println("expectedHires: "+expectedHires);
					System.out.println("hiringAuthority: "+hiringAuthority);
					System.out.println("exitURL: "+exitURL);
					System.out.println("exitMessage: "+exitMessage);

					try{					
						System.out.println("postingStartDate"+postingStartDate);
						jobStartDate = (Date)formatter.parse(postingStartDate);
						System.out.println("jobStartDate> "+jobStartDate);
					} 
					catch (Exception e) {
						jobStartDate = null;						
					}

					try{
						System.out.println("jobEndDate"+jobEndDate);
						jobEndDate = (Date)formatter.parse(postingEndDate);
						System.out.println("jobEndDate> "+jobEndDate);
					} 
					catch (Exception e) {						
						jobEndDate = null;
					}
					jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);
					System.out.println("jobOrder*******"+jobOrder);
					System.out.println("jobCategory*******"+jobCategory);

					if(apiJobId.equals("")){
						responseStatus = false;
						errorCode = 10008;
						errorMsg = "Job Code is required.";
					}/*else if(jobOrder!=null){
						responseStatus = false;
						errorCode = 10009;
						errorMsg = "JobCode "+apiJobId+" is already exist.";
					}*/
					else if(jobTitle.equals("")){
						responseStatus = false;
						errorCode = 10010;
						errorMsg = "Job Title is required.";
					}else if(jobType.equals("")){
						responseStatus = false;
						errorCode = 10011;
						errorMsg = "Job Type is required.";
					}else if(!(jobType.equalsIgnoreCase("P")|| jobType.equalsIgnoreCase("F"))){
						responseStatus = false;
						errorCode = 10012;
						errorMsg = "Invalid Job Type.";
					}					
					else if(postingStartDate.equals("")){
						responseStatus = false;
						errorCode = 10013;
						errorMsg = "Posting Start Date is required.";
					}
					else if(postingEndDate.equals("")){
						responseStatus = false;
						errorCode = 10014;
						errorMsg = "Posting End Date is required.";
					}
					else if(jobStartDate==null){
						responseStatus = false;
						errorCode = 10015;
						errorMsg = "Invalid jobStartDate date.";
					}
					else if(jobEndDate==null){
						responseStatus = false;
						errorCode = 10016;
						errorMsg = "Invalid jobEndDate date.";
					}
					else if(jobEndDate.compareTo(new Date())<=0){
						responseStatus = false;
						errorCode = 10017;
						errorMsg = "Posting End Date should be greater than current date.";
					}
					else if(jobEndDate.compareTo(jobStartDate)<0){
						responseStatus = false;
						errorCode = 10018;
						errorMsg = "Posting End Date should be greater than Posting Start Date.";
					}
					else if(jobCategory.equals("")){
						responseStatus = false;
						errorCode = 10019;
						errorMsg = "Job Category is required.";
					}
					else if(jobCategoryMaster==null){
						responseStatus = false;
						errorCode = 10020;
						
						if(districtMaster.getDistrictId()==3628590)
							errorMsg = "Job Code does not exist.";
						else
							errorMsg = "Job Category does not exist.";
					}
					else if(hiringAuthority.equals("")){
						responseStatus = false;
						errorCode = 10021;
						errorMsg = "Hiring Authority is required.";
					}										
					else if((hiringAuthority.equals("S") && (jsonArraySchool==null || jsonArraySchool.size()==0))){
						responseStatus = false;
						errorCode = 10022;
						errorMsg = "Attached Schools is required.";
					}
					else if(certErExist){
						responseStatus = false;
						errorCode = 10023;
						errorMsg = certErrorText;					
					}
					else if(schoolErExist){
						responseStatus = false;
						errorCode = 10024;
						errorMsg = schErrorText;					
					}
					else if(positionCheckFlag && (jsonArraypositionNo==null || positions==0) && hiringAuthority.equals("D") && jobWithSchool==false){
						responseStatus = false;
						errorCode = 10025;
						errorMsg = "Position No. is required";				
					}
					else if(jsonArrayPostingNo==null || postingNoSize==0){
						responseStatus = false;
						errorCode = 10040;
						errorMsg = "Posting No. is required.";	
					}
					else if(postingNoSize!=positions){
						responseStatus = false;
						errorCode = 10041;
						errorMsg = "Position Nos. must be same as Posting Nos.";	
					}
					else if(hiringAuthority.equals("D") && (expectedHires.equals("") || expectedHires.equals("0")) && jobWithSchool==false){
						responseStatus = false;
						errorCode = 10026;
						errorMsg = "Expected Hires is required.";
					}
					else if(hiringAuthority.equals("D") && positions!=0 && positions!=Integer.parseInt(expectedHires) && jobWithSchool==false){
						responseStatus = false;
						errorCode = 10027;
						errorMsg = "Position Nos. must be same as expectedHires "+expectedHires;				
					}
					//}else if(positionNoFlag && hiringAuthority.equals("D")){
					else if(positionNoFlag){
						responseStatus = false;
						errorCode = 10028;
						errorMsg = positionNoError;
					}
					else if(schoolRequisionNotEqual){
						responseStatus = false;
						errorCode = 10029;
						errorMsg = schoolRequisionNotEqualError;					
					}else if(jobOrder==null)
					{
						System.out.println("jobCategoryMaster"+jobCategoryMaster);					
						jobOrder = new JobOrder();
						jobOrder.setDistrictMaster(districtMaster);
						jobOrder.setJobTitle(jobTitle);
						jobOrder.setJobStartDate(jobStartDate);
						jobOrder.setJobEndDate(jobEndDate);
						jobOrder.setJobCategoryMaster(jobCategoryMaster);
						jobOrder.setIsJobAssessment(isJSINeeded);
						if(isJSINeeded)
							jobOrder.setJobAssessmentStatus(1);
						else
							jobOrder.setJobAssessmentStatus(0);
						
						jobOrder.setJobStatus("O");
						jobOrder.setStatus("A");
						jobOrder.setJobType(jobType);
						jobOrder.setCreatedBy(null);
						jobOrder.setJobDescription(jobDescription);
						jobOrder.setIsInviteOnly(false);
						/*if(schoolMaster!=null){
							jobOrder.setCreatedByEntity(new Integer(3));
						}
						else{
							jobOrder.setCreatedByEntity(new Integer(2));
						}*/
						jobOrder.setCreatedByEntity(new Integer(2));

						/*if(hiringAuthority.equals("S")){
							jobOrder.setCreatedForEntity(new Integer(3));						
						}
						else{*/
						jobOrder.setCreatedForEntity(new Integer(2));
						if(hiringAuthority.equalsIgnoreCase("D"))
						{
							try {
								
								jobOrder.setNoOfExpHires(new Integer(expectedHires));
							} catch (Exception e) {
								jobOrder.setNoOfExpHires(0);
								//e.printStackTrace();
							}
							jobOrder.setWritePrivilegeToSchool(false);
							jobOrder.setSelectedSchoolsInDistrict(new Integer(2));
							jobOrder.setAllGrades(new Integer(2));
							jobOrder.setAllSchoolsInDistrict(new Integer(2));
							jobOrder.setNoSchoolAttach(1);
						}else{
							jobOrder.setWritePrivilegeToSchool(true);
							jobOrder.setSelectedSchoolsInDistrict(new Integer(1));
							jobOrder.setNoSchoolAttach(2);
						}
						//}
						
						jobOrder.setApiJobId(apiJobId);
						jobOrder.setExitURL(exitURL);
						jobOrder.setExitMessage(exitMessage);
						jobOrder.setCreatedDateTime(new Date());
						jobOrder.setIpAddress(IPAddressUtility.getIpAddress(request));
						jobOrder.setIsPortfolioNeeded(false);
						jobOrder.setIsPoolJob(0);
						
						//jobOrder.setSelectedSchoolsInDistrict(2);
						jobOrder.setCreatedBy(userMaster.getUserId());
						
						jobOrder.setOfferVirtualVideoInterview(false);
						jobOrder.setVVIExpiresInDays(0);
						jobOrder.setSendAutoVVILink(false);
						jobOrder.setApprovalBeforeGoLive(1);
						jobOrder.setAssessmentDocument(null);
						jobOrder.setAttachNewPillar(2);
						jobOrder.setAttachDefaultDistrictPillar(2);
						jobOrder.setAttachDefaultSchoolPillar(2);
						jobOrder.setIsExpHireNotEqualToReqNo(false);
						jobOrder.setNotificationToschool(true);
						jobOrderDAO.makePersistent(jobOrder);

						System.out.println("apiJobId"+apiJobId);
						System.out.println("jobTitle"+jobTitle);
						System.out.println("postingStartDate"+postingStartDate);
						System.out.println("postingEndDate"+postingEndDate);

						JSONObject jsonCert = null;
						JobCertification jobCertification = null;
						CertificateTypeMaster ctm = null;
						if(jsonArrayCertification!=null)
							for (int i = 0; i < jsonArrayCertification.size(); i++) {
								jsonCert =  jsonArrayCertification.getJSONObject(i);
								jobCertification = new JobCertification();
								jobCertification.setJobId(jobOrder);
								jobCertification.setCertType(""+jsonCert.get("certificationType"));
								ctm = mapCertificateTypeMaster.get(""+jsonCert.get("certificationType"));
								System.out.println(jsonCert.get("certificationType")+" == "+ ctm.getCertType());
								jobCertification.setCertificateTypeMaster(ctm);
								jobCertification.setStateMaster(ctm.getStateId());
								jobCertificationDAO.makePersistent(jobCertification);
							}
						System.out.println("===============================: positionNos.size():::::::: "+positionNos.size());
						Map<String,DistrictRequisitionNumbers> drnMap = new HashMap<String, DistrictRequisitionNumbers>();
						DistrictRequisitionNumbers drn = null;	
						for (int i = 0; i < positionNos.size(); i++){
							drn = new DistrictRequisitionNumbers();
							drn.setDistrictMaster(districtMaster);
							drn.setIsUsed(true);
							drn.setUserMaster(userMaster);
							drn.setRequisitionNumber(positionNos.get(i).trim());
							if(postingNos!=null){
							    if(postingNos.size()==positionNos.size())
								   drn.setPostingNo(postingNos.get(i).trim());
							}
							drn.setJobCode(apiJobId);
							drn.setJobTitle(jobTitle);
							drn.setCreatedDateTime(new Date());
							drn.setUploadFrom("O");
							drn.setPosType(jobType);
							districtRequisitionNumbersDAO.makePersistent(drn);
							//System.out.println("DistrictRequisitionId::::: "+drn.getDistrictRequisitionId());
							drnMap.put(positionNos.get(i).trim(), drn);
						}
						JobRequisitionNumbers jrn = null;
						SchoolMaster scm = null;
						SchoolInJobOrder schoolInJobOrder = null;
						JSONObject jsonScm = null;		
						//if(jsonArraySchool!=null && hiringAuthority.equalsIgnoreCase("S")) //changed
						if(jsonArraySchool!=null)
							for (int i = 0; i < jsonArraySchool.size(); i++){
								jsonScm = jsonArraySchool.getJSONObject(i);
								scm = mapSchoolMaster.get(jsonScm.getString("schoolName").trim());
								if(scm==null)
									scm = schoolMasterDAO.findBySchoolName(jsonScm.getString("schoolName"),districtMaster);
								schoolInJobOrder = new SchoolInJobOrder();
								schoolInJobOrder.setJobId(jobOrder);
								schoolInJobOrder.setSchoolId(scm);
								schoolInJobOrder.setNoOfSchoolExpHires(new Integer(jsonScm.getString("expectedHires")));
								schoolInJobOrder.setCreatedDateTime(new Date());
								schoolInJobOrderDAO.makePersistent(schoolInJobOrder);

								JSONArray jsonArraypositionNoSchool = new JSONArray();
								jsonArraypositionNoSchool = jsonScm.getJSONArray("positionNo");
								for (int j = 0; j < jsonArraypositionNoSchool.size(); j++){
									jrn = new JobRequisitionNumbers();
									jrn.setJobOrder(jobOrder);
									jrn.setStatus(0);
									String position = jsonArraypositionNoSchool.getString(j).trim();
									if(position!=null)
										position = position.replace("?", "");
									
									drn = drnMap.get(position);
									jrn.setSchoolMaster(scm);
									jrn.setDistrictRequisitionNumbers(drn);
									jobRequisitionNumbersDAO.makePersistent(jrn);
									//System.out.println("jsonArraypositionNoSchool.getString(i).trim():: "+jsonArraypositionNoSchool.getString(j).trim());
								}
							}

						System.out.println("drnMap.size():::: "+drnMap.size());

						if(jsonArraypositionNo!=null && positions>0 && hiringAuthority.equalsIgnoreCase("D")){
							for (int i = 0; i < jsonArraypositionNo.size(); i++){
								jrn = new JobRequisitionNumbers();
								jrn.setJobOrder(jobOrder);
								jrn.setStatus(0);
								String position = jsonArraypositionNo.getString(i).trim();
								if(position!=null)
									position = position.replace("?", "");
								drn = drnMap.get(position);
								jrn.setDistrictRequisitionNumbers(drn);
								System.out.println("drn:: "+drn);
								jobRequisitionNumbersDAO.makePersistent(jrn);
							}
						}
						
						try{
							if(jobOrder!=null)
							jobOrderDAO.totalNoOfHires(jobOrder);
						}catch (Exception e) {
							e.printStackTrace();
						}
						
						message="Position added successfully";
						responseStatus = true;
					}//end of new job order creation
					else {
						System.out.println("jobCategoryMaster inside job updation"+jobCategoryMaster);					
						System.out.println("jobOrder::: "+jobOrder);
						
						if(hiringAuthority.equalsIgnoreCase("D"))
						{
							try {
								Integer expHire = jobOrder.getNoOfExpHires();
								System.out.println("expHire:: "+expHire);
								System.out.println("expectedHires:: "+expectedHires);
								Integer totalExpectedHires = expHire+Integer.valueOf(expectedHires);
								System.out.println("totalExpectedHires:: "+totalExpectedHires);
								jobOrder.setNoOfExpHires(totalExpectedHires);
								jobOrder.setSelectedSchoolsInDistrict(new Integer(2));
								jobOrder.setAllGrades(new Integer(2));
								jobOrder.setAllSchoolsInDistrict(new Integer(2));
								jobOrder.setNoSchoolAttach(1);
							}catch (Exception e) {
								jobOrder.setNoOfExpHires(0);
								//e.printStackTrace();
							}
						}
						else{
							jobOrder.setSelectedSchoolsInDistrict(new Integer(1));
							jobOrder.setNoSchoolAttach(2);
						}
						
						SessionFactory sessionFactory=certificateTypeMasterDAO.getSessionFactory();
						StatelessSession statelesSsession = sessionFactory.openStatelessSession();
						Transaction txOpen =statelesSsession.beginTransaction();
						statelesSsession.update(jobOrder);
						txOpen.commit();
				       	statelesSsession.close(); 
				       	
						System.out.println("getNoOfExpHires:: "+jobOrder.getNoOfExpHires());
						System.out.println("********** positionNos.size():::::::: "+positionNos.size());
										
						Map<String,DistrictRequisitionNumbers> drnMap = new HashMap<String, DistrictRequisitionNumbers>();
						DistrictRequisitionNumbers drn = null;	
						for (int i = 0; i < positionNos.size(); i++){
							drn = new DistrictRequisitionNumbers();
							drn.setDistrictMaster(districtMaster);
							drn.setIsUsed(true);
							drn.setUserMaster(userMaster);
							drn.setRequisitionNumber(positionNos.get(i).trim());
							if(postingNos!=null){
							    if(postingNos.size()==positionNos.size())
								   drn.setPostingNo(postingNos.get(i).trim());
							}
							drn.setJobCode(apiJobId);
							drn.setJobTitle(jobTitle);
							drn.setCreatedDateTime(new Date());
							drn.setUploadFrom("O");
							drn.setPosType(jobType);
							districtRequisitionNumbersDAO.makePersistent(drn);
						
							System.out.println("DistrictRequisitionId:::::Number "+drn.getDistrictRequisitionId());
							drnMap.put(positionNos.get(i).trim(), drn);
						}
						JobRequisitionNumbers jrn = null;
						SchoolMaster scm = null;
						SchoolInJobOrder schoolInJobOrder = null;
						JSONObject jsonScm = null;		
						
						if(jsonArraySchool!=null)
							for (int i = 0; i < jsonArraySchool.size(); i++){
								jsonScm = jsonArraySchool.getJSONObject(i);
								scm = mapSchoolMaster.get(jsonScm.getString("schoolName").trim());
								if(scm==null)
									scm = schoolMasterDAO.findBySchoolName(jsonScm.getString("schoolName"),districtMaster);
								
								schoolInJobOrder=schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, scm);
								System.out.println("schoolInJobOrder value for school exist or not : "+schoolInJobOrder);
								if(schoolInJobOrder==null)
								{
									schoolInJobOrder = new SchoolInJobOrder();	
									System.out.println("once new school is addded in updating position:  "+schoolInJobOrder);
								}
								schoolInJobOrder.setJobId(jobOrder);
								schoolInJobOrder.setSchoolId(scm);
							    
								SchoolInJobOrder checkScholl=schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, scm);
								List<SchoolInJobOrder> checkSchoolExistance=schoolInJobOrderDAO.getSIJO(jobOrder, scm);
								
								System.out.println("checkScholl value****:"+ checkScholl);
								System.out.println("checkSchoolExistance value**** "+ checkSchoolExistance.size());
								
								Integer exptectedhires =schoolInJobOrderDAO.getNoOfSchoolExpHiresBySchool(jobOrder, scm);
								System.out.println("geting expectedhires form schoolInJobOrder table******** : "+exptectedhires);
								System.out.println("geting expectedhires form Given JSON ******* : "+jsonScm.getString("expectedHires"));	
								
								if(checkSchoolExistance.size()==0 && checkScholl==null)
								{
									  schoolInJobOrder.setNoOfSchoolExpHires(new Integer(jsonScm.getString("expectedHires")));
								}
								else{
									int totalExptectedHres=exptectedhires+new Integer(jsonScm.getString("expectedHires"));
									schoolInJobOrder.setNoOfSchoolExpHires(totalExptectedHres);
								}
								schoolInJobOrder.setCreatedDateTime(new Date());
								schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
							
								JSONArray jsonArraypositionNoSchool = new JSONArray();
								jsonArraypositionNoSchool = jsonScm.getJSONArray("positionNo");
																
								for (int j = 0; j < jsonArraypositionNoSchool.size(); j++){
									jrn = new JobRequisitionNumbers();
									jrn.setJobOrder(jobOrder);
									jrn.setStatus(0);
									String position = jsonArraypositionNoSchool.getString(j).trim();
									
									if(position!=null)
										position = position.replace("?", "");
									
									drn = drnMap.get(position);
									
									jrn.setSchoolMaster(scm);
									jrn.setDistrictRequisitionNumbers(drn);
								
									jobRequisitionNumbersDAO.makePersistent(jrn);
									System.out.println("RequisitionNumber ***********:: "+drn.getRequisitionNumber());
								}
							}

						System.out.println("drnMap.size():::: "+drnMap.size());

						if(jsonArraypositionNo!=null && positions>0 && hiringAuthority.equalsIgnoreCase("D")){
							for (int i = 0; i < jsonArraypositionNo.size(); i++){
								jrn = new JobRequisitionNumbers();
								jrn.setJobOrder(jobOrder);
								jrn.setStatus(0);
								String position = jsonArraypositionNo.getString(i).trim();
								if(position!=null)
									position = position.replace("?", "");
								drn = drnMap.get(position);
								jrn.setDistrictRequisitionNumbers(drn);
								System.out.println("drn:: "+drn);
								jobRequisitionNumbersDAO.makePersistent(jrn);
							}
						}

						try{
							if(jobOrder!=null)
							jobOrderDAO.totalNoOfHires(jobOrder);
						}catch (Exception e) {
							e.printStackTrace();
						}
						
						message="Position updated successfully";
						responseStatus = true;
					}//end of update job order
					if(responseStatus){
						jsonResponse = new JSONObject();
						jsonResponse.put("status", true);
						jsonResponse.put("jobId", jobOrder.getApiJobId());
						jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());			
						jsonResponse.put("errorMessage", message);
					}
					else{
						jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
					}	
				}
				else{
					jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
				}

			}catch (Exception e) {
				e.printStackTrace();
				jsonResponse = UtilityAPI.writeJSONErrors(10000, "Server Error.", e);
			}

			out.print(jsonResponse);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;	
	}

	@RequestMapping(value="/service/json/updatePosition.do", method=RequestMethod.GET)
	public String doUpdatePositionByJSONGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		return doUpdatePositionByJSONPOST(map, request, response);
	}

	@Transactional(readOnly=false)
	@RequestMapping(value="/service/json/updatePosition.do", method=RequestMethod.POST)
	public String doUpdatePositionByJSONPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{

		JSONObject jsonResponse = new JSONObject();
		try {			
			PrintWriter out =null;
			out = response.getWriter();
			System.out.println("Enter in addPosition.do JSON");

			response.setContentType("application/json");
			HttpSession session  = request.getSession();
			DistrictMaster districtMaster = null;
			JobOrder jobOrder = null;
			JobCategoryMaster jobCategoryMaster = null;			

			//JSONObject jsonRequest = new JSONObject();

			System.out.println("request.getParameter(request)"+request.getParameter("request"));

			String apiJobId = "";
			String jobTitle = "";
			String postingStartDate = "";
			String postingEndDate = "";
			String jobCategory = "";
			String hiringAuthority = "";
			String expectedHires = "";
			String exitURL = "";
			String exitMessage = "";				
			Date jobStartDate = null;
			Date jobEndDate = null;
			Boolean responseStatus = false;
			Integer errorCode = 10001;
			String errorMsg = "Invalid Credentials.";
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			SchoolMaster schoolMaster = null;
			boolean schoolErExist = false;
			String schErrorText = "";
			boolean certErExist = false;
			String certErrorText = "";

			try{
				String payloadRequest = request.getParameter("request");
				if(payloadRequest==null)
				{
					try {
						payloadRequest = ServiceUtility.getBody(request);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				System.out.println("payloadRequest::::::= "+payloadRequest);

				JSONObject jsonRequest = null;
				jsonRequest = (JSONObject) JSONSerializer.toJSON( payloadRequest ); 
				String authKey = jsonRequest.get("authKey")==null?"":UtilityAPI.parseString(jsonRequest.getString("authKey")).replace(' ', '+');
				String orgType = jsonRequest.get("orgType")==null?"":UtilityAPI.parseString(jsonRequest.getString("orgType")).trim();
				String userEmail = jsonRequest.get("userEmail")==null?"":UtilityAPI.parseString(jsonRequest.getString("userEmail")).replace(' ', '+');
				JSONArray jsonArrayCertification = new JSONArray();
				JSONArray jsonArraySchool = new JSONArray();

				if(!userEmail.trim().equals("")){
					userEmail = Utility.decodeBase64(userEmail);
				}
				UserMaster userMaster = null;
				List<UserMaster> lstUMaster = userMasterDAO.findByEmail(userEmail); 
				if(lstUMaster!=null && lstUMaster.size()>0){
					userMaster=lstUMaster.get(0);
				}

				if(orgType.equalsIgnoreCase("D")){
					districtMaster =  districtMasterDAO.validateDistrictByAuthkey(authKey);				
					if(districtMaster!=null){
						session.setAttribute("apiDistrictMaster", districtMaster);						
						responseStatus = true;
					}
				}
				else if(orgType.equalsIgnoreCase("S")){
					schoolMaster = schoolMasterDAO.validateSchoolByAuthkey(authKey);					
					System.out.println("SchoolMaster IN"+schoolMaster);
					if(schoolMaster!=null){
						districtMaster =  schoolMaster.getDistrictId();
						session.setAttribute("apiSchoolMaster", schoolMaster);
						session.setAttribute("apiDistrictMaster", schoolMaster.getDistrictId());
						responseStatus = true;
					}
				}
				else{
					responseStatus = false;	
					errorCode=10001;
					errorMsg="Invalid Credentials.";
				}

				if(responseStatus){
					if(userEmail.equals("")){
						responseStatus=false;
						errorCode = 10002;
						errorMsg = "User email address is required.";
					}
					else if(!UtilityAPI.validEmail(userEmail)){
						responseStatus=false;
						errorCode = 10003;
						errorMsg = "Invalid User email address.";
					}	
					else if(userMaster==null){
						responseStatus=false;
						errorCode = 10004;
						errorMsg = "User does not exist.";
					}
					else if(!userMaster.getStatus().equals("A")){
						responseStatus=false;
						errorCode = 10006;
						errorMsg = "Candidate is inactivated.";
					}
					else if(userMaster.getDistrictId()==null){
						System.out.println("Block 1");
						responseStatus=false;
						errorCode = 10007;
						errorMsg = "User does not belong to district/school.";
					}
					else if(!userMaster.getDistrictId().equals(districtMaster)){
						System.out.println("Block 2");
						System.out.println(">"+userMaster.getDistrictId());
						System.out.println(">"+districtMaster.getDistrictId());
						responseStatus=false;
						errorCode = 10007;
						errorMsg = "User does not belong to district/school.";
					}			
					else{
						session.setAttribute("userMasterAPI", userMaster);
						responseStatus=true;
					}
				}
				boolean isJSINeeded = false;
				boolean positionCheckFlag = true;
				String jobDescription = "";
				if(responseStatus){
					System.out.println("Enter in try");				
					apiJobId = jsonRequest.get("jobCode")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobCode")).trim();
					jobTitle = jsonRequest.get("jobTitle")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobTitle")).trim();
					String jobType = jsonRequest.get("jobType")==null?"F":UtilityAPI.parseString(jsonRequest.getString("jobType")).trim();
					//jobDescription = StringEscapeUtils.unescapeHtml(jsonRequest.get("jobDescription")==null?"":jsonRequest.getString("jobDescription").trim());
					jobDescription = jsonRequest.get("jobDescription")==null?"":jsonRequest.getString("jobDescription").trim();
					
					postingStartDate = jsonRequest.get("jobStartDate")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobStartDate")).trim(); 
					postingEndDate = jsonRequest.get("jobEndDate")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobEndDate")).trim(); 
					jobCategory = jsonRequest.get("jobCategory")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobCategory")).trim();
					expectedHires = jsonRequest.get("expectedHires")==null?"":UtilityAPI.parseString(jsonRequest.getString("expectedHires")).trim();					
					hiringAuthority = jsonRequest.get("hiringAuthority")==null?"":UtilityAPI.parseString(jsonRequest.getString("hiringAuthority")).trim(); 
					exitURL = jsonRequest.get("exitURL")==null?"":UtilityAPI.parseString(jsonRequest.getString("exitURL")).trim(); 
					exitMessage = jsonRequest.get("exitMessage")==null?"":UtilityAPI.parseString(jsonRequest.getString("exitMessage")).trim();
					String isJSI = jsonRequest.get("isJSINeeded")==null?"":UtilityAPI.parseString(jsonRequest.getString("isJSINeeded")).trim();
					try {
						isJSINeeded=Boolean.valueOf(isJSI);
					} catch (Exception e1) {
						e1.printStackTrace();
					}

					if(districtMaster.getDistrictId()==3628590)
					{
						String [] jobCodeSplit = apiJobId.split("-");
						if(jobCodeSplit.length>0)
						{
							String jobCode = jobCodeSplit[0];
							DistrictSpecificJobCategoryMaster districtSpecificJobCategoryMaster = districtSpecificJobCategoryMasterDAO.checkJobCategoryByJobCode(jobCode,districtMaster);
							if(districtSpecificJobCategoryMaster!=null)
								jobCategoryMaster = districtSpecificJobCategoryMaster.getJobCategoryMaster();
						}
						
						/*System.out.println("OOOOOOOOOOOOOOOOOOOOOOOOOO "+jobCategory);
						System.out.println("JobTitle::::::::::::::: "+jobTitle);
						DistrictSpecificJobCategoryMaster districtSpecificJobCategoryMaster = districtSpecificJobCategoryMasterDAO.checkJobCategoryByJobCode(jobTitle,districtMaster);
						System.out.println("districtSpecificJobCategoryMaster::: "+districtSpecificJobCategoryMaster);
						if(districtSpecificJobCategoryMaster!=null)
							jobCategoryMaster = districtSpecificJobCategoryMaster.getJobCategoryMaster();*/
						
						positionCheckFlag = false;
					}else
						jobCategoryMaster = jobCategoryMasterDAO.findJobCategoryByNmae(jobCategory);
					
					if(districtMaster.getDistrictId()==3628590)
						positionCheckFlag = false;
					System.out.println("apiJobId::: "+apiJobId);
					jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);
					if(apiJobId.equals("")){
						responseStatus = false;
						errorCode = 10008;
						errorMsg = "Job Code is required.";
					}else if(jobOrder==null){
						responseStatus = false;
						errorCode = 10005;
						errorMsg = "Invalid Job Code "+apiJobId+".";
					}else if(hiringAuthority.equals("")){
						responseStatus = false;
						errorCode = 10021;
						errorMsg = "Hiring Authority is required.";
					}
					if(jobOrder!=null){
						List<JobCertification> jobCertificationList= new ArrayList<JobCertification>();
						jobCertificationList = jobCertificationDAO.findCertificationByJobOrder(jobOrder);
						List<JobRequisitionNumbers> jobReqList = new ArrayList<JobRequisitionNumbers>();
						jobReqList = jobRequisitionNumbersDAO.findRequisitionsByJob(jobOrder);
						Map<String,JobRequisitionNumbers> jrnUsedMap = new HashMap<String, JobRequisitionNumbers>();
						Map<String,JobRequisitionNumbers> jrnMap = new HashMap<String, JobRequisitionNumbers>();
						Map<String,JobCertification> jobCertificationMap = new HashMap<String, JobCertification>();
						System.out.println("jobCertificationList.size():::: "+jobCertificationList.size());
						System.out.println("jobReqList.size():::: "+jobReqList.size());
						List<SchoolMaster> schoolMasters = new ArrayList<SchoolMaster>();
						Map<String,List<String>> usedSchoolReqMap = new HashMap<String, List<String>>();
						for (JobRequisitionNumbers jobRequisitionNumbers : jobReqList) {
							jrnMap.put(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber(), jobRequisitionNumbers);
							if(jobRequisitionNumbers.getStatus()==1)
							{
								jrnUsedMap.put(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber(), jobRequisitionNumbers);
								SchoolMaster schoolMaster2 = jobRequisitionNumbers.getSchoolMaster();
								if(schoolMaster2!=null)
								{
									String school = schoolMaster2.getSchoolName();
									List<String> list = usedSchoolReqMap.get(school);
									if(list==null)
										list = new ArrayList<String>();

									list.add(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber());
									usedSchoolReqMap.put(school,list);
									schoolMasters.add(schoolMaster2);
								}
							}
						}
						System.out.println("=========================================== jrnUsedMap::: "+jrnUsedMap.size());

						for (JobCertification jobCertification : jobCertificationList) {
							jobCertificationMap.put(jobCertification.getCertificateTypeMaster().getCertTypeCode(), jobCertification);
						}

						JSONArray jsonArraypositionNo = new JSONArray();
						JSONArray jsonArrayPostingNo = new JSONArray();
						int positions = 0;
						Integer postingNoSize = 0;
						try {
							jsonArraypositionNo = jsonRequest.getJSONArray("positionNo");	
							positions = jsonArraypositionNo.size();
							jsonArrayPostingNo = jsonRequest.getJSONArray("postingNo");	
							postingNoSize = jsonArraypositionNo.size();
						} catch (Exception e) {
							jsonArraypositionNo = null;	
							jsonArraypositionNo = null;
						}
						boolean isUsedPosition = false;
						String isUsedPositionError = "";
						List<String> positionNos = new ArrayList<String>();
						List<String> postingNos = new ArrayList<String>();
						boolean positionNoFlag = false;
						String positionNoError ="";

						try {						
							jsonArraySchool = jsonRequest.getJSONArray("attachedSchools");
						} catch (Exception e) {						
							jsonArraySchool = null;
						}
						List<String> allreadyPosition = new ArrayList<String>();
						boolean schoolRequisionNotEqual = false;
						String schoolRequisionNotEqualError = "";
						Map<String,SchoolMaster> mapSchoolMaster = new HashMap<String, SchoolMaster>();
						boolean jobWithSchool=false;
						//if(jsonArraySchool!=null && jsonArraySchool.size()>0 && hiringAuthority.equalsIgnoreCase("S")){
						if(jsonArraySchool!=null && jsonArraySchool.size()>0){
							SchoolMaster scm = null;
							JSONObject jsonScm = null;		
							jobWithSchool=true;
							for (int i = 0; i < jsonArraySchool.size(); i++){
								jsonScm = jsonArraySchool.getJSONObject(i);
								scm = schoolMasterDAO.findBySchoolName(jsonScm.getString("schoolName"),districtMaster);
								if(scm!=null){
									mapSchoolMaster.put(scm.getSchoolName(), scm);
									try{
										JSONArray jsonArraypositionNoSchool = new JSONArray();
										jsonArraypositionNoSchool = jsonScm.getJSONArray("positionNo");
										int schoolPositionNo = jsonArraypositionNoSchool.size();
										/*									String expHrd = "0";
									try{
										expHrd = jsonScm.get("expectedHires")==null?"":UtilityAPI.parseString(jsonScm.getString("expectedHires")).trim();
									}catch(Exception e){expHrd="0";}

									if(schoolPositionNo!=Integer.parseInt(expHrd))
									{
										schoolRequisionNotEqual = true;
										schoolRequisionNotEqualError = "PositionNo. count does not match as expectedHires for school "+schoolRequisionNotEqualError+jsonScm.getString("schoolName")+",  ";
									}*/
										String expHrd = "0";
										try{
											expHrd = jsonScm.get("expectedHires")==null?"":UtilityAPI.parseString(jsonScm.getString("expectedHires")).trim();
										}catch(Exception e){expHrd="0";}
										if(expHrd.equals("") || expHrd.equalsIgnoreCase("0"))
										{
											schoolRequisionNotEqual = true;
											schoolRequisionNotEqualError = "expectedHires for school "+schoolRequisionNotEqualError+jsonScm.getString("schoolName")+" is required,  ";

										}else{
											if(!positionCheckFlag)
											{
												if(schoolPositionNo!=0 && schoolPositionNo!=Integer.parseInt(expHrd))
												{
													schoolRequisionNotEqual = true;
													schoolRequisionNotEqualError = "PositionNo. count does not match as expectedHires for school "+schoolRequisionNotEqualError+jsonScm.getString("schoolName")+",  ";
												}
											}else
											{
												if(schoolPositionNo!=Integer.parseInt(expHrd))
												{
													schoolRequisionNotEqual = true;
													schoolRequisionNotEqualError = "PositionNo. count does not match as expectedHires for school "+schoolRequisionNotEqualError+jsonScm.getString("schoolName")+",  ";
												}
											}
										}

										for (int j = 0; j < schoolPositionNo; j++){
											String position = jsonArraypositionNoSchool.getString(j).trim();
											if(position!=null)
												position = position.replace("?", "");
												
											positionNos.add(position);

											if(jrnMap.get(position)!=null)
												allreadyPosition.add(position);
										}
									}catch (Exception e) {
										e.printStackTrace();
									}
								}
								else{
									schoolErExist = true;
									schErrorText = schErrorText+jsonScm.getString("schoolName")+", ";
								}
							}
							if(schoolErExist){
								schErrorText = schErrorText.substring(0,schErrorText.length()-3);
								schErrorText = schErrorText + " is not exist in the district.";
							}
							if(schoolRequisionNotEqual){
								schoolRequisionNotEqualError = schoolRequisionNotEqualError.substring(0,schoolRequisionNotEqualError.length()-3);
								schoolRequisionNotEqualError = schoolRequisionNotEqualError + ".";
							}
						}

						if(jsonArraypositionNo!=null && positions>0){ // hiring authority D
							for (int i = 0; i < jsonArraypositionNo.size(); i++){
								String position = jsonArraypositionNo.getString(i).trim();
								if(position!=null)
								position = position.replace("?", "");
								
								System.out.println("position: "+position);
								if(jrnMap.get(position)!=null)
									allreadyPosition.add(position);

								positionNos.add(position);
							}

							if(jrnUsedMap.size()>0)
							{
								Set<String> reqs = jrnUsedMap.keySet();
								System.out.println("555555: "+positionNos.containsAll(reqs));
								if(!positionNos.containsAll(reqs))
								{
									for (String requi : reqs) {
										isUsedPosition = true;
										isUsedPositionError+=isUsedPositionError+requi+", ";
									}
								}
							}
							//postingNo update code
							if(jsonArrayPostingNo!=null && postingNoSize>0){
								for(int k = 0; k<jsonArrayPostingNo.size(); k++){
								String postingNo = jsonArrayPostingNo.getString(k).trim();
									if(postingNoSize!=null){
										postingNo = postingNo.replace("?", ""); 
									}
									postingNos.add(postingNo);
									
								}
							}
							if(positionNos.size()>0 && hiringAuthority.equalsIgnoreCase("D"))//Already have requisition no.
							{
								List<String> checkRequisitionList = new ArrayList<String>(positionNos);
								checkRequisitionList.removeAll(allreadyPosition);

								if(checkRequisitionList.size()>0)
								{
									List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = districtRequisitionNumbersDAO.getDistrictRequisitionNumbersBYReqNo(checkRequisitionList,districtMaster);
									if(lstDistrictRequisitionNumbers.size()>0)
									{
										positionNoFlag = true;
										for (DistrictRequisitionNumbers districtRequisitionNumbers : lstDistrictRequisitionNumbers) {
											positionNoError+=districtRequisitionNumbers.getRequisitionNumber()+", ";
										}
									}
								}
							}
						}else if(hiringAuthority.equalsIgnoreCase("S") || jobWithSchool) {

							List<String> checkRequisitionList = new ArrayList<String>(positionNos);
							checkRequisitionList.removeAll(allreadyPosition);
							System.out.println("checkRequisitionList:: "+checkRequisitionList.size());
							
							
							if(checkRequisitionList.size()>0){
								List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = districtRequisitionNumbersDAO.getDistrictRequisitionNumbersBYReqNo(checkRequisitionList,districtMaster);
								System.out.println("lstDistrictRequisitionNumbers:: "+lstDistrictRequisitionNumbers.size());
								if(lstDistrictRequisitionNumbers.size()>0)
								{
									positionNoFlag = true;
									for(DistrictRequisitionNumbers districtRequisitionNumbers : lstDistrictRequisitionNumbers) {
										positionNoError+=districtRequisitionNumbers.getRequisitionNumber()+", ";
									}
								}
							}
						}
						if(isUsedPosition){
							isUsedPositionError = isUsedPositionError.substring(0,isUsedPositionError.length()-2);
							isUsedPositionError = "Used positions no. "+isUsedPositionError+ ".";
						}

						if(positionNoFlag){
							positionNoError = positionNoError.substring(0,positionNoError.length()-2);
							positionNoError = "Duplicate position nos. "+positionNoError + ".";
						}

						jsonArrayCertification = new JSONArray();
						System.out.println("><><><>");
						try {
							jsonArrayCertification = jsonRequest.getJSONArray("certification");						
						} catch (Exception e) {
							jsonArrayCertification = null;						
						}
						Map<String,CertificateTypeMaster> mapCertificateTypeMaster = new HashMap<String, CertificateTypeMaster>();
						if(jsonArrayCertification!=null && jsonArrayCertification.size()>0){
							CertificateTypeMaster ctm = null;
							JSONObject jsonCtm = null;			
							for (int i = 0; i < jsonArrayCertification.size(); i++){
								jsonCtm = jsonArrayCertification.getJSONObject(i);
								ctm = certificateTypeMasterDAO.findCertificationTypeByCertTypeCode(jsonCtm.getString("certificationType"));
								if(ctm!=null){
									mapCertificateTypeMaster.put(jsonCtm.getString("certificationType"), ctm);
								}
								else{
									certErExist = true;
									certErrorText = certErrorText+jsonCtm.getString("certificationType")+", ";
								}
							}
							if(certErExist){
								certErrorText = certErrorText.substring(0,certErrorText.length()-3);
								certErrorText = "Certification "+certErrorText + " does't exist.";
							}
						}

						System.out.println("jobCode: "+apiJobId);
						System.out.println("jobTitle: "+jobTitle);
						System.out.println("jobDescription: "+jobDescription);
						System.out.println("jobType: "+jobType);
						System.out.println("jobEndDate: "+postingStartDate);
						System.out.println("jobEndDate: "+postingEndDate);
						System.out.println("jobCategory: "+jobCategory);
						System.out.println("expectedHires: "+expectedHires);
						System.out.println("hiringAuthority: "+hiringAuthority);
						System.out.println("exitURL: "+exitURL);
						System.out.println("exitMessage: "+exitMessage);

						try{					
							System.out.println("postingStartDate"+postingStartDate);
							jobStartDate = (Date)formatter.parse(postingStartDate);
							System.out.println("jobStartDate> "+jobStartDate);
						} 
						catch (Exception e) {
							jobStartDate = null;						
						}

						try{
							System.out.println("jobEndDate"+jobEndDate);
							jobEndDate = (Date)formatter.parse(postingEndDate);
							System.out.println("jobEndDate> "+jobEndDate);
						} 
						catch (Exception e) {						
							jobEndDate = null;
						}
						System.out.println("positions:::: "+positions+" expectedHires:: "+expectedHires);
						if(!(jobType.equalsIgnoreCase("P")|| jobType.equalsIgnoreCase("F") || !jobType.equals(""))){
							responseStatus = false;
							errorCode = 10015;
							errorMsg = "Invalid Job Type.";
						}else if(isUsedPosition){
							responseStatus = false;
							errorCode = 10030;
							errorMsg = isUsedPositionError;
						}					
						else if(!postingStartDate.equals("") && jobStartDate==null && jobEndDate.compareTo(new Date())<=0){
							responseStatus = false;
							errorCode = 10017;
							errorMsg = "Posting End Date should be greater than current date.";
						}
						else if(!postingEndDate.equals("") && jobEndDate!=null && jobEndDate.compareTo(jobStartDate)<0){
							responseStatus = false;
							errorCode = 10018;
							errorMsg = "Posting End Date should be greater than Posting Start Date.";
						}
						else if(!jobCategory.equals("") && jobCategoryMaster==null){
							responseStatus = false;
							errorCode = 10020;
							if(districtMaster.getDistrictId()==3628590)
								errorMsg = "Job Code does not exist.";
							else
								errorMsg = "JobCode does not exist.";
						}
						else if((hiringAuthority.equals("S") && (jsonArraySchool==null || jsonArraySchool.size()==0))){
							responseStatus = false;
							errorCode = 10022;
							errorMsg = "Attached Schools is required.";
						}
						else if(certErExist){
							responseStatus = false;
							errorCode = 10023;
							errorMsg = certErrorText;					
						}
						else if(schoolErExist){
							responseStatus = false;
							errorCode = 10024;
							errorMsg = schErrorText;					
						}
						/*else if((jsonArraypositionNo!=null && positions==0) && hiringAuthority.equals("D")){
						responseStatus = false;
						errorCode = 10005;
						errorMsg = "Position No. is required";				
					}*/
						else if(positions!=0 && jsonArraypositionNo!=null && hiringAuthority.equals("D") && positions!=Integer.parseInt(expectedHires) && jobWithSchool==false){
							responseStatus = false;
							errorCode = 10027;
							errorMsg = "Position Nos. must be same as expectedHires "+expectedHires;				
						}
						else if(jsonArraypositionNo!=null && hiringAuthority.equals("D") && (expectedHires.equals("") || expectedHires.equals("0")) && jobWithSchool==false){
							responseStatus = false;
							errorCode = 10026;
							errorMsg = "Expected Hires is required.";
						}else if(positionNoFlag){
							responseStatus = false;
							errorCode = 10028;
							errorMsg = positionNoError;
						}
						else if(schoolRequisionNotEqual){
							responseStatus = false;
							errorCode = 10029;
							errorMsg = schoolRequisionNotEqualError;					
						}else
						{
							System.out.println("jobCategoryMaster"+jobCategoryMaster);					
							System.out.println("jobOrder::: "+jobOrder);
							if(!jobTitle.equals(""))
								jobOrder.setJobTitle(jobTitle);
							if(!jobDescription.equals(""))
							{
								jobOrder.setJobDescription(jobDescription);
							}
							jobOrder.setIsInviteOnly(false);
							if(jobStartDate!=null)
								jobOrder.setJobStartDate(jobStartDate);
							if(jobEndDate!=null)
								jobOrder.setJobEndDate(jobEndDate);
							if(jobCategoryMaster!=null)
								jobOrder.setJobCategoryMaster(jobCategoryMaster);
							if(!jobType.equals(""))
								jobOrder.setJobType(jobType);

							if(hiringAuthority.equalsIgnoreCase("D"))
							{
								try {
									jobOrder.setNoOfExpHires(new Integer(expectedHires));
								} catch (Exception e) {
									jobOrder.setNoOfExpHires(0);
									//e.printStackTrace();
								}
							}
							if(!exitURL.equals(""))
								jobOrder.setExitURL(exitURL);
							if(!exitMessage.equals(""))
								jobOrder.setExitMessage(exitMessage);
							if(!isJSI.equals(""))
							{
								jobOrder.setIsJobAssessment(isJSINeeded);
								if(isJSINeeded)
									jobOrder.setJobAssessmentStatus(1);
								else
									jobOrder.setJobAssessmentStatus(0);
							}

							SessionFactory sessionFactory=certificateTypeMasterDAO.getSessionFactory();
							StatelessSession statelesSsession = sessionFactory.openStatelessSession();
							statelesSsession.update(jobOrder);
							//jobOrderDAO.makePersistent(jobOrder);

							System.out.println("apiJobId"+apiJobId);
							System.out.println("jobTitle"+jobTitle);
							System.out.println("postingStartDate"+postingStartDate);
							System.out.println("postingEndDate"+postingEndDate);

							JSONObject jsonCert = null;
							JobCertification jobCertification = null;
							CertificateTypeMaster ctm = null;
							
							if(jobCertificationList.size()>0)
								jobCertificationDAO.deleteFromJobCertification(jobCertificationList);
							
							if(jsonArrayCertification!=null)
							{
								for (int i = 0; i < jsonArrayCertification.size(); i++) {
									jsonCert =  jsonArrayCertification.getJSONObject(i);
									jobCertification = new JobCertification();
									jobCertification.setJobId(jobOrder);
									jobCertification.setCertType(""+jsonCert.get("certificationType"));
									ctm = mapCertificateTypeMaster.get(""+jsonCert.get("certificationType"));
									System.out.println(jsonCert.get("certificationType")+" == "+ ctm.getCertType());
									jobCertification.setCertificateTypeMaster(ctm);
									jobCertification.setStateMaster(ctm.getStateId());
									jobCertificationDAO.makePersistent(jobCertification);
								}
							}
							System.out.println("===============================: positionNos.size():::::::: "+positionNos.size());
							int usedCount = jrnUsedMap.size();
							List<Integer> drns = new ArrayList<Integer>();
							for (JobRequisitionNumbers jobRequisitionNumbers : jobReqList) {
								DistrictRequisitionNumbers drs = jobRequisitionNumbers.getDistrictRequisitionNumbers();
								String position = drs.getRequisitionNumber();
								if(usedCount>0 && jrnUsedMap.get(position)!=null)
								{
									System.out.println("======================1111111111111 "+position);
								}else
								{
									System.out.println("======================22222222222222 "+position);
									jobRequisitionNumbersDAO.makeTransient(jobRequisitionNumbers);
									drns.add(drs.getDistrictRequisitionId());
								}
							}
							if(drns.size()>0)
							{
								districtRequisitionNumbersDAO.deleteFromDistrictRequisition(drns);
							}

							Map<String,DistrictRequisitionNumbers> drnMap = new HashMap<String, DistrictRequisitionNumbers>();
							System.out.println("positionNos.size():: "+positionNos.size());
							DistrictRequisitionNumbers drn = null;	
							for (int i = 0; i < positionNos.size(); i++){
								drn = new DistrictRequisitionNumbers();
								drn.setDistrictMaster(districtMaster);
								drn.setIsUsed(true);
								drn.setUserMaster(userMaster);
								drn.setRequisitionNumber(positionNos.get(i).trim());
								if(postingNos!=null){
								    if(postingNos.size()==positionNos.size())
									   drn.setPostingNo(postingNos.get(i).trim());
								}
								drn.setJobCode(apiJobId);
								drn.setJobTitle(jobTitle);
								drn.setCreatedDateTime(new Date());
								drn.setUploadFrom("O");
								drn.setPosType(jobType);
								
								System.out.println("jrnUsedMap.get(positionNos.get(i).trim()):: "+jrnUsedMap.get(positionNos.get(i).trim()));
								if(jrnUsedMap.get(positionNos.get(i).trim())==null)
								{
									districtRequisitionNumbersDAO.makePersistent(drn);
									drnMap.put(positionNos.get(i).trim(), drn);
								}
								//System.out.println("DistrictRequisitionId::::: "+drn.getDistrictRequisitionId());
							}
							JobRequisitionNumbers jrn = null;
							SchoolMaster scm = null;
							SchoolInJobOrder schoolInJobOrder = null;
							JSONObject jsonScm = null;		
							//if(jsonArraySchool!=null && hiringAuthority.equalsIgnoreCase("S")){
							if(jsonArraySchool!=null){

								System.out.println("555555555555555555555555555555555");
								schoolInJobOrderDAO.deleteFromSchoolInJobOrder(schoolMasters, jobOrder);
								System.out.println("7777777777777777777777777777777777");

								for (int i = 0; i < jsonArraySchool.size(); i++){
									jsonScm = jsonArraySchool.getJSONObject(i);
									scm = mapSchoolMaster.get(jsonScm.getString("schoolName"));
									if(scm==null)
										scm = schoolMasterDAO.findBySchoolName(jsonScm.getString("schoolName"),districtMaster);
									
									schoolInJobOrder = new SchoolInJobOrder();
									schoolInJobOrder.setJobId(jobOrder);
									schoolInJobOrder.setSchoolId(scm);
									schoolInJobOrder.setNoOfSchoolExpHires(new Integer(jsonScm.getString("expectedHires")));
									schoolInJobOrder.setCreatedDateTime(new Date());
									schoolInJobOrderDAO.makePersistent(schoolInJobOrder);

									JSONArray jsonArraypositionNoSchool = new JSONArray();
									jsonArraypositionNoSchool = jsonScm.getJSONArray("positionNo");
									for (int j = 0; j < jsonArraypositionNoSchool.size(); j++){
										jrn = new JobRequisitionNumbers();
										jrn.setJobOrder(jobOrder);
										jrn.setStatus(0);
										String position = jsonArraypositionNoSchool.getString(j).trim();
										if(position!=null)
											position = position.replace("?", "");
											
										drn = drnMap.get(position);
										jrn.setSchoolMaster(scm);
										jrn.setDistrictRequisitionNumbers(drn);
										if(jrnUsedMap.get(position)==null)
											jobRequisitionNumbersDAO.makePersistent(jrn);
										//System.out.println("jsonArraypositionNoSchool.getString(i).trim():: "+jsonArraypositionNoSchool.getString(j).trim());
									}
								}
							}
							System.out.println("drnMap.size():::: "+drnMap.size());

							if(jsonArraypositionNo!=null && positions>0 && hiringAuthority.equalsIgnoreCase("D")){
								for (int i = 0; i < jsonArraypositionNo.size(); i++){
									jrn = new JobRequisitionNumbers();
									jrn.setJobOrder(jobOrder);
									jrn.setStatus(0);
									String position = jsonArraypositionNo.getString(i).trim();
									if(position!=null)
										position = position.replace("?", "");
									drn = drnMap.get(position);
									jrn.setDistrictRequisitionNumbers(drn);
									if(jrnUsedMap.get(position)==null)
									{
										jobRequisitionNumbersDAO.makePersistent(jrn);
									}
								}
							}
							responseStatus = true;
							try{
								if(jobOrder!=null)
								jobOrderDAO.totalNoOfHires(jobOrder);
							}catch (Exception e) {
								e.printStackTrace();
							}
						}
						
						
						

						if(responseStatus){
							jsonResponse = new JSONObject();
							jsonResponse.put("status", true);
							jsonResponse.put("jobId", jobOrder.getApiJobId());
							jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
							jsonResponse.put("errorMessage", "Position updated successfully.");

						}
						else{
							jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
						}	
					}else
					{
						responseStatus = false;
						errorCode = 10005;
						errorMsg = "Invalid Job Code "+apiJobId+".";
						jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
					}
				}
				else{
					jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
				}

			}catch (Exception e) {
				e.printStackTrace();
				jsonResponse = UtilityAPI.writeJSONErrors(10000, "Server Error.", e);
			}

			out.print(jsonResponse);
		}catch (Exception e) {
			e.printStackTrace();
			jsonResponse = UtilityAPI.writeJSONErrors(10000, "Server Error.", e);
		}
		return null;	
	}
	@RequestMapping(value="/service/json/rejectedApplicant.do",method=RequestMethod.GET)
	public String doRejectPositionByJSONGET(ModelMap map, HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("Enter in Reject Position API using GET Request");
		return doRejectPositionByJSONPOST(map,request,response);
	}
	
	@RequestMapping(value="/service/json/rejectedApplicant.do",method=RequestMethod.POST)
	public String  doRejectPositionByJSONPOST(ModelMap map, HttpServletRequest request, HttpServletResponse response)
	{	
		JSONObject jsonResponse = new JSONObject();
		try {			
			PrintWriter out =null;
			out = response.getWriter();
			System.out.println("Enter in Reject Position API using POST Request");

			response.setContentType("application/json");
			HttpSession session  = request.getSession();
			DistrictMaster districtMaster = null;
			JobOrder jobOrder = null;
			TeacherDetail teacherDetail=null;
			
			System.out.println("request.getParameter(request)"+request.getParameter("request"));
            
			
			String apiJobId = "";
			String rejectJobCode = "";
			String postRejectedDate ="";
			Date rejectedDate = null;
			Boolean responseStatus = false;
			Integer errorCode = 10001;
			String errorMsg = "Invalid Credentials.";
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			SchoolMaster schoolMaster = null;
			
			try{
				String payloadRequest = request.getParameter("request");
				if(payloadRequest==null)
				{
					try {
						payloadRequest = ServiceUtility.getBody(request);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				System.out.println("payloadRequest::::::= "+payloadRequest);

				JSONObject jsonRequest = null;
				jsonRequest = (JSONObject) JSONSerializer.toJSON(payloadRequest ); 
				String authKey = jsonRequest.get("authKey")==null?"":UtilityAPI.parseString(jsonRequest.getString("authKey")).replace(' ', '+');
				String orgType = jsonRequest.get("orgType")==null?"":UtilityAPI.parseString(jsonRequest.getString("orgType")).trim();
				String userEmail = jsonRequest.get("userEmail")==null?"":UtilityAPI.parseString(jsonRequest.getString("userEmail")).replace(' ', '+');
				String applicantEmail = jsonRequest.get("applicantEmail")==null?"":UtilityAPI.parseString(jsonRequest.getString("applicantEmail")).replace(' ', '+');
				
				if(!userEmail.trim().equals("")){
					userEmail = Utility.decodeBase64(userEmail);
				}
				
				if(!applicantEmail.trim().equals("")){
					applicantEmail = Utility.decodeBase64(applicantEmail);
				}
				
				UserMaster userMaster = null;
				List<UserMaster> lstUMaster = userMasterDAO.findByEmail(userEmail); 
				if(lstUMaster!=null && lstUMaster.size()>0){
					userMaster=lstUMaster.get(0);
				}
				
				
				List<TeacherDetail> lstTDetail = teacherDetailDAO.findByEmail(applicantEmail);
				System.out.println("lstTDetail lstTDetail value ::: "+lstTDetail);
				
				if(lstTDetail!=null && lstTDetail.size()>0){
					teacherDetail=lstTDetail.get(0);
					System.out.println("Teacher email ID ****::: "+teacherDetail.getEmailAddress());
					System.out.println("Teacher ID*********::: "+teacherDetail.getTeacherId());
				}

				if(orgType.equalsIgnoreCase("D")){
					districtMaster =  districtMasterDAO.validateDistrictByAuthkey(authKey);				
					System.out.println("districtMaster********* IN******"+districtMaster);
					if(districtMaster!=null){
						session.setAttribute("apiDistrictMaster", districtMaster);						
						responseStatus = true;
					}
				}
				else if(orgType.equalsIgnoreCase("S")){
					schoolMaster = schoolMasterDAO.validateSchoolByAuthkey(authKey);					
					System.out.println("SchoolMaster ********** IN**********"+schoolMaster);
					if(schoolMaster!=null){
						districtMaster =  schoolMaster.getDistrictId();
						session.setAttribute("apiSchoolMaster", schoolMaster);
						session.setAttribute("apiDistrictMaster", schoolMaster.getDistrictId());
						responseStatus = true;
					}
				}
				else{
					responseStatus = false;	
					errorCode=10001;
					errorMsg="Invalid Credentials.";
				}

				if(responseStatus){
					System.out.println("Enter in first responseStatus IF");
					if(userEmail.equals("")){
						responseStatus=false;
						errorCode = 10002;
						errorMsg = "User email address is required.";
					}
					else if(!UtilityAPI.validEmail(userEmail)){
						responseStatus=false;
						errorCode = 10003;
						errorMsg = "Invalid User email address.";
					}
					else if(applicantEmail.equals("")){
						responseStatus = false;
						errorCode = 10032;
						errorMsg = "Applicant email address is required.";
						}
					else if(!Utility.validEmailForTeacher(applicantEmail)){
						responseStatus = false;
						errorCode = 10033;
						errorMsg = "Invalid Applicant email address.";
					}
					else if(userMaster==null){
						responseStatus=false;
						errorCode = 10004;
						errorMsg = "User does not exist.";
					}
					else if(!userMaster.getStatus().equals("A")){
						responseStatus=false;
						errorCode = 10006;
						errorMsg = "User is inactivated.";
					}
					else if(userMaster.getDistrictId()==null){
						System.out.println("Block 1");
						responseStatus=false;
						errorCode = 10007;
						errorMsg = "User does not belong to district/school.";
					}
					else if(!userMaster.getDistrictId().equals(districtMaster)){
						System.out.println("Block 2");
						System.out.println(">"+userMaster.getDistrictId());
						System.out.println(">"+districtMaster.getDistrictId());
						responseStatus=false;
						errorCode = 10007;
						errorMsg = "User does not belong to district/school.";
					}
					else if(teacherDetail==null){
						responseStatus=false;
						errorCode = 10034;
						errorMsg = "Applicant does not exist.";
					}
					else if(!teacherDetail.getStatus().equals("A")){
						responseStatus=false;
						errorCode = 10035;
						errorMsg = "Applicant is inactivated.";
					}
					else{
						session.setAttribute("userMasterAPI", userMaster);
						responseStatus=true;
					}
				}
				if(responseStatus){
					System.out.println("Enter in second responseStatus IF");
					
					apiJobId = jsonRequest.get("jobCode")==null?"":UtilityAPI.parseString(jsonRequest.getString("jobCode")).trim();
					rejectJobCode = jsonRequest.get("rejectCode")==null?"":UtilityAPI.parseString(jsonRequest.getString("rejectCode")).trim();
					postRejectedDate = jsonRequest.get("rejectedDate")==null?"":UtilityAPI.parseString(jsonRequest.getString("rejectedDate")).trim();
					
					
					System.out.println("jobCode: "+apiJobId);
					System.out.println("rejectCode: "+rejectJobCode);
					System.out.println("rejectedDate: "+postRejectedDate);

					try{					
						rejectedDate = (Date)formatter.parse(postRejectedDate);
						System.out.println("rejectedDate****with date formated "+rejectedDate);
					} 
					catch (Exception e) {
						rejectedDate = null;						
					}
					
					if(apiJobId.equals("")){
						responseStatus = false;
						errorCode = 10008;
						errorMsg = "Job Code is required.";
					}
					/*else if(rejectJobCode.equals(""))
					{
						responseStatus = false;
						errorCode = 10008;
						errorMsg = "Reject Code is required.";
					}*/
					/*else if(rejectedDate.equals(""))
					{
						responseStatus = false;
						errorCode = 10008;
						errorMsg = "Reject Date is required.";
					}*/
					
					System.out.println("apiJobId::: "+apiJobId);
					jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);
					System.out.println("jobOrder vlaue ***********::: "+jobOrder);
					
					if(jobOrder==null){
						responseStatus = false;
						errorCode = 10005;
						errorMsg = "Invalid Job Code "+apiJobId+".";
					}
					if(jobOrder!=null){
						JobForTeacher jobForTeacher = jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
						if(jobForTeacher!=null){
							StatusMaster statusMaster = jobForTeacher.getStatus();
							String checkHired = statusMaster.getStatusShortName();
							if(checkHired.equalsIgnoreCase("hird")){
								StatusMaster master1=WorkThreadServlet.statusMap.get("rem");
								try{
									jobForTeacher.setStatus(master1);
									jobForTeacher.setStatusMaster(master1);
									jobForTeacher.setUpdatedDate(new Date());
									jobForTeacher.setLastActivity("Applicant Rejected");
									jobForTeacher.setLastActivityDate(new Date());
									jobForTeacher.setUpdatedBy(userMaster);
									jobForTeacher.setUserMaster(userMaster);
									if(master1!=null){
										jobForTeacher.setApplicationStatus(master1.getStatusId());
									}
									jobForTeacherDAO.makePersistent(jobForTeacher);
								}catch(Exception e){
									e.printStackTrace();
								}
								try{
									TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
									StatusMaster masterHired=WorkThreadServlet.statusMap.get("hird");
									tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(teacherDetail, jobOrder,masterHired,"A");
									if(tSHJ!=null){
										tSHJ.setStatus("I");
										tSHJ.setHiredByDate(null);
										tSHJ.setUpdatedDateTime(new Date());
										tSHJ.setUserMaster(userMaster);
										teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								
								try{
									TeacherStatusHistoryForJob teacherstatusHistroyForJob = new TeacherStatusHistoryForJob();
									teacherstatusHistroyForJob.setUserMaster(userMaster);
									teacherstatusHistroyForJob.setTeacherDetail(teacherDetail);
									teacherstatusHistroyForJob.setJobOrder(jobOrder);
									teacherstatusHistroyForJob.setStatusMaster(master1);
									teacherstatusHistroyForJob.setStatus("A");
									teacherstatusHistroyForJob.setCreatedDateTime(new Date());
									teacherStatusHistoryForJobDAO.makePersistent(teacherstatusHistroyForJob);
								}catch(Exception e){
									e.printStackTrace();
								}
								System.out.println("teacherStatusHistoryForJob inserted successfully :*****");
								try{
									TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
									teacherStatusNoteObj.setUserMaster(userMaster);
									teacherStatusNoteObj.setJobOrder(jobOrder);
									teacherStatusNoteObj.setTeacherDetail(teacherDetail);
									teacherStatusNoteObj.setStatusMaster(master1);
									teacherStatusNoteObj.setDistrictId(districtMaster.getDistrictId());
									teacherStatusNoteObj.setStatusNotes("Applicant Rejected");
									teacherStatusNoteObj.setCreatedDateTime(new Date());
									teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
								}catch(Exception e){
									e.printStackTrace();
								}
								System.out.println("teacherStatusNotes inserted successfully :*****");
							
								responseStatus = true;
							}
							else{
								responseStatus = false;
								errorCode = 10036;
								errorMsg = "Applicant is not Hired Yet.";
								jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
							}
							
						}
						else{
							responseStatus = false;
							errorCode = 10037;
							errorMsg = "Applicant does not exist.";
							jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
						}

						if(responseStatus){
							try{
								if(jobOrder!=null)
								jobOrderDAO.totalNoOfHires(jobOrder);
							}catch (Exception e) {
								e.printStackTrace();
							}
							
							jsonResponse = new JSONObject();
							jsonResponse.put("status", true);
							jsonResponse.put("jobId", jobOrder.getApiJobId());
							jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
							jsonResponse.put("errorMessage", "Applicant rejected successfully.");
							
						}
						else{
							jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
						}	
					}
					else
					{
						responseStatus = false;
						errorCode = 10005;
						errorMsg = "Job Code "+apiJobId+" does not exists.";
						jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
					}
				}
				else{
					jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg,null);
				}

			}catch (Exception e) {
				e.printStackTrace();
				jsonResponse = UtilityAPI.writeJSONErrors(10000, "Server Error.", e);
			}
			
			out.print(jsonResponse);
			
		}catch (Exception e) {
			e.printStackTrace();
			jsonResponse = UtilityAPI.writeJSONErrors(10000, "Server Error.", e);
		}
		return null;	
	}
	
	 @RequestMapping(value="/api/service/locationUpdate.do", method=RequestMethod.GET)
	  public String doUpdateLocationByGET(ModelMap map, HttpServletRequest request, HttpServletResponse response){
		 
		  PrintWriter out = null;
		  Integer errorCode = null;;
		  String errorMsg = "";
		  try{
			   System.out.println("Entering doUpdateLocationByGET Block>>>>>>>>>>>>>>>>>>"); 
			  
			   out = response.getWriter();
			   response.setContentType("text/html");
			   HttpSession session = request.getSession();
			   SchoolMaster schoolMaster =null;
			   DistrictMaster districtMaster = null; 
			   List<SchoolTypeMaster> schoolTypeMaster = null;
			   SchoolTypeMaster schoolTypeMstr = null;
			   StateMaster stateMaster = null;
			   List<GeoMapping> geoMapping = null;
			   GeoMapping geoMapp = null; 
			   List<RegionMaster> regionMaster = null;
			   RegionMaster regionMastr = null;
			   List<GeoZoneMaster> geoZoneMaster = null;
			   GeoZoneMaster geoZoneMastr = null;
			   
			   String locationCode = "";
			   String schoolName = "";
			   String schoolType = "";
			   String noOfTeachers = "";
			   String noOfStudents = "";
			   String schoolSite = "";
			   String address = "";
			   String city = "";
			   String zip = "";
			   String state ="";
			   String geography = "";
			   String region = "";
			   String phoneNumber = "";
			   String alternatePhone = "";
			   String faxNumber = "";
			   String website = "";
			   String geoZone = "";
			   
			   String orgType ="S";
			  
			   Boolean responseStatus = false;
			   
		   try{ 
		      String authKey = request.getParameter("authKey")==null?"":request.getParameter("authKey").trim();
		  
			  if(orgType.equalsIgnoreCase("S")){
				  schoolMaster = schoolMasterDAO.validateSchoolByAuthkey(authKey);
				  System.out.println("schoolMaster IN :**********:::: "+schoolMaster);
				  if(schoolMaster!=null){
					  districtMaster = schoolMaster.getDistrictId();
					  session.setAttribute("apiSchoolMaster", schoolMaster);
					  session.setAttribute("apiDistrictMaster", schoolMaster.getDistrictId());
					  responseStatus = true;
				  }else{
					  responseStatus = false;
					  errorCode=10001;
					  errorMsg="Invalid Credentials.";
				  }
			  }
			
			  if(responseStatus){
				  locationCode = request.getParameter("locationCode")==null?"":request.getParameter("locationCode");
				  schoolName = request.getParameter("schoolName")==null?"":request.getParameter("schoolName");
				  schoolType = request.getParameter("schoolType")==null?"":request.getParameter("schoolType");
				  noOfTeachers = request.getParameter("noOfTeachers")==null?"":request.getParameter("noOfTeachers");
				  noOfStudents = request.getParameter("noOfStudents")==null?"":request.getParameter("noOfStudents");
				  schoolSite = request.getParameter("schoolSite")==null?"":request.getParameter("schoolSite");
				  address = request.getParameter("address")==null?"":request.getParameter("address");
				  city = request.getParameter("city")==null?"":request.getParameter("city");
				  zip = request.getParameter("zip")==null?"":request.getParameter("zip");
				  
				  state = request.getParameter("state")==null?"":request.getParameter("state");
				  geography = request.getParameter("geography")==null?"":request.getParameter("geography");
				  region = request.getParameter("region")==null?"":request.getParameter("region");
				  phoneNumber = request.getParameter("phoneNumber")==null?"":request.getParameter("phoneNumber");
				  alternatePhone = request.getParameter("alternatePhone")==null?"":request.getParameter("alternatePhone");
				  faxNumber = request.getParameter("faxNumber")==null?"":request.getParameter("faxNumber");
				  website = request.getParameter("website")==null?"":request.getParameter("website");
				  geoZone = request.getParameter("geoZone")==null?"":request.getParameter("geoZone");
				  
				  System.out.println("locationCode: "+locationCode);
				  System.out.println("schoolName: "+schoolName);
				  System.out.println("schoolType: "+schoolType);
				  System.out.println("noOfTeachers: "+noOfTeachers);
				  System.out.println("noOfStudents: "+noOfStudents);
				  System.out.println("schoolSite: "+schoolSite);
				  System.out.println("address: "+address);
				  System.out.println("city: "+city);
				  System.out.println("zip: "+zip);
				  System.out.println("state: "+state);
				  System.out.println("geography: "+geography);
				  System.out.println("region: "+region);
				  System.out.println("phoneNumber: "+phoneNumber);
				  System.out.println("alternatePhone: "+alternatePhone);
				  System.out.println("faxNumber: "+faxNumber);
				  System.out.println("website: "+website);
				  System.out.println("geoZone: "+geoZone);
				 
				  schoolMaster.setLocationCode(locationCode);
				  schoolMaster.setSchoolName(schoolName);
				 
				  Criterion criterion1 = Restrictions.eq("schoolTypeName", schoolType);
				  schoolTypeMaster = schoolTypeMasterDAO.findByCriteria(criterion1);
				  System.out.println("schoolTypeMaster ****** : "+schoolTypeMaster);
				  if(schoolTypeMaster!=null){
				  schoolTypeMstr = schoolTypeMaster.get(0); 
				  schoolTypeMstr.setSchoolTypeName(schoolType);
				  schoolMaster.setSchoolTypeId(schoolTypeMstr);
				  }else{
					  responseStatus = false;
					  errorCode = 10038;
					  errorMsg = "School type does not exist";
				  }
				  
				  System.out.println("schoolTypeMstr : "+schoolTypeMstr.getSchoolTypeName());
				  
				  schoolMaster.setNoOfTeachers(Long.valueOf(noOfTeachers));
				  schoolMaster.setNoOfStudents(Long.valueOf(noOfStudents));
				
				  schoolMaster.setAddress(address);
				  schoolMaster.setCityName(city);
				  schoolMaster.setZip(zip);                                     
				
				  stateMaster = stateMasterDAO.findByStateCode(state); 
				  if(stateMaster!=null){
				  stateMaster.setStateShortName(state);
				  schoolMaster.setStateMaster(stateMaster);
				  }else{
					  responseStatus = false;
					  errorCode = 10039;
					  errorMsg = "State does not exist";
				  }
				  
				  Criterion criterion2 = Restrictions.eq("actualGeoName", geography);
				  geoMapping = geoMappingDAO.findByCriteria(criterion2);
				  System.out.println("geoMapping: "+geoMapping.size());
	              if(geoMapping!=null){
	            	  geoMapp = geoMapping.get(0);
	            	  geoMapp.setActualGeoName(geography);
	            	  schoolMaster.setGeoMapping(geoMapp);
	              }else{
					  responseStatus = false;
					  errorCode = 10040;
					  errorMsg = "Geography does not exist";
				  }
				  
	              Criterion criterion3 = Restrictions.eq("regionName", region);
	              regionMaster = regionMasterDAO.findByCriteria(criterion3); 
	              if(regionMaster!=null){
	            	  regionMastr = regionMaster.get(0);
	            	  regionMastr.setRegionName(region);
	            	  schoolMaster.setRegionId(regionMastr);
	              }

	              schoolMaster.setPhoneNumber(phoneNumber);
				  schoolMaster.setAlternatePhone(alternatePhone);
				  schoolMaster.setFaxNumber(faxNumber);
				  schoolMaster.setWebsite(website);
				 

				  Criterion criterion4 = Restrictions.eq("geoZoneName", geoZone);
				  geoZoneMaster = geoZoneMasterDAO.findByCriteria(criterion4);
				  if(geoZoneMaster!=null){
					  geoZoneMastr = geoZoneMaster.get(0);
					  geoZoneMastr.setGeoZoneName(geoZone);
					  schoolMaster.setGeoZoneMaster(geoZoneMastr);
				  }
				 
				  schoolMasterDAO.updatePersistent(schoolMaster);
				  System.out.println("Location updated successfully");
				  responseStatus = true;
			  }else{
				    String msg= responseMessage(errorCode,errorMsg,null);
				    out.print(msg);
			  }
			  if(responseStatus)
			  {   	errorMsg = "Location updated successfully.";
					errorCode = null;
				    String msg= responseMessage(errorCode,errorMsg,null);
				    out.print(msg);
			  }
			    
		  }catch(Exception e){
			    e.printStackTrace();
			    errorCode=1000;
				errorMsg = "Server Error.";
				String msg = responseMessage(errorCode,errorMsg,null);
				System.out.println("responseStatus inside : "+responseStatus);
				out.print(msg);
		  }
		 }catch (Exception e) {
			e.printStackTrace();
			errorCode=1000;
			errorMsg = "Server Error.";
			String msg = responseMessage(errorCode,errorMsg,null);
			out.print(msg);
		}
	return null;	  
}
	 public String responseMessage(Integer errorCode,String errorMsg, Exception e){
		
		String returnMsg =""; 
		if(errorCode==null){
	     	returnMsg = "<div style='margin-top:25%;height:auto;width:auto' align='center'>Error Message: "+errorMsg+"</div>";
	     	System.out.println("inside if : "+errorCode);
		}else{
			returnMsg = "<div style='margin-top:25%;height:auto;width:auto' align='center'>Error Message:"+errorMsg+"<br>Error Code: "+errorCode+"</div>";
		    System.out.println("inside else: "+errorCode);
		}
		return returnMsg;
	} 
   
}
