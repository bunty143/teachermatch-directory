package tm.api.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.sf.json.JSONObject;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import tm.api.UtilityAPI;
import tm.api.services.ServiceUtility;
import tm.bean.InternalTransferCandidates;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherLoginHistory;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherPreference;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherLoginHistoryDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherPreferenceDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.user.MasterPasswordDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.MD5Encryption;
import tm.services.district.PrintOnConsole;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

@Controller
public class AuthenticateController {
	
    String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private ServiceUtility serviceUtility;
	public void setServiceUtility(ServiceUtility serviceUtility) {
		this.serviceUtility = serviceUtility;
	}
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired 
	private MasterPasswordDAO masterPasswordDAO;
	
	@Autowired
	private TeacherLoginHistoryDAO teacherLoginHistoryDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private TeacherPreferenceDAO teacherPreferenceDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	
	@RequestMapping(value="/service/json/authenticateOrganization.do", method=RequestMethod.GET)
	public String doAuthenticateOrganizationJSONGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		PrintWriter out =null;
		JSONObject jsonResponse = null;
		response.setContentType("application/json");
		try {
			Boolean isValidUser = false;
			out = response.getWriter();
			
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			
			if(isValidUser){
				jsonResponse = new JSONObject();
				jsonResponse.put("status", true);
				jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());	
				jsonResponse.put("errorMessage", Utility.getLocaleValuePropByKey("msgAuthenticatedSuccessfully", locale));
			}
			else{				
				jsonResponse = UtilityAPI.writeJSONErrors(new Integer(mapDistSchDetail.get("errorCode")),mapDistSchDetail.get("errorMsg"), null);

			}						
		} 
		catch (Exception e){
			jsonResponse = UtilityAPI.writeJSONErrors(10017, Utility.getLocaleValuePropByKey("msgServerError", locale), e);
			e.printStackTrace();
		}
		out.print(jsonResponse);
		return null;
	}
	
	
	@RequestMapping(value="/service/xml/authenticateOrganization.do", method=RequestMethod.GET)
	public String doAuthenticateOrganizationXMLGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		
		Boolean isValidUser = false;
		try {
			response.setContentType("text/xml");
			
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = null;
			Document doc = null;
			Element root = null;			
			
			docBuilder = builderFactory.newDocumentBuilder();
			doc = docBuilder.newDocument();
			root = doc.createElement("teachermatch");
			doc.appendChild(root);
			
			PrintWriter out = response.getWriter();
			
			out = response.getWriter();
			
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			
			if(isValidUser){
				Element eleStatus = doc.createElement("status");
				root.appendChild(eleStatus);
				Text textStatus = doc.createTextNode("true");
				eleStatus.appendChild(textStatus);
				
				Element timeStamp = doc.createElement("timeStamp");
				root.appendChild(timeStamp);
				Text timeStampResult = doc.createTextNode(UtilityAPI.getCurrentTimeStamp());
				timeStamp.appendChild(timeStampResult);
			}
			else{
				UtilityAPI.writeXMLErrors(new Integer(mapDistSchDetail.get("errorCode")),mapDistSchDetail.get("errorMsg"),null, doc, root);
			}		
						
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
			out.println(sw.toString());
		} 
		catch (Exception e) {			
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/service/json/authenticateCandidate.do", method=RequestMethod.GET)
	public String doAuthenticateCandidateJSONGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		
		Map<String,String> mapTDetail = new HashMap<String, String>();
		mapTDetail  = serviceUtility.validateTeacher(request);
		
		TeacherDetail teacherDetail = null;		
		Boolean tCreatedOrExist = false;
		Integer errorCode=0;
		String errorMsg="";
		
		PrintWriter out = null;
		JSONObject jsonResponse = new JSONObject();
		response.setContentType("application/json");
		HttpSession session = request.getSession();
		try {			
			Boolean isValidUser = false;
			out =  response.getWriter();
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
			System.out.println("mapDistSchDetail"+mapDistSchDetail);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			System.out.println("isValidUser>"+isValidUser );
			if(!isValidUser){
				System.out.println("(((");
				jsonResponse = UtilityAPI.writeJSONErrors(new Integer(mapDistSchDetail.get("errorCode")),mapDistSchDetail.get("errorMsg"), null);
				System.out.println(jsonResponse);
			}
			else{		
				
				tCreatedOrExist = new Boolean(mapTDetail.get("tCreatedOrExist").trim());
				
				if(!tCreatedOrExist){
					System.out.println("mapTDetail.get(errorCode).trim()"+mapTDetail);
					tCreatedOrExist=false;
					errorCode = new Integer(mapTDetail.get("errorCode").trim());
					errorMsg =  mapTDetail.get("errorMsg");
				}				
				else{
					teacherDetail = (TeacherDetail) session.getAttribute("apiTeacherDetail");					
					tCreatedOrExist=true;
				}
				
				if(tCreatedOrExist){
					session.setAttribute("apiTeacherDetail", teacherDetail);
					jsonResponse.put("status",true);				
					jsonResponse.put("timestamp",UtilityAPI.getCurrentTimeStamp());
				}
				else{
					jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg, null);
				}
			}
		}catch (Exception e){
			jsonResponse = UtilityAPI.writeJSONErrors(10017, Utility.getLocaleValuePropByKey("msgServerError", locale), e);
			e.printStackTrace();
		}		
		System.out.println(jsonResponse);
		out.print(jsonResponse);
		return null;
	}
	@RequestMapping(value="/service/xml/authenticateCandidate.do", method=RequestMethod.GET)
	public String doAuthenticateCandidateXMLGET(ModelMap map,HttpServletRequest request, HttpServletResponse response){
					
		response.setContentType("text/xml");
		
		Map<String,String> mapTDetail = new HashMap<String, String>();
		mapTDetail  = serviceUtility.validateTeacher(request);
		
		TeacherDetail teacherDetail = null;
		
		
		Boolean tCreatedOrExist = false;
		Integer errorCode=0;
		String errorMsg="";
		
		PrintWriter out = null;
		HttpSession session = request.getSession();
		try {
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = null;
			Document doc = null;
			Element root = null;
			
			
			docBuilder = builderFactory.newDocumentBuilder();
			doc = docBuilder.newDocument();
			root = doc.createElement("teachermatch");
			doc.appendChild(root);			
			
			try {			
				out =  response.getWriter();
				
				Boolean isValidUser = false;						
				Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
				isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
				
				if(!isValidUser){
					UtilityAPI.writeXMLErrors(new Integer(mapDistSchDetail.get("errorCode")),mapDistSchDetail.get("errorMsg"),null, doc, root);
				}
				else{					
					tCreatedOrExist = new Boolean(mapTDetail.get("tCreatedOrExist").trim());
					if(!tCreatedOrExist){
						System.out.println("mapTDetail.get(errorCode).trim()"+mapTDetail);
						tCreatedOrExist=false;
						errorCode = new Integer(mapTDetail.get("errorCode").trim());
						errorMsg =  mapTDetail.get("errorMsg");
					}
					
					else{
						teacherDetail = (TeacherDetail) session.getAttribute("apiTeacherDetail");						
						tCreatedOrExist=true;
						
					}
					
					if(tCreatedOrExist){			
						session.setAttribute("apiTeacherDetail", teacherDetail);
						
						Element eleStatus = doc.createElement("status");
						root.appendChild(eleStatus);
						Text textStatus = doc.createTextNode("true");
						eleStatus.appendChild(textStatus);						
				
						
						Element timeStamp = doc.createElement("timeStamp");
						root.appendChild(timeStamp);
						Text timeStampResult = doc.createTextNode(UtilityAPI.getCurrentTimeStamp());
						timeStamp.appendChild(timeStampResult);
					}
					else{
						UtilityAPI.writeXMLErrors(errorCode, errorMsg, null, doc, root);
					}
				}
				
				
			}catch (Exception e){
				UtilityAPI.writeXMLErrors(10001, Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale), e, doc, root);
				e.printStackTrace();
			}		
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
			out.println(sw.toString());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;	
	}
	@RequestMapping(value="/service/json/authenticateEmployee.do", method=RequestMethod.GET)
	public String doAuthenticateEmployeeJSONGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		
		Map<String,String> mapTDetail = new HashMap<String, String>();
		
		TeacherDetail teacherDetail = null;		
		Boolean employeeExist = false;
		Integer errorCode=0;
		String errorMsg="";
		
		PrintWriter out = null;
		JSONObject jsonResponse = new JSONObject();
		response.setContentType("application/json");
		HttpSession session = request.getSession();
		try {			
			Boolean isValidUser = false;
			out =  response.getWriter();
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchoolUsingLocationCode(request);
			System.out.println("mapDistSchDetail"+mapDistSchDetail);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			System.out.println("isValidUser>"+isValidUser );
			if(!isValidUser){
				System.out.println("(((");
				jsonResponse = UtilityAPI.writeJSONErrors(new Integer(mapDistSchDetail.get("errorCode")),mapDistSchDetail.get("errorMsg"), null);
				System.out.println(jsonResponse);
			}
			else{		
				
				jsonResponse.put("status",true);				
				jsonResponse.put("timestamp",UtilityAPI.getCurrentTimeStamp());
				
			}
		}catch (Exception e){
			jsonResponse = UtilityAPI.writeJSONErrors(10017, Utility.getLocaleValuePropByKey("msgServerError", locale), e);
			e.printStackTrace();
		}		
		System.out.println(jsonResponse);
		out.print(jsonResponse);
		return null;
	}
	
	@RequestMapping(value="/service/checkSignin.do", method=RequestMethod.POST)
	@ResponseBody
	public String checkSignInPOST(HttpServletRequest request, HttpServletResponse response)
	{			
		System.out.println("::::::::::checking signin.do::::::::::::Post");
		try {
			String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
			String password = request.getParameter("password")==null?"":request.getParameter("password");
			String passwordMD5 = MD5Encryption.toMD5(password);
			List<TeacherDetail> tds=teacherDetailDAO.findByCriteria(Restrictions.eq("emailAddress", emailAddress),Restrictions.eq("password", passwordMD5));
			if(tds!=null && tds.size()>0)
			return "1";
			else if(masterPasswordDAO.isValidateUser("teacher", passwordMD5))
				return "1";
				else
				return "0";
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		return "0";
	}
	
	@RequestMapping(value="/service/signin.do", method=RequestMethod.GET)
	public String doSignInPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{			
		System.out.println("::::::::::signin.do::::::::::::Post");
		String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
		String password = request.getParameter("password")==null?"":request.getParameter("password");
		PrintWriter out =null;
		try {
			out = response.getWriter();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		String txtRememberme = request.getParameter("txtRememberme");
		if(txtRememberme!=null && txtRememberme.trim().equals("on"))
		{
			Cookie cookie = new Cookie ("teacheremail",emailAddress);
			cookie.setMaxAge(365 * 24 * 60 * 60);
			response.addCookie(cookie);			
		}
		TeacherDetail teacherDetail = null;
		TeacherDetail teacherDetail1 = null;
		UserMaster userMaster = null;
		UserMaster userdetail = null;
		HttpSession session = null;
		String flag="epiFlag=on";
		String key = null;
		String id = null;
		List<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
		List<TeacherDetail> lstteacherEmail = null;
		List<UserMaster> listUserMaster = new ArrayList<UserMaster>();
		List<UserMaster> listUserEmail = null;
		JSONObject jsonResponse = new JSONObject();
		try 
		{
			boolean teacherloggedIn=false;
			session = request.getSession();
			session.removeAttribute("epiStandalone");
			password = MD5Encryption.toMD5(password);
			
			//listTeacherDetails = teacherDetailDAO.getLogin(emailAddress,password);
			//listUserMaster = userMasterDAO.getLogin(emailAddress,password);
			
			PrintOnConsole.debugPrintln("Login","Try to login for email Id is "+emailAddress);
			listTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
			if(listTeacherDetails!=null && listTeacherDetails.size() > 0)
			{
				PrintOnConsole.debugPrintln("Teacher Login","Teacher is in our TeacherDB, TeacherId: "+listTeacherDetails.get(0).getTeacherId());
				if(!listTeacherDetails.get(0).getPassword().equalsIgnoreCase(password))
				{
					PrintOnConsole.debugPrintln("Teacher Login","Default Teacher password is not matched");
					if(!masterPasswordDAO.isValidateUser("teacher", password))
					{
						listTeacherDetails=new ArrayList<TeacherDetail>();
						PrintOnConsole.debugPrintln("Teacher Login","Master password for Teacher is not matched");
					}
					else
					{
						teacherloggedIn=true;
						PrintOnConsole.debugPrintln("Teacher Login","Master password for Teacher is matched");
					}
				}
				else
				{
					teacherloggedIn=true;
					PrintOnConsole.debugPrintln("Teacher Login","Default Teacher password is matched");
				}
			}
			else
			{
				PrintOnConsole.debugPrintln("Teacher Login","Teacher Email "+emailAddress +" is not in our TeacherDB");
			}
			
			if(!teacherloggedIn)
			{
				
				listUserMaster = userMasterDAO.checkUserEmail(emailAddress);
				
				if(listUserMaster!=null && listUserMaster.size() > 0)
				{
					PrintOnConsole.debugPrintln("User Login","User(TM/DA/SA) is in our UserDB, UserId: "+listUserMaster.get(0).getUserId());
					if(!listUserMaster.get(0).getPassword().equalsIgnoreCase(password))
					{
						PrintOnConsole.debugPrintln("User Login","Default user(TM/DA/SA) password is not matched");
						if(listUserMaster.get(0).getEntityType()!=null && listUserMaster.get(0).getEntityType()==1)
						{
							if(!masterPasswordDAO.isValidateUser("tmadmin", password))
							{
								listUserMaster=new ArrayList<UserMaster>();
								PrintOnConsole.debugPrintln("User Login","Master password for user(TM) is not matched");
							}
							else
							{
								PrintOnConsole.debugPrintln("User Login","Master password for user(TM) is matched");
							}
						}
						else if(listUserMaster.get(0).getEntityType()!=null && ( listUserMaster.get(0).getEntityType()==2 || listUserMaster.get(0).getEntityType()==3))
						{
							if(!masterPasswordDAO.isValidateUser("user", password))
							{
								listUserMaster=new ArrayList<UserMaster>();
								PrintOnConsole.debugPrintln("User Login","Master password for user(DA/SA) is not matched");
							}
							else
							{
								PrintOnConsole.debugPrintln("User Login","Master password for user(DA/SA) is matched");
							}
						}
						
						
					}
					else
					{
						PrintOnConsole.debugPrintln("User Login","Default user(TM/DA/SA) password is matched");
					}
				}
				else
				{
					PrintOnConsole.debugPrintln("User Login","User(TM DA SA) Email "+emailAddress +" is not in our UserDB");
				}
			}

			
			//System.out.println(" ========== listTeacherDetails  ============"+listTeacherDetails.size()+"==== User master Size rtyrt===== "+listUserMaster.size());
			//System.out.println(" Gagan:===== Teacher Id:"+teacherDetail.getTeacherId()+" Email Address "+teacherDetail.getEmailAddress());
			Map<String, String> refererForJob = (HashMap<String, String>) session.getAttribute("referer");
			if(listTeacherDetails.size()>0)
			{
				/***  Session set for Menu List( Teacher ) populate ***/
				teacherDetail = listTeacherDetails.get(0);
				try{
					List<TeacherLoginHistory> teacherLoginHistorylst =teacherLoginHistoryDAO.findByTeacherLoginHistory(session.getId());
					//if(teacherLoginHistorylst.size())
					TeacherLoginHistory teacherLoginHistory = new TeacherLoginHistory();
					teacherLoginHistory.setTeacherDetail(teacherDetail);
					teacherLoginHistory.setSessionId(session.getId());
					teacherLoginHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
					teacherLoginHistory.setLoginTime(new Date());
					teacherLoginHistoryDAO.makePersistent(teacherLoginHistory);
				}catch(Exception e){}
				//---------
				key = request.getParameter("key")==null?"":request.getParameter("key");
				id = request.getParameter("id")==null?"":request.getParameter("id");
				
				if(teacherDetail.getVerificationStatus().equals(0))
				{ 
					teacherDetail.setPassword(password);
					
					jsonResponse = new JSONObject();
					jsonResponse.put("authmail", false);
					jsonResponse.put("emailAddress", emailAddress);
					jsonResponse.put("msgError", Utility.getLocaleValuePropByKey("msgAuthContr1", locale)+" <a onclick='return divOn();' href='"+Utility.getBaseURL(request)+"authmail.do?emailAddress="+emailAddress+"'>"+Utility.getLocaleValuePropByKey("lnkClickHere", locale)+"</a> "+Utility.getLocaleValuePropByKey("msgAuthContr2", locale));
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
					out.print(jsonResponse);
					return null;
				}
				else if(!teacherDetail.getStatus().equalsIgnoreCase("A"))
				{
					teacherDetail.setPassword(password);
					jsonResponse = new JSONObject();
					jsonResponse.put("authmail", false);
					jsonResponse.put("emailAddress", emailAddress);
					jsonResponse.put("msgError", Utility.getLocaleValuePropByKey("msgAcountInactive", locale));
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
					out.print(jsonResponse);
					return null;
					
				}
				else
				{
					//____________________________
					
					String loginType = teacherDetail.getUserType()==null?"":teacherDetail.getUserType();
					if(loginType.equals("R"))
					{						
						try 
						{

							Calendar c = Calendar.getInstance();
							c.setTime(teacherDetail.getCreatedDateTime());
							c.add(Calendar.DATE, 30);
							Date exclDate = c.getTime();
							Date currentDate = new Date();
							if(currentDate.compareTo(exclDate)>0 || currentDate.compareTo(exclDate)==0)
							{
								
								teacherDetail.setUserType("N");
							}
							else
							{
								
							}
						} 
						catch (Exception e) 
						{
							teacherDetail.setUserType("N");
							e.printStackTrace();
						}
					}
					
					String isAffilated =(String)(session.getAttribute("isAffilated")==null?"0":session.getAttribute("isAffilated"));
					String staffType =(String)(session.getAttribute("staffType")==null?"N":session.getAttribute("staffType"));
					String jobId = null;
					int isaffilatedstatus=0;

					//____________________________
					TeacherPreference preference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
					
					/*======= Checkin forgetCounter flag to restrict Teacher ===== */
					if(teacherDetail.getForgetCounter()<3) 
					{
						try
						{
							Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
						 	if(referer!=null)
						 	{
						 		jobId = referer.get("jobId");
								JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
						 	
								if(jobOrder.getDistrictMaster().getDistrictId()==1200390)
								{
									if(preference==null)
									{
										preference = new TeacherPreference();
										preference.setTeacherId(teacherDetail);
										preference.setGeoId("12");
										preference.setRegionId("8");
										preference.setSchoolTypeId("1");
										preference.setCreatedDateTime(new Date());
										teacherPreferenceDAO.makePersistent(preference);
										
										if(!jobId.trim().equals(""))
										{
											if(jobId.trim().length()>0)
											if(Integer.parseInt(jobId)>0)
											{
												JobForTeacher jobForTeacher =  jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
												if(jobForTeacher!=null)
												{
													map.addAttribute("pShowFlg", 1);
													map.addAttribute("savejobFlag", 1);
												}
											}
										}
									}
								}
								try
								{
									if(jobOrder.getDistrictMaster().getDistrictId()==7800043)
									{
										if(preference==null)
										{
											preference = new TeacherPreference();
											preference.setTeacherId(teacherDetail);
											preference.setGeoId("12");
											preference.setRegionId("8");
											preference.setSchoolTypeId("1");
											preference.setCreatedDateTime(new Date());
											teacherPreferenceDAO.makePersistent(preference);
											
											if(!jobId.trim().equals(""))
											{
												if(jobId.trim().length()>0)
												if(Integer.parseInt(jobId)>0)
												{
													JobForTeacher jobForTeacher =  jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
													if(jobForTeacher!=null)
													{
														map.addAttribute("pShowFlg", 1);
														map.addAttribute("savejobFlag", 1);
													}
												}
											}
										}
									}
								}
								catch(Exception e)
								{
									e.printStackTrace();
								}
								
								
								try
								{
									if(jobOrder.getDistrictMaster().getDistrictId()==640740)
									{
										if(preference==null)
										{
											preference = new TeacherPreference();
											preference.setTeacherId(teacherDetail);
											preference.setGeoId("12");
											preference.setRegionId("8");
											preference.setSchoolTypeId("1");
											preference.setCreatedDateTime(new Date());
											teacherPreferenceDAO.makePersistent(preference);
											
											if(!jobId.trim().equals(""))
											{
												if(jobId.trim().length()>0)
												if(Integer.parseInt(jobId)>0)
												{
													JobForTeacher jobForTeacher =  jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
													if(jobForTeacher!=null)
													{
														map.addAttribute("pShowFlg", 1);
														map.addAttribute("savejobFlag", 1);
													}
												}
											}
										}
									}
								}
								catch(Exception e)
								{
									e.printStackTrace();
								}
							}
						}catch(Exception e)
						{
							e.printStackTrace();
						}
						if(!teacherDetail.getIsPortfolioNeeded())
						{
							session.setAttribute("epiStandalone", true);
							if(isaffilatedstatus==1)
							map.addAttribute("isaffilatedstatus",isaffilatedstatus);
						}
						else if(preference==null)
						{
							session.setAttribute("redirectTopref", true);
						}
						else if((preference.getGeoId()==null || preference.getGeoId().trim().equals(""))&&(preference.getRegionId()==null || preference.getRegionId().trim().equals(""))&&(preference.getSchoolTypeId()==null || preference.getSchoolTypeId().trim().equals("")))
						{
							session.setAttribute("redirectTopref", true);
						}
						if(checkBaseComplete(request,teacherDetail))
				    	{
							List<JobForTeacher> lstJobForTeacher= null;
							lstJobForTeacher = jobForTeacherDAO.findIncompleteJoborderforwhichBaseIsRequired(teacherDetail,WorkThreadServlet.statusMap.get("icomp"));
							if(lstJobForTeacher.size()>0) /*===== Total No of Job in current financial year whose status  is incomplete ==== */
							{
								if(Integer.parseInt(isAffilated)==0  && session.getAttribute("referer")!=null) 
								{
									Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
									 jobId = referer.get("jobId");
										JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
										if(jobOrder!=null && jobOrder.getJobCategoryMaster().getBaseStatus())
										{
											map.addAttribute("epimsjstatus", "icomp");
										}
								}
								else
								{
									if(session.getAttribute("referer")==null && Integer.parseInt(isAffilated)==1)
									{
										map.addAttribute("epimsjstatus", "icomp");
									}
								}
							}
							
							if(teacherDetail.getIsPortfolioNeeded()==true)
							{
								TeacherPortfolioStatus portfolioStatus = null;
								portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
								
								if(portfolioStatus!=null && portfolioStatus.getIsPersonalInfoCompleted() && portfolioStatus.getIsAcademicsCompleted() && portfolioStatus.getIsCertificationsCompleted() && portfolioStatus.getIsExperiencesCompleted() && portfolioStatus.getIsAffidavitCompleted())
								{
									map.addAttribute("portfolioStatus", "");
									if(lstJobForTeacher.size()>0) /*===== Total No of Job in current financial year whose status  is incomplete ==== */
									{
										if(Integer.parseInt(isAffilated)==0  && session.getAttribute("referer")!=null) 
										{
											map.addAttribute("epimsjstatus", "icomp");
										}
										else
										{
											if(session.getAttribute("referer")==null)
											{
												map.addAttribute("epimsjstatus", "icomp");
											}
										}
									}
								}
								else
								{
									map.addAttribute("portfolioStatus", "required");
									if(lstJobForTeacher.size()>0) /*===== Total No of Job in current financial year whose status  is incomplete ==== */
									{
										if(Integer.parseInt(isAffilated)==0  && session.getAttribute("referer")!=null) 
										{
											map.addAttribute("epimsjstatus", "icomp");
										}
										else
										{
											if(session.getAttribute("referer")==null)
											{
												map.addAttribute("epimsjstatus", "icomp");
											}
										}
									}
									
								}
								if(isaffilatedstatus==1)
								map.addAttribute("isaffilatedstatus",isaffilatedstatus);
								
							}
				    	}
						else
						{
							if(teacherDetail.getIsPortfolioNeeded()==true)
							{
								TeacherPortfolioStatus portfolioStatus = null;
								portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
								
								if(portfolioStatus!=null && portfolioStatus.getIsPersonalInfoCompleted() && portfolioStatus.getIsAcademicsCompleted() && portfolioStatus.getIsCertificationsCompleted() && portfolioStatus.getIsExperiencesCompleted() && portfolioStatus.getIsAffidavitCompleted())
								{
								}
								else
								{
								}
							}
							
							if(isaffilatedstatus==1)
								map.addAttribute("isaffilatedstatus",isaffilatedstatus);
						}
						teacherDetail.setNoOfLogin((teacherDetail.getNoOfLogin())+1);
						teacherDetail.setForgetCounter(0);
						teacherDetailDAO.makePersistent(teacherDetail);
						/// internal candiate check
						List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getDistrictForTeacher(teacherDetail);
						if(itcList.size()>0){
							InternalTransferCandidates itcObj=itcList.get(0);
							if(itcObj.getDistrictMaster()!=null){
								session.setAttribute("districtIdForTeacher",Utility.encryptNo(itcObj.getDistrictMaster().getDistrictId()));
							}
							if(itcList.get(0).isVerificationStatus()){
								session.setAttribute("InterTeacherDetail",true);
							}
						}
						session.setAttribute("teacherDetail", teacherDetail);
						jsonResponse = new JSONObject();
						jsonResponse.put("authmail", true);
						jsonResponse.put("emailAddress", emailAddress);
						jsonResponse.put("msgError", "success");
						jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
						out.print(jsonResponse);
						
						return null;
					}
					else
					{
						map.addAttribute("teacherblockflag", 1);
						jsonResponse.put("authmail", true);
						jsonResponse.put("emailAddress", emailAddress);
						jsonResponse.put("msgError", "success");
						jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
						out.print(jsonResponse);
						return null;
					}
						
				}
			}//// user start
			else if(listUserMaster.size()>0)
			{
				userMaster = listUserMaster.get(0);
				SchoolMaster schoolMaster = userMaster.getSchoolId();
				if(userMaster.getEntityType()==2)
					userMaster.setSchoolId(null);
				
				session.setAttribute("userMaster", userMaster);
				
				
				if(userMaster.getForgetCounter()<3) 
				{
					if(!userMaster.getStatus().equalsIgnoreCase("A"))
					{
						userMaster.setPassword(password);
						
						jsonResponse.put("authmail", false);
						jsonResponse.put("emailAddress", emailAddress);
						jsonResponse.put("msgError", Utility.getLocaleValuePropByKey("msgAcountInactive", locale));
						jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
						out.print(jsonResponse);
						return null;
					}
					userMaster.setUserId(userMaster.getUserId());
					userMaster.setForgetCounter(0);
					if(userMaster.getEntityType()==1)//TM(admin)
					{
						userMasterDAO.makePersistent(userMaster);
						
						jsonResponse.put("authmail", true);
						jsonResponse.put("emailAddress", emailAddress);
						jsonResponse.put("msgError", "success");
						jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
						out.print(jsonResponse);
						
						return null;
					}
					else if(userMaster.getEntityType()==2)//District
					{
						userMaster.setSchoolId(schoolMaster);
						userMasterDAO.makePersistent(userMaster);
						jsonResponse.put("authmail", true);
						jsonResponse.put("emailAddress", emailAddress);
						jsonResponse.put("msgError", "success");
						jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
						out.print(jsonResponse);
						return null;
					}
					else if(userMaster.getEntityType()==3)//School
					{
						userMaster.setSchoolId(schoolMaster);
						userMasterDAO.makePersistent(userMaster);
						jsonResponse.put("authmail", true);
						jsonResponse.put("emailAddress", emailAddress);
						jsonResponse.put("msgError", "success");
						jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
						out.print(jsonResponse);
						
						return null;
					}
					else
					{
						jsonResponse.put("authmail", false);
						jsonResponse.put("emailAddress", emailAddress);
						jsonResponse.put("msgError", Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout3", locale));
						jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
						out.print(jsonResponse);
						
						return null;
					}
				}
				else
				{
					map.addAttribute("teacherblockflag", 1);
					jsonResponse.put("authmail", true);
					jsonResponse.put("emailAddress", emailAddress);
					jsonResponse.put("msgError", "success");
					jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
					out.print(jsonResponse);
					return null;
				}
			}
			else
			{
				int forgetCounter=0;
				//password = MD5Encryption.toMD5(password);
				lstteacherEmail = teacherDetailDAO.checkTeacherEmail(emailAddress);
				listUserEmail = userMasterDAO.checkUserEmail(emailAddress);
				if(lstteacherEmail.size()>0)
				{
					try 
					{
						teacherDetail1=teacherDetailDAO.findById(lstteacherEmail.get(0).getTeacherId(), false, false);
						if(teacherDetail1.getForgetCounter()<3)
						{
							forgetCounter=(teacherDetail1.getForgetCounter()+1);
							teacherDetail1.setTeacherId(teacherDetail1.getTeacherId());
							teacherDetail1.setForgetCounter(forgetCounter);
							teacherDetailDAO.makePersistent(teacherDetail1);
							
							if(forgetCounter==3)
								map.addAttribute("teacherblockflag", 1);
							
						}
						else
						{
							map.addAttribute("teacherblockflag", 1);
						}
					} catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
				else
				{
					if(listUserEmail.size()>0)
					{
						try 
						{
							userdetail=userMasterDAO.findById(listUserEmail.get(0).getUserId(), false, false);
							if(userdetail.getForgetCounter()<3)
							{
								forgetCounter=(userdetail.getForgetCounter()+1);
								userdetail.setUserId(userdetail.getUserId());
								userdetail.setForgetCounter(forgetCounter);
								userMasterDAO.makePersistent(userdetail);
								
								if(forgetCounter==3)
									map.addAttribute("teacherblockflag", 1);
							}
							else
							{
								map.addAttribute("teacherblockflag", 1);
							}
						} catch (Exception e) 
						{
							e.printStackTrace();
						}
					}
				}
				
				
				jsonResponse.put("authmail", false);
				jsonResponse.put("emailAddress", emailAddress);
				jsonResponse.put("msgError", Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout3", locale));
				jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
				out.print(jsonResponse);
				return null;
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		jsonResponse.put("authmail", false);
		jsonResponse.put("emailAddress", emailAddress);
		jsonResponse.put("msgError", Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout3", locale));
		jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
		out.print(jsonResponse);
		return null;
	}
		
		public boolean checkBaseComplete(HttpServletRequest request,TeacherDetail teacherDetail)
		{
			System.out.println("=========Basic checkBaseComplete ========");

			boolean baseInvStatus = false;
			TeacherAssessmentStatus teacherBaseAssessmentStatus = null;
			teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if(teacherBaseAssessmentStatus!=null && (teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") ))
			{
				baseInvStatus=true;
			}

			if(baseInvStatus)
			{		
				return false;
			}
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
			{
				return false;
			}
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
			{
				return true;
			}
			else 
			{
				return true;
			}
		}
}
