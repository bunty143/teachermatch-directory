package tm.api.controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import tm.api.UtilityAPI;
import tm.api.services.ServiceUtility;
import tm.bean.CandidateRawScore;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAnswerDetail;
import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherSecondaryStatus;
import tm.bean.assessment.QuestionsPool;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.CandidateRawScoreDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAnswerDetailDAO;
import tm.dao.TeacherAssessmentQuestionDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherSecondaryStatusDAO;
import tm.dao.assessment.QuestionsPoolDAO;
import tm.dao.cgreport.PercentileCalculationDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.dao.cgreport.TmpPercentileWiseZScoreDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.CommonDashboardAjax;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

@Controller
public class CGReportController {
	
    String locale = Utility.getValueOfPropByKey("locale");

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherAssessmentQuestions(List<TeacherAssessmentQuestion> teacherAssessmentQuestions) {
		this.teacherAssessmentQuestions = teacherAssessmentQuestions;
	}

	@Autowired
	private QuestionsPoolDAO questionsPoolDAO;
	public void setQuestionsPoolDAO(QuestionsPoolDAO questionsPoolDAO) {
		this.questionsPoolDAO = questionsPoolDAO;
	}

	@Autowired
	private CandidateRawScoreDAO candidateRawScoreDAO;
	public void setCandidateRawScoreDAO(CandidateRawScoreDAO candidateRawScoreDAO) {
		this.candidateRawScoreDAO = candidateRawScoreDAO;
	}

	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}


	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}

	@Autowired
	private ServiceUtility serviceUtility;
	public void setServiceUtility(ServiceUtility serviceUtility) {
		this.serviceUtility = serviceUtility;
	}

	@Autowired
	private DomainMasterDAO domainMasterDAO;
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) 
	{
		this.domainMasterDAO = domainMasterDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) 
	{
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO;
	public void setTeacherAssessmentQuestionDAO(TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO) 
	{
		this.teacherAssessmentQuestionDAO = teacherAssessmentQuestionDAO;
	}

	@Autowired
	private TeacherAnswerDetailDAO teacherAnswerDetailDAO;
	public void setTeacherAnswerDetailDAO(TeacherAnswerDetailDAO teacherAnswerDetailDAO) 
	{
		this.teacherAnswerDetailDAO = teacherAnswerDetailDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}
	@Autowired
	private TeacherSecondaryStatusDAO teacherSecondaryStatusDAO;
	public void setTeacherSecondaryStatusDAO(TeacherSecondaryStatusDAO teacherSecondaryStatusDAO) {
		this.teacherSecondaryStatusDAO = teacherSecondaryStatusDAO;
	}

	@Autowired
	private CommonDashboardAjax commonDashboardAjax;

	@Autowired
	private RawDataForDomainDAO rawDataForDomainDAO;

	@Autowired
	private PercentileCalculationDAO percentileCalculationDAO;

	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;

	@Autowired
	private TmpPercentileWiseZScoreDAO tmpPercentileWiseZScoreDAO;
	
	Map<Integer,Map<Integer, Object>> mapDomainScore = null;
	double[] compositeScore = null;
	List<JobForTeacher> lstJobForTeacher = null;
	List<JobForTeacher> lstJobForTeacherVlt = null;


	List<QuestionsPool> lstQuestionsPools = null;
	List<TeacherAssessmentQuestion> teacherAssessmentQuestions = null;
	String quid = "";

	@RequestMapping(value="/service/getCGView.do", method=RequestMethod.GET)
	public String doAddJobOrderByXMLGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("/service/cgReport.do");
		boolean cgStatus = false;
		Integer errorCode=0;
		String errorMsg="";
		try {			
			HttpSession session = request.getSession();
			String apiJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId");
			JobOrder jobOrder = null;

			boolean isValidUser = false;
			UserMaster userMaster = null;

			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			Map<String,String> mapUserInfo = new HashMap<String, String>();


			if(isValidUser){
				mapUserInfo = serviceUtility.authUser(request);
				try {
					DistrictMaster districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
					jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);
				} catch (Exception e) {
					jobOrder=null;
				}

				if(! new Boolean(mapUserInfo.get("isSuccess"))){
					session.setAttribute("userMasterAPI", null);
					cgStatus = false;
					errorCode= new Integer(mapUserInfo.get("errorCode"));
					errorMsg=mapUserInfo.get("errorMsg");
				}
				else if(apiJobId.equals("")){
					cgStatus = false;
					errorCode = 10004;
					errorMsg = Utility.getLocaleValuePropByKey("msgJobReq", locale);
				}
				else if(jobOrder==null){
					System.out.println("E3");
					cgStatus = false;
					errorCode = 10004;
					errorMsg = Utility.getLocaleValuePropByKey("msgJobNotExist", locale);
				}
				else{				
					userMaster = (UserMaster)session.getAttribute("userMasterAPI");					
					cgStatus = true;
				}

			}
			else{
				errorCode = new Integer(mapDistSchDetail.get("errorCode"));
				errorMsg = mapDistSchDetail.get("errorMsg");
				cgStatus = false;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			cgStatus = false;
		}
		finally{			
		}
		map.addAttribute("cgStatus", cgStatus);
		map.addAttribute("errorCode", errorCode);
		map.addAttribute("errorMsg", errorMsg);
		return "cgreport";
	}

	@RequestMapping(value="/service/json/getCompositeScoresByJob.do", method=RequestMethod.GET)
	public String getCgStatusGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		String apiJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();

		UserMaster userMaster = null;		

		JobOrder jobOrder = null;		
		Integer errorCode=0;
		String errorMsg="";

		PrintWriter out = null;
		JSONObject jsonResponse = new JSONObject();
		JSONArray jsonCompScoreArray = new JSONArray();
		response.setContentType("application/json");

		Map<String,String> mapUserInfo = new HashMap<String, String>();


		HttpSession session = request.getSession();
		try {	

			out =  response.getWriter();
			Boolean isValidUser = false;						
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			Boolean isError = false;

			if(!isValidUser){
				jsonResponse = UtilityAPI.writeJSONErrors(new Integer(mapDistSchDetail.get("errorCode")),mapDistSchDetail.get("errorMsg"), null);
			}
			else{	
				mapUserInfo = serviceUtility.authUser(request);
				try {
					DistrictMaster districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
					jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);
				} catch (Exception e) {
					jobOrder=null;
				}


				if(! new Boolean(mapUserInfo.get("isSuccess"))){
					System.out.println("E1");
					isError = true;
					session.setAttribute("userMasterAPI", null);	
					System.out.println(""+mapUserInfo.get("errorCode"));
					errorCode= new Integer(mapUserInfo.get("errorCode"));
					errorMsg=mapUserInfo.get("errorMsg");
				}
				else if(apiJobId.equals("")){
					System.out.println("E2");
					isError = true;
					errorCode = 10004;
					errorMsg = Utility.getLocaleValuePropByKey("msgJobReq", locale);
				}
				else if(jobOrder==null){
					System.out.println("E3");
					isError = true;
					errorCode = 10004;
					errorMsg = Utility.getLocaleValuePropByKey("msgJobNotExist", locale);
				}
				else if(!jobOrder.getJobCategoryMaster().getBaseStatus()){
					System.out.println("E4");
					isError = true;
					errorCode = 10006;
					errorMsg = Utility.getLocaleValuePropByKey("msgNoDetailsForCand", locale);
				}
				else{										
					try 
					{
						userMaster = (UserMaster) session.getAttribute("userMasterAPI");
						lstJobForTeacher = jobForTeacherDAO.findJFTApplicantbyJobOrder(jobOrder);


						List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
						for (JobForTeacher jbforteacher : lstJobForTeacher) 
							teacherDetails.add(jbforteacher.getTeacherId());
						
						JSONObject scoreJSON = null;
						String tFname = null;
						String tLname = null;
						List<TeacherNormScore> teacherNormScoreList= new ArrayList<TeacherNormScore>();
						teacherNormScoreList = teacherNormScoreDAO.findNormScoreByTeacherList(teacherDetails);
						
						for(TeacherNormScore teacherNormScore:teacherNormScoreList)
						{
							scoreJSON = new JSONObject();
							
							tFname = teacherNormScore.getTeacherDetail().getFirstName()==null?"":teacherNormScore.getTeacherDetail().getFirstName();
							tLname = teacherNormScore.getTeacherDetail().getLastName()==null?"":teacherNormScore.getTeacherDetail().getLastName();

							scoreJSON.put("teacherName",tFname+" "+tLname );
							scoreJSON.put("teacherEmail", teacherNormScore.getTeacherDetail().getEmailAddress());
							scoreJSON.put("compositeScore", teacherNormScore.getTeacherNormScore());
							scoreJSON.put("decileColor", teacherNormScore.getDecileColor());
							jsonCompScoreArray.add(scoreJSON);
							System.out.println(teacherNormScore.getTeacherDetail().getEmailAddress()+" <> "+teacherNormScore.getTeacherNormScore());
						}

					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}		
				}

				if(isError){
					System.out.println("IFsuccess");
					jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg, null);
				}
				else{
					System.out.println("Else Filure");
					jsonResponse = new JSONObject();
					jsonResponse.put("status",true);
					jsonResponse.put("timestamp",UtilityAPI.getCurrentTimeStamp());
					jsonResponse.put("score", jsonCompScoreArray);

				}
			}
		}catch (Exception e){
			jsonResponse = UtilityAPI.writeJSONErrors(10017, Utility.getLocaleValuePropByKey("msgServerError", locale), e);
			e.printStackTrace();
		}		
		out.print(jsonResponse);
		return null;	
	}


	@RequestMapping(value="/service/xml/getCompositeScoresForJob.do", method=RequestMethod.GET)
	public String getCgStatusXMLGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		try {			
			String apiJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
			UserMaster userMaster = null;

			JobOrder jobOrder = null;		
			Integer errorCode=0;
			String errorMsg="";

			PrintWriter out = null;

			response.setContentType("text/xml");

			HttpSession session = request.getSession();			

			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = null;
			Document doc = null;
			Element root = null;


			docBuilder = builderFactory.newDocumentBuilder();
			doc = docBuilder.newDocument();
			root = doc.createElement("teachermatch");
			doc.appendChild(root);	

			try {	
				out =  response.getWriter();
				Boolean isValidUser = false;						
				Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
				isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
				Boolean isError = false;

				if(!isValidUser){					
					UtilityAPI.writeXMLErrors(new Integer(mapDistSchDetail.get("errorCode")),mapDistSchDetail.get("errorMsg"),null, doc, root);
				}
				else{				
					try{
						DistrictMaster districtMaster = (DistrictMaster)session.getAttribute("apiDistrictMaster");
						jobOrder = jobOrderDAO.findJobByApiJobId(apiJobId,districtMaster);
					} catch (Exception e){
						jobOrder=null;
					}
					System.out.println("jobOrder:> "+jobOrder);
					userMaster = (UserMaster) session.getAttribute("userMasterAPI");
					Map<String,String> mapUserInfo = new HashMap<String, String>();
					mapUserInfo = serviceUtility.authUser(request);

					if(! new Boolean(mapUserInfo.get("isSuccess"))){
						System.out.println("E1");
						isError = true;
						session.setAttribute("userMasterAPI", null);					
						errorCode= new Integer(mapUserInfo.get("errorCode"));
						errorMsg=mapUserInfo.get("errorMsg");
					}
					else if(apiJobId.equals("")){
						System.out.println("E2");
						isError = true;
						errorCode = 10004;
						errorMsg = Utility.getLocaleValuePropByKey("msgJobReq", locale);
					}
					else if(jobOrder==null){
						System.out.println("E3");
						isError = true;
						errorCode = 10004;
						errorMsg = Utility.getLocaleValuePropByKey("msgJobNotExist", locale);
					}
					else if(!jobOrder.getJobCategoryMaster().getBaseStatus()){
						System.out.println("E4");
						isError = true;
						errorCode = 10006;
						errorMsg = Utility.getLocaleValuePropByKey("msgNoDetailsForCand", locale);
					}
					else{										
						double domainTotatlScore = 0;
						double score = 0;
						lstJobForTeacherVlt = new ArrayList<JobForTeacher>();
						try 
						{	
							List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;

							TeacherAssessmentStatus teAssesStatusTempBase = null;
							TeacherAssessmentStatus teAssesStatusTempJSI = null;

							StatusMaster statusComp = WorkThreadServlet.statusMap.get("comp");
							StatusMaster statusIcomp = WorkThreadServlet.statusMap.get("icomp");
							StatusMaster statusHired = WorkThreadServlet.statusMap.get("hird");
							StatusMaster statusRemove = WorkThreadServlet.statusMap.get("rem");
							StatusMaster statusVlt = WorkThreadServlet.statusMap.get("vlt");

							List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
							lstStatusMasters.add(statusComp);
							lstStatusMasters.add(statusIcomp);
							lstStatusMasters.add(statusHired);
							lstStatusMasters.add(statusRemove);			

							lstJobForTeacher = jobForTeacherDAO.findJFTbyJobOrder(jobOrder,lstStatusMasters);
							List<JobForTeacher> lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacher);

							//--------------------------------------------------set global map-----

							Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();		
							Map<Integer, Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
							List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
							List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
							for (JobForTeacher jbforteacher : lstJobForTeacher) 
								teacherDetails.add(jbforteacher.getTeacherId());
							if(teacherDetails!=null && teacherDetails.size()>0){
								teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetails);
								for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus){					
									baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
									if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
										isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
									}
								}
							}
							mapDomainScore = new HashMap<Integer, Map<Integer,Object>>();
							Map<Integer, Object> mapPercentScore = null;
							List lstAns = null;
							TeacherDetail tDetail = null;
							for(DomainMaster dm: lstDomain){
								mapPercentScore = new HashMap<Integer, Object>();
								lstAns = teacherAnswerDetailDAO.findSumOfAnsByDomainAndTeacher(null, dm);
								for(Object oo: lstAns){
									Object obj[] = (Object[])oo;
									tDetail = (TeacherDetail) obj[0];
									mapPercentScore.put( tDetail.getTeacherId(), obj);
								}
								mapDomainScore.put(dm.getDomainId(), mapPercentScore);
							}

							List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = teacherSecondaryStatusDAO.findByJobAndSchool(jobOrder, userMaster.getSchoolId());
							Map<Integer,TeacherSecondaryStatus> mapTSecStatus = new HashMap<Integer, TeacherSecondaryStatus>();
							for(TeacherSecondaryStatus tSecondaryStatus: lstTeacherSecondaryStatus ){
								mapTSecStatus.put(tSecondaryStatus.getTeacherDetail().getTeacherId(), tSecondaryStatus);
							}	

							Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
							List<TeacherAssessmentStatus> lstJSI = teacherAssessmentStatusDAO.findAssessmentTaken(null, jobOrder);
							for(TeacherAssessmentStatus ts : lstJSI){

								mapJSI.put(ts.getTeacherDetail().getTeacherId(), ts);
							}
							//--------------------------------------------------set global map----

							for(JobForTeacher jft : lstJFTremove)
							{
								teAssesStatusTempBase = baseTakenMap.get(jft.getTeacherId().getTeacherId());							
								teAssesStatusTempJSI =mapJSI.get(jft.getTeacherId().getTeacherId()); 
								if((teAssesStatusTempBase==null ) || ( teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")))
								{
									lstJobForTeacher.remove(jft);
								}
								if((teAssesStatusTempBase!=null ) && (teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")))
								{	
									lstJobForTeacherVlt.add(jft);
									lstJobForTeacher.remove(jft);
								}
								else if(teAssesStatusTempJSI!=null && teAssesStatusTempJSI.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
								{
									lstJobForTeacherVlt.add(jft);
									lstJobForTeacher.remove(jft);					
								}
							}											

							int k=0;
							compositeScore = new double[lstJobForTeacher.size()];
							for(JobForTeacher jft:lstJobForTeacher)
							{		
								domainTotatlScore=0.0;
								for(DomainMaster domain: lstDomain){
									score =  getPercentScoreOfBasesAssement(domain,jft.getTeacherId()); 
									domainTotatlScore = domainTotatlScore+score*domain.getMultiplier();
								}
								jft.setCompositeScore(domainTotatlScore);
								compositeScore[k]=domainTotatlScore;
								k++;
							}



							//-- sorting CG view start
							List<JobForTeacher> lstjft = new ArrayList<JobForTeacher>(lstJobForTeacher);
							List<JobForTeacher> lstremoveJFT = new ArrayList<JobForTeacher>();
							for(JobForTeacher jft: lstjft){
								if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
									lstJobForTeacher.remove(jft);
									lstremoveJFT.add(jft);
								}
							}
							Collections.sort(lstJobForTeacher,JobForTeacher.jobForTeacherComparator);
							Collections.sort(lstremoveJFT,JobForTeacher.jobForTeacherComparator);
							lstJobForTeacher.addAll(lstremoveJFT);
							//-- sorting CG view end



							String tFname = "";
							String tLname = "";

							for(JobForTeacher jft:lstJobForTeacher){

								if(jft.getTeacherId()!=null){
									tFname = jft.getTeacherId().getFirstName()==null?"":jft.getTeacherId().getFirstName();
									tLname = jft.getTeacherId().getLastName()==null?"":jft.getTeacherId().getLastName();
								}

								System.out.println(jft.getTeacherId().getEmailAddress()+" <> "+jft.getCompositeScore());
							}


							lstStatusMasters=null;
							lstStatusMasters = new ArrayList<StatusMaster>();			
							lstStatusMasters.add(statusVlt);
							lstJobForTeacherVlt.addAll(jobForTeacherDAO.findJFTbyJobOrder(jobOrder,lstStatusMasters));

							//------------------map creation start-------

							if(teacherDetails!=null && teacherDetails.size()>0){
								for (JobForTeacher jbforteacher : lstJobForTeacherVlt) 
									teacherDetails.add(jbforteacher.getTeacherId());
								teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetails);
								for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {				
									baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
								}
							}


							//------------------map creation end--------
							lstJFTremove = new ArrayList<JobForTeacher>(lstJobForTeacherVlt);			
							for(JobForTeacher jft : lstJFTremove)
							{				
								teAssesStatusTempBase = baseTakenMap.get(jft.getTeacherId().getTeacherId());

								if((teAssesStatusTempBase==null ) || ( teAssesStatusTempBase.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")))
								{	
									lstJobForTeacherVlt.remove(jft);
								}				
							}



							//--Sorting CG report data -- start
							lstjft = new ArrayList<JobForTeacher>(lstJobForTeacherVlt);
							lstremoveJFT = new ArrayList<JobForTeacher>();
							for(JobForTeacher jft: lstjft){
								if(jft.getStatus().getStatusShortName().equalsIgnoreCase("rem")){
									lstJobForTeacherVlt.remove(jft);
									lstremoveJFT.add(jft);
								}
							}
							Collections.sort(lstJobForTeacherVlt,JobForTeacher.jobForTeacherComparatorTeacher);
							Collections.sort(lstremoveJFT,JobForTeacher.jobForTeacherComparatorTeacher);
							lstJobForTeacherVlt.addAll(lstremoveJFT);
							//--Sorting CG report data -- end					

							for(JobForTeacher jft:lstJobForTeacherVlt){	
								if(jft.getTeacherId()!=null){
									tFname = jft.getTeacherId().getFirstName()==null?"":jft.getTeacherId().getFirstName();
									tLname = jft.getTeacherId().getLastName()==null?"":jft.getTeacherId().getLastName();
								}								
								System.out.println(jft.getTeacherId().getEmailAddress()+" <> "+jft.getCompositeScore());
							}							
						} 
						catch (Exception e){
							e.printStackTrace();
						}		
					}

					if(isError){												
						UtilityAPI.writeXMLErrors(errorCode, errorMsg, null, doc, root);
					}
					else{






						Element eleStatus = doc.createElement("status");
						root.appendChild(eleStatus);
						Text textStatus = doc.createTextNode("true");
						eleStatus.appendChild(textStatus);

						Element timeStamp = doc.createElement("timeStamp");
						root.appendChild(timeStamp);
						Text timeStampResult = doc.createTextNode(UtilityAPI.getCurrentTimeStamp());
						timeStamp.appendChild(timeStampResult);



						String tFname = "";
						String tLname = "";
						for(JobForTeacher jft: lstJobForTeacher){

							if(jft.getTeacherId()!=null){
								tFname = jft.getTeacherId().getFirstName()==null?"":jft.getTeacherId().getFirstName();
								tLname = jft.getTeacherId().getLastName()==null?"":jft.getTeacherId().getLastName();
							}
							Element eleScore = doc.createElement("score");
							root.appendChild(eleScore);							

							Element eleTname = doc.createElement("teachername");
							eleScore.appendChild(eleTname);
							Text textName = doc.createTextNode(tFname+" "+tLname);
							eleTname.appendChild(textName);

							Element eleEmail = doc.createElement("emailAddress");
							eleScore.appendChild(eleEmail);
							Text textEmail = doc.createTextNode(jft.getTeacherId().getEmailAddress());
							eleEmail.appendChild(textEmail);

							Element eleComScore = doc.createElement("compositeScore");
							eleScore.appendChild(eleComScore);
							Text textCompScore = doc.createTextNode(""+Math.round(jft.getCompositeScore()));
							eleComScore.appendChild(textCompScore);
						}
						for(JobForTeacher jft: lstJobForTeacherVlt){							
							if(jft.getTeacherId()!=null){
								tFname = jft.getTeacherId().getFirstName()==null?"":jft.getTeacherId().getFirstName();
								tLname = jft.getTeacherId().getLastName()==null?"":jft.getTeacherId().getLastName();
							}
							Element eleScore = doc.createElement("score");
							root.appendChild(eleScore);							

							Element eleTname = doc.createElement("teachername");
							eleScore.appendChild(eleTname);
							Text textName = doc.createTextNode(tFname+" "+tLname);
							eleTname.appendChild(textName);

							Element eleEmail = doc.createElement("emailAddress");
							eleScore.appendChild(eleEmail);
							Text textEmail = doc.createTextNode(jft.getTeacherId().getEmailAddress());
							eleEmail.appendChild(textEmail);

							Element eleComScore = doc.createElement("compositeScore");
							eleScore.appendChild(eleComScore);
							Text textCompScore = doc.createTextNode("");
							eleComScore.appendChild(textCompScore);
						}						
						//Text textScore = doc.createTextNode(UtilityAPI.getCurrentTimeStamp());
						//eleScore.appendChild(textScore);											
					}
				}
			}catch (Exception e){
				UtilityAPI.writeXMLErrors(errorCode, errorMsg, null, doc, root);				
				e.printStackTrace();
			}		
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();

			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
			out.println(sw.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;	
	}


	@RequestMapping(value="/service/json/getCompositeScore.do", method=RequestMethod.GET)
	public String getComposteScoreJSON(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println(">>>><<<>>>");
		String candidateEmail = request.getParameter("candidateEmail")==null?"":request.getParameter("candidateEmail").trim();


		TeacherDetail teacherDetail = null;

		Integer errorCode=0;
		String errorMsg="";
		String decileColor = "";
		PrintWriter out = null;
		JSONObject jsonResponse = new JSONObject();		
		response.setContentType("application/json");

		try {	
			if(!candidateEmail.trim().equals("")){
				candidateEmail = UtilityAPI.decodeBase64(candidateEmail);
			}
			out =  response.getWriter();
			Boolean isValidUser = false;						
			Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
			isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
			Boolean isError = false;
			double domainTotatlScore = 0;
			List<TeacherDetail> lstTD = teacherDetailDAO.findByEmail(candidateEmail);
			if(lstTD!=null && lstTD.size()>0){
				teacherDetail = lstTD.get(0);
			}
			Map<String,String> mapUserInfo = new HashMap<String, String>();

			if(!isValidUser){
				jsonResponse = UtilityAPI.writeJSONErrors(new Integer(mapDistSchDetail.get("errorCode")),mapDistSchDetail.get("errorMsg"), null);
			}
			else{	
				mapUserInfo = serviceUtility.authUser(request);
				if(! new Boolean(mapUserInfo.get("isSuccess"))){
					System.out.println("E1");
					isError = true;										
					errorCode= new Integer(mapUserInfo.get("errorCode"));
					errorMsg=mapUserInfo.get("errorMsg");
				}			
				else if(candidateEmail.equals("")){
					isError = true;										
					errorCode=10002;
					errorMsg=Utility.getLocaleValuePropByKey("msgCandEmailReq", locale);
				}
				else if(teacherDetail==null){
					isError = true;										
					errorCode=10004;
					errorMsg=Utility.getLocaleValuePropByKey("msgCandNotExit", locale);
				}
				else{										
					
					try 
					{
						TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
						if(teacherBaseAssessmentStatus==null){
							isError = true;										
							errorCode=10007;
							errorMsg=Utility.getLocaleValuePropByKey("msgNotGivEPI", locale);
						}
						else if(teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equals("icomp")){
							isError = true;										
							errorCode=10008;
							errorMsg=Utility.getLocaleValuePropByKey("msgCanNotComplEPI", locale);
						}
						else if(teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equals("vlt")){
							isError = true;										
							errorCode=10009;
							errorMsg=Utility.getLocaleValuePropByKey("msgCandTimeoutEPI", locale);
						}
						else{
							List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
							teacherDetails.add(teacherDetail);

							List<TeacherNormScore> teacherNormScores = teacherNormScoreDAO.findNormScoreByTeacherList(teacherDetails);
							if(teacherNormScores.size()==0)
							{	
								isError = true;										
								errorCode=10010;
								errorMsg=Utility.getLocaleValuePropByKey("msgCanCompScoreproceed", locale);
								
							}else
							{
								TeacherNormScore tns = teacherNormScores.get(0);
								domainTotatlScore = tns.getTeacherNormScore();
								decileColor = tns.getDecileColor();
							}

						}
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}		
				}

				if(isError){
					System.out.println("IFsuccess");
					jsonResponse = UtilityAPI.writeJSONErrors(errorCode, errorMsg, null);
				}
				else{
					System.out.println("Else Filure");
					jsonResponse = new JSONObject();
					jsonResponse.put("status",true);
					jsonResponse.put("timestamp",UtilityAPI.getCurrentTimeStamp());
					jsonResponse.put("score", Math.round(domainTotatlScore));
					jsonResponse.put("decileColor", decileColor);
				}
			}
		}catch (Exception e){
			jsonResponse = UtilityAPI.writeJSONErrors(10017, "Server Error.", e);
			e.printStackTrace();
		}		
		out.print(jsonResponse);
		return null;	
	}


	@RequestMapping(value="/service/xml/getCompositeScore.do", method=RequestMethod.GET)
	public String getComposteScoreXML(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		String candidateEmail = request.getParameter("candidateEmail")==null?"":request.getParameter("candidateEmail").trim().replace(' ', '+');

		TeacherDetail teacherDetail = null;

		Integer errorCode=0;
		String errorMsg="";

		PrintWriter out = null;
		response.setContentType("text/xml");
		try {			
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = null;
			Document doc = null;
			Element root = null;

			docBuilder = builderFactory.newDocumentBuilder();
			doc = docBuilder.newDocument();
			root = doc.createElement("teachermatch");
			doc.appendChild(root);	
			try {
				if(!candidateEmail.trim().equals("")){
					candidateEmail = UtilityAPI.decodeBase64(candidateEmail);
				}
				out =  response.getWriter();
				Boolean isValidUser = false;						
				Map<String,String> mapDistSchDetail = serviceUtility.authDistOrSchool(request);
				isValidUser = new Boolean(mapDistSchDetail.get("isValidUser"));
				Boolean isError = false;
				double domainTotatlScore = 0;
				List<TeacherDetail> lstTD = teacherDetailDAO.findByEmail(candidateEmail);
				if(lstTD!=null && lstTD.size()>0){
					teacherDetail = lstTD.get(0);
				}
				Map<String,String> mapUserInfo = new HashMap<String, String>();

				if(!isValidUser){					
					UtilityAPI.writeXMLErrors(new Integer(mapDistSchDetail.get("errorCode")),mapDistSchDetail.get("errorMsg"),null, doc, root);
				}
				else{	
					mapUserInfo = serviceUtility.authUser(request);
					if(! new Boolean(mapUserInfo.get("isSuccess"))){
						System.out.println("E1");
						isError = true;										
						errorCode= new Integer(mapUserInfo.get("errorCode"));
						errorMsg=mapUserInfo.get("errorMsg");
					}			
					else if(candidateEmail.equals("")){
						isError = true;										
						errorCode=10002;
						errorMsg=Utility.getLocaleValuePropByKey("msgCandEmailReq", locale);
					}
					else if(teacherDetail==null){
						isError = true;										
						errorCode=10004;
						errorMsg=Utility.getLocaleValuePropByKey("msgCandNotExit", locale);
					}
					else{										

						double score = 0;
						lstJobForTeacherVlt = new ArrayList<JobForTeacher>();
						try 
						{
							TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
							if(teacherBaseAssessmentStatus==null){
								isError = true;										
								errorCode=10007;
								errorMsg=Utility.getLocaleValuePropByKey("msgNotGivEPI", locale);
							}
							else if(teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equals("icomp")){
								isError = true;										
								errorCode=10008;
								errorMsg=Utility.getLocaleValuePropByKey("msgCanNotComplEPI", locale);
							}
							else if(teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equals("vlt")){
								isError = true;										
								errorCode=10008;
								errorMsg=Utility.getLocaleValuePropByKey("msgCandTimeoutEPI", locale);
							}
							else{
								System.out.println("^^^^^");
								List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;

								mapDomainScore = new HashMap<Integer, Map<Integer,Object>>();
								Map<Integer, Object> mapPercentScore = null;
								List lstAns = null;
								TeacherDetail tDetail = null;
								for(DomainMaster dm: lstDomain){
									mapPercentScore = new HashMap<Integer, Object>();
									lstAns = teacherAnswerDetailDAO.findSumOfAnsByDomainAndTeacher(teacherDetail, dm);
									for(Object oo: lstAns){
										Object obj[] = (Object[])oo;
										tDetail = (TeacherDetail) obj[0];
										mapPercentScore.put( tDetail.getTeacherId(), obj);
									}
									mapDomainScore.put(dm.getDomainId(), mapPercentScore);
								}

								domainTotatlScore=0.0;
								for(DomainMaster domain: lstDomain){
									score =  getPercentScoreOfBasesAssement(domain,teacherDetail); 
									domainTotatlScore = domainTotatlScore+score*domain.getMultiplier();
									System.out.println(domainTotatlScore);
								}
							}
						} 
						catch (Exception e){
							e.printStackTrace();
						}		
					}

					if(isError){
						System.out.println("IFsuccess");
						UtilityAPI.writeXMLErrors(errorCode, errorMsg, null, doc, root);
					}
					else{
						System.out.println("Else Filure");

						Element eleStatus = doc.createElement("status");
						root.appendChild(eleStatus);
						Text textStatus = doc.createTextNode("true");
						eleStatus.appendChild(textStatus);

						Element eleComScore = doc.createElement("compositeScore");
						root.appendChild(eleComScore);
						Text textCompScore = doc.createTextNode(""+Math.round(domainTotatlScore));
						eleComScore.appendChild(textCompScore);

						Element timeStamp = doc.createElement("timeStamp");
						root.appendChild(timeStamp);
						Text timeStampResult = doc.createTextNode(UtilityAPI.getCurrentTimeStamp());
						timeStamp.appendChild(timeStampResult);


					}
				}
			}catch (Exception e){

				UtilityAPI.writeXMLErrors(10001, Utility.getLocaleValuePropByKey("msgInvalidCredentials", locale), e, doc, root);
				e.printStackTrace();
			}		
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();

			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
			out.println(sw.toString());


		} catch (Exception e) {
			e.printStackTrace();
		}


		return null;	
	}







	/*	@RequestMapping(value="/service/updateqpool.do", method=RequestMethod.GET)
	public String doUpdateQPool(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("/service/updateqpool.do>");
		try {
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");


			if(request.getParameter("re").equals("1"))
			lstQuestionsPools = questionsPoolDAO.findAll();
			else
			//lstQuestionsPools = questionsPoolDAO.findByCriteria(Restrictions.ilike("questionUId","%jsi%" ));
			lstQuestionsPools = questionsPoolDAO.findByCriteria(Restrictions.not(Restrictions.ilike("questionUId","%jsi%" )));



			out.write("Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}
	@RequestMapping(value="/service/updateqpool1.do", method=RequestMethod.GET)
	public String doPool(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{

		System.out.println("/service/updateqpool.do>");
		try {
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");



			System.out.println(lstQuestionsPools.size());

			for(QuestionsPool pool: lstQuestionsPools){				
				if(pool.getDomainMaster()==null || pool.getCompetencyMaster()==null || pool.getObjectiveMaster()==null ){
					pool.setQuestionUId("jsi."+Utility.addZeroBeforeNo(pool.getQuestionId(),5));
				}
				else{
					pool.setQuestionUId(pool.getDomainMaster().getDomainUId()+"."+pool.getCompetencyMaster().getCompetencyUId()+"."+pool.getObjectiveMaster().getObjectiveUId()+"."+Utility.addZeroBeforeNo(pool.getQuestionId(),5));
				}
				System.out.println("Question Pool Id"+pool.getQuestionId());
				questionsPoolDAO.updatePersistent(pool);				

			}			
			out.write("Done1");
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;


	}*/

	@RequestMapping(value="/service/teacherassessmentquestions.do", method=RequestMethod.GET)
	public String doUpdateTeacherAssessmentQuestions(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("/service/teacherassessmentquestions.do>");
		try {
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");

			Integer start = Integer.parseInt(request.getParameter("start"));
			Integer end = Integer.parseInt(request.getParameter("end"));

			teacherAssessmentQuestions = teacherAssessmentQuestionDAO.findWithLimit(null, start, end);

			System.out.println("size: "+teacherAssessmentQuestions.size());
			//out.write("Done  ");
			out.write(Utility.getBaseURL(request)+"service/teacherassessmentquestions.do?start="+(start+200)+"&end=200");
			//out.write("<br><a href='http://localhost:8080/teachermatch/service/teacherassessmentquestions.do?start="+(start+200)+"&end=200' > Click </a>");
			out.write("<br>11<a href='teacherassessmentquestions.do?start="+(start+200)+"&end=200' > "+Utility.getLocaleValuePropByKey("btnClick", locale)+" </a>");
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;
	}
	@RequestMapping(value="/service/teacherassessmentquestions1.do", method=RequestMethod.GET)
	public String doUpdate(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{

		System.out.println("/service/teacherassessmentquestions.do>");
		try {
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			Integer start = Integer.parseInt(request.getParameter("start"));
			Integer end = Integer.parseInt(request.getParameter("end"));
			if(end>=teacherAssessmentQuestions.size())
				end = teacherAssessmentQuestions.size();

			String uid = quid;
			out.println(teacherAssessmentQuestions.size());
			List<TeacherAssessmentQuestion> lstsortedteacherAssessmentQuestions		=	teacherAssessmentQuestions.subList(start,end);
			out.println(lstsortedteacherAssessmentQuestions.size()+"<br>");
			//for(TeacherAssessmentQuestion pool: teacherAssessmentQuestions){
			for(TeacherAssessmentQuestion pool: lstsortedteacherAssessmentQuestions){
				pool.setQuestionUId(uid);
				teacherAssessmentQuestionDAO.updatePersistent(pool);	
			}

			int enddata = (start+250+250);
			if(enddata>teacherAssessmentQuestions.size())
				enddata = teacherAssessmentQuestions.size();

			if(lstsortedteacherAssessmentQuestions.size()>0 && teacherAssessmentQuestions.size() >=(start+250))
			{
				out.write(Utility.getBaseURL(request)+"service/teacherassessmentquestions1.do?start="+(start+250)+"&end="+(enddata));
				out.write("<br><a href='teacherassessmentquestions1.do?start="+(start+250)+"&end="+(enddata)+"' > Click </a>");
			}else
			{out.print("Done");
			out.write("<br><a href='teacherassessmentquestions1.do?start="+(0)+"&end="+(250)+"' > Click </a>");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;


	}
	@RequestMapping(value="/service/updateteacherassessmentquestions.do", method=RequestMethod.GET)
	public String doUpdateQuestins(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{

		System.out.println("/service/teacherassessmentquestions.do>");
		try {
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");

			Integer start = Integer.parseInt(request.getParameter("start"));
			Integer end = Integer.parseInt(request.getParameter("end"));

			Criterion c = null;
			String type = request.getParameter("type");
			if(type.equals("b"))
				c = Restrictions.not(Restrictions.ilike("questionUId","%jsi%"));
			else
				c = Restrictions.ilike("questionUId","%jsi%");
			//findWithLimit(null, start, end);
			//lstQuestionsPools = questionsPoolDAO.findByCriteria(c);
			lstQuestionsPools = questionsPoolDAO.findWithLimit(null, start, end,c);

			out.println(lstQuestionsPools.size()+"<br>");
			System.out.println(lstQuestionsPools.size());
			String question = "";
			//for(TeacherAssessmentQuestion pool: teacherAssessmentQuestions){
			for(QuestionsPool pool: lstQuestionsPools){				

				question = pool.getQuestionUId()+":  --> "+pool.getQuestion()+"<br>";
				//teacherAssessmentQuestions = teacherAssessmentQuestionDAO.findByCriteria(Restrictions.ilike("question","%"+pool.getQuestion()+"%" ))	;
				teacherAssessmentQuestions = teacherAssessmentQuestionDAO.findByCriteria(Restrictions.eq("question",""+pool.getQuestion()+""),Restrictions.isNull("questionUId"))	;
				System.out.println("List:  "+teacherAssessmentQuestions.size());
				quid = pool.getQuestionUId();
			}	
			out.write(teacherAssessmentQuestions.size()+"<br>");
			out.write(Utility.getBaseURL(request)+"service/updateteacherassessmentquestions.do?type="+type+"&start="+(start+1)+"&end=1");
			//out.write("<br><a href='http://localhost:8080/teachermatch/service/teacherassessmentquestions.do?start="+(start+200)+"&end=200' > Click </a>");
			out.write("<br><a href='updateteacherassessmentquestions.do?type="+type+"&start="+(start+1)+"&end=1' > "+Utility.getLocaleValuePropByKey("btnClick", locale)+" </a>");
			out.write("Done1");
			out.write("<br><br><br><br>"+question);
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;


	}

	// updation
	@RequestMapping(value="/service/getrawdata.do", method=RequestMethod.GET)
	public String getRawData(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{

		System.out.println("/service/getrawdata.do>");
		try {
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			Map<String, Integer> uIdMap = new HashMap<String, Integer>();
			List<String> uniqueQuestionsIdList = teacherAssessmentQuestionDAO.findUniqueQuestionUId(); 
			out.print("Size: "+uniqueQuestionsIdList.size()+"<br>");
			int i=0;
			for (String string : uniqueQuestionsIdList) {
				out.println("q"+(++i)+": "+string+"<br>");
				uIdMap.put(string, i);
			}



			/*
			Class<?> classHandle = Class.forName("tm.bean.CandidateRawScore");
			Object myObject = classHandle.newInstance();

			for (TeacherAnswerDetail teacherAnswerDetail : teacherAnswerDetails) {
				Integer sid = uIdMap.get(teacherAnswerDetail.getTeacherAssessmentQuestion().getQuestionUId());
				if(sid!=null)
				Utility.set(myObject, "q"+sid, teacherAnswerDetail.getTotalScore());
			}

			CandidateRawScore candidateRawScore = (CandidateRawScore)myObject;
			candidateRawScore.setCreatedDateTime(new Date());
			candidateRawScore.setTeacherDetail(teacherDetail);
			 */

			Integer start = Integer.parseInt(request.getParameter("start"));
			Integer end = Integer.parseInt(request.getParameter("end"));


			List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
			StatusMaster statusMaster = new StatusMaster();
			statusMaster.setStatusId(4);
			teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTakenByTeachersByStautsAll(statusMaster);
			System.out.println("completed: "+teacherAssessmentStatusList.size());

			if(end>=teacherAssessmentStatusList.size())
				end = teacherAssessmentStatusList.size();

			List<TeacherAssessmentStatus> lstsortedteacherAssessmentStatusList		=	teacherAssessmentStatusList.subList(start,end);

			for (TeacherAssessmentStatus teacherAssessmentStatus : lstsortedteacherAssessmentStatusList) {

				List<TeacherAnswerDetail> teacherAnswerDetails = null;
				TeacherDetail teacherDetail = teacherAssessmentStatus.getTeacherDetail();
				teacherAnswerDetails = teacherAnswerDetailDAO.findTeacherEPIQuestions(teacherDetail);

				CandidateRawScore candidateRawScore = new CandidateRawScore();
				for (TeacherAnswerDetail teacherAnswerDetail : teacherAnswerDetails) {

					Integer sid = uIdMap.get(teacherAnswerDetail.getTeacherAssessmentQuestion().getQuestionUId());
					if(sid!=null && teacherAnswerDetail.getTeacherAssessmentQuestion().getQuestionWeightage()>0)
						PropertyUtils.setProperty(candidateRawScore, "q"+sid,teacherAnswerDetail.getTotalScore());
				}
				candidateRawScore.setCreatedDateTime(new Date());
				candidateRawScore.setTeacherDetail(teacherDetail);

				candidateRawScoreDAO.makePersistent(candidateRawScore);

			}

			int enddata = (start+40+40);
			if(enddata>teacherAssessmentStatusList.size())
				enddata = teacherAssessmentStatusList.size();

			if(lstsortedteacherAssessmentStatusList.size()>0 && teacherAssessmentStatusList.size() >=(start+40))
			{
				out.write(Utility.getBaseURL(request)+"service/getrawdata.do?start="+(start+40)+"&end="+(enddata));
				out.write("<br><a href='getrawdata.do?start="+(start+40)+"&end="+(enddata)+"' > "+Utility.getLocaleValuePropByKey("btnClick", locale)+" </a>");
			}else
			{out.print("Done");
			out.write("<br><a href='getrawdata.do?start="+(0)+"&end="+(40)+"' > "+Utility.getLocaleValuePropByKey("btnClick", locale)+" </a>");
			}



		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;


	}
	@RequestMapping(value="/service/getcandidatedata.do", method=RequestMethod.GET)
	public String getCandidateData(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{

		System.out.println("/service/getcandidatedata.do>");
		try {
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				response.sendRedirect("../index.jsp");
			}
			/*PrintWriter out = response.getWriter();
			response.setContentType("text/html");*/
			Map<String, Integer> uIdMap = new HashMap<String, Integer>();
			List<String> uniqueQuestionsIdList = teacherAssessmentQuestionDAO.findUniqueQuestionUId(); 
			//out.print("Size: "+uniqueQuestionsIdList.size()+"<br>");
			int i=0;
			for (String string : uniqueQuestionsIdList) {
				//out.println("q"+(++i)+": "+string+"<br>");
				uIdMap.put(string, i);
			}
			/*String time = String.valueOf(System.currentTimeMillis()).substring(6);
			//String basePath = request.getRealPath("/")+"/candidate";
			String basePath = request.getSession().getServletContext().getRealPath ("/")+"/candidate";
			String fileName =time+"candidates.xls";
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition","attachment;filename=candidates.xls");

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);

			file = new File(basePath+"/"+fileName);*/


			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));

			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat times;

			//WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);

			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition",    "attachment; filename=epicandidates.xls");

			WritableWorkbook workbook = Workbook.createWorkbook(response.getOutputStream());

			workbook.createSheet(Utility.getLocaleValuePropByKey("msgTCEPIData", locale), 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);

			// Create create a bold font with unterlines
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			header.setBackground(Colour.GRAY_25);

			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			// Write a few headers
			excelSheet.mergeCells(0, 0, 8, 1);
			Label label;
			label = new Label(0, 0, Utility.getLocaleValuePropByKey("msgTeacherMatchTCEPIData", locale), timesBoldUnderline);
			excelSheet.addCell(label);

			////////////////////// Headeer ///////////////////////////////////
			Number number;

			label = new Label(0, 3, "ID",header); 
			excelSheet.addCell(label);
			label = new Label(1, 3, "Name",header); 
			excelSheet.addCell(label);
			label = new Label(2, 3, "Email",header); 
			excelSheet.addCell(label);

			int j =3;
			for (String string : uniqueQuestionsIdList) {
				label = new Label(j, 3, string,header); 
				excelSheet.addCell(label);
				++j;
			}

			///////////////////////////////////////////////////
			List<CandidateRawScore> candidateRawScoreList = candidateRawScoreDAO.findAll();

			TeacherDetail teacherDetail;

			int k=4;
			for (CandidateRawScore candidateRawScore : candidateRawScoreList) {
				excelSheet.getSettings().setDefaultColumnWidth(15);
				teacherDetail = candidateRawScore.getTeacherDetail();
				number = new Number(0, k, teacherDetail.getTeacherId(), times);
				excelSheet.addCell(number);
				label = new Label(1, k, ""+teacherDetail.getFirstName()+" "+""+teacherDetail.getLastName()); 
				excelSheet.addCell(label);
				label = new Label(2, k, ""+teacherDetail.getEmailAddress()); 
				excelSheet.addCell(label);

				int s = 3;
				int c = 0;
				for (String string : uniqueQuestionsIdList) {
					++c;
					Double d = (Double)PropertyUtils.getProperty(candidateRawScore, "q"+c);
					if(d==null)
					{
						label = new Label(s, k, "");
						excelSheet.addCell(label);
					}else
					{
						number = new Number(s, k, d, times);
						excelSheet.addCell(number);
					}		      	
					++s;
				}		      	
				++k;
			}

			workbook.write();
			workbook.close();

			/*FileInputStream fileIn = new FileInputStream(file);
			ServletOutputStream out1 = response.getOutputStream();

			byte[] outputByte = new byte[4096];
			//copy binary contect to output stream
			while(fileIn.read(outputByte, 0, 4096) != -1)
			{
				out1.write(outputByte, 0, 4096);
			}
			fileIn.close();*/
			/*out.flush();
		    out.close();*/
			excelSheet = null;
			uIdMap =null;

		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;


	}

	public int getPercentScoreOfBasesAssement(DomainMaster domainMaster, TeacherDetail teacherDetail)
	{	
		double sum=0;
		double maxMark=0;
		double percent=0;

		try{
			Object obj[] = (Object[]) mapDomainScore.get(domainMaster.getDomainId()).get(teacherDetail.getTeacherId());			
			sum = obj[1]==null?0:(Double)obj[1];				
			maxMark = obj[2]==null?0:(Double)obj[2];
			percent = sum/maxMark*100;
		}		
		catch (Exception e){
			//System.out.println();
			e.printStackTrace();
		}
		return (int) Math.round(percent);
	}

	/*@RequestMapping(value="/upload.do", method=RequestMethod.POST)
	  public String uploadFile(@RequestParam("file") CommonsMultipartFile file){
	    if (!file.isEmpty()){
	      byte fileBytes[] = file.getBytes();
	      System.out.println("dd");
	      //return "mainView";
	    }else{
	      //return "errorView";
	    }
	    return null;
	 }*/
	@RequestMapping(value="/service/processdata.do", method=RequestMethod.GET)
	public String processData(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{

		System.out.println("/service/processdata.do>");

		try {
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				response.sendRedirect("../index.jsp");
			}

			response.setContentType("application/octet-stream");
			//response.setHeader("Content-Disposition","attachment;done.txt");
			commonDashboardAjax.UpdateRecords();

			PrintWriter out = response.getWriter();
			//response.setContentType("text/html");
			out.print("<script>alert('hi');</script>");
		}catch (Exception e) {
			// TODO: handle exception
		}
		return null;

	}
	@RequestMapping(value="/service/getcandidatedataexport.do", method=RequestMethod.GET)
	public String getCandidateDataExport(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{

		System.out.println("/service/getcandidatedataexport.do>");
		try {
			/*PrintWriter out = response.getWriter();
			response.setContentType("text/html");*/
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				response.sendRedirect("../index.jsp");
			}

			/*	String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = request.getSession().getServletContext().getRealPath ("/")+"/candidate";
			String fileName =time+"candidates.xls";
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition","attachment;filename=candidates.xls");

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);

			file = new File(basePath+"/"+fileName);*/


			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));

			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat times;

			//WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition",    "attachment; filename=candidates.xls");

			WritableWorkbook workbook = Workbook.createWorkbook(response.getOutputStream());

			workbook.createSheet(Utility.getLocaleValuePropByKey("TeacherEPIdata", locale), 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);

			// Create create a bold font with unterlines
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			header.setBackground(Colour.GRAY_25);

			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			// Write a few headers
			excelSheet.mergeCells(0, 0, 8, 1);
			Label label;
			label = new Label(0, 0, Utility.getLocaleValuePropByKey("msgTeacherMatchTeacherEPIdata", locale), timesBoldUnderline);
			excelSheet.addCell(label);

			////////////////////// Headeer ///////////////////////////////////
			Number number;

			label = new Label(0, 3, Utility.getLocaleValuePropByKey("lblFname", locale),header); 
			excelSheet.addCell(label);
			label = new Label(1, 3, Utility.getLocaleValuePropByKey("lblLname", locale) ,header); 
			excelSheet.addCell(label);
			label = new Label(2, 3, Utility.getLocaleValuePropByKey("lblEmail1", locale),header); 
			excelSheet.addCell(label);
			label = new Label(3, 3, Utility.getLocaleValuePropByKey("pdp3", locale) ,header); 
			excelSheet.addCell(label);
			label = new Label(4, 3, Utility.getLocaleValuePropByKey("lblNormScore", locale),header); 
			excelSheet.addCell(label);
			label = new Label(5, 3, Utility.getLocaleValuePropByKey("lblAttitudinal", locale),header); 
			excelSheet.addCell(label);
			label = new Label(6, 3, Utility.getLocaleValuePropByKey("msgCognitiveAbility", locale),header); 
			excelSheet.addCell(label);
			label = new Label(7, 3, Utility.getLocaleValuePropByKey("teachingSkills", locale) ,header); 
			excelSheet.addCell(label);


			///////////////////////////////////////////////////
			List<DomainMaster> domainMasters = new ArrayList<DomainMaster>();
			domainMasters = WorkThreadServlet.domainMastersPDRList;
			List<TeacherDetail> teacherDetailsBlank = new ArrayList<TeacherDetail>();
			List lstRawData = new ArrayList();
			lstRawData = rawDataForDomainDAO.findAllTeacherEpiNormScore(0,0,1,0,false,domainMasters,teacherDetailsBlank);
			TeacherDetail teacherDetail = null;
			String tFname=null;
			String tLname=null;
			String tEmail=null;
			double dScore = 0;
			int k=4;
			double one = 0;
			double two = 0;
			double three = 0;

			double done = 0;
			double dtwo = 0;
			double dthree = 0;

			System.out.println("lstRawData.size(): "+lstRawData.size());

			for(Object oo: lstRawData){
				Object obj[] = (Object[])oo;
				//teacherDetail = (TeacherDetail) obj[0];

				tFname = obj[0]==null?"":""+obj[5];
				tLname = obj[0]==null?"":""+obj[6];
				tEmail = obj[0]==null?"":""+obj[7];

				//System.out.println(tFname);
				excelSheet.getSettings().setDefaultColumnWidth(18);
				label = new Label(0, k, ""+tFname); 
				excelSheet.addCell(label);
				label = new Label(1, k, ""+tLname);
				excelSheet.addCell(label);
				label = new Label(2, k, ""+tEmail);
				excelSheet.addCell(label);
				label = new Label(3, k, ""+Utility.convertDateAndTimeToUSformatOnlyDate((Date)obj[8]));
				excelSheet.addCell(label);
				//System.out.println("ll : "+done+dtwo+dthree);
				label = new Label(4, k, ""+Math.round((Double)obj[1]));
				excelSheet.addCell(label);
				label = new Label(5, k, ""+Math.round((Double)obj[2]));
				excelSheet.addCell(label);
				label = new Label(6, k, ""+Math.round((Double)obj[3]));
				excelSheet.addCell(label);
				label = new Label(7, k, ""+Math.round((Double)obj[4]));
				excelSheet.addCell(label);

				k++;

			}

			workbook.write();
			workbook.close();

			/*FileInputStream fileIn = new FileInputStream(file);
			ServletOutputStream out1 = response.getOutputStream();

			byte[] outputByte = new byte[4096];
			//copy binary contect to output stream
			while(fileIn.read(outputByte, 0, 4096) != -1)
			{
				out1.write(outputByte, 0, 4096);
			}
			fileIn.close();*/
			/*out.flush();
		    out.close();*/
			excelSheet = null;

		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
		}
		return null;


	}
}