package tm.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherExperienceDAO;
import tm.services.EmailerService;
import tm.utility.ImageResize;
import tm.utility.Utility;


public class IdentityVerificationServlet extends HttpServlet 
{
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		System.out.println(">>>>>>> IdentityVerificationServlet :::>>>>>>>>>>");
		response.setContentType("text/html");
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
	    List uploadedItems = null;
		FileItem fileItem = null;
		String ext="",filePath="";
		int []imgSize=null;
		try{
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			String fileName="";
			String teacherId="";
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if(fileItem.getFieldName().equals("teacherIdForPicture")){
					teacherId=fileItem.getString();
					filePath=Utility.getValueOfPropByKey("teacherRootPath")+teacherId+"/";
					System.out.println("filePath=======>>"+filePath);
				
					File f=new File(filePath);
					if(!f.exists())
						 f.mkdirs();
				}
				
				if (fileItem.isFormField() == false) 
				{
					if (fileItem.getSize() > 0)	
					{
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						fileName="TeacherPicture"+teacherId+Utility.getDateTime()+ext;
						uploadedFile = new File(filePath, fileName);
						fileItem.write(uploadedFile);
						System.out.println("fileName="+fileName);
						imgSize=ImageResize.resizePicture(filePath+ fileName);
						System.out.println("imgSizeH::"+imgSize[1]+" W:"+imgSize[0]);
					}
				}
				fileItem=null;				
			}
			
			PrintWriter pw = response.getWriter();
			response.setContentType("text/html");
			//pw.print("<style>.screen{width:"+imgSize[1]+"px;height:"+imgSize[0]+"px;margin: -200 auto auto;background:#FFFFFF;border:1px dashed gray;line-height: "+imgSize[0]+"px;text-align: center;color:#000000;}</style> ");
			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			pw.print("window.top.showPicture('"+teacherId+"','"+fileName+"','"+imgSize[1]+"','"+imgSize[0]+"');");
			pw.print("</script>");
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

}
