package tm.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tm.utility.Utility;


public class DynamicImage extends HttpServlet {

	public void doGet(HttpServletRequest httpServletRequest,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			String imagePath = "";
			String getid = "";
			int _x = 0;
			int _y = 0;
			if (httpServletRequest.getParameter("image") != null) {
				getid = httpServletRequest.getParameter("image");
			}
			if (httpServletRequest.getParameter("x") != null) {
				_x = Integer.parseInt(httpServletRequest.getParameter("x"));
			}
			if (httpServletRequest.getParameter("y") != null) {
				_y = Integer.parseInt(httpServletRequest.getParameter("y"));
			}
			imagePath = Utility.getValueOfPropByKey("rootPath")+getid;  
			//System.out.println("imagePath::"+imagePath);
			File myTifFile = new File(imagePath);
			InputStream inputstream = new FileInputStream(myTifFile);
		//	InputStream in_1;  
		//	in_1 = ResizeImages.scaleImage(inputstream, _x, _y);
			byte buf[] = new byte[1024];

			response.setContentType("image/jpeg");
			int len;
			while ((len = inputstream.read(buf)) > 0)
				response.getOutputStream().write(buf, 0, len);
			response.flushBuffer();
			/** ****************close all opened parameter*********** */
			inputstream.close();
			inputstream.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}