package tm.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;

import tm.bean.TeacherDetail;
import tm.utility.Utility;

public class SelfServiceCustomFieldFileUploadServlet extends HttpServlet {

	private static final long serialVersionUID = -2577664530601476361L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		System.out.println(":::::::::::::: AnswerUploadServlet ::::::::::::::");
		PrintWriter pw = response.getWriter();
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(10485760);
		HttpSession session = request.getSession();
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		String fileName="";
		//String newName= request.getParameter("answerFileName")
	    List uploadedItems = null;
		FileItem fileItem = null;
		String filePath =Utility.getValueOfPropByKey("teacherRootPath")+teacherDetail.getTeacherId()+"/";
		File f=new File(filePath);
		if(!f.exists())
			 f.mkdirs();
		String ext="";
		
		boolean bSuccess=true;
		
		try 
		{			
			uploadedItems = upload.parseRequest(request);
			Iterator i = uploadedItems.iterator();
			String path="";
			String fullFileName="";
			int spacePost	=	-1;
	
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if (fileItem.isFormField() == false) 
				{
					if (fileItem.getSize() > 0)	
					{
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("/") > 0) ? "/" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						if(myFullFileName.length()>125)
						{
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.lastIndexOf("."));
							ext=myFullFileName.substring(myFullFileName.lastIndexOf("."),myFullFileName.length());
							myFileName = myFullFileName.substring(startIndex + 1, 120);
							spacePost=myFileName.lastIndexOf(" ");
							if(spacePost!=-1)
							{
								myFileName = myFullFileName.substring(startIndex + 1, spacePost);
							}
							myFileName=myFileName.replaceAll("[^\\w\\s]", "");
							fullFileName=myFileName+""+ext;
							fileName="proof_of_highly_qualified_"+fullFileName.toLowerCase();
						}
						else
						{
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.lastIndexOf("."));
							ext=myFullFileName.substring(myFullFileName.lastIndexOf("."),myFullFileName.length());
							myFileName=myFileName.replaceAll("[^\\w\\s]", "");
							fullFileName=myFileName+""+ext;
							fileName="proof_of_highly_qualified_"+fullFileName.toLowerCase();
						}
						System.out.println("::::::::::::::::::::::::::::::fileName== "+fileName+"::::::::::::::::::::::::::::::");
						//uploadedFile = new File(filePath, fullFileName);
						uploadedFile = new File(filePath, fileName);
						fileItem.write(uploadedFile);
						
						String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+fileName;
						String target = getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
					     
					    File sourceFile = new File(source);
					    File targetDir = new File(target);
					    if(!targetDir.exists())
					    	targetDir.mkdirs();
					        
					    File targetFile = new File(targetDir+"/"+sourceFile.getName());
					    if(sourceFile.exists()){
					      	FileUtils.copyFile(sourceFile, targetFile);
					       	path =  Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
					    }
					}
				}
				fileItem=null;
			}
			response.setContentType("text/html");
			//pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			//pw.print("window.top.saveAnswer('"+fileName+"','"+path+"')");
			//pw.print("</script>");			
		} 
		catch (FileUploadException e) 
		{
			bSuccess=false;
			e.printStackTrace();
			
		} 
		catch (Exception e) 
		{
			bSuccess=false;
			e.printStackTrace();
		}
		finally
		{
			if(bSuccess)
				pw.print(fileName);
			else
				pw.print("Error");
		}
	}
}