package tm.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import tm.utility.Utility;

public class ReferenceNoteUploadServlet extends HttpServlet 
{

	private static final long serialVersionUID = 2869668342724616617L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		System.out.println("Calling ReferenceNoteUploadServlet ...");

		PrintWriter pw = response.getWriter();
		response.setContentType("text/html");  

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List uploadedItems = null;
		FileItem fileItem = null;
		String ext="",filePath="";
		String txtNotes="";
		String file="";
		String teacherId="";
		String eleRefId="";
		
		
		try{
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			String fileName="";
			String noteDateTime=Utility.getDateTime();
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if(fileItem.getFieldName().equals("txtNotes_ref")){
					txtNotes=fileItem.getString("UTF-8").trim();
				}
				if(fileItem.getFieldName().equals("teacherIdForNote_ref")){
					teacherId=fileItem.getString();
				}
				if(fileItem.getFieldName().equals("eleRefId")){
					eleRefId=fileItem.getString();
				}
				
			}

			System.out.println("txtNotes :: "+txtNotes);
			System.out.println("teacherId :: "+teacherId);
			System.out.println("eleRefId :: "+eleRefId);

	
			if(eleRefId!=null ){
				filePath=Utility.getValueOfPropByKey("referenceNotesRootPath")+"/ref_note/"+eleRefId+"/";
				System.out.println("filePath :: "+filePath);

				File f=new File(filePath);
				if(!f.exists())
					f.mkdirs();

				if (fileItem.isFormField() == false){
					if (fileItem.getSize() > 0){
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						fileName="ref_note"+noteDateTime+ext;
						uploadedFile = new File(filePath, fileName);
						fileItem.write(uploadedFile);
					}
				}
				fileItem=null;				
			}
			System.out.println("fileName:::"+fileName);
			response.setContentType("text/html");  
			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			pw.print("window.top.saveRefNoteFile('"+noteDateTime+"');");
			pw.print("</script>");
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

}
