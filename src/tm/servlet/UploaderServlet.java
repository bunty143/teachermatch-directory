package tm.servlet;

import java.awt.image.BufferedImage;
import java.io.File;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.mortennobel.imagescaling.ResampleOp;

import tm.utility.ImageResize;
import tm.utility.Utility;

/* @Author: Vishwanath Kumar
 * @Discription: 
 */
public class UploaderServlet extends HttpServlet 
{

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		
		/*int base = 0;
		String qs = request.getQueryString();
		if (qs != null) {
		   base += qs.length();
		}
		String str = "";
		for(Enumeration e = request.getHeaderNames();
		    e.hasMoreElements() ;) {
		    str = (String) e.nextElement();
		    System.out.println(str+" : "+request.getHeader(str));
		    base += str.length() + request.getHeader(str).length() + 2;
		}
		for(Enumeration e = request.getParameterNames();
		    e.hasMoreElements() ;) {
		    str = (String) e.nextElement();
		    base += str.length() + request.getParameter(str).length() + 2;
		}
		System.out.println("&size=" + base + "&");*/
	
	String checkImgRequest=	request.getParameter("reqType");
	int length=250;
	int breadth=250;
	if(checkImgRequest!=null && checkImgRequest.length()>0){
		length=600;breadth=600;
	}
		PrintWriter pw = response.getWriter();
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		System.out.println("::::::::::::UploaderServlet::::::::::=");
		if (isMultipart) {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			String returnUrl = "";
			try {
				List items = upload.parseRequest(request);
				Iterator iterator = items.iterator();
				while (iterator.hasNext()) {
					FileItem item = (FileItem) iterator.next();
					if (!item.isFormField()) {
						String fileName = item.getName();
						String ext=fileName.substring(fileName.lastIndexOf("."),fileName.length());
						String time = String.valueOf(System.currentTimeMillis()).substring(6);
						fileName = time+ext;

						String root = getServletContext().getRealPath("/");

						System.out.println("root::::::"+root);
						//File path = new File(root + "/uploads");
						String filePath=Utility.getValueOfPropByKey("ques_imagesRootPath");
						System.out.println("fff "+filePath);
						File path = new File(filePath);

						if (!path.exists()) {
							boolean status = path.mkdirs();
						}

						File uploadedFile = new File(path + "/" + fileName);
						System.out.println(uploadedFile.length()+" uploadedFile "+uploadedFile.getAbsolutePath());
						item.write(uploadedFile);
						//ImageResize.resizePicture(filePath+ fileName);
						File f = new File(filePath+ fileName);
						BufferedImage  src = ImageIO.read(f);
						/*if(src.getHeight()>400 || src.getWidth()>400)
						{*/
							ResampleOp resampleOp = new ResampleOp(length,breadth);
							// resampleOp.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.VerySharp);
							BufferedImage rescaled = resampleOp.filter(src, null);
							ImageIO.write(rescaled, "JPG", new File(filePath+ fileName));
						//}

						//returnUrl = item.getSize()+"###"+Utility.getBaseURL(request)+"uploads/"+fileName+"###"+fileName;
						//returnUrl = item.getSize()+"###"+Utility.showFile(fileName,root)+"###"+fileName;
						returnUrl = item.getSize()+"###"+"showImage?image=ques_images/"+fileName+"###"+fileName;
					}
				}	
			} catch (FileUploadException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("returnUrl:::"+returnUrl);
			pw.println(returnUrl);
		}
	}
}
