package tm.servlet;

import java.io.FileInputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.core.InjectParam;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.ApplitrackDistricts;
import tm.bean.master.DomainMaster;
import tm.bean.master.StatusMaster;
import tm.dao.ApplitrackDistrictsDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.utility.Utility;

public class WorkThreadServlet extends HttpServlet 
{
	
	@InjectParam
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@InjectParam
	private ApplitrackDistrictsDAO applitrackDistrictsDAO;
	
	private static final long serialVersionUID = 1L;
	public void doGet(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException 
	{
		
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		
	}
	
	public static String teacherRootPath = "";
	public static String schoolRootPath = "";
	public static String tmRootPath = "";
	public static String districtRootPath = "";
	public static String authKey = "";
	public static List<StatusMaster> statusMasters = null;
	public static List<DomainMaster> domainMastersPDRList = new ArrayList<DomainMaster>(); 
	public static List<ApplitrackDistricts> applitrackDistricts = null;
	public static Map<String,ApplitrackDistricts> applitrackDisMap = new HashMap<String, ApplitrackDistricts>();
	public static Map<String,StatusMaster> statusMap = new HashMap<String, StatusMaster>(); 
	public static Map<Integer,StatusMaster> statusIdMap = new HashMap<Integer, StatusMaster>(); 
	public static ServletContext context;
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		String resourceFile;
		
		Properties properties;
		
		context = getServletContext();

//Sandeep 14-10-15		
		try {
			ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
			applitrackDistrictsDAO = (ApplitrackDistrictsDAO)context0.getBean("applitrackDistrictsDAO");
			headQuarterMasterDAO = (HeadQuarterMasterDAO)context0.getBean("headQuarterMasterDAO");
			SessionFactory sessionFactory=headQuarterMasterDAO.getSessionFactory();
			sessionFactory.openSession().beginTransaction();
			HeadQuarterMaster headQuarterMaster =headQuarterMasterDAO.findById(1, false, false);
			StatusMasterDAO stausMasterDAO = (StatusMasterDAO)context0.getBean("statusMasterDAO");
			statusMasters = stausMasterDAO.findAll();
			for(StatusMaster statusMaster:statusMasters)
			{
				statusMap.put(statusMaster.getStatusShortName().trim(), statusMaster);
				statusIdMap.put(statusMaster.getStatusId(), statusMaster);
			}
			Criterion criteria	 =	Restrictions.eq("headQuarterId",1);
			applitrackDistricts = applitrackDistrictsDAO.findByCriteria(criteria);
			System.out.println("applitrackDistricts size-------------    "+applitrackDistricts);
			for(ApplitrackDistricts applitrackDist:applitrackDistricts)
			{
				applitrackDisMap.put(applitrackDist.getEventType(), applitrackDist);
			}
			
			DomainMasterDAO domainMasterDAO = (DomainMasterDAO)context0.getBean("domainmasterdao");
			domainMastersPDRList = domainMasterDAO.getDomainsListInReport();
			System.out.println("domainMastersPDRList::: "+domainMastersPDRList.size());
			
			sessionFactory.openSession().close();
			if(headQuarterMaster != null)
			authKey = headQuarterMaster.getAuthKey();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
//Sandeep 14-10-15		
		
		try 
		{
			resourceFile = context.getInitParameter("resourceFile");
		    resourceFile = context.getRealPath(resourceFile);
			properties = new Properties();
	        properties.load(new FileInputStream(resourceFile));
	        
			String tempInitialValue = "";
				
			tempInitialValue = properties.getProperty("teacherRootPath");
			if(tempInitialValue != null)
				teacherRootPath=tempInitialValue.trim();
			
			tempInitialValue = properties.getProperty("schoolRootPath");
			if(tempInitialValue != null)
				schoolRootPath=tempInitialValue.trim();
			
			tempInitialValue = properties.getProperty("tmRootPath");
			if (tempInitialValue != null)
				tmRootPath = tempInitialValue.trim();
			
			tempInitialValue = properties.getProperty("districtRootPath");
			if (tempInitialValue != null)
				districtRootPath = tempInitialValue.trim();
			
		} 
		catch (Exception ee) 
		{
			ee.printStackTrace();
		}
		
		System.out.println("districtRootPath="+districtRootPath);
	}
	
	public static void loadStatusMasters()
	{
		try {
			ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
			StatusMasterDAO stausMasterDAO = (StatusMasterDAO)context0.getBean("statusMasterDAO");
			statusMasters = stausMasterDAO.findAll();
			for(StatusMaster statusMaster:statusMasters)
			{
				statusMap.put(statusMaster.getStatusShortName().trim(), statusMaster);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
