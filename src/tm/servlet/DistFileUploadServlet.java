package tm.servlet;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.master.DistrictAttachment;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictNotes;
import tm.bean.user.UserMaster;
import tm.dao.master.DistrictAttachmentDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.services.DistrictAjax;
import tm.services.FeedbackAndSupportService;
import tm.services.clamav.ClamAVUtil;
import tm.utility.Utility;


public class DistFileUploadServlet extends HttpServlet 
{
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private DistrictAttachmentDAO districtAttachmentDAO;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{	System.out.println("DistFileUploadServlet");
		PrintWriter pw = response.getWriter();
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		HttpSession session = request.getSession();
		UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
		System.out.println("userSession::::::::"+userSession.getUserId());
		Integer userId = userSession.getUserId();
		String districtId = "";
		String headQuarterId = "";
		String branchId = "";
		String msg="";
			if (isMultipart){
				FileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				String fileName="";
				String dateString = "";
				try {
					List items = upload.parseRequest(request);
					Iterator iterator = items.iterator();
					File path = null;
					while (iterator.hasNext()) {
						FileItem item = (FileItem) iterator.next();
						if(item.getFieldName().equals("districtId") && (item.getString()!=null && item.getString().length()>0)){
							districtId =item.getString();
						}
						if(item.getFieldName().equals("hqId") && (item.getString()!=null && item.getString().length()>0)){
							headQuarterId =item.getString();
						}
						if(item.getFieldName().equals("branchId") && (item.getString()!=null && item.getString().length()>0)){
							branchId =item.getString();
						}
						if(item.getFieldName().equals("dateString")){
							dateString =item.getString();
						}
						
						if (!item.isFormField())
						{
							String root = getServletContext().getRealPath("/");
							if(headQuarterId!="" && branchId.length()==0){
								path=new File(Utility.getValueOfPropByKey("headQuarterRootPath")+headQuarterId+"/HQAttachmentFile");
							}
							else if(branchId!=""){
								path=new File(Utility.getValueOfPropByKey("branchRootPath")+branchId+"/BranchAttachmentFile");
							}
							else
							path = new File(Utility.getValueOfPropByKey("districtRootPath")+districtId+"/DistAttachmentFile");
							if (!path.exists()) {
								boolean status = path.mkdirs();
							}
							String myFullFileName = item.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
							int startIndex = myFullFileName.lastIndexOf(slashType);
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
							String ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
							String strFileName=myFullFileName.substring(0,myFullFileName.lastIndexOf("."));
							//fileName=strFileName+Utility.getDateTime()+ext;
							fileName=strFileName+dateString+ext;
							File uploadedFile = new File(path + "/" + fileName);
							item.write(uploadedFile);
							msg = ClamAVUtil.scanAndRemove(uploadedFile.toString());
						}
					}
				} catch (FileUploadException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				if(!msg.equals(""))
				{
					response.setContentType("text/html");
					pw.print(msg);
					pw.close();
				}
				System.out.println("districtId212==="+districtId);
				/*
				 * 	Save Recortd Into Database
				 * */
				
			}
	   }
  }
