package tm.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.utility.ImageResize;
import tm.utility.Utility;

public class DistAttachmentUploadServlet extends HttpServlet 
{
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		
		response.setContentType("text/html");
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
	    List uploadedItems = null;
		FileItem fileItem = null;
		String ext="",filePath="";
		String JobOrderType="";
		try{
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			String fileName="";
	
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if(fileItem.getFieldName().equals("disHiddenId")){
					filePath=Utility.getValueOfPropByKey("districtRootPath")+fileItem.getString()+"/DistAttachmentFile";
				} else if(fileItem.getFieldName().equals("districtORSchoolNameAdmin")){
					filePath=Utility.getValueOfPropByKey("districtRootPath")+fileItem.getString()+"/DistAttachmentFile";
				}
				File f=new File(filePath);
				if(!f.exists())
					 f.mkdirs();
				if (fileItem.isFormField() == false) 
				{
					if (fileItem.getSize() > 0)	
					{
						System.out.println("::::::::::::::::::::::::::::::::::::::::::::::"+filePath);
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						String strFileName=myFullFileName.substring(0,myFullFileName.indexOf("."));
						fileName=strFileName+"_"+Utility.getDateTime()+ext;
						uploadedFile = new File(filePath, fileName);
						fileItem.write(uploadedFile);
						System.out.println("job desc file >> "+filePath+"/"+fileName);
					}
				}
				fileItem=null;
			}
			PrintWriter pw = response.getWriter();
			response.setContentType("text/html");  
			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			pw.print("window.top.saveDistFile('"+fileName+"');");
			pw.print("</script>");
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}
