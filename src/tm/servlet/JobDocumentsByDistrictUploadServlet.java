package tm.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import tm.bean.user.UserMaster;
import tm.utility.Utility;

public class JobDocumentsByDistrictUploadServlet extends HttpServlet 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8024404533016353528L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		System.out.println("JobDocumentsByDistrictUploadServlet : doGET");
		PrintWriter pw = response.getWriter();
		FileItemFactory factory = new DiskFileItemFactory();
		
		ServletFileUpload upload = new ServletFileUpload(factory);
		
		upload.setSizeMax(10485760);
		
		
		HttpSession session = request.getSession();
		PrintWriter out = response.getWriter();
	    
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		String currentFileName=request.getParameter("fileName");
		System.out.println(" fileName from action :: "+currentFileName);
		
	    List uploadedItems = null;
		FileItem fileItem = null;
		String dateTime=Utility.getDateTime();
		String filePath = "";
		try {
			uploadedItems = upload.parseRequest(request);
		} catch (FileUploadException e1) {
			e1.printStackTrace();
		}
		Iterator j = uploadedItems.iterator();
		String districtId="";
		while (j.hasNext())	
		{
			fileItem = (FileItem) j.next();
			if(fileItem.getFieldName().equals("hiddenIdDistrict")){
				districtId=fileItem.getString();
				System.out.println("districtId   "+districtId);
				
            }
		}
		
		filePath = Utility.getValueOfPropByKey("districtRootPath")+districtId+"/JobDocuments/";
		File f=new File(filePath);
		if(!f.exists())
			 f.mkdirs();
		String ext="";
		try 
		{
			File fin = new File(filePath);
			for (File file : fin.listFiles()) 
			{
				if((!file.getName().equals("Thumbs.db") && (file.getName().equals(currentFileName)) ))
				{
					file.delete();
				}
			}   
			
			//uploadedItems = upload.parseRequest(request);
			Iterator i = uploadedItems.iterator();
			String fileName="";
			String fullFileName="";
			int spacePost	=	-1;
	
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if (fileItem.isFormField() == false) 
				{
					if (fileItem.getSize() > 0)	
					{
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("/") > 0) ? "/" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						if(myFullFileName.length()>125)
						{
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.lastIndexOf("."));
							ext=myFullFileName.substring(myFullFileName.lastIndexOf("."),myFullFileName.length());
							myFileName = myFullFileName.substring(startIndex + 1, 120);
							spacePost=myFileName.lastIndexOf(" ");
							if(spacePost!=-1)
							{
								myFileName = myFullFileName.substring(startIndex + 1, spacePost);
							}
							myFileName=myFileName.replaceAll("[^\\w\\s]", "");
							fullFileName="JobDocuments_"+dateTime+""+ext;
						}
						else
						{
							myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.lastIndexOf("."));
							ext=myFullFileName.substring(myFullFileName.lastIndexOf("."),myFullFileName.length());
							myFileName=myFileName.replaceAll("[^\\w\\s]", "");
							fullFileName="JobDocuments_"+dateTime+""+ext;
							
						}
						uploadedFile = new File(filePath, fullFileName);
						fileItem.write(uploadedFile);
					}
				}
				
			}
			fileItem=null;		
			response.setContentType("text/html");
			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			//pw.print("window.top.getUploadedFileName(\""+fullFileName+"\");");
			pw.print("window.top.viewUploadedFiles(\""+fullFileName+"\");");	
			pw.print("</script>");			
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

}
