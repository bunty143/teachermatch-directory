package tm.servlet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.utility.Utility;

public class DownloadCSV extends HttpServlet {
	
	private static final long serialVersionUID = 5290491248329690526L;

	
	@Autowired
	private RawDataForDomainDAO rawDataForDomainDAO;
	
	@Override
	public void service(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		System.out.println(" ================== DownloadCSV ============== ");
		
		/*response.setHeader("Content-Disposition", "attachment; filename=\"userDirectory.csv\"");
        try
        {
            OutputStream outputStream = response.getOutputStream();
            String outputResult = "xxxx, yyyy, zzzz, aaaa, bbbb, ccccc, dddd, eeee, ffff, gggg\n";
            outputStream.write(outputResult.getBytes());
            outputStream.flush();
            outputStream.close();
        }
        catch(Exception e)
        {
            System.out.println(e.toString());
        }*/
		
		try
		{
			//response.setHeader("Content-Disposition", "attachment; filename="+fileName);
			
			HttpSession session = request.getSession(false);
			UserMaster userMaster = null;
			DistrictMaster districtMaster = null;
			
			  OutputStream outputStream = response.getOutputStream();
			
			if(session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
			}
			String fileName=null;
			
			ServletContext context = request.getSession().getServletContext();
			ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
			rawDataForDomainDAO = (RawDataForDomainDAO)context0.getBean("rawDataForDomainDAO");
			
			
			
			
			
			String startDate = request.getParameter("fromDate");
			String endDate = request.getParameter("stoDate");
			String searchDistrictId = request.getParameter("districtId");
			String pageNo = request.getParameter("page");
			String noOfRow = request.getParameter("noOfRows");
			String sortOrderType = request.getParameter("sortOrderType");
			String sortOrder = request.getParameter("sortOrderStr");
			int districtId =0;
			System.out.println("Start date==="+startDate+"End date========"+endDate);
			
			if(userMaster.getDistrictId()!=null)
				districtId = userMaster.getDistrictId().getDistrictId();
			else
			{
				if(searchDistrictId!=null)
					districtId = Integer.parseInt(searchDistrictId);
			}
			
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			int totalRecords=0;
			//------------------------------------
			
			String  sortOrderStrVal		=	null;
			String sortOrderFieldName	=	"lastName";
			
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	"asc" ;//Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	"desc";//Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	"asc";//Order.asc(sortOrderFieldName);
			}
			
			List lstRawData = new ArrayList();
			
			lstRawData = rawDataForDomainDAO.findTeacherEpiNormScoreForReport(start, noOfRowInPage, sortOrderStrVal, false,sortOrderFieldName,startDate,endDate,districtId);
		
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			String basePath = request.getSession().getServletContext().getRealPath ("/")+"/wecanexport";
			
			fileName ="wecanexport"+time+".csv";
			
			response.setHeader("Content-Type", "text/csv");
			response.setHeader("Content-Disposition", "attachment; filename="+fileName);
			
			System.out.println(" basePath :: "+basePath);
			
			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);
			
			file = new File(basePath+"/"+fileName);
			
			//Delimiter used in CSV file
			final String COMMA_DELIMITER = ",";
	//		final String NEW_LINE_SEPARATOR = "\n";

			//CSV file header
		//	final String FILE_HEADER = "CANDIDATE LAST NAME,CANDIDATE FIRST NAME,TEACHER EMAIL,EPI DATE,EPI NORM SCORE";

		//	FileWriter fileWriter = null;

		//	fileWriter = new FileWriter(file);

			//Write the CSV file header
		//	fileWriter.append(FILE_HEADER.toString());

			//Add a new line separator after the header
		//	fileWriter.append(NEW_LINE_SEPARATOR);

			PrintWriter writer = new PrintWriter(outputStream);
			
			writer.write("CANDIDATE LAST NAME");
			writer.write(COMMA_DELIMITER);
			writer.write("CANDIDATE FIRST NAME");
			writer.write(COMMA_DELIMITER);
			writer.write("TEACHER EMAIL");
			writer.write(COMMA_DELIMITER);
			writer.write("EPI DATE");
			writer.write(COMMA_DELIMITER);
			writer.write("EPI NORM SCORE");
			
			writer.println();
			
			if(lstRawData.size()==0)
			{
				writer.write("Record Not Found");
			}
			
			
			StringBuilder csvFileData = new StringBuilder();
			
			
			if(lstRawData.size()>0){
				
				 for(Object oo:lstRawData) 
				 {
					 	Object obj[] = (Object[])oo;
						String lastName = "";
						String firstname = "";
						String email = "";
						String epiDate = "";
						Integer normScore = 0;
						
						if(obj[0]!=null)
							lastName = (String) obj[0];
						
						if(obj[1]!=null)
							firstname = (String) obj[1];;
							
						if(obj[2]!=null)
							email = (String) obj[2];
								
						if(obj[3]!=null)
							epiDate = (String) obj[3];
									
						if(obj[4]!=null)
							normScore= Integer.parseInt(String.valueOf(obj[4]));
						
						if(lastName!=null)
							writer.write("\""+lastName+"\"");
						else
							writer.write(lastName);
						
						writer.write(COMMA_DELIMITER);
						
						if(firstname!=null)
							writer.write("\""+firstname+"\"");
						else
							writer.write(firstname);
						
						writer.write(COMMA_DELIMITER);
						
						if(email!=null)
							writer.write(("\""+email+"\""));
						else
							writer.write(email);
						
						writer.write(COMMA_DELIMITER);
						
						if(epiDate!=null)
							writer.write(("\""+epiDate+"\""));
						else
							writer.write(epiDate);
						
						writer.write(COMMA_DELIMITER);
						
						if(normScore!=null)
							writer.write(("\""+normScore+"\""));
						else
							writer.write((""+normScore));
						
						writer.println();
					
				 }
				 writer.flush();
					writer.close();
				}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
}