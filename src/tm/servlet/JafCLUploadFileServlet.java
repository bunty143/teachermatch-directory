package tm.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import tm.bean.TeacherDetail;
import tm.utility.Utility;

public class JafCLUploadFileServlet extends HttpServlet {

	private static final long serialVersionUID = -8865872173776799095L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("Calling Servlet File Upload for JafCLUploadFileServlet");

		PrintWriter pw = response.getWriter();
		response.setContentType("text/html");  

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();			
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		
		int teacherId=0;
		if(teacherDetail!=null){
			teacherId=teacherDetail.getTeacherId();
		}
		
		System.out.println("teacherId "+teacherId);
		
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List uploadedItems = null;
		FileItem fileItem = null;
		String ext="",filePath="";
		try{
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			String fileName="";
			String dateTime=Utility.getDateTime();
			
			String clJobId="";
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				
				if(fileItem.getFieldName().equals("clJobId"))
				{
					clJobId=fileItem.getString();
				}
				
			}
			System.out.println("teacherId "+teacherId +" clJobId "+clJobId);	
			
			if(teacherId > 0 && clJobId!=null && !clJobId.equals("") && !clJobId.equals("0"))
			{
				filePath=Utility.getValueOfPropByKey("teacherJobCLRootPath")+teacherId+"_"+clJobId+"/";
				System.out.println("Upload FilePath "+filePath);

				File f=new File(filePath);
				if(!f.exists())
					f.mkdirs();

				if (fileItem.isFormField() == false)
				{
					if (fileItem.getSize() > 0)
					{
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						fileName="teacherJobCL_"+dateTime+ext;
						uploadedFile = new File(filePath, fileName);
						fileItem.write(uploadedFile);
					}
				}
				fileItem=null;				
			}
			System.out.println("Saved FileName "+fileName);
			
			response.setContentType("text/html");  
			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			pw.print("window.top.jafUploadCLFileByServlet('"+fileName+"');");
			pw.print("</script>");
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	
	}

}
