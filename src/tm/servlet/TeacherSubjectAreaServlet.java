package tm.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import tm.bean.TeacherDetail;
import tm.utility.Utility;

public class TeacherSubjectAreaServlet extends HttpServlet 
{
	private static final long serialVersionUID = 119179350098396555L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		System.out.println("Calling Servlet File Upload for TeacherSubjectAreaServlet");

		PrintWriter pw = response.getWriter();
		response.setContentType("text/html");  

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();			
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		
		int teacherId=0;
		if(teacherDetail!=null){
			teacherId=teacherDetail.getTeacherId();
		}
		
		System.out.println("teacherId "+teacherId);
		
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List uploadedItems = null;
		FileItem fileItem = null;
		String ext="",filePath="";
		try{
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			String fileName="";
			String dateTime=Utility.getDateTime();
			
			String sbtsource_subArea="";
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				
				if(fileItem.getFieldName().equals("sbtsource_subArea")){
					sbtsource_subArea=fileItem.getString();
				}
				
			}
			System.out.println("teacherId:::::::::::"+teacherId +" sbtsource_subArea "+sbtsource_subArea);	
			if(teacherId > 0 )
			{
				filePath=Utility.getValueOfPropByKey("teacherRootPath")+teacherId+"/";
				System.out.println("filePath:::::::"+filePath);

				File f=new File(filePath);
				if(!f.exists())
					f.mkdirs();

				if (fileItem.isFormField() == false){
					if (fileItem.getSize() > 0){
						File uploadedFile = null; 
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						fileName="subjectArea_"+dateTime+ext;
						uploadedFile = new File(filePath, fileName);
						fileItem.write(uploadedFile);
					}
				}
				fileItem=null;				
			}
			System.out.println("fileName::::::"+fileName);
			response.setContentType("text/html");  
			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			pw.print("window.top.saveSubjectAreasByServlet('"+sbtsource_subArea+"','"+fileName+"');");
			pw.print("</script>");
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}
