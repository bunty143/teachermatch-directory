package tm.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.services.clamav.ClamAVUtil;
import tm.utility.ImageResize;
import tm.utility.Utility;

public class FileUploadServletForTagsIcon extends HttpServlet 
{
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		
		PrintWriter pw = response.getWriter();
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(10485760);
		HttpSession session = request.getSession();
		PrintWriter out = response.getWriter();
		Integer districtId = (Integer) session.getAttribute("districtId");
		String currentFileName=request.getParameter("f");
		
		String msg="";
	    List uploadedItems = null;
		FileItem fileItem = null;
		String fileName="";
		String fullFileName="";
		int spacePost	=	-1;
		String filePath=null;
		File f=null;
		if(districtId!=null)
		{
			if(request.getSession().getAttribute("previousTagsIconFile")!=null){
				String previousFileName=request.getSession().getAttribute("previousTagsIconFile").toString();
				if(previousFileName!=null && !previousFileName.equals("")){
					String pFileName=Utility.getValueOfPropByKey("districtRootPath")+districtId+"/tags/"+previousFileName;
					File pFile=new File(pFileName);
					if(pFile.exists()){
						pFile.delete();
						//System.out.println("previous file deleted....");
					}
					session.removeAttribute("previousTagsIconFile");
				}
			}
			filePath =Utility.getValueOfPropByKey("districtRootPath")+districtId+"/tags/";
		}else{
			if(request.getSession().getAttribute("previousTagsIconFile")!=null){
				String previousFileName=request.getSession().getAttribute("previousTagsIconFile").toString();
				if(previousFileName!=null && !previousFileName.equals("")){
					String pFileName=Utility.getValueOfPropByKey("rootPath")+"tags/"+previousFileName;
					File pFile=new File(pFileName);
					if(pFile.exists()){
						pFile.delete();
						//System.out.println("previous file deleted....");
					}
					session.removeAttribute("previousTagsIconFile");
				}
			}
			filePath =Utility.getValueOfPropByKey("rootPath")+"/tags/";
		}	
			
			f=new File(filePath);
			if(!f.exists())
				 f.mkdirs();
			String ext="";
			try 
			{
				uploadedItems = upload.parseRequest(request);
				Iterator i = uploadedItems.iterator();
				while (i.hasNext())	
				{
					fileItem = (FileItem) i.next();
					if (fileItem.isFormField() == false) 
					{
						if (fileItem.getSize() > 0)	
						{
							File uploadedFile = null; 
							String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("/") > 0) ? "/" : "/";
							int startIndex = myFullFileName.lastIndexOf(slashType);
							if(myFullFileName.length()>125)
							{
								myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.lastIndexOf("."));
								ext=myFullFileName.substring(myFullFileName.lastIndexOf("."),myFullFileName.length());
								myFileName = myFullFileName.substring(startIndex + 1, 120);
								spacePost=myFileName.lastIndexOf(" ");
								if(spacePost!=-1)
								{
									myFileName = myFullFileName.substring(startIndex + 1, spacePost);
								}
								fullFileName=myFileName+"_"+Utility.getDateTime()+ext;
							}else{
								myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.lastIndexOf("."));
								ext=myFullFileName.substring(myFullFileName.lastIndexOf("."),myFullFileName.length());
								fullFileName=myFileName+"_"+Utility.getDateTime()+ext;
							}
							uploadedFile = new File(filePath, fullFileName);
							fileItem.write(uploadedFile);
							ImageResize.resizeTag(filePath+ fullFileName);
							msg = ClamAVUtil.scanAndRemove(uploadedFile.toString());
						}
					}
					fileItem=null;				
				}
				response.setContentType("text/html");
				pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
				if(msg.equals(""))
					pw.print("window.top.saveTags(\""+fullFileName+"\");");
				else
					pw.print("window.top.fileContainsVirusDiv('"+msg+"')");
				pw.print("</script>");			
			}catch (FileUploadException e) 
			{
				e.printStackTrace();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
}
