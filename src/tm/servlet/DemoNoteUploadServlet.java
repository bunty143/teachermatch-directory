package tm.servlet;

import java.io.File;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.user.UserMaster;
import tm.dao.TeacherPersonalInfoDAO;
import tm.services.EmailerService;
import tm.services.FeedbackAndSupportService;
import tm.services.social.SocialService;
import tm.utility.Utility;

public class DemoNoteUploadServlet extends HttpServlet 
{

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		System.out.println("Calling DemoNoteUploadServlet ...");

		PrintWriter pw = response.getWriter();
		response.setContentType("text/html");  

		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List uploadedItems = null;
		FileItem fileItem = null;
		String ext="",filePath="";
		String txtNotes="";
		String file="";
		String teacherId="";
		String demoId="";
		
		
		try{
			uploadedItems = upload.parseRequest(request);
			upload.setSizeMax(10485760);
			Iterator i = uploadedItems.iterator();
			String fileName="";
			String noteDateTime=Utility.getDateTime();
			while (i.hasNext())	
			{
				fileItem = (FileItem) i.next();
				if(fileItem.getFieldName().equals("demoNoteId")){
					demoId=fileItem.getString();
				}
				
			}
				if(demoId!=null ){
					System.out.println("demoid  :"+demoId);
					System.out.println("noteDateTime  :"+noteDateTime);
				filePath=Utility.getValueOfPropByKey("demoschedulenotesRootPath")+demoId+"/";
				System.out.println("filePath :: "+filePath);

				File f=new File(filePath);
				if(!f.exists())
					f.mkdirs();
				
				if (fileItem.isFormField() == false){
					System.out.println("fileItem.getSize():: "+fileItem.getSize());
					if (fileItem.getSize() > 0){
						
						File uploadedFile = null; 
					
						String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						fileName=myFullFileName.substring(0,myFullFileName.lastIndexOf("."));
						
						int startIndex = myFullFileName.lastIndexOf(slashType);
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						
						ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						/*
						 * 5-20131127090149.txt
							demo_note20131127090149.txt
						 */
						fileName="demo_note"+noteDateTime+ext;
						uploadedFile = new File(filePath, fileName);
						fileItem.write(uploadedFile);
					}
				}
				fileItem=null;				
			}
			System.out.println("::::::::::: fileName:::"+fileName);
			
			response.setContentType("text/html");  
			pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
			pw.print("window.top.saveDemoNoteFile('"+noteDateTime+"');");
			pw.print("</script>");
		} 
		catch (FileUploadException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

}
