package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;

public class DistrictNameCompratorASC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		if(o1.getDistName()==null)
			o1.setDistName("");
		
		if(o2.getDistName()==null)
			o2.setDistName("");
		
		return o1.getDistName().toUpperCase().compareTo(o2.getDistName().toUpperCase());
	}
}
