package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;

public class InternalCandidateCompratorASC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		return (o1.getIsAffilated()-o2.getIsAffilated());
	}
}
