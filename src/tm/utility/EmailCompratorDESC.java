package tm.utility;

import java.util.Comparator;

import tm.bean.TeacherPersonalInfo;

public class EmailCompratorDESC implements Comparator<TeacherPersonalInfo> {

	public int compare(TeacherPersonalInfo o1, TeacherPersonalInfo o2) {
		return o2.getEmailAddress().toUpperCase().compareTo(o1.getEmailAddress().toUpperCase());
	}
}
