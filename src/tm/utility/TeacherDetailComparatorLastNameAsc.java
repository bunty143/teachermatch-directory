package tm.utility;

import java.util.Comparator;

import tm.bean.TeacherDetail;

public class TeacherDetailComparatorLastNameAsc implements Comparator<TeacherDetail>{

	public int compare(TeacherDetail td1, TeacherDetail td2) {
		
		return td1.gettLastName().toUpperCase().compareTo(td2.gettLastName().toUpperCase());
	}
}
