package tm.utility;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * @author Amit Chaudhary
 * @version 1.0, 08/06/2015
 */
public class AESEncryption {
	
	// aesKeyValue
	private static String aesKeyValue = Utility.getValueOfPropByKey("aesKeyValue");
	
	// Type Of Algorithm
	private static String algorithm = "AES";
	
	// Your Key
	// Must have 16 digit key or 256 bit (Advanced Encryption Standard)
	private static byte[] keyValue = aesKeyValue.getBytes();

	// Performs Encryption
	public static String encrypt(String plainText) throws Exception {
		Key key = generateKey();
		Cipher chiper = Cipher.getInstance(algorithm);
		chiper.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = chiper.doFinal(plainText.getBytes());
		String encryptedValue = new BASE64Encoder().encode(encVal);
		return encryptedValue;
	}

	// Performs Decryption
	public static String decrypt(String encryptedText) throws Exception {
		Key key = generateKey();
		Cipher chiper = Cipher.getInstance(algorithm);
		chiper.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedText);
		byte[] decValue = chiper.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}

	// generateKey() is used to generate a secret key for AES algorithm
	private static Key generateKey() throws Exception {
		Key key = new SecretKeySpec(keyValue, algorithm);
		return key;
	}
}
