package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;

public class StafferNameCompratorDESC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		
		if(o1.getStafferName()==null)
			o1.setStafferName("");
		
		if(o2.getStafferName()==null)
			o2.setStafferName("");
		
		return o2.getStafferName().toUpperCase().compareTo(o1.getStafferName().toUpperCase());
	}
}
