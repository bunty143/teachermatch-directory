package tm.utility;

import java.util.Comparator;

import tm.bean.EventDetails;

public class DistrictOrSchoolNameCompratorDESC implements Comparator<EventDetails> {

	public int compare(EventDetails o1, EventDetails o2) {
		
		if(o2.getDistrictOrSchoolName()== null)
			if(o1.getDistrictOrSchoolName()== null)
				 return 0; //equal
			else
				 return -1; //null is before other strings
		
	       else // this.member != null
	    	   if(o1.getDistrictOrSchoolName()== null)
	            return 1;  // all other strings are after null
	         else
		
		return o2.getDistrictOrSchoolName().toUpperCase().compareTo(o1.getDistrictOrSchoolName().toUpperCase());
	}
}
