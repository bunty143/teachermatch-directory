package tm.utility;

import java.util.Comparator;

import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;

public class NormScoreforHistoryTableCompratorASC implements Comparator<TeacherStatusHistoryForJob> 
{
	public int compare(TeacherStatusHistoryForJob o1, TeacherStatusHistoryForJob o2) {
		Double cScore1 =o1.getNormScore();
		Double cScore2 =o2.getNormScore();		
		int c = cScore1.compareTo(cScore2);
		return c;
	}

	
}
