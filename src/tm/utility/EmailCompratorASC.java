package tm.utility;

import java.util.Comparator;

import tm.bean.TeacherPersonalInfo;

public class EmailCompratorASC implements Comparator<TeacherPersonalInfo> {

	public int compare(TeacherPersonalInfo o1, TeacherPersonalInfo o2) {
		return o1.getEmailAddress().toUpperCase().compareTo(o2.getEmailAddress().toUpperCase());
	}
}
