package tm.utility;

import java.util.Comparator;

import tm.bean.JobOrder;

public class JobOrderSchoolNameCompratorDES implements Comparator<JobOrder> {

	public int compare(JobOrder o1, JobOrder o2) {
		return o2.getSchoolName().toUpperCase().compareTo(o1.getSchoolName().toUpperCase());
	}
}
