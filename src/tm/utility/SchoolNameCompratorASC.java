package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;

public class SchoolNameCompratorASC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		if(o1.getSchoolName()==null)
			o1.setSchoolName("");
		
		if(o2.getSchoolName()==null)
			o2.setSchoolName("");
		
		return o1.getSchoolName().toUpperCase().compareTo(o2.getSchoolName().toUpperCase());
	}
}
