package tm.utility;

import java.util.*;
import java.util.logging.Logger;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
* @author : Vishwanath Kumar
* Static convenience methods for common tasks, which eliminate code duplication.
*/
public final class Util {

   /**
   * Return <tt>true</tt> only if <tt>aText</tt> is not null,
   * and is not empty after trimming. (Trimming removes both
   * leading/trailing whitespace and ASCII control characters.)
   *
   * <P> For checking argument validity, {@link Args#checkForContent} should 
   * be used instead of this method.
   *
   * @param aText possibly-null.
   */
   public static boolean textHasContent(String aText) {
     return (aText != null) && (aText.trim().length() > 0);
   }

  /**
  * Return <tt>true</tt> only if <tt>aNumber</tt> is in the range 
  * <tt>aLow..aHigh</tt> (inclusive).
  *
  * <P> For checking argument validity, {@link Args#checkForRange} should 
  * be used instead of this method.
  *
  * @param aLow less than or equal to <tt>aHigh</tt>.
  */
  static public boolean isInRange( int aNumber, int aLow, int aHigh ){
    if (aLow > aHigh) {
      throw new IllegalArgumentException("Low is greater than High.");
    }
    return (aLow <= aNumber && aNumber <= aHigh);
  }

  /**
  * Return <tt>true</tt> if <tt>aBoolean</tt> equals "true" (ignore case), or 
  * <tt>false</tt> if <tt>aBoolean</tt> equals "false" (ignore case).
  *
  *<P>Note that this behavior is different from that of <tt>Boolean.getValue</tt>.
  *
  * @param aBoolean equals "true" or "false" (not case-sensitive).
  */
  public static Boolean parseBoolean(String aBoolean){
    if ( aBoolean.equalsIgnoreCase("true") ) {
      return Boolean.TRUE;
    }
    else if ( aBoolean.equalsIgnoreCase("false") ) {
      return Boolean.FALSE;
    }
    else {
      throw new IllegalArgumentException("Cannot parse into Boolean: " + aBoolean);
    }
  }
  
  /**
  * Convert a <tt>Collection</tt> represented in the form 
  * of <tt>AbstractCollection.toString</tt> into a <tt>List</tt> of 
  * <tt>String</tt> objects.
  * 
  * <P>Intended for use as an aid in parsing collections of objects which 
  * were stored in their <tt>toString</tt> form. 
  * This method will not parse such <tt>String</tt>s into the original objects, 
  * but will aid in doing so by tokenizing it into its parts. 
  *
  * @param aText has format of <tt>AbstractCollection.toString</tt>
  * @return <tt>String</tt> objects.
  */
  public static final List<String> getListFromString(String aText){
    if ( aText == null) throw new IllegalArgumentException("Text must not be null.");
    List<String> result = new ArrayList<String>();
    StringTokenizer parser = new StringTokenizer( aText, "[,] " );
    while ( parser.hasMoreTokens() ) {
      result.add( parser.nextToken() );
    }
    return result;
  }

  /**
  * Return <tt>true</tt> only if <tt>aMoney</tt> equals 
  * {@link Consts#ZERO_MONEY} or {@link Consts#ZERO_MONEY_WITH_DECIMAL}.
  */
  public static boolean isZeroMoney( BigDecimal aMoney ){
    return 
      aMoney.equals(Consts.ZERO_MONEY) || 
      aMoney.equals(Consts.ZERO_MONEY_WITH_DECIMAL)
    ;
  }
  
  /**
  * Return a {@link Logger} whose name follows a specific naming convention.
  *
  * <P>The conventional logger names are taken as   
  * <tt>aClass.getPackage().getName()</tt>.
  * 
  * <P>Logger names appearing in the <tt>logging.properties</tt> config file
  * must match the names returned by this method.
  */
  public static Logger getLogger(Class<?> aClass){
     return Logger.getLogger(aClass.getPackage().getName());  
   }
  

//===================Email Validation Method
public static boolean isEmailValid(String email){  
	   boolean isValid = false;  
	 
	   String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";  
	   CharSequence inputStr = email;  
	   //Make the comparison case-insensitive.  
	   Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);  
	   Matcher matcher = pattern.matcher(inputStr);  
	   if(matcher.matches()){  
	   isValid = true;  
	   }  
	   return isValid;  
	   }


	public static boolean validateDate(String dateStr, boolean allowPast)
	{
		String formatStr = "dd/MM/yyyy";
		if (formatStr == null) return false; // or throw some kinda exception, possibly a InvalidArgumentException
		SimpleDateFormat df = new SimpleDateFormat(formatStr);
		Date testDate = null;
		try
		{
			testDate = df.parse(dateStr);
		}
		catch (ParseException e)
		{
			// invalid date format
			return false;
		}
		if (!allowPast)
		{
			// initialise the calendar to midnight to prevent 
			// the current day from being rejected
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			if (cal.getTime().after(testDate)) return false;
		}
		// now test for legal values of parameters
		if (!df.format(testDate).equals(dateStr)) return false;
		return true;
	}
	
	/*
	 * 
	 * @Purpose To validate 24 hours format time
	 * @Date 
	 */
	
	public static boolean validate24HoursFormatTime(final String time){
		Pattern pattern;
		Matcher matcher;

		final String TIME24HOURS_PATTERN = "([01]?[0-9]|2[0-3]):[0-5][0-9]";

		pattern = Pattern.compile(TIME24HOURS_PATTERN);
		matcher = pattern.matcher(time);
		
		return matcher.matches();

	}
	
	public static String getCurrentDate()
	{
		String formatStr = "dd/MM/yyyy";
		SimpleDateFormat df = new SimpleDateFormat(formatStr);
		java.util.Date testDate = new java.util.Date();
		String currentDate = "";
		currentDate = df.format(testDate);
		
		return currentDate;
	}
	
	public static String getCurrentTime()
	{
		String formatStr = "HH:mm";
		SimpleDateFormat df = new SimpleDateFormat(formatStr);
		java.util.Date testDate = new java.util.Date();
		String currentTime = "";
		currentTime = df.format(testDate);
		
		return currentTime;
	}
	public static String  getFilePath(String key){
		
		String str="/home/vishwanath/public_html/fileuploaded.properties";  // for online
		//String str="c:\\fileuploaded.properties";
		
		String val="";
		try{	
			BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
			Properties pro = new Properties();
			File f = new File(str);
			if(!f.exists()){
				System.out.println("File not found!");
			}
			else{
				FileInputStream in = new FileInputStream(f);
				pro.load(in);
				val=pro.getProperty(key);
			}
			
		}
		catch(IOException e){
		System.out.println(e.getMessage());
		}
		System.out.println("path get by kenutil : "+val);
		return val;
	}
	
	public static String getRealString(String strText){
		 String  sc[]={" ","&"};
		 String rc[]={"%20","%26"};
		 for(int i=0;i<sc.length;i++){
		     strText = strText.replace(sc[i], rc[i]); 
		 } 
		    return strText;
		}
}