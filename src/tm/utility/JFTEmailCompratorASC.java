package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;
import tm.bean.TeacherPersonalInfo;

public class JFTEmailCompratorASC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		return o1.getEmailId().toUpperCase().compareTo(o2.getEmailId().toUpperCase());
	}
}
