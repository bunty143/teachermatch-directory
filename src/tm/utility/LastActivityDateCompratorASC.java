package tm.utility;

import java.util.Comparator;
import java.util.Date;

import tm.bean.JobForTeacher;

public class LastActivityDateCompratorASC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		if( o2.getLastActivityDate()!=null && o1.getLastActivityDate()!=null)
			 return o1.getLastActivityDate().compareTo(o2.getLastActivityDate());
		
		else if( o2.getLastActivityDate()!=null && o1.getLastActivityDate()==null)
			return (new Date(0)).compareTo(o2.getLastActivityDate());
		
		else if( o2.getLastActivityDate()==null && o1.getLastActivityDate()!=null)
			return o1.getLastActivityDate().compareTo(new Date(0));
		else return-1;
	}
}
