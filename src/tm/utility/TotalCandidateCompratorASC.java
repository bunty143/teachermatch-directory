package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;
import tm.bean.TeacherPersonalInfo;

public class TotalCandidateCompratorASC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		return o1.getTotalCandidate().compareTo(o2.getTotalCandidate());
	}
}
