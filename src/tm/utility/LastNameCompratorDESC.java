package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;
import tm.bean.TeacherPersonalInfo;

public class LastNameCompratorDESC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		return o2.getLastName().toUpperCase().compareTo(o1.getLastName().toUpperCase());
	}
}
