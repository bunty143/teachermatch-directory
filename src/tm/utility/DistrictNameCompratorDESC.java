package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;

public class DistrictNameCompratorDESC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		if(o1.getDistName()==null)
			o1.setDistName("");
		
		if(o2.getDistName()==null)
			o2.setDistName("");
		
		return o2.getDistName().toUpperCase().compareTo(o1.getDistName().toUpperCase());
	}
}
