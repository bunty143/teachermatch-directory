package tm.utility;


import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import tm.utility.Utility;



public class ImageResize 
{
	//[0] width [1] height
	public static void main(String[] args)throws Exception {
		int[] ar = getImageSizeByRatio( 526,413);
		System.out.println(ar[0]);
		System.out.println(ar[1]);
	}
	
	public static int[]  getImageSizeByRatio(int imgWidth, int imgHeigth) throws Exception 
	{	
		int[] imgval= new int[2]; 
		int thumbWidth = 0;
		int thumbHeight = 0;
		
		System.out.println(" inside getImageSizeByRatio Original imgWidth :: "+imgWidth);
		System.out.println(" inside getImageSizeByRatio Original imgWidth :: "+imgHeigth);
		
		
		thumbWidth = Integer.parseInt(Utility.getValueOfPropByKey("thumbWidth"));
		thumbHeight = Integer.parseInt(Utility.getValueOfPropByKey("thumbHeight"));
		
		System.out.println(" inside getImageSizeByRatio thumbWidth :: "+thumbWidth);
		System.out.println(" inside getImageSizeByRatio thumbHeight :: "+thumbHeight);
		
		
		if(thumbWidth>imgWidth && thumbHeight>imgHeigth){
			imgval[0] = imgWidth;
			imgval[1] = imgHeigth;
		}else{
			// Make sure the aspect ratio is maintained, so the image is not skewed
			double thumbRatio = (double) thumbWidth / (double) thumbHeight;
			
			double imageRatio = (double) imgWidth / (double) imgHeigth;
			if (thumbRatio < imageRatio) 
			{
				thumbHeight = (int) (thumbWidth / imageRatio);
			} 
			else 
			{
				thumbWidth = (int) (thumbHeight * imageRatio);
			}
			imgval[0] = thumbWidth;
			
			System.out.println(" inside getImageSizeByRatio WIDTH :: "+imgval[0]);
			
			System.out.println(" inside getImageSizeByRatio HEIGHT :: "+thumbHeight);
			
			imgval[1] = thumbHeight;
		}
		return imgval;
	}
	/*
	public static BufferedImage resizeImage_backup(BufferedImage originalImage, int type,int IMG_WIDTH,int IMG_HEIGHT)
	{
		BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
		g.dispose();
		return resizedImage;
	}
	*/
	
	public static boolean resizeImage(String imgPath)
	{
		try 
		{
			BufferedImage originalImage = ImageIO.read(new File(imgPath));
			int img_height = originalImage.getHeight();
			int img_width = originalImage.getWidth();
			System.out.println("inside resizeImage img_width Origional"+img_width);
			System.out.println("inside resizeImage img_height Origional"+img_height);			
			int []imgSize = getImageSizeByRatio(img_width, img_height);
			System.out.println("After resize img_width"+imgSize[0]);
			System.out.println("After resize img_height"+imgSize[1]);
			
			System.out.println("........ originalImage.getType().........."+originalImage.getType());
			
			System.out.println("........ BufferedImage.TYPE_INT_ARGB().........."+BufferedImage.TYPE_INT_ARGB);
			
			if(img_width!=imgSize[0] && img_height!=imgSize[1]){
				int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
				
				System.out.println("typeeeeeeeeeeeeeeeeeee..............."+type);
			    BufferedImage resizedImage = new BufferedImage(imgSize[0], imgSize[1], type); // @Ashish :: correct parameter height and width
			    Graphics2D g = resizedImage.createGraphics();
				g.drawImage(originalImage, 0, 0, imgSize[0],imgSize[1], null);// @Ashish :: correct parameter height and width
				g.dispose();
				//ImageIO.write(resizedImage, "jpg", new File(imgPath));
				ImageIO.write(resizedImage, type == BufferedImage.TYPE_INT_ARGB ? "jpg" : "png", new File(imgPath));
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return true;
	}
	
	public static int[]  getPictureSizeByRatio(int imgWidth, int imgHeigth) throws Exception 
	{	
		int[] imgval= new int[2]; 
		int thumbWidth = 0;
		int thumbHeight = 0;
		thumbWidth = Integer.parseInt(Utility.getValueOfPropByKey("pictureWidth"));
		thumbHeight = Integer.parseInt(Utility.getValueOfPropByKey("pictureHeight"));
		
		if(thumbWidth>imgWidth && thumbHeight>imgHeigth){
			imgval[0] = imgWidth;
			imgval[1] = imgHeigth;
		}else{
			// Make sure the aspect ratio is maintained, so the image is not skewed
			double thumbRatio = (double) thumbWidth / (double) thumbHeight;
			
			double imageRatio = (double) imgWidth / (double) imgHeigth;
			
			
			if (thumbRatio < imageRatio) 
			{
				thumbHeight = (int) (thumbWidth / imageRatio);
			} 
			else 
			{
				thumbWidth = (int) (thumbHeight * imageRatio);
			}
			imgval[0] = thumbWidth;
			imgval[1] = thumbHeight;
		}
		return imgval;
	}
	
	public static int [] resizePicture(String imgPath)
	{
		int []imgSize=null;
		try 
		{
			BufferedImage originalImage = ImageIO.read(new File(imgPath));
			int img_height = originalImage.getHeight();
			int img_width = originalImage.getWidth();
			System.out.println("img_width Origional"+img_width);
			System.out.println("img_height Origional"+img_height);			
			imgSize = getPictureSizeByRatio(img_width, img_height);
			System.out.println("After img_width"+imgSize[0]);
			System.out.println("After img_height"+imgSize[1]);
			if(img_width!=imgSize[0] && img_height!=imgSize[1]){
				int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
				BufferedImage resizedImage = new BufferedImage(imgSize[0], imgSize[1], type);
				Graphics2D g = resizedImage.createGraphics();
				g.drawImage(originalImage, 0, 0, imgSize[0], imgSize[1], null);
				g.dispose();
				ImageIO.write(resizedImage, "jpg", new File(imgPath));
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return imgSize;
	}

	public static int[]  getTagSizeByRatio(int imgWidth, int imgHeigth) throws Exception 
	{	
		int[] imgval= new int[2]; 
		int thumbWidth = 0;
		int thumbHeight = 0;
		thumbWidth = Integer.parseInt(Utility.getValueOfPropByKey("tagWidth"));
		thumbHeight = Integer.parseInt(Utility.getValueOfPropByKey("tagHeight"));
		
		if(thumbWidth>imgWidth && thumbHeight>imgHeigth){
			imgval[0] = imgWidth;
			imgval[1] = imgHeigth;
		}else{
			// Make sure the aspect ratio is maintained, so the image is not skewed
			double thumbRatio = (double) thumbWidth / (double) thumbHeight;
			
			double imageRatio = (double) imgWidth / (double) imgHeigth;
			
			
			if (thumbRatio < imageRatio) 
			{
				thumbHeight = (int) (thumbWidth / imageRatio);
			} 
			else 
			{
				thumbWidth = (int) (thumbHeight * imageRatio);
			}
			imgval[0] = thumbWidth;
			imgval[1] = thumbHeight;
		}
		return imgval;
	}
	
	public static int [] resizeTag(String imgPath)
	{
		int []imgSize=null;
		try 
		{
			BufferedImage originalImage = ImageIO.read(new File(imgPath));
			int img_height = originalImage.getHeight();
			int img_width = originalImage.getWidth();
			System.out.println("img_width Origional"+img_width);
			System.out.println("img_height Origional"+img_height);			
			imgSize = getTagSizeByRatio(img_width, img_height);
			System.out.println("After img_width"+imgSize[0]);
			System.out.println("After img_height"+imgSize[1]);
			if(img_width!=imgSize[0] && img_height!=imgSize[1]){
				int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
				BufferedImage resizedImage = new BufferedImage(imgSize[0], imgSize[1], type);
				Graphics2D g = resizedImage.createGraphics();
				g.drawImage(originalImage, 0, 0, imgSize[0], imgSize[1], null);
				g.dispose();
				ImageIO.write(resizedImage, "jpg", new File(imgPath));
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return imgSize;
	}
	
	public static void main1(String [] args)
	{
		try
		{
			/*BufferedImage originalImage = ImageIO.read(new File("D:/b/a.jpg"));

			int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
			BufferedImage resizeImageJpg = resizeImage(originalImage, type,500,500);
			ImageIO.write(resizeImageJpg, "jpg", new File("D:/b/c.jpg"));
			
			int []ar = getHeigtWidthOfImage(601,510);
			System.out.println("height"+ar[0]);
			System.out.println("width"+ar[1]);*/
			
			/*int [] ar = getImageSizeByRatio(400,500);
			System.out.println(ar[0]);
			System.out.println(ar[1]);*/
			
			resizeImage("D:/a/a.png");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
// resing image on the basis of breath And height	
	public static boolean resizeImages(File imgPath)
	{
		System.out.println("Target imgPath "+imgPath);
		try 
		{   
			BufferedImage originalImage = ImageIO.read(imgPath);
			int img_height = originalImage.getHeight();
			int img_width = originalImage.getWidth();
			int []imgSize = {850, 500};
             
			int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
			
			
				if(img_width>imgSize[0] && img_height>imgSize[1]){
					
					BufferedImage resizedImage = new BufferedImage(imgSize[0], imgSize[1], type);
				    Graphics2D g = resizedImage.createGraphics();
						g.drawImage(originalImage, 0, 0, imgSize[0],imgSize[1], null);
						g.dispose();
						ImageIO.write(resizedImage, type == BufferedImage.TYPE_INT_ARGB ? "jpg" : "png",imgPath);
				}
				else if(img_width>imgSize[0] && img_height<imgSize[1]){
					BufferedImage resizedImage = new BufferedImage(imgSize[0],img_height, type);
				    Graphics2D g = resizedImage.createGraphics();
					
					g.drawImage(originalImage, 0, 0, imgSize[0],img_height, null);
					g.dispose();
					ImageIO.write(resizedImage, type == BufferedImage.TYPE_INT_ARGB ? "jpg" : "png", imgPath);
				}
				else if(img_width<imgSize[0] && img_height>imgSize[1]){
					BufferedImage resizedImage = new BufferedImage(img_width, imgSize[1], type);
				    Graphics2D g = resizedImage.createGraphics();
					
					g.drawImage(originalImage, 0, 0, img_width,imgSize[1], null);
					g.dispose();
					ImageIO.write(resizedImage, type == BufferedImage.TYPE_INT_ARGB ? "jpg" : "png", imgPath);
				}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return true;
	}
}