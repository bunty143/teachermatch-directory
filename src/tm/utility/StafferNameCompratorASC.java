package tm.utility;

import java.util.Comparator;

import tm.bean.JobForTeacher;

public class StafferNameCompratorASC implements Comparator<JobForTeacher> {

	public int compare(JobForTeacher o1, JobForTeacher o2) {
		if(o1.getStafferName()==null)
			o1.setStafferName("");
		
		if(o2.getDistName()==null)
			o2.setStafferName("");
		
		return o1.getStafferName().toUpperCase().compareTo(o2.getStafferName().toUpperCase());
	}
}
