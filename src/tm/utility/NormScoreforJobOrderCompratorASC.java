package tm.utility;

import java.util.Comparator;

import tm.bean.JobOrder;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;

public class NormScoreforJobOrderCompratorASC implements Comparator<JobOrder> 
{
	public int compare(JobOrder o1, JobOrder o2) {
		Double cScore1 =o1.getNormScore();
		Double cScore2 =o2.getNormScore();		
		int c = cScore1.compareTo(cScore2);
		return c;
	}

	
}
