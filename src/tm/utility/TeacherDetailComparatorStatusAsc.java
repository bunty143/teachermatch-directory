package tm.utility;

import java.util.Comparator;

import tm.bean.TeacherDetail;

public class TeacherDetailComparatorStatusAsc implements Comparator<TeacherDetail> {

		public int compare(TeacherDetail td1, TeacherDetail td2) {
			
			return td1.gettStatus().compareTo(td2.gettStatus());
		}		
}
