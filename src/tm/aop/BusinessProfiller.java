package tm.aop;

 
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class BusinessProfiller { 
	
	// All classes in tm.dao package
	
	@Pointcut("execution(* tm.dao.*.*(..))")
    public void businessMethods0() {
    	System.out.println("=>=>::::::::BusinessProfiller=>::::"); 
    	}

    @Around("businessMethods0()")
    public Object profile0(ProceedingJoinPoint pjp ) throws Throwable {
            Object output = null;
			try {
				long start = System.currentTimeMillis();   double startM =  (double) (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024;
				
				/* Object[] signatureArgs = pjp.getArgs();
				   for (Object signatureArg: signatureArgs) {
				      System.out.println("Arg: " + signatureArg);				     
				   }*/
				
				  System.out.println(" ^^^^^DD^^^^^ Method execution Start=> ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
			  	output = pjp.proceed();
					//System.out.println("Output ::::::::::: "+output);
				long elapsedTime = System.currentTimeMillis() - start;
				
				System.out.println(" !!!!!!DD!!!!! Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
			} catch (Exception e) {
				
				e.printStackTrace();
			}
            return output;
    }
    
    
 // All classes in tm.dao.subPackages package

        @Pointcut("execution(* tm.dao.*.*.*(..))")
        public void businessMethods1() {
        	System.out.println("=>=>::::::::BusinessProfiller=>::::"); 
        	}

        @Around("businessMethods1()")
        public Object profile1(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					
					System.out.println(" ^^^^^^DD^^^^^^ Method execution Start=> ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" !!!!!!DD!!!!! Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
        
   
        // all classes in  tm.services package
        
        @Pointcut("execution(* tm.services.*.*(..))")
        public void businessMethods2() {
        	System.out.println("=>=>::::::::tm.services.TeacherInfoAjax  BusinessProfiller=>::::"); 
        	}

        @Around("businessMethods2()")
        public Object profile2(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					
					
				/*	 Object[] signatureArgs = pjp.getArgs();
					   for (Object signatureArg: signatureArgs) {
					      System.out.println("Arg: " + signatureArg);				     
					   }
					*/
					
					System.out.println(" ~~~ Service  ~~~ Method execution Start=> ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" ----- Service  ----- Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
        
        
        // all classes in  tm.services.subPackages package
         
        
        @Pointcut("execution(* tm.services.*.*.*(..))")
        public void businessMethods3() {
        	System.out.println("=>=>::::::::tm.services.TeacherInfoAjax  BusinessProfiller=>::::"); 
        	}

        @Around("businessMethods3()")
        public Object profile3(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					System.out.println(" ~~~ Service  ~~~ Method execution Start=> ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" ----- Service  ----- Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
        
     
        // all classes in  tm.services.rc.subPackages  package
        
        @Pointcut("execution(* tm.services.rc.*.*.*(..))")
        public void businessMethods4() {
        	System.out.println("=>=>::::::::tm.services.TeacherInfoAjax  BusinessProfiller=>::::"); 
        	}

        @Around("businessMethods4()")
        public Object profile4(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					System.out.println(" ~~~ Service  ~~~ Method execution Start=> ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" ----- Service  ----- Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
        
     // All classes in tm.servlet  package     
        
        @Pointcut("execution(* tm.servlet.*.*(..))")
        public void businessMethods5() {
        	System.out.println("=>=>::::::::tm.services.TeacherInfoAjax  BusinessProfiller=>::::"); 
        	}

        @Around("businessMethods5()")
        public Object profile5(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					System.out.println(" ~~~~~Method Start  => ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" --------- Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
   
        // Utility  classes
        
        @Pointcut("execution(* tm.utility.*.*(..))")
        public void businessMethods6() {
        	System.out.println("=>=>::::::::tm.services.TeacherInfoAjax  BusinessProfiller=>::::"); 
        	}

        @Around("businessMethods6()")
        public Object profile6(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					System.out.println(" ~~~~UUUUUUU~~~~ Method execution Start=> ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" ----UUUUUUU---- Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
 
        
//      controller   
        
        
        @Pointcut("execution(* tm.controller.*.*.*(..))")
        public void businessMethods7() {
        	System.out.println("=>=>::::::::tm.services.TeacherInfoAjax  BusinessProfiller=>::::"); 
        	}

        @Around("businessMethods7()")
        public Object profile7(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					System.out.println(" ~~~ Controller~~~ Method execution Start=> ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" ----  Controller ----- Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
        
      // All classes tm.api package
        
        
        
        @Pointcut("execution(* tm.api.*.*(..))")
        public void businessMethods8() {
        	System.out.println("=>=>::::::::tm.services.TeacherInfoAjax  BusinessProfiller=>::::"); 
        	}

        @Around("businessMethods8()")
        public Object profile8(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					System.out.println(" ~~~~~Method Start  => ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" --------- Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
        
        // All classes tm.api.subPackages package
        
        @Pointcut("execution(* tm.api.*.*.*(..))")
        public void businessMethods9() {
        	System.out.println("=>=>::::::::tm.services.TeacherInfoAjax  BusinessProfiller=>::::"); 
        	}

        @Around("businessMethods9()")
        public Object profile9(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					System.out.println(" ~~~~~Method Start  => ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" --------- Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
        
        // All classes tm.api.subPackage.subPackages package   
        
        @Pointcut("execution(* tm.api.*.*.*.*(..))")
        public void businessMethods10() {
        	System.out.println("=>=>::::::::tm.services.TeacherInfoAjax  BusinessProfiller=>::::"); 
        	}

        @Around("businessMethods10()")
        public Object profile10(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					System.out.println(" ~~~~~Method Start  => ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" --------- Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
        
        
        
        // Api    End     
        
        
       
        
        @Pointcut("execution(* tm.controller.master.TeacherController.*(..))")
        public void businessMethods12() {
        	System.out.println("=>=>::::::::tm.controller.master.TeacherController  BusinessProfiller=>::::"); 
        	}

        @Around("businessMethods12()")
        public Object profile12(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					System.out.println(" ~~~~~Method Start  => ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" --------- Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
        
  /*      
        
        @Pointcut("execution(* tm.dao.*.*(..))")
        public void businessMethods3() {
        	System.out.println("=>=>::::::::tm.dao.JobForTeacherDAO=>::::"); 
        	}

        @Around("businessMethods3()")
        public Object profile3(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					System.out.println(" ~~~~~Method Start  => ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" --------- Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
        
        
        
        @Pointcut("execution(* tm.services.*.*(..))")
        public void businessMethods4() {
        	System.out.println("=>=>::::::::tm.dao.JobForTeacherDAO=>::::"); 
        	}

        @Around("businessMethods4()")
        public Object profile4(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					System.out.println(" ~~~~~Method Start  => ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" --------- Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
        
        
        @Pointcut("execution(* tm.services.*.*.*(..))")
        public void businessMethods5() {
        	System.out.println("=>=>::::::::tm.dao.JobForTeacherDAO=>::::"); 
        	}

        @Around("businessMethods5()")
        public Object profile5(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
				try {
					long start = System.currentTimeMillis();
					System.out.println(" ~~~~~Method Start  => ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
					output = pjp.proceed();
           	
					long elapsedTime = System.currentTimeMillis() - start;
					System.out.println(" --------- Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
                return output;
        }
        */
        
        
        

        // just for testing 
        
        
        @Pointcut("execution(* tm.dao.JobForTeacherDAO.getJobForTeacherByJobAndTeachersListWithHQandBRSQL(..))")
        public void businessMethodsTest1() {
        	System.out.println("=>=>::::::::BusinessProfiller=>::::"); 
        	}

        @Around("businessMethodsTest1()")
        public Object profileTest1(ProceedingJoinPoint pjp) throws Throwable {
                Object output = null;
    			try {
    				long start = System.currentTimeMillis();
    				System.out.println(" ^^^^^New Dao Method^^^^^ Method execution Start=> ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName());
    				output = pjp.proceed();
           	
    				long elapsedTime = System.currentTimeMillis() - start;
    				System.out.println(" !!!!!!New Dao Method!!!!! Method execution time: ( "+pjp.getTarget().getClass().getName()+"->"+pjp.getSignature().getName()+" ) ======" + elapsedTime + " milliseconds.");
    			} catch (Exception e) {
    				
    				e.printStackTrace();
    			}
                return output;
        }
        
        
        
}