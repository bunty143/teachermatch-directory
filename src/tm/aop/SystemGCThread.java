package tm.aop;

import com.sun.star.util.Date;

public class SystemGCThread extends Thread {
	
	
	 private Thread t;
	   private String threadName;
	   
	   SystemGCThread( String name){
	       threadName = name;
	       System.out.println("Creating " +  threadName );
	   }
	   
	   
	public void run(){
	System.out.println("Request for Garbadge Collection :::::::::::::::::"+(new Date()).toString());
	showMemory(threadName +" ::::: before");
	 
		System.gc();
		
		showMemory(threadName +" ::::: After");
	}
	
	
	 public void start ()
	   {
	      System.out.println("Starting " +  threadName );
	      if (t == null)
	      {
	         t = new Thread (this, threadName);
	         t.start ();
	      }
	   }
	
	 public void showMemory(String tName){
		 
		 System.out.println("Called by :::::::::::::::::"+tName);
		 int mb = 1024*1024;
         
	        //Getting the runtime reference from system
	        Runtime runtime = Runtime.getRuntime();
	         
	        System.out.println("##### Heap utilization statistics [MB] #####");
	         
	        //Print used memory
	        System.out.println("Used Memory:"
	            + (runtime.totalMemory() - runtime.freeMemory()) / mb);
	 
	        //Print free memory
	        System.out.println("Free Memory:"
	            + runtime.freeMemory() / mb);
	         
	        //Print total available memory
	        System.out.println("Total Memory:" + runtime.totalMemory() / mb);
	 
	        //Print Maximum available memory
	        System.out.println("Max Memory:" + runtime.maxMemory() / mb);
	    }
	

	 
}
