package tm.dao;

import tm.bean.ReferenceNote;
import tm.dao.generic.GenericHibernateDAO;

public class ReferenceNoteDAO extends GenericHibernateDAO<ReferenceNote, Integer> 
{
	public ReferenceNoteDAO() 
	{
		super(ReferenceNote.class);
	}
}
