package tm.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.TeacherRemovedFromFeed;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherRemovedFromFeedDAO extends GenericHibernateDAO<TeacherRemovedFromFeed, Integer> 
{
	public TeacherRemovedFromFeedDAO() {
		super(TeacherRemovedFromFeed.class);
	}
	
	@Transactional
	public List<TeacherDetail> getRemovedTeachers(DistrictMaster districtMaster,SchoolMaster schoolMaster)
	{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		
		if(districtMaster!=null && schoolMaster==null)
		{
			criteria.add(Restrictions.eq("districtMaster", districtMaster));
		}else if(schoolMaster!=null)
		{
			criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
		}
		//criteria.add(Restrictions.eq("userEntityTypeId", entiyType));
		criteria.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("teacherDetail"))
		);
		
		
		return criteria.list();
	}
	
	@Transactional
	public List<TeacherDetail> getRemovedTeachersByHBD(DistrictMaster districtMaster,SchoolMaster schoolMaster,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
	{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		
		if(branchMaster!=null){
			criteria.add(Restrictions.eq("branchMaster",branchMaster));
		}else{
			criteria.add(Restrictions.isNull("branchMaster"));
		}
		if(headQuarterMaster!=null){
			criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
		}else{
			criteria.add(Restrictions.isNull("headQuarterMaster"));
		}
		if(districtMaster!=null && schoolMaster==null)
		{
			criteria.add(Restrictions.eq("districtMaster", districtMaster));
		}else if(schoolMaster!=null)
		{
			criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
		}
		//criteria.add(Restrictions.eq("userEntityTypeId", entiyType));
		criteria.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("teacherDetail"))
		);
		
		
		return criteria.list();
	}
}
