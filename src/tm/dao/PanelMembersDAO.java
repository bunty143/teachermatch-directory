package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictPanelMaster;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictPanelMaster;
import tm.bean.PanelMembers;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class PanelMembersDAO extends GenericHibernateDAO<PanelMembers, Integer> {
	PanelMembersDAO()
	{
		super(PanelMembers.class);
	}
	
	@Transactional(readOnly=true)
	public List<PanelMembers> getPanelMembersByDistrictPanelMaster(DistrictPanelMaster districtPanelMaster)
	{
	
		List <PanelMembers> districtPanelMasters = new ArrayList<PanelMembers>();
		try 
		{	         
			 Criterion criterion1 = Restrictions.eq("districtPanelMaster",districtPanelMaster);
			 districtPanelMasters = findByCriteria(criterion1);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return districtPanelMasters;
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<PanelMembers> checkDuplicatePanelMember(UserMaster userMaster, DistrictPanelMaster districtPanelMaster) 
	{

		List <PanelMembers> lstPanelMembers= new ArrayList<PanelMembers>();
		try 
		{	         
			Criterion criterion1 = Restrictions.eq("userMaster",userMaster);
			Criterion criterion2 = Restrictions.eq("districtPanelMaster",districtPanelMaster);
			lstPanelMembers = findByCriteria(criterion1,criterion2);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstPanelMembers;
	}
	
}
