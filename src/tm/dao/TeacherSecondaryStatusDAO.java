package tm.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherSecondaryStatus;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherSecondaryStatusDAO extends GenericHibernateDAO<TeacherSecondaryStatus, Integer>{
	public TeacherSecondaryStatusDAO() {
		super(TeacherSecondaryStatus.class);
	}
	
	@Transactional(readOnly=false)
	public TeacherSecondaryStatus findByTeacherAndJob(TeacherDetail teacherDetail, JobOrder jobOrder)
	{
		List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);			

			lstTeacherSecondaryStatus = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherSecondaryStatus.size()>0)
			return lstTeacherSecondaryStatus.get(0);
		else
			return null;
	}
	
	@Transactional(readOnly=false)
	public TeacherSecondaryStatus findByTeacherJobAndSchool(TeacherDetail teacherDetail, JobOrder jobOrder, SchoolMaster schoolMaster)
	{
		List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);			
			//System.out.println(" Save Status  ");
			boolean writePrivilege = jobOrder.getWritePrivilegeToSchool()==null?false:jobOrder.getWritePrivilegeToSchool();
			if(writePrivilege==false){
				if(schoolMaster==null){
					Criterion criterion3 = Restrictions.isNull("schoolMaster");
					lstTeacherSecondaryStatus = findByCriteria(criterion1,criterion2,criterion3);
				}
				else{
					Criterion criterion3 = Restrictions.eq("schoolMaster",schoolMaster);
					lstTeacherSecondaryStatus = findByCriteria(criterion1,criterion2,criterion3);
				}
			}
			else
				lstTeacherSecondaryStatus = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherSecondaryStatus.size()>0)
			return lstTeacherSecondaryStatus.get(0);
		else
			return null;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherSecondaryStatus> findByJobAndSchool(JobOrder jobOrder, SchoolMaster schoolMaster)
	{
		List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = new ArrayList<TeacherSecondaryStatus>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			boolean writePrivilege = jobOrder.getWritePrivilegeToSchool()==null?false:jobOrder.getWritePrivilegeToSchool();
			if(writePrivilege==false)
			{
				if(schoolMaster==null){
					Criterion criterion2 = Restrictions.isNull("schoolMaster");
					lstTeacherSecondaryStatus = findByCriteria(criterion1,criterion2);
				}
				else{
					Criterion criterion2 = Restrictions.eq("schoolMaster",schoolMaster);
					lstTeacherSecondaryStatus = findByCriteria(criterion1,criterion2);
				}
			}
			else
			{
				lstTeacherSecondaryStatus = findByCriteria(criterion1);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		
		return lstTeacherSecondaryStatus;
		
	}
	
	@Transactional(readOnly=false)
	public List<TeacherSecondaryStatus> findTSSListByTeacherAndJob(TeacherDetail teacherDetail, JobOrder jobOrder)
	{
		List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = new ArrayList<TeacherSecondaryStatus>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);			

			lstTeacherSecondaryStatus = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherSecondaryStatus;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherSecondaryStatus> findTSSListByTeacherJobAndDistrict(List<TeacherDetail> teacherDetails, List<JobOrder> jobOrders, DistrictMaster districtMaster)
	{
		List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = new ArrayList<TeacherSecondaryStatus>();
		try 
		{
			Criterion teacherCriteria = Restrictions.in("teacherDetail",teacherDetails);
			Criterion jobCriteria;
			if(jobOrders!=null && jobOrders.size()>0)
				jobCriteria = Restrictions.in("jobOrder",jobOrders);
			else
				jobCriteria = Restrictions.isNotNull("jobOrder");
			Criterion districtCriteria = Restrictions.eq("districtMaster",districtMaster);
			
			Criterion teacherAndJobCriterion = Restrictions.and(teacherCriteria, jobCriteria);
			Criterion criterion = Restrictions.and(districtCriteria, teacherAndJobCriterion);
			
			lstTeacherSecondaryStatus = findByCriteria(Order.desc("createdDateTime"),criterion);
		}
		catch (Exception e)
		{ e.printStackTrace(); }
		
		return lstTeacherSecondaryStatus;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherSecondaryStatus> findTSSListByTeacherAndJobOrderIsNull(List<TeacherDetail> teacherDetails, DistrictMaster districtMaster)
	{
		List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = new ArrayList<TeacherSecondaryStatus>();
		try 
		{
			Criterion teacher_cri = Restrictions.in("teacherDetail", teacherDetails);
			Criterion district_cri = Restrictions.eq("districtMaster", districtMaster);
			Criterion school_cri = Restrictions.isNull("schoolMaster");
			Criterion job_cri = Restrictions.isNull("jobOrder");
			
			lstTeacherSecondaryStatus =  findByCriteria(teacher_cri, district_cri, school_cri, job_cri);
		}
		catch (Exception e)
		{ e.printStackTrace(); }
		
		return lstTeacherSecondaryStatus;
	}
	
	//search tags on CG
	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeacherBySecondarystatusAndJob(SecondaryStatusMaster  secondaryStatusMaster, JobOrder jobOrder)
	{
		List<TeacherDetail> lstTeacherSecondaryStatus = new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion1 = Restrictions.eq("secondaryStatusMaster",secondaryStatusMaster);
			Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);			
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			lstTeacherSecondaryStatus =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherSecondaryStatus;
	}
	
	//search tags on CG new
	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeacherBySecondarystatusAndDistrict(SecondaryStatusMaster secondaryStatusMaster, DistrictMaster districtMaster)
	{
		List<TeacherDetail> lstTeacherSecondaryStatus = new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion1 = Restrictions.eq("secondaryStatusMaster",secondaryStatusMaster);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);			
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			lstTeacherSecondaryStatus =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherSecondaryStatus;
	}
	
  //search tags on Applicant pool	
	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeacherBySecondarystatus(SecondaryStatusMaster  secondaryStatusMaster,DistrictMaster districtMaster)
	{
		List<TeacherDetail> lstTeacherSecondaryStatus = new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion1 = Restrictions.eq("secondaryStatusMaster",secondaryStatusMaster);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion3 = Restrictions.isNull("jobOrder");
			
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			
			
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			lstTeacherSecondaryStatus =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherSecondaryStatus;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeacherListWithFilter(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster)
	{
		List<TeacherDetail> lstTeacherDetail = new ArrayList<TeacherDetail>();
		try 
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion2 = null; 
			Criterion criterion1 	= 	Restrictions.in("teacherDetail",teacherDetails);
			
			if(districtMaster!=null){
				criterion2 	= 	Restrictions.eq("districtMaster",districtMaster);
			}
			criteria.add(criterion1);
			if(districtMaster!=null){
				criteria.add(criterion2);
			}
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			lstTeacherDetail = criteria.list();
			
		}
		catch (Exception e)
		{ e.printStackTrace(); }
		
		return lstTeacherDetail;
	}
	//search tags on CG new
	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeacherBySecondarystatusAndDistrictHB(SecondaryStatusMaster secondaryStatusMaster, JobOrder jobOrder)
	{
		List<TeacherDetail> lstTeacherSecondaryStatus = new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion1 = Restrictions.eq("secondaryStatusMaster",secondaryStatusMaster);
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null && secondaryStatusMaster.getBranchId()!=null){
				criteria.add(Restrictions.eq("branchId",jobOrder.getBranchMaster().getBranchId()));
			}
			else if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.isNull("branchId"));			
				criteria.add(Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId()));
			}		
			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			lstTeacherSecondaryStatus =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherSecondaryStatus;
	}	

	
	
	// Optimization for Candidate Pool search replacement of  findTeacherBySecondarystatus
	@Transactional(readOnly=false)
	public List<Integer> findTeacherBySecondarystatus_Op(SecondaryStatusMaster  secondaryStatusMaster,DistrictMaster districtMaster, List<Integer> teachersId)
	{
		List<Integer> lstTeacherSecondaryStatus = new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				Criterion criterion1 = Restrictions.eq("secondaryStatusMaster",secondaryStatusMaster);
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 = Restrictions.isNull("jobOrder");
				
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				
				criteria.add(Restrictions.in("teacherDetail.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				lstTeacherSecondaryStatus =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return lstTeacherSecondaryStatus;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherSecondaryStatus> findTeacherSecondaryStatusByJobOrder(UserMaster userMaster,JobOrder jobOrder)
	{
		List<TeacherSecondaryStatus> lstTeacherSecStatus = new ArrayList<TeacherSecondaryStatus>();
		try 
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 = null; 
			Criterion school_cri = Restrictions.isNull("schoolMaster");
			Criterion job_cri = Restrictions.isNull("jobOrder");
			Criterion job_cri_inc = Restrictions.eq("jobOrder", jobOrder);
			if(userMaster.getHeadQuarterMaster()==null && userMaster.getDistrictId()!=null){
					criterion1 = Restrictions.eq("districtMaster", userMaster.getDistrictId());
					criteria.add(job_cri);
					criteria.add(school_cri);
			}
			else if(userMaster.getBranchMaster()!=null){
				criterion1 = Restrictions.eq("branchId", userMaster.getBranchMaster().getBranchId());
			}
			else if(userMaster.getHeadQuarterMaster()!=null){
				criterion1 = Restrictions.eq("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
				criteria.add(Restrictions.isNull("branchId"));
			}
			
			criteria.add(criterion1);
			lstTeacherSecStatus = criteria.list();
		}
		catch (Exception e)
		{ e.printStackTrace(); }
		
		return lstTeacherSecStatus;
	}
	
	//Ajay Jain
	@Transactional(readOnly=false)
	public List<TeacherSecondaryStatus> findTSSListByTeacherJobAndUserMaster(List<TeacherDetail> teacherDetails, List<JobOrder> jobOrders, UserMaster userMaster)
	{
		List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = new ArrayList<TeacherSecondaryStatus>();
		try 
		{
			Criterion teacherCriteria = Restrictions.in("teacherDetail",teacherDetails);
			Criterion jobCriteria;
			if(jobOrders!=null && jobOrders.size()>0)
				jobCriteria = Restrictions.in("jobOrder",jobOrders);
			else
				jobCriteria = Restrictions.isNotNull("jobOrder");
			Criterion criterion1=null;
			if(userMaster.getDistrictId()!=null)
				criterion1 = Restrictions.eq("districtMaster",userMaster.getDistrictId());
			else if(userMaster.getBranchMaster()!=null)
				criterion1	=	Restrictions.eq("branchId", userMaster.getBranchMaster().getBranchId());
			else if(userMaster.getHeadQuarterMaster()!=null)
				criterion1	=	Restrictions.eq("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
			Criterion teacherAndJobCriterion = Restrictions.and(teacherCriteria, jobCriteria);
			Criterion criterion = Restrictions.and(criterion1, teacherAndJobCriterion);
			
			lstTeacherSecondaryStatus = findByCriteria(Order.desc("createdDateTime"),criterion);
		}
		catch (Exception e)
		{ e.printStackTrace(); }
		
		return lstTeacherSecondaryStatus;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherSecondaryStatus> findTSSListByTeacherAndJobOrderIsNullNew(List<TeacherDetail> teacherDetails, UserMaster userMaster)
	{
		List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = new ArrayList<TeacherSecondaryStatus>();
		try 
		{
			Criterion teacher_cri = Restrictions.in("teacherDetail", teacherDetails);
			//Criterion district_cri = Restrictions.eq("districtMaster", districtMaster);
			Criterion school_cri = Restrictions.isNull("schoolMaster");
			Criterion job_cri = Restrictions.isNull("jobOrder");
			
			Criterion criterion1=null;
			if(userMaster.getDistrictId()!=null)
				criterion1 = Restrictions.eq("districtMaster",userMaster.getDistrictId());
			else if(userMaster.getBranchMaster()!=null)
				criterion1	=	Restrictions.eq("branchId", userMaster.getBranchMaster().getBranchId());
			else if(userMaster.getHeadQuarterMaster()!=null)
				criterion1	=	Restrictions.eq("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
			lstTeacherSecondaryStatus =  findByCriteria(teacher_cri, school_cri, job_cri, criterion1);
		}
		catch (Exception e)
		{ e.printStackTrace(); }
		
		return lstTeacherSecondaryStatus;
	}

	@Transactional(readOnly=false)
	public List<TeacherSecondaryStatus> findByJobAndSchool_Op(JobOrder jobOrder, SchoolMaster schoolMaster)
	{
		List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = new ArrayList<TeacherSecondaryStatus>();
		try 
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			
			criteria.createAlias("teacherDetail", "td").createAlias("secondaryStatusMaster", "ssm")
			.setProjection(Projections.projectionList()
					.add(Projections.property("td.teacherId"))
					.add(Projections.property("ssm.secStatusId"))
					.add(Projections.property("ssm.secStatusName"))
					.add(Projections.property("ssm.pathOfTagsIcon"))
					);
			List<String []> lstTeacherSecStatusTemp=new ArrayList<String []>();
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			boolean writePrivilege = jobOrder.getWritePrivilegeToSchool()==null?false:jobOrder.getWritePrivilegeToSchool();
			if(writePrivilege==false)
			{
				if(schoolMaster==null){
					Criterion criterion2 = Restrictions.isNull("schoolMaster");
					criteria.add(criterion2).add(criterion1);
					lstTeacherSecStatusTemp = criteria.list();
					//lstTeacherSecondaryStatus = findByCriteria(criterion1,criterion2);
				}
				else{
					Criterion criterion2 = Restrictions.eq("schoolMaster",schoolMaster);
					//lstTeacherSecondaryStatus = findByCriteria(criterion1,criterion2);
					criteria.add(criterion2).add(criterion1);
					lstTeacherSecStatusTemp = criteria.list();
				}
			}
			else
			{
				//lstTeacherSecondaryStatus = findByCriteria(criterion1);
				criteria.add(criterion1);
				lstTeacherSecStatusTemp = criteria.list();
			}
			
			if(lstTeacherSecStatusTemp.size()>0){
				for (Iterator it = lstTeacherSecStatusTemp.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();    				
					if(row[0]!=null){
						TeacherSecondaryStatus teacherSec = new TeacherSecondaryStatus();
						TeacherDetail tdetail = new TeacherDetail();
						tdetail.setTeacherId(Integer.parseInt(row[0].toString()));
							teacherSec.setTeacherDetail(tdetail);
							if(row[1]!=null){
								SecondaryStatusMaster secSecondaryStatusMaster = new SecondaryStatusMaster();
								secSecondaryStatusMaster.setSecStatusId(Integer.parseInt(row[1].toString()));
								if(row[2]!=null)
								secSecondaryStatusMaster.setSecStatusName(row[2].toString());
								if(row[3]!=null)
									secSecondaryStatusMaster.setPathOfTagsIcon(row[3].toString());
								
								secSecondaryStatusMaster.setDistrictMaster(jobOrder.getDistrictMaster());
								teacherSec.setSecondaryStatusMaster(secSecondaryStatusMaster);
							}
							teacherSec.setJobOrder(jobOrder);
						lstTeacherSecondaryStatus.add(teacherSec);
					}
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		
		return lstTeacherSecondaryStatus;
		
	}
	
	@Transactional(readOnly=false)
	public List<TeacherSecondaryStatus> findTSSListByTeacherAndUserMaster(List<TeacherDetail> teacherDetails, UserMaster userMaster)
	{
		List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = new ArrayList<TeacherSecondaryStatus>();
		try 
		{
			Criterion teacher_cri = Restrictions.in("teacherDetail", teacherDetails);
			Criterion criterion1=null;
			if(userMaster.getBranchMaster()!=null)
				criterion1	=	Restrictions.eq("branchId", userMaster.getBranchMaster().getBranchId());
			else if(userMaster.getHeadQuarterMaster()!=null)
			{
				Criterion criterion = Restrictions.isNull("branchId");
				criterion1	=	Restrictions.and(criterion, Restrictions.eq("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId()));
			}
			lstTeacherSecondaryStatus =  findByCriteria(teacher_cri, criterion1);
		}
		catch (Exception e)
		{ e.printStackTrace(); }
		
		return lstTeacherSecondaryStatus;
	}
	@Transactional(readOnly=false)
	public List<TeacherSecondaryStatus> findTSSListByTeacherAndUserMaster(String teacherDetails, UserMaster userMaster)
	{
		List<TeacherSecondaryStatus> lstTeacherSecondaryStatus = new ArrayList<TeacherSecondaryStatus>();
		try 
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass(),"tss");
			//criteria.add(Property.forName("tss.teacherDetail.teacherId").in(teacherDetails));
			criteria.add(Restrictions.sqlRestriction("this_.teacherId in("+teacherDetails+")"));
			Criterion criterion1=null;
			
			
			if(userMaster.getBranchMaster()!=null)
				criteria.add(Restrictions.eq("branchId", userMaster.getBranchMaster().getBranchId()));
			else if(userMaster.getHeadQuarterMaster()!=null)
			{
				Criterion criterion = Restrictions.isNull("branchId");
				criteria.add(Restrictions.and(criterion, Restrictions.eq("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId())));
			}
			lstTeacherSecondaryStatus = criteria.list();
		}
		catch (Exception e)
		{ e.printStackTrace(); }
		
		return lstTeacherSecondaryStatus;
	}
}
