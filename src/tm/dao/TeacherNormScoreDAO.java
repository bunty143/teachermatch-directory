package tm.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNormScore;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherNormScoreDAO extends GenericHibernateDAO<TeacherNormScore, Integer> 
{
	public TeacherNormScoreDAO() {
		super(TeacherNormScore.class);
	}
	
	@Transactional(readOnly=false)
	//public List insertTeacherActionFeed(int hired)
	public List insertTeacherActionFeed(List<Integer> statusIdList)
	{
		try 
		{
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);

			Session session = getSession();
			
			Query query = session.createSQLQuery(
			" insert into teacheractionfeed (teacherId,isInternal,districtId,schoolId,jobId,jobAppliedDate," +
			" teacherNormScore,lastActivityOnJob,lastActivityDate,lastActivityDoneBy,daysForLastActivity,createdDateTime)" +
			" select tns.teacherId,jft.isAffilated,jo.districtId,sjo.schoolId,jo.jobId,jft.createdDateTime,tns.teacherNormScore, " +
			" jft.lastActivity,jft.lastActivityDate,jft.lastActivityDoneBy,DATEDIFF(now(),jft.lastActivityDate) as noOfDays,now() " +
			" from teachernormscore tns join jobforteacher jft on tns.teacherId=jft.teacherId " +
			" left join schoolinjoborder sjo on jft.jobId=sjo.jobId join joborder jo on jft.jobId=jo.jobId" +
			" where jo.status=:statusCode and jft.status NOT IN (:jftStatus) and (jo.jobStartDate <= :from and jo.jobEndDate >= :to) ");
			//" where jo.status=:statusCode and jft.status != :jftStatus and (jo.jobStartDate <= :from and jo.jobEndDate >= :to) ");
			//IN (:teacherDetails)
			//" where jo.status=:statusCode and jft.status != :jftStatus ");
			query.setParameter("statusCode", "A");
			query.setParameterList("jftStatus", statusIdList);
			//query.setParameter("jftStatus", hired);
			query.setParameter("from", cal.getTime());
			query.setParameter("to", cal.getTime());
			
		    int res = query.executeUpdate();
		   
            System.out.println("Command successfully executed....");
            System.out.println("Numer of records effected........"+res);
	     
	        return null;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeacherListByNormScore(String normScoreVal,String normScoreSelectVal)
	{
		List<TeacherDetail> teacherList= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 =null;
			if(normScoreSelectVal.equals("1")){
				criterion1=Restrictions.eq("teacherNormScore",Integer.parseInt(normScoreVal));
			}else if(normScoreSelectVal.equals("2")){
				criterion1=Restrictions.lt("teacherNormScore",Integer.parseInt(normScoreVal));
			}else if(normScoreSelectVal.equals("3")){
				criterion1=Restrictions.le("teacherNormScore",Integer.parseInt(normScoreVal));
			}else if(normScoreSelectVal.equals("4")){
				criterion1=Restrictions.gt("teacherNormScore",Integer.parseInt(normScoreVal));
			}else if(normScoreSelectVal.equals("5")){
				criterion1=Restrictions.ge("teacherNormScore",Integer.parseInt(normScoreVal));
			}
			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			teacherList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherNormScore> findNormScoreByTeacherList(List<TeacherDetail> teacherDetails)
	{
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
		List<TeacherNormScore> teacherNormScoreList= new ArrayList<TeacherNormScore>();
		try 
		{
			if(teacherDetails!=null && teacherDetails.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass())
				.createAlias("teacherDetail", "td")
				.createAlias("teacherAssessmentdetail", "tad");
				DetachedCriteria subQuery = DetachedCriteria.forClass(TeacherNormScore.class, "tns")
				//.addOrder(Order.desc("tns.teacherAssessmentdetail.teacherAssessmentId"))
				.setProjection(Projections.projectionList()
			    		.add(Projections.max("tns.teacherAssessmentdetail.teacherAssessmentId")))
			    		.add(Restrictions.eqProperty("tns.teacherDetail.teacherId", "td.teacherId"));
				criteria.add(Property.forName("tad.teacherAssessmentId").eq(subQuery));
				criteria.add(Restrictions.in("teacherDetail", teacherDetails));
				
				long a = System.currentTimeMillis();
				teacherNormScoreList = criteria.list() ;
				long b = System.currentTimeMillis();
				System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + teacherNormScoreList.size() );
				return teacherNormScoreList;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherNormScoreList;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherNormScore> findNormScoreByTeacherList(String teacherDetails)
	{
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
		List<TeacherNormScore> teacherNormScoreList= new ArrayList<TeacherNormScore>();
		try 
		{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass(),"this")
				.createAlias("teacherDetail", "td")
				.createAlias("teacherAssessmentdetail", "tad");
				DetachedCriteria subQuery = DetachedCriteria.forClass(TeacherNormScore.class, "tns")
				.setProjection(Projections.projectionList()
			    		.add(Projections.max("tns.teacherAssessmentdetail.teacherAssessmentId")))
			    		.add(Restrictions.eqProperty("tns.teacherDetail.teacherId", "td.teacherId"));
				criteria.add(Property.forName("tad.teacherAssessmentId").eq(subQuery));
				//criteria.add(Property.forName("this.teacherDetail.teacherId").in(teacherDetails));
				criteria.add(Restrictions.sqlRestriction("this_.teacherId in("+teacherDetails+")"));
				long a = System.currentTimeMillis();
				teacherNormScoreList = criteria.list() ;
				long b = System.currentTimeMillis();
				System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + teacherNormScoreList.size() );
				return teacherNormScoreList;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherNormScoreList;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List getCandidatesPoolHQL(DistrictMaster districtMaster,SchoolMaster schoolMaster,StatusMaster statusMaster) 
	{
		Session session = getSession();
		String sql = "";
		if(districtMaster!=null && schoolMaster==null)
		{
			sql = " select teacherId from jobforteacher jft1 join joborder jo on jft1.jobId=jo.jobId  where " +
				  " jo.districtId=:districtMaster" ;
			if(statusMaster!=null)
				 sql+=" and jft1.status=:statuss";
			
			sql+=" group by jft1.teacherId";
				
		}else if(schoolMaster!=null)
		{
			sql =" select teacherId from jobforteacher jft1 join joborder jo on jft1.jobId=jo.jobId join schoolinjoborder sij on jo.jobId=sij.jobId where " +
			  " jo.districtId=:districtMaster and schoolId=:schoolMaster " ;
			if(statusMaster!=null)
				 sql+=" and jft1.status=:statuss";
				 sql+=" group by jft1.teacherId";
		}
		//System.out.println("jjjjjjjjjjjjjjj"+districtMaster.getDistrictId()+" "+sql);
		try {
			Query query = session.createSQLQuery(sql);
			
			if(statusMaster!=null)
			query.setParameter("statuss", statusMaster);
			if(districtMaster!=null)
			query.setParameter("districtMaster", districtMaster);
			if(schoolMaster!=null)
			query.setParameter("schoolMaster", schoolMaster );
		
			List<Object[]> rows = query.list();
			
			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countCandidatesPoolHQL(List<Integer> lstTeacherDetails) 
	{
		Session session = getSession();
		String sql = "";
		
		sql = "select count(*) cnt,tns.decileColor from teachernormscore tns  where tns.teacherId  " +
				  " IN (:teacherDetails) group by tns.decileColor";
		try {
			Query query = session.createSQLQuery(sql);
			
			
			if(lstTeacherDetails.size()>0)
			query.setParameterList("teacherDetails", lstTeacherDetails);
		
			List<Object[]> rows = query.list();
			
			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countCandidatesNormHQL(JobOrder jobOrder,TeacherDetail teacherDetail) 
	{
		Session session = getSession();
		String sql = "";
		
			//sql = "SELECT tns.teacherNormScore, COUNT(*) , tns.percentile, GROUP_CONCAT( tns.decileColor )" +
		/*sql = "SELECT tns.teacherNormScore, COUNT(*) , tns.percentile" +
				" FROM  `jobforteacher` jft JOIN teachernormscore tns ON jft.teacherId = tns.teacherId " +
				" WHERE  `jobId`=:jobOrder GROUP BY tns.teacherNormScore";*/
		
		sql = "SELECT tns.teacherNormScore, COUNT(*) , tns.decileColor ,COUNT( IF( tns.teacherId =:teacherDetail, 1, NULL ) ) techercnt " +
		" FROM  `jobforteacher` jft JOIN teachernormscore tns ON jft.teacherId = tns.teacherId " +
		" WHERE  `jobId`=:jobOrder GROUP BY tns.decileColor";
		//System.out.println("ssssssss: "+sql);
		try {
			Query query = session.createSQLQuery(sql);
			query.setParameter("jobOrder", jobOrder );
			query.setParameter("teacherDetail", teacherDetail );
			List<Object[]> rows = query.list();
			
			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countCandidatesNormHQLJobWise(JobOrder jobOrder,String groupBy) 
	{
		Session session = getSession();
		if(groupBy.equalsIgnoreCase("decileColor"))
			groupBy = "tns.decileColor";
		else
			groupBy = "tns.percentile";
		
		String sql = "";
		
		sql = "SELECT tns.teacherNormScore, COUNT(*), tns.decileColor,tns.percentile  " +
		" FROM  `jobforteacher` jft JOIN teachernormscore tns ON jft.teacherId = tns.teacherId " +
		" WHERE  `jobId`=:jobOrder GROUP BY "+groupBy;
		//System.out.println("ssssssss: "+sql);
		try {
			Query query = session.createSQLQuery(sql);
			query.setParameter("jobOrder", jobOrder );
			List<Object[]> rows = query.list();
			
			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countCandidatesNormHQL(String groupBy) 
	{
		Session session = getSession();
		String sql = "";
		
		if(groupBy.equalsIgnoreCase("decileColor"))
			groupBy = "tns.decileColor";
		else
			groupBy = "tns.percentile";
		
		sql = "SELECT tns.teacherNormScore, COUNT(*), tns.decileColor,tns.percentile " +
		" FROM  teachernormscore tns  " +
		" GROUP BY "+groupBy;
		//System.out.println("ssssssss: "+sql);
		try {
			Query query = session.createSQLQuery(sql);
			
			List<Object[]> rows = query.list();
			
			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherNormScore> findTeacersNormScoresList(List<TeacherDetail> lstTeacherDetails)
	{
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy :::::::::::: ");
		List<TeacherNormScore> lstTeacherNormScore=new ArrayList<TeacherNormScore>();
		try 
		{
			if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass())
				.createAlias("teacherDetail", "td")
				.createAlias("teacherAssessmentdetail", "tad");
				DetachedCriteria subQuery = DetachedCriteria.forClass(TeacherNormScore.class, "tns")
				//.addOrder(Order.desc("tns.teacherAssessmentdetail.teacherAssessmentId"))
				.setProjection(Projections.projectionList()
			    		.add(Projections.max("tns.teacherAssessmentdetail.teacherAssessmentId")))
			    		.add(Restrictions.eqProperty("tns.teacherDetail.teacherId", "td.teacherId"));
				criteria.add(Property.forName("tad.teacherAssessmentId").eq(subQuery));
				criteria.add(Restrictions.in("teacherDetail", lstTeacherDetails));
				
				long a = System.currentTimeMillis();
				lstTeacherNormScore = criteria.list() ;
				long b = System.currentTimeMillis();
				System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + lstTeacherNormScore.size() );
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherNormScore;
	}
	@Transactional(readOnly=false)
	public List<TeacherNormScore> findNormScoreByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherNormScore> teacherList= new ArrayList<TeacherNormScore>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			criteria.add(Restrictions.eq("teacherDetail",teacherDetail));
			teacherList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherNormScore> findNormScoreByTeacherByList(List<TeacherDetail> teacherDetail)
	{
		List<TeacherNormScore> teacherList= new ArrayList<TeacherNormScore>();
		try 
		{					
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetail);
			teacherList = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}

	
	// Optimization for Candidate pool search  replacement of findTeacherListByNormScore
	
	@Transactional(readOnly=false)
	public List<Integer> findTeacherListByNormScore_Op(String normScoreVal,String normScoreSelectVal,List<Integer> teachersId)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 =null;
				if(normScoreSelectVal.equals("1")){
					criterion1=Restrictions.eq("teacherNormScore",Integer.parseInt(normScoreVal));
				}else if(normScoreSelectVal.equals("2")){
					criterion1=Restrictions.lt("teacherNormScore",Integer.parseInt(normScoreVal));
				}else if(normScoreSelectVal.equals("3")){
					criterion1=Restrictions.le("teacherNormScore",Integer.parseInt(normScoreVal));
				}else if(normScoreSelectVal.equals("4")){
					criterion1=Restrictions.gt("teacherNormScore",Integer.parseInt(normScoreVal));
				}else if(normScoreSelectVal.equals("5")){
					criterion1=Restrictions.ge("teacherNormScore",Integer.parseInt(normScoreVal));
				}
				criteria.add(criterion1);
				
				criteria.add(Restrictions.in("teacherDetail.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				teacherList =  criteria.list();
				//System.out.println("teacherS:::::::::::::::::::::::: "+criteria.list().size());
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> findTeacherListByNormScore_Op(String normScoreVal,String normScoreSelectVal)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 =null;
				if(normScoreSelectVal.equals("1")){
					criterion1=Restrictions.eq("teacherNormScore",Integer.parseInt(normScoreVal));
				}else if(normScoreSelectVal.equals("2")){
					criterion1=Restrictions.lt("teacherNormScore",Integer.parseInt(normScoreVal));
				}else if(normScoreSelectVal.equals("3")){
					criterion1=Restrictions.le("teacherNormScore",Integer.parseInt(normScoreVal));
				}else if(normScoreSelectVal.equals("4")){
					criterion1=Restrictions.gt("teacherNormScore",Integer.parseInt(normScoreVal));
				}else if(normScoreSelectVal.equals("5")){
					criterion1=Restrictions.ge("teacherNormScore",Integer.parseInt(normScoreVal));
				}
				criteria.add(criterion1);
				
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				teacherList =  criteria.list();
				//System.out.println("teacherS:::::::::::::::::::::::: "+criteria.list().size());
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherNormScore> findNormScoreByTeacherByList(TeacherAssessmentdetail teacherAssessmentdetail)
	{
		List<TeacherNormScore> teacherList= new ArrayList<TeacherNormScore>();
		try 
		{					
			Criterion criterion1 = Restrictions.eq("teacherAssessmentdetail",teacherAssessmentdetail);
			teacherList = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	
}
