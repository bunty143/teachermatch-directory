package tm.dao;

import tm.bean.TeacherResidencyDetails;
import tm.bean.master.ApplitrackDistricts;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherResidencyDetailsDAO extends GenericHibernateDAO<TeacherResidencyDetails, Integer> 
{
	public TeacherResidencyDetailsDAO() {
		super(TeacherResidencyDetails.class);
	}
}
