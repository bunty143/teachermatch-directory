package tm.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.StatusWiseAutoEmailSend;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class StatusWiseAutoEmailSendDAO extends GenericHibernateDAO<StatusWiseAutoEmailSend, Integer> 
{
	public StatusWiseAutoEmailSendDAO() 
	{
		super(StatusWiseAutoEmailSend.class);
	}

	@Transactional(readOnly=false)
	public StatusWiseAutoEmailSend findAutoEmailStatusByStatusIdDistrict(Integer statusId, DistrictMaster districtMaster)
	{
		StatusWiseAutoEmailSend swe=null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("statusId",statusId);	
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
			//Criterion criterion3 = Restrictions.eq("secondaryStatusId",0);
		
			List<StatusWiseAutoEmailSend> list =  findByCriteria(criterion1 , criterion2);
			
			if(list.size()>0){
				swe= list.get(0);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return swe;
	}
	
	
	@Transactional(readOnly=false)
	public boolean findBySSIDs(List<Integer> ssId)
	{
		boolean flag =false;
		StatusWiseAutoEmailSend swAutoEmail =null;
		try 
		{
			Criterion criterion1 = Restrictions.in("secondaryStatusId",ssId);	
			List list =  findByCriteria(criterion1 );
			if(list.size()>0){
				swAutoEmail= (StatusWiseAutoEmailSend) list.get(0);
				flag = true;
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return flag;
	}
	
	
	@Transactional(readOnly=false)
	public StatusWiseAutoEmailSend findAutoEmailsIdsSID(Integer statusId,Integer sstatusId, DistrictMaster districtMaster, UserMaster userMaster)
	{
		StatusWiseAutoEmailSend swe=null;
		List<StatusWiseAutoEmailSend> list=null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("secondaryStatusId",sstatusId);	
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion3 = Restrictions.eq("statusId",statusId);
			Criterion criterion4 = Restrictions.eq("usermaster",userMaster);
			 list =  findByCriteria(criterion1 , criterion2, criterion3,criterion4);
			 System.out.println(list);
			if(list.size()>0){
				swe= list.get(0);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return swe;
	}
	
	@Transactional(readOnly=false)
	public List<StatusWiseAutoEmailSend> findAllRecords( DistrictMaster districtMaster)
	{
		List<StatusWiseAutoEmailSend> listAutoEmailSends=null;
		
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			
			listAutoEmailSends =  findByCriteria(criterion1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return listAutoEmailSends;
	}
	
	
	
	@Transactional(readOnly=false)
	public boolean deleteRecords(List<StatusWiseAutoEmailSend> statusWiseAutoEmailSend)
	{
		int iReturnValue=0;
		try 
		{
			for(StatusWiseAutoEmailSend pojo:statusWiseAutoEmailSend)
			{
				makeTransient(pojo);
				iReturnValue++;
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	return true;	
		
	}
	
	@Transactional(readOnly=false)
	public List<StatusWiseAutoEmailSend> getAutoEmail(DistrictMaster districtMaster, StatusMaster statusMaster, SecondaryStatus secondaryStatus)
	{
		List<StatusWiseAutoEmailSend> lstStatusWiseAutoEmailSends=new ArrayList<StatusWiseAutoEmailSend>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			if(statusMaster!=null && statusMaster.getStatusId() > 0)
			{
				Criterion criterion2 = Restrictions.eq("statusId",statusMaster.getStatusId());
				lstStatusWiseAutoEmailSends =  findByCriteria(criterion1 , criterion2);
				
			}
			else if(secondaryStatus!=null && secondaryStatus.getSecondaryStatusId() > 0 && secondaryStatus.getSecondaryStatus_copy()!=null && secondaryStatus.getSecondaryStatus_copy().getSecondaryStatusId() > 0)
			{
				Criterion criterion3 = Restrictions.eq("secondaryStatusId",secondaryStatus.getSecondaryStatus_copy().getSecondaryStatusId());
				lstStatusWiseAutoEmailSends =  findByCriteria(criterion1 , criterion3);
				
			}
			else if(secondaryStatus!=null && secondaryStatus.getSecondaryStatusId() > 0)
			{
				Criterion criterion3 = Restrictions.eq("secondaryStatusId",secondaryStatus.getSecondaryStatusId());
				lstStatusWiseAutoEmailSends =  findByCriteria(criterion1 , criterion3);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstStatusWiseAutoEmailSends;
	}
	
	@Transactional(readOnly=false)
	public List<StatusWiseAutoEmailSend> findAllRecordsForHeadQuarter(HeadQuarterMaster headQuarterMaster)
	{
		List<StatusWiseAutoEmailSend> listAutoEmailSends=null;		
		try 
		{
			Criterion criterion1 = Restrictions.eq("headQuarterMaster",headQuarterMaster);			
			listAutoEmailSends =  findByCriteria(criterion1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return listAutoEmailSends;
	}
	
	@Transactional(readOnly=false)
	public List<StatusWiseAutoEmailSend> findAllRecordsForBranch(BranchMaster branchMaster)
	{
		List<StatusWiseAutoEmailSend> listAutoEmailSends=null;
		
		try 
		{
			Criterion criterion1 = Restrictions.eq("branchMaster",branchMaster);
			
			listAutoEmailSends =  findByCriteria(criterion1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return listAutoEmailSends;
	}
}
