package tm.dao;

import tm.bean.DistrictSpecificTfaOptions;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificTfaOptionsDAO extends GenericHibernateDAO<DistrictSpecificTfaOptions, Integer>{

	public DistrictSpecificTfaOptionsDAO(){
		super(DistrictSpecificTfaOptions.class);
		// TODO Auto-generated constructor stub
	}

}
