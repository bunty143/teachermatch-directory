package tm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.InternalTransferCandidates;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class InternalTransferCandidatesDAO extends GenericHibernateDAO<InternalTransferCandidates, Integer> 
{
	public InternalTransferCandidatesDAO() 
	{
		super(InternalTransferCandidates.class);
	}
	
	@Transactional(readOnly=false)
	public List<InternalTransferCandidates> getDistrictForTeacher(TeacherDetail teacherDetail){
		List<InternalTransferCandidates> itcList= new ArrayList<InternalTransferCandidates>();
		try{
			if(teacherDetail!=null){
				Criterion criterion1=Restrictions.eq("teacherDetail",teacherDetail);
				itcList=findByCriteria(criterion1);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return itcList;
	}
	@Transactional(readOnly=false)
	public boolean deleteInternalCandidatesyByTeacher(TeacherDetail teacherDetail){
		try{
			Criterion criterion1=Restrictions.eq("teacherDetail",teacherDetail);
        	List<InternalTransferCandidates> jfiList	=findByCriteria(criterion1);
        	for(InternalTransferCandidates jfiObj: jfiList){
        		makeTransient(jfiObj);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<UserMaster> checkDuplicateUserEmail(Integer userId,String emailAddress) 
	{
		Session session = getSession();
		if(userId!=null)
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.not(Restrictions.in("userId",new Integer[]{userId}))) 
			.add(Restrictions.eq("emailAddress", emailAddress)) 
			.list();
			return result;
		}
		else
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.eq("emailAddress", emailAddress)) 
			.list();
			return result;
		}
	}
	
	@Transactional(readOnly=false)
	public List<InternalTransferCandidates> getInternalCandidateByDistrict(TeacherDetail teacherDetail,DistrictMaster districtMaster){
		List<InternalTransferCandidates> itcList= new ArrayList<InternalTransferCandidates>();
		try{
			if(teacherDetail!=null){
				Criterion criterion1=Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2=Restrictions.eq("districtMaster",districtMaster);
				itcList=findByCriteria(criterion1,criterion2);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return itcList;
	}
	
	@Transactional(readOnly=false)
	public List<InternalTransferCandidates> getInternalCandidateDistrict(DistrictMaster districtMaster){
		List<InternalTransferCandidates> itcList= new ArrayList<InternalTransferCandidates>();
		try{
			if(districtMaster!=null){
				Criterion criterion2=Restrictions.eq("districtMaster",districtMaster);
				itcList=findByCriteria(criterion2);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return itcList;
	}
	
	@Transactional(readOnly=false)
	public List<InternalTransferCandidates> getInternalCandidateByTeachers(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster){
		List<InternalTransferCandidates> itcList= new ArrayList<InternalTransferCandidates>();
		try{
			if(teacherDetails.size()>0){
				Criterion criterion1=Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion2=Restrictions.eq("districtMaster",districtMaster);
				itcList=findByCriteria(criterion1,criterion2);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return itcList;
	}
	
	// Get Map for Status
	@Transactional(readOnly=false)
	public HashMap<Integer,InternalTransferCandidates> getDistrictForTeacherShowSatus(List<TeacherDetail> teacherList){
		Map<Integer,InternalTransferCandidates> mapItcList= new HashMap<Integer,InternalTransferCandidates>();
		List<InternalTransferCandidates> internalCanList = new ArrayList<InternalTransferCandidates>();
		
		try{
			if(teacherList!=null && teacherList.size()>0)
			{
				Criterion criterion1=Restrictions.in("teacherDetail",teacherList);
				internalCanList=findByCriteria(criterion1);
				
				if(internalCanList!=null && internalCanList.size()>0)
				{
					for(TeacherDetail td:teacherList)
					{
						//System.out.println(" "+td.getTeacherId());
						for(InternalTransferCandidates itc:internalCanList)
						{
							if(td.getTeacherId().equals(itc.getTeacherDetail().getTeacherId()))
							{
								mapItcList.put(td.getTeacherId(), itc);
							}
						}
					}
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return (HashMap<Integer, InternalTransferCandidates>) mapItcList;
	}
	
	
	@Transactional(readOnly=false)
	public List<TeacherDetail> getInternalCandidateByDistrictAndTeacherList(List<TeacherDetail> listteacherDetail,DistrictMaster districtMaster){
		List<TeacherDetail> itcList= new ArrayList<TeacherDetail>();
		try{
			if(listteacherDetail!=null && listteacherDetail.size()>0){
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				Criterion criterion1=Restrictions.in("teacherDetail",listteacherDetail);
				Criterion criterion2=Restrictions.eq("districtMaster",districtMaster);
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.setProjection(Projections.groupProperty("teacherDetail"));
				itcList=criteria.list();
				
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return itcList;
	}
	
	
	
	
}
