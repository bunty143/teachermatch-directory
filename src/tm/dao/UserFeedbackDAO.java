package tm.dao;

import tm.bean.UserFeedback;
import tm.dao.generic.GenericHibernateDAO;

public class UserFeedbackDAO extends GenericHibernateDAO<UserFeedback, Integer>{

	public UserFeedbackDAO() {
		super(UserFeedback.class);
	}
}
