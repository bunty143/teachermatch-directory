package tm.dao.mq;

import tm.bean.mq.MQEventHistory;
import tm.dao.generic.GenericHibernateDAO;

public class MQEventHistoryDAO extends GenericHibernateDAO<MQEventHistory, Integer> {
	

	public MQEventHistoryDAO() 
	{
		super(MQEventHistory.class);
	}

}
