package tm.dao;
import tm.bean.TeacherStatusHistoryForJobHistory;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherStatusHistoryForJobHistoryDAO extends GenericHibernateDAO<TeacherStatusHistoryForJobHistory, Integer>{

	public TeacherStatusHistoryForJobHistoryDAO() {
		super(TeacherStatusHistoryForJobHistory.class);
	}
	
	}

