package tm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.CmsEventTypeMaster;
import tm.bean.EventDetails;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class EventDetailsDAO extends GenericHibernateDAO<EventDetails,Integer>{
  public EventDetailsDAO()
    {
	 super(EventDetails.class);
	}
  
    @Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<EventDetails> getEventDetailsByFilter(int entityType,int sortingcheck, Order sortOrderStrVal,int startPos,int limit,DistrictMaster districtMaster,String eventName,CmsEventTypeMaster eventTypeId ,HeadQuarterMaster headQuarterMaster, BranchMaster branchMaster,List<UserMaster> lstUserMasters)
	{
	List<EventDetails> listEventDetails = new ArrayList<EventDetails>();
	try 
	{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());

		if(entityType==1){
			criteria.add(Restrictions.isNull("headQuarterMaster"));
			criteria.add(Restrictions.isNull("branchMaster"));
		}
		
		if(lstUserMasters!=null && lstUserMasters.size()>0){
			criteria.add(Restrictions.in("createdBY", lstUserMasters));
		}
		
		
		if(districtMaster!=null)
		  criteria.add(Restrictions.eq("districtMaster",districtMaster));
		
		criteria.add(Restrictions.eq("status", "A"));
		//HQ And BA
		if(headQuarterMaster!=null)
			  criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
		if(branchMaster!=null)
			  criteria.add(Restrictions.eq("branchMaster",branchMaster));
		
		if(eventTypeId!=null)
		    criteria.add(Restrictions.eq("eventTypeId",eventTypeId));
		
		if(eventName!=null && !eventName.equals(""))
		{
			Criterion criterion = Restrictions.like("eventName", eventName.trim(),MatchMode.ANYWHERE);
			criteria.add(criterion);
		}
		if(sortingcheck==0)
		   criteria.addOrder(sortOrderStrVal);
		
		Criteria c1= null;
		if(sortingcheck==1){
			c1 = criteria.createCriteria("eventTypeId");
			c1.addOrder(sortOrderStrVal);
		}
		
		listEventDetails = criteria.list();
	} 
	catch (Exception e) {
		e.printStackTrace();
	}		
	return listEventDetails;
 }


    
    @SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
    public  Map<Integer, Integer> countEventIdSlotSelection()
    {
    	Map<Integer, Integer> mapEventIdCount = new HashMap<Integer, Integer>();
       String hql="";
       List<Object[]> objList=null;
                 
       
     try{
          Query  query=null;
          mapEventIdCount.clear();
          Session session = getSession();
             hql = "SELECT  eventschedule.eventId, COUNT(eventschedule.eventId) FROM  slotselection  LEFT OUTER JOIN eventschedule  ON (slotselection.eventScheduleId = eventschedule.eventScheduleId )GROUP BY eventschedule.eventId ;"; 
           
            query=session.createSQLQuery(hql);
           objList = query.list();
         
           if(objList.size()>0){
              for (int j = 0; j < objList.size(); j++) {
               Object[] objArr2 = objList.get(j);
               
               mapEventIdCount.put(Integer.parseInt(objArr2[0].toString()+""), Integer.parseInt(objArr2[1].toString()+""));
               
              }
            }
        
             }
      catch(Exception e){
       e.printStackTrace();
     }
      return mapEventIdCount;
     }
    
    
    @SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
    public  Map<Integer, Integer> countEventIdCandidateEventDetails()
    {
    	Map<Integer, Integer> mapEventIdCount = new HashMap<Integer, Integer>();
       String hql="";
       List<Object[]> objList=null;
       
     try{
          Query  query=null;
          mapEventIdCount.clear();
          Session session = getSession();
          hql = "SELECT ced.eventId, COUNT(ced.eventId) FROM candidateeventdetails AS ced GROUP BY ced.eventId;"; 
          query=session.createSQLQuery(hql);
          objList = query.list();
         
           if(objList.size()>0){
              for (int j = 0; j < objList.size(); j++) {
               Object[] objArr2 = objList.get(j);
               
               mapEventIdCount.put(Integer.parseInt(objArr2[0].toString()+""), Integer.parseInt(objArr2[1].toString()+""));
               
              }
            }
        
             }
      catch(Exception e){
       e.printStackTrace();
     }
      return mapEventIdCount;
     }

    
    
    @SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
    public  Map<Integer, Date> returnMinEventStartDate()
    {
    	Map<Integer, Date> mapReturnMinEventStartDate = new HashMap<Integer, Date>();
       String hql="";
       List<Object[]> objList=null;
       
     try{
          Query  query=null;
          mapReturnMinEventStartDate.clear();
          Session session = getSession();
          hql = "SELECT eventschedule.eventId, MIN(eventDateTime)FROM eventschedule   LEFT OUTER JOIN eventdetails ON (eventschedule.eventId=eventdetails.eventId) WHERE eventDateTime IS NOT NULL GROUP BY eventId"; 
          query=session.createSQLQuery(hql);
          objList = query.list();
         
           if(objList.size()>0){
              for (int j = 0; j < objList.size(); j++) {
               Object[] objArr2 = objList.get(j);
               
               mapReturnMinEventStartDate.put(Integer.parseInt(objArr2[0].toString()+"".trim()),(Date)objArr2[1]);
               
              }
            }
        
             }
      catch(Exception e){
       e.printStackTrace();
     }
      return mapReturnMinEventStartDate;
     }

    @SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
    public  Map<Integer, Date> returnMaxEventEndDate()
    {
    	Map<Integer, Date> mapReturnMaxEventStartDate = new HashMap<Integer, Date>();
       String hql="";
       List<Object[]> objList=null;
       
     try{
          Query  query=null;
          mapReturnMaxEventStartDate.clear();
          Session session = getSession();
          hql = "SELECT eventschedule.eventId, MAX(eventDateTime)FROM eventschedule   LEFT OUTER JOIN eventdetails ON (eventschedule.eventId=eventdetails.eventId) WHERE eventDateTime IS NOT NULL GROUP BY eventId";
          query=session.createSQLQuery(hql);
          objList = query.list();
         
           if(objList.size()>0){
              for (int j = 0; j < objList.size(); j++) {
               Object[] objArr2 = objList.get(j);
               
               mapReturnMaxEventStartDate.put(Integer.parseInt(objArr2[0].toString()+"".trim()), (Date)objArr2[1]);
               
              }
            }
        
             }
      catch(Exception e){
       e.printStackTrace();
     }
      return mapReturnMaxEventStartDate;
     }
}
