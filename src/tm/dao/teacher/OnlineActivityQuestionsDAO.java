package tm.dao.teacher;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.teacher.OnlineActivityQuestionSet;
import tm.bean.teacher.OnlineActivityQuestions;
import tm.dao.generic.GenericHibernateDAO;

public class OnlineActivityQuestionsDAO extends GenericHibernateDAO<OnlineActivityQuestions, Integer> 
{
	public OnlineActivityQuestionsDAO() 
	{
		super(OnlineActivityQuestions.class);
	}
	@Transactional(readOnly=false)
	public List<OnlineActivityQuestions> getOnlineActivityQuestions(OnlineActivityQuestionSet onlineActivityQuestionSet)
	{
		List<OnlineActivityQuestions> lstDistSpecQuestions= new ArrayList<OnlineActivityQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("onlineActivityQuestionSet",onlineActivityQuestionSet);
			Criterion criterion2 = Restrictions.eq("status","A");
			lstDistSpecQuestions = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	
}
