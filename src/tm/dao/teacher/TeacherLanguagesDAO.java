package tm.dao.teacher;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SpokenLanguageMaster;
import tm.bean.teacher.TeacherLanguages;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherLanguagesDAO extends GenericHibernateDAO<TeacherLanguages, Integer>{

	public TeacherLanguagesDAO(){		
		super(TeacherLanguages.class);
	}
	
	
	@Transactional(readOnly=false)
	public List<String> getDistinctTeacherLanguages()
	{
		List<String> lstTeacherLanguages=new ArrayList<String>();
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			criteria.setProjection(Projections.distinct(Projections.property("language")));
			Order  sortOrderStrVal=Order.asc("language");
			criteria.addOrder(sortOrderStrVal);
			lstTeacherLanguages = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstTeacherLanguages;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherLanguages> getNullspokenLanguageMasterId()
	{
		List<TeacherLanguages> lstTeacherLanguages= new ArrayList<TeacherLanguages>();
		try 
		{
			Criterion criterion = Restrictions.isNull("spokenLanguageMaster");
			lstTeacherLanguages = findByCriteria(criterion);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstTeacherLanguages;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> getTeacherLanguagesCountByTeacherJAF(TeacherDetail teacherDetail)
	{
		List<Integer> lstTeacherLanguages= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion=Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion);
			criteria.setProjection(Projections.projectionList().add(Projections.count("teacherDetail"))) ;
			lstTeacherLanguages =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstTeacherLanguages;
		}
		
	}

}
