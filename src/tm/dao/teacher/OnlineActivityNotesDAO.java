package tm.dao.teacher;
import tm.bean.teacher.OnlineActivityNotes;
import tm.dao.generic.GenericHibernateDAO;

public class OnlineActivityNotesDAO extends GenericHibernateDAO<OnlineActivityNotes, Integer> {
	public OnlineActivityNotesDAO() 
	{
		super(OnlineActivityNotes.class);
	}

}