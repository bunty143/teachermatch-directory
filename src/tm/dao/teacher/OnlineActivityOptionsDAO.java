package tm.dao.teacher;

import tm.bean.teacher.OnlineActivityOptions;
import tm.dao.generic.GenericHibernateDAO;

public class OnlineActivityOptionsDAO extends GenericHibernateDAO<OnlineActivityOptions, Integer> 
{
	public OnlineActivityOptionsDAO() 
	{
		super(OnlineActivityOptions.class);
	}
	
}
