package tm.dao.teacher;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.teacher.OnlineActivityQuestionSet;
import tm.dao.generic.GenericHibernateDAO;

public class OnlineActivityQuestionSetDAO extends GenericHibernateDAO<OnlineActivityQuestionSet, Integer> 
{
	public OnlineActivityQuestionSetDAO() 
	{
		super(OnlineActivityQuestionSet.class);
	}
	
	@Transactional(readOnly=false)
	public OnlineActivityQuestionSet getOnlineActivityQuestionSet(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster)
	{
		OnlineActivityQuestionSet onlineActivityQuestionSet=null;
		List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=new ArrayList<OnlineActivityQuestionSet>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criterion criterion3 = Restrictions.eq("status","A");
			onlineActivityQuestionSetList = findByCriteria(criterion1,criterion2,criterion3);
			if(onlineActivityQuestionSetList.size()>0){
				onlineActivityQuestionSet=onlineActivityQuestionSetList.get(0);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return onlineActivityQuestionSet;		
	}

	@Transactional(readOnly=false)
	public List<OnlineActivityQuestionSet> getOnlineActivityQuestionSetList(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster)
	{
		List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=new ArrayList<OnlineActivityQuestionSet>();
		try{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criterion criterion3 = Restrictions.eq("status","A");
			onlineActivityQuestionSetList = findByCriteria(criterion1,criterion2,criterion3);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return onlineActivityQuestionSetList;		
	}
	@Transactional(readOnly=false)
	public List<OnlineActivityQuestionSet> getOnlineActivityQuestionSetListByHBD(JobOrder jobOrder)
	{
		List<OnlineActivityQuestionSet> onlineActivityQuestionSetList=new ArrayList<OnlineActivityQuestionSet>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			criteria.add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster()));
			criteria.add(Restrictions.eq("status","A"));
			onlineActivityQuestionSetList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return onlineActivityQuestionSetList;		
	}
}
