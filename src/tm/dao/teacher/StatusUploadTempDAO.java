package tm.dao.teacher;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.teacher.StatusUploadTemp;
import tm.dao.generic.GenericHibernateDAO;
public class StatusUploadTempDAO extends GenericHibernateDAO<StatusUploadTemp, Integer>{

	public StatusUploadTempDAO() 
	{
		super(StatusUploadTemp.class);
	}
	
	@Transactional(readOnly=false)
	public boolean deleteStatusTemp(String sessionId){
		System.out.println("::::::::::::deleteStatusTemp::::::::::::::"+sessionId);
		try{
			Criterion criterion = Restrictions.eq("sessionid",sessionId);
        	List<StatusUploadTemp> StatusUploadTemplst	=findByCriteria(criterion);
        	for(StatusUploadTemp statusUploadTemp: StatusUploadTemplst){
        		makeTransient(statusUploadTemp);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Transactional(readOnly=true)
	public List<StatusUploadTemp> getLogin(String emailAddress, String password){
		
         List <StatusUploadTemp> lstUsers = new ArrayList<StatusUploadTemp>();
		 Criterion criterion1 = Restrictions.eq("emailAddress",emailAddress.trim());
		 Criterion criterion2 = Restrictions.eq("password", password);
		 Criterion criterion3=  Restrictions.and(criterion1, criterion2);
		 lstUsers = findByCriteria(criterion3);  
		 return lstUsers;
	}
	@Transactional(readOnly=true)
	public int getRowCountTempStatus(Criterion... criterion)
	{
		List<StatusUploadTemp> StatusUploadTempList = new ArrayList<StatusUploadTemp>();
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			StatusUploadTempList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return StatusUploadTempList.size();
	}
	@Transactional(readOnly=true)
	public List<StatusUploadTemp> findByAllTempStatusCurrentSession(String sessionId)
	{
		List<StatusUploadTemp> statusUploadTempList = new ArrayList<StatusUploadTemp>();
		try{
			Session session = getSession();
			Criterion criterion2 = Restrictions.eq("sessionid",sessionId);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion2);
			statusUploadTempList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusUploadTempList;
	}
	@Transactional(readOnly=true)
	public List<StatusUploadTemp> findByTempStatus(Order order,int startPos,int limit,Criterion... criterion)
	{
		List<StatusUploadTemp> statusUploadTempList = new ArrayList<StatusUploadTemp>();
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			criteria.addOrder(order);
			statusUploadTempList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusUploadTempList;
	}
	@Transactional(readOnly=false)
	public boolean deleteAllStatusTemp(String sessionId)
	{
		try{
			Calendar c = Calendar.getInstance(); 
			c.add(Calendar.DAY_OF_YEAR, -2);  
			Date date = c.getTime();
			Criterion criterion1=Restrictions.lt("date",date);
			Criterion criterion2 = Restrictions.eq("sessionid",sessionId);
			Criterion complete3 = Restrictions.or(criterion1,criterion2);
        	List<StatusUploadTemp> StatusUploadTemplst	=findByCriteria(complete3);
        	System.out.println("StatusUploadTemplst::::"+StatusUploadTemplst.size());
			for(StatusUploadTemp StatusUploadTemp: StatusUploadTemplst){
        		makeTransient(StatusUploadTemp);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return true;
	}
	@Transactional(readOnly=true)
	public List<StatusUploadTemp> findByAllTempStatus()
	{
		List<StatusUploadTemp> statusUploadTempList = new ArrayList<StatusUploadTemp>();
		try{
			Session session = getSession();
			Calendar c = Calendar.getInstance(); // starts with today's date and time
			c.add(Calendar.DAY_OF_YEAR, -2);  // advances day by 2
			Date date = c.getTime();
			Criterion criterion=Restrictions.lt("date",date);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion);
			statusUploadTempList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusUploadTempList;
	}
}
