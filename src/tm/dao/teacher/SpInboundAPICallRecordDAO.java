package tm.dao.teacher;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.TeacherDetail;
import tm.bean.teacher.SpInboundAPICallRecord;
import tm.dao.generic.GenericHibernateDAO;

public class SpInboundAPICallRecordDAO extends GenericHibernateDAO<SpInboundAPICallRecord, Integer>{

	public SpInboundAPICallRecordDAO(){		
		super(SpInboundAPICallRecord.class);
	}
	
	
	@Transactional(readOnly=false)
	public List<SpInboundAPICallRecord> getDetailsByTID(TeacherDetail teacherDetail)
	{
		List<SpInboundAPICallRecord> lstSPAPI = new ArrayList<SpInboundAPICallRecord>();
		try{
			Criterion criterion1	=	Restrictions.eq("teacherDetail", teacherDetail);
			lstSPAPI				=	findByCriteria(criterion1);
			
		} catch (Exception exception){
			exception.printStackTrace();
		}
		finally
		{
			return lstSPAPI;
		}
	}
	@Transactional(readOnly=false)
	public List<SpInboundAPICallRecord> getDetailsByTeacherList(List<TeacherDetail> teacherDetails)
	{
		List<SpInboundAPICallRecord> lstSpInboundAPICallRecord = new ArrayList<SpInboundAPICallRecord>();
		try{
			if(teacherDetails.size()>0){
				Criterion criterion1	=	Restrictions.in("teacherDetail", teacherDetails);
				lstSpInboundAPICallRecord				=	findByCriteria(criterion1);
			}
			
		} catch (Exception exception){
			exception.printStackTrace();
		}finally{
			return lstSpInboundAPICallRecord;
		}
	}
	
	@Transactional(readOnly=false)
	public List<SpInboundAPICallRecord> getDetailsByTeacherList(String teacherDetails)
	{
		List<SpInboundAPICallRecord> lstSpInboundAPICallRecord = new ArrayList<SpInboundAPICallRecord>();
		try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass(),"siacr");
				//criteria.add(Property.forName("siacr.teacherDetail.teacherId").in(teacherDetails));
				criteria.add(Restrictions.sqlRestriction("this_.teacherId in("+teacherDetails+")"));
				lstSpInboundAPICallRecord=criteria.list();
		} catch (Exception exception){
			exception.printStackTrace();
		}
			return lstSpInboundAPICallRecord;
	}
	
	@Transactional(readOnly=false)
	 public List<TeacherDetail> getAllSpInboundTeacherList()
	 {
	  List<TeacherDetail> lstSpInboundTeacher = new ArrayList<TeacherDetail>();
	  try{
	   
	   Session session = getSession();
	   Criteria criteria = session.createCriteria(getPersistentClass());
	   criteria.setProjection(Projections.groupProperty("teacherDetail"));
	   lstSpInboundTeacher = criteria.list();
	  } catch (Exception exception){
	   exception.printStackTrace();
	  }finally{
	   return lstSpInboundTeacher;
	  }
	 }
	
	
}
