package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.StatusWiseEmailSection;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class StatusWiseEmailSectionDAO extends GenericHibernateDAO<StatusWiseEmailSection, Integer>{

	public StatusWiseEmailSectionDAO() {
		super(StatusWiseEmailSection.class);

	}

	@Transactional(readOnly=false)
	public List<StatusWiseEmailSection> findByDistrict(DistrictMaster districtMaster)
	{
		List<StatusWiseEmailSection> emailSections = new ArrayList<StatusWiseEmailSection>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion c1 = Restrictions.eq("districtId", districtMaster.getDistrictId());
			criteria.add(c1);
			
			emailSections = criteria.list();
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return emailSections;
	}
	
	
	@Transactional(readOnly=false)
	public List<StatusWiseEmailSection> findByDistrictAndSectionName(DistrictMaster districtMaster,String Status)
	{
		List<StatusWiseEmailSection> emailSections = new ArrayList<StatusWiseEmailSection>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion c1 = Restrictions.eq("districtId", districtMaster.getDistrictId());
			Criterion c2 = Restrictions.eq("sectionName", Status).ignoreCase();
			criteria.add(c1);
			criteria.add(c2);
			
			emailSections = criteria.list();
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return emailSections;
	}
	@Transactional(readOnly=false)
	public List<StatusWiseEmailSection> findStatusWiseEmailSectionByHBD(UserMaster userMaster)
	{
		List<StatusWiseEmailSection> emailSections = new ArrayList<StatusWiseEmailSection>();
		try
		{
			if(userMaster!=null){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				if(userMaster.getDistrictId()!=null){
					criteria.add(Restrictions.eq("districtId",userMaster.getDistrictId().getDistrictId()));
				}else{
					criteria.add(Restrictions.isNull("districtId"));
				}
				if(userMaster.getBranchMaster()!=null){
					criteria.add(Restrictions.eq("branchId",userMaster.getBranchMaster().getBranchId()));
				}else{
					criteria.add(Restrictions.isNull("branchId"));
				}
				if(userMaster.getHeadQuarterMaster()!=null){
					criteria.add(Restrictions.eq("headQuarterId",userMaster.getHeadQuarterMaster().getHeadQuarterId()));
				}else{
					criteria.add(Restrictions.isNull("headQuarterId"));
				}
				criteria.add(Restrictions.eq("status", "A"));
				criteria.addOrder(Order.asc("sectionName"));
				emailSections = criteria.list();
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return emailSections;
	}
}
