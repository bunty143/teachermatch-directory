package tm.dao;

import tm.bean.JobDenyApprovalHistory;
import tm.dao.generic.GenericHibernateDAO;

public class JobDenyApprovalHistoryDAO extends GenericHibernateDAO<JobDenyApprovalHistory, Integer>{
		public JobDenyApprovalHistoryDAO() {
			super(JobDenyApprovalHistory.class);
		}
}
