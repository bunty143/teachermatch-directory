package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherInvolvement;
import tm.bean.TeacherRole;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherInvolvementDAO extends GenericHibernateDAO<TeacherInvolvement, Integer>
{
	public TeacherInvolvementDAO() 
	{
		super(TeacherInvolvement.class);
	}
	@Transactional(readOnly=false)
	public List<TeacherInvolvement> findTeacherInvolvementByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherInvolvement> lstTeacherInvolvement = new ArrayList<TeacherInvolvement>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstTeacherInvolvement = findByCriteria(Order.desc("involvementId"), criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherInvolvement;
	}

	@Transactional(readOnly=false)
	public List<TeacherInvolvement> findSortedTeacherInvolvementByTeacher(Order  sortOrderStrVal,TeacherDetail teacherDetail)
	{
		List<TeacherInvolvement> lstTeacherInvolvement = new ArrayList<TeacherInvolvement>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstTeacherInvolvement = findByCriteria(sortOrderStrVal, criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherInvolvement;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherInvolvement> findByTeacherDetailsObj(List<TeacherDetail> teacherDetailsObj)
	{
		List<TeacherInvolvement> lstTeacherInvolvement = new ArrayList<TeacherInvolvement>();
		
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetailsObj);
			lstTeacherInvolvement = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherInvolvement==null || lstTeacherInvolvement.size()==0)
			return null;
		else
			return lstTeacherInvolvement;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> getTeacherInvolvementCountByTeacherJAF(TeacherDetail teacherDetail)
	{
		List<Integer> lstTeacherInvolvement= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			
			criteria.setProjection(Projections.projectionList().add(Projections.count("teacherDetail"))) ;
			lstTeacherInvolvement =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstTeacherInvolvement;
		}
		
	}
	
}
