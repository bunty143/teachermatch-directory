package tm.dao;

import tm.bean.DocumentCategoryMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DocumentCategoryMasterDAO extends GenericHibernateDAO<DocumentCategoryMaster, Integer> 
{
	public DocumentCategoryMasterDAO() {
		super(DocumentCategoryMaster.class);
	}
}
