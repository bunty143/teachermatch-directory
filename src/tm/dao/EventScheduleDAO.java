package tm.dao;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EventDetails;
import tm.bean.EventSchedule;
import tm.dao.generic.GenericHibernateDAO;

public class EventScheduleDAO extends GenericHibernateDAO<EventSchedule,Integer>{
	public EventScheduleDAO() {
		super(EventSchedule.class);
	}
	
	@Transactional(readOnly=false)
	public List<EventSchedule> findByEventDetail(EventDetails eventDetails) {
		List<EventSchedule> eventList = new ArrayList<EventSchedule>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("eventDetails",eventDetails);
			eventList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eventList;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")	
	public List listEventDetailsl()
	{
		List lstevntId=new ArrayList();
		try{
			Session session = getSession();
			String sql =" SELECT eventId, COUNT(*) AS appliedCnt from eventschedule  group by eventId  HAVING appliedCnt>1 ";
			
			Query query = session.createSQLQuery(sql);
			lstevntId = query.list();
	    }catch(Exception e)
		   {
			 System.out.println(e);
		  }
		  return lstevntId;
	}
	
}
