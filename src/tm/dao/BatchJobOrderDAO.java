package tm.dao;
import tm.bean.BatchJobOrder;
import tm.dao.generic.GenericHibernateDAO;

public class BatchJobOrderDAO extends GenericHibernateDAO<BatchJobOrder, Integer> 
{
	public BatchJobOrderDAO() 
	{
		super(BatchJobOrder.class);
	}


}
