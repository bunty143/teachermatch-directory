package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictDocumentUpload;
import tm.bean.DistrictSpecificDocuments;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictDocumentUploadDAO extends GenericHibernateDAO<DistrictDocumentUpload, Integer> 
{
	public DistrictDocumentUploadDAO() {
		super(DistrictDocumentUpload.class);
	}
	
	@Transactional(readOnly=false)
	public List<DistrictDocumentUpload> findByFiles(List<DistrictSpecificDocuments> districtSpecificDocuments){
		List<DistrictDocumentUpload> jobDocumentsList =  new ArrayList<DistrictDocumentUpload>();
		try{
			Criterion criterion2 = Restrictions.in("districtSpecificDocuments",districtSpecificDocuments);
			jobDocumentsList = findByCriteria(Order.asc("districtSpecificDocuments"),criterion2);
		}catch(Exception e){
			e.printStackTrace();
		}			
	  return jobDocumentsList;	
	}
	

	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List findUniqueUploadedFileByDSD(List<Integer> jftList) 
	{
		Session session = getSession();
		String sql = "";
		sql = "SELECT ddu.uploadId, ddu.uploadedFileName  FROM  `districtdocumentupload` ddu JOIN districtspecificdocuments dsd ON ddu.documentId= dsd.documentId " +
		        " WHERE   ";
			sql=sql+"dsd.documentId  IN (:jftList) GROUP BY ddu.uploadedFileName order by ddu.uploadId" ;
		try {
			Query query = session.createSQLQuery(sql);
			query.setParameterList("jftList", jftList);

			List<Object[]> rows = query.list();
			return rows;
		  } catch (HibernateException e) {
			e.printStackTrace();
		  }
		return null;
	}
	

	
	@Transactional(readOnly=false)
	public List<DistrictDocumentUpload> findByuploadIds(List<Integer> uploadIds){
		List<DistrictDocumentUpload> jobDocumentsList =  new ArrayList<DistrictDocumentUpload>();
		try{
			Criterion criterion2 = Restrictions.in("uploadId",uploadIds);
			jobDocumentsList = findByCriteria(Order.asc("districtSpecificDocuments"),criterion2);
		}catch(Exception e){
			e.printStackTrace();
		}			
	  return jobDocumentsList;	
	}
	
}
