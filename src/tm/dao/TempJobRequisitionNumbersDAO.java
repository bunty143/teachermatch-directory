package tm.dao;

import tm.bean.JobRequisitionNumbers;
import tm.bean.TempJobRequisitionNumbers;
import tm.dao.generic.GenericHibernateDAO;

public class TempJobRequisitionNumbersDAO extends GenericHibernateDAO<TempJobRequisitionNumbers, Integer> {

	TempJobRequisitionNumbersDAO()
	{
		super(TempJobRequisitionNumbers.class);
	}
}
