package tm.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.DistrictPortfolioConfig;
import tm.bean.JobOrder;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.dao.generic.GenericHibernateDAO;
 
public class DistrictPortfolioConfigDAO extends GenericHibernateDAO<DistrictPortfolioConfig, Integer>{
	public DistrictPortfolioConfigDAO() {
		super(DistrictPortfolioConfig.class);
	}

	@Transactional(readOnly=false)
	public DistrictPortfolioConfig getPortfolioConfig(DistrictMaster  districtMaster,String candidateType,JobOrder jobOrder)
	{	
		DistrictPortfolioConfig districtPortfolioConfig=null;
		List<DistrictPortfolioConfig> districtPortfolioConfigs=new ArrayList<DistrictPortfolioConfig>(); 	
		try
		{
			Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion1 = Restrictions.eq("candidateType",candidateType);
			Criterion criterion2 = Restrictions.isNull("jobCategoryMaster");			

			if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null){
				Criterion criterion3 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
				districtPortfolioConfigs = findByCriteria(criterion,criterion1,criterion3);
				
				
				boolean mainJobcategoryWise = false;
				if(districtPortfolioConfigs!=null && districtPortfolioConfigs.size()==0)
					mainJobcategoryWise = true;
				System.out.println(">>>>    "+mainJobcategoryWise);
				if(mainJobcategoryWise && jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null){
					JobCategoryMaster jobCategoryMaster = new JobCategoryMaster();
					jobCategoryMaster.setJobCategoryId(jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
					Criterion criterion4 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
					districtPortfolioConfigs = findByCriteria(criterion,criterion1,criterion4);
				}
			}
			else
				districtPortfolioConfigs = findByCriteria(criterion,criterion1,criterion2);
			
			boolean districtWise = false;
			if(districtPortfolioConfigs==null)
				districtWise = true;
			
			if(districtPortfolioConfigs!=null && districtPortfolioConfigs.size()==0)
				districtWise = true;
			
			if(districtWise)
			districtPortfolioConfigs = findByCriteria(criterion,criterion1,criterion2);
			
			if(districtPortfolioConfigs.size()==1)
				districtPortfolioConfig=districtPortfolioConfigs.get(0);
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		
		//System.out.println("districtPortfolioConfig   "+districtPortfolioConfig.getDistrictPortfolioConfigId());
		return districtPortfolioConfig;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictPortfolioConfig> getPortfolioConfigs(DistrictMaster  districtMaster)
	{	
		List<DistrictPortfolioConfig> districtPortfolioConfigs=new ArrayList<DistrictPortfolioConfig>(); 	
		try
		{
			Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
			districtPortfolioConfigs = findByCriteria(criterion);
			
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return districtPortfolioConfigs;
	}
	
	@Transactional(readOnly=false)
	public DistrictPortfolioConfig getPortfolioConfigByJobCategory(DistrictMaster  districtMaster,String candidateType,JobCategoryMaster jobCategoryMaster)
	{	
		DistrictPortfolioConfig districtPortfolioConfig=null;
		List<DistrictPortfolioConfig> districtPortfolioConfigs=new ArrayList<DistrictPortfolioConfig>(); 	
		try
		{
			Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion1 = Restrictions.eq("candidateType",candidateType);
			Criterion criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			districtPortfolioConfigs = findByCriteria(criterion,criterion1,criterion2);
			if(districtPortfolioConfigs.size()==1)
				districtPortfolioConfig=districtPortfolioConfigs.get(0);
		} 
		catch (Exception e){
			//e.printStackTrace();
		}	
		return districtPortfolioConfig;
	}

	
	@Transactional(readOnly=false)
	public List<DistrictPortfolioConfig> getPortfolioConfigsByDistrictAndCandidate(DistrictMaster  districtMaster,String candidateType)
	{	
		List<DistrictPortfolioConfig> districtPortfolioConfigs=new ArrayList<DistrictPortfolioConfig>(); 	
		try
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("candidateType",candidateType);
			districtPortfolioConfigs = findByCriteria(criterion1,criterion2);
			
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return districtPortfolioConfigs;
	}

	@Transactional
	public List<DistrictPortfolioConfig> getAlldistrictDspq(List<DistrictMaster> districtMasters){
		List<DistrictPortfolioConfig> districtPortfolioConfigs= new ArrayList<DistrictPortfolioConfig>();
		try {
			Criterion criterion = Restrictions.in("districtMaster",districtMasters);
			districtPortfolioConfigs = findByCriteria(criterion);
		} catch (Exception e) {}
		return districtPortfolioConfigs;
	}
 	@Transactional(readOnly=false)
	public List<DistrictPortfolioConfig> getPortfolioConfig(String candidateType,JobOrder jobOrder)
	{	
		List<DistrictPortfolioConfig> lstDistrictPortfolioConfig=new ArrayList<DistrictPortfolioConfig>(); 
		
		Criterion criterionH =null;
		Criterion criterionD =null;
		Criterion criterionCT = Restrictions.eq("candidateType",candidateType);
		
		if(jobOrder!=null)
		{
			HeadQuarterMaster headQuarterMaster=null;
			if(jobOrder!=null && jobOrder.getHeadQuarterMaster()!=null)
			{
				headQuarterMaster=jobOrder.getHeadQuarterMaster();
				System.out.println("headQuarterMaster Id "+headQuarterMaster.getHeadQuarterId());
				criterionH = Restrictions.eq("headQuarterMaster",headQuarterMaster);
				System.out.println("DAO HQ");
			}
			
			DistrictMaster districtMaster=null;
			if(jobOrder!=null && jobOrder.getDistrictMaster()!=null)
			{
				districtMaster=jobOrder.getDistrictMaster();
				criterionD = Restrictions.eq("districtMaster",districtMaster);
				System.out.println("DAO DM");
			}
			
			try
			{
				if(headQuarterMaster!=null)
				{
					lstDistrictPortfolioConfig=findByCriteria(criterionCT,criterionH);
					System.out.println("DAO HQ 1");
				}
				else
				{
					Criterion criterionJobcat = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
					lstDistrictPortfolioConfig=findByCriteria(criterionCT,criterionD,criterionJobcat);
					
					if(lstDistrictPortfolioConfig==null || lstDistrictPortfolioConfig.size()==0){
					if(jobOrder.getJobCategoryMaster().getAttachDSPQFromJC()!=null && jobOrder.getJobCategoryMaster().getAttachDSPQFromJC()){
						if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null){
					      Criterion criteriaJC=Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster().getParentJobCategoryId());
					      lstDistrictPortfolioConfig=findByCriteria(criterionCT,criterionD,criteriaJC);
							}
						}
					 }
					
					if(lstDistrictPortfolioConfig==null || lstDistrictPortfolioConfig.size()==0){
						Criterion criteriaJ=Restrictions.isNull("jobCategoryMaster");
						lstDistrictPortfolioConfig=findByCriteria(criterionCT,criterionD,criteriaJ);
						//lstDistrictPortfolioConfig=findByCriteria(criterionCT,criterionD);
					}
					
					System.out.println("DAO DM 1 ");
					System.out.println("##########lstDistrictPortfolioConfig======"+lstDistrictPortfolioConfig);
					if(lstDistrictPortfolioConfig!=null && lstDistrictPortfolioConfig.size()>0){
						System.out.println("##########lstDistrictPortfolioConfig size======"+lstDistrictPortfolioConfig.size());
						for(DistrictPortfolioConfig dc:lstDistrictPortfolioConfig)
						System.out.println("#################Districtport ID::::"+dc.getDistrictPortfolioConfigId());
					}
					System.out.println("#############JobcategoryId::::::::::"+jobOrder.getJobCategoryMaster().getJobCategoryId());
				}
			} 
			catch (Exception e){
				e.printStackTrace();
			}
			finally
			{
				
			}
		}
		
		return lstDistrictPortfolioConfig;
		
	}
	
	@Transactional
	public List<DistrictPortfolioConfig> getDSPQByHeadquater(HeadQuarterMaster headQuarterMaster){
		List<DistrictPortfolioConfig> districtPortfolioConfigs= new ArrayList<DistrictPortfolioConfig>();
		try {
			Criterion criterion = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			districtPortfolioConfigs = findByCriteria(Order.asc("dspqName"),criterion);
		} catch (Exception e) {}
		return districtPortfolioConfigs;
	}
	@Transactional
	public List<DistrictPortfolioConfig> getDSPQByHeadquaterAndName(HeadQuarterMaster headQuarterMaster,List<String> dspqs){
		List<DistrictPortfolioConfig> districtPortfolioConfigs= new ArrayList<DistrictPortfolioConfig>();
		try {
			Criterion criterion = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion1 = Restrictions.in("dspqName",dspqs);
			districtPortfolioConfigs = findByCriteria(Order.asc("dspqName"),criterion,criterion1);
		} catch (Exception e) {}
		return districtPortfolioConfigs;
	}
@Transactional(readOnly=false)
	public DistrictPortfolioConfig getPortfolioConfigByJobCategory(DistrictMaster  districtMaster,JobCategoryMaster jobCategoryMaster)
	{	
		DistrictPortfolioConfig districtPortfolioConfig=null;
		List<DistrictPortfolioConfig> districtPortfolioConfigs=new ArrayList<DistrictPortfolioConfig>(); 	
		try
		{
			Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion1 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			districtPortfolioConfigs = findByCriteria(criterion,criterion1);
			if(districtPortfolioConfigs.size()>0)
				districtPortfolioConfig=districtPortfolioConfigs.get(0);
		} 
		catch (Exception e){
			//e.printStackTrace();
		}	
		return districtPortfolioConfig;
	}
	
	@Transactional
	public List<DistrictPortfolioConfig> getDSPQByHeadquaterAndName(HeadQuarterMaster headQuarterMaster,String dspqs){
		List<DistrictPortfolioConfig> districtPortfolioConfigs= new ArrayList<DistrictPortfolioConfig>();
		try {
			Criterion criterion = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion1 = Restrictions.eq("dspqName",dspqs);
			districtPortfolioConfigs = findByCriteria(criterion,criterion1);
		} catch (Exception e) {}
		return districtPortfolioConfigs;
	}
//Added by Kumar avinash
	@Transactional(readOnly=false)	
	 	public List<DistrictPortfolioConfig>   getAlldistrictDsp_Opp(List<DistrictMaster> districtMasters) throws Exception 
	 	{	  
 		List<Object[]> objList=null;
	 	List<DistrictPortfolioConfig> districtPortfolioConfigs= new ArrayList<DistrictPortfolioConfig>();
	  	String districtMasterId="";
	 	int p=0;
			for(DistrictMaster districtMaster: districtMasters){
				if(p==0){
					districtMasterId=districtMaster.getDistrictId()+"";
				}else{
					districtMasterId+=","+districtMaster.getDistrictId()+"";
				}
	 			p++; 
			}	
	    		 try{
	 		     Query  query=null;
	 	 	     Session session = getSession();
	 	  	    if(districtMasters!=null&&districtMasters.size()>0){ 
	 	     	String   hql = "SELECT dp.districtId,dp.jobCategoryId FROM  districtportfolioconfig dp "
	  		       +   "join districtmaster dm on dm.districtId= dp.districtId "
	  		       +   "join jobcategorymaster jcm on jcm.jobCategoryId= dp.jobCategoryId "
	  		         + " WHERE   dp.districtId  in('"+districtMasterId+"')";
	 		        query=session.createSQLQuery(hql);
	 		        objList  = query.list(); 
	 		       DistrictPortfolioConfig districtPortfolioConfig=null;
	 		        if(objList.size()>0){
	 		           for (int j = 0; j < objList.size(); j++) {
	 		       	    Object[] objArr2 = objList.get(j);
	 		       	districtPortfolioConfig = new DistrictPortfolioConfig();
	 		       	
	 		    	
	 		    	if(objArr2[1]!=null){
		 		        JobCategoryMaster jobCategoryMaster =new JobCategoryMaster();
		 		        jobCategoryMaster.setJobCategoryId(objArr2[1] == null ? null : Integer.parseInt(objArr2[1].toString()));
		 		        districtPortfolioConfig.setJobCategoryMaster(jobCategoryMaster);
	 		    	}
	 		    	if(objArr2[0]!=null){
	 		    		DistrictMaster districtMaster=new DistrictMaster();
	 		    		districtMaster.setDistrictId(objArr2[0] == null ? null : Integer.parseInt(objArr2[0].toString()));
	 		    		districtPortfolioConfig.setDistrictMaster(districtMaster);
	 		    	} 
	 		       
	 		          districtPortfolioConfigs.add(districtPortfolioConfig);
	 		             }
	 		            }
	 		          }
	 		        }
	 		   catch(Exception e){
	 		    e.printStackTrace();
	 		  }
	 		   return districtPortfolioConfigs;
	 	}		
	 
	
	@Transactional(readOnly=false)
	public List<DistrictPortfolioConfig> getPortfolioConfigByJobOrder(JobOrder jobOrder)
	{	
		List<DistrictPortfolioConfig> lstDistrictPortfolioConfig=new ArrayList<DistrictPortfolioConfig>(); 
		
		Criterion criterionH =null;
		Criterion criterionD =null;
		
		if(jobOrder!=null)
		{
			HeadQuarterMaster headQuarterMaster=null;
			if(jobOrder!=null && jobOrder.getHeadQuarterMaster()!=null)
			{
				headQuarterMaster=jobOrder.getHeadQuarterMaster();
				System.out.println("headQuarterMaster Id "+headQuarterMaster.getHeadQuarterId());
				criterionH = Restrictions.eq("headQuarterMaster",headQuarterMaster);
				System.out.println("DAO HQ");
			}
			
			DistrictMaster districtMaster=null;
			if(jobOrder!=null && jobOrder.getDistrictMaster()!=null)
			{
				districtMaster=jobOrder.getDistrictMaster();
				criterionD = Restrictions.eq("districtMaster",districtMaster);
				System.out.println("DAO DM");
			}
			
			try
			{
				if(headQuarterMaster!=null)
				{
					System.out.println("DAO HQ 1");
				}
				else
				{
					System.out.println("jobOrder.getJobCategoryMaster()"+jobOrder.getJobCategoryMaster());
					Criterion criterionJobcat = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
					lstDistrictPortfolioConfig=findByCriteria(criterionD,criterionJobcat);
					
					if(lstDistrictPortfolioConfig==null || lstDistrictPortfolioConfig.size()==0){
					if(jobOrder.getJobCategoryMaster().getAttachDSPQFromJC()!=null && jobOrder.getJobCategoryMaster().getAttachDSPQFromJC()){
						if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null){
						  System.out.println("#########jobOrder.getJobCategoryMaster().getParentJobCategoryId()"+jobOrder.getJobCategoryMaster().getParentJobCategoryId());
					      Criterion criteriaJC=Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster().getParentJobCategoryId());
					      lstDistrictPortfolioConfig=findByCriteria(criterionD,criteriaJC);
							}
						}
					 }
					
					if(lstDistrictPortfolioConfig==null || lstDistrictPortfolioConfig.size()==0){
						Criterion criteriaJ=Restrictions.isNull("jobCategoryMaster");
						lstDistrictPortfolioConfig=findByCriteria(criterionD,criteriaJ);
						//lstDistrictPortfolioConfig=findByCriteria(criterionCT,criterionD);
					}
					
					System.out.println("DAO DM 1 ");
					System.out.println("$$$$$$$$$$$$$$lstDistrictPortfolioConfig======"+lstDistrictPortfolioConfig);
					if(lstDistrictPortfolioConfig!=null && lstDistrictPortfolioConfig.size()>0){
						System.out.println("##########lstDistrictPortfolioConfig size======"+lstDistrictPortfolioConfig.size());
						for(DistrictPortfolioConfig dc:lstDistrictPortfolioConfig)
						System.out.println("#################Districtport ID::::"+dc.getDistrictPortfolioConfigId());
					}
					System.out.println("#############JobcategoryId::::::::::"+jobOrder.getJobCategoryMaster().getJobCategoryId());
				}
			} 
			catch (Exception e){
				e.printStackTrace();
			}
			finally
			{
				
			}
		}
		
		return lstDistrictPortfolioConfig;
		
	}
	
}
