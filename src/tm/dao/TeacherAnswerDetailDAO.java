package tm.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherAnswerDetail;
import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.CompetencyMaster;
import tm.bean.master.DomainMaster;
import tm.bean.master.ObjectiveMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherAnswerDetailDAO extends GenericHibernateDAO<TeacherAnswerDetail, Integer> 
{

	public TeacherAnswerDetailDAO() {
		super(TeacherAnswerDetail.class);
	}

	@Transactional(readOnly=false)
	public List<TeacherAnswerDetail> findTADetailbyQuestion(List<TeacherAssessmentQuestion> lstQuestion)
	{
		List<TeacherAnswerDetail> lstTeacherAnswerDetails = null;
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherAssessmentQuestion",lstQuestion);

			lstTeacherAnswerDetails = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherAnswerDetails;
	}


	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to count skipped questions.
	 */
	@Transactional(readOnly=true)
	public List<TeacherAnswerDetail> findNoOfSkippedQuestion(AssessmentDetail assessmentDetail,TeacherDetail teacherDetail,TeacherAssessmentdetail teacherAssessmentdetail) 
	{
		System.out.println("ddddddddddddddddddddddd teacherAnswerDetails: ");
		System.out.println("assessmentId: "+assessmentDetail.getAssessmentId());
		System.out.println("teacherId: "+teacherDetail.getTeacherId());
		System.out.println("teacherAssessmentId: "+teacherAssessmentdetail.getTeacherAssessmentId());
		
		List<TeacherAnswerDetail> teacherAnswerDetails = null;
		try {
			
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			
			Criterion criterion = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("teacherAssessmentdetail",teacherAssessmentdetail);
			Criterion criterion3 = Restrictions.isNull("selectedOptions");
			Criterion criterion4 = Restrictions.eq("selectedOptions","");
			Criterion criterion5 = Restrictions.or(criterion3, criterion4);

			Criterion criterion6 = Restrictions.eq("insertedText","");
			Criterion criterion7 = Restrictions.isNull("insertedText");
			Criterion criterion8 = Restrictions.or(criterion6, criterion7);

			Criterion criterion9 = Restrictions.eq("insertedRanks","");
			Criterion criterion10 = Restrictions.isNull("insertedRanks");
			
			//Criterion criterion11 = Restrictions.between("createdDateTime", sDate, eDate);


			//teacherAnswerDetails = findByCriteria(criterion,criterion1,criterion2,criterion5,criterion8,criterion11);
			teacherAnswerDetails = findByCriteria(criterion,criterion1,criterion2,criterion5,criterion8);

			System.out.println("qqqqqqqqqqqqqqqqqqqqq teacherAnswerDetails: "+teacherAnswerDetails.size());
			if(teacherAnswerDetails.size()>0)
			{
				TeacherAnswerDetail tad = teacherAnswerDetails.get(0);
				System.out.println("teacherAnswerDetail: "+tad);
				System.out.println("getAnswerId: "+tad.getAnswerId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAnswerDetails;

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find Percentage Obtained For Base.
	 */
	@Transactional(readOnly=false)
	public List findPercentageObtainedForBase(TeacherDetail teacherDetail,List<CompetencyMaster> competencyMasters)
	{
		try 
		{
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			//System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			//System.out.println(" LLL : "+teacherDetail.getFirstName()+" "+teacherDetail.getTeacherId());
				Session session = getSession();
				if(teacherDetail==null)
				{
										
					Criteria crit = session.createCriteria(getPersistentClass());
					Criteria c=crit.createCriteria("teacherAssessmentQuestion");
					c.add(Restrictions.between("createdDateTime", sDate, eDate));
					c.add(Restrictions.in("competencyMaster", competencyMasters));
					List result = crit.add(Restrictions.isNull("jobOrder"))
					.setProjection(Projections.projectionList()
							.add(Projections.sum("totalScore"))
							.add(Projections.sum("maxMarks"))           
					).list();
					
					//System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
					return result;
					
				}else
				{
										
					Criteria crit = session.createCriteria(getPersistentClass());
					crit.add(Restrictions.eq("teacherDetail", teacherDetail));
					Criteria c=crit.createCriteria("teacherAssessmentQuestion");

					c.add(Restrictions.between("createdDateTime", sDate, eDate));
					c.add(Restrictions.in("competencyMaster", competencyMasters));
					List result = crit.add(Restrictions.isNull("jobOrder"))
					.setProjection(Projections.projectionList()
							.add(Projections.sum("totalScore"))
							.add(Projections.sum("maxMarks"))           
					).list();
					
					//System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
					return result;
				}
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return null;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find Percentage Obtained For Base.
	 */
	@Transactional(readOnly=false)
	public List findPercentageObtainedForBaseCompetencyWise(TeacherDetail teacherDetail,CompetencyMaster competencyMaster)
	{
		try 
		{

			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			
			Session session = getSession();
			if(teacherDetail==null)
			{
				Criteria crit = session.createCriteria(getPersistentClass());

				Criteria c=crit.createCriteria("teacherAssessmentQuestion");

				c.add(Restrictions.eq("competencyMaster",competencyMaster));
				c.add(Restrictions.between("createdDateTime", sDate, eDate));
				
				List result = crit.add(Restrictions.isNull("jobOrder"))
				.setProjection(Projections.projectionList()
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))           
				).list();

				//System.out.println("ddd: "+result);
				return result;
			}else
			{

				Criteria crit = session.createCriteria(getPersistentClass());

				Criteria c=crit.createCriteria("teacherAssessmentQuestion");
				c.add(Restrictions.between("createdDateTime", sDate, eDate));
				
				c.add(Restrictions.eq("competencyMaster",competencyMaster));

				List result = crit.add(Restrictions.isNull("jobOrder"))
				.add(Restrictions.eq("teacherDetail", teacherDetail))    
				.setProjection(Projections.projectionList()
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))           
				).list();

				return result;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find Percentile Obtained For Base.
	 */
	@Transactional(readOnly=false)
	public List findPercentileForBaseCompetencyWise(TeacherDetail teacherDetail,CompetencyMaster competencyMaster)
	{
		try 
		{

			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			
			Session session = getSession();
			if(teacherDetail==null)
			{
				Criteria crit = session.createCriteria(getPersistentClass());

				Criteria c=crit.createCriteria("teacherAssessmentQuestion");

				c.add(Restrictions.eq("competencyMaster",competencyMaster));
				c.add(Restrictions.between("createdDateTime", sDate, eDate));
				
				List result = crit.add(Restrictions.isNull("jobOrder"))
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))           
				).list();

				//System.out.println("ddd: "+result);
				return result;
			}else
			{

				Criteria crit = session.createCriteria(getPersistentClass());

				Criteria c = crit.createCriteria("teacherAssessmentQuestion");

				c.add(Restrictions.eq("competencyMaster",competencyMaster));
				c.add(Restrictions.between("createdDateTime", sDate, eDate));
				
				List result = crit.add(Restrictions.isNull("jobOrder"))
				.add(Restrictions.eq("teacherDetail", teacherDetail))    
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))           
				).list();

				return result;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find Percentile Obtained For Base.
	 */
	@Transactional(readOnly=false)
	public List findPercentileForBase(TeacherDetail teacherDetail,List<CompetencyMaster> competencyMasters)
	{
		try 
		{
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			
				Session session = getSession();
				if(teacherDetail==null)
				{
					/*List result = session.createCriteria(getPersistentClass())       
					.add(Restrictions.isNull("jobOrder"))
					.setProjection(Projections.projectionList()
							.add(Projections.groupProperty("teacherDetail"))
							.add(Projections.sum("totalScore"))
							.add(Projections.sum("maxMarks"))           
					).list();

					return result;*/
					
					Criteria crit = session.createCriteria(getPersistentClass());
					Criteria c = crit.createCriteria("teacherAssessmentQuestion");
					c.add(Restrictions.between("createdDateTime", sDate, eDate));
					c.add(Restrictions.in("competencyMaster", competencyMasters));
					
					List result = crit.add(Restrictions.isNull("jobOrder"))
					.setProjection(Projections.projectionList()
							.add(Projections.groupProperty("teacherDetail"))
							.add(Projections.sum("totalScore"))
							.add(Projections.sum("maxMarks"))           
					).list();

					return result;
				}else
				{
					/*List result = session.createCriteria(getPersistentClass())       
					.add(Restrictions.eq("teacherDetail", teacherDetail))    
					.add(Restrictions.isNull("jobOrder"))
					.setProjection(Projections.projectionList()
							.add(Projections.groupProperty("teacherDetail"))
							.add(Projections.sum("totalScore"))
							.add(Projections.sum("maxMarks"))           
					).list();

					return result;*/
					
					Criteria crit = session.createCriteria(getPersistentClass());
					Criteria c = crit.createCriteria("teacherAssessmentQuestion");
					c.add(Restrictions.between("createdDateTime", sDate, eDate));
					
					List result = crit.add(Restrictions.isNull("jobOrder"))
					.add(Restrictions.eq("teacherDetail", teacherDetail))    
					.setProjection(Projections.projectionList()
							.add(Projections.groupProperty("teacherDetail"))
							.add(Projections.sum("totalScore"))
							.add(Projections.sum("maxMarks"))           
					).list();

					return result;
				}
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}
	
	
	@Transactional(readOnly=false)
	public List findSumOfAnsByDomainAndTeacher(TeacherDetail teacherDetail,DomainMaster domainMaster )
	{
		try 
		{

			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			
			Session session = getSession();
			if(teacherDetail==null)
			{
				Criteria crit = session.createCriteria(getPersistentClass());

				Criteria c=crit.createCriteria("teacherAssessmentQuestion");

				c.add(Restrictions.eq("domainMaster",domainMaster));
				c.add(Restrictions.between("createdDateTime", sDate, eDate));

				List result = crit.add(Restrictions.isNull("jobOrder"))
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))           
				).list();

				//System.out.println("ddd: "+result);
				return result;
			}else
			{
				Criteria crit = session.createCriteria(getPersistentClass());
				Criteria c=crit.createCriteria("teacherAssessmentQuestion");
				c.add(Restrictions.eq("domainMaster",domainMaster));
				c.add(Restrictions.between("createdDateTime", sDate, eDate));
				
				List result = crit.add(Restrictions.isNull("jobOrder"))
				.add(Restrictions.eq("teacherDetail", teacherDetail))    
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))           
				).list();

				return result;
			}
		} 
		catch (Exception e) 
		{
			System.out.println("dddd");
			e.printStackTrace();
		}		
		return null;
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find job specific assessment Question Answers.
	 */
	@Transactional(readOnly=false)
	public List<TeacherAnswerDetail> findAssessmentQuestionAnswers(TeacherDetail teacherDetail,JobOrder jobOrder)
	{

		List<TeacherAnswerDetail> teacherAnswerDetails = null;
		try {
				Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
					
				teacherAnswerDetails = findByCriteria(criterion,criterion1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAnswerDetails;

	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to findTeacherEPIQuestions.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<TeacherAnswerDetail> findTeacherEPIQuestions(TeacherDetail teacherDetail) 
	{
		
		List<TeacherAnswerDetail> teacherAnswerDetails = null;
		try {
			
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			
			Criterion criterion = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			teacherAnswerDetails = findByCriteria(criterion,criterion1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAnswerDetails;

	}
		
	@Transactional(readOnly=false)
	public List findSumOfAnsByComptencyAndTeacherList(List<TeacherDetail> lstTeacherDetail, CompetencyMaster competencyMaster,TeacherAssessmentdetail teacherAssessmentdetail)
	{
		try 
		{
			Session session = getSession();
			if(lstTeacherDetail==null)
			{
				Criteria crit = session.createCriteria(getPersistentClass());
				crit.add(Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail));

				Criteria c=crit.createCriteria("teacherAssessmentQuestion");

				c.add(Restrictions.eq("questionWeightage",1.0));
				c.add(Restrictions.eq("competencyMaster",competencyMaster));
				
				List result = crit.add(Restrictions.isNull("jobOrder"))
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))           
				).list();

				//System.out.println("ddd: "+result);
				return result;
			}
			else{

				Criteria crit = session.createCriteria(getPersistentClass());
				crit.add(Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail));
				
				Criteria c = crit.createCriteria("teacherAssessmentQuestion");
				c.add(Restrictions.eq("questionWeightage",1.0));
				c.add(Restrictions.eq("competencyMaster",competencyMaster));
				
				List result = crit.add(Restrictions.isNull("jobOrder"))
				.add(Restrictions.in("teacherDetail", lstTeacherDetail))    
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))           
				).list();
				
				return result;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}
	
	@Transactional(readOnly=false)
	public List findSumOfAnsByDomainAndTeacherList(List<TeacherDetail> lstTeacherDetail, DomainMaster domainMaster, TeacherAssessmentdetail teacherAssessmentdetail)
	{
		try 
		{
			Session session = getSession();
			if(lstTeacherDetail!=null && lstTeacherDetail.size()==0){
				return new ArrayList();
			}
			if(lstTeacherDetail==null)
			{
				Criteria crit = session.createCriteria(getPersistentClass());
				crit.add(Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail));

				Criteria c = crit.createCriteria("teacherAssessmentQuestion");

				c.add(Restrictions.eq("questionWeightage",1.0));
				c.add(Restrictions.eq("domainMaster",domainMaster));

				List result = crit.add(Restrictions.isNull("jobOrder"))
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))           
				).list();

				//System.out.println("ddd: "+result);
				return result;
			}else
			{
				Criteria crit = session.createCriteria(getPersistentClass());
				crit.add(Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail));
				Criteria c=crit.createCriteria("teacherAssessmentQuestion");
				c.add(Restrictions.eq("questionWeightage",1.0));
				c.add(Restrictions.eq("domainMaster",domainMaster));
				
				List result = crit.add(Restrictions.isNull("jobOrder"))
				.add(Restrictions.in("teacherDetail", lstTeacherDetail))    
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))           
				).list();


				
				Object obj[] = null;
				TeacherDetail tDetail = null;
				List<TeacherDetail> lstTeacherTemp = new LinkedList<TeacherDetail>(lstTeacherDetail);
				for(Object oo : result){
					obj = (Object[])oo;
					tDetail = (TeacherDetail) obj[0];
					if(lstTeacherTemp.contains(tDetail)){
						lstTeacherTemp.remove(tDetail);
					}
				}
				
				for(TeacherDetail teacherDetail: lstTeacherTemp){
					obj = new Object[3];
					obj[0]=teacherDetail;
					obj[1]=0;
					obj[2]=0;
					result.add(obj);
				}	
			

				return result;
			}
		} 
		catch (Exception e) 
		{
			System.out.println("dddd");
			e.printStackTrace();
		}		
		return null;
	}
	
	@Transactional(readOnly=false)
	public TeacherAnswerDetail findTADetailbyQuestion(TeacherAssessmentQuestion teacherAssessmentQuestion)
	{
		List<TeacherAnswerDetail> lstTeacherAnswerDetails = new ArrayList<TeacherAnswerDetail>();
		
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherAssessmentQuestion",teacherAssessmentQuestion);

			lstTeacherAnswerDetails = findByCriteria(criterion1);	
			if(lstTeacherAnswerDetails.size()>0)
				return lstTeacherAnswerDetails.get(0);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<BigInteger,Integer> findTeacherQuestionAnswersForJSI(TeacherDetail teacherDetail,JobOrder jobOrder) 
	{
		Session session = getSession();

		String sql =" select teacherAssessmentQuestionId,answerId from teacheranswerdetail where jobId=:jobOrder and teacherId=:teacherDetail";
		
		System.out.println(sql);

		Query query = session.createSQLQuery(sql);

		query.setParameter("teacherDetail", teacherDetail);
		query.setParameter("jobOrder", jobOrder);

		List<Object[]> rows = query.list();		
		Map<BigInteger,Integer> map = new HashMap<BigInteger, Integer>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				
				Object obj[] = (Object[])oo;				
				map.put((BigInteger)obj[0], (Integer)obj[1]);
			}
		}
		return map;
	}
	
		
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List findDuplicateQuestionId(List<TeacherDetail> teacherDetailList, TeacherAssessmentdetail teacherAssessmentdetail)
	{
		Session session = getSession();
		Integer assessmentId = teacherAssessmentdetail.getAssessmentDetail().getAssessmentId();
		String sql = "";
		sql = "select count(*) cnt,teacherAssessmentQuestionId,teacherId from teacheranswerdetail where teacherId in (:teacherDetailList) and assessmentId="+assessmentId+" and teacherAssessmentId="+teacherAssessmentdetail.getTeacherAssessmentId()+" group by teacherAssessmentQuestionId having cnt>1";
		
		System.out.println("sql : "+sql);
		try {
			Query query = session.createSQLQuery(sql);
			query.setParameterList("teacherDetailList", teacherDetailList);
			
			List<Object[]> rows = query.list();
			
			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional(readOnly=false)
	public boolean removeAnswerByTeacherIdAndQuestionId(Integer teacherId,Long teacherAssessmentQuestionId, Integer limit)
	{
		try
		{
			String sql = "delete from teacheranswerdetail where teacherId= "+teacherId+" and teacherAssessmentQuestionId= "+teacherAssessmentQuestionId+" limit "+(--limit);
			System.out.println("sql : "+sql);
			
			Query query = null;
			Session session = getSession();
			query = session.createSQLQuery(sql);
			
			System.out.println("delete from teacheranswerdetail:- "+query.executeUpdate());
			
			return true;
		}
		catch (HibernateException e)
		{
			e.printStackTrace();
		}
		return false;
	}
	
	@Transactional(readOnly=false)
	public List findScoreByDomainAndTeacherList(List<TeacherDetail> lstTeacherDetail, DomainMaster domainMaster)
	{
		try 
		{
			Session session = getSession();
			if(lstTeacherDetail!=null && lstTeacherDetail.size()==0){
				return new ArrayList();
			}
			if(lstTeacherDetail!=null)
			{
				Criteria crit = session.createCriteria(getPersistentClass());
				Criteria c=crit.createCriteria("teacherAssessmentQuestion");
				c.add(Restrictions.eq("domainMaster",domainMaster));
				
				List result = crit.add(Restrictions.isNull("jobOrder"))
				.add(Restrictions.in("teacherDetail", lstTeacherDetail))
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))
				).list();

				return result;
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return null;
	}
	
	@Transactional(readOnly=false)
	public List findScoreByDomainAndTAD(List<TeacherAssessmentdetail> teacherAssessmentdetailList, DomainMaster domainMaster)
	{
		try 
		{
			Session session = getSession();
			if(teacherAssessmentdetailList!=null && teacherAssessmentdetailList.size()==0){
				return new ArrayList();
			}
			if(teacherAssessmentdetailList!=null)
			{
				Criteria crit = session.createCriteria(getPersistentClass());
				Criteria c=crit.createCriteria("teacherAssessmentQuestion");
				c.add(Restrictions.eq("domainMaster",domainMaster));
				
				List result = crit.add(Restrictions.isNull("jobOrder"))
				.add(Restrictions.in("teacherAssessmentdetail", teacherAssessmentdetailList))
				.setProjection(Projections.projectionList()
						.add(Projections.property("teacherDetail"))
						.add(Projections.groupProperty("teacherAssessmentdetail"))
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))
				).list();

				return result;
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return null;
	}
	
	@Transactional(readOnly=false)
	public List findScoreByCompetecyAndTAD(List<TeacherAssessmentdetail> teacherAssessmentdetailList, CompetencyMaster competencyMaster)
	{
		try 
		{
			Session session = getSession();
			if(teacherAssessmentdetailList!=null && teacherAssessmentdetailList.size()==0){
				return new ArrayList();
			}
			if(teacherAssessmentdetailList!=null)
			{
				Criteria crit = session.createCriteria(getPersistentClass());
				Criteria c=crit.createCriteria("teacherAssessmentQuestion");
				c.add(Restrictions.eq("competencyMaster",competencyMaster));
				
				List result = crit.add(Restrictions.isNull("jobOrder"))
				.add(Restrictions.in("teacherAssessmentdetail", teacherAssessmentdetailList))
				.setProjection(Projections.projectionList()
						.add(Projections.property("teacherDetail"))
						.add(Projections.groupProperty("teacherAssessmentdetail"))
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))
				).list();

				return result;
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
	public List findSumOfAnsByObjectiveAndTeacherList(List<TeacherDetail> lstTeacherDetail, ObjectiveMaster objectiveMaster,TeacherAssessmentdetail teacherAssessmentdetail)
	{
		try 
		{
			Session session = getSession();
			if(lstTeacherDetail==null)
			{
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail));

				Criteria c = criteria.createCriteria("teacherAssessmentQuestion");

				c.add(Restrictions.eq("questionWeightage",1.0));
				c.add(Restrictions.eq("objectiveMaster",objectiveMaster));
				
				List result = criteria.add(Restrictions.isNull("jobOrder"))
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))           
				).list();

				return result;
			}
			else{
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail));
				
				Criteria c = criteria.createCriteria("teacherAssessmentQuestion");
				c.add(Restrictions.eq("questionWeightage",1.0));
				c.add(Restrictions.eq("objectiveMaster",objectiveMaster));
				
				List result = criteria.add(Restrictions.isNull("jobOrder"))
				.add(Restrictions.in("teacherDetail", lstTeacherDetail))    
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.sum("totalScore"))
						.add(Projections.sum("maxMarks"))           
				).list();
				
				return result;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Date findDateTimeOfLastAnswer(List<TeacherDetail> teacherDetailList, TeacherAssessmentdetail teacherAssessmentdetail)
	{
		Session session = getSession();
		String sql = "select max(createdDateTime) from teacheranswerdetail where teacherId in (:teacherDetailList) and teacherAssessmentId = "+teacherAssessmentdetail.getTeacherAssessmentId();
		System.out.println("findDateTimeOfLastAnswer : sql : "+sql);
		try {
			Query query = session.createSQLQuery(sql);
			query.setParameterList("teacherDetailList", teacherDetailList);
			
			List rows = query.list();
			if(rows!=null && rows.size()>0 && rows.get(0)!=null)
				return (Date) rows.get(0);
			else
				return null;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
}
