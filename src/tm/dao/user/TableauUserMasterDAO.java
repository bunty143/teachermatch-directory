package tm.dao.user;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.user.TableauUserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TableauUserMasterDAO extends GenericHibernateDAO<TableauUserMaster, Integer>
{

	public TableauUserMasterDAO() 
	{
		super(TableauUserMaster.class);
		
	}
	@Transactional(readOnly=false)
	public List<TableauUserMaster> getTableauDistrictIdByEntityType(Integer distId)
	{
		List<TableauUserMaster> tableauUserName		=	null;
		try{
			Criterion criterion			=	Restrictions.eq("status", "A");
			Criterion criterion1		=	Restrictions.eq("districtId", distId);
			tableauUserName	     		=	findByCriteria(criterion,criterion1);
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tableauUserName;
		
	}
	
	
	@Transactional(readOnly=false)
	public List<TableauUserMaster> getTableauDistrictIdByEntityTypeEmail(String email)
	{
		List<TableauUserMaster> tableauUserName		=	null;
		try{
			Criterion criterion			=	Restrictions.eq("status", "A");
			Criterion criterion1		=	Restrictions.eq("tableauUserName", email);
			tableauUserName	     		=	findByCriteria(criterion,criterion1);
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tableauUserName;
		
	}
	
	
	@Transactional(readOnly=false)
	public List<TableauUserMaster> getTableauAdminUserNameByEntityType(Integer entityId)
	{
		List<TableauUserMaster> tableauUserName		=	null;
		try{
			Criterion criterion			=	Restrictions.eq("status", "A");
			Criterion criterion1		=	Restrictions.eq("entityType", entityId);
			tableauUserName	     		=	findByCriteria(criterion,criterion1);
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tableauUserName;
		
	}

}
