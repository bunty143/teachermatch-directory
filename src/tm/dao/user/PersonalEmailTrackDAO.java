package tm.dao.user;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.user.PersonalEmailTrack;
import tm.dao.generic.GenericHibernateDAO;


public class PersonalEmailTrackDAO extends GenericHibernateDAO<PersonalEmailTrack, Long>
{

	public PersonalEmailTrackDAO() 
	{
		super(PersonalEmailTrack.class);
		
	}
	@Transactional(readOnly=false)
	public List<PersonalEmailTrack> getEmailIdWithPassword(String status)
	{
		List<PersonalEmailTrack> personalEmailPassword		=	null;
		try{
			Criterion criterion			=	Restrictions.eq("personalStatus", "A");
			personalEmailPassword  		=	findByCriteria(criterion);
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return personalEmailPassword;
		
	}
	
	@Transactional(readOnly=false)
	public List<PersonalEmailTrack> getLastDateAndTime(String email)
	{
		List<PersonalEmailTrack> personalEmailPassword		=	null;
		try{
			Criterion criterion			=	Restrictions.eq("personalEmailAddress", email);
			personalEmailPassword  		=	findByCriteria(criterion);
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return personalEmailPassword;
		
	}
	
	@Transactional(readOnly=false)
	public List<PersonalEmailTrack> getUserByDistrict(int districtID)
	{
		List<PersonalEmailTrack> personalUserList		=	null;
		try{
			Criterion criterion			=	Restrictions.eq("districtId", districtID);
			personalUserList  		=	findByCriteria(criterion);
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return personalUserList;
		
	}
	
	
}
