package tm.dao.user;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherUploadTemp;
import tm.bean.user.UserMasterTemp;
import tm.dao.generic.GenericHibernateDAO;
public class UserMasterTempDAO extends GenericHibernateDAO<UserMasterTemp, Integer>{

	public UserMasterTempDAO() 
	{
		super(UserMasterTemp.class);
	}
	
	@Transactional(readOnly=false)
	public boolean deleteUserTemp(String sessionId){
		try{
			Criterion criterion = Restrictions.eq("sessionid",sessionId);
        	List<UserMasterTemp> userMasterTemplst	=findByCriteria(criterion);
        	for(UserMasterTemp userMasterTemp: userMasterTemplst){
        		makeTransient(userMasterTemp);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Transactional(readOnly=true)
	public List<UserMasterTemp> getLogin(String emailAddress, String password){
		
         List <UserMasterTemp> lstUsers = new ArrayList<UserMasterTemp>();
		 Criterion criterion1 = Restrictions.eq("emailAddress",emailAddress.trim());
		 Criterion criterion2 = Restrictions.eq("password", password);
		 Criterion criterion3=  Restrictions.and(criterion1, criterion2);
		 lstUsers = findByCriteria(criterion3);  
		 return lstUsers;
	}
	@Transactional(readOnly=true)
	public int getRowCountTempUser(Criterion... criterion)
	{
		List<UserMasterTemp> userMasterTempList = new ArrayList<UserMasterTemp>();
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			userMasterTempList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return userMasterTempList.size();
	}
	@Transactional(readOnly=true)
	public List<UserMasterTemp> findByAllTempUserCurrentSession(String sessionId)
	{
		List<UserMasterTemp> userMasterTempList = new ArrayList<UserMasterTemp>();
		try{
			Session session = getSession();
			Criterion criterion2 = Restrictions.eq("sessionid",sessionId);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion2);
			userMasterTempList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return userMasterTempList;
	}
	@Transactional(readOnly=true)
	public List<UserMasterTemp> findByTempUser(Order order,int startPos,int limit,Criterion... criterion)
	{
		List<UserMasterTemp> userMasterTempList = new ArrayList<UserMasterTemp>();
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			criteria.addOrder(order);
			userMasterTempList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return userMasterTempList;
	}
	@Transactional(readOnly=false)
	public boolean deleteAllUserTemp(String sessionId)
	{
		try{
			Calendar c = Calendar.getInstance(); 
			c.add(Calendar.DAY_OF_YEAR, -2);  
			Date date = c.getTime();
			Criterion criterion1=Restrictions.lt("date",date);
			Criterion criterion2 = Restrictions.eq("sessionid",sessionId);
			Criterion complete3 = Restrictions.or(criterion1,criterion2);
        	List<UserMasterTemp> userMasterTemplst	=findByCriteria(complete3);
        	System.out.println("userMasterTemplst::::"+userMasterTemplst.size());
			for(UserMasterTemp userMasterTemp: userMasterTemplst){
        		makeTransient(userMasterTemp);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return true;
	}
	@Transactional(readOnly=true)
	public List<UserMasterTemp> findByAllTempUser()
	{
		List<UserMasterTemp> userMasterTempList = new ArrayList<UserMasterTemp>();
		try{
			Session session = getSession();
			Calendar c = Calendar.getInstance(); // starts with today's date and time
			c.add(Calendar.DAY_OF_YEAR, -2);  // advances day by 2
			Date date = c.getTime();
			Criterion criterion=Restrictions.lt("date",date);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion);
			userMasterTempList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return userMasterTempList;
	}
}
