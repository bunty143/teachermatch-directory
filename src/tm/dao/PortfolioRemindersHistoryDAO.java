package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.PortfolioReminders;
import tm.bean.PortfolioRemindersHistory;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class PortfolioRemindersHistoryDAO extends GenericHibernateDAO<PortfolioRemindersHistory,Integer> 
{
	public PortfolioRemindersHistoryDAO() {
		super(PortfolioRemindersHistory.class);
	}
	
	
	@Transactional(readOnly=false)
	public List<PortfolioRemindersHistory> getRecordByDistrict(DistrictMaster districtMaster){
		
		List<PortfolioRemindersHistory> portfolioRemindersHistoryList = new ArrayList<PortfolioRemindersHistory>();
		try{
			Criterion criterion1 	= Restrictions.eq("districtMaster",districtMaster);
			portfolioRemindersHistoryList 	= findByCriteria(criterion1);
		} catch(Exception exception){
			exception.printStackTrace();
		}
		
		return portfolioRemindersHistoryList;
		
	}
	
	
	
	
	
	

}
