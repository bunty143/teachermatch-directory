package tm.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.TeacherVideoLink;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherVideoLinksDAO extends GenericHibernateDAO<TeacherVideoLink, Integer> 
{
	public TeacherVideoLinksDAO() 
	{
		super(TeacherVideoLink.class);
	}

	/*@Transactional(readOnly=false)
	public TeacherVideoLink findTeacherExperienceByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferences> lstTeacherElectronicReferences = new ArrayList<TeacherElectronicReferences>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstTeacherElectronicReferences = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherElectronicReferences==null || lstTeacherElectronicReferences.size()==0)
			return null;
		else
			return lstTeacherElectronicReferences.get(0);
	}*/

@Transactional(readOnly=false)
public List<TeacherVideoLink>  findTeachByVideoLink(TeacherDetail teacherDetail)
{
	List<TeacherVideoLink> teacherVedioLink=null;
	try
	{
		Criterion criterion=Restrictions.eq("teacherDetail", teacherDetail);
		Criterion criterion2 = Restrictions.isNull("districtMaster");
		teacherVedioLink=findByCriteria(criterion);
	}
	catch(Exception e)
	{e.printStackTrace();}
	
	return teacherVedioLink;
}

	@Transactional(readOnly=false)
	public List<Integer> getTeacherVideoLinkCountByTeacherJAF(TeacherDetail teacherDetail)
	{
		List<Integer> lstTeacherVideoLink= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			
			criteria.setProjection(Projections.projectionList().add(Projections.count("teacherDetail"))) ;
			lstTeacherVideoLink =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstTeacherVideoLink;
		}
		
	}

}
