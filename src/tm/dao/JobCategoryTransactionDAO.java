package tm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobCategoryTransaction;
import tm.bean.master.JobCategoryMaster;
import tm.dao.generic.GenericHibernateDAO;

public class JobCategoryTransactionDAO extends GenericHibernateDAO<JobCategoryTransaction, Integer> {
	public JobCategoryTransactionDAO() {
		super(JobCategoryTransaction.class);
	}
	@Transactional(readOnly=true)
	public List<JobCategoryTransaction> findListByjobCategoryId(List<JobCategoryMaster> jobCategoryMasters)
	{
		List<JobCategoryTransaction> lstJobCategoryTrans= new ArrayList<JobCategoryTransaction>();
		try{
		List<Integer> jobCatelist=new ArrayList();
		for(JobCategoryMaster rl:jobCategoryMasters){
				jobCatelist.add(rl.getJobCategoryId());
		}
		Criterion criterion2	=	Restrictions.in("jobCategoryId", jobCatelist);
		lstJobCategoryTrans=findByCriteria(criterion2);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobCategoryTrans;
	
	}
	
	@Transactional(readOnly=true)
	public List<JobCategoryTransaction> findjobCategoryList(List<Integer> jobCategoryIds)
	{
		List<JobCategoryTransaction> lstJobCategoryTrans= new ArrayList<JobCategoryTransaction>();
		//Criterion criterion1 	= 	Restrictions.eq( "status","A");
		try{
		Criterion criterion	=	Restrictions.in("jobCategoryId", jobCategoryIds);
		lstJobCategoryTrans=findByCriteria(criterion);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobCategoryTrans;
	
	}
	
	@Transactional(readOnly=true)
	public Map<String,JobCategoryTransaction> getRecordBasedOnCandidateType(Integer jobCategoryId)
	{
		Map<String,JobCategoryTransaction> recordMap=new HashMap<String, JobCategoryTransaction>();
		try{
			List<JobCategoryTransaction> jobCategoryList= new ArrayList<JobCategoryTransaction>();
			Criterion criterion	=	Restrictions.eq("jobCategoryId", jobCategoryId);
			jobCategoryList=findByCriteria(criterion);
			for(int i=0;i<jobCategoryList.size();i++)
				recordMap.put(jobCategoryList.get(i).getCandidateType(), jobCategoryList.get(i));
			
		}catch(Exception e){e.printStackTrace();}
		return recordMap;
	}
	
	@Transactional(readOnly=true)
	public List<JobCategoryTransaction> getRecordBasedOnJobCategory(Integer jobCategoryId)
	{
		List<JobCategoryTransaction> jobCategoryList= new ArrayList<JobCategoryTransaction>();
		try{
			Criterion criterion	=	Restrictions.eq("jobCategoryId", jobCategoryId);
			jobCategoryList=findByCriteria(criterion);
			
		}catch(Exception e){e.printStackTrace();}
		return jobCategoryList;
	}
	
	@Transactional(readOnly=true)
	public List<JobCategoryTransaction> getJobCatTransList(Integer jobCategoryId,String sCandidateType)
	{
		List<JobCategoryTransaction> lstJobCategoryTrans= new ArrayList<JobCategoryTransaction>();
		try{
		Criterion criterion1 	= 	Restrictions.eq("candidateType",sCandidateType);
		Criterion criterion2	=	Restrictions.eq("jobCategoryId", jobCategoryId);
		lstJobCategoryTrans=findByCriteria(criterion1,criterion2);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobCategoryTrans;
	}
	
	@Transactional(readOnly=true)
	public List<JobCategoryTransaction> getJobCategoryByPortfolioId(Integer portfolioId)
	{
		List<JobCategoryTransaction> lstJobCategoryTrans= new ArrayList<JobCategoryTransaction>();
		try{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		criteria.add(Restrictions.eq("portfolio",portfolioId)) 
        .setProjection( Projections.projectionList()
        .add( Projections.groupProperty("jobCategoryId"), "jobCategoryId" )
        ).setResultTransformer(Transformers.aliasToBean(JobCategoryTransaction.class));
		lstJobCategoryTrans=criteria.list();
	}catch (Exception e) {
		System.out.println("Exception :::: "+e);
	}
	return lstJobCategoryTrans;
 }
	@Transactional(readOnly=true)
	public List<Integer> getPortfolioIdByJobCategoryId(List<Integer> jobCategoryIds)
	{
		List<Integer> portfolioIdList= new ArrayList<Integer>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.in("jobCategoryId", jobCategoryIds));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.distinct(Projections.property("portfolio"))));
			
			portfolioIdList=criteria.list();
		}catch(Exception e){
			e.printStackTrace();
			}
		return portfolioIdList;
	}
}
