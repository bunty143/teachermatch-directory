package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictSpecificTiming;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificTimingDAO extends GenericHibernateDAO<DistrictSpecificTiming, Integer>{

	public DistrictSpecificTimingDAO(){
		super(DistrictSpecificTiming.class);
	}

	@Transactional(readOnly=false)
	public List<DistrictSpecificTiming> getAllTimingForDistrict(DistrictMaster districtMaster)
	{
		List<DistrictSpecificTiming> districtSpecificTiming=new ArrayList<DistrictSpecificTiming>();
		try{
		Criterion criterion=Restrictions.eq("districtMaster", districtMaster);
		Criterion criterion1=Restrictions.eq("status", "A");
		districtSpecificTiming=findByCriteria(criterion,criterion1);
		}catch(Exception e){}
	return districtSpecificTiming;	
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificTiming> getAllTimingDefault()
	{
		List<DistrictSpecificTiming> districtSpecificTiming=new ArrayList<DistrictSpecificTiming>();
		try{
		Criterion criterion=Restrictions.isNull("districtMaster");
		Criterion criterion1=Restrictions.eq("status", "A");
		districtSpecificTiming=findByCriteria(criterion1,criterion);
		}catch(Exception e){
			e.printStackTrace();
		}
	return districtSpecificTiming;	
	}
	
}
