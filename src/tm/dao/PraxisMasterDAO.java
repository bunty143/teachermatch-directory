package tm.dao;

import tm.bean.PraxisMaster;
import tm.dao.generic.GenericHibernateDAO;

public class PraxisMasterDAO extends GenericHibernateDAO<PraxisMaster, Integer> 
{
	public PraxisMasterDAO() {
		super(PraxisMaster.class);
	}
}
