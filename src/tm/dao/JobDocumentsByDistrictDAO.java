package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobDocumentsByDistrict;
import tm.bean.SchoolSelectedByCandidate;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;


public class JobDocumentsByDistrictDAO extends GenericHibernateDAO<JobDocumentsByDistrict, Integer> 
{
	public JobDocumentsByDistrictDAO() {
		super(JobDocumentsByDistrict.class);
	}

	@Transactional(readOnly=false)
	public List<JobDocumentsByDistrict> getJobDocumentsDetails(DistrictMaster districtMaster, int sortingcheck,Order sortOrderStrVal,int start , int limit,Boolean flag)
	{
		System.out.println("============getJobDocumentsDetails call==========");
		List<JobDocumentsByDistrict> lstJobDocuments= new ArrayList<JobDocumentsByDistrict>();
		try 
		{
			System.out.println(" sortingcheck :: "+sortingcheck+" sortOrderStrVal :: "+sortOrderStrVal+" start :: "+start+" limit :: "+limit);
			Session session = getSession();
			Criteria criteria= session.createCriteria(getPersistentClass());
			Criterion criterion2 = null;
			Criterion criterion = null;
			if(districtMaster!=null)
			{
				criterion = Restrictions.eq("districtMaster",districtMaster);
				criteria.add(criterion);
			}
			
			/*criterion2 = Restrictions.eq("status","A");			
			criteria.add(criterion2);*/
			//criteria.addOrder(order);			
			if(sortingcheck==0)
				criteria.addOrder(sortOrderStrVal);

			Criteria c1= null;
			Criteria c2= null;
			Criteria c3= null;
			if(sortingcheck==1){
				c1 = criteria.createCriteria("userMaster");
				c1.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==2){
				//c2 = criteria.createCriteria("docsName");
				//c2.addOrder(sortOrderStrVal);
				criteria.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==3){
				//c3 = criteria.createCriteria("createdDateTime");
				criteria.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==4){
				c3 = criteria.createCriteria("jobCategoryMaster");
				c3.addOrder(sortOrderStrVal);
			}
			if(flag)
			{
				criteria.setFirstResult(start);
				criteria.setMaxResults(limit);
			}
			lstJobDocuments = criteria.list();
			System.out.println("List Size :: "+lstJobDocuments.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobDocuments;
	}

	@Transactional(readOnly=false)
	public int getTotalJobDocumentsDetails(DistrictMaster districtMaster, int sortingcheck,Order sortOrderStrVal,int start , int limit)
	{
		System.out.println("============getTotalJobDocumentsDetails call==========");
		int rowCount = 0;
		List<JobDocumentsByDistrict> lstJobDocuments= new ArrayList<JobDocumentsByDistrict>();
		try 
		{
			System.out.println(" sortingcheck :: "+sortingcheck+" sortOrderStrVal :: "+sortOrderStrVal+" start :: "+start+" limit :: "+limit);
			Session session = getSession();
			Criteria criteria= session.createCriteria(getPersistentClass());
			Criterion criterion = null;
			Criterion criterion2 = null;
			if(districtMaster!=null)
			{
				criterion = Restrictions.eq("districtMaster",districtMaster); 
				criteria.add(criterion);
			}
			/* criterion2 = Restrictions.eq("status","A");
			criteria.add(criterion2);*/
			//criteria.addOrder(order);			
			if(sortingcheck==0)
				criteria.addOrder(sortOrderStrVal);

			Criteria c1= null;
			Criteria c2= null;
			Criteria c3= null;
			if(sortingcheck==1){
				c1 = criteria.createCriteria("userMaster");
				c1.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==2){
				//c2 = criteria.createCriteria("docsName");
				//c2.addOrder(sortOrderStrVal);
				criteria.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==3){
				//c3 = criteria.createCriteria("createdDateTime");
				criteria.addOrder(sortOrderStrVal);
			}
			if(sortingcheck==4){
				c3 = criteria.createCriteria("jobCategoryMaster");
				c3.addOrder(sortOrderStrVal);
			}
			
			lstJobDocuments = criteria.list();
			System.out.println("List Size :: "+lstJobDocuments.size());
			rowCount = lstJobDocuments.size();
			System.out.println("List Size :: "+rowCount);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return rowCount;
	}

	@Transactional(readOnly=false)
	public List<JobDocumentsByDistrict> checkJobCategoryExists(UserMaster userMaster)
	{
		List<JobDocumentsByDistrict> jobDocumentsByDistricts = new ArrayList<JobDocumentsByDistrict>();
		Session session = getSession();
		Criteria criteria= session.createCriteria(getPersistentClass());
		Criterion criterion = Restrictions.eq("userMaster",userMaster); 
		criteria.add(criterion);
		//	Criterion criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster); 
		jobDocumentsByDistricts = criteria.list();
		return jobDocumentsByDistricts;
	}
	
}
