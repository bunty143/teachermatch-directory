package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.AdminActivityDetail;
import tm.dao.generic.GenericHibernateDAO;

public class AdminActivityDetailDAO extends GenericHibernateDAO<AdminActivityDetail, Integer>{

	public AdminActivityDetailDAO() {
		super(AdminActivityDetail.class); 
	}
	
	
	
	@Transactional(readOnly=true)
	public List<AdminActivityDetail> findByEntityIdNdType(Integer jobId)
	{
		List<AdminActivityDetail> lstJobDetails = new ArrayList<AdminActivityDetail>();
		Criterion criterion1 = Restrictions.eq("entityId",jobId);
		Criterion criterion2 = Restrictions.eq("entityType","Job Order");
		
		lstJobDetails = findByCriteria(Order.desc("adminactivitydetail_id") ,criterion1,criterion2);  

		return lstJobDetails;
	}

}
