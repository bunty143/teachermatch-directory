package tm.dao;

import tm.bean.JobForTeacherHistory;
import tm.dao.generic.GenericHibernateDAO;

public class JobForTeacherDAOHistory extends GenericHibernateDAO<JobForTeacherHistory, Long> 
{
	public JobForTeacherDAOHistory() 
	{
		super(JobForTeacherHistory.class);
	}

	}
