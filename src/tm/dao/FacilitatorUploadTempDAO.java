package tm.dao;

import tm.bean.FacilitatorUploadTemp;
import tm.dao.generic.GenericHibernateDAO;

public class FacilitatorUploadTempDAO extends GenericHibernateDAO<FacilitatorUploadTemp,Integer>{

	public FacilitatorUploadTempDAO() {
		super(FacilitatorUploadTemp.class);
	}

}
