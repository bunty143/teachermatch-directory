package tm.dao;

import tm.bean.CGStatusEventTemp;
import tm.dao.generic.GenericHibernateDAO;

public class CGStatusEventTempDAO extends GenericHibernateDAO<CGStatusEventTemp, Integer>{

	public CGStatusEventTempDAO() {
		super(CGStatusEventTemp.class); 
	}

}
