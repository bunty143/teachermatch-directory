package tm.dao.onboardingcheckpoint;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.onboarding.OnboardCheckPointQuestion;
import tm.dao.generic.GenericHibernateDAO;

public class AddQuestionDAO extends GenericHibernateDAO<OnboardCheckPointQuestion, Integer> {
	public AddQuestionDAO(){
		super(OnboardCheckPointQuestion.class);
	}

	@Transactional
	public List<OnboardCheckPointQuestion> getCheckPointEditQuestionDetails(Integer districtId,Integer onBoardingId,Integer checkPointId)
	{
		List<OnboardCheckPointQuestion> list=null;
		list=new ArrayList<OnboardCheckPointQuestion>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(districtId!=null)
				criteria.add(Restrictions.eq("districtId",districtId));
			if(onBoardingId!=null)
				criteria.add(Restrictions.eq("onBoardingId",onBoardingId));
			if(checkPointId!=null)
				criteria.add(Restrictions.eq("checkPointId", checkPointId));
			 list = criteria.list();
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
}
