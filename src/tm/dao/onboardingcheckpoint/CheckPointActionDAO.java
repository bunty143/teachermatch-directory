package tm.dao.onboardingcheckpoint;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import tm.bean.onboarding.CheckPointAction;
import tm.dao.generic.GenericHibernateDAO;

public class CheckPointActionDAO extends GenericHibernateDAO<CheckPointAction,Integer>{
	public CheckPointActionDAO(){
		super(CheckPointAction.class);
	}
	public List<CheckPointAction> getCheckPointActionRecordCount(Integer districtId,Integer onBoardingId){
		List<CheckPointAction> checkPointActionlst=new ArrayList<CheckPointAction>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(districtId!=null)
				criteria.add(Restrictions.eq("districtId",districtId));
			if(onBoardingId!=null)
				criteria.add(Restrictions.eq("onBoardingId",onBoardingId));
			
			checkPointActionlst=criteria.list();	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return checkPointActionlst;
	}
}
