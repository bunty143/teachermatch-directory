package tm.dao.onboardingcheckpoint;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.onboarding.AddCheckPointTransaction;
import tm.dao.generic.GenericHibernateDAO;

public class OnboardingAddCheckPointDAO extends GenericHibernateDAO<AddCheckPointTransaction, Integer>{
	
	public OnboardingAddCheckPointDAO() {
		super(AddCheckPointTransaction.class);
	}
	@Transactional
	public List<AddCheckPointTransaction> getCheckPointEditDetails(Integer districtId,Integer onBoardingId,String checkPointName,Integer checkPointId)
	{
		List<AddCheckPointTransaction> list=null;
		list=new ArrayList<AddCheckPointTransaction>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(districtId!=null)
				criteria.add(Restrictions.eq("districtId",districtId));
			if(onBoardingId!=null)
				criteria.add(Restrictions.eq("onBoardingId",onBoardingId));
			if(checkPointName!=null)
				criteria.add(Restrictions.eq("checkPointName",checkPointName));
			if(checkPointId!=null)
				criteria.add(Restrictions.eq("checkPointTransactionId", checkPointId));
			 list = criteria.list();
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
