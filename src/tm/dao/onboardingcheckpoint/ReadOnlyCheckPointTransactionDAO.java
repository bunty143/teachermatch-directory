package tm.dao.onboardingcheckpoint;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.ReadOnlyCheckPointTransaction;
import tm.dao.generic.GenericHibernateDAO;

public class ReadOnlyCheckPointTransactionDAO extends GenericHibernateDAO<ReadOnlyCheckPointTransaction,Integer>{
public ReadOnlyCheckPointTransactionDAO(){
	super(ReadOnlyCheckPointTransaction.class);
}
@Transactional
public List<ReadOnlyCheckPointTransaction> getReadOnlyCheckPointRecord(Integer districtId,Integer onBoardingId)
{
	List<ReadOnlyCheckPointTransaction> readOnlyCheckPointTransactionList=null;
	try{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		if(districtId!=null)
			criteria.add(Restrictions.eq("districtId",districtId));
		if(onBoardingId!=null)
			criteria.add(Restrictions.eq("onBoardingId",onBoardingId));
				
		readOnlyCheckPointTransactionList=criteria.list();
		
	}catch (Exception e) {
		e.printStackTrace();
	}
	return readOnlyCheckPointTransactionList;
}
}
