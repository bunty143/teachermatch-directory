package tm.dao.onboardingcheckpoint;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.onboarding.CheckPointMaster;
import tm.dao.generic.GenericHibernateDAO;

public class CheckPointMasterDAO extends GenericHibernateDAO<CheckPointMaster,Integer>{
public CheckPointMasterDAO(){
	super(CheckPointMaster.class);
}
@Transactional
public List<CheckPointMaster> getcheckpoints()
{
	List<CheckPointMaster> checkPointMasterList=null;
	try{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		criteria.add(Restrictions.eq("status","A"));
		checkPointMasterList=criteria.list();
	}catch(Exception e){
		e.printStackTrace();
	}
	return checkPointMasterList;
}
}
