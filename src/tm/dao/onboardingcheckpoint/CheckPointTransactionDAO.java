package tm.dao.onboardingcheckpoint;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.onboarding.CheckPointTransaction;
import tm.dao.generic.GenericHibernateDAO;

public class CheckPointTransactionDAO extends GenericHibernateDAO<CheckPointTransaction,Integer>{
	public CheckPointTransactionDAO(){
		super(CheckPointTransaction.class);
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<CheckPointTransaction> getCheckPointRecord(Integer districtId,Integer onBoardingId)
	{
		List<CheckPointTransaction> checkPointTransactionList=null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(districtId!=null)
				criteria.add(Restrictions.eq("districtId",districtId));
			if(onBoardingId!=null)
				criteria.add(Restrictions.eq("onBoardingId",onBoardingId));
			
			checkPointTransactionList=criteria.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return checkPointTransactionList;
	}
	@Transactional
	public Integer getCheckPointTransactionCount(Integer districtId,Integer onBoardingId)
	{
		Integer noOfRecord=0;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(districtId!=null)
				criteria.add(Restrictions.eq("districtId",districtId));
			if(onBoardingId!=null)
				criteria.add(Restrictions.eq("onBoardingId",onBoardingId));
			
			criteria.setProjection(Projections.rowCount());
			noOfRecord=(Integer) criteria.uniqueResult();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return noOfRecord;
	}
	@Transactional
	public Integer getNoOfRecords(){
		Session session = getSession();
		Integer uniqueResult=0;
		try{
		Criteria criteria = session.createCriteria(getPersistentClass());
		uniqueResult =(Integer) criteria.setProjection(Projections.max("checkPointTransactionId")).uniqueResult();
		//System.out.println("uniqueResult ::::::::::: "+uniqueResult);
		if(uniqueResult==null)
			uniqueResult=0;
		//uniqueResult = (Integer)criteria.setProjection(Projections.rowCount()).uniqueResult();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return uniqueResult;
	}
}

