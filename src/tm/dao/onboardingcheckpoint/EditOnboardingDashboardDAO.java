package tm.dao.onboardingcheckpoint;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.onboarding.EditOnboardDashboard;
import tm.dao.generic.GenericHibernateDAO;

public class EditOnboardingDashboardDAO extends GenericHibernateDAO<EditOnboardDashboard, Integer> {
	public EditOnboardingDashboardDAO() {
		super(EditOnboardDashboard.class);
	}

	@Transactional
	public List<EditOnboardDashboard> getEditDashboardRecords(Integer districtID,Integer onboardingID,Integer checkPointID)
	{
		List<EditOnboardDashboard> editCheckPointList=null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(districtID!=null)
				criteria.add(Restrictions.eq("districtId",districtID));
			if(onboardingID!=null)
				criteria.add(Restrictions.eq("onBoardingId",onboardingID));
			if(checkPointID!=null)
				criteria.add(Restrictions.eq("checkPointTransactionId",checkPointID));
			editCheckPointList=criteria.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return editCheckPointList;
	}


}
