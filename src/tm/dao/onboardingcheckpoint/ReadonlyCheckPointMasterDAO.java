package tm.dao.onboardingcheckpoint;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.ReadOnlyCheckPointMaster;
import tm.dao.generic.GenericHibernateDAO;

public class ReadonlyCheckPointMasterDAO extends GenericHibernateDAO<ReadOnlyCheckPointMaster,Integer>{
public ReadonlyCheckPointMasterDAO(){
	super(ReadOnlyCheckPointMaster.class);
}
@Transactional
public List<ReadOnlyCheckPointMaster> getReadOnlyCheckPointRecord()
{
	List<ReadOnlyCheckPointMaster> readOnlyCheckPointMasterList=null;
	try{
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		criteria.add(Restrictions.eq("status","A"));
		readOnlyCheckPointMasterList=criteria.list();
	}catch (Exception e) {
		e.printStackTrace();
	}
	return readOnlyCheckPointMasterList;
}
}
