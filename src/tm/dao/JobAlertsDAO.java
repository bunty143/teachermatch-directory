package tm.dao;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobAlerts;
import tm.dao.generic.GenericHibernateDAO;

public class JobAlertsDAO extends GenericHibernateDAO<JobAlerts, Integer>{
	public JobAlertsDAO() {
		super(JobAlerts.class);
	}
	
	@Transactional(readOnly=false)
	public List<JobAlerts> findActiveJobAlert()
	{
		List<JobAlerts> lstJobAlerts = null;
		try{
			Criterion criterion1 = Restrictions.eq("status","a");
			lstJobAlerts = findByCriteria(Order.desc("senderName"),criterion1);			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobAlerts;
	}

}
