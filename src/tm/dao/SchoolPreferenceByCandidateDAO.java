package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictPanelMaster;
import tm.bean.DistrictSpecificJobcode;
import tm.bean.JobOrder;
import tm.bean.SchoolPreferencebyCandidate;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SchoolPreferenceByCandidateDAO extends GenericHibernateDAO<SchoolPreferencebyCandidate, Integer>
{

	public SchoolPreferenceByCandidateDAO() {
		super(SchoolPreferencebyCandidate.class);
	}
	
	@Transactional(readOnly=false)
	public List<SchoolPreferencebyCandidate> findschoolpreferencebyTeacher(TeacherDetail teacherDetail,JobOrder jobId,DistrictMaster districtMaster){
		List<SchoolPreferencebyCandidate> schoolPreferencebyCandidates = new ArrayList<SchoolPreferencebyCandidate>();
		Criterion criterion2 = Restrictions.eq("teacherDetail", teacherDetail);
 	    Criterion criterion3 = Restrictions.eq("jobOrder", jobId);
 	    Criterion criterion4 = Restrictions.eq("districtMaster", districtMaster);
		try{
		Session session =getSession(); 	
 	    Criteria cri = session.createCriteria(getPersistentClass());
 	    cri.add(criterion2);
 	    cri.add(criterion3);
 	    cri.add(criterion4);
 	    schoolPreferencebyCandidates =findByCriteria(criterion2,criterion3,criterion4);
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
			}
		return  schoolPreferencebyCandidates;
	}
	
	
	@Transactional(readOnly=false)
	public List<SchoolPreferencebyCandidate> getschoolpreferencebyTeacher(TeacherDetail teacherDetail,JobOrder jobId,DistrictMaster districtMaster){
		List<SchoolPreferencebyCandidate> schoolPreferencebyCandidates = new ArrayList<SchoolPreferencebyCandidate>();
		Criterion criterion2 = Restrictions.eq("teacherDetail", teacherDetail);
 	    Criterion criterion3 = Restrictions.eq("jobOrder", jobId);
 	    Criterion criterion4 = Restrictions.eq("districtMaster", districtMaster);
		try{
		Session session =getSession(); 	
 	    Criteria cri = session.createCriteria(getPersistentClass());
 	    cri.add(criterion2);
 	    cri.add(criterion3);
 	    cri.add(criterion4);
 	    schoolPreferencebyCandidates =findByCriteria(criterion2,criterion3,criterion4);
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
			}
		return  schoolPreferencebyCandidates;
	}
	
	
	
	@Transactional(readOnly=false)
	public List<SchoolPreferencebyCandidate> getschoolpreferenceId(Integer schoolpreferenceId_f){
		List<SchoolPreferencebyCandidate> schoolPreferencebyCandidates = new ArrayList<SchoolPreferencebyCandidate>();
 	    Criterion criterion3 = Restrictions.eq("schoolpreferenceId", schoolpreferenceId_f);
		try{
		Session session =getSession(); 	
 	    Criteria cri = session.createCriteria(getPersistentClass());
 	    cri.add(criterion3);
 	    schoolPreferencebyCandidates =findByCriteria(criterion3);
		}		catch(Exception e){
			e.printStackTrace();
			return null;
			}
		return  schoolPreferencebyCandidates;
	}

}
