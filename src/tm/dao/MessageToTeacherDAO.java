package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.TeacherDetail;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class MessageToTeacherDAO extends GenericHibernateDAO<MessageToTeacher, Integer> 
{
	public MessageToTeacherDAO() 
	{
		super(MessageToTeacher.class);
	}
	
	@Transactional(readOnly=true)
	public List<MessageToTeacher> findByMessage(int teacherId,int jobId)
	{
	         List <MessageToTeacher> lstMessageToTeacher = null;
			 Criterion criterion = Restrictions.eq("teacherId",teacherId);
			 Criterion criterion2 = Restrictions.eq("jobId", jobId);
			 lstMessageToTeacher = findByCriteria(criterion,criterion2);
			 return lstMessageToTeacher;
	}
	
	@Transactional(readOnly=true)
	public List<MessageToTeacher> findByMessage(int teacherId,int jobId, Order order,int startPos,int limit)
	{
		List<MessageToTeacher> lstMessageToTeacher = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId", teacherId);
			Criterion criterion2 = Restrictions.eq("jobId", jobId);
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			if(order != null){
				criteria.addOrder(order);
			}
			
			lstMessageToTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstMessageToTeacher;
	}
	
	@Transactional(readOnly=true)
	public int getRowCountByMessage(int teacherId,int jobId)
	{
		List<MessageToTeacher> lstMessageToTeacher = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId", teacherId);
			Criterion criterion2 = Restrictions.eq("jobId",jobId);
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);
			
			lstMessageToTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstMessageToTeacher.size();
	}
	@Transactional(readOnly=true)
	public List<MessageToTeacher> findMessageByTeacher(List<TeacherDetail> teacherDetailList)
	{
	         List <MessageToTeacher> lstMessageToTeacher = new ArrayList<MessageToTeacher>();
	         try{
	        	 Criterion criterion = Restrictions.in("teacherId",teacherDetailList);
	        	 lstMessageToTeacher = findByCriteria(criterion);
	         }catch(Exception e){}
			 return lstMessageToTeacher;
	}
	
	/* Gagan : Getting Message through teacherId and JobId ========*/
	@Transactional(readOnly=false)
	public MessageToTeacher findMessageByTeacherAndJob(TeacherDetail teacherDetail, JobOrder jobOrder)
	{
		List<MessageToTeacher> lstMessageToTeacher = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);			

			lstMessageToTeacher = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstMessageToTeacher.size()>0)
			return lstMessageToTeacher.get(0);
		else
			return null;
	}
	
	@Transactional(readOnly=false)
	public List<MessageToTeacher> findMessageByTeacher(TeacherDetail teacherDetail, JobOrder jobOrder,UserMaster userMaster)
	{
		List<MessageToTeacher> lstMessageToTeacher = new ArrayList<MessageToTeacher>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			if(userMaster.getEntityType()!=1){
				if(userMaster.getDistrictId().getCommunicationsAccess()==0){
					if(userMaster.getEntityType()!=1){
						Criteria cc = criteria.createCriteria("senderId");	
						cc.add(Restrictions.eq("districtId",userMaster.getDistrictId()));
					}
				}else if(userMaster.getDistrictId().getCommunicationsAccess()==1){
					if(userMaster.getEntityType()==2){
						Criteria cc = criteria.createCriteria("senderId");	
						cc.add(Restrictions.eq("districtId",userMaster.getDistrictId()));
					}if(userMaster.getEntityType()==3){
						Criteria cc = criteria.createCriteria("senderId");	
						Criterion criterion2 = Restrictions.eq("districtId",userMaster.getDistrictId());
						Criterion criterion3 = Restrictions.eq("entityType",2);
						Criterion criterion4 = Restrictions.and(criterion2,criterion3);
						Criterion criterion5 = Restrictions.eq("schoolId",userMaster.getSchoolId());
						Criterion criterion6 = Restrictions.or(criterion4,criterion5);
						cc.add(criterion6);
					}
				}else if(userMaster.getDistrictId().getCommunicationsAccess()==2){
					if(userMaster.getEntityType()==2){
						Criteria cc = criteria.createCriteria("senderId");	
						cc.add(Restrictions.eq("districtId",userMaster.getDistrictId()));
					}if(userMaster.getEntityType()==3){
						Criteria cc = criteria.createCriteria("senderId");	
						cc.add(Restrictions.eq("schoolId",userMaster.getSchoolId()));
					}
				}
				if(jobOrder!=null){
					Criterion criterion2 = Restrictions.eq("jobId", jobOrder);
					Criterion criterion3 = Restrictions.isNull("jobId");
					if(userMaster.getDistrictId().getCommunicationsAccess()==0){
						Criterion criterion4 = Restrictions.or(criterion2,criterion3);
						criteria.add(criterion4);
					}else{
						criteria.add(criterion2);
					}
				}
			}else{
				if(jobOrder!=null){
					Criterion criterion2 = Restrictions.eq("jobId", jobOrder);
					criteria.add(criterion2);
				}
			}
			
			lstMessageToTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstMessageToTeacher;
	}
	
	
	@Transactional(readOnly=false)
	public List<MessageToTeacher> findMessageByRoleId(TeacherDetail teacherDetail)
	{
		List<MessageToTeacher> listMessageToTeacher= null;
		try 
		{

			List<RoleMaster> roleMasters=new ArrayList<RoleMaster>();

			RoleMaster roleMaster=new RoleMaster();
			roleMaster.setRoleId(7);
			roleMasters.add(roleMaster);
			roleMaster=new RoleMaster();
			roleMaster.setRoleId(8);
			roleMasters.add(roleMaster);

			Session session 	 = getSession();
			Criteria criteria 	 = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion1);
			criteria.createCriteria("senderId").add(Restrictions.not(Restrictions.in("roleId", roleMasters)));
			listMessageToTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listMessageToTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<MessageToTeacher> findMessageByRoleIdNew(TeacherDetail teacherDetail,List<UserMaster> userMasters)
	{
		List<MessageToTeacher> listMessageToTeacher= null;
		try 
		{

			List<RoleMaster> roleMasters=new ArrayList<RoleMaster>();

			RoleMaster roleMaster=new RoleMaster();
			roleMaster.setRoleId(7);
			roleMasters.add(roleMaster);
			roleMaster=new RoleMaster();
			roleMaster.setRoleId(8);
			roleMasters.add(roleMaster);

			Session session 	 = getSession();
			Criteria criteria 	 = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion_senderId = Restrictions.in("senderId", userMasters);
			criteria.add(criterion1);
			criteria.add(criterion_senderId);
			criteria.createCriteria("senderId").add(Restrictions.not(Restrictions.in("roleId", roleMasters)));
			listMessageToTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listMessageToTeacher;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherDetail> getPortfolioRecord(int noOfReminderSent)
	{
		List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
		try 
		{
			Criterion criterion1 = Restrictions.ge("noOfReminderSent", noOfReminderSent);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("teacherId"));
			
			lstTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacher;
	}
	String locale = Utility.getValueOfPropByKey("locale");	 
	@Transactional(readOnly=true)
	public List<MessageToTeacher> findByMailTextByTeacherAndJob(List<TeacherDetail>teacherDetails,JobOrder jobOrder)
	{
         List <MessageToTeacher> lstMessageToTeacher = new ArrayList<MessageToTeacher>();
         try{
			 Criterion criterion = Restrictions.in("teacherId",teacherDetails);
			 Criterion criterion2 = Restrictions.eq("jobId", jobOrder);
			 Criterion criterion3 = Restrictions.like("messageSubject",Utility.getLocaleValuePropByKey("msgInvitedApplyJob", locale),MatchMode.ANYWHERE);
			 lstMessageToTeacher = findByCriteria(Order.asc("createdDateTime"),criterion,criterion2,criterion3);
         }catch(Exception e){
        	 e.printStackTrace();
         }
		 return lstMessageToTeacher;
	}
	@Transactional(readOnly=true)
	public List<MessageToTeacher> findByAuthMailTextByTeacherAndJob(List<TeacherDetail>teacherDetails,JobOrder jobOrder)
	{
         List <MessageToTeacher> lstMessageToTeacher = new ArrayList<MessageToTeacher>();
         try{
			 Criterion criterion = Restrictions.in("teacherId",teacherDetails);
			 Criterion criterion2 = Restrictions.eq("jobId", jobOrder);
			 Criterion criterion3 = Restrictions.like("messageSubject",Utility.getLocaleValuePropByKey("msgAuthMailSubject", locale),MatchMode.ANYWHERE);
			 lstMessageToTeacher = findByCriteria(Order.asc("createdDateTime"),criterion,criterion2,criterion3);
         }catch(Exception e){
        	 e.printStackTrace();
         }
		 return lstMessageToTeacher;
	}
	@Transactional(readOnly=true)
	public List<MessageToTeacher> findByAuthMailTextByTeacher(List<TeacherDetail>teacherDetails)
	{
         List <MessageToTeacher> lstMessageToTeacher = new ArrayList<MessageToTeacher>();
         try{
			 Criterion criterion = Restrictions.in("teacherId",teacherDetails);
			 Criterion criterion3 = Restrictions.like("messageSubject",Utility.getLocaleValuePropByKey("msgAuthMailSubject", locale),MatchMode.ANYWHERE);
			 lstMessageToTeacher = findByCriteria(Order.asc("createdDateTime"),criterion,criterion3);
         }catch(Exception e){
        	 e.printStackTrace();
         }
		 return lstMessageToTeacher;
	}
}
