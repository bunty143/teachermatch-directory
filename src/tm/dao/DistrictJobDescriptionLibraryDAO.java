package tm.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictJobDescriptionLibrary;
import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;

public class DistrictJobDescriptionLibraryDAO  extends GenericHibernateDAO<DistrictJobDescriptionLibrary, Integer> {
	
	public DistrictJobDescriptionLibraryDAO() 
	{
		super(DistrictJobDescriptionLibrary.class);
	}
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<DistrictJobDescriptionLibrary> findShortedData(String sortOrderTypeVal,String sortOrderFieldName, Integer districtOrSchoolId,Integer jobcategoryId,String status) {
		
		System.out.println("districtOrSchoolId  "+districtOrSchoolId+"\tjobcategoryId  "+jobcategoryId+"\tstatus  "+status);
		DistrictMaster districtMaster=null;
		JobCategoryMaster jobCategoryMaster=null;
		Criteria crit = getSession().createCriteria(getPersistentClass());
		if(districtOrSchoolId!=0){
			districtMaster=districtMasterDAO.findById(districtOrSchoolId, false, false);
			crit.add(Restrictions.eq("districtMaster", districtMaster));
		}
		if(jobcategoryId!=0){
		 jobCategoryMaster=jobCategoryMasterDAO.findById(jobcategoryId, false, false);
			crit.add(Restrictions.eq("jobCategoryMaster", jobCategoryMaster));
		}
		if(!status.equalsIgnoreCase("0"))
			crit.add(Restrictions.eq("status", status));
		if(sortOrderTypeVal.equals("0"))
			crit.addOrder(Order.asc(sortOrderFieldName));
		else
			crit.addOrder(Order.desc(sortOrderFieldName));
		
		return crit.list();
		
	}
		@Transactional(readOnly=true)
		@SuppressWarnings("unchecked")
		public DistrictJobDescriptionLibrary findDataById(Integer districtjobdescriptionLibraryId) {
			
			List<DistrictJobDescriptionLibrary> descriptionLibraries=null;
			
			Criteria crit = getSession().createCriteria(getPersistentClass());
			
			
				crit.add(Restrictions.eq("districtjobdescriptionlibraryId", districtjobdescriptionLibraryId));
				descriptionLibraries=crit.list();
				if(descriptionLibraries!=null && descriptionLibraries.size()>0)
					return descriptionLibraries.get(0);
				else
					return null;
			
		}
		
		
		
		
		
		
	
	
}
