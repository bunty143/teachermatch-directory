package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EventDescriptionTemplates;
import tm.bean.EventEmailMessageTemplates;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class EventEmailMessageTemplatesDAO extends GenericHibernateDAO<EventEmailMessageTemplates,Integer>{

	public EventEmailMessageTemplatesDAO() {
		super(EventEmailMessageTemplates.class);
		}
	

	@Transactional(readOnly=false)
	public List<EventEmailMessageTemplates> findByDistrict(Order sortOrderStrVal,DistrictMaster districtMaster)
	{
		List<EventEmailMessageTemplates> eventEmailMessageTemplates = new ArrayList<EventEmailMessageTemplates>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion c1 = Restrictions.eq("districtMaster",districtMaster);
			criteria.addOrder(sortOrderStrVal);
			criteria.add(c1);
			eventEmailMessageTemplates = criteria.list();
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return eventEmailMessageTemplates;
	}



}
