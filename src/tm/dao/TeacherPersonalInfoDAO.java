package tm.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import edu.emory.mathcs.backport.java.util.LinkedList;
import tm.bean.JobForTeacher;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import edu.emory.mathcs.backport.java.util.LinkedList;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class TeacherPersonalInfoDAO extends GenericHibernateDAO<TeacherPersonalInfo,Integer> 
{
	public TeacherPersonalInfoDAO() {
		super(TeacherPersonalInfo.class);
	}
	
	@Autowired
	JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	JobOrderDAO jobOrderDAO;
	
	@Autowired
	TeacherDetailDAO teacherDetailDAO;

	@Transactional(readOnly=false)
	public Map<Integer, Boolean> findActiveContactStatus(List<TeacherDetail> teacherDetails)
	{


		List<TeacherPersonalInfo> lstTeacherPersonalInfo = null;
		Map<Integer,Boolean> mapTPersonalInfo= new TreeMap<Integer, Boolean>();
		try 
		{
			if(teacherDetails.size()>0)
			{
				List<Integer> teacherIds = new ArrayList<Integer>();
				for(TeacherDetail tDetail:teacherDetails){
					teacherIds.add(tDetail.getTeacherId());
				}
				String phone ="";
				String mobile = "";
				Criterion criterion1 = Restrictions.in("teacherId",teacherIds);
				lstTeacherPersonalInfo = findByCriteria(criterion1);		
				for(TeacherPersonalInfo tpInfo: lstTeacherPersonalInfo){
					phone = tpInfo.getPhoneNumber()==null?"":tpInfo.getPhoneNumber();
					mobile = tpInfo.getMobileNumber()==null?"":tpInfo.getMobileNumber();
					if((!(phone.trim().equals("") && mobile.trim().equals("")))){
						mapTPersonalInfo.put(tpInfo.getTeacherId(), true);
					}
					else{
						mapTPersonalInfo.put(tpInfo.getTeacherId(), false);
					}
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		return mapTPersonalInfo;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countHiredCandidatesVetHQL(int entityType,DistrictMaster districtMaster,SchoolMaster schoolMaster) 
	{
		Session session = getSession();
		String sql = "";

		try {
			sql = "select count(Distinct tpi.teacherId) as Veteran, tpi.stateId,sm.stateShortName FROM  teacherpersonalinfo tpi join jobforteacher jft on jft.teacherId=tpi.teacherId " +
			" join statemaster sm on sm.stateId=tpi.stateId where jft.status=:statuss group by tpi.stateId order by Veteran desc";
			Query query = session.createSQLQuery(sql);
			StatusMaster statuss = new StatusMaster();
			statuss.setStatusId(4);
			query.setParameter("statuss", statuss );

			List<Object[]> rows = query.list();
			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countHiredCandidatesIntHQL(DistrictMaster districtMaster,SchoolMaster schoolMaster) 
	{
		Session session = getSession();
		String sql = "";
		if(districtMaster!=null && schoolMaster==null)
		{
			sql = "select count(*) cnt,COUNT(IF(tns.decileColor IN (:decileColors), 1, NULL )) as topCandidates,sm.stateShortName from teacherpersonalinfo tpi join statemaster sm on sm.stateId=tpi.stateId " +
			" left join teachernormscore tns on tpi.teacherId=tns.teacherId where tpi.teacherId " +
			" IN (select teacherId from jobforteacher jft1 join joborder jo on jft1.jobId=jo.jobId " +
			" where jo.districtId=:districtMaster and jft1.status=:statuss group by jft1.teacherId) group by tpi.stateId order by topCandidates desc";
		}else if(schoolMaster!=null)
		{
			sql = "select count(*) cnt,COUNT(IF(tns.decileColor IN (:decileColors), 1, NULL )) as topCandidates,sm.stateShortName from teacherpersonalinfo tpi join statemaster sm on sm.stateId=tpi.stateId " +
			" left join teachernormscore tns on tpi.teacherId=tns.teacherId where tpi.teacherId " +
			" IN (select teacherId from jobforteacher jft1 join joborder jo on jft1.jobId=jo.jobId " +
			" join schoolinjoborder sij on jo.jobId=sij.jobId where jo.districtId=:districtMaster and schoolId=:schoolMaster " +
			" and jft1.status=:statuss group by jft1.teacherId) group by tpi.stateId order by topCandidates desc";
		}
		//System.out.println("jjjjjjjjjjjjjjj"+districtMaster.getDistrictId()+" "+sql);
		try {
			Query query = session.createSQLQuery(sql);
			StatusMaster statuss = new StatusMaster();
			statuss.setStatusId(6);
			query.setParameter("statuss", statuss );
			query.setParameter("districtMaster", districtMaster );
			if(schoolMaster!=null)
				query.setParameter("schoolMaster", schoolMaster );
			String[] decileColors = {Utility.getValueOfPropByKey("decile7"),Utility.getValueOfPropByKey("decile8"),Utility.getValueOfPropByKey("decile9")};
			query.setParameterList("decileColors", decileColors );
			query.setFirstResult(0);
			query.setMaxResults(10);    
			List<Object[]> rows = query.list();

			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}*/
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List getHiredCandidatesIntHQL(DistrictMaster districtMaster,SchoolMaster schoolMaster) 
	{
		Session session = getSession();
		String sql = "";
		if(districtMaster!=null && schoolMaster==null)
		{
			//System.out.println("districtId :: "+districtMaster.getDistrictId());
			sql = "select teacherId from jobforteacher jft1 join joborder jo on jft1.jobId=jo.jobId " +
			" where jo.districtId=:districtMaster and jft1.status=:statuss group by jft1.teacherId ";
		}else if(schoolMaster!=null)
		{
			sql = "select teacherId from jobforteacher jft1 join joborder jo on jft1.jobId=jo.jobId " +
			" join schoolinjoborder sij on jo.jobId=sij.jobId where jo.districtId=:districtMaster and schoolId=:schoolMaster " +
			" and jft1.status=:statuss group by jft1.teacherId ";
		}
		//System.out.println(":: "+districtMaster.getDistrictId()+" "+sql);
		try {
			Query query = session.createSQLQuery(sql);
			StatusMaster statuss = new StatusMaster();
			statuss.setStatusId(6);
			query.setParameter("statuss", statuss );
			query.setParameter("districtMaster", districtMaster );
			if(schoolMaster!=null)
				query.setParameter("schoolMaster", schoolMaster );
			
			List<Object[]> rows = query.list();

			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countHiredCandidatesIntHQL(List<Integer> lstTeacherDetails) 
	{
		Session session = getSession();
		String sql = "";
		
			sql = "select count(*) cnt,COUNT(IF(tns.decileColor IN (:decileColors), 1, NULL )) as topCandidates,sm.stateShortName from teacherpersonalinfo tpi join statemaster sm on sm.stateId=tpi.stateId " +
			" left join teachernormscore tns on tpi.teacherId=tns.teacherId where tpi.teacherId " +
			" IN (:teacherDetails) group by tpi.stateId order by topCandidates desc";
		
		try {
			Query query = session.createSQLQuery(sql);
			if(lstTeacherDetails.size()>0)
				query.setParameterList("teacherDetails", lstTeacherDetails);
			
			String[] decileColors = {Utility.getValueOfPropByKey("decile7"),Utility.getValueOfPropByKey("decile8"),Utility.getValueOfPropByKey("decile9")};
			query.setParameterList("decileColors", decileColors );
			query.setFirstResult(0);
			query.setMaxResults(10);    
			List<Object[]> rows = query.list();

			return rows;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Transactional(readOnly=false)
	public List<TeacherPersonalInfo> findTeacherPInfoForTeacher(List<TeacherDetail> teacherDetails)
	{
		List<TeacherPersonalInfo> teacherPersonalInfoList  = new ArrayList<TeacherPersonalInfo>();
		try 
		{
			List<Integer> teacherIds = new ArrayList<Integer>();
			for(TeacherDetail tDetail:teacherDetails){
				teacherIds.add(tDetail.getTeacherId());
			}
			if(teacherDetails.size()>0)
			{
				Criterion criterion1 = Restrictions.in("teacherId",teacherIds);
				teacherPersonalInfoList = findByCriteria(criterion1);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		

		return teacherPersonalInfoList;
	}
	@Transactional(readOnly=false)
	public TeacherPersonalInfo findTeacherPersonalInfoByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherPersonalInfo> teacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail.getTeacherId());
			teacherPersonalInfos = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(teacherPersonalInfos==null || teacherPersonalInfos.size()==0)
			return null;
		else
			return teacherPersonalInfos.get(0);
	}
	
	@Transactional(readOnly=false)
	public List<TeacherPersonalInfo> findByTeacherIds(ArrayList<Integer> teacherIds)
	{
		List<TeacherPersonalInfo> teacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherId",teacherIds);
			teacherPersonalInfos = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(teacherPersonalInfos==null || teacherPersonalInfos.size()==0)
			return null;
		else
			return teacherPersonalInfos;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherPersonalInfo> checkTeachersBySSN(String ssn)
	{
		List<TeacherPersonalInfo> teacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("SSN",ssn);
			teacherPersonalInfos = findByCriteria(criterion1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return teacherPersonalInfos;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherPersonalInfo> findByTeacherIdsEEC(org.hibernate.criterion.Order  sortOrderStrVal,ArrayList<Integer> teacherIds)
	{
	
		List<TeacherPersonalInfo> teacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherId",teacherIds);
			//Criterion criterion2= Restrictions.eq(propertyName, value)
			if(sortOrderStrVal!=null)
			{
			teacherPersonalInfos=findByCriteria(sortOrderStrVal,criterion1);
			}
			else if(sortOrderStrVal==null)
			{
				teacherPersonalInfos=findByCriteria(criterion1);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(teacherPersonalInfos==null || teacherPersonalInfos.size()==0)
			return null;
		else
			return teacherPersonalInfos;
	}
	
	
	@Transactional(readOnly=false)
	public List<TeacherPersonalInfo> checkTeachersByEmpNmbr(String empNmbr)
	{
		System.out.println(" checkTeachersByEmpNmbr :: "+empNmbr);
		List<TeacherPersonalInfo> teacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("employeeNumber",empNmbr);
			teacherPersonalInfos = findByCriteria(criterion1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return teacherPersonalInfos;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherPersonalInfo> getTPISSN()
	{
		List<TeacherPersonalInfo> teacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
		try 
		{
			Criterion criterion = Restrictions.isNotNull("SSN");
			teacherPersonalInfos = findByCriteria(criterion);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return teacherPersonalInfos;
	}

	@Transactional(readOnly=false)
	public List<TeacherPersonalInfo> getTPISSNEMPCode()
	{
		List<TeacherPersonalInfo> teacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
		try 
		{
			Criterion criterion = Restrictions.isNotNull("SSN");
			Criterion criterion1 = Restrictions.isNotNull("employeeNumber");
			teacherPersonalInfos = findByCriteria(criterion,criterion1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return teacherPersonalInfos;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherPersonalInfo> findBYTeachersIds(TreeSet<Integer> teacherIds)
	{
		List<TeacherPersonalInfo> teacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherId",teacherIds);
			 teacherPersonalInfos=findByCriteria(criterion1);
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
			return teacherPersonalInfos;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherPersonalInfo> findByState(StateMaster stateMaster)
	{
		List<TeacherPersonalInfo> teacherPersonalInfoList = new ArrayList<TeacherPersonalInfo>();
		try 
		{
			Criterion criterion = Restrictions.eq("stateId",stateMaster);
			teacherPersonalInfoList = findByCriteria(criterion);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return teacherPersonalInfoList;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherPersonalInfo> findByZipCode(String zipCode)
	{
		List<TeacherPersonalInfo> teacherPersonalInfoList = new ArrayList<TeacherPersonalInfo>();
		try 
		{
			Criterion criterion = Restrictions.eq("zipCode",zipCode);
			teacherPersonalInfoList = findByCriteria(criterion);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return teacherPersonalInfoList;
	}
		
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public  List<String[]> jobOrderEeoc(Integer districtId,String jobStatus, int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName,String startDate,String enddate,String jobendstartDate,String jobendendDate) 
	{
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();
			System.out.println("disrict id======"+districtId);
			
            System.out.println(" sortOrderStrVal :: "+sortOrderStrVal+" sortColomnName "+sortColomnName);
            System.out.println(" startDate :: "+startDate+" enddate :: "+enddate+"job status====="+jobStatus);
            
            System.out.println(" JobEndstartDate :: "+jobendstartDate+" JobEndenddate :: "+jobendendDate+"job status====="+jobStatus);

            Date startD = new Date("1/1/1990");
            Date endD = new Date("12/31/9999"); 

           if(startDate!=null && !startDate.equals(""))
	       startD =  Utility.getCurrentDateFormart(startDate);

           if(enddate!=null && !enddate.equals(""))
        	endD = Utility.getCurrentDateFormart(enddate);
           
           
           Date jobendstartD = new Date("1/1/1990");
           Date jobendendD = new Date("12/31/9999"); 

          if(jobendstartDate!=null && !jobendstartDate.equals(""))
        	  jobendstartD =  Utility.getCurrentDateFormart(jobendstartDate);

          if(jobendendDate!=null && !jobendendDate.equals(""))
        	  jobendendD = Utility.getCurrentDateFormart(jobendendDate);
			
           if(sortColomnName.equalsIgnoreCase("districtName"))
				sortColomnName = "nbyjob.District_Name";
			else if(sortColomnName.equalsIgnoreCase("jobTitle"))
				sortColomnName = "nbyjob.Job_Title";
			else if(sortColomnName.equalsIgnoreCase("status"))
				sortColomnName = "nbyjob.status";
			else if(sortColomnName.equalsIgnoreCase("Responded"))
				sortColomnName = "Responded";
			else if(sortColomnName.equalsIgnoreCase("N"))
				sortColomnName = "N";
			else if(sortColomnName.equalsIgnoreCase("N_Responded"))
				sortColomnName = "N_Responded";
			else if(sortColomnName.equalsIgnoreCase("Black"))
				sortColomnName = "Black";
			else if(sortColomnName.equalsIgnoreCase("Asian"))
				sortColomnName = "Asian";
			else if(sortColomnName.equalsIgnoreCase("Hispanic"))
				sortColomnName = "Hispanic";
			else if(sortColomnName.equalsIgnoreCase("White"))
				sortColomnName = "White";
			else if(sortColomnName.equalsIgnoreCase("AmricanIndian"))
				sortColomnName = "American_Indian";
			else if(sortColomnName.equalsIgnoreCase("Hawaiian"))
				sortColomnName = "Hawaiian";
			else if(sortColomnName.equalsIgnoreCase("Multi"))
				sortColomnName = "Multi";
			
			
           String statusValue="and jo.status = ?";
           if(jobStatus!=null)
           {
        	   jobStatus = jobStatus.trim();
        	   if(!jobStatus.equals("") && jobStatus.equalsIgnoreCase("All"))
        		   statusValue="";
        		   
           }
           
                 
			String orderby = " order by "+sortColomnName+" "+sortOrderStrVal ;
			
			 sql=" Select nbyjob.jobid, "+
		    "	nbyjob.District_Name, "+
		    "	nbyjob.Job_Title, "+
		"	sum(`N`) as `N`, "+
		"	round(sum(if(raceIdRecode!=0,N,0))/sum(`N`)*100, 2) as `Responded`,"+
		"    sum(if(ifnull(raceIdRecode,0)!=0,N,0)) as `N_Responded`, "+
		"    case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=1,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		"	end as `Black`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=2,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2) "+
		 "   end as `Asian`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=4,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2) "+
		 "   end as `Hispanic`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=5,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2) "+
		 "   end as `White`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=3,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		 "   end as `American_Indian`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=7,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		 "   end as `Hawaiian`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=9,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		"    end as `Multi`, "+
		"       nbyjob.status  "+
		" from "+
		"	( "+
		"		select "+
		"			jo.districtid, "+
		"			jo.jobid, "+
		"			jo.status, "+
		"			raceIdRecode, "+
		 "           jo.jobtitle as Job_Title, "+
		 "           dm.districtName as District_Name, "+
		"			count(*) as `N` "+
		"		from jobforteacher jft "+
		"			inner join teacherdetail td on jft.teacherid=td.teacherid "+
		"			inner join joborder jo on jft.jobid=jo.jobid "+
		 "           inner join districtmaster dm on jo.districtid=dm.districtid "+
		"			inner join ( "+
		"						select td.teacherid, "+
		"								case "+
		"									when length(tpi.raceid)=2 then  "+
		"										if(left(tpi.raceid,1)=6,0,(left(tpi.raceid,1))) "+
		"									when length(tpi.raceid)>2 then 9  "+
		"									when tpi.raceid=6 then 0 "+
		"									else ifnull(tpi.raceid,0) "+
		"								end as raceIdRecode "+
		"						from teacherdetail td "+
		"							left join teacherpersonalinfo tpi on tpi.teacherid=td.teacherid "+
		"						) as recodedRaceId on jft.teacherid=recodedRaceId.teacherid "+
		"			left join racemaster rm on recodedRaceId.raceIdRecode=rm.raceId "+
		"		 where jft.status !=8 and dm.status='A' and dm.districtid = ? "+statusValue+" and jo.jobStartDate >= ? and jo.jobStartDate <= ?  and jo.jobEndDate >= ? and jo.jobEndDate <= ?" +
		"		group by "+
		"			jo.districtid, "+
		"			jo.jobid, "+
		"			raceIdRecode "+
		 "           ) nbyjob "+
		" group by districtid, jobid "+
		""+orderby;
			 
		  java.sql.Date startD1 = new java.sql.Date(startD.getTime());
		  java.sql.Date endD1= new java.sql.Date(endD.getTime());
		  
		  java.sql.Date jobendstartD1 = new java.sql.Date(jobendstartD.getTime());
		  java.sql.Date jobendendD1= new java.sql.Date(jobendendD.getTime());
			    
		  System.out.println("job start date"+startD1);
		  System.out.println("job start end date"+endD1);
				
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
		ps.setString(1, districtId.toString());
		if(!statusValue.equals(""))
		{
			ps.setString(2, jobStatus);

			ps.setDate(3, startD1 );
			ps.setDate(4, endD1);
			ps.setDate(5, jobendstartD1 );
			ps.setDate(6, jobendendD1);
		}
		else
		{
			ps.setDate(2, startD1 );
			ps.setDate(3, endD1);
			ps.setDate(4, jobendstartD1);
			ps.setDate(5, jobendendD1);
		}
		ResultSet rs=ps.executeQuery();
			
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}

	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public  List jobOrderEeocForAdmin(Integer districtId,String jobStatus, int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName,String startDate,String enddate,String jobendstartDate,String jobendendDate) 
	{
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();
			System.out.println("disrict id======"+districtId);
			
            System.out.println(" sortOrderStrVal :: "+sortOrderStrVal+" sortColomnName "+sortColomnName);

            Date startD = new Date("1/1/2000");
            Date endD = new Date("12/31/9999"); 

           if(startDate!=null && !startDate.equals(""))
	       startD =  Utility.getCurrentDateFormart(startDate);

           if(enddate!=null && !enddate.equals(""))
        	endD = Utility.getCurrentDateFormart(enddate);
           
           Date jobendstartD = new Date("1/1/2000");
           Date jobendendD = new Date("12/31/9999"); 

          if(jobendstartDate!=null && !jobendstartDate.equals(""))
        	  jobendstartD =  Utility.getCurrentDateFormart(jobendstartDate);

          if(jobendendDate!=null && !jobendendDate.equals(""))
        	  jobendendD = Utility.getCurrentDateFormart(jobendendDate);
			
           if(sortColomnName.equalsIgnoreCase("districtName"))
				sortColomnName = "nbyjob.District_Name";
			else if(sortColomnName.equalsIgnoreCase("jobTitle"))
				sortColomnName = "nbyjob.Job_Title";
			else if(sortColomnName.equalsIgnoreCase("status"))
				sortColomnName = "nbyjob.status";
			else if(sortColomnName.equalsIgnoreCase("Responded"))
				sortColomnName = "Responded";
			else if(sortColomnName.equalsIgnoreCase("N"))
				sortColomnName = "N";
			else if(sortColomnName.equalsIgnoreCase("N_Responded"))
				sortColomnName = "N_Responded";
			else if(sortColomnName.equalsIgnoreCase("Black"))
				sortColomnName = "Black";
			else if(sortColomnName.equalsIgnoreCase("Asian"))
				sortColomnName = "Asian";
			else if(sortColomnName.equalsIgnoreCase("Hispanic"))
				sortColomnName = "Hispanic";
			else if(sortColomnName.equalsIgnoreCase("White"))
				sortColomnName = "White";
			else if(sortColomnName.equalsIgnoreCase("AmricanIndian"))
				sortColomnName = "American_Indian";
			else if(sortColomnName.equalsIgnoreCase("Hawaiian"))
				sortColomnName = "Hawaiian";
			else if(sortColomnName.equalsIgnoreCase("Multi"))
				sortColomnName = "Multi";
			
			String statusValue="and jo.status = ?";
	           if(jobStatus!=null)
	           {
	        	   jobStatus = jobStatus.trim();
	        	   if(!jobStatus.equals("") && jobStatus.equalsIgnoreCase("All"))
	        		   statusValue="";
	        		   
	           }
			
			String orderby = " order by "+sortColomnName+" "+sortOrderStrVal ;
			
			 sql=" Select nbyjob.jobid, "+
		    "	nbyjob.District_Name, "+
		    "	nbyjob.Job_Title, "+
		"	sum(`N`) as `N`, "+
		"	round(sum(if(raceIdRecode!=0,N,0))/sum(`N`)*100, 2) as `Responded`,"+
		"    sum(if(ifnull(raceIdRecode,0)!=0,N,0)) as `N_Responded`, "+
		"    case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=1,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		"	end as `Black`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=2,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2) "+
		 "   end as `Asian`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=4,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2) "+
		 "   end as `Hispanic`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=5,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2) "+
		 "   end as `White`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=3,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		 "   end as `American_Indian`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=7,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		 "   end as `Hawaiian`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=9,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		"    end as `Multi`, "+
		"       nbyjob.status  "+
		" from "+
		"	( "+
		"		select "+
		"			jo.districtid, "+
		"			jo.jobid, "+
		"			jo.status, "+
		"			raceIdRecode, "+
		 "           jo.jobtitle as Job_Title, "+
		 "           dm.districtName as District_Name, "+
		"			count(*) as `N` "+
		"		from jobforteacher jft "+
		"			inner join teacherdetail td on jft.teacherid=td.teacherid "+
		"			inner join joborder jo on jft.jobid=jo.jobid "+
		 "           inner join districtmaster dm on jo.districtid=dm.districtid "+
		"			inner join ( "+
		"						select td.teacherid, "+
		"								case "+
		"									when length(tpi.raceid)=2 then  "+
		"										if(left(tpi.raceid,1)=6,0,(left(tpi.raceid,1))) "+
		"									when length(tpi.raceid)>2 then 9  "+
		"									when tpi.raceid=6 then 0 "+
		"									else ifnull(tpi.raceid,0) "+
		"								end as raceIdRecode "+
		"						from teacherdetail td "+
		"							left join teacherpersonalinfo tpi on tpi.teacherid=td.teacherid "+
		"						) as recodedRaceId on jft.teacherid=recodedRaceId.teacherid "+
		"			left join racemaster rm on recodedRaceId.raceIdRecode=rm.raceId "+
		"		 where jft.status !=8 and dm.status='A' "+statusValue+" and jo.jobStartDate >= ? and jo.jobStartDate <= ?  and jo.jobEndDate >= ? and jo.jobEndDate <= ? " +
		"		group by "+
		"			jo.districtid, "+
		"			jo.jobid, "+
		"			raceIdRecode "+
		 "           ) nbyjob "+
		" group by districtid, jobid "+
		""+orderby;
			 
			    java.sql.Date startD1 = new java.sql.Date(startD.getTime());
			    java.sql.Date endD1= new java.sql.Date(endD.getTime());
			    
			    java.sql.Date jobendstartD1 = new java.sql.Date(jobendstartD.getTime());
				  java.sql.Date jobendendD1= new java.sql.Date(jobendendD.getTime());
			    
			    
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
		
		if(!statusValue.equals(""))
		{
			ps.setString(1, jobStatus);
			ps.setDate(2, startD1 );
			ps.setDate(3, endD1);
			ps.setDate(4, jobendstartD1 );
			ps.setDate(5, jobendendD1);
		}
		else
		{
			ps.setDate(1, startD1 );
			ps.setDate(2, endD1);
			ps.setDate(3, jobendstartD1);
			ps.setDate(4, jobendendD1);
		}
		
		ResultSet rs=ps.executeQuery();
			
			
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}
	
	/*******Hired eeoc report start***********/
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public  List<String[]> hiredEeoc(Integer districtId,String jobStatus, int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName,String startDate,String enddate,String jobendstartDate,String jobendendDate) 
	{
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();
			System.out.println("disrict id======"+districtId);
			
            System.out.println(" sortOrderStrVal :: "+sortOrderStrVal+" sortColomnName "+sortColomnName);
            System.out.println(" startDate :: "+startDate+" enddate :: "+enddate+"job status====="+jobStatus);
            
            System.out.println(" JobEndstartDate :: "+jobendstartDate+" JobEndenddate :: "+jobendendDate+"job status====="+jobStatus);

            Date startD = new Date("1/1/2000");
            Date endD = new Date("12/31/9999"); 

           if(startDate!=null && !startDate.equals(""))
	       startD =  Utility.getCurrentDateFormart(startDate);

           if(enddate!=null && !enddate.equals(""))
        	endD = Utility.getCurrentDateFormart(enddate);
           
           
           Date jobendstartD = new Date("1/1/2000");
           Date jobendendD = new Date("12/31/9999"); 

          if(jobendstartDate!=null && !jobendstartDate.equals(""))
        	  jobendstartD =  Utility.getCurrentDateFormart(jobendstartDate);

          if(jobendendDate!=null && !jobendendDate.equals(""))
        	  jobendendD = Utility.getCurrentDateFormart(jobendendDate);
			
           if(sortColomnName.equalsIgnoreCase("districtName"))
				sortColomnName = "nbyjob.District_Name";
			else if(sortColomnName.equalsIgnoreCase("jobTitle"))
				sortColomnName = "nbyjob.Job_Title";
			else if(sortColomnName.equalsIgnoreCase("status"))
				sortColomnName = "nbyjob.status";
			else if(sortColomnName.equalsIgnoreCase("Responded"))
				sortColomnName = "Responded";
			else if(sortColomnName.equalsIgnoreCase("N"))
				sortColomnName = "N";
			else if(sortColomnName.equalsIgnoreCase("N_Responded"))
				sortColomnName = "N_Responded";
			else if(sortColomnName.equalsIgnoreCase("Black"))
				sortColomnName = "Black";
			else if(sortColomnName.equalsIgnoreCase("Asian"))
				sortColomnName = "Asian";
			else if(sortColomnName.equalsIgnoreCase("Hispanic"))
				sortColomnName = "Hispanic";
			else if(sortColomnName.equalsIgnoreCase("White"))
				sortColomnName = "White";
			else if(sortColomnName.equalsIgnoreCase("AmricanIndian"))
				sortColomnName = "American_Indian";
			else if(sortColomnName.equalsIgnoreCase("Hawaiian"))
				sortColomnName = "Hawaiian";
			else if(sortColomnName.equalsIgnoreCase("Multi"))
				sortColomnName = "Multi";
			
			
           String statusValue="and jo.status = ?";
           if(jobStatus!=null)
           {
        	   jobStatus = jobStatus.trim();
        	   if(!jobStatus.equals("") && jobStatus.equalsIgnoreCase("All"))
        		   statusValue="";
        		   
           }
			String orderby = " order by "+sortColomnName+" "+sortOrderStrVal ;
			
			 sql=" Select nbyjob.jobid, "+
		    "	nbyjob.District_Name, "+
		    "	nbyjob.Job_Title, "+
		"	sum(`N`) as `N`, "+
		"	round(sum(if(raceIdRecode!=0,N,0))/sum(`N`)*100, 2) as `Responded`,"+
		"    sum(if(ifnull(raceIdRecode,0)!=0,N,0)) as `N_Responded`, "+
		"    case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=1,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		"	end as `Black`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=2,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2) "+
		 "   end as `Asian`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=4,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2) "+
		 "   end as `Hispanic`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=5,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2) "+
		 "   end as `White`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=3,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		 "   end as `American_Indian`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=7,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		 "   end as `Hawaiian`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=9,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		"    end as `Multi`, "+
		"       nbyjob.status  "+
		" from "+
		"	( "+
		"		select "+
		"			jo.districtid, "+
		"			jo.jobid, "+
		"			jo.status, "+
		"			raceIdRecode, "+
		 "           jo.jobtitle as Job_Title, "+
		 "           dm.districtName as District_Name, "+
		"			count(*) as `N` "+
		"		from jobforteacher jft "+
		"			inner join teacherdetail td on jft.teacherid=td.teacherid "+
		"			inner join joborder jo on jft.jobid=jo.jobid "+
		 "           inner join districtmaster dm on jo.districtid=dm.districtid "+
		"			inner join ( "+
		"						select td.teacherid, "+
		"								case "+
		"									when length(tpi.raceid)=2 then  "+
		"										if(left(tpi.raceid,1)=6,0,(left(tpi.raceid,1))) "+
		"									when length(tpi.raceid)>2 then 9  "+
		"									when tpi.raceid=6 then 0 "+
		"									else ifnull(tpi.raceid,0) "+
		"								end as raceIdRecode "+
		"						from teacherdetail td "+
		"							left join teacherpersonalinfo tpi on tpi.teacherid=td.teacherid "+
		"						) as recodedRaceId on jft.teacherid=recodedRaceId.teacherid "+
		"			left join racemaster rm on recodedRaceId.raceIdRecode=rm.raceId "+
		"		 where jft.status =6 and dm.status='A' and dm.districtid = ? "+statusValue+" and jo.jobStartDate >= ? and jo.jobStartDate <= ?  and jo.jobEndDate >= ? and jo.jobEndDate <= ? " +
		"		group by "+
		"			jo.districtid, "+
		"			jo.jobid, "+
		"			raceIdRecode "+
		 "           ) nbyjob "+
		" group by districtid, jobid "+
		""+orderby;
			 
		  java.sql.Date startD1 = new java.sql.Date(startD.getTime());
		  java.sql.Date endD1= new java.sql.Date(endD.getTime());
		  
		  java.sql.Date jobendstartD1 = new java.sql.Date(jobendstartD.getTime());
		  java.sql.Date jobendendD1= new java.sql.Date(jobendendD.getTime());
			    
		 
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
		ps.setString(1, districtId.toString());
		
		if(!statusValue.equals(""))
		{
			ps.setString(2, jobStatus);
			ps.setDate(3, startD1 );
			ps.setDate(4, endD1);
			ps.setDate(5, startD1 );
		 ps.setDate(6, endD1);
		}
		else
		{
			ps.setDate(2, startD1 );
			ps.setDate(3, endD1);
			ps.setDate(4, jobendstartD1);
			ps.setDate(5, jobendendD1);
		}
		ResultSet rs=ps.executeQuery();
			
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					//System.out.println("datttttttttt====="+rs.getString(1).toString()+""+rs.getString(2).toString()+"\nRow3:- "+rs.getString(3)+" "+rs.getString(4)+""+rs.getString(5).toString()+""+rs.getString(6)+""+rs.getString(7)+""+rs.getString(8)+""+rs.getString(9).toString()+""+rs.getString(10).toString()+""+rs.getString(11).toString()+""+rs.getString(12).toString()+""+rs.getString(13).toString()+"");	
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}

	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public  List hiredEeocForAdmin(Integer districtId,String jobStatus, int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName,String startDate,String enddate,String jobendstartDate,String jobendendDate) 
	{
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();
			System.out.println("disrict id======"+districtId);
			
            System.out.println(" sortOrderStrVal :: "+sortOrderStrVal+" sortColomnName "+sortColomnName);

            Date startD = new Date("1/1/1990");
            Date endD = new Date("12/31/9999"); 

           if(startDate!=null && !startDate.equals(""))
	       startD =  Utility.getCurrentDateFormart(startDate);

           if(enddate!=null && !enddate.equals(""))
        	endD = Utility.getCurrentDateFormart(enddate);
           
           
           Date jobendstartD = new Date("1/1/1990");
           Date jobendendD = new Date("12/31/9999"); 

          if(jobendstartDate!=null && !jobendstartDate.equals(""))
        	  jobendstartD =  Utility.getCurrentDateFormart(jobendstartDate);

          if(jobendendDate!=null && !jobendendDate.equals(""))
        	  jobendendD = Utility.getCurrentDateFormart(jobendendDate);
			
           if(sortColomnName.equalsIgnoreCase("districtName"))
				sortColomnName = "nbyjob.District_Name";
			else if(sortColomnName.equalsIgnoreCase("jobTitle"))
				sortColomnName = "nbyjob.Job_Title";
			else if(sortColomnName.equalsIgnoreCase("status"))
				sortColomnName = "nbyjob.status";
			else if(sortColomnName.equalsIgnoreCase("Responded"))
				sortColomnName = "Responded";
			else if(sortColomnName.equalsIgnoreCase("N"))
				sortColomnName = "N";
			else if(sortColomnName.equalsIgnoreCase("N_Responded"))
				sortColomnName = "N_Responded";
			else if(sortColomnName.equalsIgnoreCase("Black"))
				sortColomnName = "Black";
			else if(sortColomnName.equalsIgnoreCase("Asian"))
				sortColomnName = "Asian";
			else if(sortColomnName.equalsIgnoreCase("Hispanic"))
				sortColomnName = "Hispanic";
			else if(sortColomnName.equalsIgnoreCase("White"))
				sortColomnName = "White";
			else if(sortColomnName.equalsIgnoreCase("AmricanIndian"))
				sortColomnName = "American_Indian";
			else if(sortColomnName.equalsIgnoreCase("Hawaiian"))
				sortColomnName = "Hawaiian";
			else if(sortColomnName.equalsIgnoreCase("Multi"))
				sortColomnName = "Multi";
			
			String statusValue="and jo.status = ?";
	           if(jobStatus!=null)
	           {
	        	   jobStatus = jobStatus.trim();
	        	   if(!jobStatus.equals("") && jobStatus.equalsIgnoreCase("All"))
	        		   statusValue="";
	        		   
	           }
			
			String orderby = " order by "+sortColomnName+" "+sortOrderStrVal ;
			
			 sql=" Select nbyjob.jobid, "+
		    "	nbyjob.District_Name, "+
		    "	nbyjob.Job_Title, "+
		"	sum(`N`) as `N`, "+
		"	round(sum(if(raceIdRecode!=0,N,0))/sum(`N`)*100, 2) as `Responded`,"+
		"    sum(if(ifnull(raceIdRecode,0)!=0,N,0)) as `N_Responded`, "+
		"    case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=1,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		"	end as `Black`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=2,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2) "+
		 "   end as `Asian`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=4,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2) "+
		 "   end as `Hispanic`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=5,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2) "+
		 "   end as `White`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=3,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		 "   end as `American_Indian`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=7,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		 "   end as `Hawaiian`, "+
		 "   case when sum(if(ifnull(raceIdRecode,0)!=0,N,0))<10 then '' "+
		"		else round(sum(if(raceIdRecode=9,N,0))/sum(if(raceIdRecode!=0,N,0))*100, 2)  "+
		"    end as `Multi`, "+
		"       nbyjob.status  "+
		" from "+
		"	( "+
		"		select "+
		"			jo.districtid, "+
		"			jo.jobid, "+
		"			jo.status, "+
		"			raceIdRecode, "+
		 "           jo.jobtitle as Job_Title, "+
		 "           dm.districtName as District_Name, "+
		"			count(*) as `N` "+
		"		from jobforteacher jft "+
		"			inner join teacherdetail td on jft.teacherid=td.teacherid "+
		"			inner join joborder jo on jft.jobid=jo.jobid "+
		 "           inner join districtmaster dm on jo.districtid=dm.districtid "+
		"			inner join ( "+
		"						select td.teacherid, "+
		"								case "+
		"									when length(tpi.raceid)=2 then  "+
		"										if(left(tpi.raceid,1)=6,0,(left(tpi.raceid,1))) "+
		"									when length(tpi.raceid)>2 then 9  "+
		"									when tpi.raceid=6 then 0 "+
		"									else ifnull(tpi.raceid,0) "+
		"								end as raceIdRecode "+
		"						from teacherdetail td "+
		"							left join teacherpersonalinfo tpi on tpi.teacherid=td.teacherid "+
		"						) as recodedRaceId on jft.teacherid=recodedRaceId.teacherid "+
		"			left join racemaster rm on recodedRaceId.raceIdRecode=rm.raceId "+
		"		 where jft.status =6 and dm.status='A' "+statusValue+" and jo.jobStartDate >= ? and jo.jobStartDate <= ?  and jo.jobEndDate >= ? and jo.jobEndDate <= ?  " +
		"		group by "+
		"			jo.districtid, "+
		"			jo.jobid, "+
		"			raceIdRecode "+
		 "           ) nbyjob "+
		" group by districtid, jobid "+
		""+orderby;
			 
			    java.sql.Date startD1 = new java.sql.Date(startD.getTime());
			    java.sql.Date endD1= new java.sql.Date(endD.getTime());
			    
			  				  
				  java.sql.Date jobendstartD1 = new java.sql.Date(jobendstartD.getTime());
				  java.sql.Date jobendendD1= new java.sql.Date(jobendendD.getTime());
					    
				  System.out.println("job start date"+startD1);
				  System.out.println("job start end date"+endD1);
			    
			    
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
				
		if(!statusValue.equals(""))
		{
			ps.setString(1, jobStatus);
			ps.setDate(2, startD1 );
			ps.setDate(3, endD1);
			ps.setDate(4, jobendstartD1 );
			ps.setDate(5, jobendendD1);
		}
		else
		{
			ps.setDate(1, startD1 );
			ps.setDate(2, endD1);
			ps.setDate(3, jobendstartD1);
			ps.setDate(4, jobendendD1);
		}
		
		ResultSet rs=ps.executeQuery();
			
			
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}
	/*******Hired Eeoc Report End*********/
	
	
	@Transactional(readOnly=false)
	 public List<TeacherPersonalInfo> getTeacherByStatusAndSecondaryStatus(List<TeacherDetail> teacherDetails,List<TeacherDetail> sapTeacherDetail)
	 {
		
		List<TeacherPersonalInfo> lstTeacherPersonalInfo = new ArrayList<TeacherPersonalInfo>();
		
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = null;
			Criterion criterion2 = null;
			
			if(teacherDetails.size()>0){
				List<Integer> teacherIds = new ArrayList<Integer>();
				for(TeacherDetail tDetail:teacherDetails){
					teacherIds.add(tDetail.getTeacherId());
				}
				criterion1 = Restrictions.in("teacherId",teacherIds);
			}
			
			 if(sapTeacherDetail.size()>0){
				List<Integer> teacherIds2 = new ArrayList<Integer>();
				for(TeacherDetail tDetail:sapTeacherDetail){
					teacherIds2.add(tDetail.getTeacherId());
				}
				criterion2 = Restrictions.not(Restrictions.in("teacherId", teacherIds2));
			 }
			
			if(teacherDetails.size()>0)
				criteria.add(criterion1);
			
			if(sapTeacherDetail.size()>0)
				criteria.add(criterion2);
			
				lstTeacherPersonalInfo = criteria.list();
				
				System.out.println("getTeacherByStatusAndSecondaryStatus::::::::::::::::::::::::"+lstTeacherPersonalInfo.size());
			
		} catch(Exception exception){
			exception.printStackTrace();
		}
		
		return lstTeacherPersonalInfo;
	 }

	// Optimization for Candidate Pool search replacement of  findByState
	@Transactional(readOnly=false)
	public List<Integer> findByState_Op(StateMaster stateMaster, List<Integer> teachersId)
	{
		List<Integer> teacherPersonalInfoList = new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(Restrictions.eq("stateId",stateMaster));
				criteria.add(Restrictions.in("teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherId"));
				teacherPersonalInfoList= criteria.list();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		return teacherPersonalInfoList;
	}
	
	// Optimization for Candidate Pool search replacement of  findByZipCode
	@Transactional(readOnly=false)
	public List<Integer> findByZipCode_Op(String zipCode ,List<Integer> teachersId)
	{
		List<Integer> teacherPersonalInfoList = new ArrayList<Integer>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("zipCode",zipCode));
			criteria.add(Restrictions.in("teacherId", teachersId));
			criteria.setProjection(Projections.property("teacherId"));
			teacherPersonalInfoList= criteria.list();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return teacherPersonalInfoList;
	}

	//Optimization for candidate pool replacement of checkTeachersBySSN
	@Transactional(readOnly=false)
	public List<Integer> checkTeachersBySSN_Op(String ssn)
	{
		List<Integer> teacherPersonalInfos = new ArrayList<Integer>();
		try 
		{
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("SSN",ssn));
			criteria.setProjection(Projections.property("teacherId"));
			teacherPersonalInfos= criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return teacherPersonalInfos;
	}
	
	//Optimization for candidate pool replacement of checkTeachersByEmpNmbr
	@Transactional(readOnly=false)
	public List<Integer> checkTeachersByEmpNmbr_Op(String empNmbr)
	{
		System.out.println(" checkTeachersByEmpNmbr :: "+empNmbr);
		List<Integer> teacherPersonalInfos = new ArrayList<Integer>();
		try 
		{
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("employeeNumber",empNmbr));
			criteria.setProjection(Projections.property("teacherId"));
			teacherPersonalInfos= criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return teacherPersonalInfos;
	}
	
	// for Optimization of onboarding Dashboard
	
	
	@Transactional(readOnly=false)
	 public List<TeacherPersonalInfo> getTeacherByStatusAndSecondaryStatusOp(List<TeacherDetail> teacherDetails,List<Integer> sapTeacherDetail)
	 {
		
		List<TeacherPersonalInfo> lstTeacherPersonalInfo = new ArrayList<TeacherPersonalInfo>();
		
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = null;
			Criterion criterion2 = null;
			
			if(teacherDetails.size()>0){
				List<Integer> teacherIds = new ArrayList<Integer>();
				for(TeacherDetail tDetail:teacherDetails){
					teacherIds.add(tDetail.getTeacherId());
				}
				criterion1 = Restrictions.in("teacherId",teacherIds);
			}
			
			 if(sapTeacherDetail.size()>0){
				
				criterion2 = Restrictions.not(Restrictions.in("teacherId", sapTeacherDetail));
			 }
			
			if(teacherDetails.size()>0)
				criteria.add(criterion1);
			
			if(sapTeacherDetail.size()>0)
				criteria.add(criterion2);
			
				lstTeacherPersonalInfo = criteria.list();
				
				System.out.println("getTeacherByStatusAndSecondaryStatusOp::::::::::::::::::::::::"+lstTeacherPersonalInfo.size());
			
		} catch(Exception exception){
			exception.printStackTrace();
		}
		
		return lstTeacherPersonalInfo;
	 }
	
	@Transactional(readOnly=false)
	public TeacherDetail findTeacherDetailBySSN(String ssnSearch)
	{
		List<TeacherPersonalInfo>teacherPList=new ArrayList<TeacherPersonalInfo>();
		TeacherDetail ssnTeacherDetail= new TeacherDetail();
		//boolean ssnFlag=false;
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		try{
			if(ssnSearch!=null && !ssnSearch.equals("")){
				String ssnumber=Utility.encodeInBase64(ssnSearch);
				criteria.add(Restrictions.eq("SSN",ssnumber));
				teacherPList = criteria.list();
				if(teacherPList!=null && teacherPList.size()>0){

					if(teacherPList.size()>1){
						try{
							List teacherIds = new ArrayList();
							for (TeacherPersonalInfo teacherPersonalInfo : teacherPList) {
								teacherIds.add(teacherPersonalInfo.getTeacherId());
							}
							List<TeacherDetail> teacherDetails=new ArrayList<TeacherDetail>();
							teacherDetails=teacherDetailDAO.getActiveTeacherList(teacherIds);
							for (TeacherDetail teacherDetail : teacherDetails) {
								if(teacherDetail.getStatus()!=null && teacherDetail.getStatus().equalsIgnoreCase("A")){
									ssnTeacherDetail=teacherDetail;
									break;
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}else{
						ssnTeacherDetail=teacherDetailDAO.findById(teacherPList.get(0).getTeacherId(), false, false);
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return ssnTeacherDetail;
	}
	
	
}
