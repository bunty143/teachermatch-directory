package tm.dao;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.CandidateRawScore;
import tm.bean.TeacherDetail;
import tm.dao.generic.GenericHibernateDAO;

public class CandidateRawScoreDAO extends GenericHibernateDAO<CandidateRawScore, Integer> {
	
	public CandidateRawScoreDAO(){
		super(CandidateRawScore.class);
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to findTeacherCopyInCadidateRawData.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<CandidateRawScore> findTeacherCopyInCadidateRawData(TeacherDetail teacherDetail) 
	{
		
		List<CandidateRawScore> candidateRawScores = null;
		try {
			
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			
			Criterion criterion = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			candidateRawScores = findByCriteria(criterion,criterion1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return candidateRawScores;

	}
}
