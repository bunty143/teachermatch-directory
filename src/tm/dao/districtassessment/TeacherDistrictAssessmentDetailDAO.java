package tm.dao.districtassessment;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentDetail;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherDistrictAssessmentDetailDAO extends GenericHibernateDAO<TeacherDistrictAssessmentDetail, Integer>{

	public TeacherDistrictAssessmentDetailDAO() {
		super(TeacherDistrictAssessmentDetail.class);
	}
	
	@Transactional(readOnly=true)
	public List<TeacherDistrictAssessmentDetail> getTeacherDistrictAssessmentdetails(DistrictAssessmentDetail districtAssessmentDetail,TeacherDetail teacherDetail)
	{
		List<TeacherDistrictAssessmentDetail> teacherDistrictAssessmentdetails = null;
		try {
			
			
			
			Criterion criterion = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
		
			teacherDistrictAssessmentdetails = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherDistrictAssessmentdetails;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherDistrictAssessmentDetail> getTeacherDistrictAssessmentdetailsOfDistrictAndTeacher(List<Integer> teacherIdList)
	{
		List<TeacherDistrictAssessmentDetail> teacherDistrictAssessmentdetailsList = new ArrayList<TeacherDistrictAssessmentDetail>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			if(teacherIdList!=null && teacherIdList.size()>0)
			{
				criteria.createCriteria("teacherDetail").add(Restrictions.in("teacherId", teacherIdList));
				//criteria.add(Restrictions.eq("districtMaster", districtMaster));
				teacherDistrictAssessmentdetailsList = criteria.list();
				
				if(teacherDistrictAssessmentdetailsList==null)
					teacherDistrictAssessmentdetailsList = new ArrayList<TeacherDistrictAssessmentDetail>();
			}
			return teacherDistrictAssessmentdetailsList;
		}
		
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return teacherDistrictAssessmentdetailsList;
	}
}
