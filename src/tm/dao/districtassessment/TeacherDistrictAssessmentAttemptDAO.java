package tm.dao.districtassessment;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentAttempt;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentAttempt;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherDistrictAssessmentAttemptDAO extends GenericHibernateDAO<TeacherDistrictAssessmentAttempt, Integer>{

	public TeacherDistrictAssessmentAttemptDAO() {
		super(TeacherDistrictAssessmentAttempt.class);
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentAttempt> findDistrictAssessmentAttempts(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
	
		List<TeacherDistrictAssessmentAttempt> teacherDistrictAssessmentAttempts = null;
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = null;
			criterion1 = Restrictions.eq("jobOrder", jobOrder);
			teacherDistrictAssessmentAttempts = findByCriteria(criterion,criterion1);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherDistrictAssessmentAttempts;
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<TeacherDistrictAssessmentAttempt> findNoOfAttempts(DistrictAssessmentDetail districtAssessmentDetail,TeacherDetail teacherDetail) 
	{
		List<TeacherDistrictAssessmentAttempt> teacherDistrictAssessmentAttempts = null;
		try {

			Criterion criterion = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			teacherDistrictAssessmentAttempts = findByCriteria(Order.asc("attemptId"),criterion,criterion1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherDistrictAssessmentAttempts;

	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment responses of particular assessment.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int updateDistrictAssessmentAttemptSessionTime(int id,boolean forced) 
	{

		try{
			int seconds = 0; 
			TeacherDistrictAssessmentAttempt teacherDistrictAssessmentAttempt = findById(id, false, false); 
			if(teacherDistrictAssessmentAttempt!=null)
			{
				//Date asssessmentEndTime= teacherAssessmentAttempt.getAssessmentEndTime();
				Date asssessmentStartTime= teacherDistrictAssessmentAttempt.getDistrictAssessmentStartTime();
				Date d=new Date();
				//System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk "+asssessmentEndTime);

				//long diff = asssessmentEndTime.getTime() - asssessmentStartTime.getTime();
				long diff = d.getTime() - asssessmentStartTime.getTime();

				//System.out.println("diff:::::::::::: "+diff);
				seconds = (int) (diff / 1000)  ;
				//seconds = (int) (diff  % 60 );
				//seconds = (int) (diff / 1000);

				teacherDistrictAssessmentAttempt.setDistrictAssessmentEndTime(new Date());
				teacherDistrictAssessmentAttempt.setDistrictAssessmentSessionTime(seconds);
				if(forced)
					teacherDistrictAssessmentAttempt.setIsForced(true);

				makePersistent(teacherDistrictAssessmentAttempt);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}
}
