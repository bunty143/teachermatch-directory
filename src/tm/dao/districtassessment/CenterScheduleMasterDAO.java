package tm.dao.districtassessment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.districtassessment.CenterScheduleMaster;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class CenterScheduleMasterDAO extends GenericHibernateDAO<CenterScheduleMaster, Integer> 
{
	public CenterScheduleMasterDAO() {
		super(CenterScheduleMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<CenterScheduleMaster> getSchedule()
	{
		List<CenterScheduleMaster> centerScheduleMasterList=new ArrayList<CenterScheduleMaster>();
		try
		{
			centerScheduleMasterList=findByCriteria(Order.asc("csTime"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return centerScheduleMasterList;
	}
	
	@Transactional(readOnly=false)
	public List<CenterScheduleMaster> getScheduleByDate(DistrictMaster districtMaster,String startDate, String endDate)
	{
		List<CenterScheduleMaster> centerScheduleMasterList=new ArrayList<CenterScheduleMaster>();
		try
		{
			Date date1 = Utility.getCurrentDateFormart(startDate);
			Date date2 = Utility.getCurrentDateFormart(endDate);
			
			Criterion criterion = Restrictions.between("csDate", date1, date2);
			Criterion criterion2 = Restrictions.eq("districtMaster", districtMaster);
			centerScheduleMasterList=findByCriteria(Order.asc("csTime"), criterion,criterion2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return centerScheduleMasterList;
	}

	
	@Transactional(readOnly=false)
	public List<CenterScheduleMaster> getScheduleByDate(DistrictMaster districtMaster,Date date)
	{
		List<CenterScheduleMaster> centerScheduleMasterList=new ArrayList<CenterScheduleMaster>();
		try {
			Criterion criterion=Restrictions.eq("csDate",date);
			Criterion criterion2=Restrictions.eq("districtMaster",districtMaster);
			centerScheduleMasterList=findByCriteria(Order.asc("timeOrder"),criterion,criterion2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return centerScheduleMasterList;
	}
	
	@Transactional(readOnly=false)
	public List<CenterScheduleMaster> getScheduleByDistrict(DistrictMaster districtMaster)
	{
		List<CenterScheduleMaster> centerScheduleMasterList=new ArrayList<CenterScheduleMaster>();
		try {
			Criterion criterion=Restrictions.eq("districtMaster",districtMaster);
			centerScheduleMasterList=findByCriteria(Order.asc("timeOrder"),criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return centerScheduleMasterList;
	}
	
	@Transactional(readOnly=false)
	public List<CenterScheduleMaster> getScheduleByDistrictAndDate(DistrictMaster districtMaster, String startDate, String endDate)
	{
		List<CenterScheduleMaster> centerScheduleMasterList=new ArrayList<CenterScheduleMaster>();
		try
		{
			java.util.Date date1 =null;
			java.util.Date date2 =null;
			
			if(startDate!=null && !startDate.equals(""))
				date1 = Utility.getCurrentDateFormart(startDate);
			if(endDate!=null && !endDate.equals(""))
				date2 = Utility.getCurrentDateFormart(endDate);
			
			Criterion criterion1 = Restrictions.between("csDate", date1, date2);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			
			Criterion criterion = Restrictions.and(criterion1, criterion2);
						
			centerScheduleMasterList=findByCriteria(Order.asc("timeOrder"),criterion);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return centerScheduleMasterList;
	}
	
	@Transactional(readOnly=false)
	public List<CenterScheduleMaster> getScheduleByDistrictAndDate(DistrictMaster districtMaster,Date date)
	{
		List<CenterScheduleMaster> centerScheduleMasterList=new ArrayList<CenterScheduleMaster>();
		try {
			Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("csDate",date);
			centerScheduleMasterList=findByCriteria(Order.asc("timeOrder"),criterion,criterion2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return centerScheduleMasterList;
	}
	
	@Transactional(readOnly=false)
	public List<CenterScheduleMaster> getScheduleByDateAndAvailability(DistrictMaster districtMaster,Date date)
	{
		List<CenterScheduleMaster> centerScheduleMasterList=new ArrayList<CenterScheduleMaster>();
		try {
			Criterion criterion = Restrictions.eq("csDate",date);
			Criterion criterion2 = Restrictions.eq("availability",1);
			Criterion criterion3 = Restrictions.eq("districtMaster",districtMaster);
		//	Criterion criterion1 = Restrictions.or(criterion2, criterion3);
			centerScheduleMasterList=findByCriteria(Order.asc("timeOrder"),criterion, criterion2, criterion3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return centerScheduleMasterList;
	}
	
	@Transactional(readOnly=false)
	public List<CenterScheduleMaster> getScheduleByDateAndUnAvailability(DistrictMaster districtMaster,Date date)
	{
		List<CenterScheduleMaster> centerScheduleMasterList=new ArrayList<CenterScheduleMaster>();
		try {
			Criterion criterion = Restrictions.eq("csDate",date);
			Criterion criterion2 = Restrictions.eq("availability",0);
			//Criterion criterion3 = Restrictions.isNull("availability");
		//	Criterion criterion1 = Restrictions.or(criterion2, criterion3);
			centerScheduleMasterList=findByCriteria(Order.asc("timeOrder"),criterion, criterion2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return centerScheduleMasterList;
	}
	
	@Transactional(readOnly=false)
	public List<CenterScheduleMaster> getScheduleForCandidate()
	{
		List<CenterScheduleMaster> centerScheduleMasterList=new ArrayList<CenterScheduleMaster>();
		try {
			Criterion criterion = Restrictions.eq("availability",1);
			Criterion criterion1 = Restrictions.isNull("availability");
			centerScheduleMasterList=findByCriteria(Order.asc("csTime"),criterion, criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return centerScheduleMasterList;
	}
}
