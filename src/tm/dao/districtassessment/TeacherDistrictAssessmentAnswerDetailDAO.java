package tm.dao.districtassessment;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentAnswerDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentQuestion;
import tm.bean.districtassessment.TeacherDistrictAssessmentStatus;
import tm.bean.master.DistrictSpecificPortfolioAnswers;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherDistrictAssessmentAnswerDetailDAO extends GenericHibernateDAO<TeacherDistrictAssessmentAnswerDetail, Integer>{

	public TeacherDistrictAssessmentAnswerDetailDAO() {
		super(TeacherDistrictAssessmentAnswerDetail.class);
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentAnswerDetail> getAssessmentAnswerDetails(TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail,TeacherDetail teacherDetail,TeacherDistrictAssessmentStatus teacherDistrictAssessmentStatus)
	{
		List<TeacherDistrictAssessmentAnswerDetail> teacherDistrictAssessmentAnswerDetailList = new ArrayList<TeacherDistrictAssessmentAnswerDetail>();
		try 
		{
			Session session = getSession();
			Criterion criterion = Restrictions.eq("teacherDistrictAssessmentDetail", teacherDistrictAssessmentDetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDistrictAssessmentDetail.getTeacherDetail());
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion);
			criteria.add(criterion1);
			teacherDistrictAssessmentAnswerDetailList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return teacherDistrictAssessmentAnswerDetailList;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentAnswerDetail> findDistrictAssessmentStatusByTeacherAndDAMT(TeacherDetail teacherDetail,List<DistrictAssessmentDetail> districtAssessmentDetails)
	{

		List<TeacherDistrictAssessmentAnswerDetail> teacherDistrictAssessmentStatusList = null;
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = Restrictions.in("districtAssessmentDetail", districtAssessmentDetails);

			teacherDistrictAssessmentStatusList = findByCriteria(criterion,criterion1);


		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherDistrictAssessmentStatusList;

	}
	
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentAnswerDetail> findTeacherAssessmentQuestionAnswers(TeacherDetail teacherDetail,DistrictAssessmentDetail districtAssessmentDetail,List<TeacherDistrictAssessmentQuestion> questionIds)
	{

		List<TeacherDistrictAssessmentAnswerDetail> teacherDistrictAssessmentStatusList = new ArrayList<TeacherDistrictAssessmentAnswerDetail>();
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = Restrictions.eq("districtAssessmentDetail", districtAssessmentDetail);
			if(questionIds!=null)
			{
				Criterion criterion2 = Restrictions.in("teacherDistrictAssessmentQuestion", questionIds);
				teacherDistrictAssessmentStatusList = findByCriteria(criterion,criterion1,criterion2);
			}
			else
			{
				teacherDistrictAssessmentStatusList = findByCriteria(criterion,criterion1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherDistrictAssessmentStatusList;

	}
	
	@Transactional(readOnly=false)
	public List<TeacherDistrictAssessmentAnswerDetail> findTeacherAssessmentQuestionAnswersByTeachers(List<TeacherDetail> teacherDetail)
	{

		List<TeacherDistrictAssessmentAnswerDetail> teacherDistrictAssessmentStatusList = new ArrayList<TeacherDistrictAssessmentAnswerDetail>();
		try {
			Criterion criterion = Restrictions.in("teacherDetail", teacherDetail);
			teacherDistrictAssessmentStatusList = findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherDistrictAssessmentStatusList;

	}
}
