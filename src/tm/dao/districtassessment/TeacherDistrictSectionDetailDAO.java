package tm.dao.districtassessment;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.districtassessment.TeacherDistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictSectionDetail;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherDistrictSectionDetailDAO extends GenericHibernateDAO<TeacherDistrictSectionDetail, Integer> 
{
	public TeacherDistrictSectionDetailDAO() {
		super(TeacherDistrictSectionDetail.class);
	}
	
	@Transactional(readOnly=true)
	public List<TeacherDistrictSectionDetail> getSectionsByTeacherDistrictAssessmentId(TeacherDistrictAssessmentDetail teacherDistrictAssessmentDetail)
	{
		List<TeacherDistrictSectionDetail> teacherDistrictAssessmentSectionList = null;

		Criterion criterion = Restrictions.eq("teacherDistrictAssessmentdetail", teacherDistrictAssessmentDetail);
		teacherDistrictAssessmentSectionList = findByCriteria(criterion);

		return teacherDistrictAssessmentSectionList;	
	}
}
