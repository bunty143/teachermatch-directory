package tm.dao.districtassessment;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictStrikeLog;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherDistrictStrikeLogDAO extends GenericHibernateDAO<TeacherDistrictStrikeLog, Integer> 
{
	public TeacherDistrictStrikeLogDAO() {
		super(TeacherDistrictStrikeLog.class);
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDistrictStrikeLog> getTotalStrikesByTeacher(TeacherDetail teacherDetail, TeacherDistrictAssessmentDetail teacherDistrictAssessmentdetail)
	{
		List<TeacherDistrictStrikeLog> teacherDistrictStrikeLogList= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("teacherDistrictAssessmentdetail",teacherDistrictAssessmentdetail);
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);

			teacherDistrictStrikeLogList = findByCriteria(criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherDistrictStrikeLogList;
	}
}
