package tm.dao.textfile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EmployeeMaster;
import tm.bean.textfile.LicensureNBPTS;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class LicensureNBPTSDao extends GenericHibernateDAO<LicensureNBPTS, Integer> {

	public LicensureNBPTSDao() {
		super(LicensureNBPTS.class);
		 
	}
	
/*	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public LicensureNBPTS findBySSN (String ssn){
		
		Criterion crtSSN=Restrictions.eq("SSN", ssn);
		List<LicensureNBPTS> license = findByCriteria(crtSSN);
		
		if(license.size()>0)
			return license.get(0);
		else
			return null;
		
	}*/
	
	@Transactional(readOnly=false)
	public List<LicensureNBPTS> getDataBySsnList(List<String> ssnList)
	{
		List<LicensureNBPTS> LicensureNBPTSList = new ArrayList<LicensureNBPTS>();
		Date dateWithoutTime = Utility.getDateWithoutTime();
		try 
		{
			Criterion criterion1 = Restrictions.in("SSN",ssnList);
			Criterion crDoNotHire =  Restrictions.eq("status","A");
			//Criterion crDate =  Restrictions.ge("expirationDate",dateWithoutTime);
			
			LicensureNBPTSList = findByCriteria(criterion1,crDoNotHire);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return LicensureNBPTSList;
	}

}
