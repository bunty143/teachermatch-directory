package tm.dao.textfile;


import tm.bean.textfile.LicenseBasisDomain;
import tm.dao.generic.GenericHibernateDAO;

/* 
 * @Author: Sandeep Yadav
 */

public class LicenseBasisDomainDAO extends GenericHibernateDAO<LicenseBasisDomain, Integer>{
	
	
	public LicenseBasisDomainDAO() 
	{
		super(LicenseBasisDomain.class);
	}

}
