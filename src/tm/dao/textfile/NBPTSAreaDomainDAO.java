package tm.dao.textfile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.textfile.LicensureNBPTS;
import tm.bean.textfile.NBPTSAreaDomain;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class NBPTSAreaDomainDAO extends GenericHibernateDAO<NBPTSAreaDomain, Integer> {

	public NBPTSAreaDomainDAO() {
		super(NBPTSAreaDomain.class);
		// TODO Auto-generated constructor stub
	}
	
	@Transactional(readOnly=false)
	public List<NBPTSAreaDomain> getDataByNBPTSAreaCode(String areaCode)
	{
		List<NBPTSAreaDomain> LicensureNBPTSList = new ArrayList<NBPTSAreaDomain>();
	try 
		{
			Criterion criterion1 = Restrictions.eq("nbptsAreaCode",areaCode);
			Criterion crDoNotHire =  Restrictions.eq("status","A");					
			LicensureNBPTSList = findByCriteria(criterion1,crDoNotHire);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return LicensureNBPTSList;
	}

}
