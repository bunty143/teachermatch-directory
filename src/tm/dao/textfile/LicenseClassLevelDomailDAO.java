package tm.dao.textfile;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.textfile.LicenseClassLevelDomain;
import tm.dao.generic.GenericHibernateDAO;

public class LicenseClassLevelDomailDAO extends GenericHibernateDAO<LicenseClassLevelDomain, Integer>{

	public LicenseClassLevelDomailDAO() {
		super(LicenseClassLevelDomain.class);
		
	}
	
	@Transactional(readOnly=false)
	public List<LicenseClassLevelDomain> getClassName(String certName)
	{
		List<LicenseClassLevelDomain> licenseClassLevelDomain=null;
		
		try
		{
			Criterion criterion =Restrictions.eq("classCode", certName);
			licenseClassLevelDomain= findByCriteria(criterion);
		}
		catch(Exception e){System.out.println(e);}
		return licenseClassLevelDomain;
		
	}
	
	@Transactional(readOnly=false)
	public List<LicenseClassLevelDomain> getListByClassCodeList(List<String> codeList){
		List<LicenseClassLevelDomain> claLevelDomains = new ArrayList<LicenseClassLevelDomain>();
		try{
			Criterion cr = Restrictions.in("classCode", codeList);
			claLevelDomains = findByCriteria(cr);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return claLevelDomains;
	}

}
