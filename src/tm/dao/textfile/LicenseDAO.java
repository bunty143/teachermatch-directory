package tm.dao.textfile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.textfile.License;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;


	public class LicenseDAO extends GenericHibernateDAO<License, Integer>{
		
		
		public LicenseDAO() 
		{
			super(License.class);
		}
		
		@Transactional(readOnly=false)
		@SuppressWarnings("unchecked")
		public  List<License> getSSN(String ssn) 
		{
			List<License> lstSSN=new ArrayList<License>(); 
			try
			{
			System.out.println("Dao getSSN method call from ajax");
			
			Criterion criterion			=	Restrictions.eq("status", "A");
			Criterion criterion1		=	Restrictions.eq("SSN", ssn);
			lstSSN	     		=	findByCriteria(criterion,criterion1);
			System.out.println("lstSSNlstSSN::::"+lstSSN.size());
			}catch(Exception e){e.printStackTrace();}
			
	return lstSSN;
		}
		
		@Transactional(readOnly=false)
		public List<License> findListBySsn(List<String> ssnList)
		{
			List<License> lecenseList = new ArrayList<License>();
			Date dateWithoutTime = Utility.getDateWithoutTime();
			try 
			{
				if(ssnList!=null && ssnList.size()>0)
				{
				Criterion crSSN = Restrictions.in("SSN",ssnList);
				Criterion crStatus = Restrictions.eq("status","A");
			    Criterion crDate =  Restrictions.isNotNull("licenseRevokeRescindDate");
			    Criterion revokedSataus =  Restrictions.eq("revocationStatus","1");
				
			//	Criterion crDate1 =  Restrictions.eq("licenseRevokeRescindDate",dateWithoutTime);
			//	Criterion crDate2 =  Restrictions.or(crDate1);
				
				lecenseList = findByCriteria(crSSN,crStatus,revokedSataus,crDate);
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			if(lecenseList==null || lecenseList.size()==0)
				return null;
			else
				return lecenseList;
		}
		
		
		
	}
