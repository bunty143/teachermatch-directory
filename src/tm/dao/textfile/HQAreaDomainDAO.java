package tm.dao.textfile;

import tm.bean.textfile.HQAreaDomain;
import tm.dao.generic.GenericHibernateDAO;

/* 
 * @Author: Sandeep Yadav
 */

public class HQAreaDomainDAO extends GenericHibernateDAO<HQAreaDomain, Integer>{
	
	
	public HQAreaDomainDAO() 
	{
		super(HQAreaDomain.class);
	}

}
