package tm.dao.textfile;

import java.io.Serializable;

import tm.bean.textfile.LicensureEducation;
import tm.dao.generic.GenericHibernateDAO;

public class LicensureEducationDAO extends GenericHibernateDAO<LicensureEducation, Serializable> {
 

	public LicensureEducationDAO() {
		super(LicensureEducation.class);
		
	}

}
