package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.ApplitrackDistricts;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class ApplitrackDistrictsDAO extends GenericHibernateDAO<ApplitrackDistricts, Integer> 
{
	public ApplitrackDistrictsDAO() {
		super(ApplitrackDistricts.class);
	}
	
	@Transactional(readOnly=true)
	public ApplitrackDistricts findByDistrictId(DistrictMaster districtMaster)
	{
	         List <ApplitrackDistricts> lstApplitrackDistricts = new ArrayList<ApplitrackDistricts>();
			 Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
			 lstApplitrackDistricts = findByCriteria(criterion);
			 if(lstApplitrackDistricts.size()>0)
				 return lstApplitrackDistricts.get(0);
			 else
				 return null;
	}
}
