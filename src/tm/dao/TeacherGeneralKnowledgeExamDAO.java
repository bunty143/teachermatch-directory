package tm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobRequisitionNumbers;
import tm.bean.TeacherDetail;
import tm.bean.TeacherGeneralKnowledgeExam;
import tm.bean.TeacherSubjectAreaExam;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherGeneralKnowledgeExamDAO extends GenericHibernateDAO<TeacherGeneralKnowledgeExam, Integer>{
	public TeacherGeneralKnowledgeExamDAO() {
		super(TeacherGeneralKnowledgeExam.class);
	}
	
	@Transactional(readOnly=false)
	public TeacherGeneralKnowledgeExam findTeacherGeneralKnowledgeExam(TeacherDetail teacherDetail)
	{
		List<TeacherGeneralKnowledgeExam> lstExam= new ArrayList<TeacherGeneralKnowledgeExam>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstExam = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstExam==null || lstExam.size()==0)
			return null;
		else
			return lstExam.get(0);
	}
}
