package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictSpecificVideoInterviewDetails;
import tm.bean.JobOrder;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificVideoInterviewDetailsDAO extends GenericHibernateDAO<DistrictSpecificVideoInterviewDetails, Integer> 
{
	public DistrictSpecificVideoInterviewDetailsDAO() {
		super(DistrictSpecificVideoInterviewDetails.class);
	}
	
	@Transactional(readOnly=true)
	public List<DistrictSpecificVideoInterviewDetails> checkCandidateDataSubmittedByEmail(String email,DistrictMaster districtMaster)
	{
		List<DistrictSpecificVideoInterviewDetails> districtSpecificVideoInterviewDetails = new ArrayList<DistrictSpecificVideoInterviewDetails>();
		try {
			Criterion criterion = Restrictions.eq("emailAddress", email);
			Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
			districtSpecificVideoInterviewDetails = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return districtSpecificVideoInterviewDetails;
	}
	
	@Transactional(readOnly=true)
	public List<DistrictSpecificVideoInterviewDetails> checkCandidateDataSubmittedByEmails(List<String> emails,DistrictMaster districtMaster)
	{
		List<DistrictSpecificVideoInterviewDetails> districtSpecificVideoInterviewDetails = new ArrayList<DistrictSpecificVideoInterviewDetails>();
		try {
			Criterion criterion = Restrictions.in("emailAddress", emails);
			Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
			districtSpecificVideoInterviewDetails = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return districtSpecificVideoInterviewDetails;
	}
	@Transactional(readOnly=true)
	public List<DistrictSpecificVideoInterviewDetails> checkCandidateDataSubmittedByEmailsByHBD(List<String> emails,JobOrder jobOrder)
	{
		List<DistrictSpecificVideoInterviewDetails> districtSpecificVideoInterviewDetails = new ArrayList<DistrictSpecificVideoInterviewDetails>();
		
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			criteria.add(Restrictions.eq("emailAddress",emails));
			if(emails.size()!=0)
				districtSpecificVideoInterviewDetails = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return districtSpecificVideoInterviewDetails;
	}
}
