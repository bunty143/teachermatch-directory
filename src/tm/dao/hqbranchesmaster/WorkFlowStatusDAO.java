package tm.dao.hqbranchesmaster;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.WorkFlowStatus;
import tm.bean.mq.MQEvent;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class WorkFlowStatusDAO extends GenericHibernateDAO<WorkFlowStatus, Integer> 
{
	public WorkFlowStatusDAO() 
	{
		super(WorkFlowStatus.class);
	}

	@Transactional(readOnly=true)
	public List<WorkFlowStatus> findActiveSpecificWFStatus()
	{
		String workFlowStatusCOM = "COM";
		String workFlowStatusCANCEL = "CANC";
		String workFlowStatusERR = "ERR";
		String workFlowStatusN = "N";
		
		List<String> stausesInWF = new ArrayList<String>();

		stausesInWF.add(workFlowStatusCOM);
		stausesInWF.add(workFlowStatusCANCEL);
		stausesInWF.add(workFlowStatusERR);
		stausesInWF.add(workFlowStatusN);

		List <WorkFlowStatus> lstWf =new ArrayList<WorkFlowStatus>();
		try {
		    Criterion criteria1	 =	Restrictions.in("workFlowStatus",stausesInWF);
		    lstWf = findByCriteria(criteria1);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstWf;
	}
	
	@Transactional(readOnly=true)
	public List<WorkFlowStatus> findSpecificDisableWFStatus()
	{
		String workFlowStatusCANCEL = "CANC";
		String workFlowStatusERR = "ERR";
		String workFlowStatusN = "N";
		
		List<String> stausesInWF = new ArrayList<String>();

		stausesInWF.add(workFlowStatusCANCEL);
		stausesInWF.add(workFlowStatusERR);
		stausesInWF.add(workFlowStatusN);

		List <WorkFlowStatus> lstWf =new ArrayList<WorkFlowStatus>();
		try {
		    Criterion criteria1	 =	Restrictions.in("workFlowStatus",stausesInWF);
		    lstWf = findByCriteria(criteria1);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        return lstWf;
	}
}
