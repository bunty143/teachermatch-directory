package tm.dao.hqbranchesmaster;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.HqBranchesDistricts;
import tm.dao.generic.GenericHibernateDAO;

public class HqBranchesDistrictsDAO extends GenericHibernateDAO<HqBranchesDistricts, Integer> 
{
	public HqBranchesDistrictsDAO() 
	{
		super(HqBranchesDistricts.class);
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<BranchMaster> findBranchesWithHeadQuarter(HeadQuarterMaster headQuarterMaster,  Order sortOrderStrVal,int startPos,int limit)
	{
		List<BranchMaster> branchMasters = new ArrayList<BranchMaster>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
	
			criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			
				Criteria c1= null;
				c1 = criteria.createCriteria("branchMaster");			
				c1.addOrder(sortOrderStrVal);		
				criteria.setProjection(Projections.projectionList().add( Projections.groupProperty("branchMaster")));			
				criteria.setFirstResult(startPos);
				criteria.setMaxResults(limit);
				branchMasters = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return branchMasters;
	}
	
	
}
