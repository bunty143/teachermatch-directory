package tm.dao.hqbranchesmaster;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.TempDistricts;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TempDistrictsDAO extends GenericHibernateDAO<TempDistricts, Integer> 
{
	public TempDistrictsDAO() 
	{
		super(TempDistricts.class);
	}
	
	@Transactional(readOnly=false)
	public List<TempDistricts> getTempDistrictsBySessionId(String tempId)
	{
		List<TempDistricts> tempDistricts = new ArrayList<TempDistricts>();
		try {

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("tempId", tempId);
			criteria.add(criterion);
			
			tempDistricts = criteria.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return tempDistricts;
	}
	
	@Transactional(readOnly=false)
	public List<TempDistricts> getTempDistrictsBySessionIdAndDistrictId(String tempId,DistrictMaster districtMaster)
	{
		List<TempDistricts> tempDistricts = new ArrayList<TempDistricts>();
		try {

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("tempId", tempId);
			Criterion criterion2 = Restrictions.eq("districtMaster", districtMaster);
			criteria.add(criterion);
			criteria.add(criterion2);
			
			tempDistricts = criteria.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return tempDistricts;
	}
}
