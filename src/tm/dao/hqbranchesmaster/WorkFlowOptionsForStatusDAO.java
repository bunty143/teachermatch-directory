package tm.dao.hqbranchesmaster;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.hqbranchesmaster.WorkFlowOptionsForStatus;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.dao.generic.GenericHibernateDAO;

public class WorkFlowOptionsForStatusDAO extends GenericHibernateDAO<WorkFlowOptionsForStatus, Integer> 
{
	public WorkFlowOptionsForStatusDAO() 
	{
		super(WorkFlowOptionsForStatus.class);
	}
	
	@Transactional(readOnly=false)
	public List<WorkFlowOptionsForStatus> getFlowOptionsForStatus()
	{
		List<WorkFlowOptionsForStatus> flowOptionsList = new ArrayList<WorkFlowOptionsForStatus>();
		try {

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			flowOptionsList = criteria.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return flowOptionsList;
	}
	
	@Transactional(readOnly=true)
	public WorkFlowOptionsForStatus findOptionsByTeacherStatus(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
        List <WorkFlowOptionsForStatus> lstFlowOptionsForStatus =new ArrayList<WorkFlowOptionsForStatus>();
        WorkFlowOptionsForStatus flowOptionsForStatus=null;
        try{
	         Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail.getTeacherId());
	         Criterion criterion2 = Restrictions.eq("jobId",jobOrder.getJobId());
	         Criterion criterion4 = Restrictions.eq("status","A");
	         Criterion criterion3 = null;
	         if(statusMaster!=null){
	        	 criterion3 = Restrictions.eq("statusId",statusMaster.getStatusId());
	         }else{
	        	 criterion3 = Restrictions.eq("secondaryStatusId",secondaryStatus.getSecondaryStatusId());
	         }
	         lstFlowOptionsForStatus = findByCriteria(criterion1,criterion2,criterion3,criterion4);
	         if(lstFlowOptionsForStatus!=null && lstFlowOptionsForStatus.size()>0){
	        	 flowOptionsForStatus=lstFlowOptionsForStatus.get(0);
	         }
        }catch(Exception e){
        	e.printStackTrace();
        }
		return flowOptionsForStatus;
	}
}
