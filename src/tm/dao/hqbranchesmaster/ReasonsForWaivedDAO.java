package tm.dao.hqbranchesmaster;

import tm.bean.hqbranchesmaster.ReasonsForWaived;
import tm.dao.generic.GenericHibernateDAO;

public class ReasonsForWaivedDAO extends GenericHibernateDAO<ReasonsForWaived, Integer> {
	public ReasonsForWaivedDAO() {
		super(ReasonsForWaived.class);
	}

}
