package tm.dao.hqbranchesmaster;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.StatusPrivilegeForHqBrDt;
import tm.dao.generic.GenericHibernateDAO;

public class StatusPrivilegeForHqBrDtDAO extends GenericHibernateDAO<StatusPrivilegeForHqBrDt, Integer> 
{
	public StatusPrivilegeForHqBrDtDAO() 
	{
		super(StatusPrivilegeForHqBrDt.class);
	}
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForHqBrDt> findStatusPrivilegeForStatusMasterForHQ(HeadQuarterMaster headQuarterMaster)
	{
		List<StatusPrivilegeForHqBrDt> lstStatusPForSchool= new ArrayList<StatusPrivilegeForHqBrDt>();
		try 
		{			
			Criterion criterion_dm = Restrictions.eq("headQuarterMaster", headQuarterMaster);
			Criterion criterion_sm = Restrictions.isNotNull("statusMaster");
			Criterion criterion_ss = Restrictions.isNull("secondaryStatus");
			lstStatusPForSchool = findByCriteria(criterion_dm,criterion_sm,criterion_ss);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstStatusPForSchool;

	}
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForHqBrDt> findStatusPrivilegeForSecondaryStatusIdForHQ(HeadQuarterMaster headQuarterMaster)
	{
		List<StatusPrivilegeForHqBrDt> lstStatusPForSchool= new ArrayList<StatusPrivilegeForHqBrDt>();
		try 
		{
			Criterion criterion_dm = Restrictions.eq("headQuarterMaster", headQuarterMaster);
			Criterion criterion_sm = Restrictions.isNull("statusMaster");
			Criterion criterion_ss = Restrictions.isNotNull("secondaryStatus");
			lstStatusPForSchool = findByCriteria(criterion_dm,criterion_ss,criterion_sm);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

			return lstStatusPForSchool;

	}
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForHqBrDt> findStatusPrivilegeForStatusMasterForBranch(BranchMaster branchMaster)
	{
		List<StatusPrivilegeForHqBrDt> lstStatusPForSchool= new ArrayList<StatusPrivilegeForHqBrDt>();
		try 
		{			
			Criterion criterion_dm = Restrictions.eq("branchMaster", branchMaster);
			Criterion criterion_sm = Restrictions.isNotNull("statusMaster");
			Criterion criterion_ss = Restrictions.isNull("secondaryStatus");
			lstStatusPForSchool = findByCriteria(criterion_dm,criterion_sm,criterion_ss);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstStatusPForSchool;

	}
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForHqBrDt> findStatusPrivilegeForSecondaryStatusIdForBranch(BranchMaster branchMaster)
	{
		List<StatusPrivilegeForHqBrDt> lstStatusPForSchool= new ArrayList<StatusPrivilegeForHqBrDt>();
		try 
		{
			Criterion criterion_dm = Restrictions.eq("branchMaster", branchMaster);
			Criterion criterion_sm = Restrictions.isNull("statusMaster");
			Criterion criterion_ss = Restrictions.isNotNull("secondaryStatus");
			lstStatusPForSchool = findByCriteria(criterion_dm,criterion_ss,criterion_sm);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

			return lstStatusPForSchool;

	}
	
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForHqBrDt> findHqStatusForStatusMasterId(HeadQuarterMaster headQuarterMaster)
	{
		List<StatusPrivilegeForHqBrDt> lstStatusPForSchool= new ArrayList<StatusPrivilegeForHqBrDt>();
		try 
		{
			Criterion criterion = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion_status=Restrictions.eq("canBranchSetStatus", 1);
			Criterion criterion_sm=Restrictions.isNotNull("statusMaster");
			lstStatusPForSchool = findByCriteria(criterion,criterion_status,criterion_sm);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

			return lstStatusPForSchool;
	}
	
	
	@Transactional(readOnly=false)
	public List<StatusPrivilegeForHqBrDt> findHqStatusForSecondaryStatusId(HeadQuarterMaster headQuarterMaster)
	{
		List<StatusPrivilegeForHqBrDt> lstStatusPForSchool= new ArrayList<StatusPrivilegeForHqBrDt>();
		try 
		{
			Criterion criterion = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion_status=Restrictions.eq("canBranchSetStatus", 1);
			Criterion criterion_ss=Restrictions.isNotNull("secondaryStatus");
			lstStatusPForSchool = findByCriteria(criterion,criterion_status,criterion_ss);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
			return lstStatusPForSchool;
	}
}
