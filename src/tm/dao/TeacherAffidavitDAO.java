package tm.dao;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherAffidavit;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPortfolioStatus;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherAffidavitDAO extends GenericHibernateDAO<TeacherAffidavit, Integer> 
{
	public TeacherAffidavitDAO() 
	{
		super(TeacherAffidavit.class);
	}
	
	@Transactional(readOnly=false)
	public TeacherAffidavit findAffidavitByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherAffidavit> lstTeacherAffidavit= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			lstTeacherAffidavit = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherAffidavit==null || lstTeacherAffidavit.size()==0)
		return null;
		else
		return lstTeacherAffidavit.get(0);
	}

}
