package tm.dao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.BatchJobCertification;
import tm.bean.BatchJobOrder;
import tm.bean.JobCertification;
import tm.bean.SchoolInBatchJob;
import tm.bean.master.SchoolMaster;
import tm.dao.generic.GenericHibernateDAO;

public class BatchJobCertificationDAO extends GenericHibernateDAO<BatchJobCertification, Integer> 
{
	public BatchJobCertificationDAO() 
	{
		super(BatchJobCertification.class);
	}
	/*@Transactional(readOnly=false)
	public BatchJobCertification findCertificationByJob(BatchJobOrder batchJobOrder,String CertificationType)
	{
		List<BatchJobCertification> lstBatchJobCertification= null;
		try{  
			if(!CertificationType.equals("")){
				Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
				Criterion criterion2 = Restrictions.ilike("certType","%"+CertificationType.trim()+"%" );
				lstBatchJobCertification = findByCriteria(criterion1,criterion2);
				
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		if(lstBatchJobCertification==null || lstBatchJobCertification.size()==0)
			return null;
		else
			return lstBatchJobCertification.get(0);
	}
	
	@Transactional(readOnly=false)
	public String findCertificationTypeListByJob(BatchJobOrder batchJobOrder,String CertificationType)
	{
		List<BatchJobCertification> lstBatchJobCertification= null;
		String  jcRecords= "";
		try{  
			
				Criterion criterion1 = Restrictions.eq("batchJobId",batchJobOrder);
				if(!CertificationType.equals("")){
					Criterion criterion2 = Restrictions.ilike("certType","%"+CertificationType.trim()+"%" );
					lstBatchJobCertification = findByCriteria(criterion1,criterion2);
				}else{
					lstBatchJobCertification = findByCriteria(criterion1);
				}
				int iCertification=0;
				for (BatchJobCertification batchJobCertification : lstBatchJobCertification){
					if(iCertification!=0){
						jcRecords+=", ";
					}
					jcRecords+=batchJobCertification.getCertType();
					iCertification++;
				}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		if(lstBatchJobCertification==null || lstBatchJobCertification.size()==0)
			return null;
		else
			return jcRecords;
	}*/
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<Integer,BatchJobCertification> findCertificationTypeListByJob() 
	{
		Session session = getSession();
		List result = session.createCriteria(getPersistentClass())
		.list();

		Map<Integer,BatchJobCertification> batchJobCertificationMap = new HashMap<Integer, BatchJobCertification>();
		BatchJobCertification batchJobCertification = null;
		int i=0;
		for (Object object : result) {
			batchJobCertification=((BatchJobCertification)object);
			batchJobCertificationMap.put(new Integer(""+i),batchJobCertification);
			i++;
		}
		return batchJobCertificationMap;
	}
}
