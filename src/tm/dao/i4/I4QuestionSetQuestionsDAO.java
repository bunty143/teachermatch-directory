package tm.dao.i4;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4QuestionPool;
import tm.bean.i4.I4QuestionSetQuestions;
import tm.bean.i4.I4QuestionSets;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class I4QuestionSetQuestionsDAO extends GenericHibernateDAO<I4QuestionSetQuestions, Integer> 
{
	public I4QuestionSetQuestionsDAO() 
	{
		super(I4QuestionSetQuestions.class);
	}
	
	@Autowired
	private I4QuestionSetsDAO i4QuestionSetsDAO;
	
	@Transactional(readOnly=false)
	public List<I4QuestionSetQuestions> findByQuesSetId(I4QuestionSets i4QuestionSets,DistrictMaster districtMaster , HeadQuarterMaster headQuarterMaster) {
		List<I4QuestionSetQuestions> quesSetList = null;
		List<I4QuestionSets> questionSetList = new ArrayList<I4QuestionSets>();
		
		try 
		{
			Criterion criterion1 = null;
			
			if(districtMaster!=null)
				questionSetList = i4QuestionSetsDAO.findByDistrict(districtMaster);
			
			if(headQuarterMaster!=null)
				questionSetList = i4QuestionSetsDAO.findByHeadQuarter(headQuarterMaster);
			
			
			System.out.println("::::::: questionSetList ::::::: >>>>>>>> "+questionSetList.size());
			for(I4QuestionSets i4QS:questionSetList)
			{
				if(i4QS.equals(i4QuestionSets))
				{
					criterion1 = Restrictions.eq("questionSets",i4QuestionSets);
				}
			}
			
			/*Criterion criterion1 = Restrictions.in("questionSets",questionSetList);*/
			
			quesSetList = findByCriteria(Order.asc("QuestionSequence"),criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return quesSetList;
	}
	
	
	
	@Transactional(readOnly=false)
	public List<I4QuestionSetQuestions> findByQuesId(I4QuestionPool i4QuestionPool) {
		List<I4QuestionSetQuestions> quesSetList = null;
		List<I4QuestionSetQuestions> existQuestionList = new ArrayList<I4QuestionSetQuestions>();
		
		try 
		{
			Criterion criterion1 = Restrictions.eq("questionPool",i4QuestionPool);
			existQuestionList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return existQuestionList;
	}
	
}
