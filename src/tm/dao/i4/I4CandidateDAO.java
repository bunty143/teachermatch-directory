package tm.dao.i4;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.i4.I4Candidate;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class I4CandidateDAO extends GenericHibernateDAO<I4Candidate, Integer> 
{
	public I4CandidateDAO() 
	{
		super(I4Candidate.class);
	}
	
	
	@Transactional(readOnly=false)
	public List<I4Candidate> findByteacherDetail(TeacherDetail teacherDetail,DistrictMaster districtMaster) {
		List<I4Candidate> statusList = new ArrayList<I4Candidate>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			statusList = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	@Transactional(readOnly=false)
	public List<I4Candidate> findByI4Candidate(String i4candidate,DistrictMaster districtMaster) {
		List<I4Candidate> statusList = new ArrayList<I4Candidate>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("I4CandidateID",i4candidate);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			statusList = findByCriteria(criterion1,criterion2);			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
}
