package tm.dao.i4;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4DistrictAccount;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class I4DistrictAccountDAO extends GenericHibernateDAO<I4DistrictAccount, Integer> 
{
	public I4DistrictAccountDAO() 
	{
		super(I4DistrictAccount.class);
	}
	
	
	
	
	@Transactional(readOnly=false)
	public List<I4DistrictAccount> findByDistrict(DistrictMaster  districtMaster) {
		List<I4DistrictAccount> statusList = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			statusList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	
	
	
	
	/*@Transactional(readOnly=false)
	public List<I4QuestionSets> findi4QuesSetByStatus(String i4QuesStatus) {
		List<I4QuestionSets> statusList = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("Status",i4QuesStatus);
			statusList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	
	@Transactional(readOnly=false)
	public List<I4QuestionSets> findi4QuesSetByQues(String Questext) {
		List<I4QuestionSets> statusList = null;
		try 
		{
			Criterion criterion1 = Restrictions.like("QuestionSetText",Questext,MatchMode.ANYWHERE);
			statusList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	@Transactional(readOnly=false)
	public List<I4QuestionSets> findByDistrict(DistrictMaster districtMaster) {
		List<I4QuestionSets> listBydistrict = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			listBydistrict = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}*/
	
	
	@Transactional(readOnly=false)
	public List<I4DistrictAccount> findByHeadQuarter(HeadQuarterMaster  headQuarterMaster) {
		List<I4DistrictAccount> statusList = new ArrayList<I4DistrictAccount>();
		try 
		{	if(headQuarterMaster!=null){
			Criterion criterion1 = Restrictions.eq("headQuarterId", headQuarterMaster.getHeadQuarterId());
			statusList = findByCriteria(criterion1);	
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	
	
	
}
