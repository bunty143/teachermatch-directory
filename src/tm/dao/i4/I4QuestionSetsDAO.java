package tm.dao.i4;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4QuestionSets;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class I4QuestionSetsDAO extends GenericHibernateDAO<I4QuestionSets, Integer> 
{
	public I4QuestionSetsDAO() 
	{
		super(I4QuestionSets.class);
	}
	
	@Transactional(readOnly=false)
	public List<I4QuestionSets> findi4QuesSetByStatus(String i4QuesStatus) {
		List<I4QuestionSets> statusList = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("Status",i4QuesStatus);
			statusList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	
	@Transactional(readOnly=false)
	public List<I4QuestionSets> findi4QuesSetByQues(String Questext) {
		List<I4QuestionSets> statusList = null;
		try 
		{
			Criterion criterion1 = Restrictions.like("QuestionSetText",Questext,MatchMode.ANYWHERE);
			statusList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return statusList;
	}
	
	@Transactional(readOnly=false)
	public List<I4QuestionSets> findByDistrict(DistrictMaster districtMaster) {
		List<I4QuestionSets> listBydistrict = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			listBydistrict = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	@Transactional(readOnly=false)
	public List<I4QuestionSets> findExistQuestionSet(String quesTxt,DistrictMaster districtMaster) {
		List<I4QuestionSets> listBydistrict = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("QuestionSetText",quesTxt).ignoreCase();
			listBydistrict = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	@Transactional(readOnly=false)
	public List<I4QuestionSets> findQuestionSetByIdAndDist(Integer quesSetId,DistrictMaster districtMaster) {
		List<I4QuestionSets> listBydistrict = new ArrayList<I4QuestionSets>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("ID",quesSetId);
			listBydistrict = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	
	
	@Transactional(readOnly=false)
	public List<I4QuestionSets> findByHeadQuarter(HeadQuarterMaster headQuarterMaster) {
		List<I4QuestionSets> listBydistrict = null;
		try 
		{	if(headQuarterMaster!=null){
			Criterion criterion1 = Restrictions.eq("headQuarterId",headQuarterMaster.getHeadQuarterId());
			listBydistrict = findByCriteria(criterion1);
		}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listBydistrict;
	}
	
	/*
	 * @Deepak for Job Category
	 */
	
	@Transactional(readOnly=false)
	public Map<Integer,String> getQuestionSetName(List<Integer> ids){
		Map<Integer ,String> resultMap = new HashMap<Integer,String>();
		List<String[]> data =  new ArrayList<String[]>();
		try{
			Criteria criteria = getSession().createCriteria(this.getPersistentClass());
			criteria.add(Restrictions.in("ID", ids));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.property("ID"))
					.add(Projections.property("QuestionSetText"))
					);
			data = criteria.list();
			if(data.size()>0){
                for (Iterator it = data.iterator(); it.hasNext();) {
                Object[] row = (Object[]) it.next();    
                if(row[0]!=null)
                	resultMap.put(Integer.parseInt(row[0].toString()),row[1]!=null?row[1].toString():"");
             }               
           }
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return resultMap;
	}
}
