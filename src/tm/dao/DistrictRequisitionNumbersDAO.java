package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictRequisitionNumbersDAO extends GenericHibernateDAO<DistrictRequisitionNumbers, Integer> {

	DistrictRequisitionNumbersDAO()
	{
		super(DistrictRequisitionNumbers.class);
	}
	@Transactional(readOnly=false)
	public  List<DistrictRequisitionNumbers> finddistrictReqList(Order order,int startPos,int limit,Criterion...criterions)
	{
		List<DistrictRequisitionNumbers> districtRequisitionNumbers=null;
		
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for(Criterion c:criterions){
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			if(order != null)
				criteria.addOrder(order);
			
			districtRequisitionNumbers=criteria.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtRequisitionNumbers;
	}
	@Transactional(readOnly=false)
	public boolean checkDuplicateRequisitionNO(Integer districtRequisitionId,DistrictMaster districtMaster,String requisitionNumber ){
		boolean r=false;
		try{
			List result=null;
			Criterion crn=Restrictions.eq("districtMaster", districtMaster );
			if(districtRequisitionId!=null)
			{
				Criterion c1=Restrictions.eq("requisitionNumber", requisitionNumber);
				Criterion c2=Restrictions.not(Restrictions.in("districtRequisitionId",new Integer[]{districtRequisitionId}));
				result=findByCriteria(crn,c1,c2);
			}else
			{
				Criterion c1=Restrictions.eq("requisitionNumber", requisitionNumber); 
				result=findByCriteria(crn,c1);
			}
			if(result.size()>0)
				r=true;
			else
				r=false;
			
		}catch (Exception e) {
			e.printStackTrace();
			
		}
		return r;
	}
	
	/* Gagan : Get DistrictRequisitionNumbers list from reqIdList ========== */
	@Transactional(readOnly=false)
	public List<DistrictRequisitionNumbers> getDistrictRequisitionList(List reqIdList)
	{
		List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("districtRequisitionId",reqIdList);
			lstDistrictRequisitionNumbers = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictRequisitionNumbers;
	}
	@Transactional(readOnly=false)
	public List<DistrictRequisitionNumbers> getDistrictRequisitionList(List reqIdList,Boolean isUsed)
	{
		List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("districtRequisitionId",reqIdList);
			lstDistrictRequisitionNumbers = findByCriteria(criterion1,Restrictions.eq("isUsed", isUsed));		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictRequisitionNumbers;
	}
	
	/* Gagan : Get DistrictRequisitionNumbers list from reqIdList ========== */
	@Transactional(readOnly=false)
	public DistrictRequisitionNumbers getDistrictRequisitionNumber(DistrictMaster districtMaster,String requisitionNumber)
	{
		List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers= null;
		DistrictRequisitionNumbers drns=null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("requisitionNumber",requisitionNumber);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion3 = Restrictions.eq("isUsed",false);
			lstDistrictRequisitionNumbers = findByCriteria(criterion1,criterion2,criterion3);		
			if(lstDistrictRequisitionNumbers.size()>0){
				drns=lstDistrictRequisitionNumbers.get(0);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return drns;
	}

	/* Gourav :  ========== */
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public void emptyDeleteFlag(String updateParam,DistrictMaster districtMaster){
		try {
			String hql = "UPDATE DistrictRequisitionNumbers set deleteFlag = :deleteFlag where districtId = :districtMaster And uploadFrom = :uploadFrom";
			Query query = null;
			Session session = getSession();

			query = session.createQuery(hql);
			query.setParameter("deleteFlag", updateParam);
			query.setParameter("uploadFrom", "E");
			query.setParameter("districtMaster", districtMaster.getDistrictId());
			query.executeUpdate();
			//return;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public boolean updateDeleteFlag(ArrayList<Integer> deleteFlagUpdateStore){
		boolean r=false;
		try {
			
			String hql = "UPDATE DistrictRequisitionNumbers set deleteFlag = :deleteFlag WHERE districtRequisitionId IN (:districtRequisitionId)";
			Query query = null;
			Session session = getSession();

			query = session.createQuery(hql);
			query.setParameter("deleteFlag", "N");
			query.setParameterList("districtRequisitionId", deleteFlagUpdateStore);
			query.executeUpdate();
			r= true;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return r;
	}
	
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public void deleteFlagRecords(){
		try {
			String hql = "delete JobRequisitionNumbers where status = :status And districtRequisitionId in ( select districtRequisitionId from DistrictRequisitionNumbers where deleteFlag = :deleteFlag)";
			//String hql2 = "delete from DistrictRequisitionNumbers where deleteFlag = :deleteFlag";
			Query query = null;
			Session session = getSession();
			query = session.createQuery(hql);
			//query = session.createQuery(hql2);
			query.setParameter("deleteFlag", "D");
			query.setParameter("status", 0);
			query.executeUpdate();
			//return;
			deleteRequisitionRecords();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public void deleteRequisitionRecords(){
		try {
			String hql = "delete from DistrictRequisitionNumbers where deleteFlag = :deleteFlag And uploadFrom = :uploadFrom";
			Query query = null;
			Session session = getSession();
			query = session.createQuery(hql);
			//query = session.createQuery(hql2);
			query.setParameter("deleteFlag", "D");
			query.setParameter("uploadFrom", "E");
			query.executeUpdate();
			
			//return;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}
	
	/* Gagan : Get DistrictRequisitionNumbers list from reqIdList ========== */
	@Transactional(readOnly=false)
	public List<DistrictRequisitionNumbers> getDistrictRequisitionNumbers(DistrictMaster districtMaster,List<String> requisitionNumbers)
	{
		List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers= new ArrayList<DistrictRequisitionNumbers>();
		try 
		{
			if(requisitionNumbers.size()>0){
				Criterion criterion1 = Restrictions.in("requisitionNumber",requisitionNumbers);
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				lstDistrictRequisitionNumbers = findByCriteria(criterion1,criterion2);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictRequisitionNumbers;
	}
	
	
	@Transactional(readOnly=false)
	public List<DistrictRequisitionNumbers> getDistrictRequisitionNumbersBYReqNo(List<String> requisitionNumbers,DistrictMaster districtMaster)
	{
		List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers= new ArrayList<DistrictRequisitionNumbers>();
		try 
		{
			Criterion criterion1 = Restrictions.in("requisitionNumber",requisitionNumbers);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			lstDistrictRequisitionNumbers = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictRequisitionNumbers;
	}

	@Transactional(readOnly=false)
	public void deleteFromDistrictRequisition(List<Integer> districtRequisitions){
		if(districtRequisitions.size()>0)
		{
			
			try {
				String hql = "delete from DistrictRequisitionNumbers WHERE districtRequisitionId IN (:districtRequisitions)";
				Query query = null;
				Session session = getSession();
	
				query = session.createQuery(hql);
				query.setParameterList("districtRequisitions", districtRequisitions);
				System.out.println("  OOO  "+query.executeUpdate());
			} catch (HibernateException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Transactional(readOnly=false)
	public List<DistrictRequisitionNumbers> getDistrictRequisitionNumbers(DistrictMaster districtMaster,String requisitionNumber)
	{
		List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers= new ArrayList<DistrictRequisitionNumbers>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("requisitionNumber",requisitionNumber);
			if(districtMaster!=null){
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				lstDistrictRequisitionNumbers = findByCriteria(criterion1,criterion2);
			}else{
				lstDistrictRequisitionNumbers = findByCriteria(criterion1);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictRequisitionNumbers;
	}
	
	
	@Transactional(readOnly=false)
	public List<DistrictRequisitionNumbers> getRequisitionNumbersWithAndWithoutDistrict(List<String> requisitionNumbers,DistrictMaster districtMaster)
	{
		List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers= new ArrayList<DistrictRequisitionNumbers>();
		try 
		{
			Criterion criterion1 = Restrictions.in("requisitionNumber",requisitionNumbers);
			if(districtMaster!=null){
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				lstDistrictRequisitionNumbers = findByCriteria(criterion1,criterion2);
			}else{
				lstDistrictRequisitionNumbers = findByCriteria(criterion1);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictRequisitionNumbers;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictRequisitionNumbers> getRequisitionNumbersByDistricts(List<DistrictMaster> districtMasters)
	{
		List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers= new ArrayList<DistrictRequisitionNumbers>();
		try 
		{
			Criterion criterion1 = Restrictions.in("districtMaster",districtMasters);
			
			lstDistrictRequisitionNumbers = findByCriteria(criterion1);
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictRequisitionNumbers;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictRequisitionNumbers> getRequisitionNumbersBySearch(String Requisition,DistrictMaster districtMaster)
	{
		List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers= new ArrayList<DistrictRequisitionNumbers>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("requisitionNumber",Requisition);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			
			lstDistrictRequisitionNumbers = findByCriteria(criterion1,criterion2);
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictRequisitionNumbers;
	}
	
	@Transactional(readOnly=false)
	public List<SchoolMaster> getSchoolMasterBySearchSNameAndRNumber(String requisition,String schoolName)
	{
		List<SchoolMaster> lstSchoolMaster= new ArrayList<SchoolMaster>();
		try 
		{
			Session session=getSession();
			Criteria criteria=session.createCriteria(DistrictRequisitionNumbers.class)
			.createAlias("districtMaster", "dm")
			.createAlias("schoolMaster", "sm")
			.setProjection( Projections.projectionList()
	        .add( Projections.groupProperty("schoolMaster"), "schoolMaster" )
		    ).add(Restrictions.eq("status", "A"))
		    .add(Restrictions.eq("isUsed", false))
		    .add(Restrictions.eq("dm.districtId", 804800))
		    .addOrder(Order.asc("sm.schoolName"))
		    .setFirstResult(0).setMaxResults(25);
			if(requisition!=null && !requisition.trim().equalsIgnoreCase(""))
				criteria.add(Restrictions.eq("requisitionNumber", requisition));
			criteria.add(Restrictions.like("sm.schoolName", schoolName.trim(),MatchMode.ANYWHERE));
			lstSchoolMaster=criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolMaster;
	}
	
	@Transactional(readOnly=false)
	public List<SchoolMaster> getSchoolMasterByRNumber(String requisition)
	{
		List<SchoolMaster> lstSchoolMaster= new ArrayList<SchoolMaster>();
		try 
		{
			Session session=getSession();
			Criteria criteria=session.createCriteria(DistrictRequisitionNumbers.class)
			.createAlias("districtMaster", "dm")
			.createAlias("schoolMaster", "sm")
			.setProjection( Projections.projectionList()
	        .add( Projections.groupProperty("schoolMaster"), "schoolMaster" )
		    ).add(Restrictions.eq("status", "A"))
		    .add(Restrictions.eq("isUsed", false))
		    .add(Restrictions.eq("dm.districtId", 804800))
		    .addOrder(Order.asc("sm.schoolName"));
			criteria.add(Restrictions.eq("requisitionNumber", requisition));
			lstSchoolMaster=criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstSchoolMaster;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictRequisitionNumbers> getSchoolMasterBySchoolId(String schoolId)
	{
		List<DistrictRequisitionNumbers> lstDRN= new ArrayList<DistrictRequisitionNumbers>();
		try 
		{
			Session session=getSession();
			Criteria criteria=session.createCriteria(DistrictRequisitionNumbers.class)
			.createAlias("districtMaster", "dm")
			.createAlias("schoolMaster", "sm")
			.add(Restrictions.eq("status", "A"))
		    .add(Restrictions.eq("isUsed", false))
		    .add(Restrictions.eq("dm.districtId", 804800))
		    .addOrder(Order.asc("sm.schoolName"));
			criteria.add(Restrictions.eq("sm.schoolId", Long.parseLong(schoolId)));
			lstDRN=criteria.list();			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDRN;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictRequisitionNumbers> getDistrictRequisitionNumbersForNC(List<DistrictMaster> districtMaster,List<String> requisitionNumbers)
	{
		List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers= new ArrayList<DistrictRequisitionNumbers>();
		try 
		{
			if(requisitionNumbers.size()>0 && districtMaster.size()>0){
				Criterion criterion1 = Restrictions.in("requisitionNumber",requisitionNumbers);
				Criterion criterion2 = Restrictions.in("districtMaster",districtMaster);
				lstDistrictRequisitionNumbers = findByCriteria(criterion1,criterion2);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictRequisitionNumbers;
	}
	
	
}
