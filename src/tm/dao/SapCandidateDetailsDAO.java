package tm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SapCandidateDetails;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SapCandidateDetailsDAO extends GenericHibernateDAO<SapCandidateDetails, Integer> 
{
	public SapCandidateDetailsDAO() {
		super(SapCandidateDetails.class);
	}
	
	@Transactional(readOnly=true)
	public List<SapCandidateDetails> checkSAPdataByTeacherAndJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
	
		List <SapCandidateDetails> sapCandidateDetailsList = new ArrayList<SapCandidateDetails>();
		try 
		{	         
			 Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			 Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
			 sapCandidateDetailsList = findByCriteria(criterion1, criterion2);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sapCandidateDetailsList;
	}
	
	@Transactional(readOnly=true)
	public List<SapCandidateDetails> checkSAPdataByTeacherAndJobForTeacherList(List<JobForTeacher> jobForTeachers)
	{
		List <SapCandidateDetails> sapCandidateDetailsList = new ArrayList<SapCandidateDetails>();
		try 
		{	
			if(jobForTeachers.size()>0)
			{
				 Criterion criterion1 = Restrictions.in("jobForTeacher",jobForTeachers);
				 sapCandidateDetailsList = findByCriteria(criterion1);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sapCandidateDetailsList;
	}
	
	@Transactional(readOnly=true)
	public List<SapCandidateDetails> checkSAPTeachers(List<TeacherDetail> teacherDetails)
	{
		List <SapCandidateDetails> sapCandidateDetailsList = new ArrayList<SapCandidateDetails>();
		try 
		{	if(teacherDetails.size()>0)
			{
		
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				sapCandidateDetailsList = findByCriteria(criterion1);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sapCandidateDetailsList;
	}
	
	@Transactional(readOnly=true)
	public List<SapCandidateDetails> checkSAPdataByTeacherAndJobList(List<TeacherDetail> teacherDetail,List<JobOrder> jobOrder)
	{
		List <SapCandidateDetails> sapCandidateDetailsList = new ArrayList<SapCandidateDetails>();
		try 
		{	         
			 Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetail);
			 Criterion criterion2 = Restrictions.in("jobOrder",jobOrder);
			 sapCandidateDetailsList = findByCriteria(criterion1, criterion2);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sapCandidateDetailsList;
	}

	
	@Transactional(readOnly=true)
    public List<SapCandidateDetails> findAllrecordsByJObIds(DistrictMaster districtMaster)
    {
          List <SapCandidateDetails> sapCandidateDetailsList = new ArrayList<SapCandidateDetails>();
      

          List<Integer> listjobOrder=new ArrayList<Integer>();
          try 
          {
                Session session = getSession();
                Connection connection =null;
                String sql = "";
                sql = "SELECT sapcandidatedetails.sapStatus AS sapStatus, sapcandidatedetails.`jobForTeacherId` as jobForTeacherId,"+
                "jft.requisitionNumber as requisitionNumber,jft.jobId as jobId,jft.teacherId as teacherId FROM sapcandidatedetails "+ 
                      " LEFT JOIN joborder AS job ON job.jobId= sapcandidatedetails.jobId"+ 
                      " INNER JOIN jobforteacher AS jft ON jft.jobForTeacherId=sapcandidatedetails.`jobForTeacherId`"+
                      " WHERE job.districtId="+districtMaster.getDistrictId();
                
                SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) getSessionFactory();
                ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
                  connection = connectionProvider.getConnection();
                PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE); 
                 
                  ResultSet rs=ps.executeQuery();
                  rs.last();
                  int rowcount=rs.getRow();
                  rs.beforeFirst();
                  if(rs.next()){                
                        
                          do{
                           final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
                           SapCandidateDetails newSapCandidateDetails = new SapCandidateDetails();
                           JobForTeacher tempJft = new JobForTeacher();
                           tempJft.setJobForTeacherId(Long.parseLong(rs.getString(rs.findColumn("jobForTeacherId"))));
                           TeacherDetail tDetail = new TeacherDetail();
                                 tDetail.setTeacherId(Integer.parseInt(rs.getString(rs.findColumn("teacherId"))));
                           JobOrder tempJob = new JobOrder();
                           tempJft.setTeacherId(tDetail);
                           tempJob.setJobId(Integer.parseInt(rs.getString(rs.findColumn("jobId"))));
                           tempJft.setJobId(tempJob);
                           tempJft.setRequisitionNumber(rs.getString("requisitionNumber"));
                           newSapCandidateDetails.setJobForTeacher(tempJft);                                   
                           newSapCandidateDetails.setSapStatus(rs.getString("sapStatus"));
                           sapCandidateDetailsList.add(newSapCandidateDetails);
                          }while(rs.next());
                  }
                
                
                } 
          catch (Exception e) 
          {
                e.printStackTrace();
          }
          
          return sapCandidateDetailsList;
    }
	
	// Optimization for pnr dashBoard.
	
	@Transactional(readOnly=true)
	public Map<Integer,String> checkSAPTeachersOp(List<TeacherDetail> teacherDetails)
	{
		Map<Integer,String> sapCandidateDetailsMap= new HashMap<Integer,String>();
		try 
		{	
			if(teacherDetails.size()>0)
			{
		
				Criteria criteria = getSession().createCriteria(this.getPersistentClass());
				criteria.add(Restrictions.in("teacherDetail",teacherDetails)).createAlias("teacherDetail", "td");
				criteria.setProjection(Projections.projectionList()
						.add(Projections.property("td.teacherId"))
						.add(Projections.property("sapStatus")));
				
				List data= new ArrayList();
				data=criteria.list();
				for (Iterator it = data.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[0]!=null)
		            {
		            	sapCandidateDetailsMap.put((Integer) row[0], (String) (row[1]==null?"":row[1])); 
		            }
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sapCandidateDetailsMap;
	}
	@Transactional(readOnly=true)
	public List<SapCandidateDetails> getSapCandidateDetails(List<Integer> listTeacherDetail){
		List<SapCandidateDetails> sapCandidateDetailsList = new ArrayList<SapCandidateDetails>();
		Criteria criteria = getSession().createCriteria(this.getPersistentClass(),"sap").createAlias("jobOrder", "jo").createAlias("jo.districtMaster", "dm");
		criteria = criteria.createCriteria("teacherDetail", "teacherdetail");
		criteria.add(Restrictions.eq("dm.districtId", 806900));
		criteria.add(Restrictions.in("teacherdetail.teacherId", listTeacherDetail));
		criteria.add(Restrictions.eq("sap.sapStatus", "Q"));
		sapCandidateDetailsList = criteria.list();
		return sapCandidateDetailsList;
	}
	@Transactional(readOnly=false)
	public void updateSapCandidateStatus(List<Integer> listTeacherDetail,Integer districtId){
		Session session = getSession();
		String sql = "Update sapcandidatedetails sap set sap.sapStatus='S' where sap.jobId IN(select jobId from joborder jo where jo.districtId=:districtId) and teacherId IN(:teachersList) and sap.sapStatus='Q'";
		Query query = session.createSQLQuery(sql);
		System.out.println(sql);
		if(listTeacherDetail!=null && listTeacherDetail.size()>0){
			query.setParameterList("teachersList", listTeacherDetail);
			query.setParameter("districtId", districtId);
		}
		try{
		if(listTeacherDetail!=null && listTeacherDetail.size()>0){
			int status = query.executeUpdate();
			System.out.println("::::::::::status"+status);
		}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println("sql:::::::::::::::"+sql);
	}
	@Transactional(readOnly=true)
	public List<SapCandidateDetails> findSentSapCandidateDetails(Integer districtId){
		List<SapCandidateDetails> sapCandidateDetailsList = new ArrayList<SapCandidateDetails>();
		Criteria criteria = getSession().createCriteria(this.getPersistentClass(),"sap").createAlias("jobOrder", "jo").createAlias("jo.districtMaster", "dm");
		criteria.add(Restrictions.eq("dm.districtId", districtId));
		criteria.add(Restrictions.eq("sap.sapStatus", "S"));
		sapCandidateDetailsList = criteria.list();
		return sapCandidateDetailsList;
	}
	@Transactional(readOnly=true)
	public List<SapCandidateDetails> findPendingSapCandidateDetails(Integer districtId){
		List<SapCandidateDetails> sapCandidateDetailsList = new ArrayList<SapCandidateDetails>();
		Criteria criteria = getSession().createCriteria(this.getPersistentClass(),"sap").createAlias("jobOrder", "jo").createAlias("jo.districtMaster", "dm");
		criteria.add(Restrictions.eq("dm.districtId", districtId));
		criteria.add(Restrictions.eq("sap.sapStatus", "Q"));
		criteria.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("sap.teacherDetail"))
				.add(Projections.groupProperty("sap.jobOrder"))
				);
		sapCandidateDetailsList = criteria.list();
		return sapCandidateDetailsList;
	}
	@Transactional(readOnly=true)
	public List<SapCandidateDetails> checkSAPdataByTeacher(TeacherDetail teacherDetail)
	{
		List <SapCandidateDetails> sapCandidateDetailsList = new ArrayList<SapCandidateDetails>();
		try 
		{	         
			 Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			 
			 sapCandidateDetailsList = findByCriteria(criterion1);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sapCandidateDetailsList;
	}
	
}
