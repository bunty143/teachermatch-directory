package tm.dao.menu;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DistrictMaster;
import tm.bean.menu.MenuMaster;
import tm.bean.menu.MenuSectionMaster;
import tm.bean.menu.RoleAccessPermission;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.user.RoleMasterDAO;

public class RoleAccessPermissionDAO extends GenericHibernateDAO<RoleAccessPermission, Integer> {

	public RoleAccessPermissionDAO() {
		super(RoleAccessPermission.class);
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	private RoleMasterDAO roleMasterDAO;
	public void setRoleMasterDAO(RoleMasterDAO roleMasterDAO) {
		this.roleMasterDAO = roleMasterDAO;
	}
	
	@Autowired
	private MenuSectionMasterDAO menuSectionMasterDAO;
	public void setMenuSectionMasterDAO(
			MenuSectionMasterDAO menuSectionMasterDAO) {
		this.menuSectionMasterDAO = menuSectionMasterDAO;
	} 
	@Autowired
	private MenuMasterDAO menuMasterDAO;
	public void setMenuMasterDAO(MenuMasterDAO menuMasterDAO) {
		this.menuMasterDAO = menuMasterDAO;
	}
	/***  Get Menu list from MenuMaster ***/
	@Transactional(readOnly=true)
	public  List<MenuMaster>  getMenuList(HttpServletRequest request,UserMaster userMaster){
		int roleId=0;
		if(userMaster.getRoleId().getRoleId()!=null){
			roleId=userMaster.getRoleId().getRoleId();
		}
		DistrictMaster districtMaster=null;
		try{
			if(userMaster.getEntityType()!=1){
				districtMaster=userMaster.getDistrictId();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//System.out.println("roleId::="+roleId);
		
		List<MenuMaster> lstMenuMaster=new ArrayList<MenuMaster>();
		SortedMap map = new TreeMap();
		
		List<RoleAccessPermission> lstRoleAccessPermission = null;
			try{
				//RoleMaster roleMaster = roleMasterDAO.findById(roleId, false, false);
				RoleMaster roleMaster = new RoleMaster();
				roleMaster.setRoleId(roleId);
				Criterion criterion = Restrictions.eq("roleId",roleMaster);
				Criterion criterion2 = Restrictions.eq("status","A");
				lstRoleAccessPermission = findByCriteria(criterion,criterion2);
				for(int i=0; i<lstRoleAccessPermission.size();i++ ){
					RoleAccessPermission accessPermission = lstRoleAccessPermission.get(i);
					if(lstRoleAccessPermission.get(i).getMenuId().getStatus().equalsIgnoreCase("A"))
					{
						boolean districtFlag=false;
						boolean hidedistrictFlag=false;
						MenuMaster menuMaster=lstRoleAccessPermission.get(i).getMenuId();
						boolean menuDistrictFlag=false;
						if(menuMaster.getDistrictId()!=null && districtMaster!=null){
							menuDistrictFlag=true;
								if(menuMaster.getDistrictId().contains("|Hide#")){
								menuDistrictFlag=false;
								if(menuMaster.getDistrictId().contains("|Hide#"+districtMaster.getDistrictId()+"|")){
									hidedistrictFlag=true;
								}
							}
							if(menuMaster.getDistrictId().contains("|"+districtMaster.getDistrictId()+"|")){
								districtFlag=true;
								menuDistrictFlag=true;
							}
						}
						//System.out.println(districtMaster+" -----||------ "+districtFlag);
						if((districtFlag)||(menuDistrictFlag==false && districtFlag==false)){
							if(!hidedistrictFlag)
							if(lstRoleAccessPermission.get(i).getMenuId().getParentMenuId()!=null){
								map.put(lstRoleAccessPermission.get(i).getMenuId().getParentMenuId().getOrderBy()+"||"+lstRoleAccessPermission.get(i).getMenuId().getOrderBy()+"||"+lstRoleAccessPermission.get(i).getMenuId().getParentMenuId().getMenuId(),lstRoleAccessPermission.get(i).getMenuId());
							}else{
								map.put(lstRoleAccessPermission.get(i).getMenuId().getOrderBy().toString(),lstRoleAccessPermission.get(i).getMenuId());
							}
						}
					}
				}
				Iterator iterator = map.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					lstMenuMaster.add((MenuMaster) map.get(key));
					//System.out.println("key==:"+key);
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		return lstMenuMaster;
	}
	/***  Get All Menu list from MenuMaster ***/
	@Transactional(readOnly=true)
	public  List<MenuMaster>  getAllMenuList(HttpServletRequest request,UserMaster userMaster){
		int roleId=0;
		if(userMaster.getRoleId().getRoleId()!=null){
			roleId=userMaster.getRoleId().getRoleId();
		}
		
		//System.out.println("roleId::="+roleId);
		
		List<MenuMaster> lstMenuMaster=new ArrayList<MenuMaster>();
		SortedMap map = new TreeMap();
		
		List<RoleAccessPermission> lstRoleAccessPermission = null;
			try{
				//RoleMaster roleMaster = roleMasterDAO.findById(roleId, false, false);
				RoleMaster roleMaster = new RoleMaster();
				roleMaster.setRoleId(roleId);
				
				Criterion criterion = Restrictions.eq("roleId",roleMaster);
				Criterion criterion2 = Restrictions.eq("status","A");
				lstRoleAccessPermission = findByCriteria(criterion,criterion2);
				for(int i=0; i<lstRoleAccessPermission.size();i++ ){
					RoleAccessPermission accessPermission = lstRoleAccessPermission.get(i);
						if(lstRoleAccessPermission.get(i).getMenuId().getParentMenuId()!=null){
							map.put(lstRoleAccessPermission.get(i).getMenuId().getParentMenuId().getOrderBy()+"||"+lstRoleAccessPermission.get(i).getMenuId().getOrderBy()+"||"+lstRoleAccessPermission.get(i).getMenuId().getParentMenuId().getMenuId(),lstRoleAccessPermission.get(i).getMenuId());
						}else{
							map.put(lstRoleAccessPermission.get(i).getMenuId().getOrderBy().toString(),lstRoleAccessPermission.get(i).getMenuId());
						}
				}
				Iterator iterator = map.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					lstMenuMaster.add((MenuMaster) map.get(key));
					//System.out.println("key==:"+key);
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		return lstMenuMaster;
	}
	
	/***  Get Menu list ( Teacher ) from MenuMaster ***/
	@Transactional(readOnly=true)
	public  List<MenuMaster>  getMenuListForTeacher(HttpServletRequest request){
		int roleId=0;
		//System.out.println("roleId::="+roleId);
		List<MenuMaster> lstMenuMaster=new ArrayList<MenuMaster>();
		
		SortedMap map = new TreeMap();
		
		List<RoleAccessPermission> lstRoleAccessPermission = null;
			try{
				//RoleMaster roleMaster = roleMasterDAO.findById(roleId, false, false);
				RoleMaster roleMaster = new RoleMaster();
				roleMaster.setRoleId(roleId);
				Criterion criterion = Restrictions.eq("roleId",roleMaster);
				Criterion criterion2 = Restrictions.eq("status","A");
				//System.out.println("Menu List:::::::::::::::::::::::::::::::::::::::::::::");
				lstRoleAccessPermission = findByCriteria(criterion,criterion2);
				for(int i=0; i<lstRoleAccessPermission.size();i++ ){
					RoleAccessPermission accessPermission = lstRoleAccessPermission.get(i);
					if(lstRoleAccessPermission.get(i).getMenuId().getStatus().equalsIgnoreCase("A")){
						if(lstRoleAccessPermission.get(i).getMenuId().getParentMenuId()!=null){
							map.put(lstRoleAccessPermission.get(i).getMenuId().getParentMenuId().getOrderBy()+"||"+lstRoleAccessPermission.get(i).getMenuId().getOrderBy()+"||"+lstRoleAccessPermission.get(i).getMenuId().getParentMenuId().getMenuId(),lstRoleAccessPermission.get(i).getMenuId());
						}else{
							map.put(lstRoleAccessPermission.get(i).getMenuId().getOrderBy().toString(),lstRoleAccessPermission.get(i).getMenuId());
						}
					}
				}
				Iterator iterator = map.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					lstMenuMaster.add((MenuMaster) map.get(key));
					////System.out.println("key==:"+key);
				}
			} 
			catch (Exception e){
				e.printStackTrace();
			}
		return lstMenuMaster;
	}
	 
	@Transactional()
	public String  getMenuOptionList(int roleId,int menuSectionId){
		String menuOptionMasterIds="";
		List<RoleAccessPermission> lstRoleAccessPermission = null;
			try{
				//System.out.println("roleId="+roleId);
				//System.out.println("menuSectionId=="+menuSectionId);
				//MenuSectionMaster menuSectionMaster=   menuSectionMasterDAO.findById(menuSectionId, false, false);
				MenuSectionMaster menuSectionMaster=   new MenuSectionMaster();
				menuSectionMaster.setMenuSectionId(menuSectionId);
				//RoleMaster roleMaster = roleMasterDAO.findById(roleId, false, false);
				RoleMaster roleMaster = new RoleMaster();
				roleMaster.setRoleId(roleId);
				
				Criterion criterion = Restrictions.eq("menuSectionId",menuSectionMaster);
				Criterion criterion1 = Restrictions.eq("roleId",roleMaster);
				Criterion completeCondition = Restrictions.conjunction().add(criterion).add(criterion1);
				
				lstRoleAccessPermission =findByCriteria(completeCondition); 
				if(lstRoleAccessPermission.size()>0){
					RoleAccessPermission accessPermission = lstRoleAccessPermission.get(0);
					menuOptionMasterIds= accessPermission.getOptionid();
					//System.out.println("menuOptionMasterIds=="+menuOptionMasterIds);
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		return menuOptionMasterIds;
	}
	@Transactional(readOnly=true)
	public String  getMenuOptionList(int roleId,int menuSectionId,String pageName,int orderBy){
		String menuOptionMasterIds="";
		List<RoleAccessPermission> lstRoleAccessPermission = null;
			try{
				Criterion criterion1 = Restrictions.eq("pageName",pageName);
				Criterion criterion2 = Restrictions.eq("orderBy",orderBy);
			    List<MenuMaster> lstMenuMaster = null;
			    if(orderBy==0 || orderBy==100){
			    	lstMenuMaster =menuMasterDAO.findByCriteria(criterion1);
			    }else{
			    	lstMenuMaster =menuMasterDAO.findByCriteria(criterion1,criterion2);
			    }
				//System.out.println("menuMaster>=="+lstMenuMaster.get(0).getMenuId());
				
				//RoleMaster roleMaster = roleMasterDAO.findById(roleId, false, false);
			    RoleMaster roleMaster = new RoleMaster();
			    roleMaster.setRoleId(roleId);
				Criterion criterion3 = Restrictions.eq("roleId",roleMaster);

				//MenuMaster menuMaster = menuMasterDAO.findById(lstMenuMaster.get(0).getMenuId(), false, false);
				MenuMaster menuMaster = lstMenuMaster.get(0);
				Criterion criterion4 = Restrictions.eq("menuId",menuMaster);
				
				//MenuSectionMaster menuSectionMaster=   menuSectionMasterDAO.findById(menuSectionId, false, false);
				MenuSectionMaster menuSectionMaster=  new MenuSectionMaster();
				menuSectionMaster.setMenuSectionId(menuSectionId);
				Criterion criterion5 = Restrictions.eq("menuSectionId",menuSectionMaster);
				
				lstRoleAccessPermission =findByCriteria(criterion3,criterion4,criterion5); 
				if(lstRoleAccessPermission.size()>0){
					RoleAccessPermission accessPermission = lstRoleAccessPermission.get(0);
					menuOptionMasterIds= accessPermission.getOptionid();
					//System.out.println("menuOptionMasterIds=="+menuOptionMasterIds);
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		return menuOptionMasterIds;
	}
	
	@Transactional(readOnly=true)
	public String  getMenuOptionListByMenuId(int roleId,int menuSectionId,String pageName,int orderBy,int menuId){
		String menuOptionMasterIds="";
		List<RoleAccessPermission> lstRoleAccessPermission = null;
			try{
				Criterion criterion1 = Restrictions.eq("pageName",pageName);
				Criterion criterion2 = Restrictions.eq("orderBy",orderBy);
			    List<MenuMaster> lstMenuMaster = null;
			    if(orderBy==0 || orderBy==100){
			    	lstMenuMaster =menuMasterDAO.findByCriteria(criterion1);
			    }else{
			    	lstMenuMaster =menuMasterDAO.findByCriteria(criterion1,criterion2);
			    }
			    if(lstMenuMaster.size()>0)
				System.out.println("menuMaster>=="+lstMenuMaster.get(0).getMenuId());
				
				//RoleMaster roleMaster = roleMasterDAO.findById(roleId, false, false);
				RoleMaster roleMaster = new RoleMaster();
				roleMaster.setRoleId(roleId);
				Criterion criterion3 = Restrictions.eq("roleId",roleMaster);
				MenuMaster menuMaster = null;
				if(menuId!=0){
					//menuMaster = menuMasterDAO.findById(menuId, false, false);
					menuMaster = new MenuMaster();
					menuMaster.setMenuId(menuId);
				}else{
					//menuMaster = menuMasterDAO.findById(lstMenuMaster.get(0).getMenuId(), false, false);
					menuMaster = lstMenuMaster.get(0);
				}
				Criterion criterion4 = Restrictions.eq("menuId",menuMaster);
				
				//MenuSectionMaster menuSectionMaster=   menuSectionMasterDAO.findById(menuSectionId, false, false);
				MenuSectionMaster menuSectionMaster=  new MenuSectionMaster();
				menuSectionMaster.setMenuSectionId(menuSectionId);
				Criterion criterion5 = Restrictions.eq("menuSectionId",menuSectionMaster);
				
				lstRoleAccessPermission =findByCriteria(criterion3,criterion4,criterion5); 
				if(lstRoleAccessPermission.size()>0){
					RoleAccessPermission accessPermission = lstRoleAccessPermission.get(0);
					menuOptionMasterIds= accessPermission.getOptionid();
					//System.out.println("menuOptionMasterIds=="+menuOptionMasterIds);
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		return menuOptionMasterIds;
	}
	
	/***  Get Menu list ( Teacher ) from MenuMaster ***/
	@Transactional(readOnly=true)
	public  List<MenuMaster>  getMenuReportList(HttpServletRequest request,int menuId,UserMaster userMaster){
		int roleId=0;
		if(userMaster.getRoleId().getRoleId()!=null){
			roleId=userMaster.getRoleId().getRoleId();
		}
		DistrictMaster districtMaster=null;
		try{
			if(userMaster.getEntityType()!=1){
				districtMaster=userMaster.getDistrictId();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("getMenuReportList::::"+menuId);
		List<MenuMaster> lstMenuMaster=new ArrayList<MenuMaster>();
		
		SortedMap map = new TreeMap();
		
		List<RoleAccessPermission> lstRoleAccessPermission = null;
			try{
				//RoleMaster roleMaster = roleMasterDAO.findById(roleId, false, false);
				RoleMaster roleMaster = new RoleMaster();
				roleMaster.setRoleId(roleId);
				//MenuMaster menuMaster = menuMasterDAO.findById(menuId, false, false);
				Criterion criterion = Restrictions.eq("roleId",roleMaster);
				Criterion criterion2 = Restrictions.eq("status","A");
				lstRoleAccessPermission = findByCriteria(criterion,criterion2);
				System.out.println("Menu List:::::::::::::::::::::::::::::::::::::::::::::"+lstRoleAccessPermission.size());
				for(int i=0; i<lstRoleAccessPermission.size();i++ ){
					
					boolean districtFlag=false;
					boolean hidedistrictFlag=false;
					MenuMaster menuMaster1=lstRoleAccessPermission.get(i).getMenuId();
					boolean menuDistrictFlag=false;
					if(menuMaster1.getDistrictId()!=null && districtMaster!=null){
						menuDistrictFlag=true;
							if(menuMaster1.getDistrictId().contains("|Hide#")){
							menuDistrictFlag=false;
							if(menuMaster1.getDistrictId().contains("|Hide#"+districtMaster.getDistrictId()+"|")){
								hidedistrictFlag=true;
							}
						}
						if(menuMaster1.getDistrictId().contains("|"+districtMaster.getDistrictId()+"|")){
							districtFlag=true;
							menuDistrictFlag=true;
						}
					}
					if((districtFlag)||(menuDistrictFlag==false && districtFlag==false)){
						if(!hidedistrictFlag)
						if(lstRoleAccessPermission.get(i).getMenuId().getSubMenuId()!=null && lstRoleAccessPermission.get(i).getMenuId().getSubMenuId().getMenuId()==menuId){
							map.put(lstRoleAccessPermission.get(i).getMenuId().getSubMenuId().getOrderBy()+"||"+lstRoleAccessPermission.get(i).getMenuId().getOrderBy()+"||"+lstRoleAccessPermission.get(i).getMenuId().getSubMenuId().getMenuId(),lstRoleAccessPermission.get(i).getMenuId());
						}
					 }
				}
				Iterator iterator = map.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					lstMenuMaster.add((MenuMaster) map.get(key));
					//System.out.println("key==:"+key);
				}
			} 
			catch (Exception e){
				e.printStackTrace();
			}
		return lstMenuMaster;
	}
}
