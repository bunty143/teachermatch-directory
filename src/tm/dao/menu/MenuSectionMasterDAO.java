package tm.dao.menu;

import tm.bean.menu.AccessOptionMaster;
import tm.bean.menu.MenuMaster;
import tm.bean.menu.MenuSectionMaster;
import tm.dao.generic.GenericHibernateDAO;

public class MenuSectionMasterDAO extends GenericHibernateDAO<MenuSectionMaster, Integer> {
	
	public MenuSectionMasterDAO() 
	{
		super(MenuSectionMaster.class);
	}

}
