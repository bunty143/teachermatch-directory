package tm.dao.menu;

import tm.bean.menu.AccessOptionMaster;
import tm.dao.generic.GenericHibernateDAO;

public class AccessOptionMasterDAO extends GenericHibernateDAO<AccessOptionMaster, Integer> {
	
	public AccessOptionMasterDAO() 
	{
		super(AccessOptionMaster.class);
	}
}
