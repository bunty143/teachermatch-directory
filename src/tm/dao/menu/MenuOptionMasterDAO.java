package tm.dao.menu;

import tm.bean.menu.MenuOptionMaster;
import tm.dao.generic.GenericHibernateDAO;

public class MenuOptionMasterDAO extends GenericHibernateDAO<MenuOptionMaster, Integer> {

	public MenuOptionMasterDAO() {
		super(MenuOptionMaster.class);
		// TODO Auto-generated constructor stub
	}
	
}
