package tm.dao.menu;

import tm.bean.menu.MenuMaster;
import tm.dao.generic.GenericHibernateDAO;

public class MenuMasterDAO extends GenericHibernateDAO<MenuMaster, Integer> {
	
	public MenuMasterDAO() 
	{
		super(MenuMaster.class);
	}

}
