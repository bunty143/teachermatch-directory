package tm.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.TeacherLoginHistory;
import tm.bean.TeacherNotes;
import tm.bean.TeacherProfileVisitHistory;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherLoginHistoryDAO extends GenericHibernateDAO<TeacherLoginHistory, Integer>{

	public TeacherLoginHistoryDAO() {
		super(TeacherLoginHistory.class);
	}
	@Transactional(readOnly=true)
	public List<TeacherLoginHistory> findByTeacherLoginHistory(String sessionId)
	{
	         List <TeacherLoginHistory> lstTeacherLoginHistory = null;
	         Criterion criterion = Restrictions.eq("sessionId",sessionId);
	         lstTeacherLoginHistory = findByCriteria(criterion);
			 return lstTeacherLoginHistory;
	}
	@Transactional(readOnly=false)
	public List<TeacherLoginHistory> findTeachers(Set<TeacherDetail> teacherDetailList)
	{
		List<TeacherLoginHistory> lstTLH =new ArrayList<TeacherLoginHistory>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetailList);	
			criteria.add(criterion1);
			criteria.addOrder(Order.asc("loginTime"));
			lstTLH = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTLH;
	}
}
