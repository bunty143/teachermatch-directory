package tm.dao;

import tm.bean.JsiCommunicationLog;
import tm.dao.generic.GenericHibernateDAO;

public class JsiCommunicationLogDAO extends GenericHibernateDAO<JsiCommunicationLog, Integer> 
{
	public JsiCommunicationLogDAO() {
		super(JsiCommunicationLog.class);
	}
}
