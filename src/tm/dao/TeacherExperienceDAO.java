package tm.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.master.TFAAffiliateMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherExperienceDAO extends GenericHibernateDAO<TeacherExperience, Integer> 
{
	public TeacherExperienceDAO() 
	{
		super(TeacherExperience.class);
	}

	@Transactional(readOnly=false)
	public TeacherExperience findTeacherExperienceByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherExperience> lstTeacherExperience = new ArrayList<TeacherExperience>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstTeacherExperience = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherExperience==null || lstTeacherExperience.size()==0)
			return null;
		else
			return lstTeacherExperience.get(0);
	}

	@Transactional(readOnly=false)
	public List<TeacherExperience> findExperienceForTeachers(List<TeacherDetail> teacherDetails)
	{
		List<TeacherExperience> teacherExperienceList  = new ArrayList<TeacherExperience>();
		try 
		{
			if(teacherDetails.size()>0)
			{
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				teacherExperienceList = findByCriteria(criterion1);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		

		return teacherExperienceList;
	}

	@Transactional(readOnly=false)
	public Map<Integer, Boolean> findActiveResume(List<TeacherDetail> teacherDetails)
	{
		List<TeacherExperience> teacherExperienceList = new ArrayList<TeacherExperience>();
		Map<Integer,Boolean> mapResume = new TreeMap<Integer, Boolean>();
		try{
			if(teacherDetails.size()>0)
			{
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				teacherExperienceList = findByCriteria(criterion1);


				String resume = null;
				for(TeacherExperience teacherExperience: teacherExperienceList ){													        			        
					resume = teacherExperience.getResume();
					if(resume==null || resume.trim().equals("")){
						mapResume.put(teacherExperience.getTeacherId().getTeacherId(), false);
					}
					else{
						mapResume.put(teacherExperience.getTeacherId().getTeacherId(), true);
					}
				}
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}		

		return mapResume;
	}

	@Transactional(readOnly=false)
	public Map<Integer, Boolean> findActiveExpStatus(List<TeacherDetail> teacherDetails)
	{
		List<TeacherExperience> lstTeacherExp=new ArrayList<TeacherExperience>();
		Map<Integer,Boolean> mapTeacherCertificate= new TreeMap<Integer, Boolean>();
		try 
		{
			if(teacherDetails.size()>0)
			{
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				lstTeacherExp = findByCriteria(criterion1);		
				for(TeacherExperience tExp : lstTeacherExp){
					if(tExp.getExpCertTeacherTraining()>0)
						mapTeacherCertificate.put(tExp.getTeacherId().getTeacherId(), true);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		return mapTeacherCertificate;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeachersByExperience(String YOTE,String YOTESelectVal)
	{
		List<TeacherDetail> teacherList= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 =null;
		
			if(YOTESelectVal.equals("1")){
				criterion1=Restrictions.eq("expCertTeacherTraining",Double.parseDouble(YOTE));
			}else if(YOTESelectVal.equals("2")){
				criterion1=Restrictions.lt("expCertTeacherTraining",Double.parseDouble(YOTE));
			}else if(YOTESelectVal.equals("3")){
				criterion1=Restrictions.le("expCertTeacherTraining",Double.parseDouble(YOTE));
			}else if(YOTESelectVal.equals("4")){
				criterion1=Restrictions.gt("expCertTeacherTraining",Double.parseDouble(YOTE));
			}else if(YOTESelectVal.equals("5")){
				criterion1=Restrictions.ge("expCertTeacherTraining",Double.parseDouble(YOTE));
			}
			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			teacherList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeachersBySubstitute(String canServeAsSubTeacherYes,String canServeAsSubTeacherNo,String canServeAsSubTeacherDA)
	{
		/*
		 * canServeAsSubTeacherYes 1
		   canServeAsSubTeacherNo 0
           canServeAsSubTeacherDA 2
		 */

		List<TeacherDetail> teacherList= new ArrayList<TeacherDetail>();
		try 
		{
			ArrayList<Integer> subtList = new ArrayList<Integer>();
			if(canServeAsSubTeacherYes.equalsIgnoreCase("true"))
				subtList.add(1);
			if(canServeAsSubTeacherNo.equalsIgnoreCase("true"))
				subtList.add(0);
			if(canServeAsSubTeacherDA.equalsIgnoreCase("true"))
				subtList.add(2);

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 =null;
			criterion1 = Restrictions.in("canServeAsSubTeacher", subtList);
			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			teacherList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeachersByTFA(String TFAA,String TFAC,String TFAN)
	{
		/*
			TFAA 1
			TFAC 2 
			TFAN 3
		 */

		List<TeacherDetail> teacherList= new ArrayList<TeacherDetail>();
		try 
		{
			TFAAffiliateMaster tfaAffiliateMaster = null; 
			ArrayList<TFAAffiliateMaster> subtList = new ArrayList<TFAAffiliateMaster>();
			if(TFAA.equalsIgnoreCase("true"))
			{
				tfaAffiliateMaster = new TFAAffiliateMaster(); 
				tfaAffiliateMaster.setTfaAffiliateId(1);
				subtList.add(tfaAffiliateMaster);
			}
			if(TFAC.equalsIgnoreCase("true"))
			{
				tfaAffiliateMaster = new TFAAffiliateMaster(); 
				tfaAffiliateMaster.setTfaAffiliateId(2);
				subtList.add(tfaAffiliateMaster);
			}
			if(TFAN.equalsIgnoreCase("true"))
			{
				tfaAffiliateMaster = new TFAAffiliateMaster(); 
				tfaAffiliateMaster.setTfaAffiliateId(3);
				subtList.add(tfaAffiliateMaster);
			}

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 =null;
			criterion1 = Restrictions.in("tfaAffiliateMaster", subtList);
			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			teacherList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeachersByResume(Boolean isResume)
	{
		List<TeacherDetail> teacherList= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 =null;
			if(isResume)
				criterion1 = Restrictions.and(Restrictions.isNotNull("resume"),Restrictions.ne("resume", ""));
			else
				criterion1 = Restrictions.or(Restrictions.isNull("resume"),Restrictions.eq("resume", ""));

			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			teacherList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	
	
	
	@Transactional(readOnly=false)
	public List<TeacherExperience> findByTeacherDetailsObj(List<TeacherDetail> teacherDetailsObj)
	{
		List<TeacherExperience> lstTeacherExperience = new ArrayList<TeacherExperience>();
		
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetailsObj);
			lstTeacherExperience = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherExperience==null || lstTeacherExperience.size()==0)
			return null;
		else
			return lstTeacherExperience;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherExperience> findExperienceForTeachers(List<TeacherDetail> teacherDetails, Double teacherExperience)
	{
		List<TeacherExperience> teacherExperienceList  = new ArrayList<TeacherExperience>();
		try 
		{
			if(teacherDetails.size()>0)
			{
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion2 = Restrictions.eq("expCertTeacherTraining",teacherExperience);
				teacherExperienceList = findByCriteria(criterion1,criterion2);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherExperienceList;
	}
	
	//Optimization and replacement of findTeachersByResume
	
	@Transactional(readOnly=false)
	public List<Integer> findTeachersByResume_Op(Boolean isResume, List<Integer> teachersId)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 =null;
				if(isResume)
					criterion1 = Restrictions.and(Restrictions.isNotNull("resume"),Restrictions.ne("resume", ""));
				else
					criterion1 = Restrictions.or(Restrictions.isNull("resume"),Restrictions.eq("resume", ""));
	
				criteria.add(criterion1);
				criteria.add(Restrictions.in("teacherDetail.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}
	
	//Optimization and replacement of findTeachersByExperience
	@Transactional(readOnly=false)
	public List<Integer> findTeachersByExperience_Op(String YOTE,String YOTESelectVal, List<Integer> teachersId)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
				try 
				{
					Session session = getSession();
					Criteria criteria = session.createCriteria(getPersistentClass());
					Criterion criterion1 =null;
				
					if(YOTESelectVal.equals("1")){
						criterion1=Restrictions.eq("expCertTeacherTraining",Double.parseDouble(YOTE));
					}else if(YOTESelectVal.equals("2")){
						criterion1=Restrictions.lt("expCertTeacherTraining",Double.parseDouble(YOTE));
					}else if(YOTESelectVal.equals("3")){
						criterion1=Restrictions.le("expCertTeacherTraining",Double.parseDouble(YOTE));
					}else if(YOTESelectVal.equals("4")){
						criterion1=Restrictions.gt("expCertTeacherTraining",Double.parseDouble(YOTE));
					}else if(YOTESelectVal.equals("5")){
						criterion1=Restrictions.ge("expCertTeacherTraining",Double.parseDouble(YOTE));
					}
					criteria.add(criterion1);
					criteria.add(Restrictions.in("teacherDetail.teacherId", teachersId));
					criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
					criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
					teacherList =  criteria.list();
				} 
				catch (Exception e) {
					e.printStackTrace();
				}		
		return teacherList;
	}

	//Optimizatiom for cadidate pool search
	
	@Transactional(readOnly=false)
	public List<Integer> findTeachersBySubstitute_Op(String canServeAsSubTeacherYes,String canServeAsSubTeacherNo,String canServeAsSubTeacherDA, List<Integer> teachersId)
	{
		/*
		 * canServeAsSubTeacherYes 1
		   canServeAsSubTeacherNo 0
           canServeAsSubTeacherDA 2
		 */

		List<Integer> teacherList= new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
			try 
			{
				ArrayList<Integer> subtList = new ArrayList<Integer>();
				if(canServeAsSubTeacherYes.equalsIgnoreCase("true"))
					subtList.add(1);
				if(canServeAsSubTeacherNo.equalsIgnoreCase("true"))
					subtList.add(0);
				if(canServeAsSubTeacherDA.equalsIgnoreCase("true"))
					subtList.add(2);
	
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 =null;
				criterion1 = Restrictions.in("canServeAsSubTeacher", subtList);
				criteria.add(criterion1);
				criteria.add(Restrictions.in("teacherDetail.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}
	
	
	//Optimizatiom for cadidate pool search
	
	@Transactional(readOnly=false)
	public List<Integer> findTeachersByTFA_Op(String TFAA,String TFAC,String TFAN, List<Integer> teachersId)
	{
		/*
			TFAA 1
			TFAC 2 
			TFAN 3
		 */
		List<Integer> teacherList= new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
			try 
			{
				TFAAffiliateMaster tfaAffiliateMaster = null; 
				ArrayList<TFAAffiliateMaster> subtList = new ArrayList<TFAAffiliateMaster>();
				if(TFAA.equalsIgnoreCase("true"))
				{
					tfaAffiliateMaster = new TFAAffiliateMaster(); 
					tfaAffiliateMaster.setTfaAffiliateId(1);
					subtList.add(tfaAffiliateMaster);
				}
				if(TFAC.equalsIgnoreCase("true"))
				{
					tfaAffiliateMaster = new TFAAffiliateMaster(); 
					tfaAffiliateMaster.setTfaAffiliateId(2);
					subtList.add(tfaAffiliateMaster);
				}
				if(TFAN.equalsIgnoreCase("true"))
				{
					tfaAffiliateMaster = new TFAAffiliateMaster(); 
					tfaAffiliateMaster.setTfaAffiliateId(3);
					subtList.add(tfaAffiliateMaster);
				}
	
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 =null;
				criterion1 = Restrictions.in("tfaAffiliateMaster", subtList);
				criteria.add(criterion1);
				criteria.add(Restrictions.in("teacherDetail.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}

	@Transactional(readOnly=false)
	public List<Integer> findTeachersBySubstitute_Op(String canServeAsSubTeacherYes,String canServeAsSubTeacherNo,String canServeAsSubTeacherDA)
	{
		/*
		 * canServeAsSubTeacherYes 1
		   canServeAsSubTeacherNo 0
           canServeAsSubTeacherDA 2
		 */

		List<Integer> teacherList= new ArrayList<Integer>();
		
			try 
			{
				ArrayList<Integer> subtList = new ArrayList<Integer>();
				if(canServeAsSubTeacherYes.equalsIgnoreCase("true"))
					subtList.add(1);
				if(canServeAsSubTeacherNo.equalsIgnoreCase("true"))
					subtList.add(0);
				if(canServeAsSubTeacherDA.equalsIgnoreCase("true"))
					subtList.add(2);
	
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 =null;
				criterion1 = Restrictions.in("canServeAsSubTeacher", subtList);
				criteria.add(criterion1);
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}	
	
	@Transactional(readOnly=false)
	public List<Integer> findTeachersByTFA_Op(String TFAA,String TFAC,String TFAN)
	{
		/*
			TFAA 1
			TFAC 2 
			TFAN 3
		 */
		List<Integer> teacherList= new ArrayList<Integer>();
		
			try 
			{
				TFAAffiliateMaster tfaAffiliateMaster = null; 
				ArrayList<TFAAffiliateMaster> subtList = new ArrayList<TFAAffiliateMaster>();
				if(TFAA.equalsIgnoreCase("true"))
				{
					tfaAffiliateMaster = new TFAAffiliateMaster(); 
					tfaAffiliateMaster.setTfaAffiliateId(1);
					subtList.add(tfaAffiliateMaster);
				}
				if(TFAC.equalsIgnoreCase("true"))
				{
					tfaAffiliateMaster = new TFAAffiliateMaster(); 
					tfaAffiliateMaster.setTfaAffiliateId(2);
					subtList.add(tfaAffiliateMaster);
				}
				if(TFAN.equalsIgnoreCase("true"))
				{
					tfaAffiliateMaster = new TFAAffiliateMaster(); 
					tfaAffiliateMaster.setTfaAffiliateId(3);
					subtList.add(tfaAffiliateMaster);
				}
	
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 =null;
				criterion1 = Restrictions.in("tfaAffiliateMaster", subtList);
				criteria.add(criterion1);
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> findTeachersByResume_Op(Boolean isResume)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
		
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 =null;
				if(isResume)
					criterion1 = Restrictions.and(Restrictions.isNotNull("resume"),Restrictions.ne("resume", ""));
				else
					criterion1 = Restrictions.or(Restrictions.isNull("resume"),Restrictions.eq("resume", ""));
	
				criteria.add(criterion1);
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}
}



