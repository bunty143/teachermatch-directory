package tm.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.TeacherEPIChangedStatusLog;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherEPIChagedStatusLogDAO extends GenericHibernateDAO<TeacherEPIChangedStatusLog, Integer> 
{

	public TeacherEPIChagedStatusLogDAO() {
		super(TeacherEPIChangedStatusLog.class);
	}
	
	@Transactional(readOnly=false)
	public List<TeacherEPIChangedStatusLog> epiByTeachers(TeacherDetail teacherDetail)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		List<TeacherEPIChangedStatusLog> teacherEPIChangedStatusLogList = new ArrayList<TeacherEPIChangedStatusLog>();
		List list=new ArrayList();
		list.add(1);
		list.add(2);
		try {			
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);	
			teacherEPIChangedStatusLogList = findByCriteria(criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherEPIChangedStatusLogList;
	}
	
}
