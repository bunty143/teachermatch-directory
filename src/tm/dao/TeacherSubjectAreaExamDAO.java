package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherAdditionalDocuments;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherSubjectAreaExam;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherSubjectAreaExamDAO extends GenericHibernateDAO<TeacherSubjectAreaExam, Integer>{
	public TeacherSubjectAreaExamDAO() {
		super(TeacherSubjectAreaExam.class);
	}

	
	@Transactional(readOnly=false)
	public TeacherSubjectAreaExam findTeacherSubjectAreaExam(TeacherDetail teacherDetail)
	{
		List<TeacherSubjectAreaExam> lstExam= new ArrayList<TeacherSubjectAreaExam>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstExam = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstExam==null || lstExam.size()==0)
			return null;
		else
			return lstExam.get(0);
	}
	
	@Transactional(readOnly=false)
	public List<TeacherSubjectAreaExam> getTeacherSubjectAreaExamsByTeacher(Order  sortOrderStrVal,TeacherDetail teacherDetail)
	{
		List<TeacherSubjectAreaExam> lstTeacherSubjectAreaExams= new ArrayList<TeacherSubjectAreaExam>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstTeacherSubjectAreaExams = findByCriteria(sortOrderStrVal,criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherSubjectAreaExams;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherSubjectAreaExam> findTeacherSubjectAreaExamLst(TeacherDetail teacherDetail)
	{
		List<TeacherSubjectAreaExam> lstExam= new ArrayList<TeacherSubjectAreaExam>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstExam = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		

		return lstExam;
	}
	
	
}
