package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.TeacherHonor;
import tm.bean.TeacherInvolvement;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherHonorDAO extends GenericHibernateDAO<TeacherHonor, Integer> 
{
	public TeacherHonorDAO() 
	{
		super(TeacherHonor.class);
	}
	@Transactional(readOnly=false)
	public List<TeacherHonor> findTeacherHonersByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherHonor> lstTeacherInvolvement = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			lstTeacherInvolvement = findByCriteria(Order.desc("honorId"), criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherInvolvement;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherHonor> findByTeacherDetailsObj(List<TeacherDetail> teacherDetailsObj)
	{
		List<TeacherHonor> lstTeHonors = new ArrayList<TeacherHonor>();
		
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetailsObj);
			lstTeHonors = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeHonors==null || lstTeHonors.size()==0)
			return null;
		else
			return lstTeHonors;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> getTeacherHonorCountByTeacherJAF(TeacherDetail teacherDetail)
	{
		List<Integer> lstTeacherHonor= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			
			criteria.setProjection(Projections.projectionList().add(Projections.count("teacherDetail"))) ;
			lstTeacherHonor =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstTeacherHonor;
		}
		
	}
	
}
