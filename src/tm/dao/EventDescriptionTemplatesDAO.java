package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictTemplatesforMessages;
import tm.bean.EventDescriptionTemplates;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class EventDescriptionTemplatesDAO extends GenericHibernateDAO<EventDescriptionTemplates, Integer>{

	public EventDescriptionTemplatesDAO() {
		super(EventDescriptionTemplates.class);
	}
	
	@Transactional(readOnly=false)
	public List<EventDescriptionTemplates> findByDistrict(Order sortOrderStrVal,DistrictMaster districtMaster)
	{
		List<EventDescriptionTemplates> eventDescriptionTemplatesList = new ArrayList<EventDescriptionTemplates>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion c1 = Restrictions.eq("districtMaster",districtMaster);
			criteria.addOrder(sortOrderStrVal);
			criteria.add(c1);
			eventDescriptionTemplatesList = criteria.list();
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return eventDescriptionTemplatesList;
	}
}
