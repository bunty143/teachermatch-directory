package tm.dao;

import java.util.Calendar;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobUploadTemp;
import tm.dao.generic.GenericHibernateDAO;

public class JobUploadTempDAO extends GenericHibernateDAO<JobUploadTemp,Integer> 
{
	public JobUploadTempDAO() {
		super(JobUploadTemp.class);
	}
	
	
	@Transactional(readOnly=false)
	public boolean deleteJobTemp(String sessionId){
		try{
			Criterion criterion = Restrictions.eq("sessionid",sessionId);
        	List<JobUploadTemp> jobUploadTemplst	=findByCriteria(criterion);
        	for(JobUploadTemp jobUploadTemp: jobUploadTemplst){
        		makeTransient(jobUploadTemp);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	@Transactional(readOnly=false)
	public boolean deleteAllJobTemp(String sessionId)
	{
		try{
			Calendar c = Calendar.getInstance(); 
			c.add(Calendar.DAY_OF_YEAR, -2);  
			Date date = c.getTime();
			Criterion criterion1=Restrictions.lt("date",date);
			Criterion criterion2 = Restrictions.eq("sessionid",sessionId);
			Criterion complete3 = Restrictions.or(criterion1,criterion2);
        	List<JobUploadTemp> jobUploadTemplst	=findByCriteria(complete3);
			for(JobUploadTemp jobUploadTemp: jobUploadTemplst){
        		makeTransient(jobUploadTemp);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return true;
	}

	@Transactional(readOnly=true)
	public List<JobUploadTemp> findByTempJob(Order order,int startPos,int limit,Criterion... criterion)
	{
		List<JobUploadTemp> lstJobUploadTemp = null;
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			criteria.addOrder(order);
			lstJobUploadTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobUploadTemp;
	}
	@Transactional(readOnly=true)
	public List<JobUploadTemp> findByAllTempJob()
	{
		List<JobUploadTemp> lstJobUploadTemp = null;
		try{
			Session session = getSession();
			Calendar c = Calendar.getInstance(); // starts with today's date and time
			c.add(Calendar.DAY_OF_YEAR, -2);  // advances day by 2
			Date date = c.getTime();
			Criterion criterion=Restrictions.lt("date",date);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion);
			lstJobUploadTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobUploadTemp;
	}
	
	@Transactional(readOnly=true)
	public int getRowCountTempJob(Criterion... criterion)
	{
		List<JobUploadTemp> lstJobUploadTemp = null;
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			lstJobUploadTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobUploadTemp.size();
	}
}
