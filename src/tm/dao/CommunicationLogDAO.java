package tm.dao;

import static tm.services.district.GlobalServices.println;

import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.CommunicationLog;
import tm.bean.DistrictApprovalGroups;
import tm.bean.DistrictSpecificApprovalFlow;
import tm.bean.DistrictWiseApprovalGroup;
import tm.bean.EventDetails;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.UserEmailNotificationsDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.CommonService;
import tm.services.EmailerService;
import tm.services.MailSendToTeacher;
import tm.services.ReferenceCheckAjax;
import tm.services.district.GlobalServices;
import tm.services.district.PrintOnConsole;
import tm.services.es.ElasticSearchService;
import tm.services.report.MailSendVVIThread;
import tm.services.teacher.AcceptOrDeclineMailHtml;
import tm.servlet.WorkThreadServlet;
import tm.utility.ElasticSearchConfig;
import tm.utility.IPAddressUtility;
import tm.utility.TestTool;
import tm.utility.Utility;

public class CommunicationLogDAO extends GenericHibernateDAO<CommunicationLog, Long> 
{
	public CommunicationLogDAO() 
	{
		super(CommunicationLog.class);
	}
	
	
	//save  CommunicationLog using Teacherid , modulename  , subject , message
	
	 @Transactional(readOnly=false)
		public void saveCommunicationLogData(UserMaster usermaster,TeacherDetail teacherDetail,String modulename, String subject , String message)
		{
			CommunicationLog communicationLog=new CommunicationLog();
		
			try 
			{
					System.out.println("");
			}
			catch (Exception e) {
				e.printStackTrace();
			}		
			
		}
	
	
}

  
