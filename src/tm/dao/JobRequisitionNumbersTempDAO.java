package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.Jobrequisitionnumberstemp;
import tm.bean.TeacherDetail;
import tm.bean.master.SchoolMaster;
import tm.dao.generic.GenericHibernateDAO;

public class JobRequisitionNumbersTempDAO extends GenericHibernateDAO<Jobrequisitionnumberstemp, Integer> {

	JobRequisitionNumbersTempDAO()
	{
		super(Jobrequisitionnumberstemp.class);
	}
	
	
	@Transactional(readOnly=false)
	public List<Jobrequisitionnumberstemp> findJobRequisitionNumbersByJobWithSchool(JobOrder jobOrder,SchoolMaster schoolMaster)
	{
		List<Jobrequisitionnumberstemp> list=new ArrayList<Jobrequisitionnumberstemp>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion3 = Restrictions.eq("status",new Integer(0));
			if(schoolMaster!=null)
			{
				Criterion criterion2 = Restrictions.eq("schoolMaster",schoolMaster);
				if(jobOrder.getIsExpHireNotEqualToReqNo()!=null && jobOrder.getIsExpHireNotEqualToReqNo().equals(new Boolean(true)))
					list = findByCriteria(criterion1,criterion2);
				else
					list = findByCriteria(criterion1,criterion2,criterion3);
				
			}
			else
			{
				if(jobOrder.getIsExpHireNotEqualToReqNo()!=null && jobOrder.getIsExpHireNotEqualToReqNo().equals(new Boolean(true)))
					list = findByCriteria(criterion1);
				else
					list = findByCriteria(criterion1,criterion3);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	@Transactional(readOnly=false)
	public Jobrequisitionnumberstemp findJobReqNumbersObj(DistrictRequisitionNumbers districtRequisitionNumbers)
	{
		List<Jobrequisitionnumberstemp> list=new ArrayList<Jobrequisitionnumberstemp>();
		Jobrequisitionnumberstemp jrn=null;
		try{
			
			if(districtRequisitionNumbers!=null){
				Criterion criterion1 = Restrictions.eq("districtRequisitionNumbers",districtRequisitionNumbers);
				list = findByCriteria(criterion1);
				if(list.size()>0){
					jrn=list.get(0);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return jrn;
	}	
	@Transactional(readOnly=false)
	public Jobrequisitionnumberstemp findBySchoolMasterAndTempId(SchoolMaster schoolMaster,String tempId)
	{
		List<Jobrequisitionnumberstemp> list=new ArrayList<Jobrequisitionnumberstemp>();
		Jobrequisitionnumberstemp jobrequisitionnumberstemp=null;
		try{
			
				Criterion criterion1 = Restrictions.eq("schoolMaster",schoolMaster);
				Criterion criterion2 = Restrictions.eq("tempId",tempId);
				list = findByCriteria(criterion1,criterion2);
			
		}catch (Exception e) {
			e.printStackTrace();
		}	
		if(list.size()!=0)
			jobrequisitionnumberstemp=list.get(0);
		
		return jobrequisitionnumberstemp;
	}
	@Transactional(readOnly=false)
	public Jobrequisitionnumberstemp findByTempId(String findByTempId)
	{
		List<Jobrequisitionnumberstemp> list=new ArrayList<Jobrequisitionnumberstemp>();
		Jobrequisitionnumberstemp jobrequisitionnumberstemp=null;
		try{
				Criterion criterion2 = Restrictions.eq("tempId",findByTempId);
				list = findByCriteria(criterion2);
			
		}catch (Exception e) {
			e.printStackTrace();
		}	
		if(list.size()!=0)
			jobrequisitionnumberstemp=list.get(0);
		
		return jobrequisitionnumberstemp;
	}
	@Transactional(readOnly=false)
	public List<Jobrequisitionnumberstemp> findByTempIdList(String findByTempId)
	{
		List<Jobrequisitionnumberstemp> list=new ArrayList<Jobrequisitionnumberstemp>();	
		try{
				Criterion criterion2 = Restrictions.eq("tempId",findByTempId);
				list = findByCriteria(criterion2);
			
		}catch (Exception e) {
			e.printStackTrace();
		}	
		
		
		return list;
	}
	@Transactional(readOnly=false)
	public List<Jobrequisitionnumberstemp> findByDRID(DistrictRequisitionNumbers districtRequisitionNumbers)
	{
		List<Jobrequisitionnumberstemp> list=new ArrayList<Jobrequisitionnumberstemp>();	
		try{
				Criterion criterion2 = Restrictions.eq("districtRequisitionNumbers",districtRequisitionNumbers);
				list = findByCriteria(criterion2);
		}catch (Exception e) {
			e.printStackTrace();
		}	
		
		
		return list;
	}
}
