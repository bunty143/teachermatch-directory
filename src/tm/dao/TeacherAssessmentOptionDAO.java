package tm.dao;

import tm.bean.TeacherAssessmentOption;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherAssessmentOptionDAO extends GenericHibernateDAO<TeacherAssessmentOption, Long> 
{
	public TeacherAssessmentOptionDAO() {
		super(TeacherAssessmentOption.class);
	}
}
