package tm.dao;

import tm.bean.StatusSpecificEmailTemplates;
import tm.dao.generic.GenericHibernateDAO;

public class StatusSpecificEmailTemplatesDAO extends GenericHibernateDAO<StatusSpecificEmailTemplates, Integer> {
	StatusSpecificEmailTemplatesDAO()
	{
		super(StatusSpecificEmailTemplates.class);
	}

}
