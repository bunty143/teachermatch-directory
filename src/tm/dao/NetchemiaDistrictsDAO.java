package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.NetchemiaDistricts;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class NetchemiaDistrictsDAO extends GenericHibernateDAO<NetchemiaDistricts, Integer> 
{
	public NetchemiaDistrictsDAO() {
		super(NetchemiaDistricts.class);
	}
	
	@Transactional(readOnly=true)
	public NetchemiaDistricts findByDistrictId(DistrictMaster districtMaster,boolean epiFlag)
	{
	         List <NetchemiaDistricts> lstNetchemiaDistricts = new ArrayList<NetchemiaDistricts>();
			 Criterion criterion = Restrictions.eq("districtMaster",districtMaster);
			 Criterion criterion2 = Restrictions.eq("status","a");
			 Criterion criterionepiOnly = Restrictions.eq("epiOnly",epiFlag);
			 
			 lstNetchemiaDistricts = findByCriteria(criterion,criterion2,criterionepiOnly);
			 if(lstNetchemiaDistricts.size()>0)
				 return lstNetchemiaDistricts.get(0);
			 else
				 return null;
	}
}
