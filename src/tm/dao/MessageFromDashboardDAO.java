package tm.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DashboardEmailsToTeacher;
import tm.bean.MessageFromDashboard;
import tm.bean.TeacherDetail;
import tm.dao.generic.GenericHibernateDAO;

public class MessageFromDashboardDAO extends GenericHibernateDAO<MessageFromDashboard, Integer>  
{
	public MessageFromDashboardDAO() {
		super(MessageFromDashboard.class);
	}
	
	@Transactional(readOnly=true)
	public List<MessageFromDashboard> getMailFormates(String[] status)
	{
		 List<MessageFromDashboard>  messageFromDashboards =null;
		 
	     try {
	    	 
				Criterion criterion2 = Restrictions.in("teacherStatus",status);
				messageFromDashboards = findByCriteria(criterion2);
				
				return messageFromDashboards;
				
	     }catch (Exception e) {
	    	 e.printStackTrace();
	     }
		return null;
	}
}
