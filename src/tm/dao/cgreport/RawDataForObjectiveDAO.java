package tm.dao.cgreport;

import tm.bean.cgreport.RawDataForObjective;
import tm.dao.generic.GenericHibernateDAO;

/**
 * @author Amit Chaudhary
 * @version 1.0, 26/11/2015
 */
public class RawDataForObjectiveDAO extends GenericHibernateDAO<RawDataForObjective, Integer> {

	public RawDataForObjectiveDAO() {
		super(RawDataForObjective.class);
	}
	
}
