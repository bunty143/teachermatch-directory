package tm.dao.cgreport;

import tm.bean.cgreport.TeacherStatusNotesHistory;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherStatusNotesHistoryDAO extends GenericHibernateDAO<TeacherStatusNotesHistory, Integer>{
	public TeacherStatusNotesHistoryDAO(){
		super(TeacherStatusNotesHistory.class);
	}
	
	}
