package tm.dao.cgreport;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.cgreport.PercentileCalculationCompetency;
import tm.bean.master.CompetencyMaster;
import tm.dao.generic.GenericHibernateDAO;

public class PercentileCalculationCompetencyDAO extends GenericHibernateDAO<PercentileCalculationCompetency, Integer>{
	public PercentileCalculationCompetencyDAO() {
		super(PercentileCalculationCompetency.class);
	}
	
	@Transactional(readOnly=false)
	public List<PercentileCalculationCompetency> findPercentileCalculationByScore(List<Integer> lstScore, CompetencyMaster competencyMaster)
	{
		List<PercentileCalculationCompetency> lstPercentileCalculationCompetency = null;
		try {
			Criterion criterion1 = Restrictions.in("score", lstScore);
			Criterion criterion2 = Restrictions.eq("competencyMaster", competencyMaster);
			Criterion criterionStatus = Restrictions.eq("status", "A");
			lstPercentileCalculationCompetency = findByCriteria(criterion1, criterion2, criterionStatus);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstPercentileCalculationCompetency;
	}
	
	@Transactional(readOnly=false)
	public List<PercentileCalculationCompetency> findPercentileCalculationByCompetency(CompetencyMaster competencyMaster)
	{
		List<PercentileCalculationCompetency> lstPercentileCalculationCompetency= null;
		try	{
			Criterion criterion1 = Restrictions.eq("competencyMaster",competencyMaster);
			Criterion criterionStatus = Restrictions.eq("status", "A");
			lstPercentileCalculationCompetency = findByCriteria(criterion1, criterionStatus);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstPercentileCalculationCompetency;
	}
	@Transactional(readOnly=false)
	public List<PercentileCalculationCompetency> findPercentileCalculation()
	{
		List<PercentileCalculationCompetency> lstPercentileCalculationCompetency= null;
		try	{
			Criterion criterionStatus = Restrictions.eq("status", "A");
			lstPercentileCalculationCompetency = findByCriteria(Order.asc("tValue"),criterionStatus);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstPercentileCalculationCompetency;
	}
	@Transactional(readOnly=false)
	public List<PercentileCalculationCompetency> findAllInCurrenctYear()
	{
		List<PercentileCalculationCompetency> lstPercentileCalculationCompetency= null;
		try	{
			Criterion criterionStatus = Restrictions.eq("status", "A");
			lstPercentileCalculationCompetency = findByCriteria(criterionStatus);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstPercentileCalculationCompetency;
	}
}
