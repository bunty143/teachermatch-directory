package tm.dao.cgreport;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.cgreport.TeacherStatusScores;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherStatusScoresDAO extends GenericHibernateDAO<TeacherStatusScores, Integer>{
	public TeacherStatusScoresDAO(){
		super(TeacherStatusScores.class);
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusScores> getStatusScoreList(TeacherDetail teacherDetail,JobOrder jobOrder,Order order,int startPos,int limit)
	{
		List<TeacherStatusScores> lstTeacherStatusScores = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			if(order != null){
				criteria.addOrder(order);
			}
			
			lstTeacherStatusScores = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusScores;
	}
	@Transactional(readOnly=true)
	public List<TeacherStatusScores> getStatusScoreAllList(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherStatusScores> lstTeacherStatusScores = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			lstTeacherStatusScores = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusScores;
	}
	@Transactional(readOnly=true)
	public List<TeacherStatusScores> getStatusScore(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster)
	{
		List<TeacherStatusScores> lstTeacherStatusScores = new ArrayList<TeacherStatusScores>();
		try 
		{
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			}else{
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			}
			
			Criterion criterion3 = Restrictions.eq("userMaster", userMaster);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterionStatus);
			lstTeacherStatusScores = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusScores;
	}
	@Transactional(readOnly=true)
	public List<TeacherStatusScores> getStatusScoreAll(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		List<TeacherStatusScores> lstTeacherStatusScores = null;
		try 
		{
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			}else{
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			}
			
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterionStatus);
			lstTeacherStatusScores = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusScores;
	}
	@Transactional(readOnly=true)
	public List<TeacherStatusScores> getFinalizeStatusScore(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		List<TeacherStatusScores> lstTeacherStatusScores = null;
		try 
		{
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			}else{
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			}
			
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion4 = Restrictions.eq("finalizeStatus", true);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion4);
			criteria.add(criterionStatus);
			lstTeacherStatusScores = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusScores;
	}
	@Transactional(readOnly=true)
	public List<TeacherStatusScores> getAllFinalizeStatusScore(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherStatusScores> lstTeacherStatusScores = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion4 = Restrictions.eq("finalizeStatus", true);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion4);
			lstTeacherStatusScores = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusScores;
	}
	
	@Transactional(readOnly=true)
	public Double[] getAllFinalizeStatusScoreAvg(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		Double[] arrMix=new Double[2];
		Double scoreP=0.0;
		Double maxScore=0.0;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.isNull("statusMaster");
			Criterion criterion4 = Restrictions.eq("finalizeStatus", true);
			Criterion criterion5 = Restrictions.isNull("secondaryStatus");
			Session session = getSession();
			List result = session.createCriteria(getPersistentClass()) 
			.add(criterion1) 
			.add(criterion2) 
			.add(criterion5) 
			.add(criterion4) 
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("statusMaster"))
					.add(Projections.avg("scoreProvided"))
					.add(Projections.avg("maxScore")) 
			).list();
			Iterator itr = result.iterator();
			while(itr.hasNext()){
				Object[] obj = (Object[]) itr.next();
				scoreP+=Double.parseDouble(obj[1]+"");
				maxScore+=Double.parseDouble(obj[2]+"");
			}
			result = session.createCriteria(getPersistentClass()) 
			.add(criterion1) 
			.add(criterion2) 
			.add(criterion4) 
			.add(criterion3) 
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("secondaryStatus"))
					.add(Projections.avg("scoreProvided"))
					.add(Projections.avg("maxScore")) 
			).list();
			itr = result.iterator();
			while(itr.hasNext()){
				Object[] obj = (Object[]) itr.next();
				scoreP+=Double.parseDouble(obj[1]+"");
				maxScore+=Double.parseDouble(obj[2]+"");
			}
			arrMix[0]=scoreP;
			arrMix[1]=maxScore;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return arrMix;
	}

	
	@Transactional(readOnly=true)
	public List<TeacherStatusScores> getStatusScoreByJobList(TeacherDetail teacherDetail,List<JobOrder> lstJobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster)
	{
		List<TeacherStatusScores> lstTeacherStatusScores = new ArrayList<TeacherStatusScores>();
		try 
		{
			Criterion criterionStatus=null;
			if(statusMaster!=null)
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			else
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.in("jobOrder", lstJobOrder);
			Criterion criterion3 = Restrictions.eq("userMaster", userMaster);
			lstTeacherStatusScores = findByCriteria(criterion1,criterion2,criterion3,criterionStatus);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusScores;
	}

	
	/*@Transactional(readOnly=true)
	public List<TeacherStatusScores> getTeacherStatusScores_msu(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherStatusScores> lstTeacherStatusScores = new ArrayList<TeacherStatusScores>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("finalizeStatus", true);
			lstTeacherStatusScores = findByCriteria(criterion1,criterion2,criterion3);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return lstTeacherStatusScores;
	}*/
	@Transactional(readOnly=true)
	public String[] getAllFinalizeStatusScoreBreakAvg(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		String[] arrMix=new String[1];
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.isNull("statusMaster");
			Criterion criterion4 = Restrictions.eq("finalizeStatus", true);
			Criterion criterion5 = Restrictions.isNull("secondaryStatus");
			Session session = getSession();
			List result1 = session.createCriteria(getPersistentClass()) 
			.add(criterion1) 
			.add(criterion2) 
			.add(criterion5) 
			.add(criterion4) 
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("statusMaster"))
					.add(Projections.avg("scoreProvided"))
					.add(Projections.avg("maxScore")) 
			).list();
			
			
			
			List result2 = session.createCriteria(getPersistentClass()) 
			.add(criterion1) 
			.add(criterion2) 
			.add(criterion4) 
			.add(criterion3) 
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("secondaryStatus"))
					.add(Projections.avg("scoreProvided"))
					.add(Projections.avg("maxScore")) 
			).list();
			
			arrMix[0]+="<table>";
			Iterator itr = result1.iterator();
			while(itr.hasNext()){
				Object[] obj = (Object[]) itr.next();
				StatusMaster statusMaster=(StatusMaster)obj[0];				
				Double val1= (Double) obj[1];
				Double val2= (Double) obj[2];
				arrMix[0]+="<tr><td>"+statusMaster.getStatus()+": "+Math.round(val1)+"/"+Math.round(val2)+"</td></tr>";
			}
						
			itr = result2.iterator();
			while(itr.hasNext()){
				Object[] obj = (Object[]) itr.next();
				Double val1= (Double) obj[1];
				Double val2= (Double) obj[2];
				SecondaryStatus secondaryStatus=(SecondaryStatus)obj[0];				
				arrMix[0]+="<tr><td>"+secondaryStatus.getSecondaryStatusName()+": "+Math.round(val1)+"/"+Math.round(val2)+"</td></tr>";
				
				/*arrMix[1]=obj[1]+"";
				arrMix[2]=obj[2]+"";
				arrMix[3]="SS";*/
			}
			arrMix[0]+="</table>";
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return arrMix;
	}
	
	@Transactional(readOnly=true)
	public List getAllFinalizeStatusScoreBreakAvgList(List<TeacherDetail> teacherDetail,JobOrder jobOrder)
	{
		List resultList =new ArrayList();
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.isNull("statusMaster");
			Criterion criterion4 = Restrictions.eq("finalizeStatus", true);
			Criterion criterion5 = Restrictions.isNull("secondaryStatus");
			Session session = getSession();
			List result1  = session.createCriteria(getPersistentClass()) 
			.add(criterion1) 
			.add(criterion2) 
			.add(criterion5) 
			.add(criterion4) 
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("statusMaster"))
					.add(Projections.groupProperty("teacherDetail"))
					.add(Projections.avg("scoreProvided"))
					.add(Projections.avg("maxScore")) 
			).list();
			
			resultList.add(result1);
			
			List result2 = session.createCriteria(getPersistentClass()) 
			.add(criterion1) 
			.add(criterion2) 
			.add(criterion4) 
			.add(criterion3) 
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("secondaryStatus"))
					.add(Projections.groupProperty("teacherDetail"))
					.add(Projections.avg("scoreProvided"))
					.add(Projections.avg("maxScore")) 
			).list();
			
			resultList.add(result2);
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return resultList;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusScores> getFinalizeTeacherStatusScoreCGMass(List<TeacherDetail> lstTeacherDetails,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster)
	{
		List<TeacherStatusScores> lstTeacherStatusScores = new ArrayList<TeacherStatusScores>();
		try 
		{
			Criterion criterionStatus=null;
			if(statusMaster!=null)
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			else
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			
			Criterion criterion1 = Restrictions.in("teacherDetail", lstTeacherDetails);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("userMaster", userMaster);
			Criterion criterion4 = Restrictions.eq("finalizeStatus", true);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			criteria.add(criterionStatus);
			lstTeacherStatusScores = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstTeacherStatusScores;
		}
	}
	
	
	@Transactional(readOnly=true)
	public List<TeacherStatusScores> getStatusScore_CGMass(List<TeacherDetail> lstTeacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster)
	{
		List<TeacherStatusScores> lstTeacherStatusScores = null;
		try 
		{
			Criterion criterionStatus=null;
			if(statusMaster!=null)
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			else
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			
			Criterion criterion3 = Restrictions.eq("userMaster", userMaster);
			Criterion criterion1 = Restrictions.in("teacherDetail", lstTeacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterionStatus);
			lstTeacherStatusScores = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusScores;
	}
	@Transactional(readOnly=false)	
	public List<TeacherStatusScores> findJobByTeachersWithDistrict(TeacherDetail teacherDetail,JobOrder jobOrder) 
	{	
		List<TeacherStatusScores> teacherList = new ArrayList<TeacherStatusScores>();
		
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			criteria.addOrder(Order.desc("createdDateTime"));
			if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null)
				criteria.createCriteria("jobOrder").addOrder(Order.desc("createdDateTime")).add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			else
				criteria.createCriteria("jobOrder").addOrder(Order.desc("createdDateTime")).add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			teacherList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	
	@Transactional(readOnly=false)	
	public List<TeacherStatusScores> getTSSDelta(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster,List<JobOrder> lstJobOrder) 
	{	
		List<TeacherStatusScores> teacherList = new ArrayList<TeacherStatusScores>();
		if(teacherDetails!=null && teacherDetails.size()>0)
		{
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				criteria.add(criterion1);
				criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster",districtMaster));
				if(lstJobOrder!=null && lstJobOrder.size()>0)
				{
					Criterion criterion6 = Restrictions.in("jobOrder",lstJobOrder);
					criteria.add(criterion6);
				}
				teacherList = criteria.list();
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
				
		return teacherList;
	}
	
	@Transactional(readOnly=true)
	public List getAllFinalizeStatusScoreAvgDelta(List<TeacherDetail> lstTeacherDetail,List<JobOrder> lstJobOrder)
	{
		List result=null;
		if(lstTeacherDetail!=null && lstTeacherDetail.size()>0)
		{
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherDetail", lstTeacherDetail);
			Criterion criterion2 = Restrictions.in("jobOrder", lstJobOrder);
			Criterion criterion3 = Restrictions.eq("finalizeStatus", true);
			Session session = getSession();
			result = session.createCriteria(getPersistentClass()) 
			.add(criterion1) 
			.add(criterion2) 
			.add(criterion3) 
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("jobOrder"))
					.add(Projections.groupProperty("teacherDetail"))
					.add(Projections.avg("scoreProvided"))
					.add(Projections.avg("maxScore")) 
			).list();
			
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
	}
		return result;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusScores> getFinalizeStatusScoreOp(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		List<TeacherStatusScores> lstTeacherStatusScores = null;
		try 
		{
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			}else{
				criterionStatus = Restrictions.eq("secondaryStatus", secondaryStatus);
			}
			
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion4 = Restrictions.eq("finalizeStatus", true);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion4);
			criteria.add(criterionStatus).createAlias("userMaster", "usermaster").setProjection(
					Projections.projectionList()
					.add(Projections.property("usermaster.userId"))
					.add(Projections.property("teacherStatusScoreId"))
					);
			List<Object[]> ts = new ArrayList<Object[]>();
			ts = criteria.list();
			if(ts.size()>0){
				lstTeacherStatusScores = new ArrayList<TeacherStatusScores>();
			for(Iterator it = ts.iterator(); it.hasNext();){
				
				Object[] row = (Object[]) it.next(); 
				if(row[0]!=null){
					TeacherStatusScores teacherStSc = new TeacherStatusScores();
					UserMaster umm = new UserMaster();
					umm.setUserId(Integer.parseInt(row[0].toString()));
					teacherStSc.setUserMaster(umm);
					lstTeacherStatusScores.add(teacherStSc);
				}
			}
			//lstTeacherStatusScores = criteria.list();
		} 
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherStatusScores;
	}
}
