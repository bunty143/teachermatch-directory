package tm.dao.cgreport;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.cgreport.PercentileNationalCompositeTScore;
import tm.dao.generic.GenericHibernateDAO;

public class PercentileNationalCompositeTScoreDAO extends GenericHibernateDAO<PercentileNationalCompositeTScore, Integer>{
	public PercentileNationalCompositeTScoreDAO() {
		super(PercentileNationalCompositeTScore.class);
	}
	@Transactional(readOnly=false)
	public List<PercentileNationalCompositeTScore> findPercentileCalculationByOrder()
	{
		List<PercentileNationalCompositeTScore> lstPercentileNationalCompositeTScore= null;
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		try	{
			Criterion criterion0 = Restrictions.between("createdDateTime", sDate, eDate);
			lstPercentileNationalCompositeTScore = findByCriteria(Order.asc("compositeTValue"),criterion0);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstPercentileNationalCompositeTScore;
	}
}
