package tm.dao.cgreport;

import tm.bean.UserSupport;
import tm.bean.cgreport.TmpPercentileWiseZScore;
import tm.dao.generic.GenericHibernateDAO;

public class TmpPercentileWiseZScoreDAO extends GenericHibernateDAO<TmpPercentileWiseZScore, Integer>{

	public TmpPercentileWiseZScoreDAO() {
		super(TmpPercentileWiseZScore.class);
	}
}
