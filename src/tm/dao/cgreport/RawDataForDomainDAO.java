package tm.dao.cgreport;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.cgreport.RawDataForDomain;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DomainMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.utility.Utility;

public class RawDataForDomainDAO extends GenericHibernateDAO<RawDataForDomain, Integer>{	
	
	public RawDataForDomainDAO(){
		super(RawDataForDomain.class);
	}
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private DistrictMasterDAO districtMaster;
	
	@Transactional(readOnly=false)
	public List<RawDataForDomain> findByDomain(DomainMaster domainMaster){

		List<RawDataForDomain> lstRawDataForDomain = null;
		try{
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			Criterion criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion = Restrictions.eq("domainMaster", domainMaster);
			lstRawDataForDomain = findByCriteria(criterion,criterion1);
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return lstRawDataForDomain;
	}

	@Transactional(readOnly=false)
	public List<RawDataForDomain> findByTeachers(List<TeacherDetail> lstTeacherDetails)
	{
		List<RawDataForDomain> lstRawDataForDomain = new LinkedList<RawDataForDomain>();
		try{		
			if(lstTeacherDetails!=null && lstTeacherDetails.size()>0){
				Calendar cal = Calendar.getInstance();
				cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
				Date sDate=cal.getTime();
				cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
				Date eDate=cal.getTime();

				Criterion criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
				Criterion criterion2 = Restrictions.in("teacherDetail", lstTeacherDetails);
				lstRawDataForDomain = findByCriteria(criterion1,criterion2);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return lstRawDataForDomain;
	}

	@Transactional(readOnly=false)
	public List<RawDataForDomain> findByDomainAndTeachers(DomainMaster domainMaster, List<TeacherDetail> lstTeacherDetails)
	{
		List<RawDataForDomain> lstRawDataForDomain = new LinkedList<RawDataForDomain>();
		try{		
			if(lstTeacherDetails!=null && lstTeacherDetails.size()>0){
				Calendar cal = Calendar.getInstance();
				cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
				Date sDate=cal.getTime();
				cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
				Date eDate=cal.getTime();

				Criterion criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
				Criterion criterion = Restrictions.eq("domainMaster", domainMaster);
				Criterion criterion2 = Restrictions.in("teacherDetail", lstTeacherDetails);
				lstRawDataForDomain = findByCriteria(criterion,criterion1,criterion2);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return lstRawDataForDomain;
	}

	@Transactional(readOnly=false)
	public List<RawDataForDomain> findByDomainAndTeachersNew(DomainMaster domainMaster, List<TeacherAssessmentdetail> lstTeacherDetailsAssessmentdetails)
	{
		List<RawDataForDomain> lstRawDataForDomain = new LinkedList<RawDataForDomain>();
		try{		
			if(lstTeacherDetailsAssessmentdetails!=null && lstTeacherDetailsAssessmentdetails.size()>0){
				
				Criterion criterion = Restrictions.eq("domainMaster", domainMaster);
				Criterion criterion2 = Restrictions.in("teacherAssessmentdetail", lstTeacherDetailsAssessmentdetails);
				lstRawDataForDomain = findByCriteria(criterion,criterion2);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return lstRawDataForDomain;
	}
	@Transactional(readOnly=false)
	public List<RawDataForDomain> findAllInCurrentYear()
	{
		List<RawDataForDomain> lstRawDataForDomain = null;
		try {
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			Criterion criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
			lstRawDataForDomain = findByCriteria(criterion1);			

		} catch (Exception e) {
			e.printStackTrace();
		}		
		return lstRawDataForDomain;
	}

	@Transactional(readOnly=false)
	public List findTeacherCompositeScore()
	{
		try 
		{
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			System.out.println(getPersistentClass());
			Session session = getSession();
			Criteria crit = session.createCriteria(getPersistentClass());
			//crit.createAlias("courses", "course");
			crit.addOrder(Order.desc("totalScore"));
			List result = crit.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherDetail"))
					.add(Projections.sum("score").as("totalScore"))	
					//.add( Projections.rowCount() )
			).list();
			return result;

		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Transactional(readOnly=false)
	public List findTeacherCompositeScoreByLimit(int startPos,int limit,Criterion... criterion)
	{
		try 
		{
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			System.out.println(getPersistentClass());
			Session session = getSession();
			Criteria crit = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				crit.add(c);
			}
			crit.setFirstResult(startPos);
			crit.setMaxResults(limit);

			crit.addOrder(Order.desc("totalScore"));
			List result = crit.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherDetail"))
					.add(Projections.sum("score").as("totalScore"))	
					//.add( Projections.rowCount() )
			).list();
			return result;

		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Transactional(readOnly=false)
	public List findTeacherEpiNormScoreByLimit(int startPos,int limit,int order,int normscore,boolean isLimit,List<DomainMaster> domainMasters,List<TeacherDetail> teacherDetails)
	{
		try 
		{
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			List<Integer> domains = new ArrayList<Integer>();
			for(DomainMaster domainMaster : domainMasters)
				domains.add(domainMaster.getDomainId());
			String orderby="asc";
			if(order==1)
				orderby="desc";


			Session session = getSession();
			String queryText = "SELECT rdd.teacherDetail, (ROUND(SUM( dm.multiplier * np.tValue ))) AS epinorm " +
			" FROM RawDataForDomain rdd, DomainMaster dm, PercentileCalculation np " +
			" WHERE rdd.domainMaster = dm.domainId " +
			" AND rdd.domainMaster = np.domainMaster and rdd.score = np.score " +
			" AND dm.domainId IN (:domainMasters) and (rdd.createdDateTime between :from and :to)" ;
			if(teacherDetails.size()>0)
				queryText=queryText+" AND rdd.teacherDetail IN (:teacherDetails) ";

			//queryText=queryText+" GROUP BY rdd.teacherDetail order by ROUND( SUM( dm.multiplier * np.tValue )) "+orderby;
			queryText=queryText+" GROUP BY rdd.teacherDetail having ROUND( SUM( dm.multiplier * np.tValue ))>"+normscore+" order by ROUND( SUM( dm.multiplier * np.tValue )) "+orderby;

			
			//System.out.println("queryText: "+queryText);

			Query query = session.createQuery(queryText);
			//query.setParameterList("domainMasters", new Integer[]{1,2,3});
			query.setParameterList("domainMasters", domains);
			query.setParameter("from", sDate);
			query.setParameter("to", eDate);
			if(teacherDetails.size()>0)
				query.setParameterList("teacherDetails", teacherDetails);

			if(isLimit)
			{
				query.setFirstResult(startPos);
				query.setMaxResults(limit);
			}
			List<Object[]> rows = query.list();
			//System.out.println("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL : "+rows.size());
			return rows;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional(readOnly=false)
	public List findTeacherEpiNormScore(String normScoreWithSelectVal,List<DomainMaster> domainMasters,List<TeacherDetail> teacherDetails)
	{
		try 
		{
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			List<Integer> domains = new ArrayList<Integer>();
			for(DomainMaster domainMaster : domainMasters)
				domains.add(domainMaster.getDomainId());
			String orderby="asc";

			Session session = getSession();
			String queryText = "SELECT rdd.teacherDetail, (ROUND(SUM( dm.multiplier * np.tValue ))) AS epinorm " +
			" FROM RawDataForDomain rdd, DomainMaster dm, PercentileCalculation np " +
			" WHERE rdd.domainMaster = dm.domainId " +
			" AND rdd.domainMaster = np.domainMaster and rdd.score = np.score " +
			" AND dm.domainId IN (:domainMasters) and (rdd.createdDateTime between :from and :to)" ;
			if(teacherDetails.size()>0)
				queryText=queryText+" AND rdd.teacherDetail IN (:teacherDetails) ";

			//queryText=queryText+" GROUP BY rdd.teacherDetail order by ROUND( SUM( dm.multiplier * np.tValue )) "+orderby; "+normScoreWithSelectVal+"
			queryText=queryText+" GROUP BY rdd.teacherDetail having ROUND( SUM( dm.multiplier * np.tValue ))"+normScoreWithSelectVal+" order by ROUND( SUM( dm.multiplier * np.tValue )) "+orderby;

			System.out.println("normScoreWithSelectVal::::"+normScoreWithSelectVal);
			System.out.println("queryText: "+queryText);

			Query query = session.createQuery(queryText);
			//query.setParameterList("domainMasters", new Integer[]{1,2,3});
			query.setParameterList("domainMasters", domains);
			query.setParameter("from", sDate);
			query.setParameter("to", eDate);
			if(teacherDetails.size()>0)
				query.setParameterList("teacherDetails", teacherDetails);

			List<Object[]> rows = query.list();
			System.out.println(">>>>>>>>>>>|||||>>>>>>>>> : "+rows.size());
			return rows;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional(readOnly=false)
	public List findAllTeacherEpiNormScoreByLimit(int startPos,int limit,int order,int normscore,boolean isLimit,List<DomainMaster> domainMasters,List<TeacherDetail> teacherDetails)
	{
		try 
		{
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			List<Integer> domains = new ArrayList<Integer>();
			for(DomainMaster domainMaster : domainMasters)
				domains.add(domainMaster.getDomainId());
			String orderby="asc";
			if(order==1)
				orderby="desc";


			Session session = getSession();
			//String queryText = "SELECT rdd.teacherDetail, (ROUND(SUM( dm.multiplier * np.tValue ))) AS epinorm  " +
			String queryText = "SELECT rdd.teacherDetail, (dm.multiplier * np.tValue) AS epinorm,rdd.domainMaster,np.tValue  " +
					//", sum(if(dm.domainId = 1,np.tValue,null)) as one,sum(if(dm.domainId = 1,np.tValue,null)) as two,sum(if(dm.domainId = 1,np.tValue,null)) as three" +
			" FROM RawDataForDomain rdd, DomainMaster dm, PercentileCalculation np" +
			" WHERE rdd.domainMaster = dm.domainId " +
			" AND rdd.domainMaster = np.domainMaster and rdd.score = np.score " +
			" AND dm.domainId IN (:domainMasters) and (rdd.createdDateTime between :from and :to)" ;
			if(teacherDetails.size()>0)
				queryText=queryText+" AND rdd.teacherDetail IN (:teacherDetails) ";
			
			queryText=queryText+" order BY rdd.teacherDetail,dm.domainId ";
			//queryText=queryText+" GROUP BY rdd.teacherDetail having ROUND( SUM( dm.multiplier * np.tValue ))>"+normscore+" order by ROUND( SUM( dm.multiplier * np.tValue )) "+orderby;

			
			System.out.println("queryText: "+queryText);

			Query query = session.createQuery(queryText);
			//query.setParameterList("domainMasters", new Integer[]{1,2,3});
			query.setParameterList("domainMasters", domains);
			query.setParameter("from", sDate);
			query.setParameter("to", eDate);
			if(teacherDetails.size()>0)
				query.setParameterList("teacherDetails", teacherDetails);

			if(isLimit)
			{
				query.setFirstResult(startPos);
				query.setMaxResults(limit);
			}
			List<Object[]> rows = query.list();
			//System.out.println("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL : "+rows.size());
			return rows;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional(readOnly=false)
	public List findAllTeacherEpiNormScore(int startPos,int limit,int order,int normscore,boolean isLimit,List<DomainMaster> domainMasters,List<TeacherDetail> teacherDetails)
	{
		try 
		{
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			List<Integer> domains = new ArrayList<Integer>();
			for(DomainMaster domainMaster : domainMasters)
				domains.add(domainMaster.getDomainId());
			String orderby="asc";
			if(order==1)
				orderby="desc";

			Session session = getSession();
			System.out.println("Teachers::::::::: "+teacherDetails.size());
			String sql = "";
			
			sql = "SELECT rdd.teacherId, (ROUND(SUM( dm.multiplier * np.tValue ))) AS epinorm ," +
				" ROUND(SUM(IF(dm.domainId = 1, np.tValue,0))) one,ROUND(SUM(IF(dm.domainId = 2, np.tValue,0))) two,ROUND(SUM(IF(dm.domainId = 3, np.tValue,0))) three ," +
				" td.firstName,td.lastName,td.emailAddress,ts.createdDateTime FROM rawdatafordomain rdd, domainmaster dm, percentilecalculation np,teacherdetail td " +
				" ,teacherassessmentstatus ts WHERE rdd.domainId = dm.domainId  AND rdd.domainId = np.domainId and rdd.score = np.score  " +
				" AND dm.domainId IN (:domainMasters) and (rdd.createdDateTime between :from and :to) and (ts.createdDateTime between :from and :to) and " +
				" td.teacherId=rdd.teacherId and ts.teacherId=rdd.teacherId and ts.assessmentType=1 group BY rdd.teacherId order by td.firstName";
			
			/*sql = "SELECT rdd.teacherId, (ROUND(SUM( dm.multiplier * np.tValue ))) AS epinorm ," +
			" ROUND(SUM(IF(dm.domainId = 1, np.tValue,0))) one,ROUND(SUM(IF(dm.domainId = 2, np.tValue,0))) two,ROUND(SUM(IF(dm.domainId = 3, np.tValue,0))) three ," +
			" td.firstName,td.lastName,td.emailAddress,td.createdDateTime FROM rawdatafordomain rdd, domainmaster dm, percentilecalculation np,teacherdetail td " +
			" WHERE rdd.domainId = dm.domainId  AND rdd.domainId = np.domainId and rdd.score = np.score  " +
			" AND dm.domainId IN (:domainMasters) and (rdd.createdDateTime between :from and :to)  and td.teacherId=rdd.teacherId group BY rdd.teacherId";*/
			
			//System.out.println(sql);
			Query query = session.createSQLQuery(sql);
			query.setParameterList("domainMasters", domains);
			query.setParameter("from", sDate);
			query.setParameter("to", eDate);
			if(teacherDetails.size()>0)
				query.setParameterList("teacherDetails", teacherDetails);

			if(isLimit)
			{
				query.setFirstResult(startPos);
				query.setMaxResults(limit);
			}
			
	            
			List<Object[]> rows = query.list();
			System.out.println("KKKKKKKKKKKKKKKKKKKKKKKKKKKK : "+rows.size());
			return rows;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@Transactional(readOnly=false)
	public List<RawDataForDomain> findAllInCurrentYearByTeacher(List<TeacherDetail> teacherDetails, TeacherAssessmentdetail teacherAssessmentdetail)
	{
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		List<RawDataForDomain> lstRawDataForDomain = null;
		try {
			Criterion criterion2 = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion3 = Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail);
			criteria.add(criterion2);
			criteria.add(criterion3);
			lstRawDataForDomain = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		
		return lstRawDataForDomain;
	}
	
	@Transactional(readOnly=false)
	public List findTeacherEpiNormScoreForReport(int startPos,int limit,String sortOrderStrVal,boolean isLimit,String sortColomnName,String startDate,String enddate,Integer districtId)
	{
		System.out.println("  isLimit     >>>>>>>>>>>>>> "+isLimit);
		try 
		{

			System.out.println(" startDate :: "+startDate+" enddate :: "+enddate);
			DistrictMaster districtmaster= districtMaster.findById(districtId, false, false);
			String districtname=districtmaster.getDistrictName();
			Date startD = new Date("1/1/2000");
			Date endD = new Date(); 
			
			if(startDate!=null && !startDate.equals(""))
				startD =  Utility.getCurrentDateFormart(startDate);
			
			if(enddate!=null && !enddate.equals(""))
				endD = Utility.getCurrentDateFormart(enddate);
			
			Session session = getSession();
			String sql = "";
			
			if(sortColomnName.equalsIgnoreCase("lastName"))
				sortColomnName = "td.lastName";
			else if(sortColomnName.equalsIgnoreCase("firstName"))
				sortColomnName = "td.firstName";
			else if(sortColomnName.equalsIgnoreCase("emailAddress"))
				sortColomnName = "td.emailAddress";
			else if(sortColomnName.equalsIgnoreCase("normScore"))
				sortColomnName = "epi.teachernormScore";
			else if(sortColomnName.equalsIgnoreCase("epiDate"))
				sortColomnName = "tas.createdDateTime";
			
			String orderby = " order by "+sortColomnName+" "+sortOrderStrVal ;
			
			sql = " Select distinct " +
			"   td.lastName , "+
			"	td.firstName , "+
			"	td.emailAddress , "+
			"	date_format(epi.assessmentDateTime, '%c/%e/%y') `EPI Date`, "+
			"	nullif(epi.teachernormScore,0) `EPI Norm Score` "+
			"   from jobforteacher jft "+
			"	inner join teacherdetail td on jft.teacherid=td.teacherid and td.status <> 'I'  "+
			"	inner join districtmaster dm on jft.districtid=dm.districtid "+
			"	left join teachernormscore epi on td.teacherid=epi.teacherid  "+
			"			and epi.createddatetime=(select max(createddatetime) lastestepi  "+
			"									from teachernormscore where teacherid=epi.teacherid) "+
			"	left join teacherassessmentstatus tas on tas.teacherid=epi.teacherid  "+
			"			and tas.assessmenttype=1 and tas.statusId=4  "+
			 "          and assessmentcompletedDateTime  "+
			 "                = (select max(assessmentcompletedDateTime) latest  "+
			"				   from teacherassessmentstatus "+
			"				  where teacherid=tas.teacherid and assessmenttype=1 and statusId=4 ) "+
			"				  left join teacheranswerdetail tad on tad.teacherassessmentid=tas.teacherAssessmentId "+
			"				  and tad.answerId=(select max(answerId) lastitem "+
			"				  from teacheranswerdetail "+
			"				  where tad.teacherassessmentid=teacherassessmentid) "+
			" where  "+
			"	jft.status <> 8  "+
			"	and epi.teachernormScore is not null "+
			 "   and if (:districtname='', dm.districtname <>'', dm.districtname=:districtname) "+
			"	and ( "+
			"			(jft.createdDateTime >= :sDate and jft.createdDateTime < :eDate) "+
			"			or  "+
			"			(epi.assessmentDateTime >= :sDate and epi.assessmentDateTime < :eDate "+
			"			) "+
			"       )" +
			""+orderby; 
			System.out.println("orderby  "+orderby);
			System.out.println("Sql Query : "+sql);
			Query query = session.createSQLQuery(sql);
			
			query.setParameter("sDate", startD);
			query.setParameter("eDate", endD);
			query.setParameter("districtname", districtname);
			
			System.out.println(" isLimit :: "+isLimit+" startD ::"+startD+" endD ::"+endD+" districtId "+districtId);
			
			if(isLimit)
			{
				System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> limit :: "+limit);
				query.setFirstResult(startPos);
				query.setMaxResults(limit);
			}
			
			List<Object[]> rows = query.list();
			return rows;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transactional(readOnly=false)
	public int findTeacherEpiNormScoreForReportCount(String startDate,String enddate,Integer districtId)
	{
		try 
		{
			System.out.println(" startDate :: "+startDate+" enddate :: "+enddate);
			DistrictMaster districtmaster= districtMaster.findById(districtId, false, false);
			String districtname=districtmaster.getDistrictName();
			
			Date startD = new Date("1/1/2000");
			Date endD = new Date(); 
			
			if(startDate!=null && !startDate.equals(""))
				startD =  Utility.getCurrentDateFormart(startDate);
			
			if(enddate!=null && !enddate.equals(""))
				endD = Utility.getCurrentDateFormart(enddate);
			
			Session session = getSession();
			
			String sql = "";
			sql = " Select distinct " +
			"   td.lastName ,   "+
			"	td.firstName ,  "+
			"	td.emailAddress , "+
			"	date_format(epi.assessmentDateTime, '%c/%e/%y') `EPI Date`, "+
			"	nullif(epi.teachernormScore,0) `EPI Norm Score` "+
			"   from jobforteacher jft "+
			"	inner join teacherdetail td on jft.teacherid=td.teacherid and td.status <> 'I'  "+
			"	inner join districtmaster dm on jft.districtid=dm.districtid "+
			"	left join teachernormscore epi on td.teacherid=epi.teacherid  "+
			"			and epi.createddatetime=(select max(createddatetime) lastestepi  "+
			"									from teachernormscore where teacherid=epi.teacherid) "+
			"	left join teacherassessmentstatus tas on tas.teacherid=epi.teacherid  "+
			"			and tas.assessmenttype=1 and tas.statusId=4  "+
			 "          and assessmentcompletedDateTime  "+
			 "                = (select max(assessmentcompletedDateTime) latest  "+
			"				   from teacherassessmentstatus "+
			"				  where teacherid=tas.teacherid and assessmenttype=1 and statusId=4 ) "+
			"				  left join teacheranswerdetail tad on tad.teacherassessmentid=tas.teacherAssessmentId "+
			"				  and tad.answerId=(select max(answerId) lastitem "+
			"				  from teacheranswerdetail "+
			"				  where tad.teacherassessmentid=teacherassessmentid) "+
			" where  "+
			"	jft.status <> 8  "+
			"	and epi.teachernormScore is not null "+
			 "   and if (:districtname='', dm.districtname <>'', dm.districtname=:districtname) "+
			"	and ( "+
			"			(jft.createdDateTime >= :sDate and jft.createdDateTime < :eDate) "+
			"			or  "+
			"			(epi.assessmentDateTime >= :sDate and epi.assessmentDateTime < :eDate "+
			"			) "+
			"       ) ";
			
			Query query = session.createSQLQuery(sql);
			
			query.setParameter("sDate", startD);
			query.setParameter("eDate", endD);
			query.setParameter("districtname", districtname);
			List<Object[]> rows = query.list();
			return rows.size();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param teacherDetailList
	 * @param assessmentDetail
	 * @param assessmentType
	 * @param assessmentTakenCount
	 * @return rawDataForDomainList
	 */
	@Transactional(readOnly=true)
	public List<RawDataForDomain> getRawDataForDomain(List<TeacherDetail> teacherDetailList,AssessmentDetail assessmentDetail,Integer assessmentType,Integer assessmentTakenCount)
	{
		List<RawDataForDomain> rawDataForDomainList = null;
		try {
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetailList);
			Criterion criterion2 = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion3 = Restrictions.eq("assessmentType", assessmentType);
			Criterion criterion4 = Restrictions.eq("assessmentTakenCount", assessmentTakenCount);
			
			rawDataForDomainList = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rawDataForDomainList;
	}
	
	@Transactional(readOnly=false)
	public List<RawDataForDomain> findByDomainAndTeacherList(DomainMaster domainMaster, List<TeacherDetail> teacherDetailList)
	{
		List<RawDataForDomain> rawDataForDomainList = new LinkedList<RawDataForDomain>();
		try{		
			if(teacherDetailList!=null && teacherDetailList.size()>0){
				
				Criterion criterion1 = Restrictions.eq("domainMaster", domainMaster);
				Criterion criterion2 = Restrictions.in("teacherDetail", teacherDetailList);
				rawDataForDomainList = findByCriteria(criterion1,criterion2);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return rawDataForDomainList;
	}
	
	@Transactional(readOnly=false)
	public List<RawDataForDomain> findByDomainTeacherAndTAD(DomainMaster domainMaster, List<TeacherDetail> teacherDetailList, List<TeacherAssessmentdetail> teacherAssessmentdetailList)
	{
		List<RawDataForDomain> rawDataForDomainList = new LinkedList<RawDataForDomain>();
		try{		
			if(teacherDetailList!=null && teacherDetailList.size()>0){
				
				Criterion criterion1 = Restrictions.eq("domainMaster", domainMaster);
				Criterion criterion2 = Restrictions.in("teacherDetail", teacherDetailList);
				Criterion criterion3 = Restrictions.in("teacherAssessmentdetail", teacherAssessmentdetailList);
				rawDataForDomainList = findByCriteria(criterion1,criterion2,criterion3);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return rawDataForDomainList;
	}
	
	@Transactional(readOnly=false)
	public List<RawDataForDomain> findByTeacher(TeacherDetail teacherDetail)
	{
		List<RawDataForDomain> rawDataForDomainList = new LinkedList<RawDataForDomain>();
		try{		
			
				Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
				rawDataForDomainList = findByCriteria(criterion1);
			
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return rawDataForDomainList;
	}
	
	//::::::::::::::::::::::::::::::::added by brajesh:::::::::::::::::::::::::::::::::::::::::::
	
	@Transactional(readOnly=false)
	public List<RawDataForDomain> findByDomainAndTeacherListForSpScore(List<TeacherDetail> teacherDetailList, DomainMaster master)
	{
		List<RawDataForDomain> rawDataForDomainList = new ArrayList<RawDataForDomain>();
		try 
		{
			if(teacherDetailList!=null && teacherDetailList.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass())
				.createAlias("teacherDetail", "td")
				.createAlias("teacherAssessmentdetail", "tad");
				DetachedCriteria subQuery = DetachedCriteria.forClass(RawDataForDomain.class, "tns")
				.setProjection(Projections.projectionList()
			    		.add(Projections.max("tns.teacherAssessmentdetail.teacherAssessmentId")))
			    		.add(Restrictions.eqProperty("tns.teacherDetail.teacherId", "td.teacherId"));
				criteria.add(Property.forName("tad.teacherAssessmentId").eq(subQuery));
				criteria.add(Restrictions.in("teacherDetail", teacherDetailList));
				criteria.add(Restrictions.eq("domainMaster", master));
				rawDataForDomainList = criteria.list() ;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return rawDataForDomainList;
	}
}
