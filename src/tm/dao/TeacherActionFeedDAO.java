package tm.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherActionFeed;
import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class TeacherActionFeedDAO extends GenericHibernateDAO<TeacherActionFeed, Integer> 
{
	public TeacherActionFeedDAO() {
		super(TeacherActionFeed.class);
	}

	@Transactional(readOnly=false)
	public List findTeacherListByNormScore(int startPos,int limit,DistrictMaster districtMaster,SchoolMaster schoolMaster, int normScoreVal,List<TeacherDetail> teacherDetails)
	{
		try 
		{
			Integer candidateFeedNormScore=0;
			Integer candidateFeedDaysOfNoActivity=0;

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			if(districtMaster!=null && schoolMaster==null)
			{
				candidateFeedNormScore = districtMaster.getCandidateFeedNormScore();
				candidateFeedNormScore = candidateFeedNormScore==null?0:candidateFeedNormScore;
				candidateFeedDaysOfNoActivity = districtMaster.getCandidateFeedDaysOfNoActivity();
				candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity==null?0:candidateFeedDaysOfNoActivity; 
				criteria.add(Restrictions.gt("teacherNormScore", candidateFeedNormScore));
				criteria.add(Restrictions.eq("districtMaster", districtMaster));
			}else if(schoolMaster!=null)
			{
				candidateFeedNormScore = schoolMaster.getCandidateFeedNormScore();
				candidateFeedNormScore = candidateFeedNormScore==null?0:candidateFeedNormScore;
				candidateFeedDaysOfNoActivity = schoolMaster.getCandidateFeedDaysOfNoActivity();
				candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity==null?0:candidateFeedDaysOfNoActivity;
				criteria.add(Restrictions.gt("teacherNormScore", candidateFeedNormScore));
				criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}
			
			/*System.out.println("schoolMaster: "+schoolMaster);
			System.out.println("districtMaster: "+districtMaster);
			System.out.println("candidateFeedNormScore: "+candidateFeedNormScore);
			System.out.println("candidateFeedDaysOfNoActivity: "+candidateFeedDaysOfNoActivity);*/
			
			if(candidateFeedDaysOfNoActivity>0)
				//criteria.add(Restrictions.sqlRestriction(" DATEDIFF(current_date, lastActivityDate) >= "+candidateFeedDaysOfNoActivity));
				criteria.add(Restrictions.gt("daysForLastActivity",candidateFeedDaysOfNoActivity));
			if(teacherDetails.size()>0)
			criteria.add(Restrictions.not(Restrictions.in("teacherDetail", teacherDetails)));
			
			criteria.setProjection( Projections.projectionList()
					.add(Projections.property("teacheractionfeedId"))
					.add(Projections.groupProperty("teacherDetail"))
					.add(Projections.property("isInternal"))
					.add(Projections.property("jobAppliedDate"))
					.add(Projections.property("teacherNormScore"))
					.add(Projections.property("lastActivityOnJob"))
					//.add(Projections.property("districtMaster"))
					//.add(Projections.property("schoolMaster"))
					.add(Projections.property("jobOrder"))
					//.add(Projections.property("lastActivityDate"))
					//.add(Projections.property("daysForLastActivity"))
			);
			criteria.addOrder(Order.desc("teacherNormScore"));
			criteria.addOrder(Order.desc("lastActivityDate"));
			
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			List rows = criteria.list();
			//System.out.println("rows()::::::::::::::::::::: "+rows.size());
			return rows;


		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	
	@Transactional(readOnly=false)
	public List findTeacherListByNormScoreByHBD(int startPos,int limit,DistrictMaster districtMaster,SchoolMaster schoolMaster, int normScoreVal,List<TeacherDetail> teacherDetails,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
	{
		try 
		{
			Integer candidateFeedNormScore=0;
			Integer candidateFeedDaysOfNoActivity=0;

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			if(branchMaster!=null){
				criteria.add(Restrictions.eq("branchMaster",branchMaster));
				candidateFeedNormScore = branchMaster.getCandidateFeedNormScore();
				candidateFeedNormScore = candidateFeedNormScore==null?0:candidateFeedNormScore;
				candidateFeedDaysOfNoActivity = branchMaster.getCandidateFeedDaysOfNoActivity();
				candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity==null?0:candidateFeedDaysOfNoActivity; 
				criteria.add(Restrictions.gt("teacherNormScore", candidateFeedNormScore));
				criteria.add(Restrictions.eq("branchMaster", branchMaster));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(headQuarterMaster!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
				
				candidateFeedNormScore = headQuarterMaster.getCandidateFeedNormScore();
				candidateFeedNormScore = candidateFeedNormScore==null?0:candidateFeedNormScore;
				candidateFeedDaysOfNoActivity = headQuarterMaster.getCandidateFeedDaysOfNoActivity();
				candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity==null?0:candidateFeedDaysOfNoActivity;
				criteria.add(Restrictions.gt("teacherNormScore", candidateFeedNormScore));
				criteria.add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			
			if(districtMaster!=null && schoolMaster==null)
			{
				candidateFeedNormScore = districtMaster.getCandidateFeedNormScore();
				candidateFeedNormScore = candidateFeedNormScore==null?0:candidateFeedNormScore;
				candidateFeedDaysOfNoActivity = districtMaster.getCandidateFeedDaysOfNoActivity();
				candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity==null?0:candidateFeedDaysOfNoActivity; 
				criteria.add(Restrictions.gt("teacherNormScore", candidateFeedNormScore));
				criteria.add(Restrictions.eq("districtMaster", districtMaster));
			}else if(schoolMaster!=null)
			{
				candidateFeedNormScore = schoolMaster.getCandidateFeedNormScore();
				candidateFeedNormScore = candidateFeedNormScore==null?0:candidateFeedNormScore;
				candidateFeedDaysOfNoActivity = schoolMaster.getCandidateFeedDaysOfNoActivity();
				candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity==null?0:candidateFeedDaysOfNoActivity;
				criteria.add(Restrictions.gt("teacherNormScore", candidateFeedNormScore));
				criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}
			
			/*System.out.println("schoolMaster: "+schoolMaster);
			System.out.println("districtMaster: "+districtMaster);
			System.out.println("candidateFeedNormScore: "+candidateFeedNormScore);
			System.out.println("candidateFeedDaysOfNoActivity: "+candidateFeedDaysOfNoActivity);*/
			
			if(candidateFeedDaysOfNoActivity>0)
				//criteria.add(Restrictions.sqlRestriction(" DATEDIFF(current_date, lastActivityDate) >= "+candidateFeedDaysOfNoActivity));
				criteria.add(Restrictions.gt("daysForLastActivity",candidateFeedDaysOfNoActivity));
			if(teacherDetails.size()>0)
			criteria.add(Restrictions.not(Restrictions.in("teacherDetail", teacherDetails)));
			
			criteria.setProjection( Projections.projectionList()
					.add(Projections.property("teacheractionfeedId"))
					.add(Projections.groupProperty("teacherDetail"))
					.add(Projections.property("isInternal"))
					.add(Projections.property("jobAppliedDate"))
					.add(Projections.property("teacherNormScore"))
					.add(Projections.property("lastActivityOnJob"))
					//.add(Projections.property("districtMaster"))
					//.add(Projections.property("schoolMaster"))
					.add(Projections.property("jobOrder"))
					//.add(Projections.property("lastActivityDate"))
					//.add(Projections.property("daysForLastActivity"))
			);
			criteria.addOrder(Order.desc("teacherNormScore"));
			criteria.addOrder(Order.desc("lastActivityDate"));
			
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			List rows = criteria.list();
			//System.out.println("rows()::::::::::::::::::::: "+rows.size());
			return rows;


		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	
	@Transactional(readOnly=false)
	public List findAllTeacherList(int startPos,int limit,DistrictMaster districtMaster,SchoolMaster schoolMaster, int normScoreVal,List<TeacherDetail> teacherDetails,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
	{

		try 
		{
			Integer candidateFeedNormScore=0;
			Integer candidateFeedDaysOfNoActivity=0;

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			if(branchMaster!=null){
				criteria.add(Restrictions.eq("branchMaster",branchMaster));
				candidateFeedNormScore = branchMaster.getCandidateFeedNormScore();
				candidateFeedNormScore = candidateFeedNormScore==null?0:candidateFeedNormScore;
				candidateFeedDaysOfNoActivity = branchMaster.getCandidateFeedDaysOfNoActivity();
				candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity==null?0:candidateFeedDaysOfNoActivity; 
				criteria.add(Restrictions.gt("teacherNormScore", candidateFeedNormScore));
				criteria.add(Restrictions.eq("branchMaster", branchMaster));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(headQuarterMaster!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
				
				candidateFeedNormScore = headQuarterMaster.getCandidateFeedNormScore();
				candidateFeedNormScore = candidateFeedNormScore==null?0:candidateFeedNormScore;
				candidateFeedDaysOfNoActivity = headQuarterMaster.getCandidateFeedDaysOfNoActivity();
				candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity==null?0:candidateFeedDaysOfNoActivity;
				criteria.add(Restrictions.gt("teacherNormScore", candidateFeedNormScore));
				criteria.add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			
			if(districtMaster!=null && schoolMaster==null)
			{
				candidateFeedNormScore = districtMaster.getCandidateFeedNormScore();
				candidateFeedNormScore = candidateFeedNormScore==null?0:candidateFeedNormScore;
				candidateFeedDaysOfNoActivity = districtMaster.getCandidateFeedDaysOfNoActivity();
				candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity==null?0:candidateFeedDaysOfNoActivity; 
				criteria.add(Restrictions.gt("teacherNormScore", candidateFeedNormScore));
				criteria.add(Restrictions.eq("districtMaster", districtMaster));
			}else if(schoolMaster!=null)
			{
				candidateFeedNormScore = schoolMaster.getCandidateFeedNormScore();
				candidateFeedNormScore = candidateFeedNormScore==null?0:candidateFeedNormScore;
				candidateFeedDaysOfNoActivity = schoolMaster.getCandidateFeedDaysOfNoActivity();
				candidateFeedDaysOfNoActivity = candidateFeedDaysOfNoActivity==null?0:candidateFeedDaysOfNoActivity;
				criteria.add(Restrictions.gt("teacherNormScore", candidateFeedNormScore));
				criteria.add(Restrictions.eq("districtMaster", districtMaster));
			}
			
			if(candidateFeedDaysOfNoActivity>0)
				//criteria.add(Restrictions.sqlRestriction(" DATEDIFF(current_date, lastActivityDate) >= "+candidateFeedDaysOfNoActivity));
				criteria.add(Restrictions.gt("daysForLastActivity",candidateFeedDaysOfNoActivity));
			if(teacherDetails.size()>0)
			criteria.add(Restrictions.not(Restrictions.in("teacherDetail", teacherDetails)));
			
			criteria.setProjection( Projections.projectionList()
					.add(Projections.property("teacheractionfeedId"))
					.add(Projections.groupProperty("teacherDetail"))
					.add(Projections.property("isInternal"))
					.add(Projections.property("jobAppliedDate"))
					.add(Projections.property("teacherNormScore"))
					.add(Projections.property("lastActivityOnJob"))
					//.add(Projections.property("districtMaster"))
					//.add(Projections.property("schoolMaster"))
					.add(Projections.property("jobOrder"))
					//.add(Projections.property("lastActivityDate"))
					//.add(Projections.property("daysForLastActivity"))
			);
			criteria.addOrder(Order.desc("teacherNormScore"));
			criteria.addOrder(Order.desc("lastActivityDate"));
			
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			List rows = criteria.list();
			//System.out.println("rows()::::::::::::::::::::: "+rows.size());
			return rows;


		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
}
