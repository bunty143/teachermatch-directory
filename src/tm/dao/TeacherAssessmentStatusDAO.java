package tm.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.servlet.WorkThreadServlet;

public class TeacherAssessmentStatusDAO extends GenericHibernateDAO<TeacherAssessmentStatus, Integer> 
{
	public TeacherAssessmentStatusDAO() 
	{
		super(TeacherAssessmentStatus.class);
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void UserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find Assessment Taken.
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTaken(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		//criteria.add(Restrictions.between("auditDate", cal.getTime(), eDate));
		
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = null;

			if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0){
				criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
				if(teacherDetail==null){
					teacherAssessmentStatusList = findByCriteria(criterion1,Restrictions.isNull("jobOrder"),Restrictions.eq("assessmentType", 1));
				}
				else{
					teacherAssessmentStatusList = findByCriteria(criterion,criterion1,Restrictions.isNull("jobOrder"),Restrictions.eq("assessmentType", 1));
				}

			}
			else{
				criterion1 = Restrictions.eq("jobOrder", jobOrder);
				//criterion1 = Restrictions.eq("jobOrder", null);
				//criterion1 = Restrictions.isNull("jobOrder");
				if(teacherDetail==null){
					teacherAssessmentStatusList = findByCriteria(criterion1);
				}
				else{
					teacherAssessmentStatusList = findByCriteria(criterion,criterion1);
				}

			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	// Add PDP Configuration...
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTakenIPIandEPI(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		//criteria.add(Restrictions.between("auditDate", cal.getTime(), eDate));
		List<Integer> assessmenttype=new ArrayList<Integer>();
		assessmenttype.add(1);
		assessmenttype.add(4);
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = null;
			Criterion criterion3 = Restrictions.in("assessmentType",assessmenttype);

			if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0){
				criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
				if(teacherDetail==null){
					teacherAssessmentStatusList = findByCriteria(criterion1,Restrictions.isNull("jobOrder"),criterion3);
				}
				else{
					teacherAssessmentStatusList = findByCriteria(criterion,criterion1,Restrictions.isNull("jobOrder"),criterion3);
				}

			}
			else{
				criterion1 = Restrictions.eq("jobOrder", jobOrder);
				//criterion1 = Restrictions.eq("jobOrder", null);
				//criterion1 = Restrictions.isNull("jobOrder");
				if(teacherDetail==null){
					teacherAssessmentStatusList = findByCriteria(criterion1);
				}
				else{
					teacherAssessmentStatusList = findByCriteria(criterion,criterion1);
				}

			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	/**
	 * @author Amit Chaudhary
	 * @param assessmentDetail
	 * @param assessmentTakenCount
	 * @param teacherDetail
	 * @param jobOrder
	 * @return teacherAssessmentStatusList
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTaken(AssessmentDetail assessmentDetail,Integer assessmentTakenCount,TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion3 = Restrictions.eq("assessmentTakenCount", assessmentTakenCount);
			Criterion criterion4 = null;

			if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()<=0){
				teacherAssessmentStatusList = findByCriteria(criterion1,criterion2,criterion3,Restrictions.isNull("jobOrder"));
			}
			else if(jobOrder!=null && jobOrder.getJobId()!=null){
				criterion4 = Restrictions.eq("jobOrder", jobOrder);
				teacherAssessmentStatusList = findByCriteria(criterion1,criterion2,criterion3,criterion4);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	/**
	 * @author Amit Chaudhary
	 * @param teacherDetail
	 * @param jobOrder
	 * @param assessmentType
	 * @return teacherAssessmentStatusList
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTaken(TeacherDetail teacherDetail,JobOrder jobOrder, Integer assessmentType, StatusMaster statusMaster)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = null;
			Criterion criterion3 = null;

			if(jobOrder!=null && jobOrder.getJobId()!=null && (jobOrder.getJobId()==-3 || jobOrder.getJobId()==-4)){
				if(assessmentType==3)
					criterion2 = Restrictions.eq("assessmentType", 3);
				else if(assessmentType==4)
					criterion2 = Restrictions.eq("assessmentType", 4);
				if(statusMaster!=null){
					criterion3 = Restrictions.eq("statusMaster", statusMaster);
					teacherAssessmentStatusList = findByCriteria(criterion1,criterion2,criterion3,Restrictions.isNull("jobOrder"));
				}
				else
					teacherAssessmentStatusList = findByCriteria(criterion1,criterion2,Restrictions.isNull("jobOrder"));
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param teacherDetail
	 * @param assessmentDetailList
	 * @return teacherAssessmentStatusList
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTakenByAssessmentDetail(TeacherDetail teacherDetail, List<AssessmentDetail> assessmentDetailList)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.in("assessmentDetail", assessmentDetailList);
			
			teacherAssessmentStatusList = findByCriteria(criterion1,criterion2,Restrictions.isNull("jobOrder"));
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find JSI Taken.
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findJSITaken(TeacherDetail teacherDetail,List<JobOrder> jobOrders)
	{

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = null;
			criterion1 = Restrictions.in("jobOrder", jobOrders);
			teacherAssessmentStatusList = findByCriteria(criterion,criterion1);
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find Assessment Taken by Teachers.
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTakenByTeachers(List<TeacherDetail> teacherDetails)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		//criteria.add(Restrictions.between("auditDate", cal.getTime(), eDate));

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			Criterion criterion = Restrictions.eq("assessmentType", 1);
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			teacherAssessmentStatusList = findByCriteria(criterion,criterion1,criterion2,Restrictions.isNull("jobOrder"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}

// Add PDP Configuration...
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findLatestAssessmentTakenByTeachersEPI(TeacherDetail teacherDetails)
	{
		Session session = getSession();
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		//criteria.add(Restrictions.between("auditDate", cal.getTime(), eDate));

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			Criteria crit = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("assessmentType", 1);
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetails);
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion3 = Restrictions.isNull("jobOrder");
			crit.add(criterion);
			crit.add(criterion1);
			crit.add(criterion2);
			crit.add(criterion3);
			crit.addOrder(Order.desc("assessmentCompletedDateTime"));
			
			teacherAssessmentStatusList = crit.list();
				//findByCriteria(criterion,criterion1,criterion2,Restrictions.isNull("jobOrder"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	// Indra Jeet	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findLatestAssessmentTakenByTeachersIPI(TeacherDetail teacherDetails)
	{
		Session session = getSession();
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		//criteria.add(Restrictions.between("auditDate", cal.getTime(), eDate));

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			Criteria crit = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("assessmentType", 4);
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetails);
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion3 = Restrictions.isNull("jobOrder");
			crit.add(criterion);
			crit.add(criterion1);
			crit.add(criterion2);
			crit.add(criterion3);
			crit.addOrder(Order.desc("assessmentCompletedDateTime"));
			
			teacherAssessmentStatusList = crit.list();
				//findByCriteria(criterion,criterion1,criterion2,Restrictions.isNull("jobOrder"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find Assessment Taken by Teachers.
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTakenByTeachersYesterday(List<TeacherDetail> teacherDetails)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Date dateWithoutTime = cal.getTime();
		//System.out.println("dateWithoutTime: "+dateWithoutTime);

		Calendar cal1 = Calendar.getInstance();
		cal1.add(Calendar.DATE, -1);
		cal1.set(Calendar.HOUR_OF_DAY, 0);
		cal1.set(Calendar.MINUTE, 0);
		cal1.set(Calendar.SECOND, 0);
		cal1.set(Calendar.MILLISECOND, 0);


		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			Criterion criterion = Restrictions.eq("assessmentType", 1);
			Criterion criterion0 = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion1 = Restrictions.le("createdDateTime",dateWithoutTime);
			Criterion criterion2 = Restrictions.ge("createdDateTime",cal1.getTime());

			teacherAssessmentStatusList = findByCriteria(criterion,criterion0,criterion1,criterion2,Restrictions.isNull("jobOrder"));


		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find Assessment Taken by Teachers.
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTakenByTeachersByStauts(List<TeacherDetail> teacherDetails,StatusMaster statusMaster)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();


		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			Criterion criterion = Restrictions.eq("assessmentType", 1);
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion3 = Restrictions.eq("statusMaster", statusMaster);
			teacherAssessmentStatusList = findByCriteria(criterion,criterion1,criterion2,criterion3,Restrictions.isNull("jobOrder"));


		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find Assessment Taken by Teachers.
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTakenByTotalTeachers()
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();


		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			Criterion criterion = Restrictions.eq("assessmentType", 1);
			Criterion criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
			teacherAssessmentStatusList = findByCriteria(criterion,criterion1,Restrictions.isNull("jobOrder"));


		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find Assessment Taken By Teacher.
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTakenByTeacher(TeacherDetail teacherDetail,AssessmentDetail assessmentDetail)
	{

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = Restrictions.eq("assessmentDetail", assessmentDetail);

			teacherAssessmentStatusList = findByCriteria(criterion,criterion1);


		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}

	@Transactional(readOnly=false)
	public TeacherAssessmentStatus findAssessmentStatusByTeacherForBase(TeacherDetail teacherDetail)
	{
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("assessmentType",new Integer(1));
			//Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Criterion criterion4 = Restrictions.between("createdDateTime", sDate, eDate);
			lstTeacherAssessmentStatus = findByCriteria(criterion1,criterion2,criterion4);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherAssessmentStatus==null || lstTeacherAssessmentStatus.size()==0)
			return null;
		else
			return lstTeacherAssessmentStatus.get(0);
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find Assessment status.
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentStatusByTeacherAssessmentdetail(TeacherAssessmentdetail teacherAssessmentdetail)
	{
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;


		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherAssessmentdetail",teacherAssessmentdetail);

			lstTeacherAssessmentStatus = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		

		return lstTeacherAssessmentStatus;
		/*if(lstTeacherAssessmentStatus==null || lstTeacherAssessmentStatus.size()==0)
		return null;
		else
		return lstTeacherAssessmentStatus.get(0);*/
	}

	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentStatusByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;

		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);			
			lstTeacherAssessmentStatus = findByCriteria(criterion1);	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherAssessmentStatus;
	}

	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentStatusByTeacher1(TeacherDetail teacherDetail,List<JobOrder> lstjobOrder,StatusMaster statusInComplete)
	{
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;

		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);		
			Criterion criterion2 = Restrictions.in("jobOrder", lstjobOrder);		
			Criterion criterion3 = Restrictions.ne("statusMaster", statusInComplete);
			lstTeacherAssessmentStatus = findByCriteria(criterion1,criterion2,criterion3);	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherAssessmentStatus;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findTASByTeacherAndAssessment(TeacherDetail teacherDetail, AssessmentDetail assessmentDetail)
	{
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;

		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("assessmentDetail", assessmentDetail);
			lstTeacherAssessmentStatus = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherAssessmentStatus;
	}

	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<TeacherAssessmentStatus> findHireStatusByJob(JobOrder jobOrder) 
	{
		Session session = getSession();
		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
		List result = session.createCriteria(getPersistentClass())       
		.add(Restrictions.eq("jobOrder", jobOrder))
		.add(Restrictions.eq("statusMaster",statusMaster))
		.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("assessmentDetail"))
				.add(Projections.count("jobOrder"))  
		).list();
		return result;
	}



	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int findHireStatusByJobAndSchool(JobOrder jobOrder,SchoolMaster schoolMaster) 
	{
		int SchoolFlag=0;
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus=null;
		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
		Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);	
		Criterion criterion2 = Restrictions.eq("statusMaster", statusMaster);	
		Criterion criterion3 = Restrictions.eq("updatedByEntity",3);
		lstTeacherAssessmentStatus = findByCriteria(criterion1,criterion2,criterion3);
		for(TeacherAssessmentStatus teacherAssessmentStatus :lstTeacherAssessmentStatus){
			UserMaster userMaster=userMasterDAO.findById(teacherAssessmentStatus.getUpdatedBy().getUserId(),false,false);
			if(userMaster.getSchoolId().getSchoolId().equals(schoolMaster.getSchoolId())){
				SchoolFlag=1;
			}
		}
		return SchoolFlag;
	}

	@Transactional(readOnly=false)
	public Map<Integer, TeacherAssessmentStatus> findActiveJSIStatus(List<TeacherDetail> teacherDetails, JobOrder jobOrder)
	{	
		Map<Integer,TeacherAssessmentStatus> mapJSI = new TreeMap<Integer, TeacherAssessmentStatus>();		
		try{

			if(teacherDetails.size()>0)
			{
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;

				Criterion criterion = Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);

				teacherAssessmentStatusList = findByCriteria(criterion,criterion1);				

				for(TeacherAssessmentStatus  teacherAssessmentStatus : teacherAssessmentStatusList){
					mapJSI.put(teacherAssessmentStatus.getTeacherDetail().getTeacherId() , teacherAssessmentStatus);
				}
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return mapJSI;
	}

	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus>  findByTeacherAndListJob(TeacherDetail teacherDetail,List<JobOrder> lstJobOrder)
	{	

		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.in("jobOrder", lstJobOrder);	
			Criterion criterion3 = Restrictions.between("createdDateTime", sDate, eDate);
			
			lstTeacherAssessmentStatus = findByCriteria(criterion1,criterion2,criterion3);		

			System.out.println("lstTeacherAssessmentStatus>>>>"+lstTeacherAssessmentStatus.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		

		return lstTeacherAssessmentStatus;
	}	

	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTakenByTeachersByStautsAll(StatusMaster statusMaster)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();


		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			Criterion criterion = Restrictions.eq("assessmentType", 1);
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion3 = Restrictions.eq("statusMaster", statusMaster);
			teacherAssessmentStatusList = findByCriteria(criterion,criterion2,criterion3,Restrictions.isNull("jobOrder"));


		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	@Transactional(readOnly=false)
	public String epiStatus(TeacherDetail teacherDetail)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		String status="";

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			Criterion criterion = Restrictions.eq("assessmentType", 1);
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			teacherAssessmentStatusList = findByCriteria(criterion,criterion1,criterion2,Restrictions.isNull("jobOrder"));
			
			if(teacherAssessmentStatusList.size()>0){
				status=teacherAssessmentStatusList.get(0).getStatusMaster().getStatusShortName();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;

	}
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findByAllTeacherAssessmentStatus(List teacherIdList)
	{
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		try 
		{
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherIdList);
			Criterion criterion2 = Restrictions.eq("assessmentType",new Integer(1));
			Criterion criterion4 = Restrictions.between("createdDateTime", sDate, eDate);
			lstTeacherAssessmentStatus = findByCriteria(criterion1,criterion2,criterion4);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherAssessmentStatus==null || lstTeacherAssessmentStatus.size()==0)
			return null;
		else
			return lstTeacherAssessmentStatus;
	}
	
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentDone(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("comp");
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = null;
			Criterion criterionStatus = Restrictions.eq("statusMaster", statusMaster);

			if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0){
				criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
				
				if(teacherDetail==null){
					teacherAssessmentStatusList = findByCriteria(criterionStatus, criterion1,Restrictions.isNull("jobOrder"));
				}
				else{
					teacherAssessmentStatusList = findByCriteria(criterion, criterionStatus, criterion1,Restrictions.isNull("jobOrder"));
				}
			}
			else{
				criterion1 = Restrictions.eq("jobOrder", jobOrder);
				if(teacherDetail==null){
					teacherAssessmentStatusList = findByCriteria(criterionStatus, criterion1);
				}
				else{
					teacherAssessmentStatusList = findByCriteria(criterionStatus, criterion, criterion1);
				}
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentDoneWithCGStatus(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("comp");
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = null;
			Criterion criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			
			Criterion criterionCGStatusFalse = Restrictions.eq("cgUpdated", false);			
			Criterion criterionCGStatusNull = Restrictions.isNull("cgUpdated");
			
			Criterion criterionCGStatus = Restrictions.or(criterionCGStatusFalse, criterionCGStatusNull);

			if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0){
				criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
				
				if(teacherDetail==null){
					teacherAssessmentStatusList = findByCriteria(criterionStatus,criterionCGStatus, criterion1,Restrictions.isNull("jobOrder"));
				}
				else{
					teacherAssessmentStatusList = findByCriteria(criterion, criterionStatus, criterionCGStatus, criterion1,Restrictions.isNull("jobOrder"));
				}
			}
			else{
				criterion1 = Restrictions.eq("jobOrder", jobOrder);
				if(teacherDetail==null){
					teacherAssessmentStatusList = findByCriteria(criterionStatus, criterionCGStatus, criterion1);
				}
				else{
					teacherAssessmentStatusList = findByCriteria(criterionStatus, criterionCGStatus, criterion, criterion1);
				}
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentDoneByTeachers(List<TeacherDetail> teacherDetails)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		//criteria.add(Restrictions.between("auditDate", cal.getTime(), eDate));

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("comp");
			Criterion criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			Criterion criterion = Restrictions.eq("assessmentType", 1);
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			teacherAssessmentStatusList = findByCriteria(criterionStatus, criterion,criterion1,criterion2,Restrictions.isNull("jobOrder"));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findByAllTeacherAssessStatus()
	{
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		try 
		{
			Criterion criterion2 = Restrictions.eq("assessmentType",new Integer(1));
			Criterion criterion4 = Restrictions.between("createdDateTime", sDate, eDate);
			lstTeacherAssessmentStatus = findByCriteria(criterion2,criterion4);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherAssessmentStatus==null || lstTeacherAssessmentStatus.size()==0)
			return null;
		else
			return lstTeacherAssessmentStatus;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment responses of assessments.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> findAssessmentsResponses() 
	{
		Session session = getSession();

		List result = session.createCriteria(getPersistentClass())       
		.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("assessmentDetail"))
				.add(Projections.count("assessmentDetail"))           
		).list();

		Map<Integer,Integer> assessmentAttempsMap = new HashMap<Integer, Integer>();
		AssessmentDetail assessmentDetail = null;
		for (Object object : result) {
			Object rows[] = (Object[])object;
			assessmentDetail=((AssessmentDetail)rows[0]);
			assessmentAttempsMap.put(assessmentDetail.getAssessmentId(), ((Integer)rows[1]));
		}
		return assessmentAttempsMap;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findByAllTeacherAssessStatusByTeacher(List<TeacherDetail> teacherDetails)
	{
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = new ArrayList<TeacherAssessmentStatus>();
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		try 
		{
			if(teacherDetails.size()>0){
				Criterion criterion2 = Restrictions.eq("assessmentType",new Integer(1));
				Criterion criterion4 = Restrictions.between("createdDateTime", sDate, eDate);
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				lstTeacherAssessmentStatus = findByCriteria(criterion1,criterion2,criterion4);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherAssessmentStatus==null || lstTeacherAssessmentStatus.size()==0)
			return null;
		else
			return lstTeacherAssessmentStatus;
	}
	
	@Transactional(readOnly=false)
	public TeacherAssessmentStatus findLastBaseTaken(Boolean isResearchEPI)
	{
		try 
		{
				Criterion criterion1 = Restrictions.eq("assessmentType",new Integer(1));
				Criterion criterion2 = Restrictions.eq("isResearchEPI",isResearchEPI);
				Session session = getSession();
				Criteria crit = session.createCriteria(getPersistentClass());
				crit.add(criterion1);
				Criteria c=crit.createCriteria("assessmentDetail");
				c.add(criterion2);
				crit.addOrder(Order.desc("teacherAssessmentStatusId"));
				crit.setMaxResults(1);
				List result = crit.list();
				return (TeacherAssessmentStatus)result.get(0);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentVltByTeachers(List<TeacherDetail> teacherDetails)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("vlt");
			
			Criterion criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			Criterion criterion = Restrictions.eq("assessmentType", 1);
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			teacherAssessmentStatusList = findByCriteria(criterionStatus, criterion,criterion1,criterion2,Restrictions.isNull("jobOrder"));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findBaseStatusByTeachers(List<StatusMaster> statusMasterList)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("assessmentType", 1);
			Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
			Criterion criterion3 = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion4 = Restrictions.isNull("jobOrder");
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			lstTeacherDetail =  criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstTeacherDetail;

	}
	/* Sekhar */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findActiveJSIStatusByTeacher(TeacherDetail teacherDetail)
	{	
		List<TeacherAssessmentStatus> teacherAssessmentStatusList= new ArrayList<TeacherAssessmentStatus>();		
		try{

			if(teacherDetail!=null)
			{
				Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
				teacherAssessmentStatusList = findByCriteria(criterion);	
				System.out.println(teacherDetail.getTeacherId()+":::teacherAssessmentStatusList:::"+teacherAssessmentStatusList.size());
				
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return teacherAssessmentStatusList;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTakenByTeacherList(List<TeacherDetail> teacherDetails,AssessmentDetail assessmentDetail)
	{

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			Criterion criterion = Restrictions.in("teacherDetail", teacherDetails);
			Criterion criterion1 = Restrictions.eq("assessmentDetail", assessmentDetail);

			teacherAssessmentStatusList = findByCriteria(criterion,criterion1);


		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findJSITakenByAssessMentList(TeacherDetail teacherDetail,List<AssessmentDetail> assessmentDetails)
	{

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = Restrictions.in("assessmentDetail", assessmentDetails);

			teacherAssessmentStatusList = findByCriteria(criterion,criterion1);


		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentByTeacher(TeacherDetail teacherDetail,AssessmentDetail assessmentDetail)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = Restrictions.eq("assessmentDetail", assessmentDetail);
			teacherAssessmentStatusList = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	
	@Transactional(readOnly=false)
	public HashMap<Integer, TeacherAssessmentStatus> findAssessmentStatusByTeacherListForBase(List<TeacherDetail> teacherList)
	{
		Map<Integer,TeacherAssessmentStatus> mapTas = new HashMap<Integer, TeacherAssessmentStatus>();
		
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		try 
		{
			if(teacherList.size()>0){
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherList);
				Criterion criterion2 = Restrictions.eq("assessmentType",new Integer(1));
				//Criterion criterion3 = Restrictions.and(criterion1, criterion2);
				Criterion criterion4 = Restrictions.between("createdDateTime", sDate, eDate);
				lstTeacherAssessmentStatus = findByCriteria(criterion1,criterion2,criterion4);
				
				if(lstTeacherAssessmentStatus!=null && lstTeacherAssessmentStatus.size()>0)
				{
					for(TeacherDetail td:teacherList)
					{
						for(TeacherAssessmentStatus tas:lstTeacherAssessmentStatus)
						{
							if(td.getTeacherId().equals(tas.getTeacherDetail().getTeacherId()))
							{
								mapTas.put(td.getTeacherId(), tas);
							}
						}
					}
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(mapTas==null || mapTas.size()==0)
			return null;
		else
			return  (HashMap<Integer, TeacherAssessmentStatus>) mapTas;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findJSITakenByAssessMentListAndTeacherList(List<TeacherDetail> teacherList,List<AssessmentDetail> assessmentDetails)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		
		try {
			Criterion criterion = Restrictions.in("teacherDetail", teacherList);
			Criterion criterion1 = Restrictions.in("assessmentDetail", assessmentDetails);

			teacherAssessmentStatusList = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;
	}
	

	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentJsiByTeachers(List<TeacherDetail> teacherList) throws ParseException
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();		
		try {			
			Criterion criterion = Restrictions.in("teacherDetail", teacherList);
			Criterion criterion1 = Restrictions.eq("assessmentType", 2);		
			teacherAssessmentStatusList = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return teacherAssessmentStatusList;
	}
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentEpiByTeachers(List<TeacherDetail> teacherList)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		
		try {			
			Criterion criterion = Restrictions.in("teacherDetail", teacherList);
			Criterion criterion1 = Restrictions.eq("assessmentType", 1);		
			teacherAssessmentStatusList = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return teacherAssessmentStatusList;
	}
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> jsiByTeachers(TeacherDetail teacherDetail, Order sortOrderStrVal)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		List list=new ArrayList();
		list.add(1);
		list.add(2);
		try {			
			Criterion criterion = Restrictions.in("assessmentType",list);	
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);	
			//Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			teacherAssessmentStatusList = findByCriteria(sortOrderStrVal,criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherAssessmentStatusList;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> finTeacherAssessmentStatusbyTeachersAndAssessmentId(List<TeacherDetail> teacherDetails,AssessmentDetail assessmentDetail)
	{

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
            StatusMaster statusMaster= WorkThreadServlet.statusMap.get("comp");
            
            
			Criterion criterionStatus = Restrictions.eq("statusMaster", statusMaster);
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetails);
			Criterion criterion2 = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion3 = Restrictions.eq("assessmentType",new Integer(2));
			

			teacherAssessmentStatusList = findByCriteria(criterionStatus, criterion1,criterion2,criterion3);


		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<TeacherAssessmentStatus> findHireStatusByJobAndSchools(JobOrder jobOrder,List<SchoolMaster> schoolMasters) 
	{
		List<TeacherAssessmentStatus> aList= new ArrayList<TeacherAssessmentStatus>();
		if(schoolMasters.size()>0){
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);	
			Criterion criterion2 = Restrictions.eq("statusMaster", statusMaster);	
			Criterion criterion3 = Restrictions.eq("updatedByEntity",3);

			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.createCriteria("updatedBy").add(Restrictions.in("schoolId", schoolMasters));
			aList = criteria.list();
		}
		return aList;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findJSITakenByAssessMentListAndTIDS(List<TeacherDetail> teacherDetails,List<AssessmentDetail> assessmentDetails)
	{

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Criterion criterion = Restrictions.in("teacherDetail", teacherDetails);
			Criterion criterion1 = Restrictions.in("assessmentDetail", assessmentDetails);

			teacherAssessmentStatusList = findByCriteria(criterion,criterion1);


		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	/* @Author: Ashish Chaudhary
	 * @Discription: It is used to get total number of attempts of an assessment.
	 */
	@Transactional(readOnly=false)
	public int getAssessmentAttepts(AssessmentDetail assessmentDetail) {
		int size = 0;
		try {
			size = (Integer) getSession().createCriteria(getPersistentClass())
					.add(Restrictions.eq("assessmentDetail", assessmentDetail))
					.setProjection(Projections.rowCount()).uniqueResult();
					
		}catch(Exception e) {
			e.printStackTrace();
		}
		return size;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param assessmentType type of assessment like 1,2,3.
	 * @param statusMaster object of StatusMaster.
	 * @param epiFromDate from date.
	 * @param epiToDate to date.
	 * @return teacherAssessmentStatusList
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> getAssessmentCompletedDateTimeTeacherList(Integer assessmentType,StatusMaster statusMaster,Date epiFromDate,Date epiToDate)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("assessmentType", assessmentType));
			criteria.add(Restrictions.eq("statusMaster", statusMaster));
			if(epiFromDate!=null && epiToDate!=null)
				criteria.add(Restrictions.ge("assessmentCompletedDateTime",epiFromDate)).add(Restrictions.le("assessmentCompletedDateTime",epiToDate));
			else if(epiFromDate!=null && epiToDate==null)
				criteria.add(Restrictions.ge("assessmentCompletedDateTime",epiFromDate));
			else if(epiToDate!=null && epiFromDate==null)
				criteria.add(Restrictions.le("assessmentCompletedDateTime",epiToDate));
			
			teacherAssessmentStatusList = criteria.list();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;
	}
	
	@Transactional(readOnly=false)
	public Map<Integer,TeacherAssessmentStatus> findAssessmentStatusByTeachersForBase(List<TeacherDetail> teacherDetails)
	{
		Map<Integer,TeacherAssessmentStatus>  mapOfTeacherAssessmentStatus = new  HashMap<Integer, TeacherAssessmentStatus>();  
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		try 
		{
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion2 = Restrictions.eq("assessmentType",new Integer(1));
			//Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Criterion criterion4 = Restrictions.between("createdDateTime", sDate, eDate);
			lstTeacherAssessmentStatus = findByCriteria(criterion1,criterion2,criterion4);	
			if(lstTeacherAssessmentStatus!=null && lstTeacherAssessmentStatus.size()>0){
				for (TeacherAssessmentStatus teacherAssessmentStatus : lstTeacherAssessmentStatus) {
					mapOfTeacherAssessmentStatus.put(teacherAssessmentStatus.getTeacherDetail().getTeacherId(), teacherAssessmentStatus);
				}
			}
		 
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		return mapOfTeacherAssessmentStatus;	
	}

	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findJSITakenByAssessMentListAndTeacherLists(List<TeacherDetail> teacherList,List<AssessmentDetail> assessmentDetails)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			if(assessmentDetails!=null && assessmentDetails.size()>0){
				Criterion criterion = Restrictions.in("teacherDetail", teacherList);
				Criterion criterion1 = Restrictions.in("assessmentDetail", assessmentDetails);
				teacherAssessmentStatusList = findByCriteria(criterion,criterion1);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;
	}	
	

	
	@Transactional(readOnly=false)
	public TeacherAssessmentStatus findAssessmentStatusByTeacherForSP(TeacherDetail teacherDetail)
	{
		List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("assessmentType",new Integer(3));
			
			//Projections.projectionList().add(Projections.max("createdDateTime"));
			
			lstTeacherAssessmentStatus = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstTeacherAssessmentStatus==null || lstTeacherAssessmentStatus.size()==0)
			return null;
		else
			return lstTeacherAssessmentStatus.get(0);
	}
	
	
	

	/**
	 * @author Amit Chaudhary
	 * @param teacherDetail
	 * @param assessmentType
	 * @return TeacherAssessmentStatus
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findTeacherAssessmentStatusByAssessmentType(TeacherDetail teacherDetail, Integer assessmentType)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		
		try
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("assessmentType",assessmentType);
			teacherAssessmentStatusList = findByCriteria(criterion1,criterion2);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		if(teacherAssessmentStatusList==null || teacherAssessmentStatusList.size()==0)
			return null;
		else
			return teacherAssessmentStatusList;
	}
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findSmartPracticesTakenByTeachers(List<TeacherDetail> teacherDetails)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Criterion criterion = Restrictions.eq("assessmentType", 3);
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
			teacherAssessmentStatusList = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findByTeacherAssessmentTypeAndStatus(TeacherDetail teacherDetail, Integer assessmentType, List<StatusMaster> statusMasterList)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("assessmentType",assessmentType);
			Criterion criterion3 = Restrictions.in("statusMaster",statusMasterList);
			teacherAssessmentStatusList = findByCriteria(Order.asc("teacherAssessmentStatusId"),criterion1,criterion2,criterion3);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		if(teacherAssessmentStatusList==null || teacherAssessmentStatusList.size()==0)
			return null;
		else
			return teacherAssessmentStatusList;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherAssessmentStatus> getTASByTAssAndStatus(List<TeacherDetail> teacherDetailList,AssessmentDetail assessmentDetail, StatusMaster statusMaster)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetailList);
			Criterion criterion2 = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion3 = Restrictions.eq("statusMaster", statusMaster);
			
			teacherAssessmentStatusList = findByCriteria(criterion1,criterion2,criterion3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;
	}
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> getAllAssessmentByTeacher(TeacherDetail teacherDetail, Order sortOrderStrVal, UserMaster userMaster)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			String[] statuss = {"comp","vlt"};
			List<StatusMaster> statusMasterList= statusMasterDAO.findStatusByStatusByShortNames(statuss);
			List list=new ArrayList();
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			
			if(userMaster.getEntityType()==1){
			    list.add(1);
			    list.add(2);
			    list.add(3);
			    //list.add(4);
			}else if(userMaster.getEntityType()==2 || userMaster.getEntityType()==3){
				list.add(1);
				list.add(2);
				//list.add(4);	
			}else if(userMaster.getEntityType()==5 || userMaster.getEntityType()==6){
				list.add(1);
				list.add(3);	
			}
			
			Criterion criterion = Restrictions.in("assessmentType",list);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.in("statusMaster", statusMasterList);
			
			if(userMaster.getEntityType()==1){
				teacherAssessmentStatusList = findByCriteria(sortOrderStrVal,criterion,criterion1,criterion2);
			}
			else {
			if(userMaster.getEntityType()==2 || userMaster.getEntityType()==3)
			    criteria.add(criterion).add(criterion1).add(criterion2).addOrder(sortOrderStrVal).createCriteria("assessmentDetail").add(Restrictions.or(Restrictions.in("assessmentType", new Object[]{1,3}), Restrictions.conjunction().add(Restrictions.eq("assessmentType", 2)).add(Restrictions.eq("districtMaster", userMaster.getDistrictId()))));
			else if(userMaster.getEntityType()==5)
				criteria.add(criterion).add(criterion1).add(criterion2).addOrder(sortOrderStrVal).createCriteria("assessmentDetail").add(Restrictions.or(Restrictions.in("assessmentType", new Object[]{1,3}), Restrictions.conjunction().add(Restrictions.eq("assessmentType", 2)).add(Restrictions.eq("headQuarterMaster", userMaster.getHeadQuarterMaster()))));
			else if(userMaster.getEntityType()==6)
				criteria.add(criterion).add(criterion1).add(criterion2).addOrder(sortOrderStrVal).createCriteria("assessmentDetail").add(Restrictions.or(Restrictions.in("assessmentType", new Object[]{1,3}), Restrictions.conjunction().add(Restrictions.eq("assessmentType", 2)).add(Restrictions.eq("branchMaster", userMaster.getBranchMaster()))));
			
			teacherAssessmentStatusList = criteria.list();
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherAssessmentStatusList;
	}
	
	/**
	 * It is used to find the {@link List} of <code>TeacherAssessmentStatus</code> by <code>TeacherDetail</code> and <code>teacherAssessmentStatusId</code>.
	 * @author Amit Chaudhary
	 * @param teacherDetail
	 * @param teacherAssessmentStatusId
	 * @return {@link List} of <code>TeacherAssessmentStatus</code>
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findTASByTeacherAndTAS(TeacherDetail teacherDetail, Integer teacherAssessmentStatusId)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("teacherAssessmentStatusId", teacherAssessmentStatusId);
			
			teacherAssessmentStatusList = findByCriteria(criterion1,criterion2);
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;
	}

	// Optimization for Candidate Pool search replacement of  getAssessmentCompletedDateTimeTeacherList
	
	
	@Transactional(readOnly=false)
	public List<Integer> getAssessmentCompletedDateTimeTeacherList_Op(Integer assessmentType,StatusMaster statusMaster,Date epiFromDate,Date epiToDate , List<Integer> teachersId)
	{
		List<Integer> teacherAssessmentStatusList = new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(Restrictions.eq("assessmentType", assessmentType));
				criteria.add(Restrictions.eq("statusMaster", statusMaster));
				if(epiFromDate!=null && epiToDate!=null)
					criteria.add(Restrictions.ge("assessmentCompletedDateTime",epiFromDate)).add(Restrictions.le("assessmentCompletedDateTime",epiToDate));
				else if(epiFromDate!=null && epiToDate==null)
					criteria.add(Restrictions.ge("assessmentCompletedDateTime",epiFromDate));
				else if(epiToDate!=null && epiFromDate==null)
					criteria.add(Restrictions.le("assessmentCompletedDateTime",epiToDate));
				
				
				criteria.add(Restrictions.in("teacherDetail.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				teacherAssessmentStatusList = criteria.list();
			}
			catch (Exception e){
				e.printStackTrace();
			}
		return teacherAssessmentStatusList;
	}
//Optimization for Candidate pool
	
	@Transactional(readOnly=false)
	public List<Integer> findBaseStatusByTeachers_Op(List<StatusMaster> statusMasterList, List<Integer> teachersId)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		List<Integer> lstTeacherDetail= new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
			try {
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.eq("assessmentType", 1);
				Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
				Criterion criterion3 = Restrictions.between("createdDateTime", sDate, eDate);
				Criterion criterion4 = Restrictions.isNull("jobOrder");
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.add(Restrictions.in("teacherDetail.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				lstTeacherDetail =  criteria.list();
			} catch (Exception e) {
				e.printStackTrace();
			}
		return lstTeacherDetail;

	}


	//brajesh
	@Transactional(readOnly=false)
	public List<TeacherDetail> findBaseStatusByTeachersList(List<StatusMaster> statusMasterList)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			//Criterion criterion1 = Restrictions.eq("assessmentType", 1);
			Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
			Criterion criterion3 = Restrictions.between("createdDateTime", sDate, eDate);
			//Criterion criterion4 = Restrictions.isNull("jobOrder");
			//criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			//criteria.add(criterion4);
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			lstTeacherDetail =  criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstTeacherDetail;

	}

	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTakenByTeachersCGOp(List<TeacherDetail> teacherDetails)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();

		//criteria.add(Restrictions.between("auditDate", cal.getTime(), eDate));

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();;
		try {
			Criterion criterion = Restrictions.eq("assessmentType", 1);
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			
			Session session 		= 	getSession();   		
			
			Criteria criteria = this.getSession().createCriteria(this.getPersistentClass());
			criteria.createAlias("teacherDetail", "td").createAlias("statusMaster", "statusmaster",Criteria.LEFT_JOIN);
			criteria.setProjection(Projections.distinct(Projections.projectionList()
			           .add( Projections.property("td.teacherId"), "teacherId" )
			           .add( Projections.property("statusmaster.statusId"), "statusId" )
			           .add( Projections.property("statusmaster.statusShortName"), "statusShortName" )
			           .add(Projections.property("teacherAssessmentStatusId"))
			           )			           
					);
			criteria.add(criterion).add(criterion1).add(criterion2).add(Restrictions.isNull("jobOrder"));
			List<String []> teacherAssessmentStatusLst=new ArrayList<String []>();
			teacherAssessmentStatusLst = criteria.list();
			//teacherAssessmentStatusList = findByCriteria(criterion,criterion1,criterion2,Restrictions.isNull("jobOrder"));
			
			for (Iterator it = teacherAssessmentStatusLst.iterator(); it.hasNext();)
			{
				
				Object[] row = (Object[]) it.next();    				
				if(row[0]!=null){
					TeacherAssessmentStatus teacherAssessmentStatus = new TeacherAssessmentStatus();
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(Integer.parseInt(row[0].toString()));
					teacherAssessmentStatus.setTeacherDetail(teacherDetail);
					if(row[1]!=null){
						StatusMaster stauMaster = new StatusMaster();
						stauMaster.setStatusId(Integer.parseInt(row[1].toString()));
						stauMaster.setStatusShortName(row[2].toString());
						teacherAssessmentStatus.setStatusMaster(stauMaster);
					}
					teacherAssessmentStatus.setTeacherAssessmentStatusId(Integer.parseInt(row[3].toString()));
					teacherAssessmentStatusList.add(teacherAssessmentStatus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}

	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findAssessmentTakenByTeacherListOp(List<TeacherDetail> teacherDetails,AssessmentDetail assessmentDetail)
	{

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Criterion criterion = Restrictions.in("teacherDetail", teacherDetails);
			Criterion criterion1 = Restrictions.eq("assessmentDetail", assessmentDetail);

			Session session 		= 	getSession();
			Criteria criteria = this.getSession().createCriteria(this.getPersistentClass());
			criteria.createAlias("teacherDetail", "td").createAlias("statusMaster", "statusmaster",Criteria.LEFT_JOIN);
			criteria.setProjection(Projections.distinct(Projections.projectionList()
			           .add( Projections.property("td.teacherId"), "teacherId" )
			           .add( Projections.property("statusmaster.statusId"), "statusId" )
			           .add( Projections.property("statusmaster.statusShortName"), "statusShortName" )
			           .add(Projections.property("teacherAssessmentStatusId"))
			           )			           
					);
			criteria.add(criterion1).add(criterion);
			List<String []> teacherAssessmentStatusLst=new ArrayList<String []>();
			teacherAssessmentStatusLst = criteria.list();
			//teacherAssessmentStatusList = findByCriteria(criterion,criterion1,criterion2,Restrictions.isNull("jobOrder"));
			
			for (Iterator it = teacherAssessmentStatusLst.iterator(); it.hasNext();)
			{
				
				Object[] row = (Object[]) it.next();    				
				if(row[0]!=null){
					TeacherAssessmentStatus teacherAssessmentStatus = new TeacherAssessmentStatus();
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(Integer.parseInt(row[0].toString()));
					teacherAssessmentStatus.setTeacherDetail(teacherDetail);
					if(row[1]!=null){
						StatusMaster stauMaster = new StatusMaster();
						stauMaster.setStatusId(Integer.parseInt(row[1].toString()));
						stauMaster.setStatusShortName(row[2].toString());
						teacherAssessmentStatus.setStatusMaster(stauMaster);
					}
					teacherAssessmentStatus.setTeacherAssessmentStatusId(Integer.parseInt(row[3].toString()));
					teacherAssessmentStatusList.add(teacherAssessmentStatus);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	
	
	/**
	 * 
	 * @param teacherDetails
	 * @return
	 * @implemented for kelly onboarding dashboard
	 */
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findSmartPracticesTakenByTeachers_Op(List<TeacherDetail> teacherDetails)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("assessmentType", 3);
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
			criteria.add(criterion);
			criteria.add(criterion1);
			criteria.createAlias("statusMaster", "st", Criteria.LEFT_JOIN)
			.setProjection(Projections.projectionList()
					.add(Projections.property("teacherDetail.teacherId"))
					.add(Projections.property("pass"))
					.add(Projections.property("st.statusShortName")));
			
			List<String []> teacherAssessmentStatusLst=new ArrayList<String []>();
			teacherAssessmentStatusLst = criteria.list();
			
			for(Iterator it = teacherAssessmentStatusLst.iterator(); it.hasNext();){
				Object[] row = (Object[]) it.next();
				TeacherAssessmentStatus teacherAssessmentStatus = new TeacherAssessmentStatus();
				if(row[0]!=null){
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(Integer.parseInt(row[0].toString()));
					teacherAssessmentStatus.setTeacherDetail(teacherDetail);
				}
				
				if(row[1]!=null){
					teacherAssessmentStatus.setPass(row[1].toString());
				}
				
				if(row[2]!=null){
					StatusMaster statusMaster = new StatusMaster();
					statusMaster.setStatusShortName(row[2].toString());
					teacherAssessmentStatus.setStatusMaster(statusMaster);
				}
				
				teacherAssessmentStatusList.add(teacherAssessmentStatus);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAssessmentStatus> findSmartPracticesTakenByTeachers_Op(String teacherDetails)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass(),"tas");
			Criterion criterion = Restrictions.eq("assessmentType", 3);
			//criteria.add(Property.forName("tas.teacherDetail.teacherId").in(teacherDetails));
			criteria.add(Restrictions.sqlRestriction("this_.teacherId in("+teacherDetails+")"));
			criteria.add(criterion);
			criteria.createAlias("statusMaster", "st", Criteria.LEFT_JOIN)
			.setProjection(Projections.projectionList()
					.add(Projections.property("teacherDetail.teacherId"))
					.add(Projections.property("pass"))
					.add(Projections.property("st.statusShortName")));
			
			List<String []> teacherAssessmentStatusLst=new ArrayList<String []>();
			teacherAssessmentStatusLst = criteria.list();
			
			for(Iterator it = teacherAssessmentStatusLst.iterator(); it.hasNext();){
				Object[] row = (Object[]) it.next();
				TeacherAssessmentStatus teacherAssessmentStatus = new TeacherAssessmentStatus();
				if(row[0]!=null){
					TeacherDetail teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(Integer.parseInt(row[0].toString()));
					teacherAssessmentStatus.setTeacherDetail(teacherDetail);
				}
				
				if(row[1]!=null){
					teacherAssessmentStatus.setPass(row[1].toString());
				}
				
				if(row[2]!=null){
					StatusMaster statusMaster = new StatusMaster();
					statusMaster.setStatusShortName(row[2].toString());
					teacherAssessmentStatus.setStatusMaster(statusMaster);
				}
				teacherAssessmentStatusList.add(teacherAssessmentStatus);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherAssessmentStatus> getTeacherAssessmentStatus(AssessmentDetail assessmentDetail,TeacherDetail teacherDetail)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		try {
			Criterion criterion = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Order order = Order.desc("teacherAssessmentdetail");
			teacherAssessmentStatusList = findByCriteria(order,criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherAssessmentStatus> findByTeacherAssessmentdetails(List<TeacherAssessmentdetail> teacherAssessmentdetailList, StatusMaster statusMaster)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try{
			Criterion criterion1 = Restrictions.in("teacherAssessmentdetail",teacherAssessmentdetailList);
			Criterion criterion2 = Restrictions.eq("statusMaster", statusMaster);
			teacherAssessmentStatusList = findByCriteria(criterion1,criterion2);		
		}
		catch (Exception e){
			e.printStackTrace();
		}		
		return teacherAssessmentStatusList;
	}
	// Optimized For personal planning indrajeet
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
	public List<Object> findAssessmentStatusByTeacher_Op(TeacherDetail teacherDetail)
	{
		List <Object> lst=new ArrayList<Object>();

		try 
		{
			Criteria crit = getSession().createCriteria(getPersistentClass()).createAlias("jobOrder", "jo").createAlias("statusMaster", "sm");
			//Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);	
			crit.add(Restrictions.eq("teacherDetail", teacherDetail));
			//crit.setProjection(Projections.property("jo.jobId"));
			//crit.setProjection(Projections.property("sm.statusShortName"));
			//crit.add(criterion1);
			crit.setProjection(Projections.projectionList()
					.add(Projections.property("jo.jobId"))   //0
					.add(Projections.property("sm.statusShortName")));
			lst = (List<Object>) crit.list();

			System.out.println("size of list ::::::::"+lst.size());
			
			/*for(Object ta:lst)
			{
			  try{Object  row[]= (Object[])ta;
			 // System.out.println("JobId....."+row[0]+"StatusshortName:   "+row[1]);
			  }catch(Exception e){e.printStackTrace();}
			}*/
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lst;
	}
	
 
	
	
	
	//Added by kumar avinash	
	@Transactional(readOnly=false)
	 public TeacherAssessmentStatus findAssessmentStatusByTeacherForBasety_Op(TeacherDetail teacherDetail)
	 {
		 TeacherAssessmentStatus teacherAssessmentStatus = new TeacherAssessmentStatus();
		Calendar cal = Calendar.getInstance();
 		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
	  try{
	     List<Object[]> objList=null;
	      Query  query=null;
	   if(teacherDetail!=null){
	     Session session = getSession();
	      String hql= "SELECT  tss.teacherAssessmentStatusId,sm.statusShortName,tss.statusId  FROM  teacherassessmentstatus tss "
	         +   "left join statusmaster sm on sm.statusId=tss.statusId "
	         +   "left join teacherdetail td on td.teacherId= tss.teacherId "
	         + " WHERE   tss.teacherId=? and tss.assessmentType=? and tss.createdDateTime between ? and ? ";
	      query=session.createSQLQuery(hql);
	      query.setParameter(0, teacherDetail.getTeacherId());
	      query.setParameter(1,new Integer(1));
 	      query.setParameter(2, sDate);
	      query.setParameter(3, eDate);
 	       objList = query.list();
 	    if(objList.size()>0){
	   //  for (int j = 0; j < objList.size(); j++) {
	      Object[] objArr2 = objList.get(0);
 	      teacherAssessmentStatus.setTeacherAssessmentStatusId(objArr2[0] == null ? null : Integer.parseInt(objArr2[0].toString()));
	      StatusMaster statusMaster=null;
	      statusMaster=new  StatusMaster();
	      String statusShortName   =objArr2[1] == null ? "NA" :  objArr2[1].toString();
	      statusMaster.setStatusShortName(statusShortName);
	      int statusId=objArr2[2] == null ? null : Integer.parseInt(objArr2[2].toString());
	      statusMaster.setStatusId(statusId);
	      teacherAssessmentStatus.setStatusMaster(statusMaster);
 			  // lstJobOrder.add(jobOrder);
	              }
	             }
	  if(objList.size()==0)
	     	return null;
	     //      }
	         }
	   catch(Exception e){
	    e.printStackTrace();
	  }
	   return teacherAssessmentStatus;
	  }
	
 
	
	
	
	
//Added by kumar avinash	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
	public  List<TeacherAssessmentStatus>  findAssessmentStatusByTeacher_Opp(TeacherDetail teacherDetail)
	{
		List<TeacherAssessmentStatus>  lstTeacherAssessmentStatus=new ArrayList<TeacherAssessmentStatus>();
		  List<Object[]> objList=null;
		try 
		{
		      Query  query=null;
			   if(teacherDetail!=null){
			     Session session = getSession();
			      String hql= "SELECT  tss.teacherAssessmentStatusId,sm.statusShortName,tss.jobId   FROM  teacherassessmentstatus tss "
			         +   "left join statusmaster sm on sm.statusId=tss.statusId "
			         +   "left join teacherdetail td on td.teacherId= tss.teacherId "
			         +   "left join joborder jo on jo.jobId= tss.jobId "
		 	         + " WHERE   tss.teacherId=?  ";
			      query=session.createSQLQuery(hql);
			      query.setParameter(0, teacherDetail.getTeacherId());
			     
			      objList = query.list();
		 	    if(objList.size()>0){
		 	    	TeacherAssessmentStatus teacherAssessmentStatus=null;
			    for (int j = 0; j < objList.size(); j++) {
			    	teacherAssessmentStatus=new TeacherAssessmentStatus();
			      Object[] objArr2 = objList.get(j);
		 	      teacherAssessmentStatus.setTeacherAssessmentStatusId(objArr2[0] == null ? null : Integer.parseInt(objArr2[0].toString()));
			      StatusMaster statusMaster=null;
			      statusMaster=new  StatusMaster();
			      String statusShortName   =objArr2[1] == null ? "NA" :  objArr2[1].toString();
			      statusMaster.setStatusShortName(statusShortName);
			      teacherAssessmentStatus.setStatusMaster(statusMaster);
			      JobOrder jobOrder=new JobOrder();
			      jobOrder.setJobId(objArr2[2] == null ? null : Integer.parseInt(objArr2[2].toString()));
			       
			      // JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();
		         	
		         	//jobCategoryMaster.setBaseStatus(Boolean.parseBoolean(objArr2[3].toString()));
		         	//jobOrder.setJobCategoryMaster(jobCategoryMaster);
			       
			       teacherAssessmentStatus.setJobOrder(jobOrder)  ;
			       lstTeacherAssessmentStatus.add(teacherAssessmentStatus);
			              }
			             }
			           }			
 		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherAssessmentStatus;
	}
	
	@Transactional(readOnly=false)	
 	public List<TeacherAssessmentStatus>  findJSITakenByAssessMentList_Opp(TeacherDetail teacherDetail,List<AssessmentDetail> assessmentDetails) throws Exception 
 	{	  
		List<Object[]> objList=null;
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
 	
 	String assessmentDetailIdTemp="";
 
 	int p=0;
		for(AssessmentDetail assessmentDetail: assessmentDetails){
			if(p==0){
				assessmentDetailIdTemp=assessmentDetail.getAssessmentId()+"";
			}else{
				assessmentDetailIdTemp+=","+assessmentDetail.getAssessmentId()+"";
			}
 			p++; 
		}	
    		 try{
 		     Query  query=null;
 	 	     Session session = getSession();
 	  	    if(teacherDetail!=null && assessmentDetails.size()>0){ 
 	     	String   hql = "SELECT   ta.assessmentId,ta.teacherAssessmentStatusId FROM  teacherassessmentstatus ta "
  		       +   "left join assessmentdetail ad on ad.assessmentId= ta.assessmentId "
  		     +   "left join teacherdetail td on td.teacherId= ta.teacherId "
  		         + " WHERE   ta.teacherId='"+teacherDetail.getTeacherId()+"' and ta.assessmentId in('"+assessmentDetailIdTemp+"') ";
 		        query=session.createSQLQuery(hql);
 		        objList  = query.list(); 
 		       TeacherAssessmentStatus teacherAssessmentStatus=null;
 		        if(objList.size()>0){
 		           for (int j = 0; j < objList.size(); j++) {
 		       	    Object[] objArr2 = objList.get(j);
 	   	       	teacherAssessmentStatus = new TeacherAssessmentStatus();
 	 	         AssessmentDetail assessmentDetail=new AssessmentDetail();
 	 	        assessmentDetail.setAssessmentId(objArr2[0] == null ? null : Integer.parseInt(objArr2[0].toString()));
 		          teacherAssessmentStatus.setAssessmentDetail(assessmentDetail);
 		         teacherAssessmentStatusList.add(teacherAssessmentStatus);
 		              }
 		            }
 		          }
 		        }
 		   catch(Exception e){
 		    e.printStackTrace();
 		  }
 		   return teacherAssessmentStatusList;
 	}		
 
	
}
