package tm.dao.master;

import tm.bean.master.PortfolioItem;
import tm.dao.generic.GenericHibernateDAO;

public class PortfolioitemDAO extends  GenericHibernateDAO<PortfolioItem, Integer>{
	public PortfolioitemDAO(){
		super(PortfolioItem.class);
	}
}
	