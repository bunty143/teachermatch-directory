package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.CountryMaster;
import tm.bean.master.DegreeMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DegreeMasterDAO extends GenericHibernateDAO<DegreeMaster, Long> 
{
	public DegreeMasterDAO() 
	{
		super(DegreeMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<DegreeMaster> getDegreeMasterAutocompleteList(String degreeName)
	{
		Session session = getSession();
		String sql = null;
		Query query=null;
		List<DegreeMaster> result = null;
		try 
		{
			sql = "from DegreeMaster where degreeName like '"+degreeName+"%' union from DegreeMaster where degreeName like '% "+degreeName+"%'";
			
			query = session.createQuery(sql);
			
			result = query.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		
		return result;
	}
	
	@Transactional(readOnly=true)
	public DegreeMaster getDMByShortCode(String sDegreeShortName)
	{
		DegreeMaster degreeMaster=null;
		Criterion criterion1 = Restrictions.eq("degreeShortName",sDegreeShortName);
		List<DegreeMaster> degreeMasters=new ArrayList<DegreeMaster>();
		degreeMasters=findByCriteria(criterion1);
		if(degreeMasters!=null && degreeMasters.size() >0)
			degreeMaster=degreeMasters.get(0);
		return degreeMaster;
	}
}
