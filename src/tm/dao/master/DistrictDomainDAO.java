package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DistrictDomain;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SecondaryStatusMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictDomainDAO  extends  GenericHibernateDAO<DistrictDomain, Integer> {
	public DistrictDomainDAO(){
		super(DistrictDomain.class);
	}
	
	@Transactional(readOnly=false)
	public  List<DistrictDomain> findDistrictDomains(Order order,int startPos,int limit,Criterion...criterions)
	{
		List<DistrictDomain> districtDomains=null;
		
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for(Criterion c:criterions){
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			if(order != null)
				criteria.addOrder(order);
			
			districtDomains=criteria.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtDomains;
	}
	
	@Transactional(readOnly=false)
	public  List<DistrictDomain> findDomainsByDistrict(DistrictMaster districtMaster )
	{
		List<DistrictDomain> districtDomains=new ArrayList<DistrictDomain>();
		
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1=Restrictions.eq("districtMaster",districtMaster);	
			Criterion criterion2=Restrictions.eq("status","A");
			criteria.add(criterion1);
			criteria.add(criterion2);
			districtDomains=criteria.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtDomains;
	}
	
	public boolean checkDuplicateDistrictDomain(Integer districtDomainId,DistrictMaster districtMaster,String domainName){
		boolean r=false;
		try{
			List result=null;
			Criterion crn=Restrictions.eq("districtMaster", districtMaster );
			if(districtDomainId!=null)
			{
				Criterion c1=Restrictions.eq("domainName", domainName);
				Criterion c2=Restrictions.not(Restrictions.in("districtDomainId",new Integer[]{districtDomainId}));
				result=findByCriteria(crn,c1,c2);
			}
			else
			{
				Criterion c1=Restrictions.eq("domainName", domainName); 
				result=findByCriteria(crn,c1);
			}
			if(result.size()>0)
				r=true;
			else
				r=false;
		}catch (Exception e) {
			e.printStackTrace();
			
		}
		return r;
	}
}
