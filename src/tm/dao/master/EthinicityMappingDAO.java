package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DistrictMaster;
import tm.bean.master.EthinicityMapping;
import tm.dao.generic.GenericHibernateDAO;

public class EthinicityMappingDAO extends GenericHibernateDAO<EthinicityMapping,Integer> {
	public EthinicityMappingDAO() {
		super(EthinicityMapping.class);
	}
	
	@Transactional(readOnly=true)
	public EthinicityMapping getMappedEthinicity(String name,DistrictMaster districtMaster)
	{
		List<EthinicityMapping> lstEthinicityMapping = new ArrayList<EthinicityMapping>();
		EthinicityMapping ethinicityMapping=null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("mappingName",name);
			lstEthinicityMapping = findByCriteria(criterion1,criterion2);
			if(lstEthinicityMapping!=null && lstEthinicityMapping.size() >0)
				ethinicityMapping=lstEthinicityMapping.get(0);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return ethinicityMapping;
		}
	}
	
}
