package tm.dao.master;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.FieldOfStudyMaster;
import tm.bean.master.UniversityMaster;
import tm.dao.generic.GenericHibernateDAO;

public class FieldOfStudyMasterDAO extends GenericHibernateDAO<FieldOfStudyMaster, Long> 
{
	public FieldOfStudyMasterDAO() 
	{
		super(FieldOfStudyMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<FieldOfStudyMaster> findFieldOfStuddyByName(String fieldName)
	{
		List<FieldOfStudyMaster> lstFieldOfStudyMaster = null;
		try 
		{	
			Criterion criterion1 = Restrictions.eq("fieldName", fieldName);			
			lstFieldOfStudyMaster = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstFieldOfStudyMaster;		
	}
	
}
