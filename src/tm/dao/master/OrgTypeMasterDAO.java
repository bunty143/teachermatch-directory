package tm.dao.master;

import tm.bean.master.OrgTypeMaster;
import tm.dao.generic.GenericHibernateDAO;

public class OrgTypeMasterDAO extends GenericHibernateDAO<OrgTypeMaster, Integer> 
{
	public OrgTypeMasterDAO() 
	{
		super(OrgTypeMaster.class);
	}
}
