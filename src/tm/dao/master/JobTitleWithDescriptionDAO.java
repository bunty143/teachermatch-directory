package tm.dao.master;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.TeacherDetail;
import tm.bean.master.JobTitleWithDescription;
import tm.dao.generic.GenericHibernateDAO;

public class JobTitleWithDescriptionDAO extends GenericHibernateDAO<JobTitleWithDescription, Integer> 
{
	public JobTitleWithDescriptionDAO() 
	{
		super(JobTitleWithDescription.class);
	}
	@Transactional(readOnly=false)
	public List<JobTitleWithDescription> findDescriptionByTitle(Integer job)
	{
		List<JobTitleWithDescription> jobTitleWithDescription= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("jobTitleWithDescritionId",job);
			jobTitleWithDescription = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobTitleWithDescription;
	}
	
}