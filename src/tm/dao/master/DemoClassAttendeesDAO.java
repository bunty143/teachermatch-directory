package tm.dao.master;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.python.core.exceptions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DemoClassAttendees;
import tm.bean.master.DemoClassNotesDetails;
import tm.bean.master.DemoClassSchedule;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DemoClassAttendeesDAO extends
		GenericHibernateDAO<DemoClassAttendees, java.lang.Integer> {
	public DemoClassAttendeesDAO() {
		super(DemoClassAttendees.class);
	}
	
	@Transactional(readOnly=true)
	public List<DemoClassAttendees> getDemoClassAttendees(DemoClassSchedule demoClassSchedule){
		List<DemoClassAttendees> list=null;
		try{
			Criteria  criteria=getSession().createCriteria(getPersistentClass());
			list=criteria.add(Restrictions.eq("demoId", demoClassSchedule)).list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	@Transactional(readOnly=true)
	public List<DemoClassSchedule> getMyDemoInviteeSchedule(UserMaster userMaster,Criterion... criterion){
		List<DemoClassSchedule> myInvitelist=new ArrayList<DemoClassSchedule>();
		try{
			Criteria criteria=getSession().createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			criteria.add(Restrictions.eq("inviteeId",userMaster));
			criteria.setProjection(Projections.distinct(Projections.property("demoId")));
			
			myInvitelist=criteria.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			return myInvitelist;
		}
		
	}
	
	
}
