package tm.dao.master;

import tm.bean.master.FacebookConfig;
import tm.dao.generic.GenericHibernateDAO;

/* @Author: Vishwanath Kumar
 * @Discription: 
 */
public class FacebookConfigDAO extends GenericHibernateDAO<FacebookConfig, Integer> 
{
	public FacebookConfigDAO() {
		super(FacebookConfig.class);
	}
}
