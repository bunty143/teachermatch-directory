package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.ReferenceCheckFitScore;
import tm.dao.generic.GenericHibernateDAO;

public class ReferenceCheckFitScoreDAO extends GenericHibernateDAO<ReferenceCheckFitScore, Integer> 
{
	public ReferenceCheckFitScoreDAO() 
	{
		super(ReferenceCheckFitScore.class);
	}
	
	@Transactional(readOnly=false)
	public List<ReferenceCheckFitScore> getRecordsByDistAndTeacherId(Integer headQuarterId , DistrictMaster districtMaster, TeacherDetail teacherDetail){
		
		List<ReferenceCheckFitScore> allRecord = new ArrayList<ReferenceCheckFitScore>();
		try{
			Criterion criterion1=null;
			
			if(districtMaster!=null)
			criterion1	=	Restrictions.eq("districtMaster", districtMaster);
			else if(headQuarterId!=null && headQuarterId !=0)
				criterion1 = Restrictions.eq("headQuarterId", headQuarterId);
			Criterion criterion2	=	Restrictions.eq("teacherDetail", teacherDetail);
			
			if(criterion1!=null)
			allRecord				=	findByCriteria(criterion1,criterion2);
			
		} catch (Exception exception){
			exception.printStackTrace();
		}
		return allRecord;
	}
	
	@Transactional(readOnly=false)
	public List<ReferenceCheckFitScore> getRecordsByDistId(DistrictMaster districtMaster){
		
		List<ReferenceCheckFitScore> allRecord = new ArrayList<ReferenceCheckFitScore>();
		try{
			Criterion criterion1	=	Restrictions.eq("districtMaster", districtMaster);
			allRecord				=	findByCriteria(criterion1);
			
		} catch (Exception exception){
			exception.printStackTrace();
		}
		return allRecord;
	}
	@Transactional(readOnly=false)
	public List<ReferenceCheckFitScore> getRecordsByDistrictAndTeacherList(DistrictMaster districtMaster,List<TeacherDetail> teacherDetails){
		
		List<ReferenceCheckFitScore> allRecord = new ArrayList<ReferenceCheckFitScore>();
		try{
			Criterion criterion1	=	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2	=	Restrictions.in("teacherDetail", teacherDetails);
			allRecord				=	findByCriteria(criterion1,criterion2);
			
		} catch (Exception exception){
			exception.printStackTrace();
		}
		return allRecord;
	}
}
