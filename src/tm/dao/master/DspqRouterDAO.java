package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DistrictMaster;
import tm.bean.master.DspqFieldMaster;
import tm.bean.master.DspqPortfolioName;
import tm.bean.master.DspqRouter;
import tm.bean.master.DspqSectionMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DspqRouterDAO extends GenericHibernateDAO<DspqRouter, Integer> 
{
	public DspqRouterDAO() 
	{
		super(DspqRouter.class);
	}	
	
	@Transactional(readOnly=false)
	public List<DspqRouter> getDspqFieldListByDspqPortfolioNameId(DspqPortfolioName dspqPortfolioName)
	{
		List<DspqRouter> dspqRouterList = new ArrayList<DspqRouter>();	
		Criterion criterion=null;
		try
		{
			System.out.println("dspqPortfolioName  "+dspqPortfolioName.getDspqPortfolioNameId());
			criterion = Restrictions.eq("dspqPortfolioName",dspqPortfolioName);			
			dspqRouterList=findByCriteria(Order.asc("dspqRouterId"),criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return dspqRouterList;
	}
	
	
	@Transactional(readOnly=false)
	public List<DspqRouter> getRounterByPortfolioNameAndCT(DspqPortfolioName dspqPortfolioName,String applicantType)
	{
		List<DspqRouter> dspqRouterList = new ArrayList<DspqRouter>();	
		Criterion criterion1=null;
		Criterion criterion2=null;
		Criterion criterion3=null;
		
		try
		{
			criterion1=Restrictions.eq("dspqPortfolioName",dspqPortfolioName);
			criterion2=Restrictions.eq("applicantType",applicantType);
			criterion3=Restrictions.eq("status","A");
			dspqRouterList=findByCriteria(criterion1,criterion2,criterion3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqRouterList;
	}
	
	@Transactional(readOnly=false)
	public List<DspqRouter> getSectionByPortfolioNameAndCT(DspqPortfolioName dspqPortfolioName,String applicantType)
	{
		List<DspqRouter> dspqRouterList = new ArrayList<DspqRouter>();	
		Criterion criterion1=null;
		Criterion criterion2=null;
		Criterion criterion3=null;
		Criterion criterion4=null;
		Criterion criterion5=null;
		try
		{
			criterion1=Restrictions.eq("dspqPortfolioName",dspqPortfolioName);
			criterion2=Restrictions.eq("applicantType",applicantType);
			criterion3=Restrictions.isNull("dspqFieldMaster");
			criterion4=Restrictions.isNotNull("dspqSectionMaster");
			criterion5=Restrictions.eq("status","A");
			dspqRouterList=findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqRouterList;
	}
	
	
	@Transactional(readOnly=false)
	public List<DspqRouter> getControlsByPortfolioNameAndCTAndField(DspqPortfolioName dspqPortfolioName,String applicantType,List<DspqFieldMaster> lstDspqFieldMaster,List<DspqSectionMaster> dspqSectionMasters)
	{
		List<DspqRouter> dspqRouterList = new ArrayList<DspqRouter>();	
		Criterion criterion1=null;
		Criterion criterion2=null;
		Criterion criterion3=null;
		Criterion criterion4=null;
		Criterion criterion5=null;
		try
		{
			criterion1=Restrictions.eq("dspqPortfolioName",dspqPortfolioName);
			criterion3=Restrictions.eq("status","A");
			criterion4=Restrictions.in("dspqFieldMaster",lstDspqFieldMaster);
			criterion5=Restrictions.in("dspqSectionMaster",dspqSectionMasters);
			LogicalExpression orExp = Restrictions.or(criterion4, criterion5);
			if(applicantType.equalsIgnoreCase("")){
				criterion2=Restrictions.eq("applicantType","E");
			}else{
				criterion2=Restrictions.eq("applicantType",applicantType);
			}
			dspqRouterList=findByCriteria(criterion1,criterion2,criterion3,orExp);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqRouterList;
	}
	
	
	@Transactional(readOnly=false)
	public List<DspqRouter> getSectionByPortfolioNameAndCTAndSec(DspqPortfolioName dspqPortfolioName,String applicantType,DspqSectionMaster dspqSectionMaster)
	{
		List<DspqRouter> dspqRouterList = new ArrayList<DspqRouter>();	
		Criterion criterion1=null;
		Criterion criterion2=null;
		Criterion criterion3=null;
		Criterion criterion4=null;
		Criterion criterion5=null;
		try
		{
			criterion1=Restrictions.eq("dspqPortfolioName",dspqPortfolioName);
			criterion2=Restrictions.eq("applicantType",applicantType);
			criterion3=Restrictions.isNull("dspqFieldMaster");
			criterion4=Restrictions.eq("dspqSectionMaster",dspqSectionMaster);
			criterion5=Restrictions.eq("status","A");
			dspqRouterList=findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqRouterList;
	}
	
	@Transactional(readOnly=false)
	public List<DspqRouter> getSectionByPortfolioNameAndCTAndLstSec(DspqPortfolioName dspqPortfolioName,String applicantType,List<DspqSectionMaster> lstdspqSectionMaster)
	{
		List<DspqRouter> dspqRouterList = new ArrayList<DspqRouter>();	
		Criterion criterion1=null;
		Criterion criterion2=null;
		Criterion criterion3=null;
		Criterion criterion4=null;
		Criterion criterion5=null;
		try
		{
			criterion1=Restrictions.eq("dspqPortfolioName",dspqPortfolioName);
			criterion2=Restrictions.eq("applicantType",applicantType);
			criterion3=Restrictions.isNull("dspqFieldMaster");
			criterion4=Restrictions.in("dspqSectionMaster",lstdspqSectionMaster);
			criterion5=Restrictions.eq("status","A");
			dspqRouterList=findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqRouterList;
	}
	
	@Transactional(readOnly=false)
	public List<DspqRouter> getDspqFieldListById(List<DspqPortfolioName> portfolioId)
	{
		List<DspqRouter> dspqRouterList = new ArrayList<DspqRouter>();	
		Criterion criterion=null;
		try
		{			
			criterion = Restrictions.in("dspqPortfolioName",portfolioId);			
			dspqRouterList=findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqRouterList;
	}
	/*
	 * @Deepak
	 * For portfolio name according to applicant type
	 */
	@Transactional(readOnly=false)
	public List<String[]> getPortfolioIdByApplicantType(DistrictMaster districtMaster,String applicantType)
	{
		List<String[]> DspqPortfolioNameList = new ArrayList<String[]>();	
		Criterion criterion1=null;
		Criterion criterion2=null;
		Criterion criterion3=null;
		try
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass()).createAlias("dspqPortfolioName", "dpn").createAlias("dpn.districtMaster", "dm");
		    criteria.setProjection(
		    		Projections.distinct(Projections.projectionList()
		    		.add(Projections.property("dpn.dspqPortfolioNameId"), "dspqPortfolioNameId")
		    		.add(Projections.property("dpn.portfolioName"), "portfolioName")
		    		));
			criterion1=Restrictions.eq("applicantType",applicantType);
			criterion2=Restrictions.eq("applicantTypeStatus","A");
			criterion3=Restrictions.eq("status","A");
			criteria.add(criterion1).add(criterion2).add(criterion3).addOrder(Order.asc("dpn.portfolioName"));
			criteria.add(Restrictions.eq("dm.districtId",districtMaster.getDistrictId()));
			DspqPortfolioNameList=criteria.list();			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return DspqPortfolioNameList;
	}
	@Transactional(readOnly=false)
	public List<DspqRouter> getDspqSectionListByTypeAndId(Integer portfolioId,String candidateType)
	{
		List<DspqRouter> dspqRouterList = new ArrayList<DspqRouter>();
		try
		{
			Criterion criterion1 = Restrictions.eq("dspqPortfolioName.dspqPortfolioNameId",portfolioId);
			Criterion criterion3 = Restrictions.eq("status", "A");
			if(candidateType.equalsIgnoreCase("")){
				Criterion criterion2 = Restrictions.eq("applicantType","E");
				dspqRouterList=findByCriteria(criterion1,criterion2,criterion3);
			}else{
				Criterion criterion2 = Restrictions.eq("applicantType",candidateType);
				dspqRouterList=findByCriteria(criterion1,criterion2,criterion3);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqRouterList;
	}
	@Transactional(readOnly=false)
	public List<DspqRouter> getDspqSectionListByTypeAndId(Integer portfolioId,String candidateType,List<DspqFieldMaster> dspqFieldList)
	{
		List<DspqRouter> dspqRouterList = new ArrayList<DspqRouter>();
		try
		{
			Criterion criterion1 = Restrictions.eq("dspqPortfolioName.dspqPortfolioNameId",portfolioId);
			Criterion criterion3=Restrictions.in("dspqFieldMaster",dspqFieldList);
			Criterion criterion4 = Restrictions.eq("status", "A");
			if(candidateType.equalsIgnoreCase("")){
				Criterion criterion2 = Restrictions.eq("applicantType","E");
				dspqRouterList=findByCriteria(criterion1,criterion2,criterion3,criterion4);
			}else{
				Criterion criterion2 = Restrictions.eq("applicantType",candidateType);
				dspqRouterList=findByCriteria(criterion1,criterion2,criterion3,criterion4);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqRouterList;
	}
	@Transactional(readOnly=false)
	public List<DspqRouter> getDspqSectionListByTypeAndId(List<Integer> portfolioIds)
	{
		List<DspqRouter> dspqRouterList = new ArrayList<DspqRouter>();
		try
		{
			Criterion criterion1 = Restrictions.in("dspqPortfolioName.dspqPortfolioNameId",portfolioIds);
				Criterion criterion3 = Restrictions.eq("status", "A");
				dspqRouterList=findByCriteria(criterion1,criterion3);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqRouterList;
	}
	@Transactional(readOnly=false)
	public List<DspqRouter> getControlsByPortfolioNameAndCTAndField(List<DspqPortfolioName> dspqPortfolioName,String applicantType,List<DspqFieldMaster> lstDspqFieldMaster,List<DspqSectionMaster> dspqSectionMasters)
	{
		List<DspqRouter> dspqRouterList = new ArrayList<DspqRouter>();	
		Criterion criterion1=null;
		Criterion criterion2=null;
		Criterion criterion3=null;
		Criterion criterion4=null;
		Criterion criterion5=null;
		try
		{
			criterion1=Restrictions.in("dspqPortfolioName",dspqPortfolioName);
			criterion3=Restrictions.eq("status","A");
			criterion4=Restrictions.in("dspqFieldMaster",lstDspqFieldMaster);
			criterion5=Restrictions.in("dspqSectionMaster",dspqSectionMasters);
			LogicalExpression orExp = Restrictions.or(criterion4, criterion5);
			if(applicantType.equalsIgnoreCase("")){
				criterion2=Restrictions.eq("applicantType","E");
				dspqRouterList=findByCriteria(criterion1,criterion2,criterion3,orExp);
			}else{
				criterion2=Restrictions.eq("applicantType",applicantType);
				dspqRouterList=findByCriteria(criterion1,criterion2,criterion3,orExp);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqRouterList;
	}
	@Transactional(readOnly=false)
	public List<DspqRouter> getDspqSectionListByPortfolioId(DspqPortfolioName dspqPortfolioName)
	{
		List<DspqRouter> dspqRouterList = new ArrayList<DspqRouter>();	
		Criterion criterion=null;
		try
		{
			Criterion criterion1=Restrictions.isNotNull("dspqSectionMaster");
			Criterion criterion3=Restrictions.isNotNull("sectionOrder");
			Criterion criterion2=Restrictions.eq("applicantType", "E");
			Criterion criterion4=Restrictions.eq("status", "A");		
			criterion = Restrictions.eq("dspqPortfolioName",dspqPortfolioName);			
			dspqRouterList=findByCriteria(Order.asc("sectionOrder"),criterion,criterion1,criterion2,criterion3,criterion4);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return dspqRouterList;
	}
}
