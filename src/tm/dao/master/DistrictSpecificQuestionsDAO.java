package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificQuestions;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificQuestionsDAO extends GenericHibernateDAO<DistrictSpecificQuestions, Integer> 
{
	public DistrictSpecificQuestionsDAO() 
	{
		super(DistrictSpecificQuestions.class);
	}
	
	
	/* Gagan : This method is used to get District Specific Questions   
	 * */
	@Transactional(readOnly=false)
	public List<DistrictSpecificQuestions> getDistrictSpecificQuestionBYDistrict(DistrictMaster districtMaster)
	{
		List<DistrictSpecificQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("status","A");

			lstDistSpecQuestions = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificQuestions> getDistrictSpecificQuestionBYHeadQuater(HeadQuarterMaster headQuarterMaster)
	{
		List<DistrictSpecificQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion2 = Restrictions.eq("status","A");

			lstDistSpecQuestions = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	
	
	@Transactional(readOnly=false)
	public List<DistrictMaster> getDistrictSpecificQuestionBYDistricts(List<DistrictMaster> districtMasters)
	{
		List<DistrictMaster> districts= new ArrayList<DistrictMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.in("districtMaster",districtMasters);
			Criterion criterion2 = Restrictions.eq("status","A");

			//lstDistSpecQuestions = findByCriteria(criterion1,criterion2);	
			
			Session session = getSession();
			districts = session.createCriteria(getPersistentClass()) 
			.add(criterion1) 
			.add(criterion2) 
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("districtMaster"))
			).list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return districts;		
	}
	@Transactional(readOnly=false)
	 public List<DistrictSpecificQuestions> findQuesByQues(String Questext) {
	  List<DistrictSpecificQuestions> statusList = null;
	  try 
	  {
	   Criterion criterion1 = Restrictions.ilike("question",Questext,MatchMode.ANYWHERE);
	   statusList = findByCriteria(criterion1);
	  } 
	  catch (Exception e) {
	   e.printStackTrace();
	  }
	  return statusList;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificQuestions> getDistrictSpecificQuestionBYQues(List<Integer> districtSpecificQuestions)
	{
		List<DistrictSpecificQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.in("questionId",districtSpecificQuestions);
			Criterion criterion2 = Restrictions.eq("status","A");

			lstDistSpecQuestions = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	@Transactional(readOnly=false)
	public List<DistrictSpecificQuestions> getDistrictSpecificQuestionBYDistrictAndHB(JobOrder jobOrder)
	{
		List<DistrictSpecificQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificQuestions>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}
			else if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}
			else if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}
			
			criteria.add(Restrictions.eq("status","A"));
			lstDistSpecQuestions = criteria.list();	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	@Transactional(readOnly=false)
	public List<DistrictSpecificQuestions> getDistrictSpecificQuestionByHQAndBranch(HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
	{
		List<DistrictSpecificQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificQuestions>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			 if(headQuarterMaster!=null)
				criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			else if(branchMaster!=null)
				criteria.add(Restrictions.eq("branchMaster",branchMaster));	

			lstDistSpecQuestions = criteria.list();		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		System.out.println("lstDistSpecQuestions::::::::::::::::::"+lstDistSpecQuestions.size());
		return lstDistSpecQuestions;		
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificQuestions> getDistrictSpecificQuestionBYDistrictAll(DistrictMaster districtMaster)
	{
		List<DistrictSpecificQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			//Criterion criterion3 = Restrictions.in("parentQuestionId",questions);
			lstDistSpecQuestions = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	@Transactional(readOnly=false)
	public List<DistrictSpecificQuestions> getDistrictSpecificQuestionBYDistrict(DistrictMaster districtMaster,List<Integer> questions)
	{
		List<DistrictSpecificQuestions> lstDistSpecQuestions= new ArrayList<DistrictSpecificQuestions>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.in("parentQuestionId",questions);
			lstDistSpecQuestions = findByCriteria(criterion1,criterion2,criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstDistSpecQuestions;		
	}
	
}
