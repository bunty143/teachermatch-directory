package tm.dao.master;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
 
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.QqQuestionSets;

import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import tm.bean.master.TeacherAnswerDetailsForDistrictSpecificQuestions;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherAnswerDetailsForDistrictSpecificQuestionsDAO extends GenericHibernateDAO<TeacherAnswerDetailsForDistrictSpecificQuestions, Integer> 
{
	public TeacherAnswerDetailsForDistrictSpecificQuestionsDAO() 
	{
		super(TeacherAnswerDetailsForDistrictSpecificQuestions.class);
	}
	
	
	
	
	
	@Transactional(readOnly=false)
	public List<TeacherAnswerDetailsForDistrictSpecificQuestions> getDistrictSpecificQuestionBYDistrict(DistrictMaster districtMaster,TeacherDetail teacherDetail)
	{
		System.out.println(" inside getDistrictSpecificQuestionBYDistrict.......................");
		
		
		List<TeacherAnswerDetailsForDistrictSpecificQuestions> lstDistSpecQuestions= null;
		try 
		{
			//System.out.println("Gagan : getDistrictSpecificQuestionBYDistrict Method :  "+districtMaster.getDistrictId() );
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("teacherDetail",teacherDetail);
			Order order=Order.desc("createdDateTime");
			Criteria criteria  =getSession().createCriteria(TeacherAnswerDetailsForDistrictSpecificQuestions.class);
			criteria.add(criterion1).add(criterion2).addOrder(order);
			lstDistSpecQuestions=criteria.list();
			JobOrder jobOrder=new JobOrder();
			
			if(lstDistSpecQuestions!=null && lstDistSpecQuestions.size()>0){
				jobOrder=lstDistSpecQuestions.get(0).getJobOrder();
				lstDistSpecQuestions=findTeacherAnswersByDistrictAndJob(teacherDetail,jobOrder);
			}
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstDistSpecQuestions==null || lstDistSpecQuestions.size()==0)
			return null;
		else
			return lstDistSpecQuestions;		
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> findLastTeacherAnswersByDistrictAndJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()));
			criteria.addOrder(Order.desc("createdDateTime"));
			//criteria.setProjection(Projections.groupProperty("jobOrder"));
			criteria.setProjection(Projections.property("jobOrder"));
			criteria.setMaxResults(1);
			jobOrders = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return jobOrders;
	}
	@Transactional(readOnly=false)
	public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findTeacherAnswersByDistrictAndJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
			
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			//criteria.addOrder(Order.desc("createdDateTime"));
			
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findTeacherAnswersByDistrict(TeacherDetail teacherDetail,DistrictMaster districtMaster)
	{
		List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			//criteria.addOrder(Order.desc("createdDateTime"));
			
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> findTeacherAnswersForQQByDistrict(TeacherDetail teacherDetail,DistrictMaster districtMaster) 
	{
		Session session = getSession();

		String sql =" select questionId,answerId from teacheranswerdetailsfordistrictspecificquestions where districtId=:districtMaster and teacherId=:teacherDetail";
		
		System.out.println(sql);

		Query query = session.createSQLQuery(sql);

		query.setParameter("teacherDetail", teacherDetail);
		query.setParameter("districtMaster", districtMaster);

		List<Object[]> rows = query.list();

		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				Object obj[] = (Object[])oo;
				map.put((Integer)obj[0], (Integer)obj[1]);
			}
		}
		return map;
	}
	
	
	/* @Start
	 * @Ashish Kumar
	 * @Description:: Get Flag For Answer
	 * */
	
	@Transactional(readOnly=false)
	public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findTeacherAnswersByDistrictAndTeacher(TeacherDetail teacherDetail,DistrictMaster districtMaster)
	{
		List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion3 = Restrictions.eq("isActive",true);
			
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.addOrder(Order.desc("createdDateTime"));
			
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}
	
	
	
	
		@Transactional(readOnly=false)
		public int getFlagForAnswer(TeacherDetail teacherDetail,DistrictMaster districtMaster)
		{
			//System.out.println(" -------------inside getFlagForAnswer------------------- "+teacherDetail.getTeacherId()+" "+districtMaster.getDistrictId());
			
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			int validFlag = 0;
			
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 = Restrictions.eq("isActive",true);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			
				
				//System.out.println(" teacherAnswerDetailsForDistrictSpecificQuestionList size-------- :: "+teacherAnswerDetailsForDistrictSpecificQuestionList.size());
				
				if(teacherAnswerDetailsForDistrictSpecificQuestionList.size()>0)
				{
					validFlag=1;
					for(TeacherAnswerDetailsForDistrictSpecificQuestions tdsql:teacherAnswerDetailsForDistrictSpecificQuestionList)
					{
						if(!tdsql.getQuestionType().equalsIgnoreCase("ml"))
						{
							//System.out.println(" tdsql.getIsValidAnswer() :: "+tdsql.getIsValidAnswer());
							
							if(tdsql.getIsValidAnswer()==false)
							{
								validFlag=2;
							}
						}
					}
					return validFlag;
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return validFlag;
		}
	/* @End
	 * @Ashish Kumar
	 * @Description:: Get Flag For Answer
	 * */
		
		@Transactional(readOnly=false)
		@SuppressWarnings("unchecked")
		public String updateOtherQQ(TeacherDetail teacherDetail,DistrictMaster districtMaster,List<Integer> answers) 
		{
			Session session = getSession();
			String hql = "UPDATE TeacherAnswerDetailsForDistrictSpecificQuestions set isActive = :isActive WHERE districtMaster = :districtMaster " +
					" and teacherDetail = :teacherDetail and answerId not IN (:answers) ";
			
			Query query = session.createQuery(hql);
			query.setParameter("isActive", false);
			query.setParameter("districtMaster", districtMaster);
			query.setParameter("teacherDetail", teacherDetail);
			query.setParameterList("answers", answers);
			int result = query.executeUpdate();
			System.out.println("Rows affected: " + result);
			
			return ""+result;
		}
		
		@Transactional(readOnly=false)
		public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findTeacherAnswersDistrict(DistrictMaster districtMaster)
		{
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			try 
			{
				Session session = getSession();
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 = Restrictions.eq("isActive",true);
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.addOrder(Order.desc("createdDateTime"));
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return teacherAnswerDetailsForDistrictSpecificQuestionList;
		}
		@Transactional(readOnly=false)
		public List<TeacherDetail> findQQStatusByTeacher(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster,boolean isValidAnswer)
		{
			List<TeacherDetail> lstTeacher= null;
			try 
			{
 
				Session session = getSession();
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 = Restrictions.eq("isActive",true);
				Criterion criterion4 = Restrictions.eq("isValidAnswer",isValidAnswer);
				Criterion criterion5 = Restrictions.ne("questionType","ml");
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.add(criterion5);
				if(isValidAnswer)
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail")));
				else
				criteria.setProjection(Projections.property("teacherDetail"));
				lstTeacher = criteria 
				.add(Restrictions.in("teacherDetail",teacherDetails)) 
				.list();

			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}

			return lstTeacher;
		}
		@Transactional(readOnly=false)
		public List<TeacherDetail> findQQStatusByTeacher(DistrictMaster districtMaster,boolean isValidAnswer)
		{
			List<TeacherDetail> lstTeacher= null;
			try 
			{
 
				Session session = getSession();
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 = Restrictions.eq("isActive",true);
				Criterion criterion4 = Restrictions.eq("isValidAnswer",isValidAnswer);
				Criterion criterion5 = Restrictions.ne("questionType","ml");
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.add(criterion5);
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail")));
				lstTeacher = criteria.list();

			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}

			return lstTeacher;
		}
		@Transactional(readOnly=false)
		public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findByTeacherListAnswersDistrictList(DistrictMaster districtMaster,List<Integer> lstTeacherDetails)
		{
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.in("teacherDetail",lstTeacherDetails);
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 = Restrictions.eq("isActive",true);
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.addOrder(Order.desc("createdDateTime"));
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return teacherAnswerDetailsForDistrictSpecificQuestionList;
		}
		
		@Transactional(readOnly=false)
		public List<DistrictMaster> findByQQTakenByTeacher(List<DistrictMaster> districtMasters,TeacherDetail teacherDetail)
		{
			List<DistrictMaster> districtMastersList = new ArrayList<DistrictMaster>();
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.in("districtMaster",districtMasters);
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.setProjection(Projections.distinct(Projections.property("districtMaster")));
				districtMastersList = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return districtMastersList;
		}
		
		@Transactional(readOnly=false)
		public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findTeacherAnswersBySetQues(TeacherDetail teacherDetail,QqQuestionSets qqQuestionSets)
		{
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.eq("questionSets",qqQuestionSets);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				//criteria.addOrder(Order.desc("createdDateTime"));
				
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return teacherAnswerDetailsForDistrictSpecificQuestionList;
		}
		
		@Transactional(readOnly=false)
		@SuppressWarnings("unchecked")
		public Map<Integer,Integer> findTeacherAnswersForQQByDistrictSet(TeacherDetail teacherDetail,QqQuestionSets qqQuestionSets) 
		{
			/*Session session = getSession();

			String sql =" select questionId,answerId from teacheranswerdetailsfordistrictspecificquestions where QuestionSetID=:qqQuestionSets and teacherId=:teacherDetail";
			
			System.out.println(">>>   "+sql);

			Query query = session.createSQLQuery(sql);

			query.setParameter("teacherDetail", teacherDetail);
			query.setParameter("questionSets", qqQuestionSets);

			List<Object[]> rows = query.list();

			Map<Integer,Integer> map = new HashMap<Integer, Integer>();
			if(rows.size()>0)
			{
				for(Object oo: rows){
					Object obj[] = (Object[])oo;
					map.put((Integer)obj[0], (Integer)obj[1]);
				}
			}*/
			Map<Integer,Integer> map = new HashMap<Integer, Integer>();
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.eq("questionSets",qqQuestionSets);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				//criteria.addOrder(Order.desc("createdDateTime"));
				
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
				if(teacherAnswerDetailsForDistrictSpecificQuestionList!=null && teacherAnswerDetailsForDistrictSpecificQuestionList.size()>0){
					for (TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestions : teacherAnswerDetailsForDistrictSpecificQuestionList) {
						map.put(teacherAnswerDetailsForDistrictSpecificQuestions.getDistrictSpecificQuestions().getQuestionId(), teacherAnswerDetailsForDistrictSpecificQuestions.getAnswerId());
					}
					
				}
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return map;
		}
		@Transactional(readOnly=false)
		public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findTeacherAnswersByDistrictTeacherAndQuestionSet(TeacherDetail teacherDetail,DistrictMaster districtMaster,QqQuestionSets qSet)
		{
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.eq("isActive",true);
				Criterion criterion3 = Restrictions.eq("questionSets",qSet);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);				
				criteria.add(criterion2);
				criteria.add(criterion3);
				if(districtMaster!=null)
					criteria.add(Restrictions.eq("districtMaster",districtMaster));
				//criteria.addOrder(Order.desc("createdDateTime"));
				
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return teacherAnswerDetailsForDistrictSpecificQuestionList;
		}
		@Transactional(readOnly=false)
		public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findTeacherAnswersByDistrictTeacherAndJobOrder(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobOrder job)
		{
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 = Restrictions.eq("isActive",true);
				Criterion criterion4 = Restrictions.eq("jobOrder",job);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.addOrder(Order.desc("createdDateTime"));
				
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return teacherAnswerDetailsForDistrictSpecificQuestionList;
		}
		
		@Transactional(readOnly=false)
		public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findTeacherAnswersByDistrictTeacherAndJobCategory(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobCategoryMaster jobCateg)
		{
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 = Restrictions.eq("isActive",true);
				//Criterion criterion4 = Restrictions.eq("jobOrder",jobCateg);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				//criteria.add(criterion4);
				criteria.createCriteria("jobOrder").add(Restrictions.eq("jobCategoryMaster", jobCateg));
				criteria.addOrder(Order.desc("createdDateTime"));
				
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return teacherAnswerDetailsForDistrictSpecificQuestionList;
		}
		@Transactional(readOnly=false)
		public List<TeacherDetail> findQQStatusByJobCategory(List<TeacherDetail> teacherDetails,JobCategoryMaster jobCategoryMaster,boolean isValidAnswer)
		{
			List<TeacherDetail> lstTeacher= null;
			try 
			{
 
				Session session = getSession();
				
				Criterion criterion3 = Restrictions.eq("isActive",true);
				Criterion criterion4 = Restrictions.eq("isValidAnswer",isValidAnswer);
				Criterion criterion5 = Restrictions.ne("questionType","ml");
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.createCriteria("jobOrder").add(Restrictions.eq("jobCategoryMaster",jobCategoryMaster));
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.add(criterion5);
				if(isValidAnswer)
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail")));
				else
				criteria.setProjection(Projections.property("teacherDetail"));
				lstTeacher = criteria 
				.add(Restrictions.in("teacherDetail",teacherDetails)) 
				.list();

			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}

			return lstTeacher;
		}
		@Transactional(readOnly=false)
		public List<TeacherDetail> findQQStatusByQuestionSet(List<TeacherDetail> teacherDetails,JobCategoryMaster jobCategoryMaster,boolean isValidAnswer)
		{
			List<TeacherDetail> lstTeacher= null;
			try 
			{
 
				Session session = getSession();
				
				Criterion criterion3 = Restrictions.eq("isActive",true);
				Criterion criterion4 = Restrictions.eq("isValidAnswer",isValidAnswer);
				Criterion criterion5 = Restrictions.ne("questionType","ml");
				Criteria criteria = session.createCriteria(getPersistentClass());
				if(jobCategoryMaster.getQuestionSets()!=null)
				criteria.add(Restrictions.eq("questionSets",jobCategoryMaster.getQuestionSets()));
				
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.add(criterion5);
				if(isValidAnswer)
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail")));
				else
				criteria.setProjection(Projections.property("teacherDetail"));
				lstTeacher = criteria 
				.add(Restrictions.in("teacherDetail",teacherDetails)) 
				.list();

			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}

			return lstTeacher;
		}
		
		@Transactional(readOnly=false)
		public List<TeacherDetail> findQQStatusByHQAndBranch(List<TeacherDetail> teacherDetails, HeadQuarterMaster headQuarterMaster, BranchMaster branchMaster, boolean isValidAnswer)
		{
			List<TeacherDetail> lstTeacher= null;
			try 
			{
 
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("isActive",true);
				Criterion criterion2 = Restrictions.eq("isValidAnswer",isValidAnswer);
				Criterion criterion3 = Restrictions.ne("questionType","ml");
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				
				if(headQuarterMaster!=null && branchMaster!=null)
				criteria.createCriteria("jobOrder").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
				else if(headQuarterMaster!=null)
				criteria.createCriteria("jobOrder").add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
				else if(branchMaster!=null)
				criteria.createCriteria("jobOrder").add(Restrictions.eq("branchMaster",branchMaster));
				
				if(isValidAnswer)
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail")));
				else
				criteria.setProjection(Projections.property("teacherDetail"));
				lstTeacher = criteria 
				.add(Restrictions.in("teacherDetail",teacherDetails)) 
				.list();

			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return lstTeacher;
		}
		
		@Transactional(readOnly=false)
		public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findTeacherAnswersByHQBranchAndTeacher(TeacherDetail teacherDetail,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
		{
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion3 = Restrictions.eq("isActive",true);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion3);
				if(headQuarterMaster!=null && branchMaster!=null)
					criteria.createCriteria("jobOrder").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
				else if(headQuarterMaster!=null)
					criteria.createCriteria("jobOrder").add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
				else if(branchMaster!=null)
					criteria.createCriteria("jobOrder").add(Restrictions.eq("branchMaster",branchMaster));
				criteria.addOrder(Order.desc("createdDateTime"));
				
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return teacherAnswerDetailsForDistrictSpecificQuestionList;
		}
		@Transactional(readOnly=false)
		public int getFlagForAnswerForHQB(TeacherDetail teacherDetail,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
		{
			//System.out.println(" -------------inside getFlagForAnswer------------------- "+teacherDetail.getTeacherId()+" "+districtMaster.getDistrictId());
			
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			int validFlag = 0;
			
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion3 = Restrictions.eq("isActive",true);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion3);
				if(headQuarterMaster!=null)
					criteria.createCriteria("jobOrder").add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
				else if(branchMaster!=null)
					criteria.createCriteria("jobOrder").add(Restrictions.eq("branchMaster",branchMaster));
				
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			
				
				//System.out.println(" teacherAnswerDetailsForDistrictSpecificQuestionList size-------- :: "+teacherAnswerDetailsForDistrictSpecificQuestionList.size());
				
				if(teacherAnswerDetailsForDistrictSpecificQuestionList.size()>0)
				{
					validFlag=1;
					for(TeacherAnswerDetailsForDistrictSpecificQuestions tdsql:teacherAnswerDetailsForDistrictSpecificQuestionList)
					{
						if(!tdsql.getQuestionType().equalsIgnoreCase("ml"))
						{
							//System.out.println(" tdsql.getIsValidAnswer() :: "+tdsql.getIsValidAnswer());
							
							if(tdsql.getIsValidAnswer()==false)
							{
								validFlag=2;
							}
						}
					}
					return validFlag;
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return validFlag;
		}
		
		@Transactional(readOnly=false)
		@SuppressWarnings("unchecked")
		public Map<Integer,Integer> findTeacherAnswersForQQByDistrictSetONB(TeacherDetail teacherDetail,QqQuestionSets qqQuestionSets) 
		{
			Map<Integer,Integer> map = new HashMap<Integer, Integer>();
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.eq("questionSets",qqQuestionSets);
				Criterion criterion3 = Restrictions.eq("isonboarding",true);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				//criteria.addOrder(Order.desc("createdDateTime"));
				
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
				if(teacherAnswerDetailsForDistrictSpecificQuestionList!=null && teacherAnswerDetailsForDistrictSpecificQuestionList.size()>0){
					for (TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestions : teacherAnswerDetailsForDistrictSpecificQuestionList) {
						map.put(teacherAnswerDetailsForDistrictSpecificQuestions.getDistrictSpecificQuestions().getQuestionId(), teacherAnswerDetailsForDistrictSpecificQuestions.getAnswerId());
					}
					
				}
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return map;
		}
		
		@Transactional(readOnly=false)
		public List<JobOrder> findLastTeacherAnswersByDistrictAndJobONB(TeacherDetail teacherDetail,JobOrder jobOrder, Boolean flagONB)
		{
			List<JobOrder> jobOrders = null;
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()));
				criteria.addOrder(Order.desc("createdDateTime"));
				//criteria.setProjection(Projections.groupProperty("jobOrder"));
				criteria.setProjection(Projections.property("jobOrder"));
				criteria.add(Restrictions.eq("isonboarding", flagONB));
				criteria.setMaxResults(1);
				jobOrders = criteria.list();

			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return jobOrders;
		}
		
		@Transactional(readOnly=false)
		public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findTeacherAnswersBySetQuesONB(TeacherDetail teacherDetail,QqQuestionSets qqQuestionSets, Boolean flagONB)
		{
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.eq("questionSets",qqQuestionSets);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(Restrictions.eq("isonboarding", flagONB));
				//criteria.addOrder(Order.desc("createdDateTime"));
				
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return teacherAnswerDetailsForDistrictSpecificQuestionList;
		}
		@Transactional(readOnly=false)
		@SuppressWarnings("unchecked")
		public String updateOtherQQONB(TeacherDetail teacherDetail,DistrictMaster districtMaster,List<Integer> answers, boolean flagONB) 
		{
			Session session = getSession();
			String hql = "UPDATE TeacherAnswerDetailsForDistrictSpecificQuestions set isActive = :isActive WHERE districtMaster = :districtMaster " +
					" and teacherDetail = :teacherDetail and answerId not IN (:answers) ";
			
			Query query = session.createQuery(hql);
			query.setParameter("isActive", false);
			query.setParameter("isonboarding", flagONB);
			query.setParameter("districtMaster", districtMaster);
			query.setParameter("teacherDetail", teacherDetail);
			query.setParameterList("answers", answers);
			int result = query.executeUpdate();
			System.out.println("Rows affected: " + result);
			
			return ""+result;
		}
		
		
		@Transactional(readOnly=false)
		public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findTeacherAnswersByHQBranchAndTeacherForONB(TeacherDetail teacherDetail,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
		{
			System.out.println(" findTeacherAnswersByHQBranchAndTeacherForONB ");
			
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion3 = Restrictions.eq("isActive",true);
				Criterion criterion4 = Restrictions.eq("isonboarding",true);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion3);
				criteria.add(criterion4);
				if(headQuarterMaster!=null && branchMaster!=null)
					criteria.createCriteria("jobOrder").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
				else if(headQuarterMaster!=null)
					criteria.createCriteria("jobOrder").add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
				else if(branchMaster!=null)
					criteria.createCriteria("jobOrder").add(Restrictions.eq("branchMaster",branchMaster));
				criteria.addOrder(Order.desc("createdDateTime"));
				
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return teacherAnswerDetailsForDistrictSpecificQuestionList;
		}
		
		@Transactional(readOnly=false)
		public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findTeacherAnswersByDistrictAndTeacherForONB(TeacherDetail teacherDetail,DistrictMaster districtMaster)
		{
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 = Restrictions.eq("isActive",true);
				Criterion criterion4 = Restrictions.eq("isonboarding",true);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.addOrder(Order.desc("createdDateTime"));
				
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return teacherAnswerDetailsForDistrictSpecificQuestionList;
		}
		
		@Transactional(readOnly=false)
		public Map<String,String> findTADFDQByTeaJobForONB(List<Integer> teacherIds,List<Integer> jobIds)
		{
			System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyfindTADFDQByTeaJobForONB");
			Map<String,String> thumbsMap = new LinkedHashMap<String, String>();
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(TeacherAnswerDetailsForDistrictSpecificQuestions.class);
				criteria.createAlias("jobOrder", "jo").createAlias("teacherDetail", "td")
			    .setProjection( Projections.projectionList()
			        .add( Projections.property("jo.jobId"), "jobId" )
			        .add( Projections.property("td.teacherId"), "teacherId" )
			    );
				criteria.add(Restrictions.in("jo.jobId", jobIds));
				criteria.add(Restrictions.in("td.teacherId", teacherIds));
				criteria.add(Restrictions.eq("isActive", true));
				criteria.add(Restrictions.eq("isonboarding", true));
				List<String[]>  listTeacherAndJobId= criteria.list();
				for(Object[] str:listTeacherAndJobId){
					thumbsMap.put(str[0].toString()+"##"+str[1].toString(), "1");
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return thumbsMap;
		}
		
		@Transactional(readOnly=false)
		public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findTeacherAnswersByDistrictTeacherAndQuestionSetONB(TeacherDetail teacherDetail,DistrictMaster districtMaster,QqQuestionSets qSet,JobOrder jobOrder)
		{
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList = null;
			try 
			{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.eq("isActive",true);
				Criterion criterion3 = Restrictions.eq("questionSets",qSet);
				Criterion criterion4 = Restrictions.eq("jobOrder",jobOrder);
				Criterion criterion5 = Restrictions.eq("isonboarding", true);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);				
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.add(criterion5);
				
				if(districtMaster!=null)
					criteria.add(Restrictions.eq("districtMaster",districtMaster));
				criteria.addOrder(Order.desc("createdDateTime"));
				
				teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return teacherAnswerDetailsForDistrictSpecificQuestionList;
		}

		// Optimization for Candidate pool search  replacement of findQQStatusByTeacher
		@Transactional(readOnly=false)
		public List<Integer> findQQStatusByTeacher_Op(DistrictMaster districtMaster,boolean isValidAnswer,List<Integer> teachersId)
		{
			List<Integer> lstTeacher= new ArrayList<Integer>();
			
			if(teachersId!=null && teachersId.size()>0)
				try 
				{
					Session session = getSession();
					Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
					Criterion criterion3 = Restrictions.eq("isActive",true);
					Criterion criterion4 = Restrictions.eq("isValidAnswer",isValidAnswer);
					Criterion criterion5 = Restrictions.ne("questionType","ml");
					Criteria criteria = session.createCriteria(getPersistentClass());
					criteria.add(criterion2);
					criteria.add(criterion3);
					criteria.add(criterion4);
					criteria.add(criterion5);
					
					criteria.add(Restrictions.in("teacherDetail.teacherId", teachersId));
					criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
					criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
					lstTeacher = criteria.list();
	
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}

			return lstTeacher;
		}
		
		@Transactional(readOnly=false)
		public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findQQStatusByDistrictAndQuestionSet(DistrictMaster districtMaster, QqQuestionSets questionSet)
		{
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> lstTeacherAnswers= null;
			try 
			{
				Criterion criterion1 = Restrictions.eq("isActive",true);
				Criterion criterion2 = Restrictions.eq("districtMaster", districtMaster);
				Criterion criterion3 = Restrictions.eq("questionSets",questionSet);
				
				lstTeacherAnswers = findByCriteria(criterion1,criterion2,criterion3);	
				

			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}

			return lstTeacherAnswers;
		}

	@Transactional(readOnly=false)
		public List<TeacherAnswerDetailsForDistrictSpecificQuestions> findByTeacherListAnswersDistrictList_Opp(DistrictMaster districtMaster,List<Integer> lstTeacherDetails)
		{
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList =  new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			 List<Object[]>listObject=null;
			try 
			{
		 		Session session = getSession();
		 	  	Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.createAlias("districtMaster", "dm").createAlias("teacherDetail", "td")
				 .setProjection( Projections.projectionList()
						  .add( Projections.property("td.teacherId"), "teacherId" )
						  .add( Projections.property("dm.districtId"), "districtId" )
			              .add( Projections.property("question"), "question" )
                         .add( Projections.property("questionOption"), "questionOption" )
                         .add( Projections.property("questionType"), "questionType" )
                           .add( Projections.property("insertedText"), "insertedText" )
                       );
				
				 criteria.add(Restrictions.eq("dm.districtId",districtMaster.getDistrictId())).add(Restrictions.eq("isActive", true)).addOrder(Order.desc("createdDateTime"));
	 			 listObject = criteria.list();
	  		  	if(listObject.size()>0){
              		 for(int j = 0; j < listObject.size(); j++){
              			 Object[] objArr2 = listObject.get(j); 
              			TeacherAnswerDetailsForDistrictSpecificQuestions taObj=new TeacherAnswerDetailsForDistrictSpecificQuestions();
              			DistrictMaster districtMaster1=new DistrictMaster();
                  		TeacherDetail teacherDetail=new TeacherDetail();
                  		if(objArr2[0]!=null){
                  			teacherDetail.setTeacherId(objArr2[0] == null ? null : Integer.parseInt(objArr2[0].toString()));
                  			taObj.setTeacherDetail(teacherDetail);
                  		}
                  		if(objArr2[1] !=null){
                  			districtMaster1.setDistrictId(objArr2[1] == null ? null : Integer.parseInt(objArr2[1].toString()));
                  			taObj.setDistrictMaster(districtMaster1);
                    		}
                  		if(objArr2[2] !=null){
                  			taObj.setQuestion(objArr2[2] == null ? null :  objArr2[2].toString());
                  		}
                  if(objArr2[3] !=null){
                  		taObj.setQuestionOption(objArr2[3] == null ? null :  objArr2[3].toString())	;
                  		}
                  if(objArr2[4] !=null){
             			taObj.setQuestionType( objArr2[4] == null ? null :  objArr2[4].toString());
             		}
                  if(objArr2[5] !=null){
             			taObj.setInsertedText(objArr2[5] == null ? null :  objArr2[5].toString());
             		}
                     teacherAnswerDetailsForDistrictSpecificQuestionList.add(taObj);
              		 }
              	}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
	 		return teacherAnswerDetailsForDistrictSpecificQuestionList;
		}
  
}
