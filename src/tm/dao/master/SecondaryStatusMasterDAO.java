package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.JobOrder;
import tm.bean.master.SecondaryStatusMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SecondaryStatusMasterDAO extends GenericHibernateDAO<SecondaryStatusMaster, Integer>{
	public SecondaryStatusMasterDAO() {
		super(SecondaryStatusMaster.class);
	}
	
	@Transactional(readOnly=false)
	public  List<SecondaryStatusMaster> findStatusList(List<Integer> sSMList)
	{
		List<SecondaryStatusMaster> lstStatus= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("secStatusId",sSMList);
			lstStatus = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public  List<SecondaryStatusMaster> findActiveSecStaus()
	{
		List<SecondaryStatusMaster> lstStatus= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("status","A");
			lstStatus = findByCriteria(Order.asc("secStatusName"), criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=true)
	public boolean checkDuplicateJobCategory(SecondaryStatusMaster secondaryStatusMaster, String secStatusName,DistrictMaster districtMaster,Integer hqId, Integer branchId)	{
		List<SecondaryStatusMaster>list=new ArrayList<SecondaryStatusMaster>();
		try{
			Criterion criterion=Restrictions.eq("secStatusName", secStatusName);
			Criterion criterion1=null;
			if(secondaryStatusMaster!=null){
				Criterion duplicateCheck=Restrictions.not(Restrictions.in("secStatusId",new Integer[]{secondaryStatusMaster.getSecStatusId()}));
				if(branchId!=null)
					criterion1=Restrictions.eq("branchId", branchId);
				else if(hqId!=null)
					criterion1=Restrictions.and(Restrictions.isNull("branchId"),Restrictions.eq("headQuarterId", hqId));
				else{
					if(districtMaster!=null){
						criterion1=Restrictions.eq("districtMaster", districtMaster);
					}else{
						criterion1=Restrictions.isNull("districtMaster");
					}
				}
				list=findByCriteria(criterion,criterion1,duplicateCheck);
			}else{
				if(branchId!=null)
					criterion1=Restrictions.eq("branchId", branchId);
				else if(hqId!=null)
					criterion1=Restrictions.and(Restrictions.isNull("branchId"),Restrictions.eq("headQuarterId", hqId));
				else{
					if(districtMaster!=null){
						criterion1=Restrictions.eq("districtMaster", districtMaster);
					}else{
						criterion1=Restrictions.isNull("districtMaster");
					}
				}
				list=findByCriteria(criterion,criterion1);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		if(list.size()>0)
			return true;
		else
			return false;
	}
	
	@Transactional(readOnly=false)
	public  List<SecondaryStatusMaster> findSecStatus(Order order,int startPos,int limit,Criterion...criterions)
	{
		List<SecondaryStatusMaster> secondaryStatusMasters=null;
		
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for(Criterion c:criterions){
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			if(order != null)
				criteria.addOrder(order);
			
			secondaryStatusMasters=criteria.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return secondaryStatusMasters;
	}
	
	/*================ Gagan : Method will return Default(common) Tags with District Specific Tags ================*/
	@Transactional(readOnly=false)
	public  List<SecondaryStatusMaster> findTagswithDistrictSpecificTags(JobOrder jobOrder)
	{
		List<SecondaryStatusMaster> lstStatus= new ArrayList<SecondaryStatusMaster>();
		try 
		{
				Criterion criterion1 = Restrictions.eq("status","A");
				Criterion criterion2 = null;
				Criterion criterion3 = null;				
				Criterion criterion4 = null;
			    if(jobOrder.getDistrictMaster()!=null)
			    {
			    	criterion3=	Restrictions.isNull("districtMaster");
			    	criterion4 =  Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
			    	criterion2	=	Restrictions.or(criterion3, criterion4);
			    }
			    else if(jobOrder.getBranchMaster()!=null)
			    {
			    	criterion2 =  Restrictions.eq("branchId",jobOrder.getBranchMaster().getBranchId());
			    }
			    else if(jobOrder.getHeadQuarterMaster()!=null)
			    {
			    	criterion3=	Restrictions.isNull("branchId");
			    	criterion4 =  Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId());
			    	criterion2	=	Restrictions.and(criterion3, criterion4);
			    }
				
				lstStatus = findByCriteria(Order.asc("secStatusName"), criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public  List<SecondaryStatusMaster> findTagswithDistrictInCandidatePool(DistrictMaster districtMaster)
	{
		List<SecondaryStatusMaster> lstStatus= null;
		try 
		{
			//System.out.println(" Method : TagswithDistrictSpecificTags :  "+jobOrder.getJobId());
			Criterion criterion1 = Restrictions.eq("status","A");
			Criterion criterion3	=	Restrictions.isNull("districtMaster");
			Criterion criterion4 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2	=	Restrictions.or(criterion3, criterion4);
			
			lstStatus = findByCriteria(Order.asc("secStatusName"), criterion1,criterion2);		
			//System.out.println(" \n \n\n Sec status dao method lstStatus size : "+lstStatus.size()+" jobOrder.getDistrictMaster() "+jobOrder.getDistrictMaster()+" District Id : "+jobOrder.getDistrictMaster().getDistrictId());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public  List<SecondaryStatusMaster> findTagswithDistrictSpecificTagsByYHBD(JobOrder jobOrder)
	{
		List<SecondaryStatusMaster> lstStatus= new ArrayList<SecondaryStatusMaster>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				Criterion criterion3	=	Restrictions.isNull("districtMaster");
				Criterion criterion4 	= 	Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
				Criterion criterion2	=	Restrictions.or(criterion3, criterion4);
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				Criterion criterion3	=	Restrictions.isNull("branchMaster");
				Criterion criterion4 	= 	Restrictions.eq("branchMaster",jobOrder.getBranchMaster());
				Criterion criterion2	=	Restrictions.or(criterion3, criterion4);
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				Criterion criterion3	=	Restrictions.isNull("headQuarterMaster");
				Criterion criterion4 	= 	Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster());
				Criterion criterion2	=	Restrictions.or(criterion3, criterion4);
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			criteria.add(Restrictions.eq("status","A"));
			criteria.addOrder(Order.asc("secStatusName"));
			lstStatus = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public  List<SecondaryStatusMaster> findTagsWithHeadQuarterInCandidatePool(int headQuarterMasterId)
	{
		List<SecondaryStatusMaster> lstStatus= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("status","A");
			Criterion criterion3	=	Restrictions.isNull("branchId");
			Criterion criterion4 	= 	Restrictions.eq("headQuarterId",headQuarterMasterId);
			Criterion criterion2	=	Restrictions.and(criterion3, criterion4);
			lstStatus = findByCriteria(Order.asc("secStatusName"), criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public  List<SecondaryStatusMaster> findTagsWithBranchInCandidatePool(int branchId)
	{
		List<SecondaryStatusMaster> lstStatus= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("status","A");
			//Criterion criterion3	=	Restrictions.isNull("districtMaster");
			Criterion criterion2 	= 	Restrictions.eq("branchId",branchId);
			//Criterion criterion2	=	Restrictions.or(criterion3, criterion4);
			lstStatus = findByCriteria(Order.asc("secStatusName"), criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
}
