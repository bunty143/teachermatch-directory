package tm.dao.master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import edu.emory.mathcs.backport.java.util.LinkedList;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.EligibilityMaster;
import tm.dao.generic.GenericHibernateDAO;

public class EligibilityMasterDAO extends GenericHibernateDAO<EligibilityMaster, Integer> 
{

	public EligibilityMasterDAO()
	{
		super(EligibilityMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<EligibilityMaster> getEligibilityByCode(String eligibilityCodes[])
	{
		List<EligibilityMaster> eligibilityMasters= new ArrayList<EligibilityMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.in("eligibilityCode",eligibilityCodes);
			Criterion criterion2 = Restrictions.eq("status","A");

			eligibilityMasters = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return eligibilityMasters;
	}
	
	@Transactional(readOnly=false)
	public Map<Integer, EligibilityMaster> getEligibilityMap()
	{
		Map<Integer, EligibilityMaster> eMap = new HashMap<Integer, EligibilityMaster>();
		List<EligibilityMaster> eligibilityMasters= new ArrayList<EligibilityMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("status","A");
			eligibilityMasters = findByCriteria(criterion1);	
			
			for (EligibilityMaster eligibilityMaster : eligibilityMasters) {
				eMap.put(eligibilityMaster.getEligibilityId(), eligibilityMaster);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return eMap;
	}
	
	public List<String> getColumnByDistrict(Integer districtId){
		List<String> listColoumn=new LinkedList();
		Session session=getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(EligibilityMaster.class)
	    .setProjection( Projections.projectionList()
	        .add( Projections.property("eligibilityCode"), "eligibilityCode" )
	    ).add(Restrictions.eq("districtId", districtId)).addOrder(Order.asc("orderBy"));
		System.out.println("listColoumn==="+listColoumn);
		List<String> allColoumn=criteria.list();
		boolean flagOS=false;
		for(Object columnName:allColoumn){
			if(columnName.toString().equalsIgnoreCase("os"))
			flagOS=true;
			else
			listColoumn.add(columnName.toString());
		}
		if(flagOS)listColoumn.add("Summary");
		return listColoumn;
	}
	
	public Integer getEligibilityId(Integer districtId,String elibilityName){
		Session session=getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(EligibilityMaster.class)
	    .setProjection( Projections.projectionList()
	        .add( Projections.property("eligibilityId"), "eligibilityId" )
	    ).add(Restrictions.eq("districtId", districtId)).add(Restrictions.eq("eligibilityName", elibilityName));
		List<Integer> allColoumn=criteria.list();
		if(allColoumn!=null && !allColoumn.isEmpty())
			return allColoumn.get(0);
		System.out.println("listColoumn==="+allColoumn);
		return null;
	}
	
}
