package tm.dao.master;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.ContactTypeMaster;
import tm.dao.generic.GenericHibernateDAO;

/* @Author:Gagan
 * @Discription: ContactTypeMasterDAO
 */
public class ContactTypeMasterDAO extends GenericHibernateDAO<ContactTypeMaster, Integer>
{
	public ContactTypeMasterDAO() 
	{
		super(ContactTypeMaster.class);
	}
	
	
	@Transactional(readOnly=false)
	public ContactTypeMaster getActiveMaster(Integer contactTypeId)
	{
		ContactTypeMaster contactTypeMaster=null;
		List<ContactTypeMaster> contactTypeMasters=new ArrayList<ContactTypeMaster>();
		try 
		{	
			
			Criterion criterion1 = Restrictions.eq("status","A");
			Criterion criterion2 = Restrictions.eq("contactTypeId",contactTypeId);
			contactTypeMasters = findByCriteria(criterion1,criterion2);
			if(contactTypeMasters.size()==1)
				contactTypeMaster=contactTypeMasters.get(0);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return contactTypeMaster;
	}
	@Transactional(readOnly=false)
	public List<ContactTypeMaster> findByContactTypeId(List<Integer> list)
	{
		List<ContactTypeMaster> contactTypeMasterList=null;
		try{	
		Criterion criterion2 =  Restrictions.in("contactTypeId",list);
			contactTypeMasterList=	findByCriteria(criterion2);
			} catch(Exception ex){	
		}
		
		return contactTypeMasterList;
	}
}
