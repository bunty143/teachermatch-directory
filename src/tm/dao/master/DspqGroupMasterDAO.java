package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DspqGroupMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DspqGroupMasterDAO extends GenericHibernateDAO<DspqGroupMaster, Integer> 
{
	public DspqGroupMasterDAO() 
	{
		super(DspqGroupMaster.class);
	}
	
	
	@Transactional(readOnly=false)
	public List<DspqGroupMaster> getActiveDspqGroupMaster()
	{
		List<DspqGroupMaster> dspqGroupMasterList = new ArrayList<DspqGroupMaster>();	
		Criterion criterion1=Restrictions.eq("status","A");
		dspqGroupMasterList=findByCriteria(criterion1);
		return dspqGroupMasterList;
	}
	
	
}
