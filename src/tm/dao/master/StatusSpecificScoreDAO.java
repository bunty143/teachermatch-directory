package tm.dao.master;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.StatusSpecificQuestions;
import tm.bean.master.StatusSpecificScore;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class StatusSpecificScoreDAO extends GenericHibernateDAO<StatusSpecificScore, Integer> 
{
	public StatusSpecificScoreDAO() 
	{
		super(StatusSpecificScore.class);
	}
	
	@Autowired
	private StatusSpecificQuestionsDAO statusSpecificQuestionsDAO;
	public void setStatusSpecificQuestionsDAO(
			StatusSpecificQuestionsDAO statusSpecificQuestionsDAO) {
		this.statusSpecificQuestionsDAO = statusSpecificQuestionsDAO;
	}
	
	@Transactional(readOnly=true)
	public List<StatusSpecificScore> findByScore(Order order,int startPos,int limit,Criterion... criterion)
	{
		List<StatusSpecificScore> lststatusSpecificScore = new ArrayList<StatusSpecificScore>();
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			if(limit!=0)
			{
				criteria.setFirstResult(startPos);
				criteria.setMaxResults(limit);
			}
			
			if(order != null)
				criteria.addOrder(order);
				
			lststatusSpecificScore = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lststatusSpecificScore;
		}
	}
	
	@Transactional(readOnly=false)
	public boolean copyQuestionAnswer(JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster,TeacherDetail teacherDetail)
	{
		boolean isReturn=true;
		List<StatusSpecificQuestions> lstStatusSpecificQuestions= new ArrayList<StatusSpecificQuestions>();
		try 
		{
			if(jobOrder.getDistrictMaster()!=null){
				Criterion criterion_districtMaster = Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
				Criterion criterion_jobCategoryMaster = Restrictions.eq("jobCategoryId", jobOrder.getJobCategoryMaster().getJobCategoryId());
				Criterion criterionStatus=null;
				if(statusMaster!=null){
					criterionStatus = Restrictions.eq("statusId", statusMaster.getStatusId());
				}else{
					criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
				}
				Criterion criterion_status= Restrictions.eq("status","A");
				lstStatusSpecificQuestions=statusSpecificQuestionsDAO.findByCriteria(criterion_districtMaster,criterion_jobCategoryMaster,criterionStatus,criterion_status);
				
				if(lstStatusSpecificQuestions.size()>0)
				{
					int i=0;
					for(StatusSpecificQuestions pojo:lstStatusSpecificQuestions)
					{
						StatusSpecificScore statusSpecificScore=new StatusSpecificScore();
						statusSpecificScore.setDistrictId(pojo.getDistrictId());
						
						statusSpecificScore.setJobCategoryId(pojo.getJobCategoryId());
						statusSpecificScore.setJobOrder(jobOrder);
						
						if(pojo.getStatusId()!=null)
							statusSpecificScore.setStatusId(pojo.getStatusId());
						if(pojo.getSecondaryStatusId()!=null)
							statusSpecificScore.setSecondaryStatusId(pojo.getSecondaryStatusId());
						
						statusSpecificScore.setStatusSpecificQuestions(pojo);
						statusSpecificScore.setQuestion(pojo.getQuestion());
						if(pojo.getSkillAttributesMaster()!=null){
							statusSpecificScore.setSkillAttributesMaster(pojo.getSkillAttributesMaster());
							statusSpecificScore.setScoringCaption(pojo.getSkillAttributesMaster().getSkillName());
						}
						statusSpecificScore.setScoreProvided(0);
						statusSpecificScore.setMaxScore(pojo.getMaxScore());
						statusSpecificScore.setUserMaster(userMaster);
						statusSpecificScore.setCreatedDateTime(new Date());
						
						statusSpecificScore.setTeacherDetail(teacherDetail);
						statusSpecificScore.setFinalizeStatus(0);
						
						if(userMaster.getSchoolId()!=null)
							statusSpecificScore.setSchoolId(userMaster.getSchoolId().getSchoolId());
						
						
						makePersistent(statusSpecificScore);
						i++;
						if(i%3==0)
						{
							flush();
							clear();
						}
					}
					//System.out.println("Copied question no. "+i);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
			isReturn=false;
		}
		finally
		{
			return isReturn;
		}
	}
	
	@Transactional(readOnly=false)
	public List<StatusSpecificScore> getQuestionList(JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,TeacherDetail teacherDetail,UserMaster userMaster)
	{
		List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
		try 
		{
			if(jobOrder.getDistrictMaster()!=null){
				Criterion criterion_districtMaster = Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
				Criterion criterion_jobCategoryMaster = Restrictions.eq("jobCategoryId", jobOrder.getJobCategoryMaster().getJobCategoryId());
				Criterion criterion_job = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion_user = Restrictions.eq("userMaster", userMaster);
				Criterion criterion_teacherDetails = Restrictions.eq("teacherDetail", teacherDetail);
				
				Criterion criterionStatus=null;
				if(statusMaster!=null){
					criterionStatus = Restrictions.eq("statusId", statusMaster.getStatusId());
				}else{
					criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
				}
				SchoolMaster schoolMaster=null;
				Criterion criterion_school =null;
				if(userMaster.getSchoolId()!=null){
					schoolMaster=userMaster.getSchoolId();
					criterion_school=Restrictions.eq("schoolId", schoolMaster.getSchoolId());
				}
				
				if(schoolMaster==null)
					lstStatusSpecificScore=findByCriteria(criterion_districtMaster,criterion_jobCategoryMaster,criterion_job,criterionStatus,criterion_teacherDetails,criterion_user);
				else
					lstStatusSpecificScore=findByCriteria(criterion_districtMaster,criterion_school,criterion_jobCategoryMaster,criterion_job,criterionStatus,criterion_teacherDetails,criterion_user);
			}
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstStatusSpecificScore;
		}
	}
	@Transactional(readOnly=false)
	public List<StatusSpecificScore> getQuestionListForSuperAdministration(JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,TeacherDetail teacherDetail)
	{
		List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
		try 
		{
			Criteria criteria=getSession().createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId()));
			criteria.add(Restrictions.eq("jobCategoryId", jobOrder.getJobCategoryMaster().getJobCategoryId()));
			criteria.add(Restrictions.eq("jobOrder", jobOrder));
			//Criterion criterion_user = Restrictions.eq("userMaster", userMaster);
			criteria.add(Restrictions.eq("teacherDetail", teacherDetail));
			
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criteria.add(Restrictions.eq("statusId", statusMaster.getStatusId()));
			}else{
				criteria.add(Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId()));
			}
			criteria.addOrder(Order.asc("statusSpecificQuestions.questionId"));
			/*SchoolMaster schoolMaster=null;
			if(userMaster.getSchoolId()!=null)
				schoolMaster=userMaster.getSchoolId();
			Criterion criterion_school = Restrictions.eq("schoolId", schoolMaster);
			
			if(schoolMaster==null)
				lstStatusSpecificScore=findByCriteria(criterion_districtMaster,criterion_jobCategoryMaster,criterion_job,criterionStatus,criterion_teacherDetails,criterion_user);
			else*/
				lstStatusSpecificScore=criteria.list();//findByCriteria(criterion_districtMaster,criterion_jobCategoryMaster,criterion_job,criterionStatus,criterion_teacherDetails,sort);
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstStatusSpecificScore;
		}
	}
	@Transactional(readOnly=false)
	public List<StatusSpecificScore> getStatusSpecificScoreList(Integer[] answerIds)
	{
		List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
		try{
			Criterion criterion = Restrictions.in("answerId", answerIds);
			lstStatusSpecificScore=findByCriteria(criterion);
			
		}catch (Exception e) {
			//e.printStackTrace();
		}
		finally{
			return lstStatusSpecificScore;
		}
	}
	
	
	@Transactional(readOnly=false)
	public List<StatusSpecificScore> getQuestionList_msu(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,List<JobOrder> LstjobOrders,StatusMaster statusMaster,SecondaryStatus secondaryStatus,TeacherDetail teacherDetail,UserMaster userMaster)
	{
		List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
		try 
		{
			if(LstjobOrders.size()>0){
				Criterion criterion_districtMaster = Restrictions.eq("districtId", districtMaster.getDistrictId());
				Criterion criterion_jobCategoryMaster = Restrictions.eq("jobCategoryId",jobCategoryMaster.getJobCategoryId());
				Criterion criterion_job = Restrictions.in("jobOrder", LstjobOrders);
				Criterion criterion_user = Restrictions.eq("userMaster", userMaster);
				Criterion criterion_teacherDetails = Restrictions.eq("teacherDetail", teacherDetail);
				
				Criterion criterionStatus=null;
				if(statusMaster!=null)
				{
					criterionStatus = Restrictions.eq("statusId", statusMaster.getStatusId());
					//System.out.println("statusMaster "+statusMaster.getStatusId());
				}
				else
				{
					criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
					//System.out.println("secondaryStatus "+secondaryStatus.getSecondaryStatusId());
				}
				
				//System.out.println("TID "+teacherDetail.getTeacherId() +" UID "+userMaster.getUserId());
				
				lstStatusSpecificScore=findByCriteria(Order.asc("createdDateTime"),criterion_districtMaster,criterion_jobCategoryMaster,criterion_job,criterionStatus,criterion_teacherDetails,criterion_user);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstStatusSpecificScore;
		}
	}
	
	
	
	@Transactional(readOnly=false)
	public boolean copyQuestionAnswersForAllJobLst_msu(List<JobOrder> LstJobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster,TeacherDetail teacherDetail)
	{
		boolean isReturn=true;
		
		DistrictMaster districtMaster=null;
		JobCategoryMaster jobCategoryMaster=null;
		if(LstJobOrder.size() > 0)
		{
			districtMaster=LstJobOrder.get(0).getDistrictMaster();
			jobCategoryMaster=LstJobOrder.get(0).getJobCategoryMaster();
			
			List<StatusSpecificQuestions> lstStatusSpecificQuestions= new ArrayList<StatusSpecificQuestions>();
			Criterion criterion_districtMaster = Restrictions.eq("districtId", districtMaster.getDistrictId());
			Criterion criterion_jobCategoryMaster = Restrictions.eq("jobCategoryId", jobCategoryMaster.getJobCategoryId());
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusId", statusMaster.getStatusId());
			}else{
				criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
			}
			
			Criterion criterion_status= Restrictions.eq("status","A");
			
			lstStatusSpecificQuestions=statusSpecificQuestionsDAO.findByCriteria(criterion_districtMaster,criterion_jobCategoryMaster,criterionStatus,criterion_status);
			
			for(JobOrder jobOrder:LstJobOrder)
			{
				try 
				{
					if(lstStatusSpecificQuestions.size()>0)
					{
						int i=0;
						for(StatusSpecificQuestions pojo:lstStatusSpecificQuestions)
						{
							StatusSpecificScore statusSpecificScore=new StatusSpecificScore();
							statusSpecificScore.setDistrictId(pojo.getDistrictId());
							
							statusSpecificScore.setJobCategoryId(pojo.getJobCategoryId());
							statusSpecificScore.setJobOrder(jobOrder);
							
							if(pojo.getStatusId()!=null)
								statusSpecificScore.setStatusId(pojo.getStatusId());
							if(pojo.getSecondaryStatusId()!=null)
								statusSpecificScore.setSecondaryStatusId(pojo.getSecondaryStatusId());
							
							statusSpecificScore.setStatusSpecificQuestions(pojo);
							statusSpecificScore.setQuestion(pojo.getQuestion());
							if(pojo.getSkillAttributesMaster()!=null){
								statusSpecificScore.setSkillAttributesMaster(pojo.getSkillAttributesMaster());
								statusSpecificScore.setScoringCaption(pojo.getSkillAttributesMaster().getSkillName());
							}
							statusSpecificScore.setScoreProvided(0);
							statusSpecificScore.setMaxScore(pojo.getMaxScore());
							statusSpecificScore.setUserMaster(userMaster);
							statusSpecificScore.setCreatedDateTime(new Date());
							
							statusSpecificScore.setTeacherDetail(teacherDetail);
							statusSpecificScore.setFinalizeStatus(0);
							
							if(userMaster.getSchoolId()!=null)
								statusSpecificScore.setSchoolId(userMaster.getSchoolId().getSchoolId());
							
							
							makePersistent(statusSpecificScore);
							i++;
							if(i%3==0)
							{
								flush();
								clear();
							}
						}
						//System.out.println("Copied question no. "+i);
					}
				} 
				catch (Exception e) {
					e.printStackTrace();
					isReturn=false;
				}
			}
		}
		
		return isReturn;
	}
	@Transactional(readOnly=false)
	public boolean copyQuestionAnswersForAllJobCategoryLst_msu(List<JobCategoryMaster> jobCategoryMasters,List<JobOrder> LstJobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,List<SecondaryStatus> secondaryStatuss,UserMaster userMaster,TeacherDetail teacherDetail)
	{
		boolean isReturn=true;
		Map<String,StatusSpecificScore> scoreMap= new HashMap<String,StatusSpecificScore>();
		DistrictMaster districtMaster=null;
		if(LstJobOrder.size() > 0)
		{
			districtMaster=LstJobOrder.get(0).getDistrictMaster();
			List<StatusSpecificQuestions> lstStatusSpecificQuestions= new ArrayList<StatusSpecificQuestions>();
			if(jobCategoryMasters.size()>0){
				List<Integer> jobCategoyList=new ArrayList<Integer>();
				for (int i=0;i<jobCategoryMasters.size();i++) {
					jobCategoyList.add(jobCategoryMasters.get(i).getJobCategoryId());
				}
				Criterion criterion_districtMaster = Restrictions.eq("districtId", districtMaster.getDistrictId());
				Criterion criterion_jobCategoryMaster = Restrictions.in("jobCategoryId", jobCategoyList);
				Criterion criterionStatus=null;
				if(secondaryStatuss.size()>0){
					List<Integer> secondaryStatussList=new ArrayList<Integer>();
					for (int i=0;i<secondaryStatuss.size();i++) {
						secondaryStatussList.add(secondaryStatuss.get(i).getSecondaryStatusId());
					}
					criterionStatus = Restrictions.in("secondaryStatusId",secondaryStatussList);
				}else{
					criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
				}
				
				Criterion criterion_status= Restrictions.eq("status","A");
				try{
					lstStatusSpecificQuestions=statusSpecificQuestionsDAO.findByCriteria(criterion_districtMaster,criterion_jobCategoryMaster,criterionStatus,criterion_status);
				}catch(Exception e){e.printStackTrace();}
			
				System.out.println("lstStatusSpecificQuestions:::::::::::::::::::::"+lstStatusSpecificQuestions.size());
				
				try{
					List<StatusSpecificScore> list= new ArrayList<StatusSpecificScore>();
					Criterion criterion_teacher= Restrictions.eq("teacherDetail",teacherDetail);
					Criterion criterion_usermster= Restrictions.eq("userMaster",userMaster);
					list=findByCriteria(criterion_districtMaster,criterion_jobCategoryMaster,criterionStatus,criterion_teacher,criterion_usermster);
					for (StatusSpecificScore statusSpecificScore : list) {
						String key=statusSpecificScore.getSecondaryStatusId()+"##"+statusSpecificScore.getJobOrder().getJobId()+"##"+statusSpecificScore.getTeacherDetail().getTeacherId();
						scoreMap.put(key, statusSpecificScore);
					}
				}catch(Exception e){}
			
			}
			for(JobOrder jobOrder:LstJobOrder)
			{
				try 
				{
					if(lstStatusSpecificQuestions.size()>0)
					{
						int i=0;
						for(StatusSpecificQuestions pojo:lstStatusSpecificQuestions)
						{
							String keyCheck=pojo.getSecondaryStatusId()+"##"+jobOrder.getJobId()+"##"+teacherDetail.getTeacherId();
							if(scoreMap.get(keyCheck)==null){
								if(jobOrder.getJobCategoryMaster().equals(pojo.getJobCategoryId())){
									StatusSpecificScore statusSpecificScore=new StatusSpecificScore();
									statusSpecificScore.setDistrictId(pojo.getDistrictId());
									
									statusSpecificScore.setJobCategoryId(pojo.getJobCategoryId());
									statusSpecificScore.setJobOrder(jobOrder);
									
									if(pojo.getStatusId()!=null)
										statusSpecificScore.setStatusId(pojo.getStatusId());
									if(pojo.getSecondaryStatusId()!=null)
										statusSpecificScore.setSecondaryStatusId(pojo.getSecondaryStatusId());
									
									statusSpecificScore.setStatusSpecificQuestions(pojo);
									statusSpecificScore.setQuestion(pojo.getQuestion());
									if(pojo.getSkillAttributesMaster()!=null){
										statusSpecificScore.setSkillAttributesMaster(pojo.getSkillAttributesMaster());
										statusSpecificScore.setScoringCaption(pojo.getSkillAttributesMaster().getSkillName());
									}
									statusSpecificScore.setScoreProvided(0);
									statusSpecificScore.setMaxScore(pojo.getMaxScore());
									statusSpecificScore.setUserMaster(userMaster);
									statusSpecificScore.setCreatedDateTime(new Date());
									
									statusSpecificScore.setTeacherDetail(teacherDetail);
									statusSpecificScore.setFinalizeStatus(0);
									
									if(userMaster.getSchoolId()!=null)
										statusSpecificScore.setSchoolId(userMaster.getSchoolId().getSchoolId());
									
									
									makePersistent(statusSpecificScore);
									i++;
									if(i%3==0)
									{
										flush();
										clear();
									}
								}
							}
						}
						//System.out.println("Copied question no. "+i);
					}
				} 
				catch (Exception e) {
					e.printStackTrace();
					isReturn=false;
				}
			}
		}
		
		return isReturn;
	}
	@Transactional(readOnly=false)
	public List<StatusSpecificScore> getQuestionAllJobCategoryList_msu(DistrictMaster districtMaster,List<JobCategoryMaster> jobCategoryMasters,List<JobOrder> LstjobOrders,StatusMaster statusMaster,SecondaryStatus secondaryStatus,List<SecondaryStatus> secondaryStatuss,TeacherDetail teacherDetail,UserMaster userMaster)
	{
		List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
		try 
		{
			if(jobCategoryMasters.size()>0 && LstjobOrders.size()>0){
				List<Integer> jobCategoyList=new ArrayList<Integer>();
				for (int i=0;i<jobCategoryMasters.size();i++) {
					jobCategoyList.add(jobCategoryMasters.get(i).getJobCategoryId());
				}
				Criterion criterion_districtMaster = Restrictions.eq("districtId", districtMaster.getDistrictId());
				Criterion criterion_jobCategoryMaster = Restrictions.in("jobCategoryId",jobCategoyList);
				Criterion criterion_job = Restrictions.in("jobOrder", LstjobOrders);
				Criterion criterion_user = Restrictions.eq("userMaster", userMaster);
				Criterion criterion_teacherDetails = Restrictions.eq("teacherDetail", teacherDetail);
				
				Criterion criterionStatus=null;
				if(secondaryStatuss.size()>0){
					List<Integer> secondaryStatussList=new ArrayList<Integer>();
					for (int i=0;i<secondaryStatuss.size();i++) {
						secondaryStatussList.add(secondaryStatuss.get(i).getSecondaryStatusId());
					}
					criterionStatus = Restrictions.in("secondaryStatusId", secondaryStatussList);
				}else{
					criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
				}
				lstStatusSpecificScore=findByCriteria(Order.asc("createdDateTime"),criterion_districtMaster,criterion_jobCategoryMaster,criterion_job,criterionStatus,criterion_teacherDetails,criterion_user);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstStatusSpecificScore;
		}
	}
	@Transactional(readOnly=false)
	public boolean copyQuestionAnswerAllJobCategory(List<JobCategoryMaster> categoryMasters,List<JobOrder> jobOrderList,StatusMaster statusMaster,SecondaryStatus secondaryStatus,List<SecondaryStatus> secondaryStatuss,UserMaster userMaster,TeacherDetail teacherDetail)
	{
		boolean isReturn=true;
		List<StatusSpecificQuestions> lstStatusSpecificQuestions= new ArrayList<StatusSpecificQuestions>();
		Map<String,StatusSpecificScore> scoreMap= new HashMap<String,StatusSpecificScore>();
		try 
		{
			if(categoryMasters.size()>0){
				Criterion criterion_districtMaster = Restrictions.eq("districtId", jobOrderList.get(0).getDistrictMaster().getDistrictId());
				Criterion criterion_jobCategoryMaster = Restrictions.in("jobCategoryId", categoryMasters);
				Criterion criterionStatus=null;
				if(secondaryStatuss.size()>0){
					List<Integer> secondaryStatussList=new ArrayList<Integer>();
					for (int i=0;i<secondaryStatuss.size();i++) {
						secondaryStatussList.add(secondaryStatuss.get(i).getSecondaryStatusId());
					}
					criterionStatus = Restrictions.in("secondaryStatusId",secondaryStatussList);
				}else{
					criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
				}
				Criterion criterion_status= Restrictions.eq("status","A");
				lstStatusSpecificQuestions=statusSpecificQuestionsDAO.findByCriteria(criterion_districtMaster,criterion_jobCategoryMaster,criterionStatus,criterion_status);
			
				try{
					List<StatusSpecificScore> list= new ArrayList<StatusSpecificScore>();
					Criterion criterion_teacher= Restrictions.eq("teacherDetail",teacherDetail);
					Criterion criterion_usermster= Restrictions.eq("userMaster",userMaster);
					list=findByCriteria(criterion_districtMaster,criterion_jobCategoryMaster,criterionStatus,criterion_teacher,criterion_usermster);
					for (StatusSpecificScore statusSpecificScore : list) {
						String key=statusSpecificScore.getSecondaryStatusId()+"##"+statusSpecificScore.getJobOrder().getJobId()+"##"+statusSpecificScore.getTeacherDetail().getTeacherId();
						scoreMap.put(key, statusSpecificScore);
					}
				}catch(Exception e){}
			}
			for(JobOrder jobOrder:jobOrderList){
			
				if(lstStatusSpecificQuestions.size()>0)
				{
					int i=0;
					for(StatusSpecificQuestions pojo:lstStatusSpecificQuestions)
					{
						
						String keyCheck=pojo.getSecondaryStatusId()+"##"+jobOrder.getJobId()+"##"+teacherDetail.getTeacherId();
						if(scoreMap.get(keyCheck)==null){
							StatusSpecificScore statusSpecificScore=new StatusSpecificScore();
							statusSpecificScore.setDistrictId(pojo.getDistrictId());
							
							statusSpecificScore.setJobCategoryId(pojo.getJobCategoryId());
							statusSpecificScore.setJobOrder(jobOrder);
							
							if(pojo.getStatusId()!=null)
								statusSpecificScore.setStatusId(pojo.getStatusId());
							if(pojo.getSecondaryStatusId()!=null)
								statusSpecificScore.setSecondaryStatusId(pojo.getSecondaryStatusId());
							
							statusSpecificScore.setStatusSpecificQuestions(pojo);
							statusSpecificScore.setQuestion(pojo.getQuestion());
							if(pojo.getSkillAttributesMaster()!=null){
								statusSpecificScore.setSkillAttributesMaster(pojo.getSkillAttributesMaster());
								statusSpecificScore.setScoringCaption(pojo.getSkillAttributesMaster().getSkillName());
							}
							statusSpecificScore.setScoreProvided(0);
							statusSpecificScore.setMaxScore(pojo.getMaxScore());
							statusSpecificScore.setUserMaster(userMaster);
							statusSpecificScore.setCreatedDateTime(new Date());
							
							statusSpecificScore.setTeacherDetail(teacherDetail);
							statusSpecificScore.setFinalizeStatus(0);
							
							if(userMaster.getSchoolId()!=null)
								statusSpecificScore.setSchoolId(userMaster.getSchoolId().getSchoolId());
							
							
							makePersistent(statusSpecificScore);
							
							i++;
							if(i%3==0)
							{
								flush();
								clear();
							}
						}
					}
					//System.out.println("Copied question no. "+i);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
			isReturn=false;
		}
		finally
		{
			return isReturn;
		}
	}
	
	
	@Transactional(readOnly=false)
	public List getQuestionList_mass(JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,List<TeacherDetail> lstTeacherDetail,UserMaster userMaster)
	{
		List lstStatusSpecificScore=new ArrayList();
		try 
		{
			Criterion criterion_districtMaster = Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
			Criterion criterion_jobCategoryMaster = Restrictions.eq("jobCategoryId", jobOrder.getJobCategoryMaster().getJobCategoryId());
			Criterion criterion_job = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion_user = Restrictions.eq("userMaster", userMaster);
			Criterion criterion_teacherDetails = Restrictions.in("teacherDetail", lstTeacherDetail);
			
			//Criterion criterion_finalizeStatus = Restrictions.eq("finalizeStatus", 1);
			
			
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusId", statusMaster.getStatusId());
			}else{
				criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
			}
			SchoolMaster schoolMaster=null;
			if(userMaster.getSchoolId()!=null)
				schoolMaster=userMaster.getSchoolId();
			Criterion criterion_school = Restrictions.eq("schoolId",schoolMaster.getSchoolId());
			
			if(schoolMaster==null)
			{
				Session session = getSession();
				lstStatusSpecificScore = session.createCriteria(getPersistentClass()) 
				.add(criterion_districtMaster) 
				.add(criterion_jobCategoryMaster) 
				.add(criterion_job) 
				.add(criterion_user)
				.add(criterion_teacherDetails)
				.add(criterionStatus)
				//.add(criterion_finalizeStatus)
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.property("teacherDetail"))
						.add(Projections.count("teacherDetail")) 
				).list();
				
			}
			else
			{
				Session session = getSession();
				lstStatusSpecificScore = session.createCriteria(getPersistentClass()) 
				.add(criterion_districtMaster) 
				.add(criterion_jobCategoryMaster) 
				.add(criterion_job) 
				.add(criterion_user)
				.add(criterion_teacherDetails)
				.add(criterionStatus)
				.add(criterion_school)
				//.add(criterion_finalizeStatus)
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.property("teacherDetail"))
						.add(Projections.count("teacherDetail")) 
				).list();
			}
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstStatusSpecificScore;
		}
	}
	
	
	@Transactional(readOnly=false)
	public boolean copyQuestionAnswer_CGMass(JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster,List<TeacherDetail> lstTeacherDetail)
	{
		boolean isReturn=true;
		List<StatusSpecificQuestions> lstStatusSpecificQuestions= new ArrayList<StatusSpecificQuestions>();
		try 
		{
			Criterion criterion_districtMaster = Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
			Criterion criterion_jobCategoryMaster = Restrictions.eq("jobCategoryId", jobOrder.getJobCategoryMaster().getJobCategoryId());
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusId", statusMaster.getStatusId());
			}else{
				criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
			}
			Criterion criterion_status= Restrictions.eq("status","A");
			lstStatusSpecificQuestions=statusSpecificQuestionsDAO.findByCriteria(criterion_districtMaster,criterion_jobCategoryMaster,criterionStatus,criterion_status);
			
			if(lstTeacherDetail!=null && lstTeacherDetail.size() >0)
			{
				for(TeacherDetail teacherDetail :lstTeacherDetail)
				{
					if(lstStatusSpecificQuestions.size()>0)
					{
						int i=0;
						for(StatusSpecificQuestions pojo:lstStatusSpecificQuestions)
						{
							StatusSpecificScore statusSpecificScore=new StatusSpecificScore();
							statusSpecificScore.setDistrictId(pojo.getDistrictId());
							
							statusSpecificScore.setJobCategoryId(pojo.getJobCategoryId());
							statusSpecificScore.setJobOrder(jobOrder);
							
							if(pojo.getStatusId()!=null)
								statusSpecificScore.setStatusId(pojo.getStatusId());
							if(pojo.getSecondaryStatusId()!=null)
								statusSpecificScore.setSecondaryStatusId(pojo.getSecondaryStatusId());
							
							statusSpecificScore.setStatusSpecificQuestions(pojo);
							statusSpecificScore.setQuestion(pojo.getQuestion());
							if(pojo.getSkillAttributesMaster()!=null){
								statusSpecificScore.setSkillAttributesMaster(pojo.getSkillAttributesMaster());
								statusSpecificScore.setScoringCaption(pojo.getSkillAttributesMaster().getSkillName());
							}
							statusSpecificScore.setScoreProvided(0);
							statusSpecificScore.setMaxScore(pojo.getMaxScore());
							statusSpecificScore.setUserMaster(userMaster);
							statusSpecificScore.setCreatedDateTime(new Date());
							
							statusSpecificScore.setTeacherDetail(teacherDetail);
							statusSpecificScore.setFinalizeStatus(0);
							
							if(userMaster.getSchoolId()!=null)
								statusSpecificScore.setSchoolId(userMaster.getSchoolId().getSchoolId());
							
							
							makePersistent(statusSpecificScore);
							i++;
							if(i%3==0)
							{
								flush();
								clear();
							}
						}
						//System.out.println("Copied question no. "+i);
					}
				}
			}
			
			
			
		} 
		catch (Exception e) {
			e.printStackTrace();
			isReturn=false;
		}
		finally
		{
			return isReturn;
		}
	}
	
	
	@Transactional(readOnly=false)
	public List<StatusSpecificScore> getQuestionList_CGMass(JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,List<TeacherDetail> lstTeacherDetails,UserMaster userMaster)
	{
		List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
		try 
		{
			
			Criterion criterion_districtMaster = Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
			Criterion criterion_jobCategoryMaster = Restrictions.eq("jobCategoryId", jobOrder.getJobCategoryMaster().getJobCategoryId());
			Criterion criterion_job = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion_user = Restrictions.eq("userMaster", userMaster);
			Criterion criterion_teacherDetails = Restrictions.in("teacherDetail", lstTeacherDetails);
			
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusId", statusMaster.getStatusId());
			}else{
				criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
			}
			Criterion criterion_school =null;
			if(userMaster.getSchoolId()!=null)
				criterion_school = Restrictions.eq("schoolId", userMaster.getSchoolId().getSchoolId());
			
			if(userMaster.getSchoolId()==null)
				lstStatusSpecificScore=findByCriteria(criterion_districtMaster,criterion_jobCategoryMaster,criterion_job,criterionStatus,criterion_teacherDetails,criterion_user);
			else
				lstStatusSpecificScore=findByCriteria(criterion_districtMaster,criterion_school,criterion_jobCategoryMaster,criterion_job,criterionStatus,criterion_teacherDetails,criterion_user);
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstStatusSpecificScore;
		}
	}
	
	
	public boolean deleteSSS(List<StatusSpecificScore> lstStatusSpecificScore)
	{
		boolean bReturnValue=true;
		try {
			if(lstStatusSpecificScore!=null && lstStatusSpecificScore.size()>0)
			{
				int i=0;
				for(StatusSpecificScore specificScore:lstStatusSpecificScore)
				{
					makeTransient(specificScore);
					i++;
					if(i%3==0)
					{
						flush();
						clear();
					}
				}
				
			}
		} catch (Exception e) {
			bReturnValue=false;
		}
		finally
		{
			return bReturnValue;
		}
	}
	
	
	@Transactional(readOnly=false)
	public List<StatusSpecificScore> getQuestionFinalizeList_CGMass(JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,List<TeacherDetail> lstTeacherDetails,UserMaster userMaster)
	{
		List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
		try 
		{
			Criterion criterion_districtMaster = Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
			Criterion criterion_jobCategoryMaster = Restrictions.eq("jobCategoryId", jobOrder.getJobCategoryMaster().getJobCategoryId());
			Criterion criterion_job = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion_user = Restrictions.eq("userMaster", userMaster);
			Criterion criterion_teacherDetails = Restrictions.in("teacherDetail", lstTeacherDetails);
			
			Criterion criterion_finalizeStatus = Restrictions.eq("finalizeStatus", 1);
			
			
			Criterion criterionStatus=null;
			if(statusMaster!=null){
				criterionStatus = Restrictions.eq("statusId", statusMaster.getStatusId());
			}else{
				criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
			}
			SchoolMaster schoolMaster=null;
			if(userMaster.getSchoolId()!=null)
				schoolMaster=userMaster.getSchoolId();
			Criterion criterion_school = Restrictions.eq("schoolId", schoolMaster.getSchoolId());
			
			if(schoolMaster==null)
				lstStatusSpecificScore=findByCriteria(criterion_districtMaster,criterion_jobCategoryMaster,criterion_job,criterionStatus,criterion_teacherDetails,criterion_user,criterion_finalizeStatus);
			else
				lstStatusSpecificScore=findByCriteria(criterion_districtMaster,criterion_school,criterion_jobCategoryMaster,criterion_job,criterionStatus,criterion_teacherDetails,criterion_user,criterion_finalizeStatus);
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstStatusSpecificScore;
		}
	}
	
		@Transactional(readOnly=false)
	public List<StatusSpecificScore> getSSSByDistrictAndSecondaryStatus(Integer districtId, String secondaryStatusName)
	{
		List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
		try 
		{
			Session session=getSession();
			Criteria criteria=session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("districtId", districtId));
			criteria.createCriteria("secondaryStatusId").add(Restrictions.eq("secondaryStatusName", secondaryStatusName));
			criteria.addOrder(Order.asc("teacherDetail"));
			criteria.addOrder(Order.asc("jobOrder"));
			criteria.addOrder(Order.asc("secondaryStatusId"));
			lstStatusSpecificScore=criteria.list();
			return lstStatusSpecificScore;
		}catch (Exception e) {
			e.printStackTrace();
			return lstStatusSpecificScore;
		}
	}
	
	@Transactional(readOnly=false)
	public List<StatusSpecificScore> getQuestionList_msuByHBD(JobOrder jobOrder,List<JobOrder> LstjobOrders,StatusMaster statusMaster,SecondaryStatus secondaryStatus,TeacherDetail teacherDetail,UserMaster userMaster)
	{
		List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
		try 
		{
			if(LstjobOrders.size()>0){
				Criterion criterion_user = Restrictions.eq("userMaster", userMaster);
				Criterion criterion_teacherDetails = Restrictions.eq("teacherDetail", teacherDetail);
				
				Criterion criterionStatus=null;
				if(statusMaster!=null){
					criterionStatus = Restrictions.eq("statusId", statusMaster.getStatusId());
				}else{
					criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
				}
				
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				if(jobOrder.getDistrictMaster()!=null){
					criteria.add(Restrictions.eq("districtId",jobOrder.getDistrictMaster().getDistrictId()));
				}else{
					criteria.add(Restrictions.isNull("districtId"));
				}
				if(jobOrder.getBranchMaster()!=null){
					criteria.add(Restrictions.eq("branchId",jobOrder.getBranchMaster().getBranchId()));
				}else{
					criteria.add(Restrictions.isNull("branchId"));
				}
				if(jobOrder.getHeadQuarterMaster()!=null){
					criteria.add(Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId()));
				}else{
					criteria.add(Restrictions.isNull("headQuarterId"));
				}
				criteria.add(Restrictions.in("jobOrder", LstjobOrders));
				criteria.add(Restrictions.eq("jobCategoryId",jobOrder.getJobCategoryMaster().getJobCategoryId()));
				criteria.add(criterion_teacherDetails);
				criteria.add(Restrictions.eq("status","A"));
				criteria.addOrder(Order.asc("createdDateTime"));
				criteria.add(criterion_user);
				criteria.add(criterionStatus);
				lstStatusSpecificScore = criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstStatusSpecificScore;
		}
	}
	@Transactional(readOnly=false)
	public boolean copyQuestionAnswersForAllJobCategoryLst_msuByHBD(List<JobCategoryMaster> jobCategoryMasters,List<JobOrder> LstJobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,List<SecondaryStatus> secondaryStatuss,UserMaster userMaster,TeacherDetail teacherDetail)
	{
		boolean isReturn=true;
		Map<String,StatusSpecificScore> scoreMap= new HashMap<String,StatusSpecificScore>();
		DistrictMaster districtMaster=null;
		HeadQuarterMaster headQuarterMaster=null;
		BranchMaster branchMaster=null;
		if(LstJobOrder.size() > 0)
		{
			if(LstJobOrder.get(0).getDistrictMaster()!=null){
				districtMaster=LstJobOrder.get(0).getDistrictMaster();
			}
			if(LstJobOrder.get(0).getHeadQuarterMaster()!=null){
				headQuarterMaster=LstJobOrder.get(0).getHeadQuarterMaster();
			}
			if(LstJobOrder.get(0).getBranchMaster()!=null){
				branchMaster=LstJobOrder.get(0).getBranchMaster();
			}
			
			List<StatusSpecificQuestions> lstStatusSpecificQuestions= new ArrayList<StatusSpecificQuestions>();
			if(jobCategoryMasters.size()>0){
				List<Integer> jobCategoyList=new ArrayList<Integer>();
				for (int i=0;i<jobCategoryMasters.size();i++) {
					jobCategoyList.add(jobCategoryMasters.get(i).getJobCategoryId());
				}
				Criterion criterion_jobCategoryMaster = Restrictions.in("jobCategoryId", jobCategoyList);
				Criterion criterionStatus=null;
				if(secondaryStatuss.size()>0){
					List<Integer> secondaryStatussList=new ArrayList<Integer>();
					for (int i=0;i<secondaryStatuss.size();i++) {
						secondaryStatussList.add(secondaryStatuss.get(i).getSecondaryStatusId());
					}
					criterionStatus = Restrictions.in("secondaryStatusId", secondaryStatussList);
				}else{
					criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
				}
				
				Criterion criterion_status= Restrictions.eq("status","A");
				
				
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId",districtMaster.getDistrictId()));
				}else{
					criteria.add(Restrictions.isNull("districtId"));
				}
				if(branchMaster!=null){
					criteria.add(Restrictions.eq("branchId",branchMaster.getBranchId()));
				}else{
					criteria.add(Restrictions.isNull("branchId"));
				}
				if(headQuarterMaster!=null){
					criteria.add(Restrictions.eq("headQuarterId",headQuarterMaster.getHeadQuarterId()));
				}else{
					criteria.add(Restrictions.isNull("headQuarterId"));
				}
				criteria.add(criterion_jobCategoryMaster);
				criteria.add(criterionStatus);
				criteria.add(criterion_status);
				lstStatusSpecificQuestions = criteria.list();

				System.out.println("lstStatusSpecificQuestions:::::::::::::::::::::"+lstStatusSpecificQuestions.size());
				
				try{
					List<StatusSpecificScore> list= new ArrayList<StatusSpecificScore>();
					Criterion criterion_teacher= Restrictions.eq("teacherDetail",teacherDetail);
					Criterion criterion_usermster= Restrictions.eq("userMaster",userMaster);
					Criteria criteriaNew = session.createCriteria(getPersistentClass());
					if(districtMaster!=null){
						criteriaNew.add(Restrictions.eq("districtId",districtMaster.getDistrictId()));
					}else{
						criteriaNew.add(Restrictions.isNull("districtId"));
					}
					if(branchMaster!=null){
						criteriaNew.add(Restrictions.eq("branchId",branchMaster.getBranchId()));
					}else{
						criteriaNew.add(Restrictions.isNull("branchId"));
					}
					if(headQuarterMaster!=null){
						criteriaNew.add(Restrictions.eq("headQuarterId",headQuarterMaster.getHeadQuarterId()));
					}else{
						criteriaNew.add(Restrictions.isNull("headQuarterId"));
					}
					criteriaNew.add(criterion_jobCategoryMaster);
					criteriaNew.add(criterionStatus);
					criteriaNew.add(criterion_status);
					criteriaNew.add(criterion_teacher);
					criteriaNew.add(criterion_usermster);
					lstStatusSpecificQuestions = criteriaNew.list();
					for (StatusSpecificScore statusSpecificScore : list) {
						String key=statusSpecificScore.getSecondaryStatusId()+"##"+statusSpecificScore.getJobOrder().getJobId()+"##"+statusSpecificScore.getTeacherDetail().getTeacherId();
						scoreMap.put(key, statusSpecificScore);
					}
				}catch(Exception e){}
			
			}
			for(JobOrder jobOrder:LstJobOrder)
			{
				try 
				{
					if(lstStatusSpecificQuestions.size()>0)
					{
						int i=0;
						for(StatusSpecificQuestions pojo:lstStatusSpecificQuestions)
						{
							String keyCheck=pojo.getSecondaryStatusId()+"##"+jobOrder.getJobId()+"##"+teacherDetail.getTeacherId();
							if(scoreMap.get(keyCheck)==null){
								if(jobOrder.getJobCategoryMaster().getJobCategoryId().equals(pojo.getJobCategoryId())){
									StatusSpecificScore statusSpecificScore=new StatusSpecificScore();
									if(pojo.getDistrictId()!=null){
										statusSpecificScore.setDistrictId(pojo.getDistrictId());
									}
									
									statusSpecificScore.setJobCategoryId(pojo.getJobCategoryId());
									statusSpecificScore.setJobOrder(jobOrder);
									
									if(pojo.getStatusId()!=null)
										statusSpecificScore.setStatusId(pojo.getStatusId());
									if(pojo.getSecondaryStatusId()!=null)
										statusSpecificScore.setSecondaryStatusId(pojo.getSecondaryStatusId());
									
									statusSpecificScore.setStatusSpecificQuestions(pojo);
									statusSpecificScore.setQuestion(pojo.getQuestion());
									if(pojo.getSkillAttributesMaster()!=null){
										statusSpecificScore.setSkillAttributesMaster(pojo.getSkillAttributesMaster());
										statusSpecificScore.setScoringCaption(pojo.getSkillAttributesMaster().getSkillName());
									}
									statusSpecificScore.setScoreProvided(0);
									statusSpecificScore.setMaxScore(pojo.getMaxScore());
									statusSpecificScore.setUserMaster(userMaster);
									statusSpecificScore.setCreatedDateTime(new Date());
									
									statusSpecificScore.setTeacherDetail(teacherDetail);
									statusSpecificScore.setFinalizeStatus(0);
									
									if(userMaster.getSchoolId()!=null)
										statusSpecificScore.setSchoolId(userMaster.getSchoolId().getSchoolId());
									
									
									makePersistent(statusSpecificScore);
									i++;
									if(i%3==0)
									{
										flush();
										clear();
									}
								}
							}
						}
						//System.out.println("Copied question no. "+i);
					}
				} 
				catch (Exception e) {
					e.printStackTrace();
					isReturn=false;
				}
			}
		}
		
		return isReturn;
	}
	@Transactional(readOnly=false)
	public List<StatusSpecificScore> getQuestionAllJobCategoryList_msuBYHBD(JobOrder  jobOrder,List<JobCategoryMaster> jobCategoryMasters,List<JobOrder> LstjobOrders,StatusMaster statusMaster,SecondaryStatus secondaryStatus,List<SecondaryStatus> secondaryStatuss,TeacherDetail teacherDetail,UserMaster userMaster)
	{
		List<StatusSpecificScore> lstStatusSpecificScore=new ArrayList<StatusSpecificScore>();
		try 
		{
			if(jobCategoryMasters.size()>0 && LstjobOrders.size()>0){
				List<Integer> jobCategoyList=new ArrayList<Integer>();
				for (int i=0;i<jobCategoryMasters.size();i++) {
					jobCategoyList.add(jobCategoryMasters.get(i).getJobCategoryId());
				}
				Criterion criterion_jobCategoryMaster = Restrictions.in("jobCategoryId",jobCategoyList);
				Criterion criterion_job = Restrictions.in("jobOrder", LstjobOrders);
				Criterion criterion_user = Restrictions.eq("userMaster", userMaster);
				Criterion criterion_teacherDetails = Restrictions.eq("teacherDetail", teacherDetail);
				
				Criterion criterionStatus=null;
				if(secondaryStatuss.size()>0){
					List<Integer> secondaryStatussList=new ArrayList<Integer>();
					for (int i=0;i<secondaryStatuss.size();i++) {
						secondaryStatussList.add(secondaryStatuss.get(i).getSecondaryStatusId());
					}
					criterionStatus = Restrictions.in("secondaryStatusId", secondaryStatussList);
				}else{
					criterionStatus = Restrictions.eq("secondaryStatusId", secondaryStatus.getSecondaryStatusId());
				}
				
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				if(jobOrder.getDistrictMaster()!=null){
					criteria.add(Restrictions.eq("districtId",jobOrder.getDistrictMaster().getDistrictId()));
				}else{
					criteria.add(Restrictions.isNull("districtId"));
				}
				if(jobOrder.getBranchMaster()!=null){
					criteria.add(Restrictions.eq("branchId",jobOrder.getBranchMaster().getBranchId()));
				}else{
					criteria.add(Restrictions.isNull("branchId"));
				}
				if(jobOrder.getHeadQuarterMaster()!=null){
					criteria.add(Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId()));
				}else{
					criteria.add(Restrictions.isNull("headQuarterId"));
				}
				criteria.add(criterion_jobCategoryMaster);
				criteria.add(criterion_job);
				criteria.add(criterion_user);
				criteria.add(criterion_teacherDetails);
				criteria.add(criterionStatus);
				criteria.add(Restrictions.eq("status","A"));
				criteria.addOrder(Order.asc("createdDateTime"));
				lstStatusSpecificScore = criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstStatusSpecificScore;
		}
	}
}




