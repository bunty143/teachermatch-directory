package tm.dao.master;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.python.core.exceptions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.master.DemoClassAttendees;
import tm.bean.master.DemoClassNotesDetails;
import tm.bean.master.DemoClassSchedule;
import tm.bean.master.PanelAttendees;
import tm.bean.master.PanelSchedule;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class PanelAttendeesDAO extends
		GenericHibernateDAO<PanelAttendees, java.lang.Integer> {
	public PanelAttendeesDAO() {
		super(PanelAttendees.class);
	}
	
	@Transactional(readOnly=true)
	public List<PanelAttendees> getPanelAttendees(PanelSchedule panelSchedule){
		List<PanelAttendees> list=null;
		try{
			Criteria  criteria=getSession().createCriteria(getPersistentClass());
			list=criteria.add(Restrictions.eq("panelSchedule", panelSchedule)).list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	@Transactional(readOnly=true)
	public List<PanelAttendees> getPanelAttendeeList(List<PanelSchedule> panelScheduleList){
		List<PanelAttendees> panelAttendeesList=new ArrayList<PanelAttendees>();
		try
		{
			Criterion criterion = Restrictions.in("panelSchedule", panelScheduleList);
			panelAttendeesList=findByCriteria(criterion);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return panelAttendeesList;
	}
	
	@Transactional(readOnly=true)
	public List<PanelAttendees> getPanelAttendee(PanelSchedule panelSchedule,UserMaster userMaster){
		List<PanelAttendees> list=null;
		try{
			Criteria  criteria=getSession().createCriteria(getPersistentClass());
			list=criteria.add(Restrictions.eq("panelSchedule", panelSchedule)).add(Restrictions.eq("panelInviteeId", userMaster)).list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	/*@Transactional(readOnly=true)
	public List<PanelSchedule> getMyDemoInviteeSchedule(UserMaster userMaster,Criterion... criterion){
		List<PanelSchedule> myInvitelist=new ArrayList<PanelSchedule>();
		try{
			Criteria criteria=getSession().createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			criteria.add(Restrictions.eq("inviteeId",userMaster));
			criteria.setProjection(Projections.distinct(Projections.property("demoId")));
			
			myInvitelist=criteria.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			return myInvitelist;
		}
		
	}*/
	@Transactional(readOnly=true)
	public List<PanelAttendees> getPanelAttendeesOp(PanelSchedule panelSchedule){
		List<PanelAttendees> list=new ArrayList<PanelAttendees>();
		try{
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());			
			criteria.setProjection(Projections.projectionList()					
					.add(Projections.property("panelattendeeId"))
					.add(Projections.property("panelSchedule.panelId"))
					.add(Projections.property("panelInviteeId.userId"))
					.add(Projections.property("hostId.userId"))
					);
			criteria.add(Restrictions.eq("panelSchedule", panelSchedule));
			List<String []> panelAttendeesTemp=new ArrayList<String []>();
			panelAttendeesTemp= criteria.list();
			
			for (Iterator it = panelAttendeesTemp.iterator(); it.hasNext();)
			{				
				Object[] row = (Object[]) it.next();    				
				if(row[0]!=null){
					PanelAttendees panelAttendees = new PanelAttendees();
					panelAttendees.setPanelattendeeId(Integer.parseInt(row[0].toString()));
					panelAttendees.setPanelSchedule(panelSchedule);					
					if(row[2]!=null){
						UserMaster userMaster = new UserMaster();
						userMaster.setUserId(Integer.parseInt(row[2].toString()));
						panelAttendees.setPanelInviteeId(userMaster);
					}
					if(row[3]!=null){
						UserMaster userMaster = new UserMaster();
						userMaster.setUserId(Integer.parseInt(row[3].toString()));
						panelAttendees.setHostId(userMaster);
					}
					list.add(panelAttendees);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}	
	
}
