package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DistrictChatSupport;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DspqFieldMaster;
import tm.bean.master.DspqSectionMaster;

import tm.dao.generic.GenericHibernateDAO;

public class DistrictChatSupportDAO extends GenericHibernateDAO<DistrictChatSupport, Integer> 
{
	public DistrictChatSupportDAO() 
	{
		super(DistrictChatSupport.class);
	}	
	@Transactional(readOnly=false)
	public DistrictChatSupport getDistrictChatSupportByDistrictId(DistrictMaster districtMaster)
	{
		List<DistrictChatSupport> districtChatSupportList = new ArrayList<DistrictChatSupport>();
		DistrictChatSupport districtChatSupport=null;
		Criterion criterion = Restrictions.eq("status", "A");
		try
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);			
			districtChatSupportList=findByCriteria(criterion,criterion1);
			if(districtChatSupportList.size()>0)
				districtChatSupport=districtChatSupportList.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return districtChatSupport;
	}
}
