package tm.dao.master;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.FutureDistrictMaster;
import tm.bean.master.StateMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictMasterDAO extends GenericHibernateDAO<DistrictMaster, Integer> 
{
	public DistrictMasterDAO() 
	{
		super(DistrictMaster.class);
	}
	
	@Transactional(readOnly=true)
	public List<DistrictMaster> findDistrictByZip(String zipCode)
	{
		List <DistrictMaster> lstDistrictMaster = null;
		try {
			Criterion criterion = Restrictions.eq("zipCode",zipCode);
			lstDistrictMaster = findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
			lstDistrictMaster = new ArrayList<DistrictMaster>();
		}
        return lstDistrictMaster;
	}
	
	
	/* @Start
	 * @Ashish Kuamr
	 * @Description :: Find District By ZipCode List
	 * */
	@Transactional(readOnly=true)
	public List<DistrictMaster> findDistrictByZipCodeList(List<String> zipCode)
	{
		List <DistrictMaster> lstDistrictMaster = null;
		try {
			Criterion criterion = Restrictions.in("zipCode",zipCode);
			lstDistrictMaster = findByCriteria(criterion);
			System.out.println(" findDistrictByZipCodeList ::::::::    "+lstDistrictMaster.size());
		} catch (Exception e) {
			e.printStackTrace();
			lstDistrictMaster = new ArrayList<DistrictMaster>();
		}
	    return lstDistrictMaster;
	}
	
	@Transactional(readOnly=true)
	public List<DistrictMaster> findDistrictByZipCode(String zipCode)
	{
		List <DistrictMaster> lstDistrictMaster = null;
		try {
			Criterion criterion = Restrictions.eq("zipCode",zipCode);
			lstDistrictMaster = findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
			lstDistrictMaster = new ArrayList<DistrictMaster>();
		}
        return lstDistrictMaster;
	}
	
	
	/* @End
	 * @Ashish Kuamr
	 * @Description :: Find District By ZipCode List
	 * */
	
	
	
	
	@Transactional(readOnly=false)
	public DistrictMaster validateDistrictByAuthkey(String authKey)
	{
		List<DistrictMaster> lstDistrictMaster = null;
		try 
		{		
			Criterion criterion2 = Restrictions.eq("authKey",authKey);
				lstDistrictMaster = findByCriteria(criterion2);
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstDistrictMaster==null || lstDistrictMaster.size()==0)
			return null;
		else
			return lstDistrictMaster.get(0);	
	}
	
	@Transactional(readOnly=false)
	public List<DistrictMaster> findDistrictsWeeklyCgReportRequired(Integer val)
	{
		List<DistrictMaster> lstDistrictMaster = null;
		try 
		{		
			Criterion criterion2 = Restrictions.eq("isWeeklyCgReport",val);
				lstDistrictMaster = findByCriteria(criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictMaster;
	}
	@Transactional(readOnly=false)
	public String getDistributionEmail(DistrictMaster districtMaster)
	{
		String email=null;		
		List<DistrictMaster> lstDistrictMaster = new ArrayList<DistrictMaster>();
		try 
		{		
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster.getDistrictId());
			lstDistrictMaster = findByCriteria(criterion2);
			if(lstDistrictMaster.size()!=0){
				email=lstDistrictMaster.get(0).getDistributionEmail();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return email;
	}
	
	
	@Transactional(readOnly=true)
	public List<DistrictMaster> findActiveDistrictMaster()
	{
		System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>    findActiveDistrictMaster   >>>>>>>>>>>>>>>>>>>>>>>>");
		
		List <DistrictMaster> lstDistrictMaster = null;
		try {
			
			System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>    findActiveDistrictMaster   >>>>>>>111111111111>>>>>>>>>>>>>>>>>");
			
			Criterion criterion = Restrictions.eq("status","A");
			lstDistrictMaster = findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
			lstDistrictMaster = new ArrayList<DistrictMaster>();
		}
        return lstDistrictMaster;
	}
	
	@Transactional(readOnly=true)
	public List<DistrictMaster> getDistrictListByState(StateMaster stateMaster)
	{
		
		List <DistrictMaster> lstDistrictMaster = null;
		try {
			Criterion criterion = Restrictions.eq("status","A");
			Criterion criterion1 = Restrictions.eq("stateId",stateMaster);
			lstDistrictMaster = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
			lstDistrictMaster = new ArrayList<DistrictMaster>();
		}
        return lstDistrictMaster;
	}
	@Transactional(readOnly=false)	
	public DistrictMaster findByDistrictId(String id)
	{
		
		List<DistrictMaster> districtMaster=null;
		Criterion criterion=Restrictions.eq("districtId",Integer.valueOf(id));
		districtMaster=findByCriteria(criterion);
		System.out.println(districtMaster.get(0).getDistrictName());
		return districtMaster.get(0);
	}
	
	@Transactional(readOnly=false)
	public List<DistrictMaster> findDistrictsReminderRequired(Boolean val)
	{
		List<DistrictMaster> lstDistrictMaster = new ArrayList<DistrictMaster>();
		try 
		{		
			Criterion criterion2 = Restrictions.eq("sendReminderToIcompCandiates",val);
				lstDistrictMaster = findByCriteria(criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictMaster;
	}
	
	@Transactional(readOnly=true)
	public DistrictMaster findDistrictByLocationCode(String locationCode)
	{
		DistrictMaster districtMaster = null;
		List <DistrictMaster> lstDistrictMaster = new ArrayList<DistrictMaster>();
		try {
			Criterion criterion = Restrictions.eq("locationCode",locationCode);
			lstDistrictMaster = findByCriteria(criterion);
			if(lstDistrictMaster.size()>0)
				districtMaster = lstDistrictMaster.get(0);
				
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return districtMaster;
	}
	
	@Transactional(readOnly=true)
	public List<DistrictMaster> getDistrictListByBranches(List<String> branchNames)
	{
		
		List <DistrictMaster> lstDistrictMaster = null;
		try {
			Criterion criterion = Restrictions.eq("status","A");
			Criterion criterion1 = Restrictions.in("districtName",branchNames);
			lstDistrictMaster = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
			lstDistrictMaster = new ArrayList<DistrictMaster>();
		}
        return lstDistrictMaster;
	}
	
	@Transactional(readOnly=true)
	public List<DistrictMaster> getDistrictListByHeadQuater(HeadQuarterMaster headQuarterMaster)
	{
		
		List <DistrictMaster> lstDistrictMaster = null;
		try {
			Criterion criterion = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			lstDistrictMaster = findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
			lstDistrictMaster = new ArrayList<DistrictMaster>();
		}
        return lstDistrictMaster;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictMaster> findDistrictsReminderForPortfolio(Boolean val)
	{
		List<DistrictMaster> lstDistrictMaster = new ArrayList<DistrictMaster>();
		try 
		{		
			Criterion criterion2 = Restrictions.eq("sendReminderForPortfolio",val);
				lstDistrictMaster = findByCriteria(criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstDistrictMaster;
	}
	 
	@Transactional(readOnly=true)
	public List<DistrictMaster> getActiveDistrictListByHeadQuater(HeadQuarterMaster headQuarterMaster)
	{
		List <DistrictMaster> lstDistrictMaster = new  ArrayList<DistrictMaster>();
		try {
			Criterion criterion = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			lstDistrictMaster = findByCriteria(criterion,Restrictions.eq("status","A"));
		} catch (Exception e) {
			e.printStackTrace();
			lstDistrictMaster = new ArrayList<DistrictMaster>();
		}
        return lstDistrictMaster;
	}
	
	@Transactional(readOnly=true)
	public DistrictMaster getDistrictMasterByDistrictId_Op(Integer districtId)
	{
		DistrictMaster districtMaster=new DistrictMaster();
		try {
			Session session = getSession();
			districtMaster = (DistrictMaster) session.createCriteria(DistrictMaster.class).add(Restrictions.eq("districtId", districtId)).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			 
		}
        return districtMaster;
	}
	 
	@Transactional(readOnly=false)
	 public List<FutureDistrictMaster>  getFutureDistrictListByDistrictName(String districtName)
	 {
		List<FutureDistrictMaster> districtMasterList=new ArrayList<FutureDistrictMaster>();
	     String hql="";
	     List<Object[]> objList=null;
	  try{
	       Query  query=null;
	       Session session = getSession();
	          hql = " select distinct fd.districtId,fd.districtName  FROM futuredistrictmaster fd WHERE fd.status='I' and fd.districtName LIKE '"+districtName+"%' limit 0,10  " ; 
	         query=session.createSQLQuery(hql);
	         
	         objList = query.list();
	         FutureDistrictMaster futureDistrictMaster=null;
	         if(objList.size()>0){
              for(int j = 0; j < objList.size(); j++){
           	   futureDistrictMaster=new FutureDistrictMaster();
           	   Object[] objArr2 = objList.get(j);
           	   if(objArr2[0]!=null){
           		   futureDistrictMaster.setDistrictId(objArr2[0] == null ? null : Integer.parseInt(objArr2[0].toString())); 
         	           }
         	           if(objArr2[1]!=null){
         	        	 futureDistrictMaster.setDistrictName(objArr2[1] == null ? null :  objArr2[1].toString());
         	           }
         	         districtMasterList.add(futureDistrictMaster) ; 
	            }
	           }
	          }
	   catch(Exception e){
	    e.printStackTrace();
	  }
	   return districtMasterList;
	  } 
   
	@Transactional(readOnly=false)
	 public JSONArray[] activeNewDistrictDetails(Order order,int startPos,int limit, String districtName)
	 {
		JSONArray jsonArray = new JSONArray();
		JSONArray jsonCountArray = new JSONArray();
		JSONArray[] returnJsonArray = new JSONArray[2];
		Connection connection =null;
		int totalCount=0;
		List<DistrictMaster> districtMasterList=new ArrayList<DistrictMaster>();
	    String queryString="";
	    List<Object[]> objList=null;
	    try{
	       Query  query=null;
	       Session session = getSession();
	       queryString = " SELECT SQL_CALC_FOUND_ROWS districtId, districtName, dmName, createdDateTime, STATUS,(SELECT stateName FROM statemaster WHERE stateid=fdm.stateid) AS stateName, totalNoOfTeachers, totalNoOfStudents, " +
	          		" +  totalNoOfSchools FROM futuredistrictmaster fdm WHERE STATUS='I' " ;
	       
	       if(districtName!=null && !districtName.equals("")){
	    	   queryString=queryString +" and fdm.districtname like '%"+districtName+"%' ";
	       }
	       queryString = queryString+" limit "+startPos+","+limit;
	       System.out.println("queryString    "+queryString);
	       
				SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) getSessionFactory();
				ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
				connection = connectionProvider.getConnection();
				PreparedStatement ps=connection.prepareStatement(queryString,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);
		
				PreparedStatement psCount=connection.prepareStatement("SELECT FOUND_ROWS()",ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);
		
				ResultSet rs=ps.executeQuery();
				ResultSet rsCount=psCount.executeQuery();	
				while(rsCount.next()){
					totalCount=rsCount.getInt(1);
				}
				rs.last();
				int rowcount=rs.getRow();
				
				rs.beforeFirst();
				int cnt=0;
				ResultSetMetaData rsmd = rs.getMetaData();
				if(rs.next()){
					do{  	
						JSONObject jsonRec = new JSONObject();					
						for (int i = 1; i <=rsmd.getColumnCount(); i++) {
							String name = rsmd.getColumnName(i);
							jsonRec.put(name, rs.getString(name));
						}
		
						jsonArray.add(jsonRec);
					}while(rs.next());
				}
		
				JSONObject jsonCount = new JSONObject();
				jsonCount.put("totalCount", totalCount);
				jsonCountArray.add(jsonCount);
				}catch (Exception e) {
					e.printStackTrace();
				}
				finally{
					if(connection!=null)
						try {connection.close();} catch (SQLException e) {	e.printStackTrace();}
				}

				returnJsonArray[0] = jsonArray;
				returnJsonArray[1] = jsonCountArray;
				return returnJsonArray;
	  } 
	
	@Transactional(readOnly=false)
	 public boolean updateActiveNewDistrict(DistrictMasterDAO districtMasterDAO, int districtId)
	 {
		boolean updateFlag = false;
		 
		try{ 
			 Session session = getSession();
			Query query = session.createSQLQuery(
			"CALL insertDistrictMaster(:districtId)")
			.addEntity(FutureDistrictMaster.class)
			.setParameter("districtId",districtId);
			int value=query.executeUpdate(); 
			if(value!=0){
			   	String	 hql = " update futuredistrictmaster set status='A'  WHERE districtId = " +districtId ; 
			   	Query query1 	=session.createSQLQuery(hql);
			   	query1.executeUpdate();
			}
	 		updateFlag=true;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		  
			
	 
		return updateFlag;
	 }

	private void setParameter(String string, String string2) {
		// TODO Auto-generated method stub
		
	}
	
}
