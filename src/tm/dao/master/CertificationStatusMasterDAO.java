package tm.dao.master;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.CertificationStatusMaster;
import tm.dao.generic.GenericHibernateDAO;

public class CertificationStatusMasterDAO extends GenericHibernateDAO<CertificationStatusMaster, Integer> 
{
	public CertificationStatusMasterDAO() 
	{
		super(CertificationStatusMaster.class);
	}
	
	
	@Transactional(readOnly=true)
	public List<CertificationStatusMaster> findAllCertificationStatusMasterByOrder()
	{
		List<CertificationStatusMaster> lstCertificationStatusMaster= null;
		try 
		{
			Criterion criterion = Restrictions.eq("status", "A");
			lstCertificationStatusMaster = findByCriteria(Order.asc("certificationStatusName"),criterion);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstCertificationStatusMaster;
	}

}
