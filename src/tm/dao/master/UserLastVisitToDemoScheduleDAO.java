package tm.dao.master;


import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.UserLastVisitToDemoSchedule;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class UserLastVisitToDemoScheduleDAO extends GenericHibernateDAO<UserLastVisitToDemoSchedule, Integer> {
	public UserLastVisitToDemoScheduleDAO() {
		super(UserLastVisitToDemoSchedule.class);
	}
	
	@Transactional(readOnly=false)
	public UserLastVisitToDemoSchedule getuserVisitID(DistrictMaster districtMaster,SchoolMaster schoolMaster,UserMaster userMaster)
	{
		List<UserLastVisitToDemoSchedule> list=null;
		try{
			Criterion criterion1=Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2=Restrictions.eq("schoolMaster", schoolMaster);
			Criterion criterion3=Restrictions.or(criterion1, criterion2);
			
			Criterion criterion4=Restrictions.eq("userMaster", userMaster);
			Criterion criterion=Restrictions.and(criterion3, criterion4);
			list=findByCriteria(criterion);
			System.out.println("No of user in visit list :111>>>>"+list.size());
		}catch (Exception e) {
			e.printStackTrace();
		}
		if(list.size()>0)
			return list.get(0);
		else
			return null;
	}
}
