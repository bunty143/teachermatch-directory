package tm.dao.master;

import tm.bean.master.EthnicOriginMaster;
import tm.dao.generic.GenericHibernateDAO;

public class EthnicOriginMasterDAO extends GenericHibernateDAO<EthnicOriginMaster,Integer>
{
	public EthnicOriginMasterDAO()
	{
		super(EthnicOriginMaster.class);
	}
}
