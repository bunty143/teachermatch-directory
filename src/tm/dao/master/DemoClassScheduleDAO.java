package tm.dao.master;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.master.DemoClassSchedule;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.UserLastVisitToDemoSchedule;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class DemoClassScheduleDAO extends GenericHibernateDAO<DemoClassSchedule, Integer> {
	public DemoClassScheduleDAO() {
		super(DemoClassSchedule.class);
	}
	@Autowired
	private UserLastVisitToDemoScheduleDAO userLastVisitToDemoScheduleDAO;
	public void setUserLastVisitToDemoScheduleDAO(
			UserLastVisitToDemoScheduleDAO userLastVisitToDemoScheduleDAO) {
		this.userLastVisitToDemoScheduleDAO = userLastVisitToDemoScheduleDAO;
	}
	@Transactional(readOnly=false)
	public List<DemoClassSchedule> getDemoscheduleRecord(DistrictMaster districtMaster,Date fromDate,Date toDate)
	{
		List<DemoClassSchedule> list=null;
		try{
			Criterion criterion1=Restrictions.between("demoDate",fromDate,toDate);
			Criterion criterion2=Restrictions.eq("districtId",districtMaster);
			Criterion criterion3=Restrictions.ne("demoStatus","Cancelled");
			Criterion criterion4=Restrictions.and(criterion1, criterion2);
			Criterion criterion5=Restrictions.and(criterion3, criterion4);
			list=findByCriteria(criterion5);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	@Transactional(readOnly=false)
	public List<DemoClassSchedule> findDemoClassSchedules(List<TeacherDetail> teacherDetails, JobOrder jobOrder)
	{	
		List<DemoClassSchedule> demoClassScheduleList= new ArrayList<DemoClassSchedule>();	
		try{
			if(teacherDetails.size()>0)
			{
				Criterion criterion = Restrictions.in("teacherId",teacherDetails);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
				demoClassScheduleList = findByCriteria(criterion,criterion1);
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return demoClassScheduleList;
	}
	
	@Transactional(readOnly=false)
	public Boolean getUserDemoDateTime(DistrictMaster districtMaster,SchoolMaster schoolMaster,UserMaster userMaster,Date visitedDateTime)
	{
		List<DemoClassSchedule> list=null;
		Date userLastVisitDate=new Date();
		UserLastVisitToDemoSchedule lastVisitToDemoSchedule=null;
		try{
			lastVisitToDemoSchedule=userLastVisitToDemoScheduleDAO.getuserVisitID(districtMaster, schoolMaster, userMaster);
			if(lastVisitToDemoSchedule!=null){
				userLastVisitDate=lastVisitToDemoSchedule.getVisitedDateTime();
			}
			Criterion criterion1=Restrictions.eq("districtId", districtMaster);
			Criterion criterion2=Restrictions.eq("schoolId", schoolMaster);
			Criterion criterion3=Restrictions.or(criterion1, criterion2);
			Criterion criterion4=Restrictions.ge("demoStandardTime", userLastVisitDate);
			Criterion criterion5=Restrictions.and(criterion3, criterion4); 
			list=findByCriteria(criterion5);
			if(list.size()>0){
				return true;	
			}else{
				return false;
			} 
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Transactional(readOnly=false)
	public Map<Integer,String> findDemoClassSchedulesTeacherWise(TeacherDetail teacherDetail, List<JobOrder> jobOrders)
	{	
		
		Map<Integer,String> scoremap = new HashMap<Integer, String>();
		try{
			if(jobOrders.size()>0)
			{
				List<DemoClassSchedule> demoClassScheduleList= new ArrayList<DemoClassSchedule>();
				Criterion criterion = Restrictions.eq("teacherId",teacherDetail);
				Criterion criterion1 = Restrictions.in("jobOrder", jobOrders);
				demoClassScheduleList = findByCriteria(criterion,criterion1);
				
				String demoStatus = null;
				if(demoClassScheduleList!=null)
					for(DemoClassSchedule demoClassSchedule: demoClassScheduleList){
						if(demoClassSchedule.getTeacherId().equals(teacherDetail)){
							if(demoClassSchedule.getDemoStatus().equalsIgnoreCase("Scheduled")){
								demoStatus=Utility.convertDateAndTimeToUSformatOnlyDate(demoClassSchedule.getDemoDate())+" "+demoClassSchedule.getDemoTime()+" "+demoClassSchedule.getTimeFormat();	
							}else if(demoClassSchedule.getDemoStatus().equalsIgnoreCase("Completed")){
								demoStatus="Completed";
							}else{
								demoStatus="N/A";
							}
							scoremap.put(demoClassSchedule.getJobOrder().getJobId(), demoStatus);
						}
					}	
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return scoremap;
	}
	@Transactional(readOnly=false)
	public List<DemoClassSchedule> findDemoClassSchedulesByTeacherAndJob(TeacherDetail teacherDetail, JobOrder jobOrder,DistrictMaster districtMaster,SchoolMaster schoolMaster)
	{	
		List<DemoClassSchedule> demoClassScheduleList= new ArrayList<DemoClassSchedule>();	
		try{
			
			Criterion criterion = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion2 = Restrictions.eq("districtId", districtMaster);
			Criterion criterion3 = Restrictions.ne("demoStatus", "Cancelled");
			//boolean writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
			boolean writePrivilegeToSchool = jobOrder.getWritePrivilegeToSchool();
			System.out.println("writePrivilegeToSchool: "+writePrivilegeToSchool);
			if(schoolMaster==null || writePrivilegeToSchool)
				demoClassScheduleList = findByCriteria(criterion,criterion1,criterion2,criterion3);
			else
				demoClassScheduleList = findByCriteria(criterion,criterion1,criterion2,criterion3,Restrictions.eq("schoolId", schoolMaster));
			
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return demoClassScheduleList;
	}
	@Transactional(readOnly=false)
	public List<DemoClassSchedule> findDemoClassSchedules_Opp(List<TeacherDetail> teacherDetails, JobOrder jobOrder)
	{	
		List<DemoClassSchedule> demoClassScheduleList= new ArrayList<DemoClassSchedule>();	
		List<Object[]>  objList=null;
 		try{
		if(teacherDetails.size()>0)
			{
			String teacherDetailsList ="";
		  int p=0;
 	  		for(TeacherDetail teacherDetail: teacherDetails){
				if(p==0){
					teacherDetailsList=teacherDetail.getTeacherId()+"";
				}else{
					teacherDetailsList+=","+teacherDetail.getTeacherId()+"";
				}
				p++; 
			}
			  Query  query=null;
		       Session session = getSession();
		     	String hql = " select  dcs.demoId,dcs.demoTime,dcs.timeFormat,dcs.demoStatus, dcs.teacherId,dcs.jobId  from  democlassschedule dcs "
		     		 + " left join teacherdetail td on td.teacherId=dcs.teacherId "
		     		  +  " Where  dcs.teacherId in ("+teacherDetailsList+")  and dcs.jobId = '"+jobOrder.getJobId()+"' " ;
		            query=session.createSQLQuery(hql);
	 		        objList = query.list();
			        DemoClassSchedule demoClassSchedule=null;
			        TeacherDetail teacherDetail=null;
			        JobOrder jobOrder1=null;
			        if(objList.size()>0){
			           for (int j = 0; j < objList.size(); j++) {
			       	    Object[] objArr2 = objList.get(j);
			       	    demoClassSchedule = new DemoClassSchedule();
			       	    teacherDetail=new TeacherDetail();
			       	     jobOrder1  =new JobOrder();
				        if(objArr2[0]!=null)
				        	demoClassSchedule.setDemoId(objArr2[0] == null ? null : Integer.parseInt(objArr2[0].toString()));
				        if(objArr2[1]!=null)
				        	demoClassSchedule.setDemoTime(objArr2[1] == null ? "NA" :  objArr2[1].toString());
				        if(objArr2[2]!=null)
				        	demoClassSchedule.setTimeFormat(objArr2[2] == null ? "NA" :  objArr2[2].toString());
				        if(objArr2[3]!=null)
				        	demoClassSchedule.setDemoStatus(objArr2[3] == null ? "NA" :  objArr2[3].toString());
				        if(objArr2[4]!=null){
				           teacherDetail.setTeacherId(objArr2[4] == null ? null : Integer.parseInt(objArr2[4].toString()));
				           demoClassSchedule.setTeacherId(teacherDetail);
				          }
				        if(objArr2[5]!=null){
				           jobOrder1.setJobId(objArr2[5] == null ? null : Integer.parseInt(objArr2[5].toString()));
				           demoClassSchedule.setJobOrder(jobOrder1);
				           }
				        demoClassScheduleList.add(demoClassSchedule);
			           }
			          }	
			     }
 		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return demoClassScheduleList;
	}	
	
}
