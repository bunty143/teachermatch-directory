package tm.dao.master;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.QuestionTypeMaster;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Vishwanath Kumar
 * @Discription: QuestionTypeMaster DAO.
 */
public class QuestionTypeMasterDAO extends GenericHibernateDAO<QuestionTypeMaster, Integer>
{
	public QuestionTypeMasterDAO() {
		super(QuestionTypeMaster.class);
	}
	
/* @Author: Vishwanath Kumar
 * @Discription: It is used to get active Question Types.
 */	
	@Transactional(readOnly=false)
	public List<QuestionTypeMaster> getActiveQuestionTypes()
	{
		List<QuestionTypeMaster> questionTypeMasters = null;
		try{
			Criterion criterion = Restrictions.eq("status", "a");
			questionTypeMasters = findByCriteria(Order.asc("questionType"),criterion);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return questionTypeMasters;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get active Question Types.
	 */	
	@Transactional(readOnly=false)
	public List<QuestionTypeMaster> getActiveQuestionTypesByShortName(String [] questionTypes)
	{
		List<QuestionTypeMaster> questionTypeMasters = null;
		try{
			//Criterion criterion = Restrictions.eq("status", "a");
			Criterion criterion1 = Restrictions.in("questionTypeShortName", questionTypes);
			questionTypeMasters = findByCriteria(Order.asc("questionType"),criterion1);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return questionTypeMasters;
	}
}
