package tm.dao.master;

import tm.bean.master.PoolMaster;
import tm.dao.generic.GenericHibernateDAO;


	public class PoolMasterDAO extends GenericHibernateDAO<PoolMaster, Integer>{
		
		public PoolMasterDAO()
		{
			super(PoolMaster.class);
		}
	}
