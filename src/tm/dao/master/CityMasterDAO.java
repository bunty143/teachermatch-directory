package tm.dao.master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.master.CityMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.StateMaster;
import tm.dao.generic.GenericHibernateDAO;

public class CityMasterDAO extends GenericHibernateDAO<CityMaster, Long> 
{
	public CityMasterDAO() 
	{
		super(CityMaster.class);
	}
	
	@Transactional(readOnly=true)
	public CityMaster findCityByZipcode(String zipCode)
	{
		List<CityMaster> lstCity= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("zipCode",zipCode);
			lstCity = findByCriteria(criterion1);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		if(lstCity==null || lstCity.size()==0)
		return null;
		else
		return lstCity.get(0);
	}
	
	@Transactional(readOnly=true)
	public StateMaster findStateByZipCode(String zipCode)
	{
		List<CityMaster> lstCity= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("zipCode",zipCode);
			lstCity = findByCriteria(criterion1);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		if(lstCity==null || lstCity.size()==0)
		return null;
		else
		return lstCity.get(0).getStateId();
	}
	
	@Transactional(readOnly=true)
	public List<CityMaster> findCityByState(StateMaster state)
	{
		
		List<CityMaster> lstCitytMaster = null;
		try 
		{
			getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion = Restrictions.eq("stateId",state);
			
			criteria.add(criterion);			
			criteria.addOrder(Order.asc("cityName"));
			
			lstCitytMaster = criteria.list();
			Map<String,CityMaster> mapCityMaster = new TreeMap<String, CityMaster>();
			for(CityMaster city: lstCitytMaster)
			{
				mapCityMaster.put(""+city.getCityName(), city);
			}
			lstCitytMaster = new ArrayList<CityMaster>();
			for(Map.Entry<String,CityMaster> entry : mapCityMaster.entrySet()) 
			{
				  //String key = entry.getKey();
				  CityMaster value = entry.getValue();
				  lstCitytMaster.add(value);
			  
			}
			//lstCitytMaster = findByCriteria(Order.asc("cityName"),criterion);
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstCitytMaster;
	}
	
	
	/* @Start
	 * @AShish
	 * @Description :: find City By Name
	 * */
		@Transactional(readOnly=true)
		public List<String> findCityByName(String cityName)
		{
			List<CityMaster> cityMasterList =  new ArrayList<CityMaster>();
			List<String> zipCodeList	=	new ArrayList<String>(); 
			
			try 
			{
				if(cityName.trim()!=null && cityName.trim().length()>0)
				{
					getSession();
					Criteria criteria = session.createCriteria(getPersistentClass());
					Criterion criterion = Restrictions.ilike("cityName",cityName.trim() );
					cityMasterList = criteria.add(criterion).list();
					
					for(CityMaster c:cityMasterList){
						zipCodeList.add(c.getZipCode());
					}
				}	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return zipCodeList;
		}
	/* @End
	 * @AShish
	 * @Description :: find City By Name
	 * */
		
		@Transactional(readOnly=true)
		public List<CityMaster> findCityByNameAndState(String cityName,StateMaster stateMaster)
		{
			List<CityMaster> cityMasterList =  new ArrayList<CityMaster>();
			
			try 
			{
				if(cityName.trim()!=null && cityName.trim().length()>0)
				{
					getSession();
					Criteria criteria = session.createCriteria(getPersistentClass());
					Criterion criterion = Restrictions.eq("stateId",stateMaster);
					criteria.add(criterion);	
					Criterion criterion1 = Restrictions.ilike("cityName",cityName.trim() );
					criteria.add(criterion1);
					cityMasterList = criteria.list();
				}	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return cityMasterList;
		}
		
		@Transactional(readOnly=true)
		public List<CityMaster> getAllCity()
		{
			
			List<CityMaster> lstCitytMaster = null;
			try 
			{
				getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion = Restrictions.eq("status","A");
				
				criteria.add(criterion);			
				criteria.addOrder(Order.asc("cityName"));
				
				lstCitytMaster = criteria.list();
				Map<String,CityMaster> mapCityMaster = new TreeMap<String, CityMaster>();
				for(CityMaster city: lstCitytMaster)
				{
					mapCityMaster.put(""+city.getCityName(), city);
				}
				lstCitytMaster = new ArrayList<CityMaster>();
				for(Map.Entry<String,CityMaster> entry : mapCityMaster.entrySet()) 
				{
					  //String key = entry.getKey();
					  CityMaster value = entry.getValue();
					  lstCitytMaster.add(value);
				  
				}
				//lstCitytMaster = findByCriteria(Order.asc("cityName"),criterion);
				
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return lstCitytMaster;
		}
		
		@Transactional(readOnly=true)
		public LinkedHashMap<Long,List<CityMaster>> findAllCityByZipcode(String zipCode)
		{
			LinkedHashMap<Long,List<CityMaster>> listMap= new LinkedHashMap<Long,List<CityMaster>>();
			try 
			{
				Session session = getSession();				
				Criteria criteria = session.createCriteria(CityMaster.class,"cm").createAlias("stateId", "sm");
				criteria.add(Restrictions.eq("zipCode",zipCode));
				criteria.addOrder(Order.asc("sm.stateName"));
				List<CityMaster> lstCity = criteria.list();
				for(CityMaster co:lstCity){
					if(listMap.get(co.getStateId().getStateId())==null){
						List<CityMaster> listCM =new ArrayList<CityMaster>();
						listCM.add(co);
						listMap.put(co.getStateId().getStateId(),listCM);
					}else{
						List<CityMaster> listCM =listMap.get(co.getStateId().getStateId());
						listCM.add(co);
						listMap.put(co.getStateId().getStateId(),listCM);
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}		
			if(listMap==null || listMap.size()==0)
			return null;
			else
			return listMap;
		}
		
		@Transactional(readOnly=true)
		public List<CityMaster> findCityByState(StateMaster state,String zipCode)
		{
			
			List<CityMaster> lstCitytMaster = null;
			try 
			{
				getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(Restrictions.eq("stateId",state));	
				/*if(zipCode!=null && !zipCode.trim().equals(""))
				criteria.add(Restrictions.eq("zipCode", zipCode));*/
				criteria.addOrder(Order.asc("cityName"));
				
				lstCitytMaster = criteria.list();
				System.out.println(lstCitytMaster.size());
				Map<String,CityMaster> mapCityMaster = new TreeMap<String, CityMaster>();
				List<CityMaster> cobj=new ArrayList<CityMaster>();
				for(CityMaster city: lstCitytMaster)
				{
					if(zipCode.trim().equals(city.getZipCode().trim()))
						cobj.add(city);
					mapCityMaster.put(""+city.getCityName(), city);
				}
				lstCitytMaster = new ArrayList<CityMaster>();
				if(cobj!=null && cobj.size()>0){
					mapCityMaster.clear();
					for(CityMaster obj:cobj)
					mapCityMaster.put(""+obj.getCityName(), obj);
				}
				for(Map.Entry<String,CityMaster> entry : mapCityMaster.entrySet()) 
				{
					  //String key = entry.getKey();
					  CityMaster value = entry.getValue();
					  lstCitytMaster.add(value);
				  
				}
				//lstCitytMaster = findByCriteria(Order.asc("cityName"),criterion);
				
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return lstCitytMaster;
		}
}
