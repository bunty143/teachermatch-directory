package tm.dao.master;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.GeographyMaster;
import tm.dao.generic.GenericHibernateDAO;

public class GeographyMasterDAO extends GenericHibernateDAO<GeographyMaster, Long> 
{
	public GeographyMasterDAO() {
		super(GeographyMaster.class);
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get active Geography.
	 */
	@Transactional(readOnly=true)
	public List<GeographyMaster> getActiveGeography()
	{
		List<GeographyMaster> geographyMasters = null;
		try {
			Criterion criterion = Restrictions.eq("status", "A");
			geographyMasters = findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return geographyMasters;
	}
}
