package tm.dao.master;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictPortfolioConfigNew;
import tm.bean.master.DspqFieldMaster;
import tm.bean.master.DspqPortfolioName;
import tm.bean.master.JobCategoryMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictPortfolioConfigNewDAO extends GenericHibernateDAO<DistrictPortfolioConfigNew, Integer> 
{
	public DistrictPortfolioConfigNewDAO() 
	{
		super(DistrictPortfolioConfigNew.class);
	}
	
	@Transactional(readOnly=false)
	public List<DistrictPortfolioConfigNew> findByDistrictAndJobCategory(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster)
	{
		List<DistrictPortfolioConfigNew> districtPortfolioConfigNewList = new ArrayList<DistrictPortfolioConfigNew>();
		try{
			
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1	=	null;
			Criterion criterion2	=	null;
			Criterion criterion3	=	null;
			
			criterion1 = Restrictions.eq("districtMaster",districtMaster);
			//criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			criterion1 = Restrictions.eq("districtMaster",districtMaster);
			if(jobCategoryMaster!=null)
				criterion2 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			else
				criterion2 = Restrictions.isNull("jobCategoryMaster");
			criteria.add(criterion1);
			criteria.add(criterion2);

			districtPortfolioConfigNewList = criteria.list();
			
		} catch(Exception e){
			e.printStackTrace();
		}
		
		return districtPortfolioConfigNewList;
	}
	@Transactional(readOnly=false)
	public List<DistrictPortfolioConfigNew> getDspqFieldListByDspqPortfolioNameId(DspqPortfolioName dspqPortfolioName)
	{
		List<DistrictPortfolioConfigNew> districtPortfolioConfigNewList = new ArrayList<DistrictPortfolioConfigNew>();	
		Criterion criterion=null;
		try
		{
			criterion = Restrictions.eq("dspqPortfolioName",dspqPortfolioName);			
			districtPortfolioConfigNewList=findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return districtPortfolioConfigNewList;
	}
	
}
