package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.SpokenLanguageMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SpokenLanguageMasterDAO extends GenericHibernateDAO<SpokenLanguageMaster, Integer> 
{
	public SpokenLanguageMasterDAO() 
	{
		super(SpokenLanguageMaster.class);
	}	
	
	@Transactional(readOnly=true)
	public List<SpokenLanguageMaster> findAllActiveSpokenLanguageMasterByOrder()
	{
		List<SpokenLanguageMaster> lstSpokenLanguageMaster= new ArrayList<SpokenLanguageMaster>();
		try 
		{
			Criterion criterion = Restrictions.eq("status", "A");
			Order order=Order.asc("status");
			lstSpokenLanguageMaster = findByCriteria(order,criterion);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstSpokenLanguageMaster;
	}
	
}
