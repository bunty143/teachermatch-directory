package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SaveCandidatesToFolder;
import tm.bean.master.UserFolderStructure;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SaveCandidatesToFolderDAO extends GenericHibernateDAO<SaveCandidatesToFolder, Integer> 
{
	public SaveCandidatesToFolderDAO()
	{
		super(SaveCandidatesToFolder.class);
	}
	
	@Transactional(readOnly=false)
	public List<SaveCandidatesToFolder> findDuplicateCandidateList(List<TeacherDetail> lstteacherDetail,UserMaster userMaster,UserFolderStructure userFolder)
	{
		List<SaveCandidatesToFolder> lstdulicateCandidates= new ArrayList<SaveCandidatesToFolder>();
		try 
		{
			if(lstteacherDetail.size()>0){
				Criterion criterion1 	= 	Restrictions.in("teacherId",lstteacherDetail);
				Criterion criterion2 	= 	Restrictions.eq("userFolder",userFolder);
				Criterion criterion3	=	Restrictions.eq("saveUserMaster", userMaster);
				Criterion criterion4	=	Restrictions.eq("districtMaster", userMaster.getDistrictId());
				Criterion criterion5 	= 	Restrictions.and(criterion1,criterion2);
				Criterion criterion6	= 	Restrictions.and(criterion3,criterion4);
				Criterion criterion7	= 	Restrictions.and(criterion5,criterion6);
				
				lstdulicateCandidates =findByCriteria(criterion7);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstdulicateCandidates;
	}
	
	
	@Transactional(readOnly=false)
	public List<SaveCandidatesToFolder> getlistBySaveCandidateId(List  lstSaveCandidates)
	{
		List<SaveCandidatesToFolder> lstdulicateCandidates= new ArrayList<SaveCandidatesToFolder>();
		try 
		{
			if(lstSaveCandidates.size()>0){
				Criterion criterion1 	= 	Restrictions.in("savedTeacherId",lstSaveCandidates);
				
				lstdulicateCandidates =findByCriteria(criterion1);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstdulicateCandidates;
	}
	
	/* ================= Gagan : Ger Duplicate Records by Teacher Object =================== */
	@Transactional(readOnly=false)
	public List<SaveCandidatesToFolder> getDuplicateCandidateBYTeacherObject(TeacherDetail teacherDetail,UserMaster userMaster,UserFolderStructure userFolder)
	{
		List<SaveCandidatesToFolder> lstdulicateCandidates= new ArrayList<SaveCandidatesToFolder>();
		try 
		{
				Criterion criterion1 	= 	Restrictions.eq("teacherId",teacherDetail);
				Criterion criterion2 	= 	Restrictions.eq("userFolder",userFolder);
				//Criterion criterion3	=	Restrictions.eq("saveUserMaster", userMaster);
				Criterion criterion4	=	Restrictions.eq("districtMaster", userMaster.getDistrictId());
				Criterion criterion5 	= 	Restrictions.and(criterion1,criterion2);
				//Criterion criterion6	= 	Restrictions.and(criterion3,criterion4);
				//Criterion criterion6	= 	Restrictions.and(criterion3,criterion4);
				//Criterion criterion7	= 	Restrictions.and(criterion5,criterion6);
				Criterion criterion7	= 	Restrictions.and(criterion5,criterion4);
				
				lstdulicateCandidates =findByCriteria(criterion7);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstdulicateCandidates;
	}
	

	@Transactional(readOnly=false)
	public List<TeacherDetail> getAllCandidatesByFolder(UserFolderStructure userFolderStructure,DistrictMaster districtMaster)
	{
		List<SaveCandidatesToFolder> lstdulicateCandidates= new ArrayList<SaveCandidatesToFolder>();
		List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
		
		try 
		{
			Criterion c1 = Restrictions.eq("userFolder", userFolderStructure);
			Criterion c2 = Restrictions.eq("districtMaster", districtMaster);
			
			lstdulicateCandidates  = findByCriteria(c1,c2);
		
			System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>> lstdulicateCandidates >>>>>>>>>>>>>>>>>> "+lstdulicateCandidates);
			System.out.println(" >>>>>>>>> 0000000000000000000 >>>>>>>>>>>>> "+lstdulicateCandidates.size());
			
			
			
			for(SaveCandidatesToFolder sCanToFolder:lstdulicateCandidates){
				teacherDetails.add(sCanToFolder.getTeacherId());
			}
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return teacherDetails;
	}
	
	@Transactional(readOnly=false)
	public List<SaveCandidatesToFolder> getCandidateByFolderAndUserMaster(UserMaster userMaster,UserFolderStructure userFolder)
	{
		List<SaveCandidatesToFolder> lstCandidates= new ArrayList<SaveCandidatesToFolder>();
		try 
		{
				Criterion criterion1 	= 	Restrictions.eq("userFolder",userFolder);				
				Criterion criterion2	=	null;
				if(userMaster.getDistrictId()!=null)
					criterion2 			=   Restrictions.eq("districtMaster", userMaster.getDistrictId());
				else if(userMaster.getBranchMaster()!=null)
					criterion2 			=   Restrictions.eq("branchId", userMaster.getBranchMaster().getBranchId());
				else if(userMaster.getHeadQuarterMaster()!=null)
					criterion2			=	Restrictions.eq("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
				
				lstCandidates 			=	findByCriteria(criterion1, criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstCandidates;
	}
	
	
	
}
