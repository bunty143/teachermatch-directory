package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.master.RaceMaster;
import tm.dao.generic.GenericHibernateDAO;

public class RaceMasterDAO extends GenericHibernateDAO<RaceMaster, Integer> 
{
	public RaceMasterDAO() 
	{
		super(RaceMaster.class);
	}
	
	@Transactional(readOnly=true)
	public List<RaceMaster> findAllRaceByOrder()
	{
		List<RaceMaster> lstRace= new ArrayList<RaceMaster>();
		try 
		{
			Criterion criterion=Restrictions.ne("raceId",0); 
			Criterion criterion1=Restrictions.eq("status","A"); 
			lstRace = findByCriteria(Order.asc("orderBy"),criterion,criterion1);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstRace;
	}
	
	@Transactional(readOnly=true)
	public List<RaceMaster> findRacesByRaceCodeByOrder(List<Integer> raceIds)
	{
		List<RaceMaster> lstRace= null;
		try 
		{
			Criterion criterion1=Restrictions.eq("status","A"); 
			Criterion criterion2=Restrictions.in("raceId", raceIds);
			lstRace = findByCriteria(Order.asc("orderBy"),criterion1,criterion2);	
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstRace;
	}
	
	
	
	
}
