package tm.dao.master;

import tm.bean.master.ExclusivePeriodMaster;
import tm.dao.generic.GenericHibernateDAO;

public class ExclusivePeriodMasterDAO extends GenericHibernateDAO<ExclusivePeriodMaster, Long> 
{
	public ExclusivePeriodMasterDAO() 
	{
		super(ExclusivePeriodMaster.class);
	}
}
