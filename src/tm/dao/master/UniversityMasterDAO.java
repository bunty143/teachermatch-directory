package tm.dao.master;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherAcademics;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.master.UniversityMaster;
import tm.dao.generic.GenericHibernateDAO;

public class UniversityMasterDAO extends GenericHibernateDAO<UniversityMaster, Integer> 
{
	public UniversityMasterDAO() {
		super(UniversityMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<UniversityMaster> findUniversityByName(String universityName)
	{
		List<UniversityMaster> lstUniversityMaster = null;
		try 
		{	
			Criterion criterion1 = Restrictions.eq("universityName",universityName);			
			lstUniversityMaster = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstUniversityMaster;		
	}

}
