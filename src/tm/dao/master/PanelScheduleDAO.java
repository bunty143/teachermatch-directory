package tm.dao.master;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.PanelSchedule;
import tm.bean.master.SchoolMaster;
import tm.dao.generic.GenericHibernateDAO;

public class PanelScheduleDAO extends GenericHibernateDAO<PanelSchedule, Integer> {
	public PanelScheduleDAO() {
		super(PanelSchedule.class);
	}
	
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelSchedules(List<TeacherDetail> teacherDetails, JobOrder jobOrder)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
			if(teacherDetails.size()>0)
			{
				Criterion criterion = Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
				panelScheduleList = findByCriteria(criterion,criterion1,criterion3);
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelSchedulesByTeacherAndJob(TeacherDetail teacherDetail, JobOrder jobOrder,DistrictMaster districtMaster,SchoolMaster schoolMaster)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
			
			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion2 = Restrictions.eq("districtId", districtMaster.getDistrictId());
			Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
			//boolean writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
			boolean writePrivilegeToSchool = jobOrder.getWritePrivilegeToSchool();
			System.out.println("writePrivilegeToSchool: "+writePrivilegeToSchool);
			if(schoolMaster==null || writePrivilegeToSchool)
				panelScheduleList = findByCriteria(criterion,criterion1,criterion2,criterion3);
			else
				panelScheduleList = findByCriteria(criterion,criterion1,criterion2,criterion3,Restrictions.eq("schoolId", schoolMaster));
			
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	/*
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelSchedulesByTeacherJobAndStatus(TeacherDetail teacherDetail, JobOrder jobOrder,DistrictMaster districtMaster,SchoolMaster schoolMaster,JobWisePanelStatus jobWisePanelStatus)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
			
			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion2 = Restrictions.eq("districtId", districtMaster);
			Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
			Criterion criterion4 = Restrictions.eq("jobWisePanelStatus", jobWisePanelStatus);
			//boolean writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
			boolean writePrivilegeToSchool = jobOrder.getWritePrivilegeToSchool();
			System.out.println("writePrivilegeToSchool: "+writePrivilegeToSchool);
			if(schoolMaster==null || writePrivilegeToSchool)
				panelScheduleList = findByCriteria(criterion,criterion1,criterion2,criterion3,criterion4);
			else
				panelScheduleList = findByCriteria(criterion,criterion1,criterion2,criterion3,Restrictions.eq("schoolId", schoolMaster),criterion4);
			
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}*/
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelSchedules(TeacherDetail teacherDetail, JobOrder jobOrder)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
			if(teacherDetail!=null && jobOrder!=null){
				Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
				panelScheduleList = findByCriteria(criterion,criterion1,criterion3);
			}
			
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelSchedulesByJobWisePanelStatusList(List<JobWisePanelStatus> jobWisePanelStatusList)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
				Criterion criterion = Restrictions.in("jobWisePanelStatus",jobWisePanelStatusList);
				Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
				panelScheduleList = findByCriteria(criterion,criterion3);
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelSchedulesByJobWisePanelStatusListAndTeacher(List<JobWisePanelStatus> jobWisePanelStatusList,TeacherDetail teacherDetail)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
				if(jobWisePanelStatusList.size()>0){
					Criterion criterion1 = Restrictions.in("jobWisePanelStatus",jobWisePanelStatusList);
					Criterion criterion2 = Restrictions.eq("teacherDetail",teacherDetail);
					Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
					panelScheduleList = findByCriteria(criterion1,criterion2,criterion3);
				}
		}catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelSchedulesByTeacherJobAndStatus(TeacherDetail teacherDetail, JobOrder jobOrder,DistrictMaster districtMaster,SchoolMaster schoolMaster,JobWisePanelStatus jobWisePanelStatus)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
			
			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion2 = Restrictions.eq("districtId", districtMaster.getDistrictId());
			Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
			Criterion criterion4 = Restrictions.eq("jobWisePanelStatus", jobWisePanelStatus);
			//boolean writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
			boolean writePrivilegeToSchool = jobOrder.getWritePrivilegeToSchool();
			System.out.println("writePrivilegeToSchool: "+writePrivilegeToSchool);
			if(schoolMaster==null || writePrivilegeToSchool)
				panelScheduleList = findByCriteria(criterion,criterion1,criterion2,criterion3,criterion4);
			else
				panelScheduleList = findByCriteria(criterion,criterion1,criterion2,criterion3,Restrictions.eq("schoolId", schoolMaster),criterion4);
			
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> getPanelSchedulesByTIDAndJIDs_msu(TeacherDetail teacherDetail, List<JobOrder> jobOrderLst)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
			if(jobOrderLst.size() > 0)
			{
				Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion1 = Restrictions.in("jobOrder", jobOrderLst);
				Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
				panelScheduleList = findByCriteria(criterion,criterion1,criterion3);
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelSchedulesByTJSAndStatus(TeacherDetail teacherDetail, JobOrder jobOrder,DistrictMaster districtMaster,SchoolMaster schoolMaster,JobWisePanelStatus jobWisePanelStatus)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
			
			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion2 = Restrictions.eq("districtId", districtMaster.getDistrictId());
			Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
			Criterion criterion4 = Restrictions.eq("jobWisePanelStatus", jobWisePanelStatus);
			if(schoolMaster==null)
				panelScheduleList = findByCriteria(criterion,criterion1,criterion2,criterion3,criterion4);
			else
				panelScheduleList = findByCriteria(criterion,criterion1,criterion2,criterion3,Restrictions.eq("schoolId",schoolMaster),criterion4);
			
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	
	@Transactional(readOnly=false)
	public PanelSchedule findPanelSchedule(TeacherDetail teacherDetail, JobOrder jobOrder,JobWisePanelStatus jobWisePanelStatus)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		PanelSchedule psObj=null;
		try{
				Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
				Criterion criterion4 = Restrictions.eq("jobWisePanelStatus", jobWisePanelStatus);
				panelScheduleList = findByCriteria(criterion,criterion1,criterion3,criterion4);
				System.out.println(":::::::::::::::::::::::::::::::panelScheduleList:::::>>>>>:::::::::"+panelScheduleList.size());
				if(panelScheduleList.size()>0){
					psObj=panelScheduleList.get(0);
				}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return psObj;
	}
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelScheduleList(List<TeacherDetail> teacherDetails, JobOrder jobOrder,JobWisePanelStatus jobWisePanelStatus)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
				Criterion criterion = Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
				Criterion criterion4 = Restrictions.eq("jobWisePanelStatus", jobWisePanelStatus);
				panelScheduleList = findByCriteria(criterion,criterion1,criterion3,criterion4);
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	
	@Transactional(readOnly=false)
	public PanelSchedule updatePanelSchedule(TeacherDetail teacherDetail, JobOrder jobOrder,JobWisePanelStatus jobWisePanelStatus)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		PanelSchedule psObj=null;
		try{
				Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
				Criterion criterion4 = Restrictions.eq("jobWisePanelStatus", jobWisePanelStatus);
				panelScheduleList = findByCriteria(criterion,criterion1,criterion3,criterion4);
				if(panelScheduleList.size()>0){
					psObj=panelScheduleList.get(0);
					psObj.setPanelStatus("Cancelled");
					updatePersistent(psObj);
				}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return psObj;
	}
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelScheduleList(List<TeacherDetail> teacherDetails, List<JobOrder> jobOrders,List<JobWisePanelStatus> jobWisePanelStatuss)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
			Criterion criterion = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion1 = Restrictions.in("jobOrder", jobOrders);
			Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
			Criterion criterion4 = Restrictions.in("jobWisePanelStatus", jobWisePanelStatuss);
			panelScheduleList = findByCriteria(criterion,criterion1,criterion3,criterion4);
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelScheduleList(TeacherDetail teacherDetail, List<JobOrder> jobOrders,List<JobWisePanelStatus> jobWisePanelStatuss)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion1 = Restrictions.in("jobOrder", jobOrders);
			Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
			Criterion criterion4 = Restrictions.in("jobWisePanelStatus", jobWisePanelStatuss);
			panelScheduleList = findByCriteria(criterion,criterion1,criterion3,criterion4);
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelScheduleListByTeachersAndJobs(List<TeacherDetail> teacherDetails, List<JobOrder> jobOrders,DistrictMaster districtMaster)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
			Criterion criterion = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
			Criterion criterion1 = Restrictions.in("jobOrder", jobOrders);
			panelScheduleList = findByCriteria(criterion,criterion1,criterion3);
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> getPanelSchedulesByTeacherJob(TeacherDetail teacherDetail, JobOrder jobOrder)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
			if(teacherDetail!=null && jobOrder!=null)
			{
				Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
				panelScheduleList = findByCriteria(criterion,criterion1);
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelScheduleDelta(List<TeacherDetail> lstTeacherDetail, List<JobOrder> lstJobOrder,List<JobWisePanelStatus> lstJobWisePanelStatus)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		if(lstTeacherDetail!=null && lstTeacherDetail.size()>0 && lstJobOrder!=null && lstJobOrder.size()>0 && lstJobWisePanelStatus!=null && lstJobWisePanelStatus.size()>0)
		try{
				Criterion criterion1 = Restrictions.in("teacherDetail",lstTeacherDetail);
				Criterion criterion2 = Restrictions.in("jobOrder", lstJobOrder);
				Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
				Criterion criterion4 = Restrictions.in("jobWisePanelStatus", lstJobWisePanelStatus);
				panelScheduleList = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
	
	@Transactional(readOnly=false)
	public List findPanelSchedulesCount(TeacherDetail teacherDetail, JobOrder jobOrder)
	{	
		List resultList =new ArrayList();	
		try{
			if(teacherDetail!=null && jobOrder!=null){
				Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
				//panelScheduleList = findByCriteria(criterion,criterion1,criterion3);
				
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());			
				criteria.setProjection(Projections.projectionList()					
						.add(Projections.count("panelId"))
						);				
				criteria.add(criterion).add(criterion1).add(criterion3);
				//Projections.count("jobId")))
				resultList = criteria.list();
			}
			
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return resultList;
	}
	
	@Transactional(readOnly=false)
	public List<PanelSchedule> findPanelSchedulesCgOp(List<TeacherDetail> teacherDetails, JobOrder jobOrder)
	{	
		List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();	
		try{
			if(teacherDetails.size()>0)
			{
				Criterion criterion = Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.ne("panelStatus", "Cancelled");
				Session session = getSession();
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			criteria.createAlias("teacherDetail", "td")
    			.setProjection(Projections.projectionList()
    					.add(Projections.property("td.teacherId"))
    					.add(Projections.property("panelStatus"))
    					.add(Projections.property("panelDate"))
    					.add(Projections.property("panelTime"))
    					.add(Projections.property("timeFormat"))
    					);
    			criteria.add(criterion).add(criterion1).add(criterion3);
				List<String []> panelSchedule=new ArrayList<String []>();
				panelSchedule = criteria.list();
				//panelScheduleList = findByCriteria(criterion,criterion1,criterion3);
				for (Iterator it = panelSchedule.iterator(); it.hasNext();)
				{
    				//System.out.println("call");
    				Object[] row = (Object[]) it.next();
    				if(row[0]!=null){
    					PanelSchedule panelSchedule2 = new PanelSchedule();
    					TeacherDetail teacherDetail = new TeacherDetail();
    					teacherDetail.setTeacherId(Integer.parseInt(row[0].toString()));
    					panelSchedule2.setTeacherDetail(teacherDetail);
    					panelSchedule2.setPanelStatus(row[1].toString());
    					try {
    						DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        					Date panelDate = df.parse(row[2].toString());
        					panelSchedule2.setPanelDate(panelDate);
						} catch (Exception e) {}
    					panelSchedule2.setPanelTime(row[3].toString());
    					panelSchedule2.setTimeFormat(row[4].toString());
    					panelScheduleList.add(panelSchedule2);
    				}
				}
				
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return panelScheduleList;
	}
}
