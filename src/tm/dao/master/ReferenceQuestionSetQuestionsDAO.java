package tm.dao.master;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificRefChkQuestions;
import tm.bean.master.ReferenceQuestionSetQuestions;
import tm.bean.master.ReferenceQuestionSets;
import tm.dao.generic.GenericHibernateDAO;

public class ReferenceQuestionSetQuestionsDAO extends GenericHibernateDAO<ReferenceQuestionSetQuestions, Integer> 
{
	public ReferenceQuestionSetQuestionsDAO() 
	{
		super(ReferenceQuestionSetQuestions.class);
	}
	
	@Autowired
	private ReferenceQuestionSetsDAO referenceQuestionSetsDAO;
	
	@Transactional(readOnly=false)
	public List<ReferenceQuestionSetQuestions> findByQuesSetId(ReferenceQuestionSets referenceQuestionSets,DistrictMaster districtMaster, HeadQuarterMaster headQuarterMaster) {
		
		// @ Anurag
		
		List<ReferenceQuestionSetQuestions> quesSetList = null;
		List<ReferenceQuestionSets> questionSetList = new ArrayList<ReferenceQuestionSets>();
		
		try 
		{
			Criterion criterion1 = null;
			
			if(districtMaster!=null)
				questionSetList = referenceQuestionSetsDAO.findByDistrict(districtMaster);
			if(headQuarterMaster!=null)
				questionSetList = referenceQuestionSetsDAO.findbyHeadQuarter(headQuarterMaster);
			
			System.out.println("::::::: questionSetList ::::::: >>>>>>>> "+questionSetList.size());
			for(ReferenceQuestionSets i4QS:questionSetList)
			{
				if(i4QS.equals(referenceQuestionSets))
				{
					criterion1 = Restrictions.eq("questionSets",referenceQuestionSets);
				}
			}

			/*Criterion criterion1 = Restrictions.in("questionSets",questionSetList);*/
			
			quesSetList = findByCriteria(Order.asc("QuestionSequence"),criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return quesSetList;
	}
	
	@Transactional(readOnly=false)
	public List<ReferenceQuestionSetQuestions> findByQuesId(DistrictSpecificRefChkQuestions districtSpecificRefChkQuestions) {
		List<ReferenceQuestionSetQuestions> quesSetList = null;
		List<ReferenceQuestionSetQuestions> existQuestionList = new ArrayList<ReferenceQuestionSetQuestions>();
		
		try 
		{
			Criterion criterion1 = Restrictions.eq("questionPool",districtSpecificRefChkQuestions);
			existQuestionList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return existQuestionList;
	}
	
	@Transactional(readOnly=false)
	public List<ReferenceQuestionSetQuestions> findByQuestionSet(ReferenceQuestionSets referenceQuestionSets) {
		List<ReferenceQuestionSetQuestions> quesSetList = null;
		List<ReferenceQuestionSetQuestions> existQuestionList = new ArrayList<ReferenceQuestionSetQuestions>();

		try 
		{
			Criterion criterion1 = Restrictions.eq("questionSets",referenceQuestionSets);
			existQuestionList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return existQuestionList;
	}
	
}
