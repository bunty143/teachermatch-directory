package tm.dao.master;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.TeacherAnswerDetailsForDistrictSpecificQuestionsHistory;

import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherAnswerDetailsForDistrictSpecificQuestionsHistoryDAO extends GenericHibernateDAO<TeacherAnswerDetailsForDistrictSpecificQuestionsHistory, Integer> 
{
	public TeacherAnswerDetailsForDistrictSpecificQuestionsHistoryDAO() 
	{
		super(TeacherAnswerDetailsForDistrictSpecificQuestionsHistory.class);
	}
	
}
