package tm.dao.master;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.DistrictSpecificRefChkQuestions;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.QqQuestionsetQuestions;
import tm.bean.master.ReferenceQuestionSetQuestions;
import tm.bean.master.ReferenceQuestionSets;
import tm.dao.generic.GenericHibernateDAO;

public class QqQuestionsetQuestionsDAO extends GenericHibernateDAO<QqQuestionsetQuestions, Integer> 
{
	public QqQuestionsetQuestionsDAO() 
	{
		super(QqQuestionsetQuestions.class);
	}
	
	@Autowired
	private QqQuestionSetsDAO qqQuestionSetsDAO;
	
	@Transactional(readOnly=false)
	public List<QqQuestionsetQuestions> findByQuesSetId(QqQuestionSets qqQuestionSets,DistrictMaster districtMaster) {
		List<QqQuestionsetQuestions> quesSetList = null;
		List<QqQuestionSets> questionSetList = new ArrayList<QqQuestionSets>();
		
		try 
		{
			Criterion criterion1 = null;
			
			if(districtMaster!=null)
				questionSetList = qqQuestionSetsDAO.findByDistrict(districtMaster);
			
			//System.out.println("::::::: questionSetList ::::::: >>>>>>>> "+questionSetList.size());
			if(questionSetList!=null){
				for(QqQuestionSets i4QS:questionSetList)
				{
					if(i4QS.equals(qqQuestionSets))
					{
						criterion1 = Restrictions.eq("questionSets",qqQuestionSets);
					}
				}
			}
			

			/*Criterion criterion1 = Restrictions.in("questionSets",questionSetList);*/
			
			quesSetList = findByCriteria(Order.asc("QuestionSequence"),criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return quesSetList;
	}
	
	@Transactional(readOnly=false)
	public List<QqQuestionsetQuestions> findByQuesSetByHeadQuater(QqQuestionSets qqQuestionSets,HeadQuarterMaster headQuarterMaster) {
		List<QqQuestionsetQuestions> quesSetList = null;
		List<QqQuestionSets> questionSetList = new ArrayList<QqQuestionSets>();
		
		try 
		{
			Criterion criterion1 = null;
			
			if(headQuarterMaster!=null)
				questionSetList = qqQuestionSetsDAO.findByHeadQuater(headQuarterMaster);
			
			//System.out.println("::::::: questionSetList ::::::: >>>>>>>> "+questionSetList.size());
			if(questionSetList!=null){
				for(QqQuestionSets i4QS:questionSetList)
				{
					if(i4QS.equals(qqQuestionSets))
					{
						criterion1 = Restrictions.eq("questionSets",qqQuestionSets);
					}
				}
			}

			/*Criterion criterion1 = Restrictions.in("questionSets",questionSetList);*/
			
			quesSetList = findByCriteria(Order.asc("QuestionSequence"),criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return quesSetList;
	}
	
	
	
	@Transactional(readOnly=false)
	public List<QqQuestionsetQuestions> findByQuesId(DistrictSpecificQuestions districtSpecificQuestions) {
		List<QqQuestionsetQuestions> quesSetList = null;
		List<QqQuestionsetQuestions> existQuestionList = new ArrayList<QqQuestionsetQuestions>();
		
		try 
		{
			Criterion criterion1 = Restrictions.eq("questionPool",districtSpecificQuestions);
			existQuestionList = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return existQuestionList;
	}
	
	@Transactional(readOnly=false)
	public List<QqQuestionsetQuestions> findByQuestionSet(QqQuestionSets qqQuestionSets) {
		List<QqQuestionsetQuestions> quesSetList = null;
		List<QqQuestionsetQuestions> existQuestionList = new ArrayList<QqQuestionsetQuestions>();

		try 
		{
			Criterion criterion1 = Restrictions.eq("questionSets",qqQuestionSets);
			existQuestionList = findByCriteria(Order.asc("QuestionSequence"),criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return existQuestionList;
	}
	
}
