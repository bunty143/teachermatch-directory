package tm.dao.master;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DistrictMaster;
import tm.bean.master.SubjectAreaExamMaster;
import tm.bean.master.SubjectMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SubjectAreaExamMasterDAO extends GenericHibernateDAO<SubjectAreaExamMaster, Integer> {
	public SubjectAreaExamMasterDAO() {
		super(SubjectAreaExamMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<SubjectAreaExamMaster> findActiveSubject(){
		List<SubjectAreaExamMaster> subjectAreaExamMasters=new ArrayList<SubjectAreaExamMaster>();
		try{
			subjectAreaExamMasters=getSession().createCriteria(SubjectAreaExamMaster.class).add(Restrictions.eq("status", "A")).addOrder(Order.asc("subjectAreaExamName")).list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return subjectAreaExamMasters;
	}
	
	/* @Start
	 * @Ashish
	 * @Description :: get Object List of SubjectAreaExamMaster according subject Id   
	 * */
	
	
		@Transactional(readOnly=false)
		public List<SubjectAreaExamMaster> findActiveSubjectExamByDistrict(DistrictMaster districtMaster)
		{
			List<SubjectAreaExamMaster> subjectAreaExamMasters=new ArrayList<SubjectAreaExamMaster>();
			try{
				subjectAreaExamMasters=getSession().createCriteria(SubjectAreaExamMaster.class)
								.add(Restrictions.eq("status", "A"))
								.add(Restrictions.eq("districtMaster", districtMaster))
								.addOrder(Order.asc("subjectAreaExamName")).list();
			}catch (Exception e) {
				e.printStackTrace();
			}
			return subjectAreaExamMasters;
		}

	
	
	
		@Transactional(readOnly=false)
		public List<SubjectAreaExamMaster> getSubjectAreaExamMasterlist(ArrayList<Integer> subjectIdList)
		{
			List<SubjectAreaExamMaster> subjectAreaExamMasters=new ArrayList<SubjectAreaExamMaster>();
			try{
				subjectAreaExamMasters=getSession().createCriteria(SubjectAreaExamMaster.class).add(Restrictions.in("subjectId", subjectIdList)).list();
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			return subjectAreaExamMasters;
		}
		
		@Transactional(readOnly=false)
		public SubjectAreaExamMaster getSubjectByNameAndDistrict(DistrictMaster districtMaster,String subName)
		{	
			System.out.println(" ===========Inside getSubjectByNameAndDistrict ===========");
			SubjectAreaExamMaster subjectAreaExamMaster = new SubjectAreaExamMaster();
			
			try {
				Criterion c1 = Restrictions.eq("status", "A");
				Criterion c2 = Restrictions.eq("districtMaster", districtMaster);
				Criterion c3 = Restrictions.eq("subjectAreaExamName", subName.trim());
				subjectAreaExamMaster = findByCriteria(c1,c2,c3).get(0);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return subjectAreaExamMaster;
		}
		
		
	/* @End
	 * @Ashish
	 * @Description :: get Object List of SubjectAreaExamMaster according subject Id   
	 * */
	
	
}
