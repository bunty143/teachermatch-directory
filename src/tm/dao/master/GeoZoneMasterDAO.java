package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoZoneMaster;
import tm.dao.generic.GenericHibernateDAO;

public class GeoZoneMasterDAO extends	GenericHibernateDAO<GeoZoneMaster, Integer> {
	public GeoZoneMasterDAO() {
		super(GeoZoneMaster.class);
	}
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Transactional(readOnly=false)
	public int findDupicateGeoZone(Integer geozoneId,Integer districtId,String geozonename,String geozonecode)
	{
		Criterion criterion1=null;
		Criterion criterion2=null;
		Criterion criterion=null;
		List result=null;
		DistrictMaster districtMaster=null;
		try{
			if(districtId>0)
			{
				districtMaster=districtMasterDAO.findById(districtId, false, false);
			}
			if(!geozonename.equals("")&& !geozonecode.equals(""))
			{
				criterion=Restrictions.eq("districtMaster", districtMaster);
				criterion1=Restrictions.like("geoZoneName", geozonename);
				criterion2=Restrictions.like("geoZoneCode", geozonecode);
				
				if(geozoneId!=null)
				{
					Criterion c3=Restrictions.not(Restrictions.in("geoZoneId",new Integer[]{geozoneId}));
					result=findByCriteria(criterion,criterion1,c3);
					if(result.size()>0)
						return 1;
					result=findByCriteria(criterion,criterion2,c3);
					if(result.size()>0)
						return 2;
					
				}else
				{
					result=findByCriteria(criterion,criterion1);
					if(result.size()>0){
						return 1;
					}	
					result=findByCriteria(criterion,criterion2);
					if(result.size()>0){
						return 2;
						
					}	
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			
		}
		return 0;
	}
	@Transactional(readOnly=false)
	public List<GeoZoneMaster> findByCriteria_OP(DistrictMaster districtMaster)
	{
		List<GeoZoneMaster> geoZoneMasterList= new ArrayList<GeoZoneMaster>();
		try 
		{
  			Query query1=null;
			if(districtMaster!=null){
 				Session session = getSession();
 				String hql=	"SELECT   gz.geoZoneId,gz.geoZoneName  FROM  geozonemaster gz "
			 	    +   "left join districtmaster dm on dm.districtId= gz.districtId and gz.districtId=dm.districtId"
				    + " WHERE   gz.districtId=? ";
 				    hql=hql+ "order by gz.geoZoneName ASC ";
 				query1=session.createSQLQuery(hql);
 				query1.setParameter(0, districtMaster.getDistrictId());
 				List<Object[]> objList = query1.list(); 
				GeoZoneMaster obj=null;
				if(objList.size()>0){
				for (int j = 0; j < objList.size(); j++) {
						Object[] objArr2 = objList.get(j);
						obj = new GeoZoneMaster();
					 	obj.setGeoZoneId(objArr2[0] == null ? 0 : Integer.parseInt(objArr2[0].toString()));
						obj.setGeoZoneName(objArr2[1] == null ? "NA" :  objArr2[1].toString());
						geoZoneMasterList.add(obj);
				   }
				}
 			}
		} 
	 	catch(Exception e){
 		         e.printStackTrace(); 
		}finally{
			// session.close(); 
		}
		return geoZoneMasterList;
	}
}
