package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.DspqFieldMaster;
import tm.bean.master.DspqGroupMaster;
import tm.bean.master.DspqSectionMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DspqFieldMasterDAO extends GenericHibernateDAO<DspqFieldMaster, Integer> 
{
	public DspqFieldMasterDAO() 
	{
		super(DspqFieldMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<DspqFieldMaster> getDspqFieldListBySections(List<DspqSectionMaster> dspqSectionMaster)
	{
		List<DspqFieldMaster> dspqFieldList = new ArrayList<DspqFieldMaster>();
		Criterion criterion = Restrictions.eq("status", "A");
		try
		{
			Criterion criterion1 = Restrictions.in("dspqSectionMaster",dspqSectionMaster);
			Criterion criterion2 = Restrictions.eq("isAdditionalField", false);
			dspqFieldList=findByCriteria(Order.asc("fieldByOrder"),criterion1,criterion2,criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqFieldList;
	}
	@Transactional(readOnly=false)
	public List<DspqFieldMaster> getDspqAdditionalFieldListBySections(List<DspqSectionMaster> dspqSectionMaster)
	{
		List<DspqFieldMaster> dspqFieldList = new ArrayList<DspqFieldMaster>();
		Criterion criterion = Restrictions.eq("status", "A");
		try
		{
			Criterion criterion1 = Restrictions.in("dspqSectionMaster",dspqSectionMaster);
			Criterion criterion2 = Restrictions.eq("isAdditionalField", true);
			dspqFieldList=findByCriteria(criterion1,criterion2,criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqFieldList;
	}
	@Transactional(readOnly=false)
	public List<DspqFieldMaster> getDspqAllFieldListBySections(List<DspqSectionMaster> dspqSectionMaster)
	{
		List<DspqFieldMaster> dspqFieldList = new ArrayList<DspqFieldMaster>();
		Criterion criterion = Restrictions.eq("status", "A");
		try
		{
			Criterion criterion1 = Restrictions.in("dspqSectionMaster",dspqSectionMaster);			
			dspqFieldList=findByCriteria(criterion1,criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqFieldList;
	}
	
	@Transactional(readOnly=false)
	public List<DspqFieldMaster> getFieldListBySection(DspqSectionMaster dspqSectionMaster)
	{
		List<DspqFieldMaster> dspqFieldList = new ArrayList<DspqFieldMaster>();
		Criterion criterion1 = Restrictions.eq("status", "A");
		Criterion criterion2 = Restrictions.eq("dspqSectionMaster", dspqSectionMaster);
		try
		{
			dspqFieldList=findByCriteria(criterion1,criterion2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqFieldList;
	}
	
	
	@Transactional(readOnly=false)
	public List<DspqFieldMaster> getFieldListBySections(List<DspqSectionMaster> dspqSectionMasters)
	{
		List<DspqFieldMaster> dspqFieldList = new ArrayList<DspqFieldMaster>();
		Criterion criterion1 = Restrictions.eq("status", "A");
		Criterion criterion2 = Restrictions.in("dspqSectionMaster", dspqSectionMasters);
		try
		{
			if(dspqSectionMasters!=null && dspqSectionMasters.size() >0)
				dspqFieldList=findByCriteria(criterion1,criterion2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqFieldList;
	}
	@Transactional(readOnly=false)
	public List<DspqFieldMaster> getDspqFieldListByFieldId(Integer fieldId)
	{
		List<DspqFieldMaster> dspqFieldMasterList = new ArrayList<DspqFieldMaster>();
		try
		{
			Criterion criterion = Restrictions.eq("dspqFieldId", fieldId);
			dspqFieldMasterList=findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqFieldMasterList;
	}
	@Transactional(readOnly=false)
	public List<DspqFieldMaster> getDspqFieldListBySectionsID(DspqSectionMaster dspqSectionMaster)
	{
		List<DspqFieldMaster> dspqFieldList = new ArrayList<DspqFieldMaster>();
		Criterion criterion = Restrictions.eq("status", "A");
		try
		{
			Criterion criterion1 = Restrictions.eq("dspqSectionMaster",dspqSectionMaster);
			Criterion criterion2 = Restrictions.eq("isAdditionalField", false);
			dspqFieldList=findByCriteria(criterion1,criterion2,criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqFieldList;
	}
	@Transactional(readOnly=false)
	public List<DspqFieldMaster> getDspqAllFieldListBySectionsID(DspqSectionMaster dspqSectionMaster)
	{
		List<DspqFieldMaster> dspqFieldList = new ArrayList<DspqFieldMaster>();
		Criterion criterion = Restrictions.eq("status", "A");
		try
		{
			Criterion criterion1 = Restrictions.eq("dspqSectionMaster",dspqSectionMaster);			
			dspqFieldList=findByCriteria(criterion1,criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqFieldList;
	}
	@Transactional(readOnly=false)
	public List<DspqFieldMaster> getDspqAdditionalFieldListBySectionsID(DspqSectionMaster dspqSectionMaster)
	{
		List<DspqFieldMaster> dspqFieldList = new ArrayList<DspqFieldMaster>();
		Criterion criterion = Restrictions.eq("status", "A");
		try
		{
			Criterion criterion1 = Restrictions.eq("dspqSectionMaster",dspqSectionMaster);
			Criterion criterion2 = Restrictions.eq("isAdditionalField", true);
			dspqFieldList=findByCriteria(criterion1,criterion2,criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dspqFieldList;
	}
	
}
