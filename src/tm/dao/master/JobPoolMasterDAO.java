package tm.dao.master;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.JobPoolMaster;
import tm.bean.TeacherDetail;
import tm.dao.generic.GenericHibernateDAO;

public class JobPoolMasterDAO extends GenericHibernateDAO<JobPoolMaster, Integer> 
{
	public JobPoolMasterDAO() 
	{
		super(JobPoolMaster.class);
	}
	
	
}