package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SkillAttributesMaster;
import tm.dao.generic.GenericHibernateDAO;

public class SkillAttributesMasterDAO extends GenericHibernateDAO<SkillAttributesMaster, Integer> 
{
	public SkillAttributesMasterDAO() 
	{
		super(SkillAttributesMaster.class);
	}
	
	@Autowired JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	
	@Transactional(readOnly=true)
	public List<SkillAttributesMaster> getActiveList(Integer jobCategory){
		List<SkillAttributesMaster> skillAttributesMasters=new ArrayList<SkillAttributesMaster>();
		
		try
		{
			JobCategoryMaster jobCategoryMaster=null;
			if(jobCategory!=null)
				jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategory, false, false);
			Boolean baseStatus=null;
			if(jobCategoryMaster!=null)
				baseStatus=jobCategoryMaster.getBaseStatus();
			
			Criterion criterion_baseStatus = Restrictions.eq("baseStatus",baseStatus);
			
			Criterion criterion_status = Restrictions.eq("status","A");
			skillAttributesMasters=findByCriteria(Order.asc("skillName"), criterion_status,criterion_baseStatus);
			
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return skillAttributesMasters;
		}
	}
	
}
