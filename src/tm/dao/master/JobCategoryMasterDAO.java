package tm.dao.master;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class JobCategoryMasterDAO extends GenericHibernateDAO<JobCategoryMaster, Integer>
{
	public JobCategoryMasterDAO() {
		super(JobCategoryMaster.class);
	}
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findActiveJobCategory()
	{
	         List <JobCategoryMaster> lstJobCategoryMasters = null;
			 Criterion criterion = Restrictions.eq( "status","A");
			 lstJobCategoryMasters = findByCriteria(Order.asc("jobCategoryName"),criterion);  
			 
			 return lstJobCategoryMasters;
	}

	@Transactional(readOnly=true)
	public JobCategoryMaster findJobCategoryByNmae(String jobCategoryName)
	{
	         List <JobCategoryMaster> lstJobCategoryMasters = null;
			 Criterion criterion = Restrictions.eq( "jobCategoryName",jobCategoryName);
			 lstJobCategoryMasters = findByCriteria(Order.asc("jobCategoryName"),criterion);  
			 if(lstJobCategoryMasters==null || lstJobCategoryMasters.size()==0)
				 return null;
			 else
				 return lstJobCategoryMasters.get(0);

	}
	
	
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findAllJobCategoryNameByOrder()
	{
		List<JobCategoryMaster> lstJobCategoryMaster= null;
		try 
		{
			
			lstJobCategoryMaster = findByCriteria(Order.asc("jobCategoryName"));		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstJobCategoryMaster;
	}
	
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findAllJobCategoryNameByJob(JobOrder jobOrder)
	{
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		try{
			if(jobOrder!=null){
				 boolean baseStatus=jobOrder.getJobCategoryMaster().getBaseStatus();
				 DistrictMaster districtMaster=jobOrder.getDistrictMaster();
				 
				 Criterion criterion1 	= 	Restrictions.eq( "status","A");
				 Criterion	criterion2 	= 	Restrictions.eq( "baseStatus",baseStatus);
				 Criterion criterion3	=	Restrictions.isNull("districtMaster");
				 Criterion criterion4	=	Restrictions.eq("districtMaster", districtMaster);
				 Criterion criterion5	=	Restrictions.or(criterion3, criterion4);
				 Criterion criterion8	=	Restrictions.isNull("parentJobCategoryId");
				// Criterion criterion7	=	Restrictions.eq("parentJobCategoryId",0);
				// Criterion criterion8	=   Restrictions.or(criterion6, criterion7);
				 
				 
				 boolean dip=false;
				 if(districtMaster.getDisplayTMDefaultJobCategory()!=null){
					 if(districtMaster.getDisplayTMDefaultJobCategory()){
						 dip=true;
					 } 
				 }
				 
				 if(districtMaster!=null && dip==true){
					 criterion3 = Restrictions.and(Restrictions.and(Restrictions.isNull("branchMaster"), Restrictions.isNull("headQuarterMaster")), Restrictions.isNull("districtMaster"));
					 criterion4	=	Restrictions.eq("districtMaster", districtMaster);
					 criterion5	=	Restrictions.or(criterion3, criterion4);
					 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion5,criterion8);
				 }else if(districtMaster!=null && dip==false){
					 criterion4	=	Restrictions.eq("districtMaster", districtMaster);
					 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion4,criterion8);
				 }
				 boolean jobExist=false;
				 if(jobOrder.getJobCategoryMaster()!=null){
					 if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null && jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()>0)
					 {
						 jobExist = true;
					 }
					 else
					 {
						 for(JobCategoryMaster job:lstJobCategoryMaster){
							 if(job.getJobCategoryId().equals(jobOrder.getJobCategoryMaster().getJobCategoryId()) ){
								 jobExist=true;
							 }
						 }
					 }
				 }
				 if(jobExist==false){
					 lstJobCategoryMaster.add(jobOrder.getJobCategoryMaster());
				 }
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return lstJobCategoryMaster;
	}

	@Transactional(readOnly=true)
    public List<JobCategoryMaster> findAllJobCategoryNameByDistrict(DistrictMaster districtMaster)
    {
         List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
         try{
             if(districtMaster!=null){
                   Criterion criterion1   =    Restrictions.eq( "status","A");
                   Criterion criterion3   =    null;
                   Criterion criterion4   =    null;
                   Criterion criterion5   =    null;
                   Criterion criterion6   =    null;
                   Criterion criterion7   =    null;
                   Criterion criterion9   =    null;
                   Criterion criterion8   =     Restrictions.isNull("parentJobCategoryId");
             //   Criterion criterion7   =     Restrictions.eq("parentJobCategoryId",0);
             //   Criterion criterion8   =   Restrictions.or(criterion6, criterion7);
                   boolean dip=false;
                   if(districtMaster.getDisplayTMDefaultJobCategory()!=null){
                        if(districtMaster.getDisplayTMDefaultJobCategory()){
                             dip=true;
                        } 
                   }
                   
                   if(districtMaster!=null && dip==true){
                        criterion3   =     Restrictions.isNull("districtMaster");
                        criterion4   =     Restrictions.eq("districtMaster", districtMaster);
                        criterion6   =     Restrictions.isNull("branchMaster");
                        criterion7   =     Restrictions.isNull("headQuarterMaster");
                        
                        criterion9 = Restrictions.and(Restrictions.and(criterion3, criterion6), criterion7);
                        
                        criterion5   =    Restrictions.or(criterion4, criterion9);
                        lstJobCategoryMaster   =     findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion5,criterion8);
                   }else if(districtMaster!=null && dip==false){
                        criterion4   =     Restrictions.eq("districtMaster", districtMaster);
                        lstJobCategoryMaster   =     findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion4,criterion8);
                   }
             }
         }catch (Exception e){
             e.printStackTrace();
         }
         return lstJobCategoryMaster;
    }

	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findAllJobSubCategoryNameByDistrict(DistrictMaster districtMaster)
	{
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		try{
			if(districtMaster!=null){
				 Criterion criterion1 	= 	Restrictions.eq( "status","A");
				 Criterion criterion3	=	null;
				 Criterion criterion4	=	null;
				 Criterion criterion5	=	null;
				 Criterion criterion6	=	Restrictions.isNotNull("parentJobCategoryId");
				 boolean dip=false;
				 if(districtMaster.getDisplayTMDefaultJobCategory()!=null){
					 if(districtMaster.getDisplayTMDefaultJobCategory()){
						 dip=true;
					 } 
				 }
				 
				 if(districtMaster!=null && dip==true){
					 criterion3	=	Restrictions.isNull("districtMaster");
					 criterion4	=	Restrictions.eq("districtMaster", districtMaster);
					 criterion5	=	Restrictions.or(criterion3, criterion4);
					 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion5,criterion6);
				 }else if(districtMaster!=null && dip==false){
					 criterion4	=	Restrictions.eq("districtMaster", districtMaster);
					 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion4,criterion6);
				 }
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return lstJobCategoryMaster;
	}
	
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findAllJobSubCategoryByJobCateId(JobCategoryMaster jobCateId,DistrictMaster  districtMaster)
	{
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		
		try
		{
			 Criterion criterion1 	= 	Restrictions.eq( "status","A");
			 Criterion criterion2	=	Restrictions.eq( "districtMaster",districtMaster);
			 Criterion criterion3	=	Restrictions.eq( "parentJobCategoryId",jobCateId);
			 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion3);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		return lstJobCategoryMaster;
	}
		
	
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findAllJobSubCategoryByJobCateIds(List<JobCategoryMaster> jobCateIds,DistrictMaster  districtMaster)
	{
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		
		try
		{
			if(jobCateIds.size()>0){
				 Criterion criterion1 	= 	Restrictions.eq( "status","A");
				 Criterion criterion2	=	Restrictions.eq( "districtMaster",districtMaster);
				 Criterion criterion3	=	Restrictions.in( "parentJobCategoryId",jobCateIds);
				 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion3);
			}	 
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		return lstJobCategoryMaster;
	}
	
		@Transactional(readOnly=false)
	public List<JobCategoryMaster> findAllJobSubCategoryByJobCateIdForList(Integer jobCateId,DistrictMaster  districtMaster)
	{
		System.out.println(" findAllJobSubCategoryByJobCateIdForList >>>>>>>>>>>>>>>>>>>>>>"+jobCateId);
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		if(districtMaster!=null)System.out.println("districtMaster:::::::"+districtMaster.getDistrictName());
		try
		{
			 Criterion criterion1 	= 	Restrictions.eq( "status","A");
			 Criterion criterion2	=	Restrictions.eq( "districtMaster",districtMaster);
			 Criterion criterion3	=	Restrictions.eq( "parentJobCategoryId",jobCateId);
			 Criterion criterion4	=	Restrictions.eq( "jobCategoryId",jobCateId);
			 Criterion criterion5	=   Restrictions.or(criterion4, criterion3);
			 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion5);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		return lstJobCategoryMaster;
	}
	
	
	
	@Transactional(readOnly=true)
	public boolean baseStatusChecked(int jobCategoryId)
	{
		boolean baseFlag=false;
		try 
		{
			List<JobCategoryMaster> lstJobCategoryMaster= null;
			 Criterion criterion1 = Restrictions.eq( "status","A");
			 Criterion criterion2 = Restrictions.eq( "baseStatus",true);
			 Criterion criterion3 = Restrictions.eq( "jobCategoryId",jobCategoryId);
			 lstJobCategoryMaster = findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion3); 
			 
			 if(lstJobCategoryMaster.size()>0){
				 baseFlag=true; 
			 }
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return baseFlag;
	}
	
	@Transactional(readOnly=true)
	public boolean baseForFullTimeTeacher(int jobCategoryId)
	{
		boolean baseFullTimeFlag=false;
		try 
		{
			List<JobCategoryMaster> lstJobCategoryMaster= null;
			 Criterion criterion1 = Restrictions.eq( "status","A");
			 Criterion criterion2 = Restrictions.eq( "epiForFullTimeTeachers",true);
			 Criterion criterion3 = Restrictions.eq( "jobCategoryId",jobCategoryId);
			 lstJobCategoryMaster = findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion3); 
			 
			 if(lstJobCategoryMaster.size()>0){
				 baseFullTimeFlag=true; 
			 }
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return baseFullTimeFlag;
	}
	
	@Transactional(readOnly=true)
	public boolean checkDuplicateJobCategory(JobCategoryMaster jobCategoryMaster,String jobCategoryName,DistrictMaster districtMaster)
	{
		List<JobCategoryMaster>list=new ArrayList<JobCategoryMaster>();
		try{
			Criterion criterion=Restrictions.eq("jobCategoryName", jobCategoryName);
			//System.out.println("11 jobCategoryMaster 11"+jobCategoryMaster);
			if(jobCategoryMaster!=null){
				Criterion duplicateCheck=Restrictions.not(Restrictions.in("jobCategoryId",new Integer[]{jobCategoryMaster.getJobCategoryId()}));
				if(districtMaster!=null){
					Criterion criterion1=Restrictions.eq("districtMaster", districtMaster);
					list=findByCriteria(criterion,criterion1,duplicateCheck);
				}else{
					Criterion criterion1=Restrictions.isNull("districtMaster");
					list=findByCriteria(criterion,criterion1,duplicateCheck);
				}
			}else{
				if(districtMaster!=null){
					Criterion criterion1=Restrictions.eq("districtMaster", districtMaster);
					list=findByCriteria(criterion,criterion1);
				}else{
					Criterion criterion1=Restrictions.isNull("districtMaster");
					list=findByCriteria(criterion,criterion1);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		if(list.size()>0)
			return true;
		else
			return false;
	}
	
	@Transactional(readOnly=true)
	public boolean checkDuplicateJobCategoryWithBranch(JobCategoryMaster jobCategoryMaster, BranchMaster branchMaster,String jobCategoryName,DistrictMaster districtMaster)
	{
		List<JobCategoryMaster>list=new ArrayList<JobCategoryMaster>();
		try{
			Criterion criterion=Restrictions.eq("jobCategoryName", jobCategoryName);
			//System.out.println("11 jobCategoryMaster 11"+jobCategoryMaster);
			if(jobCategoryMaster!=null){
				Criterion duplicateCheck=Restrictions.not(Restrictions.in("jobCategoryId",new Integer[]{jobCategoryMaster.getJobCategoryId()}));
				if(districtMaster!=null){
					Criterion criterion1=Restrictions.eq("districtMaster", districtMaster);
					list=findByCriteria(criterion,criterion1,duplicateCheck);
				}else{
					Criterion criterion1=Restrictions.isNull("districtMaster");
					list=findByCriteria(criterion,criterion1,duplicateCheck);
				}
			}else{
				if(districtMaster!=null){
					Criterion criterion1=Restrictions.eq("districtMaster", districtMaster);
					Criterion criterion2=Restrictions.eq("branchMaster", branchMaster);
					list=findByCriteria(criterion,criterion1,criterion2);
					
				}else{
					Criterion criterion1=Restrictions.isNull("districtMaster");
					list=findByCriteria(criterion,criterion1);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		if(list.size()>0)
			return true;
		else
			return false;
	}
	
	@Transactional(readOnly=false)
	public  List<JobCategoryMaster> findJobCatList(Order order,int startPos,int limit,Criterion...criterions)
	{
		List<JobCategoryMaster> jobCategoryMasters=null;
		
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			for(Criterion c:criterions){
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			if(order != null)
				criteria.addOrder(order);
			
			jobCategoryMasters=criteria.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return jobCategoryMasters;
	}
	
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findAssessmentDocumentByDistrictJobCategory(DistrictMaster districtMaster,Integer jobCategoryId)
	{
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		try{
			 Criterion criterion1 	= 	Restrictions.eq( "status","A");
			 Criterion criterion2	=	null;
			 Criterion criterion3	=	null;
			 Criterion criterion4	=	null;
			 if(districtMaster!=null){
				 criterion2	=	Restrictions.eq("districtMaster", districtMaster);
				 criterion4	=	Restrictions.isNotNull("assessmentDocument");
				 if(jobCategoryId!=null){
					 criterion3	=	Restrictions.eq("jobCategoryId", jobCategoryId);
					 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion3,criterion4);
				 }else{
					 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion4);
				 }
			 }
		}catch (Exception e){
			e.printStackTrace();
		}
		return lstJobCategoryMaster;
	}
	
		
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findByCategoryID(List<Integer> jobcategoryIds)
	{
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		List<JobCategoryMaster> lstParentJobCat= new ArrayList<JobCategoryMaster>();
		
		if(jobcategoryIds!=null && jobcategoryIds.size()>0)
		{
		 JobCategoryMaster jobCatId=new JobCategoryMaster();
			for(Integer id: jobcategoryIds)
			{
				jobCatId=new JobCategoryMaster();
				jobCatId.setJobCategoryId(id);
				lstParentJobCat.add(jobCatId);
			}
		}
		
		try{
			
			if(jobcategoryIds.size()>0){
			 Criterion criterion1 	= 	Restrictions.in("parentJobCategoryId",lstParentJobCat);
			 lstJobCategoryMaster 	=	findByCriteria(criterion1);
			}	
			
		}catch (Exception e){
			e.printStackTrace();
		}
		return lstJobCategoryMaster;
	}
	
	
	
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findBySubCategoryID(List<Integer> jobcategoryIds)
	{
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		try{
			
			if(jobcategoryIds.size()>0){
			 Criterion criterion1 	= 	Restrictions.in("parentJobCategoryId",jobcategoryIds);
			 Criterion criterion2 	= 	Restrictions.eq("status","A");
			 lstJobCategoryMaster 	=	findByCriteria(criterion1,criterion2);
			}	
			
		}catch (Exception e){
			e.printStackTrace();
		}
		return lstJobCategoryMaster;
	}
	
	
	@Transactional(readOnly=true)
	public JobCategoryMaster findJobCategoryByNmaeAPI(String jobCategoryName,DistrictMaster districtMaster)
	{
	         List <JobCategoryMaster> lstJobCategoryMasters = null;
			 Criterion criterion1=Restrictions.eq( "jobCategoryName",jobCategoryName);
			 Criterion criterion2=Restrictions.eq("districtMaster", districtMaster);
			 Criterion criterion3=Restrictions.eq( "status","A");
			 Criterion criterion4=Restrictions.eq( "baseStatus",true);
			 Criterion criterion5=Restrictions.eq( "epiForFullTimeTeachers",true);
			 
			 lstJobCategoryMasters = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);  
			 if(lstJobCategoryMasters==null || lstJobCategoryMasters.size()==0)
				 return null;
			 else
				 return lstJobCategoryMasters.get(0);

	}
	
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findAllJobCategoryNameByDistrictTotal(DistrictMaster districtMaster)
	{
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		try{
			if(districtMaster!=null){
				 Criterion criterion1 	= 	Restrictions.eq("status","A");
				 Criterion criterion3	=	null;
				 Criterion criterion4	=	null;
				 Criterion criterion5	=	null;
				
				 boolean dip=false;
				 if(districtMaster.getDisplayTMDefaultJobCategory()!=null){
					 if(districtMaster.getDisplayTMDefaultJobCategory()){
						 dip=true;
					 } 
				 }
				 
				 if(districtMaster!=null && dip==true){
					 criterion3	=	Restrictions.isNull("districtMaster");
					 criterion4	=	Restrictions.eq("districtMaster", districtMaster);
					 criterion5	=	Restrictions.or(criterion3, criterion4);
					 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion5);
				 }else if(districtMaster!=null && dip==false){
					 criterion4	=	Restrictions.eq("districtMaster", districtMaster);
					 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion4);
				 }
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return lstJobCategoryMaster;
	}
	@Transactional(readOnly=false)
	public List<JobCategoryMaster> findJobSubCategoryByJobCateIdForList(JobCategoryMaster jobCategoryMaster,DistrictMaster  districtMaster)
	{
		System.out.println(" findJobSubCategoryByJobCateIdForList >>>>>>>>>>>>>>>>>>>>>>");
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		try
		{
			 Criterion criterion1 	= 	Restrictions.eq( "status","A");
			 Criterion criterion2	=	Restrictions.eq( "districtMaster",districtMaster);
			 Criterion criterion3	=	Restrictions.eq( "parentJobCategoryId",jobCategoryMaster);
			 Criterion criterion4	=	Restrictions.eq( "jobCategoryId",jobCategoryMaster.getJobCategoryId());
			 Criterion criterion5	=   Restrictions.or(criterion4, criterion3);
			 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion5);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		return lstJobCategoryMaster;
	}
	
	@Transactional(readOnly=false)
	public  List<JobCategoryMaster> findJobCategoryByHQAandBranchAndDistrict(HeadQuarterMaster headQuarterMaster, BranchMaster branchMaster, DistrictMaster districtMaster)
	{
		List<JobCategoryMaster> jobCategoryMasters=null;
		
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterionHQ = Restrictions.eq("headQuarterMaster", headQuarterMaster);
			Criterion criterionB = Restrictions.eq("branchMaster", branchMaster);
			Criterion criterionD = Restrictions.eq("districtMaster", districtMaster);
			Criterion criterionStatus = Restrictions.eq("status", "A");
			
			if(headQuarterMaster!=null){
				criteria.add(criterionHQ);
			}
			
			if(branchMaster!=null){
				criteria.add(criterionB);
			}
			
			if(districtMaster!=null){
				criteria.add(criterionD);
			}
			
			criteria.add(criterionStatus);
			jobCategoryMasters=criteria.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return jobCategoryMasters;
	}
	
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findAllJobCategoryNameByDistrictBranchAndHead(HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster)
	{
		System.out.println("::::::::::::::::::::::->----findAllJobCategoryNameByDistrictBranchAndHead:::::::::::::::::"+headQuarterMaster);
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		try{
				 Criterion criterion3	=	null;
				 Criterion criterion4	=	null;
				 Criterion criterion5	=	null;
				 Criterion criterion8	=	Restrictions.isNull("parentJobCategoryId");
			//	 Criterion criterion7	=	Restrictions.eq("parentJobCategoryId",0);
			//	 Criterion criterion8	=   Restrictions.or(criterion6, criterion7);
				 boolean dip=false;
				 if(districtMaster!=null && districtMaster.getDisplayTMDefaultJobCategory()!=null){
					 if(districtMaster.getDisplayTMDefaultJobCategory()){
						 dip=true;
					 } 
				 }
			 	Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				 if(districtMaster!=null && dip==true){
					 criterion3	=	Restrictions.isNull("districtMaster");
					 criterion4	=	Restrictions.eq("districtMaster", districtMaster);
					 criterion5	=	Restrictions.or(criterion3, criterion4);
				}else{
					criterion5	=	Restrictions.eq("districtMaster", districtMaster);
				}
				if(districtMaster!=null){
					criteria.add(criterion5);
				}else{
					criterion5	=	Restrictions.isNull("districtMaster");
				}
				criteria.add(criterion8);
				if(branchMaster!=null){
					criteria.add(Restrictions.eq("branchMaster",branchMaster));
				}else{
					criteria.add(Restrictions.isNull("branchMaster"));
				}
				if(headQuarterMaster!=null && headQuarterMaster.getHeadQuarterId()==1){
					criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
				}
				else{
					criteria.add(Restrictions.isNull("headQuarterMaster"));
				}
				criteria.add(Restrictions.eq("status","A"));
				criteria.addOrder(Order.asc("jobCategoryName"));
				lstJobCategoryMaster = criteria.list();
				
				System.out.println("lstJobCategoryMaster:::::::::::::::::::New"+lstJobCategoryMaster.size());
		}catch (Exception e){
			e.printStackTrace();
		}
		return lstJobCategoryMaster;
	}
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findAssessmentDocumentByDistrictJobCategoryHeadBranch(SecondaryStatus secondaryStatus)
	{
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		try{
			 Criterion criterion1 	= 	Restrictions.eq( "status","A");
			 Criterion criterion2	=	null;
			 Criterion criterion3	=	null;
			 Criterion criterion4	=	null;
			 if(secondaryStatus.getHeadQuarterMaster()!=null){
				 criterion2	=	Restrictions.eq("headQuarterMaster", secondaryStatus.getHeadQuarterMaster());
				 criterion4	=	Restrictions.isNotNull("assessmentDocument");
				 if(secondaryStatus.getJobCategoryMaster()!=null){
					 criterion3	=	Restrictions.eq("jobCategoryId", secondaryStatus.getJobCategoryMaster().getJobCategoryId());
					 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion3,criterion4);
				 }else{
					 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion4);
				 }
			 }else{
				 criterion2	=	Restrictions.eq("districtMaster", secondaryStatus.getDistrictMaster());
				 criterion4	=	Restrictions.isNotNull("assessmentDocument");
				 if(secondaryStatus.getJobCategoryMaster()!=null){
					 criterion3	=	Restrictions.eq("jobCategoryId", secondaryStatus.getJobCategoryMaster().getJobCategoryId());
					 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion3,criterion4);
				 }else{
					 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2,criterion4);
				 }
			 }
		}catch (Exception e){
			e.printStackTrace();
		}
		return lstJobCategoryMaster;
	}

	@Transactional(readOnly=true)
	public List<JobCategoryMaster> getJobSubCategoryByJobCateId(List<JobCategoryMaster> jobCateIds)
	{
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		System.out.println("<--------jobCateIds--------->"+jobCateIds);
		try
		{
			 Criterion criterion1 	= 	Restrictions.eq( "status","A");
			 Criterion criterion2	=	Restrictions.in( "parentJobCategoryId",jobCateIds);
			 lstJobCategoryMaster 	=	findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("--------lstJobCategoryMaster---------"+lstJobCategoryMaster.size());
		return lstJobCategoryMaster;
	}

	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findActiveHeadQuarterJobCategory()
	{
	         List <JobCategoryMaster> lstJobCategoryMasters = new ArrayList<JobCategoryMaster>();
			 Criterion criterion1 = Restrictions.eq( "status","A");
			 Criterion criterion2 = Restrictions.isNotNull("headQuarterMaster");
			 
			 lstJobCategoryMasters = findByCriteria(Order.asc("jobCategoryName"),criterion1,criterion2, Restrictions.isNull("branchMaster"),Restrictions.isNull("districtMaster"));  
			 
			 return lstJobCategoryMasters;
	}
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findByCategoryIDList(List<Integer> jobcategoryIds)
	{
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		try{
			
			if(jobcategoryIds.size()>0){
			 Criterion criterion1 	= 	Restrictions.in("jobCategoryId",jobcategoryIds);
			 lstJobCategoryMaster 	=	findByCriteria(criterion1);
			}	
			
		}catch (Exception e){
			e.printStackTrace();
		}
		return lstJobCategoryMaster;
	}
	
	
	//all category including master and sub
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> getMasterAndSubByCategoryID(List<Integer> jobcategoryIds)
	{
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		List<JobCategoryMaster> lstParentJobCat= new ArrayList<JobCategoryMaster>();
		
		if(jobcategoryIds!=null && jobcategoryIds.size()>0)
		{
		 JobCategoryMaster jobCatId=new JobCategoryMaster();
			for(Integer id: jobcategoryIds)
			{
				jobCatId=new JobCategoryMaster();
				jobCatId.setJobCategoryId(id);
				lstParentJobCat.add(jobCatId);
			}
		}
		
		try{
			
			if(jobcategoryIds.size()>0){
			 Criterion criterion1 	= 	Restrictions.in("parentJobCategoryId",lstParentJobCat);
			 Criterion criterion2 	= 	Restrictions.in("jobCategoryId",jobcategoryIds);
			 Criterion criterion3   = 	Restrictions.or(criterion1,criterion2);
			 lstJobCategoryMaster 	=	findByCriteria(criterion3);
			}	
			
		}catch (Exception e){
			e.printStackTrace();
		}
		return lstJobCategoryMaster;
	}
	
	@Transactional(readOnly=false)
	public List<JobCategoryMaster> findJobCategorysByDistricts(List<DistrictMaster> districtMasters){
		List<JobCategoryMaster> lstJobCategoryMaster= null;
		try{
			Criterion criterionDistrict = Restrictions.in("districtMaster",districtMasters);
			lstJobCategoryMaster = findByCriteria(criterionDistrict);
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return lstJobCategoryMaster;
	}
	
	@Transactional(readOnly=false)
	public List<JobCategoryMaster> findJobCategorysByHeadQuarter(HeadQuarterMaster headQuarterMaster){
		List<JobCategoryMaster> lstJobCategoryMaster= null;
		try 
		{
			Criterion criterion = Restrictions.and(Restrictions.and(Restrictions.isNull("branchMaster"), Restrictions.isNotNull("headQuarterMaster")), Restrictions.isNull("districtMaster"));
			Criterion criterion1 = Restrictions.eq("headQuarterMaster", headQuarterMaster);
			lstJobCategoryMaster = findByCriteria(criterion,criterion1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstJobCategoryMaster;
	}
	@Transactional(readOnly=false)
	public List<JobCategoryMaster> findJobCategorysByHeadQuarterAndID(HeadQuarterMaster headQuarterMaster,List<Integer> jobCatList){
		List<JobCategoryMaster> lstJobCategoryMaster= null;
		try 
		{
			Criterion criterion = Restrictions.and(Restrictions.and(Restrictions.isNull("branchMaster"), Restrictions.isNotNull("headQuarterMaster")), Restrictions.isNull("districtMaster"));
			Criterion criterion1 = Restrictions.eq("headQuarterMaster", headQuarterMaster);
			Criterion criterion2 = Restrictions.in("jobCategoryId", jobCatList);
			lstJobCategoryMaster = findByCriteria(criterion,criterion1,criterion2);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstJobCategoryMaster;
	}
	
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> getJobSubCategoryByParentJobCategory(JobCategoryMaster jobCateId)
	{
		List<JobCategoryMaster> lstJobCategoryMaster= new ArrayList<JobCategoryMaster>();
		try
		{
			 Criterion criterion1 	= 	Restrictions.eq( "status","A");
			 Criterion criterion2	=	Restrictions.eq( "parentJobCategoryId",jobCateId);
			 lstJobCategoryMaster 	=	findByCriteria(criterion1,criterion2);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return lstJobCategoryMaster;
	}
	@Transactional(readOnly=true)
	public JobCategoryMaster findJobCategoryByNameAndDistrict(String jobCategoryName,DistrictMaster districtMaster)
	{
	         List <JobCategoryMaster> lstJobCategoryMasters = null;
			 Criterion criterion = Restrictions.eq( "jobCategoryName",jobCategoryName);
			 Criterion criterion1 = Restrictions.eq( "districtMaster",districtMaster);
			 lstJobCategoryMasters = findByCriteria(Order.asc("jobCategoryName"),criterion,criterion1);  
			 if(lstJobCategoryMasters==null || lstJobCategoryMasters.size()==0)
				 return null;
			 else
				 return lstJobCategoryMasters.get(0);
	}
	@Transactional(readOnly=false)
	public List<JobCategoryMaster> findAllMasterJobCategorysOfHeadQuarter(HeadQuarterMaster headQuarterMaster){
		List<JobCategoryMaster> lstJobCategoryMaster= null;
		try 
		{
			Criterion criterion = Restrictions.and(Restrictions.isNull("branchMaster"), Restrictions.isNull("districtMaster"));
			Criterion criterion1 = Restrictions.isNull("parentJobCategoryId");
			Criterion criterion2 = Restrictions.eq("headQuarterMaster", headQuarterMaster);
			lstJobCategoryMaster = findByCriteria(criterion,criterion1,criterion2);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstJobCategoryMaster;
	}
	
	// Link to KSN
	@Transactional(readOnly=true)
	public List<JobCategoryMaster> findJobCategoryByNameAndHQBR(String jobCategoryName,UserMaster userMaster)
	{
	         List <JobCategoryMaster> lstJobCategoryMasters = new ArrayList<JobCategoryMaster>();
			 Criterion criterion = Restrictions.eq( "jobCategoryName",jobCategoryName);
			 Criterion criterion1 = null;
			 criterion1 = Restrictions.eq("headQuarterMaster",userMaster.getHeadQuarterMaster());
			 lstJobCategoryMasters = findByCriteria(criterion,criterion1);  
			 
		return lstJobCategoryMasters;
	}	
	
	@Transactional(readOnly=true)
	public JobCategoryMaster findJobCategoryByDistrictandId(Integer jobCategoryId,DistrictMaster districtMaster)
	{

	         List <JobCategoryMaster> lstJobCategoryMasters = null;
			 Criterion criterion1=Restrictions.eq( "jobCategoryId",jobCategoryId);
			 Criterion criterion2=Restrictions.eq("districtMaster", districtMaster);
			 Criterion criterion3=Restrictions.eq( "status","A");
			 			 
			 lstJobCategoryMasters = findByCriteria(criterion1,criterion2,criterion3);  
			 if(lstJobCategoryMasters==null || lstJobCategoryMasters.size()==0)
				 return null;
			 else
				 System.out.println("????????????????????????????????????????????????????          "+lstJobCategoryMasters);
				 return lstJobCategoryMasters.get(0);

	}
	
	@Transactional(readOnly=false)
	public List<JobCategoryMaster> findJobCategoryByOfferJSI()
	{

	         List <JobCategoryMaster> lstJobCategoryMasters = null;
			 Criterion criterion1=Restrictions.eq( "offerJSI",1);	
			 Criterion criterion2=Restrictions.isNotNull("districtMaster");	
			 
			 lstJobCategoryMasters = findByCriteria(criterion1,criterion2); 			
			 return lstJobCategoryMasters;

	}
	
	
	@Transactional(readOnly=true)
	public JobCategoryMaster getCategoryMasterByCategory_Op(Integer categoryId)
	{
		JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();
		try {
			Session session = getSession();
			jobCategoryMaster = (JobCategoryMaster)session.createCriteria(JobCategoryMaster.class).add(Restrictions.eq("jobCategoryId", categoryId)).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			 
		}
        return jobCategoryMaster;
	}
@Transactional(readOnly=false)
	public List<JobCategoryMaster> findAllJobSubCategoryByJobCateId_Op(JobCategoryMaster jobCateId,DistrictMaster  districtMaster)
	{
 		 List<JobCategoryMaster> list=new ArrayList<JobCategoryMaster>();
		try 
		{
  			Query query1=null;
  			List<Object[]> objList=null;
			if(jobCateId!=null &&districtMaster!=null){
 				Session session = getSession();
  				
  				String hql=	"SELECT  jcm.jobCategoryId,jcm.jobCategoryName,jcm.approvalByPredefinedGroups, jcm.baseStatus, jcm.assessmentDocument FROM  jobcategorymaster jcm "
			 	    +   "left join districtmaster dm on dm.districtId= jcm.districtId "
			 	    + " WHERE   jcm.status=? and jcm.districtId=? and jcm.parentJobCategoryId=?  order by jcm.jobCategoryName ASC ";
 				query1=session.createSQLQuery(hql);
 				query1.setParameter(0, "A");
 				query1.setParameter(1, districtMaster.getDistrictId());
 				query1.setParameter(2, jobCateId.getJobCategoryId());
 	 	 		objList = query1.list(); 
 	 	 		JobCategoryMaster obj=null;
				if(objList.size()>0 && objList!=null ){
				for (int j = 0; j < objList.size(); j++) {
						Object[] objArr2 = objList.get(j);
						obj = new JobCategoryMaster();
						  obj.setJobCategoryId(objArr2[0] == null ? 0 : Integer.parseInt(objArr2[0].toString()));
						  obj.setJobCategoryName(objArr2[1] == null ? "NA" :  objArr2[1].toString());
						  obj.setApprovalByPredefinedGroups(Boolean.parseBoolean(objArr2[2].toString()));
						  obj.setBaseStatus(Boolean.parseBoolean(objArr2[3].toString()));
						  obj.setAssessmentDocument(objArr2[4] == null ? "NA" :  objArr2[4].toString());
			 		 	list.add(obj);
				   }
				}
 			}
		} 
	 	catch(Exception e){
			   
		         e.printStackTrace(); 
		}finally{
			// session.close(); 
		}
		return list;
	}	 
	
	@Transactional(readOnly=false)
	public List<JobWisePanelStatus> checkSecStatusJobOrderWise_Op(JobOrder jobOrder)
	{
 		 List<JobWisePanelStatus> list=new ArrayList<JobWisePanelStatus>();
		try 
		{
  			Query query1=null;
  			List<Object[]> objList=null;
			if(jobOrder!=null){
 				Session session = getSession();
  				String hql=	"SELECT  jWP.secondaryStatusId,jWP.panelStatus   FROM  jobwisepanelstatus jWP "
			 	    +   "left join joborder jo on jo.jobId= jWP.jobId "
			 	   +   "left join secondarystatus ss on ss.secondaryStatusId= jWP.secondaryStatusId "
				    + " WHERE   jWP.jobId=? and jWP.secondaryStatusId  IS NOT NULL ";
 				query1=session.createSQLQuery(hql);
 				query1.setParameter(0, jobOrder.getJobId());
 				objList = query1.list(); 
  		 		JobWisePanelStatus obj=null;
				if(objList.size()>0 && objList!=null ){
				for (int j = 0; j < objList.size(); j++) {
						Object[] objArr2 = objList.get(j);
						obj = new JobWisePanelStatus();
						int secondaryStatusId=objArr2[0] == null ? 0 : Integer.parseInt(objArr2[0].toString());
						SecondaryStatus secondaryStatus=new SecondaryStatus();
	 		 				secondaryStatus.setSecondaryStatusId(secondaryStatusId);
			 				obj.setSecondaryStatus(secondaryStatus);
	 				 	obj.setPanelStatus(Boolean.parseBoolean(objArr2[1].toString()));
		 			 	list.add(obj);
				   }
				}
			 
			}
		} 
	 	catch(Exception e){
			   
		         e.printStackTrace(); 
		}finally{
	 	}
		return list;
	}
	
	
	 
}