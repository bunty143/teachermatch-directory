package tm.dao.master;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.master.StatusMaster;
import tm.bean.master.StatusNodeMaster;
import tm.dao.generic.GenericHibernateDAO;

public class StatusNodeMasterDAO extends GenericHibernateDAO<StatusNodeMaster, Integer>
{
	public StatusNodeMasterDAO()
	{
		super(StatusNodeMaster.class);
	}
	
	@Transactional(readOnly=false)
	public List<StatusNodeMaster> findStatusNodeList()
	{
		List<StatusNodeMaster> lstStatus= null;
		try 
		{
			Criterion criterion=Restrictions.eq("status","A");
			lstStatus = findByCriteria(Order.asc("orderNumber"),criterion);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus==null || lstStatus.size()==0)
		return null;
		else
		return lstStatus;
	}
}
