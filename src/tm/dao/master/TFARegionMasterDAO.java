package tm.dao.master;

import tm.bean.master.TFARegionMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TFARegionMasterDAO extends GenericHibernateDAO<TFARegionMaster, Integer> 
{
	public TFARegionMasterDAO()
	{
		super(TFARegionMaster.class);
	}
}
