package tm.dao.master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificRefChkAnswers;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificRefChkAnswersDAO extends GenericHibernateDAO<DistrictSpecificRefChkAnswers, Integer> 
{
	public DistrictSpecificRefChkAnswersDAO() 
	{
		super(DistrictSpecificRefChkAnswers.class);
	}

	@Transactional(readOnly=false)
	public List<DistrictSpecificRefChkAnswers> findTeacherAnswersByDistrictAndJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<DistrictSpecificRefChkAnswers> teacherAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<DistrictSpecificRefChkAnswers>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
			
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}

	@Transactional(readOnly=false)
	public List<JobOrder> findLastTeacherAnswersByDistrictAndJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()));
			criteria.addOrder(Order.desc("createdDateTime"));
			criteria.setProjection(Projections.property("jobOrder"));
			criteria.setMaxResults(1);
			jobOrders = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return jobOrders;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> findTeacherAnswersForQQByDistrict(TeacherDetail teacherDetail,DistrictMaster districtMaster) 
	{
		Session session = getSession();
		
		System.out.println("session::::::"+session.getEntityName(teacherDetail));
		
		String sql =" select questionId,answerId from districtspecificrefchkanswers where districtId=:districtMaster and teacherId=:teacherDetail";
		
		//System.out.println(sql);

		Query query = session.createSQLQuery(sql);

		query.setParameter("teacherDetail", teacherDetail);
		query.setParameter("districtMaster", districtMaster);

		List<Object[]> rows = query.list();

		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				Object obj[] = (Object[])oo;
				map.put((Integer)obj[0], (Integer)obj[1]);
			}
		}
		
		System.out.println(":::::::::::::::::::::::"+map);
		return map;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public String updateOtherQQ(TeacherDetail teacherDetail,DistrictMaster districtMaster,List<Integer> answers) 
	{
		Session session = getSession();
		String hql = "UPDATE districtspecificrefchkanswers set isActive = :isActive WHERE districtMaster = :districtMaster " +
				" and teacherDetail = :teacherDetail and answerId not IN (:answers) ";
		
		Query query = session.createQuery(hql);
		query.setParameter("isActive", false);
		query.setParameter("districtMaster", districtMaster);
		query.setParameter("teacherDetail", teacherDetail);
		query.setParameterList("answers", answers);
		int result = query.executeUpdate();
		System.out.println("Rows affected: " + result);
		
		return ""+result;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificRefChkAnswers> findSpecificRefChkAnswers(TeacherDetail teacherDetail,JobOrder jobOrder,int dspqFlag)
	{
		List<DistrictSpecificRefChkAnswers> teacherAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<DistrictSpecificRefChkAnswers>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion5 = null;
			if( jobOrder.getDistrictMaster()!=null)
			  criterion5 = Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
			else if( jobOrder.getHeadQuarterMaster()!=null)
				 criterion5 = Restrictions.eq("headQuarterMasterId", jobOrder.getHeadQuarterMaster().getHeadQuarterId());
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(dspqFlag==0){
				Criterion criterion3 = Restrictions.eq("jobOrder",jobOrder);
				criteria.add(criterion3);
			}else if( dspqFlag==1){
				Criterion criterion4 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
				criteria.add(criterion4);
			}else{
				Criterion criterion6 = Restrictions.isNull("jobCategoryMaster");
				criteria.add(criterion6);
			}
			criteria.add(criterion1);
			if(criterion5!=null)
			criteria.add(criterion5);
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificRefChkAnswers> findSpecificRefChkAnswersByDistrictAndJob(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtMaster,int dspqFlag)
	{
		List<DistrictSpecificRefChkAnswers> teacherAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<DistrictSpecificRefChkAnswers>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion5 =null;
			if((jobOrder!=null && jobOrder.getDistrictMaster()!=null) || districtMaster!=null){
				criterion5=Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()!=null?jobOrder.getDistrictMaster() :districtMaster);
			}else if (jobOrder!=null && jobOrder.getHeadQuarterMaster()!=null){
				criterion5=Restrictions.eq("headQuarterMasterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId());
			}
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(dspqFlag==0){
				Criterion criterion3 = Restrictions.eq("jobOrder",jobOrder);
				criteria.add(criterion3);
			}else if( dspqFlag==1){
				Criterion criterion4 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
				criteria.add(criterion4);
			}else{
				Criterion criterion6 = Restrictions.isNull("jobCategoryMaster");
				criteria.add(criterion6);
			}
			criteria.add(criterion1);
			criteria.add(criterion5);
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}
	
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificRefChkAnswers> findTeacherAnswersByDistrict(TeacherDetail teacherDetail)
	{
		List<DistrictSpecificRefChkAnswers> teacherAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<DistrictSpecificRefChkAnswers>();
		try 
		{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificRefChkAnswers> findTeacherAnswersByDistrictAndRefChk(HeadQuarterMaster headQuarterMaster, DistrictMaster districtMaster, Integer elerefAutoId)
	{
		List<DistrictSpecificRefChkAnswers> teacherAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<DistrictSpecificRefChkAnswers>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = null;
			if(districtMaster!=null)
			criterion1 = Restrictions.eq("districtMaster",districtMaster);
			else if(headQuarterMaster!=null){
				criterion1 = Restrictions.eq("headQuarterMasterId",headQuarterMaster.getHeadQuarterId());
			}
			Criterion criterion2 = Restrictions.eq("elerefAutoId",elerefAutoId);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificRefChkAnswers> allRecordByDistrictIdAndTeacherId(DistrictMaster districtMaster, TeacherDetail teacherDetail){
		
		List<DistrictSpecificRefChkAnswers>  districtSpecificRefChkAnswersList = new ArrayList<DistrictSpecificRefChkAnswers>();
		try{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
				Criterion criterion2 = Restrictions.eq("teacherDetail", teacherDetail);
				Criteria criteria	 = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				districtSpecificRefChkAnswersList = criteria.list();

		} catch (Exception exception){
			exception.printStackTrace();
		}

		return districtSpecificRefChkAnswersList;
	}

	@Transactional(readOnly=false)
	public List<DistrictSpecificRefChkAnswers> allRecordByDistrictIdAndTeacherList(DistrictMaster districtMaster, List<TeacherDetail> teacherDetails){
		
		List<DistrictSpecificRefChkAnswers>  districtSpecificRefChkAnswersList = new ArrayList<DistrictSpecificRefChkAnswers>();
		try{
				Session session = getSession();
				Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
				Criterion criterion2 = Restrictions.in("teacherDetail", teacherDetails);
				Criteria criteria	 = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				districtSpecificRefChkAnswersList = criteria.list();

		} catch (Exception exception){
			exception.printStackTrace();
		}

		return districtSpecificRefChkAnswersList;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificRefChkAnswers> recordByTeacherIdAndRef(int ref,TeacherDetail tid){
		
		List<DistrictSpecificRefChkAnswers>  districtSpecificRefChkAnswersList = new ArrayList<DistrictSpecificRefChkAnswers>();
		try{
				
				Criterion criterion1 = Restrictions.eq("elerefAutoId", ref);
				Criterion criterion2 = Restrictions.eq("teacherDetail", tid);
				districtSpecificRefChkAnswersList=findByCriteria(criterion1,criterion2);
				
		} catch (Exception exception){
			exception.printStackTrace();
		}

		return districtSpecificRefChkAnswersList;
	}
	
}
