package tm.dao.master;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioAnswers;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.bean.master.JobCategoryMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class DistrictSpecificPortfolioAnswersDAO extends GenericHibernateDAO<DistrictSpecificPortfolioAnswers, Integer> 
{
	public DistrictSpecificPortfolioAnswersDAO() 
	{
		super(DistrictSpecificPortfolioAnswers.class);
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioAnswers> findTeacherAnswersByDistrictAndJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<DistrictSpecificPortfolioAnswers> teacherAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
			
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> findLastTeacherAnswersByDistrictAndJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()));
			criteria.addOrder(Order.desc("createdDateTime"));
			criteria.setProjection(Projections.property("jobOrder"));
			criteria.setMaxResults(1);
			jobOrders = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return jobOrders;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> findTeacherAnswersForQQByDistrict(TeacherDetail teacherDetail,DistrictMaster districtMaster) 
	{
		Session session = getSession();

		String sql =" select questionId,answerId from districtspecificportfolioanswers where districtId=:districtMaster and teacherId=:teacherDetail";
		
		System.out.println(sql);

		Query query = session.createSQLQuery(sql);

		query.setParameter("teacherDetail", teacherDetail);
		query.setParameter("districtMaster", districtMaster);

		List<Object[]> rows = query.list();

		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				Object obj[] = (Object[])oo;
				map.put((Integer)obj[0], (Integer)obj[1]);
			}
		}
		return map;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public String updateOtherQQ(TeacherDetail teacherDetail,DistrictMaster districtMaster,List<Integer> answers) 
	{
		Session session = getSession();
		String hql = "UPDATE districtspecificportfolioanswers set isActive = :isActive WHERE districtMaster = :districtMaster " +
				" and teacherDetail = :teacherDetail and answerId not IN (:answers) ";
		
		Query query = session.createQuery(hql);
		query.setParameter("isActive", false);
		query.setParameter("districtMaster", districtMaster);
		query.setParameter("teacherDetail", teacherDetail);
		query.setParameterList("answers", answers);
		int result = query.executeUpdate();
		System.out.println("Rows affected: " + result);
		
		return ""+result;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioAnswers> findSpecificPortfolioAnswers(TeacherDetail teacherDetail,JobOrder jobOrder,int dspqFlag)
	{
		List<DistrictSpecificPortfolioAnswers> teacherAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion5 = null;
			if(jobOrder!=null && jobOrder.getDistrictMaster()!=null)
				criterion5=Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
			
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(dspqFlag==0){
				Criterion criterion3 = Restrictions.eq("jobOrder",jobOrder);
				criteria.add(criterion3);
				if(criterion5!=null)
					criteria.add(criterion5);
			}else if( dspqFlag==1){
				Criterion criterion4 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
				criteria.add(criterion4);
				if(criterion5!=null)
					criteria.add(criterion5);
			}else if( dspqFlag==2){
				Criterion criterion6 = Restrictions.isNull("jobCategoryMaster");
				criteria.add(criterion6);
				if(jobOrder.getHeadQuarterMaster()==null && criterion5!=null)
					criteria.add(criterion5);
			}
			else if( dspqFlag==3){
				//Criterion criterionH = Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster());
				//Criterion criterionB = Restrictions.isNull("branchMaster");
				Criterion criterionH = Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId());
				Criterion criterionB = Restrictions.isNull("branchId");
				criteria.add(criterionH);
				criteria.add(criterionB);
			}
			else if( dspqFlag==4){
				//Criterion criterionH = Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster());
				//Criterion criterionB = Restrictions.eq("branchMaster",jobOrder.getBranchMaster());
				
				Criterion criterionH = Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId());
				Criterion criterionB = Restrictions.eq("branchId",jobOrder.getBranchMaster().getBranchId());
				
				criteria.add(criterionH);
				criteria.add(criterionB);
			}
			criteria.add(criterion1);
			
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		System.out.println("teacherAnswerDetailsForDistrictSpecificQuestionList   "+teacherAnswerDetailsForDistrictSpecificQuestionList.size());
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}

	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioAnswers> findSpecificPortfolioAnswersOnProfile(TeacherDetail teacherDetail,UserMaster userMaster,int dspqFlag)
	{
		List<DistrictSpecificPortfolioAnswers> teacherAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("districtMaster", userMaster.getDistrictId());
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion3 = Restrictions.isNull("jobCategoryMaster");
			Criterion criterion4 = Restrictions.isNull("jobOrder");
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioAnswers> findSpecificPortfolioAnswersByTeacherAndJob(List<TeacherDetail> teacherDetails,JobOrder jobOrder)
	{
		List<DistrictSpecificPortfolioAnswers> tADFDSQuestionList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		List<DistrictSpecificPortfolioAnswers> tADFDSQuestionFinalList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		try 
		{
			if(teacherDetails.size()>0){
				Map<Integer,TeacherDetail> mapTeachers= new HashMap<Integer, TeacherDetail>();
				for (TeacherDetail teacherDetail : teacherDetails) {
					mapTeachers.put(teacherDetail.getTeacherId(),teacherDetail);
				}
				System.out.println("teacherDetails:::"+teacherDetails.size());
				Criterion criterion7 = Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
				Criterion criterion2 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
				Criterion criterion3 = Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
				tADFDSQuestionList = findByCriteria(criterion1,criterion3,criterion7);
				if(tADFDSQuestionList.size()>0){
					tADFDSQuestionFinalList.addAll(tADFDSQuestionList);
				}
				System.out.println("tADFDSQuestionFinalList:::::::::::::"+tADFDSQuestionFinalList.size());
				List<TeacherDetail> teacherDetailsListA=new ArrayList<TeacherDetail>();
				if(tADFDSQuestionList.size()>0){
					for (DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : tADFDSQuestionList) {
						if(mapTeachers.get(districtSpecificPortfolioAnswers.getTeacherDetail().getTeacherId())==null){
							teacherDetailsListA.add(districtSpecificPortfolioAnswers.getTeacherDetail());
						}else{
							mapTeachers.remove(districtSpecificPortfolioAnswers.getTeacherDetail().getTeacherId());
						}
					}
				}
				System.out.println("teacherDetailsListA::"+teacherDetailsListA.size());
				
				if(tADFDSQuestionList.size()==0){
					teacherDetailsListA=teacherDetails;
				}
				if(teacherDetailsListA.size()>0){
					Criterion criterion8 = Restrictions.in("teacherDetail",teacherDetailsListA);
					Criterion criterion6 = Restrictions.isNull("jobOrder");
					tADFDSQuestionList = findByCriteria(criterion2,criterion3,criterion6,criterion8);
					if(tADFDSQuestionList.size()>0){
						tADFDSQuestionFinalList.addAll(tADFDSQuestionList);
					}
				}
				System.out.println("tADFDSQuestionFinalList::::::::::A:::"+tADFDSQuestionFinalList.size());
				List<TeacherDetail> teacherDetailsListB=new ArrayList<TeacherDetail>();
				if(tADFDSQuestionList.size()>0){
					for (DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : tADFDSQuestionList) {
						if(mapTeachers.get(districtSpecificPortfolioAnswers.getTeacherDetail().getTeacherId())==null){
							teacherDetailsListB.add(districtSpecificPortfolioAnswers.getTeacherDetail());
						}else{
							mapTeachers.remove(districtSpecificPortfolioAnswers.getTeacherDetail().getTeacherId());
						}
					}
				}
				System.out.println("teacherDetailsListB::"+teacherDetailsListB.size());
				if(tADFDSQuestionList.size()==0){
					teacherDetailsListB=teacherDetails;
				}
				if(teacherDetailsListB.size()>0){
					Criterion criterion9 = Restrictions.in("teacherDetail",teacherDetailsListB);
					Criterion criterion5 = Restrictions.isNull("jobCategoryMaster");
					Criterion criterion6 = Restrictions.isNull("jobOrder");
					tADFDSQuestionList = findByCriteria(criterion3,criterion5,criterion6,criterion9);
					if(tADFDSQuestionList.size()>0){
						tADFDSQuestionFinalList.addAll(tADFDSQuestionList);
					}
				}	
				System.out.println("tADFDSQuestionFinalList::::::::::B:::"+tADFDSQuestionFinalList.size());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		
		return tADFDSQuestionFinalList;
	}

	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioAnswers> findSpecificPortfolioAnswersByTeacherAndDistrict(DistrictMaster districtMaster)
	{
		List<DistrictSpecificPortfolioAnswers> tADFDSQuestionList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		List<DistrictSpecificPortfolioAnswers> tADFDSQuestionFinalList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		try 
		{
				
					Criterion criterion1 = Restrictions.isNull("jobCategoryMaster");
					Criterion criterion2 = Restrictions.isNull("jobOrder");
					Criterion criterion3 = Restrictions.eq("districtMaster",districtMaster);
					tADFDSQuestionList = findByCriteria(criterion3,criterion2,criterion1);
					if(tADFDSQuestionList.size()>0){
						tADFDSQuestionFinalList.addAll(tADFDSQuestionList);
					}
				System.out.println("tADFDSQuestionFinalList::::::::::B:::"+tADFDSQuestionFinalList.size());
			
		}catch (Exception e) {
			e.printStackTrace();
		}		
		
		return tADFDSQuestionFinalList;
	}
	
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioAnswers> getDSPAByDID_TIDs(DistrictMaster districtMaster,List<TeacherDetail> teacherDetails,List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestions)
	{
		List<DistrictSpecificPortfolioAnswers> lstDistrictSpecificPortfolioAnswers = new ArrayList<DistrictSpecificPortfolioAnswers>();
		try 
		{
			if(teacherDetails!=null && teacherDetails.size() >0 && districtSpecificPortfolioQuestions!=null && districtSpecificPortfolioQuestions.size()>0)
			{
				Criterion criterion1 = Restrictions.isNull("jobCategoryMaster");
				Criterion criterion2 = Restrictions.isNull("jobOrder");
				Criterion criterion3 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion4 = Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion5 = Restrictions.in("districtSpecificPortfolioQuestions",districtSpecificPortfolioQuestions);
				lstDistrictSpecificPortfolioAnswers = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstDistrictSpecificPortfolioAnswers;
		}
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioAnswers> findByDistrictAndQuestion(DistrictMaster districtMaster, DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions){
		
		List<DistrictSpecificPortfolioAnswers> districtSpecificPortfolioAnswersList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		try{
			Criterion criterion1 = null;
			Criterion criterion2 = null;
			
			if(districtMaster!=null){
				criterion1 = Restrictions.eq("districtMaster",districtMaster);
			}
			criterion2 = Restrictions.eq("districtSpecificPortfolioQuestions", districtSpecificPortfolioQuestions);
			
			if(districtMaster!=null){
				districtSpecificPortfolioAnswersList = findByCriteria(criterion1,criterion2);
			} else {
				districtSpecificPortfolioAnswersList = findByCriteria(criterion2);
			}
		}catch(Exception exception){
			exception.printStackTrace();
		}
		return districtSpecificPortfolioAnswersList;
	}
	
	
	/*@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int getNumberOfRecordByDistrictAndQuestion(DistrictMaster districtMaster, DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions)
	{
		int numberOfRecord = 0;
		if(districtMaster!=null){
			try{
				Session session = getSession();
				String sql  = "SELECT COUNT(referenceQuestionId) AS count FROM districtspecificportfolioanswers dsp WHERE dsp.questionId = "+districtSpecificPortfolioQuestions.getQuestionId()+" AND dsp.districtId = "+districtMaster.getDistrictId()+"";
				Query query = session.createSQLQuery(sql);
				List<BigInteger> rowCount = query.list();
				if(rowCount!=null && rowCount.size()==1)
					numberOfRecord=rowCount.get(0).intValue();			
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		return numberOfRecord;	
	}*/
		@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioAnswers> findSpecificPortfolioAnswersForTeachers(List<TeacherDetail> teacherDetails,JobOrder jobOrder,int dspqFlag)
	{
		List<DistrictSpecificPortfolioAnswers> teacherAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion5 = Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(dspqFlag==0){
				Criterion criterion3 = Restrictions.eq("jobOrder",jobOrder);
				criteria.add(criterion3);
			}else if( dspqFlag==1){
				Criterion criterion4 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
				criteria.add(criterion4);
			}else{
				Criterion criterion6 = Restrictions.isNull("jobCategoryMaster");
				criteria.add(criterion6);
			}
			criteria.add(criterion1);
			criteria.add(criterion5);
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioAnswers> findDSPQAnswersForPortfolioReminder(TeacherDetail teacherDetail,DistrictMaster districtMaster,int dspqFlag)
	{
		List<DistrictSpecificPortfolioAnswers> teacherAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion5 = Restrictions.eq("districtMaster", districtMaster);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion5);
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}
	
	@Transactional(readOnly=false)
	public List nobleStDSPQ(int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName)
	{
		System.out.println("sortingcheck="+sortingcheck+"sortOrderStrVal=="+sortOrderStrVal+"start="+start+"noOfRow="+noOfRow+"report="+report+"sortColomnName="+sortColomnName);
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();
			
			
	        System.out.println(" sortOrderStrVal :: "+sortOrderStrVal+" sortColomnName "+sortColomnName);
	      
			

	                    if(sortColomnName.equalsIgnoreCase("teacherId"))
	        				sortColomnName = "Internal_Teacher_ID";
	        			else if(sortColomnName.equalsIgnoreCase("schoolname"))
	        				sortColomnName = "School_Name";
	        			else if(sortColomnName.equalsIgnoreCase("questionid"))
	        				sortColomnName = "Question_ID";
	        			else if(sortColomnName.equalsIgnoreCase("question"))
	        				sortColomnName = "Questions";
	        			else if(sortColomnName.equalsIgnoreCase("answer"))
	        				sortColomnName = "Answer"; 
	        			else if(sortColomnName.equalsIgnoreCase("wianswer"))
	        				sortColomnName = "Write_In_Answer";
	        			else if(sortColomnName.equalsIgnoreCase("dspqscore"))
	        				sortColomnName = "DSPQ_Score";
	        			else if(sortColomnName.equalsIgnoreCase("scoreby"))
	        				sortColomnName = "Scored_By";
	        			else if(sortColomnName.equalsIgnoreCase("scoredata"))
	        				sortColomnName = "Scored_Date";
	        			else if(sortColomnName.equalsIgnoreCase("Answer_Date"))
	        				sortColomnName = "epi.teachernormScore";
	        			else if(sortColomnName.equalsIgnoreCase("answerorder"))
	        				sortColomnName = "Order_of_Answers_for_Multiple_Opeions";
			
	      
			String orderby = " order by "+sortColomnName+" "+sortOrderStrVal ;
			
			 sql=" select "+
			 "	teacherid as `Internal_Teacher_ID`,  "+
			 "   ifnull(schoolmaster.schoolname,'') as `School_Name`, "+
			 "	questionid as `Question_ID`,  "+
			  "   question as `Questions`,  "+
			 "	ifnull(questionOption,'') as `Answer`,  "+
			 "	left(ifnull(REPLACE(REPLACE(insertedText, CHAR(13), ' '), CHAR(10), ' '),''),850) as `Write_In_Answer`,  "+
			  "   ifnull(sliderScore,'') `DSPQ_Score`,  "+
			   "  case when concat(ifnull(usermaster.lastname,''), ', ', ifnull(usermaster.firstname,''), ' ', ifnull(usermaster.title,''))=',  ' then '' "+
			 "		else concat(ifnull(usermaster.lastname,''), ', ', ifnull(usermaster.firstname,''), ' ', ifnull(usermaster.title,'')) end as `Scored_By`, "+
			 "	ifnull(scoreDateTime,'') as `Scored_Date`,  "+
			  "   allanswers.createddatetime as `Answer_Date`,  "+
			 "	Source_Order as `Order_of_Answers_for_Multiple_Opeions` "+
			 " from  "+
			 "	( "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspa.selectedOptions, dspa.insertedRanks, "+
			 "			dspa.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "		where districtid='7800038' "+
			 "			and questionid != '7' "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 " union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '1' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 1), length(substring_index(selectedOptions, '|', 1 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01'  "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore, "+ 
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '2' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 2), length(substring_index(selectedOptions, '|', 2 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '3' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 3), length(substring_index(selectedOptions, '|', 3 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '4' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 4), length(substring_index(selectedOptions, '|', 4 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union            "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '5' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 5), length(substring_index(selectedOptions, '|', 5 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '6' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 6), length(substring_index(selectedOptions, '|', 6 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '7' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join teacherdetail td on td.teacherid=dspa.teacherid "+
			 "			left join districtspecificportfoliooptions dspo "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 7), length(substring_index(selectedOptions, '|', 7 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union  "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '8' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 8), length(substring_index(selectedOptions, '|', 8 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '9' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 9), length(substring_index(selectedOptions, '|', 9 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '10' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 10), length(substring_index(selectedOptions, '|', 10 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '11' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 11), length(substring_index(selectedOptions, '|', 11 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '12' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 12), length(substring_index(selectedOptions, '|', 12 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '13' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 13), length(substring_index(selectedOptions, '|', 13 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime, "+
			 "			dspa.createdDateTime, '14' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 14), length(substring_index(selectedOptions, '|', 14 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore, "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '15' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 15), length(substring_index(selectedOptions, '|', 15 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '16' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 16), length(substring_index(selectedOptions, '|', 16 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '17' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 17), length(substring_index(selectedOptions, '|', 17 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '18' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 18), length(substring_index(selectedOptions, '|', 18 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '19' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 19), length(substring_index(selectedOptions, '|', 19 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	union "+
			 "		select  "+
			 "			dspa.answerid, dspa.teacherid, dspa.districtid, dspa.jobCategoryId, dspa.jobId, dspa.schoolId, "+
			 "			dspa.questionId, dspa.question, dspa.questionTypeId, dspa.questionType, dspo.optionid, dspa.insertedRanks,  "+
			 "			dspo.questionOption, dspa.insertedText, dspa.optionScore, dspa.totalScore, dspa.maxMarks, dspa.sliderScore,  "+
			 "			dspa.scoreBy, dspa.scoreDateTime, dspa.isValidAnswer, dspa.isActive, dspa.updatedBy, dspa.updatedDateTime,  "+
			 "			dspa.createdDateTime, '20' as `Source_Order` "+
			 "		from districtspecificportfolioanswers dspa "+
			 "			left join districtspecificportfoliooptions dspo  "+
			 "			on dspo.optionid=replace(substring(substring_index(selectedOptions, '|', 20), length(substring_index(selectedOptions, '|', 20 - 1)) + 1), '|', '') "+
			 "		where districtid='7800038' "+
			 "			and dspa.questionid in ( '7') "+
			 "			and dspa.createdDateTime>='2014-12-01' "+
			 "			and dspo.questionOption is not null "+
			 "	) as allanswers "+
			 "		left join schoolmaster on allanswers.schoolid=schoolmaster.schoolid "+
			 "		left join usermaster on allanswers.scoreby=usermaster.userid "+
			""+orderby;
			
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		

		ResultSet rs=ps.executeQuery();
			
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			System.out.println("list size at dspq====="+lst.size());
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();  
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioAnswers> getDSPAByTID(TeacherDetail teacherDetail,List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestions)
	{
		List<DistrictSpecificPortfolioAnswers> lstDistrictSpecificPortfolioAnswers = new ArrayList<DistrictSpecificPortfolioAnswers>();
		try 
		{
			if(districtSpecificPortfolioQuestions!=null && districtSpecificPortfolioQuestions.size()>0)
			{
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.in("districtSpecificPortfolioQuestions",districtSpecificPortfolioQuestions);
				lstDistrictSpecificPortfolioAnswers = findByCriteria(criterion1,criterion2);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstDistrictSpecificPortfolioAnswers;
		}
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioAnswers> getByDistrictAndTeacherList(List<TeacherDetail> teacherDetailList,DistrictMaster districtMaster)
	{
		List<DistrictSpecificPortfolioAnswers> lstDistrictSpecificPortfolioAnswers = new ArrayList<DistrictSpecificPortfolioAnswers>();
		try 
		{
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetailList);
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				lstDistrictSpecificPortfolioAnswers = findByCriteria(criterion1,criterion2);
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstDistrictSpecificPortfolioAnswers;
		}
	}
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioAnswers> getByDistrictAndTeacher(DistrictMaster districtMaster,TeacherDetail teacherDetail)
	{
		List<DistrictSpecificPortfolioAnswers> lstDistrictSpecificPortfolioAnswers = new ArrayList<DistrictSpecificPortfolioAnswers>();
		try 
		{
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				lstDistrictSpecificPortfolioAnswers = findByCriteria(criterion1,criterion2);
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstDistrictSpecificPortfolioAnswers;
		}
	}
	
	
	@Transactional(readOnly=false)
	public List<DistrictSpecificPortfolioAnswers> findSpecificPortfolioAnswersTP(TeacherDetail teacherDetail,JobOrder jobOrder,int dspqFlag)
	{
		List<DistrictSpecificPortfolioAnswers> teacherAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		try 
		{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion5 = null;
			if(jobOrder!=null && jobOrder.getDistrictMaster()!=null)
				criterion5=Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
			
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion3 = Restrictions.isNull("jobOrder");
			criteria.add(criterion3);
			/*if(dspqFlag==0){
				Criterion criterion3 = Restrictions.eq("jobOrder",jobOrder);
				criteria.add(criterion3);
				criteria.add(criterion5);
			}else */if( dspqFlag==1){
				Criterion criterion4 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
				criteria.add(criterion4);
				criteria.add(criterion5);
			}else if( dspqFlag==2){
				Criterion criterion6 = Restrictions.isNull("jobCategoryMaster");
				criteria.add(criterion6);
				if(jobOrder.getHeadQuarterMaster()==null)
				criteria.add(criterion5);
			}
			else if( dspqFlag==3){
				//Criterion criterionH = Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster());
				//Criterion criterionB = Restrictions.isNull("branchMaster");
				Criterion criterionH = Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId());
				Criterion criterionB = Restrictions.isNull("branchId");
				criteria.add(criterionH);
				criteria.add(criterionB);
			}
			else if( dspqFlag==4){
				//Criterion criterionH = Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster());
				//Criterion criterionB = Restrictions.eq("branchMaster",jobOrder.getBranchMaster());
				
				Criterion criterionH = Restrictions.eq("headQuarterId",jobOrder.getHeadQuarterMaster().getHeadQuarterId());
				Criterion criterionB = Restrictions.eq("branchId",jobOrder.getBranchMaster().getBranchId());
				
				criteria.add(criterionH);
				criteria.add(criterionB);
			}
			criteria.add(criterion1);
			
			teacherAnswerDetailsForDistrictSpecificQuestionList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		System.out.println("teacherAnswerDetailsForDistrictSpecificQuestionList   "+teacherAnswerDetailsForDistrictSpecificQuestionList.size());
		return teacherAnswerDetailsForDistrictSpecificQuestionList;
	}
	
}


