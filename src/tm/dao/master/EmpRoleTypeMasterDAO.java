package tm.dao.master;

import tm.bean.master.EmpRoleTypeMaster;
import tm.dao.generic.GenericHibernateDAO;

public class EmpRoleTypeMasterDAO extends GenericHibernateDAO<EmpRoleTypeMaster, Integer> 
{
	public EmpRoleTypeMasterDAO() 
	{
		super(EmpRoleTypeMaster.class);
	}

}
