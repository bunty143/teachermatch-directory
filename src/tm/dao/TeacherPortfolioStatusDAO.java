package tm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.TeacherPortfolioStatus;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherPortfolioStatusDAO extends GenericHibernateDAO<TeacherPortfolioStatus, Integer>
{
	public TeacherPortfolioStatusDAO() 
	{
		super(TeacherPortfolioStatus.class);
	}

	@Transactional(readOnly=false)
	public TeacherPortfolioStatus findPortfolioStatusByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherPortfolioStatus> lstStatus= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			lstStatus = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus==null || lstStatus.size()==0)
			return null;
		else
			return lstStatus.get(0);
	}	
	@Transactional(readOnly=false)
	public List<TeacherPortfolioStatus> findAllTeacherPortfolioStatus(List teacherIds){
		List<TeacherPortfolioStatus> teacherPortfolioStatusList= null;
		try{
			if(teacherIds.size()>0){
				Criterion criterion1 = Restrictions.in("teacherId",teacherIds);	
				teacherPortfolioStatusList =	findByCriteria(criterion1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return teacherPortfolioStatusList;
	}
	@Transactional(readOnly=false)
	public Map<Integer, Boolean> findActivePortfolioStatus(List<TeacherDetail> teacherDetails)
	{
		List<TeacherPortfolioStatus> lstTeacherPortfolioStatus = null;
		Map<Integer,Boolean> mapPortfolioStatus = new TreeMap<Integer, Boolean>();
		try 
		{
			if(teacherDetails.size()>0)
			{
				Criterion criterion1 = Restrictions.in("teacherId",teacherDetails);
				lstTeacherPortfolioStatus = findByCriteria(criterion1);		
				for(TeacherPortfolioStatus portfolioStatus: lstTeacherPortfolioStatus){
					if(portfolioStatus.getIsPersonalInfoCompleted() && portfolioStatus.getIsAcademicsCompleted() && portfolioStatus.getIsCertificationsCompleted() && portfolioStatus.getIsExperiencesCompleted() && portfolioStatus.getIsAffidavitCompleted()){
						mapPortfolioStatus.put(portfolioStatus.getTeacherId().getTeacherId(), true);
					}
					else{
						mapPortfolioStatus.put(portfolioStatus.getTeacherId().getTeacherId(), false);
					}
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		return mapPortfolioStatus;
	}
	
	
	// Get portfolio status by teacher List
	@Transactional(readOnly=false)
	public HashMap<Integer, TeacherPortfolioStatus> findPortfolioStatusByTeacherlist(List<TeacherDetail> teacherList)
	{
		Map<Integer, TeacherPortfolioStatus> maplstStatus= new HashMap<Integer, TeacherPortfolioStatus>(); 
		
		List<TeacherPortfolioStatus> lstStatus= null;
		
		try 
		{
			if(teacherList.size()>0){
				Criterion criterion1 = Restrictions.in("teacherId",teacherList);
				lstStatus = findByCriteria(criterion1);
				
				if(lstStatus!=null && lstStatus.size()>0)
				{
					for(TeacherDetail td:teacherList)
					{
						for(TeacherPortfolioStatus tpsf:lstStatus)
						{
							if(td.getTeacherId().equals(tpsf.getTeacherId().getTeacherId()))
							{
								maplstStatus.put(td.getTeacherId(), tpsf);
							}
						}
					}
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(maplstStatus==null || maplstStatus.size()==0)
			return null;
		else
			return (HashMap<Integer, TeacherPortfolioStatus>) maplstStatus;
	}
	
	// Get portfolio status by teacher List
	@Transactional(readOnly=false)
	public Map<Integer, TeacherPortfolioStatus> findPortfolioStatusByTeacherlists(List<TeacherDetail> teacherList)
	{
		Map<Integer, TeacherPortfolioStatus> maplstStatus= new HashMap<Integer, TeacherPortfolioStatus>(); 
		List<TeacherPortfolioStatus> lstStatus= new ArrayList<TeacherPortfolioStatus>();
		try 
		{
			if(teacherList.size()>0){
				Criterion criterion1 = Restrictions.in("teacherId",teacherList);
				lstStatus = findByCriteria(criterion1);
				
				if(lstStatus!=null && lstStatus.size()>0)
				{
					for(TeacherPortfolioStatus tpsf:lstStatus)
					{
						maplstStatus.put(tpsf.getTeacherId().getTeacherId(), tpsf);
					}
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
			return  maplstStatus;
	}
	
 
	@Transactional(readOnly=true)
	public TeacherPortfolioStatus findPortfolioStatusByTeacherd_Opp(TeacherDetail teacherDetail)
	{
		TeacherPortfolioStatus teacherPortfolioStatus=new TeacherPortfolioStatus();
		try {
			Session session = getSession();
			if(teacherDetail!=null)
			teacherPortfolioStatus = (TeacherPortfolioStatus)session.createCriteria(TeacherPortfolioStatus.class).add(Restrictions.eq("teacherId", teacherDetail)).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			 
		}
        return teacherPortfolioStatus;
	}
	
}
