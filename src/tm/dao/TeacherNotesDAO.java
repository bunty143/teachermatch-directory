package tm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.MessageToTeacher;
import tm.bean.TeacherDetail;
import tm.bean.TeacherNotes;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherNotesDAO extends GenericHibernateDAO<TeacherNotes, Integer> 
{
	public TeacherNotesDAO() {
		super(TeacherNotes.class);
	}
	
	@Transactional(readOnly=true)
	public List<TeacherNotes> findByTeacher(TeacherDetail teacherDetail)
	{
	         List <TeacherNotes> lstTeacherNotes = null;
			 Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			 lstTeacherNotes = findByCriteria(criterion);
			 return lstTeacherNotes;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherNotes> findByTeacher(TeacherDetail teacherDetail, Order order,int startPos,int limit)
	{
		List<TeacherNotes> lstTeacherNotes = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", "A");
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);

			
			if(order!=null && order.toString().contains("firstName")){				
				criteria.createCriteria("noteCreatedByUser").addOrder(order);
			}
			else if(order != null){
				criteria.addOrder(order);
			}
			
			lstTeacherNotes = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherNotes;
	}
	
	@Transactional(readOnly=true)
	public int getRowcountByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherNotes> lstTeacherNotes = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", "A");
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);
			
			lstTeacherNotes = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherNotes.size();
	}
	
	
	
	
	
	
	@Transactional(readOnly=true)
	public List<TeacherNotes> findByTeacherAndDistrict(TeacherDetail teacherDetail, DistrictMaster districtMaster, Order order,int startPos,int limit)
	{
		List<TeacherNotes> lstTeacherNotes = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", "A");
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);

			Criteria cc = criteria.createCriteria("noteCreatedByUser");			
			cc.add(Restrictions.eq("districtId", districtMaster));
			
			if(order!=null && order.toString().contains("firstName")){				
				cc.addOrder(order);
			}
			else if(order != null){
				criteria.addOrder(order);
			}
			lstTeacherNotes = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherNotes;
	}
	
	@Transactional(readOnly=true)
	public int getRowcountByTeacherAndDistrict(TeacherDetail teacherDetail, DistrictMaster districtMaster)
	{
		List<TeacherNotes> lstTeacherNotes = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", "A");
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);
			
			criteria.createCriteria("noteCreatedByUser")			
			.add(Restrictions.eq("districtId", districtMaster));			
			lstTeacherNotes = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherNotes.size();
	}
	
	
	
	@Transactional(readOnly=true)
	public List<TeacherNotes> findByTeacherAndDistrict1(TeacherDetail teacherDetail, DistrictMaster districtMaster, Order order,int startPos,int limit)
	{
		List<TeacherNotes> lstTeacherNotes = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", "A");
			Criterion criterion3 = Restrictions.eq("districtMaster", districtMaster);
			
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);

			
			lstTeacherNotes = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherNotes;
	}
	
	@Transactional(readOnly=true)
	public int getRowcountByTeacherAndDistrict1(TeacherDetail teacherDetail, DistrictMaster districtMaster)
	{
		List<TeacherNotes> lstTeacherNotes = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", "A");	
			Criterion criterion3 = Restrictions.eq("districtMaster", districtMaster);
			lstTeacherNotes = findByCriteria(criterion1,criterion2,criterion3);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherNotes.size();
	}
	
	
	
	@Transactional(readOnly=true)
	public List<TeacherNotes> findByTeacherAndSchool(TeacherDetail teacherDetail, SchoolMaster schoolMaster, Order order,int startPos,int limit)
	{
		List<TeacherNotes> lstTeacherNotes = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", "A");
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);

			
			Criteria cc = criteria.createCriteria("noteCreatedByUser");			
			cc.add(Restrictions.eq("schoolId", schoolMaster));			
			
			if(order!=null && order.toString().contains("firstName")){				
				cc.addOrder(order);
			}
			else if(order != null){
				criteria.addOrder(order);
			}
			lstTeacherNotes = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherNotes;
	}
	
	@Transactional(readOnly=true)
	public int getRowcountByTeacherAndSchool(TeacherDetail teacherDetail, SchoolMaster schoolMaster)
	{
		List<TeacherNotes> lstTeacherNotes = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", "A");
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);
			criteria.createCriteria("noteCreatedByUser")			
			.add(Restrictions.eq("schoolId", schoolMaster));			
			lstTeacherNotes = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherNotes.size();
	}
	@Transactional(readOnly=true)
	public List<TeacherNotes> findByTeacherNotes(List<TeacherDetail> teacherDetailList)
	{
	         List <TeacherNotes> lstTeacherNotes = null;
			 Criterion criterion = Restrictions.in("teacherDetail",teacherDetailList);
			 lstTeacherNotes = findByCriteria(criterion);
			 return lstTeacherNotes;
	}
	@Transactional(readOnly=true)
	public List<TeacherNotes> findByTeacherAndDistrict(TeacherDetail teacherDetail, DistrictMaster districtMaster)
	{
		List<TeacherNotes> lstTeacherNotes = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", "A");
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);

			Criteria cc = criteria.createCriteria("noteCreatedByUser");			
			cc.add(Restrictions.eq("districtId", districtMaster));
			lstTeacherNotes = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherNotes;
	}
	@Transactional(readOnly=true)
	public List<TeacherNotes> findByTeacherAndSchool(TeacherDetail teacherDetail, SchoolMaster schoolMaster)
	{
		List<TeacherNotes> lstTeacherNotes = null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", "A");
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);
			
			Criteria cc = criteria.createCriteria("noteCreatedByUser");			
			cc.add(Restrictions.eq("schoolId", schoolMaster));			
			lstTeacherNotes = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherNotes;
	}
	@Transactional(readOnly=true)
	public List<TeacherNotes> findByTeacherForCommunication(TeacherDetail teacherDetail,UserMaster userMaster)
	{
		List<TeacherNotes> lstTeacherNotes = new ArrayList<TeacherNotes>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterionSttuas = Restrictions.eq("status", "A");
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterionSttuas);
			if(userMaster.getEntityType()!=1){
				if(userMaster.getDistrictId().getCommunicationsAccess()==0){
					if(userMaster.getEntityType()!=1){
						Criteria cc = criteria.createCriteria("noteCreatedByUser");	
						cc.add(Restrictions.eq("districtId",userMaster.getDistrictId()));
					}
				}else if(userMaster.getDistrictId().getCommunicationsAccess()==1){
					if(userMaster.getEntityType()==2){
						Criteria cc = criteria.createCriteria("noteCreatedByUser");	
						cc.add(Restrictions.eq("districtId",userMaster.getDistrictId()));
					}if(userMaster.getEntityType()==3){
						Criteria cc = criteria.createCriteria("noteCreatedByUser");	
						Criterion criterion2 = Restrictions.eq("districtId",userMaster.getDistrictId());
						Criterion criterion3 = Restrictions.eq("entityType",2);
						Criterion criterion4 = Restrictions.and(criterion2,criterion3);
						Criterion criterion5 = Restrictions.eq("schoolId",userMaster.getSchoolId());
						Criterion criterion6 = Restrictions.or(criterion4,criterion5);
						cc.add(criterion6);
					}
				}else if(userMaster.getDistrictId().getCommunicationsAccess()==2){
					if(userMaster.getEntityType()==2){
						Criteria cc = criteria.createCriteria("noteCreatedByUser");	
						cc.add(Restrictions.eq("districtId",userMaster.getDistrictId()));
					}if(userMaster.getEntityType()==3){
						Criteria cc = criteria.createCriteria("noteCreatedByUser");	
						cc.add(Restrictions.eq("schoolId",userMaster.getSchoolId()));
					}
				}
			}
			lstTeacherNotes = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherNotes;
	}
	
	
	@Transactional(readOnly=false)
	public List<TeacherNotes> findNotesByRoleId(TeacherDetail teacherDetail)
	{
		List<TeacherNotes> listTeacherNotes = null;
		try 
		{

			List<RoleMaster> roleMasters=new ArrayList<RoleMaster>();

			RoleMaster roleMaster=new RoleMaster();
			roleMaster.setRoleId(7);
			roleMasters.add(roleMaster);
			roleMaster=new RoleMaster();
			roleMaster.setRoleId(8);
			roleMasters.add(roleMaster);

			Session session 	 = getSession();
			Criteria criteria 	 = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			criteria.createCriteria("noteCreatedByUser").add(Restrictions.not(Restrictions.in("roleId", roleMasters)));
			listTeacherNotes = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listTeacherNotes;
	}
	
	
	@Transactional(readOnly=false)
	public List<TeacherNotes> findNotesByRoleIdNew(TeacherDetail teacherDetail,List<UserMaster> userMasters)
	{
		List<TeacherNotes> listTeacherNotes = null;
		try 
		{

			List<RoleMaster> roleMasters=new ArrayList<RoleMaster>();

			RoleMaster roleMaster=new RoleMaster();
			roleMaster.setRoleId(7);
			roleMasters.add(roleMaster);
			roleMaster=new RoleMaster();
			roleMaster.setRoleId(8);
			roleMasters.add(roleMaster);

			Session session 	 = getSession();
			Criteria criteria 	 = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.in("noteCreatedByUser", userMasters);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.createCriteria("noteCreatedByUser").add(Restrictions.not(Restrictions.in("roleId", roleMasters)));
			listTeacherNotes = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listTeacherNotes;
	}
}
