package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictSpecificCustomQuestion;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class DistrictSpecificCustomQuestionDAO extends GenericHibernateDAO<DistrictSpecificCustomQuestion, Long>{
	public DistrictSpecificCustomQuestionDAO(){
		super(DistrictSpecificCustomQuestion.class);
	}
	
	@Transactional
	public List<DistrictSpecificCustomQuestion> getdistrictCustomQuestion(DistrictMaster districtMaster)
	{
		List<DistrictSpecificCustomQuestion> districtSpecificCustomQuestions = new ArrayList<DistrictSpecificCustomQuestion>();
		Criterion criterion=Restrictions.eq("districtId", districtMaster);
		districtSpecificCustomQuestions=findByCriteria(criterion);
		try {
			return districtSpecificCustomQuestions;
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return districtSpecificCustomQuestions;
	}

}
