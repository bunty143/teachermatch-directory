package tm.dao;

import tm.bean.TeacherSectionDetail;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherSectionDetailDAO extends GenericHibernateDAO<TeacherSectionDetail, Integer>
{
	public TeacherSectionDetailDAO() {
		super(TeacherSectionDetail.class);
	}

}
