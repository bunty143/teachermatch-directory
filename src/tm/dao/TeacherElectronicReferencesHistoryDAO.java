package tm.dao;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherElectronicReferencesHistory;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.ReferenceCheckFitScore;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherElectronicReferencesHistoryDAO extends GenericHibernateDAO<TeacherElectronicReferencesHistory, Integer> 
{
	public TeacherElectronicReferencesHistoryDAO() 
	{
		super(TeacherElectronicReferencesHistory.class);
	}
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferencesHistory> findAllHistoryByJobAndDistId(DistrictMaster districtMaster, JobOrder jobOrder)
	{
		List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistory= new ArrayList<TeacherElectronicReferencesHistory>();
		try
		{
			
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 	= 	Restrictions.eq("jobOrder",jobOrder);
			criteria.add(criterion1);
			criteria.add(criterion2);
			teacherElectronicReferencesHistory = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherElectronicReferencesHistory;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferencesHistory> findAllHistoryByDistId(DistrictMaster districtMaster)
	{
		List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistory= new ArrayList<TeacherElectronicReferencesHistory>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			criteria.add(criterion1);
			teacherElectronicReferencesHistory = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherElectronicReferencesHistory;
	}
	// public List<TeacherElectronicReferencesHistory> findAllHistoryByTeacherId(DistrictMaster districtMaster, JobOrder jobOrder, TeacherDetail teacherDetail)
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferencesHistory> findAllHistoryByTeacherId(DistrictMaster districtMaster, TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistory= new ArrayList<TeacherElectronicReferencesHistory>();
		try
		{
			
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			//Criterion criterion2 	= 	Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion3 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			//criteria.add(criterion2);
			criteria.add(criterion3);
			teacherElectronicReferencesHistory = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherElectronicReferencesHistory;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferencesHistory> findAllHistoryByRefId(TeacherElectronicReferences teacherElectronicReferences, DistrictMaster districtMaster, JobOrder jobOrder, TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistory= new ArrayList<TeacherElectronicReferencesHistory>();
		try
		{
			
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("teacherElectronicReferences",teacherElectronicReferences);
			Criterion criterion2 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion3 	= 	Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion4 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			teacherElectronicReferencesHistory = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherElectronicReferencesHistory;
	}
	
	
	// update @ Anurag 19 Aug
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferencesHistory> findAllHistoryByRefIdAndDist(TeacherElectronicReferences teacherElectronicReferences,HeadQuarterMaster headQuarterMaster, DistrictMaster districtMaster, TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistory= new ArrayList<TeacherElectronicReferencesHistory>();
		try
		{
			
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("teacherElectronicReferences",teacherElectronicReferences);
			Criterion criterion2 	= null;
			if(districtMaster!=null)
				criterion2 =	Restrictions.eq("districtMaster",districtMaster);
			else if(headQuarterMaster!=null)
				criterion2 =  Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion3 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			if(criterion2!=null)
			criteria.add(criterion2);
			criteria.add(criterion3);
			teacherElectronicReferencesHistory = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherElectronicReferencesHistory;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferencesHistory> findAllHistoryByRefIdContactStatus(TeacherElectronicReferences teacherElectronicReferences, DistrictMaster districtMaster, JobOrder jobOrder, TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistory= new ArrayList<TeacherElectronicReferencesHistory>();
		try
		{
			
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("teacherElectronicReferences",teacherElectronicReferences);
			Criterion criterion2 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion3 	= 	Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion4 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion5 	= 	Restrictions.eq("contactStatus",1);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			criteria.add(criterion5);
			teacherElectronicReferencesHistory = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherElectronicReferencesHistory;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferencesHistory> findAllHistoryByRefIdAndDistIdContactStatus(TeacherElectronicReferences teacherElectronicReferences,HeadQuarterMaster headQuarterMaster, DistrictMaster districtMaster, TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistory= new ArrayList<TeacherElectronicReferencesHistory>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("teacherElectronicReferences",teacherElectronicReferences);
			Criterion criterion2 	= null;
			if(districtMaster!=null)
			 	criterion2 = Restrictions.eq("districtMaster",districtMaster);
			else if(headQuarterMaster!=null)
			 	criterion2 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion3 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion4 	= 	Restrictions.eq("contactStatus",1);
			criteria.add(criterion1);
			if(criterion2!=null)
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			teacherElectronicReferencesHistory = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherElectronicReferencesHistory;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferencesHistory> countContactStatusRefChk(HeadQuarterMaster headQuarterMaster , DistrictMaster districtMaster, TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferencesHistory> countContactStatusRefChkCount= new ArrayList<TeacherElectronicReferencesHistory>();
		try
		{

			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			
			Criterion criterion1 	= null;
			if(districtMaster!=null)
			 criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			else  if(headQuarterMaster!=null)
				criterion1 = Restrictions.eq("headQuarterMaster", headQuarterMaster);
			Criterion criterion2 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3	=	Restrictions.eq("contactStatus", 1);
			if(criterion1 !=null)
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			countContactStatusRefChkCount = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return countContactStatusRefChkCount;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferencesHistory> countContactStatusRefChkByDistrict(DistrictMaster districtMaster, TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferencesHistory> countContactStatusRefChkCount= new ArrayList<TeacherElectronicReferencesHistory>();
		try
		{

			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3	=	Restrictions.eq("contactStatus", 1);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			countContactStatusRefChkCount = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return countContactStatusRefChkCount;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferencesHistory> countReplyStatusRefChk(HeadQuarterMaster headQuarterMaster , DistrictMaster districtMaster, TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferencesHistory> countContactStatusRefChkCount= new ArrayList<TeacherElectronicReferencesHistory>();
		try
		{

			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 = null;
			if(districtMaster!=null)
			 criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			else if(headQuarterMaster!=null)
				criterion1 	= 	Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion2 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 	= 	Restrictions.eq("replyStatus",1);
			
			if(criterion1!=null)
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			countContactStatusRefChkCount = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return countContactStatusRefChkCount;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferencesHistory> getRecordByDistrictIdAndTeacherList(DistrictMaster districtMaster, List<TeacherDetail> teacherDetail)
	{
		List<TeacherElectronicReferencesHistory> countContactStatusRefChkCount= new ArrayList<TeacherElectronicReferencesHistory>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 	= 	Restrictions.in("teacherDetail",teacherDetail);
			Criterion criterion3	=	Restrictions.eq("contactStatus", 1);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			countContactStatusRefChkCount = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return countContactStatusRefChkCount;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferencesHistory> getRecordsByDistrictAndTeacherList(DistrictMaster districtMaster,List<TeacherDetail> teacherDetails){
		
		List<TeacherElectronicReferencesHistory> allRecord = new ArrayList<TeacherElectronicReferencesHistory>();
		try{
			Criterion criterion1	=	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2	=	Restrictions.in("teacherDetail", teacherDetails);
			allRecord				=	findByCriteria(criterion1,criterion2);
			
		} catch (Exception exception){
			exception.printStackTrace();
		}
		return allRecord;
	}
	
	@Transactional(readOnly=false)
	public List getRefHistory(List<TeacherDetail> teacherDetailsObj)
	{
		System.out.println("---------------- teacherDetailsObj size "+teacherDetailsObj.size());
		List lst = null;
		if(teacherDetailsObj.size() > 0){
			try 
			{
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetailsObj);
				Session session = getSession();
				lst = session.createCriteria(getPersistentClass()) 
				.add(criterion1) 
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.count("teacherDetail")) 
				).list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lst;
	}
	
	@Transactional(readOnly=false)
	public List getRefHistoryContactStatus(List<TeacherDetail> teacherDetailsObj)
	{
		System.out.println("---------------- teacherDetailsObj size "+teacherDetailsObj.size());
		List lst = null;
		if(teacherDetailsObj.size() > 0){
			try 
			{
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetailsObj);
				Criterion criterion2 = Restrictions.eq("contactStatus",1);
				Session session = getSession();
				lst = session.createCriteria(getPersistentClass()) 
				.add(criterion1)
				.add(criterion2)
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.count("teacherDetail")) 
				).list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lst;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int findTotalReferenceContactHistory(HeadQuarterMaster headQuarterMaster , DistrictMaster districtMaster, TeacherDetail teacherDetail)
	{
		int iReturnValue=0;
		if(districtMaster!=null){
			try{
				Session session = getSession();
				String sql ="";
				
				if(districtMaster!=null)				
				 sql ="SELECT COUNT(elerefHAutoId) AS teacherElectHis " +
						    "FROM teacherelectronicreferencehistory tElecHis " +
						    "WHERE tElecHis.districtId = "+districtMaster.getDistrictId()+" " +
						    "AND tElecHis.teacherId = "+teacherDetail.getTeacherId()+" " +
						    "AND tElecHis.contactStatus = 1";
				
				else if(headQuarterMaster!=null)
					sql ="SELECT COUNT(elerefHAutoId) AS teacherElectHis " +
				    "FROM teacherelectronicreferencehistory tElecHis " +
				    "WHERE tElecHis.headQuarterId = "+headQuarterMaster.getHeadQuarterId()+" " +
				    "AND tElecHis.teacherId = "+teacherDetail.getTeacherId()+" " +
				    "AND tElecHis.contactStatus = 1";
				
				Query query = session.createSQLQuery(sql);
				List<BigInteger> rowCount = query.list();
				if(rowCount!=null && rowCount.size()==1)
					iReturnValue=rowCount.get(0).intValue();			
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		return iReturnValue;
		
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int findTotalReferenceReplyHistory(HeadQuarterMaster headQuarterMaster , DistrictMaster districtMaster, TeacherDetail teacherDetail)
	{
		int iReturnValue=0;
		if(districtMaster!=null){
			try{
				Session session = getSession();
				
				String sql ="";				
				if(districtMaster!=null)				
				 sql ="SELECT COUNT(elerefHAutoId) AS teacherElectHis " +
						    "FROM teacherelectronicreferencehistory tElecHis " +
						    "WHERE tElecHis.districtId = "+districtMaster.getDistrictId()+" " +
						    "AND tElecHis.teacherId = "+teacherDetail.getTeacherId()+" " +
						    "AND tElecHis.replyStatus = 1";
				else if(headQuarterMaster!=null)
					 sql ="SELECT COUNT(elerefHAutoId) AS teacherElectHis " +
					    "FROM teacherelectronicreferencehistory tElecHis " +
					    "WHERE tElecHis.headQuarterId = "+headQuarterMaster.getHeadQuarterId()+" " +
					    "AND tElecHis.teacherId = "+teacherDetail.getTeacherId()+" " +
					    "AND tElecHis.replyStatus = 1";
			
				
				
				Query query = session.createSQLQuery(sql);
				List<BigInteger> rowCount = query.list();
				if(rowCount!=null && rowCount.size()==1)
					iReturnValue=rowCount.get(0).intValue();			
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		return iReturnValue;
		
	}
	
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int findAllReferenceHistory(HeadQuarterMaster headQuarterMaster , DistrictMaster districtMaster, TeacherDetail teacherDetail)
	{
		int iReturnValue=0;
		if(districtMaster!=null){
			try{
				Session session = getSession();
				String sql ="";				
				if(districtMaster!=null)				
				 sql ="SELECT COUNT(elerefHAutoId) AS teacherElectHis " +
						    "FROM teacherelectronicreferencehistory tElecHis " +
						    "WHERE tElecHis.districtId = "+districtMaster.getDistrictId()+" " +
						    "AND tElecHis.teacherId = "+teacherDetail.getTeacherId()+" ";
				else if(headQuarterMaster!=null)
					 sql ="SELECT COUNT(elerefHAutoId) AS teacherElectHis " +
					    "FROM teacherelectronicreferencehistory tElecHis " +
					    "WHERE tElecHis.headQuarterId = "+headQuarterMaster.getHeadQuarterId()+" " +
					    "AND tElecHis.teacherId = "+teacherDetail.getTeacherId()+" ";
				
				Query query = session.createSQLQuery(sql);
				List<BigInteger> rowCount = query.list();
				if(rowCount!=null && rowCount.size()==1)
					iReturnValue=rowCount.get(0).intValue();			
			} catch(Exception exception){
				exception.printStackTrace();
			}
		}
		return iReturnValue;
		
	}
	
	@Transactional(readOnly=false)
	public List getRefHistoryByDistrict(List<TeacherDetail> teacherDetailsObj,HeadQuarterMaster headQuarterMaster, DistrictMaster districtMaster)
	{
		System.out.println("---------------- teacherDetailsObj size "+teacherDetailsObj.size());
		List lst = null;
		if(teacherDetailsObj.size() > 0){
			try 
			{
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetailsObj);
				Criterion criterion2 =null;
				if(districtMaster!=null)
				 criterion2 = Restrictions.eq("districtMaster",districtMaster);
				else if(headQuarterMaster!=null)
					 criterion2 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
				Session session = getSession();
				lst = session.createCriteria(getPersistentClass()) 
				.add(criterion1)
				.add(criterion2)
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.count("teacherDetail")) 
				).list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lst;
	}
	
	@Transactional(readOnly=false)
	public List getRefHistoryContactStatusByDistrict(List<TeacherDetail> teacherDetailsObj, HeadQuarterMaster headQuarterMaster , DistrictMaster districtMaster)
	{
		System.out.println("---------------- teacherDetailsObj size "+teacherDetailsObj.size());
		List lst = null;
		if(teacherDetailsObj.size() > 0){
			try 
			{
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetailsObj);
				Criterion criterion2 =null;
				if(districtMaster!=null)
				 criterion2 = Restrictions.eq("districtMaster",districtMaster);
				else if(headQuarterMaster!=null)
					 criterion2 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
				
				Criterion criterion3 = Restrictions.eq("contactStatus",1);
				Session session = getSession();
				lst = session.createCriteria(getPersistentClass()) 
				.add(criterion1)
				.add(criterion2)
				.add(criterion3)
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherDetail"))
						.add(Projections.count("teacherDetail")) 
				).list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lst;
	}
	@Transactional(readOnly=false)
	public List<TeacherElectronicReferencesHistory> countContactStatusRefChkByHBD(JobOrder jobOrder,TeacherDetail teacherDetail)
	{
		List<TeacherElectronicReferencesHistory> countContactStatusRefChkCount= new ArrayList<TeacherElectronicReferencesHistory>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			criteria.add(Restrictions.eq("teacherDetail",teacherDetail));
			criteria.add(Restrictions.eq("contactStatus",1));
			countContactStatusRefChkCount = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return countContactStatusRefChkCount;
	}
	
}
