package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.SapCandidateDetailsHistory;
import tm.bean.TeacherDetail;
import tm.dao.generic.GenericHibernateDAO;

public class SapCandidateDetailsHistoryDAO extends GenericHibernateDAO<SapCandidateDetailsHistory, Integer> 
{
	public SapCandidateDetailsHistoryDAO() {
		super(SapCandidateDetailsHistory.class);
	}
	
	@Transactional(readOnly=false)
	public List<SapCandidateDetailsHistory> getSingleRecordByTeacherDetail(TeacherDetail teacherDetail)
	{
		List<SapCandidateDetailsHistory> sapCandidateDetailsHistoryList = new ArrayList<SapCandidateDetailsHistory>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			criteria.add(criterion1);
			criteria.addOrder(Order.desc("sapCandidateId"));
			criteria.setFirstResult(0);
			criteria.setMaxResults(1);
			sapCandidateDetailsHistoryList = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return sapCandidateDetailsHistoryList;
	}
	
	@Transactional(readOnly=false)
	public List<SapCandidateDetailsHistory> getRecordByTeacherDetailList(List<TeacherDetail> teacherDetailList)
	{
		List<SapCandidateDetailsHistory> sapCandidateDetailsHistoryList = new ArrayList<SapCandidateDetailsHistory>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.in("teacherDetail",teacherDetailList);
			criteria.add(criterion1);
			criteria.addOrder(Order.desc("sapCandidateId"));
			sapCandidateDetailsHistoryList = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return sapCandidateDetailsHistoryList;
	}

}
