package tm.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EligibilityVerificationHistroy;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.master.DistrictMaster;
import tm.bean.master.EligibilityMaster;
import tm.dao.generic.GenericHibernateDAO;

public class EligibilityVerificationHistroyDAO extends GenericHibernateDAO<EligibilityVerificationHistroy, Integer> 
{
	public EligibilityVerificationHistroyDAO() 
	{
		super(EligibilityVerificationHistroy.class);
	}
	
	
	@Transactional(readOnly=false)
	public List<EligibilityVerificationHistroy> findAllHistory(DistrictMaster districtMaster,JobForTeacher jobForTeacher)
	{
		List<EligibilityVerificationHistroy> eligibilityVerificationHistroy= new ArrayList<EligibilityVerificationHistroy>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtId",districtMaster);
			
			 	criteria.add(criterion1);
				Criteria criteria1= criteria.createCriteria("teacherId").add(Restrictions.eq("jobForTeacher",jobForTeacher));
			 
			eligibilityVerificationHistroy = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityVerificationHistroy;
	}
	
	@Transactional(readOnly=false)
	public List<EligibilityVerificationHistroy> findAllHistory1(DistrictMaster districtMaster)
	{
		List<EligibilityVerificationHistroy> eligibilityVerificationHistroy= new ArrayList<EligibilityVerificationHistroy>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			criteria.add(criterion1);

			eligibilityVerificationHistroy = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityVerificationHistroy;
	}

	@Transactional(readOnly=false)
	public List<EligibilityVerificationHistroy> findAllHistoryFp(DistrictMaster districtMaster,TeacherDetail teacherDetail,EligibilityMaster eligibilityMaster)
	{
		List<EligibilityVerificationHistroy> eligibilityVerificationHistroy= new ArrayList<EligibilityVerificationHistroy>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 	= 	Restrictions.eq("eligibilityMaster",eligibilityMaster);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.addOrder(Order.desc("eligibilityVerificationId"));

			eligibilityVerificationHistroy = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityVerificationHistroy;
	}
	
	
	@Transactional(readOnly=false)
	public List<EligibilityVerificationHistroy> findAllHistoryByJobId(DistrictMaster districtMaster,TeacherDetail teacherDetail,JobOrder jobOrder,EligibilityMaster eligibilityMaster)
	{
		List<EligibilityVerificationHistroy> eligibilityVerificationHistroy= new ArrayList<EligibilityVerificationHistroy>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 	= 	Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion4 	= 	Restrictions.eq("eligibilityMaster",eligibilityMaster);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			criteria.addOrder(Order.desc("eligibilityVerificationId"));
			criteria.setFirstResult(0);
			criteria.setMaxResults(1);
			eligibilityVerificationHistroy = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityVerificationHistroy;
	}
	
	@Transactional(readOnly=false)
	public List<EligibilityVerificationHistroy> findAllHistoryByJobIdTrans(DistrictMaster districtMaster,TeacherDetail teacherDetail,EligibilityMaster eligibilityMaster)
	{
		List<EligibilityVerificationHistroy> eligibilityVerificationHistroy= new ArrayList<EligibilityVerificationHistroy>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion4 	= 	Restrictions.eq("eligibilityMaster",eligibilityMaster);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion4);
			criteria.addOrder(Order.desc("eligibilityVerificationId"));
			criteria.setFirstResult(0);
			criteria.setMaxResults(1);
			eligibilityVerificationHistroy = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityVerificationHistroy;
	}
	
	@Transactional(readOnly=true)
	public List<EligibilityVerificationHistroy> findAllHistoryByJobId(List<TeacherDetail> teacherDetails)
	{
		List <EligibilityVerificationHistroy> eligibilityVerificationHistroyList = new ArrayList<EligibilityVerificationHistroy>();
		try 
		{	 if(teacherDetails.size()>0){        
			 	Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
			 	eligibilityVerificationHistroyList = findByCriteria(criterion1);
			 }
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return eligibilityVerificationHistroyList;
	}
	@Transactional(readOnly=false)
	public List<EligibilityVerificationHistroy> findAllHistoryByJobIdAndTrans(JobOrder jobOrder,TeacherDetail teacherDetail,EligibilityMaster eligibilityMaster)
	{
		List<EligibilityVerificationHistroy> eligibilityVerificationHistroy= new ArrayList<EligibilityVerificationHistroy>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
			Criterion criterion2 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 	= 	Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion4 	= 	Restrictions.eq("eligibilityMaster",eligibilityMaster);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion4);
			criteria.add(criterion3);
			criteria.addOrder(Order.desc("eligibilityVerificationId"));
			criteria.setFirstResult(0);
			criteria.setMaxResults(1);
			eligibilityVerificationHistroy = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityVerificationHistroy;
	}
	
	@Transactional(readOnly=false)
	public List<EligibilityVerificationHistroy> findAllHistoryByTeacher(DistrictMaster districtMaster,List<TeacherDetail> teacherDetails)
	{
		List<EligibilityVerificationHistroy> eligibilityVerificationHistroy= new ArrayList<EligibilityVerificationHistroy>();
		try
		{
			if(teacherDetails.size()>0){
				Session session 		= 	getSession();
				Criteria criteria 		= 	session.createCriteria(getPersistentClass());
				Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion2 	= 	Restrictions.in("teacherDetail",teacherDetails);
				criteria.add(criterion1);
				criteria.add(criterion2);
				eligibilityVerificationHistroy = criteria.list();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityVerificationHistroy;
	}
	
	
	@Transactional(readOnly=false)
	public List<EligibilityVerificationHistroy> findDrugTestStatus(DistrictMaster districtMaster,TeacherDetail teacherDetail,JobOrder jobOrder,EligibilityMaster eligibilityMaster)
	{
		List<EligibilityVerificationHistroy> eligibilityVerificationHistroy= new ArrayList<EligibilityVerificationHistroy>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 	= 	Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 	= 	Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion4 	= 	Restrictions.eq("eligibilityMaster",eligibilityMaster);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			criteria.addOrder(Order.desc("eligibilityVerificationId"));
			criteria.setFirstResult(0);
			criteria.setMaxResults(1);
			eligibilityVerificationHistroy = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityVerificationHistroy;
	}
	
	@Transactional(readOnly=false)
	public List<EligibilityVerificationHistroy> findDrugTestStatusForTeacherList(DistrictMaster districtMaster,List<TeacherDetail> teacherDetails,JobOrder jobOrder,EligibilityMaster eligibilityMaster)
	{
		List<EligibilityVerificationHistroy> eligibilityVerificationHistroy= new ArrayList<EligibilityVerificationHistroy>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 	= 	Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion3 	= 	Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion4 	= 	Restrictions.eq("eligibilityMaster",eligibilityMaster);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			criteria.addOrder(Order.desc("eligibilityVerificationId"));
			criteria.setFirstResult(0);
			criteria.setMaxResults(1);
			eligibilityVerificationHistroy = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityVerificationHistroy;
	}
	
	@Transactional(readOnly=false)
	public List<EligibilityVerificationHistroy> findDrugTestStatusForTeacherListForPool(DistrictMaster districtMaster,List<TeacherDetail> teacherDetails,EligibilityMaster eligibilityMaster)
	{
		List<EligibilityVerificationHistroy> eligibilityVerificationHistroy= new ArrayList<EligibilityVerificationHistroy>();
		try
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criterion criterion1 	= 	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 	= 	Restrictions.in("teacherDetail",teacherDetails);
			Criterion criterion3 	= 	Restrictions.eq("eligibilityMaster",eligibilityMaster);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.addOrder(Order.desc("eligibilityVerificationId"));
			criteria.setFirstResult(0);
			criteria.setMaxResults(1);
			eligibilityVerificationHistroy = criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return eligibilityVerificationHistroy;
	}
	
	@Transactional(readOnly=false)
	public List<String[]> findByEVHistory(TeacherStatusHistoryForJob teacherStatusHistoryForJob, Integer eligibilityId)
	{
		List<String[]> eligibilityVerificationHistroyList= new ArrayList<String[]>();
		try
		{
			Session session = getSession();				
			Criteria criteria = session.createCriteria(EligibilityVerificationHistroy.class);
			criteria.createAlias("jobOrder", "jo").createAlias("teacherDetail", "td").createAlias("districtMaster", "dm").createAlias("eligibilityMaster", "em").createAlias("userMaster", "um",Criteria.LEFT_JOIN).createAlias("eligibilityStatusMaster", "esm")//.setFetchMode("sapCandidateDetails", FetchMode.JOIN).createAlias("sapCandidateDetails", "scd",CriteriaSpecification.LEFT_JOIN)
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("employeeId"), "employeeId" )
		        .add( Projections.property("salary"), "salary" )
		        .add( Projections.property("grade"), "grade" )
		        .add( Projections.property("step"), "step" )
		        .add( Projections.property("createdDateTime"), "createdDateTime" )
		        .add( Projections.property("um.firstName"), "firstName" )
		        .add( Projections.property("um.lastName"), "lastName" )
		        .add( Projections.property("esm.statusName"), "statusName" )
		        .add( Projections.property("notes"), "notes" )
		        .add( Projections.property("eligibilityVerificationId"), "eligibilityVerificationId" )
		        .add( Projections.property("startDate"), "startDate" )
		        .add( Projections.property("endDate"), "endDate" )
		);
			criteria.add(Restrictions.eq("districtMaster", teacherStatusHistoryForJob.getJobOrder().getDistrictMaster()));
			criteria.add(Restrictions.eq("jobOrder", teacherStatusHistoryForJob.getJobOrder()));
			criteria.add(Restrictions.eq("teacherDetail", teacherStatusHistoryForJob.getTeacherDetail()));
			criteria.add(Restrictions.eq("em.eligibilityId", eligibilityId));
			long a = System.currentTimeMillis();
			eligibilityVerificationHistroyList=criteria.list();
			long b = System.currentTimeMillis();
			System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + eligibilityVerificationHistroyList.size() );
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	return eligibilityVerificationHistroyList;
	}
	
	@Transactional(readOnly=false)
	public List<EligibilityVerificationHistroy> findByLastEntryEVHistoryByJobOrderAndTeacherDetail(TeacherDetail td, JobOrder jobOrder)
	{
		List<EligibilityVerificationHistroy> evhList=new ArrayList<EligibilityVerificationHistroy>();
		try{
		Session hbSession=getSession();
		Criteria criteria=hbSession.createCriteria(EligibilityVerificationHistroy.class, "evh").createAlias("teacherDetail", "td").createAlias("jobOrder", "jo").createAlias("districtMaster", "dm").createAlias("eligibilityMaster", "em");
		criteria.add(Restrictions.eq("td.teacherId", td.getTeacherId()));
		criteria.add(Restrictions.eq("jo.jobId", jobOrder.getJobId()));
		criteria.add(Restrictions.eq("dm.districtId", jobOrder.getDistrictMaster().getDistrictId()));
		DetachedCriteria subQueryforNormScore = DetachedCriteria.forClass(EligibilityVerificationHistroy.class, "evh1")
	    .setProjection(Projections.projectionList()
	    		.add( Projections.max("evh1.eligibilityVerificationId"), "eligibilityVerificationId" ))
	    .add(Restrictions.eqProperty("evh1.teacherDetail.teacherId", "td.teacherId"))
		.add(Restrictions.eqProperty("evh1.jobOrder.jobId", "jo.jobId"))
		.add(Restrictions.eqProperty("evh1.districtMaster.districtId", "em.districtId"))
		.add(Restrictions.eqProperty("evh1.eligibilityMaster.eligibilityId", "em.eligibilityId"));
		criteria.add(Property.forName("evh.eligibilityVerificationId").eq(subQueryforNormScore));
		evhList=criteria.list();
		}catch(Exception e){e.printStackTrace();}
		return evhList;
	}
}

