package tm.dao;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolWiseCandidateStatus;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.master.StatusMasterDAO;
import tm.servlet.WorkThreadServlet;

public class SchoolWiseCandidateStatusDAO extends GenericHibernateDAO<SchoolWiseCandidateStatus, Integer>{

	public SchoolWiseCandidateStatusDAO() {
		super(SchoolWiseCandidateStatus.class);
	}
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Transactional(readOnly=true)
	public List<SchoolWiseCandidateStatus> findByTeacherStatusHistoryByUser(TeacherDetail teacherDetail,JobOrder jobOrder,UserMaster userMaster)
	{
        List <SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus =new ArrayList<SchoolWiseCandidateStatus>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Criterion criterion4 = Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
			Criterion criterion5 = Restrictions.eq("userMaster", userMaster);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			criteria.add(criterion5);
			lstSchoolWiseCandidateStatus = criteria.list();
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstSchoolWiseCandidateStatus;
	}
	@Transactional(readOnly=true)
	public List<SchoolWiseCandidateStatus> findByTeacherStatusHistory(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
        List <SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus =new ArrayList<SchoolWiseCandidateStatus>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Criterion criterion4 = Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			lstSchoolWiseCandidateStatus = criteria.list();
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstSchoolWiseCandidateStatus;
	}
	@Transactional(readOnly=true)
	public List<SchoolWiseCandidateStatus> findByTeacherStatusHistoryByUsers(List<TeacherDetail> teacherDetails,JobOrder jobOrder,UserMaster userMaster)
	{
        List <SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus =new ArrayList<SchoolWiseCandidateStatus>();
        try{
        	if(teacherDetails.size()>0){
	        	Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetails);
				Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.eq("status", "A");
				Criterion criterion4 = Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
				Criterion criterion5 = Restrictions.eq("userMaster", userMaster);
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.add(criterion5);
				lstSchoolWiseCandidateStatus = criteria.list();
        	}
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstSchoolWiseCandidateStatus;
	}
	
	@Transactional(readOnly=true)
	public List<SchoolWiseCandidateStatus> findByTeacherStatusListHistoryForUser(TeacherDetail teacherDetail,List<JobOrder> jobOrders,UserMaster userMaster)
	{
        List <SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus =new ArrayList<SchoolWiseCandidateStatus>();
        try{
        	if(jobOrders.size()>0){
	        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
				Criterion criterion2 = Restrictions.in("jobOrder", jobOrders);
				Criterion criterion3 = Restrictions.eq("status", "A");
				Criterion criterion5 = Restrictions.eq("userMaster", userMaster);
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion5);
				lstSchoolWiseCandidateStatus = criteria.list();
        	}
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstSchoolWiseCandidateStatus;
	}
	
	@Transactional(readOnly=true)
	public List<SchoolWiseCandidateStatus> findByTeacherStatusHistoryForSchool(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolMaster)
	{
        List <SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus =new ArrayList<SchoolWiseCandidateStatus>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Criterion criterion4 = Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
			Criterion criterion5 = Restrictions.eq("schoolId", schoolMaster.getSchoolId());
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			criteria.add(criterion5);
			lstSchoolWiseCandidateStatus = criteria.list();
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstSchoolWiseCandidateStatus;
	}
	
	@Transactional(readOnly=true)
	public List<SchoolWiseCandidateStatus> findByTeacherStatusListHistoryForSchool(List<TeacherDetail> teacherDetails,JobOrder jobOrder,SchoolMaster schoolMaster)
	{
        List <SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus =new ArrayList<SchoolWiseCandidateStatus>();
        try{
        	if(teacherDetails.size()>0){
	        	Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetails);
				Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.eq("status", "A");
				Criterion criterion4 = Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
				Criterion criterion5 = Restrictions.eq("schoolId", schoolMaster.getSchoolId());
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.add(criterion5);
				lstSchoolWiseCandidateStatus = criteria.list();
        	}
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstSchoolWiseCandidateStatus;
	}
	public List<Long> getSchool(List<SchoolMaster> schoolMasters){
		  List<Long> schoolList=new ArrayList<Long>();
		 if(schoolMasters.size()>0)
		        for (SchoolMaster school: schoolMasters) {
		        	schoolList.add(school.getSchoolId());
				}
		 return schoolList;
	}
	@Transactional(readOnly=true)
	public List<SchoolWiseCandidateStatus> findByTeacherStatusListHistoryForSchoolList(List<TeacherDetail> teacherDetails,JobOrder jobOrder,List<SchoolMaster> schoolMasters)
	{
        List <SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus =new ArrayList<SchoolWiseCandidateStatus>();
        try{
        	if(teacherDetails.size()>0 && schoolMasters.size()>0){
	        	Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetails);
				Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.eq("status", "A");
				Criterion criterion4 = Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
				Criterion criterion5 = Restrictions.in("schoolId", getSchool(schoolMasters));
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.add(criterion5);
				lstSchoolWiseCandidateStatus = criteria.list();
        	}
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstSchoolWiseCandidateStatus;
	}
	@Transactional(readOnly=true)
	public List<SchoolWiseCandidateStatus> findByTeacherStatusListHistoryForSchoolByJob(TeacherDetail teacherDetail,List<JobOrder> jobOrders,SchoolMaster schoolMaster)
	{
        List <SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus =new ArrayList<SchoolWiseCandidateStatus>();
        try{
        	if(jobOrders.size()>0){
	        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
				Criterion criterion2 = Restrictions.in("jobOrder", jobOrders);
				Criterion criterion3 = Restrictions.eq("status", "A");
				Criterion criterion5 = Restrictions.eq("schoolId", schoolMaster.getSchoolId());
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion5);
				lstSchoolWiseCandidateStatus = criteria.list();
        	}
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstSchoolWiseCandidateStatus;
	}
	
	@Transactional(readOnly=true)
	public List<SchoolWiseCandidateStatus> findByTeacherStatusListHistoryForSchoolList(TeacherDetail teacherDetail,List<JobOrder> jobOrders,List<SchoolMaster> schoolMasters)
	{
        List <SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus =new ArrayList<SchoolWiseCandidateStatus>();
        try{
        	if(jobOrders.size()>0 && schoolMasters.size()>0){
	        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
				Criterion criterion2 = Restrictions.in("jobOrder", jobOrders);
				Criterion criterion3 = Restrictions.eq("status", "A");
				Criterion criterion5 = Restrictions.in("schoolId", getSchool(schoolMasters));
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion5);
				lstSchoolWiseCandidateStatus = criteria.list();
        	}
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstSchoolWiseCandidateStatus;
	}
	@Transactional(readOnly=true)
	public List<JobForTeacher> findByDistrictRejectAndDecline(DistrictMaster districtMaster)
	{
        List <JobForTeacher> lstJobForTeacher =new ArrayList<JobForTeacher>();
        try{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster.getDistrictId());
			Criterion criterion3 = Restrictions.eq("status", "A");
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion3);
			criteria.setProjection(Projections.distinct(Projections.property("jobForTeacher")));
			lstJobForTeacher = criteria.list();
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=true)
	public List<SchoolWiseCandidateStatus> findByTeacherOrJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
        List <SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus =new ArrayList<SchoolWiseCandidateStatus>();
        try{
        	if(teacherDetail!=null){
	        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				if(jobOrder!=null){
					Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
					criteria.add(criterion2);
				}
				lstSchoolWiseCandidateStatus = criteria.list();
        	}
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstSchoolWiseCandidateStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SchoolWiseCandidateStatus> findRejectedTeacherForAnyJob(DistrictMaster districtMaster,List<TeacherDetail> teacherDetails)
	{
		List<SchoolWiseCandidateStatus> schoolWiseCandidateStatus= new ArrayList<SchoolWiseCandidateStatus>();
		
		StatusMaster statusMaster = WorkThreadServlet.statusIdMap.get(10);
		try
		{
			if(teacherDetails.size()>0){
				Session session 		= 	getSession();
				Criteria criteria 		= 	session.createCriteria(getPersistentClass());
				Criterion criterion1 	= 	Restrictions.eq("districtId",districtMaster.getDistrictId());
				Criterion criterion2 	= 	Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion3 	= 	Restrictions.eq("status", "A");
				Criterion criterion4 	= 	Restrictions.eq("statusMaster", statusMaster);
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion4);
				schoolWiseCandidateStatus = criteria.list();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolWiseCandidateStatus;
	}
	@Transactional(readOnly=true)
	public List<SchoolWiseCandidateStatus> findByTeacherStatusHistoryByHBDForSchool(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolMaster)
	{
        List <SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus =new ArrayList<SchoolWiseCandidateStatus>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Criterion criterion5 = Restrictions.eq("schoolId", schoolMaster.getSchoolId());
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtId",jobOrder.getDistrictMaster().getDistrictId()));
			}else{
				criteria.add(Restrictions.isNull("districtId"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			criteria.add(criterion5);
			lstSchoolWiseCandidateStatus = criteria.list();
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstSchoolWiseCandidateStatus;
	}
	@Transactional(readOnly=true)
	public List<SchoolWiseCandidateStatus> findByTeacherStatusHistoryByUserAndHBD(TeacherDetail teacherDetail,JobOrder jobOrder,UserMaster userMaster)
	{
        List <SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus =new ArrayList<SchoolWiseCandidateStatus>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Criterion criterion5 = Restrictions.eq("userMaster", userMaster);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtId",jobOrder.getDistrictMaster().getDistrictId()));
			}else{
				criteria.add(Restrictions.isNull("districtId"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			criteria.add(criterion5);
			lstSchoolWiseCandidateStatus = criteria.list();
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstSchoolWiseCandidateStatus;
	}
	@Transactional(readOnly=true)
	public List<SchoolWiseCandidateStatus> findByTeacherStatusHistoryForSchoolByHBD(TeacherDetail teacherDetail,JobOrder jobOrder,SchoolMaster schoolMaster)
	{
        List <SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus =new ArrayList<SchoolWiseCandidateStatus>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Criterion criterion5 = Restrictions.eq("schoolId", schoolMaster.getSchoolId());
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtId",jobOrder.getDistrictMaster().getDistrictId()));
			}else{
				criteria.add(Restrictions.isNull("districtId"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			
			criteria.add(criterion5);
			lstSchoolWiseCandidateStatus = criteria.list();
        }catch(Exception e){
        	//e.printStackTrace();
        }
		return lstSchoolWiseCandidateStatus;
	}

	@Transactional(readOnly=true)
	public List<Long> findByDistrictRejectAndDeclineOP(DistrictMaster districtMaster)
	{
        List<Long> lstJobForTeacher =new ArrayList<Long>();
        List <String[]> lstJobForTeacherw =null;
        try{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster.getDistrictId());
			Criterion criterion3 = Restrictions.eq("status", "A");
			String distinctJFT ="distinct(this_.jobForTeacherId)";
        	Session session = getSession();
        	 Criteria criteria = session.createCriteria(getPersistentClass()).createAlias("jobForTeacher", "jft");
        	 criteria.setProjection( Projections.projectionList().add(Projections.sqlProjection(""+distinctJFT, new String[]{"jobForTeacherId"}, new Type[]{ Hibernate.INTEGER}),"jobForTeacherId").add(Projections.property("jft.status.statusId")));
        	        	
        	 	criteria.add(criterion1);
    			criteria.add(criterion3);//.add(Restrictions.ne("statusmaster.statusId", 18)).add(Restrictions.ne("statusmaster.statusId", 6));    			
    			lstJobForTeacherw = criteria.list();    		
    			
    			if(lstJobForTeacherw!=null && lstJobForTeacherw.size()>0){
    				int cnt=0;
    				for (Iterator it = lstJobForTeacherw.iterator(); it.hasNext();) {
    		            Object[] row = (Object[]) it.next();
    		            
    		            if(!row[1].equals(6) && !row[1].equals(18)){
							lstJobForTeacher.add(Long.parseLong(row[0].toString()));							
    		            }
					}
    			}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstJobForTeacher;
	}
}

