package tm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherLoginHistory;
import tm.bean.TeacherNotes;
import tm.bean.UserLoginHistory;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class UserLoginHistoryDAO extends GenericHibernateDAO<UserLoginHistory, Integer>{

	public UserLoginHistoryDAO() {
		super(UserLoginHistory.class);
	}
	@Transactional(readOnly=true)
	public List<UserLoginHistory> findByUserLoginHistory(String sessionId)
	{
	         List <UserLoginHistory> lstUserLoginHistory = null;
	         Criterion criterion = Restrictions.eq("sessionId",sessionId);
	         lstUserLoginHistory = findByCriteria(criterion);
			 return lstUserLoginHistory;
	}
	
	@Transactional(readOnly=true)
	public List<UserLoginHistory> findUMHByUserMaster(UserMaster userMaster)
	{
	         List <UserLoginHistory> lstUserLoginHistory = null;
	         Criterion criterion = Restrictions.eq("userMaster",userMaster);
	         lstUserLoginHistory = findByCriteria(Order.desc("logTime"),criterion);
			 return lstUserLoginHistory;
	}
	
	@Transactional(readOnly=false)
	public void insertUserLoginHistory(UserMaster userMaster,String sessionId,String remoteAddr,Date date,String LogType)
	{
		try{
			UserLoginHistory userLoginHistory = new UserLoginHistory();
			userLoginHistory.setUserMaster(userMaster);
			userLoginHistory.setSessionId(sessionId);
			userLoginHistory.setIpAddress(remoteAddr);
			userLoginHistory.setLogTime(date);
			if(userMaster.getHeadQuarterMaster()!=null)
				userLoginHistory.setHeadQuarterMaster(userMaster.getHeadQuarterMaster());
			if(userMaster.getBranchMaster()!=null)
				userLoginHistory.setBranchMaster(userMaster.getBranchMaster());
			if(userMaster.getDistrictId()!=null)
				userLoginHistory.setDistrictId(userMaster.getDistrictId());
			if(userMaster.getSchoolId()!=null)
				userLoginHistory.setSchoolId(userMaster.getSchoolId());
			userLoginHistory.setLogType(LogType);
			makePersistent(userLoginHistory);
			if(userMaster.getEntityType()==1){
				userMaster.setSchoolId(null);
				userMaster.setDistrictId(null);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Transactional(readOnly=false)
	public List<UserLoginHistory> findUser(DistrictMaster districtMaster)
	{
		List<UserLoginHistory> lstUserLoginHistory= new ArrayList<UserLoginHistory>();;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.eq("logType","Enter in district dashboard");
			Criterion criterion3 = Restrictions.eq("logType","Enter in school dashboard");
			Criterion criterion4= Restrictions.or(criterion2, criterion3);
			criteria.add(criterion4);
			criteria.add(criterion1);
			criteria.createCriteria("userMaster").add(Restrictions.eq("districtId", districtMaster));
			criteria.addOrder(Order.asc("logTime"));
			lstUserLoginHistory = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstUserLoginHistory;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> countUserLoginByDistrict(boolean underLEA,List<DistrictMaster> districtMasters,List<SchoolMaster> schoolMasters) 
	{
		
		Session session = getSession();
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		String sql =" SELECT ulh.districtId, COUNT(*) AS distLogin from userloginhistory ulh " ;
		
		if(districtMasters!=null && districtMasters.size()>0){
			sql+="where ulh.districtId IN (:districtId)  ";	
			sql+=" and  ulh.logTime>=date_sub(now(), INTERVAL 7 DAY)";
		}
		
		sql+=" group by ulh.districtId ";// having distLogin>=5";
		
		if(underLEA){
			sql+=" having distLogin<10 and distLogin>0";
		}
		try
		{
			Query query = session.createSQLQuery(sql);
			if(districtMasters!=null && districtMasters.size()>0){
				query.setParameterList("districtId", districtMasters);
			}
			List<Object[]> rows = query.list();
			if(rows.size()>0)
			{
				for(Object oo: rows)
				{
					Object obj[] = (Object[])oo;
					map.put((Integer)obj[0], Utility.getIntValue(obj[1]+""));
				}
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> countUserLoginBySchool(boolean underLEA,List<DistrictMaster> districtMasters,List<SchoolMaster> schoolMasters) 
	{
		
		Session session = getSession();
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		String sql =" SELECT ulh.districtId, COUNT(*) AS distLogin from userloginhistory ulh " ;
		
		if(schoolMasters!=null && schoolMasters.size()>0){
			sql+=" where ulh.schoolId IN (:schoolIds)  ";	
			sql+=" and  ulh.logTime>=date_sub(now(), INTERVAL 7 DAY)";
		}else {
			return map;
		}
		
		sql+=" group by ulh.schoolId ";// having distLogin>=5";
		if(underLEA){
			sql+=" having distLogin<10 and distLogin>0";
		}
		try
		{
			Query query = session.createSQLQuery(sql);
			if(schoolMasters!=null && schoolMasters.size()>0){
				query.setParameterList("schoolIds", schoolMasters);
			}
			List<Object[]> rows = query.list();
			if(rows.size()>0)
			{
				for(Object oo: rows)
				{
					Object obj[] = (Object[])oo;
					map.put((Integer)obj[0], Utility.getIntValue(obj[1]+""));
				}
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> countUserLoginByDistrictLEA(boolean LEA,List<DistrictMaster> districtMasters,List<SchoolMaster> schoolMasters) 
	{
		
		Session session = getSession();
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		String sql =" SELECT ulh.districtId, COUNT(*) AS distLogin from userloginhistory ulh " ;
		
		if(districtMasters!=null && districtMasters.size()>0){
			sql+="where ulh.districtId IN (:districtId)  ";	
		}

		if(LEA){
			sql+=" and  ulh.logTime>=date_sub(now(), INTERVAL 7 DAY) ";
		}
		
		sql+=" group by ulh.districtId ";// having distLogin>=5";
		
		if(LEA)
			sql+="having distLogin>=10";
		
		try
		{
			Query query = session.createSQLQuery(sql);
			if(districtMasters!=null && districtMasters.size()>0){
				query.setParameterList("districtId", districtMasters);
			}
			List<Object[]> rows = query.list();
			if(rows.size()>0)
			{
				for(Object oo: rows)
				{
					Object obj[] = (Object[])oo;
					map.put((Integer)obj[0], Utility.getIntValue(obj[1]+""));
				}
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return map;
	}
	  
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> countUserLoginBySchoolLEA(boolean LEA,List<DistrictMaster> districtMasters,List<SchoolMaster> schoolMasters) 
	{
		
		Session session = getSession();
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		String sql =" SELECT ulh.districtId, COUNT(*) AS distLogin from userloginhistory ulh " ;
		
		if(schoolMasters!=null && schoolMasters.size()>0){
			sql+=" where ulh.schoolId IN (:schoolIds)  ";	
		}
		
		if(LEA){
			sql+=" and  ulh.logTime>=date_sub(now(), INTERVAL 7 DAY)";
		}
		
		sql+=" group by ulh.schoolId ";
		if(LEA)
			sql+="having distLogin>=10";
		
		try
		{
			Query query = session.createSQLQuery(sql);
			if(schoolMasters!=null && schoolMasters.size()>0){
				query.setParameterList("schoolIds", schoolMasters);
			}
			List<Object[]> rows = query.list();
			if(rows.size()>0)
			{
				for(Object oo: rows)
				{
					Object obj[] = (Object[])oo;
					map.put((Integer)obj[0], Utility.getIntValue(obj[1]+""));
				}
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	
}
