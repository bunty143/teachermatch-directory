package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.TeacherAchievementScore;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherAchievementScoreDAO extends GenericHibernateDAO<TeacherAchievementScore, Integer>{

	public TeacherAchievementScoreDAO() {
		super(TeacherAchievementScore.class);
		// TODO Auto-generated constructor stub
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAchievementScore> getAchievementScore(List<TeacherDetail> teacherDetails, JobOrder jobOrder)
	{	
		List<TeacherAchievementScore> teacherAchievementScoreList= new ArrayList<TeacherAchievementScore>();	
		try{
			if(teacherDetails.size()>0)
			{
				Criterion criterion = Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion1 = Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
			//	teacherAchievementScoreList = findByCriteria(criterion,criterion1);
				
				//  changed By Anurag For OPtimization 
				Criteria criteria = getSession().createCriteria(getPersistentClass());
				criteria.add(criterion).add(criterion1);
				criteria.setFetchMode("teacherDetail", FetchMode.LAZY);
				criteria.setFetchMode("districtMaster", FetchMode.LAZY);
				teacherAchievementScoreList = criteria.list();
				//  changed By Anurag For OPtimization  End
				
			}
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return teacherAchievementScoreList;
	}
	
	/**
	 * 
	 * @param teacherDetail
	 * @param jobOrder
	 * @return
	 * @author Ramesh Kumar Bhartiya
	 * Date Nov 16, 2013
	 * Use later
	 */
	@Transactional(readOnly=false)
	public List<TeacherAchievementScore> getAchievementScoreByTeacherAndJobOrder(TeacherDetail teacherDetail, JobOrder jobOrder)
	{	
		List<TeacherAchievementScore> teacherAchievementScoreList= new ArrayList<TeacherAchievementScore>();	
		try{
				Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion1 = Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
				teacherAchievementScoreList = findByCriteria(criterion,criterion1);				
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return teacherAchievementScoreList;
	}
	
	/**
	 * 
	 * @param teacherDetail
	 * @param districtMaster
	 * @return
	 * @author Ramesh Kumar Bhartiya
	 * Date Nov 16, 2013
	 * use later
	 */
	@Transactional(readOnly=false)
	public List<TeacherAchievementScore> getAchievementScoreByTeacherAndDistrict(TeacherDetail teacherDetail, DistrictMaster districtMaster)
	{	
		List<TeacherAchievementScore> teacherAchievementScoreList= new ArrayList<TeacherAchievementScore>();	
		try{
				Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
				teacherAchievementScoreList = findByCriteria(criterion,criterion1);				
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return teacherAchievementScoreList;
	}
	
	/**
	 * @author Vishwanath Kumar
	 */
	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeachersByAscoreOrLScore(String AScore,String AScoreSelectVal,DistrictMaster districtMaster,boolean isAscore)
	{
		List<TeacherDetail> teacherList= new ArrayList<TeacherDetail>();
		try 
		{
			String colField = "leadershipAchievementScore";
			if(isAscore)
				colField="academicAchievementScore";
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 =null;
			if(AScoreSelectVal.equals("1")){
				criterion1=Restrictions.eq(colField,Integer.parseInt(AScore));
			}else if(AScoreSelectVal.equals("2")){
				criterion1=Restrictions.lt(colField,Integer.parseInt(AScore));
			}else if(AScoreSelectVal.equals("3")){
				criterion1=Restrictions.le(colField,Integer.parseInt(AScore));
			}else if(AScoreSelectVal.equals("4")){
				criterion1=Restrictions.gt(colField,Integer.parseInt(AScore));
			}else if(AScoreSelectVal.equals("5")){
				criterion1=Restrictions.ge(colField,Integer.parseInt(AScore));
			}
			criteria.add(criterion1);
			criteria.add(Restrictions.eq("districtMaster", districtMaster));
			criteria.setProjection(Projections.groupProperty("teacherDetail"));
			teacherList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAchievementScore> getAchievementScoreListByTDList(List<TeacherDetail> teacherDetails)
	{	
		List<TeacherAchievementScore> teacherAchievementScoreList= new ArrayList<TeacherAchievementScore>();	
		try{
				Criterion criterion = Restrictions.in("teacherDetail",teacherDetails);
				
				teacherAchievementScoreList = findByCriteria(criterion);				
		} 
		catch (Exception e){
			e.printStackTrace();
		}	
		return teacherAchievementScoreList;
	}
	

	// Optimization for candidatePool search
	
	@Transactional(readOnly=false)
	public List<Integer> findTeachersByAscoreOrLScore_Op(String AScore,String AScoreSelectVal,DistrictMaster districtMaster,boolean isAscore, List<Integer> teachersId)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
			try 
			{
				String colField = "leadershipAchievementScore";
				if(isAscore)
					colField="academicAchievementScore";
				
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 =null;
				if(AScoreSelectVal.equals("1")){
					criterion1=Restrictions.eq(colField,Integer.parseInt(AScore));
				}else if(AScoreSelectVal.equals("2")){
					criterion1=Restrictions.lt(colField,Integer.parseInt(AScore));
				}else if(AScoreSelectVal.equals("3")){
					criterion1=Restrictions.le(colField,Integer.parseInt(AScore));
				}else if(AScoreSelectVal.equals("4")){
					criterion1=Restrictions.gt(colField,Integer.parseInt(AScore));
				}else if(AScoreSelectVal.equals("5")){
					criterion1=Restrictions.ge(colField,Integer.parseInt(AScore));
				}
				criteria.add(criterion1);
				criteria.add(Restrictions.eq("districtMaster", districtMaster));
				criteria.add(Restrictions.in("teacherDetail.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				
				criteria.setProjection(Projections.groupProperty("teacherDetail"));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}
	
	
	@Transactional(readOnly=false)
	public List<Integer> findTeachersByAscoreOrLScore_Op(String AScore,String AScoreSelectVal,DistrictMaster districtMaster,boolean isAscore)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
		
			try 
			{
				String colField = "leadershipAchievementScore";
				if(isAscore)
					colField="academicAchievementScore";
				
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 =null;
				if(AScoreSelectVal.equals("1")){
					criterion1=Restrictions.eq(colField,Integer.parseInt(AScore));
				}else if(AScoreSelectVal.equals("2")){
					criterion1=Restrictions.lt(colField,Integer.parseInt(AScore));
				}else if(AScoreSelectVal.equals("3")){
					criterion1=Restrictions.le(colField,Integer.parseInt(AScore));
				}else if(AScoreSelectVal.equals("4")){
					criterion1=Restrictions.gt(colField,Integer.parseInt(AScore));
				}else if(AScoreSelectVal.equals("5")){
					criterion1=Restrictions.ge(colField,Integer.parseInt(AScore));
				}
				criteria.add(criterion1);
				criteria.add(Restrictions.eq("districtMaster", districtMaster));
				criteria.setProjection(Projections.property("teacherDetail.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherDetail.teacherId")));
				
				criteria.setProjection(Projections.groupProperty("teacherDetail"));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}
	
}
