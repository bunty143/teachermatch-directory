package tm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherAcademics;
import tm.bean.TeacherDetail;
import tm.bean.master.DegreeMaster;
import tm.bean.master.FieldOfStudyMaster;
import tm.bean.master.UniversityMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherAcademicsDAO extends GenericHibernateDAO<TeacherAcademics, Long> 
{	
	public TeacherAcademicsDAO() 
	{
		super(TeacherAcademics.class);
	}

	@Transactional(readOnly=false)
	public List<TeacherAcademics> findAcadamicDetailByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherAcademics> lstAcademics= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			lstAcademics = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstAcademics;
	}

	@Transactional(readOnly=false)
	public List<TeacherAcademics> findSortedAcadamicDetailByTeacher(Order  sortOrderStrVal,TeacherDetail teacherDetail)
	{
		List<TeacherAcademics> lstAcademics= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			lstAcademics = findByCriteria(sortOrderStrVal,criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstAcademics;
	}


	@Transactional(readOnly=false)
	public List<TeacherAcademics> findAcademicsForTeachers(List<TeacherDetail> teacherDetails)
	{
		List<TeacherAcademics> teacherAcademicList = null;
		try 
		{
			if(teacherDetails.size()>0){
				Criterion criterion1 = Restrictions.in("teacherId",teacherDetails);
				teacherAcademicList = findByCriteria(Order.asc("teacherId"),criterion1);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		

		return teacherAcademicList;
	}

	@Transactional(readOnly=false)
	public Map<Integer, Boolean> findActiveAcademicsStatus(List<TeacherDetail> teacherDetails)
	{
		List<TeacherAcademics> lstTeacherAcademics = null;
		Map<Integer,Boolean> mapTeacherAcademics = new TreeMap<Integer, Boolean>();
		try 
		{
			if(teacherDetails.size()>0)
			{
				Criterion criterion1 = Restrictions.in("teacherId",teacherDetails);
				lstTeacherAcademics = findByCriteria(criterion1);		
				for(TeacherAcademics tAcademics: lstTeacherAcademics){
					mapTeacherAcademics.put(tAcademics.getTeacherId().getTeacherId(), true);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		return mapTeacherAcademics;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> findTeacherByDegree(DegreeMaster degreeMaster)
	{
		List<TeacherDetail> teacherList= new ArrayList<TeacherDetail>();
		List<Integer> teacherIdList= new ArrayList<Integer>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("degreeId",degreeMaster);
			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("teacherId"));
			teacherList =  criteria.list();
			
			for(TeacherDetail teacherObj: teacherList){
				teacherIdList.add(teacherObj.getTeacherId());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherIdList;
	}
	@Transactional(readOnly=true)
	public List<TeacherAcademics> findAcademicsByTeacher(List<TeacherDetail> teacherDetailList)
	{
	         List <TeacherAcademics> lstTeacherAcademics = new ArrayList<TeacherAcademics>();
	         try{
	        	 Criterion criterion = Restrictions.in("teacherId",teacherDetailList);
	        	 lstTeacherAcademics = findByCriteria(Order.desc("createdDateTime"),criterion);
	         }catch(Exception e){}
			 return lstTeacherAcademics;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeacherListByDegree(DegreeMaster degreeMaster)
	{
		List<TeacherDetail> teacherList= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("degreeId",degreeMaster);
			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("teacherId"));
			teacherList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeacherListByUniversity(UniversityMaster universityMaster)
	{
		List<TeacherDetail> teacherList= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("universityId",universityMaster);
			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("teacherId"));
			teacherList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeacherListByCGPA(String CGPA,String CGPASelectVal)
	{
		List<TeacherDetail> teacherList= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 =null;
			if(CGPASelectVal.equals("1")){
				criterion1=Restrictions.eq("gpaCumulative",Double.parseDouble(CGPA));
			}else if(CGPASelectVal.equals("2")){
				criterion1=Restrictions.lt("gpaCumulative",Double.parseDouble(CGPA));
			}else if(CGPASelectVal.equals("3")){
				criterion1=Restrictions.le("gpaCumulative",Double.parseDouble(CGPA));
			}else if(CGPASelectVal.equals("4")){
				criterion1=Restrictions.gt("gpaCumulative",Double.parseDouble(CGPA));
			}else if(CGPASelectVal.equals("5")){
				criterion1=Restrictions.ge("gpaCumulative",Double.parseDouble(CGPA));
			}
			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("teacherId"));
			teacherList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	
	
	@Transactional(readOnly=false)
	public List<TeacherAcademics> findTranscript_Null(TeacherDetail teacherDetail)
	{
		List<TeacherAcademics> lstAcademics= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			//Criterion criterion2 = Restrictions.isEmpty("pathOfTranscript");
			Criterion criterion2 = Restrictions.disjunction()
	        .add(Restrictions.isNull("pathOfTranscript"))
	        .add(Restrictions.eq("pathOfTranscript", ""));
	        
			lstAcademics = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstAcademics;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAcademics> findByTeacherDetailsObj(List<TeacherDetail> teacherDetailsObj)
	{
		List<TeacherAcademics> lstteacherAcademics = new ArrayList<TeacherAcademics>();
		
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherId",teacherDetailsObj);
			lstteacherAcademics = findByCriteria(criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstteacherAcademics==null || lstteacherAcademics.size()==0)
			return null;
		else
			return lstteacherAcademics;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAcademics> findAcadamicDetailByTeacherWithBType(TeacherDetail teacherDetail)
	{
		List<TeacherAcademics> lstAcademics= new ArrayList<TeacherAcademics>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion1);
			criteria.createCriteria("degreeId").add(Restrictions.eq("degreeType", "B"));
			lstAcademics = criteria.list();	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstAcademics;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherAcademics> findCGPAByTeacher(List<TeacherDetail> teacherDetailList)
	{
	         List <TeacherAcademics> lstTeacherAcademics = new ArrayList<TeacherAcademics>();
	         try{
	        	 Session session = getSession();
	 			Criteria criteria = session.createCriteria(getPersistentClass());
	 			Criterion criterion1 = Restrictions.in("teacherId",teacherDetailList);
	 			criteria.add(criterion1).addOrder(Order.asc("leftInYear"));
	 			criteria.createCriteria("degreeId").add(Restrictions.eq("degreeType", "B"));
	        	 lstTeacherAcademics = criteria.list();

	        	 
	        	 
	        	 /*Criterion criterion = Restrictions.in("teacherId",teacherDetailList);
	        	 lstTeacherAcademics = findByCriteria(Order.asc("leftInYear"),criterion);*/
	         }catch(Exception e){
	        	 e.printStackTrace();
	         }
			 return lstTeacherAcademics;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherAcademics> ChkIdentical(TeacherDetail teacherDetail,DegreeMaster degreeMaster,UniversityMaster universityMaster,FieldOfStudyMaster fieldOfStudyMaster)
	{
		List<TeacherAcademics> teacherAcademicList = new ArrayList<TeacherAcademics>();
		try 
		{
				Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
				Criterion criterion2 = Restrictions.eq("degreeId",degreeMaster);
				Criterion criterion3 = Restrictions.eq("universityId",universityMaster);
				Criterion criterion4 = Restrictions.eq("fieldId",fieldOfStudyMaster);
				teacherAcademicList = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherAcademicList;
	}

	// Optimization for Candidate Pool search replacement of  findTeacherListByDegree

	@Transactional(readOnly=false)
	public List<Integer> findTeacherListByDegree_Op(DegreeMaster degreeMaster, List<Integer> teachersId)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.eq("degreeId",degreeMaster);
				criteria.add(criterion1);
	
				criteria.add(Restrictions.in("teacherId.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherId.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return teacherList;
		}

	// Optimization for Candidate Pool search replacement of  findTeacherListByUniversity
	
	@Transactional(readOnly=false)
	public List<Integer> findTeacherListByUniversity_Op(UniversityMaster universityMaster, List<Integer> teachersId)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.eq("universityId",universityMaster);
				criteria.add(criterion1);
				criteria.add(Restrictions.in("teacherId.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherId.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}
	
	// Optimization for Candidate Pool search replacement of  findTeacherListByCGPA
	
	@Transactional(readOnly=false)
	public List<Integer> findTeacherListByCGPA_Op(String CGPA,String CGPASelectVal, List<Integer> teachersId)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
		
		if(teachersId!=null && teachersId.size()>0)
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 =null;
				if(CGPASelectVal.equals("1")){
					criterion1=Restrictions.eq("gpaCumulative",Double.parseDouble(CGPA));
				}else if(CGPASelectVal.equals("2")){
					criterion1=Restrictions.lt("gpaCumulative",Double.parseDouble(CGPA));
				}else if(CGPASelectVal.equals("3")){
					criterion1=Restrictions.le("gpaCumulative",Double.parseDouble(CGPA));
				}else if(CGPASelectVal.equals("4")){
					criterion1=Restrictions.gt("gpaCumulative",Double.parseDouble(CGPA));
				}else if(CGPASelectVal.equals("5")){
					criterion1=Restrictions.ge("gpaCumulative",Double.parseDouble(CGPA));
				}
				criteria.add(criterion1);
				criteria.add(Restrictions.in("teacherId.teacherId", teachersId));
				criteria.setProjection(Projections.property("teacherId.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}
	
	
	
	@Transactional(readOnly=false)
	public List<TeacherAcademics> findAcademicsForTeachersOp(List<TeacherDetail> teacherDetails)
	{
		List<TeacherAcademics> teacherAcademicList = new ArrayList<TeacherAcademics>();
		List data = new ArrayList();
		try 
		{
			
		
			if(teacherDetails.size()>0){
				Criteria criteria = getSession().createCriteria(this.getPersistentClass());
				criteria.add(Restrictions.in("teacherId",teacherDetails));
				criteria.setProjection(Projections.projectionList()
						.add(Projections.property("academicId"))
						.add(Projections.property("status"))
						.add(Projections.property("degreeConferredDate"))
						.add(Projections.property("teacherId.teacherId")));
				
				data = criteria.list();
				
				Long id =null;
				String status = "";
				Date date = null;
				for (Iterator it = data.iterator(); it.hasNext();)
				{
					 TeacherAcademics techAcd =  new TeacherAcademics();
					 TeacherDetail td = new TeacherDetail();
		            Object[] row = (Object[]) it.next();

		            if(row[0]!=null)
		            {
		            	id= Long.parseLong(row[0].toString());
		            	status = row[1]!=null?row[1].toString():"";
		            	date = (Date) row[2];
		            	techAcd.setAcademicId(id);
		            	techAcd.setStatus(status);
		            	techAcd.setDegreeConferredDate(date);
		            	td.setTeacherId(Integer.parseInt(row[3].toString()));
		            	techAcd.setTeacherId(td);
		            	teacherAcademicList.add(techAcd);
		            }
		            
		            
		            
		            }
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		

		return teacherAcademicList;
	}

	@Transactional(readOnly=false)
	public List<Integer> findTeacherListByDegreeId_Op(DegreeMaster degreeMaster)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
		
		if(degreeMaster!=null)
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.eq("degreeId",degreeMaster);
				criteria.add(criterion1);
	
				criteria.setProjection(Projections.property("teacherId.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return teacherList;
		}

	@Transactional(readOnly=false)
	public List<Integer> findTeacherListByUniversityId_Op(UniversityMaster universityMaster)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
		
		if(universityMaster!=null)
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.eq("universityId",universityMaster);
				criteria.add(criterion1);
				criteria.setProjection(Projections.property("teacherId.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> findTeacherListByCGPA_Op(String CGPA,String CGPASelectVal)
	{
		List<Integer> teacherList= new ArrayList<Integer>();
		
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 =null;
				if(CGPASelectVal.equals("1")){
					criterion1=Restrictions.eq("gpaCumulative",Double.parseDouble(CGPA));
				}else if(CGPASelectVal.equals("2")){
					criterion1=Restrictions.lt("gpaCumulative",Double.parseDouble(CGPA));
				}else if(CGPASelectVal.equals("3")){
					criterion1=Restrictions.le("gpaCumulative",Double.parseDouble(CGPA));
				}else if(CGPASelectVal.equals("4")){
					criterion1=Restrictions.gt("gpaCumulative",Double.parseDouble(CGPA));
				}else if(CGPASelectVal.equals("5")){
					criterion1=Restrictions.ge("gpaCumulative",Double.parseDouble(CGPA));
				}
				criteria.add(criterion1);
				criteria.setProjection(Projections.property("teacherId.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
				teacherList =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
		return teacherList;
	}
}
