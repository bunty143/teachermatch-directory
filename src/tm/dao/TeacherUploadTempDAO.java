package tm.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.TeacherUploadTemp;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictNotes;
import tm.bean.master.DistrictSchools;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherUploadTempDAO extends GenericHibernateDAO<TeacherUploadTemp,Integer> 
{
	public TeacherUploadTempDAO() {
		super(TeacherUploadTemp.class);
	}
	
	
	@Transactional(readOnly=false)
	public boolean deleteTeacherTemp(String sessionId){
		try{
			Criterion criterion = Restrictions.eq("sessionid",sessionId);
        	List<TeacherUploadTemp> teacherUploadTemplst	=findByCriteria(criterion);
        	for(TeacherUploadTemp teacherUploadTemp: teacherUploadTemplst){
        		makeTransient(teacherUploadTemp);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	@Transactional(readOnly=false)
	public boolean deleteAllTeacherTemp(String sessionId)
	{
		try{
			Calendar c = Calendar.getInstance(); 
			c.add(Calendar.DAY_OF_YEAR, -2);  
			Date date = c.getTime();
			Criterion criterion1=Restrictions.lt("date",date);
			Criterion criterion2 = Restrictions.eq("sessionid",sessionId);
			Criterion complete3 = Restrictions.or(criterion1,criterion2);
        	List<TeacherUploadTemp> teacherUploadTemplst	=findByCriteria(complete3);
        	System.out.println("teacherUploadTemplst::::"+teacherUploadTemplst.size());
			for(TeacherUploadTemp teacherUploadTemp: teacherUploadTemplst){
        		makeTransient(teacherUploadTemp);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return true;
	}

	@Transactional(readOnly=true)
	public List<TeacherUploadTemp> findByTempTeacher(Order order,int startPos,int limit,Criterion... criterion)
	{
		List<TeacherUploadTemp> lstTeacherUploadTemp = null;
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			criteria.addOrder(order);
			lstTeacherUploadTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherUploadTemp;
	}
	@Transactional(readOnly=true)
	public List<TeacherUploadTemp> findByAllTempTeacher()
	{
		List<TeacherUploadTemp> lstTeacherUploadTemp = null;
		try{
			Session session = getSession();
			Calendar c = Calendar.getInstance(); // starts with today's date and time
			c.add(Calendar.DAY_OF_YEAR, -2);  // advances day by 2
			Date date = c.getTime();
			Criterion criterion=Restrictions.lt("date",date);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion);
			lstTeacherUploadTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherUploadTemp;
	}
	
	@Transactional(readOnly=true)
	public int getRowCountTempTeacher(Criterion... criterion)
	{
		List<TeacherUploadTemp> lstTeacherUploadTemp = null;
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			lstTeacherUploadTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherUploadTemp.size();
	}
	@Transactional(readOnly=true)
	public List<TeacherUploadTemp> findByAllTempTeacherCurrentSession(String sessionId)
	{
		List<TeacherUploadTemp> lstTeacherUploadTemp = null;
		try{
			Session session = getSession();
			Criterion criterion2 = Restrictions.eq("sessionid",sessionId);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion2);
			lstTeacherUploadTemp = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherUploadTemp;
	}
	
	
	// ************* Adding By Deepak   ****************************
	@Transactional(readOnly=true)
	public List<TeacherUploadTemp> findByAllTempTeacherEmail(List emails)
	{
		List<TeacherUploadTemp> TeacherDetailListTemp= null;
		try 
		{
			Criterion criterion = Restrictions.in("user_email",emails);	
			TeacherDetailListTemp =	findByCriteria(criterion);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return TeacherDetailListTemp;
	}
	//******************************************************************
}
