package tm.dao.assessment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.InternalTransferCandidates;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentGroupDetails;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.TeacherAnswerDetailsForDistrictSpecificQuestionsDAO;
import tm.servlet.WorkThreadServlet;
/* @Author: Vishwanath Kumar
 * @Discription: AssessmentDetail DAO.
 */
public class AssessmentDetailDAO extends GenericHibernateDAO<AssessmentDetail, Integer> 
{

	public AssessmentDetailDAO() {
		super(AssessmentDetail.class);
	}
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;

	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	
	@Autowired
	private TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO;
	
/* @Author: Vishwanath Kumar
 * @Discription: It is used to check active Base Assessments.
 */
	@Transactional(readOnly=true)
	public List<AssessmentDetail> checkActiveBaseAssessment()
	{
		List<AssessmentDetail> assessmentDetails = null;
		try {
			Criterion criterion = Restrictions.eq("status", "A");
			Criterion criterion1 = Restrictions.eq("assessmentType", 1);
			assessmentDetails = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return assessmentDetails;
	}

/* @Author: Vishwanath Kumar
 * @Discription: It is used to check Default Assessment For District Or School.
 */
	@Transactional(readOnly=true)
	public List<AssessmentDetail> checkDefaultAssessmentForDistrictOrSchool(AssessmentDetail assessmentDetail)//,Integer jobOrderType,DistrictMaster districtMaster,SchoolMaster schoolMaster)
	{
		List<AssessmentDetail> assessmentDetails = null;
		try {
			Criterion criterion = Restrictions.eq("isDefault", true);
			Criterion criterion1 = Restrictions.eq("assessmentType", 2);
			Criterion criterion2 = Restrictions.eq("jobOrderType", assessmentDetail.getJobOrderType());
			Criterion criterion3 = null;
			Criterion criterionJobCat = Restrictions.eq("jobCategoryMaster", assessmentDetail.getJobCategoryMaster());
			
			if(assessmentDetail.getJobOrderType()==1)
				criterion3= Restrictions.eq("districtMaster", assessmentDetail.getDistrictMaster());
			else 
				criterion3= Restrictions.eq("schoolMaster", assessmentDetail.getSchoolMaster());
			
			if(assessmentDetail.getAssessmentId()==null)
			{
				if(assessmentDetail.getJobCategoryMaster()==null)
					assessmentDetails = findByCriteria(criterion,criterion1,criterion2,criterion3);
				else
					assessmentDetails = findByCriteria(criterion,criterion1,criterion2,criterion3,criterionJobCat);
			}
			else{
				
				Criterion criterion4 = Restrictions.not(Restrictions.in("assessmentId", new Integer[] {assessmentDetail.getAssessmentId()}));
				if(assessmentDetail.getJobCategoryMaster()==null)
					assessmentDetails = findByCriteria(criterion,criterion1,criterion2,criterion3,criterion4);
				else
					assessmentDetails = findByCriteria(criterion,criterion1,criterion2,criterion3,criterion4,criterionJobCat);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return assessmentDetails;
	}
	/* @Author: Sudhansu Sekhar Swain
	 * @Discription: It is used to check isDefault.
	 */
		@Transactional(readOnly=true)
		public AssessmentDetail checkIsDefault(DistrictMaster districtMaster, SchoolMaster schoolMaster,int defaultType,JobOrder jobOrder)
		{
			List<AssessmentDetail> assessmentDetailslst = null;
			try {
				Criterion criterion = Restrictions.eq("status", "A");
				Criterion criterion1 = Restrictions.eq("isDefault",true);
				if(defaultType==1 && jobOrder==null){
					Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
					Criterion criterion3 = Restrictions.eq("jobOrderType",1);
					Criterion criterion4 = Restrictions.isNull("jobCategoryMaster");
					assessmentDetailslst = findByCriteria(criterion,criterion1,criterion2,criterion3,criterion4);
				}else if(defaultType==2 && jobOrder==null){
					Criterion criterion2 = Restrictions.eq("jobOrderType",2);
					Criterion criterion3 = Restrictions.eq("schoolMaster",schoolMaster);
					Criterion criterion4 = Restrictions.isNull("jobCategoryMaster");
					assessmentDetailslst = findByCriteria(criterion,criterion1,criterion2,criterion3,criterion4);
				}else if(jobOrder.getJobCategoryMaster()!=null){
					Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
					Criterion criterion3 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
					assessmentDetailslst = findByCriteria(criterion,criterion1,criterion2,criterion3);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(assessmentDetailslst.size()>0){
				return assessmentDetailslst.get(0);
			}else{
				return null;
			}
		}
		
		/* @Author: Vishwanath Kumar
		 * @Discription: It is used to check active Base Assessments.
		 */
		@Transactional(readOnly=true)
		public List<AssessmentDetail> checkAssessmentByName(String assessmentName)
		{
			List<AssessmentDetail> assessmentDetails = null;
			try {
				Criterion criterion = Restrictions.eq("assessmentName", assessmentName);
				assessmentDetails = findByCriteria(criterion);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return assessmentDetails;
		}
		
		@Transactional(readOnly=false)	
		public StatusMaster findByTeacherIdJobStaus(JobForTeacher jobForTeacher,int[] flagList) 
		{	
			TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
			JobOrder jobOrder =  jobForTeacher.getJobId();
			Integer jobAssessmentStatus = jobOrder.getJobAssessmentStatus();
			
			
			 if(jobOrder.getJobCategoryMaster()!=null){
				 if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==4218990 && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Principal") && jobForTeacher.getIsAffilated()==1){
				 		jobAssessmentStatus=0;
				 	}
				 }
			 
			 System.out.println("jobAssessmentStatus::: "+jobAssessmentStatus);
			 
			TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			StatusMaster statusMaster = null;
			statusMaster = WorkThreadServlet.statusMap.get("icomp");
			//System.out.println("Flags:"+flagList[0]+" : "+flagList[1]+" : "+flagList[2]+" : "+ flagList[3]+" : "+flagList[4]);
			AssessmentDetail assessmentDetail = null;
			TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jobForTeacher.getTeacherId());
			try
			{
				/*boolean statusIcomp=false;
				try{
					if(jobOrder.getIsInviteOnly()!=null && jobOrder.getIsInviteOnly() && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("icomp")){
						statusIcomp=true;
		            }
				}catch(Exception e){
					e.printStackTrace();
				}*/
				if(((teacherDetail.getIsPortfolioNeeded()) && teacherPortfolioStatus==null && flagList[2]==0 && flagList[1]==1))
				{
					System.out.println("1");
					statusMaster = WorkThreadServlet.statusMap.get("icomp");
				}
				else
				{
					if(flagList[1]==0 || (flagList[1]==1 && teacherPortfolioStatus!=null  && ((teacherDetail.getIsPortfolioNeeded()&& teacherPortfolioStatus.getIsPersonalInfoCompleted() && teacherPortfolioStatus.getIsAcademicsCompleted() && teacherPortfolioStatus.getIsCertificationsCompleted() && teacherPortfolioStatus.getIsExperiencesCompleted() && teacherPortfolioStatus.getIsAffidavitCompleted()))))
					{
						statusMaster = WorkThreadServlet.statusMap.get("comp");
						if(!jobOrder.getJobCategoryMaster().getBaseStatus() || (flagList[2]==1 && flagList[3]==0)||(flagList[2]==0 && flagList[3]==0 && flagList[0]==1)){
							if(jobOrder.getIsJobAssessment() && ((flagList[4]==1 && flagList[2]==1 )|| (flagList[2]==0 && flagList[0]==1 && flagList[4]==1 ) || (flagList[2]==0 && (flagList[0]==0 || flagList[0]==2))))
							{
								if(jobAssessmentStatus==1)
								{
									List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
									if(lstAssessmentJobRelation.size()>0)
									{
										AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
										assessmentDetail = assessmentJobRelation.getAssessmentId();
										List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail, assessmentDetail);
	
										if(lstTeacherAssessmentStatus.size()>0)
										{	
											System.out.println("2");
											teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
											statusMaster = teacherAssessmentStatus.getStatusMaster();
										}
										else
										{
											System.out.println("3");
											statusMaster = WorkThreadServlet.statusMap.get("icomp");
										}					
									}
									else
									{
										System.out.println("4");
										statusMaster = WorkThreadServlet.statusMap.get("icomp");
									}
								}
							}
							else{
								System.out.println("5");
								statusMaster = WorkThreadServlet.statusMap.get("comp");
							}
						}
						else if(jobOrder.getIsJobAssessment()&& ((flagList[4]==1 && flagList[2]==1 )|| (flagList[2]==0 && flagList[0]==1 && flagList[4]==1 ) || (flagList[2]==0 && (flagList[0]==0 || flagList[0]==2)))){
							/*============== Gagan : Job specific Inventory (JSI) Status is checking here : ==============*/
							if(teacherAssessmentStatus!=null){
								if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
									if(jobForTeacher.getIsAffilated()!=null && jobOrder.getJobCategoryMaster().getOfferJSI()!=1 && jobForTeacher.getIsAffilated()==1)
									{
										// No JSI required 
										statusMaster = WorkThreadServlet.statusMap.get("comp");
										System.out.println("5.1 No JSI required");
									}else 
									{
										if(jobAssessmentStatus==1)
										{
											List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
											if(lstAssessmentJobRelation.size()>0)
											{
												AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
												assessmentDetail = assessmentJobRelation.getAssessmentId();
												List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail, assessmentDetail);
				
												if(lstTeacherAssessmentStatus.size()>0)
												{
													System.out.println("6");
													teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
													statusMaster = teacherAssessmentStatus.getStatusMaster();
												}
												else
												{
													System.out.println("7");
													statusMaster = WorkThreadServlet.statusMap.get("icomp");
												}					
											}
											else
											{
												System.out.println("8");
												statusMaster = WorkThreadServlet.statusMap.get("icomp");
											}
										}
									}
								}else{
									System.out.println("9");
									statusMaster=teacherAssessmentStatus.getStatusMaster();
								}
							}else{
								System.out.println("10");
								statusMaster = WorkThreadServlet.statusMap.get("icomp");
							}
						}
						else{
							boolean  evDistrict=false;
							try{
								if(jobOrder.getDistrictMaster()!=null){
									if(jobOrder.getDistrictMaster().getDistrictId()==5513170){
										evDistrict=true;
									}
								}
							}catch(Exception e){}
							/*============== Gagan : Base Status is checking here : ==============*/
							if((flagList[0]!=1 || evDistrict )&&((flagList[3]==1 && flagList[2]==1 )|| (flagList[2]==0 && flagList[0]==0) || (flagList[2]==0 && flagList[0]==1 && flagList[3]==1))) //if block will execute only if Integer.parseInt(isAffilated) value is 0 or null
							{	
								List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
								JobOrder jOrder = new JobOrder();
								jOrder.setJobId(0);
								teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(),jOrder);
								if(teacherAssessmentStatusList.size()>0){
									System.out.println("11");
									teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
									statusMaster = teacherAssessmentStatus.getStatusMaster();
								}else{
									System.out.println("12");
									statusMaster = WorkThreadServlet.statusMap.get("icomp");
								}
							}
							else // isAffilated else block
							{
								System.out.println("13");
								statusMaster = WorkThreadServlet.statusMap.get("comp");
							}
						}
					}
					else
					{
						System.out.println("14");
						statusMaster = WorkThreadServlet.statusMap.get("icomp");
					}
				}
				if(statusMaster!=null)
				System.out.println("Main statusMaster::"+statusMaster.getStatusShortName());
			}
			catch(Exception n)
			{
				n.printStackTrace();
				//return 0;
			}
			return statusMaster;

		}
		public int[] getInternalTeacherFalgs(JobForTeacher jobForTeacher){
			int[] flagList = new int[5];
			try{
				TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
				JobOrder jobOrder =  jobForTeacher.getJobId();
				
				 int isAffilated=0;
				 boolean epiInternal=false;
				
				 int isPortfolio=0;
				 if(jobOrder.getIsPortfolioNeeded()!=null){
					 if(jobOrder.getIsPortfolioNeeded())
						 isPortfolio=1;
				 }
				DistrictMaster districtMaster = jobOrder.getDistrictMaster();
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
				int internalFlag=0;
				int offerEPI =0;
				int offerJSI =0;
				if(itcList.size()>0){
					internalFlag=1;
				}
				if(districtMaster!=null && internalFlag==1){
					if(districtMaster.getOfferPortfolioNeeded()){
						isPortfolio=1;
					}else{
						isPortfolio=0;
					}
					if(districtMaster.getOfferEPI()){
						offerEPI=1;
					}else{
						offerEPI=0;
					}
					if(districtMaster.getOfferJSI()){
						offerJSI=1;
					}else{
						offerJSI=0;
					}
				}
				try{
				 if(jobOrder.getJobCategoryMaster()!=null){
						if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()!=null){
							if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
								epiInternal=true;
							}
							if(jobForTeacher.getIsAffilated()!=null){
								 isAffilated=jobForTeacher.getIsAffilated();
							}
							if(internalFlag==0 && isAffilated==1){
								if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
									offerEPI=1;
								}else{
									offerEPI=0;
								}
								if(jobOrder.getJobCategoryMaster().getOfferJSI()!=0){
									offerJSI=1;
								}else{
									offerJSI=0;
								}
								
								if(jobOrder.getJobCategoryMaster().getOfferPortfolioNeeded()){
									isPortfolio=1;
								}else{
									isPortfolio=0;
								}
							}
						}
				 }
				}catch(Exception e){
					e.printStackTrace();
				}
				 if(jobForTeacher.getIsAffilated()!=null){
					 System.out.println("PPPPPPPPPPPPP epiInternal "+epiInternal);
					 if(epiInternal==false){
						 isAffilated=jobForTeacher.getIsAffilated();
					 }else{
						 isAffilated=0;
					 }
				 }
				 
				//JSI optional for internal principal and principal job category
				 if(jobOrder.getJobCategoryMaster()!=null){
				 	if(jobOrder.getJobCategoryMaster().equals("Principal") && isAffilated==1){
				 	    offerJSI=0;
				 	}
				 }
				 
				flagList[0]=isAffilated;
				flagList[1]=isPortfolio;
				flagList[2]=internalFlag;
				flagList[3]=offerEPI;
				flagList[4]=offerJSI;
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return flagList;
		}
		
		@Transactional(readOnly=true)
		public boolean getRequiredDSPQAndQQCheck(TeacherDetail teacherDetail, JobOrder jobOrder)
		{
			System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<:::::::getRequiredDSPQAndQQCheck::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			int isDSPQRequired=0;
			int isQQRequired=0;
			int finalComplete=0;
			try{
			JobCategoryMaster jobCategoryMaster=jobOrder.getJobCategoryMaster();
			if(jobCategoryMaster.getOfferDistrictSpecificItems())
				isDSPQRequired=1;
			
			System.out.println("isRequired===================="+isDSPQRequired+" finalComplete==================="+finalComplete);
			if(isDSPQRequired==1){
				List<String[]> lstJobForTeacher=new ArrayList<String[]>();		    	
			         try{
			               Session session = getSession();			               
			               //Start DSPQ check required
			               Criteria criteria = session.createCriteria(JobForTeacher.class);             
			               criteria.setProjection( Projections.projectionList()
			                       .add( Projections.property("flagForDspq"), "flagFor_Dspq")			                       
			                   );
			               criteria.add(Restrictions.eq("teacherId",teacherDetail));
			               Criteria cri=criteria.createCriteria("jobId");
			               cri.add(Restrictions.eq("jobCategoryMaster", jobCategoryMaster));			               
			               cri.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			               criteria.add(Restrictions.eq("flagForDspq",true));
			               lstJobForTeacher = criteria.list() ;
			               if(lstJobForTeacher.size()>0){
			            	   finalComplete=1;
			               }
			               //End DSPQ			             
			         }catch (Exception e) {
			               e.printStackTrace();
			         }   
			}
			System.out.println("isRequired===================="+isDSPQRequired+"  isQQRequired=="+isQQRequired+" finalComplete==================="+finalComplete);
			//Start QQ Check Required
			//if(finalComplete==1){				
			List<DistrictMaster> districts = new ArrayList<DistrictMaster>();
			districts.add(jobOrder.getDistrictMaster());	       
	       	List<DistrictMaster> districtDSQList = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistricts(districts);
	       			if(districtDSQList.size()>0){
	       				finalComplete=0;
	       				isQQRequired=1;
	       				districtDSQList.clear();
	       				districtDSQList = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findByQQTakenByTeacher(districts,teacherDetail);
	       				if(districtDSQList.size()>0){
	       					finalComplete=1;
	       				}
	       			}       		
			//}
            //END QQ
			System.out.println("isRequired===================="+isDSPQRequired+"  isQQRequired=="+isQQRequired+" finalComplete==================="+finalComplete);
			if(isDSPQRequired==0 && isQQRequired==0)
				return false;
			else if(isDSPQRequired==1 && isQQRequired==0 && finalComplete==1)
				return false;
			else if(isDSPQRequired==0 && isQQRequired==1 && finalComplete==1)
				return false;
			else if(isDSPQRequired==1 && isQQRequired==1 && finalComplete==1)
				return false;
			else
				return true;
			}catch(Exception e){e.printStackTrace();return false;}
		}
		
		@Transactional(readOnly=false)	
		public StatusMaster findByTeacherIdJobStausForLoop(InternalTransferCandidates internalITRA,JobForTeacher jobForTeacher,Map<Integer, Integer> mapAssess,TeacherAssessmentStatus teacherAssessmentStatusJSI,Map<Integer, TeacherAssessmentStatus>mapJSI,DistrictMaster districtMasterInternal,TeacherPortfolioStatus tPortfolioStatus,Map<String, StatusMaster>mapStatus,TeacherAssessmentStatus teacherBaseAssessmentStatus) 
		{	
			System.out.println("findByTeacherIdJobStausForLoopfindByTeacherIdJobStausForLoopfindByTeacherIdJobStausForLoop");
			TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
			JobOrder jobOrder =  jobForTeacher.getJobId();
			StatusMaster statusMaster =  mapStatus.get("icomp");
			try{
				if(mapAssess!=null){
					Integer assessmentId=mapAssess.get(jobForTeacher.getJobId().getJobId());
					teacherAssessmentStatusJSI = mapJSI.get(assessmentId);
				}
				 int isAffilated=0;
				 boolean epiInternal=false;
				 int isPortfolio=0;
				 if(jobOrder.getIsPortfolioNeeded()!=null){
					 if(jobOrder.getIsPortfolioNeeded())
						 isPortfolio=1;
				 }
				int offerEPI=0;
				int offerJSI=0;
				int internalFlag=0;
				try{
					if(districtMasterInternal!=null)
					if(internalITRA!=null && districtMasterInternal.getDistrictId().equals(jobOrder.getDistrictMaster().getDistrictId())){
						internalFlag=1;
						if(districtMasterInternal.getOfferPortfolioNeeded()){
							isPortfolio=1;
						}else{
							isPortfolio=0;
						}
						if(districtMasterInternal.getOfferEPI()){
							offerEPI=1;
						}else{
							offerEPI=0;
						}
						if(districtMasterInternal.getOfferJSI()){
							offerJSI=1;
						}else{
							offerJSI=0;
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				try{
					 if(jobOrder.getJobCategoryMaster()!=null){
							if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()!=null){
								if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
									epiInternal=true;
								}
								if(jobForTeacher.getIsAffilated()!=null){
									 isAffilated=jobForTeacher.getIsAffilated();
								}
								if(internalFlag==0 && isAffilated==1){
									if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
										offerEPI=1;
									}else{
										offerEPI=0;
									}
									if(jobOrder.getJobCategoryMaster().getOfferJSI()!=0){
										offerJSI=1;
									}else{
										offerJSI=0;
									}
									if(jobOrder.getJobCategoryMaster().getOfferPortfolioNeeded()){
										isPortfolio=1;
									}else{
										isPortfolio=0;
									}
								}
							}
					 }
				}catch(Exception e){
					e.printStackTrace();
				}
				if(jobForTeacher.getIsAffilated()!=null){
					 if(epiInternal==false){
						 isAffilated=jobForTeacher.getIsAffilated();
					 }else{
						 isAffilated=0;
					 }
				}
				//JSI optional for internal principal and principal job category 
				if(jobOrder.getJobCategoryMaster()!=null){
	                if(jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Principal") && isAffilated==1){
	                    offerJSI=0;
	                }
	            }
				//System.out.println("Flags :"+isAffilated+" "+isPortfolio+" "+internalFlag+" "+offerEPI+" "+offerJSI+" ");
				if((teacherDetail.getIsPortfolioNeeded()) && tPortfolioStatus==null && internalFlag==0 && isPortfolio==1)
				{
					statusMaster = mapStatus.get("icomp");
				}else{
					if(isPortfolio==0 || (isPortfolio==1 && tPortfolioStatus!=null  && ((teacherDetail.getIsPortfolioNeeded()&& tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted()))))
					{
						statusMaster = mapStatus.get("comp");
						if(!jobOrder.getJobCategoryMaster().getBaseStatus()  || (internalFlag==1 && offerEPI==0 )||(internalFlag==0 && offerEPI==0 && isAffilated==1)){
							if(jobOrder.getIsJobAssessment() && ((offerJSI==1 && internalFlag==1 )||(internalFlag==0 && isAffilated==1 && offerJSI==1)|| (internalFlag==0 && isAffilated==0))){
								if(teacherAssessmentStatusJSI!=null){
									statusMaster=teacherAssessmentStatusJSI.getStatusMaster();
								}else{
									statusMaster = mapStatus.get("icomp");
								}
							}else{
								statusMaster = mapStatus.get("comp");
							}
						}                                                                        
						else if(jobOrder.getIsJobAssessment()&& ((offerJSI==1 && internalFlag==1 )|| (internalFlag==0 && isAffilated==1 && offerJSI==1)|| (internalFlag==0 && isAffilated==0))){
							/*============== Gagan : Job specific Inventory (JSI) Status is checking here : ==============*/
							if(teacherBaseAssessmentStatus!=null){
								if(teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
									if(teacherAssessmentStatusJSI!=null){
										statusMaster=teacherAssessmentStatusJSI.getStatusMaster();
									}else{
										statusMaster = mapStatus.get("icomp");
									}
								}else{
									statusMaster=teacherBaseAssessmentStatus.getStatusMaster();
								}
							}else{
								statusMaster = mapStatus.get("icomp");
							}
						}
						else{
							boolean  evDistrict=false;
							try{
								if(jobOrder.getDistrictMaster()!=null){
									if(jobOrder.getDistrictMaster().getDistrictId()==5513170){
										evDistrict=true;
									}
								}
							}catch(Exception e){}
							/*============== Gagan : Base Status is checking here : ==============*/
							if((isAffilated!=1 || evDistrict )&&((offerEPI==1 && internalFlag==1 )|| ( internalFlag==0 && isAffilated==0) || ( internalFlag==0 && isAffilated==1 && offerEPI==1))) //if block will execute only if Integer.parseInt(isAffilated) value is 0 or null
							{	
								if(teacherBaseAssessmentStatus!=null){
									statusMaster=teacherBaseAssessmentStatus.getStatusMaster();
								}else{
									statusMaster = mapStatus.get("icomp");
								}
							}
							else // isAffilated else block
							{
								statusMaster = mapStatus.get("comp");
							}
						}
					}
					else
					{
						statusMaster = mapStatus.get("icomp");
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			return statusMaster;

		}
		

		@Transactional(readOnly=true)
		public List<AssessmentDetail> findAssessmentByHeadQuarter(HeadQuarterMaster headQuarterMaster)
		{
			List<AssessmentDetail> assessmentDetails = null;
			try {
				Criterion criterion = Restrictions.eq("status", "A");
				Criterion criterion1 = Restrictions.eq("assessmentType", 2);
				Criterion criterion2 = Restrictions.eq("jobOrderType", 5);
				Criterion criterion3 = Restrictions.eq("headQuarterMaster", headQuarterMaster);
				Criterion criterion4 = Restrictions.isNull("branchMaster");
				assessmentDetails = findByCriteria(criterion,criterion1,criterion2,criterion3,criterion4);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return assessmentDetails;
		}
		
		
		@Transactional(readOnly=true)
		public List<AssessmentDetail> findAssessmentByHeadQuarterDefault(HeadQuarterMaster headQuarterMaster)
		{
			List<AssessmentDetail> assessmentDetails = null;
			try {
				Criterion criterion = Restrictions.eq("status", "A");
				Criterion criterion1 = Restrictions.eq("assessmentType", 2);
				Criterion criterion2 = Restrictions.eq("jobOrderType", 5);
				Criterion criterion3 = Restrictions.eq("headQuarterMaster", headQuarterMaster);
				Criterion criterion4 = Restrictions.isNull("branchMaster");
				Criterion criterion5 = Restrictions.eq("isDefault", true);
				assessmentDetails = findByCriteria(criterion,criterion1,criterion2,criterion3,criterion4,criterion5);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return assessmentDetails;
		}
		
		@Transactional(readOnly=true)
		public AssessmentDetail checkIsDefaultForHeadQuarter(HeadQuarterMaster headQuarterMaster, BranchMaster branchMaster,int defaultType,JobOrder jobOrder)
		{
			List<AssessmentDetail> assessmentDetailslst = new ArrayList<AssessmentDetail>();
			try {
				Criterion criterion = Restrictions.eq("status", "A");
				Criterion criterion1 = Restrictions.eq("isDefault",true);
				if(defaultType==5){
					Criterion criterion2 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
					Criterion criterion3 = Restrictions.eq("jobOrderType",5);
					Criterion criterion4 = Restrictions.isNull("jobCategoryMaster");
					assessmentDetailslst = findByCriteria(criterion,criterion1,criterion2,criterion3,criterion4);
				}/*else if(defaultType==6){
					Criterion criterion2 = Restrictions.eq("jobOrderType",6);
					Criterion criterion3 = Restrictions.eq("branchMaster",branchMaster);
					Criterion criterion4 = Restrictions.isNull("jobCategoryMaster");
					assessmentDetailslst = findByCriteria(criterion,criterion1,criterion3,criterion4);
				}*/
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(assessmentDetailslst.size()>0){
				return assessmentDetailslst.get(0);
			}else{
				return null;
			}
		}
		
		/**
		 * @author Amit Chaudhary
		 * @param assessmentGroupDetails
		 * @return assessmentDetailList
		 */
		@Transactional(readOnly=false)
		public List<AssessmentDetail> getAssessmentDetailListByGroup(AssessmentGroupDetails assessmentGroupDetails)
		{
			List<AssessmentDetail> assessmentDetailList = new ArrayList<AssessmentDetail>();
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(Restrictions.eq("assessmentGroupDetails", assessmentGroupDetails));
				criteria.add(Restrictions.eq("status", "A"));
				
				assessmentDetailList = criteria.list();
			}
			catch (Exception e){
				e.printStackTrace();
			}
			return assessmentDetailList;
		}
		
		@Transactional(readOnly=true)
		public List<AssessmentDetail> findAssessmentsByHeadQuarter(HeadQuarterMaster headQuarterMaster)
		{
			List<AssessmentDetail> assessmentDetails = null;
			try {
				
				Criterion criterion = Restrictions.eq("headQuarterMaster", headQuarterMaster);
				
				assessmentDetails = findByCriteria(criterion);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return assessmentDetails;
		}
		@Transactional(readOnly=true)
		public List<AssessmentDetail> findAssessmentsByDistrict(DistrictMaster districtMaster)
		{
			List<AssessmentDetail> assessmentDetails = null;
			try {
				
				Criterion criterion = Restrictions.eq("districtMaster", districtMaster);
				
				assessmentDetails = findByCriteria(criterion);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return assessmentDetails;
		}
		
		@Transactional(readOnly=true)
		public List<AssessmentDetail> findAssessmentsNameByAssessmentType(Integer assessmentType)
		{
			List<AssessmentDetail> assessmentDetails = null;
			try {
				Criterion criterion = Restrictions.eq("assessmentType", assessmentType);
				Criterion criterion1 = Restrictions.eq("status", "A");
				assessmentDetails = findByCriteria(criterion,criterion1);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return assessmentDetails;
		}
		
		@Transactional(readOnly=true)
		public List<AssessmentDetail> findAssessmentsNameByDistrictAndType(Integer assessmentType,DistrictMaster districtMaster)
		{
			//System.out.println("Assessment type=  "+assessmentType+"  District Id :::"+districtMaster.getDistrictId());
			List<AssessmentDetail> assessmentDetails = null;
			try {
				Criterion criterion = Restrictions.eq("assessmentType", assessmentType);
				Criterion criterion1 = Restrictions.eq("districtMaster", districtMaster);
				Criterion criterion2 = Restrictions.isNull("jobCategoryMaster");
				Criterion criterion3 = Restrictions.eq("isDefault",true);
				Criterion criterion4 = Restrictions.eq("status", "A");
				assessmentDetails = findByCriteria(criterion,criterion1,criterion2,criterion3,criterion4);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return assessmentDetails;
		}
		
		@Transactional(readOnly=true)
		public List<AssessmentDetail> checkDefaultEpi()
		{
			List<AssessmentDetail> assessmentDetailList = null;
			try {
				Criterion criterion1 = Restrictions.eq("assessmentType", 1);
				Criterion criterion2 = Restrictions.eq("isDefault",true);
				Criterion criterion3 = Restrictions.eq("status", "A");
				assessmentDetailList = findByCriteria(criterion1,criterion2,criterion3);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return assessmentDetailList;
		}
		@Transactional(readOnly=true)
		public List<AssessmentDetail> assessmentIdByJobCategory(List<DistrictMaster> distList,List<JobCategoryMaster> jobCateList)
		{
			List<AssessmentDetail> assessmentDetailList = null;
			try {				
				Criterion criterion1 = Restrictions.in("districtMaster", distList);
				Criterion criterion2 = Restrictions.in("jobCategoryMaster",jobCateList);				
				assessmentDetailList = findByCriteria(criterion1,criterion2);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return assessmentDetailList;
		}
}