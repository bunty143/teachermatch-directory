package tm.dao.assessment;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.PdpDetail;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Indra Jeet Singh
 * @Discription: PDP Detail DAO.
 */
public class PdpDetailDAO extends GenericHibernateDAO<PdpDetail, Integer>{

	public PdpDetailDAO() {
		super(PdpDetail.class);
	}

	@Transactional(readOnly=true)
	public List<PdpDetail> findAllPdpActiveList()
	{
		List<PdpDetail> pdpDetailList = null;
		try {
			Criterion criterion = Restrictions.eq("status", "A");
			pdpDetailList = findByCriteria(criterion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pdpDetailList;
	}
}
