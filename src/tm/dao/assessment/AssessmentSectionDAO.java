package tm.dao.assessment;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherAssessmentAttempt;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentQuestions;
import tm.bean.assessment.AssessmentSections;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Vishwanath Kumar
 * @Discription: AssessmentSection DAO.
 */
public class AssessmentSectionDAO extends GenericHibernateDAO<AssessmentSections, Integer> 
{
	public AssessmentSectionDAO() {
		super(AssessmentSections.class);
	}
	
/* @Author: Vishwanath Kumar
 * @Discription: It is used to check duplicate assessment section from database.
 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<AssessmentSections> checkDuplicateAssessmentSection(AssessmentDetail assessmentDetail,String sectionName,Integer sectionId) 
	{
		Session session = getSession();

		if(sectionId!=null)
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.eq("assessmentDetail", assessmentDetail)) 
			.add(Restrictions.eq("sectionName", sectionName)) 
			.add(Restrictions.not(Restrictions.in("sectionId",new Integer[]{sectionId}))) 
			.list();
			return result;

		}else
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.eq("assessmentDetail", assessmentDetail)) 
			.add(Restrictions.eq("sectionName", sectionName)) 
			.list();
			return result;
		}
		//System.out.println("Response:::::::::::: "+result);


	}
	/* @Author: Vishwanath Kumar
	 * @Discription: .
	 */
	@Transactional(readOnly=true)
	public List<AssessmentSections> getSectionsByAssessmentId(AssessmentDetail assessmentDetail)
	{
		List<AssessmentSections> assessmentSections = null;

		Criterion criterion = Restrictions.eq("assessmentDetail", assessmentDetail);
		assessmentSections = findByCriteria(criterion);

		return assessmentSections;	
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: .
	 */
	@Transactional(readOnly=true)
	public List<AssessmentSections> getRNDSectionsByAssessmentId(AssessmentDetail assessmentDetail)
	{
		List<AssessmentSections> assessmentSections = null;

		Criterion criterion = Restrictions.eq("assessmentDetail", assessmentDetail);
		Criterion criterion1 = Restrictions.sqlRestriction("1=1 order by rand()");
		//Restrictions.sqlRestriction("1=1 order by rand()");
		assessmentSections = findByCriteria(criterion,criterion1);

		return assessmentSections;	
	}
	
	
}
