package tm.dao.assessment;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentCompetencyScore;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.ScoreLookupMaster;
import tm.bean.master.CompetencyMaster;
import tm.dao.generic.GenericHibernateDAO;

public class AssessmentCompetencyScoreDAO extends GenericHibernateDAO<AssessmentCompetencyScore, Integer> 
{

	public AssessmentCompetencyScoreDAO() {
		super(AssessmentCompetencyScore.class);
	}

	@Transactional(readOnly=false)
	public List<AssessmentCompetencyScore> findAllCompetencyByTeacher(TeacherDetail teacherDetail,ScoreLookupMaster scoreLookupMaster,List<CompetencyMaster> competencyMasters)
	{
		List<AssessmentCompetencyScore> assessmentCompetencyScoreList = new ArrayList<AssessmentCompetencyScore>();
		try {
			
			Criterion criterion1 = Restrictions.eq("scoreLookupMaster",scoreLookupMaster);
			Criterion criterion2 = Restrictions.eq("teacherDetail",teacherDetail);
			if(competencyMasters!=null && competencyMasters.size()>0)
			{
				Criterion criterion3 = Restrictions.in("competencyMaster",competencyMasters);
				assessmentCompetencyScoreList = findByCriteria(criterion1,criterion2,criterion3);
			}else
				assessmentCompetencyScoreList = findByCriteria(criterion1,criterion2);

		} catch (Exception e) {
			e.printStackTrace();
		}		
		return assessmentCompetencyScoreList;
	} 
	
	/**
	 * @author Amit Chaudhary
	 * @param teacherDetailList
	 * @param assessmentDetail
	 * @param assessmentType
	 * @param assessmentTakenCount
	 * @return assessmentCompetencyScoreList
	 */
	@Transactional(readOnly=true)
	public List<AssessmentCompetencyScore> getAssessmentCompetencyScore(List<TeacherDetail> teacherDetailList,AssessmentDetail assessmentDetail,Integer assessmentType,Integer assessmentTakenCount)
	{
		List<AssessmentCompetencyScore> assessmentCompetencyScoreList = null;
		try {
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetailList);
			Criterion criterion2 = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion3 = Restrictions.eq("assessmentType", assessmentType);
			Criterion criterion4 = Restrictions.eq("assessmentTakenCount", assessmentTakenCount);
			
			assessmentCompetencyScoreList = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return assessmentCompetencyScoreList;
	}
	
	@Transactional(readOnly=false)
	public List<AssessmentCompetencyScore> findAllCompetencyByTAS(List<CompetencyMaster> competencyMasters,TeacherAssessmentStatus teacherAssessmentStatus)
	{
		List<AssessmentCompetencyScore> assessmentCompetencyScoreList = new ArrayList<AssessmentCompetencyScore>();
		try {
			
			Criterion criterion1 = Restrictions.eq("scoreLookupMaster",teacherAssessmentStatus.getAssessmentDetail().getScoreLookupMaster());
			Criterion criterion2 = Restrictions.eq("teacherDetail",teacherAssessmentStatus.getTeacherDetail());
			Criterion criterion3 = Restrictions.eq("assessmentDetail", teacherAssessmentStatus.getAssessmentDetail());
			Criterion criterion4 = Restrictions.eq("assessmentType", teacherAssessmentStatus.getAssessmentType());
			Criterion criterion5 = Restrictions.eq("assessmentTakenCount", teacherAssessmentStatus.getAssessmentTakenCount());
			if(competencyMasters!=null && competencyMasters.size()>0)
			{
				Criterion criterion6 = Restrictions.in("competencyMaster",competencyMasters);
				assessmentCompetencyScoreList = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion6);
			}else
				assessmentCompetencyScoreList = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);

		} catch (Exception e) {
			e.printStackTrace();
		}		
		return assessmentCompetencyScoreList;
	}
}
