package tm.dao.assessment;

import tm.bean.assessment.AssessmentTemplateMaster;
import tm.dao.generic.GenericHibernateDAO;

public class AssessmentTemplateMasterDAO  extends GenericHibernateDAO<AssessmentTemplateMaster, Integer> {
	
	public AssessmentTemplateMasterDAO() {
		super(AssessmentTemplateMaster.class);
	}

}
