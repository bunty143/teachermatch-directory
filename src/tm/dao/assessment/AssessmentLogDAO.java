package tm.dao.assessment;

import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import tm.bean.assessment.AssessmentLog;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;

public class AssessmentLogDAO extends GenericHibernateDAO<AssessmentLog, Long> 
{

	public AssessmentLogDAO() {
		super(AssessmentLog.class);
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to save Assessment Log.
	 */

	@Transactional(readOnly=false)	
	public boolean saveAssessmentLog(UserMaster userMaster,String actionPerformed,String assessment,String section,String question,String option)
	{
		AssessmentLog assessmentLog = new AssessmentLog();
		
		assessmentLog.setActionPerformed(actionPerformed);
		assessmentLog.setAssessment(assessment);
		assessmentLog.setCreatedDateTime(new Date());
		assessmentLog.setSection(section);
		assessmentLog.setQuestion(question);
		assessmentLog.setOptionName(option);
		assessmentLog.setUserMaster(userMaster);
		
		try {
			makePersistent(assessmentLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}
}
