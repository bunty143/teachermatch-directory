package tm.dao.assessment;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.assessment.InventoryInvitationsTemp;
import tm.dao.generic.GenericHibernateDAO;

public class InventoryInvitationsTempDAO extends GenericHibernateDAO<InventoryInvitationsTemp,Integer>
{
	public InventoryInvitationsTempDAO(){
		super(InventoryInvitationsTemp.class);
	}

	@Transactional(readOnly=false)
	public boolean deleteTeacherTemp(String sessionId)
	{
		try{
			Criterion criterion = Restrictions.eq("sessionId",sessionId);
        	List<InventoryInvitationsTemp> inventoryInvitationsTempList = findByCriteria(criterion);
        	for(InventoryInvitationsTemp inventoryInvitationsTemp: inventoryInvitationsTempList){
        		makeTransient(inventoryInvitationsTemp);
			}
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Transactional(readOnly=false)
	public boolean deleteAllTeacherTemp(String sessionId)
	{
		try{
			Calendar c = Calendar.getInstance(); 
			c.add(Calendar.DAY_OF_YEAR, -2);
			Date date = c.getTime();
			Criterion criterion1 = Restrictions.lt("date",date);
			Criterion criterion2 = Restrictions.eq("sessionId",sessionId);
			Criterion complete3 = Restrictions.or(criterion1,criterion2);
        	List<InventoryInvitationsTemp> inventoryInvitationsTempList = findByCriteria(complete3);
        	System.out.println("inventoryInvitationsTempList::::"+inventoryInvitationsTempList.size());
			for(InventoryInvitationsTemp inventoryInvitationsTemp: inventoryInvitationsTempList){
        		makeTransient(inventoryInvitationsTemp);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return true;
	}
	
	@Transactional(readOnly=true)
	public List<InventoryInvitationsTemp> findByTempTeacher(Order order,int startPos,int limit,Criterion... criterion)
	{
		List<InventoryInvitationsTemp> inventoryInvitationsTempList = null;
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			criteria.addOrder(order);
			inventoryInvitationsTempList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return inventoryInvitationsTempList;
	}
	
	@Transactional(readOnly=true)
	public List<InventoryInvitationsTemp> findByAllTempTeacher()
	{
		List<InventoryInvitationsTemp> inventoryInvitationsTempList = null;
		try{
			Session session = getSession();
			Calendar c = Calendar.getInstance(); // starts with today's date and time
			c.add(Calendar.DAY_OF_YEAR, -2);  // advances day by 2
			Date date = c.getTime();
			Criterion criterion=Restrictions.lt("date",date);
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion);
			inventoryInvitationsTempList = criteria.list();
		} 
		catch (Exception e){
			e.printStackTrace();
		}		
		return inventoryInvitationsTempList;
	}
	
	@Transactional(readOnly=true)
	public int getRowCountTempTeacher(Criterion... criterion)
	{
		List<InventoryInvitationsTemp> inventoryInvitationsTempList = null;
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			inventoryInvitationsTempList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return inventoryInvitationsTempList.size();
	}
}
