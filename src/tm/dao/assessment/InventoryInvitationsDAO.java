package tm.dao.assessment;

import tm.bean.assessment.InventoryInvitations;
import tm.dao.generic.GenericHibernateDAO;

public class InventoryInvitationsDAO extends GenericHibernateDAO<InventoryInvitations, Integer>
{
	public InventoryInvitationsDAO(){
		super(InventoryInvitations.class);
	}
}
