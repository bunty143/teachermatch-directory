package tm.dao.assessment;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.assessment.StageOneStatus;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Hanzala Subhani
 * @Discription: StatusStage1 DAO.
 */
public class StageOneStatusDAO extends GenericHibernateDAO<StageOneStatus, Integer> 
{
	public StageOneStatusDAO() {
		super(StageOneStatus.class);
	}

	@Transactional(readOnly=false)
	public List<StageOneStatus> getActiveStageOne(){
		List<StageOneStatus> stageOneStatus =  new ArrayList<StageOneStatus>();
		try{
			Criterion criterion = 	Restrictions.eq("status", "A");
			stageOneStatus		=	findByCriteria(Order.asc("name") ,criterion);

		} catch(Exception e){

		}
		return stageOneStatus;
	}
	
}
