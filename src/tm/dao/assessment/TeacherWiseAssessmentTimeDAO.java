package tm.dao.assessment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.assessment.TeacherWiseAssessmentTime;
import tm.dao.generic.GenericHibernateDAO;

public class TeacherWiseAssessmentTimeDAO extends GenericHibernateDAO<TeacherWiseAssessmentTime, Integer> 
{	
	public TeacherWiseAssessmentTimeDAO() {
		super(TeacherWiseAssessmentTime.class);
	}
	
	@Transactional(readOnly=true)
	public List<TeacherWiseAssessmentTime> findTeacherWiseAssessmentTime(TeacherDetail teacherDetail) 
	{
		List<TeacherWiseAssessmentTime> teacherWiseAssessmentTimes = new ArrayList<TeacherWiseAssessmentTime>();
		try {
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();

			Criterion criterion = Restrictions.eq("assessmentType", 1);
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			teacherWiseAssessmentTimes = findByCriteria(Order.desc("createdDateTime"),criterion,criterion1,criterion2);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacherWiseAssessmentTimes;

	}

	
}



