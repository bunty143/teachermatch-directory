package tm.dao.assessment;

import tm.bean.assessment.TemplateWiseStepMessage;
import tm.dao.generic.GenericHibernateDAO;

public class TemplateWiseStepMessageDAO extends GenericHibernateDAO<TemplateWiseStepMessage, Integer> {

	public TemplateWiseStepMessageDAO() {
		super(TemplateWiseStepMessage.class);
	} 
}
