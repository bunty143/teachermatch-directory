package tm.dao.assessment;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.assessment.QuestionsPool;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Vishwanath Kumar
 * @Discription: QuestionsPoolDAO.
 */
public class QuestionsPoolDAO extends GenericHibernateDAO<QuestionsPool, Integer> 
{
	public QuestionsPoolDAO() {
	 super(QuestionsPool.class);
	}
	
	@Transactional(readOnly=false)
	public void updateQuestionsCount(List<Integer> questionsPools){
		try {
			//String hql = "delete from DashboardEmailsToTeacher WHERE teacherId IN (:questionsPools)";
			String hql = "update QuestionsPool set questionTaken=questionTaken+1 WHERE questionId IN (:questionsPools)";
			//update questionspool set `questionTaken`=`questionTaken`+1
			Query query = null;
			Session session = getSession();

			query = session.createQuery(hql);
			query.setParameterList("questionsPools", questionsPools);
			System.out.println("  OOO  "+query.executeUpdate());
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int getLastQuestionsPoolId() 
	{
		int questionId = 0; 
		List<QuestionsPool> questionsPoolList = new ArrayList<QuestionsPool>();
		try {
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.addOrder(Order.desc("questionId"));
				criteria.setFirstResult(0);
				criteria.setMaxResults(1);
				questionsPoolList = criteria.list();
				
				if(questionsPoolList!=null && questionsPoolList.size()>0)
					questionId = questionsPoolList.get(0).getQuestionId();
				
		} catch (HibernateException e) {
				e.printStackTrace();
		}
		return questionId;
	}
	
	@Transactional(readOnly=true)
	public List<QuestionsPool> getQuestionByItemCodes(List<String> itemCodeList){
		List<QuestionsPool> questionsPoolList = new ArrayList<QuestionsPool>();
		try{
			Criterion criterion = Restrictions.in("itemCode", itemCodeList);
			questionsPoolList = findByCriteria(criterion);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return questionsPoolList;
	}
}


