package tm.dao;

import tm.bean.TempSchedule;
import tm.dao.generic.GenericHibernateDAO;

public class TempScheduleDAO extends GenericHibernateDAO<TempSchedule,Integer>{

	public TempScheduleDAO() {
		super(TempSchedule.class);
	}
}
