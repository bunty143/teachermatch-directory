package tm.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.TeacherDetail;
import tm.bean.TeacherProfileVisitHistory;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolMasterDAO;

public class TeacherProfileVisitHistoryDAO extends GenericHibernateDAO<TeacherProfileVisitHistory, Integer> 
{
	public TeacherProfileVisitHistoryDAO() 
	{
		super(TeacherProfileVisitHistory.class);
	}
	
	
	@Autowired
	DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	SchoolMasterDAO schoolMasterDAO;
	
	@Transactional(readOnly=false)
	public List<TeacherProfileVisitHistory> findTeacherProfileVisitHistoryByTeacher(TeacherDetail teacherDetail)
	{
		List<TeacherProfileVisitHistory> lstTeacherProfileVisitHistories =new ArrayList<TeacherProfileVisitHistory>();
		try 
		{
			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			lstTeacherProfileVisitHistories = findByCriteria(criterion);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherProfileVisitHistories;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherProfileVisitHistory> findTeacherProfileVisitHistoryByTeachers(Set<TeacherDetail> teacherDetailList)
	{
		List<TeacherProfileVisitHistory> lstTeacherProfileVisitHistories =new ArrayList<TeacherProfileVisitHistory>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetailList);	
			criteria.add(criterion1);
			criteria.addOrder(Order.asc("teacherDetail"));
			criteria.addOrder(Order.asc("userMaster"));
			lstTeacherProfileVisitHistories = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherProfileVisitHistories;
	}	
	
	/**************add by ankit*********/
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<String[]> getInfoTest(Integer districtId, int sortingcheck, Order sortOrderStrVal ,int start, int noOfRow,boolean report,Integer schoolId)
    {
          List<String[]> tpvh=new ArrayList<String[]>();
          try{
        	          	  
        	 // TeacherProfileVisitHistory
                Session session = getSession();                       
                Criteria criteria = session.createCriteria(getPersistentClass());             
                criteria.createAlias("userMaster", "um").createAlias("districtId", "dm").createAlias("schoolId", "sm",CriteriaSpecification.LEFT_JOIN)
              .setProjection( Projections.projectionList()
                              .add( Projections.property("dm.districtName"), "districtName" )
                              .add( Projections.property("sm.schoolName"), "schoolName" )
                              .add( Projections.property("um.firstName"), "firstName" )
                              .add( Projections.property("um.lastName"), "lastName" )
                              .add( Projections.property("um.title"), "title" )
                              .add( Projections.property("visitLocation"), "visitLocation" )
                              .add( Projections.count("visitId").as("vid")) 
                              .add( Projections.max("visitedDateTime"))
                              .add( Projections.min("visitedDateTime").as("lastDate"))
                              .add( Projections.groupProperty("userMaster"))
                              .add( Projections.groupProperty("visitLocation"))
                              .add( Projections.property("um.emailAddress"), "emailAddress" )
                           );
                if(districtId!=null && districtId!=0 && schoolId!=null && schoolId!=0 )
                {
                	criteria.add(Restrictions.and(Restrictions.eq("dm.districtId",districtId), Restrictions.eq("sm.schoolId",schoolId.longValue()) )  );
                }
                else if(districtId!=null && districtId!=0)
                {
                	  criteria.add(Restrictions.eq("dm.districtId",districtId));
                }
                             				
        			criteria.addOrder(sortOrderStrVal);
                
                long a = System.currentTimeMillis();
                tpvh = criteria.list() ;                  
                long b = System.currentTimeMillis();
               System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + tpvh.size() );
                        
                } 
          catch (Exception e) {
                e.printStackTrace();
          }
          return tpvh;
    }
	
	
	
	/*@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<String[]> getInfoTestAdmin(Integer districtId, int sortingcheck, Order sortOrderStrVal ,int start, int noOfRow,boolean report)
    {
          List<String[]> tpvh=new ArrayList<String[]>();
          try{
        	          	  
        	 // TeacherProfileVisitHistory
                Session session = getSession();                       
                Criteria criteria = session.createCriteria(getPersistentClass());             
                criteria.createAlias("userMaster", "um").createAlias("districtId", "dm").createAlias("schoolId", "sm",CriteriaSpecification.LEFT_JOIN)
              .setProjection( Projections.projectionList()
                              .add( Projections.property("dm.districtName"), "districtName" )
                              .add( Projections.property("sm.schoolName"), "schoolName" )
                              .add( Projections.property("um.firstName"), "firstName" )
                              .add( Projections.property("um.lastName"), "lastName" )
                              .add( Projections.property("um.title"), "title" )
                              .add( Projections.property("visitLocation"), "visitLocation" )
                              .add( Projections.count("visitId").as("vid")) 
                              .add( Projections.max("visitedDateTime"))
                              .add( Projections.min("visitedDateTime").as("lastDate"))
                             .add( Projections.groupProperty("userMaster"))
                              .add( Projections.groupProperty("visitLocation"))
                              .add( Projections.property("um.emailAddress"), "emailAddress" )
                           );
                //criteria.add(Restrictions.eq("dm.districtId",districtId));
                if(districtId!=null )
    				criteria.add(Restrictions.eq("dm.districtId",districtId));
                criteria.addOrder(sortOrderStrVal);
                
                long a = System.currentTimeMillis();
                tpvh = criteria.list() ;                  
                long b = System.currentTimeMillis();
               System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + tpvh.size() );
               if(tpvh.size()>0){
                   for (Iterator it = tpvh.iterator(); it.hasNext();) {
                   Object[] row = (Object[]) it.next()       ;                                                                 
                  //System.out.println(""+row[0]+""+row[1]);
               }               
              }
             
                } 
          catch (Exception e) {
                e.printStackTrace();
          }
          return tpvh;
    }
		
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<String[]> getSearchedData(Integer districtId, Integer schoolId,int sortingcheck, Order sortOrderStrVal ,int start, int noOfRow,boolean report)
    {
          List<String[]> tpvh=new ArrayList<String[]>();
          try{
        	  
        	  
        	 // TeacherProfileVisitHistory
                Session session = getSession();                       
                Criteria criteria = session.createCriteria(getPersistentClass());             
                criteria.createAlias("userMaster", "um").createAlias("districtId", "dm").createAlias("schoolId", "sm",CriteriaSpecification.LEFT_JOIN)
              .setProjection( Projections.projectionList()
                              .add( Projections.property("dm.districtName"), "districtName" )
                              .add( Projections.property("sm.schoolName"), "schoolName" )
                              .add( Projections.property("um.firstName"), "firstName" )
                              .add( Projections.property("um.lastName"), "lastName" )
                              .add( Projections.property("um.title"), "title" )
                              .add( Projections.property("visitLocation"), "visitLocation" )
                              .add( Projections.count("visitId").as("vid")) 
                              .add( Projections.max("visitedDateTime"))
                              .add( Projections.min("visitedDateTime").as("lastDate"))
                             .add( Projections.groupProperty("userMaster"))
                              .add( Projections.groupProperty("visitLocation"))
                              .add( Projections.property("um.emailAddress"), "emailAddress" )
                           );
                
                criteria.add(Restrictions.and(Restrictions.eq("dm.districtId",districtId), Restrictions.eq("sm.schoolId",schoolId.longValue()) )  );
                				
        			criteria.addOrder(sortOrderStrVal);
                
                
                long a = System.currentTimeMillis();
                tpvh = criteria.list() ;                  
                long b = System.currentTimeMillis();
              // System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + tpvh.size() );
               if(tpvh.size()>0){
                   for (Iterator it = tpvh.iterator(); it.hasNext();) {
                   Object[] row = (Object[]) it.next()       ;                                                                 
                  //System.out.println(""+row[0]+""+row[1]);
               }               
              }
             
                } 
          catch (Exception e) {
                e.printStackTrace();
          }
          return tpvh;
    }*/
	/**************add by ankit end*********/
	
	
	
}
