package tm.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.Order;

import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;


import tm.bean.DashboardEmailsToTeacher;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.dao.generic.GenericHibernateDAO;

public class DashboardEmailsToTeacherDAO extends GenericHibernateDAO<DashboardEmailsToTeacher, Integer> 
{
	public DashboardEmailsToTeacherDAO() {
		super(DashboardEmailsToTeacher.class);
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to findMailSentToTeachers.
	 */
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> findMailSentToTeachers(List<TeacherDetail> teacherDetails) 
	{
		
		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		
		List result;
		Map<Integer, Integer> dashboardEmailsToTeacherMap = null;
		try {
			Session session = getSession();

			Criterion criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion2 = Restrictions.in("teacherDetail",teacherDetails);
			
			result = session.createCriteria(getPersistentClass()) 
			.add(criterion1)
			.add(criterion2)
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherDetail"))
					.add(Projections.count("teacherDetail"))  
			).list();

			dashboardEmailsToTeacherMap = new HashMap<Integer, Integer>();
			TeacherDetail teacherDetail = null;

			for (Object object : result) {
				
					Object rows[] = (Object[])object;
					teacherDetail=((TeacherDetail)rows[0]);
					dashboardEmailsToTeacherMap.put(teacherDetail.getTeacherId(), (Integer)rows[1]);
					//System.out.println("Response:::::::::::: "+result.size());
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return dashboardEmailsToTeacherMap;
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Map<TeacherDetail,DashboardEmailsToTeacher> findMailSentToTeachersNotAuthenticated(List<TeacherDetail> teacherDetails) 
	{

		Calendar cal = Calendar.getInstance();

		cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
		Date sDate=cal.getTime();
		cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
		Date eDate=cal.getTime();
		
		List<DashboardEmailsToTeacher>  dashboardEmailsToTeacherList =null;
		 
	     try {
	   
	    	 	Criterion criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
				Criterion criterion2 = Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion3 = Restrictions.eq("teacherStatus","NotAuthenticated");
				
				dashboardEmailsToTeacherList = findByCriteria(org.hibernate.criterion.Order.asc("teacherDetail"),criterion1,criterion2,criterion3);
				Map<TeacherDetail, DashboardEmailsToTeacher> teacherEmailMap = new HashMap<TeacherDetail, DashboardEmailsToTeacher>();
				TeacherDetail teacher= null;
				Integer i = 0;
				Integer j = 0;
				for (DashboardEmailsToTeacher dashboardEmailsToTeacher : dashboardEmailsToTeacherList) {
					teacher = dashboardEmailsToTeacher.getTeacherDetail();
					if(i==0 || i==teacher.getTeacherId())
						j++;
					else
						j=1;
					//System.out.println("j: "+j);
					if(j==3)
					teacherEmailMap.put(teacher, dashboardEmailsToTeacher);
					i=teacher.getTeacherId();
				}
				/*System.out.println("si "+teacherEmailMap.size());
				teacher.setTeacherId(6);
				System.out.println(" Sysout:  "+teacherEmailMap.get(teacher).getMailId());*/
				
				return teacherEmailMap;
				
	     }catch (Exception e) {
	    	 e.printStackTrace();
	     }
		return null;
	
		
	}
	@Transactional(readOnly=true)
	public Map<TeacherDetail, DashboardEmailsToTeacher> getMailsByTypeAndStatusWithoutDateRange(List<TeacherDetail> teacherDetails, String type,String teacherStatus)
	{
		 List<DashboardEmailsToTeacher>  dashboardEmailsToTeacherList =null;
		 Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			
		 
	     try {
	    	 	Criterion criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
				Criterion criterion2 = Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion4 = Restrictions.eq("teacherStatus",teacherStatus);
				dashboardEmailsToTeacherList = findByCriteria(criterion1,criterion2,criterion4);
				
				Map<TeacherDetail, DashboardEmailsToTeacher> teacherEmailMap = new HashMap<TeacherDetail, DashboardEmailsToTeacher>();
				
				for (DashboardEmailsToTeacher dashboardEmailsToTeacher : dashboardEmailsToTeacherList) {
					teacherEmailMap.put(dashboardEmailsToTeacher.getTeacherDetail(), dashboardEmailsToTeacher);
				}
				
				return teacherEmailMap;
				
	     }catch (Exception e) {
	    	 e.printStackTrace();
	     }
		return null;
	}
	
	@Transactional(readOnly=true)
	public Map<TeacherDetail, DashboardEmailsToTeacher> getMailsByTypeAndStatus(List<TeacherDetail> teacherDetails, String type,String teacherStatus)
	{
		 List<DashboardEmailsToTeacher>  dashboardEmailsToTeacherList =null;
		 
		 
	     try {
	    	 	Calendar cal = Calendar.getInstance();

				cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
				Date sDate=cal.getTime();
				cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
				Date eDate=cal.getTime();
				
				Criterion criterion1 = Restrictions.between("createdDateTime", sDate, eDate);
				Criterion criterion2 = Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion3 = Restrictions.eq("teacherStatus",teacherStatus);
				
				dashboardEmailsToTeacherList = findByCriteria(criterion1,criterion2,criterion3);
				
				Map<TeacherDetail, DashboardEmailsToTeacher> teacherEmailMap = new HashMap<TeacherDetail, DashboardEmailsToTeacher>();
				
				for (DashboardEmailsToTeacher dashboardEmailsToTeacher : dashboardEmailsToTeacherList) {
					teacherEmailMap.put(dashboardEmailsToTeacher.getTeacherDetail(), dashboardEmailsToTeacher);
				}
				
				
				return teacherEmailMap;
				
	     }catch (Exception e) {
	    	 e.printStackTrace();
	     }
		return null;
	}
	
	@Transactional(readOnly=true)
	public List<DashboardEmailsToTeacher> getSubGroup(String groupCode)
    {
     List<DashboardEmailsToTeacher>  subGroupList=null;
     try {
		Session session = getSession();
		 //subGroupList=  session.createCriteria(DashboardEmailsToTeacher.class).setFetchMode("TeacherDetail", FetchMode.JOIN).add(Restrictions.eq("groupCode", groupCode)).list();
		 //subGroupList=  session.createCriteria(DashboardEmailsToTeacher.class).setFetchMode("TeacherDetail", FetchMode.JOIN).list();
		// String sqlQuery = "select distinct ccm.CompltCode,ccm.CompltDesc from TVehFamilyGroupLink fgl,TCompltCodeMaster ccm where ccm.GroupCode=fgl.GroupCode and fgl.GroupCode='" + groupCode + "' order by ccm.CompltDesc ";
		 //System.out.println("Size: "+subGroupList.size());
		return subGroupList;
		 
		 /*String queryText = "select teachers.*, emails.* from TeacherDetail teachers left join DashboardEmailsToTeachers emails on (teachers.teacherId=emails.teacherId) where status = 'A' and verificationStatus=0";

		 	List<Object[]> rows = session.createSQLQuery(queryText)
		           .addEntity("teachers", TeacherDetail.class)
		           .addEntity("emails", DashboardEmailsToTeacher.class)
		           .list();
		 	
		 	System.out.println("llllllllllllllllllllllllllllllllllllllllllll "+rows.size());
		 	System.out.println("llllllllllllllllllllllllllllllllllllllllllll "+rows.get(0).getClass());
		 	System.out.println("llllllllllllllllllllllllllllllllllllllllllll "+rows.get(0)[0]);
		 	System.out.println("llllllllllllllllllllllllllllllllllllllllllll "+rows.get(0)[1]);*/
	} catch (Exception e) {
		e.printStackTrace();
	}
     	return null;
    }
	
	@Transactional(readOnly=false)
	public boolean saveDashboardMail(TeacherDetail teacherDetail,String subject,String mailText,String emailType,String teacherStatus)
	{
		try {
			
			DashboardEmailsToTeacher dashboardEmailsToTeacher = new DashboardEmailsToTeacher();
			dashboardEmailsToTeacher.setCreatedDateTime(new Date());
			dashboardEmailsToTeacher.setEmailAddress(teacherDetail.getEmailAddress());
			dashboardEmailsToTeacher.setSubject(subject);
			dashboardEmailsToTeacher.setMailText(mailText);
			dashboardEmailsToTeacher.setEmailType(emailType);
			dashboardEmailsToTeacher.setTeacherStatus(teacherStatus);
			dashboardEmailsToTeacher.setTeacherDetail(teacherDetail);
			Session session = getSession();
			
			session.saveOrUpdate(dashboardEmailsToTeacher);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	@Transactional(readOnly=true)
	public List<DashboardEmailsToTeacher> getMailsByTeachers(List<TeacherDetail> teacherDetails)
	{
		 List<DashboardEmailsToTeacher>  dashboardEmailsToTeacherList =null;
		 
	     try {
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				dashboardEmailsToTeacherList = findByCriteria(org.hibernate.criterion.Order.asc("teacherDetail") ,criterion1);
								
				return dashboardEmailsToTeacherList;
				
	     }catch (Exception e) {
	    	 e.printStackTrace();
	     }
		return null;
	}
	
	@Transactional(readOnly=false)
	public void deleteFromDashboradEmailsToTeacher(List<TeacherDetail> teacherDetails){
		try {
			String hql = "delete from DashboardEmailsToTeacher WHERE teacherId IN (:teacherDetails)";
			Query query = null;
			Session session = getSession();

			query = session.createQuery(hql);
			query.setParameterList("teacherDetails", teacherDetails);
			System.out.println("  OOO  "+query.executeUpdate());
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}
}
