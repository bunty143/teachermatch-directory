package tm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EligibilityStatusMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.EligibilityMaster;
import tm.dao.generic.GenericHibernateDAO;

public class EligibilityStatusMasterDAO extends GenericHibernateDAO<EligibilityStatusMaster, Integer> 
{
	public EligibilityStatusMasterDAO() 
	{
		super(EligibilityStatusMaster.class);
	}
	
	@Transactional(readOnly=false)
	public Map<Integer, EligibilityStatusMaster> getEligibilityStatusMap()
	{
		Map<Integer, EligibilityStatusMaster> esMap = new HashMap<Integer, EligibilityStatusMaster>();
		List<EligibilityStatusMaster> eligibilityStatsMasters= new ArrayList<EligibilityStatusMaster>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("status","A");
			eligibilityStatsMasters = findByCriteria(criterion1);	
			
			for (EligibilityStatusMaster eligibilityStatusMaster : eligibilityStatsMasters) {
				esMap.put(eligibilityStatusMaster.getEligibilityStatusId(), eligibilityStatusMaster);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return esMap;
	}
	
	@Transactional(readOnly=false)
	public List<EligibilityStatusMaster> getEligibleStatusByDistrict(DistrictMaster districtMaster)
	{
		List<EligibilityStatusMaster> statusList = new ArrayList<EligibilityStatusMaster>();
		try
		{
			Criterion criterion = Restrictions.eq("districtId",districtMaster);
			Criterion criterion1 = Restrictions.eq("status","A");
			statusList = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusList;
	}
	
	@Transactional(readOnly=false)
	public List<EligibilityStatusMaster> getEligibleStatusDefault()
	{
		List<EligibilityStatusMaster> statusList = new ArrayList<EligibilityStatusMaster>();
		try
		{
			Criterion criterion = Restrictions.isNull("districtId");
			Criterion criterion1 = Restrictions.eq("status","A");
			statusList = findByCriteria(criterion,criterion1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusList;
	}
	
	@Transactional(readOnly=false)
	public EligibilityStatusMaster getESMasterByEId_DIdAndSName(EligibilityMaster em, DistrictMaster dm, String statusName)
	{
		EligibilityStatusMaster objESM = null;
		try
		{
			Session session=getSession();
			Criteria criteria=session.createCriteria(EligibilityStatusMaster.class)
			.setProjection( Projections.projectionList()
	        .add( Projections.property("eligibilityStatusId"), "eligibilityStatusId" )
		    ).add(Restrictions.eq("districtId", dm)).add(Restrictions.eq("eligibilityMaster", em)).add(Restrictions.eq("statusName", statusName));
			List<Integer> listColoumn=criteria.list();
			if(listColoumn.size()>0){
				objESM=new EligibilityStatusMaster();
				objESM.setEligibilityStatusId(listColoumn.get(0));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objESM;
	}

}

