package tm.controller.i4;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.i4.I4QuestionSets;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.i4.I4QuestionSetsDAO;
import tm.dao.master.DistrictMasterDAO;

@Controller
public class I4Controller 
{
	
	@Autowired
	private I4QuestionSetsDAO i4QuestionSetsDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	
	@RequestMapping(value="/virtualvideoquestions.do", method=RequestMethod.GET)
	public String I4QuestionsGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		UserMaster userSession	=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userSession		=	(UserMaster) session.getAttribute("userMaster");
		}
		
		
		String districtName = "";
		String districtId = "";
		
		if(userSession.getEntityType()==2)
		{
			districtId = userSession.getDistrictId().getDistrictId().toString();
			districtName = userSession.getDistrictId().getDistrictName();
		}
		
		System.out.println(" userSession.getEntityType() :: "+userSession.getEntityType());
		
		map.addAttribute("entityType",userSession.getEntityType());
		map.addAttribute("districtId",districtId);
		map.addAttribute("districtName",districtName);
		
		return "i4questions";
	}
	
	
	@RequestMapping(value="/virtualvideoquestionsSet.do", method=RequestMethod.GET)
	public String I4QuestionsSetGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		UserMaster userSession	=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userSession		=	(UserMaster) session.getAttribute("userMaster");
		}
		
		String districtName = "";
		String districtId = "";
		
		if(userSession.getEntityType()==2)
		{
			districtId = userSession.getDistrictId().getDistrictId().toString();
			districtName = userSession.getDistrictId().getDistrictName();
		}
		
		System.out.println(" userSession.getEntityType() :: "+userSession.getEntityType());
		
		map.addAttribute("entityType",userSession.getEntityType());
		map.addAttribute("districtId",districtId);
		map.addAttribute("districtName",districtName);
		
		return "i4questionsset";
	}
	
	@RequestMapping(value="/virtualvideoquestionSetQuestion.do", method=RequestMethod.GET)
	public String I4questionSetQuestionSetGET(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		HttpSession session = request.getSession(false);
		UserMaster userSession	=null;
		boolean notAccesFlag=false;
		DistrictMaster dMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userSession		=	(UserMaster) session.getAttribute("userMaster");
			if(userSession!=null){
				dMaster=userSession.getDistrictId();
			}

		}
		try
		{
			I4QuestionSets i4QuestionSets = new I4QuestionSets();
			DistrictMaster districtMaster = new DistrictMaster();
			
			
			
			int quesSetId=0;
			if(request.getParameter("quesSetId")!=null){
				quesSetId=Integer.parseInt(request.getParameter("quesSetId"));
				i4QuestionSets = i4QuestionSetsDAO.findById(quesSetId, false, false);
			
			}
			try {
				if(i4QuestionSets!=null  && !userSession.getEntityType().equals(1) ){
					if(i4QuestionSets.getDistrictMaster()!=null && !i4QuestionSets.getDistrictMaster().getDistrictId().equals(dMaster.getDistrictId())){
						notAccesFlag=true;
					}
				}
				if(notAccesFlag)
				{
					PrintWriter out = response.getWriter();
					response.setContentType("text/html"); 
					out.write("<script type='text/javascript'>");
					out.write("alert('You have no access to this virtual video question(s).');");
					out.write("window.location.href='virtualvideoquestionsSet.do';");
					out.write("</script>");
					return null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				if(i4QuestionSets!=null  && !userSession.getEntityType().equals(1) ){
					if(i4QuestionSets.getDistrictMaster()!=null && !i4QuestionSets.getDistrictMaster().getDistrictId().equals(dMaster.getDistrictId())){
						notAccesFlag=true;
					}
				}
				if(notAccesFlag)
				{
					PrintWriter out = response.getWriter();
					response.setContentType("text/html"); 
					out.write("<script type='text/javascript'>");
					out.write("alert('You have no access to this virtual video question(s).');");
					out.write("window.location.href='virtualvideoquestionsSet.do';");
					out.write("</script>");
					return null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			Integer districtID = 0;
			if(request.getParameter("districtId")!=null)
			{
				districtID = Integer.parseInt(request.getParameter("districtId"));
				districtMaster = districtMasterDAO.findById(districtID, false, false);
			}
			
			map.addAttribute("quesSetId", quesSetId);
			map.addAttribute("distID", districtMaster.getDistrictId());
			map.addAttribute("quesSetName", i4QuestionSets.getQuestionSetText());
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return "i4questionssetques";
	}
	
	
}
