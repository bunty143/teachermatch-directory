package tm.controller.user;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolKeyContact;
import tm.bean.master.SchoolMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolKeyContactDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.utility.ImageResize;
import tm.utility.Utility;


@Controller
public class UserController 
{
	String locale = Utility.getValueOfPropByKey("locale");
	@Autowired
	private UserMasterDAO usermasterdao;
	public void setUsermasterdao(UserMasterDAO usermasterdao) {
		this.usermasterdao = usermasterdao;
	}
	
	@Autowired
	private  RoleMasterDAO roleMasterDAO;
	public void setRoleMasterDAO(RoleMasterDAO roleMasterDAO) {
		this.roleMasterDAO = roleMasterDAO;
	}
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) {
		this.districtKeyContactDAO = districtKeyContactDAO;
	}
	@Autowired
	private SchoolKeyContactDAO schoolKeyContactDAO;
	public void setSchoolKeyContactDAO(SchoolKeyContactDAO schoolKeyContactDAO) {
		this.schoolKeyContactDAO = schoolKeyContactDAO;
	}
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	// by hafeez
	
	@Autowired 
	private ContactTypeMasterDAO contactTypeMasterDAO;
	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}
	//
	
	@RequestMapping(value="/manageuser.do", method=RequestMethod.GET)
	public String doManageUserGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		manageuserMethod(map,request,usermasterdao,roleAccessPermissionDAO);
		try{
			DistrictKeyContact userKeyContact=null;
			UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
			List<DistrictKeyContact>	districtKeyContactList=districtKeyContactDAO.findByContactType(userMaster, "Manage Users");
			if(districtKeyContactList!=null && districtKeyContactList.size()>0){
				userKeyContact=districtKeyContactList.get(0);
			}
			map.addAttribute("userKeyContact", userKeyContact);
		}catch (Exception e) {
			e.printStackTrace();
		}
		/*String userId = request.getParameter("userId")		==	null?"0":request.getParameter("userId").trim();
		String insertUpdateCheck = request.getParameter("insertUpdateCheck")==	null?"0":request.getParameter("insertUpdateCheck").trim();
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		int roleId=0;
		// =============== for Getting the All database Field Values from database ====================
		 List<UserMaster> beans = usermasterdao.findAll();
			for(UserMaster d: beans) // For Printing All Rows from Database
			{
				d.getFirstName(); // For Printing First Name from Database
				d.getRoleId();
			}
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			String roleAccess=null;
			try{
				if(roleId==10 || roleId==11)
					roleAccess=roleAccessPermissionDAO.getMenuOptionListByMenuId(roleId,96,"manageuser.do",1,100);
				else
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,1,"manageuser.do",0);
				
				System.out.println(" roleAccess :: "+roleAccess);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
		try 
		{
			map.addAttribute("beans", beans);
			map.addAttribute("userMaster", userMaster);	
			map.addAttribute("userId", userId);	
			map.addAttribute("insertUpdateCheck", insertUpdateCheck);	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("user entity type===="+userMaster.getEntityType());
		if(userMaster.getEntityType().toString().equalsIgnoreCase("1") || userMaster.getEntityType().toString().equalsIgnoreCase("5") || userMaster.getEntityType().toString().equalsIgnoreCase("6")){
			try{
			map.addAttribute("selectedRoleCombo", userMaster.getEntityType());
			map.addAttribute("roleCombo", getComboForHeadQuarterOrBranch(userMaster));
			if(!userMaster.getEntityType().toString().equalsIgnoreCase("1"))
			map.addAttribute("isBracnchExists",(userMaster.getHeadQuarterMaster().getIsBranchExist()==null?"0":(userMaster.getHeadQuarterMaster().getIsBranchExist()?"1":"0")));
			//return "manageuserhq";
			}catch(Exception e){e.printStackTrace();}
		}*/
		return "manageuser";
	}
	private static Map<Integer,String> getComboForHeadQuarterOrBranch(UserMaster userMaster){
		System.out.println("usermaster=========pp======="+userMaster.getEntityType());
		Map<Integer,String> comboRole=new LinkedHashMap<Integer, String>();
		if(userMaster.getEntityType().toString().equalsIgnoreCase("5")){
			comboRole.put(5, "Headquarter");
			if(userMaster.getHeadQuarterMaster().getIsBranchExist()){				
				comboRole.put(6, "Branch");
			}
			else{				
				comboRole.put(2, "District");
			}
			//comboRole.put(2, "District");
			//comboRole.put(3,"School");
		}
		if(userMaster.getEntityType().toString().equalsIgnoreCase("6")){
			comboRole.put(6, "Branch");			
			//comboRole.put(2, "District");
			//comboRole.put(3,"School");
		}
		if(userMaster.getEntityType().toString().equalsIgnoreCase("1")){
			comboRole.put(5, "Head Quarter");
			comboRole.put(6, "Branch");						
		}
		System.out.println("size=------------>>>::"+comboRole.size());
		return comboRole;
	}
	
	@RequestMapping(value="/addedituser.do", method=RequestMethod.GET)
	public String doAddEditManageUserGET(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		try 
		{
		HttpSession session = request.getSession(false);
		boolean notAccesFlag=false;
		DistrictMaster dMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		dMaster=userSession.getDistrictId();
		Utility utility			=	new Utility();
		UserMaster userMaster							=	null;
		String userId = request.getParameter("userId")	==	null?"0":request.getParameter("userId").trim();
		String authenticationCode						=	utility.getUniqueCodebyId((Integer.parseInt(userId)+1));
		int roleId=0;
		if(userId.equals("0"))
		{
			userMaster			=	new UserMaster();
			map.addAttribute("authenticationCode", authenticationCode);
		}
		else
		{
			userMaster=	usermasterdao.findById(Integer.parseInt(request.getParameter("userId")), false, false);
		    if(userSession.getEntityType()==2){
				if(!userMaster.getDistrictId().getDistrictId().equals(dMaster.getDistrictId())){
					notAccesFlag=true;
				}
			}
		}
	    if(notAccesFlag)
		{
			PrintWriter out = response.getWriter();
			response.setContentType("text/html"); 
			out.write("<script type='text/javascript'>");
			out.write("alert('You have no access to this user.');");
			out.write("window.location.href='manageuser.do';");
			out.write("</script>");
			return null;
		}
		
		if(userSession.getRoleId().getRoleId()!=null){
			roleId=userSession.getRoleId().getRoleId();
		}
		String roleAccess=null;
		try{
			if(roleId==10 || roleId==11)
				roleAccess=roleAccessPermissionDAO.getMenuOptionListByMenuId(roleId,138,"addedituser.do",0,146);
			else
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,2,"addedituser.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		map.addAttribute("roleAccess", roleAccess);
		// end 
		// =============== for Getting the All database Field Values from database ====================
	
			Criterion criterion					=	Restrictions.eq("entityType", userSession.getEntityType());
			Criterion criterion1				=	Restrictions.eq("status", "A");
			List<RoleMaster> roleMaster			=	roleMasterDAO.findByCriteria(Order.asc("roleName"),criterion,criterion1);
			String SchoolOrDistrictName=null;
			String onlyDistrictName=null;
			String districtOrSchooHiddenlId="";
			
			String headQuarterId="";
			String branchId="";
			String headQuarterHiddenId="";
			String branchHiddenId="";
			String isBracnchExists="0";
			String districtHiddenId="";
			
			if(userMaster.getEntityType()!=null){
				if(userMaster.getEntityType()==2&& userMaster.getDistrictId()!=null){
					if(userMaster.getHeadQuarterMaster()!=null){
					headQuarterId=userMaster.getHeadQuarterMaster().getHeadQuarterName();					
					headQuarterHiddenId=userMaster.getHeadQuarterMaster().getHeadQuarterId().toString();
					Boolean isExistsBranch=userMaster.getHeadQuarterMaster().getIsBranchExist();
					isBracnchExists=(isExistsBranch==null?"0":(isExistsBranch?"1":"0"));
					}
					if(userMaster.getBranchMaster()!=null){
					branchId=userMaster.getBranchMaster().getBranchName();
					branchHiddenId=userMaster.getBranchMaster().getBranchId().toString();
					}
					districtHiddenId=userMaster.getDistrictId().getDistrictId().toString();
					onlyDistrictName=userMaster.getDistrictId().getDistrictName();
					districtOrSchooHiddenlId=userMaster.getDistrictId().getDistrictId()+"";
					SchoolOrDistrictName=getSchoolOrDistrictName(userMaster.getEntityType(),userMaster.getDistrictId().getDistrictId());
				}else if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null){
					if(userMaster.getHeadQuarterMaster()!=null){
						headQuarterId=userMaster.getHeadQuarterMaster().getHeadQuarterName();					
						headQuarterHiddenId=userMaster.getHeadQuarterMaster().getHeadQuarterId().toString();
						Boolean isExistsBranch=userMaster.getHeadQuarterMaster().getIsBranchExist();
						isBracnchExists=(isExistsBranch==null?"0":(isExistsBranch?"1":"0"));
						}
						if(userMaster.getBranchMaster()!=null){
						branchId=userMaster.getBranchMaster().getBranchName();
						branchHiddenId=userMaster.getBranchMaster().getBranchId().toString();
						}
					districtOrSchooHiddenlId=userMaster.getSchoolId().getSchoolId()+"";					
					SchoolOrDistrictName=getSchoolOrDistrictName(userMaster.getEntityType(),Integer.parseInt(""+userMaster.getSchoolId().getSchoolId()));
					districtHiddenId=userMaster.getDistrictId().getDistrictId().toString();					
					onlyDistrictName=getSchoolOrDistrictName(2,Integer.parseInt(""+userMaster.getDistrictId().getDistrictId()));
				}
				else if(userMaster.getEntityType()==5 && userMaster.getHeadQuarterMaster()!=null){
					headQuarterId=userMaster.getHeadQuarterMaster().getHeadQuarterName()+"";					
					headQuarterHiddenId=userMaster.getHeadQuarterMaster().getHeadQuarterId().toString();
					Boolean isExistsBranch=userMaster.getHeadQuarterMaster().getIsBranchExist();
					isBracnchExists=(isExistsBranch==null?"0":(isExistsBranch?"1":"0"));					
				}
				else if(userMaster.getEntityType()==6 && userMaster.getBranchMaster()!=null){
					headQuarterId=userMaster.getHeadQuarterMaster().getHeadQuarterName();					
					headQuarterHiddenId=userMaster.getHeadQuarterMaster().getHeadQuarterId().toString();
					Boolean isExistsBranch=userMaster.getHeadQuarterMaster().getIsBranchExist();
					isBracnchExists=(isExistsBranch==null?"0":(isExistsBranch?"1":"0"));
					branchId=userMaster.getBranchMaster().getBranchName();
					branchHiddenId=userMaster.getBranchMaster().getBranchId().toString();
				}
			}else{
				if(userSession.getEntityType()==2 && userSession.getDistrictId()!=null){
					if(userSession.getHeadQuarterMaster()!=null){
						headQuarterId=userSession.getHeadQuarterMaster().getHeadQuarterName();					
						headQuarterHiddenId=userSession.getHeadQuarterMaster().getHeadQuarterId().toString();
						Boolean isExistsBranch=userSession.getHeadQuarterMaster().getIsBranchExist();
						isBracnchExists=(isExistsBranch==null?"0":(isExistsBranch?"1":"0"));
						}
						if(userSession.getBranchMaster()!=null){
						branchId=userSession.getBranchMaster().getBranchName();
						branchHiddenId=userSession.getBranchMaster().getBranchId().toString();
						}
					onlyDistrictName=userSession.getDistrictId().getDisplayName();
					districtHiddenId=userSession.getDistrictId().getDistrictId().toString();
					districtOrSchooHiddenlId=userSession.getDistrictId().getDistrictId()+"";
					SchoolOrDistrictName=getSchoolOrDistrictName(userSession.getEntityType(),userSession.getDistrictId().getDistrictId());
				}else if(userSession.getEntityType()==3 && userSession.getSchoolId()!=null){
					if(userSession.getHeadQuarterMaster()!=null){
						headQuarterId=userSession.getHeadQuarterMaster().getHeadQuarterName();					
						headQuarterHiddenId=userSession.getHeadQuarterMaster().getHeadQuarterId().toString();
						Boolean isExistsBranch=userSession.getHeadQuarterMaster().getIsBranchExist();
						isBracnchExists=(isExistsBranch==null?"0":(isExistsBranch?"1":"0"));
						}
						if(userSession.getBranchMaster()!=null){
						branchId=userSession.getBranchMaster().getBranchName();
						branchHiddenId=userSession.getBranchMaster().getBranchId().toString();
						}
						districtHiddenId=userSession.getDistrictId().getDistrictId().toString();
						onlyDistrictName=userSession.getDistrictId().getDistrictName();
					districtOrSchooHiddenlId=userSession.getSchoolId().getSchoolId()+"";
					SchoolOrDistrictName=getSchoolOrDistrictName(userSession.getEntityType(),Integer.parseInt(""+userSession.getSchoolId().getSchoolId()));
				}
				else if(userSession.getEntityType()==5 && userSession.getHeadQuarterMaster()!=null){
					headQuarterId=userSession.getHeadQuarterMaster().getHeadQuarterName();					
					headQuarterHiddenId=userSession.getHeadQuarterMaster().getHeadQuarterId().toString();
					Boolean isExistsBranch=userSession.getHeadQuarterMaster().getIsBranchExist();
					isBracnchExists=(isExistsBranch==null?"0":(isExistsBranch?"1":"0"));
					/*branchId=userSession.getBranchMaster().getBranchName();
					branchHiddenId=userSession.getBranchMaster().getBranchId().toString();*/
				}
				else if(userSession.getEntityType()==6 && userSession.getBranchMaster()!=null){
					headQuarterId=userSession.getHeadQuarterMaster().getHeadQuarterName();					
					headQuarterHiddenId=userSession.getHeadQuarterMaster().getHeadQuarterId().toString();
					Boolean isExistsBranch=userSession.getHeadQuarterMaster().getIsBranchExist();
					isBracnchExists=(isExistsBranch==null?"0":(isExistsBranch?"1":"0"));
					branchId=userSession.getBranchMaster().getBranchName();
					branchHiddenId=userSession.getBranchMaster().getBranchId().toString();
				}
				
			}
			List<ContactTypeMaster> contactTypeMaster			=contactTypeMasterDAO.findByCriteria(Order.asc("contactType"));	
			List<ContactTypeMaster> attachedcontactTypeMaster= new ArrayList<ContactTypeMaster>();
			List	<DistrictKeyContact>	districtKeyContactList	=null;
			List	<SchoolKeyContact>	schoolKeyContactList	=null;
			if(userMaster.getUserId()!=null)
			{
			//	if(userMaster.getEntityType()==2){ 
				 map.addAttribute("keyEntity", "2");
				districtKeyContactList	=districtKeyContactDAO.findKeyContacts(userMaster.getEmailAddress());
				if(districtKeyContactList!=null && districtKeyContactList.size()>0){
					  for(DistrictKeyContact districtKeyContact:districtKeyContactList) {
						  attachedcontactTypeMaster.add(districtKeyContact.getKeyContactTypeId());
					  }
					 
				   } else{  
			//	} else if(userMaster.getEntityType()==3){
				   schoolKeyContactList=schoolKeyContactDAO.findKeyContacts(userMaster.getEmailAddress());
				   if(schoolKeyContactList!=null && schoolKeyContactList.size()>0){
					   map.addAttribute("keyEntity", "3"); 
					   for(SchoolKeyContact schoolKeyContact:schoolKeyContactList){
						   attachedcontactTypeMaster.add(schoolKeyContact.getKeyContactTypeId());
						  } 
				   }
			//	}
				   }
				
			   for(ContactTypeMaster ctm:attachedcontactTypeMaster){ 
					  Iterator<ContactTypeMaster> iter = contactTypeMaster.iterator();
					  while(iter.hasNext()) {
						  ContactTypeMaster ctmremove = iter.next();
						  if(ctm.getContactTypeId().equals(ctmremove.getContactTypeId())){
					          iter.remove();
					      }
					  }
					  
				  } 
			}
			map.addAttribute("availableContactType", contactTypeMaster);
			map.addAttribute("attachContactType", attachedcontactTypeMaster);
			/*======== For creating authentication code ===============*/
			map.addAttribute("userSession", userSession);	
			map.addAttribute("roleMaster", roleMaster);		
			map.addAttribute("userMaster", userMaster);	
			map.addAttribute("SchoolOrDistrictName", SchoolOrDistrictName);
			map.addAttribute("onlyDistrictName", onlyDistrictName);
			map.addAttribute("districtOrSchooHiddenlId", districtOrSchooHiddenlId);	
			
			map.addAttribute("headQuarterId", headQuarterId);		
			map.addAttribute("headQuarterHiddenId", headQuarterHiddenId);	
			map.addAttribute("branchId", branchId);
			map.addAttribute("branchHiddenId", branchHiddenId);
			if(userMaster!=null && userMaster.getUserId()!=null)
			map.addAttribute("isBracnchExists", isBracnchExists);	
			map.addAttribute("districtHiddenId", districtHiddenId);	
					
					
	
			System.out.println("user entity type===="+userSession.getEntityType());
			if(userSession.getEntityType().toString().equalsIgnoreCase("1") || userSession.getEntityType().toString().equalsIgnoreCase("5") || userSession.getEntityType().toString().equalsIgnoreCase("6")){
				try{
					if(userMaster!=null && userMaster.getUserId()!=null){
						map.addAttribute("selectedRoleCombo", userMaster.getEntityType());
						map.addAttribute("selectrole", userMaster.getRoleId().getRoleId());
					}else{
						map.addAttribute("selectedRoleCombo", userSession.getEntityType());
						map.addAttribute("selectrole", 0);
					}
				
				map.addAttribute("roleCombo", getComboForHeadQuarterOrBranch(userSession));
				//System.out.println("userSession.getHeadQuarterMaster()=============="+userSession.getHeadQuarterMaster().getIsBranchExist());
				if(!userSession.getEntityType().toString().equalsIgnoreCase("1")){
					Boolean isExistsBranch=userSession.getHeadQuarterMaster().getIsBranchExist();
					map.addAttribute("isBracnchExists",(isExistsBranch==null?"0":(isExistsBranch?"1":"0")));
				}
				
				//return "manageuserhq";
				}catch(Exception e){e.printStackTrace();}
			}
			try{
				DistrictKeyContact userKeyContact=null;
				List<DistrictKeyContact>	districtKeyContactList1=districtKeyContactDAO.findByContactType(userSession, "Manage Users");
				if(districtKeyContactList1!=null && districtKeyContactList1.size()>0){
					userKeyContact=districtKeyContactList1.get(0);
				}
				map.addAttribute("userKeyContact", userKeyContact);
			}catch (Exception e) {
				e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "addedituser";
	}
	@RequestMapping(value="/addedituser.do", method=RequestMethod.POST)
	public String doEditUserPOST(@ModelAttribute(value="userMaster") UserMaster userMaster,BindingResult result, ModelMap map,HttpServletRequest request)
	{
		
		HttpSession session = request.getSession(false);
		UserMaster userSession	=null;
		String entityType=null;
		int entityTypeVal=0;
		UserMaster uMaster 	=null;
		UserMaster userMasterForPhoto 	= new UserMaster();
		DistrictMaster districtMasterForNC=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userSession		=	(UserMaster) session.getAttribute("userMaster");
		}
		int insertOrUpdateUserId=0;
		int insertOrUpdateCheck=0;
		try 
		{
			String userId = request.getParameter("userId")		==	null?"0":request.getParameter("userId").trim();
			String roleId = request.getParameter("AEU_RoleName")		==	null?"0":request.getParameter("AEU_RoleName").trim();
			String userFlag=request.getParameter("userFlag")==null?"0":request.getParameter("userFlag").trim();
			String branchId=request.getParameter("branchHiddenId");
			String headQuarterHiddenId=request.getParameter("headQuarterHiddenId");
		// keycontacttypeid
			String keyEntityType=request.getParameter("dropkeyEntityType");
			String keyContactTypeId[]=request.getParameterValues("attachContactTypeAdduser");
		//end of keycontacttypeid	
			String districtId=request.getParameter("districtHiddenId");
			System.out.println("branchId===="+branchId+"    headQuarterHiddenId======"+headQuarterHiddenId+"  request.getParameter(AEU_EntityType)=="+request.getParameter("AEU_EntityType")+" districtId============"+districtId);
			
			if(districtId!=null && !districtId.equals(""))
			{
			districtMasterForNC=districtMasterDAO.findByDistrictId(districtId);			
			if(districtMasterForNC!=null && districtMasterForNC.getHeadQuarterMaster()!=null && districtMasterForNC.getHeadQuarterMaster().getHeadQuarterId()==2)
			{
				headQuarterHiddenId="2";
			}			
			}
			
			String emailPrev="";
			if(userFlag.equalsIgnoreCase("1")){
				emailPrev=usermasterdao.getEmailId(userId);
			}
			int salutation = Integer.parseInt(request.getParameter("salutation")		==	null?"0":request.getParameter("salutation").trim());
			entityType = request.getParameter("AEU_EntityType")		==	null?"0":request.getParameter("AEU_EntityType").trim();
			
			int districtOrSchooHiddenlId =0;
			if(request.getParameter("districtOrSchooHiddenlId")!=null && request.getParameter("districtOrSchooHiddenlId")!=""){
				try{
				districtOrSchooHiddenlId=Integer.parseInt(request.getParameter("districtOrSchooHiddenlId")==null||request.getParameter("districtOrSchooHiddenlId")==""?"0":request.getParameter("districtOrSchooHiddenlId").trim());
				}catch(Exception e){
					//e.printStackTrace();
				}
			}
			int schoolHiddenId =0;
			
			if(request.getParameter("schoolHiddenId")!=null && request.getParameter("schoolHiddenId")!=""){
				try{
					schoolHiddenId=Integer.parseInt(request.getParameter("schoolHiddenId")==null||request.getParameter("schoolHiddenId")==""?"0":request.getParameter("schoolHiddenId").trim());
				}catch(Exception e){
					//e.printStackTrace();
				}
			}
			if(entityType!=null){
				entityTypeVal=Integer.parseInt(entityType);
			}
			if(!userId.equals("")){
				uMaster 	=	usermasterdao.findById(Integer.parseInt(userId), false, false);
				userMaster.setUserId(Integer.parseInt(userId));
			}
			
			userMasterForPhoto.setPhotoPathFile(userMaster.getPhotoPathFile());
					
			int verificationCode=(int) Math.round(Math.random() * 2000000);
			
			if(userSession.getEntityType()==2 && userSession.getDistrictId().getDistrictId()!=null && districtOrSchooHiddenlId==0){
				districtOrSchooHiddenlId=userSession.getDistrictId().getDistrictId();
			}
			if(userSession.getEntityType()==3 && userSession.getDistrictId().getDistrictId()!=null && districtOrSchooHiddenlId==0){
				districtOrSchooHiddenlId=Integer.parseInt(""+userSession.getSchoolId().getSchoolId());
			}
			if(entityTypeVal==2){
				DistrictMaster districtMaster =districtMasterDAO.findById(districtOrSchooHiddenlId, false, false);
				SchoolMaster schoolMaster =schoolMasterDAO.findById((long)schoolHiddenId, false, false);
				
				userMaster.setDistrictId(districtMaster);
				userMaster.setSchoolId(schoolMaster);
				
			}
			
			if(entityTypeVal==3){
				SchoolMaster schoolMaster =schoolMasterDAO.findById((long)districtOrSchooHiddenlId, false, false);
				userMaster.setSchoolId(schoolMaster);
				userMaster.setDistrictId(getDistrictId(districtOrSchooHiddenlId));
			}
		
			if(!userId.equals("")){	
				userMaster.setPassword(uMaster.getPassword());
				userMaster.setVerificationCode(uMaster.getVerificationCode());
				userMaster.setAuthenticationCode(uMaster.getAuthenticationCode());
				userMaster.setSalutation(salutation);
				userMaster.setStatus(uMaster.getStatus());
			}else{
				
				userMaster.setSalutation(salutation);
				userMaster.setStatus("A");
				userMaster.setVerificationCode(""+verificationCode);
				userMaster.setPassword(MD5Encryption.toMD5("teachermatch"));
		   
			}
			userMaster.setEntityType(entityTypeVal);
			RoleMaster roleMaster = roleMasterDAO.findById(Integer.parseInt(roleId), false, false);			
			userMaster.setRoleId(roleMaster);
			if(userMaster.getPhotoPathFile().getSize()>0){
			}else{
				userMaster.setPhotoPathFile(null);
			}
			
			userMaster.setForgetCounter(0);
			userMaster.setIsQuestCandidate(false);
			userMaster.setCreatedDateTime(new Date());
			
			/****************************Head Quarter *****************************************/
			System.out.println("userSession.getEntityType()=========="+userSession.getEntityType());
			if(userSession.getEntityType()==1){
				if(userSession.getHeadQuarterMaster()!=null && userSession.getHeadQuarterMaster().getHeadQuarterId()==2)
				{
					headQuarterHiddenId="2";
				}
				if(branchId!=null && !branchId.trim().equalsIgnoreCase("")){
					userMaster.setBranchMaster(branchMasterDAO.findById(Integer.parseInt(branchId), false, false));
				}
				
				if(headQuarterHiddenId!=null && !headQuarterHiddenId.trim().equalsIgnoreCase("")){
					userMaster.setHeadQuarterMaster(headQuarterMasterDAO.findById(Integer.parseInt(headQuarterHiddenId), false, false));
				}
				
				if(schoolHiddenId!=0){
					userMaster.setSchoolId(schoolMasterDAO.findById((long)schoolHiddenId, false, false));
				}
				if(districtId!=null && !districtId.trim().equalsIgnoreCase("")){
					userMaster.setDistrictId(districtMasterDAO.findById(Integer.parseInt(districtId), false, false));
				}	
			}
			if(userSession.getEntityType()==2){
				userMaster.setDistrictId(userSession.getDistrictId());
				userMaster.setBranchMaster(userSession.getBranchMaster());
				userMaster.setHeadQuarterMaster(userSession.getHeadQuarterMaster());
			}
			if(userSession.getEntityType()==3){
				userMaster.setSchoolId(userSession.getSchoolId());
				userMaster.setDistrictId(userSession.getDistrictId());
				userMaster.setBranchMaster(userSession.getBranchMaster());
				userMaster.setHeadQuarterMaster(userSession.getHeadQuarterMaster());
			}
			if(userSession.getEntityType()==5){
				userMaster.setHeadQuarterMaster(userSession.getHeadQuarterMaster());
				if(branchId!=null && !branchId.trim().equalsIgnoreCase("")){
					userMaster.setBranchMaster(branchMasterDAO.findById(Integer.parseInt(branchId), false, false));
				}
				if(districtId!=null && !districtId.trim().equalsIgnoreCase("")){
					userMaster.setDistrictId(districtMasterDAO.findById(Integer.parseInt(districtId), false, false));
				}						
			}
			if(userSession.getEntityType()==6){
				userMaster.setHeadQuarterMaster(userSession.getHeadQuarterMaster());
				userMaster.setBranchMaster(userSession.getBranchMaster());		
				if(districtId!=null && !districtId.trim().equalsIgnoreCase("")){
					userMaster.setDistrictId(districtMasterDAO.findById(Integer.parseInt(districtId), false, false));
				}
			}
			
			if(entityTypeVal==2){
					userMaster.setSchoolId(null);
			}else if(entityTypeVal==5){
					userMaster.setSchoolId(null);
					userMaster.setDistrictId(null);	
					userMaster.setBranchMaster(null);
			}else if(entityTypeVal==6){
					userMaster.setSchoolId(null);
					userMaster.setDistrictId(null);			
			}
			/****************************************End********************************************/
			
			
			if(userId.equals("")){
				userMaster.setPhotoPathFile(null); 
				userMaster.setPhotoPath(null);
				usermasterdao.makePersistent(userMaster);
				}
			
			/*********Photo Upload File  *********/
			try{
				if(userMasterForPhoto.getPhotoPathFile()!=null){
					if(userMasterForPhoto.getPhotoPathFile().getSize()>0){
						if(!userId.equals("")){
							if(uMaster.getPhotoPath()!=null ){
								try{
									String filePath="";
									filePath=Utility.getValueOfPropByKey("userRootPath")+userMaster.getUserId()+"/"+uMaster.getPhotoPath();
									File file = new File(filePath);
						    		if(file.delete()){
						    			//System.out.println(file.getName() + " is deleted!");
						    		}else{
						    			//System.out.println("Delete operation is failed.");
						    		}
								}catch(Exception e){
							 		e.printStackTrace();
							  	}
							}
						}
						
						String filePath=Utility.getValueOfPropByKey("userRootPath")+userMaster.getUserId()+"/";
						
						File f=new File(filePath);
						if(!f.exists())
							 f.mkdirs();
						
						FileItem fileItem =userMasterForPhoto.getPhotoPathFile().getFileItem();
						String fileName="Logo"+Utility.getUploadFileName(fileItem);
						fileItem.write(new File(filePath, fileName));
						ImageResize.resizeImage(filePath+ fileName);
						userMaster.setPhotoPath(fileName);
					}else{
						if(!userId.equals("")){
							userMaster.setPhotoPath(uMaster.getPhotoPath());
						}
					}
				}else{
					if(!userId.equals("")){
						userMaster.setPhotoPath(uMaster.getPhotoPath());
					}
				}
			}catch(FileNotFoundException e){
				if(!userId.equals("")){
					userMaster.setPhotoPath(uMaster.getPhotoPath());
				}
				e.printStackTrace();
			}

			userMaster.setPhotoPathFile(null);
			usermasterdao.updatePersistent(userMaster);
			

			// keycontactsave
			if(userSession.getRoleId().getRoleId()==1||userSession.getRoleId().getRoleId()==4||userSession.getRoleId().getRoleId()==7)
				if(entityTypeVal==2||entityTypeVal==3)
				{	
					int	keyEntityTypeVal=Integer.parseInt(keyEntityType);
					if(keyContactTypeId!=null)
					{
						Integer districtIdNumber=null;
						if(districtId!=null && !districtId.equals("")){
							districtIdNumber=Integer.parseInt(districtId);
						}
						if(districtIdNumber==null){
							if(userSession.getEntityType()==2)
							districtIdNumber=Integer.parseInt(userSession.getDistrictId().getDistrictId().toString());
						}
					if(keyEntityTypeVal==2){
						if(districtIdNumber!=null){
							saveUserAsDistrictKeyContact(keyContactTypeId,districtIdNumber, userMaster.getFirstName(), userMaster.getLastName(), userMaster.getEmailAddress(), userMaster.getPhoneNumber(), userMaster.getTitle(), userMaster);	
						}
					}
					else if(keyEntityTypeVal==3){
							if(entityTypeVal==3){
								saveUserAsSchoolKeyContact(keyContactTypeId,""+districtOrSchooHiddenlId, userMaster.getFirstName(), userMaster.getLastName(), userMaster.getEmailAddress(), userMaster.getPhoneNumber(), userMaster.getTitle(), userMaster);
							} 
					}
					
					}else{
						if(keyEntityTypeVal==2){
						List<DistrictKeyContact>	districtKeyContactList=	districtKeyContactDAO.checkEmailAndContactTypeId(userMaster.getEmailAddress(), null,userMaster.getDistrictId().getDistrictId());
						if(districtKeyContactList!=null && districtKeyContactList.size()>0){
							SessionFactory sessionFactory=districtKeyContactDAO.getSessionFactory();
						       StatelessSession statelesSsession = sessionFactory.openStatelessSession();
						       Transaction txOpen =statelesSsession.beginTransaction();
							for(DistrictKeyContact districtKeyContact:districtKeyContactList){
								statelesSsession.delete(districtKeyContact);
								}
							txOpen.commit();
							}
						}else if(keyEntityTypeVal==3){
							Long schoolIdNumber= null;
							if(districtOrSchooHiddenlId!=0)
							schoolIdNumber=new Long(districtOrSchooHiddenlId);
							List<SchoolKeyContact>	schoolKeyContactList=	schoolKeyContactDAO.checkEmailAndContactTypeId(userMaster.getEmailAddress(), null,schoolIdNumber);
							if(schoolKeyContactList!=null && schoolKeyContactList.size()>0){
								SessionFactory sessionFactory=schoolKeyContactDAO.getSessionFactory();
							       StatelessSession statelesSsession = sessionFactory.openStatelessSession();
							       Transaction txOpen =statelesSsession.beginTransaction();
								for(SchoolKeyContact schoolKeyContact:schoolKeyContactList){
									statelesSsession.delete(schoolKeyContact);
									}
								txOpen.commit();
								}
						}
					}
				}
			// end keycontact save
			if(!emailPrev.equalsIgnoreCase("")){
				
				List<DistrictKeyContact> districtKeyContactList=districtKeyContactDAO.findKeyContacts(emailPrev);
				List<SchoolKeyContact> schoolKeyContactList=schoolKeyContactDAO.findKeyContacts(emailPrev);
				
				SessionFactory sessionFactory=districtKeyContactDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				Transaction txOpen =statelesSsession.beginTransaction();
				for(DistrictKeyContact districtKeyContact:districtKeyContactList){
					districtKeyContact.setKeyContactFirstName(userMaster.getFirstName());
					districtKeyContact.setKeyContactLastName(userMaster.getLastName());
					districtKeyContact.setKeyContactEmailAddress(userMaster.getEmailAddress());
					districtKeyContact.setKeyContactPhoneNumber(userMaster.getPhoneNumber());
					districtKeyContact.setKeyContactTitle(userMaster.getTitle());
					statelesSsession.update(districtKeyContact);
				}
				for(SchoolKeyContact schoolKeyContact:schoolKeyContactList){
					schoolKeyContact.setKeyContactFirstName(userMaster.getFirstName());
					schoolKeyContact.setKeyContactLastName(userMaster.getLastName());
					schoolKeyContact.setKeyContactEmailAddress(userMaster.getEmailAddress());
					schoolKeyContact.setKeyContactPhoneNumber(userMaster.getPhoneNumber());
					schoolKeyContact.setKeyContactTitle(userMaster.getTitle());
					statelesSsession.update(schoolKeyContact);
				}
				txOpen.commit();
			}
			    String roleid = roleMaster.getRoleName();
			    if(userId.equals("")){
				insertOrUpdateCheck=1;
				if(userSession.getEntityType()==5 || userSession.getEntityType()==6){
				emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), Utility.getLocaleValuePropByKey("msgHaveAddedYouUser1", locale),MailText.createUserPwdMailToOtUserForKelly(request,userMaster,roleid));
				}else{
				emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), Utility.getLocaleValuePropByKey("msgHaveAddedYouUser1", locale),MailText.createUserPwdMailToOtUser(request,userMaster));
				}
				}
			    
			if(userMaster.getUserId()!=null){
				insertOrUpdateUserId=userMaster.getUserId();
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();	
		}
		
		return "redirect:manageuser.do?userId="+insertOrUpdateUserId+"&insertUpdateCheck="+insertOrUpdateCheck;
	}
	public DistrictMaster getDistrictId(int schooId)
	{
		DistrictMaster districtMaster =null;
			List<SchoolMaster> fieldOfSchoolList = null;
			long schoolId = (long) (schooId);
			try{
				Criterion criterion = Restrictions.eq("schoolId",schoolId);
				fieldOfSchoolList = schoolMasterDAO.findByCriteria(criterion);
				districtMaster =districtMasterDAO.findById(fieldOfSchoolList.get(0).getDistrictId().getDistrictId(), false, false);
				
			}catch (Exception e) {
				e.printStackTrace();
			}
		return districtMaster;
	}
	public String getSchoolOrDistrictName(int entityType,int id)
	{
		String Name="";
		if(entityType==2){
			List<DistrictMaster> fieldOfDistrictList = null;
			try{
				Criterion criterion = Restrictions.eq("districtId",id);
				fieldOfDistrictList = districtMasterDAO.findByCriteria(criterion);
				Name=fieldOfDistrictList.get(0).getDistrictName();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}else if(entityType==3){
			List<SchoolMaster> fieldOfSchoolList = null;
			long schoolId = (long) (id);
			try{
				Criterion criterion = Restrictions.eq("schoolId",schoolId);
				fieldOfSchoolList = schoolMasterDAO.findByCriteria(criterion);
				Name=fieldOfSchoolList.get(0).getSchoolName();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return Name;
	}
	 @RequestMapping(value="/importuserdetails.do", method=RequestMethod.GET)
		public String teacherjobuploadGET(ModelMap map,HttpServletRequest request)
		{
			try{
				System.out.println("\n =========== importuserdetails.do Controller ===============");
				HttpSession session = request.getSession(false);
				int roleId=0;
				UserMaster userMaster =null;
				if (session == null || session.getAttribute("userMaster") == null) 
				{
					return "redirect:index.jsp";
				}else{
					userMaster=	(UserMaster) session.getAttribute("userMaster");
					if(userMaster.getEntityType()==2)
					{
					   map.addAttribute("districtId", userMaster.getDistrictId().getDistrictId());
					   map.addAttribute("entityType", userMaster.getEntityType());
					}
					
					if(userMaster.getRoleId().getRoleId()!=null){
						roleId=userMaster.getRoleId().getRoleId();
					}
				}
			}catch (Exception e){
				e.printStackTrace();
			}
			return "userupload";
		}
	    @RequestMapping(value="/usertemplist.do", method=RequestMethod.GET)
		public String teacherTempListGET(ModelMap map,HttpServletRequest request)
		{
			try{
				System.out.println("\n=@@@@@@@@@@@@@@= teacherjobupload Controller ======@@@@@@@@==== ");
				HttpSession session = request.getSession(false);
				int roleId=0;
				UserMaster userMaster =null;
				if (session == null || session.getAttribute("userMaster") == null) 
				{
					return "redirect:index.jsp";
				}else{
					userMaster=	(UserMaster) session.getAttribute("userMaster");
					if(userMaster.getRoleId().getRoleId()!=null){
						roleId=userMaster.getRoleId().getRoleId();
					}
				}
				map.addAttribute("sessionIdTxt", session.getId());
			}catch (Exception e){
				e.printStackTrace();
			}
			return "usertemplist";
		}
	    //************************ Adding by Deepak *************************************************
		public synchronized static void manageuserMethod(ModelMap map,HttpServletRequest request,UserMasterDAO usermasterdao,RoleAccessPermissionDAO roleAccessPermissionDAO)
		{
			
			HttpSession session = request.getSession(false);
			String userId = request.getParameter("userId")		==	null?"0":request.getParameter("userId").trim();
			String insertUpdateCheck = request.getParameter("insertUpdateCheck")==	null?"0":request.getParameter("insertUpdateCheck").trim();
			
			UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
			int roleId=0;
			// =============== for Getting the All database Field Values from database ====================
			 List<UserMaster> beans = usermasterdao.findAll();
				for(UserMaster d: beans) // For Printing All Rows from Database
				{
					d.getFirstName(); // For Printing First Name from Database
					d.getRoleId();
				}
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				String roleAccess=null;
				try{
					if(roleId==10 || roleId==11)
						roleAccess=roleAccessPermissionDAO.getMenuOptionListByMenuId(roleId,96,"manageuser.do",1,100);
					else
						roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,1,"manageuser.do",0);
					
					System.out.println(" roleAccess :: "+roleAccess);
				}catch(Exception e){
					e.printStackTrace();
				}
				map.addAttribute("roleAccess", roleAccess);
			try 
			{
				map.addAttribute("beans", beans);
				map.addAttribute("userMaster", userMaster);	
				map.addAttribute("userId", userId);	
				map.addAttribute("insertUpdateCheck", insertUpdateCheck);	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			System.out.println("user entity type==111=="+userMaster.getEntityType());
			if(userMaster.getEntityType().toString().equalsIgnoreCase("1") || userMaster.getEntityType().toString().equalsIgnoreCase("5") || userMaster.getEntityType().toString().equalsIgnoreCase("6")){
				try{
				map.addAttribute("selectedRoleCombo", userMaster.getEntityType());
				map.addAttribute("roleCombo", getComboForHeadQuarterOrBranch(userMaster));
				if(!userMaster.getEntityType().toString().equalsIgnoreCase("1"))
				map.addAttribute("isBracnchExists",(userMaster.getHeadQuarterMaster().getIsBranchExist()==null?"0":(userMaster.getHeadQuarterMaster().getIsBranchExist()?"1":"0")));
				//return "manageuserhq";
				}catch(Exception e){e.printStackTrace();}
			}
		}
	
		// method Adding by hafeez
		public int saveUserAsDistrictKeyContact(String keyContactTypeId[],Integer districtId,String keyContactFirstName,String keyContactLastName,String keyContactEmailAddress,String keyContactPhoneNumber,String keyContactTitle,UserMaster userMaster)
		{
			ArrayList<Integer> idList=new ArrayList<Integer>();
			List<ContactTypeMaster> keyContactTypeList=null;
			SessionFactory sessionFactory=districtKeyContactDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			Transaction txOpen =statelesSsession.beginTransaction();
			 for(String id:keyContactTypeId){
				 if(id!=null && !id.equals(""))
				 idList.add(Integer.parseInt(id));
			 }
			 keyContactTypeList	=	contactTypeMasterDAO.findByContactTypeId(idList);
				List<DistrictKeyContact> removeKeyContactList=districtKeyContactDAO.checkEmailAndContactTypeId(keyContactEmailAddress, keyContactTypeList,districtId);
				if(removeKeyContactList!=null && removeKeyContactList.size()>0)
				for(DistrictKeyContact districtKeyContact:removeKeyContactList){
					statelesSsession.delete(districtKeyContact);
				}
				List<DistrictKeyContact> districtKeyContactList=	districtKeyContactDAO.findByContactTypeAndEmail(keyContactEmailAddress,userMaster.getDistrictId());
				if(keyContactTypeList!=null && keyContactTypeList.size()>0)
				for(ContactTypeMaster contactTypeMaster: keyContactTypeList){
					int saveOrupdate=1;
					if(districtKeyContactList!=null && districtKeyContactList.size()>0){
						for(DistrictKeyContact districtKeyContact:districtKeyContactList){
							if(districtKeyContact.getKeyContactTypeId().getContactTypeId().equals(contactTypeMaster.getContactTypeId())){
								saveOrupdate=2;
								districtKeyContact.setDistrictId(districtId);
								districtKeyContact.setKeyContactTypeId(contactTypeMaster);
								districtKeyContact.setKeyContactFirstName(keyContactFirstName);
								districtKeyContact.setKeyContactLastName(keyContactLastName);
								districtKeyContact.setKeyContactEmailAddress(keyContactEmailAddress);
								districtKeyContact.setKeyContactPhoneNumber(keyContactPhoneNumber);
								districtKeyContact.setKeyContactTitle(keyContactTitle);
								districtKeyContact.setUserMaster(userMaster);
								statelesSsession.update(districtKeyContact);
							}
						}
					}
					if(saveOrupdate==1){
					DistrictKeyContact districtKeyContactAddNew	=	new DistrictKeyContact();
					districtKeyContactAddNew.setDistrictId(districtId);
					districtKeyContactAddNew.setKeyContactTypeId(contactTypeMaster);
					districtKeyContactAddNew.setKeyContactFirstName(keyContactFirstName);
					districtKeyContactAddNew.setKeyContactLastName(keyContactLastName);
					districtKeyContactAddNew.setKeyContactEmailAddress(keyContactEmailAddress);
					districtKeyContactAddNew.setKeyContactPhoneNumber(keyContactPhoneNumber);
					districtKeyContactAddNew.setKeyContactTitle(keyContactTitle);
					districtKeyContactAddNew.setUserMaster(userMaster);
					statelesSsession.insert(districtKeyContactAddNew);
					}		
				}
				txOpen.commit();
			return 1;
		}
	public int saveUserAsSchoolKeyContact(String keyContactTypeId[],String schoolId,String keyContactFirstName,String keyContactLastName,String keyContactEmailAddress,String keyContactPhoneNumber,String keyContactTitle,UserMaster userMaster)
	{
		
		ArrayList<Integer> idList=new ArrayList<Integer>();
		 for(String id:keyContactTypeId){
			 if(id!=null && !id.equals(""))
			 idList.add(Integer.parseInt(id));
		 }
		List<ContactTypeMaster> keyContactTypeList=null;
		Long schoolIdNumber= null;
		if(schoolId!=null)schoolIdNumber=new Long(schoolId);
	   SessionFactory sessionFactory=schoolKeyContactDAO.getSessionFactory();
       StatelessSession statelesSsession = sessionFactory.openStatelessSession();
       Transaction txOpen =statelesSsession.beginTransaction();
       keyContactTypeList	=	contactTypeMasterDAO.findByContactTypeId(idList);
		List<SchoolKeyContact> removeKeyContactList=schoolKeyContactDAO.checkEmailAndContactTypeId(keyContactEmailAddress, keyContactTypeList,schoolIdNumber);
		if(removeKeyContactList!=null && removeKeyContactList.size()>0)
			for(SchoolKeyContact schoolKeyContact:removeKeyContactList){
				statelesSsession.delete(schoolKeyContact);
			}
		
		List<SchoolKeyContact> schoolKeyContactList=	schoolKeyContactDAO.findBySchoolIdeAndEmail(keyContactEmailAddress,schoolIdNumber);
					if(keyContactTypeList!=null && keyContactTypeList.size()>0)
					for(ContactTypeMaster contactTypeMaster: keyContactTypeList){
					int saveOrupdate=1;
					if(schoolKeyContactList!=null && schoolKeyContactList.size()>0){
					for(SchoolKeyContact schoolKeyContact:schoolKeyContactList){
					if(schoolKeyContact.getKeyContactTypeId().getContactTypeId().equals(contactTypeMaster.getContactTypeId())){
						saveOrupdate=2;
						schoolKeyContact.setSchoolId(schoolIdNumber);
						schoolKeyContact.setKeyContactTypeId(schoolKeyContact.getKeyContactTypeId());
						schoolKeyContact.setKeyContactFirstName(keyContactFirstName);
						schoolKeyContact.setKeyContactLastName(keyContactLastName);
						schoolKeyContact.setKeyContactEmailAddress(keyContactEmailAddress);
						schoolKeyContact.setKeyContactPhoneNumber(keyContactPhoneNumber);
						schoolKeyContact.setKeyContactTitle(keyContactTitle);
						schoolKeyContact.setUserMaster(userMaster);
						statelesSsession.update(schoolKeyContact);
							}
						}
					}
					if(saveOrupdate==1){
						SchoolKeyContact SchoolKeyContactAddNew	=	new SchoolKeyContact();
						SchoolKeyContactAddNew.setSchoolId(schoolIdNumber);
						SchoolKeyContactAddNew.setKeyContactTypeId(contactTypeMaster);
						SchoolKeyContactAddNew.setKeyContactFirstName(keyContactFirstName);
						SchoolKeyContactAddNew.setKeyContactLastName(keyContactLastName);
						SchoolKeyContactAddNew.setKeyContactEmailAddress(keyContactEmailAddress);
						SchoolKeyContactAddNew.setKeyContactPhoneNumber(keyContactPhoneNumber);
						SchoolKeyContactAddNew.setKeyContactTitle(keyContactTitle);
						SchoolKeyContactAddNew.setUserMaster(userMaster);
						statelesSsession.insert(SchoolKeyContactAddNew);
					}
				}
		
		txOpen.commit();
		return 1;
	}
}
