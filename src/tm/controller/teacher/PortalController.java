package tm.controller.teacher;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPreference;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.dao.FavTeacherDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobShareDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherPreferenceDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.services.AssessmentCampaignAjax;
import tm.services.EmailerService;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

@Controller
public class PortalController {
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) 
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}

	@Autowired
	private TeacherPreferenceDAO teacherPreferenceDAO;
	public void setTeacherPreferenceDAO(TeacherPreferenceDAO teacherPreferenceDAO) 
	{
		this.teacherPreferenceDAO = teacherPreferenceDAO;
	}

	@Autowired
	private FavTeacherDAO favTeacherDAO;
	public void setFavTeacherDAO(FavTeacherDAO favTeacherDAO) 
	{
		this.favTeacherDAO = favTeacherDAO;
	}

	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) 
	{
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) 
	{
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
	{
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) 
	{
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}

	@Autowired
	private JobShareDAO jobShareDAO;
	public void setJobShareDAO(JobShareDAO jobShareDAO) {
		this.jobShareDAO = jobShareDAO;
	}

	@Autowired
	private ServletContext servletContext;
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	
	@Autowired
	private ApplicationContext appContext;
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}
	
	@Autowired
	private AssessmentCampaignAjax assessmentCampaignAjax;
	
	@InitBinder
	public void initBinder(HttpServletRequest request,DataBinder binder) 
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true,10));
	}
	
	
	@RequestMapping(value="/portaldashboard.do", method=RequestMethod.GET)
	public String epiDashboardGET(ModelMap map,HttpServletRequest request, HttpServletResponse response){

		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}

		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		map.addAttribute("teacherDetail", teacherDetail);
		String jobId = request.getParameter("jobId");
		JobOrder jobOrder  = null;
		
		System.out.println("jobId:"+jobId);
		
		boolean startEPIflag = false;
		
		try 
		{
			if(jobId!=null && jobId.trim().length()>0)
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);

			map.addAttribute("jobOrder", jobOrder);
			
			boolean baseInvStatus = false;
			
			
			TeacherAssessmentStatus teacherBaseAssessmentStatus = null;
			teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if(teacherBaseAssessmentStatus!=null && (teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") ))
			{
				baseInvStatus=true;
			}
			
			if(teacherDetail!=null && teacherDetail.getIsPortfolioNeeded().equals(false) && session.getAttribute("referer")!=null){
				Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
				jobId = referer.get("jobId");
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
				if(jobOrder!=null && jobOrder.getIsPortfolioNeeded()){
					map.addAttribute("chkRefer", true);				
					session.removeAttribute("referer");
					return "portaldashboard";
				}				
			}
			
			if(baseInvStatus)
			{		
				startEPIflag =  false;
			}
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
			{
				startEPIflag =  false;	
			}
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
			{
				startEPIflag =  true;
				map.addAttribute("baseStatus", "icomp");
			}
			else 
			{
				map.addAttribute("baseStatus", "start");
				startEPIflag =  true;
			}
			
			jobId = request.getParameter("jobId");
			String mf = request.getParameter("mf");
			String compltd = "N";
			if(jobId!=null && jobId.trim().length()>0)
			{
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
				JobForTeacher jft = null;
				jft = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
				if(jft!=null)
				{
					if(jft.getStatus().getStatusShortName().equalsIgnoreCase("comp"))
						compltd = "Y";
				}
			}
			
			map.addAttribute("compltd", compltd); 
			map.addAttribute("jobOrder", jobOrder); 
			map.addAttribute("mf", mf);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		map.addAttribute("startEPIflag", startEPIflag);
		return "portaldashboard";		
	}
	
	@RequestMapping(value="/epiboard.do", method=RequestMethod.GET)
	public String epiboardGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}

		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		map.addAttribute("teacherDetail", teacherDetail);
		String jobId = request.getParameter("jobId");
		JobOrder jobOrder  = null;

		try 
		{
			Integer epiJobId = null;
			JobForTeacher jobForTeacher = null;
			List<JobForTeacher> jobForTeacherList = null;
			if(jobId!=null && jobId.trim().length()>0){
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
				epiJobId = Integer.parseInt(jobId);
			}
			if(epiJobId==null){
				jobForTeacherList = jobForTeacherDAO.findByCriteria(Order.desc("createdDateTime"),Restrictions.eq("teacherId", teacherDetail));
				if(jobForTeacherList!=null && jobForTeacherList.size()>0){
					jobForTeacher = jobForTeacherList.get(0);
					if(jobForTeacher!=null)
						epiJobId = jobForTeacher.getJobId().getJobId();
				}
			}

			map.addAttribute("jobOrder", jobOrder);	
			
			boolean baseInvStatus = false;
			
			
			TeacherAssessmentStatus teacherBaseAssessmentStatus = null;
			teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if(teacherBaseAssessmentStatus!=null && (teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") ))
			{
				baseInvStatus=true;
			}
			
			if(baseInvStatus)
			{
				map.addAttribute("baseStatus", "comp");
				return "redirect:portaldashboard.do";
			}
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
			{
				map.addAttribute("baseStatus", "vlt");
				return "redirect:portaldashboard.do";
			}
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
			{
				map.addAttribute("baseStatus", "icomp");
				if(epiJobId!=null)
					map.addAttribute("epiJobId", epiJobId);
				else if(epiJobId==null)
					map.addAttribute("epiJobId", "null");
				map.addAttribute("assessmentDetail", teacherBaseAssessmentStatus.getAssessmentDetail());
				
			}
			else 
			{
				map.addAttribute("baseStatus", "start");
				AssessmentDetail assessmentDetail = new AssessmentDetail();
				JobOrder jobOrder2 = new JobOrder();
				jobOrder2.setJobId(0);
				assessmentCampaignAjax.setHttpServletRequest(request);
				assessmentDetail = assessmentCampaignAjax.checkInventory(jobOrder2,epiJobId);
				if(epiJobId!=null)
					map.addAttribute("epiJobId", epiJobId);
				else if(epiJobId==null)
					map.addAttribute("epiJobId", "null");
				map.addAttribute("assessmentDetail", assessmentDetail);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return "epiboard";
	}
	
	
	@RequestMapping(value="/getportalmilestones.do", method=RequestMethod.GET)
	public void getDashboardMilestonesGrid(HttpServletRequest request, HttpServletResponse response)
	{
		HttpSession session = request.getSession();
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		

		TeacherAssessmentStatus teacherBaseAssessmentStatus = null;

		List<JobForTeacher> lstJBSViolatedJobForTeacher = null;

		
		
		int noOfJBSViolatedJobForTeacher = 0;
		boolean baseInvStatus = false;

		try 
		{
			
			StatusMaster statusViolated =WorkThreadServlet.statusMap.get("vlt");

			teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if(teacherBaseAssessmentStatus!=null && (teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") ))
			{
				baseInvStatus=true;

			}
			
			lstJBSViolatedJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail, statusViolated);

			if(lstJBSViolatedJobForTeacher!=null)
			{

				for (JobForTeacher jobForTeacher : lstJBSViolatedJobForTeacher) {
					if(jobForTeacher.getJobId().getIsJobAssessment())
						noOfJBSViolatedJobForTeacher++;
				}
			}

			PrintWriter pw = response.getWriter();			
			pw.write("<table width='100%' border='0' class='table table-bordered table-striped'>");
			pw.write("<thead class='bg'>");
			pw.write("<tr>");
			pw.write("<th><img class='fl' src='images/status-icon.png'> <h4> "+Utility.getLocaleValuePropByKey("msgPortalController2", locale)+"</h4></th>");
			pw.write("<th>"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
			pw.write("<th>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			pw.write("</tr>");
			pw.write("</thead>");

			//--Base Inventory start
			pw.write("<tr>");

			String baseMsg=Utility.getLocaleValuePropByKey("msgUserDashbrd2", locale);

			pw.write("<td>"+Utility.getLocaleValuePropByKey("msgPortalController1", locale)+"&nbsp;<a  href='#' id='iconpophoverBase' rel='tooltip' data-original-title=\""+baseMsg+"\"><img src='images/qua-icon.png' width='15' height='15' alt=''></a></td>");
			pw.write("<td>");
			if(baseInvStatus)
			{
				pw.write("<strong class='text-success'>"+Utility.getLocaleValuePropByKey("lblCompleted", locale)+"</strong>");
			}
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
			{
				pw.write("<strong class='text-error'>"+Utility.getLocaleValuePropByKey("optTOut", locale)+"</strong>");
			}
			else
			{
				pw.write("<strong class='text-error' id='bStatus' >"+Utility.getLocaleValuePropByKey("optIncomlete", locale)+"</strong>");
			}				
			pw.write("</td>");
			pw.write("<td class='pagination-centered' id='bClick'>");
					
			String toolTipText = "";
			if(teacherBaseAssessmentStatus==null)
			{
				toolTipText = Utility.getLocaleValuePropByKey("btmStartBaseInventory", locale);			
			}
			else if(teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
			{
				toolTipText = Utility.getLocaleValuePropByKey("btnResumeBaseInventory", locale);				
			}

			if(baseInvStatus)
			{}
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
			{}
			else
			{
				pw.write("<a data-original-title='"+toolTipText+"' rel='tooltip' id='iconpophover3' href='javascript:void(0)' onclick=\"checkInventory(0,null)\">");
				pw.write("<img src='images/option06.png'>");
				pw.write("</a>");
			}	
			
			pw.write("</td>");
			pw.write("</tr>");			
			pw.write("</table>");

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value="/getportaljobsofintrest.do", method=RequestMethod.GET)
	public void getDashboardJbofIntresGrid(HttpServletRequest request, HttpServletResponse response)
	{
		try 
		{
			PrintWriter pw = response.getWriter();			
			pw.write(showDashboardJbofIntresGrid(request));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public String showDashboardJbofIntresGrid(HttpServletRequest request)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{		
			HttpSession session = request.getSession();
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");

			Order sortOrderStrVal	=	null;
			
			List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
			if(teacherDetail.getUserType().equalsIgnoreCase("N")){
				lstJobOrder = getJobByPref(request,sortOrderStrVal);
			}
			else{	
				List<JobForTeacher> lstJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);	
				DistrictMaster districtMaster = null;
				if(lstJFT.size()>0)
				{ 
					districtMaster = lstJFT.get(0).getJobId().getDistrictMaster();
					lstJobOrder = jobOrderDAO.findJobOrderbyDistrict(districtMaster);
				}
			}


			StatusMaster statusComplete =WorkThreadServlet.statusMap.get("comp");
			StatusMaster statusInComplete =WorkThreadServlet.statusMap.get("icomp");
			StatusMaster statusViolated =WorkThreadServlet.statusMap.get("vlt");
			StatusMaster statusHired = WorkThreadServlet.statusMap.get("hird");
			StatusMaster statusRemoved = WorkThreadServlet.statusMap.get("rem");
			StatusMaster statusWidrw = WorkThreadServlet.statusMap.get("widrw");
			


			List<JobForTeacher> lstAllJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);
			List<JobForTeacher> lstcompletJFT = jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusComplete);
			List<JobForTeacher> lstincompletJFT = jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusInComplete);
			List<JobForTeacher> lstviolatedJFT = jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusViolated);			
			List<JobForTeacher> lstWidrwJFT = jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusWidrw);
			List<JobForTeacher> lstHiredJFT = jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusHired);
			List<JobForTeacher> lstRemoveddJFT = jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusRemoved);
			lstcompletJFT.addAll(lstHiredJFT);
			lstcompletJFT.addAll(lstRemoveddJFT);

			for(JobForTeacher jft: lstAllJFT)
			{				
				JobOrder jobOrder = jft.getJobId();				
				if(lstJobOrder.contains(jobOrder))
				{
					lstJobOrder.remove(jobOrder);
				}
			}

			sb.append("<table width='100%' border='0' class='table table-bordered table-striped'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th width='67%'><img class='fl' src='images/search-icon.png'> <h4>"+Utility.getLocaleValuePropByKey("lblJobsApplied", locale)+"</h4></th>");
			sb.append("<th width='20%'>"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
			sb.append("<th width='13%'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");

			int rowCount=0;
			for(JobForTeacher jbTeacher : lstincompletJFT)
			{
				if(rowCount==4)
					break;
				rowCount++;								

				sb.append("<tr>");
				sb.append("<td><a href='applyjob.do?jobId="+jbTeacher.getJobId().getJobId()+"'>"+jbTeacher.getJobId().getJobTitle()+"</a></td>");
				sb.append("<td><strong class='text-error'>"+Utility.getLocaleValuePropByKey("optIncomlete", locale)+"</strong></td>");
				sb.append("<td>");				
				sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png'></a>");
				sb.append("</td>");	   				
				sb.append("</tr>");
			}

			for(JobForTeacher jbTeacher : lstcompletJFT)
			{
				if(rowCount==4)
					break;
				rowCount++;
				sb.append("<tr>");
				sb.append("<td> <a href='applyjob.do?jobId="+jbTeacher.getJobId().getJobId()+"'>"+jbTeacher.getJobId().getJobTitle()+"</a></td>");
				sb.append("<td><strong class='text-success'>"+Utility.getLocaleValuePropByKey("lblCompleted", locale)+"</strong></td>");
				sb.append("<td>");				
				sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png'></a>");
				sb.append("</td>");	   				
				sb.append("</tr>");
			}

			for(JobForTeacher jbTeacher : lstviolatedJFT)
			{
				if(rowCount==4)
					break;
				rowCount++;
				sb.append("<tr>");
				sb.append("<td> <a href='applyjob.do?jobId="+jbTeacher.getJobId().getJobId()+"'>"+jbTeacher.getJobId().getJobTitle()+"</a></td>");
				sb.append("<td><strong class='text-error'>"+Utility.getLocaleValuePropByKey("optTOut", locale)+"</strong></td>");
				sb.append("<td>");				
				sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("btnShare", locale)+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png'></a>");
				sb.append("</td>");	 				
				sb.append("</tr>");
			}
			
			for(JobForTeacher jbTeacher : lstWidrwJFT)
			{
				if(rowCount==4)
					break;
				rowCount++;
				sb.append("<tr>");
				sb.append("<td> <a href='applyjob.do?jobId="+jbTeacher.getJobId().getJobId()+"'>"+jbTeacher.getJobId().getJobTitle()+"</a></td>");
				sb.append("<td><strong class='text-error'>"+Utility.getLocaleValuePropByKey("optWithdrawn", locale)+"</strong></td>");
				sb.append("<td>");				
				sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("btnShare", locale)+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png'></a>");
				sb.append("</td>");	   				
				sb.append("</tr>");
			}

			sb.append("<tr>");
			sb.append("<td colspan='3' >"+Utility.getLocaleValuePropByKey("msgYouhave", locale)+" "
					+(lstcompletJFT.size()==0?"0":"<a href='jobsofinterest.do?searchby=comp'>"+lstcompletJFT.size()+"</a>")	+" "+Utility.getLocaleValuePropByKey("optCmplt", locale)+", "
					+(lstincompletJFT.size()==0?"0":"<a href='jobsofinterest.do?searchby=icomp'>"+lstincompletJFT.size()+"</a>")+" "+Utility.getLocaleValuePropByKey("optIncomlete", locale)+", "
					+(lstviolatedJFT.size()==0?"0":"<a href='jobsofinterest.do?searchby=vlt'>"+lstviolatedJFT.size()+"</a>")+" "+Utility.getLocaleValuePropByKey("optTOut", locale)+" and "
					+(lstWidrwJFT.size()==0?"0":"<a href='jobsofinterest.do?searchby=widrw'>"+lstWidrwJFT.size()+"</a>")+" "+Utility.getLocaleValuePropByKey("optWithdrawn", locale)+"");
			sb.append("<div class='right'><a href='jobsofinterest.do'><strong>"+Utility.getLocaleValuePropByKey("lblSeeAll", locale)+"</strong></a></div>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return sb.toString();
	}


	public List<JobOrder> getJobByPref(HttpServletRequest request,Order sortOrderStrVal)
	{	

		List<JobOrder> lstJobOrder = null;
		try
		{	
			List<String> lstRegionId=null;
			List<String> lstSchoolTypeId=null;
			List<String> lstGeographyId=null;
			List<SchoolMaster> lstSchoolMasters = null;
			List<SchoolMaster> lstSMtoRemove = new ArrayList<SchoolMaster>();



			HttpSession session = request.getSession();			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			TeacherPreference preference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);

			lstRegionId = Arrays.asList(preference.getRegionId().trim().split("\\|"));

			lstSchoolTypeId = Arrays.asList(preference.getSchoolTypeId().trim().split("\\|"));

			lstGeographyId = Arrays.asList(preference.getGeoId().trim().split("\\|"));



			if(sortOrderStrVal!=null)
			{
				lstJobOrder = jobOrderDAO.findSortedJobtoShow(sortOrderStrVal);
			}
			else
			{
				lstJobOrder = jobOrderDAO.findJobtoShow();
			}
			for(JobOrder jb:lstJobOrder)
			{
				lstSchoolMasters=jb.getSchool();
				if(lstSchoolMasters!=null)
				{			
					for(SchoolMaster school: lstSchoolMasters)
					{				
						if(lstGeographyId.contains(""+school.getGeoMapping().getGeoId().getGeoId()) && lstSchoolTypeId.contains(""+school.getSchoolTypeId().getSchoolTypeId() ) && lstRegionId.contains(""+school.getRegionId().getRegionId()))
						{
							//do nothing for now
						}
						else
						{
							//lstSMasters.remove(school);
							lstSMtoRemove.add(school);

						}

					}
					if(jb.getCreatedForEntity() == 3)
					{
						for(SchoolMaster school:lstSMtoRemove)
						{
							lstSchoolMasters.remove(school);
						}
						lstSMtoRemove.clear();
					}
				}

			}

			List<JobOrder> lstJbOdrToremove = new ArrayList<JobOrder>(lstJobOrder);
			for( JobOrder jb:lstJbOdrToremove)
			{

				if(jb.getCreatedForEntity() == 3)
				{
					if(jb.getSchool()==null || jb.getSchool().size()==0)
					{

						lstJobOrder.remove(jb);
					}
					else
					{

					}
				}
				else
				{
					if(!lstRegionId.contains(""+jb.getDistrictMaster().getStateId().getRegionId().getRegionId()))
					{
						lstJobOrder.remove(jb);
					}
				}
			}

		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
			lstJobOrder = new ArrayList<JobOrder>();
			
		}
		return lstJobOrder;
	}
	
}
