package tm.controller.teacher;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherPreference;
import tm.bean.master.GeographyMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.RegionMaster;
import tm.bean.master.SchoolTypeMaster;
import tm.bean.master.StateMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherPreferenceDAO;
import tm.dao.master.GeographyMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.RegionMasterDAO;
import tm.dao.master.SchoolTypeMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

/*
 * Preference related page url
 * 
 */

@Controller
public class UserPreferenceController 
{	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	@Autowired
	private GeographyMasterDAO geographyMasterDAO;
	public void setGeographyMasterDAO(GeographyMasterDAO geographyMasterDAO) {
		this.geographyMasterDAO = geographyMasterDAO;
	}
	
	@Autowired
	private RegionMasterDAO regionMasterDAO;
	public void setRegionMasterDAO(RegionMasterDAO regionMasterDAO) {
		this.regionMasterDAO = regionMasterDAO;
	}
	
	@Autowired
	private SchoolTypeMasterDAO schoolTypeMasterDAO;
	public void setSchoolTypeMasterDAO(SchoolTypeMasterDAO schoolTypeMasterDAO) {
		this.schoolTypeMasterDAO = schoolTypeMasterDAO;
	}
	
	@Autowired
	private TeacherPreferenceDAO teacherPreferenceDAO;
	public void setTeacherPreferenceDAO
	(
			TeacherPreferenceDAO teacherPreferenceDAO) {
		this.teacherPreferenceDAO = teacherPreferenceDAO;
	}

	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	@InitBinder
	public void initBinder(HttpServletRequest request,DataBinder binder) 
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true,10));
	}
	
	String locale = Utility.getValueOfPropByKey("locale");
	String kellyUrlValue = Utility.getLocaleValuePropByKey("kellyUrlValue", locale);
	
	@RequestMapping(value="/userpreference.do", method=RequestMethod.GET)
	public String doSignUpGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}		
		
		RegionMaster regionMaster = null;
		
		List<RegionMaster> lstPairentRegion=new ArrayList<RegionMaster>();
		
		Iterator<RegionMaster> iterator = null;
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		
		boolean isKellyJobApply = false;
        try {
            isKellyJobApply = (Boolean)session.getAttribute("isKelly");//jobForTeacherDAO.getJFTByTeachers(teacherDetail);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>  :: "+isKellyJobApply);
		try 
		{
			/*List<GeographyMaster> lstGeographyMasters = geographyMasterDAO.findAll();
			List<RegionMaster> lstRegionMasters = regionMasterDAO.findAll();
			List<SchoolTypeMaster> lstSchoolTypeMasters = schoolTypeMasterDAO.findAll();*/
			
			List<GeographyMaster> lstGeographyMasters = geographyMasterDAO.getActiveGeography();
			List<RegionMaster> lstRegionMasters = regionMasterDAO.getActiveRegions();
			for(RegionMaster obj :lstRegionMasters){
				System.out.println("regionNmae:: "+obj.getRegionName());
			}
			List<SchoolTypeMaster> lstSchoolTypeMasters = schoolTypeMasterDAO.getActiveSchoolTypes();
			
			iterator = lstRegionMasters.iterator();
			while (iterator.hasNext()) 
			{
				regionMaster = iterator.next();
				if(regionMaster.getParentRegionId().equals(new Integer(0)))
				{
					lstPairentRegion.add(regionMaster);
				}				
			}
			
			TeacherPreference teacherPreference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
			
			Boolean kellyUrl = false;
			if(request.getRequestURL().toString().contains(kellyUrlValue) || request.getRequestURL().toString().contains("kelly") ||  request.getServerName().toLowerCase().contains("smartpractice") || request.getRequestURL().toString().contains("smartpractice")){
				kellyUrl = true;
			}
			
			int onOffRegion=0; 
			String basepath = Utility.getValueOfPropByKey("basePath");
			if(basepath.equalsIgnoreCase("https://canada-en-test.teachermatch.org/")){
				onOffRegion=1;
			}
			map.addAttribute("lstGeographyMasters", lstGeographyMasters);
			map.addAttribute("lstRegionMasters", lstRegionMasters);
			map.addAttribute("lstSchoolTypeMasters", lstSchoolTypeMasters);
			map.addAttribute("lstPairentRegion", lstPairentRegion);
			map.addAttribute("teacherPreference", teacherPreference);
		    map.addAttribute("onOffRegion", onOffRegion);
			if(isKellyJobApply || kellyUrl){
				List<StateMaster> lstStateMasters=new ArrayList<StateMaster>();
				List<JobCategoryMaster> lstJobCategoryMasters= new ArrayList<JobCategoryMaster>();
				
				lstStateMasters=stateMasterDAO.findAllActiveState();
				map.addAttribute("lstStateMasters",lstStateMasters);
				
				lstJobCategoryMasters=jobCategoryMasterDAO.findActiveHeadQuarterJobCategory();
				map.addAttribute("lstJobCategoryMasters",lstJobCategoryMasters);
				
				map.addAttribute("hide1","hide");
				map.addAttribute("hide2","");
				map.addAttribute("isKelly",1);
			}else{
				map.addAttribute("hide1","");
				map.addAttribute("hide2","hide");
				map.addAttribute("isKelly",0);
			}
			
			// kellyUrlValue = "localhost";
			String jobId ="";
			if(kellyUrl){
				jobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
				map.addAttribute("jobId", jobId);
			}
			map.addAttribute("kellyUrl", kellyUrl);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "userpreference";
	}
	
	@RequestMapping(value="/savepreference.do", method=RequestMethod.POST)
	public String savePreferencePOST(ModelMap map,HttpServletRequest request)
	{
		
		HttpSession session = request.getSession(false);
		String redirectUrl="redirect:userdashboard.do";
		String flag="epiFlag=on";
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		//kellyUrlValue = "localhost";
		try 
		{
			String isaffilatedstatus = request.getParameter("isaffilatedstatus")==null?"0":request.getParameter("isaffilatedstatus").trim();
			String jobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
			String ok_cancelflag = request.getParameter("ok_cancelflag")==null?"0":request.getParameter("ok_cancelflag").trim();
			String epimsjstatus = request.getParameter("epimsjstatus")==null?"":request.getParameter("epimsjstatus").trim();
			System.out.println("==epimsjstatus"+epimsjstatus+" = isaffilatedstatus ==="+isaffilatedstatus+" === jobId ==="+jobId+"=== ok_cancelflag ==== "+ok_cancelflag);
			
			String[] geo = request.getParameterValues("geo");
			String[] schoolType = request.getParameterValues("schoolType");
			String[] regions =request.getParameterValues("regions");

			//preferences for kelly
			String[] jobCategory =request.getParameterValues("jobCategory");
			String[] states =request.getParameterValues("state");
			
			String geoId="";
			String schoolTypeId="";
			String regionsId="";
			String jobCategoryId="";
			String stateId="";
			
			String saveJobFlagForkelly=request.getParameter("savejobFlag")==null?"0":request.getParameter("savejobFlag").trim();
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			
			
			TeacherPreference teacherPreference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
			
			if(geo!=null)
			{
				for(String ge:geo)
				{
					geoId=geoId+ge+"|";					
				}
				geoId=geoId.substring(0, geoId.length()-1);
			}
			
			if(schoolType!=null)
			{
				for(String st:schoolType)
				{
					schoolTypeId = schoolTypeId+st+"|";					
				}
				schoolTypeId=schoolTypeId.substring(0, schoolTypeId.length()-1);
			}
			
			if(regions!=null)
			{
				
				for(String rg:regions)
				{
					regionsId=regionsId+rg+"|";					
				}
				regionsId=regionsId.substring(0, regionsId.length()-1);
				
			}

			//for Kelly user
			if(jobCategory!=null)
			{
				jobCategoryId="|";
				for(String cat:jobCategory)
				{
					jobCategoryId = jobCategoryId+cat+"|";					
				}
				//jobCategoryId=jobCategoryId.substring(0, jobCategoryId.length()-1);
			}
			if(states!=null)
			{
				stateId = "|";
				for(String sat:states)
				{
					stateId = stateId+sat+"|";					
				}
				//stateId=stateId.substring(0, stateId.length()-1);
			}
			
			if(teacherPreference==null)
			{
				teacherPreference = new TeacherPreference();
				teacherPreference.setCreatedDateTime(new Date());
				
				try {
					if(checkBaseComplete(request, teacherDetail))
					{
						System.out.println("\n\n\n\n\n :  User Preference Controller -------------------------------------------------------------------------------------");
						System.out.println("========================= $$$$$$$$$$$$ checking Portfolio for Normal User User Preference Controller ==== "+teacherDetail.getIsPortfolioNeeded());
						
						
						List<JobForTeacher> lstJobForTeacher= null;
						lstJobForTeacher = jobForTeacherDAO.findIncompleteJoborderforwhichBaseIsRequired(teacherDetail,WorkThreadServlet.statusMap.get("icomp"));
						System.out.println(" ======= Total No of Job in current financial year whose status  is incomplete ======"+lstJobForTeacher.size());
						//System.out.println(" ==== isAffilated==0 means Show EPI Incomplete Maessage because isAffilated  check is false");
						
						if(lstJobForTeacher.size()>0) /*===== Total No of Job in current financial year whose status  is incomplete ==== */
						{
							if(Integer.parseInt(ok_cancelflag)==0  && session.getAttribute("referer")!=null) 
							{
								System.out.println(" refer"+session.getAttribute("referer"));
								Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
								 jobId = referer.get("jobId");
									JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
									if(jobOrder!=null && jobOrder.getJobCategoryMaster().getBaseStatus())
									{
										System.out.println(" isAffilated check is false  and Base status is Requred and Base is Complete while All job's status is complete ====");
										map.addAttribute("epimsjstatus", "icomp");
									}
							}
							else
							{
								if(session.getAttribute("referer")==null && Integer.parseInt(ok_cancelflag)==1)
								{
									map.addAttribute("epimsjstatus", "icomp");
								}
							}
						}
						
						
						
						if(teacherDetail.getIsPortfolioNeeded()==true)
						{
							TeacherPortfolioStatus portfolioStatus = null;
							portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
							if(isaffilatedstatus!="")
							if(Integer.parseInt(isaffilatedstatus)==1)
							{
								map.addAttribute("ok_cancelflag", Integer.parseInt(ok_cancelflag));
								map.addAttribute("jobId", Integer.parseInt(jobId));
							}
							
							flag=flag+"&isaffilatedstatus="+isaffilatedstatus+"&jobId="+jobId+"&ok_cancelflag="+ok_cancelflag;
							
							System.out.println(" flag  ===== dsfg;kd;gkdf;g ;df "+flag);
							if(portfolioStatus!=null && portfolioStatus.getIsPersonalInfoCompleted() && portfolioStatus.getIsAcademicsCompleted() && portfolioStatus.getIsCertificationsCompleted() && portfolioStatus.getIsExperiencesCompleted() && portfolioStatus.getIsAffidavitCompleted())
							{
								System.out.println("Portfolio Completd");
								map.addAttribute("portfolioStatus", "");
								
								if(lstJobForTeacher.size()>0) /*===== Total No of Job in current financial year whose status  is incomplete ==== */
								{
									System.out.println(" isAffilated "+Integer.parseInt(ok_cancelflag)+"=== referrer "+session.getAttribute("referer")+"= Portfolio Completd lstJobForTeacher.size()  "+lstJobForTeacher.size());
									if(Integer.parseInt(ok_cancelflag)==0  && session.getAttribute("referer")!=null) 
									{
										System.out.println(" portfolio redirect url "+session.getAttribute("referer"));
										flag=flag+"&epimsjstatus="+"icomp";
									}
									else
									{
										if(session.getAttribute("referer")==null)
										{
											System.out.println(" else block ----------");
											flag=flag+"&epimsjstatus="+"icomp";
										}
											
									}
								}
								redirectUrl=redirectUrl+"?"+flag;
							}
							else
							{
								System.out.println(" Portfolio required  sxgsdf");
								map.addAttribute("portfolioStatus", "required");
								if(lstJobForTeacher.size()>0) /*===== Total No of Job in current financial year whose status  is incomplete ==== */
								{
									System.out.println(" isAffilated "+Integer.parseInt(ok_cancelflag)+"=== referrer "+session.getAttribute("referer")+"= Portfolio Completd lstJobForTeacher.size()  "+lstJobForTeacher.size());
									if(Integer.parseInt(ok_cancelflag)==0  && session.getAttribute("referer")!=null) 
									{
										System.out.println(" portfolio redirect url "+session.getAttribute("referer"));
										flag=flag+"&epimsjstatus="+"icomp";
									}
									else
									{
										if(session.getAttribute("referer")==null)
										{
											System.out.println(" else block ----------");
											flag=flag+"&epimsjstatus="+"icomp";
										}
											
									}
								}
								redirectUrl=redirectUrl+"?"+flag;
							}
							System.out.println(" \n\n User Preference Controller  redirectUrl : "+redirectUrl);
						}
						//redirectUrl=redirectUrl+"?"+flag;
					}
					else
					{
						System.out.println(" =============setting value in map isaffilatedstatus "+isaffilatedstatus);
						System.out.println("User Preference Controller: Base (EPI) status is other than Complete");
						if(teacherDetail.getIsPortfolioNeeded()==true)
						{
							TeacherPortfolioStatus portfolioStatus = null;
							portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
							
							if(portfolioStatus!=null && portfolioStatus.getIsPersonalInfoCompleted() && portfolioStatus.getIsAcademicsCompleted() && portfolioStatus.getIsCertificationsCompleted() && portfolioStatus.getIsExperiencesCompleted() && portfolioStatus.getIsAffidavitCompleted())
							{
								System.out.println("Portfolio Completd");
								//map.addAttribute("portfolioStatus", "");
								//redirectUrl=redirectUrl+"?"+flag;
							}
							else
							{
								System.out.println(" Portfolio required");
								//map.addAttribute("portfolioStatus", "required");
								//redirectUrl=redirectUrl+"?"+flag;
							}
						}
						
						if(Integer.parseInt(isaffilatedstatus)==1)
							map.addAttribute("isaffilatedstatus",isaffilatedstatus);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			System.out.println("========== redirectUrl ========="+redirectUrl);
						
			teacherPreference.setGeoId(geoId);
			teacherPreference.setRegionId(regionsId);
			teacherPreference.setSchoolTypeId(schoolTypeId);
			teacherPreference.setJobCategoryId(jobCategoryId);
			teacherPreference.setStateId(stateId);
			teacherPreference.setTeacherId(teacherDetail);
			
			teacherPreferenceDAO.makePersistent(teacherPreference);
			session.removeAttribute("redirectTopref"); // now page will redirect to every page
			System.out.println("sdfdfdf: "+jobId.equals(""));
			if(!jobId.trim().equals(""))
			{
				if(jobId.trim().length()>0)
				if(Integer.parseInt(jobId)>0)
				{
					JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
					JobForTeacher jobForTeacher =  jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
					if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2)
					{
						String ncInternalFlag =(String)(session.getAttribute("Currnrt")==null?"":session.getAttribute("Currnrt"));
						String ncInternalSSN =(String)(session.getAttribute("ssnNcInternal")==null?"":session.getAttribute("ssnNcInternal"));
						map.addAttribute("ncInternalFlag", ncInternalFlag);
						map.addAttribute("ncInternalSSN", ncInternalSSN);
						
					}
					if(jobForTeacher!=null)
					{
						map.addAttribute("pShowFlg", 1);
						map.addAttribute("savejobFlag", 1);
					}
				}
			}
			
			if(request.getRequestURL().toString().contains(kellyUrlValue) && saveJobFlagForkelly!=null && saveJobFlagForkelly.length()>0 && !saveJobFlagForkelly.equals("0")) {
				map.addAttribute("savejobFlag", saveJobFlagForkelly);
			    map.addAttribute("ok_cancelflag", Integer.parseInt(ok_cancelflag));
			    map.addAttribute("jobId", Integer.parseInt(jobId));
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return redirectUrl;
	}

	public boolean checkBaseComplete(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		System.out.println("=User Preference Controller======== checkBaseComplete ========");
		boolean baseInvStatus = false;
		TeacherAssessmentStatus teacherBaseAssessmentStatus = null;
		teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
		if(teacherBaseAssessmentStatus!=null && (teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") ))
		{
			baseInvStatus=true;
		}
		
		if(baseInvStatus)
		{		
			return false;
		}
		else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
		{
			return false;
		}
		else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
		{
			return true;
		}
		else 
		{
			return true;
		}
	}
}
