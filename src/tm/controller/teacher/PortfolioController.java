package tm.controller.teacher;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.JobForTeacher;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAffidavit;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.master.CertificationStatusMaster;
import tm.bean.master.CertificationTypeMaster;
import tm.bean.master.CityMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.CurrencyMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.EmpRoleTypeMaster;
import tm.bean.master.EthinicityMaster;
import tm.bean.master.EthnicOriginMaster;
import tm.bean.master.FieldMaster;
import tm.bean.master.GenderMaster;
import tm.bean.master.OrgTypeMaster;
import tm.bean.master.PeopleRangeMaster;
import tm.bean.master.RaceMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.TFAAffiliateMaster;
import tm.bean.master.TFARegionMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAffidavitDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.master.CertificationStatusMasterDAO;
import tm.dao.master.CertificationTypeMasterDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.CurrencyMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.EmpRoleTypeMasterDAO;
import tm.dao.master.EthinicityMasterDAO;
import tm.dao.master.EthnicOriginMasterDAO;
import tm.dao.master.FieldMasterDAO;
import tm.dao.master.FieldOfStudyMasterDAO;
import tm.dao.master.GenderMasterDAO;
import tm.dao.master.OrgTypeMasterDAO;
import tm.dao.master.PeopleRangeMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.TFAAffiliateMasterDAO;
import tm.dao.master.TFARegionMasterDAO;
import tm.dao.master.UniversityMasterDAO;
import tm.propertyeditor.CityPropertyEditor;
import tm.propertyeditor.CountryPropertyEditor;
import tm.propertyeditor.GenderPropertyEditor;
import tm.propertyeditor.RacePropertyEditor;
import tm.propertyeditor.StatePropertyEditor;
import tm.utility.Utility;

/*
 * 
 * Portfolio related page url
 * like personal info, academics, certification etc.. 
 */
@Controller
public class PortfolioController 
{
	@Autowired
	public CertificationTypeMasterDAO certificationTypeMasterDAO;
	
	@Autowired
	public EthinicityMasterDAO ethinicityMasterDAO;
	
	@Autowired
	public EthnicOriginMasterDAO ethnicOriginMasterDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	
	@Autowired
	private CurrencyMasterDAO currencyMasterDAO;
	public void setCurrencyMasterDAO(CurrencyMasterDAO currencyMasterDAO) {
		this.currencyMasterDAO = currencyMasterDAO;
	}
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	public void setRaceMasterDAO(RaceMasterDAO raceMasterDAO) {
		this.raceMasterDAO = raceMasterDAO;
	}
	@Autowired
	private GenderMasterDAO genderMasterDAO;
	public void setGenderMasterDAO(GenderMasterDAO genderMasterDAO) {
		this.genderMasterDAO = genderMasterDAO;
	}
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO) {
		this.cityMasterDAO = cityMasterDAO;
	}
	
	@Autowired
	private StatePropertyEditor statePropertyEditor;	
	public void setStatePropertyEditor(StatePropertyEditor statePropertyEditor) {
		this.statePropertyEditor = statePropertyEditor;
	}
	
	@Autowired
	private CountryPropertyEditor countryPropertyEditor;	
	public void setCountryPropertyEditor(
			CountryPropertyEditor countryPropertyEditor) {
		this.countryPropertyEditor = countryPropertyEditor;
	}
	
	@Autowired
	private CityPropertyEditor cityPropertyEditor;
	public void setCityPropertyEditor(CityPropertyEditor cityPropertyEditor) 
	{
		this.cityPropertyEditor = cityPropertyEditor;
	}
	
	@Autowired
	private RacePropertyEditor racePropertyEditor;	
	public void setRacePropertyEditor(RacePropertyEditor racePropertyEditor) {
		this.racePropertyEditor = racePropertyEditor;
	}
	
	@Autowired
	private GenderPropertyEditor genderPropertyEditor;	
	public void setGenderPropertyEditor(GenderPropertyEditor genderPropertyEditor) {
		this.genderPropertyEditor = genderPropertyEditor;
	}
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(TeacherPersonalInfoDAO teacherPersonalInfoDAO) 
	{
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	
	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	public void setTeacherAcademicsDAO(TeacherAcademicsDAO teacherAcademicsDAO) {
		this.teacherAcademicsDAO = teacherAcademicsDAO;
	}
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) 
	{
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}
	
	@Autowired
	private TeacherAffidavitDAO teacherAffidavitDAO;
	public void setTeacherAffidavitDAO(TeacherAffidavitDAO teacherAffidavitDAO) 
	{
		this.teacherAffidavitDAO = teacherAffidavitDAO;
	}
	
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	public void setTeacherExperienceDAO(TeacherExperienceDAO teacherExperienceDAO) 
	{
		this.teacherExperienceDAO = teacherExperienceDAO;
	}
	
	@Autowired
	private FieldOfStudyMasterDAO fieldOfStudyMasterDAO;
	public void setFieldOfStudyMasterDAO(FieldOfStudyMasterDAO fieldOfStudyMasterDAO) 
	{
		this.fieldOfStudyMasterDAO = fieldOfStudyMasterDAO;
	}
	
	@Autowired
	private FieldMasterDAO fieldMasterDAO;
	public void setFieldMasterDAO(FieldMasterDAO fieldMasterDAO) 
	{
		this.fieldMasterDAO = fieldMasterDAO;
	}
	
	@Autowired
	private EmpRoleTypeMasterDAO empRoleTypeMasterDAO;
	public void setEmpRoleTypeMasterDAO(EmpRoleTypeMasterDAO empRoleTypeMasterDAO) 
	{
		this.empRoleTypeMasterDAO = empRoleTypeMasterDAO;
	}
	
	@Autowired
	private OrgTypeMasterDAO orgTypeMasterDAO;
	public void setOrgTypeMasterDAO(OrgTypeMasterDAO orgTypeMasterDAO) 
	{
		this.orgTypeMasterDAO = orgTypeMasterDAO;
	}
	
	@Autowired
	private PeopleRangeMasterDAO peopleRangeMasterDAO;
	public void setPeopleRangeMasterDAO(PeopleRangeMasterDAO peopleRangeMasterDAO) 
	{
		this.peopleRangeMasterDAO = peopleRangeMasterDAO;
	}
	
	@Autowired
	private UniversityMasterDAO universityMasterDAO;
	public void setUniversityMasterDAO(UniversityMasterDAO universityMasterDAO) 
	{
		this.universityMasterDAO = universityMasterDAO;
	}
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO)
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	
	@Autowired
	private TFAAffiliateMasterDAO tfaAffiliateMasterDAO;
	public void setTfaAffiliateMasterDAO(TFAAffiliateMasterDAO tfaAffiliateMasterDAO) {
		this.tfaAffiliateMasterDAO = tfaAffiliateMasterDAO;
	}
	
	@Autowired
	private TFARegionMasterDAO tfaRegionMasterDAO;
	public void setTfaRegionMasterDAO(TFARegionMasterDAO tfaRegionMasterDAO) {
		this.tfaRegionMasterDAO = tfaRegionMasterDAO;
	}

	
	@Autowired
	private CertificationStatusMasterDAO certificationStatusMasterDAO;
	public void setCertificationStatusMasterDAO(
			CertificationStatusMasterDAO certificationStatusMasterDAO) {
		this.certificationStatusMasterDAO = certificationStatusMasterDAO;
	}
	
	@Autowired
	private CountryMasterDAO countryMasterDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@InitBinder
	public void initBinder(HttpServletRequest request,DataBinder binder) 
	{
		
		binder.registerCustomEditor(StateMaster.class, statePropertyEditor);
		binder.registerCustomEditor(CityMaster.class, cityPropertyEditor);
		binder.registerCustomEditor(RaceMaster.class, racePropertyEditor);
		binder.registerCustomEditor(GenderMaster.class, genderPropertyEditor);
		binder.registerCustomEditor(CountryMaster.class, countryPropertyEditor);
	}
	
	@RequestMapping(value="/portfolio.do", method=RequestMethod.GET)
	public String doPortfolioGET(ModelMap map,HttpServletRequest request)
	{
		
		try 
		{
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("teacherDetail") == null) 
			{
				return "redirect:index.jsp";
			}	
			
			if(session.getAttribute("isImCandidate")!=null)
			{
				return "redirect:myportfolio.do";
			}
			
			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
			boolean bTeacherPortfolioStatus=false;
			TeacherPortfolioStatus portfolioStatus = null;
			portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			
			if(portfolioStatus==null)
			{
				bTeacherPortfolioStatus=true;
				portfolioStatus = new TeacherPortfolioStatus();
				portfolioStatus.setTeacherId(teacherDetail);
				portfolioStatus.setIsPersonalInfoCompleted(false);
				portfolioStatus.setIsAcademicsCompleted(false);
				portfolioStatus.setIsCertificationsCompleted(false);
				portfolioStatus.setIsExperiencesCompleted(false);
				portfolioStatus.setIsAffidavitCompleted(false);
				
				//teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				TeacherPortfolioStatus tpsTemp=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				if(bTeacherPortfolioStatus && tpsTemp==null)
					teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				else
				{
					portfolioStatus.setId(tpsTemp.getId());
					teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				}
				
				return "redirect:personalinfo.do";
				
			}
			
			if(!portfolioStatus.getIsPersonalInfoCompleted())
			{
				return "redirect:personalinfo.do";
			}
			else if(!portfolioStatus.getIsAcademicsCompleted())
			{
				return "redirect:academics.do";
			}
			else if(!portfolioStatus.getIsCertificationsCompleted())
			{
				return "redirect:certification.do";
			}
			else if(!portfolioStatus.getIsExperiencesCompleted())
			{
				return "redirect:experiences.do";
			}
			else if(!portfolioStatus.getIsAffidavitCompleted())
			{
				return "redirect:affidavit.do";
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "redirect:personalinfo.do";
	}
	
	
	
	@RequestMapping(value="/personalinfo.do", method=RequestMethod.GET)
	public String doPersonalInfoGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}		
		
		List<StateMaster> listStateMasters = null;
		List<CityMaster> listCityMasters = null;
		TeacherPersonalInfo teacherpersonalinfo = null;
		TeacherDetail tDetail = null;
		
		List<EthnicOriginMaster> lstEthnicOriginMasters=null;
		List<EthinicityMaster> lstEthinicityMasters=null;
		List<RaceMaster> listRaceMasters = null;
		List<GenderMaster> listGenderMasters = null;
		
		
		try 
		{
			
			tDetail =(TeacherDetail) session.getAttribute("teacherDetail");
			
			teacherpersonalinfo = teacherPersonalInfoDAO.findById(tDetail.getTeacherId(), false, false);					
			
			if(teacherpersonalinfo!=null)
			{	try{
					String[] splitRaceId = teacherpersonalinfo.getRaceId().split(",");
					map.addAttribute("splitRaceId",splitRaceId);
				}catch(Exception e){}
			}
			
			try
			{
				if(teacherpersonalinfo!=null)
				{
					
					if(teacherpersonalinfo.getPhoneTypeUSNonUS()!=null){
						map.addAttribute("PhoneTypeUSNonUS", teacherpersonalinfo.getPhoneTypeUSNonUS());
					}
					if(teacherpersonalinfo.getPhoneTypeUSNonUS()!=null && teacherpersonalinfo.getPhoneTypeUSNonUS()==1){
						if(teacherpersonalinfo.getPhoneNumber()!=null && !teacherpersonalinfo.getPhoneNumber().equals(""))
						{	
							map.addAttribute("phonNumber", teacherpersonalinfo.getPhoneNumber());
							map.addAttribute("ph1", teacherpersonalinfo.getPhoneNumber().subSequence(0, 3));
							map.addAttribute("ph2", teacherpersonalinfo.getPhoneNumber().subSequence(3, 6));
							map.addAttribute("ph3", teacherpersonalinfo.getPhoneNumber().subSequence(6, 10));
						}else
						{
							map.addAttribute("ph1", "");
							map.addAttribute("ph2", "");
							map.addAttribute("ph3", "");
						}
					}else{
						if(teacherpersonalinfo.getPhoneNumber()!=null && !teacherpersonalinfo.getPhoneNumber().equals(""))
						{	
							map.addAttribute("phonNumber", teacherpersonalinfo.getPhoneNumber());
							map.addAttribute("phonNumber1", teacherpersonalinfo.getPhoneNumber().subSequence(0, 3));
							//map.addAttribute("phonNumber2", teacherpersonalinfo.getPhoneNumber().subSequence(3, 6));
							map.addAttribute("phonNumber3", teacherpersonalinfo.getPhoneNumber().subSequence(3, (teacherpersonalinfo.getPhoneNumber().length())));
						}else
						{
							map.addAttribute("phonNumber1", "");
							//map.addAttribute("phonNumber2", "");
							map.addAttribute("phonNumber3", "");
						}
						
					}
					
					if(teacherpersonalinfo.getMobileNumber()!=null && !teacherpersonalinfo.getMobileNumber().equals(""))
					{
						map.addAttribute("mb1", teacherpersonalinfo.getMobileNumber().subSequence(0, 3));
						map.addAttribute("mb2", teacherpersonalinfo.getMobileNumber().subSequence(3, 6));
						map.addAttribute("mb3", teacherpersonalinfo.getMobileNumber().subSequence(6, 10));
					}else
					{
						map.addAttribute("mb1", "");
						map.addAttribute("mb2", "");
						map.addAttribute("mb3", "");
					}
					
				}	
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			
			
			if(teacherpersonalinfo==null)
			{
				teacherpersonalinfo = new TeacherPersonalInfo();
				teacherpersonalinfo.setTeacherId(tDetail.getTeacherId());
				teacherpersonalinfo.setFirstName(tDetail.getFirstName());
				teacherpersonalinfo.setLastName(tDetail.getLastName());
			}
			else
			{
				if(teacherpersonalinfo.getStateId()!=null)
				{
					if(teacherpersonalinfo.getZipCode()!=null && teacherpersonalinfo.getZipCode()!="")
						listCityMasters = cityMasterDAO.findCityByState(teacherpersonalinfo.getStateId(),teacherpersonalinfo.getZipCode());
					else
						listCityMasters = cityMasterDAO.findCityByState(teacherpersonalinfo.getStateId());
				}
			}
			TeacherDetail teacherDetail=teacherDetailDAO.findById(tDetail.getTeacherId(), false,false);
			
			listRaceMasters = raceMasterDAO.findAllRaceByOrder();
			listGenderMasters = genderMasterDAO.findAllGenderByOrder();
			listStateMasters = stateMasterDAO.findAllStateByOrder();
			
			lstEthinicityMasters=ethinicityMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
			lstEthnicOriginMasters=ethnicOriginMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
			
			List<CountryMaster> countryMasters=new ArrayList<CountryMaster>();
			countryMasters=countryMasterDAO.findAllActiveCountry();
			
			map.addAttribute("lstEthinicityMasters", lstEthinicityMasters);
			map.addAttribute("lstEthnicOriginMasters", lstEthnicOriginMasters);
			
			map.addAttribute("teacherDetail", teacherDetail);
			map.addAttribute("listGenderMasters", listGenderMasters);
			map.addAttribute("listRaceMasters", listRaceMasters);
			map.addAttribute("listStateMasters", listStateMasters);
			map.addAttribute("listCityMasters", listCityMasters);	
			map.addAttribute("teacherpersonalinfo", teacherpersonalinfo);
			String locale = Utility.getValueOfPropByKey("locale");
			String basepath = Utility.getValueOfPropByKey("basePath");
			if(basepath.equalsIgnoreCase("https://canada-en-test.teachermatch.org/")){
				map.addAttribute("lblState","Province");
			}else{
				map.addAttribute("lblState", Utility.getLocaleValuePropByKey("lblSt", locale));
			}

			if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr"))
			{
					map.addAttribute("isPortfolioNeeded", false);
					map.addAttribute("isExpectedSalNeeded", false);
			}
			else
			{
				map.addAttribute("isPortfolioNeeded", getTeacherPortfolioStatusForNativeAndRefered(teacherDetail));
				map.addAttribute("isExpectedSalNeeded", true);
			}
			
			map.addAttribute("listCountryMaster", countryMasters);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "personalinfo";
	}
	
	@RequestMapping(value="/academics.do", method=RequestMethod.GET)
	public String doAcademicsGET(ModelMap map,HttpServletRequest request)
	{
		
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		List<TeacherAcademics> lstTeacherAcademics = teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
		
		String otherFieldOfStudyId = "";
		String otherUniversityId = "";
		try 
		{
			otherFieldOfStudyId = ""+fieldOfStudyMasterDAO.findFieldOfStuddyByName("Other").get(0).getFieldId();
			otherUniversityId = ""+universityMasterDAO.findUniversityByName("Other").get(0).getUniversityId();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		map.addAttribute("lstTeacherAcademics", lstTeacherAcademics);
		map.addAttribute("lstLastYear", Utility.getLasterYearByYear(1965));
		
		map.addAttribute("otherFieldOfStudyId", otherFieldOfStudyId);
		map.addAttribute("otherUniversityId", otherUniversityId);
		boolean isFR = true;
		if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr"))
			isFR = false;

		map.addAttribute("isFR", isFR);	
		return "academics";
	}
	
	
	@RequestMapping(value="/personalinfo.do",method=RequestMethod.POST)
	public String doSignUpPOST(@ModelAttribute(value="teacherpersonalinfo") TeacherPersonalInfo teacherpersonalinfo,BindingResult result, ModelMap map,HttpServletRequest request)
	{		
		try 
		{
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("teacherDetail") == null) 
			{
				return "redirect:index.jsp";
			}
			
			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
			

			TeacherPersonalInfo teacherPersonalInfo2=teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
			if(teacherPersonalInfo2!=null)
			{
				if(teacherPersonalInfo2.getMiddleName()!=null && !teacherPersonalInfo2.getMiddleName().equalsIgnoreCase(""))
					teacherpersonalinfo.setMiddleName(teacherPersonalInfo2.getMiddleName());
				
				if(teacherPersonalInfo2.getSSN()!=null && !teacherPersonalInfo2.getSSN().equalsIgnoreCase(""))
					teacherpersonalinfo.setSSN(teacherPersonalInfo2.getSSN());
				
				if(teacherPersonalInfo2.getDob()!=null)
					teacherpersonalinfo.setDob(teacherPersonalInfo2.getDob());
				
				if(teacherPersonalInfo2.getRetirementdate()!=null)
					teacherpersonalinfo.setRetirementdate(teacherPersonalInfo2.getRetirementdate());
				
				if(teacherPersonalInfo2.getMoneywithdrawaldate()!=null)
					teacherpersonalinfo.setMoneywithdrawaldate(teacherPersonalInfo2.getMoneywithdrawaldate());
				
				if(teacherPersonalInfo2.getEmployeeType()!=null)
					teacherpersonalinfo.setEmployeeType(teacherPersonalInfo2.getEmployeeType());
				
				if(teacherPersonalInfo2.getEmployeeNumber()!=null)
					teacherpersonalinfo.setEmployeeNumber(teacherPersonalInfo2.getEmployeeNumber());
				
				if(teacherPersonalInfo2.getIsCurrentFullTimeTeacher()!=null)
					teacherpersonalinfo.setIsCurrentFullTimeTeacher(teacherPersonalInfo2.getIsCurrentFullTimeTeacher());;
				
				if(teacherPersonalInfo2.getIsVateran()!=null)
					teacherpersonalinfo.setIsVateran(teacherPersonalInfo2.getIsVateran());
				
				
				
			}
			
			teacherpersonalinfo.setIsDone(true);
			teacherpersonalinfo.setCreatedDateTime(new Date());
			
			try
			{
				if(request.getParameter("ethnicOriginId")!=null && !request.getParameter("ethnicOriginId").equals(""))
				{
					Integer ethOrgId=Integer.parseInt(request.getParameter("ethnicOriginId"));
					if( ethOrgId>0){
						EthnicOriginMaster ethnicOriginMaster=ethnicOriginMasterDAO.findById(ethOrgId, false, false);
						teacherpersonalinfo.setEthnicOriginId(ethnicOriginMaster);
					}
				}
				
				if(request.getParameter("ethnicityId")!=null && !request.getParameter("ethnicityId").equals(""))
				{
					Integer ethMId=Integer.parseInt(request.getParameter("ethnicityId"));
					if( ethMId>0){
						EthinicityMaster ethinicityMaster=ethinicityMasterDAO.findById(ethMId, false, false);
						teacherpersonalinfo.setEthnicityId(ethinicityMaster);
					}
				}
				
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			if(teacherpersonalinfo.getRaceId()==null){
				RaceMaster raceMaster=raceMasterDAO.findById(0, false, false);
				teacherpersonalinfo.setRaceId(String.valueOf(raceMaster.getRaceId()));
			}else{
				String[] checkedRaceIds = request.getParameterValues("raceId");
				
				String raceIdsvalue="";				
				for(int i=0;i<checkedRaceIds.length;i++){
					raceIdsvalue+=checkedRaceIds[i]+",";
				}
				raceIdsvalue = raceIdsvalue.substring(0, raceIdsvalue.length() - 1);
				teacherpersonalinfo.setRaceId(raceIdsvalue);
				 System.out.println(raceIdsvalue);
			}
			
			if(teacherpersonalinfo.getGenderId()==null){
				GenderMaster genderMaster=genderMasterDAO.findById(0, false, false);
				teacherpersonalinfo.setGenderId(genderMaster);
			}
			
			try{
				if(teacherpersonalinfo.getCountryId().getCountryId()>0)
				{
					CountryMaster countryMaster  = countryMasterDAO.findById(teacherpersonalinfo.getCountryId().getCountryId(), false, false);
					List<StateMaster> lstMasters = stateMasterDAO.findActiveStateByCountryId(countryMaster);
					
					if(teacherpersonalinfo.getCountryId().getCountryId()==223)
					{
						teacherpersonalinfo.setOtherState(null);
						teacherpersonalinfo.setOtherCity(null);
					}
					else
					{
						if(lstMasters!=null && lstMasters.size()>0)
						{
							teacherpersonalinfo.setOtherState(null);
							teacherpersonalinfo.setCityId(null);
						}
						else
						{
							teacherpersonalinfo.setStateId(null);
							teacherpersonalinfo.setCityId(null);
						}
					}
				}
				
				//teacherpersonalinfo
				
				teacherDetail.setFirstName(teacherpersonalinfo.getFirstName());
				teacherDetail.setLastName(teacherpersonalinfo.getLastName());				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			System.out.println("phoneTypeUSNonUS"+request.getParameter("phoneTypeUSNonUS"));
			
			teacherpersonalinfo.setPhoneTypeUSNonUS(Integer.parseInt(request.getParameter("phoneTypeUSNonUS")));
			teacherpersonalinfo.setPhoneNumber(request.getParameter("phoneNumber"));
			teacherPersonalInfoDAO.makePersistent(teacherpersonalinfo);
			teacherDetailDAO.makePersistent(teacherDetail);
			
			boolean bTeacherPortfolioStatus=false;
			TeacherPortfolioStatus portfolioStatus = null;
			portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			
			if(portfolioStatus==null)
			{
				bTeacherPortfolioStatus=true;
				portfolioStatus = new TeacherPortfolioStatus();
				portfolioStatus.setTeacherId(teacherDetail);
				portfolioStatus.setIsPersonalInfoCompleted(true);
				portfolioStatus.setIsAcademicsCompleted(false);
				portfolioStatus.setIsCertificationsCompleted(false);
				portfolioStatus.setIsExperiencesCompleted(false);
				portfolioStatus.setIsAffidavitCompleted(false);				
				//teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				TeacherPortfolioStatus tpsTemp=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				if(bTeacherPortfolioStatus && tpsTemp==null)
					teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				else
				{
					portfolioStatus.setId(tpsTemp.getId());
					teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				}
				
			}
			else
			{
				try{
					if(portfolioStatus.getIsPersonalInfoCompleted()==null)
					{
						portfolioStatus.setIsPersonalInfoCompleted(false);
					}
				}catch(Exception e){
					portfolioStatus.setIsPersonalInfoCompleted(false);
					e.printStackTrace();
				}

				try{
					if(portfolioStatus.getIsCertificationsCompleted()==null){
					portfolioStatus.setIsCertificationsCompleted(false);
					}
				}catch(Exception e){
					portfolioStatus.setIsCertificationsCompleted(false);
					e.printStackTrace();
				}

				try{
					if(portfolioStatus.getIsExperiencesCompleted()==null){
					portfolioStatus.setIsExperiencesCompleted(false);
					}
				}catch(Exception e){
					portfolioStatus.setIsExperiencesCompleted(false);
					e.printStackTrace();
				}

				try{
					if(portfolioStatus.getIsAffidavitCompleted()==null){
					portfolioStatus.setIsAffidavitCompleted(false);
					}
				}catch(Exception e){
					portfolioStatus.setIsAffidavitCompleted(false);
					e.printStackTrace();
				}

				try{
					if(portfolioStatus.getIsAcademicsCompleted()==null){
					portfolioStatus.setIsAcademicsCompleted(false);
					}
				}catch(Exception e){
					portfolioStatus.setIsAcademicsCompleted(false);
					e.printStackTrace();
				}
				
				
				
				
				portfolioStatus.setIsPersonalInfoCompleted(true);
				//teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				
				TeacherPortfolioStatus tpsTemp=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				if(bTeacherPortfolioStatus && tpsTemp==null)
					teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				else
				{
					portfolioStatus.setId(tpsTemp.getId());
					teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				}
				
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		
		return "redirect:academics.do";
	}
	
	@RequestMapping(value="/findcitybyzip.do", method=RequestMethod.POST)
	public String doCityByZipGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		
		String zipCode = request.getParameter("zipCode")==null?"":request.getParameter("zipCode");
		
		
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		
		PrintWriter pw = response.getWriter();
		CityMaster cityMaster = null;
		
		cityMaster = cityMasterDAO.findCityByZipcode(zipCode);
		if(cityMaster!=null)
		{
			//		
			//pw.println(cityMaster.getCityId()+"|"+cityMaster.getStateId().getStateId())	;
			
					
			pw.println(cityMaster.getCityName()+"|"+cityMaster.getStateId().getStateId())	;
			
		}
		else
		{					
			pw.println("");
		}
		
		return null;
	}
	
	@RequestMapping(value="/findcityhtmlbystate.do", method=RequestMethod.POST)
	public String doCityByStateGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		
		
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		
		PrintWriter pw = response.getWriter();
		StringBuffer cityOptionHTML=new StringBuffer();
		StateMaster stateMaster = null;
		
		String stateId = request.getParameter("stateId")==null?"":request.getParameter("stateId");
		String zipCode = request.getParameter("zipCode")==null?"":request.getParameter("zipCode");
		System.out.println("stateId------------->"+stateId);
		List<CityMaster> listCityMasters = null;
		try 
		{
			if(stateId!=null && !stateId.equals("") && !stateId.equals("0"))
			{
				stateMaster = stateMasterDAO.findById(Long.parseLong(stateId), false, false);
				if(zipCode==null || zipCode.trim().equals(""))
					listCityMasters = cityMasterDAO.findCityByState(stateMaster);
				else
					listCityMasters = cityMasterDAO.findCityByState(stateMaster,zipCode);
					
				
				//cityOptionHTML.append("<select id='cityId'> ");
				//cityOptionHTML.append("<option value=''>Select City</option>");
				for(CityMaster city:listCityMasters)
				{
					
					//cityOptionHTML.append("<option id='ct"+city.getCityId()+"' value='"+city.getCityId()+"'>"+city.getCityName()+"</option>##");
					cityOptionHTML.append("ct"+city.getCityName()+"$$"+city.getCityId()+"$$"+city.getCityName()+"##");
				}
				//cityOptionHTML.append("</select>");
				
				pw.print(cityOptionHTML.toString());
			}
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	@RequestMapping(value="/webPicture.do", method=RequestMethod.POST)
	public String webPicture(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		
		System.out.println(":::::::webPicture::::::::::::::::");
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		
		 PrintWriter pw = response.getWriter();
		 InputStream in = request.getInputStream();
	        
	        String teacherId=request.getParameter("teacherId");
	        String docOFileName="OTeacherPicture"+teacherId+Utility.getDateTime()+".jpg";
	        String filePath=Utility.getValueOfPropByKey("teacherRootPath")+teacherId+"/"+docOFileName;
        	try{
			  String files;
	       	  File folder = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherId);
	       	  File[] listOfFiles = folder.listFiles(); 
	       	 
	       	  for (int i = 0; i < listOfFiles.length; i++){
	       		  if (listOfFiles[i].isFile()){
	   		    	   files = listOfFiles[i].getName();
	   		    	   if(files.indexOf("TeacherPicture")!=-1 && files.indexOf(docOFileName)!=0){
	   		    		   File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherId+"/"+files);
	   			    	   file.delete();
	   		    	   }
	       	      }
	       	  }
			}catch(Exception e){
				e.printStackTrace();
			}
			try{
		        File file = new File(filePath);
		        OutputStream out = new FileOutputStream(file);
		        byte[] buffer = new byte[1024];
	
		        int len =0;
		        while((len=in.read(buffer))!=-1){
		            out.write(buffer, 0, len);
		        }
		        out.close();
		        in.close();
			}catch(Exception e){
				e.printStackTrace();
			}
	        try{
        		TeacherDetail teacherDetail = teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);
        		teacherDetail.setIdentityVerificationPicture(docOFileName);
        		teacherDetailDAO.makePersistent(teacherDetail);
    	
			}catch (Exception e){
				e.printStackTrace();
			}
			response.setContentType("text/html");  
			pw.print("<script type=\"text/javascript\" language=\"javascript\">");
			pw.print("document.getElementById('identityVPicture').value="+docOFileName+";");
			pw.print("</script>");
		
		return null;
	}
	
	@RequestMapping(value="/certification.do", method=RequestMethod.GET)
	public String doCertificationGET(ModelMap map,HttpServletRequest request)throws Exception
	{
		List<StateMaster> listStateMaster;
		HttpSession session = request.getSession(false);
		TeacherExperience teacherExperience = null;

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			List<CertificationStatusMaster> lstCertificationStatusMaster=certificationStatusMasterDAO.findAllCertificationStatusMasterByOrder();
			Criterion certIficatCri = Restrictions.eq("status", "A");
			List<CertificationTypeMaster>listCertificationTypeMasters=certificationTypeMasterDAO.findByCriteria(certIficatCri);	
			List<TFAAffiliateMaster> lstTFAAffiliateMaster	=	tfaAffiliateMasterDAO.findByCriteria(Order.asc("tfaAffiliateName"));
			List<TFARegionMaster> lstTFARegionMaster		=	tfaRegionMasterDAO.findByCriteria(Order.asc("tfaRegionName"));
			
			map.addAttribute("lstTFAAffiliateMaster", lstTFAAffiliateMaster);
			map.addAttribute("lstTFARegionMaster", lstTFARegionMaster);
			map.addAttribute("lstCorpsYear", Utility.getLasterYeartillAdvanceYear(1990));
			
			map.addAttribute("teacherExperience", teacherExperience);
			map.addAttribute("listStateMaster", listStateMaster);
			map.addAttribute("lstLastYear", Utility.getLasterYearByYear(1965));
			map.addAttribute("lstComingYear", Utility.getComingYearsByYear());
			map.addAttribute("lstCertificationTypeMasters", Utility.getComingYearsByYear());
			map.addAttribute("listCertTypeMasters", listCertificationTypeMasters);
			map.addAttribute("lstCertificationStatusMaster", lstCertificationStatusMaster);
			map.addAttribute("isPortfolioNeeded", getTeacherPortfolioStatusForNativeAndRefered(teacherDetail));
			
			String openDiv = (request.getParameter("openDiv")==null)? "" : request.getParameter("openDiv").trim(); 
			map.addAttribute("openDiv",openDiv);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "certification";
	}
	
	@RequestMapping(value="/experiences.do", method=RequestMethod.GET)
	public String doExperiencesGET(ModelMap map,HttpServletRequest request)throws Exception
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		
		
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			Criterion criterion = Restrictions.eq("status", "A");
			List<FieldMaster> lstFieldMaster = fieldMasterDAO.findByCriteria(Order.asc("fieldName"),criterion);
			
			Criterion criterionEmpRTstatus = Restrictions.eq("status", "A");
			List<EmpRoleTypeMaster> lstEmpRoleTypeMaster = empRoleTypeMasterDAO.findByCriteria(criterionEmpRTstatus);
			
			
			List<PeopleRangeMaster> lstPeopleRangeMasters = peopleRangeMasterDAO.findAll();
			
			Criterion criterionOrg = Restrictions.eq("status", "A");
			List<OrgTypeMaster> lstOrgTypeMasters = orgTypeMasterDAO.findByCriteria(Order.asc("orgType"),criterionOrg);
			
			List<CurrencyMaster> lstCurrencyMasters = currencyMasterDAO.findAllCurrencyByOrder();
			
			map.addAttribute("teacherExperience", teacherExperience);
			map.addAttribute("lstMonth", Utility.getMonthList());
			map.addAttribute("lstYear", Utility.getLasterYearByYear(1965));
			map.addAttribute("lstFieldMaster", lstFieldMaster);
			map.addAttribute("lstEmpRoleTypeMaster", lstEmpRoleTypeMaster);
			
			map.addAttribute("lstPeopleRangeMasters", lstPeopleRangeMasters);
			map.addAttribute("lstOrgTypeMasters", lstOrgTypeMasters);
			map.addAttribute("lstCurrencyMasters", lstCurrencyMasters);
			
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		
		return "experiences";
	}
	
	@RequestMapping(value="/affidavit.do", method=RequestMethod.GET)
	public String doAffidavitGET(ModelMap map,HttpServletRequest request)throws Exception
	{
		
		HttpSession session = request.getSession(false);
		
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		
		
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		
		TeacherAffidavit teacherAffidavit = teacherAffidavitDAO.findAffidavitByTeacher(teacherDetail);
		
		if(teacherAffidavit==null)
		{
			teacherAffidavit = new TeacherAffidavit();
			teacherAffidavit.setAffidavitAccepted(false);
		}
		
		TeacherPortfolioStatus portfolioStatus = null;
		portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
		
		if(portfolioStatus.getIsPersonalInfoCompleted() && portfolioStatus.getIsAcademicsCompleted() && portfolioStatus.getIsExperiencesCompleted() && portfolioStatus.getIsCertificationsCompleted())
		{
			map.addAttribute("portfolioDone", "1");
		}
		
		
		map.addAttribute("teacherAffidavit", teacherAffidavit);
		
		return "affidavit";
	}
	
	@RequestMapping(value="/affidavit.do",method=RequestMethod.POST)
	public String doAffidavitPOST(@ModelAttribute(value="teacherAffidavit") TeacherAffidavit teacherAffidavit, BindingResult result, ModelMap map,HttpServletRequest request)
	{
		
		HttpSession session = request.getSession(false);
		TeacherPortfolioStatus portfolioStatus = null;
		
		
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		boolean bTeacherPortfolioStatus=false;
		try 
		{
			
			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
			teacherAffidavit.setAffidavitAccepted(true);
			teacherAffidavit.setTeacherId(teacherDetail);
			teacherAffidavit.setIsDone(true);
			
			teacherAffidavitDAO.makePersistent(teacherAffidavit);
			
			portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(portfolioStatus==null)
			{
				bTeacherPortfolioStatus=true;
				portfolioStatus=new TeacherPortfolioStatus();
			}
			
			try{
				if(portfolioStatus.getIsPersonalInfoCompleted()==null)
				{
					portfolioStatus.setIsPersonalInfoCompleted(false);
				}
			}catch(Exception e){
				portfolioStatus.setIsPersonalInfoCompleted(false);
				e.printStackTrace();
			}

			try{
				if(portfolioStatus.getIsCertificationsCompleted()==null){
				portfolioStatus.setIsCertificationsCompleted(false);
				}
			}catch(Exception e){
				portfolioStatus.setIsCertificationsCompleted(false);
				e.printStackTrace();
			}

			try{
				if(portfolioStatus.getIsExperiencesCompleted()==null){
				portfolioStatus.setIsExperiencesCompleted(false);
				}
			}catch(Exception e){
				portfolioStatus.setIsExperiencesCompleted(false);
				e.printStackTrace();
			}

			try{
				if(portfolioStatus.getIsAffidavitCompleted()==null){
				portfolioStatus.setIsAffidavitCompleted(false);
				}
			}catch(Exception e){
				portfolioStatus.setIsAffidavitCompleted(false);
				e.printStackTrace();
			}

			try{
				if(portfolioStatus.getIsAcademicsCompleted()==null){
				portfolioStatus.setIsAcademicsCompleted(false);
				}
			}catch(Exception e){
				portfolioStatus.setIsAcademicsCompleted(false);
				e.printStackTrace();
			}
			
			portfolioStatus.setIsAffidavitCompleted(true);
			//teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
			TeacherPortfolioStatus tpsTemp=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(bTeacherPortfolioStatus && tpsTemp==null)
				teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
			else
			{
				portfolioStatus.setId(tpsTemp.getId());
				teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		
		return "redirect:userdashboard.do";
	}
	
	
	
	
	@RequestMapping(value="/portfolioheader.do", method=RequestMethod.GET)
	public String doPortfolioHeaderGET(ModelMap map,HttpServletRequest request)throws Exception
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
		
		TeacherPortfolioStatus portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
		
		map.addAttribute("portfolioStatus",portfolioStatus );
		
				
		return "portfolioheader";
	}
	@Transactional(readOnly=false)
	public boolean getTeacherPortfolioStatusForNativeAndRefered(TeacherDetail teacherDetail){
		/*boolean isPanelNeeded=true;
		try{
			List<JobForTeacher> forTeachers=null;
			
			if(teacherDetail.getUserType().equalsIgnoreCase("R")){
				Criterion criterion1=Restrictions.eq("teacherId",teacherDetail);
				
				Calendar teacherCreateDateTime=Calendar.getInstance();
				teacherCreateDateTime.setTime(teacherDetail.getCreatedDateTime());
				Calendar teacherCreateDateTimeWithExPeriod=teacherCreateDateTime;
				
				
				if(teacherDetail.getExclusivePeriod()!=null){
					teacherCreateDateTimeWithExPeriod.add(Calendar.DATE,teacherDetail.getExclusivePeriod());
				}else{
					teacherCreateDateTimeWithExPeriod.add(Calendar.DATE,0);
				}
				if(teacherCreateDateTimeWithExPeriod.compareTo(teacherCreateDateTime)>=0){
					forTeachers=jobForTeacherDAO.findByCriteria(criterion1);
					for(JobForTeacher jobForTeacher:forTeachers){
						isPanelNeeded=jobForTeacher.getJobId().getDistrictMaster().getIsPortfolioNeeded();
						if(isPanelNeeded){
							break;
						}
					}
				}
			}else{
				isPanelNeeded=true;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}*/
		boolean bReturnValue=false;
		Criterion criterionTID=Restrictions.eq("teacherId",teacherDetail);
		List<JobForTeacher> forTeachers=new ArrayList<JobForTeacher>();
		forTeachers=jobForTeacherDAO.findByCriteria(criterionTID);
		if(forTeachers!=null && forTeachers.size()>0)
		{
			for(JobForTeacher forTeacher:forTeachers)
			{
				if(forTeacher!=null && forTeacher.getJobId()!=null && forTeacher.getJobId().getDistrictMaster()!=null && forTeacher.getJobId().getDistrictMaster().getDistrictId()!=1704170 && forTeacher.getJobId().getDistrictMaster().getDistrictId()!=7800049)
				{
					bReturnValue=true;
					break;
				}
			}
		}
		else
			bReturnValue=true;
			
		
		return bReturnValue;
	}
	@RequestMapping(value="/findcitybystate.do", method=RequestMethod.POST)
	public String doCityByState(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		PrintWriter pw = response.getWriter();
		StringBuffer cityOptionHTML=new StringBuffer();
		StateMaster stateMaster = null;
		
		String stateId = request.getParameter("stateId")==null?"":request.getParameter("stateId");
		System.out.println("stateId------------->"+stateId);
		List<CityMaster> listCityMasters = null;
		try 
		{
			if(stateId!=null && !stateId.equals("") && !stateId.equals("0"))
			{
				stateMaster = stateMasterDAO.findById(Long.parseLong(stateId), false, false);
				listCityMasters = cityMasterDAO.findCityByState(stateMaster);
				for(CityMaster city:listCityMasters)
				{
					cityOptionHTML.append("ct"+city.getCityName()+"$$"+city.getCityId()+"$$"+city.getCityName()+"##");
				}
				//cityOptionHTML.append("</select>");
				
				pw.print(cityOptionHTML.toString());
			}
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	@RequestMapping(value="/findstateAndcitybyzip.do", method=RequestMethod.POST)
	public String doStateAndCityByZipGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		
		String zipCode = request.getParameter("zipCode")==null?"":request.getParameter("zipCode");
		
		
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		
		PrintWriter pw = response.getWriter();
		
		LinkedHashMap<Long,List<CityMaster>>  allCityMasterMap = cityMasterDAO.findAllCityByZipcode(zipCode);
		String allInfo="";
		if(allCityMasterMap!=null && allCityMasterMap.size()>0)
		{
			String allcity="",allstate="";
			int countParent=0;
			for(Long key:allCityMasterMap.keySet()){
				allstate=key+"#";
				List<CityMaster> allCityMaster=allCityMasterMap.get(key);
				for(int count=0; count<allCityMaster.size();count++){
					CityMaster cObj=allCityMaster.get(count);
					if(count==0){
						allcity=cObj.getCityId().toString();
					}else{
						allcity+="@"+cObj.getCityId();
					}
				}
				if(countParent==0) allInfo=allstate+allcity; else allInfo+="@#@"+allstate+allcity;
				++countParent;
			}
			System.out.println(allInfo);
			pw.println(allInfo);
		}
		else
		{					
			pw.println("");
		}
		
		return null;
	}
	
}
