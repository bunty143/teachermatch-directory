package tm.controller.teacher;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.TeacherLoginHistory;
import tm.bean.user.UserMaster;
import tm.dao.TeacherLoginHistoryDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

/*
 * Logout Controller
 *  
 */

@Controller
public class LogoutController 
{
	@Autowired
	private TeacherLoginHistoryDAO teacherLoginHistoryDAO;
	public void setTeacherLoginHistoryDAO(TeacherLoginHistoryDAO teacherLoginHistoryDAO) {
		this.teacherLoginHistoryDAO = teacherLoginHistoryDAO;
	}
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}
	@RequestMapping(value="logout.do", method=RequestMethod.GET)
	public String Logout(HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		/**
		 * Change by Amit
		 * @date 12-Mar-15
		 */
		boolean isSmartPractices=false;
		boolean isPhiladelphia=false;
		try{
			boolean sessionExist=true;
			boolean isImCandidate=false;
			if(request.getParameter("smartpractices")!=null){
				if(request.getParameter("smartpractices").equals("1")){
					isSmartPractices=true;
				}
			}
			if(session.getAttribute("userMaster")!=null){
				if(!session.getAttribute("userMaster").equals("") && !session.getAttribute("userMaster").equals("null")){
					UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");
					if(userMaster.getDistrictId()!=null && !userMaster.getDistrictId().equals("") && userMaster.getEntityType()!=null && !userMaster.getEntityType().equals("") && userMaster.getEntityType()!=0){
						if(userMaster.getDistrictId().getDistrictId().equals(4218990) && (userMaster.getEntityType().equals(2) || userMaster.getEntityType().equals(3))){
							isPhiladelphia=true;
						}
					}
				}
			}
			if(session.getAttribute("isImCandidate")!=null){
				if(!session.getAttribute("isImCandidate").equals("") && !session.getAttribute("isImCandidate").equals("null")){
					isImCandidate=(Boolean)session.getAttribute("isImCandidate");
					if(isImCandidate)
						isPhiladelphia=true;
				}
			}
			if(session.getAttribute("userMaster")!=null){
				if(!session.getAttribute("userMaster").equals("") && !session.getAttribute("userMaster").equals("null")){
					sessionExist=false;
					try{
						UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");
						userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Log Out");
					}catch(Exception e){
						//e.printStackTrace();
					}
				}
			}
			if(sessionExist){
				List<TeacherLoginHistory> teacherLoginHistorylst =teacherLoginHistoryDAO.findByTeacherLoginHistory(session.getId());
				if(teacherLoginHistorylst.size()>0){
					TeacherLoginHistory teacherLoginHistory=null;
					teacherLoginHistory=teacherLoginHistorylst.get(teacherLoginHistorylst.size()-1);
					teacherLoginHistory.setLogOutTime(new Date());
					String timeSpent=Utility.getDateDiff(teacherLoginHistory.getLoginTime(),new Date());
					teacherLoginHistory.setTimeSpent(timeSpent);
					teacherLoginHistoryDAO.updatePersistent(teacherLoginHistory);
				}
			}
		}catch(Exception e){
			//e.printStackTrace();
		}
		if(session != null)
			session.invalidate();
		
		System.out.println("logged out.");
		if(isPhiladelphia)
			  return "redirect:thankyoumsg.do?isPhili=true";
		else if(isSmartPractices)
			return "redirect:thankyoumsg.do?isSmartPractices=true";
		else
			return "redirect:thankyoumsg.do";
	}
	
	@RequestMapping(value="logoutcode.do", method=RequestMethod.GET)
	public String LogoutCode(HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		
		try{
			boolean sessionExist=true;
			if(session.getAttribute("userMaster")!=null){
				if(!session.getAttribute("userMaster").equals("") && !session.getAttribute("userMaster").equals("null")){
					sessionExist=false;
					try{
						UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");
						userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Log Out");
					}catch(Exception e){
						//e.printStackTrace();
					}
				}
			}
			if(sessionExist){
				List<TeacherLoginHistory> teacherLoginHistorylst =teacherLoginHistoryDAO.findByTeacherLoginHistory(session.getId());
				if(teacherLoginHistorylst.size()>0){
					TeacherLoginHistory teacherLoginHistory=null;
					teacherLoginHistory=teacherLoginHistorylst.get(teacherLoginHistorylst.size()-1);
					teacherLoginHistory.setLogOutTime(new Date());
					String timeSpent=Utility.getDateDiff(teacherLoginHistory.getLoginTime(),new Date());
					teacherLoginHistory.setTimeSpent(timeSpent);
					teacherLoginHistoryDAO.updatePersistent(teacherLoginHistory);
				}
			}
		}catch(Exception e){
			//e.printStackTrace();
		}
		if(session != null)
			session.invalidate();
		System.out.println("logged out.");
		return "redirect:signincode.do";
	}
	
	/**
	 * Added by Amit
	 * @param request
	 * @return thankyoumsg
	 * @date 11-Mar-15
	 */
	@RequestMapping(value="thankyoumsg.do", method=RequestMethod.GET)
	public String logoutWithThanks(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		String isPhili = request.getParameter("isPhili")==null?"":request.getParameter("isPhili").trim();
		if(isPhili!=null && isPhili.equals("true"))
			map.addAttribute("isPhili", true);
		
		String isSmartPractices = request.getParameter("isSmartPractices")==null?"":request.getParameter("isSmartPractices").trim();
		if(isSmartPractices!=null && isSmartPractices.equals("true"))
			map.addAttribute("isSmartPractices", true);
		
		System.out.println("DA/SA/T logged out.");
		return "thankyoumsg";
	}
	
}