package tm.controller.teacher;

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.NationalBoardCertificationMaster;
import tm.bean.DistrictPortfolioConfig;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.FavTeacher;
import tm.bean.InternalTransferCandidates;
import tm.bean.JobCertification;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.JobShare;
import tm.bean.SchoolSelectedByCandidate;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.TeacherGeneralKnowledgeExam;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherPreference;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.TeacherSubjectAreaExam;
import tm.bean.TeacherVideoLink;
import tm.bean.WithdrawnReasonMaster;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CertificationStatusMaster;
import tm.bean.master.CertificationTypeMaster;
import tm.bean.master.CityMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.CurrencyMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.EmpRoleTypeMaster;
import tm.bean.master.EthinicityMaster;
import tm.bean.master.EthnicOriginMaster;
import tm.bean.master.FieldMaster;
import tm.bean.master.GenderMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.OrgTypeMaster;
import tm.bean.master.PanelAttendees;
import tm.bean.master.PanelSchedule;
import tm.bean.master.PeopleRangeMaster;
import tm.bean.master.RaceMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectAreaExamMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.master.TFAAffiliateMaster;
import tm.bean.master.TFARegionMaster;
import tm.bean.user.UserMaster;
import tm.dao.EmployeeMasterDAO;
import tm.dao.NationalBoardCertificationMasterDAO;
import tm.dao.DistrictPortfolioConfigDAO;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.EligibilityVerificationHistroyDAO;
import tm.dao.FavTeacherDAO;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.JobShareDAO;
import tm.dao.SapAcademicDetailsDAO;
import tm.dao.SapCandidateDetailsDAO;
import tm.dao.SapScreentestDetailsDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.SchoolSelectedByCandidateDAO;
import tm.dao.SchoolWiseCandidateStatusDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherGeneralKnowledgeExamDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherPreferenceDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.TeacherSubjectAreaExamDAO;
import tm.dao.TeacherVideoLinksDAO;
import tm.dao.WithdrawnReasonMasterDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.CertificationStatusMasterDAO;
import tm.dao.master.CertificationTypeMasterDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.CurrencyMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.EmpRoleTypeMasterDAO;
import tm.dao.master.EthinicityMasterDAO;
import tm.dao.master.EthnicOriginMasterDAO;
import tm.dao.master.FieldMasterDAO;
import tm.dao.master.GenderMasterDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.JobWisePanelStatusDAO;
import tm.dao.master.OrgTypeMasterDAO;
import tm.dao.master.PanelAttendeesDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.PeopleRangeMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.SchoolKeyContactDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectAreaExamMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.TFAAffiliateMasterDAO;
import tm.dao.master.TFARegionMasterDAO;
import tm.dao.master.UserEmailNotificationsDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.CommonService;
import tm.services.EmailerService;
import tm.services.MailSendToTeacher;
import tm.services.MailText;
import tm.services.PaginationAndSorting;
import tm.services.ScheduleMailThreadByListToBcc;
import tm.services.district.PrintOnConsole;
import tm.services.hqbranches.BranchesAjax;
import tm.services.hqbranches.KellyONRAjax;
import tm.services.report.CandidateGridService;
import tm.services.teacher.DashboardAjax;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

@Controller
public class UserDashboardController 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private WithdrawnReasonMasterDAO withdrawnReasonMasterDAO;
	
	@Autowired
	private SchoolSelectedByCandidateDAO schoolSelectedByCandidateDAO;
	
	@Autowired
	private TeacherStatusNotesDAO teacherStatusNotesDAO;
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired
	private SubjectAreaExamMasterDAO subjectAreaExamMasterDAO;
	@Autowired
	private CertificationTypeMasterDAO certificationTypeMasterDAO;
	
	@Autowired
	private TeacherSubjectAreaExamDAO teacherSubjectAreaExamDAO;
	
	@Autowired
	private TeacherGeneralKnowledgeExamDAO teacherGeneralKnowledgeExamDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private GeoZoneMasterDAO geoZoneMasterDAO;
	
	@Autowired
	private EthinicityMasterDAO ethinicityMasterDAO;
	
	@Autowired
	private EthnicOriginMasterDAO ethnicOriginMasterDAO;
	
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	@Autowired
	private OrgTypeMasterDAO orgTypeMasterDAO;
	
	@Autowired
	private CurrencyMasterDAO currencyMasterDAO;
	
	@Autowired
	private PeopleRangeMasterDAO peopleRangeMasterDAO;
	
	@Autowired
	private CountryMasterDAO countryMasterDAO;
	
	@Autowired
	private GenderMasterDAO genderMasterDAO;
	
	@Autowired
	private JobWisePanelStatusDAO jobWisePanelStatusDAO;
	
	@Autowired
	private PanelScheduleDAO panelScheduleDAO;
	
	@Autowired
	private PanelAttendeesDAO panelAttendeesDAO;
	
	@Autowired
	private SapAcademicDetailsDAO sapAcademicDetailsDAO;
	
	@Autowired
	private SapCandidateDetailsDAO sapCandidateDetailsDAO;
	
	@Autowired
	private SapScreentestDetailsDAO sapScreentestDetailsDAO;
	
	@Autowired
	private EligibilityVerificationHistroyDAO eligibilityVerificationHistroyDAO;
	
	@Autowired
	private SchoolWiseCandidateStatusDAO schoolWiseCandidateStatusDAO;
	
	@Autowired
	private NationalBoardCertificationMasterDAO nationalBoardCertificationMasterDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private TeacherVideoLinksDAO linksDAO;
	
	@Autowired
	private TeacherCertificateDAO certificateDAO;
	
	@Autowired
	private EmailerService emailerService;
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}

	public void setNationalBoardCertificationMasterDAO(
			NationalBoardCertificationMasterDAO nationalBoardCertificationMasterDAO) {
		this.nationalBoardCertificationMasterDAO = nationalBoardCertificationMasterDAO;
	}





	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) 
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}

	@Autowired
	private TeacherPreferenceDAO teacherPreferenceDAO;
	public void setTeacherPreferenceDAO(TeacherPreferenceDAO teacherPreferenceDAO) 
	{
		this.teacherPreferenceDAO = teacherPreferenceDAO;
	}

	@Autowired
	private FavTeacherDAO favTeacherDAO;
	public void setFavTeacherDAO(FavTeacherDAO favTeacherDAO) 
	{
		this.favTeacherDAO = favTeacherDAO;
	}

	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) 
	{
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) 
	{
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
	{
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) 
	{
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}

	@Autowired
	private JobShareDAO jobShareDAO;
	public void setJobShareDAO(JobShareDAO jobShareDAO) {
		this.jobShareDAO = jobShareDAO;
	}

	@Autowired
	private ServletContext servletContext;
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	@InitBinder
	public void initBinder(HttpServletRequest request,DataBinder binder) 
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true,10));
	}

	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	public void setTeacherExperienceDAO(TeacherExperienceDAO teacherExperienceDAO) 
	{
		this.teacherExperienceDAO = teacherExperienceDAO;
	}
	
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) {
		this.districtKeyContactDAO = districtKeyContactDAO;
	}
	
	@Autowired
	private SchoolKeyContactDAO schoolKeyContactDAO;
	public void setSchoolKeyContactDAO(SchoolKeyContactDAO schoolKeyContactDAO) {
		this.schoolKeyContactDAO = schoolKeyContactDAO;
	}
	
	@Autowired
	private UserEmailNotificationsDAO userEmailNotificationsDAO;
	public void setUserEmailNotificationsDAO(
			UserEmailNotificationsDAO userEmailNotificationsDAO) {
		this.userEmailNotificationsDAO = userEmailNotificationsDAO;
	}

	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	
	@Autowired
	private CertificationStatusMasterDAO certificationStatusMasterDAO;
	public void setCertificationStatusMasterDAO(
			CertificationStatusMasterDAO certificationStatusMasterDAO) {
		this.certificationStatusMasterDAO = certificationStatusMasterDAO;
	}
	
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	
	@Autowired
	private TFAAffiliateMasterDAO tfaAffiliateMasterDAO;
	public void setTfaAffiliateMasterDAO(TFAAffiliateMasterDAO tfaAffiliateMasterDAO) {
		this.tfaAffiliateMasterDAO = tfaAffiliateMasterDAO;
	}
	
	@Autowired
	private TFARegionMasterDAO tfaRegionMasterDAO;
	public void setTfaRegionMasterDAO(TFARegionMasterDAO tfaRegionMasterDAO) {
		this.tfaRegionMasterDAO = tfaRegionMasterDAO;
	}
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(
			TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO) {
		this.cityMasterDAO = cityMasterDAO;
	}
	
	@Autowired
	private FieldMasterDAO fieldMasterDAO;
	public void setFieldMasterDAO(FieldMasterDAO fieldMasterDAO) {
		this.fieldMasterDAO = fieldMasterDAO;
	}
	
	@Autowired
	private EmpRoleTypeMasterDAO empRoleTypeMasterDAO;
	public void setEmpRoleTypeMasterDAO(
			EmpRoleTypeMasterDAO empRoleTypeMasterDAO) {
		this.empRoleTypeMasterDAO = empRoleTypeMasterDAO;
	}
	
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	public void setSubjectMasterDAO(SubjectMasterDAO subjectMasterDAO) {
		this.subjectMasterDAO = subjectMasterDAO;
	}
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	
	@Autowired
	private DistrictPortfolioConfigDAO districtPortfolioConfigDAO;
	
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	
	@RequestMapping(value="/userdashboard.do", method=RequestMethod.GET)
	public String doSignUpGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
       
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
			
		TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
		try 
		{		
			FavTeacher favTeacher = favTeacherDAO.findFavTeacherByTeacher(teacherDetail);
			map.addAttribute("favTeacher", favTeacherDAO.findRandomFavTeacher(favTeacher));

			String jobId = request.getParameter("jobId");
			String mf = request.getParameter("mf");
			String isAffilated =null;
			JobForTeacher jft = null;
			if(request.getParameter("ok_cancelflag")!=null){
				isAffilated=request.getParameter("ok_cancelflag");
			}
			JobOrder jobOrder  = null;
			String compltd = "N";
			boolean testvar=false;
			if(jobId!=null && jobId.trim().length()>0)
			{
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
				jft = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
				if(jft!=null)
				{	testvar=true;
					if(jft.getStatus().getStatusShortName().equalsIgnoreCase("comp"))
						compltd = "Y";
				}
			}
			
			if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster()!=null &&  jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId().equals(2)){
				//JobForTeacher jft=jobForTeacherDAO.getJFTByTeacherIdAndJobId(teacherDetail.getTeacherId(), jobOrder.getJobId());
				if(jft!=null && jft.getjFTWiseDetails()==null)
					return BasicController.getAffidavitReturnUrl(map, statusMasterDAO, jobForTeacherDAO, jft.getJobId().getJobId(), jft.getTeacherId().getTeacherId());
				else if(jft!=null && jft.getjFTWiseDetails()!=null && jft.getjFTWiseDetails().getApplyJobWithSignUp()!=null && jft.getjFTWiseDetails().getApplyJobWithSignUp() && jft.getjFTWiseDetails().getApplyJobWithSignUpFirstTime()!=null)
				if(jft.getjFTWiseDetails()!=null && (jft.getjFTWiseDetails().getAffidavit()==null || !jft.getjFTWiseDetails().getAffidavit()))
					return BasicController.getAffidavitReturnUrl(map, statusMasterDAO, jobForTeacherDAO, jft.getJobId().getJobId(), jft.getTeacherId().getTeacherId());
			}
			
			try{
				List<JobForTeacher> lstJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);
				if(lstJFT!=null && lstJFT.size()>0){
					for (JobForTeacher jobForTeacher : lstJFT) {
						int schoolSelectionflag=0;
						if(jobForTeacher.getCandidateConsideration()!=null){
							schoolSelectionflag=1;
						}
						session.setAttribute("schoolSelectionflag",schoolSelectionflag);
						if(schoolSelectionflag==1){
							break;
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			try {
				
				boolean isKelly = false;
				if(request.getServerName().contains("kelly") || request.getServerName().toLowerCase().contains("smartpractice") || request.getRequestURL().toString().contains("smartpractice")){
					isKelly = true;
				}
				//boolean isKellyJobApply = false;
				//if(!isKelly)
					//isKellyJobApply = jobForTeacherDAO.getJFTByTeachers(teacherDetail);
				
				if(isKelly) // isKellyJobApply || 
				{
					request.getSession().setAttribute("powerProfile", "hide");
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			boolean openDspq=true;
			if(jobOrder!=null && jobOrder.getIsInviteOnly()!=null && jobOrder.getHiddenJob()!=null){
				System.out.println("openDspq :: "+openDspq +" isInviteOnly :: "+jobOrder.getIsInviteOnly()+" hiddenJob :: "+  jobOrder.getHiddenJob());
				if(jobOrder.getIsInviteOnly() && !jobOrder.getHiddenJob() && testvar){
					openDspq=false;//true
				}else if(jobOrder.getIsInviteOnly() && !jobOrder.getHiddenJob() && !testvar){
					openDspq=true;//false
				}
			}
			
			//System.out.println("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP openDspq ::: "+openDspq);
			map.addAttribute("compltd", compltd); 
			map.addAttribute("jobOrder", jobOrder); 
			map.addAttribute("mf", mf);
			map.addAttribute("openDspq", openDspq);
			
			/* Sekhar Add Branch Details*/
			
			boolean prefCheck=false;
			if(request.getParameter("pShowFlg")!=null){
				prefCheck=true;
			}
			/* Sekhar Add Branch Details*/
			if(prefCheck==false && isAffilated!=null){
				BranchesAjax branchesAjax=new BranchesAjax();
				List<JobForTeacher> jobForTeacherKes=jobForTeacherDAO.findJobByTeacherForKES(teacherDetail,true);
				try{
					if(jobForTeacherKes.size()>0){
						String kesData=branchesAjax.getBranchDetails(jobForTeacherKes.get(0).getJobId());
						if(kesData!=null){
							map.addAttribute("kesData",kesData);
							map.addAttribute("kesDataFlag",true);
						}else{
							map.addAttribute("kesDataFlag",false);
						}
					}else{
						map.addAttribute("kesDataFlag",false);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			// Start ... dynamic portfolio for External Job Apply
			map.addAttribute("customQuestionMultipleOption", null);
			map.addAttribute("lstLastYear", Utility.getLasterYearByYear(1955));
			
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			map.addAttribute("teacherExperience", teacherExperience);
			
			//Criterion criterionGenderActive = Restrictions.eq("status", "A");
			List<GenderMaster> lstGenderMasters  = genderMasterDAO.findAllGenderByOrder();
			
			List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			map.addAttribute("listStateMaster", listStateMaster);
			
			List<CertificationStatusMaster> lstCertificationStatusMaster=certificationStatusMasterDAO.findAllCertificationStatusMasterByOrder();
			map.addAttribute("lstCertificationStatusMaster", lstCertificationStatusMaster);
			
			List<TFAAffiliateMaster> lstTFAAffiliateMaster	=	tfaAffiliateMasterDAO.findByCriteria(Order.asc("tfaAffiliateName"));
			List<TFARegionMaster> lstTFARegionMaster		=	tfaRegionMasterDAO.findByCriteria(Order.asc("tfaRegionName"));
			map.addAttribute("lstTFAAffiliateMaster", lstTFAAffiliateMaster);
			map.addAttribute("lstTFARegionMaster", lstTFARegionMaster);
			map.addAttribute("lstCorpsYear", Utility.getLasterYeartillAdvanceYear(1990));
			
			Criterion certIficatCri = Restrictions.eq("status", "A");
			List<CertificationTypeMaster>listCertificationTypeMasters=certificationTypeMasterDAO.findByCriteria(certIficatCri);
			map.addAttribute("listCertTypeMasters", listCertificationTypeMasters);
			map.addAttribute("lstComingYear", Utility.getComingYearsByYear());
			map.addAttribute("lstDays", Utility.getDays());
			
			String districtDName=null;
			if(jobOrder!=null)
			{
				if(jobOrder.getDistrictMaster()!=null){
					DistrictMaster districtMaster=null;
					districtMaster=jobOrder.getDistrictMaster();
					if(districtMaster.getDisplayName() !=null && !districtMaster.getDisplayName().equals("")){
						districtDName=districtMaster.getDisplayName();
					}else{
						districtDName=districtMaster.getDistrictName();
					}
				}
				else if(jobOrder.getHeadQuarterMaster()!=null){
					districtDName = jobOrder.getHeadQuarterMaster().getHeadQuarterName();
				}
				map.addAttribute("districtDName", districtDName);
			}
			// End ... dynamic portfolio for External Job Apply
			boolean dspqFlag =true;
			boolean getQQ =true;
			boolean isMiami = false;
			boolean internalFlag=false;
			DistrictMaster districtMaster=null;
			try{
				if(jobOrder!=null){
					
					if(jobOrder.getDistrictMaster()!=null){
						if(jobOrder.getDistrictMaster().getDistrictId().equals(1200390))
							isMiami=true;
					}
					
					
					if(jobOrder.getDistrictMaster()!=null){
						districtMaster=jobOrder.getDistrictMaster();
					}
					List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
					
					if(itcList.size()>0){
						internalFlag=true;
					}
					if(jobOrder.getDistrictMaster()!=null && internalFlag){
						if(districtMaster.getOfferDistrictSpecificItems()){
							dspqFlag=true;
						}else{
							dspqFlag=false;
						}
						if(districtMaster.getOfferQualificationItems()){
							getQQ=true;
						}else{
							getQQ=false;
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			String staffTypeSession = "";
			try{
				staffTypeSession = isAffilated =(String)(session.getAttribute("staffType")==null?"0":session.getAttribute("staffType"));
				if(isAffilated==null){
					isAffilated =(String)(session.getAttribute("isAffilated")==null?"0":session.getAttribute("isAffilated"));
				}
				System.out.println("isAffilated:::::>>>:::::::"+isAffilated);
				if(jobOrder!=null && isAffilated.equalsIgnoreCase("1") && internalFlag==false){
					if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferDistrictSpecificItems()){
						dspqFlag=true;
					}else{
						dspqFlag=false;
					}
					if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferQualificationItems()){
						getQQ=true;
					}else{
						getQQ=false;
					}
				}
				List<DistrictPortfolioConfig> lstdistrictPortfolioConfig = districtPortfolioConfigDAO.getPortfolioConfig("E",jobOrder);
				DistrictPortfolioConfig districtPortfolioConfig=null;
				if(lstdistrictPortfolioConfig!=null && lstdistrictPortfolioConfig.size()>0)
				{
					districtPortfolioConfig=lstdistrictPortfolioConfig.get(0);
				}
				boolean isDspqReqForKelly=false;
				boolean isKelly=false;
				if(districtPortfolioConfig!=null)
				{					
					if(jobOrder!=null && jobOrder.getHeadQuarterMaster()!=null)
					{						
						isKelly=true;
						isDspqReqForKelly=jobOrder.getJobCategoryMaster().getOfferDSPQ();
					}
				}
				
				map.addAttribute("isKelly", isKelly);
				map.addAttribute("isDspqReqForKelly", isDspqReqForKelly);
				
				boolean isKellyJobApply = false;
				isKellyJobApply = jobForTeacherDAO.getJFTByTeachers(teacherDetail);
				if(request.getServerName().contains("kelly") || request.getServerName().toLowerCase().contains("smartpractice") || request.getRequestURL().toString().contains("smartpractice")){
					isKelly = true;
				}
				if(isKellyJobApply || isKelly)
					map.addAttribute("isKellyUser", true);
				else
					map.addAttribute("isKellyUser", false);
			}catch(Exception e){
				e.printStackTrace();
			
			}
			System.out.println("staffTypeSession    "+staffTypeSession);
			System.out.println("dspqFlag::::::::"+dspqFlag);
			System.out.println("getQQ::::::::"+getQQ);
			map.addAttribute("dspqFlag", dspqFlag);
			map.addAttribute("getQQ", getQQ);
			map.addAttribute("staffTypeSession", staffTypeSession);
			
			map.addAttribute("isMiami", isMiami);
			if(districtMaster!=null)
			map.addAttribute("districtMasterDSPQ", districtMaster);
			map.addAttribute("teacherIdForDSPQ", teacherDetail);
			if(districtMaster!=null)
			{
				map.addAttribute("districtMaster", districtMaster);
			}
			
			List<RaceMaster> listRaceMasters = null;
			listRaceMasters = raceMasterDAO.findAllRaceByOrder();
			map.addAttribute("listRaceMasters", listRaceMasters);
			
			List<EthnicOriginMaster> lstethnicOriginMasters = null;
			List<EthinicityMaster> lstEthinicityMasters = null;

			lstEthinicityMasters=ethinicityMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
			lstethnicOriginMasters=ethnicOriginMasterDAO.findByCriteria(Restrictions.eq("status", "A"));

			System.out.println(" lstEthinicityMasters "+lstEthinicityMasters.size()+" lstethnicOriginMasters "+lstethnicOriginMasters.size());

			map.addAttribute("lstEthinicityMasters", lstEthinicityMasters);
			map.addAttribute("lstethnicOriginMasters", lstethnicOriginMasters);
			
			// For Work Experience
			Criterion criterion = Restrictions.eq("status", "A");
			List<FieldMaster> lstFieldMaster = fieldMasterDAO.findByCriteria(Order.asc("fieldName"),criterion);

			Criterion criterionEmpRTstatus = Restrictions.eq("status", "A");
			List<EmpRoleTypeMaster> lstEmpRoleTypeMaster = empRoleTypeMasterDAO.findByCriteria(criterionEmpRTstatus);

			List<PeopleRangeMaster> lstPeopleRangeMasters = peopleRangeMasterDAO.findAll();

			Criterion criterionOrg = Restrictions.eq("status", "A");
			List<OrgTypeMaster> lstOrgTypeMasters = orgTypeMasterDAO.findByCriteria(Order.asc("orgType"),criterionOrg);

			List<CurrencyMaster> lstCurrencyMasters = currencyMasterDAO.findAllCurrencyByOrder();

			//map.addAttribute("teacherExperience", teacherExperience);
			map.addAttribute("lstMonth", Utility.getMonthList());
			map.addAttribute("lstYear", Utility.getLasterYearByYear(1955));
			map.addAttribute("lstFieldMaster", lstFieldMaster);
			map.addAttribute("lstEmpRoleTypeMaster", lstEmpRoleTypeMaster);

			map.addAttribute("lstPeopleRangeMasters", lstPeopleRangeMasters);
			map.addAttribute("lstOrgTypeMasters", lstOrgTypeMasters);
			map.addAttribute("lstCurrencyMasters", lstCurrencyMasters);
			
			List<CountryMaster> countryMasters=new ArrayList<CountryMaster>();
			countryMasters=countryMasterDAO.findAllActiveCountry();
			map.addAttribute("listCountryMaster", countryMasters);
			map.addAttribute("lstGenderMasters", lstGenderMasters);
			System.out.println(" lstFieldMaster :: "+lstFieldMaster.size());

			String teacherAssessmentStatus=teacherAssessmentStatusDAO.epiStatus(teacherDetail);	
			System.out.println("teacherAssessmentStatus "+teacherAssessmentStatus);
			map.addAttribute("epistatus", teacherAssessmentStatus);
			
			List<TeacherVideoLink> teacherVideoLink=linksDAO.findTeachByVideoLink(teacherDetail);
			map.addAttribute("videoProfile", teacherVideoLink);	
			
			List<TeacherCertificate> teacherCertificates=certificateDAO.findCertificateByTeacher(teacherDetail);
			map.addAttribute("teacherCertificate", teacherCertificates);	
			
			List<JobForTeacher> jobForTeachers=jobForTeacherDAO.findJobByTeacherOneYear(teacherDetail);
			map.addAttribute("jobsApplied", jobForTeachers);
			
			List<TeacherDetail> teacherDetails = teacherDetailDAO.findByEmail(teacherDetail.getEmailAddress());
			if(teacherDetails!=null && teacherDetails.size()>0)
			{
				Boolean status = false;
				Boolean verify = false;
				status = teacherDetails.get(0).getStatus().equalsIgnoreCase("A")?true:false;
				verify = teacherDetails.get(0).getVerificationStatus()==1?true:false;
				if(verify && status)
					map.addAttribute("activation",true);
				else
					map.addAttribute("activation",false);
			}
			
			boolean  isAlsoSA=false;
			if(session != null && (session.getAttribute("isAlsoSA")!=null))
			{
				isAlsoSA = (Boolean)session.getAttribute("isAlsoSA");
			}
			map.addAttribute("isAlsoSA",isAlsoSA);
			
//mukesh code for Internal movement of candidate
			boolean  isImCandidate=false;
			DistrictMaster districtMasterIM=null;
			if(session != null && (session.getAttribute("isImCandidate")!=null))
			{
				isImCandidate = (Boolean)session.getAttribute("isImCandidate");
				districtMasterIM =(DistrictMaster) session.getAttribute("districtIdForImCandidate");
			}
			map.addAttribute("isImCandidate",isImCandidate);
			
			//List<UserMaster> listUserMasters =userMasterDAO.findByEmail(teacherDetail.getEmailAddress().trim());
			String districtOrSchoolName="";
			if(districtMasterIM!=null){
				districtOrSchoolName=districtMasterIM.getDistrictName();
			}
			map.addAttribute("districtOrSchoolName",districtOrSchoolName+"'s");
			int year = Calendar.getInstance().get(Calendar.YEAR);
			String selectionyear=year+"-"+(year+1);
			map.addAttribute("selectionyear",selectionyear);
			
			//for Logo
			String source = "";
			String target = "";
			String path = "";
			try{
				if(districtMasterIM!=null){
					source = Utility.getValueOfPropByKey("districtRootPath")+districtMasterIM.getDistrictId()+"/"+districtMasterIM.getLogoPath();
					target = servletContext.getRealPath("/")+"/"+"/district/"+districtMasterIM.getDistrictId()+"/";
					File sourceFile = new File(source);
					File targetDir = new File(target);
					if(!targetDir.exists())
						targetDir.mkdirs();
					File targetFile = new File(targetDir+"/"+sourceFile.getName());
					if(sourceFile.exists())
					{
						FileUtils.copyFile(sourceFile, targetFile);
						path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+districtMasterIM.getDistrictId()+"/"+sourceFile.getName();
					}
				}
			  map.addAttribute("logoPathSSO", path);	
			}catch (Exception e) {
				e.printStackTrace();
			}
			if(!Utility.existsURL(path)){
				path = "images/dashboard-icon.png";
				map.addAttribute("logoPathSSO", path);
			}
			map.addAttribute("logoPathSSO", path);
			
			
			String sJAFExternalRegistered = (String)(session.getAttribute("newjafexternal")==null?"0":session.getAttribute("newjafexternal"));
			System.out.println("JAFExternal "+sJAFExternalRegistered);
			map.addAttribute("JAFExternal", sJAFExternalRegistered);
			//session.setAttribute("newjafexternal", "0");
			
			try{
				List withdrawnReasonMasterList = new ArrayList();
				withdrawnReasonMasterList = withdrawnReasonMasterDAO.findAllWithdrawnReasonMaster(districtMasterIM);
				map.addAttribute("withdrawnReasonMaster",withdrawnReasonMasterList);
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "userdashboard";
	}

	
	@RequestMapping(value="/favteacher.do", method=RequestMethod.GET)
	public String doFavTeacherGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}

		FavTeacher favoriteTeacher = null;
		TeacherDetail teacherDetail = null;

		teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");



		try 
		{
			favoriteTeacher = favTeacherDAO.findFavTeacherByTeacher(teacherDetail);
			//favoriteTeacher = favTeacherDAO.findById(new Integer(1), false, false);
			if(favoriteTeacher==null)
			{

				favoriteTeacher = new FavTeacher();				

			}
			map.addAttribute("favoriteTeacher", favoriteTeacher);

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "favteacher";
	}

	@RequestMapping(value="/favteacher.do", method=RequestMethod.POST)
	public String doFavTeacherPOST(@ModelAttribute(value="favoriteTeacher") FavTeacher favoriteTeacher,BindingResult result,ModelMap map,HttpServletRequest request)
	{

		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		TeacherDetail teacherDetail = null;		

		teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		try 
		{
			favoriteTeacher.setTeacherId(teacherDetail);
			favTeacherDAO.makePersistent(favoriteTeacher);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "redirect:userdashboard.do";
	}

	@RequestMapping(value={"/jobsofinterest.do","/jobsofinterestnew.do"}, method=RequestMethod.GET)
	public String doJobsOfIntrestGET(ModelMap map,HttpServletRequest request)
	{	
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		
		try 
		{
			List<SubjectMaster> lstSubjectMasters = subjectMasterDAO.findActiveSubject();
			List<StateMaster> lstStateMasters = stateMasterDAO.findActiveState();
			
			map.addAttribute("lstSubjectMasters", lstSubjectMasters);
			map.addAttribute("lstStateMasters", lstStateMasters);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		try
		{
			
			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
			List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
			List<CityMaster> listCityMasters = new ArrayList<CityMaster>();
			
			TeacherPersonalInfo teacherpersonalinfo = null;
			TeacherPortfolioStatus teacherPortfolioStatus = null;
			TeacherDetail tDetail = null;
			tDetail =(TeacherDetail) session.getAttribute("teacherDetail");
			teacherpersonalinfo = teacherPersonalInfoDAO.findById(tDetail.getTeacherId(), false, false);
			teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			
			if(teacherpersonalinfo!=null)
			{
				listCityMasters = cityMasterDAO.findCityByState(teacherpersonalinfo.getStateId());
				map.addAttribute("teacherpersonalinfo", teacherpersonalinfo);
			}else{
			}
			
			if(teacherPortfolioStatus!=null){
				map.addAttribute("teacherPortfolioStatus", teacherPortfolioStatus);
			}
			
			map.addAttribute("listStateMaster", listStateMaster);
			map.addAttribute("listCityMasters", listCityMasters);
			
			List<CertificationStatusMaster> lstCertificationStatusMaster=certificationStatusMasterDAO.findAllCertificationStatusMasterByOrder();
			map.addAttribute("lstCertificationStatusMaster", lstCertificationStatusMaster);
			
			List<TFAAffiliateMaster> lstTFAAffiliateMaster	=	tfaAffiliateMasterDAO.findByCriteria(Order.asc("tfaAffiliateName"));
			List<TFARegionMaster> lstTFARegionMaster		=	tfaRegionMasterDAO.findByCriteria(Order.asc("tfaRegionName"));
			map.addAttribute("lstTFAAffiliateMaster", lstTFAAffiliateMaster);
			map.addAttribute("lstTFARegionMaster", lstTFARegionMaster);
			map.addAttribute("lstCorpsYear", Utility.getLasterYeartillAdvanceYear(1990));
			
			List<RaceMaster> listRaceMasters = null;
			listRaceMasters = raceMasterDAO.findAllRaceByOrder();
			map.addAttribute("listRaceMasters", listRaceMasters);
			
			List<EthnicOriginMaster> lstethnicOriginMasters = null;
			List<EthinicityMaster> lstEthinicityMasters = null;

			lstEthinicityMasters=ethinicityMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
			lstethnicOriginMasters=ethnicOriginMasterDAO.findByCriteria(Restrictions.eq("status", "A"));

			map.addAttribute("lstEthinicityMasters", lstEthinicityMasters);
			map.addAttribute("lstethnicOriginMasters", lstethnicOriginMasters);
			
			
			// For Work Experience
			Criterion criterion = Restrictions.eq("status", "A");
			List<FieldMaster> lstFieldMaster = fieldMasterDAO.findByCriteria(Order.asc("fieldName"),criterion);

			Criterion criterionEmpRTstatus = Restrictions.eq("status", "A");
			List<EmpRoleTypeMaster> lstEmpRoleTypeMaster = empRoleTypeMasterDAO.findByCriteria(Order.desc("empRoleTypeId"),criterionEmpRTstatus);

			//Criterion criterionGenderActive = Restrictions.eq("status", "A");
			List<GenderMaster> lstGenderMasters  = genderMasterDAO.findAllGenderByOrder();
			
			
			List<PeopleRangeMaster> lstPeopleRangeMasters = peopleRangeMasterDAO.findAll();

			Criterion criterionOrg = Restrictions.eq("status", "A");
			List<OrgTypeMaster> lstOrgTypeMasters = orgTypeMasterDAO.findByCriteria(Order.asc("orgType"),criterionOrg);

			List<CurrencyMaster> lstCurrencyMasters = currencyMasterDAO.findAllCurrencyByOrder();
			
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			
			map.addAttribute("teacherExperience", teacherExperience);
			map.addAttribute("lstMonth", Utility.getMonthList());
			map.addAttribute("lstYear", Utility.getLasterYearByYear(1955));
			map.addAttribute("lstFieldMaster", lstFieldMaster);
			map.addAttribute("lstEmpRoleTypeMaster", lstEmpRoleTypeMaster);
			map.addAttribute("lstGenderMasters", lstGenderMasters);
			
			map.addAttribute("lstPeopleRangeMasters", lstPeopleRangeMasters);
			map.addAttribute("lstOrgTypeMasters", lstOrgTypeMasters);
			map.addAttribute("lstCurrencyMasters", lstCurrencyMasters);
			
			List<CountryMaster> countryMasters=new ArrayList<CountryMaster>();
			countryMasters=countryMasterDAO.findAllActiveCountry();
			map.addAttribute("listCountryMaster", countryMasters);

			System.out.println(" lstFieldMaster :: "+lstFieldMaster.size());

			
			TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam = teacherGeneralKnowledgeExamDAO.findTeacherGeneralKnowledgeExam(teacherDetail);
			TeacherSubjectAreaExam teacherSubjectAreaExam = teacherSubjectAreaExamDAO.findTeacherSubjectAreaExam(teacherDetail);
			
			Criterion certIficatCri = Restrictions.eq("status", "A");
			List<CertificationTypeMaster>listCertificationTypeMasters=certificationTypeMasterDAO.findByCriteria(certIficatCri);;
			map.addAttribute("listCertTypeMasters", listCertificationTypeMasters);
			map.addAttribute("teacherGeneralKnowledgeExam",teacherGeneralKnowledgeExam);
			map.addAttribute("teacherSubjectAreaExam",teacherSubjectAreaExam);
			map.addAttribute("lstDays", Utility.getDays());
			map.addAttribute("lstLastYear", Utility.getLasterYearByYear(1955));
			// End ... dynamic portfolio for External Job Apply
			
			try{
				DistrictMaster districtMasterIM = null;
				List withdrawnReasonMasterList = new ArrayList();
				withdrawnReasonMasterList = withdrawnReasonMasterDAO.findAllWithdrawnReasonMaster(districtMasterIM);
				map.addAttribute("withdrawnReasonMaster",withdrawnReasonMasterList);
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "jobsofinterest";
	}
// mukersh
	@RequestMapping(value="/getjobsofinterestgrid.do", method=RequestMethod.GET)
	public String getJobsOfIntrestGrid(ModelMap modelmap,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		boolean statusAvlbl = false;
		boolean statusComp = false;
		boolean statusIcomp = false;
		boolean statusVlt =	false;
		boolean statusWidrwn = false;
		boolean statusHiden = false;
		boolean statusAll =	false;
		boolean isEpiStandalone = false; 
        
		Integer geoZoneId=0;
		String zoneId   =  request.getParameter("geoZoneId");
		if(zoneId!=null && zoneId!="")
			geoZoneId =  Integer.parseInt(zoneId);
      
		HttpSession session = request.getSession(false);
		PrintWriter out = response.getWriter();
		StringBuffer sb = new StringBuffer();

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		try
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			//-- set start and end position
			if(session.getAttribute("epiStandalone")!=null)
				isEpiStandalone = (Boolean) session.getAttribute("epiStandalone");

			String noOfRow = request.getParameter("noOfRows")==null?"1":request.getParameter("noOfRows");
			String pageNo =	request.getParameter("page")==null?"1":request.getParameter("page");
			int noOfRowInPage =	Integer.parseInt(noOfRow);
			int pgNo =	Integer.parseInt(pageNo);
			String sortOrder = request.getParameter("sortOrderStr").trim();
			String sortOrderType = request.getParameter("sortOrderType").trim();
			
			// For searching parameter
			String districtId = request.getParameter("districtId").trim();
			String schoolId1 = request.getParameter("schoolId").trim();
			String cityName = request.getParameter("cityId").trim();
			String stateId = request.getParameter("stateId").trim();
			String subjectId = request.getParameter("subjectId").trim();
						
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------


			/***** Sorting by Domain Name,Competency Name and Objective Name Stating ( Sekhar ) ******/
			SortedMap map = new TreeMap();
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";
			//System.out.println("\n  Gagan : sort order sortOrderFieldName  "+sortOrderFieldName+" sortOrderNoField "+sortOrderNoField);
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("jobTitle") && !sortOrder.equals("districtName")&& !sortOrder.equals("schoolName") && !sortOrder.equals("location") && !sortOrder.equals("geoZoneName")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("jobTitle"))
				{
					sortOrderNoField="jobTitle";
				}
				if(sortOrder.equals("districtName"))
				{
					sortOrderNoField="districtName";
				}
				if(sortOrder.equals("schoolName"))
				{
					sortOrderNoField="schoolName";
				}
				if(sortOrder.equals("location"))
				{
					sortOrderNoField="location";
				}
				if(sortOrder.equals("geoZoneName"))
				{
					sortOrderNoField="geoZoneName";
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
			/**End ------------------------------------**/




			boolean prefStatus = false;
			boolean portfolioStatus = false;
			boolean baseInvStatus=false;
			TeacherPreference tPreference=null;
			TeacherPortfolioStatus tPortfolioStatus=null;

			boolean flagJobOrderList = false; 
			
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			
			ArrayList<Integer> subjectIdList=  new ArrayList<Integer>();
			List<SubjectMaster> ObjSubjectList = new ArrayList<SubjectMaster>();
			
			List<JobOrder> jobOrderAllList = new ArrayList<JobOrder>(); 
			List<JobOrder> districtMasterlst = new ArrayList<JobOrder>(); 
			List<JobOrder> schoolMasterlst = new ArrayList<JobOrder>(); 
			List<JobOrder> subjectList = new ArrayList<JobOrder>(); 
			List<JobOrder> zipCodeList2 = new ArrayList<JobOrder>();
			List<String> zipList	=	new ArrayList<String>();
			List<CityMaster> lstcityMasters = new ArrayList<CityMaster>();
			List<JobOrder> jobOrderAllListWithIsPool = new ArrayList<JobOrder>();
			
			
			try
			{
				jobOrderAllListWithIsPool = jobOrderDAO.findAllJobWithPoolCondition();
			
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			tPreference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
			if(tPreference!=null){
				prefStatus = true;
			}

			tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(tPortfolioStatus!=null && tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted()){
				portfolioStatus = true;
			}
			TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if((teacherBaseAssessmentStatus!=null) && (!teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp") )){
				baseInvStatus=true;				 
			}
			List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
			if(teacherDetail.getUserType().equalsIgnoreCase("N"))
			{
				lstJobOrder = getJobByPref(request,sortOrderStrVal);
			}
			else
			{

				List<JobForTeacher> lstJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);
				if(lstJFT.size()>0)
				{
					districtMaster = lstJFT.get(0).getJobId().getDistrictMaster(); 
					lstJobOrder = jobOrderDAO.findSortedJobOrderbyDistrict(districtMaster,sortOrderStrVal);
				}

			}
			
			CandidateGridService gridService=new CandidateGridService();
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			String[] statuss = {"icomp","vlt","widrw","hide"};
			try{
				lstStatusMasters = Utility.getStaticMasters(statuss);
			}catch (Exception e) {
				e.printStackTrace();
			}
			List<JobForTeacher> lstAllJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);
			
			///////////////////////////////////////////////////////////////////
			
			
			List<JobOrder> jobLst= new ArrayList<JobOrder>();
			for (JobForTeacher jobForTeacher : lstAllJFT) {
				jobLst.add(jobForTeacher.getJobId());
			}
			List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
			Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
			for (StatusMaster statusMaster : statusMasterList) {
				mapStatus.put(statusMaster.getStatusShortName(),statusMaster);
			}
			
			TeacherAssessmentStatus teacherAssessmentStatusJSI = null;
			Map<Integer, Integer> mapAssess = new HashMap<Integer, Integer>();
			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 
			List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobLst);
			List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
			if(assessmentJobRelations1.size()>0){
				for(AssessmentJobRelation ajr: assessmentJobRelations1){
					mapAssess.put(ajr.getJobId().getJobId(),ajr.getAssessmentId().getAssessmentId());
					adList.add(ajr.getAssessmentId());
				}
				if(adList.size()>0)
				lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentList(teacherDetail,adList);
			}
			for(TeacherAssessmentStatus ts : lstJSI){
				mapJSI.put(ts.getAssessmentDetail().getAssessmentId(), ts);
			}
			
			DistrictMaster districtMasterInternal=null;
			List<InternalTransferCandidates> lstITRA=internalTransferCandidatesDAO.getDistrictForTeacher(teacherDetail);
			InternalTransferCandidates internalITRA = null;
			if(lstITRA.size()>0){
				internalITRA=lstITRA.get(0);
				districtMasterInternal=internalITRA.getDistrictMaster();
			}
			//////////////////////////////////
			List<JobForTeacher> lstincompletJFT =new ArrayList<JobForTeacher>(); // jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusInComplete);			
			List<JobForTeacher> lstviolatedJFT = new ArrayList<JobForTeacher>();//jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusViolated);
			List<JobForTeacher> lstHideJFT =new ArrayList<JobForTeacher>(); //jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusHide);
			List<JobForTeacher> lstWidrwJFT =new ArrayList<JobForTeacher>(); //jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusWidrw);
			
			List<JobForTeacher> lstcompletJFT =new ArrayList<JobForTeacher>(); //jobForTeacherDAO.findJobByTeacherAndSatusList(teacherDetail,statusMasters);
			
			for(JobForTeacher jobForTeacher: lstAllJFT)
			{				
				JobOrder jobOrder = jobForTeacher.getJobId();				
				if(lstJobOrder.contains(jobOrder))
				{
					lstJobOrder.remove(jobOrder);					
				}
				
				StatusMaster statusMaster = mapStatus.get("icomp");
				DashboardAjax dbajax=new DashboardAjax();
				statusMaster=dbajax.findByTeacherIdJobStausForLoop(internalITRA,jobForTeacher,mapAssess,teacherAssessmentStatusJSI,mapJSI,districtMasterInternal,tPortfolioStatus,mapStatus,teacherBaseAssessmentStatus);
				//System.out.println(jobOrder.getJobId()+"  "+statusMaster.getStatusShortName());
				
				if(statusMaster.getStatusShortName().equalsIgnoreCase("comp") && (jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")|| jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("rem") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("scomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("ecomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vcomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("dcln"))){
					lstcompletJFT.add(jobForTeacher);
				}else if(!statusMaster.getStatusShortName().equalsIgnoreCase("vlt") && !jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vlt") && !jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("widrw") && !jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hide") && (statusMaster.getStatusShortName().equalsIgnoreCase("icomp") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("icomp")|| jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("rem") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("scomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("ecomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vcomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("dcln"))){
					lstincompletJFT.add(jobForTeacher);
				}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vlt")){
					lstviolatedJFT.add(jobForTeacher);
				}else if(jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("widrw")){
					lstWidrwJFT.add(jobForTeacher);
				}else if(jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hide")){
					lstHideJFT.add(jobForTeacher);
				}
				/////////////////////////////////////////////////////////////////
			}
			
		/*	System.out.println("lstcompletJFT:::::::"+lstcompletJFT.size());
			System.out.println("lstincompletJFT:::::::"+lstincompletJFT.size());
			System.out.println("lstviolatedJFT:::::::"+lstviolatedJFT.size());
			System.out.println("lstWidrwJFT:::::::"+lstWidrwJFT.size());
			System.out.println("lstHideJFT:::::::"+lstHideJFT.size());
			*/
			List<JobForTeacher> finalListForJobsOfInterest	=	new ArrayList<JobForTeacher>();
			List<JobOrder> finalListForJobsOfInterestJobOrders	=	new ArrayList<JobOrder>();

			String searchBy = request.getParameter("searchBy")==null?"":request.getParameter("searchBy");
			
		// Start Searching	
			if(searchBy.equalsIgnoreCase("avlbl"))
			{
				statusAvlbl=true;
				finalListForJobsOfInterestJobOrders.addAll(lstJobOrder);
				flagJobOrderList=true;
			}
			else if(searchBy.equalsIgnoreCase("comp"))
			{
				statusComp=true;
				flagJobOrderList=true;
				for (JobForTeacher jobForTeacher : lstcompletJFT) 
				{
					JobOrder jo=jobForTeacher.getJobId();
					jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
					finalListForJobsOfInterestJobOrders.add(jo);
				}
			}
			else if(searchBy.equalsIgnoreCase("icomp"))
			{
				statusIcomp=true;
				flagJobOrderList=true;
				for (JobForTeacher jobForTeacher : lstincompletJFT) 
				{
					JobOrder jo=jobForTeacher.getJobId();
					jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
					finalListForJobsOfInterestJobOrders.add(jo);
				}
			}
			else if(searchBy.equalsIgnoreCase("vlt"))
			{
				statusVlt=true;
				flagJobOrderList=true;
				for (JobForTeacher jobForTeacher : lstviolatedJFT) 
				{
					JobOrder jo=jobForTeacher.getJobId();
					jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
					finalListForJobsOfInterestJobOrders.add(jo);
				}
			}
			else if(searchBy.equalsIgnoreCase("widrw"))
			{
				statusWidrwn=true;
				flagJobOrderList=true;
				for (JobForTeacher jobForTeacher : lstWidrwJFT) 
				{
					JobOrder jo=jobForTeacher.getJobId();
					jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
					finalListForJobsOfInterestJobOrders.add(jo);
				}
			}
			else if(searchBy.equalsIgnoreCase("hide"))
			{
				statusHiden=true;
				flagJobOrderList=true;
				for (JobForTeacher jobForTeacher : lstHideJFT) 
				{
					JobOrder jo=jobForTeacher.getJobId();
					jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
					finalListForJobsOfInterestJobOrders.add(jo);
				}
			}
			else
			{
				statusAll = true;
				flagJobOrderList=true;
				finalListForJobsOfInterestJobOrders.clear();

				for (JobForTeacher jobForTeacher : lstincompletJFT) 
				{
					JobOrder jo=jobForTeacher.getJobId();
					jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
					jo.setExitURL("Incomplete");
					finalListForJobsOfInterestJobOrders.add(jo);
				}
				for (JobOrder job : lstJobOrder) 
				{
					JobOrder jo=job;
					jo.setExitURL("Available");
					finalListForJobsOfInterestJobOrders.add(jo);
				}

				for (JobForTeacher jobForTeacher : lstcompletJFT) 
				{
					JobOrder jo=jobForTeacher.getJobId();
					jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
					jo.setExitURL("Completed");
					finalListForJobsOfInterestJobOrders.add(jo);
				}
				for (JobForTeacher jobForTeacher : lstviolatedJFT) 
				{
					JobOrder jo=jobForTeacher.getJobId();
					jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
					jo.setExitURL("Timed Out");
					finalListForJobsOfInterestJobOrders.add(jo);
				}
				for (JobForTeacher jobForTeacher : lstWidrwJFT) 
				{
					JobOrder jo=jobForTeacher.getJobId();
					jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
					jo.setExitURL("Withdrawn");
					finalListForJobsOfInterestJobOrders.add(jo);
				}
				for (JobForTeacher jobForTeacher : lstHideJFT) 
				{
					JobOrder jo=jobForTeacher.getJobId();
					jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
					jo.setExitURL("Hidden");
					finalListForJobsOfInterestJobOrders.add(jo);
				}
			}
			
	/* @Start
	 * @Ashish Kumar
	 * @Description :: Searching Data
	 * */
			
			if(!cityName.equals("0")){
				zipList = cityMasterDAO.findCityByName(cityName);
			}else{
				if(stateId!=null && !stateId.equals("")){
					StateMaster stateMaster = stateMasterDAO.findById(Long.parseLong(stateId), false, false);
					
					// city List By State Id
					lstcityMasters	=	cityMasterDAO.findCityByState(stateMaster);
					
					for(CityMaster c:lstcityMasters)
					{
						//System.out.println(" city name :: "+c.getCityName()+" zip code :: "+c.getZipCode());
						zipList.add(c.getZipCode());
					}
				}
			}
			
			
				if(finalListForJobsOfInterestJobOrders.size()>0){
					flagJobOrderList=true;
					jobOrderAllList.addAll(finalListForJobsOfInterestJobOrders);
				}
				
				// For District Search
				if(!districtId.equals("") && !districtId.equals(""))
				{
					districtMaster  = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
					districtMasterlst = jobOrderDAO.findJobOrderByDistrict(districtMaster);
					
					if(flagJobOrderList==true){
						jobOrderAllList.retainAll(districtMasterlst);
					}else{
						jobOrderAllList.addAll(districtMasterlst);
					}
					
				}
				
				// For School Search
				if(!schoolId1.equals("0") && !schoolId1.equals(""))
				{
					schoolMaster 	=	schoolMasterDAO.findById(new Long(schoolId1), false, false);
					schoolMasterlst.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, null,ObjSubjectList));
					
					if(flagJobOrderList==true)
						jobOrderAllList.retainAll(schoolMasterlst);
					else
						jobOrderAllList.addAll(schoolMasterlst);
					  
				}
				
				
				// For Subject
				if(subjectId !=null && !subjectId.equals("0,") && !subjectId.equals(""))
				{
					String subIdList[] = subjectId.split(",");
					if(Integer.parseInt(subIdList[0])!=0)
					{
						for(int i=0;i<subIdList.length;i++)
						{
							subjectIdList.add(Integer.parseInt(subIdList[i]));
						}
						ObjSubjectList = subjectMasterDAO.getSubjectMasterlist(subjectIdList);
						
						subjectList = jobOrderDAO.findJobOrderBySubjectList(ObjSubjectList);
						
						if(flagJobOrderList==true){
							jobOrderAllList.retainAll(subjectList);
						}else{
							jobOrderAllList.addAll(subjectList);
						}
					}
				}
				
				// For Cities (ZipCode List)
				if(zipList!=null && zipList.size()>0)
				{
					List<DistrictMaster> lstDistrictMasters = new ArrayList<DistrictMaster>();
					List<SchoolMaster> lstSchoolMasters = null;
					
					lstSchoolMasters = schoolMasterDAO.findByZipCodeList(zipList);
					if(lstSchoolMasters!=null)
					{
						for(SchoolMaster s:lstSchoolMasters)
						{
								lstDistrictMasters.add(s.getDistrictId());
						}
					}
					
					if(lstDistrictMasters!=null && lstDistrictMasters.size()>0)
						zipCodeList2 = jobOrderDAO.findJobOrderByDistrictList(lstDistrictMasters);
			
					if(flagJobOrderList==true){
						jobOrderAllList.retainAll(zipCodeList2);
					}else{
						jobOrderAllList.addAll(zipCodeList2);
					}
				}
				
			/* @End
			 * @Ashish Kumar
			 * @Description :: Searching Data
			 * */
// filtering by mukesh
				
				List<JobOrder> listjobOrdersgeoZone = new ArrayList<JobOrder>();
				  if(geoZoneId>0)
					{
						GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
						listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(geoZoneMaster);
						
					}
			
				  if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
				  {
					  if(flagJobOrderList==true){
							jobOrderAllList.retainAll(listjobOrdersgeoZone);
						}else{
							jobOrderAllList.addAll(listjobOrdersgeoZone);
						}
		
				  }				
			
				  if(listjobOrdersgeoZone.size()==0 && geoZoneId==-1 ||geoZoneId==0)
				  {
					  jobOrderAllList.addAll(listjobOrdersgeoZone);
				  
				  }
				  else if(listjobOrdersgeoZone.size()==0 &&geoZoneId!=-1)
				  {
					  jobOrderAllList.retainAll(listjobOrdersgeoZone);
				  }
			/*============For Paging and  Sorting ===============*/
			List<JobOrder> sortedlstJobOrder		=	new ArrayList<JobOrder>();

			SortedMap<String,JobOrder>	sortedMap 	= 	new TreeMap<String,JobOrder>();
			if(sortOrderNoField.equals("jobTitle"))
			{
				sortOrderFieldName	=	"jobTitle";
			}
			if(sortOrderNoField.equals("jobEndDate"))
			{
				sortOrderFieldName	=	"jobEndDate";
			}
			if(sortOrderNoField.equals("districtName"))
			{
				sortOrderFieldName	=	"districtName";
			}
			if(sortOrderNoField.equals("schoolName"))
			{
				sortOrderFieldName	=	"schoolName";
			}
			if(sortOrderNoField.equals("location"))
			{
				sortOrderFieldName	=	"location";
			}
			if(sortOrderNoField.equals("geoZoneName"))
			{
				sortOrderFieldName	=	"geoZoneName";
			}
			if(jobOrderAllList!=null && jobOrderAllList.size()>0 && jobOrderAllListWithIsPool!=null && jobOrderAllListWithIsPool.size()>0)
			{
				jobOrderAllList.retainAll(jobOrderAllListWithIsPool);
			}
			
			int mapFlag=2;
			for (JobOrder jobOrder : jobOrderAllList){
				String orderFieldName=jobOrder.getStatus();
				if(sortOrderFieldName.equals("jobTitle")){
					orderFieldName=jobOrder.getJobTitle().toUpperCase()+"||"+jobOrder.getJobId();
					sortedMap.put(orderFieldName+"||",jobOrder);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("jobEndDate")){
					orderFieldName=Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobEndDate())+"||"+jobOrder.getJobId();
					sortedMap.put(orderFieldName+"||",jobOrder);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
				if(jobOrder.getGeoZoneMaster()!=null){
					orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName();
					if(sortOrderFieldName.equals("geoZoneName")){
						orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName()+"||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}else{
					if(sortOrderFieldName.equals("geoZoneName")){
						orderFieldName="0||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}
				
				if(sortOrderFieldName.equals("districtName")){
					orderFieldName=jobOrder.getDistrictMaster().getDistrictName()+"||"+jobOrder.getJobId();
					sortedMap.put(orderFieldName+"||",jobOrder);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
				if(sortOrderFieldName.equals("schoolName")){
					if(jobOrder.getCreatedForEntity().equals(3)){
						orderFieldName=jobOrder.getSchool().get(0).getSchoolName()+"||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
					else{
						if(jobOrder.getSchool().size()==1){
							orderFieldName=jobOrder.getSchool().get(0).getSchoolName()+"||"+jobOrder.getJobId();
						}else if(jobOrder.getSchool().size()>1){
							orderFieldName="Many Schools||"+jobOrder.getJobId();
						}else{
							orderFieldName=""+"0000||"+jobOrder.getJobId();
						}
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}
				if(sortOrderFieldName.equals("location")){

					if(jobOrder.getCreatedForEntity()!=3)
					{
						if(!jobOrder.getDistrictMaster().getAddress().isEmpty())
							orderFieldName=jobOrder.getDistrictMaster().getAddress()+"||"+jobOrder.getJobId();
						else
							orderFieldName="0000"+"||"+jobOrder.getJobId();
					}
					else
					{
						if(!jobOrder.getSchool().get(0).getAddress().isEmpty())
							orderFieldName=jobOrder.getSchool().get(0).getAddress()+"||"+jobOrder.getJobId();
						else
							orderFieldName="0000"+"||"+jobOrder.getJobId();
					}
					sortedMap.put(orderFieldName+"||",jobOrder);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}

				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstJobOrder.add((JobOrder) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstJobOrder.add((JobOrder) sortedMap.get(key));
				}
			}else{
				
				sortedlstJobOrder=jobOrderAllList;
			}

			totalRecord =sortedlstJobOrder.size();

			if(totalRecord<end)
				end=totalRecord;
			List<JobOrder> lstsortedJobOrder =	sortedlstJobOrder.subList(start,end);


			String responseText="";

			sb.append("<table border='0' id='tblGridJoF'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			responseText=PaginationAndSorting.responseSortingLink("Job Title",sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
			sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink("Expiry Date",sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
			sb.append("<th width='7%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("District Name",sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
			sb.append("<th width='18%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink("Zone",sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
			sb.append("<th width='10%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("School",sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
			sb.append("<th width='18%' valign='top'>"+responseText+"</th>");
			
			responseText=PaginationAndSorting.responseSortingLink("Address",sortOrderFieldName,"location",sortOrderTypeVal,pgNo);
			sb.append("<th width='15%' valign='top'>"+responseText+"</th>");
			
			sb.append("<th width='5%' valign='top'>Job Status</th>");
			sb.append("<th width='10%' valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");

			int rowCount=0;
			int tempCount = 0;
			boolean dispName=false;
			String zone = "";
			if(statusIcomp)//for status Incomplete
			{
				//System.out.println("1  >>>>>>>>>>>>>  lstsortedJobOrder  >>> "+lstsortedJobOrder.get(0).getGeoZoneMaster().getGeoZoneName());
				for(JobOrder jb : lstsortedJobOrder)
				{
					
					dispName=false;
					rowCount++;
					sb.append("<tr>");
					sb.append("<td><a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");
					
					sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+"</td>");
					if(jb.getCreatedForEntity()==3)
					{
						tempCount=0;
						for(SchoolMaster school:jb.getSchool())
						{
							if(jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}							
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{	
								if(jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jb.getDistrictMaster().getDistrictId()+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							
							sb.append("<td>");
							sb.append("&nbsp;"+school.getSchoolName());
							sb.append("</td>");
							
							sb.append("<td>");
							//sb.append("&nbsp;"+school.getAddress());
							sb.append("&nbsp;"+getSchoolAddress(jb));
							sb.append("</td>");
							
							if(tempCount==0)
							{
								tempCount++;
								break;
							}
						}
						if(tempCount==0)
						{
							sb.append("<td>&nbsp;&nbsp;</td>");
							sb.append("<td>&nbsp;&nbsp;</td>");
							sb.append("<td>&nbsp;&nbsp;</td>");

						}
					}
					else
					{
						
						if(jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
						{
							sb.append("<td>");
							sb.append(""+jb.getDistrictMaster().getDisplayName());
							sb.append("</td>");
						}
						else
						{
							sb.append("<td>");
							sb.append(""+jb.getDistrictMaster().getDistrictName());
							sb.append("</td>");
						}

						if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
						{						
							if(jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
								//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}else{
								sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jb.getDistrictMaster().getDistrictId()+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}
						}
						else
						{
							sb.append("<td>");							
							sb.append("</td>");
						}
						
						if(jb.getSchool().size()==1)
						{
							if(jb.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
									sb.append("<td>");
									sb.append(""+getSchoolAddress(jb));
									sb.append("</td>");
								}
							
						}
						else if(jb.getSchool().size()>1)
						{
							String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
							for(int i=1;i<jb.getSchool().size();i++)
							{
								schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
							}
							if(jb.getIsPoolJob()==2){
							sb.append("<td>&nbsp;</td>");
							sb.append("<td>"+getDistrictAddress(jb)+"</td>");
							}else{
								sb.append("<td><input type='hidden' id='schoolIds' value='"+schoolListIds+"'><a href='javascript:void(0);' onclick=showSchoolList()>Many Schools</a></td>");
								sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList()>Many Address</a></td>");
							}
					
						}else{
							sb.append("<td>&nbsp;</td>");
							sb.append("<td>");
							sb.append(""+getDistrictAddress(jb));
							sb.append("</td>");
						}
					}


					sb.append("<td><strong class='text-error'>Incomplete</strong></td>");
					sb.append("<td>");


					//sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('"+jb.getJobId()+"')\"><img src='images/option02.png'></a>");
					if(isEpiStandalone){
						sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png'></a>");
					}
					else{
						/*if(prefStatus == false || portfolioStatus == false){
							sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"chkJobSpecificInventory("+prefStatus+","+portfolioStatus+","+false+");\"><img src='images/option02.png'></a>");
						}
						else if(jb.getJobCategoryMaster().getBaseStatus() && (!baseInvStatus)){
							sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('0')\"><img src='images/option02.png'></a>");
						}
						else{
							sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('"+jb.getJobId()+"')\"><img src='images/option02.png'></a>");
						}*/
						sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"getTeacherCriteria("+jb.getJobId()+");\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
						sb.append("&nbsp;<a data-original-title='Withdraw' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
						sb.append("&nbsp;<a data-original-title='Cover Letter' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
						sb.append("&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
					}
					


					sb.append("</td>");	   				
					sb.append("</tr>");
				}
			}

			
			if(statusAvlbl)//for status available jobs
			{
				//totaRecord=totaRecord+lstJobOrder.size();
				for(JobOrder jbOrder: lstsortedJobOrder)
				{			
					rowCount++;
					sb.append("<tr>");
					sb.append("<td><a href='applyjob.do?jobId="+jbOrder.getJobId()+"'>"+jbOrder.getJobTitle()+"</a></td>");
					
				//	sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobStartDate())+"</td>");
					
					sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobEndDate())+"</td>");
					
					
					if(jbOrder.getCreatedForEntity()==3)//School
					{
						tempCount=0;
						for(SchoolMaster school: jbOrder.getSchool())
						{
							
							if(jbOrder.getDistrictMaster().getDisplayName()!=null && (!jbOrder.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jbOrder.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jbOrder.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}                         
							
							if(jbOrder.getGeoZoneMaster()!=null && !jbOrder.getGeoZoneMaster().getGeoZoneName().equals(""))
							{						
								if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jbOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"',"+jbOrder.getDistrictMaster().getDistrictId()+");\">"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							sb.append("<td>");
							sb.append("&nbsp;"+school.getSchoolName());
							sb.append("</td>");
							if(tempCount==0)
							{
								tempCount++;
								break;
							}
							
							sb.append("<td>");
							//sb.append("&nbsp;"+school.getAddress());
							sb.append("&nbsp;"+getSchoolAddress(jbOrder));
							sb.append("</td>");
						}
						if(tempCount==0)
						{
							sb.append("<td>&nbsp;&nbsp;</td>");
							sb.append("<td>&nbsp;&nbsp;</td>");
							sb.append("<td>&nbsp;&nbsp;</td>");
						}
					}
					else
					{
						sb.append("<td>");
						sb.append(""+jbOrder.getDistrictMaster().getDistrictName());
						sb.append("</td>");
						if(jbOrder.getGeoZoneMaster()!=null && !jbOrder.getGeoZoneMaster().getGeoZoneName().equals(""))
						{						
							if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
								//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}else{
								sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jbOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"',"+jbOrder.getDistrictMaster().getDistrictId()+");\">"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}
						}
						else
						{
							sb.append("<td>");							
							sb.append("</td>");
						}
						if(jbOrder.getSchool().size()==1)
						{
							if(jbOrder.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
							}else{
								sb.append("<td>"+jbOrder.getSchool().get(0).getSchoolName()+"</td>");
								sb.append("<td>");
								sb.append(""+getSchoolAddress(jbOrder));
								sb.append("</td>");
							}
						}
						else if(jbOrder.getSchool().size()>1)
						{
							String schoolListIds = jbOrder.getSchool().get(0).getSchoolId().toString();
							for(int i=1;i<jbOrder.getSchool().size();i++)
							{
								schoolListIds = schoolListIds+","+jbOrder.getSchool().get(i).getSchoolId().toString();
							}
							if(jbOrder.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
							}else{
								sb.append("<td><input type='hidden' id='schoolIds' value='"+schoolListIds+"'><a href='javascript:void(0);' onclick=showSchoolList()>Many Schools</a></td>");
								sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList()>Many Address</a></td>");
							}
						}
						else
						{
							sb.append("<td>&nbsp;</td>");
							sb.append("<td>");
							sb.append(""+getDistrictAddress(jbOrder));
							sb.append("</td>");
						}
							
					}

					sb.append("<td><strong class='text-info'>Available</strong></td>");
					sb.append("<td>");
						
					if(isEpiStandalone){
						sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
					}
					else{
						sb.append("<a data-original-title=' Apply Now' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jbOrder.getJobId()+"' ><img src='images/option01.png' style='width:21px; height:21px;'></a>");
						sb.append("&nbsp;&nbsp;<a data-original-title='Hide' rel='tooltip' id='tpHide"+rowCount+"' href='#' onclick=\"return getJbsIHide('"+jbOrder.getJobId()+"')\"><img src='images/unhide.png' style='width:21px; height:21px;'></a>");
						sb.append("&nbsp;&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
					}
					

					sb.append("</td>");	   				
					sb.append("</tr>");
				}
			}
			
			if(statusComp)//for status complete
			{
				//totaRecord=totaRecord+lstcompletJFT.size();
				for(JobOrder jb : lstsortedJobOrder)
				{

					rowCount++;
					sb.append("<tr>");
					sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");
					
			//		sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");
					
					sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+"</td>");

					if(jb.getCreatedForEntity()==3)
					{
						tempCount=0;
						for(SchoolMaster school:jb.getSchool())
						{
							
							if(jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{

								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{						
								if(jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jb.getDistrictMaster().getDistrictId()+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							sb.append("<td>");
							sb.append("&nbsp;"+school.getSchoolName());
							sb.append("</td>");
							
							sb.append("<td>");
							//sb.append("&nbsp;"+school.getAddress());
							sb.append("&nbsp;"+getSchoolAddress(jb));
							sb.append("</td>");
							
							if(tempCount==0)
							{
								tempCount++;
								break;
							}
						}
						if(tempCount==0)
						{
							sb.append("<td>&nbsp;&nbsp;</td>");
							sb.append("<td>&nbsp;&nbsp;</td>");
							sb.append("<td>&nbsp;&nbsp;</td>");

						}

					}
					else
					{
						if(jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
						{
							sb.append("<td>");
							sb.append(""+jb.getDistrictMaster().getDisplayName());
							sb.append("</td>");
						}
						else
						{
							sb.append("<td>");
							sb.append(""+jb.getDistrictMaster().getDistrictName());
							sb.append("</td>");
						}
						if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
						{						
							if(jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
								//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}else{
								sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jb.getDistrictMaster().getDistrictId()+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}
						}
						else
						{
							sb.append("<td>");							
							sb.append("</td>");
						}
						if(jb.getSchool().size()==1)
						{
							if(jb.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>"+getDistrictAddress(jb)+"</td>");
							}else{
								sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
								sb.append("<td>");
								sb.append(""+getSchoolAddress(jb));
								sb.append("</td>");
							}
						}
						else if(jb.getSchool().size()>1)
						{
							String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
							for(int i=1;i<jb.getSchool().size();i++)
							{
								schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
							}
							if(jb.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>"+getDistrictAddress(jb)+"</td>");
							}else{
							sb.append("<td><input type='hidden' id='schoolIds' value='"+schoolListIds+"'><a href='javascript:void(0);' onclick=showSchoolList()>Many Schools</a></td>");
							sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList()>Many Address</a></td>");
							}
						}
						else
						{
							sb.append("<td>&nbsp;</td>");
							sb.append("<td>");
							sb.append(""+getDistrictAddress(jb));
							sb.append("</td>");
						}
						
					}

					sb.append("<td><strong class='text-success'>Completed</strong></td>");
					sb.append("<td>");
					if(isEpiStandalone){
						sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
					}
					else{
						sb.append("<a data-original-title='Withdraw' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
						sb.append("&nbsp;&nbsp;<a data-original-title='Cover Letter' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
						sb.append("&nbsp;&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
					}
					
					sb.append("</td>");	   				
					sb.append("</tr>");
				}
			}
			
			if(statusVlt)//for violated
			{
				//totaRecord=totaRecord+lstviolatedJFT.size();
				//for(JobForTeacher jbTeacher : lstviolatedJFT)
				for(JobOrder jb : lstsortedJobOrder)
				{
					rowCount++;
					sb.append("<tr>");
					sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");
					
				//	sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");
					
					sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+"</td>");
					
					if(jb.getCreatedForEntity()==3)
					{
						for(SchoolMaster school:jb.getSchool())
						{
							if(jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{						
								if(jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jb.getDistrictMaster().getDistrictId()+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							sb.append("<td>");
							sb.append("&nbsp;"+school.getSchoolName());
							sb.append("</td>");
							
							
							sb.append("<td>");
						//	sb.append("&nbsp;"+school.getAddress());
							
							sb.append("&nbsp;"+getSchoolAddress(jb));
							sb.append("</td>");
							
							break;
						}
					}
					else
					{
						if(jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
						{
							sb.append("<td>");
							sb.append(""+jb.getDistrictMaster().getDisplayName());
							sb.append("</td>");
						}
						else
						{
							sb.append("<td>");
							sb.append(""+jb.getDistrictMaster().getDistrictName());
							sb.append("</td>");
						}
						if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
						{						
							if(jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
								//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}else{
								sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jb.getDistrictMaster().getDistrictId()+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}
						}
						else
						{
							sb.append("<td>");							
							sb.append("</td>");
						}
						if(jb.getSchool().size()==1)
						{
							if(jb.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>"+getDistrictAddress(jb)+"</td>");
							}else{
							sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
							sb.append("<td>");
							sb.append(""+getSchoolAddress(jb));
							sb.append("</td>");
							}
						}
						else if(jb.getSchool().size()>1)
						{
							String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
							for(int i=1;i<jb.getSchool().size();i++)
							{
								schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
							}
							if(jb.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>"+getDistrictAddress(jb)+"</td>");
							}else{
							sb.append("<td><input type='hidden' id='schoolIds' value='"+schoolListIds+"'><a href='javascript:void(0);' onclick=showSchoolList()>Many Schools</a></td>");
							sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList()>Many Address</a></td>");
							}
						}
						else
						{
							sb.append("<td>&nbsp;</td>");
							sb.append("<td>");
							sb.append(""+getDistrictAddress(jb));
							sb.append("</td>");
						}
					
					}

					sb.append("<td><strong class='text-error'>Timed Out</strong></td>");
					sb.append("<td>");
					if(isEpiStandalone){
						sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
					}
					else{
						sb.append("<a data-original-title='Withdraw' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' ></a>");
						sb.append("&nbsp;&nbsp;<a data-original-title='Cover Letter' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
						sb.append("&nbsp;&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
					}
					
					sb.append("</td>");	   				
					sb.append("</tr>");
				}
			}
			
			/////////////////// Withdrawn ///////////////////////////
			if(statusWidrwn)//for Withdrawn
			{
				//totaRecord=totaRecord+lstWidrwJFT.size();
				//for(JobForTeacher jbTeacher : lstWidrwJFT)
				for(JobOrder jb : lstsortedJobOrder)
				{
					rowCount++;
					sb.append("<tr>");
					sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");
					
			//		sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");
					
					sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+"</td>");
					
					if(jb.getCreatedForEntity()==3)
					{
						for(SchoolMaster school:jb.getSchool())
						{
							
							if(jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{						
								if(jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jb.getDistrictMaster().getDistrictId()+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							sb.append("<td>");
							sb.append("&nbsp;"+school.getSchoolName());
							sb.append("</td>");
							
							sb.append("<td>");
					//		sb.append("&nbsp;"+school.getAddress());
							
							sb.append("&nbsp;"+getSchoolAddress(jb));
							sb.append("</td>");
							
							break;
						}
					}
					else
					{
						
						if(jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
						{
							sb.append("<td>");
							sb.append(""+jb.getDistrictMaster().getDisplayName());
							sb.append("</td>");
						}
						else
						{
							sb.append("<td>");
							sb.append(""+jb.getDistrictMaster().getDistrictName());
							sb.append("</td>");
						}
						if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
						{						
							if(jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
								//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}else{
								sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jb.getDistrictMaster().getDistrictId()+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}
						}
						else
						{
							sb.append("<td>");							
							sb.append("</td>");
						}

						if(jb.getSchool().size()==1)
						{
							if(jb.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>"+getDistrictAddress(jb)+"</td>");
							}else{
							sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
							sb.append("<td>");
							sb.append(""+getSchoolAddress(jb));
							sb.append("</td>");
							}
						}
						else if(jb.getSchool().size()>1)
						{
							String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
							for(int i=1;i<jb.getSchool().size();i++)
							{
								schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
							}
							if(jb.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>"+getDistrictAddress(jb)+"</td>");
							}else{
							sb.append("<td><input type='hidden' id='schoolIds' value='"+schoolListIds+"'><a href='javascript:void(0);' onclick=showSchoolList()>Many Schools</a></td>");
							sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList()>Many Address</a></td>");
							}
						}
						else
						{
							sb.append("<td>&nbsp;</td>");
							sb.append("<td>");
							sb.append(""+getDistrictAddress(jb));
							sb.append("</td>");
						}
					}

					sb.append("<td><strong class='text-error'>Withdrawn</strong></td>");
					sb.append("<td>");
					if(isEpiStandalone){						
						sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
					}
					else{
						sb.append("<a data-original-title=' Re-Apply' rel='tooltip' id='tpApply"+rowCount+"' href='javascript:void(0);' onclick=\"getStateBeforeWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option01.png' style='width:21px; height:21px;'></a>");
						sb.append("&nbsp;&nbsp;<a data-original-title='Cover Letter' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
						sb.append("&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
					}
					
					sb.append("</td>");	   				
					sb.append("</tr>");
				}
			}
			/////////////////// widraw end ///////////////////////////
			if(statusHiden)//for Hidden
			{
				//totaRecord=totaRecord+lstHideJFT.size();
				for(JobOrder jb : lstsortedJobOrder)
				{
					rowCount++;
					sb.append("<tr>");
					sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");
					
			//		sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");
					
					sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+"</td>");
					
					if(jb.getCreatedForEntity()==3)
					{
						for(SchoolMaster school:jb.getSchool())
						{
							
							if(jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{	
								if(jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jb.getDistrictMaster().getDistrictId()+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}					
								
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							sb.append("<td>");
							sb.append("&nbsp;"+school.getSchoolName());
							sb.append("</td>");
							
							sb.append("<td>");
					//		sb.append("&nbsp;"+school.getAddress());
							sb.append("&nbsp;"+getSchoolAddress(jb));
							sb.append("</td>");
							
							break;
						}
					}
					else
					{
						
						if(jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
						{
							sb.append("<td>");
							sb.append(""+jb.getDistrictMaster().getDisplayName());
							sb.append("</td>");
						}
						else
						{
							sb.append("<td>");
							sb.append(""+jb.getDistrictMaster().getDistrictName());
							sb.append("</td>");
						}
						if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
						{	
							if(jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
								//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}else{
								sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jb.getDistrictMaster().getDistrictId()+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}
						}
						else
						{
							sb.append("<td>");							
							sb.append("</td>");
						}
						if(jb.getSchool().size()==1)
						{
							if(jb.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>"+getDistrictAddress(jb)+"</td>");
							}else{
							sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
							sb.append("<td>");
							sb.append(""+getSchoolAddress(jb));
							sb.append("</td>");
							}
						}
						else if(jb.getSchool().size()>1)
						{
							String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
							for(int i=1;i<jb.getSchool().size();i++)
							{
								schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
							}
							if(jb.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>"+getDistrictAddress(jb)+"</td>");
							}else{
							sb.append("<td><input type='hidden' id='schoolIds' value='"+schoolListIds+"'><a href='javascript:void(0);' onclick=showSchoolList()>Many Schools</a></td>");
							sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList()>Many Address</a></td>");
							}
						}
						else
						{
							sb.append("<td>&nbsp;</td>");
							sb.append("<td>");
							sb.append(""+getDistrictAddress(jb));
							sb.append("</td>");
						}
					}

					sb.append("<td><strong class='text-error'>Hidden</strong></td>");
					sb.append("<td>");
					if(isEpiStandalone){						
						sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
					}
					else{
						sb.append("<a data-original-title='Un-hide' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"unHideJob('"+jb.getIpAddress()+"');\"><img src='images/option03.png' style='width:21px; height:21px;'></a>");
						sb.append("&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
					}
					
					sb.append("</td>");	   				
					sb.append("</tr>");
				}
			}
			/////////////////// Hidden end ///////////////////////////
			if(statusAll)//for statusAll
			{
				//totaRecord=totaRecord+lstHideJFT.size();

				//for(JobForTeacher jbTeacher : finalListForJobsOfInterest)
				for(JobOrder jb : lstsortedJobOrder)
				{
					rowCount++;
					sb.append("<tr>");
					sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");
					
		//			sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");
					
					sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+"</td>");
					
					if(jb.getCreatedForEntity()==3)
					{
						for(SchoolMaster school:jb.getSchool())
						{
							if(jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{				
								if(jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jb.getDistrictMaster().getDistrictId()+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							sb.append("<td>");
							sb.append("&nbsp;"+school.getSchoolName());
							sb.append("</td>");
							
							sb.append("<td>");
					//		sb.append(school.getAddress());
							sb.append("&nbsp;"+getSchoolAddress(jb));
							sb.append("</td>");
							
							break;
						}
					}
					else
					{
						if(jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
						{
							sb.append("<td>");
							sb.append(""+jb.getDistrictMaster().getDisplayName());
							sb.append("</td>");
						}
						else
						{
							sb.append("<td>");
							sb.append(""+jb.getDistrictMaster().getDistrictName());
							sb.append("</td>");
						}
						if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
						{						
							if(jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
								//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}else{
								sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jb.getDistrictMaster().getDistrictId()+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}
						}
						else
						{
							sb.append("<td>");							
							sb.append("</td>");
						}

						if(jb.getSchool().size()==1)
						{
							if(jb.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>"+getDistrictAddress(jb)+"</td>");
							}else{
							sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
							sb.append("<td>");
							sb.append(""+getSchoolAddress(jb));
							sb.append("</td>");
							}
						}
						else if(jb.getSchool().size()>1)
						{
							String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
							for(int i=1;i<jb.getSchool().size();i++)
							{
								schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
							}
							if(jb.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>"+getDistrictAddress(jb)+"</td>");
							}else{
							sb.append("<td><input type='hidden' id='schoolIds' value='"+schoolListIds+"'><a href='javascript:void(0);' onclick=showSchoolList()>Many Schools</a></td>");
							sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList()>Many Address</a></td>");
							}
						}
						else
						{
							sb.append("<td>&nbsp;</td>");
							sb.append("<td>");
							sb.append(""+getDistrictAddress(jb));
							sb.append("</td>");
						}
					}

					if(jb.getExitURL().equalsIgnoreCase("Incomplete"))
					{
						sb.append("<td><strong class='text-error'>Incomplete</strong></td>");
						sb.append("<td>");

						

						if(isEpiStandalone){
							sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png'></a>");
						}
						else{
							/*if(prefStatus == false || portfolioStatus == false){
								sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"chkJobSpecificInventory("+prefStatus+","+portfolioStatus+","+false+");\"><img src='images/option02.png'></a>");
							}
							else if(jb.getJobCategoryMaster().getBaseStatus() && (!baseInvStatus)){
								sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('0')\"><img src='images/option02.png'></a>");
							}
							else{
								sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('"+jb.getJobId()+"')\"><img src='images/option02.png'></a>");
							}*/
							sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"getTeacherCriteria('"+jb.getJobId()+"');\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='Withdraw' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='Cover Letter' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						
						sb.append("</td>");	   		

					}
					else if(jb.getExitURL().equalsIgnoreCase("Available"))
					{
						sb.append("<td><strong class='text-info'>Available</strong></td>");
						sb.append("<td>");
						if(isEpiStandalone){
							sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png'></a>");
						}
						else{
							sb.append("<a data-original-title=' Apply Now' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jb.getJobId()+"' ><img src='images/option01.png' style='width:21px; height:21px;' ></a>");
							sb.append("&nbsp;<a data-original-title='Hide' rel='tooltip' id='tpHide"+rowCount+"' href='#' onclick=\"return getJbsIHide('"+jb.getJobId()+"')\"><img src='images/unhide.png' style='width:21px; height:21px;' ></a>");
							sb.append("&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						
						sb.append("</td>");	   		
					}
					else if(jb.getExitURL().equalsIgnoreCase("Completed"))
					{
						sb.append("<td><strong class='text-success'>Completed</strong></td>");
						sb.append("<td>");
						if(isEpiStandalone){
							sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							sb.append("<a data-original-title='Withdraw' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='Cover Letter' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						
						sb.append("</td>");	
					}
					else if(jb.getExitURL().equalsIgnoreCase("Timed Out"))
					{
						sb.append("<td><strong class='text-error'>Timed Out</strong></td>");
						sb.append("<td>");
						if(isEpiStandalone){
							sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							sb.append("<a data-original-title='Withdraw' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='Cover Letter' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						
						sb.append("</td>");	   	
					}
					else if(jb.getExitURL().equalsIgnoreCase("Withdrawn"))
					{
						sb.append("<td><strong class='text-error'>Withdrawn</strong></td>");
						sb.append("<td>");
						if(isEpiStandalone){
							sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png'></a>");
						}
						else{
							sb.append("<a data-original-title=' Re-Apply' rel='tooltip' id='tpApply"+rowCount+"' href='javascript:void(0);' onclick=\"getStateBeforeWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option01.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='Cover Letter' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						
						sb.append("</td>");	   		
					}
					else if(jb.getExitURL().equalsIgnoreCase("Hidden"))
					{
						sb.append("<td><strong class='text-error'>Hidden</strong></td>");
						sb.append("<td>");
						if(isEpiStandalone){
							sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							//sb.append("<a data-original-title=' Apply Now' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jb.getJobId()+"' ><img src='images/option01.png'></a>");
							sb.append("<a data-original-title='Un-hide' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"unHideJob('"+jb.getIpAddress()+"');\"><img src='images/option03.png' style='width:21px; height:21px;'></a>");
							//sb.append("&nbsp;&nbsp;<a data-original-title='Cover Letter' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png'></a>");
							sb.append("&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						sb.append("</td>");	 
					}

					sb.append("</tr>");
				}
			}
			if(rowCount==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='6'>");
				sb.append("No record found.");
				sb.append("</td>");	   				
				sb.append("</tr>");
			}

			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			
			out.write(sb.toString());

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}


	@RequestMapping(value="/jobwithdraw.do", method=RequestMethod.POST)
	public String doJobWithdrawPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("@@@@@@@@@@@@@@@@@@@::::::::::::::jobwithdraw.do:::::::::::::::");
		HttpSession session = request.getSession(false);
		try 
		{
			PrintWriter out = response.getWriter();
			if (session == null || session.getAttribute("teacherDetail") == null) 
			{	
				out.write("100001");
				return null;
			}
			List<StatusMaster> lstStatusMaster=null;
			Map<String,StatusMaster> getStatusMap=new HashMap<String, StatusMaster>();
			try{
				lstStatusMaster=WorkThreadServlet.statusMasters;
				for (StatusMaster statusMaster : lstStatusMaster) {
					getStatusMap.put(statusMaster.getStatusShortName(),statusMaster);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			StatusMaster statusMaster = getStatusMap.get("widrw");
			
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			String jobForTeacherId 		= 	request.getParameter("jobForTeacherId")==null?"":request.getParameter("jobForTeacherId");
			String mailSentFlag 		= 	request.getParameter("mailSentFlag")==null?"":request.getParameter("mailSentFlag");
			JobForTeacher jobForTeacher = 	jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
			
			WithdrawnReasonMaster withdrawnReasonMaster = null;
			try{
				String withdrawnReasonId= 	request.getParameter("withdrawnReasonId")==null?"":request.getParameter("withdrawnReasonId");
				if(withdrawnReasonId!="")
					withdrawnReasonMaster   = 	withdrawnReasonMasterDAO.findById(Integer.parseInt(withdrawnReasonId), false, false);

				System.out.println(jobForTeacherId+":::::::::::: Withdrawn Reason Id ::::::::::::::"+withdrawnReasonId);
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			jobForTeacher.setWithdrwanDateTime(new Date());
			
			out.print(jobForTeacher.getStatus().getStatusShortName());
			///////////////////Start isMiami////////////////////////
			
			if(mailSentFlag.equals("Yes")){
					if(jobForTeacher.getStatus()!=null	&& !jobForTeacher.getStatus().getStatusShortName().equals("hird")){
						boolean isMiami=false,statusUpdate=false,statusOfferAccepted = false,statusOfferReady = false;
						try{
							if(jobForTeacher.getJobId().getDistrictMaster()!=null && jobForTeacher.getJobId().getDistrictMaster().getDistrictId().equals(1200390))
								isMiami=true;
						}catch(Exception e){};
						
						try{
							if(jobForTeacher.getOfferAccepted()!=null && jobForTeacher.getOfferAccepted()){
								statusOfferAccepted	 =	true;
							}
							if(jobForTeacher.getOfferReady()!=null){
								statusOfferReady	 =	true;
							}
							System.out.println(":::::::statusOfferAccepted:::::::"+statusOfferAccepted);
							System.out.println("statusOfferReady:::::::"+statusOfferReady);
						}catch(Exception e){}
						
						SchoolMaster panelSchool=null;
						/* Set Unhire Email And Send Email to DA & SA */ 
						if(isMiami && (statusOfferReady || statusOfferAccepted)){
							System.out.println("::::::::::::indisde ::::::::::::::::::::::");
							UserMaster userMaster	=	jobForTeacher.getUpdatedBy();
							
							List<String> requisitionNumbers= new ArrayList<String>();
							List<JobOrder> jobOrders= new ArrayList<JobOrder>();
							List<TeacherDetail> teacherDetails= new ArrayList<TeacherDetail>();
								if(jobForTeacher.getRequisitionNumber()!=null)
									requisitionNumbers.add(jobForTeacher.getRequisitionNumber());
								
								jobOrders.add(jobForTeacher.getJobId());
								teacherDetails.add(jobForTeacher.getTeacherId());
							List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();
							List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
							try{
								jobWisePanelStatusList =jobWisePanelStatusDAO.findJobWisePanelStatusList(jobOrders);
								if(jobWisePanelStatusList.size()>0){
									panelScheduleList=panelScheduleDAO.findPanelScheduleList(teacherDetails,jobOrders,jobWisePanelStatusList);
								}
							}catch(Exception e){e.printStackTrace();}
			
							boolean staffer=false;
							Map<String, Integer> mapPanelSchedule=new HashMap<String, Integer>();
							Map<Integer, List<UserMaster>> mapPanelAttendee=new HashMap<Integer, List<UserMaster>>();
							if(panelScheduleList!=null && panelScheduleList.size() > 0)
							{
								for(PanelSchedule schedule:panelScheduleList)
								{
									if(schedule!=null)
									{
										String sJID_TID=schedule.getJobOrder().getJobId()+"_"+schedule.getTeacherDetail().getTeacherId();
										if(mapPanelSchedule.get(sJID_TID)==null)
											mapPanelSchedule.put(sJID_TID, schedule.getPanelId());
									}
								}
								List<PanelAttendees> panelAttendees=new ArrayList<PanelAttendees>();
								panelAttendees=panelAttendeesDAO.getPanelAttendeeList(panelScheduleList);
								if(panelAttendees!=null && panelAttendees.size() > 0)
								{
									PrintOnConsole.debugPrintln("Withdraw","panelAttendees() "+panelAttendees.size());
									for(PanelAttendees attendees:panelAttendees)
									{
										if(attendees!=null)
										{
											if(attendees.getPanelInviteeId()!=null){
												UserMaster panelUserMaster=attendees.getPanelInviteeId();
												if(panelUserMaster.getEntityType()==3){
													panelSchool=panelUserMaster.getSchoolId();
												}
												if(panelUserMaster.getUserId().equals(userMaster.getUserId())){
													staffer=true;
												}
											}
											List<UserMaster> lstTemp=new ArrayList<UserMaster>();
											if(mapPanelAttendee.get(attendees.getPanelSchedule().getPanelId())==null)
											{
												lstTemp=new ArrayList<UserMaster>();
												lstTemp.add(attendees.getPanelInviteeId());
												mapPanelAttendee.put(attendees.getPanelSchedule().getPanelId(), lstTemp);
											}
											else
											{
												lstTemp=new ArrayList<UserMaster>();
												lstTemp=mapPanelAttendee.get(attendees.getPanelSchedule().getPanelId());
												lstTemp.add(attendees.getPanelInviteeId());
												mapPanelAttendee.put(attendees.getPanelSchedule().getPanelId(), lstTemp);
											}
										}
									}
								}
							}
							Map<Integer,List<String>> statusHistoryMap=new HashMap<Integer, List<String>>();
							Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
							Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
							List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
							
							List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobForTeacher.getJobId());
							LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
							Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
							CandidateGridService cgService=new CandidateGridService();
							List<TeacherStatusNotes> statusNoteList=new ArrayList<TeacherStatusNotes>();
							try{
								statusNoteList=teacherStatusNotesDAO.getStatusNoteAllList(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
							}catch(Exception e){
								e.printStackTrace();
							}
							if(staffer){
								try{
									int counter=0; 
									for(SecondaryStatus tree: lstTreeStructure)
									{
										if(tree.getSecondaryStatus()==null)
										{
											if(tree.getChildren().size()>0){
												counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
											}
										}
									}
								}catch(Exception e){}
								
								if(statusNoteList.size()>0)
								for (TeacherStatusNotes teacherStatusNotes : statusNoteList) {
									if((teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready"))){
										teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
									}
								}
								boolean vComp=false;
								if(historyList.size()>0)
								for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
									if((teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready"))){
										if(teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp")){
											vComp=true;	
										}
										teacherStatusHistoryForJobDAO.makeTransient(teacherStatusHistoryForJob);
									}
									
								}
								
								try{		
									historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
									List<String> statusList=new ArrayList<String>();
									if(historyList.size()>0)
									for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
										if(statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null){
											statusList=new ArrayList<String>();
											if(teacherStatusHistoryForJob.getStatusMaster()!=null){
												statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
											}else{
												statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
											}
											statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
										}else{
											statusList=statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
											if(teacherStatusHistoryForJob.getStatusMaster()!=null){
												statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
											}else{
												statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
											}
											statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
										}
									}
								}catch(Exception e){}
								
								System.out.println("statusHistoryMap:::::::::::"+statusHistoryMap.size());
								
								List<String> statusIds=statusHistoryMap.get(jobForTeacher.getTeacherId().getTeacherId());
								String schoolStatusId=cgService.getSchoolStatusId(statusIds, statusHistMap, statusNameMap);
								SecondaryStatus secondaryStatusObj=null;
								StatusMaster statusMasterObj=null;
								try{
									String statusIdMapValue[]=schoolStatusId.split("#");
									if(statusIdMapValue[0].equals("0")){
										secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
										jobForTeacher.setSecondaryStatus(secondaryStatusObj);
										if(vComp){
											jobForTeacher.setStatus( getStatusMap.get("ecomp"));
											jobForTeacher.setStatusMaster(null);
										}
									}else{
										statusMasterObj=statusMasterMap.get(schoolStatusId);
									}
								}catch(Exception e){e.printStackTrace();}
								
								System.out.println("schoolStatusId:::::::::::::::>>>>>>>>>>>>>>>>>>>>"+schoolStatusId);
								if(statusMasterObj!=null){
									System.out.println("statusMasterObj::::::::::: "+statusMasterObj.getStatus());
								}else if( secondaryStatusObj!=null){
									System.out.println("secondaryStatusObj::::::::::: "+secondaryStatusObj.getSecondaryStatusName());
								}
								Map<Integer,Integer> jwpMap= new HashMap<Integer,Integer>();
								Map<String,PanelSchedule> sldMap= new HashMap<String,PanelSchedule>();
				
								if(jobWisePanelStatusList.size()>0){
									for (JobWisePanelStatus jobWisePanelStatus : jobWisePanelStatusList) {
										jwpMap.put(jobWisePanelStatus.getJobOrder().getJobId(),jobWisePanelStatus.getJobPanelStatusId());
									}
								}
								if(panelScheduleList.size()>0){
									for (PanelSchedule panelSchedule : panelScheduleList) {
										sldMap.put(panelSchedule.getJobOrder().getJobId()+"##"+panelSchedule.getTeacherDetail().getTeacherId()+"##"+panelSchedule.getJobWisePanelStatus().getJobPanelStatusId(), panelSchedule);
									}
								}
								int jobPanelStatusId =0;
								if(jwpMap.get(jobForTeacher.getJobId().getJobId())!=null){
									jobPanelStatusId=jwpMap.get(jobForTeacher.getJobId().getJobId());
								}
								String keyforSLD=jobForTeacher.getJobId().getJobId()+"##"+jobForTeacher.getTeacherId().getTeacherId()+"##"+jobPanelStatusId;
								if(sldMap.get(keyforSLD)!=null){
									PanelSchedule panelSchedule =sldMap.get(keyforSLD);
									panelSchedule.setPanelStatus("Cancelled");
									panelScheduleDAO.updatePersistent(panelSchedule);
								}
								System.out.println("mapPanelAttendee:::::::"+mapPanelAttendee.size());
								System.out.println("mapPanelSchedule:::::::"+mapPanelSchedule.size());
							}
							
							jobForTeacher.setOfferReady(null);
							jobForTeacher.setOfferMadeDate(new Date());
							jobForTeacher.setRequisitionNumber(null);
							jobForTeacher.setOfferAcceptedDate(new Date());
							jobForTeacher.setOfferAccepted(null);
							jobForTeacher.setStatus(statusMaster);
							jobForTeacher.setStatusMaster(statusMaster);
							jobForTeacherDAO.updatePersistent(jobForTeacher);
							try{
								if(jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null){
									List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobForTeacher.getJobId(),jobForTeacher.getJobId().getDistrictMaster(),jobForTeacher.getRequisitionNumber());
									if(jobReqList.size()>0){
										boolean updateFlag=false;
										for (JobRequisitionNumbers jobReqObj : jobReqList) {
											try{
												if(panelSchool!=null && jobReqObj.getSchoolMaster().getSchoolId().equals(panelSchool.getSchoolId()) && jobReqObj.getStatus()==1){
													updateFlag=true;
													jobReqObj.setStatus(0);
													jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
													break;
												}
											}catch(Exception e){
												e.printStackTrace();
											}
										}
										if(updateFlag==false){
											for (JobRequisitionNumbers jobReqObj : jobReqList) {
												try{
													if(jobReqObj.getStatus()==1){
														updateFlag=true;
														jobReqObj.setStatus(0);
														jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
														break;
													}
												}catch(Exception e){
													e.printStackTrace();
												}
											}
										}
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							try {
								sendEmailOnRejectOrUnHire(jobForTeacher, mapPanelSchedule, mapPanelAttendee,"Reject");
							} catch (Exception e) {
								e.printStackTrace();
							}
						}else{
							///////////////////End isMiami////////////////////////
							jobForTeacher.setStatus(statusMaster);
							jobForTeacher.setStatusMaster(statusMaster);
							if(statusMaster!=null){
								jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
							}
							jobForTeacherDAO.makePersistent(jobForTeacher);
							
							try{
								TeacherStatusHistoryForJob tSHJ=null;
								StatusMaster statusMasterObj  =WorkThreadServlet.statusMap.get("widrw");
								tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterObj,"A");
								UserMaster userMaster = new UserMaster();
								System.out.println(":::::::::::::: tSHJ :::::::::::::::::"+tSHJ);
								if(tSHJ==null){
									System.out.println(":::::::: 111111111111111 :::::::::::::::::::");
									tSHJ= new TeacherStatusHistoryForJob();
									userMaster.setUserId(1);
									tSHJ.setStatus("A");
									tSHJ.setStatusMaster(statusMasterObj);
									tSHJ.setUpdatedDateTime(new Date());
									tSHJ.setUserMaster(userMaster);
									tSHJ.setActiontakenByCandidate(true);
									tSHJ.setJobOrder(jobForTeacher.getJobId());
									tSHJ.setTeacherDetail(jobForTeacher.getTeacherId());
									tSHJ.setWithdrawnReasonMaster(withdrawnReasonMaster);
									teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
									System.out.println(":::::::: 2222222222222222 :::::::::::::::::::");
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							
							List<UserMaster> lstusermaster	=	new ArrayList<UserMaster>();
							String[] arrHrDetail = commonService.getHrDetailToTeacher(request,jobForTeacher.getJobId());
							arrHrDetail[10]	=	Utility.getBaseURL(request)+"signin.do";
							arrHrDetail[11] =	Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jobForTeacher.getJobId().getJobId()+"&JobOrderType="+jobForTeacher.getJobId().getCreatedForEntity();
			
							String flagforEmail	="withdrawJob";
							System.out.println("::::::::::::::::::"+arrHrDetail.toString());
							MailSendToTeacher mst =new MailSendToTeacher(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),request,arrHrDetail,flagforEmail,lstusermaster,emailerService,jobForTeacher);
							Thread currentThread = new Thread(mst);
							currentThread.start();
							System.out.println(":::::::: 333333333333333333 :::::::::::::::::::");
						}
					  
						
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}		
		return null;
	}

	
	/* Mail Sent Function */
public void sendEmailOnRejectOrUnHire(JobForTeacher jobForTeacher,Map<String, Integer> mapPanelSchedule,Map<Integer, List<UserMaster>> mapPanelAttendee,String source)
	{
		System.out.println(":::::::::::::::::::::::sendEmailOnRejectOrUnHire::::::::::::::::::");
		String sJID_TID=jobForTeacher.getJobId().getJobId()+"_"+jobForTeacher.getTeacherId().getTeacherId();
		if(mapPanelSchedule.get(sJID_TID)!=null)
		{
			Integer iPanelId=mapPanelSchedule.get(sJID_TID);
			if(iPanelId!=null)
			{
				if(mapPanelAttendee.get(iPanelId)!=null)
				{
					List<UserMaster> lstEmailsUserMaster=new ArrayList<UserMaster>();
					lstEmailsUserMaster=mapPanelAttendee.get(iPanelId);
					
					if(lstEmailsUserMaster!=null && lstEmailsUserMaster.size() > 0)
					{
						List<String> lstPrincipleEmailAddress=new ArrayList<String>();
						List<String> lstStafferEmailAddress=new ArrayList<String>();
						
						String sCandidateName="";
						if(jobForTeacher.getTeacherId().getFirstName()!=null && !jobForTeacher.getTeacherId().getFirstName().equalsIgnoreCase(""))
							sCandidateName=jobForTeacher.getTeacherId().getFirstName();
						
						if(jobForTeacher.getTeacherId().getLastName()!=null && !jobForTeacher.getTeacherId().getLastName().equalsIgnoreCase(""))
							sCandidateName=sCandidateName+" "+jobForTeacher.getTeacherId().getLastName();
						
						UserMaster userMasterStaffer=new UserMaster();
						for(UserMaster userMasterTemp:lstEmailsUserMaster)
						{
							if(userMasterTemp!=null)
							{
								if(userMasterTemp.getEntityType()!=null && userMasterTemp.getEmailAddress()!=null && !userMasterTemp.getEmailAddress().equalsIgnoreCase(""))
								{
									if(userMasterTemp.getEntityType()==2)
										lstStafferEmailAddress.add(userMasterTemp.getEmailAddress());
									else if(userMasterTemp.getEntityType()==3)
										lstPrincipleEmailAddress.add(userMasterTemp.getEmailAddress());
									
									userMasterStaffer=userMasterTemp;
								}
							}
						}
						
						ScheduleMailThreadByListToBcc smt = new ScheduleMailThreadByListToBcc();
						
						String mailContent = MailText.PNRRejectOrUnHire(jobForTeacher.getTeacherId(), userMasterStaffer);
						smt.setEmailerService(emailerService);
						
						if(lstPrincipleEmailAddress!=null && lstPrincipleEmailAddress.size() > 0)
						{
							smt.setLstMailTo(lstPrincipleEmailAddress);
							if(lstStafferEmailAddress!=null && lstStafferEmailAddress.size() > 0)
								smt.setLstMailBcc(lstStafferEmailAddress);
						}
						else
						{
							if(lstStafferEmailAddress!=null && lstStafferEmailAddress.size() > 0)
								smt.setLstMailTo(lstStafferEmailAddress);
							else
								smt.setLstMailTo(null);
							
							smt.setLstMailBcc(null);
						}
						
						smt.setMailSubject(sCandidateName+" job withdraw");
						smt.setMailContent(mailContent);
						
						PrintOnConsole.debugPrintln("PNR "+source+"","Rejected Email");
						PrintOnConsole.debugPrintln("PNR "+source+"",sCandidateName+" is removed from On-Boarding");
						PrintOnConsole.debugPrintln("PNR "+source+"","mailContent "+mailContent);
						smt.start();
					}
					else
					{
						PrintOnConsole.debugPrintln("PNR "+source+"","Could not sent email .... for sJID_TID "+sJID_TID);
					}
				}
				else
				{
					PrintOnConsole.debugPrintln("PNR "+source+"","No PanelAttendee");
				}
			}
			else
			{
				PrintOnConsole.debugPrintln("PNR "+source+"","No PanelId");
			}
			
		}
		else
		{
			PrintOnConsole.debugPrintln("PNR "+source+"","No PanelSchedule");
			UserMaster userMaster	=	jobForTeacher.getUpdatedBy();
			if(userMaster != null ){
				String mailContent = MailText.PNRRejectOrUnHire(jobForTeacher.getTeacherId(), userMaster);
				emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), "job withdraw",mailContent);
				PrintOnConsole.debugPrintln("PNR "+source+"","mailContent "+mailContent);
			}else{
				PrintOnConsole.debugPrintln("PNR Email Not Sent");
			}
		}
	  }


	@RequestMapping(value="/jobhide.do", method=RequestMethod.POST)
	public String doJobHidePOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{

		HttpSession session = request.getSession(false);
		try 
		{
			PrintWriter out = response.getWriter();
			if (session == null || session.getAttribute("teacherDetail") == null) 
			{

				out.write("100001");
				return null;
			}
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			StatusMaster statusMaster = WorkThreadServlet.statusMap.get("hide");
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			String jobId = request.getParameter("jobId")==null?"":request.getParameter("jobId");

			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);

			JobForTeacher jobForTeacher = new JobForTeacher();
			jobForTeacher.setTeacherId(teacherDetail);
			jobForTeacher.setJobId(jobOrder);
			jobForTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
			jobForTeacher.setStatus(statusMaster);
			jobForTeacher.setCreatedDateTime(new Date());
			jobForTeacher.setStatusMaster(statusMaster);
			if(statusMaster!=null){
				jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
			}
			jobForTeacherDAO.makePersistent(jobForTeacher);

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return null;
	}


	@RequestMapping(value="/sharejob.do", method=RequestMethod.GET)
	public String doJobsestGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		try {
			if(session.getAttribute("teacherDetail")!=null){
				TeacherDetail tDetail = (TeacherDetail) session.getAttribute("teacherDetail");			
				map.addAttribute("tName", tDetail.getFirstName());
				map.addAttribute("tEmail", tDetail.getEmailAddress());



			}
			String jobId = request.getParameter("jobId");

			if(jobId!=null){
				JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
				map.addAttribute("jobOrder",jobOrder);
				String jobUrl = Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobOrder.getJobId();
				map.addAttribute("jobUrl",jobUrl);				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "sharejobs";
	}

	@RequestMapping(value="/sharejob.do", method=RequestMethod.POST)
	public String doJobsestPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		HttpSession session = request.getSession();
		try 
		{
			//TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

			String jobId = request.getParameter("jobId");

			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);

			//String teacherFname=teacherDetail.getFirstName()==null?"":teacherDetail.getFirstName();
			//String teacherLname=teacherDetail.getLastName()==null?"":teacherDetail.getLastName();

			String yrname = request.getParameter("yrname")==null?"":request.getParameter("yrname").trim();
			String yremail =  request.getParameter("yremail")==null?"":request.getParameter("yremail").trim();


			String fname1 = request.getParameter("fname1")==null?"":request.getParameter("fname1").trim();
			String fname2 = request.getParameter("fname2")==null?"":request.getParameter("fname2").trim();
			String fname3 = request.getParameter("fname3")==null?"":request.getParameter("fname3").trim();
			String fname4 = request.getParameter("fname4")==null?"":request.getParameter("fname4").trim();
			String fname5 = request.getParameter("fname5")==null?"":request.getParameter("fname5").trim();

			String fremail1 = request.getParameter("fremail1")==null?"":request.getParameter("fremail1").trim();
			String fremail2 = request.getParameter("fremail2")==null?"":request.getParameter("fremail2").trim();
			String fremail3 = request.getParameter("fremail3")==null?"":request.getParameter("fremail3").trim();
			String fremail4 = request.getParameter("fremail4")==null?"":request.getParameter("fremail4").trim();
			String fremail5 = request.getParameter("fremail5")==null?"":request.getParameter("fremail5").trim();

			boolean isLogin = false;
			if(session.getAttribute("teacherDetail")!=null)
				isLogin=true;
			if(!fname1.equals("") && !fremail1.equals(""))
			{
				JobShare jobShare = new JobShare();
				jobShare.setSenderName(yrname);
				jobShare.setSenderEmail(yremail);
				jobShare.setRecipientName(fname1);
				jobShare.setRecipientEmail(fremail1);
				jobShare.setIsUserLogin(isLogin);
				jobShare.setCreatedDatetime(new Date());
				jobShare.setIpAddress(IPAddressUtility.getIpAddress(request));
				jobShareDAO.makePersistent(jobShare);

				emailerService.sendMailAsHTMLText(fremail1, yrname+" found a job for you", MailText.getShareJobMailText(request, yrname,yremail, jobOrder, fname1, fremail1));
			}
			if(!fname2.equals("") && !fremail2.equals(""))
			{
				JobShare jobShare = new JobShare();
				jobShare.setSenderName(yrname);
				jobShare.setSenderEmail(yremail);
				jobShare.setRecipientName(fname2);
				jobShare.setRecipientEmail(fremail2);
				jobShare.setIsUserLogin(isLogin);
				jobShare.setCreatedDatetime(new Date());
				jobShare.setIpAddress(IPAddressUtility.getIpAddress(request));
				jobShareDAO.makePersistent(jobShare);
				emailerService.sendMailAsHTMLText(fremail2, yrname+" found a job for you", MailText.getShareJobMailText(request, yrname,yremail, jobOrder, fname2, fremail2));
			}
			if(!fname3.equals("") && !fremail3.equals(""))
			{
				JobShare jobShare = new JobShare();
				jobShare.setSenderName(yrname);
				jobShare.setSenderEmail(yremail);
				jobShare.setRecipientName(fname3);
				jobShare.setRecipientEmail(fremail3);
				jobShare.setIsUserLogin(isLogin);
				jobShare.setCreatedDatetime(new Date());
				jobShare.setIpAddress(IPAddressUtility.getIpAddress(request));
				jobShareDAO.makePersistent(jobShare);
				emailerService.sendMailAsHTMLText(fremail3, yrname+" found a job for you", MailText.getShareJobMailText(request, yrname,yremail, jobOrder, fname3, fremail3));
			}
			if(!fname4.equals("") && !fremail4.equals(""))
			{
				JobShare jobShare = new JobShare();
				jobShare.setSenderName(yrname);
				jobShare.setSenderEmail(yremail);
				jobShare.setRecipientName(fname4);
				jobShare.setRecipientEmail(fremail4);
				jobShare.setIsUserLogin(isLogin);
				jobShare.setCreatedDatetime(new Date());
				jobShare.setIpAddress(IPAddressUtility.getIpAddress(request));
				jobShareDAO.makePersistent(jobShare);
				emailerService.sendMailAsHTMLText(fremail4, yrname+" found a job for you", MailText.getShareJobMailText(request, yrname,yremail, jobOrder, fname4, fremail4));
			}
			if(!fname5.equals("") && !fremail5.equals(""))
			{
				JobShare jobShare = new JobShare();
				jobShare.setSenderName(yrname);
				jobShare.setSenderEmail(yremail);
				jobShare.setRecipientName(fname5);
				jobShare.setRecipientEmail(fremail5);
				jobShare.setIsUserLogin(isLogin);
				jobShare.setCreatedDatetime(new Date());
				jobShare.setIpAddress(IPAddressUtility.getIpAddress(request));
				jobShareDAO.makePersistent(jobShare);
				emailerService.sendMailAsHTMLText(fremail5, yrname+" found a job for you", MailText.getShareJobMailText(request, yrname,yremail, jobOrder, fname5, fremail5));
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();	
		}
		
		
		if(session.getAttribute("epiStandalone")!=null){
			return "redirect:portaldashboard.do?mm="+Calendar.getInstance().getTimeInMillis();
		}

		if(session.getAttribute("teacherDetail")!=null)
			return "redirect:userdashboard.do?mm="+Calendar.getInstance().getTimeInMillis();
		else{

			String referer = request.getParameter("refererPageURL");
			if(referer==null)
				return "redirect:userdashboard.do?mm="+Calendar.getInstance().getTimeInMillis();
			else
				return "redirect:"+referer;

		}

	}
	@RequestMapping(value="/applyjob.do", method=RequestMethod.GET)
	public String doApplyNowGET(ModelMap map,HttpServletRequest request)
	{
		//System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		HeadQuarterMaster hQ= null;
		try{
		
	    hQ= headQuarterMasterDAO.findById(2, false, false);
	    if(hQ!=null)
	    {
		System.out.println("Pavan Test "+hQ.getHeadQuarterId());
		map.addAttribute("HQID", hQ.getHeadQuarterId());
	    }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
			
		
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		try 
		{
			Utility.setAllRefererURL(request);
			DistrictMaster districtMaster = null;
			TeacherAssessmentStatus teacherAssessmentStatus = null;
			String jobid = request.getParameter("jobId")==null?"0":request.getParameter("jobId");
			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobid), false, false);	
			String startDate="";
			String endDate="";
			String positionStart ="";
			String positionStartDate="";
			String jobStatusFlag = "I";
			if(jobOrder!=null){
				startDate = Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobStartDate());
				endDate   = Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobEndDate());
				if(jobOrder.getPositionStart()!=null)
					 positionStart = jobOrder.getPositionStart();
						else 
					 positionStart = "N/A";
				if(jobOrder.getPositionStartDate()!=null){
					positionStartDate=Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getPositionStartDate());
				}
				jobStatusFlag = jobOrder.getStatus();
				
				List<DistrictPortfolioConfig> lstdistrictPortfolioConfig = districtPortfolioConfigDAO.getPortfolioConfig("E",jobOrder);
				DistrictPortfolioConfig districtPortfolioConfig=null;
				if(lstdistrictPortfolioConfig!=null && lstdistrictPortfolioConfig.size()>0)
				{
					districtPortfolioConfig=lstdistrictPortfolioConfig.get(0);
				}
				boolean isDspqReqForKelly=false;
				boolean isKelly=false;
				boolean isCoverLetterNeeded = false;
				if(districtPortfolioConfig!=null)
				{
					if(districtPortfolioConfig.getCoverLetter())
						isCoverLetterNeeded = true;
					
					if(jobOrder!=null && jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId()==1 && jobOrder.getIsInviteOnly()!=null && jobOrder.getIsInviteOnly())
						isCoverLetterNeeded = false;
					else if(jobOrder!=null && jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId()==1)
						isCoverLetterNeeded = true;
					
					if(jobOrder!=null && jobOrder.getHeadQuarterMaster()!=null)
					{
						isKelly=true;
						isDspqReqForKelly=jobOrder.getJobCategoryMaster().getOfferDSPQ();
					}
				}
				
				System.out.println("isCoverLetterNeeded:: "+isCoverLetterNeeded);
				map.addAttribute("isCoverLetterNeeded", isCoverLetterNeeded);
				map.addAttribute("isKelly", isKelly);
				map.addAttribute("isDspqReqForKelly", isDspqReqForKelly);
				
				
				
				if(jobStatusFlag.equalsIgnoreCase("A"))
				{
					Date currTime = Utility.getDateWithoutTime();
					if(currTime.equals(jobOrder.getJobStartDate()) || currTime.equals(jobOrder.getJobEndDate()))
						jobStatusFlag="A";
					else if(Utility.between(currTime,jobOrder.getJobStartDate(), jobOrder.getJobEndDate()))
					{
						jobStatusFlag="A";
					}
					else 
					{
						if(currTime.before(jobOrder.getJobStartDate()))
						{
							System.out.println("Inactive");
							jobStatusFlag="I";
						}else if(!Utility.checkDateBetweenTwoDate(jobOrder.getJobStartDate(), jobOrder.getJobEndDate()))
						{
							System.out.println("expired");
							jobStatusFlag="E";
						}
					}
				}
				System.out.println("jobStatusFlag:::: "+jobStatusFlag);
			}
			map.addAttribute("jobStatusFlag",jobStatusFlag);
			map.addAttribute("jobOrder",jobOrder);
			map.addAttribute("startDate",startDate);
			map.addAttribute("endDate",endDate);	
			map.addAttribute("positionStart",positionStart);	
			map.addAttribute("positionStartDate",positionStartDate);
			boolean isMiami = false;
			String districtDName=null;
			
			
			if(jobOrder.getDistrictMaster()!=null){
				districtMaster=jobOrder.getDistrictMaster();
				if(districtMaster.getDistrictId().equals(1200390))
					isMiami=true;
				
				if(districtMaster.getDisplayName() !=null && !districtMaster.getDisplayName().equals("")){
					districtDName=districtMaster.getDisplayName();
				}else{
					districtDName=districtMaster.getDistrictName();
				}
			}
			else if(jobOrder.getHeadQuarterMaster()!=null){
				districtDName = jobOrder.getHeadQuarterMaster().getHeadQuarterName();
			}
			
			String jeffcoSpecificSchool="";
			if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("804800")){					
				if(jobOrder.getRequisitionNumber()!=null && !jobOrder.getRequisitionNumber().equalsIgnoreCase("")){
					DistrictRequisitionNumbers districtRequisitionNumbers = districtRequisitionNumbersDAO.getDistrictRequisitionNumber(jobOrder.getDistrictMaster(), jobOrder.getRequisitionNumber());
					if(districtRequisitionNumbers!=null){
						JobRequisitionNumbers jobRequisitionNumbers = jobRequisitionNumbersDAO.findJobReqNumbersObj(districtRequisitionNumbers);
							if(jobRequisitionNumbers!=null){
								jeffcoSpecificSchool = jobRequisitionNumbers.getSchoolMaster().getSchoolName()+"</br>"+BasicController.getSchoolFullAddress(jobOrder);
							}
						
					}
				}
				
				map.addAttribute("jeffcoSpecificSchool", jeffcoSpecificSchool);
			}
			
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");

			//////**** Sekhar Add Code ******///////
			JobForTeacher jobForTeacher = jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
			
			/* Sekhar Add Branch Details*/
			BranchesAjax branchesAjax=new BranchesAjax();
			List<JobForTeacher> jobForTeacherKes=jobForTeacherDAO.findJobByTeacherForKES(teacherDetail,true);
			try{
				if(jobForTeacherKes.size()>0){
					String kesData=branchesAjax.getBranchDetails(jobForTeacherKes.get(0).getJobId());
					if(kesData!=null){
						map.addAttribute("kesData",kesData);
						jobStatusFlag="H";
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("jobStatusFlag",jobStatusFlag);
			districtMaster=jobOrder.getDistrictMaster();
			
			List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
			boolean internalFlag=false;
			boolean offerEPI =false;
			boolean offerJSI =false;
			if(itcList.size()>0){
				internalFlag=true;
			}
			
			boolean isPortfolio =false;
			if(jobOrder.getDistrictMaster()!=null && internalFlag){
				districtMaster = jobOrder.getDistrictMaster();
				if(districtMaster.getOfferPortfolioNeeded()){
					isPortfolio=true;
				}
				if(districtMaster.getOfferEPI()){
					offerEPI=true;
				}
				if(districtMaster.getOfferJSI()){
					offerJSI=true;
				}
			}
			 int IsAffilated=0;
			try{
				
				 if(jobForTeacher!=null && jobForTeacher.getIsAffilated()!=null){
					 if(jobForTeacher.getIsAffilated()==1)
					 IsAffilated=1;
				 }
			    if(jobOrder.getJobCategoryMaster()!=null && IsAffilated==1 && internalFlag==false){
						if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
							offerEPI=true;
						}else{
							offerEPI=false;
						}
						if(jobOrder.getJobCategoryMaster().getOfferJSI()!=0){
							offerJSI=true;
						}else{
							offerJSI=false;
						}
						if(jobOrder.getJobCategoryMaster().getOfferPortfolioNeeded()){
							isPortfolio=true;
						}else{
							isPortfolio=true;
						}
				 }
			}catch(Exception e){
				e.printStackTrace();
			}
			//////**** Sekhar End Code ******///////
			
			List<NationalBoardCertificationMaster> nationalBoardCertificationMasters=nationalBoardCertificationMasterDAO.getdistrictCustomQuestion(districtMaster);	
			
			map.addAttribute("customQuestionMultipleOption",nationalBoardCertificationMasters);
			
			TeacherPortfolioStatus tPortfolioStatus = null;

			
			JobForTeacher jFTCover		= jobForTeacherDAO.findJobByTeacherAndCoverLetter(teacherDetail);
			if(jobForTeacher!=null)
			{
				if(isMiami && jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Aspiring Assistant Principal") && jobForTeacher.getIsAffilated()!=null && jobForTeacher.getIsAffilated()==1 && jobForTeacher.getFlagForDistrictSpecificQuestions()==null)
				{
					jobForTeacher.setFlagForDistrictSpecificQuestions(true);
				}
			}
			map.addAttribute("jobForTeacher", jobForTeacher);
			tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			map.addAttribute("teacherDetail", teacherDetail);
			map.addAttribute("offerIsPortfolio",isPortfolio);
			if(isPortfolio){
				if((jobOrder.getIsPortfolioNeeded()!=null && jobOrder.getIsPortfolioNeeded())||(IsAffilated==1 && internalFlag==false && isPortfolio)||(internalFlag==true && isPortfolio))
				if(tPortfolioStatus==null)
				{
					map.addAttribute("portfolioStatus", true);
					map.addAttribute("portfolioURL", "personalinfo.do");
				}
				else if(!tPortfolioStatus.getIsPersonalInfoCompleted())
				{
					map.addAttribute("portfolioStatus", true);
					map.addAttribute("portfolioURL", "personalinfo.do");				
				}
				else if(!tPortfolioStatus.getIsAcademicsCompleted())
				{
					map.addAttribute("portfolioStatus", true);
					map.addAttribute("portfolioURL", "academics.do");				
				}
				else if(!tPortfolioStatus.getIsCertificationsCompleted())
				{
					map.addAttribute("portfolioStatus", true);
					map.addAttribute("portfolioURL", "certification.do");				
				}
				else if(!tPortfolioStatus.getIsExperiencesCompleted())
				{
					map.addAttribute("portfolioStatus", true);
					map.addAttribute("portfolioURL", "experiences.do");				
				}
				else if(!tPortfolioStatus.getIsAffidavitCompleted())
				{
					map.addAttribute("portfolioStatus", true);
					map.addAttribute("portfolioURL", "affidavit.do");				
				}
				else
				{
					map.addAttribute("portfolioStatus", false);								
				}	
			}else{
				map.addAttribute("portfolioStatus", false);	
			}
			teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			
			if((offerEPI && internalFlag )||(internalFlag==false && IsAffilated==0) ||(internalFlag==false && IsAffilated==1 && offerEPI)){
				if(jobOrder.getJobCategoryMaster().getBaseStatus())
				if(jobOrder.getJobCategoryMaster().getBaseStatus() && teacherAssessmentStatus==null)
				{
					map.addAttribute("startBaseAssessStatus", true);				
				}
				else if(jobOrder.getJobCategoryMaster().getBaseStatus() && teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
				{
					map.addAttribute("resumeBaseAssessStatus", true);				
				}	
			}
			
			
			if(jobForTeacher==null)
			{
				map.addAttribute("applyJobStatus", true);
				
				if(jFTCover==null)
					map.addAttribute("latestCoverletter","");
				else
					map.addAttribute("latestCoverletter",jFTCover.getCoverLetter());
			}
			
			if(jobOrder.getJobAssessmentStatus()==1 && ((offerJSI && internalFlag )||(internalFlag==false && IsAffilated==0) ||(internalFlag==false && IsAffilated==1 && offerJSI))){
				if(jobOrder.getIsJobAssessment()!=null &&  jobOrder.getIsJobAssessment()==true)
				if((jobForTeacher!=null) && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("icomp"))
				{
					List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail, jobOrder);
					if(lstTeacherAssessmentStatus==null || lstTeacherAssessmentStatus.size()==0)
					{
						map.addAttribute("startJobSpecAssessStatus", true);
					}
					else
					{
						teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
						map.addAttribute("resumeJobSpecAssessStatus", true);
					}
				}	
			}


			String source = "";
			String target = "";
			String path = "";
			
			// Anurag for display logo for kelly------ strat------
			
			if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId().equals(1)){
				
				boolean isLogoExist=false;
				
				try {
					
					
				 	HeadQuarterMaster headQuarterMaster = jobOrder.getHeadQuarterMaster(); 
					BranchMaster branchMaster = jobOrder.getBranchMaster(); 
					if(branchMaster!=null){
						
						map.addAttribute("branchMaster", branchMaster);

						if(branchMaster.getLogoPath()!=null ){							
							source = Utility.getValueOfPropByKey("branchRootPath")+branchMaster.getBranchId()+"/"+branchMaster.getLogoPath();
							target = servletContext.getRealPath("/")+"/"+"/branch/"+branchMaster.getBranchId()+"/";

							File targetDir = new File(target);
							if(!targetDir.exists())
								targetDir.mkdirs();
							File sourceFile = new File(source);					        
							File targetFile = new File(targetDir+"/"+sourceFile.getName());

							if(sourceFile.exists())
							{
								FileUtils.copyFile(sourceFile, targetFile);
								path = Utility.getValueOfPropByKey("contextBasePath")+"/branch/"+branchMaster.getBranchId()+"/"+sourceFile.getName();	
								isLogoExist =true;
							}
							map.addAttribute("logoPath", path);
						}
					 
					else{		}
						
					}
					if(!isLogoExist){
					
					map.addAttribute("headQuarterMaster", headQuarterMaster);

						if(headQuarterMaster!=null && headQuarterMaster.getLogoPath()!=null){							
							source = Utility.getValueOfPropByKey("headQuarterRootPath")+headQuarterMaster.getHeadQuarterId()+"/"+headQuarterMaster.getLogoPath();
							target = servletContext.getRealPath("/")+"/"+"/headquarter/"+headQuarterMaster.getHeadQuarterId()+"/";

							File targetDir = new File(target);
							if(!targetDir.exists())
								targetDir.mkdirs();
							File sourceFile = new File(source);					        
							File targetFile = new File(targetDir+"/"+sourceFile.getName());

							if(sourceFile.exists())
							{
								FileUtils.copyFile(sourceFile, targetFile);
								path = Utility.getValueOfPropByKey("contextBasePath")+"/headquarter/"+headQuarterMaster.getHeadQuarterId()+"/"+sourceFile.getName();						        
							   isLogoExist=true;
							}
							map.addAttribute("logoPath", path);
						}
					 
					else{		}
					}


				} 
				catch (Exception e) {
				}
				
				if(!isLogoExist){
					path = "images/applyfor-job.png";
					map.addAttribute("logoPath", path);
				}
				
			}
		//// Anurag for display logo for kelly------ end------	
			
			else{
				
			if(jobOrder.getCreatedForEntity().equals(3)){
				try {
					SchoolMaster schoolMaster = jobOrder.getSchool().get(0);
					map.addAttribute("schoolMaster", schoolMaster);


					if(schoolMaster!=null){
						if(schoolMaster!=null){							
							source = Utility.getValueOfPropByKey("schoolRootPath")+schoolMaster.getSchoolId()+"/"+schoolMaster.getLogoPath();
							target = servletContext.getRealPath("/")+"/"+"/school/"+schoolMaster.getSchoolId()+"/";

							File targetDir = new File(target);
							if(!targetDir.exists())
								targetDir.mkdirs();
							File sourceFile = new File(source);					        
							File targetFile = new File(targetDir+"/"+sourceFile.getName());

							if(sourceFile.exists())
							{
								FileUtils.copyFile(sourceFile, targetFile);
								path = Utility.getValueOfPropByKey("contextBasePath")+"/school/"+schoolMaster.getSchoolId()+"/"+sourceFile.getName();						        
							}
							map.addAttribute("logoPath", path);
						}
					}
					else{

					}


				} 
				catch (Exception e) {
				}
			}
			else{
				try{
					map.addAttribute("districtMaster", districtMaster);

					if(districtMaster!=null){


						source = Utility.getValueOfPropByKey("districtRootPath")+districtMaster.getDistrictId()+"/"+districtMaster.getLogoPath();
						target = servletContext.getRealPath("/")+"/"+"/district/"+districtMaster.getDistrictId()+"/";

						File sourceFile = new File(source);
						File targetDir = new File(target);
						if(!targetDir.exists())
							targetDir.mkdirs();

						File targetFile = new File(targetDir+"/"+sourceFile.getName());

						if(sourceFile.exists())
						{
							FileUtils.copyFile(sourceFile, targetFile);
							path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+districtMaster.getDistrictId()+"/"+sourceFile.getName();
						}
						map.addAttribute("logoPath", path);					
					}
					else{
						//response.sendRedirect("signin.do");
					}	
				} 
				catch (Exception e) {
				}
			}
			}

			if(!Utility.existsURL(path)){
				path = "images/applyfor-job.png";
				map.addAttribute("logoPath", path);
			}
			
			// Start ... dynamic portfolio
			/*Criterion criterion = Restrictions.eq("status", "A");
			List<FieldMaster> lstFieldMaster = fieldMasterDAO.findByCriteria(Order.asc("fieldName"),criterion);
			
			Criterion criterionEmpRTstatus = Restrictions.eq("status", "A");
			List<EmpRoleTypeMaster> lstEmpRoleTypeMaster = empRoleTypeMasterDAO.findByCriteria(criterionEmpRTstatus);*/
			
			
			List<SubjectAreaExamMaster> subjectAreaExamMasters =new ArrayList<SubjectAreaExamMaster>();
			if(jobOrder.getDistrictMaster()!=null){
				subjectAreaExamMasters = subjectAreaExamMasterDAO.findActiveSubjectExamByDistrict(jobOrder.getDistrictMaster());
			}
			map.addAttribute("subjectList",subjectAreaExamMasters);
			map.addAttribute("isMiami", isMiami);
			map.addAttribute("districtIdForDSPQ", districtMaster);
			map.addAttribute("teacherIdForDSPQ", teacherDetail);
			
			//District Address--------------------
			
			String districtAddress="";
			String address="";
			String state="";
			String city = "";
			String zipcode="";
			
			if(jobOrder.getDistrictMaster()!=null){
				
				//Get district address
				if(jobOrder.getDistrictMaster()!=null)
				{
					if(jobOrder.getDistrictMaster().getAddress()!=null && !jobOrder.getDistrictMaster().getAddress().equals(""))
					{
						if(!jobOrder.getDistrictMaster().getCityName().equals(""))
						{
							districtAddress = jobOrder.getDistrictMaster().getAddress()+", ";
						}
						else
						{
							districtAddress = jobOrder.getDistrictMaster().getAddress();
						}
					}
					if(!jobOrder.getDistrictMaster().getCityName().equals(""))
					{
						if(jobOrder.getDistrictMaster().getStateId()!=null && !jobOrder.getDistrictMaster().getStateId().getStateName().equals(""))
						{
							city = jobOrder.getDistrictMaster().getCityName()+", ";
						}else{
							city = jobOrder.getDistrictMaster().getCityName();
						}
					}
					if(jobOrder.getDistrictMaster().getStateId()!=null && !jobOrder.getDistrictMaster().getStateId().getStateName().equals(""))
					{
						if(!jobOrder.getDistrictMaster().getZipCode().equals(""))
						{
							state = jobOrder.getDistrictMaster().getStateId().getStateName()+", ";
						}
						else
						{
							state = jobOrder.getDistrictMaster().getStateId().getStateName();
						}	
					}
					if(!jobOrder.getDistrictMaster().getZipCode().equals(""))
					{
							zipcode = jobOrder.getDistrictMaster().getZipCode();
					}
					
					if(districtAddress !="" || city!="" ||state!="" || zipcode!=""){
						address = districtAddress+city+state+zipcode;
					}else{
						address="";
					}
				}
			}
			
			
			
			map.addAttribute("districtAddress", address);
			map.addAttribute("districtDName", districtDName);
			map.addAttribute("lstLastYear", Utility.getLasterYearByYear(1955));
			
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			map.addAttribute("teacherExperience", teacherExperience);
			List<String> customQuestionId=new ArrayList<String>();
			if(teacherExperience!=null && teacherExperience.getCustomquestion()!=null){
				String customQuestionId2[]=teacherExperience.getCustomquestion().split(",");
				for (String customQuestionId1 : customQuestionId2) {
					customQuestionId.add(customQuestionId1);
				}
			}
			/*map.addAttribute("lstMonth", Utility.getMonthList());
			map.addAttribute("lstYear", Utility.getLasterYearByYear(1955));
			map.addAttribute("lstFieldMaster", lstFieldMaster);
			map.addAttribute("lstEmpRoleTypeMaster", lstEmpRoleTypeMaster);*/
			
			// For Work Experience
			map.addAttribute("customQuestionId", customQuestionId);
			Criterion criterion = Restrictions.eq("status", "A");
			List<FieldMaster> lstFieldMaster = fieldMasterDAO.findByCriteria(Order.asc("fieldName"),criterion);
			
			Criterion criterionEmpRTstatus = Restrictions.eq("status", "A");
			List<EmpRoleTypeMaster> lstEmpRoleTypeMaster = empRoleTypeMasterDAO.findByCriteria(Order.desc("empRoleTypeId"),criterionEmpRTstatus);
			
			//Criterion criterionGenderActive = Restrictions.eq("status", "A");
			List<GenderMaster> lstGenderMasters  = genderMasterDAO.findAllGenderByOrder();
			
			List<PeopleRangeMaster> lstPeopleRangeMasters = peopleRangeMasterDAO.findAll();
			
			Criterion criterionOrg = Restrictions.eq("status", "A");
			List<OrgTypeMaster> lstOrgTypeMasters = orgTypeMasterDAO.findByCriteria(Order.asc("orgType"),criterionOrg);
			
			List<CurrencyMaster> lstCurrencyMasters = currencyMasterDAO.findAllCurrencyByOrder();
			
			//map.addAttribute("teacherExperience", teacherExperience);
			map.addAttribute("lstMonth", Utility.getMonthList());
			map.addAttribute("lstYear", Utility.getLasterYearByYear(1955));
			map.addAttribute("lstFieldMaster", lstFieldMaster);
			map.addAttribute("lstEmpRoleTypeMaster", lstEmpRoleTypeMaster);
			map.addAttribute("lstGenderMasters", lstGenderMasters);
			
			map.addAttribute("lstPeopleRangeMasters", lstPeopleRangeMasters);
			map.addAttribute("lstOrgTypeMasters", lstOrgTypeMasters);
			map.addAttribute("lstCurrencyMasters", lstCurrencyMasters);
			
			
			List<CountryMaster> countryMasters=new ArrayList<CountryMaster>();
			countryMasters=countryMasterDAO.findAllActiveCountry();
			map.addAttribute("listCountryMaster", countryMasters);
			
			List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
			List<CityMaster> listCityMasters = new ArrayList<CityMaster>();
			
			TeacherPersonalInfo teacherpersonalinfo = null;
			TeacherPortfolioStatus teacherPortfolioStatus = null;
			TeacherDetail tDetail = null;
			tDetail =(TeacherDetail) session.getAttribute("teacherDetail");
			teacherpersonalinfo = teacherPersonalInfoDAO.findById(tDetail.getTeacherId(), false, false);
			teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			
			if(teacherpersonalinfo!=null)
			{
				listCityMasters = cityMasterDAO.findCityByState(teacherpersonalinfo.getStateId());
				map.addAttribute("teacherpersonalinfo", teacherpersonalinfo);
			}else{
			}
			
			if(teacherPortfolioStatus!=null){
				map.addAttribute("teacherPortfolioStatus", teacherPortfolioStatus);
			}
			
			map.addAttribute("listStateMaster", listStateMaster);
			map.addAttribute("listCityMasters", listCityMasters);
			
			List<CertificationStatusMaster> lstCertificationStatusMaster=certificationStatusMasterDAO.findAllCertificationStatusMasterByOrder();
			map.addAttribute("lstCertificationStatusMaster", lstCertificationStatusMaster);
			
			List<TFAAffiliateMaster> lstTFAAffiliateMaster	=	tfaAffiliateMasterDAO.findByCriteria(Order.asc("tfaAffiliateName"));
			List<TFARegionMaster> lstTFARegionMaster		=	tfaRegionMasterDAO.findByCriteria(Order.asc("tfaRegionName"));
			map.addAttribute("lstTFAAffiliateMaster", lstTFAAffiliateMaster);
			map.addAttribute("lstTFARegionMaster", lstTFARegionMaster);
			map.addAttribute("lstCorpsYear", Utility.getLasterYeartillAdvanceYear(1990));
			
			List<RaceMaster> listRaceMasters = null;
			listRaceMasters = raceMasterDAO.findAllRaceByOrder();
			map.addAttribute("listRaceMasters", listRaceMasters);
			
			List<EthnicOriginMaster> lstethnicOriginMasters = null;
			List<EthinicityMaster> lstEthinicityMasters = null;
			
			lstEthinicityMasters=ethinicityMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
			lstethnicOriginMasters=ethnicOriginMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
			
			System.out.println(" lstEthinicityMasters "+lstEthinicityMasters.size()+" lstethnicOriginMasters "+lstethnicOriginMasters.size());
			
			map.addAttribute("lstEthinicityMasters", lstEthinicityMasters);
			map.addAttribute("lstethnicOriginMasters", lstethnicOriginMasters);
			
			Criterion certIficatCri = Restrictions.eq("status", "A");
			List<CertificationTypeMaster>listCertificationTypeMasters=certificationTypeMasterDAO.findByCriteria(certIficatCri);
			map.addAttribute("listCertTypeMasters", listCertificationTypeMasters);
			map.addAttribute("lstComingYear", Utility.getComingYearsByYear());
			map.addAttribute("lstDays", Utility.getDays());
			// End ... dynamic portfolio
			
			StringBuffer jobCertifications = new StringBuffer();
			List<JobCertification> jobCertificationList = jobCertificationDAO.findCertificationByJobOrder(jobOrder);
			if(jobCertificationList.size()>0)
			{
				for (JobCertification jobCertification : jobCertificationList) {
					if(jobCertification.getCertificateTypeMaster()!=null)
					jobCertifications.append(jobCertification.getCertificateTypeMaster().getCertType()+"</br>");
				}
			}
			
			map.addAttribute("jobCertifications", jobCertifications.toString());
			map.addAttribute("internalFlag",internalFlag);
			
			TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam = teacherGeneralKnowledgeExamDAO.findTeacherGeneralKnowledgeExam(teacherDetail);
			TeacherSubjectAreaExam teacherSubjectAreaExam = teacherSubjectAreaExamDAO.findTeacherSubjectAreaExam(teacherDetail);
			
			map.addAttribute("teacherGeneralKnowledgeExam",teacherGeneralKnowledgeExam);
			map.addAttribute("teacherSubjectAreaExam",teacherSubjectAreaExam);
			map.addAttribute("isAlsoSA",session.getAttribute("isAlsoSA"));
			System.out.println("session.getAttribute    "+session.getAttribute("isAlsoSA"));
			
			int isFrmSubmitForJSI=0;
			if(jobForTeacher==null)
			{
				isFrmSubmitForJSI=1;
			}
			else
			{
				if(jobOrder.getHeadQuarterMaster()!=null)
				{
					isFrmSubmitForJSI=0;
				}
			}
			
			map.addAttribute("isFrmSubmitForJSI",isFrmSubmitForJSI);
			
			boolean spKellyjobFlag = false;
			
			// check for only smart practices & no workflow attached (for continue button)
			if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getJobCategoryMaster().getJobInviteOnly()!=null 
			&& jobOrder.getJobCategoryMaster().getJobInviteOnly() && jobOrder.getJobCategoryMaster().getOfferDSPQ()!=null 
			&& !jobOrder.getJobCategoryMaster().getOfferDSPQ() && !jobOrder.getJobCategoryMaster().getBaseStatus() 
			&& (jobOrder.getJobCategoryMaster().getOfferQualificationItems()==null || !jobOrder.getJobCategoryMaster().getOfferQualificationItems()) 
			&& (jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null && (jobOrder.getJobCategoryMaster().getPreHireSmartPractices() || !jobOrder.getJobCategoryMaster().getPreHireSmartPractices()))){
				if(jobForTeacher.getStatus().getStatusId()==4){
					spKellyjobFlag = true;
				}
			}
			map.addAttribute("spKellyJob", spKellyjobFlag);
			if(jobOrder!=null && jobOrder.getGradeLevel()!=null)
				map.addAttribute("schoolType", jobOrder.getGradeLevel());
			else
				map.addAttribute("schoolType", null);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "applyjob";
	}

	@RequestMapping(value="/applynow.do", method=RequestMethod.POST)
	public String doApplyNow(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		HttpSession session = request.getSession(false);
		System.out.println("::::::::::::::Gaurav Kumar:::::::::::::/applynow.do::::::::::::::::");
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		try{	
			
			String redirectURL = null;
			String jobid = request.getParameter("jobId")==null?"0":request.getParameter("jobId");
			String coverLetter = request.getParameter("coverLetter")==null?"0":request.getParameter("coverLetter");
			String isAffilated = request.getParameter("isAffilated")==null?"0":request.getParameter("isAffilated");
			String staffType = request.getParameter("staffType")==null?"N":request.getParameter("staffType");
			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobid), false, false);
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
			System.out.println("coverLetter   "+coverLetter);
			/////////////////////////Check before save/////////////////////////////////
			JobForTeacher jft= jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail,jobOrder);

			if(jft!=null){
				if(coverLetter.length()>0)
					jft.setCoverLetter(coverLetter);
					jft.setStaffType(staffType);
					jobForTeacherDAO.updatePersistent(jft);
				//return null;
			}

			//////////////////////////////////////////////////////////
			try{
				DistrictMaster districtMaster = jobOrder.getDistrictMaster();
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
				if(itcList.size()>0){
					isAffilated="1";
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			JobForTeacher jobForTeacher = new JobForTeacher();
			
			if(jft!=null)
				jobForTeacher = jft;
			
			jobForTeacher.setTeacherId(teacherDetail);
			jobForTeacher.setJobId(jobOrder);
			if(jobOrder.getDistrictMaster()!=null)
				jobForTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
			jobForTeacher.setCoverLetter(coverLetter);
			jobForTeacher.setCreatedDateTime(new Date());
			StatusMaster statusMaster = null;
			if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(614730) && staffType.equalsIgnoreCase("PI")){
				jobForTeacher.setIsAffilated(0);
			}
			else if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2)
			{
				String teacherStatus="";
				if(session.getAttribute("Currnrt")!=null)
					teacherStatus=session.getAttribute("Currnrt").toString();
				
				System.out.println("teacherStatus=======>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+teacherStatus);
				if(teacherStatus!=null && teacherStatus!="" && teacherStatus.equalsIgnoreCase("c"))
				{
					jobForTeacher.setIsAffilated(1);
					session.setAttribute("Currnrt","");
				}
				else if(teacherStatus!=null && teacherStatus!="" && teacherStatus.equalsIgnoreCase("x"))
				{
					jobForTeacher.setIsAffilated(0);
					session.setAttribute("Currnrt","");
				}
				else
				{
					jobForTeacher.setIsAffilated(0);
				}
			}			
			else{
				jobForTeacher.setIsAffilated(Integer.parseInt(isAffilated));
			}
			jobForTeacher.setStaffType(staffType);
			
			 int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher);
			 statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
						 
			 boolean completeFlag=false;
			 try{
					if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
						completeFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			 
			jobForTeacher.setStatus(statusMaster);
			
			if(session.getAttribute("jobValid"+jobOrder.getJobId())!=null)
			{
				if((Boolean)session.getAttribute("jobValid"+jobOrder.getJobId())==true)
					jobForTeacher.setFlagForDistrictSpecificQuestions(true);
				else
					jobForTeacher.setFlagForDistrictSpecificQuestions(false);
				
				session.removeAttribute("jobValid"+jobOrder.getJobId());
			}
			
			if(session.getAttribute("dspqComplete"+jobOrder.getJobId())!=null)
			{
				if((Boolean)session.getAttribute("dspqComplete"+jobOrder.getJobId())==true)
					jobForTeacher.setFlagForDspq(true);
				else
					jobForTeacher.setFlagForDspq(false);
				
				session.removeAttribute("dspqComplete"+jobOrder.getJobId());
			}
			
			jobForTeacher.setStatusMaster(statusMaster);
			//jobForTeacher.setIsAffilated(Integer.parseInt(isAffilated));
			
			PrintOnConsole.debugPrintln("Try to set setIsAffilated in JFT from doApplyNow [UserDashboardController]");
			if (session != null && session.getAttribute("isCurrentEmploymentNeeded") != null) 
			{
				boolean isCurrentEmploymentNeeded=false;
				isCurrentEmploymentNeeded=(Boolean)session.getAttribute("isCurrentEmploymentNeeded");
				PrintOnConsole.debugPrintln("saveJobForTeacher isCurrentEmploymentNeeded "+isCurrentEmploymentNeeded);
				if(isCurrentEmploymentNeeded)
				{
					TeacherPersonalInfo teacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType()!=null && teacherPersonalInfo.getEmployeeType()==1)
						jobForTeacher.setIsAffilated(1);
					else{
						jobForTeacher.setIsAffilated(Integer.parseInt(isAffilated));
					}
				}
				session.removeAttribute("isCurrentEmploymentNeeded");
			}
			try{
				commonService.autoRejectCandidate(jobOrder,teacherDetail,jobForTeacher);
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 if(statusMaster!=null){
					jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
			 }
			jobForTeacherDAO.makePersistent(jobForTeacher);
			 try{
				 if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
					commonService.futureJobStatus(jobOrder, teacherDetail,jobForTeacher);
					
				 }
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			/* ======== Gagan : [ Start ] Functinality for to Send Email to all active DA\SA Admins acording to Job order, who has checked job apply CHECKBOX in notification [ Available ] checkbox ============== */
			List<UserMaster> lstusermaster	=	new ArrayList<UserMaster>();
			
			try{
				/* ===== Gagan : statusJobApply is used to get status short name against Apply which is used in Email sending to all active DA\SA Active Admins who has checked thier job apply CHECKBOX in notification [ Available ]======= */
				StatusMaster statusJobApply = WorkThreadServlet.statusMap.get("apl");
				String statusShortName = statusJobApply.getStatusShortName();
				boolean isSchool=false;
				if(jobOrder.getSelectedSchoolsInDistrict()!=null){
					if(jobOrder.getSelectedSchoolsInDistrict()==1){
						isSchool=true;
					}
				}
				if(jobOrder.getCreatedForEntity()==2 && isSchool==false){
					lstusermaster=userEmailNotificationsDAO.getUserByDistrict(jobOrder.getDistrictMaster(),statusShortName);
				}else if(jobOrder.getCreatedForEntity()==2 && isSchool){
					List<SchoolMaster>  lstschoolMaster= new ArrayList<SchoolMaster>();
					if(jobOrder.getSchool()!=null){
						if(jobOrder.getSchool().size()!=0){
							for(SchoolMaster sMaster : jobOrder.getSchool()){
								lstschoolMaster.add(sMaster);
							}
						}
					}
					lstusermaster=userEmailNotificationsDAO.getUserByDistrictAndSchoolList(jobOrder.getDistrictMaster(),lstschoolMaster,jobOrder,statusShortName);
				//}else if(jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==3 && isSchool){
					//lstusermaster=userEmailNotificationsDAO.getUserByDistrictListOnlySchool(districtMaster,userMaster.getSchoolId(),statusShortName);
				}else if(jobOrder.getCreatedForEntity()==3){
					lstusermaster=userEmailNotificationsDAO.getUserByOnlySchool(jobOrder.getSchool().get(0),statusShortName);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			/*========= HC Means Hard Code ======= Gagan [Start] : for Adding a hardcoded Email Id to TM Admin ===============*/
			/*List<UserMaster> lstuserHC	= new ArrayList<UserMaster>();
			UserMaster userHC = new UserMaster();
			userHC.setFirstName("Sanjeev");
			userHC.setLastName("Arora");
			userHC.setEmailAddress("sanjeev.arora@thriventus.com");
			
			lstuserHC.add(userHC);
			//lstusermaster.addAll(lstuserHC);
			
			//System.out.println(" Size of Hard coded List of User : "+lstuserHC.size()+"\n Name : "+lstuserHC.get(0).getFirstName()+" "+lstuserHC.get(0).getLastName()+"\n  Email Address "+lstuserHC.get(0).getEmailAddress()+" \n After Adding to final User ListSize   : "+lstusermaster.size());
			*/
			
			/*================ Gagan [END] : for Adding a hardcoded Email Id to TM Admin ===============*/
			
			
			/* ======== Gagan : [ END ] Functinality for to Send Email to all active DA\SA Admins acording to Job order, who has checked job apply CHECKBOX in notification [ Available ] checkbox ============== */
			
			/*======= Gagan[ Start ] : Check for  Base status for that job which against EPI required ==================*/
			
			TeacherAssessmentStatus _teacherAssessmentStatus =null;
			StatusMaster _statusMaster =	null;
			
			if(jobForTeacher.getJobId().getJobCategoryMaster().getBaseStatus())
			{
				List<TeacherAssessmentStatus> _teacherAssessmentStatusList = null;
				JobOrder _jOrder = new JobOrder();
				_jOrder.setJobId(0);
				_teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(),_jOrder);
				if(_teacherAssessmentStatusList.size()>0){
					_teacherAssessmentStatus = _teacherAssessmentStatusList.get(0);
					_statusMaster = _teacherAssessmentStatus.getStatusMaster();
				}else{
					_statusMaster = WorkThreadServlet.statusMap.get("icomp");
				}
			}
			
			/*======= Gagan[ END ] : Check for  Base status for that job which against EPI required ==================*/
			String[] arrHrDetail = commonService.getHrDetailToTeacher(request,jobForTeacher.getJobId());
			try{
				arrHrDetail[10]	=	Utility.getBaseURL(request)+"signin.do";
				arrHrDetail[11] =	Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jobForTeacher.getJobId().getJobId()+"&JobOrderType="+jobForTeacher.getJobId().getCreatedForEntity();
				if(jobForTeacher.getJobId().getDistrictMaster()!=null)
					arrHrDetail[12] =	Utility.getShortURL(Utility.getBaseURL(request)+"jobsboard.do?districtId="+Utility.encryptNo(jobForTeacher.getJobId().getDistrictMaster().getDistrictId()));
				else
					arrHrDetail[12] = "";
				arrHrDetail[13] =	Utility.getValueOfPropByKey("basePath")+"/forgotpassword.do";
				try {
					if(_statusMaster.getStatusShortName()!=null)
						arrHrDetail[14] =	_statusMaster.getStatusShortName();
				} catch (NullPointerException e) {
					//e.printStackTrace();
				}
				String flagforEmail	="applyJob";
				//headquarter check
				//System.out.println(jobForTeacher.getStatus().getStatusId()+"-------------------------"+jobForTeacher.getSendMail()+"--------------"+jobForTeacher.getJobId().getDistrictMaster().getJobCompletionEmail());
				if(jobForTeacher!=null && jobForTeacher.getJobId().getDistrictMaster()!=null 
						&& jobForTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster()!=null 
						&& jobForTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()!=null 
						&& jobForTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId().equals(2))
				{
					if(jobForTeacher.getJobId().getDistrictMaster().getJobCompletionEmail() 
							&& jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusId()!=null
							&& jobForTeacher.getStatus().getStatusId()==4 && (jobForTeacher.getSendMail()==null || !jobForTeacher.getSendMail()))
					{
						System.out.println("---------------------userDashboard----------------------");
						jobForTeacher.setSendMail(true);
						MailSendToTeacher mst =new MailSendToTeacher(teacherDetail,jobOrder,request,arrHrDetail,flagforEmail,lstusermaster,emailerService,jobForTeacher);
						Thread currentThread = new Thread(mst);
						currentThread.start();
						jobForTeacherDAO.makePersistent(jobForTeacher);
					}
				}
				
				else
				{
					MailSendToTeacher mst =new MailSendToTeacher(teacherDetail,jobOrder,request,arrHrDetail,flagforEmail,lstusermaster,emailerService,jobForTeacher);
					Thread currentThread = new Thread(mst);
					currentThread.start();
				}
					
				 
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(statusMaster.getStatusShortName().equalsIgnoreCase("comp") || statusMaster.getStatusShortName().equalsIgnoreCase("vlt"))
			{
				StringBuffer sb = new StringBuffer();
				boolean onDashboard = false;
				String exitMessage = null;
				String exitURL = null;
				int flagForURL = 0;
				if(jobOrder.getFlagForURL()!=null)
				 flagForURL = jobOrder.getFlagForURL();
				int flagForMessage=0;
				if(jobOrder.getFlagForMessage()!=null)
					flagForMessage  = jobOrder.getFlagForMessage();
				exitMessage = jobOrder.getExitMessage();
				exitURL = jobOrder.getExitURL();

				PrintWriter out = response.getWriter();				
				response.setContentType("text/html"); 

				sb.append("<script type=\"text/javascript\" language=\"javascript\">");
				if(exitMessage!=null && !exitMessage.trim().equals("") && exitURL!=null && !exitURL.trim().equals(""))
				{
					onDashboard = true;
				}else if(flagForMessage==1)
				{
					onDashboard = false;
					sb.append("window.location.href='thankyoumessage.do?jobId="+jobOrder.getJobId()+"'");

				}else if(flagForURL==1)
				{
					//sb.append("alert('Thank you for completing the TeacherMatch part of the job application. You will be directed to "+exitURL+" in a new browser window, however please note that TeacherMatch session is also available to you in current browser window.');");
					sb.append("window.location.href='userdashboard.do?jobId="+jobOrder.getJobId()+"';");
				}else
					onDashboard = true;


				if(onDashboard){
					sb.append("alert('Thank you for completing the TeacherMatch part of the job application.');");
					sb.append("window.location.href='jobsofinterest.do'");
				}
				sb.append("</script>");
				out.print(sb.toString());
			}
			else
				return "redirect:applyjob.do?jobId="+jobOrder.getJobId();			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		//return "redirect:userdashboard.do";
		return null;
	}

	@RequestMapping(value="/jobspecificinventory.do", method=RequestMethod.GET)
	public String doJobSpecificInventoryGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		try 
		{
			//List<JobForTeacher> lstJobForTeacher = getJobSpecInventory(request);
			//map.addAttribute("lstJobForTeacher", lstJobForTeacher);
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
			/*List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
			List<CityMaster> listCityMasters = new ArrayList<CityMaster>();
			
			TeacherPersonalInfo teacherpersonalinfo = null;
			TeacherPortfolioStatus teacherPortfolioStatus = null;
			TeacherDetail tDetail = null;
			tDetail =(TeacherDetail) session.getAttribute("teacherDetail");
			teacherpersonalinfo = teacherPersonalInfoDAO.findById(tDetail.getTeacherId(), false, false);
			teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			
			if(teacherpersonalinfo!=null)
			{
				listCityMasters = cityMasterDAO.findCityByState(teacherpersonalinfo.getStateId());
				map.addAttribute("teacherpersonalinfo", teacherpersonalinfo);
			}else{
			}
			
			if(teacherPortfolioStatus!=null){
				map.addAttribute("teacherPortfolioStatus", teacherPortfolioStatus);
			}
			
			map.addAttribute("listStateMaster", listStateMaster);
			map.addAttribute("listCityMasters", listCityMasters);
			
			List<CertificationStatusMaster> lstCertificationStatusMaster=certificationStatusMasterDAO.findAllCertificationStatusMasterByOrder();
			map.addAttribute("lstCertificationStatusMaster", lstCertificationStatusMaster);
			
			List<TFAAffiliateMaster> lstTFAAffiliateMaster	=	tfaAffiliateMasterDAO.findByCriteria(Order.asc("tfaAffiliateName"));
			List<TFARegionMaster> lstTFARegionMaster		=	tfaRegionMasterDAO.findByCriteria(Order.asc("tfaRegionName"));
			map.addAttribute("lstTFAAffiliateMaster", lstTFAAffiliateMaster);
			map.addAttribute("lstTFARegionMaster", lstTFARegionMaster);
			map.addAttribute("lstCorpsYear", Utility.getLasterYeartillAdvanceYear(1990));
			
			List<RaceMaster> listRaceMasters = null;
			listRaceMasters = raceMasterDAO.findAllRaceByOrder();
			map.addAttribute("listRaceMasters", listRaceMasters);
			
			
			// For Work Experience
			Criterion criterion = Restrictions.eq("status", "A");
			List<FieldMaster> lstFieldMaster = fieldMasterDAO.findByCriteria(Order.asc("fieldName"),criterion);

			Criterion criterionEmpRTstatus = Restrictions.eq("status", "A");
			List<EmpRoleTypeMaster> lstEmpRoleTypeMaster = empRoleTypeMasterDAO.findByCriteria(Order.desc("empRoleTypeId"),criterionEmpRTstatus);

			//Criterion criterionGenderActive = Restrictions.eq("status", "A");
			List<GenderMaster> lstGenderMasters  = genderMasterDAO.findAllGenderByOrder();
			
			List<PeopleRangeMaster> lstPeopleRangeMasters = peopleRangeMasterDAO.findAll();

			Criterion criterionOrg = Restrictions.eq("status", "A");
			List<OrgTypeMaster> lstOrgTypeMasters = orgTypeMasterDAO.findByCriteria(Order.asc("orgType"),criterionOrg);

			List<CurrencyMaster> lstCurrencyMasters = currencyMasterDAO.findAllCurrencyByOrder();

			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			
			map.addAttribute("teacherExperience", teacherExperience);
			map.addAttribute("lstMonth", Utility.getMonthList());
			map.addAttribute("lstYear", Utility.getLasterYearByYear(1955));
			map.addAttribute("lstFieldMaster", lstFieldMaster);
			map.addAttribute("lstEmpRoleTypeMaster", lstEmpRoleTypeMaster);
			map.addAttribute("lstGenderMasters", lstGenderMasters);
			
			List<CountryMaster> countryMasters=new ArrayList<CountryMaster>();
			countryMasters=countryMasterDAO.findAllActiveCountry();
			map.addAttribute("listCountryMaster", countryMasters);
			
			
			map.addAttribute("lstPeopleRangeMasters", lstPeopleRangeMasters);
			map.addAttribute("lstOrgTypeMasters", lstOrgTypeMasters);
			map.addAttribute("lstCurrencyMasters", lstCurrencyMasters);
			map.addAttribute("lstDays", Utility.getDays());
			System.out.println(" lstFieldMaster :: "+lstFieldMaster.size());
*/
			FavTeacher favTeacher = favTeacherDAO.findFavTeacherByTeacher(teacherDetail);
			map.addAttribute("favTeacher", favTeacherDAO.findRandomFavTeacher(favTeacher));

			String jobId = request.getParameter("jobId");
			String mf = request.getParameter("mf");
			String isAffilated =null;
			if(request.getParameter("ok_cancelflag")!=null){
				isAffilated=request.getParameter("ok_cancelflag");
			}
			JobOrder jobOrder  = null;
 
			if(jobId!=null && jobId.trim().length()>0)
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);

			map.addAttribute("jobOrder", jobOrder); 
			map.addAttribute("mf", mf);
			
			// Start ... dynamic portfolio for External Job Apply
			
			map.addAttribute("lstLastYear", Utility.getLasterYearByYear(1955));
			
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			map.addAttribute("teacherExperience", teacherExperience);
			
			//Criterion criterionGenderActive = Restrictions.eq("status", "A");
			List<GenderMaster> lstGenderMasters  = genderMasterDAO.findAllGenderByOrder();
			
			List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			map.addAttribute("listStateMaster", listStateMaster);
			
			List<CertificationStatusMaster> lstCertificationStatusMaster=certificationStatusMasterDAO.findAllCertificationStatusMasterByOrder();
			map.addAttribute("lstCertificationStatusMaster", lstCertificationStatusMaster);
			
			List<TFAAffiliateMaster> lstTFAAffiliateMaster	=	tfaAffiliateMasterDAO.findByCriteria(Order.asc("tfaAffiliateName"));
			List<TFARegionMaster> lstTFARegionMaster		=	tfaRegionMasterDAO.findByCriteria(Order.asc("tfaRegionName"));
			map.addAttribute("lstTFAAffiliateMaster", lstTFAAffiliateMaster);
			map.addAttribute("lstTFARegionMaster", lstTFARegionMaster);
			map.addAttribute("lstCorpsYear", Utility.getLasterYeartillAdvanceYear(1990));
			
			Criterion certIficatCri = Restrictions.eq("status", "A");
			List<CertificationTypeMaster>listCertificationTypeMasters=certificationTypeMasterDAO.findByCriteria(certIficatCri);
			map.addAttribute("listCertTypeMasters", listCertificationTypeMasters);
			map.addAttribute("lstComingYear", Utility.getComingYearsByYear());
			map.addAttribute("lstDays", Utility.getDays());
			
			String districtDName=null;
			if(jobOrder!=null)
			{
				if(jobOrder.getDistrictMaster()!=null){
					DistrictMaster districtMaster=null;
					districtMaster=jobOrder.getDistrictMaster();
					if(districtMaster.getDisplayName() !=null && !districtMaster.getDisplayName().equals("")){
						districtDName=districtMaster.getDisplayName();
					}else{
						districtDName=districtMaster.getDistrictName();
					}
				}
				else if(jobOrder.getHeadQuarterMaster()!=null){
					districtDName = jobOrder.getHeadQuarterMaster().getHeadQuarterName();
				}
				map.addAttribute("districtDName", districtDName);
			}
			// End ... dynamic portfolio for External Job Apply
			boolean dspqFlag =true;
			boolean getQQ =true;
			boolean isMiami = false;
			boolean internalFlag=false;
			DistrictMaster districtMaster=null;
			try{
				if(jobOrder!=null){
					
					if(jobOrder.getDistrictMaster()!=null){
						if(jobOrder.getDistrictMaster().getDistrictId().equals(1200390))
							isMiami=true;
					}
					
					
					if(jobOrder.getDistrictMaster()!=null){
						districtMaster=jobOrder.getDistrictMaster();
					}
					List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
					
					if(itcList.size()>0){
						internalFlag=true;
					}
					if(jobOrder.getDistrictMaster()!=null && internalFlag){
						if(districtMaster.getOfferDistrictSpecificItems()){
							dspqFlag=true;
						}else{
							dspqFlag=false;
						}
						if(districtMaster.getOfferQualificationItems()){
							getQQ=true;
						}else{
							getQQ=false;
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			try{
				if(isAffilated==null){
					isAffilated =(String)(session.getAttribute("isAffilated")==null?"0":session.getAttribute("isAffilated"));
				}
				System.out.println("isAffilated:::::>>>:::::::"+isAffilated);
				if(jobOrder!=null && isAffilated.equalsIgnoreCase("1") && internalFlag==false){
					if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferDistrictSpecificItems()){
						dspqFlag=true;
					}else{
						dspqFlag=false;
					}
					if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferQualificationItems()){
						getQQ=true;
					}else{
						getQQ=false;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			
			}
			System.out.println("dspqFlag::::::::"+dspqFlag);
			System.out.println("getQQ::::::::"+getQQ);
			map.addAttribute("dspqFlag", dspqFlag);
			map.addAttribute("getQQ", getQQ);
			
			map.addAttribute("isMiami", isMiami);
			if(districtMaster!=null)
			map.addAttribute("districtMasterDSPQ", districtMaster);
			map.addAttribute("teacherIdForDSPQ", teacherDetail);
			if(districtMaster!=null)
			{
				map.addAttribute("districtMaster", districtMaster);
			}
			
			List<RaceMaster> listRaceMasters = null;
			listRaceMasters = raceMasterDAO.findAllRaceByOrder();
			map.addAttribute("listRaceMasters", listRaceMasters);
			
			List<EthnicOriginMaster> lstethnicOriginMasters = null;
			List<EthinicityMaster> lstEthinicityMasters = null;

			lstEthinicityMasters=ethinicityMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
			lstethnicOriginMasters=ethnicOriginMasterDAO.findByCriteria(Restrictions.eq("status", "A"));

			System.out.println(" lstEthinicityMasters "+lstEthinicityMasters.size()+" lstethnicOriginMasters "+lstethnicOriginMasters.size());

			map.addAttribute("lstEthinicityMasters", lstEthinicityMasters);
			map.addAttribute("lstethnicOriginMasters", lstethnicOriginMasters);
			
			// For Work Experience
			Criterion criterion = Restrictions.eq("status", "A");
			List<FieldMaster> lstFieldMaster = fieldMasterDAO.findByCriteria(Order.asc("fieldName"),criterion);

			Criterion criterionEmpRTstatus = Restrictions.eq("status", "A");
			List<EmpRoleTypeMaster> lstEmpRoleTypeMaster = empRoleTypeMasterDAO.findByCriteria(criterionEmpRTstatus);

			List<PeopleRangeMaster> lstPeopleRangeMasters = peopleRangeMasterDAO.findAll();

			Criterion criterionOrg = Restrictions.eq("status", "A");
			List<OrgTypeMaster> lstOrgTypeMasters = orgTypeMasterDAO.findByCriteria(Order.asc("orgType"),criterionOrg);

			List<CurrencyMaster> lstCurrencyMasters = currencyMasterDAO.findAllCurrencyByOrder();

			//map.addAttribute("teacherExperience", teacherExperience);
			map.addAttribute("lstMonth", Utility.getMonthList());
			map.addAttribute("lstYear", Utility.getLasterYearByYear(1955));
			map.addAttribute("lstFieldMaster", lstFieldMaster);
			map.addAttribute("lstEmpRoleTypeMaster", lstEmpRoleTypeMaster);

			map.addAttribute("lstPeopleRangeMasters", lstPeopleRangeMasters);
			map.addAttribute("lstOrgTypeMasters", lstOrgTypeMasters);
			map.addAttribute("lstCurrencyMasters", lstCurrencyMasters);
			
			List<CountryMaster> countryMasters=new ArrayList<CountryMaster>();
			countryMasters=countryMasterDAO.findAllActiveCountry();
			map.addAttribute("listCountryMaster", countryMasters);
			map.addAttribute("lstGenderMasters", lstGenderMasters);
			System.out.println(" lstFieldMaster :: "+lstFieldMaster.size());
			
			
			// End ... dynamic portfolio for External Job Apply
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "jobspecificinventory";
	}


	@RequestMapping(value="/getdashboardjobsofintrest.do", method=RequestMethod.GET)
	public void getDashboardJbofIntresGrid(HttpServletRequest request, HttpServletResponse response)
	{
		try 
		{
			PrintWriter pw = response.getWriter();			
			pw.write(showDashboardJbofIntresGrid(request));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	@RequestMapping(value="/getdashboardjobsofintrestavailable.do", method=RequestMethod.GET)
	public void getDashboardJbofIntresGridAvailable(HttpServletRequest request, HttpServletResponse response)
	{
		try 
		{
			PrintWriter pw = response.getWriter();			
			pw.write(showDashboardJbofIntresGridAvailable(request));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value="/getdashboardmilestones.do", method=RequestMethod.GET)
	public void getDashboardMilestonesGrid(HttpServletRequest request, HttpServletResponse response)
	{
		HttpSession session = request.getSession();
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		TeacherPreference tPreference = null;
		TeacherPortfolioStatus tPortfolioStatus;

		TeacherAssessmentStatus teacherBaseAssessmentStatus = null;

		List<JobForTeacher> lstJBSIncompJobForTeacher = null;
		List<JobForTeacher> lstJBSCompJobForTeacher = null;
		List<JobForTeacher> lstJBSViolatedJobForTeacher = null;

		int noOfJBSIncompJobForTeacher = 0;
		int noOfJBSCompJobForTeacher = 0;
		int noOfJBSViolatedJobForTeacher = 0;

		boolean prefStatus = false;
		boolean portfolioStatus = false;
		boolean baseInvStatus = false;
		String baseStatus="";


		try 
		{
			StatusMaster statusComplete =WorkThreadServlet.statusMap.get("comp");
			//StatusMaster statusInComplete =WorkThreadServlet.statusMap.get("icomp");
			StatusMaster statusViolated =WorkThreadServlet.statusMap.get("vlt");

			tPreference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
			if(tPreference!=null)
			{
				prefStatus = true;
			}

			tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(tPortfolioStatus!=null && tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted())
			{
				portfolioStatus = true;
			}

			teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if(teacherBaseAssessmentStatus!=null && (teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") ))
			{
				baseInvStatus=true;

			}
			if(teacherBaseAssessmentStatus!=null)
				baseStatus = teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName();

			lstJBSIncompJobForTeacher = getJobSpecInventory(request);
			lstJBSCompJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail, statusComplete);
			lstJBSViolatedJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail, statusViolated);

			if(lstJBSIncompJobForTeacher!=null)
			{
				noOfJBSIncompJobForTeacher = lstJBSIncompJobForTeacher.size();
			}
			if(lstJBSCompJobForTeacher!=null)
			{
				noOfJBSCompJobForTeacher = lstJBSCompJobForTeacher.size();
			}
			if(lstJBSViolatedJobForTeacher!=null)
			{

				for (JobForTeacher jobForTeacher : lstJBSViolatedJobForTeacher) {
					if(jobForTeacher.getJobId().getIsJobAssessment())
						noOfJBSViolatedJobForTeacher++;
				}
			}

			PrintWriter pw = response.getWriter();			
			pw.write("<table width='100%' border='0' class='table table-bordered table-striped'>");
			pw.write("<thead class='bg'>");
			pw.write("<tr>");
			pw.write("<th><img class='fl' src='images/status-icon.png'> <h4> Milestones</h4></th>");
			pw.write("<th>Status</th>");
			pw.write("<th>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			pw.write("</tr>");
			pw.write("</thead>");

			//--Job Preferences row Start 
			pw.write("<tr>");
			pw.write("<td>Job Preferences</td>");
			pw.write("<td>");
			if(prefStatus)
			{
				pw.write("<strong class='text-success'>Completed</strong>");				
			}
			else
			{
				pw.write("<strong class='text-error'>Incomplete</strong>");
			}
			pw.write("</td>");
			pw.write("<td class='pagination-centered'>");
			pw.write("<a data-original-title='Update' rel='tooltip' id='iconpophover1' href='userpreference.do'>");
			pw.write("<img src='images/option08.png'>");
			pw.write("</a>");
			pw.write("</td>");
			pw.write("</tr>");
			//--Job Preferences row End

			//--Portfolio row Start
			pw.write("<tr>");
			pw.write("<td>Portfolio</td>");
			pw.write("<td>");

			String portfolioTooltip="";
			if(portfolioStatus)
			{
				portfolioTooltip="Update";
				pw.write("<strong class='text-success'>Completed</strong>");
			}
			else
			{
				portfolioTooltip="Complete Now";
				pw.write("<strong class='text-error'>Incomplete</strong>");
			}
			pw.write("</td>");
			pw.write("<td class='pagination-centered'>");
			pw.write("<a data-original-title='"+portfolioTooltip+"' rel='tooltip' id='iconpophover2' href='portfolio.do'>");
			pw.write("<img src='images/option02.png'>");
			pw.write("</a>");
			pw.write("</td>");
			pw.write("</tr>");
			//--Portfolio row End

			//--Base Inventory start
			pw.write("<tr>");
			//pw.write("<td>Base Inventory</td>");
			String baseMsg="The TeacherMatch Educator\'s Professional Inventory (Base Inventory) is a software-as-a-service tool that uses research-based predictive analytics to provide schools and school districts data about a teacher candidate\'s ability to grow students before they are hired." +
					"</br></br>Before you start the Base Inventory, please note the following guidelines.  These guidelines have been implemented to ensure that all candidates take the Base Inventory under equitable conditions." +
					"</br></br>Each item in the Base Inventory has a stipulated time limit and you must respond to each question within its stipulated time limit. You must answer all of the questions in one sitting. Make sure that you have at least 90 minutes of uninterrupted time to complete the Base Inventory. Make sure that you have a stable and reliable Internet connection. Do not close your browser or hit the 'back' button on your browser. You are not able to skip questions."+
					"</br></br>If you do not follow these guidelines when taking the Base Inventory, your status may become 'Timed out' and you will not be able to complete the Base Inventory for twelve months.  " +
					"As per the terms of service and under penalty of being excluded from all application processes, candidates are not allowed to create a second account on the TeacherMatch platform.   If a candidate\'s status is 'Timed Out' this does not prevent him or her from applying for jobs. It is up to each school and school district to decide how to consider candidates with this designation.";

			pw.write("<td>Base Inventory&nbsp;<a  href='#' id='iconpophoverBase' rel='tooltip' data-original-title=\""+baseMsg+"\"><img src='images/qua-icon.png' width='15' height='15' alt=''></a></td>");
			pw.write("<td>");
			if(baseInvStatus)
			{
				pw.write("<strong class='text-success'>Completed</strong>");
			}
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
			{
				pw.write("<strong class='text-error'>Timed Out</strong>");
			}
			else
			{
				pw.write("<strong class='text-error' id='bStatus' >Incomplete</strong>");
			}				
			pw.write("</td>");
			pw.write("<td class='pagination-centered' id='bClick'>");
			if(prefStatus && portfolioStatus )
			{		
				String toolTipText = "";
				if(teacherBaseAssessmentStatus==null)
				{
					toolTipText = "Start Base Inventory";			
				}
				else if(teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
				{
					toolTipText = "Resume Base Inventory";				
				}

				if(baseInvStatus)
				{}
				else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
				{}
				else
				{
					pw.write("<a data-original-title='"+toolTipText+"' rel='tooltip' id='iconpophover3' href='javascript:void(0)' onclick=\"checkInventory(0,null)\">");
					pw.write("<img src='images/option06.png'>");
					pw.write("</a>");
				}		

			}
			else
			{

				if(((teacherBaseAssessmentStatus==null) ||(!baseInvStatus) && (!teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))))
				{
					//vishwanath
					Boolean epiStandalone = session.getAttribute("epiStandalone")==null?false:(Boolean)session.getAttribute("epiStandalone");
					if(epiStandalone)
						pw.write("<a data-original-title='Start Base Inventory' rel='tooltip' id='iconpophover3' href='javascript:void(0)' onclick=\"checkInventory(0,null);\">");
					else
						pw.write("<a data-original-title='Start Base Inventory' rel='tooltip' id='iconpophover3' href='javascript:void(0)' onclick=\"chkBaseInventoryStatus("+prefStatus+","+portfolioStatus+");\">");
					pw.write("<img src='images/option06.png'>");
					pw.write("</a>");
				}
			}
			pw.write("</td>");
			pw.write("</tr>");
			//--Base Inventory end


			//--Job Specific Inventory start
			pw.write("<tr>");
			pw.write("<td>Job Specific Inventory</td>");
			pw.write("<td>");
			if(noOfJBSIncompJobForTeacher>0)
			{
				pw.write("<strong class='text-error'>Incomplete</strong>");
			}
			else if(noOfJBSIncompJobForTeacher==0 && noOfJBSCompJobForTeacher==0 && noOfJBSViolatedJobForTeacher>0 )
			{
				pw.write("<strong class='text-error'>Timed Out</strong>");
			}
			else
			{
				pw.write("<strong class='text-success'>Completed</strong>");
			}
			pw.write("</td>");
			pw.write("<td class='pagination-centered' >");
			if(noOfJBSIncompJobForTeacher==0)
			{
				/*pw.write("<a data-original-title='Complete Now' rel='tooltip' id='iconpophover4' onclick=\"alert('You do not have any Job Specific Inventory pending')\">");
				pw.write("<img src='images/option02.png'>");
				pw.write("</a>");
				 */
			}
			else if(prefStatus == false || portfolioStatus == false  )
			{
				pw.write("<a data-original-title='Complete Now' rel='tooltip' id='iconpophover4' href='#' onclick=\"chkJobSpecificInventory("+prefStatus+","+portfolioStatus+");\">");
				pw.write("<img src='images/option02.png'>");
				pw.write("</a>");
			}
			else if(noOfJBSIncompJobForTeacher==1)
			{
				JobForTeacher jft = lstJBSIncompJobForTeacher.get(0);
				if(jft.getJobId().getJobCategoryMaster().getBaseStatus() && (!baseInvStatus))
				{
					pw.write("<a data-original-title='Complete Now' rel='tooltip' id='iconpophover4' href='javascript:void(0);'  onclick=\"checkInventory("+0+",null);\">");
					pw.write("<img src='images/option02.png'>");
					pw.write("</a>");
				}else
				{
					pw.write("<a data-original-title='Complete Now' rel='tooltip' id='iconpophover4' href='javascript:void(0);'  onclick=\"checkInventory("+jft.getJobId().getJobId()+",null);\">");
					pw.write("<img src='images/option02.png'>");
					pw.write("</a>");
				}
			}
			else
			{
				pw.write("<a data-original-title='Complete Now' rel='tooltip' id='iconpophover4' href='jobspecificinventory.do' >");
				pw.write("<img src='images/option02.png'>");
				pw.write("</a>");
			}

			pw.write("</td>");
			pw.write("</tr>");			
			//--Job Specific Inventory end

			pw.write("<tr>");			
			pw.write("<td colspan='3'>You have "+(noOfJBSIncompJobForTeacher<2?""+noOfJBSIncompJobForTeacher:"<a href='jobspecificinventory.do'>"+noOfJBSIncompJobForTeacher+"</a>")+" Incomplete Job Specific Inventories");
			pw.write("<div class='pagination-right'>&nbsp;</div>");
			pw.write("</td>");
			pw.write("</tr>");
			pw.write("</table>");

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}


	@RequestMapping(value="/getDashboardReport.do", method=RequestMethod.GET)
	public void getDashboardReport(HttpServletRequest request, HttpServletResponse response)
	{
		try 
		{
			PrintWriter pw = response.getWriter();
			HttpSession session = request.getSession();
			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

			TeacherPreference tPreference = null;
			TeacherPortfolioStatus tPortfolioStatus;

			boolean prefStatus = false;
			boolean portfolioStatus = false;
			boolean baseInvStatus = false;
			String baseStatus="";

			tPreference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
			if(tPreference!=null)
			{
				prefStatus = true;
			}

			tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(tPortfolioStatus!=null && tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted())
			{
				portfolioStatus = true;
			}

			TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if(teacherBaseAssessmentStatus!=null && (teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") ))
			{
				baseInvStatus=true;				 
			}
			if(teacherBaseAssessmentStatus!=null)
				baseStatus = teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName();
			pw.write("<table width='100%' border='0' class='table table-bordered table-striped'>");
			pw.write("<thead class='bg'>");
			pw.write("<tr>");
			pw.write("<th><img class='fl' src='images/reports-icon.png'> <h4>Reports</h4></th>");
			pw.write("<th>Status</th>");
			pw.write("<th>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			pw.write("</tr>");
			pw.write("</thead>");
			pw.write("<tr>");
			pw.write("<td>Professional Development Report&nbsp;<a  href='#' id='iconpophoverRe' rel='tooltip' data-original-title='Upon completion of the Base Inventory, a detailed and customized Professional Development Report is available for purchase. The report identifies strengths and opportunities and provides specific, actionable steps you can take to grow your skills - all based on decades of research.  Click on the cart icon to gain access to this valuable learning opportunity.'><img src='images/qua-icon.png' width='15' height='15' alt=''></a></td>");

			pw.write("<td>");
			if(baseInvStatus)
			{
				pw.write("<strong class='text-success'>Completed</strong>");
			}
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
			{
				pw.write("<strong class='text-error'>Timed Out</strong>");
			}
			else
			{
				pw.write("<strong class='text-error'>Incomplete</strong>");
			}
			pw.write("</td>");

			if(portfolioStatus)
			{
				pw.write("<td class='pagination-centered'><a data-original-title='Buy' rel='tooltip' id='iconpophover5' href='javascript:void(0);' onclick=\"checkBaseCompeleted("+teacherDetail.getTeacherId()+")\"><img src='images/option07.png'></a></td>");				
			}
			else
			{
				pw.write("<td class='pagination-centered'><a data-original-title='Buy' rel='tooltip' id='iconpophover5' href='javascript:void(0);' onclick=\"alert('Please complete your Portfolio.')\"><img src='images/option07.png'></a></td>");
			}



			pw.write("</tr>");
			pw.write("<tr>");
			pw.write("<td>Portfolio Report</td>");

			pw.write("<td>");
			if(portfolioStatus)
			{
				pw.write("<strong class='text-success'>Completed</strong>");
			}
			else
			{
				pw.write("<strong class='text-error'>Incomplete</strong>");
			}
			pw.write("</td>");


			if(portfolioStatus)
			{
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				pw.write("<td class='pagination-centered'><a data-original-title='View' rel='tooltip' id='iconpophover6' href='javascript:void(0);'  onclick=\"downloadPortfolioReport('"+teacherDetail.getTeacherId()+"','iconpophover6');"+windowFunc+"\"><img src='images/option06.png'></a></td>");
			}
			else
			{
				pw.write("<td class='pagination-centered'><a data-original-title='View' rel='tooltip' id='iconpophover6' href='#' onclick=\"alert('Please complete your Portfolio.')\"><img src='images/option06.png'></a></td>");
			}

			pw.write("</tr>");
			pw.write("<tr>");
			pw.write("<td>&nbsp;</td>");
			pw.write("<td>&nbsp;</td>");
			pw.write("<td>&nbsp;</td>");
			pw.write("</tr>");	
			pw.write("<tr>");
			pw.write("<td>&nbsp;</td>");
			pw.write("<td>&nbsp;</td>");
			pw.write("<td>&nbsp;</td>");
			pw.write("</tr>");
			pw.write("<tr>");
			pw.write("<td>&nbsp;</td>");
			pw.write("<td>&nbsp;</td>");
			pw.write("<td>&nbsp;</td>");
			pw.write("</tr>");
			pw.write("<tr>");
			pw.write("<td>&nbsp;</td>");
			pw.write("<td>&nbsp;</td>");
			pw.write("<td>&nbsp;</td>");
			pw.write("</tr>");
			pw.write("<tr>");
			pw.write("<td>&nbsp;</td>");
			pw.write("<td>&nbsp;</td>");
			pw.write("<td>&nbsp;</td>");
			pw.write("</tr>");
			pw.write("</table>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	public List<JobForTeacher> getJobSpecInventory(HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		List<JobForTeacher> lstJobForTeacher = null;
		try 
		{
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			StatusMaster statusInComplete = WorkThreadServlet.statusMap.get("icomp");
			lstJobForTeacher = jobForTeacherDAO.findJobsForJobSpecificInventory(teacherDetail,statusInComplete,true);
			List<JobForTeacher> lstTempJobForTeacher = new ArrayList<JobForTeacher>(lstJobForTeacher);

			List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacher(teacherDetail);
			JobOrder jobOrder = null;
			for(TeacherAssessmentStatus teacherAssessmentStatus: lstTeacherAssessmentStatus)
			{
				if(!teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
				{
					jobOrder = teacherAssessmentStatus.getJobOrder();
					for(JobForTeacher jbfTeacher: lstTempJobForTeacher)
					{
						if(jbfTeacher.getJobId().equals(teacherAssessmentStatus.getJobOrder()))
						{
							lstJobForTeacher.remove(jbfTeacher);							
						}
					}
				}
			}

			//map.addAttribute("lstJobForTeacher", lstJobForTeacher);

		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstJobForTeacher;
	}

	public List<JobOrder> getJobByPref(HttpServletRequest request,Order sortOrderStrVal)
	{	
		List<JobOrder> lstJobOrder = null;
		try
		{	
			List<String> lstRegionId=null;
			List<String> lstSchoolTypeId=null;
			List<String> lstGeographyId=null;
			List<SchoolMaster> lstSchoolMasters = null;
			List<SchoolMaster> lstSMtoRemove = new ArrayList<SchoolMaster>();



			HttpSession session = request.getSession();			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			TeacherPreference preference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);

			lstRegionId = Arrays.asList(preference.getRegionId().trim().split("\\|"));

			lstSchoolTypeId = Arrays.asList(preference.getSchoolTypeId().trim().split("\\|"));

			lstGeographyId = Arrays.asList(preference.getGeoId().trim().split("\\|"));



			if(sortOrderStrVal!=null)
			{
				lstJobOrder = jobOrderDAO.findSortedJobtoShow(sortOrderStrVal);
			}
			else
			{
				lstJobOrder = jobOrderDAO.findJobtoShow();
			}
		
			for(JobOrder jb:lstJobOrder)
			{
				lstSchoolMasters=jb.getSchool();
				if(lstSchoolMasters!=null)
				{			
					for(SchoolMaster school: lstSchoolMasters)
					{				
						if(lstGeographyId.contains(""+school.getGeoMapping().getGeoId().getGeoId()) && lstSchoolTypeId.contains(""+school.getSchoolTypeId().getSchoolTypeId() ) && lstRegionId.contains(""+school.getRegionId().getRegionId()))
						{
							//do nothing for now
						}
						else
						{
							//lstSMasters.remove(school);
							lstSMtoRemove.add(school);

						}

					}
					if(jb.getCreatedForEntity() == 3)
					{
						for(SchoolMaster school:lstSMtoRemove)
						{
							lstSchoolMasters.remove(school);
						}
						lstSMtoRemove.clear();
					}
				}

			}
			
			List<JobOrder> lstJbOdrToremove = new ArrayList<JobOrder>(lstJobOrder);
			for( JobOrder jb:lstJbOdrToremove)
			{

				if(jb.getCreatedForEntity() == 3)
				{
					if(jb.getSchool()==null || jb.getSchool().size()==0)
					{

						lstJobOrder.remove(jb);
					}
					else
					{

					}
				}
				else
				{
					if(!lstRegionId.contains(""+jb.getDistrictMaster().getStateId().getRegionId().getRegionId()))
					{
						lstJobOrder.remove(jb);
					}
				}
			}

		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
			lstJobOrder = new ArrayList<JobOrder>();
			
		}
		return lstJobOrder;
	}



	public String showDashboardJbofIntresGrid(HttpServletRequest request)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{		
			HttpSession session = request.getSession();
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");

			boolean prefStatus = false;
			boolean portfolioStatus = false;
			boolean baseInvStatus=false;
			TeacherPreference tPreference=null;
			TeacherPortfolioStatus tPortfolioStatus=null;


			tPreference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
			if(tPreference!=null){
				prefStatus = true;
			}

			tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(tPortfolioStatus!=null && tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted()){
				portfolioStatus = true;
			}
			TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if((teacherBaseAssessmentStatus!=null) && (!teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp") )){
				baseInvStatus=true;				 
			}

			List<JobForTeacher> lstAllJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);
			
			List<JobForTeacher> lstcompletJFT = new ArrayList<JobForTeacher>();
			List<JobForTeacher> lstincompletJFT = new ArrayList<JobForTeacher>();
			List<JobForTeacher> lstviolatedJFT = new ArrayList<JobForTeacher>();
			String jobStatus = "";
			for (JobForTeacher jobForTeacher : lstAllJFT) {
				jobStatus = jobForTeacher.getStatus().getStatusShortName();
				if(jobStatus.equalsIgnoreCase("comp") || jobStatus.equalsIgnoreCase("rem") || jobStatus.equalsIgnoreCase("hird"))
					lstcompletJFT.add(jobForTeacher);
				else if(jobStatus.equalsIgnoreCase("icomp"))
					lstincompletJFT.add(jobForTeacher);
				else if(jobStatus.equalsIgnoreCase("vlt"))
					lstviolatedJFT.add(jobForTeacher);
			}
			
			sb.append("<table width='100%' border='0' class='table table-bordered table-striped'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th width='50%'><img class='fl' src='images/search-icon.png'> <h4>Jobs Applied</h4></th>");
			sb.append("<th width='25%'>Status</th>");
			sb.append("<th width='25%'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");

			int rowCount=0;
			for(JobForTeacher jbTeacher : lstincompletJFT)
			{
				if(rowCount==4)
					break;
				rowCount++;								

				sb.append("<tr>");
				sb.append("<td><a href='applyjob.do?jobId="+jbTeacher.getJobId().getJobId()+"'>"+jbTeacher.getJobId().getJobTitle()+"</a></td>");
				sb.append("<td><strong class='text-error'>Incomplete</strong></td>");
				sb.append("<td>");
				
				
				if((!jbTeacher.getJobId().getIsPortfolioNeeded()) && (jbTeacher.getJobId().getJobCategoryMaster().getBaseStatus()) &&  (!baseInvStatus)){
					sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('0',null)\"><img src='images/option02.png'></a>");					
				}
				else if((!jbTeacher.getJobId().getIsPortfolioNeeded()) && (!jbTeacher.getJobId().getJobCategoryMaster().getBaseStatus()) && (jbTeacher.getJobId().getIsJobAssessment()) ){
					sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('"+jbTeacher.getJobId().getJobId()+"',null)\"><img src='images/option02.png'></a>");					
				}else if(prefStatus == false || portfolioStatus == false){
					sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"chkJobSpecificInventory("+prefStatus+","+portfolioStatus+","+false+");\"><img src='images/option02.png'></a>");					
				}
				else if(jbTeacher.getJobId().getJobCategoryMaster().getBaseStatus() && (!baseInvStatus)){
					sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('0',null)\"><img src='images/option02.png'></a>");					
				}
				else{
					sb.append("<a data-original-title='Complete Now' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('"+jbTeacher.getJobId().getJobId()+"',null)\"><img src='images/option02.png'></a>");					
				}

				sb.append("&nbsp;&nbsp;<a data-original-title='Withdraw' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return jbWithdraw('"+jbTeacher.getJobForTeacherId()+"')\"><img src='images/option05.png'></a>");
				sb.append("&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png'></a>");
				sb.append("</td>");	   				
				sb.append("</tr>");
			}

			for(JobForTeacher jbTeacher : lstcompletJFT)
			{
				if(rowCount==4)
					break;
				rowCount++;
				sb.append("<tr>");
				sb.append("<td> <a href='applyjob.do?jobId="+jbTeacher.getJobId().getJobId()+"'>"+jbTeacher.getJobId().getJobTitle()+"</a></td>");
				sb.append("<td><strong class='text-success'>Completed</strong></td>");
				sb.append("<td>");
				sb.append("<a data-original-title='Withdraw' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return jbWithdraw('"+jbTeacher.getJobForTeacherId()+"')\"><img src='images/option05.png'></a>");
				sb.append("&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png'></a>");
				sb.append("</td>");	   				
				sb.append("</tr>");
			}

			for(JobForTeacher jbTeacher : lstviolatedJFT)
			{
				if(rowCount==4)
					break;
				rowCount++;
				sb.append("<tr>");
				sb.append("<td> <a href='applyjob.do?jobId="+jbTeacher.getJobId().getJobId()+"'>"+jbTeacher.getJobId().getJobTitle()+"</a></td>");
				sb.append("<td><strong class='text-error'>Timed Out</strong></td>");
				sb.append("<td>");
				sb.append("<a data-original-title='Withdraw' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return jbWithdraw('"+jbTeacher.getJobForTeacherId()+"')\"><img src='images/option05.png'></a>");
				sb.append("&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png'></a>");
				sb.append("</td>");	   				
				sb.append("</tr>");
			}

			sb.append("<tr>");
			sb.append("<td colspan='3' >You have "+(lstcompletJFT.size()==0?"0":"<a href='jobsofinterest.do?searchby=comp'>"+lstcompletJFT.size()+"</a>")
					+" Complete, "+(lstincompletJFT.size()==0?"0":"<a href='jobsofinterest.do?searchby=icomp'>"+lstincompletJFT.size()+"</a>")
					+" Incomplete, "
					+"and "+(lstviolatedJFT.size()==0?"0":"<a href='jobsofinterest.do?searchby=vlt'>"+lstviolatedJFT.size()+"</a>")
					+" Timed Out ");
			sb.append("<div class='right'><a href='jobsofinterest.do'><strong>See All</strong></a></div>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return sb.toString();
	}

	public String showDashboardJbofIntresGridAvailable(HttpServletRequest request)
	{
		System.out.println(":::::::::::::::::showDashboardJbofIntresGridAvailable:::::::::::::::::");
		StringBuffer sb = new StringBuffer();
		try 
		{		
			HttpSession session = request.getSession();
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");

			Order sortOrderStrVal	=	null;

			List<JobForTeacher> lstAllJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);
			
			List<JobOrder> lstJobOrder = null;
			if(teacherDetail.getUserType().equalsIgnoreCase("N")){
				lstJobOrder = getJobByPref(request,sortOrderStrVal);
			}
			else{	
				DistrictMaster districtMaster = lstAllJFT.get(0).getJobId().getDistrictMaster(); 
				lstJobOrder = jobOrderDAO.findJobOrderbyDistrict(districtMaster);
			}

			
			List<JobForTeacher> lstwidrwJFT = new ArrayList<JobForTeacher>();

			for(JobForTeacher jft: lstAllJFT)
			{				
				JobOrder jobOrder = jft.getJobId();				
				if(lstJobOrder.contains(jobOrder))
				{
					lstJobOrder.remove(jobOrder);
				}
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("widrw"))
					lstwidrwJFT.add(jft);
				
			}


			sb.append("<table width='100%' border='0' class='table table-bordered table-striped'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th width='50%'><img class='fl' src='images/search-icon.png'> <h4>"+Utility.getLocaleValuePropByKey("lblJoOfInterest", locale)+"</h4></th>");
			sb.append("<th width='25%'>Status</th>");
			sb.append("<th width='25%'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");

			int rowCount=0;

			for(JobOrder jbOrder: lstJobOrder)
			{
				if(rowCount==4)
					break;
				rowCount++;
				sb.append("<tr>");
				sb.append("<td><a href='applyjob.do?jobId="+jbOrder.getJobId()+"'>"+jbOrder.getJobTitle()+"</a></td>");
				sb.append("<td><strong class='text-info'>Available</strong></td>");
				sb.append("<td>");
				
				sb.append("<a data-original-title=' Apply Now' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jbOrder.getJobId()+"' ><img src='images/option01.png' style='width:21px; height:21px;' ></a>");
				sb.append("&nbsp;&nbsp;<a data-original-title='Hide' rel='tooltip' id='tpHide"+rowCount+"' href='#' onclick=\"return jbHide('"+jbOrder.getJobId()+"')\"><img src='images/option03.png' style='width:21px; height:21px;' ></a>");
				sb.append("&nbsp;&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+(rowCount+4)+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;' ></a>");

				sb.append("</td>");	   				
				sb.append("</tr>");
			}
			
			
			sb.append("<tr>");
			sb.append("<td colspan='3' >You have "
					+(lstJobOrder.size()==0?"0":"<a href='jobsofinterest.do?searchby=avlbl'>"+lstJobOrder.size()+"</a>")
					+" Not Applied and "+(lstwidrwJFT.size()==0?"0":"<a href='jobsofinterest.do?searchby=widrw'>"+lstwidrwJFT.size()+"</a>")+" Withdrawn");
			sb.append("<div class='right'><a href='jobsofinterest.do'><strong>See All</strong></a></div>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	@RequestMapping(value="/divpopup.do", method=RequestMethod.GET)
	public String doDivPopUpGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		return "divpopup";
	}
	
	
	public static void main(String[] args) 
	{
		String s = "1|2|3";

		String [] sArray =s.split("\\|");
		//System.out.println("<"+sArray[0]+">");

	}
	
	
	
	/* @Start
	 * @Ashish Kumar
	 * @description :: Get District Address
	 * */
		public String getDistrictAddress(JobOrder jobOrderDetails)
		{
			////System.out.println(" >>>>>>>>>>>>>>>>>>>> Inside Get Dsitrict Address :: >>>>>>>>>>>>>>>>>>> ");
			String districtAddress="";
			String address="";
			String state="";
			String city = "";
			String zipcode="";
			
			//Get district address
			if(jobOrderDetails.getDistrictMaster()!=null)
			{
				if(jobOrderDetails.getDistrictMaster().getAddress()!=null && !jobOrderDetails.getDistrictMaster().getAddress().equals(""))
				{
					if(!jobOrderDetails.getDistrictMaster().getCityName().equals(""))
					{
						districtAddress = jobOrderDetails.getDistrictMaster().getAddress()+", ";
					}
					else
					{
						districtAddress = jobOrderDetails.getDistrictMaster().getAddress();
					}
				}
				if(!jobOrderDetails.getDistrictMaster().getCityName().equals(""))
				{
					if(jobOrderDetails.getDistrictMaster().getStateId()!=null && !jobOrderDetails.getDistrictMaster().getStateId().getStateName().equals(""))
					{
						city = jobOrderDetails.getDistrictMaster().getCityName()+", ";
					}else{
						city = jobOrderDetails.getDistrictMaster().getCityName();
					}
				}
				if(jobOrderDetails.getDistrictMaster().getStateId()!=null && !jobOrderDetails.getDistrictMaster().getStateId().getStateName().equals(""))
				{
					
					if(jobOrderDetails.getDistrictMaster().getZipCode()!=null && !jobOrderDetails.getDistrictMaster().getZipCode().equals(""))
					{
						state = jobOrderDetails.getDistrictMaster().getStateId().getStateName()+", ";
					}
					else
					{
						state = jobOrderDetails.getDistrictMaster().getStateId().getStateName();
					}	
				}
				if(jobOrderDetails.getDistrictMaster().getZipCode()!=null && !jobOrderDetails.getDistrictMaster().getZipCode().equals(""))
				{
						zipcode = jobOrderDetails.getDistrictMaster().getZipCode();
				}
				
				if(districtAddress !="" || city!="" ||state!="" || zipcode!=""){
					address = districtAddress+city+state+zipcode;
				}else{
					address="";
				}
				
			}
		
			return address;
		}
		
		public String getSchoolAddress(JobOrder jbOrder){
			String schoolAddress="";
			String sAddress="";
			String sstate="";
			String scity = "";
			String szipcode="";
			
			// Get School address
				if(jbOrder.getSchool()!=null)
				{
					if(jbOrder.getSchool().get(0).getAddress()!=null && !jbOrder.getSchool().get(0).getAddress().equals(""))
					{
						if(!jbOrder.getSchool().get(0).getCityName().equals(""))
						{
							schoolAddress = jbOrder.getSchool().get(0).getAddress()+", ";
						}
						else
						{
							schoolAddress = jbOrder.getSchool().get(0).getAddress();
						}
					}
					if(!jbOrder.getSchool().get(0).getCityName().equals(""))
					{
						if(jbOrder.getSchool().get(0).getStateMaster()!=null && !jbOrder.getSchool().get(0).getStateMaster().getStateName().equals(""))
						{
							scity = jbOrder.getSchool().get(0).getCityName()+", ";
						}else{
							scity = jbOrder.getSchool().get(0).getCityName();
						}
					}
					if(jbOrder.getSchool().get(0).getStateMaster()!=null && !jbOrder.getSchool().get(0).getStateMaster().getStateName().equals(""))
					{
						if(!jbOrder.getSchool().get(0).getZip().equals(""))
						{
							sstate = jbOrder.getSchool().get(0).getStateMaster().getStateName()+", ";
						}
						else
						{
							sstate = jbOrder.getSchool().get(0).getStateMaster().getStateName();
						}	
					}
					if(!jbOrder.getSchool().get(0).getZip().equals(""))
					{
							szipcode = jbOrder.getSchool().get(0).getZip().toString();
					}
					
					if(schoolAddress !="" || scity!="" ||sstate!="" || szipcode!=""){
						sAddress = schoolAddress+scity+sstate+szipcode;
					}else{
						sAddress="";
					}
				}
			
			return sAddress;
		}
		
	//New Phili Candidate portfolio.
		
		@RequestMapping(value="/myportfolio.do", method=RequestMethod.GET)
		public String doMyPOrtFolioGET(ModelMap map,HttpServletRequest request)
		{	
			HttpSession session = request.getSession(false);
			List<StateMaster> listStateMaster;
			TeacherExperience teacherExperience = null;
			
			if (session == null || session.getAttribute("teacherDetail") == null) 
			{
				return "redirect:index.jsp";
			}
			try
			{ 
				String teacherFullName="";
				TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
				 String firstName=teacherDetail.getFirstName()==null?"":teacherDetail.getFirstName();
			     String lastName=teacherDetail.getLastName()==null?"":teacherDetail.getLastName();
			     teacherFullName=firstName +" "+lastName;
				
			     map.addAttribute("fullName", teacherFullName);
				 map.addAttribute("teacherDetail", teacherDetail);
				 listStateMaster = stateMasterDAO.findAllStateByOrder();
					teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
					List<CertificationStatusMaster> lstCertificationStatusMaster=certificationStatusMasterDAO.findAllCertificationStatusMasterByOrder();
					Criterion certIficatCri = Restrictions.eq("status", "A");
					List<CertificationTypeMaster>listCertificationTypeMasters=certificationTypeMasterDAO.findByCriteria(certIficatCri);	
					List<TFAAffiliateMaster> lstTFAAffiliateMaster	=	tfaAffiliateMasterDAO.findByCriteria(Order.asc("tfaAffiliateName"));
					List<TFARegionMaster> lstTFARegionMaster		=	tfaRegionMasterDAO.findByCriteria(Order.asc("tfaRegionName"));
					
					map.addAttribute("lstTFAAffiliateMaster", lstTFAAffiliateMaster);
					map.addAttribute("lstTFARegionMaster", lstTFARegionMaster);
					map.addAttribute("lstCorpsYear", Utility.getLasterYeartillAdvanceYear(1990));
					
					map.addAttribute("teacherExperience", teacherExperience);
					map.addAttribute("listStateMaster", listStateMaster);
					map.addAttribute("lstLastYear", Utility.getLasterYearByYear(1965));
					map.addAttribute("lstComingYear", Utility.getComingYearsByYear());
					map.addAttribute("lstCertificationTypeMasters", Utility.getComingYearsByYear());
					map.addAttribute("listCertTypeMasters", listCertificationTypeMasters);
					map.addAttribute("lstCertificationStatusMaster", lstCertificationStatusMaster);
					//map.addAttribute("isPortfolioNeeded", getTeacherPortfolioStatusForNativeAndRefered(teacherDetail));
					
					String openDiv = (request.getParameter("openDiv")==null)? "" : request.getParameter("openDiv").trim(); 
					map.addAttribute("openDiv",openDiv);
				 
				 
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}

			return "myportfolio";
		}	
		
		
		// Optimization for UserDashBoard
		
		@RequestMapping(value="/userdashboardnew.do", method=RequestMethod.GET)
		public String doSignUpGET_Op(ModelMap map,HttpServletRequest request)
		{
			HttpSession session = request.getSession(false);
	       
			if (session == null || session.getAttribute("teacherDetail") == null) 
			{
				return "redirect:index.jsp";
			}
				
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
			try 
			{		
				FavTeacher favTeacher = favTeacherDAO.findFavTeacherByTeacher(teacherDetail);
				map.addAttribute("favTeacher", favTeacherDAO.findRandomFavTeacher(favTeacher));

				String jobId = request.getParameter("jobId");
				String mf = request.getParameter("mf");
				String isAffilated =null;
				if(request.getParameter("ok_cancelflag")!=null){
					isAffilated=request.getParameter("ok_cancelflag");
				}
				JobOrder jobOrder  = null;
				String compltd = "N";
				boolean testvar=false;
				if(jobId!=null && jobId.trim().length()>0)
				{
					jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
					JobForTeacher jft = null;
					jft = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
					if(jft!=null)
					{	testvar=true;
						if(jft.getStatus().getStatusShortName().equalsIgnoreCase("comp"))
							compltd = "Y";
					}
				}
				try{
					Integer schoolSelectionflag = jobForTeacherDAO.countCandidateConsideration(teacherDetail);
					
					if(schoolSelectionflag!=null && schoolSelectionflag > 0){
						schoolSelectionflag=1;
					}
					session.setAttribute("schoolSelectionflag",schoolSelectionflag);
					
				//	List<JobForTeacher> lstJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);
				/*	if(lstJFT!=null && lstJFT.size()>0){
						for (JobForTeacher jobForTeacher : lstJFT) {
							int schoolSelectionflag=0;
							if(jobForTeacher.getCandidateConsideration()!=null){
								schoolSelectionflag=1;
							}
							session.setAttribute("schoolSelectionflag",schoolSelectionflag);
							if(schoolSelectionflag==1){
								break;
							}
						}
					}*/
				}catch(Exception e){
					e.printStackTrace();
				}
				
				try {
				//	List<JobForTeacher> jobForTeacherList = jobForTeacherDAO.findJobByTeacherNotKES(teacherDetail);
				//	List<JobForTeacher> jobForTeacherListForKES = jobForTeacherDAO.findJobByTeacherForKES(teacherDetail,false);
					
					Integer jobForTeacherList = jobForTeacherDAO.findJobByTeacherNotKES_Op(teacherDetail);
					Integer jobForTeacherListForKES  =  jobForTeacherDAO.findJobByTeacherForKES_Op(teacherDetail,false);
					
					if(!((jobForTeacherList!=null && jobForTeacherList>0) || jobForTeacherListForKES==0))
					{
						request.getSession().setAttribute("powerProfile", "hide");
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				boolean openDspq=true;
				if(jobOrder!=null && jobOrder.getIsInviteOnly()!=null && jobOrder.getHiddenJob()!=null){
					System.out.println("openDspq :: "+openDspq +" isInviteOnly :: "+jobOrder.getIsInviteOnly()+" hiddenJob :: "+  jobOrder.getHiddenJob());
					if(jobOrder.getIsInviteOnly() && !jobOrder.getHiddenJob() && testvar){
						openDspq=false;//true
					}else if(jobOrder.getIsInviteOnly() && !jobOrder.getHiddenJob() && !testvar){
						openDspq=true;//false
					}
				}
				
				//System.out.println("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP openDspq ::: "+openDspq);
				map.addAttribute("compltd", compltd); 
				map.addAttribute("jobOrder", jobOrder); 
				map.addAttribute("mf", mf);
				map.addAttribute("openDspq", openDspq);
				
				/* Sekhar Add Branch Details*/
				
				boolean prefCheck=false;
				if(request.getParameter("pShowFlg")!=null){
					prefCheck=true;
				}
				/* Sekhar Add Branch Details*/
				if(prefCheck==false && isAffilated!=null){
					BranchesAjax branchesAjax=new BranchesAjax();
					List<JobForTeacher> jobForTeacherKes=jobForTeacherDAO.findJobByTeacherForKES(teacherDetail,true);
					try{
						if(jobForTeacherKes.size()>0){
							String kesData=branchesAjax.getBranchDetails(jobForTeacherKes.get(0).getJobId());
							if(kesData!=null){
								map.addAttribute("kesData",kesData);
								map.addAttribute("kesDataFlag",true);
							}else{
								map.addAttribute("kesDataFlag",false);
							}
						}else{
							map.addAttribute("kesDataFlag",false);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
				// Start ... dynamic portfolio for External Job Apply
				
				map.addAttribute("lstLastYear", Utility.getLasterYearByYear(1955));
				
				TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
				map.addAttribute("teacherExperience", teacherExperience);
				
				//Criterion criterionGenderActive = Restrictions.eq("status", "A");
				List<GenderMaster> lstGenderMasters  = genderMasterDAO.findAllGenderByOrder();
				
				List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
				listStateMaster = stateMasterDAO.findAllStateByOrder();
				map.addAttribute("listStateMaster", listStateMaster);
				
				List<CertificationStatusMaster> lstCertificationStatusMaster=certificationStatusMasterDAO.findAllCertificationStatusMasterByOrder();
				map.addAttribute("lstCertificationStatusMaster", lstCertificationStatusMaster);
				
				List<TFAAffiliateMaster> lstTFAAffiliateMaster	=	tfaAffiliateMasterDAO.findByCriteria(Order.asc("tfaAffiliateName"));
				List<TFARegionMaster> lstTFARegionMaster		=	tfaRegionMasterDAO.findByCriteria(Order.asc("tfaRegionName"));
				map.addAttribute("lstTFAAffiliateMaster", lstTFAAffiliateMaster);
				map.addAttribute("lstTFARegionMaster", lstTFARegionMaster);
				map.addAttribute("lstCorpsYear", Utility.getLasterYeartillAdvanceYear(1990));
				
				Criterion certIficatCri = Restrictions.eq("status", "A");
				List<CertificationTypeMaster>listCertificationTypeMasters=certificationTypeMasterDAO.findByCriteria(certIficatCri);
				map.addAttribute("listCertTypeMasters", listCertificationTypeMasters);
				map.addAttribute("lstComingYear", Utility.getComingYearsByYear());
				map.addAttribute("lstDays", Utility.getDays());
				
				String districtDName=null;
				if(jobOrder!=null)
				{
					if(jobOrder.getDistrictMaster()!=null){
						DistrictMaster districtMaster=null;
						districtMaster=jobOrder.getDistrictMaster();
						if(districtMaster.getDisplayName() !=null && !districtMaster.getDisplayName().equals("")){
							districtDName=districtMaster.getDisplayName();
						}else{
							districtDName=districtMaster.getDistrictName();
						}
					}
					else if(jobOrder.getHeadQuarterMaster()!=null){
						districtDName = jobOrder.getHeadQuarterMaster().getHeadQuarterName();
					}
					map.addAttribute("districtDName", districtDName);
				}
				// End ... dynamic portfolio for External Job Apply
				boolean dspqFlag =true;
				boolean getQQ =true;
				boolean isMiami = false;
				boolean internalFlag=false;
				DistrictMaster districtMaster=null;
				try{
					if(jobOrder!=null){
						
						if(jobOrder.getDistrictMaster()!=null){
							if(jobOrder.getDistrictMaster().getDistrictId().equals(1200390))
								isMiami=true;
						}
						
						
						if(jobOrder.getDistrictMaster()!=null){
							districtMaster=jobOrder.getDistrictMaster();
						}
						List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
						
						if(itcList.size()>0){
							internalFlag=true;
						}
						if(jobOrder.getDistrictMaster()!=null && internalFlag){
							if(districtMaster.getOfferDistrictSpecificItems()){
								dspqFlag=true;
							}else{
								dspqFlag=false;
							}
							if(districtMaster.getOfferQualificationItems()){
								getQQ=true;
							}else{
								getQQ=false;
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				String staffTypeSession = "";
				try{
					staffTypeSession = isAffilated =(String)(session.getAttribute("staffType")==null?"0":session.getAttribute("staffType"));
					if(isAffilated==null){
						isAffilated =(String)(session.getAttribute("isAffilated")==null?"0":session.getAttribute("isAffilated"));
					}
					System.out.println("isAffilated:::::>>>:::::::"+isAffilated);
					if(jobOrder!=null && isAffilated.equalsIgnoreCase("1") && internalFlag==false){
						if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferDistrictSpecificItems()){
							dspqFlag=true;
						}else{
							dspqFlag=false;
						}
						if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferQualificationItems()){
							getQQ=true;
						}else{
							getQQ=false;
						}
					}
					List<DistrictPortfolioConfig> lstdistrictPortfolioConfig = districtPortfolioConfigDAO.getPortfolioConfig("E",jobOrder);
					DistrictPortfolioConfig districtPortfolioConfig=null;
					if(lstdistrictPortfolioConfig!=null && lstdistrictPortfolioConfig.size()>0)
					{
						districtPortfolioConfig=lstdistrictPortfolioConfig.get(0);
					}
					boolean isDspqReqForKelly=false;
					boolean isKelly=false;
					if(districtPortfolioConfig!=null)
					{					
						if(jobOrder!=null && jobOrder.getHeadQuarterMaster()!=null)
						{						
							isKelly=true;
							isDspqReqForKelly=jobOrder.getJobCategoryMaster().getOfferDSPQ();
						}
					}
					
					map.addAttribute("isKelly", isKelly);
					map.addAttribute("isDspqReqForKelly", isDspqReqForKelly);
					
					boolean isKellyJobApply = false;
					isKellyJobApply = jobForTeacherDAO.getJFTByTeachers(teacherDetail);
					if(request.getServerName().contains("kelly") || request.getServerName().toLowerCase().contains("smartpractice") || request.getRequestURL().toString().contains("smartpractice")){
						isKelly = true;
					}
					if(isKellyJobApply || isKelly)
						map.addAttribute("isKellyUser", true);
					else
						map.addAttribute("isKellyUser", false);
				}catch(Exception e){
					e.printStackTrace();
				
				}
				System.out.println("staffTypeSession    "+staffTypeSession);
				System.out.println("dspqFlag::::::::"+dspqFlag);
				System.out.println("getQQ::::::::"+getQQ);
				map.addAttribute("dspqFlag", dspqFlag);
				map.addAttribute("getQQ", getQQ);
				map.addAttribute("staffTypeSession", staffTypeSession);
				
				map.addAttribute("isMiami", isMiami);
				if(districtMaster!=null)
				map.addAttribute("districtMasterDSPQ", districtMaster);
				map.addAttribute("teacherIdForDSPQ", teacherDetail);
				if(districtMaster!=null)
				{
					map.addAttribute("districtMaster", districtMaster);
				}
				
				List<RaceMaster> listRaceMasters = null;
				listRaceMasters = raceMasterDAO.findAllRaceByOrder();
				map.addAttribute("listRaceMasters", listRaceMasters);
				
				List<EthnicOriginMaster> lstethnicOriginMasters = null;
				List<EthinicityMaster> lstEthinicityMasters = null;

				lstEthinicityMasters=ethinicityMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
				lstethnicOriginMasters=ethnicOriginMasterDAO.findByCriteria(Restrictions.eq("status", "A"));

				System.out.println(" lstEthinicityMasters "+lstEthinicityMasters.size()+" lstethnicOriginMasters "+lstethnicOriginMasters.size());

				map.addAttribute("lstEthinicityMasters", lstEthinicityMasters);
				map.addAttribute("lstethnicOriginMasters", lstethnicOriginMasters);
				
				// For Work Experience
				Criterion criterion = Restrictions.eq("status", "A");
				List<FieldMaster> lstFieldMaster = fieldMasterDAO.findByCriteria(Order.asc("fieldName"),criterion);

				Criterion criterionEmpRTstatus = Restrictions.eq("status", "A");
				List<EmpRoleTypeMaster> lstEmpRoleTypeMaster = empRoleTypeMasterDAO.findByCriteria(criterionEmpRTstatus);

				List<PeopleRangeMaster> lstPeopleRangeMasters = peopleRangeMasterDAO.findAll();

				Criterion criterionOrg = Restrictions.eq("status", "A");
				List<OrgTypeMaster> lstOrgTypeMasters = orgTypeMasterDAO.findByCriteria(Order.asc("orgType"),criterionOrg);

				List<CurrencyMaster> lstCurrencyMasters = currencyMasterDAO.findAllCurrencyByOrder();

				//map.addAttribute("teacherExperience", teacherExperience);
				map.addAttribute("lstMonth", Utility.getMonthList());
				map.addAttribute("lstYear", Utility.getLasterYearByYear(1955));
				map.addAttribute("lstFieldMaster", lstFieldMaster);
				map.addAttribute("lstEmpRoleTypeMaster", lstEmpRoleTypeMaster);

				map.addAttribute("lstPeopleRangeMasters", lstPeopleRangeMasters);
				map.addAttribute("lstOrgTypeMasters", lstOrgTypeMasters);
				map.addAttribute("lstCurrencyMasters", lstCurrencyMasters);
				
				List<CountryMaster> countryMasters=new ArrayList<CountryMaster>();
				countryMasters=countryMasterDAO.findAllActiveCountry();
				map.addAttribute("listCountryMaster", countryMasters);
				map.addAttribute("lstGenderMasters", lstGenderMasters);
				System.out.println(" lstFieldMaster :: "+lstFieldMaster.size());

				String teacherAssessmentStatus=teacherAssessmentStatusDAO.epiStatus(teacherDetail);	
				System.out.println("teacherAssessmentStatus "+teacherAssessmentStatus);
				map.addAttribute("epistatus", teacherAssessmentStatus);
				
				List<TeacherVideoLink> teacherVideoLink=linksDAO.findTeachByVideoLink(teacherDetail);
				map.addAttribute("videoProfile", teacherVideoLink);	
				
				List<TeacherCertificate> teacherCertificates=certificateDAO.findCertificateByTeacher(teacherDetail);
				map.addAttribute("teacherCertificate", teacherCertificates);	
				
				Integer jobForTeachers=jobForTeacherDAO.findJobByTeacherOneYear_Op(teacherDetail);
				map.addAttribute("jobsApplied", jobForTeachers);
				
				List<TeacherDetail> teacherDetails = teacherDetailDAO.findByEmail(teacherDetail.getEmailAddress());
				if(teacherDetails!=null && teacherDetails.size()>0)
				{
					Boolean status = false;
					Boolean verify = false;
					status = teacherDetails.get(0).getStatus().equalsIgnoreCase("A")?true:false;
					verify = teacherDetails.get(0).getVerificationStatus()==1?true:false;
					if(verify && status)
						map.addAttribute("activation",true);
					else
						map.addAttribute("activation",false);
				}
				
				boolean  isAlsoSA=false;
				if(session != null && (session.getAttribute("isAlsoSA")!=null))
				{
					isAlsoSA = (Boolean)session.getAttribute("isAlsoSA");
				}
				map.addAttribute("isAlsoSA",isAlsoSA);
				
	//mukesh code for Internal movement of candidate
				boolean  isImCandidate=false;
				DistrictMaster districtMasterIM=null;
				if(session != null && (session.getAttribute("isImCandidate")!=null))
				{
					isImCandidate = (Boolean)session.getAttribute("isImCandidate");
					districtMasterIM =(DistrictMaster) session.getAttribute("districtIdForImCandidate");
				}
				map.addAttribute("isImCandidate",isImCandidate);
				
				//List<UserMaster> listUserMasters =userMasterDAO.findByEmail(teacherDetail.getEmailAddress().trim());
				String districtOrSchoolName="";
				if(districtMasterIM!=null){
					districtOrSchoolName=districtMasterIM.getDistrictName();
				}
				map.addAttribute("districtOrSchoolName",districtOrSchoolName+"'s");
				int year = Calendar.getInstance().get(Calendar.YEAR);
				String selectionyear=year+"-"+(year+1);
				map.addAttribute("selectionyear",selectionyear);
				
				//for Logo
				String source = "";
				String target = "";
				String path = "";
				try{
					if(districtMasterIM!=null){
						source = Utility.getValueOfPropByKey("districtRootPath")+districtMasterIM.getDistrictId()+"/"+districtMasterIM.getLogoPath();
						target = servletContext.getRealPath("/")+"/"+"/district/"+districtMasterIM.getDistrictId()+"/";
						File sourceFile = new File(source);
						File targetDir = new File(target);
						if(!targetDir.exists())
							targetDir.mkdirs();
						File targetFile = new File(targetDir+"/"+sourceFile.getName());
						if(sourceFile.exists())
						{
							FileUtils.copyFile(sourceFile, targetFile);
							path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+districtMasterIM.getDistrictId()+"/"+sourceFile.getName();
						}
					}
				  map.addAttribute("logoPathSSO", path);	
				}catch (Exception e) {
					e.printStackTrace();
				}
				if(!Utility.existsURL(path)){
					path = "images/dashboard-icon.png";
					map.addAttribute("logoPathSSO", path);
				}
				map.addAttribute("logoPathSSO", path);
				
				
				String sJAFExternalRegistered = (String)(session.getAttribute("newjafexternal")==null?"0":session.getAttribute("newjafexternal"));
				System.out.println("JAFExternal "+sJAFExternalRegistered);
				map.addAttribute("JAFExternal", sJAFExternalRegistered);
				//session.setAttribute("newjafexternal", "0");
				
				try{
					List withdrawnReasonMasterList = new ArrayList();
					withdrawnReasonMasterList = withdrawnReasonMasterDAO.findAllWithdrawnReasonMaster(districtMasterIM);
					map.addAttribute("withdrawnReasonMaster",withdrawnReasonMasterList);
				}catch(Exception exception){
					exception.printStackTrace();
				}
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return "userdashboard_Op";
		}
		
		
}