package tm.controller.teacher;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.InternalTransferCandidates;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.Jobrequisitionnumberstemp;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherLoginHistory;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherPreference;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersTempDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherLoginHistoryDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherPreferenceDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.StatusNodeMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.UserFolderStructureDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.MasterPasswordDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.services.social.SocialService;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

@Controller
public class QuestController {
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	@Autowired
	private StatusNodeMasterDAO statusNodeMasterDAO;
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	@Autowired
	private JobOrderDAO jobOrderDAO;
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	@Autowired
	private UserMasterDAO userMasterDAO;
	@Autowired
	private TeacherPreferenceDAO teacherPreferenceDAO;
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	@Autowired
	private TeacherLoginHistoryDAO teacherLoginHistoryDAO;
	@Autowired
	private UserFolderStructureDAO userFolderStructureDAO;
	@Autowired
	private MasterPasswordDAO masterPasswordDAO;
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	@Autowired
	private StateMasterDAO stateMasterDAO;
	@Autowired
	private SocialService socialService;
	@Autowired
	private EmailerService emailerService;
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	@Autowired
	private JobRequisitionNumbersTempDAO jobrequisitionnumberstempDao;
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void SchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}

	public void initBinder(HttpServletRequest request, DataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, true, 10));
	}

	@RequestMapping(value = "/quest.do", method = RequestMethod.GET)
	public String quest(ModelMap map, HttpServletRequest request) {
		request.getSession();
		String success = request.getParameter("success");
		System.out.println(" Success :: "+success);
		if (success != null) {
			map.addAttribute("status", success);
			map.addAttribute("districtsignup","districtsignup");
		}

		try {
			// Utility.setRefererURL(request);

			Cookie[] cookies = request.getCookies(); // request is an instance
			// of type
			// HttpServletRequest
			boolean foundCookie = false;
			String teacheremail = null;
			String uteacheremail = null;
			if (cookies != null)
				for (int i = 0; i < cookies.length; i++) {
					Cookie c = cookies[i];
					if (c.getName().equals("qteacheremail")) {

						teacheremail = c.getValue();
						System.out.println("email is" + teacheremail);
						map.addAttribute("qemailAddress", teacheremail);

					}

					if (c.getName().equals("uteacheremail")) {

						uteacheremail = c.getValue();
						System.out.println("uemail" + uteacheremail);
						map.addAttribute("uemailAddress", uteacheremail);

					}

				}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("quest.do");
		return "quest";
	}

	@RequestMapping(value="/aboutus.do" ,method=RequestMethod.GET)
	public String aboutUs(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		request.getSession();
		map.addAttribute("aboutus","aboutus");
		map.addAttribute("resources","");
		map.addAttribute("sitemap", "");
		System.out.println("aboutus.....do");
		return "aboutus";
	}
	@RequestMapping(value="/intheq.do" ,method=RequestMethod.GET)
	public String questResources(ModelMap map, HttpServletRequest request,HttpServletResponse response) {			
		System.out.println("intheq.....do");		
		try {

			HttpSession session = request.getSession();			
			if (session == null || session.getAttribute("teacherDetail")==null) {				
						
				return "redirect:meetyourmentor.do";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "intheq";
	}
	/*
	@RequestMapping(value = "/powerprofile.do", method = RequestMethod.GET)
    public String powerprofile(ModelMap map, HttpServletRequest request) {        

          try {

                HttpSession session = request.getSession();                 
                if (session.getAttribute("teacherDetail")==null) {                        
                      return "redirect:quest.do";
                }

          } catch (Exception e) {
                e.printStackTrace();
          }

          System.out.println("powerprofile.do");
          return "powerprofile";
    }

    @RequestMapping(value = "/powerprofileindex.do", method = RequestMethod.GET)
    public String powerprofileindex(ModelMap map, HttpServletRequest request) {         

          try {        

        	  HttpSession session = request.getSession();                 
              if (session.getAttribute("teacherDetail")!=null) {                        
                    return "redirect:powerprofile.do";
              }

          } catch (Exception e) {
                e.printStackTrace();
          }

          System.out.println("powerprofileindex.do");
          return "powerprofileindex";
    }*/

	/*@RequestMapping(value="/powerprofile.do" ,method=RequestMethod.GET)
	public String powerProfile(ModelMap map)
	{
		map.addAttribute("aboutus","aboutus");
		map.addAttribute("resources","");
		map.addAttribute("sitemap", "");
		System.out.println("aboutus.....do");

		map.addAttribute("resources", "resources");		
		System.out.println("powerprofile.....do");
		return "powerprofile";
	}*/

	/*
	 * @RequestMapping(value="/signin1.do", method=RequestMethod.GET) public
	 * String doSignInGET(ModelMap map,HttpServletRequest request) {
	 * System.out.println("/signin.do GET"); try { Cookie[] cookies =
	 * request.getCookies(); // request is an instance of type
	 * HttpServletRequest boolean foundCookie = false; String teacheremail =
	 * null;
	 * 
	 * if(cookies!=null) for(int i = 0; i < cookies.length; i++) { Cookie c =
	 * cookies[i]; if (c.getName().equals("teacheremail")) { teacheremail=
	 * c.getValue(); map.addAttribute("emailAddress", teacheremail); break; } }
	 * } catch (Exception e) { e.printStackTrace(); } return "quest"; }
	 */
	@RequestMapping(value = "/teacherhome.do", method = RequestMethod.POST)
	public String powertracker(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String emailAddress = request.getParameter("emailAddress") == null ? ""
					: request.getParameter("emailAddress").trim();
			String password = request.getParameter("password") == null ? ""
					: request.getParameter("password");

			password = MD5Encryption.toMD5(password);
			HttpSession session = null;
			session = request.getSession();
			int i = 0;
			String pageRedirect = "redirect:userdashboard.do";
			String flag = "epiFlag=on";
			List<TeacherDetail> teacherdetail = teacherDetailDAO
			.findByEmail(emailAddress);
			if (teacherdetail != null && teacherdetail.size() > 0) {
				session.setAttribute("lstMenuMaster", roleAccessPermissionDAO
						.getMenuListForTeacher(request));
				TeacherDetail teacherDetail = teacherdetail.get(0);
				try {
					TeacherLoginHistory teacherLoginHistory = new TeacherLoginHistory();
					teacherLoginHistory.setTeacherDetail(teacherDetail);
					teacherLoginHistory.setSessionId(session.getId());
					teacherLoginHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
					teacherLoginHistory.setLoginTime(new Date());
					teacherLoginHistoryDAO.makePersistent(teacherLoginHistory);
				} catch (Exception e) {
				}
				String loginType = teacherDetail.getUserType() == null ? ""
						: teacherDetail.getUserType();
				if (loginType.equals("R")) {
					try {
						Map<String, String> referer = (HashMap<String, String>) session
						.getAttribute("referer");
						if (referer != null) {
							String jobId = referer.get("jobId");
							JobOrder jobOrder = jobOrderDAO.findById(
									new Integer(jobId), false, false);
							List<JobForTeacher> lstJFT = jobForTeacherDAO
							.findJobByTeacher(teacherDetail);

							if (lstJFT != null && lstJFT.size() != 0) {
								if (!lstJFT.get(0).getJobId()
										.getDistrictMaster().getDistrictId()
										.equals(
												jobOrder.getDistrictMaster()
												.getDistrictId())) {
									teacherDetail.setUserType("N");
								}
							}
						} else {
							// if(!teacherDetail.getNoOfLogin().equals(0))
							// teacherDetail.setUserType("N");
						}

						Calendar c = Calendar.getInstance();
						c.setTime(teacherDetail.getCreatedDateTime());
						c.add(Calendar.DATE, 30);
						Date exclDate = c.getTime();
						Date currentDate = new Date();
						if (currentDate.compareTo(exclDate) > 0
								|| currentDate.compareTo(exclDate) == 0) {

							teacherDetail.setUserType("N");
						} else {

						}
					} catch (Exception e) {
						teacherDetail.setUserType("N");
						e.printStackTrace();
					}
				}

				String isAffilated = (String) (session
						.getAttribute("isAffilated") == null ? "0" : session
								.getAttribute("isAffilated"));
				String staffType = (String) (session.getAttribute("staffType") == null ? "N"
						: session.getAttribute("staffType"));
				String jobId = null;
				int isaffilatedstatus = 0;
				if (teacherDetail != null
						&& teacherDetail.getIsPortfolioNeeded()) {
					Map<String, String> referer = (HashMap<String, String>) session
					.getAttribute("referer");
					System.out.println("referer:: 111 " + referer);
					if (session.getAttribute("referer") != null) {
						jobId = referer.get("jobId");
						System.out.println("referer:: 222 " + referer);
						isaffilatedstatus = checkIsAffilated(request,
								teacherDetail, Integer.parseInt(jobId), Integer
								.parseInt(isAffilated));
						map.addAttribute("savejobFlag", 1); // savejobFlag is
						// only for saving
						// this job record
						// in job for
						// teacher table.
						// ok_cancelflag is the value isAffilated value for
						// latest applied job.
						map.addAttribute("ok_cancelflag", Integer
								.parseInt(isAffilated));
						map.addAttribute("jobId", Integer.parseInt(jobId));
						map.addAttribute("stp", staffType);
					}
				} else if (teacherDetail != null
						&& teacherDetail.getIsPortfolioNeeded().equals(false)
						&& session.getAttribute("referer") != null) {
					Map<String, String> referer = (HashMap<String, String>) session
					.getAttribute("referer");
					jobId = referer.get("jobId");
					JobOrder jobOrder = jobOrderDAO.findById(Integer
							.parseInt(jobId), false, false);
					if (jobOrder != null
							&& jobOrder.getIsPortfolioNeeded().equals(false)) {
					}
				}

				// ____________________________
				TeacherPreference preference = teacherPreferenceDAO
				.findPrefByTeacher(teacherDetail);
				System.out.println("forgot counter=====   "
						+ teacherDetail.getForgetCounter());
				/* ======= Checkin forgetCounter flag to restrict Teacher ===== */
				if (teacherDetail.getForgetCounter() < 3) {
					try {
						Map<String, String> referer = (HashMap<String, String>) session
						.getAttribute("referer");
						if (referer != null) {
							jobId = referer.get("jobId");
							JobOrder jobOrder = jobOrderDAO.findById(Integer
									.parseInt(jobId), false, false);

							if (jobOrder.getDistrictMaster().getDistrictId() == 1200390) {
								if (preference == null) {
									preference = new TeacherPreference();
									preference.setTeacherId(teacherDetail);
									preference.setGeoId("12");
									preference.setRegionId("8");
									preference.setSchoolTypeId("1");
									preference.setCreatedDateTime(new Date());
									teacherPreferenceDAO
									.makePersistent(preference);

									if (!jobId.trim().equals("")) {
										if (jobId.trim().length() > 0)
											if (Integer.parseInt(jobId) > 0) {
												JobForTeacher jobForTeacher = jobForTeacherDAO
												.findJobByTeacherAndJob(
														teacherDetail,
														jobOrder);
												if (jobForTeacher != null) {
													map.addAttribute(
															"pShowFlg", 1);
													map.addAttribute(
															"savejobFlag", 1);
												}
											}
									}
								}
							}
							try {
								if (jobOrder.getDistrictMaster()
										.getDistrictId() == 7800043) {
									if (preference == null) {
										preference = new TeacherPreference();
										preference.setTeacherId(teacherDetail);
										preference.setGeoId("12");
										preference.setRegionId("8");
										preference.setSchoolTypeId("1");
										preference
										.setCreatedDateTime(new Date());
										teacherPreferenceDAO
										.makePersistent(preference);

										if (!jobId.trim().equals("")) {
											if (jobId.trim().length() > 0)
												if (Integer.parseInt(jobId) > 0) {
													JobForTeacher jobForTeacher = jobForTeacherDAO
													.findJobByTeacherAndJob(
															teacherDetail,
															jobOrder);
													if (jobForTeacher != null) {
														map.addAttribute(
																"pShowFlg", 1);
														map.addAttribute(
																"savejobFlag",
																1);
													}
												}
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}

							try {
								if (jobOrder.getDistrictMaster()
										.getDistrictId() == 640740) {
									if (preference == null) {
										preference = new TeacherPreference();
										preference.setTeacherId(teacherDetail);
										preference.setGeoId("12");
										preference.setRegionId("8");
										preference.setSchoolTypeId("1");
										preference
										.setCreatedDateTime(new Date());
										teacherPreferenceDAO
										.makePersistent(preference);

										if (!jobId.trim().equals("")) {
											if (jobId.trim().length() > 0)
												if (Integer.parseInt(jobId) > 0) {
													JobForTeacher jobForTeacher = jobForTeacherDAO
													.findJobByTeacherAndJob(
															teacherDetail,
															jobOrder);
													if (jobForTeacher != null) {
														map.addAttribute(
																"pShowFlg", 1);
														map.addAttribute(
																"savejobFlag",
																1);
													}
												}
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}

						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (!teacherDetail.getIsPortfolioNeeded()) {
						session.setAttribute("epiStandalone", true);
						pageRedirect = "redirect:portaldashboard.do";
						if (isaffilatedstatus == 1)
							map.addAttribute("isaffilatedstatus",
									isaffilatedstatus);
					} else if (preference == null) {
						session.setAttribute("redirectTopref", true);
						pageRedirect = "redirect:userpreference.do";
					} else if ((preference.getGeoId() == null || preference
							.getGeoId().trim().equals(""))
							&& (preference.getRegionId() == null || preference
									.getRegionId().trim().equals(""))
									&& (preference.getSchoolTypeId() == null || preference
											.getSchoolTypeId().trim().equals(""))) {
						session.setAttribute("redirectTopref", true);
						pageRedirect = "redirect:userpreference.do";
					}
					if (checkBaseComplete(request, teacherDetail)) {
						List<JobForTeacher> lstJobForTeacher = null;
						lstJobForTeacher = jobForTeacherDAO
						.findIncompleteJoborderforwhichBaseIsRequired(
								teacherDetail, statusMasterDAO
								.findStatusByShortName("icomp"));
						if (lstJobForTeacher.size() > 0) /*
						 * ===== Total No of Job
						 * in current financial
						 * year whose status is
						 * incomplete ====
						 */
						{
							if (Integer.parseInt(isAffilated) == 0
									&& session.getAttribute("referer") != null) {
								Map<String, String> referer = (HashMap<String, String>) session
								.getAttribute("referer");
								jobId = referer.get("jobId");
								JobOrder jobOrder = jobOrderDAO.findById(
										Integer.parseInt(jobId), false, false);
								if (jobOrder != null
										&& jobOrder.getJobCategoryMaster()
										.getBaseStatus()) {
									map.addAttribute("epimsjstatus", "icomp");
								}
							} else {
								if (session.getAttribute("referer") == null
										&& Integer.parseInt(isAffilated) == 1) {
									map.addAttribute("epimsjstatus", "icomp");
								}
							}
						}

						if (teacherDetail.getIsPortfolioNeeded() == true) {
							TeacherPortfolioStatus portfolioStatus = null;
							portfolioStatus = teacherPortfolioStatusDAO
							.findPortfolioStatusByTeacher(teacherDetail);

							if (portfolioStatus != null
									&& portfolioStatus
									.getIsPersonalInfoCompleted()
									&& portfolioStatus
									.getIsAcademicsCompleted()
									&& portfolioStatus
									.getIsCertificationsCompleted()
									&& portfolioStatus
									.getIsExperiencesCompleted()
									&& portfolioStatus
									.getIsAffidavitCompleted()) {
								map.addAttribute("portfolioStatus", "");
								if (lstJobForTeacher.size() > 0) /*
								 * ===== Total
								 * No of Job in
								 * current
								 * financial
								 * year whose
								 * status is
								 * incomplete
								 * ====
								 */
								{
									if (Integer.parseInt(isAffilated) == 0
											&& session.getAttribute("referer") != null) {
										pageRedirect = pageRedirect + "?"
										+ flag;
										map.addAttribute("epimsjstatus",
										"icomp");
									} else {
										if (session.getAttribute("referer") == null) {
											pageRedirect = pageRedirect + "?"
											+ flag;
											map.addAttribute("epimsjstatus",
											"icomp");
										}
									}
								}
							} else {
								map.addAttribute("portfolioStatus", "required");
								pageRedirect = pageRedirect + "?" + flag;
								if (lstJobForTeacher.size() > 0) /*
								 * ===== Total
								 * No of Job in
								 * current
								 * financial
								 * year whose
								 * status is
								 * incomplete
								 * ====
								 */
								{
									if (Integer.parseInt(isAffilated) == 0
											&& session.getAttribute("referer") != null) {
										map.addAttribute("epimsjstatus",
										"icomp");
									} else {
										if (session.getAttribute("referer") == null) {
											map.addAttribute("epimsjstatus",
											"icomp");
										}
									}
								}

							}
							if (isaffilatedstatus == 1)
								map.addAttribute("isaffilatedstatus",
										isaffilatedstatus);

						}
					} else {
						if (teacherDetail.getIsPortfolioNeeded() == true) {
							TeacherPortfolioStatus portfolioStatus = null;
							portfolioStatus = teacherPortfolioStatusDAO
							.findPortfolioStatusByTeacher(teacherDetail);

							if (portfolioStatus != null
									&& portfolioStatus
									.getIsPersonalInfoCompleted()
									&& portfolioStatus
									.getIsAcademicsCompleted()
									&& portfolioStatus
									.getIsCertificationsCompleted()
									&& portfolioStatus
									.getIsExperiencesCompleted()
									&& portfolioStatus
									.getIsAffidavitCompleted()) {
							} else {
							}
						}

						if (isaffilatedstatus == 1)
							map.addAttribute("isaffilatedstatus",
									isaffilatedstatus);
					}
					teacherDetail
					.setNoOfLogin((teacherDetail.getNoOfLogin()) + 1);
					teacherDetail.setForgetCounter(0);
					teacherDetailDAO.makePersistent(teacherDetail);
					// / internal candiate check
					List<InternalTransferCandidates> itcList = internalTransferCandidatesDAO
					.getDistrictForTeacher(teacherDetail);
					if (itcList.size() > 0) {
						InternalTransferCandidates itcObj = itcList.get(0);
						if (itcObj.getDistrictMaster() != null) {
							session.setAttribute("districtIdForTeacher",
									Utility.encryptNo(itcObj
											.getDistrictMaster()
											.getDistrictId()));
						}
						if (itcList.get(0).isVerificationStatus()) {
							session.setAttribute("InterTeacherDetail", true);
						}
					}
					session.setAttribute("teacherDetail", teacherDetail);
					session.setAttribute("teacherName",	getTeacherName(teacherDetail));
					System.out.println("Name Set :: "+getTeacherName(teacherDetail));
					i = teacherdetail.get(0).getQuestCandidate();

					String txtRememberme = request
					.getParameter("txtRememberme");
					System.out.println(txtRememberme);
					if (txtRememberme != null && txtRememberme.equals("on")) {
						System.out.println(emailAddress);
						Cookie cookie = new Cookie("qteacheremail",
								emailAddress);
						cookie.setMaxAge(365 * 24 * 60 * 60);
						response.addCookie(cookie);
					}

					PrintWriter pw = response.getWriter();
					response.setContentType("text/html");
					pw
					.print("<script type=\"text/javascript\" language=\"javascript\">");
					pw.print("window.top.afterLogin(" + i + ");");
					pw.print("</script>");
					// return null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	public boolean checkBaseComplete(HttpServletRequest request,
			TeacherDetail teacherDetail) {
		System.out.println("=========Basic checkBaseComplete ========");

		boolean baseInvStatus = false;
		TeacherAssessmentStatus teacherBaseAssessmentStatus = null;
		teacherBaseAssessmentStatus = teacherAssessmentStatusDAO
		.findAssessmentStatusByTeacherForBase(teacherDetail);
		if (teacherBaseAssessmentStatus != null
				&& (teacherBaseAssessmentStatus.getStatusMaster()
						.getStatusShortName().equalsIgnoreCase("comp"))) {
			baseInvStatus = true;
		}

		if (baseInvStatus) {
			return false;
		} else if (teacherBaseAssessmentStatus != null
				&& teacherBaseAssessmentStatus.getStatusMaster()
				.getStatusShortName().equalsIgnoreCase("vlt")) {
			return false;
		} else if (teacherBaseAssessmentStatus != null
				&& teacherBaseAssessmentStatus.getStatusMaster()
				.getStatusShortName().equalsIgnoreCase("icomp")) {
			return true;
		} else {
			return true;
		}
	}

	/*
	 * @Author: Gagan
	 * 
	 * @Discription: It is used to check isAffilated check.
	 */
	public int checkIsAffilated(HttpServletRequest request,
			TeacherDetail teacherDetail, Integer jobId, Integer isAffilated) {
		System.out.println(" Basic checkIsAffilated ::::::::::::");

		HttpSession session = request.getSession(false);
		try {
			// System.out.println(" Teachedetail : Teacher Id  "+teacherDetail.getTeacherId());
			JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);

			String[] statuss = { "comp", "icomp", "vlt" };
			List<StatusMaster> lstStatusMasters = null;
			try {
				lstStatusMasters = statusMasterDAO
				.findStatusByShortNames(statuss);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// System.out.println(" ==== lstStatusMasters ===== "+lstStatusMasters.size());

			List<JobForTeacher> lstjobForTeacher = jobForTeacherDAO
			.findDistrictOrSchoolLatestAppliedJobByTeacher(
					teacherDetail, jobOrder.getDistrictMaster(),
					lstStatusMasters);
			if (lstjobForTeacher != null && lstjobForTeacher.size() > 0) {
				// System.out.println("\n\n\n Size : "+lstjobForTeacher.size()+"--------------------------------- db is affilated value  "+lstjobForTeacher.get((lstjobForTeacher.size()-1)).getIsAffilated()+" === Js is affilated value "+isAffilated+" Difference "+(lstjobForTeacher.get((lstjobForTeacher.size()-1)).getIsAffilated()-isAffilated)
				// );
				/*
				 * ========= checking Last Latest Job Applied == isAffilated
				 * value should be different ===================
				 */
				if ((lstjobForTeacher.get((lstjobForTeacher.size() - 1))
						.getIsAffilated() - isAffilated) != 0) {
					// System.out.println(" If condition Last Latest Job "+lstjobForTeacher.get((lstjobForTeacher.size()-1)).getJobId()+" District Id "+lstjobForTeacher.get((lstjobForTeacher.size()-1)).getJobId().getDistrictMaster().getDistrictId());
					return 1;
				} else {
					// System.out.println("Else condition");
					return 0;
				}
			} else {
				return 0;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	public String getTeacherName(TeacherDetail teacherDetail) {
		String fullName = "";

		String firstName = "";
		String lastName = "";
		firstName = teacherDetail.getFirstName();
		lastName = teacherDetail.getLastName();
		return fullName = firstName + " " + lastName;
	}

	@RequestMapping(value = "/teacherhome.do", method = RequestMethod.GET)
	public String powertrackerGET(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("teacherDetail") == null) {
			return "redirect:quest.do";
		} else {
			return "teacherhome";
		}
	}

	@RequestMapping(value = "logoutUser.do", method = RequestMethod.GET)
	public String Logout(HttpServletRequest request) {
		System.out.println("logout");
		HttpSession session = request.getSession(false);
		session.getAttribute("teacherDetail");
		if (session != null)
			session.invalidate();

		return "redirect:quest.do";
	}

	@RequestMapping(value = "quest1.do", method = RequestMethod.GET)
	public String signup1() {
		System.out.println("quest1..");
		return "quest1";
	}

	@RequestMapping(value = "/userhome.do", method = RequestMethod.POST)
	public String questUserPOST(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) {
		String savePassword = "";
		//System.out.println(" userhome.do");
		/*Code here for to identify that user is type 3 user.*/
		int i = 0;
		System.out.println("Quest Controller--->userhome.do POST is called");
		try {
			String emailAddress = request.getParameter("emailAddress") == null ? ""
					: request.getParameter("emailAddress").trim();
			String password = request.getParameter("password") == null ? ""
					: request.getParameter("password");

			password = MD5Encryption.toMD5(password);
			HttpSession session = null;
			session = request.getSession();
			UserMaster userMaster = null;

			List<UserMaster> userdetail = userMasterDAO.findByEmail(emailAddress);

			//Only for user entityType=3
			if(userdetail!=null && userdetail.size()>1 && userdetail.get(0).getEntityType()!=null && userdetail.get(0).getEntityType()==3)
			{
				long schoolId = request.getParameter("schoolID")==null?0:Long.parseLong(request.getParameter("schoolID"));
				String schoolName = request.getParameter("schoolName")==null?"":request.getParameter("schoolName");

				if(schoolId!=0 && !schoolName.equals(""))
					for(UserMaster master: userdetail)
						if(master.getSchoolId().getSchoolId()==schoolId)
						{
							userdetail.add(0, master);
							userMaster = master;
							break;
						}
			}//End of user entityType=3
			else
				if (userdetail.size() > 0)
					userMaster = userdetail.get(0);

			if (userMaster != null) {
				SchoolMaster schoolMaster = userMaster.getSchoolId();
				if (userMaster.getEntityType() == 2)
					userMaster.setSchoolId(null);

				session.setAttribute("userMaster", userMaster);
				session.setAttribute("lstMenuMaster", roleAccessPermissionDAO
						.getMenuList(request, userMaster));
				session.setAttribute("userName", getUserName(userMaster));
				if(userMaster.getIsQuestCandidate())
					i = 1;
			}

			String txtRememberme = request.getParameter("txtRememberme");
			System.out.println(txtRememberme);
			if (txtRememberme != null && txtRememberme.equals("on")) {
				System.out.println("email=" + emailAddress);
				Cookie cookie = new Cookie("uteacheremail", emailAddress);
				cookie.setMaxAge(365 * 24 * 60 * 60);
				response.addCookie(cookie);
			}

			/*PrintWriter pw = response.getWriter();
			response.setContentType("text/html");
			pw
			.print("<script type=\"text/javascript\" language=\"javascript\">");
			pw.print("window.top.afterUserLogin(" + i + ");");
			pw.print("</script>");*/

			if(i==1)
			{
				return "redirect:userhome.do";
			}
			else
			{
				return "redirect:tmdashboard.do";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@RequestMapping(value = "/nonclientsignin.do", method = RequestMethod.POST)
	public String nonClientSignInPost(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) {
		String savePassword = "";

		int i=0;
		String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
		String password = request.getParameter("password")==null?"":request.getParameter("password");

		String txtRememberme = request.getParameter("txtRememberme");
		if (txtRememberme != null && txtRememberme.equals("on")) {
			System.out.println("email=" + emailAddress);
			Cookie cookie = new Cookie("uteacheremail", emailAddress);
			cookie.setMaxAge(365 * 24 * 60 * 60);
			response.addCookie(cookie);
		}
		HttpSession session = null;
		session = request.getSession();
		UserMaster userMaster = null;

		List<UserMaster> userdetail = userMasterDAO.findByEmail(emailAddress);

		//Only for user entityType=3
		if(userdetail!=null && userdetail.size()>1 && userdetail.get(0).getEntityType()!=null && userdetail.get(0).getEntityType()==3)
		{
			long schoolId = request.getParameter("schoolID")==null?0:Long.parseLong(request.getParameter("schoolID"));
			String schoolName = request.getParameter("schoolName")==null?"":request.getParameter("schoolName");

			if(schoolId!=0 && !schoolName.equals(""))
				for(UserMaster master: userdetail)
					if(master.getSchoolId().getSchoolId()==schoolId)
					{
						userdetail.add(0, master);
						userMaster = master;
						break;
					}
		}//End of user entityType=3
		else
			if (userdetail.size() > 0)
				userMaster = userdetail.get(0);

		if (userMaster != null) {
			SchoolMaster schoolMaster = userMaster.getSchoolId();
			if (userMaster.getEntityType() == 2)
				userMaster.setSchoolId(null);

			session.setAttribute("userMaster", userMaster);
			session.setAttribute("lstMenuMaster", roleAccessPermissionDAO
					.getMenuList(request, userMaster));
			session.setAttribute("userName", getUserName(userMaster));
			if(userMaster.getIsQuestCandidate())
				i = 1;
		}

		if(i==1)
		{
			return "redirect:userhome.do";
		}
		else
		{
			return "redirect:tmdashboard.do";
		}
	}

	public String getUserName(UserMaster userMaster) {
		String fullName = "";
		String firstName = "";
		String lastName = "";
		firstName = userMaster.getFirstName();
		lastName = userMaster.getLastName();
		return fullName = firstName + " " + lastName;
	}

	@RequestMapping(value = "/userhome.do", method = RequestMethod.GET)
	public String questUserGET(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("Quest Controller--->userhome.do GET is called");
		System.out.println("sign in");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("userMaster") == null) {
			return "redirect:quest.do";
		} else {
			map.addAttribute("userhomes", "userhomes");
			return "userhome";
		}
	}

	/*@RequestMapping(value = "/questSignUp.do", method = RequestMethod.POST)
	public String questSignUpPOST(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("sign up controller");
		try {
			String nonclient = request.getParameter("nonclient");
			if(nonclient.equals("1"))
			{
				PrintWriter pw = response.getWriter();
				response.setContentType("text/html");
				pw
						.print("<script type=\"text/javascript\" language=\"javascript\">");
				pw.print("window.top.afterSignUpNonClient();");
				pw.print("</script>");
			}
			else if(nonclient.equals("0"))
			{
				PrintWriter pw = response.getWriter();
				response.setContentType("text/html");
				pw
						.print("<script type=\"text/javascript\" language=\"javascript\">");
				pw.print("window.top.afterSignUp();");
				pw.print("</script>");	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/questSignUp.do", method = RequestMethod.GET)
	public String questSignUpGET(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("sign up get");
		return "quest";

	}*/

	@RequestMapping(value = "/educationjob.do", method = RequestMethod.GET)
	public String educationJob(ModelMap map, HttpServletRequest request,HttpServletResponse response) {
		request.getSession();
		System.out.println("educationjob.....do");		
		map.addAttribute("educationjob","educationjob");		
		return "educationjob";
	}

	@RequestMapping(value = "/powerupeducation.do", method = RequestMethod.GET)
	public String powerupEducation(ModelMap map, HttpServletRequest request,HttpServletResponse response) {
		request.getSession();
		System.out.println("powerupeducation.....do");
		return "powerupeducation";
	}

	@RequestMapping(value = "/browse.do", method = RequestMethod.GET)
	public String browse(ModelMap map, HttpServletRequest request,HttpServletResponse response) {
		request.getSession();
		System.out.println("browse.do.....do");
		return "browse";
	}

	@RequestMapping(value = "/Quest-EPI.do", method = RequestMethod.GET)
	public String booststatus(ModelMap map, HttpServletRequest request,HttpServletResponse response) {
		request.getSession();
		System.out.println("booststatus.....do");
		return "booststatus";
	}

	@RequestMapping(value = "/manage.do", method = RequestMethod.GET)
	public String manage(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null) {
				return "redirect:quest.do";
			}
			UserMaster userMaster = (UserMaster) session
			.getAttribute("userMaster");
			DistrictMaster districtMaster = districtMasterDAO
			.findByDistrictId(userMaster.getDistrictId()
					.getDistrictId()
					+ "");

			if (districtMaster != null) {
				if (districtMaster.getJobApplicationCriteriaForProspects() == 1) {
					map.put("jobApplicationCriteria1", "checked");
				} else if (districtMaster
						.getJobApplicationCriteriaForProspects() == 2) {
					map.put("jobApplicationCriteria2", "checked");
				} else {
					map.put("jobApplicationCriteria3", "checked");
				}
			}
			map.addAttribute("manages", "manages");
			map.put("userMaster", userMaster);
		} catch (Exception e) {
		}
		return "manage";
	}

	@RequestMapping(value = "/jobs.do", method = RequestMethod.GET)
	public String jobs(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) {
		UserMaster userMaster = null;
		DistrictMaster districtMaster = null;
		int roleId = 0;
		try {
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null) {
				return "redirect:quest.do";
			} else {
				map.addAttribute("jobs", "jobs");
				userMaster = (UserMaster) session.getAttribute("userMaster");
			}
			map.addAttribute("DistrictOrSchoolName", userMaster.getDistrictId()
					.getDistrictName());
			map.addAttribute("DistrictOrSchoolId", userMaster.getDistrictId()
					.getDistrictId());
			String jobAuthKey = (Utility.randomString(8) + Utility
					.getDateTime());
			map.addAttribute("jobAuthKey", jobAuthKey);
			map.addAttribute("JobOrderType", "2");
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:quest.do";
		}

		return "jobs";
	}

	@RequestMapping(value="/editJobs.do",method=RequestMethod.GET)
	public String editJob(ModelMap map, HttpServletRequest request)
	{
		try 
		{
			UserMaster userMaster = null;
			HttpSession session = request.getSession(false);
			int roleId = 0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "redirect:index.jsp";
			} else {
				userMaster = (UserMaster) session.getAttribute("userMaster");
				if (userMaster.getRoleId().getRoleId() != null) {
					roleId = userMaster.getRoleId().getRoleId();
				}
			}	
			map.addAttribute("entityType", userMaster.getEntityType());
			map.addAttribute("userMaster", userMaster);
			int JobOrderType = 0;
			if (request.getParameter("JobOrderType") != null) {
				JobOrderType = Integer.parseInt(request
						.getParameter("JobOrderType"));
				map.addAttribute("JobOrderType", "2");
				System.out.println("JobOrderType====" + JobOrderType);
			}
			int JobId = 0;
			JobOrder jobOrder = new JobOrder();
			if(request.getParameter("JobId")!=null)
			{
				JobId = Integer.parseInt(request.getParameter("JobId"));
				map.addAttribute("JobId",JobId);			
				jobOrder = jobOrderDAO.findById(JobId, false, false);
				map.addAttribute("jobOrder",jobOrder);
			String baseURL=Utility.getValueOfPropByKey("basePath");
			String forMated=baseURL+"applyteacherjob.do?jobId="+JobId;
			String tinnyUrl =	Utility.getShortURL(forMated);
			map.addAttribute("bitly", tinnyUrl);	
			map.addAttribute("jobType", jobOrder.getJobType());
			map.addAttribute("jobStartDate",Utility.getCalenderDateFormart(jobOrder.getJobStartDate()+""));
			map.addAttribute("jobEndDate",Utility.getCalenderDateFormart(jobOrder.getJobEndDate()+""));
			map.addAttribute("jobDescription",jobOrder.getJobDescription());
			map.addAttribute("PageFlag","0");
			map.addAttribute("DistrictOrSchoolName", userMaster.getDistrictId().getDistrictName());
			map.addAttribute("DistrictOrSchoolId", userMaster.getDistrictId().getDistrictId());	
			map.addAttribute("SchoolId",0);
			if(jobOrder.getWritePrivilegeToSchool()){
				System.out.println("Write Privilege to School :"+jobOrder.getWritePrivilegeToSchool());
				map.addAttribute("allSchoolGradeDistrictVal",2);
			}else if(!jobOrder.getWritePrivilegeToSchool()){
				System.out.println("Write Privilege to School :"+jobOrder.getWritePrivilegeToSchool());
				map.addAttribute("allSchoolGradeDistrictVal",1);
				if(userMaster.getEntityType()==3 && JobOrderType==2){
					int noOfExpHires=schoolInJobOrderDAO.findNoOfExpHires(jobOrder,userMaster.getSchoolId(),0);
					if(noOfExpHires!=0){
						map.addAttribute("noOfExpHires",noOfExpHires);
					}else{
						map.addAttribute("noOfExpHires","");
					}
				}else{
					Criterion criterion = Restrictions.eq("jobId", JobId);
					List<JobOrder> jobOrders = jobOrderDAO.findByCriteria(criterion);
					if(jobOrders.get(0).getNoOfExpHires()!=null)
					{
						int noOfExpHires= jobOrders.get(0).getNoOfExpHires();
						if(noOfExpHires!=0){
							map.addAttribute("noOfExpHires",noOfExpHires);
						}else{
							map.addAttribute("noOfExpHires","");
						}
					}
				}
			}
			map.addAttribute("DistrictExitURL",jobOrder.getExitURL());
			List<StateMaster> listStateMaster = new ArrayList<StateMaster>();
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			map.addAttribute("listStateMaster", listStateMaster);

			/*List <JobCategoryMaster> jobCategoryMasters	= new ArrayList<JobCategoryMaster>();		
			Criterion criterion		= 	Restrictions.eq("status","A");
			jobCategoryMasters = jobCategoryMasterDAO.findByCriteria(criterion);
			map.addAttribute("jobCategoryMasters", jobCategoryMasters);*/
			List <JobCategoryMaster> jobCategoryMasters	= new ArrayList<JobCategoryMaster>();		
			Criterion criterion		= 	Restrictions.isNull("districtMaster");
			Criterion criterionHQ		= 	Restrictions.isNull("headQuarterMaster");
			jobCategoryMasters = jobCategoryMasterDAO.findByCriteria(org.hibernate.criterion.Order.asc("jobCategoryName"),criterion,criterionHQ);
			map.addAttribute("jobCategoryMasters", jobCategoryMasters);
			
			List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();
			System.out.println(" District Name :: "+userMaster.getDistrictId().getDistrictName()+"  &  District Id ::  " +userMaster.getDistrictId().getDistrictId());
			Criterion criterionD = Restrictions.eq("districtMaster", userMaster.getDistrictId());
			subjectMasters = subjectMasterDAO.findByCriteria(criterionD);
			map.addAttribute("subjectMasters", subjectMasters);
			}
			//SchoolInJobOrder schoolInJobOrder = new SchoolInJobOrder();
			//schoolInJobOrder = schoolInJobOrderDAO.findJobOrder(jobOrder);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "addjob";	
	}

	@RequestMapping(value = "/addjob.do", method = RequestMethod.GET)
	public String AddEditJobAction(ModelMap map, HttpServletRequest request) {
		try {
			UserMaster userMaster = null;
			HttpSession session = request.getSession(false);
			int roleId = 0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "redirect:index.jsp";
			} else {
				userMaster = (UserMaster) session.getAttribute("userMaster");
				if (userMaster.getRoleId().getRoleId() != null) {
					roleId = userMaster.getRoleId().getRoleId();
				}
			}
			map.addAttribute("entityType", userMaster.getEntityType());
			map.addAttribute("userMaster", userMaster);
			int JobOrderType = 0;
			if (request.getParameter("JobOrderType") != null) {
				JobOrderType = Integer.parseInt(request
						.getParameter("JobOrderType"));
				System.out.println("JobOrderType====" + JobOrderType);
			}
			map.addAttribute("JobId", 0);
			String roleAccess = null;
			try {
				if (JobOrderType == 2) {
					roleAccess = roleAccessPermissionDAO.getMenuOptionList(
							roleId, 27, "addeditjoborder.do", 1);
				} else {
					roleAccess = roleAccessPermissionDAO.getMenuOptionList(
							roleId, 28, "addeditjoborder.do", 2);
				}
			} catch (Exception e) {
				// e.printStackTrace();
			}
			try {
				String logType = "";
				if (JobOrderType == 2)
					logType = "Enter in add District Job Order";
				else
					logType = "Enter in add School Job Order";

				// userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new
				// Date(),logType);
			} catch (Exception e) {
				// e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);

			JobOrder jobOrder = new JobOrder();
			boolean miamiShowFlag = true;
			try {
				if (userMaster.getDistrictId().getDistrictId() == 1200390
						&& userMaster.getRoleId().getRoleId() == 3
						&& userMaster.getEntityType() == 3) {
					miamiShowFlag = false;
				}
			} catch (Exception e) {
			}
			map.addAttribute("miamiShowFlag", miamiShowFlag);
			if (userMaster.getEntityType() == 3 && JobOrderType == 2) {
				map.addAttribute("addJobFlag", false);
			} else {
				map.addAttribute("addJobFlag", true);
			}
			if (userMaster.getEntityType() == 2 && JobOrderType == 2) {
				try {
					map.addAttribute("DAssessmentUploadURL", userMaster
							.getDistrictId().getAssessmentUploadURL().trim());
				} catch (Exception e) {
					map.addAttribute("DAssessmentUploadURL", null);
				}
				map.addAttribute("SAssessmentUploadURL", "");
				map.addAttribute("DistrictOrSchoolName", userMaster
						.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId", userMaster
						.getDistrictId().getDistrictId());
				map.addAttribute("DistrictExitURL", userMaster.getDistrictId()
						.getExitURL());
				map.addAttribute("DistrictExitMessage", userMaster
						.getDistrictId().getExitMessage());
				map.addAttribute("districtHiddenId", userMaster.getDistrictId()
						.getDistrictId());
				map.addAttribute("SchoolName", null);
				map.addAttribute("SchoolId", 0);
				int exitURLMessageVal = 1;
				if (userMaster.getDistrictId().getFlagForMessage() != 0) {
					if (userMaster.getDistrictId().getFlagForMessage() == 1) {
						exitURLMessageVal = 2;
					}
				}
				map
				.addAttribute("DistrictExitURLMessageVal",
						exitURLMessageVal);
				map.addAttribute("PageFlag", "0");
				map.addAttribute("SchoolDistrictNameFlag", "1");

				DistrictMaster districtMaster = districtMasterDAO.findById(
						userMaster.getDistrictId().getDistrictId(), false,
						false);
				map.addAttribute("writePrivilegfordistrict", districtMaster
						.getWritePrivilegeToSchool());

			} else if (userMaster.getEntityType() == 3 && JobOrderType == 3) {
				try {
					map.addAttribute("DAssessmentUploadURL", userMaster
							.getSchoolId().getDistrictId()
							.getAssessmentUploadURL().trim());
				} catch (Exception e) {
					map.addAttribute("DAssessmentUploadURL", null);
				}
				try {
					map.addAttribute("SAssessmentUploadURL", userMaster
							.getSchoolId().getAssessmentUploadURL().trim());
				} catch (Exception e) {
					map.addAttribute("SAssessmentUploadURL", null);
				}
				map.addAttribute("DistrictOrSchoolName", userMaster
						.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId", userMaster
						.getDistrictId().getDistrictId());
				map.addAttribute("SchoolName", userMaster.getSchoolId()
						.getSchoolName());
				map.addAttribute("SchoolId", userMaster.getSchoolId()
						.getSchoolId());
				map.addAttribute("DistrictExitURL", userMaster.getSchoolId()
						.getExitURL());
				map.addAttribute("DistrictExitMessage", userMaster
						.getSchoolId().getExitMessage());
				map.addAttribute("districtHiddenId", userMaster.getSchoolId()
						.getDistrictId().getDistrictId());
				int exitURLMessageVal = 1;
				if (userMaster.getSchoolId().getFlagForMessage() != 0) {
					if (userMaster.getSchoolId().getFlagForMessage() == 1) {
						exitURLMessageVal = 2;
					}
				}
				map
				.addAttribute("DistrictExitURLMessageVal",
						exitURLMessageVal);
				map.addAttribute("PageFlag", "0");
				map.addAttribute("SchoolDistrictNameFlag", "1");
			} else {
				map.addAttribute("DistrictOrSchoolName", null);
				map.addAttribute("DistrictOrSchoolId", null);
				map.addAttribute("SchoolName", null);
				map.addAttribute("SchoolId", 0);
				map.addAttribute("PageFlag", "0");
				map.addAttribute("SchoolDistrictNameFlag", "0");
			}
			if (JobOrderType == 2) {
				map.addAttribute("districtRootPath", Utility
						.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath", Utility
						.getValueOfPropByKey("schoolRootPath"));
			} else if (JobOrderType == 3) {
				map.addAttribute("districtRootPath", Utility
						.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath", Utility
						.getValueOfPropByKey("schoolRootPath"));
			} else {
				map.addAttribute("districtRootPath", null);
				map.addAttribute("schoolRootPath", null);
			}

			map.addAttribute("allSchoolGradeDistrictVal", 1);
			map.addAttribute("JobOrderType", JobOrderType);
			map.addAttribute("jobOrder", jobOrder);
			map.addAttribute("attachJobAssessmentVal", 2);
			map.addAttribute("noOfExpHires", "");
			map.addAttribute("dateTime", Utility.getDateTime());
			String windowFunc = "if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc", windowFunc);

			/* === values to dropdown from jobCategoryMaster Table === */
			List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(userMaster.getDistrictId());
			map.addAttribute("jobCategoryMasterlst", jobCategoryMasterlst);

			if (userMaster.getDistrictId() != null) {
				List<SubjectMaster> subjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(userMaster.getDistrictId());
				map.addAttribute("subjectList", subjectMasters);
			}

			List<StateMaster> listStateMaster = new ArrayList<StateMaster>();
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			map.addAttribute("listStateMaster", listStateMaster);

			List <JobCategoryMaster> jobCategoryMasters	= new ArrayList<JobCategoryMaster>();		
			Criterion criterion		= 	Restrictions.isNull("districtMaster");
			Criterion criterionHQ		= 	Restrictions.isNull("headQuarterMaster");
			jobCategoryMasters = jobCategoryMasterDAO.findByCriteria(org.hibernate.criterion.Order.asc("jobCategoryName"),criterion,criterionHQ);
			map.addAttribute("jobCategoryMasters", jobCategoryMasters);
			
			// for subject list by district
			List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();
			System.out.println(" District Name :: "+userMaster.getDistrictId().getDistrictName()+"  &  District Id ::  " +userMaster.getDistrictId().getDistrictId());
			Criterion criterionD = Restrictions.eq("districtMaster", userMaster.getDistrictId());
			subjectMasters = subjectMasterDAO.findByCriteria(criterionD);
			map.addAttribute("subjectMasters", subjectMasters);
			/*
			 * ======== Gagan [Start] : mapping State Id to get auto select
			 * State acording to District ===============
			 */
			if (userMaster.getEntityType() == 2) {
				map.addAttribute("stateId", userMaster.getDistrictId()
						.getStateId().getStateId());
				map.addAttribute("districtReqNumFlag", userMaster
						.getDistrictId().getIsReqNoRequired());
			} else if (userMaster.getEntityType() == 3)
				map.addAttribute("stateId", userMaster.getSchoolId()
						.getStateMaster().getStateId());

			/*
			 * ======== Gagan [END] : mapping State Id to get auto select State
			 * acording to District ===============
			 */

			List<DistrictRequisitionNumbers> avlbList = new ArrayList<DistrictRequisitionNumbers>();
			List<JobRequisitionNumbers> attachedList = new ArrayList<JobRequisitionNumbers>();
			List<JobRequisitionNumbers> attachedSchoolList = new ArrayList<JobRequisitionNumbers>();
			StringBuffer schoolReqList = new StringBuffer();

			Criterion criterion1 = Restrictions.isNull("schoolMaster");
			Criterion criterion2 = Restrictions.isNotNull("schoolMaster");
			Criterion criterion3 = Restrictions.eq("districtMaster", userMaster
					.getDistrictId());
			Criterion criterion4 = Restrictions.eq("isUsed", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "addjob";
	}

	@RequestMapping(value = "/userchkpwdexist.do", method = RequestMethod.POST)
	public String isPasswordExistPOST(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			HttpSession session = request.getSession();
			PrintWriter out = response.getWriter();
			TeacherDetail teacherDetail = null;
			UserMaster userMaster = null;
			String savePassword = "";
			if (session == null
					|| (session.getAttribute("teacherDetail") == null && session
							.getAttribute("userMaster") == null)) {
				out.write("100001");
				return null;
			}
			if (session.getAttribute("userMaster") == null) {
				teacherDetail = (TeacherDetail) session
				.getAttribute("teacherDetail");
				savePassword = teacherDetail.getPassword();
			} else {
				userMaster = (UserMaster) session.getAttribute("userMaster");
				savePassword = userMaster.getPassword();
			}
			String encryptedPWD = null;
			String password = request.getParameter("password") == null ? ""
					: request.getParameter("password").trim();
			encryptedPWD = MD5Encryption.toMD5(password.trim());
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			System.out.println("::" + savePassword + ":::" + encryptedPWD);
			if (savePassword.equals(encryptedPWD)) {
				pw.println("1");
			} else {
				pw.println("0");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/checkschoolName.do", method = RequestMethod.POST)
	public String checkschoolName(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			HttpSession session = request.getSession();
			PrintWriter out = response.getWriter();
			if (session == null || session.getAttribute("userMaster") == null) {
				out.write("100001");
				return null;
			}
			String schoolName = request.getParameter("schoolname") == null ? ""
					: request.getParameter("schoolname").trim();
			System.out.println("schoolName====" + schoolName);
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			SchoolMaster schoolMaster = new SchoolMaster();
			schoolMaster = schoolMasterDAO.findByschoolName(schoolName);
			if (schoolMaster == null) {
				pw.println("0");
			} else {
				pw.println("1");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value="/resources.do" ,method=RequestMethod.GET)
	public String resources(ModelMap map, HttpServletRequest request)
	{
		request.getSession();
		map.addAttribute("aboutus","");
		map.addAttribute("resources", "resources");
		map.addAttribute("sitemap", "");
		System.out.println("resources.....do");
		return "resources";
	}

	@RequestMapping(value = "/talentsearch.do", method = RequestMethod.GET)
	public String talentsearch(ModelMap map, HttpServletRequest request) {
		request.getSession();
		System.out.println("talentsearch.....do");
		return "talentsearch";
	}

	@RequestMapping(value = "/rtalentsearch.do", method = RequestMethod.GET)
	public String rtalentsearch(ModelMap map, HttpServletRequest request) {
		request.getSession();
		System.out.println("rtalentsearch.....do");
		return "rtalentsearch";
	}

	@RequestMapping(value = "/powertracker.do", method = RequestMethod.GET)
	public String powertracker(ModelMap map, HttpServletRequest request) {
		request.getSession();
		System.out.println("powertracker.....do");
		return "powertracker";
	}

	@RequestMapping(value="/geomap.do" ,method=RequestMethod.GET)
	public String geomap(ModelMap map, HttpServletRequest request)
	{
		request.getSession();
		map.addAttribute("aboutus", "");
		map.addAttribute("resources", "");		
		map.addAttribute("geomap", "geomap");
		System.out.println("geomap.....do");
		return "geomap";
	}

	@RequestMapping(value = "/questusersettings.do", method = RequestMethod.GET)
	public String usersetting(ModelMap map, HttpServletRequest request) {
		HttpSession session = request.getSession(false);

		TeacherDetail teacherDetail = null;
		UserMaster userMaster = null;
		if (session == null
				|| (session.getAttribute("teacherDetail") == null && session
						.getAttribute("userMaster") == null)) {
			return "redirect:quest.do";
		}
		if (session.getAttribute("userMaster") == null) {
			teacherDetail = (TeacherDetail) session
			.getAttribute("teacherDetail");
			map.addAttribute("teacherDetail", teacherDetail);
		} else {
			userMaster = (UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userMaster", userMaster);

		}

		List<String> socialList = socialService
		.checkSocialConfigDetails(request);

		if (socialList.size() > 0) {
			if (socialList.contains("facebook"))
				map.addAttribute("facebook", "1");
			else
				map.addAttribute("facebook", "0");

			if (socialList.contains("twitter"))
				map.addAttribute("twitter", "1");
			else
				map.addAttribute("twitter", "0");

			if (socialList.contains("linkedIn"))
				map.addAttribute("linkedIn", "1");
			else
				map.addAttribute("linkedIn", "0");
		} else {
			map.addAttribute("facebook", "0");
			map.addAttribute("twitter", "0");
			map.addAttribute("linkedIn", "0");
		}
		map.addAttribute("appId", Utility.getValueOfPropByKey("appId"));
		map.addAttribute("redirectURL", Utility.getBaseURL(request));
		map.addAttribute("inAppId", Utility.getValueOfPropByKey("inAppId"));
		return "questusersettings";
	}

	@RequestMapping(value = "/questusersettings.do", method = RequestMethod.POST)
	// public String doPersonalInfoPOST(@ModelAttribute(value="teacherDetail")
	// TeacherDetail teacherDetail,BindingResult result,ModelMap
	// map,HttpServletRequest request)
	public String doPersonalInfoPOST(
			@ModelAttribute(value = "userMaster") UserMaster userMaster,
			BindingResult result, ModelMap map, HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null
				|| (session.getAttribute("teacherDetail") == null && session
						.getAttribute("userMaster") == null)) {
			return "redirect:quest.do";
		}
		UserMaster userDetailSession = null;
		if (session.getAttribute("userMaster") != null) {
			try {
				boolean sendMailEmailChange = false;
				String oldEmail = null;
				userDetailSession = (UserMaster) session
				.getAttribute("userMaster");
				oldEmail = userDetailSession.getEmailAddress();

				if (!userMaster.getEmailAddress().trim().equalsIgnoreCase(
						userDetailSession.getEmailAddress().trim())) {
					sendMailEmailChange = true;
				}

				userDetailSession.setFirstName(userMaster.getFirstName());
				userDetailSession.setLastName(userMaster.getLastName());
				userDetailSession.setEmailAddress(userMaster.getEmailAddress());

				// System.out.println("schoolid   "+userDetailSession.getSchoolId());
				UserMaster userSchoolId = userMasterDAO.findById(
						userDetailSession.getUserId(), false, false);
				userDetailSession.setSchoolId(userSchoolId.getSchoolId());

				userMasterDAO.makePersistent(userDetailSession);

				if (sendMailEmailChange) {
					emailerService.sendMailAsHTMLText(userMaster
							.getEmailAddress(),
							"Your TeacherMatch email has been changed",
							MailText.getUserSettingEmailChangeMailText(request,
									userMaster, oldEmail));
					emailerService.sendMailAsHTMLText(oldEmail,
							"Your TeacherMatch email has been changed",
							MailText.getUserSettingEmailChangeMailText(request,
									userMaster, oldEmail));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (session.getAttribute("teacherDetail") != null) {
			return "redirect:teacherhome.do";
		} else {
			return "redirect:userhome.do";
		}
	}

	@RequestMapping(value = "/manageschools.do", method = RequestMethod.GET)
	public String manageschools(ModelMap map, HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "redirect:quest.do";
		} else {
			map.addAttribute("manageschoolss", "manageschoolss");
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		map.addAttribute("DistrictOrSchoolId", userMaster.getDistrictId()
				.getDistrictId());
		System.out.println("manageschools.....do");
		return "manageschools";
	}

	@RequestMapping(value = "/checkSchoolId.do", method = RequestMethod.POST)
	public String checkSchoolId(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			HttpSession session = request.getSession();
			PrintWriter out = response.getWriter();
			if (session == null || session.getAttribute("userMaster") == null) {
				out.write("100001");
				return null;
			}
			String schoolId = request.getParameter("schoolId") == null ? ""
					: request.getParameter("schoolId").trim();
			System.out.println("schoolId====" + schoolId);
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			String tempId = session.getId();
			Jobrequisitionnumberstemp jobrequisitionnumberstemp = null;
			SchoolMaster schoolMaster = schoolMasterDAO.findById(Long
					.valueOf(schoolId), false, false);
			if (schoolMaster != null)
				jobrequisitionnumberstemp = jobrequisitionnumberstempDao
				.findBySchoolMasterAndTempId(schoolMaster, tempId);

			if (jobrequisitionnumberstemp == null) {
				pw.println("0");
			} else {
				pw.println("1");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/checkSchoolInTable.do", method = RequestMethod.POST)
	public String checkSchoolInTable(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			HttpSession session = request.getSession();
			PrintWriter out = response.getWriter();
			if (session == null || session.getAttribute("userMaster") == null) {
				out.write("100001");
				return null;
			}
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			String tempId = session.getId();
			Jobrequisitionnumberstemp jobrequisitionnumberstemp = null;

			jobrequisitionnumberstemp = jobrequisitionnumberstempDao
			.findByTempId(tempId);

			if (jobrequisitionnumberstemp == null) {
				pw.println("0");
			} else {
				pw.println("1");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/deleteSchoolForTempTable.do", method = RequestMethod.POST)
	public String deleteSchoolForTempTable(ModelMap map,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		try {
			HttpSession session = request.getSession();
			PrintWriter out = response.getWriter();
			if (session == null || session.getAttribute("userMaster") == null) {
				out.write("100001");
				return null;
			}
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			String tempId = session.getId();
			List<Jobrequisitionnumberstemp> jobrequisitionnumberstemp = null;
			jobrequisitionnumberstemp = jobrequisitionnumberstempDao
			.findByTempIdList(tempId);
			for (Jobrequisitionnumberstemp jobrequisitionnumberstemp2 : jobrequisitionnumberstemp) {
				jobrequisitionnumberstempDao
				.makeTransient(jobrequisitionnumberstemp2);
			}
			if (jobrequisitionnumberstemp == null) {
				pw.println("0");
			} else {
				pw.println("1");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/districtverified.do", method = RequestMethod.GET)
	public String districtverified(ModelMap map,HttpServletRequest request) {
		request.getSession();
		
		System.out.println("districtverified.....do");
		String email = request.getParameter("email");
		System.out.println("Mail Id: Recieved : "+email);
		List<UserMaster> userMasters = userMasterDAO.findByEmail(email);
		if(userMasters!=null && userMasters.size() > 0)
		{
			UserMaster activateUser = userMasters.get(0);
			activateUser.setStatus("A");
			userMasterDAO.updatePersistent(activateUser);
			map.addAttribute("verified", "verified");
			map.addAttribute("email", email);
			emailerService.sendMailAsHTMLText(activateUser.getEmailAddress(), " Welcome to TeacherMatch ",MailText.getRegistrationMailForQuestUser(request,activateUser));
		}
		else
		{
			map.addAttribute("verified", "not verified");	
		}
		return "districtverified";
	}

	@RequestMapping(value = "/questteacherlogin.do", method = RequestMethod.GET)
	public String questteacherlogin(ModelMap map,HttpServletRequest request) {
		request.getSession();
		System.out.println("questteacherlogin.....do");
		return "questteacherlogin";
	}

	@RequestMapping(value = "/questsignup.do", method = RequestMethod.POST)
	public String questsignupPOST(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("sign up controller");
		//String flag="0";
		try {

			//String success = request.getParameter("success");			
			//System.out.println(" Success Value :: "+success);
			/*if(success!=null && !success.isEmpty())
			{
				if(success.equals("0"))
				{
					flag = "1";
					return "redirect:quest.do?success="+flag;
				}
				else
				{
					return "redirect:quest.do?success="+flag;
				}
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:quest.do?success=0";
	}

	@RequestMapping(value = "/questsignup.do", method = RequestMethod.GET)
	public String questsignupGET(ModelMap map, HttpServletRequest request,
			HttpServletResponse response) {
		request.getSession();
		String userVal = request.getParameter("user");
		if(userVal==null || userVal.isEmpty())
		{
			return "quest";
		}
		else
		{
			map.addAttribute("userVal",userVal );
		}
		System.out.println("sign up get");
		return "questsignup";

	}

	@RequestMapping(value = "/meetyourmentor.do", method = RequestMethod.GET)
	public String meetyourmentor(ModelMap map, HttpServletRequest request) {
		request.getSession();
		System.out.println("meetyourmentor.....do");
		map.addAttribute("aboutus","");
		map.addAttribute("meetyourmentor", "meetyourmentor");
		map.addAttribute("sitemap", "");				
		return "meetyourmentor";
	}

	@RequestMapping(value = "/meetyourmentortab.do", method = RequestMethod.GET)
	public String meetyourmentortab(ModelMap map, HttpServletRequest request,HttpServletResponse response) {
		System.out.println("meetyourmentortab.....do");
		System.out.println("robynreview get");
		try {

			HttpSession session = request.getSession();			
			if (session == null || session.getAttribute("teacherDetail")==null) {				
						
				return "redirect:meetyourmentor.do";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		map.addAttribute("meetyourmentor", "meetyourmentor");
		return "meetyourmentortab";
	}


	@RequestMapping(value = "/powerprofile.do", method = RequestMethod.GET)
	public String powerprofile(ModelMap map, HttpServletRequest request) {        

		try {

			HttpSession session = request.getSession();                 
			if (session.getAttribute("teacherDetail")==null) {                        
				return "redirect:quest.do";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("powerprofile.do");
		return "powerprofile";
	}

	@RequestMapping(value = "/powerprofileindex.do", method = RequestMethod.GET)
	public String powerprofileindex(ModelMap map, HttpServletRequest request) {         

		try {        

			HttpSession session = request.getSession();                 
			if (session.getAttribute("teacherDetail")!=null) {                        
				return "redirect:powerprofile.do";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("powerprofileindex.do");
		return "powerprofileindex";
	}


	@RequestMapping(value={"/questcandidates.do","/questcandidatesnew.do"}, method=RequestMethod.GET)
	public String questcandidate(ModelMap map,HttpServletRequest request)
	{
		try {

			HttpSession session = request.getSession();			
			if (session == null || session.getAttribute("userMaster")==null) {				
				return "redirect:quest.do";
			}
			map.addAttribute("questcandidatess", "questcandidatess");
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("questcandidates.do.........");
		return "questcandidates";
	}

	@RequestMapping(value="/toolkitessentials.do",  method = RequestMethod.GET)
	public String toolkitessentials(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{	
		try {

			HttpSession session = request.getSession();			
			if (session == null || session.getAttribute("teacherDetail")==null) {				
							
				return "redirect:meetyourmentor.do";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("toolkitessentials.do.........");
		return "toolkitessentials";
	}

	@RequestMapping(value = "/candidate-sign-up.do", method = RequestMethod.POST)
	public String candidatesignupPOST(ModelMap map, HttpServletRequest request,HttpServletResponse response) {
		System.out.println("Candidate sign up controller");	
		return "candidatesignup";
	}

	@RequestMapping(value = "/candidate-sign-up.do", method = RequestMethod.GET)
	public String candidatesignupGET(ModelMap map, HttpServletRequest request,HttpServletResponse response) {
		System.out.println("candidate sign up get");
		request.getSession();
		return "candidatesignup";

	}
	@RequestMapping(value = "/employer-sign-up.do", method = RequestMethod.POST)
	public String districtsignupPOST(ModelMap map, HttpServletRequest request,HttpServletResponse response) {
		System.out.println("districtsignup sign up controller");	
		request.getSession();
		return "districtsignup";
	}

	@RequestMapping(value = "/employer-sign-up.do", method = RequestMethod.GET)
	public String districtsignupGET(ModelMap map, HttpServletRequest request,HttpServletResponse response) {		
		System.out.println("districtsignup sign up get");
		request.getSession();
		return "districtsignup";

	}
	
	@RequestMapping(value = "/questconnect.do", method = RequestMethod.GET)
	public String questconnectGET(ModelMap map, HttpServletRequest request,HttpServletResponse response) {		
		System.out.println("questconnect get");
		try {

			HttpSession session = request.getSession();			
			if (session == null || session.getAttribute("teacherDetail")==null) {
				
				return "redirect:meetyourmentor.do";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "questconnect";

	}
	
	@RequestMapping(value = "/review.do", method = RequestMethod.GET)
	public String robynreviewGET(ModelMap map, HttpServletRequest request,HttpServletResponse response) {		
		System.out.println("robynreview get");
		try {

			HttpSession session = request.getSession();			
			if (session == null || session.getAttribute("teacherDetail")==null) {				
						
				return "redirect:meetyourmentor.do";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		map.addAttribute("meetyourmentor", "meetyourmentor");
		return "robynreview";

	}
}
