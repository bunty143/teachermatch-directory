package tm.controller.teacher;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.TeacherDetail;
import tm.bean.TeacherLoginHistory;
import tm.bean.assessment.AssessmentGroupDetails;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.SpAssessmentConfig;
import tm.bean.master.DistrictMaster;
import tm.bean.master.StatusMaster;
import tm.bean.teacher.SpInboundAPICallRecord;
import tm.bean.user.UserMaster;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherLoginHistoryDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentGroupDetailsDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.SpAssessmentConfigDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.ExclusivePeriodMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.teacher.SpInboundAPICallRecordDAO;
import tm.dao.user.MasterPasswordDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.servlet.WorkThreadServlet;
import tm.utility.AESEncryption;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;


@Controller
public class SmartPracticesController {
	
	@Autowired
	private MessageToTeacherDAO messageToTeacherDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO){
		this.teacherDetailDAO = teacherDetailDAO;
	}
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO){
		this.userMasterDAO = userMasterDAO;
	}
	
	@Autowired
	private ExclusivePeriodMasterDAO exclusivePeriodMasterDAO;
	public void setExclusivePeriodMasterDAO(ExclusivePeriodMasterDAO exclusivePeriodMasterDAO){
		this.exclusivePeriodMasterDAO = exclusivePeriodMasterDAO;
	}
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO){
		this.jobOrderDAO = jobOrderDAO;
	}
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService){
		this.emailerService = emailerService;
	}
	
	@Autowired
	private TeacherLoginHistoryDAO teacherLoginHistoryDAO;
	public void setTeacherLoginHistoryDAO(TeacherLoginHistoryDAO teacherLoginHistoryDAO) {
		this.teacherLoginHistoryDAO = teacherLoginHistoryDAO;
	}
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	
	@Autowired 
	private MasterPasswordDAO masterPasswordDAO;
	
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private SpInboundAPICallRecordDAO spInboundAPICallRecordDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private AssessmentGroupDetailsDAO assessmentGroupDetailsDAO;
	
	@Autowired
	private SpAssessmentConfigDAO spAssessmentConfigDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	//Smart Practices Sign Up Controller method GET
	@RequestMapping(value="/smartpractices.do", method=RequestMethod.GET)
	public String slotSelection(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println("SmartPracticesController :: smartpractices.do GET");
		
		String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
		String msgError = request.getParameter("msgError")==null?"":request.getParameter("msgError");
		
		if(emailAddress!=null && emailAddress!=""){
			map.addAttribute("emailAddress", emailAddress);
		}
		if(msgError!=null && msgError!=""){
			map.addAttribute("msgError1", msgError);
		}
		TeacherDetail teacherDetail;
		String environment = "";
		try
		{
			Utility.setReferralURL(request);
			//Utility.setRefererURL(request);
			teacherDetail = new TeacherDetail();
			environment = Utility.getValueOfPropByKey("serverEnvironMent");
			map.addAttribute("teacherDetail", teacherDetail);
			map.addAttribute("login", "");
			map.addAttribute("popup",false);
			map.addAttribute("environment", environment);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return "smartpractices";
	}
	String locale = Utility.getValueOfPropByKey("locale");	 
	//Smart Practices Sign Up Controller method post
	@RequestMapping(value="/smartpractices.do", method=RequestMethod.POST)
	public String doSignUpPOST(@ModelAttribute(value="teacherDetail") TeacherDetail teacherDetail,BindingResult result, ModelMap map, HttpServletRequest request)
	{
		System.out.println("SmartPracticesController :: smartpractices.do POST");
		try 
		{
			List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(teacherDetail.getEmailAddress());
			List<UserMaster> lstUserMaster = userMasterDAO.findByEmail(teacherDetail.getEmailAddress());
			if(teacherDetail.getEmailAddress()==null || teacherDetail.getEmailAddress().trim().equals("")){
				map.addAttribute("msgError", "&#149; Please enter Email");
				map.addAttribute("teacherDetail", teacherDetail);				
				return "smartpractices.do";
			}
			else if(!Utility.validEmailForTeacher(teacherDetail.getEmailAddress())){
				map.addAttribute("msgError", "&#149; Please enter valid Email");
				map.addAttribute("teacherDetail", teacherDetail);				
				return "smartpractices.do";
			}
			else if((lstTeacherDetails!=null && lstTeacherDetails.size()>0) || (lstUserMaster!=null && lstUserMaster.size()>0)){
				map.addAttribute("msgError", "&#149; A Member has already registered with the email, you provided. Please provide another email address to register");
				map.addAttribute("teacherDetail", teacherDetail);				
				return "smartpractices.do";
			}
			else{
				Integer jobId = null;
				BranchMaster branchMaster = branchMasterDAO.findById(Integer.parseInt(request.getParameter("branchId")), false, false);
				List<JobOrder> jobOrderList1 = jobOrderDAO.getActiveBranchJobs(branchMaster,true);
				if(jobOrderList1!=null && jobOrderList1.size()>0)
					jobId = jobOrderList1.get(0).getJobId();
				if(jobId!=null && jobId>0){
					int authorizationkey=(int) Math.round(Math.random() * 2000000);
					HttpSession session = request.getSession();
					
					teacherDetail.setUserType("R");
					teacherDetail.setPassword(MD5Encryption.toMD5(teacherDetail.getPassword()));	
					teacherDetail.setFbUser(false);
					teacherDetail.setQuestCandidate(0);
					teacherDetail.setAuthenticationCode(""+authorizationkey);
					teacherDetail.setVerificationCode(""+authorizationkey);
					teacherDetail.setVerificationStatus(0);
					teacherDetail.setIsPortfolioNeeded(true);
					teacherDetail.setNoOfLogin(0);
					teacherDetail.setStatus("A");
					teacherDetail.setSendOpportunity(false);
					teacherDetail.setIsResearchTeacher(false);
					teacherDetail.setForgetCounter(0);
					teacherDetail.setCreatedDateTime(new Date());
					teacherDetail.setIpAddress(IPAddressUtility.getIpAddress(request));
					teacherDetail.setInternalTransferCandidate(false);
					teacherDetail.setSmartPracticesCandidate(1);
					teacherDetail.setAssociatedBranchId(branchMaster);
					String referralURL = session.getAttribute("referralURL")==null?"":(String)session.getAttribute("referralURL");
					teacherDetail.setReferralURL(referralURL);
											
					/*System.out.println("Password == "+teacherDetail.getPassword());
					System.out.println("FbUser == "+teacherDetail.getFbUser());
					System.out.println("QuestCandidate == "+teacherDetail.getQuestCandidate());
					System.out.println("AuthenticationCode == "+teacherDetail.getAuthenticationCode());
					System.out.println("VerificationCode == "+teacherDetail.getVerificationCode());
					System.out.println("VerificationStatus == "+teacherDetail.getVerificationStatus());
					System.out.println("IsPortfolioNeeded == "+teacherDetail.getIsPortfolioNeeded());
					System.out.println("NoOfLogin == "+teacherDetail.getNoOfLogin());
					System.out.println("Status == "+teacherDetail.getStatus());
					System.out.println("SendOpportunity == "+teacherDetail.getSendOpportunity());
					System.out.println("IsResearchTeacher == "+teacherDetail.getIsResearchTeacher());
					System.out.println("ForgetCounter == "+teacherDetail.getForgetCounter());
					System.out.println("CreatedDateTime == "+teacherDetail.getCreatedDateTime());
					System.out.println("IpAddress == "+teacherDetail.getIpAddress());
					System.out.println("InternalTransferCandidate == "+teacherDetail.getInternalTransferCandidate());
					System.out.println("SmartPracticesCandidate == "+teacherDetail.getSmartPracticesCandidate());
					System.out.println("AssociatedBranchId == "+teacherDetail.getAssociatedBranchId());*/
						
					teacherDetailDAO.makePersistent(teacherDetail);
	
					//saveJobForTeacher(request, teacherDetail);
					
					try{
						System.out.println("jobId :: "+jobId);
						JobOrder jobOrder = jobOrderList1.get(0); 
						JobForTeacher jobForTeacher = jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
						StatusMaster statusMaster = null;
						if(jobForTeacher==null)
						{
							jobForTeacher = new JobForTeacher();
							jobForTeacher.setTeacherId(teacherDetail);
							jobForTeacher.setJobId(jobOrder);
							try{
								int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
								statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
							}catch(Exception ex){
								statusMaster = WorkThreadServlet.statusMap.get("icomp");
							}
							jobForTeacher.setStatus(statusMaster);
							jobForTeacher.setStatusMaster(statusMaster);
							jobForTeacher.setCreatedDateTime(new Date());
							
							jobForTeacher.setLastActivity(statusMaster.getStatus());
							jobForTeacher.setLastActivityDate(new Date());
							if(statusMaster!=null){
								jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
							}
							List<UserMaster> userMasters= userMasterDAO.findByEmail("hq@teachermatch.com");
							try{
								if(userMasters==null || userMasters.size()==0){
									userMasters= userMasterDAO.getAllHeadquarterAdmins();
									if(userMasters.size()>0){
										jobForTeacher.setUserMaster(userMasters.get(0));
									}
								}
							}catch(Exception e){}
							jobForTeacherDAO.makePersistent(jobForTeacher);	
						}
						
						emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Welcome to "+branchMaster.getHeadQuarterMaster().getHeadQuarterName()+Utility.getLocaleValuePropByKey("msgAuthMailSubject", locale),MailText.getRegistrationMailForSmartPracticesNew(request,teacherDetail,branchMaster));
						
						/* Insert mail in log By Sekhar  */
						try{
								List<UserMaster> userMasters= userMasterDAO.findByEmail("hq@teachermatch.com");
								try{
									if(userMasters==null || userMasters.size()==0){
										userMasters= userMasterDAO.getAllHeadquarterAdmins();
									}
								}catch(Exception e){}
								MessageToTeacher  messageToTeacher= new MessageToTeacher();
								messageToTeacher.setTeacherId(teacherDetail);
								if(jobOrder!=null){
									messageToTeacher.setJobId(jobOrder);
								}
								messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
								messageToTeacher.setMessageSubject("Welcome to "+branchMaster.getHeadQuarterMaster().getHeadQuarterName()+Utility.getLocaleValuePropByKey("msgAuthMailSubject", locale));
								messageToTeacher.setMessageSend(MailText.getRegistrationMailForSmartPracticesNew(request,teacherDetail,branchMaster));
								messageToTeacher.setSenderId(userMasters.get(0));
								messageToTeacher.setSenderEmailAddress(userMasters.get(0).getEmailAddress());
								messageToTeacher.setEntityType(userMasters.get(0).getEntityType());
								messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
								messageToTeacher.setImportType("A");
								messageToTeacherDAO.makePersistent(messageToTeacher);
						}catch(Exception e){
							e.printStackTrace();
						}
						/* End mail in log by Sekhar */
						
						TeacherDetail teacherDetail2 = teacherDetailDAO.findByEmail(teacherDetail.getEmailAddress()).get(0);
						session.setAttribute("teacherDetail", teacherDetail2);
						session.setAttribute("lstMenuMaster", roleAccessPermissionDAO.getMenuListForTeacher(request));
						boolean isKellyJobApply = false;
						try{
							isKellyJobApply = jobForTeacherDAO.getJFTByTeachers(teacherDetail2);
						}catch (Exception e) {
							e.printStackTrace();
						}
						if(isKellyJobApply){
							System.out.println("isKellyJobApply >>>>>>>>>>>>>>>>>>>>>>::::::>>>>>>>>>>>>>>>>>>>>>> "+isKellyJobApply);
							session.setAttribute("isKelly", isKellyJobApply);
						}
					}catch (Exception e) {
						e.printStackTrace();
						
						String sEmailSubject ="SP SignUp Issue L0";
						String sEmailMsg = "";						
						sEmailMsg=sEmailMsg+"EmailAddress "+teacherDetail.getEmailAddress()+"<BR>";
						sEmailMsg=sEmailMsg+"BranchCode "+branchMaster.getBranchCode()+" "+branchMaster.getBranchId()+"<BR>";
						sEmailMsg=sEmailMsg+"jobId "+jobId+"<BR>";
						
						DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("amit.kumar@netsutra.com");
						dsmt.setMailto("amit.kumar@netsutra.com");
						dsmt.setMailsubject(sEmailSubject);
						dsmt.setMailcontent(sEmailMsg);
						try {
							dsmt.start();	
						} catch (Exception e1) {}
					}
					map.addAttribute("smartpractices", 1);
					map.addAttribute("popup",true);
				}
				else
				{
					String sEmailSubject ="SP SignUp Issue L1";
					String sEmailMsg = "";						
					sEmailMsg=sEmailMsg+"EmailAddress "+teacherDetail.getEmailAddress()+"<BR>";
					sEmailMsg=sEmailMsg+"BranchCode "+branchMaster.getBranchCode()+" "+branchMaster.getBranchId()+"<BR>";
					sEmailMsg=sEmailMsg+"jobId "+jobId+"<BR>";
					
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("amit.kumar@netsutra.com");
					dsmt.setMailto("amit.kumar@netsutra.com");
					dsmt.setMailsubject(sEmailSubject);
					dsmt.setMailcontent(sEmailMsg);
					try {
						dsmt.start();	
					} catch (Exception e) {}
				}
			}
			return "smartpractices";
			//return "redirect:thankyou.do";
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return null;
	}

	//Smart Practices Sign In Controller method post
	@RequestMapping(value="/smartpracticessignin.do", method=RequestMethod.POST)
	public String doTeacherSignInPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("SmartPracticesController :: smartpracticessignin.do POST");
		String emailAddress = request.getParameter("emailAddress1")==null?"":request.getParameter("emailAddress1").trim();
		String password = request.getParameter("password1")==null?"":request.getParameter("password1");
		response.setContentType("text/html");
		String txtRememberme = request.getParameter("txtRememberme");
		if(txtRememberme!=null && txtRememberme.trim().equals("on"))
		{
			Cookie cookie = new Cookie ("teacheremail",emailAddress);
			cookie.setMaxAge(365 * 24 * 60 * 60);
			response.addCookie(cookie);
		}
		PrintWriter pw = null;
		try {
			pw = response.getWriter();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		TeacherDetail teacherDetail = null;
		HttpSession session = null;
		String pageRedirect= "redirect:spuserdashboard.do";
		String key = null;
		String id = null;
		List<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
		String [] retVal = new String[5];
		System.out.println("emailAddress == "+emailAddress+" ::: password =="+password+" ::: pageRedirect == "+pageRedirect);
		try 
		{
			session = request.getSession();
			session.removeAttribute("epiStandalone");
			password = MD5Encryption.toMD5(password);

			//listTeacherDetails = teacherDetailDAO.getLogin(emailAddress,password);
			//listUserMaster = userMasterDAO.getLogin(emailAddress,password);

			System.out.println("Try to login for email Id is "+emailAddress);
			listTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
			if(listTeacherDetails!=null && listTeacherDetails.size() > 0)
			{
				System.out.println("Teacher is in our TeacherDB, TeacherId: "+listTeacherDetails.get(0).getTeacherId());
				if(!listTeacherDetails.get(0).getPassword().equalsIgnoreCase(password))
				{
					System.out.println("Default Teacher password is not matched");
					if(!masterPasswordDAO.isValidateUser("teacher", password))
					{
						listTeacherDetails=new ArrayList<TeacherDetail>();
						System.out.println("Master password for Teacher is not matched");
					}
					else
					{
						System.out.println("Master password for Teacher is matched");
					}
				}
				else
				{
					System.out.println("Default Teacher password is matched");
				}
			}
			else
			{
				System.out.println("Teacher Email "+emailAddress +" is not in our TeacherDB");
			}
			
			if(listTeacherDetails.size()>0)
			{
				//***  Session set for Menu List( Teacher ) populate ***//
				session.setAttribute("lstMenuMaster", roleAccessPermissionDAO.getMenuListForTeacher(request));
				teacherDetail = listTeacherDetails.get(0);
				
				try{
					//List<TeacherLoginHistory> teacherLoginHistorylst =teacherLoginHistoryDAO.findByTeacherLoginHistory(session.getId());
					//if(teacherLoginHistorylst.size())
					TeacherLoginHistory teacherLoginHistory = new TeacherLoginHistory();
					teacherLoginHistory.setTeacherDetail(teacherDetail);
					teacherLoginHistory.setSessionId(session.getId());
					teacherLoginHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
					teacherLoginHistory.setLoginTime(new Date());
					teacherLoginHistoryDAO.makePersistent(teacherLoginHistory);
				}catch(Exception e){e.printStackTrace();}

				key = request.getParameter("key")==null?"":request.getParameter("key");
				id = request.getParameter("id")==null?"":request.getParameter("id");
				
				if(teacherDetail.getVerificationStatus().equals(0))
				{
					map.addAttribute("authmail",false);
					teacherDetail.setPassword(password);
					map.addAttribute("emailAddress",emailAddress);
					map.addAttribute("msgError","You are unable to login since you have not authenticated your account. We perform authentication to help with your account security and identity protection. Please <a onclick='return divOn();' href='"+Utility.getBaseURL(request)+"authmailforsmartpractices.do?emailAddress="+emailAddress+"'>Click here</a> to receive a fresh authentication link to your registered email address. Please click on the authentication link received and then log into your account. Please note that the link is only active for 72 Hrs.");
					return "redirect:smartpractices.do";
				}
				else if(!teacherDetail.getStatus().equalsIgnoreCase("A"))
				{
					teacherDetail.setPassword(password);
					map.addAttribute("authmail",false);
					map.addAttribute("emailAddress",emailAddress);
					map.addAttribute("msgError","Your account is inactive.");
					return "redirect:smartpractices.do";
				}
				else
				{
					//======= Check forgetCounter flag to restrict Teacher ===== 
					if(teacherDetail.getForgetCounter()<3) 
					{
						teacherDetail.setNoOfLogin((teacherDetail.getNoOfLogin())+1);
						teacherDetail.setForgetCounter(0);
						teacherDetailDAO.makePersistent(teacherDetail);
						boolean isKellyJobApply = false;
						try{
							isKellyJobApply = jobForTeacherDAO.getJFTByTeachers(teacherDetail);
						}catch (Exception e) {
							e.printStackTrace();
						}
						if(isKellyJobApply){
							System.out.println("isKellyJobApply >>>>>>>>>>>>>>>>>>>>>>:::::: "+isKellyJobApply);
							session.setAttribute("isKelly", isKellyJobApply);
						}
						
						session.setAttribute("teacherDetail", teacherDetail);
						return "redirect:spuserdashboard.do?unit=01";
					}
					else
					{
						map.addAttribute("teacherblockflag", 1);
						return "redirect:smartpractices.do";
					}
				}
			}
			else
			{
				List<TeacherDetail> lstteacherEmail = null;
				TeacherDetail teacherDetail1 = null;
				int forgetCounter=0;
				//password = MD5Encryption.toMD5(password);
				lstteacherEmail = teacherDetailDAO.checkTeacherEmail(emailAddress);
				if(lstteacherEmail.size()>0)
				{
					try 
					{
						teacherDetail1=teacherDetailDAO.findById(lstteacherEmail.get(0).getTeacherId(), false, false);
						if(teacherDetail1.getForgetCounter()<3)
						{
							forgetCounter=(teacherDetail1.getForgetCounter()+1);
							teacherDetail1.setTeacherId(teacherDetail1.getTeacherId());
							teacherDetail1.setForgetCounter(forgetCounter);
							teacherDetailDAO.makePersistent(teacherDetail1);

							if(forgetCounter==3)
								map.addAttribute("teacherblockflag", 1);
						}
						else
						{
							map.addAttribute("teacherblockflag", 1);
						}
					}
					catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
				map.addAttribute("authmail",false);
				map.addAttribute("emailAddress", emailAddress);
				map.addAttribute("msgError","&#149; Invalid Email or Password. Please check the Email and Password and try again. Password is case sensitive.");
				return "redirect:smartpractices.do";
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		map.addAttribute("authmail",false);
		map.addAttribute("emailAddress", emailAddress);
		map.addAttribute("msgError","&#149; Invalid Email or Password. Please check the Email and Password and try again. Password is case sensitive.");
		return "redirect:smartpractices.do";
	}
	
	@RequestMapping(value="/authmailforsmartpractices.do", method=RequestMethod.GET)
	public String authorizationMail(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		request.getSession();
		String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress");
		List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
		if(lstTeacherDetails!=null)
			emailerService.sendMailAsHTMLText(lstTeacherDetails.get(0).getEmailAddress(), "Welcome to TeacherMatch, please confirm your account",MailText.getRegistrationMailForSmartPractices(request,lstTeacherDetails.get(0)));
		map.addAttribute("authmsg","We have sent you an email with login details and an authentication link.<br/><br/>Please check your registered email to authenticate.");
		map.addAttribute("authmail",true);
		return "redirect:smartpractices.do";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/spuserdashboard.do", method=RequestMethod.GET)
	public String doSignUpGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println("SmartPracticesController :: spuserdashboard.do GET");
		HttpSession session = request.getSession(false);
       
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:smartpractices.do";
		}
		String iFrameCallingURL="";	
		TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
		try
		{
			if(teacherDetail!=null)
			{
				String unit = request.getParameter("unit")==null?"":request.getParameter("unit").trim();
				if(unit==null || unit.equals("")){
					unit = "01";
				}
				System.out.println("teacherDetail == "+teacherDetail);
				map.addAttribute("teacherDetail", teacherDetail);
				
				Integer currentLessonNo=1;
				String lpm=null;
				/*
				*//**
				 * Date: Aug 27,2015
				 * Add By Ankit Sharma 
				 * For Add Uid paramater in spAssessment iFrame Url
				 * on the basis of candidate applied job and its configured jobcategory
				 * Start
				 *//*
				String unit = "";
				
				try{
					Integer spAssessmentId = 0;
					List<JobForTeacher> jft = jobForTeacherDAO.findBySmartPracticesAssessment(teacherDetail);
					if(jft!=null && jft.size()>0){
						for(JobForTeacher j : jft){
							if(j.getJobId().getJobCategoryMaster().getSpAssessmentId()!=null){
								spAssessmentId = j.getJobId().getJobCategoryMaster().getSpAssessmentId();
			*//**
			 * Retrieve spassessmentid: No reference was created in jobcategorymaster table because of 61 issue
			 *//*
								break;
							}
						}
					}
					if(spAssessmentId!=0){
						SpAssessmentConfig spAssessmentConfig = spAssessmentConfigDAO.findById(spAssessmentId, false, false);
						if(spAssessmentConfig!=null){
							unit = spAssessmentConfig.getAssessmentCode().split("=")[1];
						}
					}
					if(unit.length()==0){
						unit = "01";
					}
				}catch (Exception e) {
				}
				
				*//**
				 * End
				 *//*
				*/
				SpInboundAPICallRecord spInboundAPICallRecord = null;
				List<SpInboundAPICallRecord> spInboundAPICallRecordList = spInboundAPICallRecordDAO.getDetailsByTID(teacherDetail);
				if(spInboundAPICallRecordList==null || spInboundAPICallRecordList.size()==0)
				{
					spInboundAPICallRecord = new SpInboundAPICallRecord();
					spInboundAPICallRecord.setTeacherDetail(teacherDetail);
					if(unit!=null && !unit.equals("")){
						if(unit.equals("01")){
							spInboundAPICallRecord.setCurrentLessonNo(currentLessonNo);
						}
						else if(unit.equals("03")){
							spInboundAPICallRecord.setUnit03currentLessonNo(currentLessonNo);
						}
						else if(unit.equals("04")){
							spInboundAPICallRecord.setUnit04currentLessonNo(currentLessonNo);
						}
					}
					spInboundAPICallRecord.setIPAddress(IPAddressUtility.getIpAddress(request));
					spInboundAPICallRecord.setLastUpdateDate(new Date());
					spInboundAPICallRecordDAO.makePersistent(spInboundAPICallRecord);
					spInboundAPICallRecordList.add(spInboundAPICallRecord);
				}
				
				if(spInboundAPICallRecordList!=null && spInboundAPICallRecordList.size() >0)
				{
					spInboundAPICallRecord = spInboundAPICallRecordList.get(0);
					if(spInboundAPICallRecord!=null)
					{
						if(unit!=null && !unit.equals("")){
							if(unit.equals("01")){
								currentLessonNo = spInboundAPICallRecord.getCurrentLessonNo();
								lpm = spInboundAPICallRecord.getLpm();
							}
							else if(unit.equals("03")){
								currentLessonNo = spInboundAPICallRecord.getUnit03currentLessonNo();
								lpm = spInboundAPICallRecord.getUnit03lpm();
							}
							else if(unit.equals("04")){
								currentLessonNo = spInboundAPICallRecord.getUnit04currentLessonNo();
								lpm = spInboundAPICallRecord.getUnit04lpm();
							}
						}
						
						JSONObject jsonObject = new JSONObject();
		       			jsonObject.put("applicantId", spInboundAPICallRecord.getTeacherDetail().getTeacherId());
		       			jsonObject.put("emailAddress", Utility.encodeInBase64(teacherDetail.getEmailAddress()));
		       			if(unit!=null && !unit.equals("")){
							if(unit.equals("01")){
								jsonObject.put("currentLessonNo", spInboundAPICallRecord.getCurrentLessonNo());
				       			jsonObject.put("totalLessons", spInboundAPICallRecord.getTotalLessons());
							}
							else if(unit.equals("03")){
								jsonObject.put("currentLessonNo", spInboundAPICallRecord.getUnit03currentLessonNo());
				       			jsonObject.put("totalLessons", spInboundAPICallRecord.getUnit03TotalLessons());
							}
							else if(unit.equals("04")){
								jsonObject.put("currentLessonNo", spInboundAPICallRecord.getUnit04currentLessonNo());
				       			jsonObject.put("totalLessons", spInboundAPICallRecord.getUnit04TotalLessons());
							}
						}

		       			System.out.println("jsonObject.toString() "+jsonObject.toString());
		       			
		       			String encryptText = AESEncryption.encrypt(jsonObject.toString());
		       			String authKey = null;
		       			DistrictMaster districtMaster = districtMasterDAO.findById(1200390, false, false);
		       			authKey=districtMaster.getAuthKey();
		       			
		       			String assessmentGroupName=null;
		       			String encryptedGrpName = null;
		       			String assessmentType="3";
		       			String orgType="D";
		       			String fname="",lname="";
		       			
		       			List<AssessmentGroupDetails> assessmentGroupDetailsList = assessmentGroupDetailsDAO.getAllAssessmentGroupDetailsList(3, 1);
		       			if(assessmentGroupDetailsList!=null && assessmentGroupDetailsList.size() >0)
		       			{
		       				for(AssessmentGroupDetails assessmentGroupDetails : assessmentGroupDetailsList){
		       					if(unit!=null && !unit.equals("")){
									if(unit.equals("01")){
										if(assessmentGroupDetails.getGroupShortName()!=null && assessmentGroupDetails.getGroupShortName().equalsIgnoreCase("unit1")){
											assessmentGroupName = assessmentGroupDetails.getAssessmentGroupName();
											break;
										}
									}
									else if(unit.equals("03")){
										if(assessmentGroupDetails.getGroupShortName()!=null && assessmentGroupDetails.getGroupShortName().equalsIgnoreCase("unit3")){
											assessmentGroupName = assessmentGroupDetails.getAssessmentGroupName();
											break;
										}
									}
									else if(unit.equals("04")){
										if(assessmentGroupDetails.getGroupShortName()!=null && assessmentGroupDetails.getGroupShortName().equalsIgnoreCase("unit4")){
											assessmentGroupName = assessmentGroupDetails.getAssessmentGroupName();
											break;
										}
									}
								}
		       				}
		       				if(assessmentGroupName!=null)
		       					encryptedGrpName = Utility.encodeInBase64(assessmentGroupName);
		       			}
		       			
		       			JSONObject jsonObject1 = new JSONObject();
		       			jsonObject1.put("authKey", authKey);
		       			jsonObject1.put("orgType", orgType);
		       			jsonObject1.put("assessmentType", assessmentType);
		       			jsonObject1.put("grpName", encryptedGrpName);
		       			jsonObject1.put("candidateFirstName", teacherDetail.getFirstName());
		       			jsonObject1.put("candidateLastName", teacherDetail.getLastName());
		       			jsonObject1.put("candidateEmail", Utility.encodeInBase64(teacherDetail.getEmailAddress()));
		       			String encryptText1 = AESEncryption.encrypt(jsonObject1.toString());
						String invitationLink = Utility.getBaseURL(request)+"service/inventory.do?id="+encryptText1;
						
		       			if(teacherDetail.getFirstName()!=null && !teacherDetail.getFirstName().equals(""))
		       				fname=teacherDetail.getFirstName();
		       			if(teacherDetail.getLastName()!=null && !teacherDetail.getLastName().equals(""))
		       				lname=teacherDetail.getLastName();
		       			
		       			//iFrameCallingURL="https://smartpractices.teachermatch.org/01/dashboard.html?id="+encryptText;
						//iFrameCallingURL="https://smartpractices.teachermatch.org/dashboard.html?nav=0&uid="+currentLessonNo+"&lpm="+lpm+"&id="+encryptText+"&inventory_id="+invitationLink+"&fname="+fname+"&lname="+lname;
		       			//iFrameCallingURL="https://smartpractices.teachermatch.org/dashboard.html?nav=0&uid="+currentLessonNo+"&lpm="+lpm+"&id="+encryptText+"&inventory_id="+invitationLink+"&fname="+fname+"&lname="+lname;
		       			iFrameCallingURL="https://smartpractices.teachermatch.org/dashboard.html?nav=0&uid=1&unit="+unit+"&lpm="+lpm+"&id="+encryptText+"&inventory_id="+invitationLink+"&fname="+fname+"&lname="+lname;
		       			System.out.println("iFrameCallingURL "+iFrameCallingURL);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("iFrameCallingURL", iFrameCallingURL);
		return "spuserdashboard";	
	}
	
	@RequestMapping(value="/addBranchAndSignin.do")
	public @ResponseBody String addBranchAndSignin(@RequestParam String teacherId, @RequestParam String jobId , @RequestParam String branchId)
	{
		System.out.println("SmartPracticesController :: smartpracticessignin.do POST");
		 
		TeacherDetail teacherDetail = null;
		BranchMaster branchMaster = null;
		HttpSession session = null;
//		String pageRedirect= "redirect:spuserdashboard.do";
		String result = null;
		Integer tId = Integer.parseInt(teacherId);
		Integer bId = Integer.parseInt(branchId);
		teacherDetail =  teacherDetailDAO.findById(tId, false, false);
		branchMaster = branchMasterDAO.findById(bId, false, false);
			try 
			{ 
				if(teacherDetail!=null)
				{
					teacherDetail.setAssociatedBranchId(branchMaster);
					teacherDetail.setSmartPracticesCandidate(1);
					teacherDetailDAO.makePersistent(teacherDetail);
				}
				
				JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false); 
				JobForTeacher jobForTeacher = jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
				StatusMaster statusMaster =null;
				if(jobForTeacher==null)
				{
					jobForTeacher = new JobForTeacher();
					jobForTeacher.setTeacherId(teacherDetail);
					jobForTeacher.setJobId(jobOrder);
					try{
						int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
						statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
					}catch(Exception ex){
						statusMaster = WorkThreadServlet.statusMap.get("icomp");
					}
					jobForTeacher.setStatus(statusMaster);
					jobForTeacher.setStatusMaster(statusMaster);
					if(statusMaster!=null){
						jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
					}
					jobForTeacher.setCreatedDateTime(new Date());
					jobForTeacherDAO.makePersistent(jobForTeacher);	
				}
			 result = "ok";
			}catch (Exception e)
		 	{
				result = null;
				e.printStackTrace();
			}
			System.out.println("************** Result  = "+result);
		return result;
	}
	
	
}