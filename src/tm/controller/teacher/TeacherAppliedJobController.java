package tm.controller.teacher;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.Utility;

@Controller
public class TeacherAppliedJobController
{
	@Autowired
	private UserMasterDAO userMasterDAO;
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	@Autowired
	private JobOrderDAO jobOrderDAO;
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@RequestMapping(value="/candidatelist.do", method=RequestMethod.GET)
	public String teaccherAppliedJobList(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		try 
		{
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			String districtIdStr=request.getParameter("districtId");
			Integer districtId=null;
			DistrictMaster districtMaster=null;
			if(districtIdStr!=null){
				if(districtIdStr.length()>0){
					districtId=Integer.parseInt(districtIdStr);
					districtMaster=districtMasterDAO.findById(districtId, false, false);
				}
			}
			if(districtMaster!=null)
			{
				Criterion districtCr=Restrictions.eq("districtMaster", districtMaster); 
				List<JobOrder>jobOrdersList=jobOrderDAO.findByCriteria(districtCr);
				List<JobForTeacher>jobforTeachersLst=jobForTeacherDAO.findByCriteria(Restrictions.in("jobId", jobOrdersList));
				List<Integer>teacherIds=new ArrayList<Integer>();
				Map<Integer, TeacherDetail>teacherMap=new HashMap<Integer, TeacherDetail>();
				for(JobForTeacher jbt:jobforTeachersLst){
					teacherIds.add(jbt.getTeacherId().getTeacherId());
					teacherMap.put(jbt.getTeacherId().getTeacherId(), jbt.getTeacherId());
				}
				List<TeacherPersonalInfo>list=teacherPersonalInfoDAO.findByCriteria(Restrictions.in("teacherId", teacherIds));
				
				System.out.println(teacherMap.size() +"\tno of job of "+districtMaster.getDistrictName()+"\ttotal job :\t"+jobOrdersList.size()+" applied job :\t"+jobforTeachersLst.size()+"\t no of candidate :"+list.size());
				
				String filePath=Utility.getValueOfPropByKey("rootPath")+"/CandidateDetails.xls";
								
				WritableWorkbook workbook =  Workbook.createWorkbook(new File(filePath));
		        WritableSheet sheet = workbook.createSheet("CandidateList", 0);
				
				String Header[] = new String[5];
	            Header[0] = "Candidate ID";
	            Header[1] = "Email ID";
	            Header[2] = "Candidate Name";
	            Header[3] = "SSN";
	            Header[4] = "Employee#";
	 
	            WritableCellFormat cellFormat = new WritableCellFormat();
	            WritableFont wfobj=new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
	            WritableCellFormat cfobj=new WritableCellFormat(wfobj);
				cfobj.setWrap(true);
				
	            for (int i = 0; i < Header.length; i++) {
	                Label label = new Label(i, 0, Header[i],cfobj);
	                sheet.addCell(label);
	            }
	           
	            int counter=1;
	            for (TeacherPersonalInfo tp:list) {
	            	TeacherDetail teacherDetail=teacherMap.get(tp.getTeacherId());
	                Label label0 = new Label(0, counter, tp.getTeacherId()+"");
	                sheet.addCell(label0);
	                sheet.setColumnView(0, 15);
	                
	                Label label1 = new Label(1, counter,teacherDetail.getEmailAddress());
	                sheet.addCell(label1);
	                sheet.setColumnView(1, 40);
	                
	                String teacherName=tp.getMiddleName()!=null?tp.getFirstName()+" "+tp.getMiddleName()+" "+tp.getLastName():tp.getFirstName()+" "+tp.getLastName();
	                Label label2 = new Label(2, counter,teacherName);
	                sheet.addCell(label2);
	                sheet.setColumnView(2, 40);
	                
	                Label label3 = new Label(3, counter,Utility.decodeBase64(tp.getSSN()));
	                sheet.addCell(label3);
	                sheet.setColumnView(3, 20);
	                
	                Label label4 = new Label(4, counter,tp.getEmployeeNumber());
	                sheet.addCell(label4);
	                sheet.setColumnView(4, 20);
	                
	                counter++;
	            }
	            
	            workbook.write();
	            workbook.close();
	            
	            /*download file*/
	            try{
	            	File downloadFile = new File(filePath);
	            	if(downloadFile.exists()){
	            		FileInputStream inStream = new FileInputStream(downloadFile);
			             
			            response.setContentType("application/vnd.ms-excel");
			            response.setContentLength((int) downloadFile.length());
			             
			            String headerKey = "Content-Disposition";
			            String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
			            response.setHeader(headerKey, headerValue);
			             
			            OutputStream outStream = response.getOutputStream();
			            byte[] buffer = new byte[4096];
			            int bytesRead = -1;
			            while ((bytesRead = inStream.read(buffer)) != -1) {
			                outStream.write(buffer, 0, bytesRead);
			            }
			            inStream.close();
			            outStream.close();
	            	}else{
	            		System.out.println("Candidate applied job file not found .");
	            	}
	            }catch (Exception e) {
					e.printStackTrace();
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	 @RequestMapping(value="/importstatusdetails.do", method=RequestMethod.GET)
		public String teacherjobuploadGET(ModelMap map,HttpServletRequest request)
		{
			try{
				System.out.println("\n >=========== importstatusdetails.do Controller >>===============");
				HttpSession session = request.getSession(false);
				int roleId=0;
				UserMaster userMaster =null;
				if (session == null || session.getAttribute("userMaster") == null) 
				{
					return "redirect:index.jsp";
				}else{
					userMaster=	(UserMaster) session.getAttribute("userMaster");
					if(userMaster.getEntityType()==2){
					   map.addAttribute("districtId", userMaster.getDistrictId().getDistrictId());
					   map.addAttribute("entityType", userMaster.getEntityType());
					}else if(userMaster.getEntityType()==5 || userMaster.getEntityType()==6){
						   map.addAttribute("entityType", userMaster.getEntityType());
						   map.addAttribute("headQuarterId", userMaster.getHeadQuarterMaster().getHeadQuarterId());
						   if(userMaster.getEntityType()==6){
							   map.addAttribute("branchId", userMaster.getBranchMaster().getBranchId());
						   }
						}
					
					if(userMaster.getRoleId().getRoleId()!=null){
						roleId=userMaster.getRoleId().getRoleId();
					}
				}
			}catch (Exception e){
				e.printStackTrace();
			}
			return "statusupload";
		}
	    @RequestMapping(value="/statustemplist.do", method=RequestMethod.GET)
		public String teacherTempListGET(ModelMap map,HttpServletRequest request)
		{
			try{
				System.out.println("========== statustemplist.do Controller ========== ");
				HttpSession session = request.getSession(false);
				int roleId=0;
				UserMaster userMaster =null;
				if (session == null || session.getAttribute("userMaster") == null) 
				{
					return "redirect:index.jsp";
				}else{
					userMaster=	(UserMaster) session.getAttribute("userMaster");
					if(userMaster.getRoleId().getRoleId()!=null){
						roleId=userMaster.getRoleId().getRoleId();
					}
				}
				map.addAttribute("sessionIdTxt", session.getId());
			}catch (Exception e){
				e.printStackTrace();
			}
			return "statustemplist";
		}
}
