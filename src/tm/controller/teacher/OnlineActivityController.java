package tm.controller.teacher;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.teacher.OnlineActivityQuestionSet;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.teacher.OnlineActivityQuestionSetDAO;
import tm.utility.Utility;

@Controller
public class OnlineActivityController {
	@Autowired
	DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	OnlineActivityQuestionSetDAO onlineActivityQuestionSetDAO; 
	
	
	@RequestMapping(value="/onlineactivity.do", method=RequestMethod.GET)
	public String selectSchoolGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println(":::::::::::::::onlineactivity.do:::::::::::");
		try 
		{		
			String key = request.getParameter("key")==null?"":request.getParameter("key").trim();
			String keyValue=Utility.decodeBase64(key);
			System.out.println("keyValue:::>"+keyValue);
			String teacherId=keyValue.split("##")[0];
			String jobCategoryId=keyValue.split("##")[1];
			String districtId=keyValue.split("##")[2];
			
			JobCategoryMaster jobCategoryMaster=null;
			try{
				jobCategoryMaster=jobCategoryMasterDAO.findById(Integer.parseInt(jobCategoryId), false, false);
			}catch(Exception e){
				e.printStackTrace();
			}
			if(jobCategoryMaster!=null && jobCategoryMaster.getJobCategoryName().equalsIgnoreCase("Principal")){
				try{
					try{
						String questionSetId=keyValue.split("##")[3];
						System.out.println("questionSetId:: here::"+questionSetId);
						map.addAttribute("questionSetId",questionSetId);
					}catch(Exception e){
						DistrictMaster districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
						OnlineActivityQuestionSet onlineActivityQuestionSet=onlineActivityQuestionSetDAO.getOnlineActivityQuestionSet(districtMaster, jobCategoryMaster);
						if(onlineActivityQuestionSet!=null){
							System.out.println("questionSetId inside::::"+onlineActivityQuestionSet.getQuestionSetId());
							map.addAttribute("questionSetId",onlineActivityQuestionSet.getQuestionSetId());
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}else{
				try{
					String questionSetId=keyValue.split("##")[3];
					System.out.println("questionSetId::::"+questionSetId);
					map.addAttribute("questionSetId",questionSetId);
				}catch(Exception e){}
			}
			map.addAttribute("teacherId",teacherId);
			map.addAttribute("jobCategoryId",jobCategoryId);
			map.addAttribute("districtId",districtId);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "onlineactivity";
	}
}
