package tm.controller.teacher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.TeacherDetail;

@Controller
public class SchoolSelectionController {
	
	@RequestMapping(value="/selectschool.do", method=RequestMethod.GET)
	public String selectSchoolGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		try 
		{		
			System.out.println(":::::::::::::::selectschool.do:::::::::::");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "selectschool";
	}
	@RequestMapping(value="/addselectschool.do", method=RequestMethod.GET)
	public String addSelectSchoolGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		System.out.println(":::::::::::::::addselectschool.do:::::::::::");
		TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
		try 
		{		
			int jobId=0;
			if(request.getParameter("jobId")!=null){
				jobId=Integer.parseInt(request.getParameter("jobId"));
			}
			System.out.println("jobId:::::::"+jobId);
			map.addAttribute("jobId",jobId);
			if(teacherDetail!=null){
				map.addAttribute("teacherId",teacherDetail.getTeacherId());
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "addselectschool";
	}
}
