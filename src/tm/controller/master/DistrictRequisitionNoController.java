package tm.controller.master;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.user.UserMaster;
import tm.dao.user.UserMasterDAO;

@Controller
public class DistrictRequisitionNoController {
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	@RequestMapping(value="/managerequisitionno.do",method=RequestMethod.GET)
	public String DistrictRequisitionNoGet(ModelMap map,HttpServletRequest request){
		UserMaster userSession=null;
		try 
		{
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}

			userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			map.addAttribute("entityID", entityID);
			
			if(userSession.getEntityType()!=1){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("districtName",null);
			}
			
			if(userSession.getEntityType()==2){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		if(userSession.getEntityType()==3){
			return "signin";
		}else{
			return "managerequisitionno";
		}	
	}
}
