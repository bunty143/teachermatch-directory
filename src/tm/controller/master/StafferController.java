package tm.controller.master;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.user.UserMaster;

@Controller
public class StafferController {
	@RequestMapping(value="/addeditstaffer.do", method=RequestMethod.GET)
    public String onr(ModelMap map,HttpServletRequest request){

		UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		if (session == null
				|| (session.getAttribute("teacherDetail") == null && session.getAttribute("userMaster") == null)) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		 if(userMaster.getEntityType()==2){
				map.addAttribute("districtId",userMaster.getDistrictId().getDistrictId());
		 }		
		return "addeditstaffer";
    }
}
