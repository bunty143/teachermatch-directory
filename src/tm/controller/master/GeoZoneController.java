package tm.controller.master;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.python.modules.synchronize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.user.UserMaster;


@Controller
public class GeoZoneController 
{
	@RequestMapping(value="/geozonesetup.do", method=RequestMethod.GET)
	public String geozoneGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
		}
		session.setAttribute("displayType","0");
		geozonesetupmethod(map,userMaster);
		/*try{
			map.addAttribute("entityType",userMaster.getEntityType());
			if(userMaster.getEntityType()==2){
				map.addAttribute("DistrictMaster",userMaster.getDistrictId());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}*/
		return "geozonesetup";
	}
	
	public synchronized static void geozonesetupmethod(ModelMap map,UserMaster userMaster)
	{
		try{
			map.addAttribute("entityType",userMaster.getEntityType());
			if(userMaster.getEntityType()==2){
				map.addAttribute("DistrictMaster",userMaster.getDistrictId());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
