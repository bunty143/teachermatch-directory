package tm.controller.master;

/* @Author: Gagan 
* @Discription: view of Batch Job Order Controller.
*/
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.BatchJobOrder;
import tm.bean.JobOrder;
import tm.bean.UserLoginHistory;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.BatchJobOrderDAO;
import tm.dao.SchoolInBatchJobDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;
@Controller
public class BatchJobOrderController 
{
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	@Autowired
	private BatchJobOrderDAO batchJobOrderDAO;
	public void setBatchJobOrderDAO(BatchJobOrderDAO batchJobOrderDAO) {
		this.batchJobOrderDAO = batchJobOrderDAO;
	}
	@Autowired
	private SchoolInBatchJobDAO schoolInBatchJobDAO;
	public void setSchoolInBatchJobDAO(SchoolInBatchJobDAO schoolInBatchJobDAO) {
		this.schoolInBatchJobDAO = schoolInBatchJobDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}
	/*============ Get Method of AdminDashboard ====================*/
	@RequestMapping(value="/batchjoborder.do", method=RequestMethod.GET)
	public String batchJobOrderGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== dashboard.do in Batch Job Order Controller  ===============");
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,26,"batchjoborder.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("roleAccess="+roleAccess);
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("userSession", userMaster);
			if(userMaster.getEntityType()!=1){
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictOrSchoolName",null);
			}
			map.addAttribute("JobOrderType","2");
			if(userMaster.getEntityType()==3){
				map.addAttribute("addBatchJobFlag",false);
			}else {
				map.addAttribute("addBatchJobFlag",true);
			}
			map.addAttribute("noOfExpHires","");
			try{
				if(userMaster.getEntityType()!=1){
					userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in Batch Job Orders");
				}else{
					userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in Batch Job Orders For District");
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "batchjoborder";
	}
	/*============ Get School List ====================*/
	@RequestMapping(value="/batchschoollist.do", method=RequestMethod.GET)
	public String schoolListGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== school list in Batch Job Order Controller  ===============");
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			int roleId=0;
			int batchJobId=0;
			int type=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			if(request.getParameter("batchJobId")!=null){
				batchJobId=Integer.parseInt(request.getParameter("batchJobId"));
			}
			if(request.getParameter("type")!=null){
				type=Integer.parseInt(request.getParameter("type"));
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,26,"batchjoborder.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			BatchJobOrder batchJobOrder=batchJobOrderDAO.findById(batchJobId,false,false);
			System.out.println("roleAccess="+roleAccess);
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("userSession", userMaster);
			map.addAttribute("batchJobId",batchJobId);
			map.addAttribute("BatchJobTitle",batchJobOrder.getJobTitle());
			map.addAttribute("type",type);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "batchschoollist";
	}
	@RequestMapping(value="/batchschooljoborder.do", method=RequestMethod.GET)
	public String batchSchoolJobOrderGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== dashboard.do in Batch Job Order Controller  ===============");
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,40,"batchschooljoborder.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("roleAccess=>"+roleAccess);
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("userSession", userMaster);
			if(userMaster.getEntityType()!=1){
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictOrSchoolName",null);
			}
			
			try{
				if(userMaster.getEntityType()!=1){
					userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in Batch Job Orders");
				}else{
					userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in Batch Job Orders For School");
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("JobOrderType","3");
			map.addAttribute("addBatchJobFlag",true);
			map.addAttribute("noOfExpHires","");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "batchjoborder";
	}
	@RequestMapping(value="/addeditbatchjoborder.do", method=RequestMethod.GET)
	public String AddEditJobAction(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("AddEditBatchJobAction Method of BatchJobOrdersController Controller");
			
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			try{
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in add Batch Job Order");
			}catch(Exception e){
				e.printStackTrace();
			
			}

			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,37,"addeditbatchjoborder.do",0);
			}catch(Exception e){
				//e.printStackTrace();
			}
			System.out.println("roleAccess="+roleAccess);
			map.addAttribute("roleAccess", roleAccess);

			int JobOrderType=0;
			if(request.getParameter("JobOrderType")!=null){
				JobOrderType=Integer.parseInt(request.getParameter("JobOrderType"));
			}
			JobOrder jobOrder	=	new JobOrder(); 
			if(userMaster.getEntityType()==3){
				map.addAttribute("addBatchJobFlag",false);
			}else {
				map.addAttribute("addBatchJobFlag",true);
			}
			if(userMaster.getEntityType()==2 && JobOrderType==2){
				try{
					map.addAttribute("DAssessmentUploadURL",userMaster.getDistrictId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
				}
				map.addAttribute("SAssessmentUploadURL","");
				map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
				map.addAttribute("DistrictExitURL",userMaster.getDistrictId().getExitURL());
				map.addAttribute("DistrictExitMessage",userMaster.getDistrictId().getExitMessage());
				map.addAttribute("districtHiddenId",userMaster.getDistrictId().getDistrictId());
				int exitURLMessageVal=1;
				if(userMaster.getDistrictId().getFlagForMessage()!=null){
					if(userMaster.getDistrictId().getFlagForMessage()==1){
						exitURLMessageVal=2;
					}
				}
				map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","1");
			}else if(userMaster.getEntityType()==3 && JobOrderType==3){
				try{
					map.addAttribute("DAssessmentUploadURL",userMaster.getSchoolId().getDistrictId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
				}
				try{
					map.addAttribute("SAssessmentUploadURL",userMaster.getSchoolId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("SAssessmentUploadURL",null);
				}
				map.addAttribute("DistrictOrSchoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("DistrictOrSchoolId",userMaster.getSchoolId().getSchoolId());
				map.addAttribute("DistrictExitURL",userMaster.getSchoolId().getExitURL());
				map.addAttribute("DistrictExitMessage",userMaster.getSchoolId().getExitMessage());
				map.addAttribute("districtHiddenId",userMaster.getSchoolId().getDistrictId().getDistrictId());
				int exitURLMessageVal=1;
				if(userMaster.getSchoolId().getFlagForMessage()!=null){
					if(userMaster.getSchoolId().getFlagForMessage()==1){
						exitURLMessageVal=2;
					}
				}
				map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","1");
			}else{
				map.addAttribute("DistrictOrSchoolName",null);
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","0");
			}
			if(JobOrderType==2){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else if(JobOrderType==3){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else{
				map.addAttribute("districtRootPath",null);
				map.addAttribute("schoolRootPath",null);
			}
			List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			/*======== Gagan [Start] : mapping State Id to get auto select State acording to District ===============*/
			map.addAttribute("entityType", userMaster.getEntityType());
			if(userMaster.getEntityType()==2 )
				map.addAttribute("stateId",userMaster.getDistrictId().getStateId().getStateId());
			else
				if(userMaster.getEntityType()==3)
					map.addAttribute("stateId",userMaster.getSchoolId().getStateMaster().getStateId());
			/*======== Gagan [END] : mapping State Id to get auto select State acording to District ===============*/
			
			map.addAttribute("listStateMaster", listStateMaster);
			map.addAttribute("allSchoolGradeDistrictVal",1);
			System.out.println("JobOrderType="+JobOrderType);
			map.addAttribute("JobOrderType",JobOrderType);
			map.addAttribute("jobOrder",jobOrder);
			map.addAttribute("attachJobAssessmentVal",2);
			map.addAttribute("isJobAcceptedFlag",false);
			map.addAttribute("isAcceptStatus",true);
			map.addAttribute("copyBatchJobId",0);
			map.addAttribute("dateTime",Utility.getDateTime());
			map.addAttribute("schoolBatchId",null);
			map.addAttribute("jobCategoryMasterlst",null);	
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc",windowFunc);
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "addeditbatchjoborder";
	}
	
	/*============End Of Get Method of Domain Controller ====================*/
	/* @Author: Sekhar 
	 * @Discription: editbatchjoborder.do Controller.
	 */	
	@RequestMapping(value="/editbatchjoborder.do", method=RequestMethod.GET)
	public String doEditJobOrderGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("EditBatchJobAction Method of BatchJobOrdersController Controller::");
			
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			int roleId=0;
			String schoolId=null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,37,"addeditbatchjoborder.do",0);
			}catch(Exception e){
				//e.printStackTrace();
			}
			System.out.println("roleAccess="+roleAccess);
			map.addAttribute("roleAccess", roleAccess);
			
			int JobOrderType=0;
			int batchJobId=0;
			SchoolMaster schoolMaster=null;
			BatchJobOrder batchJobOrder=null;
			
			if(request.getParameter("JobOrderType")!=null){
				JobOrderType=Integer.parseInt(request.getParameter("JobOrderType"));
			}
			if(request.getParameter("batchJobId")!=null){
				batchJobId=Integer.parseInt(request.getParameter("batchJobId"));
			}
			if(request.getParameter("schoolId")!=null && userMaster.getEntityType()==1){
				schoolId=request.getParameter("schoolId");
				schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId),false,false);
				map.addAttribute("schoolBatchId",schoolId);
			}else{
				map.addAttribute("schoolBatchId",null);
			}
			if(batchJobId==0)
			{
				batchJobOrder	=	new BatchJobOrder();
			}
			else
			{
				batchJobOrder	=batchJobOrderDAO .findById(batchJobId, false, false);
			}
			
			System.out.println("JobId="+batchJobId);
			System.out.println("JobOrderType="+JobOrderType);
			
			
			if(userMaster.getEntityType()==3 || ( userMaster.getEntityType()==1 && JobOrderType==3)){
				map.addAttribute("addBatchJobFlag",false);
			}else {
				map.addAttribute("addBatchJobFlag",true);
			}
			int exitURLMessageVal=1;
			
			if(userMaster.getEntityType()==2 && JobOrderType==2){
				try{
					if(batchJobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
						map.addAttribute("DAssessmentUploadURL",batchJobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
					}else{
						map.addAttribute("DAssessmentUploadURL",null);
					}
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
				}
				map.addAttribute("SAssessmentUploadURL","");
				map.addAttribute("DistrictOrSchoolName",batchJobOrder.getDistrictMaster().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",batchJobOrder.getDistrictMaster().getDistrictId());
				try{	
					map.addAttribute("DSAssessmentUploadURL",batchJobOrder.getDistrictMaster().getAssessmentUploadURL());
				}catch(Exception e){
					map.addAttribute("DSAssessmentUploadURL",null);
				}
				map.addAttribute("DistrictExitURL",userMaster.getDistrictId().getExitURL());
				map.addAttribute("DistrictExitMessage",userMaster.getDistrictId().getExitMessage());
				map.addAttribute("PageFlag","1");
				map.addAttribute("SchoolDistrictNameFlag","1");
			}else if(userMaster.getEntityType()==3 && JobOrderType==3){
				if(userMaster.getSchoolId().getFlagForMessage()!=null){
					if(userMaster.getSchoolId().getFlagForMessage()==1){
						exitURLMessageVal=2;
					}
				}
				try{
					map.addAttribute("DAssessmentUploadURL",batchJobOrder.getSchool().get(0).getDistrictId().getAssessmentUploadURL().trim());
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
					//e.printStackTrace();
				}
				try{
					if(batchJobOrder.getSchool().get(0).getAssessmentUploadURL()!=null){
						map.addAttribute("SAssessmentUploadURL",batchJobOrder.getSchool().get(0).getAssessmentUploadURL().trim());	
					}else{
						map.addAttribute("SAssessmentUploadURL",null);
					}
				}catch(Exception e){
					map.addAttribute("SAssessmentUploadURL",null);
					//e.printStackTrace();
				}
				try{
					map.addAttribute("DistrictOrSchoolName",batchJobOrder.getSchool().get(0).getSchoolName());
				}catch(Exception e){
					map.addAttribute("DistrictOrSchoolName",null);
					//e.printStackTrace();
				}
				try{
					map.addAttribute("DistrictOrSchoolId",batchJobOrder.getSchool().get(0).getSchoolId());
				}catch(Exception e){
					map.addAttribute("DistrictOrSchoolId",null);
					//e.printStackTrace();
				}
				try{
					if(batchJobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
						map.addAttribute("DSAssessmentUploadURL",batchJobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
					}else{
						map.addAttribute("DSAssessmentUploadURL",null);
					}
				}catch(Exception e){
					map.addAttribute("DSAssessmentUploadURL",null);
				}
				
				map.addAttribute("DistrictExitURL",userMaster.getSchoolId().getExitURL());
				map.addAttribute("DistrictExitMessage",userMaster.getSchoolId().getExitMessage());
				map.addAttribute("PageFlag","1");
				map.addAttribute("SchoolDistrictNameFlag","1");
			}else if(userMaster.getEntityType()==1 && JobOrderType==3){
					if(schoolMaster.getFlagForMessage()!=null){
						if(schoolMaster.getFlagForMessage()==1){
							exitURLMessageVal=2;
						}
					}
					try{
						map.addAttribute("DAssessmentUploadURL",schoolMaster.getDistrictId().getAssessmentUploadURL().trim());
					}catch(Exception e){
						map.addAttribute("DAssessmentUploadURL",null);
					}
					try{
						if(batchJobOrder.getSchool().get(0).getAssessmentUploadURL()!=null){
							map.addAttribute("SAssessmentUploadURL",schoolMaster.getAssessmentUploadURL().trim());	
						}else{
							map.addAttribute("SAssessmentUploadURL",null);
						}
					}catch(Exception e){
						map.addAttribute("SAssessmentUploadURL",null);
					}
					try{
						map.addAttribute("DistrictOrSchoolName",schoolMaster.getSchoolName());
					}catch(Exception e){
						map.addAttribute("DistrictOrSchoolName",null);
					}
					try{
						map.addAttribute("DistrictOrSchoolId",schoolMaster.getSchoolId());
					}catch(Exception e){
						map.addAttribute("DistrictOrSchoolId",null);
					}
					try{
						if(batchJobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
							map.addAttribute("DSAssessmentUploadURL",schoolMaster.getDistrictId().getAssessmentUploadURL().trim());
						}else{
							map.addAttribute("DSAssessmentUploadURL",null);
						}
					}catch(Exception e){
						map.addAttribute("DSAssessmentUploadURL",null);
					}
					
				map.addAttribute("DistrictExitURL",schoolMaster.getExitURL());
				map.addAttribute("DistrictExitMessage",schoolMaster.getExitMessage());
				map.addAttribute("PageFlag","1");
				map.addAttribute("SchoolDistrictNameFlag","1");
			}else{
				if(JobOrderType==2){
					map.addAttribute("DistrictOrSchoolName",batchJobOrder.getDistrictMaster().getDistrictName());
					
					try{	
						if(batchJobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
							map.addAttribute("DAssessmentUploadURL",batchJobOrder.getDistrictMaster().getAssessmentUploadURL().trim());	
						}else{
							map.addAttribute("DAssessmentUploadURL",null);	
						}
					}catch(Exception e){
						map.addAttribute("DAssessmentUploadURL",null);
					}
					
					map.addAttribute("SAssessmentUploadURL","");
					map.addAttribute("DistrictOrSchoolId",batchJobOrder.getDistrictMaster().getDistrictId());
					try{
						map.addAttribute("DSAssessmentUploadURL",batchJobOrder.getDistrictMaster().getAssessmentUploadURL());
					}catch(Exception e){
						map.addAttribute("DSAssessmentUploadURL",null);
					}
				}else if(JobOrderType==3){
						try{
							map.addAttribute("DistrictOrSchoolName",batchJobOrder.getSchool().get(0).getSchoolName());
						}catch(Exception e){
							map.addAttribute("DistrictOrSchoolName",null);
						}
						try{	
							map.addAttribute("DAssessmentUploadURL",batchJobOrder.getSchool().get(0).getDistrictId().getAssessmentUploadURL().trim());
						}catch(Exception e){
							map.addAttribute("DAssessmentUploadURL",null);
						}
						try{	
							if(batchJobOrder.getSchool().get(0).getAssessmentUploadURL()!=null){
								map.addAttribute("SAssessmentUploadURL",batchJobOrder.getSchool().get(0).getAssessmentUploadURL().trim());
							}else{
								map.addAttribute("SAssessmentUploadURL",null);
							}
						}catch(Exception e){
							map.addAttribute("SAssessmentUploadURL",null);
						}
						try{
							map.addAttribute("DistrictOrSchoolId",batchJobOrder.getSchool().get(0).getSchoolId());
						}catch(Exception e){
							map.addAttribute("DistrictOrSchoolId",null);
						}
						
						
						try{
							if(batchJobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
								map.addAttribute("DSAssessmentUploadURL",batchJobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
							}else{
								map.addAttribute("DSAssessmentUploadURL",null);
							}
						}catch(Exception e){
							map.addAttribute("DSAssessmentUploadURL",null);
						}
						
				}
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","0");
			}
			System.out.println("======="+batchJobOrder.getBatchJobId());
			if(batchJobOrder.getSelectedSchoolsInDistrict()==1){
				map.addAttribute("allSchoolGradeDistrictVal",3);
			}else if(batchJobOrder.getAllGrades()==1){
				map.addAttribute("allSchoolGradeDistrictVal",2);
			}else{
				map.addAttribute("allSchoolGradeDistrictVal",1);
			}
			
			map.addAttribute("districtHiddenId",batchJobOrder.getDistrictMaster().getDistrictId());
			map.addAttribute("JobOrderType",JobOrderType);
			map.addAttribute("batchJobOrder",batchJobOrder);
	
			int attachJobAssessmentVal=2;

			if(JobOrderType==2){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else if(JobOrderType==3){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else{
				map.addAttribute("districtRootPath",null);
				map.addAttribute("schoolRootPath",null);
			}
			map.addAttribute("attachJobAssessmentVal",attachJobAssessmentVal);
			map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
			boolean isJobAcceptedFlag = schoolInBatchJobDAO.findJobAccepted(batchJobOrder);
			map.addAttribute("isJobAcceptedFlag",isJobAcceptedFlag);
			
			if(userMaster.getEntityType()==3){
				if(batchJobOrder.getSchool()!=null){
					boolean acceptStatus=schoolInBatchJobDAO.findAcceptStatus(batchJobOrder,userMaster.getSchoolId());
					if(!acceptStatus){
						map.addAttribute("isAcceptStatus",true);
					}else{
						map.addAttribute("isAcceptStatus",false);
					}
				}
				int noOfExpHires=schoolInBatchJobDAO.findNoOfExpHires(batchJobOrder,userMaster.getSchoolId(),0);
				if(noOfExpHires!=0){
					map.addAttribute("noOfExpHires",noOfExpHires);
				}else{
					map.addAttribute("noOfExpHires","");
				}
			}else if(JobOrderType==3 && userMaster.getEntityType()==1){
				if(batchJobOrder.getSchool()!=null){
					boolean acceptStatus=schoolInBatchJobDAO.findAcceptStatus(batchJobOrder,schoolMaster);
					if(!acceptStatus){
						map.addAttribute("isAcceptStatus",true);
					}else{
						map.addAttribute("isAcceptStatus",false);
					}
				}
				int noOfExpHires=schoolInBatchJobDAO.findNoOfExpHires(batchJobOrder,schoolMaster,0);
				if(noOfExpHires!=0){
					map.addAttribute("noOfExpHires",noOfExpHires);
				}else{
					map.addAttribute("noOfExpHires","");
				}
			}else{
				map.addAttribute("isAcceptStatus",true);
				int noOfExpHires=schoolInBatchJobDAO.findNoOfExpHires(batchJobOrder,schoolMaster,1);
				if(noOfExpHires!=0){
					map.addAttribute("noOfExpHires",noOfExpHires);
				}else{
					map.addAttribute("noOfExpHires","");
				}
			}
			List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			map.addAttribute("listStateMaster", listStateMaster);
			
			/*======== Gagan [Start] : mapping State Id to get auto select State acording to District ===============*/
			map.addAttribute("entityType", userMaster.getEntityType());
			if(userMaster.getEntityType()==2 )
				map.addAttribute("stateId",userMaster.getDistrictId().getStateId().getStateId());
			else
				if(userMaster.getEntityType()==3)
					map.addAttribute("stateId",userMaster.getSchoolId().getStateMaster().getStateId());
			/*======== Gagan [END] : mapping State Id to get auto select State acording to District ===============*/
			
			map.addAttribute("copyBatchJobId",0);
			map.addAttribute("dateTime",Utility.getDateTime());
			/*=== values to dropdown from jobCategoryMaster Table ===*/
			//List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByOrder();
			List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(batchJobOrder.getDistrictMaster());
			map.addAttribute("jobCategoryMasterlst",jobCategoryMasterlst);	
			System.out.println(" jobCategoryMasterlst size : "+jobCategoryMasterlst.size());
			
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc",windowFunc);
			
			try{
				if(userMaster.getEntityType()!=1){
					if(batchJobOrder.getDistrictMaster()!=null)
						userMaster.setDistrictId(batchJobOrder.getDistrictMaster());
					userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in accept Batch Job Order");
				}else{
					if(JobOrderType==2)
						userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in edit Batch Job Order");
					else
						userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in accept Batch Job Order");
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return "addeditbatchjoborder";
	}
	/* @Author: Sekhar 
	 * @Discription: copybatchjoborder.do Controller.
	 */	
	@RequestMapping(value="/copybatchjoborder.do", method=RequestMethod.GET)
	public String doCopyBatchJobOrderGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			UserMaster userMaster=null;
			HttpSession session = request.getSession(false);
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,37,"addeditbatchjoborder.do",0);
			}catch(Exception e){
				//e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("schoolBatchId",null);
			
			int JobOrderType=0;
			int batchJobId=0;
			BatchJobOrder batchJobOrder=null;
			
			if(request.getParameter("JobOrderType")!=null){
				JobOrderType=Integer.parseInt(request.getParameter("JobOrderType"));
			}
			if(request.getParameter("batchJobId")!=null){
				batchJobId=Integer.parseInt(request.getParameter("batchJobId"));
			}
			if(batchJobId==0)
			{
				batchJobOrder	=	new BatchJobOrder();
				map.addAttribute("copyBatchJobId",0);
			}
			else
			{
				batchJobOrder	=batchJobOrderDAO .findById(batchJobId, false, false);
				
				map.addAttribute("copyBatchJobId",batchJobOrder.getBatchJobId());
				batchJobOrder.setBatchJobId(0);
			}
			
			map.addAttribute("addBatchJobFlag",true);
		
			if(userMaster.getEntityType()==2 && JobOrderType==2){
				try{
					if(batchJobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
						map.addAttribute("DAssessmentUploadURL",batchJobOrder.getDistrictMaster().getAssessmentUploadURL().trim());
					}else{
						map.addAttribute("DAssessmentUploadURL",null);
					}
				}catch(Exception e){
					map.addAttribute("DAssessmentUploadURL",null);
				}
				map.addAttribute("SAssessmentUploadURL","");
				map.addAttribute("DistrictOrSchoolName",batchJobOrder.getDistrictMaster().getDistrictName());
				map.addAttribute("DistrictOrSchoolId",batchJobOrder.getDistrictMaster().getDistrictId());
				try{	
					map.addAttribute("DSAssessmentUploadURL",batchJobOrder.getDistrictMaster().getAssessmentUploadURL());
				}catch(Exception e){
					map.addAttribute("DSAssessmentUploadURL",null);
				}
				map.addAttribute("PageFlag","1");
				map.addAttribute("SchoolDistrictNameFlag","1");
			}else{
				if(JobOrderType==2){
					map.addAttribute("DistrictOrSchoolName",batchJobOrder.getDistrictMaster().getDistrictName());
					
					try{	
						if(batchJobOrder.getDistrictMaster().getAssessmentUploadURL()!=null){
							map.addAttribute("DAssessmentUploadURL",batchJobOrder.getDistrictMaster().getAssessmentUploadURL().trim());	
						}else{
							map.addAttribute("DAssessmentUploadURL",null);	
						}
					}catch(Exception e){
						map.addAttribute("DAssessmentUploadURL",null);
					}
					
					map.addAttribute("SAssessmentUploadURL","");
					map.addAttribute("DistrictOrSchoolId",batchJobOrder.getDistrictMaster().getDistrictId());
					try{
						map.addAttribute("DSAssessmentUploadURL",batchJobOrder.getDistrictMaster().getAssessmentUploadURL());
					}catch(Exception e){
						map.addAttribute("DSAssessmentUploadURL",null);
					}
				}
				map.addAttribute("PageFlag","0");
				map.addAttribute("SchoolDistrictNameFlag","0");
			}
			if(batchJobOrder.getSelectedSchoolsInDistrict()==1){
				map.addAttribute("allSchoolGradeDistrictVal",3);
			}else if(batchJobOrder.getAllGrades()==1){
				map.addAttribute("allSchoolGradeDistrictVal",2);
			}else{
				map.addAttribute("allSchoolGradeDistrictVal",1);
			}
			
			map.addAttribute("DistrictExitURL",batchJobOrder.getExitURL());
			map.addAttribute("DistrictExitMessage",batchJobOrder.getExitMessage());
			map.addAttribute("districtHiddenId",batchJobOrder.getDistrictMaster().getDistrictId());
			map.addAttribute("JobOrderType",JobOrderType);
			map.addAttribute("batchJobOrder",batchJobOrder);
			int exitURLMessageVal=1;
			if(batchJobOrder.getFlagForMessage()!=null){
				if(batchJobOrder.getFlagForMessage()==1){
					exitURLMessageVal=2;
				}
			}
			int attachJobAssessmentVal=2;
			if(JobOrderType==2){
				map.addAttribute("districtRootPath",Utility.getValueOfPropByKey("districtRootPath"));
				map.addAttribute("schoolRootPath",Utility.getValueOfPropByKey("schoolRootPath"));
			}else{
				map.addAttribute("districtRootPath",null);
				map.addAttribute("schoolRootPath",null);
			}
			map.addAttribute("attachJobAssessmentVal",attachJobAssessmentVal);
			map.addAttribute("DistrictExitURLMessageVal",exitURLMessageVal);
			boolean isJobAcceptedFlag = schoolInBatchJobDAO.findJobAccepted(batchJobOrder);
			map.addAttribute("isJobAcceptedFlag",isJobAcceptedFlag);
			

			map.addAttribute("isAcceptStatus",true);
			int noOfExpHires=schoolInBatchJobDAO.findNoOfExpHires(batchJobOrder,userMaster.getSchoolId(),1);
			if(noOfExpHires!=0){
				map.addAttribute("noOfExpHires",noOfExpHires);
			}else{
				map.addAttribute("noOfExpHires","");
			}
			map.addAttribute("dateTime",Utility.getDateTime());
			map.addAttribute("jobCategoryMasterlst",null);
			try{
				if(batchJobOrder.getDistrictMaster()!=null)
					userMaster.setDistrictId(batchJobOrder.getDistrictMaster());
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in copy Batch Job Order");
			}catch(Exception e){
				e.printStackTrace();
			
			}
			
			List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
			listStateMaster = stateMasterDAO.findAllStateByOrder();
			map.addAttribute("listStateMaster", listStateMaster);
			
			/*======== Gagan [Start] : mapping State Id to get auto select State acording to District ===============*/
			map.addAttribute("entityType", userMaster.getEntityType());
			if(userMaster.getEntityType()==2 )
				map.addAttribute("stateId",userMaster.getDistrictId().getStateId().getStateId());
			else
				if(userMaster.getEntityType()==3)
					map.addAttribute("stateId",userMaster.getSchoolId().getStateMaster().getStateId());
			/*======== Gagan [END] : mapping State Id to get auto select State acording to District ===============*/
			
		}catch (Exception e){
			e.printStackTrace();
		}
		return "addeditbatchjoborder";
	}

}
