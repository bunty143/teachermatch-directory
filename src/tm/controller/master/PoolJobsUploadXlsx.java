package tm.controller.master;
	
import java.io.File;	
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import org.python.antlr.PythonParser.print_stmt_return;
import org.python.core.exceptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sun.mail.iap.Response;
import com.sun.net.ssl.internal.ssl.Debug;
import com.sun.org.apache.xpath.internal.operations.Bool;


import tm.bean.DistrictRequisitionNumbers;


import tm.bean.JobForTeacher;
import tm.bean.SchoolInJobOrder;
import tm.bean.UserUploadFolderAccess;
import tm.bean.master.DistrictSchools;
import tm.bean.JobRequisitionNumbers;
import tm.bean.UserUploadFolderAccess;
import tm.bean.master.DistrictMaster;
import tm.bean.JobOrder;

import tm.bean.master.SchoolMaster;
import tm.bean.master.JobMaster;
import tm.bean.master.PoolMaster;


import tm.bean.master.JobPoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.master.JobMasterDAO;
import tm.dao.master.JobPoolMasterDAO;
import tm.dao.master.PoolMasterDAO;
import tm.dao.FileUploadHistoryDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;

import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.dao.JobOrderDAO;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.utility.Utility;
	
@Controller
public class PoolJobsUploadXlsx {
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private JobMasterDAO jobMasterDAO;
	
	@Autowired
	private PoolMasterDAO poolMasterDAO;
	
	@Autowired
	private JobPoolMasterDAO jobpoolMasterDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	public void setUserUploadFolderAccessDAO(UserUploadFolderAccessDAO userUploadFolderAccessDAO) {
		this.userUploadFolderAccessDAO = userUploadFolderAccessDAO;
	}
	
	@Autowired
	private FileUploadHistoryDAO fileUploadHistoryDAO;
	public void setFileUploadHistoryDAO(FileUploadHistoryDAO fileUploadHistoryDAO){
		this.fileUploadHistoryDAO = fileUploadHistoryDAO;
	}
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService){
	    this.emailerService = emailerService;
	}
	
		
	@RequestMapping(value="/pooljobsuploadxlsx.do", method=RequestMethod.GET)
	public String getallactivedistrict(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		    request.getSession();
		    
			System.out.println("::::: Use PositionImportXlsxController controller :::::");
				
		List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
		
		try {
			Criterion functionality = Restrictions.eq("functionality", "jobsUpload");
		Criterion active = Restrictions.eq("status", "A");
			uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for (UserUploadFolderAccess getRec : uploadFolderAccessesrec) {
			Integer districtId = getRec.getDistrictId().getDistrictId();
			//System.out.println("folde path      "+getRec.getFolderPath());
		File filesList = findLatestFilesByPath(getRec.getFolderPath());
		
		if(filesList==null){
			String to= "gourav@netsutra.com";
		String subject = "TM_instructional_vacancies file not found";
		String msg = "Not found any file for upload";						
		String from= "gourav@netsutra.com";
		
		System.out.println(msg);		
		emailerService.sendMail(to, subject, msg);
		}else{
			try {
				String FileName = filesList.getName();
				Integer rowId = getRec.getUseuploadFolderId();
				String folderPath = getRec.getFolderPath();
				String lastUploadDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(filesList.lastModified()));
						
						String[] actionValue = positionImport(FileName,districtId,rowId,folderPath,lastUploadDateTime);
						
						PrintWriter output = response.getWriter();
						if(actionValue[0].equalsIgnoreCase("noproblem")){
							
							
							//output.print("NoPorblem");
							String[] deleteRecords = deleteRecords(folderPath);
							output.print(actionValue[1]);
							output.print("\n"+deleteRecords[1]);
							output.print("\n"+deleteRecords[2]);
							
							
							List<String> lstTo = new ArrayList<String>();
							
							lstTo.add("Harshbarger@dadeschools.net");
							lstTo.add("clientservices@teachermatch.net");							
							lstTo.add("tmhotline@netsutra.com");
							lstTo.add("CArcher@dadeschools.net");
							lstTo.add("gourav@netsutra.com");
							
							String subject = "Import the data on Platform from Vacancy file";
							String msg = "Successfully imported the data on Platform.";
							
							String newRecords="";
							String delRecords="";
							if(actionValue[2]!=null)
								newRecords = actionValue[2];
							if(deleteRecords[2]!=null)
								delRecords = deleteRecords[2];
							
							String url = request.getRequestURL().toString();
							
							if(url.contains("platform")){
								for (String string : lstTo) {
									emailerService.sendMailAsHTMLText(string, subject, MailText.getMailTextInVacancy(request, newRecords, delRecords, msg));
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}    
			}
			return null;
	}
	
	public File findLatestFilesByPath(String path)
	{
		String pathToScan = path;
		File dir = new File(pathToScan);	
		
		File[] files2 = dir.listFiles( new FilenameFilter() {
		    public boolean accept( File dir,String name ) {
		    	//return name.startsWith("TM_instructional_vacancies");
		    	return name.endsWith("TM_instructional_vacancies.CSV");
		    }
		} );
				        
        if(files2.length == 0)
            return null;       
		        
		 File lastModifiedFile = files2[0];
	        for(int i = 1; i < files2.length; i++){
		            if(lastModifiedFile.lastModified() < files2[i].lastModified()){	            	
		                lastModifiedFile = files2[i];	            	
		            }
	        }       
		        		return lastModifiedFile;
	}
		
	private Vector vectorDataExcelXLSX = new Vector();
	public String[] positionImport(String fileName,Integer districtId,Integer rowId,String folderPath,String lastUploadDateTime)
	{			
		int district_Id = districtId;
		String[] arr = new String[3]; 	
		
		StringBuffer sb = new StringBuffer(); 
		//arr[1] = sb.toString();
		
		sb.append("\n:::: Import vacancy ::::positionImport:::::::::"+district_Id);
		Map<String,DistrictRequisitionNumbers> jobOrderMap = new HashMap<String, DistrictRequisitionNumbers>();
		   
	    int mapCount=0;
	    int updateCount=0;
	    int createCount=0;
	    int numOfError=0;
		String returnVal="";
		
		try{
				
			String filePath=folderPath+fileName;

			 try {
				 //
				 sb.append("\nRead records from csv\n");
			 if(fileName.contains(".csv") || fileName.contains(".CSV")){
					 vectorDataExcelXLSX = readDataExcelCSV(filePath);
				 }
			} catch (Exception e) {
				// TODO: handle exception
			}
			 
				    	 
			int run_date_count=11;
			int Position_ID_count=11;
			int Loc_Num_count=11;
			int Job_Code_count=11;
			int run_time_count=11;
			int Job_Category_count=11;
			int Expiration_Date_count=11;
			int pos_type_count=11;
			int A=0;
			
			String Position_ID1="";
			int Position_ID_req_count=11;
			List<String> excelReq=new ArrayList<String>();
			for(A=0; A<vectorDataExcelXLSX.size(); A++) {
				Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(A);
				for(int j=0; j<vectorCellEachRowData.size(); j++) {

					if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Position_ID")){
						Position_ID_req_count=j;
					}
					if(Position_ID_req_count==j)
						Position_ID1=vectorCellEachRowData.get(j).toString().trim();
				}
				if(Position_ID1!=null)
					excelReq.add(Position_ID1);
				
			}
			SessionFactory sessionFactory=jobMasterDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			Transaction txOpen =statelesSsession.beginTransaction();
			
			
		    DistrictMaster districtMaster=districtMasterDAO.findById(district_Id, false, false);
		    UserMaster userMaster = userMasterDAO.findById(1, false, false);
			   	
		    //districtRequisitionNumbersDAO.emptyDeleteFlag("D",districtMaster);
		    Criterion districtMaster_districtID = Restrictions.eq("districtMaster", districtMaster);
		    List<DistrictRequisitionNumbers> requisitionNumberUpdate = districtRequisitionNumbersDAO.findByCriteria(districtMaster_districtID);
		    for(DistrictRequisitionNumbers requisitionnumber:requisitionNumberUpdate)
			 {		    	
		    	requisitionnumber.setDeleteFlag("D");
		    	txOpen =statelesSsession.beginTransaction();		    	
				statelesSsession.update(requisitionnumber);
				txOpen.commit();
			 }
			
			List<DistrictRequisitionNumbers> requisitionNumberExist = districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(districtMaster,excelReq);
			List<Integer> allrequisitionNumberExist= new ArrayList<Integer>();
			sb.append(excelReq.size()+"   ==   "+requisitionNumberExist.size());
			Map<String,DistrictRequisitionNumbers>requisitionNumberMap=new HashMap<String, DistrictRequisitionNumbers>();
			 for(DistrictRequisitionNumbers requisitionnumber:requisitionNumberExist)
			 {	if(requisitionnumber.getIsUsed()){
				 allrequisitionNumberExist.add(requisitionnumber.getDistrictRequisitionId());
			 	}
				 requisitionNumberMap.put(requisitionnumber.getRequisitionNumber(), requisitionnumber);
			 }
			 
			 List<JobRequisitionNumbers> requisitionNumIsHired=null;
			 Criterion distReqIds = Restrictions.in("districtRequisitionNumbers", requisitionNumberExist);
			 if(requisitionNumberExist.size()>0){				
				 requisitionNumIsHired = jobRequisitionNumbersDAO.findByCriteria(distReqIds);
			 }			 
			
			 Map<Integer, JobRequisitionNumbers> listOfallRequsitionMap = new HashMap<Integer, JobRequisitionNumbers>();
			 Map<String, JobRequisitionNumbers> listOfallReqWithJobLocMap = new HashMap<String, JobRequisitionNumbers>();
			 Map<String, Integer> listOfReqWithMultipleJobMap = new HashMap<String, Integer>();
			 Map<String, Boolean> listOfReqIsHiredJobMap = new HashMap<String, Boolean>();
			 Map<String, Boolean> listOfReqIsNotHiredJobMap = new HashMap<String, Boolean>();
			 
			 if(requisitionNumIsHired.size()>0){
				 for (JobRequisitionNumbers jobRequisitionNumbers1 : requisitionNumIsHired) {
					 listOfallRequsitionMap.put(jobRequisitionNumbers1.getDistrictRequisitionNumbers().getDistrictRequisitionId(), jobRequisitionNumbers1);
					 String reqWithJobLoc =null;
					 try{
						 reqWithJobLoc=jobRequisitionNumbers1.getDistrictRequisitionNumbers().getDistrictRequisitionId()+"||"+jobRequisitionNumbers1.getJobOrder().getJobId()+"||"+jobRequisitionNumbers1.getSchoolMaster().getLocationCode();
					 }catch(Exception e){}
					 if(reqWithJobLoc!=null)
					 listOfallReqWithJobLocMap.put(reqWithJobLoc, jobRequisitionNumbers1);
					 
					 //if(jobRequisitionNumbers1.getJobOrder().getJobCategoryMaster().getJobCategoryName().contains("Hourly Teacher")){
					
						 if(jobRequisitionNumbers1.getStatus().intValue()==1){
							 listOfReqIsHiredJobMap.put(jobRequisitionNumbers1.getDistrictRequisitionNumbers().getRequisitionNumber(), true);					
						 }
						 if(jobRequisitionNumbers1.getStatus().intValue()==0){
							 listOfReqIsNotHiredJobMap.put(jobRequisitionNumbers1.getDistrictRequisitionNumbers().getRequisitionNumber(), true);
						 }
						 
						 
						 
						 Integer noOfrecs=0;
						 if(listOfReqWithMultipleJobMap.containsKey(jobRequisitionNumbers1.getDistrictRequisitionNumbers().getRequisitionNumber())){
							 
							 noOfrecs=listOfReqWithMultipleJobMap.get(jobRequisitionNumbers1.getDistrictRequisitionNumbers().getRequisitionNumber())+1;
							 listOfReqWithMultipleJobMap.remove(jobRequisitionNumbers1.getDistrictRequisitionNumbers().getRequisitionNumber());
							 listOfReqWithMultipleJobMap.put(jobRequisitionNumbers1.getDistrictRequisitionNumbers().getRequisitionNumber(),noOfrecs);
						 }else{
							 noOfrecs=1;
							 listOfReqWithMultipleJobMap.put(jobRequisitionNumbers1.getDistrictRequisitionNumbers().getRequisitionNumber(),noOfrecs);
						 }
					 //}
				}
			 }
			
			 List<SchoolMaster> schoolLoc = schoolMasterDAO.findSchoolListByDistrict(districtMaster);
			 Map<String, SchoolMaster>schoolLocExistMap = new HashMap<String, SchoolMaster>();
			
			 if(schoolLoc.size()!=0){
				 for (SchoolMaster school : schoolLoc) {
			
					 String ss=school.getLocationCode();
					 schoolLocExistMap.put(ss, school);
				}
			 }
			
			 List<JobOrder> jobIdList = jobOrderDAO.findJobOrderListByDistrictAndPoolJob(districtMaster);
			 			 			 
			 Map<String, JobOrder>poolJobMap = new HashMap<String, JobOrder>();
			 Map<String, JobOrder>poolJobNoZoneMap = new HashMap<String, JobOrder>();
			 if(jobIdList!=null && jobIdList.size()!=0){
				 String JobId="";
				 
			 for(JobOrder apiJob : jobIdList){
				 if(!apiJob.getJobId().equals(8782)){
				 poolJobNoZoneMap.put(apiJob.getJobTitle(), apiJob);
				 if(apiJob.getGeoZoneMaster()!=null){
					 JobId= apiJob.getJobTitle()+"||"+apiJob.getGeoZoneMaster().getGeoZoneId();
						 poolJobMap.put(JobId, apiJob);
					 }else{
						 poolJobNoZoneMap.put(apiJob.getJobTitle(), apiJob);							 
					 }
				 	}
				 }
			 }
			
			 List<JobForTeacher> reqInJFTList = jobForTeacherDAO.getHiredRequisitionNumbers(excelReq,jobIdList);
			 Map<String, Boolean> reqIsHired = new HashMap<String, Boolean>();
			 Map<String, Boolean> reqIsNotHired = new HashMap<String, Boolean>();
			 
			 if(reqInJFTList.size()>0){
				 for(JobForTeacher reqInJFT:reqInJFTList){					 
					 if(reqInJFT.getStatus().getStatusId().equals(6) && reqInJFT.getStatusMaster().getStatusId().equals(6)){
						 reqIsHired.put(reqInJFT.getRequisitionNumber(), true);
					 }else{
						 reqIsNotHired.put(reqInJFT.getRequisitionNumber(), true);
					 }
				 }
			 }
			 
			 Criterion jobCodeStatus = Restrictions.eq("status", "A");
			 List<JobMaster> jobMasterList = jobMasterDAO.findByCriteria(districtMaster_districtID,jobCodeStatus);
			 Map<String, JobMaster> jobMasterMap = new HashMap<String, JobMaster>();
			 for(JobMaster job:jobMasterList){
				 jobMasterMap.put(job.getJobCode(), job);
			 }
			
			 Map<String, Boolean> jobMasterExistMap = new HashMap<String, Boolean>();
			 for(JobMaster job:jobMasterList){
				 jobMasterExistMap.put(job.getJobCode(), true);
			 }
			
			 List<JobPoolMaster> jobPoolMaster = jobpoolMasterDAO.findByCriteria(districtMaster_districtID);
						
			 Map<Integer, Integer>jobPoolMasterMap = new HashMap<Integer, Integer>();
			 for(JobPoolMaster jobpool:jobPoolMaster){
				 jobPoolMasterMap.put(jobpool.getJobId().getJobId(), jobpool.getPoolId().getPoolId());
			 }
			 
			 List<SchoolInJobOrder> lstSchoolInJobOrders=null;
			 if(jobIdList.size()!=0){
				 lstSchoolInJobOrders = schoolInJobOrderDAO.getSIJO(jobIdList);
			 }
			 
			Map<String, SchoolInJobOrder>schoolInJobOrderMap = new HashMap<String, SchoolInJobOrder>();
			String jobAndsch=null;
			if(lstSchoolInJobOrders!=null && lstSchoolInJobOrders.size()!=0){				
				for(SchoolInJobOrder schoolInJO :lstSchoolInJobOrders){
					jobAndsch=schoolInJO.getJobId().getJobId()+"||"+schoolInJO.getSchoolId().getSchoolId();
					schoolInJobOrderMap.put(jobAndsch, schoolInJO);
				}
			}
	
			List<PoolMaster> lstPoolMasters = poolMasterDAO.findByCriteria(districtMaster_districtID);				
			Map<Integer, PoolMaster> lstPoolMastersMap = new HashMap<Integer, PoolMaster>();				
				if(lstPoolMasters!=null && lstPoolMasters.size()!=0){
					for(PoolMaster poolDesc:lstPoolMasters){
						lstPoolMastersMap.put(poolDesc.getPoolId(), poolDesc);
				}
			}
					
			String requisitionNumber_store="";
			   boolean row_error=false;
			   String final_error_store="";
			   String job_code_store="";
			   
			ArrayList<Integer> deleteFlagUpdateStore = new ArrayList<Integer>();
			
			txOpen =statelesSsession.beginTransaction();
			sb.append("\nMapping complete");
			int insertCount=0;
			for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
				
			    Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
			    String run_date="";
				String Position_ID="";
				String Loc_Num="";
				String Job_Code="";
				String run_time="";
				String Job_Category="";
				String Expiration_Date="";
				String pos_type="";

				String errorText="";
				String rowErrorText="";
				  
				for(int j=0; j<vectorCellEachRowData.size(); j++) {
					try{
					if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("run_date")){
						run_date_count=j;
					}
					if(run_date_count==j)
						run_date=vectorCellEachRowData.get(j).toString().trim();
					
					if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Position_ID")){
						Position_ID_count=j;
					}
					if(Position_ID_count==j)
						Position_ID=vectorCellEachRowData.get(j).toString().trim();
					
					if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Job_Code")){
						Job_Code_count=j;
					}
					if(Job_Code_count==j)
						Job_Code=vectorCellEachRowData.get(j).toString().trim();
					
					if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Loc_Num")){
								Loc_Num_count=j;
							}
					if(Loc_Num_count==j)
						Loc_Num=vectorCellEachRowData.get(j).toString().trim();
					
					if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("run_time")){
						run_time_count=j;
					}
					if(run_time_count==j)
						run_time=vectorCellEachRowData.get(j).toString().trim();
					
					if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Job_Category")){
						Job_Category_count=j;
					}
					if(Job_Category_count==j)
						Job_Category=vectorCellEachRowData.get(j).toString().trim();
					
					if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Expiration_Date")){
							Expiration_Date_count=j;
						}
						if(Expiration_Date_count==j)
							Expiration_Date=vectorCellEachRowData.get(j).toString().trim();
					}catch(Exception e){}
					
					if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("pos_type")){
							pos_type_count=j;
						}
						if(pos_type_count==j)
							pos_type=vectorCellEachRowData.get(j).toString().trim();
				}
				
				if(i==0){
				
					if(!run_date.equals("run_date")){
						rowErrorText+="run_date column is not found";
						row_error=true;
					}
					
					if(!Position_ID.equals("Position_ID")){
						 if(row_error){
								rowErrorText+=",";
						  }
						  
						 rowErrorText+="Position_ID column is not found";
						 row_error=true;
					}
					
					if(!Loc_Num.equals("Loc_Num")){
						 if(row_error){
								rowErrorText+=",";
						  }
							  
						rowErrorText+="Loc_Num column is not found";
						row_error=true;
					}
					
					if(!Job_Code.equals("Job_Code")){
						 if(row_error){
								rowErrorText+=",";
						  }
							  
						rowErrorText+="Job_Code column is not found";
						row_error=true;
					}
					if(!run_time.equals("run_time")){
						if(row_error){
								rowErrorText+=",";
						}
							  
						rowErrorText+="run_time column is not found";
						row_error=true;
					}
					
					if(!Job_Category.equals("Job_Category")){
						 if(row_error){
								rowErrorText+=",";
						  }
							  
						rowErrorText+="Job_Category column is not found";
						row_error=true;
					}
					
					if(!Expiration_Date.equals("Expiration_Date")){
						 if(row_error){
								rowErrorText+=",";
						  }
							  
						rowErrorText+="Expiration_Date column is not found";
						row_error=true;
					}
					if(!pos_type.equals("pos_type")){
						 if(row_error){
								rowErrorText+=",";
						  }
							  
						rowErrorText+="pos_type column is not found";
						row_error=true;
					}
				}
				  
				if(row_error){
				  sb.append("\nerror in text file");
				  File file = new File(folderPath+"excelUploadError.txt");
				
				if (!file.exists()) {
					file.createNewFile();
				}
					    				
				String to= "gourav@netsutra.com";
				String subject = "TeacherMatchAccessFile_ Upload Error";
				String msg = "Error in "+fileName+" file";
				String from= "gourav@netsutra.com";
				String errorFileName="excelUploadError.txt";			
					emailerService.sendMailWithAttachments(to,subject,from,msg,folderPath+errorFileName);
					
					 break;
				 }
				if(i!=0 && row_error==false){					
					boolean errorFlag=false;
					boolean categoryFlag=false;
					if(Job_Category.equalsIgnoreCase("")){
				
				errorText+="Job_Category is empty";
					errorFlag=true;		  	    			
				}else{
					categoryFlag=true;
				}
					
					
				
				if(categoryFlag && !Job_Category.contains("Hourly Teacher")){
					boolean updateFlag=false;
					boolean updateDistReqFlag=false;
					boolean insertDistReqFlag=false;
					boolean deleteReqDetail=false;
					boolean deleteMultipleFlag=false;	
					boolean jobReqUpdateFlag=false;	
					
					Long School_Id=null;
					Integer geoZone_Id=null;
					SchoolMaster oldSchoolId = new SchoolMaster();
					
					if(!Loc_Num.equalsIgnoreCase("")){
						if(schoolLocExistMap.get(Loc_Num)!=null && schoolLocExistMap.get(Loc_Num).getDistrictId().getStatus().equalsIgnoreCase("I")){
							if(errorFlag){
								errorText+=",";	
							}
							errorText+="Loc_Num does not found";
							errorFlag=true;
						}else if(schoolLocExistMap.get(Loc_Num)!=null && schoolLocExistMap.get(Loc_Num).getDistrictId().getStatus().equalsIgnoreCase("A") && schoolLocExistMap.get(Loc_Num).getStatus().equalsIgnoreCase("I")){
							if(errorFlag){
							errorText+=",";	
							}
							errorText+="Loc_Num is not exist";
								errorFlag=true;
						}/*
						gone error block
						else if(schoolLocExistMap.get(Loc_Num)!=null && schoolLocExistMap.get(Loc_Num).getDistrictId().getStatus().equalsIgnoreCase("A") && schoolLocExistMap.get(Loc_Num).getStatus().equalsIgnoreCase("A") && schoolLocExistMap.get(Loc_Num).getGeoZoneMaster()==null){	  	        			
								if(errorFlag){
									errorText+=",";	
								}
							errorText+="School not attached with a zone";
							errorFlag=true;			  	        		
						}*/
						else if(schoolLocExistMap.get(Loc_Num)!=null && schoolLocExistMap.get(Loc_Num).getDistrictId().getStatus().equalsIgnoreCase("A") && schoolLocExistMap.get(Loc_Num).getStatus().equalsIgnoreCase("A")){	  	        			
								School_Id = schoolLocExistMap.get(Loc_Num).getSchoolId();
								if(schoolLocExistMap.get(Loc_Num).getGeoZoneMaster()!=null){
									geoZone_Id = schoolLocExistMap.get(Loc_Num).getGeoZoneMaster().getGeoZoneId();
								}
						}else{
							if(errorFlag){
								errorText+=",";	
							}
							errorText+="Loc_Num does not found";
							errorFlag=true;
							}
					}
					else{
						if(errorFlag){
							errorText+=",";	
						}
						errorText+="Loc_Num is empty";
						errorFlag=true;
					}
								  	    			  	    	
					boolean onlYDistReqNum=false;			  	    	
					if(!Position_ID.equalsIgnoreCase("")){
									
						if(requisitionNumber_store.contains("||"+Position_ID+"||")){
							if(errorFlag){
								errorText+=",";	
							}
							errorText+="Position_ID already exist in sheet";
							errorFlag=true;
						}
						else if(requisitionNumberMap.get(Position_ID)==null){
							insertDistReqFlag=true;		  	        			
						}else if(listOfReqIsHiredJobMap.get(Position_ID)!=null && listOfReqIsHiredJobMap.get(Position_ID)!=null && reqIsNotHired.containsKey(Position_ID)){{
							if(errorFlag){
								errorText+=",";	
							}
							errorText+="Position_ID is in progress";
							errorFlag=true;
							onlYDistReqNum=true;
							}
						}
						else if(listOfReqIsHiredJobMap.get(Position_ID)!=null && listOfReqIsNotHiredJobMap.get(Position_ID)==null && reqIsHired.containsKey(Position_ID)){			
							onlYDistReqNum=true;
							insertDistReqFlag=true;
						}else if(requisitionNumberMap.get(Position_ID)!=null && listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId())!=null && !listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getLocationCode().equalsIgnoreCase(Loc_Num) && listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getStatus()==0){
							jobReqUpdateFlag=true;
							updateDistReqFlag=true;
							
							oldSchoolId=listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster();
						}
						else if(requisitionNumberMap.get(Position_ID)!=null && requisitionNumberMap.get(Position_ID).getIsUsed() && requisitionNumberMap.get(Position_ID).getJobCode()!=null && !requisitionNumberMap.get(Position_ID).getJobCode().equalsIgnoreCase(Job_Code.substring(4)) && listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId())!=null){
							deleteMultipleFlag=true;
							insertDistReqFlag=true;	
							sb.append("\ndeleted ============================");
						}	
						else if(requisitionNumberMap.get(Position_ID)!=null && requisitionNumberMap.get(Position_ID).getIsUsed()==false){
							updateDistReqFlag=true;
						}else{
							onlYDistReqNum=true;
							}
					}else{
						if(errorFlag){
							errorText+=",";	
						}
						errorText+="Position_ID is empty";
						errorFlag=true;
					}
					
					String new_job_code = "";
					Integer poolId=null;
					String jobPathDesc ="";
					String poolTitle ="";
					String jobTitle2 ="";
					if(!Job_Code.equalsIgnoreCase("")){
						new_job_code = Job_Code.substring(4);
					if(jobMasterExistMap.size()>0){
						if(jobMasterExistMap.containsKey(new_job_code)){	  	        				
							Integer jobId = jobMasterMap.get(new_job_code).getJobId();
							jobTitle2 = jobMasterMap.get(new_job_code).getJobTitle();
							Integer ispooljob =jobPoolMasterMap.get(jobId);
								poolTitle		= lstPoolMastersMap.get(jobPoolMasterMap.get(jobId)).getPoolTitle();	  	        					
							jobPathDesc = lstPoolMastersMap.get(jobPoolMasterMap.get(jobId)).getPoolDescription();	  	        				
						}else{
							if(errorFlag){
								errorText+=",";	
					}
							errorText+="Job_Code does not found";
							errorFlag=true;
						}
					}else{
						if(errorFlag){
							errorText+=",";	
						}
							errorText+="Job_Code is empty";
							errorFlag=true;
						}
					}
					else{
						if(errorFlag){
							errorText+=",";	
					}
					errorText+="Job_Code is empty";
						errorFlag=true;
					}
					
					if(Job_Category.equalsIgnoreCase("")){
					
					if(errorFlag){
						errorText+=",";	
					}
					errorText+="Job_Category is empty";
						errorFlag=true;
					}
					
					
					Integer finalJobId=null;
					String chkOthers =poolTitle;
					boolean noZoneJob=false;
					boolean ZoneJob=false;
					 if(!poolTitle.equals("") && geoZone_Id != null && geoZone_Id.intValue() != 0){
					if(geoZone_Id==1){
						poolTitle=poolTitle+" (Mid County)";
					}
					else if(geoZone_Id==2){
					poolTitle=poolTitle+" (North County)";
					}
					else if(geoZone_Id==3){
					poolTitle=poolTitle+" (South County)";
							}
						}
					 
					if(chkOthers.equalsIgnoreCase("Other")){
					errorText = "This is not a pool job.";
					}else{
						
						if(poolJobMap.containsKey(poolTitle+"||"+geoZone_Id) && poolJobMap.get(poolTitle+"||"+geoZone_Id)!=null){
							finalJobId = poolJobMap.get(poolTitle+"||"+geoZone_Id).getJobId();
						ZoneJob=true;		  	        				
					}else{
						//sb.append("\ncalll 222222222");
					if(poolJobNoZoneMap.containsKey(chkOthers) && poolJobNoZoneMap.get(chkOthers)!=null && poolJobNoZoneMap.get(chkOthers).getJobTitle()!=""){
						finalJobId = poolJobNoZoneMap.get(chkOthers).getJobId();
						noZoneJob=true;
					}
					else{
						if(errorFlag){
							errorText+=",";	
					}
					errorText+="Job does not found";
								errorFlag=true;
							}
						}
					}
					
					if (pos_type.length() > 0 && pos_type.charAt(pos_type.length()-1)=='T') {
						pos_type = pos_type.substring(0, pos_type.length()-1);
				    }
					
					DistrictRequisitionNumbers districtRequisitionNumbers = new DistrictRequisitionNumbers();
					JobRequisitionNumbers jobRequisitionNumbers = new JobRequisitionNumbers();
					SchoolInJobOrder schoolInJobOrder 		= new SchoolInJobOrder();
					SchoolInJobOrder schoolInJobOrderUpdate 		= new SchoolInJobOrder();
					
					if(onlYDistReqNum==true){
						DistrictRequisitionNumbers districtRequisitionNumbersUpdate = requisitionNumberMap.get(Position_ID);						
						districtRequisitionNumbersUpdate.setDeleteFlag("N");						
						districtRequisitionNumbersUpdate.setUploadFrom("E");
						statelesSsession.update(districtRequisitionNumbersUpdate);
					}
					
					boolean districtRequisitionNumbersFlag=false;
					boolean school_in_job_order=false;
					boolean school_in_job_update=false;
					JobOrder jobDetail=null;
					if(ZoneJob==true){			  	      		
					  	jobDetail=poolJobMap.get(poolTitle+"||"+geoZone_Id);
							}else{
								jobDetail=poolJobNoZoneMap.get(chkOthers);
							}
					if(errorText.equals("") && (updateDistReqFlag==true || insertDistReqFlag==true)){

					if(deleteReqDetail==true){	  	        				
						jobRequisitionNumbers.setJobRequisitionId(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobRequisitionId());					  	      	
						statelesSsession.delete(jobRequisitionNumbers);
						
						districtRequisitionNumbers.setDistrictRequisitionId(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId());	  		  	      		
						statelesSsession.delete(districtRequisitionNumbers);
					
						if(schoolInJobOrderMap.get(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder().getJobId()+"||"+listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getSchoolId())!=null){
					
						Integer row_id_dlt=schoolInJobOrderMap.get(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder().getJobId()+"||"+listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getSchoolId()).getJobOrderSchoolId();
						
						Integer delNoOEhr = schoolInJobOrderMap.get(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder().getJobId()+"||"+listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getSchoolId()).getNoOfSchoolExpHires()-1;
						
						
						//if(delNoOEhr>0){
							schoolInJobOrder.setJobOrderSchoolId(row_id_dlt);
							schoolInJobOrder.setJobId(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder());
							schoolInJobOrder.setSchoolId(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster());		  	      			
							schoolInJobOrder.setNoOfSchoolExpHires(delNoOEhr);
							schoolInJobOrder.setCreatedDateTime(new Date());
							statelesSsession.update(schoolInJobOrder);
							
							String reqJobId = listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder().getJobId()+"";
							String reqSchoolId = listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getSchoolId()+"";
							schoolInJobOrderMap.remove(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder().getJobId()+"||"+listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getSchoolId());
							schoolInJobOrderMap.put(reqJobId+"||"+reqSchoolId,schoolInJobOrder);
						//}
						/*else{
							SchoolInJobOrder deltSclNjob = schoolInJobOrderMap.get(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder().getJobId()+"||"+listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getSchoolId());
							deltSclNjob.setJobOrderSchoolId(deltSclNjob.getJobOrderSchoolId());
							statelesSsession.delete(deltSclNjob);
							schoolInJobOrderMap.remove(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder().getJobId()+"||"+listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getSchoolId());
						}*/
						
						}
					}
					
					if(deleteMultipleFlag==true){
						boolean deletePosiition=false;
						SchoolInJobOrder dltSchoolInJoborder = new SchoolInJobOrder();
						for(JobRequisitionNumbers objectDelete : requisitionNumIsHired) {							
							
							if(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId().equals(objectDelete.getDistrictRequisitionNumbers().getDistrictRequisitionId())){
								JobRequisitionNumbers jobRequisitionNumbersDlt = new JobRequisitionNumbers();
								
								jobRequisitionNumbersDlt.setJobRequisitionId(objectDelete.getJobRequisitionId());					  	      	
								statelesSsession.delete(jobRequisitionNumbersDlt);
								
								deletePosiition=true;
								String seacrhVar = objectDelete.getJobOrder().getJobId()+"||"+objectDelete.getSchoolMaster().getSchoolId();
								if(schoolInJobOrderMap.get(seacrhVar)!=null){
									
									
									Integer row_id_dlt=schoolInJobOrderMap.get(seacrhVar).getJobOrderSchoolId();
									Integer delNoOEhr = schoolInJobOrderMap.get(seacrhVar).getNoOfSchoolExpHires()-1;
									SchoolInJobOrder schoolInJobOrdermul = schoolInJobOrderMap.get(seacrhVar);
									
									schoolInJobOrdermul.setJobOrderSchoolId(schoolInJobOrdermul.getJobOrderSchoolId());											  	      			
									schoolInJobOrdermul.setNoOfSchoolExpHires(delNoOEhr);
									schoolInJobOrdermul.setCreatedDateTime(new Date());
									//if(delNoOEhr>0){
									statelesSsession.update(schoolInJobOrdermul);
									schoolInJobOrderMap.remove(seacrhVar);
									schoolInJobOrderMap.put(seacrhVar,schoolInJobOrdermul);
									//}
									/*else{
										statelesSsession.delete(schoolInJobOrdermul);
										schoolInJobOrderMap.remove(seacrhVar);
									}
										*/								
									
									}
							}							
						}	
						
						if(deletePosiition){
							districtRequisitionNumbers.setDistrictRequisitionId(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId());	  		  	      		
							statelesSsession.delete(districtRequisitionNumbers);
						}
					}
					
					if(updateDistReqFlag==true){
						districtRequisitionNumbers=requisitionNumberMap.get(Position_ID);
						districtRequisitionNumbers.setDeleteFlag("N");
						districtRequisitionNumbers.setIsUsed(true);
						districtRequisitionNumbers.setPosType(pos_type);
						districtRequisitionNumbers.setUploadFrom("E");
						statelesSsession.update(districtRequisitionNumbers);
						districtRequisitionNumbersFlag = true;
					}
					
					if(insertDistReqFlag==true){
					districtRequisitionNumbers.setDistrictMaster(districtMaster);
					districtRequisitionNumbers.setRequisitionNumber(Position_ID);
					districtRequisitionNumbers.setJobCode(new_job_code);
					districtRequisitionNumbers.setJobTitle(jobTitle2);
					districtRequisitionNumbers.setIsUsed(true);
					districtRequisitionNumbers.setPosType(pos_type);
					districtRequisitionNumbers.setDeleteFlag("N");
					districtRequisitionNumbers.setUploadFrom("E");
					districtRequisitionNumbers.setUserMaster(userMaster);
					districtRequisitionNumbers.setCreatedDateTime(new Date());
					statelesSsession.insert(districtRequisitionNumbers);
					insertCount++;
					districtRequisitionNumbersFlag = true;
					}
					
					if(districtRequisitionNumbersFlag==true){		
						jobRequisitionNumbers.setJobOrder(jobDetail);								
						jobRequisitionNumbers.setDistrictRequisitionNumbers(districtRequisitionNumbers);
						jobRequisitionNumbers.setSchoolMaster(schoolLocExistMap.get(Loc_Num));
						jobRequisitionNumbers.setStatus(0);
					if(jobReqUpdateFlag==true){
						jobRequisitionNumbers.setJobRequisitionId(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobRequisitionId());
						statelesSsession.update(jobRequisitionNumbers);
						school_in_job_update=true;
					}else{
						statelesSsession.insert(jobRequisitionNumbers);
					school_in_job_order=true;
					}
						
					}
					
					//oldSchoolId
					
					
					if(school_in_job_update){
					  	Integer noOEhr=null;
					  	Integer noOEhrs=null;
					//old school
					if(schoolInJobOrderMap.get(jobDetail.getJobId()+"||"+oldSchoolId.getSchoolId())!=null){
					
					noOEhrs=schoolInJobOrderMap.get(jobDetail.getJobId()+"||"+oldSchoolId.getSchoolId()).getNoOfSchoolExpHires();
						
						
						if(noOEhrs<0){
							noOEhrs=0;
						}
						
						noOEhr = noOEhrs-1;
						
					Integer row_id=schoolInJobOrderMap.get(jobDetail.getJobId()+"||"+oldSchoolId.getSchoolId()).getJobOrderSchoolId();
					
					schoolInJobOrderUpdate.setJobOrderSchoolId(row_id);
					schoolInJobOrderUpdate.setJobId(jobDetail);	
					schoolInJobOrderUpdate.setSchoolId(oldSchoolId);		  	      			
					schoolInJobOrderUpdate.setNoOfSchoolExpHires(noOEhr);
					schoolInJobOrderUpdate.setCreatedDateTime(new Date());				  	      			
						statelesSsession.update(schoolInJobOrderUpdate);	
						
					schoolInJobOrderMap.remove(jobDetail.getJobId()+"||"+oldSchoolId.getSchoolId());
					schoolInJobOrderMap.put(jobDetail.getJobId()+"||"+oldSchoolId.getSchoolId(),schoolInJobOrderUpdate);
					   }
					  	
					
						
						//new school
					if(schoolInJobOrderMap.get(jobDetail.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId())!=null){
					
					noOEhrs=schoolInJobOrderMap.get(jobDetail.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId()).getNoOfSchoolExpHires();
					
					
					if(noOEhrs<0){
						noOEhrs=0;
					}
					
					noOEhr = noOEhrs+1;
					
					
					Integer row_id=schoolInJobOrderMap.get(jobDetail.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId()).getJobOrderSchoolId();
					schoolInJobOrder.setJobOrderSchoolId(row_id);
					schoolInJobOrder.setJobId(jobDetail);	
					schoolInJobOrder.setSchoolId(schoolLocExistMap.get(Loc_Num));		  	      			
					schoolInJobOrder.setNoOfSchoolExpHires(noOEhr);
					schoolInJobOrder.setCreatedDateTime(new Date());
					statelesSsession.update(schoolInJobOrder);
					schoolInJobOrderMap.remove(jobDetail.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId());		  	      			
					mapCount++;
					}else{
						noOEhr=1;
						
						
						schoolInJobOrder.setJobId(jobDetail);								
						schoolInJobOrder.setSchoolId(schoolLocExistMap.get(Loc_Num));		  	      			
						schoolInJobOrder.setNoOfSchoolExpHires(noOEhr);
						schoolInJobOrder.setCreatedDateTime(new Date());
						statelesSsession.insert(schoolInJobOrder);
						mapCount++;
					}
					schoolInJobOrderMap.put(jobDetail.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId(),schoolInJobOrder);		  	      		
					  	}
					  	
					  
						if(school_in_job_order){
						Integer noOEhr=null;
					  	Integer noOEhrs=null;
							//poolJobNoZoneMap.get(chkOthers)
					if(schoolInJobOrderMap.get(jobDetail.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId())!=null){
					
					noOEhrs=schoolInJobOrderMap.get(jobDetail.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId()).getNoOfSchoolExpHires();
					
					
					if(noOEhrs<0){
						noOEhrs=0;
					}
					
					noOEhr = noOEhrs+1;
					
					
					Integer row_id=schoolInJobOrderMap.get(jobDetail.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId()).getJobOrderSchoolId();
					
					schoolInJobOrder.setJobOrderSchoolId(row_id);
					schoolInJobOrder.setJobId(jobDetail);	
					schoolInJobOrder.setSchoolId(schoolLocExistMap.get(Loc_Num));		  	      			
					schoolInJobOrder.setNoOfSchoolExpHires(noOEhr);
					schoolInJobOrder.setCreatedDateTime(new Date());
					statelesSsession.update(schoolInJobOrder);
					schoolInJobOrderMap.remove(jobDetail.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId());		  	      			
					mapCount++;
					}else{
						noOEhr=1;						
						schoolInJobOrder.setJobId(jobDetail);								
						schoolInJobOrder.setSchoolId(schoolLocExistMap.get(Loc_Num));		  	      			
						schoolInJobOrder.setNoOfSchoolExpHires(noOEhr);
						schoolInJobOrder.setCreatedDateTime(new Date());
						statelesSsession.insert(schoolInJobOrder);
						mapCount++;
					}
					schoolInJobOrderMap.put(jobDetail.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId(),schoolInJobOrder);
						}
						}
				}
				else{
					//sb.append("\nAnother category start  ......");
					boolean updateFlag2=false;
					boolean updateDistReqFlag2=false;
					boolean insertDistReqFlag2=false;
					boolean deleteUpdateFlag2=false;	
					boolean jobReqUpdateFlag2=false;
					boolean deleteReqDetail2=false;		
					
					Long School_Id=null;
					Integer geoZone_Id=null;
					SchoolMaster oldSchoolId = new SchoolMaster();
					if(!Loc_Num.equalsIgnoreCase("")){
						if(schoolLocExistMap.get(Loc_Num)!=null && schoolLocExistMap.get(Loc_Num).getDistrictId().getStatus().equalsIgnoreCase("I")){
							if(errorFlag){
								errorText+=",";	
							}
							errorText+="Loc_Num does not found";
							errorFlag=true;
						}else if(schoolLocExistMap.get(Loc_Num)!=null && schoolLocExistMap.get(Loc_Num).getDistrictId().getStatus().equalsIgnoreCase("A") && schoolLocExistMap.get(Loc_Num).getStatus().equalsIgnoreCase("I")){
							if(errorFlag){
								errorText+=",";	
							}
							errorText+="Loc_Num is not exist";
							errorFlag=true;
						}else if(schoolLocExistMap.get(Loc_Num)!=null && schoolLocExistMap.get(Loc_Num).getDistrictId().getStatus().equalsIgnoreCase("A") && schoolLocExistMap.get(Loc_Num).getStatus().equalsIgnoreCase("A")){	  	        			
							School_Id = schoolLocExistMap.get(Loc_Num).getSchoolId();
							if(schoolLocExistMap.get(Loc_Num).getGeoZoneMaster()!=null){
								geoZone_Id = schoolLocExistMap.get(Loc_Num).getGeoZoneMaster().getGeoZoneId();
							}
							}else{
								if(errorFlag){
									errorText+=",";	
								}
								errorText+="Loc_Num does not found";
								errorFlag=true;
							}
					}
					else{
						if(errorFlag){
							errorText+=",";	
						}
						errorText+="Loc_Num is empty";
						errorFlag=true;
					}
								  	    			  	    	
					boolean onlYDistReqNum2=false;
					boolean deleteReqDetail=false;
					boolean onlYDistReqNumIfUsed=false;
					if(!Position_ID.equalsIgnoreCase("")){				  	    		
						try{
							if(requisitionNumber_store.contains("||"+Position_ID+"||")){
								if(errorFlag){
									errorText+=",";	
								}
								errorText+="Position_ID already exist";
								errorFlag=true;				  	    			
							}else if(requisitionNumberMap.get(Position_ID)==null){
								insertDistReqFlag2=true;		  	        			
							}else if(requisitionNumberMap.get(Position_ID)!=null && listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId())!=null && listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getLocationCode()!=null && !listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getLocationCode().equalsIgnoreCase(Loc_Num) && listOfReqIsHiredJobMap.get(Position_ID)!=null){
								jobReqUpdateFlag2=true;
								updateDistReqFlag2=true;
								oldSchoolId=listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster();
							}else if(requisitionNumberMap.get(Position_ID)!=null && requisitionNumberMap.get(Position_ID).getIsUsed()==false){
								updateDistReqFlag2=true;
							}else{
								updateDistReqFlag2=true;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						
					}else{
						if(errorFlag){
							errorText+=",";	
					}
					errorText+="Position_ID is empty";
						errorFlag=true;
					}
					
					String new_job_code = "";				  	    	
					String poolTitle ="";
					String jobTitle3 ="";
					if(!Job_Code.equalsIgnoreCase("")){
						new_job_code = Job_Code.substring(4);
					if(jobMasterExistMap.size()>0){
						if(jobMasterExistMap.containsKey(new_job_code)){	  	        				
							Integer jobId = jobMasterMap.get(new_job_code).getJobId();
							jobTitle3 = jobMasterMap.get(new_job_code).getJobTitle();
							Integer ispooljob =jobPoolMasterMap.get(jobId);
								poolTitle		= lstPoolMastersMap.get(jobPoolMasterMap.get(jobId)).getPoolTitle();
						}else{
							if(errorFlag){
								errorText+=",";	
					}
					errorText+="Job_Code does not found";
							errorFlag=true;
						}
					}else{
						if(errorFlag){
							errorText+=",";	
					}
					errorText+="Job_Code is empty";
							errorFlag=true;
						}
					}
					else{
						if(errorFlag){
							errorText+=",";	
					}
					errorText+="Job_Code is empty";
						errorFlag=true;
					}
					//database level 2
					if (pos_type.length() > 0 && pos_type.charAt(pos_type.length()-1)=='T') {
						pos_type = pos_type.substring(0, pos_type.length()-1);
				    }			  	        	
					if(onlYDistReqNumIfUsed==true){
						DistrictRequisitionNumbers districtRequisitionNumbersUpdate = requisitionNumberMap.get(Position_ID);
						districtRequisitionNumbersUpdate.setDeleteFlag("N");						
						districtRequisitionNumbersUpdate.setUploadFrom("E");
						statelesSsession.update(districtRequisitionNumbersUpdate);
					}
					
					DistrictRequisitionNumbers districtRequisitionNumbers2 = new DistrictRequisitionNumbers();
					
					if(errorText.equals("") && (updateDistReqFlag2==true || insertDistReqFlag2==true)){

							if(deleteReqDetail==true){
								JobRequisitionNumbers jobRequisitionNumberschngcat = listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId());
								DistrictRequisitionNumbers districtRequisitionNumbers = requisitionNumberMap.get(Position_ID);
								jobRequisitionNumberschngcat.setJobRequisitionId(jobRequisitionNumberschngcat.getJobRequisitionId());
								SchoolInJobOrder schoolInJobOrderchngcat = new SchoolInJobOrder();
								
								statelesSsession.delete(jobRequisitionNumberschngcat);
								
								districtRequisitionNumbers.setDistrictRequisitionId(districtRequisitionNumbers.getDistrictRequisitionId());	  		  	      		
								statelesSsession.delete(districtRequisitionNumbers);
							
								if(schoolInJobOrderMap.get(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder().getJobId()+"||"+listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getSchoolId())!=null){
							
								Integer row_id_dlt=schoolInJobOrderMap.get(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder().getJobId()+"||"+listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getSchoolId()).getJobOrderSchoolId();
								
								Integer delNoOEhr = schoolInJobOrderMap.get(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder().getJobId()+"||"+listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getSchoolId()).getNoOfSchoolExpHires()-1;
								
								schoolInJobOrderchngcat.setJobOrderSchoolId(row_id_dlt);
								schoolInJobOrderchngcat.setJobId(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder());
								schoolInJobOrderchngcat.setSchoolId(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster());		  	      			
								schoolInJobOrderchngcat.setNoOfSchoolExpHires(delNoOEhr);
								schoolInJobOrderchngcat.setCreatedDateTime(new Date());
								
								//if(delNoOEhr>0){
									statelesSsession.update(schoolInJobOrderchngcat);
									
									String reqJobId = listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder().getJobId()+"";
									String reqSchoolId = listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getSchoolId()+"";
									schoolInJobOrderMap.remove(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder().getJobId()+"||"+listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getSchoolId());
									schoolInJobOrderMap.put(reqJobId+"||"+reqSchoolId,schoolInJobOrderchngcat);
							//	}
								/*else{
									schoolInJobOrderMap.remove(listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getJobOrder().getJobId()+"||"+listOfallRequsitionMap.get(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId()).getSchoolMaster().getSchoolId());
									statelesSsession.delete(schoolInJobOrderchngcat);
								}*/					
								
								}
							}
							
							boolean ifDltRe=false;
							boolean ifUpdateRe=false;
							boolean ifInsertRe=false;
							boolean districtRequisitionNumbersFlag2=false;
					for (JobOrder newCatJobId : jobIdList) {
						if(!newCatJobId.getJobId().equals(8782)){
						
						JobRequisitionNumbers jobRequisitionNumbers2 = new JobRequisitionNumbers();
						SchoolInJobOrder schoolInJobOrder2 		= new SchoolInJobOrder();
						SchoolInJobOrder schoolInJobOrderUpdate2 		= new SchoolInJobOrder();
									  	        			
						boolean school_in_job_order2=false;
						boolean school_in_job_update2=false;
						
						if(newCatJobId.getJobCategoryMaster().getJobCategoryName().contains("Instructional") && newCatJobId.getJobType().equalsIgnoreCase("F")){
							
								if(deleteReqDetail2==true){	 
										String chckreJobSchl1 = districtRequisitionNumbers2.getDistrictRequisitionId()+"||"+newCatJobId.getJobId()+"||"+Loc_Num;
								
								if(listOfallReqWithJobLocMap.get(chckreJobSchl1)!=null){
									jobRequisitionNumbers2.setJobRequisitionId(listOfallReqWithJobLocMap.get(chckreJobSchl1).getJobRequisitionId());					  	      	
									statelesSsession.delete(jobRequisitionNumbers2);
								}
								
								if(ifDltRe==false){
									districtRequisitionNumbers2.setDistrictRequisitionId(requisitionNumberMap.get(Position_ID).getDistrictRequisitionId());	  		  	      		
									statelesSsession.delete(districtRequisitionNumbers2);
									ifDltRe=true;
								}
								
								if(schoolInJobOrderMap.get(listOfallReqWithJobLocMap.get(chckreJobSchl1).getJobOrder().getJobId()+"||"+listOfallReqWithJobLocMap.get(chckreJobSchl1).getSchoolMaster().getSchoolId())!=null){
								
								Integer row_id_dlt=schoolInJobOrderMap.get(listOfallReqWithJobLocMap.get(chckreJobSchl1).getJobOrder().getJobId()+"||"+listOfallReqWithJobLocMap.get(chckreJobSchl1).getSchoolMaster().getSchoolId()).getJobOrderSchoolId();
								
								Integer delNoOEhr = schoolInJobOrderMap.get(listOfallReqWithJobLocMap.get(chckreJobSchl1).getJobOrder().getJobId()+"||"+listOfallReqWithJobLocMap.get(chckreJobSchl1).getSchoolMaster().getSchoolId()).getNoOfSchoolExpHires()-1;
								
								schoolInJobOrder2.setJobOrderSchoolId(row_id_dlt);							  	      					  	      			
								schoolInJobOrder2.setNoOfSchoolExpHires(delNoOEhr);
								schoolInJobOrder2.setCreatedDateTime(new Date());							  	      			
								statelesSsession.update(schoolInJobOrder2);
								
								
								String reqJobId = newCatJobId.getJobId()+"";
								String reqSchoolId = listOfallReqWithJobLocMap.get(chckreJobSchl1).getSchoolMaster().getSchoolId()+"";
								schoolInJobOrderMap.remove(listOfallReqWithJobLocMap.get(chckreJobSchl1).getJobOrder().getJobId()+"||"+listOfallReqWithJobLocMap.get(chckreJobSchl1).getSchoolMaster().getSchoolId());
								schoolInJobOrderMap.put(reqJobId+"||"+reqSchoolId,schoolInJobOrder2);
												}
										}
								
								if(updateDistReqFlag2==true){
									if(districtRequisitionNumbersFlag2==false){
										districtRequisitionNumbers2=requisitionNumberMap.get(Position_ID);
										districtRequisitionNumbers2.setDeleteFlag("N");
										districtRequisitionNumbers2.setIsUsed(true);
										districtRequisitionNumbers2.setPosType(pos_type);
										districtRequisitionNumbers2.setJobCode(new_job_code);
										districtRequisitionNumbers2.setJobTitle(jobTitle3);
										districtRequisitionNumbers2.setUploadFrom("E");
										statelesSsession.update(districtRequisitionNumbers2);
										districtRequisitionNumbersFlag2= true;
									}
								}
								
								if(insertDistReqFlag2){
									if(districtRequisitionNumbersFlag2==false){
										districtRequisitionNumbers2.setDistrictMaster(districtMaster);
										districtRequisitionNumbers2.setRequisitionNumber(Position_ID);
										districtRequisitionNumbers2.setJobCode(new_job_code);
										districtRequisitionNumbers2.setJobTitle(jobTitle3);
										districtRequisitionNumbers2.setIsUsed(true);
										districtRequisitionNumbers2.setPosType(pos_type);
										districtRequisitionNumbers2.setDeleteFlag("N");
										districtRequisitionNumbers2.setUploadFrom("E");
										districtRequisitionNumbers2.setUserMaster(userMaster);
										districtRequisitionNumbers2.setCreatedDateTime(new Date());
										statelesSsession.insert(districtRequisitionNumbers2);
										insertCount++;
										districtRequisitionNumbersFlag2 = true;
									}
								} 	        				
								
								String chckreJobSchl = districtRequisitionNumbers2.getDistrictRequisitionId()+"||"+newCatJobId.getJobId()+"||"+Loc_Num;
								if(listOfallReqWithJobLocMap.get(chckreJobSchl)==null){	
											if(districtRequisitionNumbersFlag2==true){
												
												jobRequisitionNumbers2.setJobOrder(newCatJobId);								
										  		jobRequisitionNumbers2.setDistrictRequisitionNumbers(districtRequisitionNumbers2);
										  		jobRequisitionNumbers2.setSchoolMaster(schoolLocExistMap.get(Loc_Num));
										  		
										  	      	if(jobReqUpdateFlag2==true){
										  	      	jobRequisitionNumbers2.setJobRequisitionId(listOfallReqWithJobLocMap.get(districtRequisitionNumbers2.getDistrictRequisitionId()+"||"+newCatJobId.getJobId()+"||"+oldSchoolId.getLocationCode()).getJobRequisitionId());
										  	      	jobRequisitionNumbers2.setStatus(listOfallReqWithJobLocMap.get(districtRequisitionNumbers2.getDistrictRequisitionId()+"||"+newCatJobId.getJobId()+"||"+oldSchoolId.getLocationCode()).getStatus());
										  	      	
										  	      	if(listOfallReqWithJobLocMap.get(districtRequisitionNumbers2.getDistrictRequisitionId()+"||"+newCatJobId.getJobId()+"||"+oldSchoolId.getLocationCode()).getStatus()==0){
											      		statelesSsession.update(jobRequisitionNumbers2);
											      		school_in_job_update2=true;
										  	      	}
												}else{
													jobRequisitionNumbers2.setStatus(0);
										      		statelesSsession.insert(jobRequisitionNumbers2);
										      	school_in_job_order2=true;
												}							  	      		
										}
										
										if(school_in_job_update2){
										
										//old school
										if(schoolInJobOrderMap.get(newCatJobId.getJobId()+"||"+oldSchoolId.getSchoolId())!=null){
												Integer noOEhr=null;
												Integer noOEhrs=null;      		
												noOEhrs=schoolInJobOrderMap.get(newCatJobId.getJobId()+"||"+oldSchoolId.getSchoolId()).getNoOfSchoolExpHires();
												
												if(noOEhrs<0){
													noOEhrs=0;
												}
												
												noOEhr = noOEhrs-1;
												SchoolInJobOrder deleteExpHire = schoolInJobOrderMap.get(newCatJobId.getJobId()+"||"+oldSchoolId.getSchoolId());
												Integer row_id=schoolInJobOrderMap.get(newCatJobId.getJobId()+"||"+oldSchoolId.getSchoolId()).getJobOrderSchoolId();			
												deleteExpHire.setJobOrderSchoolId(row_id);
												deleteExpHire.setNoOfSchoolExpHires(noOEhr);
												//if(noOEhr>0){
													statelesSsession.update(deleteExpHire);
													schoolInJobOrderMap.remove(newCatJobId.getJobId()+"||"+oldSchoolId.getSchoolId());
													schoolInJobOrderMap.put(newCatJobId.getJobId()+"||"+oldSchoolId.getSchoolId(),deleteExpHire);
												//}
												/*else{
													statelesSsession.delete(deleteExpHire);
													schoolInJobOrderMap.remove(newCatJobId.getJobId()+"||"+oldSchoolId.getSchoolId());													
													sb.append("\nupdate one");
												}	*/
										  }
											
											//new school
										if(schoolInJobOrderMap.get(newCatJobId.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId())!=null){
												
											Integer noOEhr=null;
											Integer noOEhrs=null;
											noOEhrs=schoolInJobOrderMap.get(newCatJobId.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId()).getNoOfSchoolExpHires();
																	
											if(noOEhrs<0){
												noOEhrs=0;
											}
											
											noOEhr = noOEhrs+1;						
											
											Integer row_id2=schoolInJobOrderMap.get(newCatJobId.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId()).getJobOrderSchoolId();
											schoolInJobOrder2.setJobOrderSchoolId(row_id2);
											schoolInJobOrder2.setJobId(newCatJobId);	
											schoolInJobOrder2.setSchoolId(schoolLocExistMap.get(Loc_Num));		  	      			
											schoolInJobOrder2.setNoOfSchoolExpHires(noOEhr);
											schoolInJobOrder2.setCreatedDateTime(new Date());
											statelesSsession.update(schoolInJobOrder2);
											schoolInJobOrderMap.remove(newCatJobId.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId());		  	      			
											mapCount++;
										}else{
											Integer noOEhr=1;
											
											
											schoolInJobOrder2.setJobId(newCatJobId);								
											schoolInJobOrder2.setSchoolId(schoolLocExistMap.get(Loc_Num));		  	      			
											schoolInJobOrder2.setNoOfSchoolExpHires(noOEhr);
											schoolInJobOrder2.setCreatedDateTime(new Date());
											statelesSsession.insert(schoolInJobOrder2);
											mapCount++;
										}
											schoolInJobOrderMap.put(newCatJobId.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId(),schoolInJobOrder2);		  	      		
									}
										
									if(school_in_job_order2){
									   
										if(schoolInJobOrderMap.get(newCatJobId.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId())!=null){
											Integer noOEhr=null;
											Integer noOEhrs=null;	
											noOEhrs=schoolInJobOrderMap.get(newCatJobId.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId()).getNoOfSchoolExpHires();
											
											
											if(noOEhrs<0){
												noOEhrs=0;
											}
											
											noOEhr = noOEhrs+1;
											
											Integer row_id=schoolInJobOrderMap.get(newCatJobId.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId()).getJobOrderSchoolId();
											
											schoolInJobOrder2.setJobOrderSchoolId(row_id);
											schoolInJobOrder2.setJobId(newCatJobId);	
											schoolInJobOrder2.setSchoolId(schoolLocExistMap.get(Loc_Num));		  	      			
											schoolInJobOrder2.setNoOfSchoolExpHires(noOEhr);
											schoolInJobOrder2.setCreatedDateTime(new Date());
											statelesSsession.update(schoolInJobOrder2);
											schoolInJobOrderMap.remove(newCatJobId.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId());		  	      			
											mapCount++;
										}else{						        				
											
											schoolInJobOrder2.setJobId(newCatJobId);								
											schoolInJobOrder2.setSchoolId(schoolLocExistMap.get(Loc_Num));		  	      			
											schoolInJobOrder2.setNoOfSchoolExpHires(1);
											schoolInJobOrder2.setCreatedDateTime(new Date());
											statelesSsession.insert(schoolInJobOrder2);
											mapCount++;
										}
										schoolInJobOrderMap.put(newCatJobId.getJobId()+"||"+schoolLocExistMap.get(Loc_Num).getSchoolId(),schoolInJobOrder2);
									}
											
								}
							}
						}
						}//job for loop end
					}					
				}
				
				if(!Position_ID.equalsIgnoreCase("")){
				requisitionNumber_store+="||"+Position_ID+"||";
				}
				
				if(!errorText.equals("")){
					int row = i+1;
					final_error_store+="Row "+row+" : "+errorText+"\r\n"+run_date+","+run_time+","+Position_ID+","+Loc_Num+","+Job_Code+","+Job_Category+","+Expiration_Date+","+pos_type+"<>";
				}
			}
		} // i for loop end
			 
			 
			 txOpen.commit();
			 statelesSsession.close();
			 sb.append("\n:::::::  database level  ::::::");
			//boolean notDeleteRec = districtRequisitionNumbersDAO.updateDeleteFlag(deleteFlagUpdateStore);
			
			//districtRequisitionNumbersDAO.updateSchoolInJoborder(allDeleteRecords);
			//districtRequisitionNumbersDAO.deleteFlagRecords();
			//districtRequisitionNumbersDAO.emptyDeleteFlag(null,districtMaster);
			 
			deleteXlsAndXlsxFile(filePath);
			
			Date d =new Date();
			System.out.println("Time: "+d.getTime());
			
			if(!final_error_store.equalsIgnoreCase("")){
			String content = final_error_store;		    				
			File file = new File(folderPath+"excelUploadError"+d.getTime()+".txt");
			
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			String[] parts = content.split("<>");	    				
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("run_date,run_time,Position_ID,Loc_Num,Job_Code,Job_Category,Expiration_Date,pos_type");
			bw.write("\r\n\r\n");
			int k =0;
			for(String cont :parts) {
				bw.write(cont+"\r\n\r\n");
			    k++;
			}
			bw.close();
			sb.append("\n------------------Complete------------");
			String to= "gourav@netsutra.com";
			String subject = "Job Upload Error";
			String msg = "Error in upload excel";
			String from= "gourav@netsutra.com";
			String errorFileName=file.toString();
			//String file_txt = Utility.getValueOfPropByKey("teacherRootPath")+"uploadvacancy/excelUploadError.txt";
				
				emailerService.sendMailWithAttachments(to,subject,from,msg,errorFileName);
			}
			sb.append("\nNew records    "+insertCount);
			arr[1] = sb.toString();
			arr[2] = "\n"+insertCount+" New positions added";
			sb.append("\ndelete start");
			arr[0] = "noproblem";
					return arr;
				    
		}catch (Exception e){
				e.printStackTrace();
			}
		arr[0] = "problem";
		arr[1] = sb.toString();
				return arr;
	}
	
	public String[] deleteRecords(String folderPath){
		
		StringBuffer sb = new StringBuffer();
		String[] arr = new String[3];
		Criterion dltFlag = Restrictions.eq("deleteFlag", "D");
	Criterion uploadFrom = Restrictions.eq("uploadFrom", "E");
	List<DistrictRequisitionNumbers> allDeleteRecords = districtRequisitionNumbersDAO.findByCriteria(uploadFrom,dltFlag);
	
	if(allDeleteRecords.size()!=0){
		Map<Integer,DistrictRequisitionNumbers>requisitionNumberMap=new HashMap<Integer, DistrictRequisitionNumbers>();
		 for(DistrictRequisitionNumbers requisitionnumber:allDeleteRecords)
		 {	
			 requisitionNumberMap.put(requisitionnumber.getDistrictRequisitionId(), requisitionnumber);
		 }
		 
		
		 List<JobRequisitionNumbers> jobreqDlt=null;
		Criterion allDeleteRecordsCr = Restrictions.in("districtRequisitionNumbers", allDeleteRecords);
	if(allDeleteRecords.size()>0){
		jobreqDlt = jobRequisitionNumbersDAO.findByCriteria(allDeleteRecordsCr);
	}
	
	
	
	 List<JobOrder> jobOrders= new ArrayList<JobOrder>();
	 Map<String, Integer> listOfReqWithMultipleJobMap = new HashMap<String, Integer>();
	 Map<String, Boolean> listOfReqIsHiredJobMap = new HashMap<String, Boolean>();
		for (JobRequisitionNumbers jobRequisitionNumbers : jobreqDlt) {
			jobOrders.add(jobRequisitionNumbers.getJobOrder());
			 if(jobRequisitionNumbers.getStatus().intValue()==1){
				 listOfReqIsHiredJobMap.put(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber(), true);					
			 }else{
				 listOfReqIsHiredJobMap.put(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber(), false);
			 }

			 Integer noOfrecs=0;			 
			if(listOfReqWithMultipleJobMap.containsKey(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber())){
				 noOfrecs=listOfReqWithMultipleJobMap.get(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber())+1;
				 listOfReqWithMultipleJobMap.remove(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber());
				 listOfReqWithMultipleJobMap.put(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber(),noOfrecs);
			 }else{
				 noOfrecs=1;
				 listOfReqWithMultipleJobMap.put(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber(),noOfrecs);
			 }
		}	
		
	Criterion criteria1 = Restrictions.in("jobId", jobOrders);
	List<SchoolInJobOrder> allRecInSchool = null;
	if(jobOrders.size()>0){
		allRecInSchool = schoolInJobOrderDAO.findByCriteria(criteria1);
	}
	
	Map<String, SchoolInJobOrder> schoolInJobOrderMap = new HashMap<String, SchoolInJobOrder>();
	String jobAndsch=null;
	if(allRecInSchool!=null && allRecInSchool.size()!=0){				
		for(SchoolInJobOrder schoolInJO :allRecInSchool){
			jobAndsch=schoolInJO.getJobId().getJobId()+"||"+schoolInJO.getSchoolId().getSchoolId();
			schoolInJobOrderMap.put(jobAndsch, schoolInJO);
		}
	}
	
	SessionFactory sessionFactory=jobRequisitionNumbersDAO.getSessionFactory();
	StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	Transaction txOpen =statelesSsession.beginTransaction();
	
	String req_store="";
	String del_req_store="";
	int i=0;
	try {
		for (JobRequisitionNumbers jobRequisitionNumbers : jobreqDlt) {				
			if(listOfReqWithMultipleJobMap.get(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber())!=null && listOfReqWithMultipleJobMap.get(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber()).equals(1)){
				/*	if(listOfReqWithMultipleJobMap.get(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber())>1){
					// listOfReqIsHiredJobMap
					if(jobRequisitionNumbers.getStatus()==0){
					sb.append("\nmultiple delete      "+jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber()+"  "+jobRequisitionNumbers.getJobRequisitionId());
					}
					
				}else{*/
					if(jobRequisitionNumbers.getStatus()==0){
						DistrictRequisitionNumbers districtRequisitionNumbers = new DistrictRequisitionNumbers();
						
						Integer noOEhrs=0;
						if(schoolInJobOrderMap.get(jobRequisitionNumbers.getJobOrder().getJobId()+"||"+jobRequisitionNumbers.getSchoolMaster().getSchoolId())!=null){
					
						SchoolInJobOrder schoolInJobOrder =new SchoolInJobOrder();
						schoolInJobOrder=schoolInJobOrderMap.get(jobRequisitionNumbers.getJobOrder().getJobId()+"||"+jobRequisitionNumbers.getSchoolMaster().getSchoolId());
						
						noOEhrs=schoolInJobOrder.getNoOfSchoolExpHires();
						noOEhrs = noOEhrs-1;					
						
						//if(noOEhrs>0){
							schoolInJobOrder.setNoOfSchoolExpHires(noOEhrs);
							statelesSsession.update(schoolInJobOrder);
							schoolInJobOrderMap.remove(jobRequisitionNumbers.getJobOrder().getJobId()+"||"+jobRequisitionNumbers.getSchoolMaster().getSchoolId());
							schoolInJobOrderMap.put(jobRequisitionNumbers.getJobOrder().getJobId()+"||"+jobRequisitionNumbers.getSchoolMaster().getSchoolId(),schoolInJobOrder);
						//}
						/*else{
							schoolInJobOrderMap.remove(jobRequisitionNumbers.getJobOrder().getJobId()+"||"+jobRequisitionNumbers.getSchoolMaster().getSchoolId());
							statelesSsession.delete(schoolInJobOrder);  	      			
						}*/
					}
					
					JobRequisitionNumbers jobRequisitionNumbers2 =new JobRequisitionNumbers();				
					jobRequisitionNumbers2.setDistrictRequisitionNumbers(jobRequisitionNumbers.getDistrictRequisitionNumbers());
					jobRequisitionNumbers2.setJobOrder(jobRequisitionNumbers.getJobOrder());
					jobRequisitionNumbers2.setJobRequisitionId(jobRequisitionNumbers.getJobRequisitionId());
					jobRequisitionNumbers2.setSchoolMaster(jobRequisitionNumbers.getSchoolMaster());
					jobRequisitionNumbers2.setStatus(jobRequisitionNumbers.getStatus());
					statelesSsession.delete(jobRequisitionNumbers2);
				
					if(listOfReqWithMultipleJobMap.get(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber())!=null){
						
						Integer delreq = listOfReqWithMultipleJobMap.get(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber())-1;
						
						listOfReqWithMultipleJobMap.remove(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber());
						listOfReqWithMultipleJobMap.put(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber(),delreq);
						
						if(delreq==0 && !listOfReqIsHiredJobMap.get(jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber())){
							DistrictRequisitionNumbers districtRequisitionNumbers2 = new DistrictRequisitionNumbers();
							 districtRequisitionNumbers2=requisitionNumberMap.get(jobRequisitionNumbers.getDistrictRequisitionNumbers().getDistrictRequisitionId());
							 statelesSsession.delete(districtRequisitionNumbers2);
							 req_store+="Delete\r\n"+districtRequisitionNumbers2.getRequisitionNumber()+"<>";
							 del_req_store+=","+districtRequisitionNumbers2.getRequisitionNumber();
							 i++;
						}
					}
						
					}
				}
				
			/*}*/
			
			}
	} catch (Exception e) {
		// TODO: handle exception
	}
	
	//allDeleteRecords
	
	/*for(DistrictRequisitionNumbers requisitionnumberSdel:allDeleteRecords)
	 {	
		if(!requisitionnumberSdel.getIsUsed()){
			DistrictRequisitionNumbers districtRequisitionNumbers3 = new DistrictRequisitionNumbers();
			districtRequisitionNumbers3.setDistrictRequisitionId(requisitionnumberSdel.getDistrictRequisitionId());	  		  	      		
	  		statelesSsession.delete(districtRequisitionNumbers3);
	  		req_store+="Delete\r\n"+districtRequisitionNumbers3.getRequisitionNumber()+"<>";
	  		i++;
		}
	 }*/
	
	 txOpen.commit();
	 statelesSsession.close();
	 sb.append("\nAll delete position : "+del_req_store);
	 arr[1] = "All delete position : "+del_req_store;
	String content = req_store;	
	Date d =new Date();
	File file = new File(folderPath+"deleteRequisition"+d.getTime()+".txt");
	
	// if file doesnt exists, then create it
	try{
		if(i>0){
	if (!file.exists()) {
		file.createNewFile();
	}
	String[] parts = content.split("<>");	    				
	FileWriter fw = new FileWriter(file.getAbsoluteFile());
	BufferedWriter bw = new BufferedWriter(fw);
	bw.write("Position_ID");
	bw.write("\r\n\r\n");
	int k =0;
	for(String cont :parts) {
		bw.write(cont+"\r\n\r\n");
	    k++;
	}
	bw.close(); 
		sb.append("\nTotal delete   "+i);
		arr[2] = k+" Existing positions removed";
		arr[0]="success";
	String to= "gourav@netsutra.com";
	String subject = "Deleted Position Numbers";
	String msg = "Deleted position numbers in file";
	String from= "gourav@netsutra.com";
	String errorFileName=file.toString();
	//String file_txt = Utility.getValueOfPropByKey("teacherRootPath")+"uploadvacancy/excelUploadError.txt";
		
		emailerService.sendMailWithAttachments(to,subject,from,msg,errorFileName);
		}
		return arr;
	}catch (Exception e) {
		// TODO: handle exception
			}
	 	 	
		}
		arr[0] = "problem";
		arr[1]="";
		arr[2]="";
		return arr;
		
	}
	
	public static Vector readDataExcelCSV(String fileName) throws FileNotFoundException{
				
				Vector vectorData = new Vector();
				String filepathhdr=fileName;
	//			String filepathhdr="E:/Teachermatch/teacher/uploadjob/TM_instructional_vacancies_20140714-0706_.csv";
	
	int rowshdr=0;
	int run_date_count=11;
	int run_time_count=11;
	int Position_ID_count=11;
	int Loc_Num_count=11; 
	int Job_Code_count=11;	        
	int Job_Category_count=11;
	int Expiration_Date_count=11;
	int pos_type_count=11;
	
	    BufferedReader brhdr=new BufferedReader(new FileReader(filepathhdr));
	    
	    String strLineHdr="";	        
	String hdrstr="";
	
	StringTokenizer sthdr=null;
	StringTokenizer stdtl=null;
	int lineNumberHdr=0;
	try{
	    String indexCheck="";
	while((strLineHdr=brhdr.readLine())!=null){               
		Vector vectorCellEachRowData = new Vector();
	    int cIndex=0;
	    boolean cellFlag=false,dateFlag=false;
	    Map<String,String> mapCell = new TreeMap<String, String>();
	    int i=1;
	    
	    String[] parts = strLineHdr.split(",");
	
	for(int j=0; j<parts.length; j++) {
		hdrstr=parts[j];
		cIndex=j;
		
	        
	        if(hdrstr.toString().trim().equalsIgnoreCase("run_date")){
	run_date_count=cIndex;
	indexCheck+="||"+cIndex+"||";
	}
	
	if(run_date_count==cIndex){
		cellFlag=true;
	}
	
	if(hdrstr.toString().trim().equalsIgnoreCase("run_time")){
	run_time_count=cIndex;
	indexCheck+="||"+cIndex+"||";
	}
	if(run_time_count==cIndex){
		cellFlag=true;
		
	}	
	  
	if(hdrstr.toString().trim().equalsIgnoreCase("Position_ID")){
	Position_ID_count=cIndex;
	indexCheck+="||"+cIndex+"||";
	}
	if(Position_ID_count==cIndex){
		cellFlag=true;
	}
	
	if(hdrstr.toString().trim().equalsIgnoreCase("Loc_Num")){
	Loc_Num_count=cIndex;
	indexCheck+="||"+cIndex+"||";
	}
	if(Loc_Num_count==cIndex){
		cellFlag=true;
	}
	
	if(hdrstr.toString().trim().equalsIgnoreCase("Job_Code")){
	Job_Code_count=cIndex;
	indexCheck+="||"+cIndex+"||";
	}
	if(Job_Code_count==cIndex){
		cellFlag=true;
	}
	
	if(hdrstr.toString().trim().equalsIgnoreCase("Job_Category")){
	Job_Category_count=cIndex;
	indexCheck+="||"+cIndex+"||";
	}
	if(Job_Category_count==cIndex){
		cellFlag=true;
	}
	if(hdrstr.toString().trim().equalsIgnoreCase("Expiration_Date")){
	Expiration_Date_count=cIndex;
	indexCheck+="||"+cIndex+"||";
	}
	if(Expiration_Date_count==cIndex){
		cellFlag=true;
	}
	
	if(hdrstr.toString().trim().equalsIgnoreCase("pos_type")){
	pos_type_count=cIndex;
	indexCheck+="||"+cIndex+"||";
	}
	if(pos_type_count==cIndex){
		cellFlag=true;
		
	}
	
	
	if(cellFlag){
		if(!dateFlag){
			try{
				mapCell.put(cIndex+"",hdrstr);
	}catch(Exception e){
		mapCell.put(cIndex+"",hdrstr);
		}
	}else{
		mapCell.put(cIndex+"",hdrstr);
	            		}
	            	}
	                
	            }
	            if(mapCell.size()==8){
		            vectorCellEachRowData=cellValuePopulate(mapCell);
		            vectorData.addElement(vectorCellEachRowData);
	            }
	        lineNumberHdr++;
	    }
	    brhdr.close();
	}
	
	catch(Exception e){
		System.out.println(e.getMessage());
	}
	System.out.println("read data csv complete");       
		return vectorData;
	}
	
	public static Vector cellValuePopulate(Map<String,String> mapCell){
	 Vector vectorCellEachRowData = new Vector();
	 Map<String,String> mapCellTemp = new TreeMap<String, String>();
	 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false;
	 for(Map.Entry<String, String> entry : mapCell.entrySet()){
		 String key=entry.getKey();
		String cellValue=null;
		if(entry.getValue()!=null)
			cellValue=entry.getValue().trim();
		
		if(key.equals("0")){
		mapCellTemp.put(key, cellValue);
		flag0=true;
	}
	if(key.equals("1")){
		flag1=true;
		mapCellTemp.put(key, cellValue);
	}
	if(key.equals("2")){
		flag2=true;
		mapCellTemp.put(key, cellValue);
	}
	if(key.equals("3")){
		flag3=true;
		mapCellTemp.put(key, cellValue);
	}
	if(key.equals("4")){
		flag4=true;
		mapCellTemp.put(key, cellValue);
	}
	if(key.equals("5")){
		flag5=true;
		mapCellTemp.put(key, cellValue);
	}
	if(key.equals("6")){
		flag6=true;
		mapCellTemp.put(key, cellValue);
	}
	if(key.equals("7")){
			flag7=true;
			mapCellTemp.put(key, cellValue);
		}
	 }
	 if(flag0==false){
		 mapCellTemp.put(0+"", "");
	 }
	 if(flag1==false){
		 mapCellTemp.put(1+"", "");
	 }
	 if(flag2==false){
		 mapCellTemp.put(2+"", "");
	 }
	 if(flag3==false){
		 mapCellTemp.put(3+"", "");
	 }
	 if(flag4==false){
		 mapCellTemp.put(4+"", "");
	 }
	 if(flag5==false){
		 mapCellTemp.put(5+"", "");
	 }
	 if(flag6==false){
		 mapCellTemp.put(6+"", "");
	 }
	 if(flag7==false){
		 mapCellTemp.put(7+"", "");
		 }
		 			 
		 for(Map.Entry<String, String> entry : mapCellTemp.entrySet()){
			 vectorCellEachRowData.addElement(entry.getValue());
		 }
		 return vectorCellEachRowData;
	}
	 
	public String deleteXlsAndXlsxFile(String fileName){
	
	String filePath="";
	File file=null;
	String returnDel="";
	try{			
		filePath=fileName;
		file = new File(filePath);
		file.delete();
		returnDel="deleted";
		}catch(Exception e){}
		return returnDel;
	}
	
		public static String monthReplace(String date){
		String currentDate="";
	try{
		
	   if(date.contains("Jan")){
	   currentDate=date.replaceAll("Jan","01");
	   }
	   if(date.contains("Feb")){
	   currentDate=date.replaceAll("Feb","02");
	   }
	   if(date.contains("Mar")){
	   currentDate=date.replaceAll("Mar","03");
	   }
	   if(date.contains("Apr")){
	   currentDate=date.replaceAll("Apr","04");
	   }
	   if(date.contains("May")){
	   currentDate=date.replaceAll("May","05");
	   }
	   if(date.contains("Jun")){
	   currentDate=date.replaceAll("Jun","06");
	   }
	   if(date.contains("Jul")){
	   currentDate=date.replaceAll("Jul","07");
	   }
	   if(date.contains("Aug")){
	   currentDate=date.replaceAll("Aug","08");
	   }
	   if(date.contains("Sep")){
	   currentDate=date.replaceAll("Sep","09");
	   }
	   if(date.contains("Oct")){
	   currentDate=date.replaceAll("Oct","10");
	   }
	   if(date.contains("Nov")){
	   currentDate=date.replaceAll("Nov","11");
	   }
	   if(date.contains("Dec")){
	   currentDate=date.replaceAll("Dec","12");
	   }
	   String uDate=currentDate.replaceAll("-","/");
	   if(Utility.isDateValid(uDate,0)==2){
		   currentDate=date; 
	   }else{
		   try{
			   String dateSplit[]=uDate.split("/");
	   currentDate=dateSplit[1]+"/"+dateSplit[0]+"/"+dateSplit[2];
			   }catch(Exception e){
				   currentDate=date; 
			   }
		   }
		}catch(Exception e){
			currentDate=date;
		}
		   return currentDate;
	 }
	
		public static Date getCurrentDateFormart2(String oldDateString) throws ParseException{
	
			String OLD_FORMAT = "yyyy-MM-dd HH:mm:ss";
	String NEW_FORMAT = "yyyy-MM-dd HH:mm:ss";
		String newDateString;
		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date d = sdf.parse(oldDateString);
		sdf.applyPattern(NEW_FORMAT);
		newDateString = sdf.format(d);
	
		SimpleDateFormat stodate = new SimpleDateFormat(NEW_FORMAT);
		Date date = stodate.parse(newDateString);
		return date;
	}
	
	public static int isDateValidOnly(String date) 
	{
		String DATE_FORMAT ="mm-dd-yyyy";
	
	try {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		sdf.setLenient(false);
		sdf.parse(date);
		//sb.append(sdf.parse(date));
		            return 1;
		        } catch (Exception e) {
		            return 2;
		        }
			}
}
