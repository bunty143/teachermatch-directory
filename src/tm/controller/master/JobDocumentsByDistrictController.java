package tm.controller.master;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tm.bean.DistrictPanelMaster;
import tm.bean.DocumentFileMaster;
import tm.bean.JobForTeacher;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictPanelMasterDAO;
import tm.dao.DistrictSpecificDocumentsDAO;
import tm.dao.DocumentCategoryMasterDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.Utility;


@Controller
public class JobDocumentsByDistrictController 
{
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	
	@Autowired
	private DistrictSpecificDocumentsDAO districtSpecificDocumentsDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	
	@RequestMapping(value="/jobdocumentsbydistrict.do", method=RequestMethod.GET)
	public String jobCategoryGET(ModelMap map,HttpServletRequest request)
	{
		UserMaster userSession=null;
		try 
		{
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			map.addAttribute("entityID", entityID);
			map.addAttribute("userId",userSession.getUserId());
			map.addAttribute("dateTime",Utility.getDateTime());
			map.addAttribute("fileName","JobDocument"+Utility.getDateTime());
			System.out.println(" entity id :: "+entityID);
			
			if(userSession.getEntityType()==2){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				List<JobCategoryMaster> jobCategoryMasters = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(userSession.getDistrictId());
				System.out.println("Total Job Categories Get :: "+jobCategoryMasters.size());
				/*List<JobDocumentsByDistrict> jbDocList =new ArrayList<JobDocumentsByDistrict>();
				jbDocList = jobDocumentsByDistrictDAO.checkJobCategoryExists(userSession);
				if(jbDocList.size()==0)
				{					
					map.addAttribute("jobCategoryList", jobCategoryMasters);
					System.out.println(" Job Categories Get :: "+jobCategoryMasters.size());
				}
				else
				{
					List<JobCategoryMaster> jobCategoryUsedList =new ArrayList<JobCategoryMaster>();
					for(int i=0;i<jbDocList.size();i++)
					{
						jobCategoryUsedList.add(jbDocList.get(i).getJobCategoryMaster());
					}
					jobCategoryMasters.removeAll(jobCategoryUsedList);
					map.addAttribute("jobCategoryList", jobCategoryMasters);
					System.out.println(" Job Categories Get :: "+jobCategoryMasters.size());
				}	*/	
				map.addAttribute("jobCategoryList", jobCategoryMasters);
			}else if(userSession.getEntityType()==1){
				List<JobCategoryMaster> jobCategoryMasters = jobCategoryMasterDAO.findAll();
				map.addAttribute("jobCategoryList", jobCategoryMasters);
				//map.addAttribute("districtName",null);
			}			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		if(userSession.getEntityType()==3){
			return "signin";
		}else{
			return "jobdocumentsbydistrict";

		}	
	}
	
	
	@RequestMapping(value="/jobdocuments.do", method=RequestMethod.GET)
	public String jobdocumentsGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println("============= jobdocuments.do ===============");
		TeacherDetail teacherDetail = null;
		List<JobForTeacher> jobForTeachers = new ArrayList<JobForTeacher>();
		try 
		{
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("teacherDetail") == null){
				return "redirect:index.jsp";
			}
			teacherDetail		=	(TeacherDetail) session.getAttribute("teacherDetail");
			if(teacherDetail!=null)
			{
				map.addAttribute("teacherId", teacherDetail.getTeacherId());
				Criterion criterion = Restrictions.eq("teacherId", teacherDetail);
				jobForTeachers = jobForTeacherDAO.findByCriteria(criterion);
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
			return "jobdocuments";
	}
	
	
	
	@RequestMapping(value="/documentuploadbycandidate.do", method=RequestMethod.GET)
	public String jobdocumentsuploadByCandidateGET(ModelMap map,HttpServletRequest request)
	{
		TeacherDetail teacherDetail = null;
		List<JobForTeacher> jobForTeachers = new ArrayList<JobForTeacher>();
		try 
		{
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("teacherDetail") == null){
				return "redirect:index.jsp";
			}
			teacherDetail		=	(TeacherDetail) session.getAttribute("teacherDetail");
			if(teacherDetail!=null)
			{
				map.addAttribute("teacherId", teacherDetail.getTeacherId());
				Criterion criterion = Restrictions.eq("teacherId", teacherDetail);
				//jobForTeachers = jobForTeacherDAO.findByCriteria(criterion);
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
			return "documentuploadbycandidate";
	}
	

}
