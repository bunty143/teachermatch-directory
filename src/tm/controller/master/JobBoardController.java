package tm.controller.master;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tm.bean.master.MasterSubjectMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.SubjectMaster;
import tm.dao.master.MasterSubjectMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.utility.Utility;

@Controller
public class JobBoardController 
{
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	
	@Autowired 
	private SubjectMasterDAO subjectMasterDAO;
	public void setSubjectMasterDAO(SubjectMasterDAO subjectMasterDAO) {
		this.subjectMasterDAO = subjectMasterDAO;
	}
	
	@Autowired
	private MasterSubjectMasterDAO masterSubjectMasterDAO;
	public void setMasterSubjectMasterDAO(
			MasterSubjectMasterDAO masterSubjectMasterDAO) {
		this.masterSubjectMasterDAO = masterSubjectMasterDAO;
	}
	
	@RequestMapping(value="/questjobboard.do", method=RequestMethod.GET)
	public String jobBoardGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println("Calling questjobboard.do "+new Date());
		try 
		{
			Utility.setRefererURLByJobsBoard(request);
			List<SubjectMaster> lstSubjectMasters = subjectMasterDAO.findActiveSubject();
			List<MasterSubjectMaster> lstMasterSubjectMasters = masterSubjectMasterDAO.findAllActiveSuject();
			List<StateMaster> lstStateMasters = stateMasterDAO.findActiveState();
			map.addAttribute("lstSubjectMasters", lstSubjectMasters);
			map.addAttribute("lstMasterSubjectMasters", lstMasterSubjectMasters);
			map.addAttribute("lstStateMasters", lstStateMasters);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "questjobboard";
	}
	
	
	
	
	
}
