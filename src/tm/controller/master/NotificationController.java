package tm.controller.master;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class NotificationController {
	

	@RequestMapping(value="/notification.do", method=RequestMethod.GET)
	public String doMySchduleGET(ModelMap map,HttpServletRequest request)
	{
		
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null) 
			return "redirect:index.jsp";
		
		return "notification";
	}
}
