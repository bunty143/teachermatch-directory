package tm.controller.master;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tm.bean.user.UserMaster;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.Utility;


@Controller
public class ManageTagsController 
{
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(
			RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	/*============ Get Method of TeacherController ====================*/
	@RequestMapping(value="/managetags.do", method=RequestMethod.GET)
	public String tagsGET(ModelMap map,HttpServletRequest request)
	{
		UserMaster userSession=null;
		try 
		{
			//System.out.println("\n =========== jobcategory.do in TeacherController  ===============");
			HttpSession session = request.getSession(false);
			String hqId = null;
			
			
			
			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			//managetagsmethod(map,request,userMasterDAO);
			userSession		=	(UserMaster) session.getAttribute("userMaster");
			session.setAttribute("displayType", "0");
			map.addAttribute("userSession", userSession);
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("entityID", entityID);
			
			//for Kelly
			if(userSession.getHeadQuarterMaster()!=null && userSession.getHeadQuarterMaster().getHeadQuarterId()==1)
			{
				if(userSession.getBranchMaster()!=null)
				{
					map.addAttribute("branchId",userSession.getBranchMaster().getBranchId());
				}
				else
					map.addAttribute("hqId",userSession.getHeadQuarterMaster().getHeadQuarterId());
				map.addAttribute("districtName",null);
				map.addAttribute("schoolName",null);
			}else{
				if(userSession.getEntityType()!=1){
					map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
					map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				}else{
					map.addAttribute("districtName",null);
				}
				
				if(userSession.getEntityType()==2){
					map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
					map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
					map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
				}
				
				if(userSession.getEntityType()==3){
					map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
					map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
					map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
				}else{
					map.addAttribute("schoolName",null);
				}
			}
			
			
			//map.addAttribute("roleAccess", roleAccess);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		/*if(userSession.getEntityType()==3){
			return "signin";
		}else{*/
			return "managetags";

	//	}	
	}
	
	
	/*============ Get Method of Attachments ====================*/
	@RequestMapping(value="/managedocuments.do", method=RequestMethod.GET)
	public String attachmentsGET(ModelMap map,HttpServletRequest request)
	{
		UserMaster userSession=null;
		try 
		{
			//System.out.println("\n =========== jobcategory.do in TeacherController  ===============");
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			session.setAttribute("displayType","0");
			managedocumentsmethod(map,request,roleAccessPermissionDAO,userMasterDAO);
			map.addAttribute("kellyupdatecodelive", Utility.getLocaleValuePropByKey("kellyupdatecodelive", Utility.getValueOfPropByKey("locale")));
			/*userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("entityID", entityID);
			if(userSession.getEntityType()!=1){
				if(userSession.getDistrictId()!=null){
					map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
					map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				}
				if(userSession.getSchoolId()!=null){
					map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
					map.addAttribute("schoolId",userSession.getSchoolId().getSchoolId());
				}
			}else{
				map.addAttribute("districtName",null);
			}
			
			if(userSession.getEntityType()==2){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
			}
			
			if(userSession.getEntityType()==3){
				map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("schoolId",userSession.getSchoolId().getSchoolId());
				map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
			}else{
				map.addAttribute("schoolName",null);
			}
			
			if(userSession.getEntityType()==5){
				map.addAttribute("headQuarterName",userSession.getHeadQuarterMaster().getHeadQuarterName());
				map.addAttribute("headQuarterId",userSession.getHeadQuarterMaster().getHeadQuarterId());
				map.addAttribute("activeuser",userMasterDAO.getUserByHqBr(userSession.getHeadQuarterMaster(),null,userSession.getEntityType()));
			}
			if(userSession.getEntityType()==6){
				map.addAttribute("branchName",userSession.getBranchMaster().getBranchName());
				map.addAttribute("branchId",userSession.getBranchMaster().getBranchId());
				map.addAttribute("activeuser",userMasterDAO.getUserByHqBr(null,userSession.getBranchMaster(),userSession.getEntityType()));
			}*/
			//map.addAttribute("roleAccess", roleAccess);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
			return "managedocuments";

		
	}
	//******************** Adding By Deepak *****************************************
	public synchronized static void managetagsmethod(ModelMap map,HttpServletRequest request,UserMasterDAO userMasterDAO)
	{
		
			UserMaster userSession=null;
			HttpSession session = request.getSession(false);
			userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			/*try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}*/
			map.addAttribute("entityID", entityID);
			if(userSession.getEntityType()!=1){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("districtName",null);
			}
			
			if(userSession.getEntityType()==2){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
			}
			
			if(userSession.getEntityType()==3){
				map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
				map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
			}else{
				map.addAttribute("schoolName",null);
			}
	}
	//******************** Adding By Deepak *****************************
	public synchronized static void managedocumentsmethod(ModelMap map,HttpServletRequest request,RoleAccessPermissionDAO roleAccessPermissionDAO,UserMasterDAO userMasterDAO)
	{
		UserMaster userSession=null;

			HttpSession session = request.getSession(false);			

			userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			/*try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}*/
			map.addAttribute("entityID", entityID);
			if(userSession.getEntityType()!=1){
				if(userSession.getDistrictId()!=null){
					map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
					map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				}
				if(userSession.getSchoolId()!=null){
					map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
					map.addAttribute("schoolId",userSession.getSchoolId().getSchoolId());
				}
			}else{
				map.addAttribute("districtName",null);
			}
			
			if(userSession.getEntityType()==2){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
			}
			
			if(userSession.getEntityType()==3){
				map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("schoolId",userSession.getSchoolId().getSchoolId());
				map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
			}else{
				map.addAttribute("schoolName",null);
			}
			
			if(userSession.getEntityType()==5){
				map.addAttribute("headQuarterName",userSession.getHeadQuarterMaster().getHeadQuarterName());
				map.addAttribute("headQuarterId",userSession.getHeadQuarterMaster().getHeadQuarterId());
				map.addAttribute("activeuser",userMasterDAO.getUserByHqBr(userSession.getHeadQuarterMaster(),null,userSession.getEntityType()));
			}
			if(userSession.getEntityType()==6){
				map.addAttribute("branchName",userSession.getBranchMaster().getBranchName());
				map.addAttribute("branchId",userSession.getBranchMaster().getBranchId());
				map.addAttribute("activeuser",userMasterDAO.getUserByHqBr(null,userSession.getBranchMaster(),userSession.getEntityType()));
			}
	}
}
