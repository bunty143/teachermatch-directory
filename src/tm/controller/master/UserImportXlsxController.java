package tm.controller.master;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.EmployeeMaster;
import tm.bean.FileUploadHistory;
import tm.bean.UserUploadFolderAccess;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.EmployeeMasterDAO;
import tm.dao.FileUploadHistoryDAO;
import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.utility.Utility;

@Controller
public class UserImportXlsxController {
	
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	public void setUserUploadFolderAccessDAO(UserUploadFolderAccessDAO userUploadFolderAccessDAO) {
		this.userUploadFolderAccessDAO = userUploadFolderAccessDAO;
	}
	
	@Autowired
	private FileUploadHistoryDAO fileUploadHistoryDAO;
	public void setFileUploadHistoryDAO(FileUploadHistoryDAO fileUploadHistoryDAO){
		this.fileUploadHistoryDAO = fileUploadHistoryDAO;
	}
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService){
        this.emailerService = emailerService;
    }
	
	@Autowired
	private RoleMasterDAO roleMasterDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	
	@RequestMapping(value="/accessfileupload.do", method=RequestMethod.GET)
	public String getallactivedistrict(ModelMap map,HttpServletRequest request)
	{
		System.out.println("::::: Use UserImportXlsxController controller :::::");
				
		List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
		
		try {
			Criterion functionality = Restrictions.eq("functionality", "userUpload");
			Criterion active = Restrictions.eq("status", "A");
			uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		int i=0;
		for (UserUploadFolderAccess getRec : uploadFolderAccessesrec) {
			Integer districtId = getRec.getDistrictId().getDistrictId();
			File filesList = findLatestFilesByPath(getRec.getFolderPath());
			System.out.println("filesList   "+filesList);
			if(filesList==null){
				String to= "gourav@netsutra.com";
			
				String subject = Utility.getLocaleValuePropByKey("TeacherMatchAccessFile_file1", locale);
				
				String msg = Utility.getLocaleValuePropByKey("msgNotFoundFileUpload1", locale);;						
				String from= "gourav@netsutra.com";
				
				//Debug.println("filenotfound", msg);
				System.out.println("filenotfound  "+msg);
				emailerService.sendMail(to, subject, msg);
			}else{
				System.out.println("call");
				try {
					String FileName = filesList.getName();
					Integer rowId = getRec.getUseuploadFolderId();
					String folderPath = getRec.getFolderPath();
					String lastUploadDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(filesList.lastModified()));
					
					savejob(FileName,districtId,rowId,folderPath,lastUploadDateTime);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}  
			if(i==1){
				break;
			}
		}
		return null;
	}
	
	public File findLatestFilesByPath(String path)
    {
        String pathToScan = path;
        File dir = new File(pathToScan);
        //File files[] = dir.listFiles();
        
        File[] files2 = dir.listFiles( new FilenameFilter() {
            public boolean accept( File dir,String name ) {
            	return name.matches( "TeacherMatchAccessFile.*\\.csv" ) || name.matches( "TeacherMatchAccessFile.*\\.CSV" );
            }
        } );
        
        if(files2.length == 0)
            return null;
        
            File lastModifiedFile = files2[0];
        for(int i = 1; i < files2.length; i++){
	            if(lastModifiedFile.lastModified() < files2[i].lastModified()){	            	
	                lastModifiedFile = files2[i];	            	
	            }	            
        	/*}else{
        		lastModifiedFile = null;
        	}*/
        }       
        		return lastModifiedFile;
    }
		
	 private Vector vectorDataExcelXLSX = new Vector();
	 public String savejob(String fileName,Integer districtId,Integer rowId,String folderPath,String lastUploadDateTime)
			{	
		int district_Id = districtId;
		System.out.println("::::::::saveJob:::::::::"+district_Id);
		
		Map<String,UserMaster> userMasterMap = new HashMap<String, UserMaster>();
		String returnVal="";
	    try{
	    	 
	    	 String filePath=folderPath+fileName;
	    	 System.out.println("folderPath  "+folderPath);
	    	 try {
	    		 if(fileName.contains(".csv") || fileName.contains(".CSV")){
		    		 vectorDataExcelXLSX = readDataExcelCSV(filePath);
		    	 }else{
		    			 //break;
		    	 }
			} catch (Exception e) {
				e.printStackTrace();
			}
	    	 
	    	 int Loc_count=11;	    	 
	    	 int LocationName_count=11;
	    	 int AccessLevel_count=11;
	    	 int Emp_count=11; 
	    	 int EmployeeName_count=11;
	    	 
	    	 String Emp_store="";
	    	 
	    	 boolean row_error=false;
				
				SessionFactory sessionFactory=userMasterDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	     	    Transaction txOpen =statelesSsession.beginTransaction();
	     	    
	     	   DistrictMaster districtMaster=districtMasterDAO.findById(district_Id, false, false);
	     	   
	     	  Criterion districtMaster_districtID = Restrictions.eq("districtMaster", districtMaster);
	     	   List<EmployeeMaster> EmployeeMaster= employeeMasterDAO.findByCriteria(districtMaster_districtID);
	     	   
	     	   Map<String, EmployeeMaster>EmployeeMasterMap = new HashMap<String, EmployeeMaster>();
				 for(EmployeeMaster employeeList:EmployeeMaster){
					 EmployeeMasterMap.put(employeeList.getEmployeeCode(), employeeList);
				 }
				 
				 Criterion districtID = Restrictions.eq("districtId", districtMaster);
				 List<SchoolMaster> schoolLoc = schoolMasterDAO.findByCriteria(districtID);
				 
				 Map<String, SchoolMaster>schoolLocExistMap = new HashMap<String, SchoolMaster>();
				 for (SchoolMaster school : schoolLoc) {
					 schoolLocExistMap.put(school.getLocationCode(), school);
				}
				 
				 List<RoleMaster> roleId = roleMasterDAO.findAll();				 
				 Map<Integer, RoleMaster>roleMasterMap = new HashMap<Integer, RoleMaster>();
				 for (RoleMaster role : roleId) {
					 roleMasterMap.put(role.getRoleId(), role);
				}
				 
				 List<UserMaster> usersList = userMasterDAO.findByCriteria(districtID);
				 Map<String, UserMaster> usersListMap= new HashMap<String, UserMaster>();
				 String userexist=null; 
				 String scoolid=null;
				 String employeeid=null;
				 String roleid="";
				 for (UserMaster userMaster : usersList) {
					 if(userMaster.getSchoolId()==null)
						 scoolid="null";
					 else
						 scoolid=userMaster.getSchoolId().getSchoolId()+"";					 
					 if(userMaster.getEmployeeMaster()==null)
						 employeeid="null";
					 else
						 employeeid=userMaster.getEmployeeMaster().getEmployeeId()+"";
					 if(userMaster.getRoleId()==null)
						 roleid="null";
					 else
						 roleid=userMaster.getRoleId().getRoleId()+"";
										 
					 userexist=scoolid+"||"+employeeid+"||"+roleid;					 
					 usersListMap.put(userexist, userMaster);
				}
				 
				 String scoolid2=null;
				 String employeeid2=null;
				 for (UserMaster userMaster2 : usersList) {
					 if(userMaster2.getSchoolId()==null)
						 scoolid2="null";
					 else
						 scoolid2=userMaster2.getSchoolId().getSchoolId()+"";					 
					 if(userMaster2.getEmployeeMaster()==null)
						 employeeid2="null";
					 else
						 employeeid2=userMaster2.getEmployeeMaster().getEmployeeId()+"";
					 userMasterMap.put(scoolid2+"||"+employeeid2,userMaster2);
				}
				 
				 
				 
				/* Map<String, UserMaster> usersEmpChkMap= new HashMap<String, UserMaster>();				 
				 for (UserMaster userMasterEmpChk : usersList) {
					 usersEmpChkMap.put(userMasterEmpChk.getEmployeeMaster().getEmployeeId()+"", userMasterEmpChk);
				}*/
				 
		        int mapCount=0;
		        int updateCount=0;
		        int createCount=0;
		        int numOfError=0;
		        String final_error_store="";
		        
			 for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
				 Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
		            String Loc="";
		            String LocationName="";
		            String AccessLevel="";
		            String Emp=""; 
		            String EmployeeName="";
		  	        //String read_write_previlege="";
		  	      String errorText="";
		  	      String rowErrorText="";
		  	      
		  	    for(int j=0; j<vectorCellEachRowData.size(); j++) {
	  	        	try{
	  	        		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Loc")){
	  	        			Loc_count=j;
		            	}
		            	if(Loc_count==j)
		            		Loc=vectorCellEachRowData.get(j).toString().trim();
	  	        		
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Access Level")){
		            		AccessLevel_count=j;
		            	}
		            	if(AccessLevel_count==j)
		            		AccessLevel=vectorCellEachRowData.get(j).toString().trim();
		            	
		            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Emp. #")){
		            		Emp_count=j;
								}
		            	if(Emp_count==j)
		            		Emp=vectorCellEachRowData.get(j).toString().trim();
	            	}catch(Exception e){}
	  	        }

			  	  if(i==0){
			  		  
			  		  if(!Loc.equalsIgnoreCase("Loc")){
			  			rowErrorText=Utility.getLocaleValuePropByKey("msgLocColumnNotFound1", locale);;
			  			row_error=true;
			  		  }
			  		  if(!AccessLevel.equalsIgnoreCase("Access Level")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound2", locale);
			  			row_error=true;
			  		  }
			  		  if(!Emp.equalsIgnoreCase("Emp. #")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound3", locale);
			  			row_error=true;  
			  		  }
			  		 }
			  	  
			  	 if(row_error){
			  		numOfError++;
			  		File file = new File(folderPath+"excelUploadError.txt");
					// if file doesnt exists, then create it
					if (!file.exists()) {
						file.createNewFile();
					}
						    				
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(rowErrorText);
	
					bw.close();
			  		
					String to= "gourav@netsutra.com";
					String subject = "TeacherMatchAccessFile_ Upload Error";
					String msg = "Error in "+fileName+" file";
					String from= "gourav@netsutra.com";
					String errorFileName="excelUploadError.txt";			
					emailerService.sendMailWithAttachments(to,subject,from,msg,folderPath+errorFileName);
					
			  		 break;
			  	 }
			  	  
		  	    if(i!=0 && row_error==false){
		  	    	boolean errorFlag=false;
		  	    	Long School_Id=null;
		  	    	
/*		  	    	if(Loc.length()==3)
		  	    		Loc= "0"+Loc;
		  	    	if(Loc.length()==2)
		  	    		Loc= "00"+Loc;
		  	    	if(Loc.length()==1)
		  	    		Loc= "000"+Loc;
*/		  	    	if(!Loc.equalsIgnoreCase("9999")){
	  	        	if(!Loc.equalsIgnoreCase("")){
		  	        	if(schoolLocExistMap.get(Loc)!=null && schoolLocExistMap.get(Loc).getDistrictId().getStatus().equalsIgnoreCase("I")){
		  	        		if(errorFlag){
	  							errorText+=",";	
	  						}
		  	        		errorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound4", locale);
		  	    			errorFlag=true;
		  	        	}else if(schoolLocExistMap.get(Loc)!=null && schoolLocExistMap.get(Loc).getDistrictId().getStatus().equalsIgnoreCase("A") && schoolLocExistMap.get(Loc).getStatus().equalsIgnoreCase("I")){
		  	        		if(errorFlag){
	  							errorText+=",";	
	  						}
		  	        		errorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound4", locale);
		  	    			errorFlag=true;
	  	        		}else if(schoolLocExistMap.get(Loc)!=null && schoolLocExistMap.get(Loc).getDistrictId().getStatus().equalsIgnoreCase("A") && schoolLocExistMap.get(Loc).getStatus().equalsIgnoreCase("A")){
		  	        		School_Id = schoolLocExistMap.get(Loc).getSchoolId();
		  	        		}else{
		  	        			if(errorFlag){
		  							errorText+=",";	
		  						}
		  	        		errorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound4", locale);
		  	    			errorFlag=true;
		  	        	}
  	        		
  	        		}else{
  	        			if(errorFlag){
  							errorText+=",";	
  						}
  	        			errorText+=Utility.getLocaleValuePropByKey("msgLocEmpty1", locale);
	  	    			errorFlag=true;
  	        		}
				}
	  	        	
	  	        	int entity_id=0;
	  	        	
	  	        	RoleMaster role_id=null;
	  	        	if(!AccessLevel.equalsIgnoreCase("")){
	  	        				  	        		
	  	        		if(AccessLevel.equals("DA")){
	  	        			role_id=roleMasterMap.get(2);
	  	        			entity_id=2;
	  	        		}else if(AccessLevel.equals("DT")){
	  	        			role_id=roleMasterMap.get(5);
	  	        			entity_id=2;
	  	        		}else if(AccessLevel.equals("OA")){
	  	        			role_id=roleMasterMap.get(7);
	  	        			entity_id=2;
	  	        		}else if(AccessLevel.equals("OT")){
	  	        			role_id=roleMasterMap.get(8);
	  	        			entity_id=2;
	  	        		}else if(AccessLevel.equals("SA")){
	  	        			role_id=roleMasterMap.get(3);
	  	        			entity_id=3;
	  	        		}else if(AccessLevel.equals("ST")){
	  	        			role_id=roleMasterMap.get(6);
	  	        			entity_id=3;
	  	        		}else if(AccessLevel.equals("OP")){
	  	        			role_id=roleMasterMap.get(9);
	  	        			entity_id=2;
	  	        		}
	  	        		else if(AccessLevel.equals("XX")){
	  	        			if(errorFlag){
	  							errorText+=",";	
	  						}
	  	        			errorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound5", locale);
		  	    			errorFlag=true;
	  	        		}
	  	        	}else{
	  	        		if(errorFlag){
  							errorText+=",";	
  						}
  	        			errorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound5", locale);
	  	    			errorFlag=true;
	  	        	}
	  	        	
	  	        	String first_name=null;
	  	        	String last_name=null;
	  	        	String middle_name=null;
	  	        	String email_address=null;
	  	        	EmployeeMaster employee_id=null;
	  	        	
	  	        	/*while (Emp.length() < 6) Emp = "0" + Emp;*/
	  	        	if(!Emp.equalsIgnoreCase("")){
	  	        		
	  	        		if(EmployeeMasterMap.size()>0){
	  	        			if(EmployeeMasterMap.containsKey(Emp)){
		  	        			if(EmployeeMasterMap.get(Emp).getEmailAddress().equals(""))
		  	        			{
		  	        				if(errorFlag){
		  	        					errorText+=",";	
		  	        				}
		  	        				errorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound6", locale);
		  	        				errorFlag=true;
		  	        			}
		  	        			else if(EmployeeMasterMap.get(Emp).getFirstName().equals(""))
		  	        			{
		  	        				if(errorFlag){
		  	        					errorText+=",";	
		  	        				}
		  	        				errorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound7", locale);
		  	        				errorFlag=true;
		  	        			}
		  	        			else if(EmployeeMasterMap.get(Emp).getLastName().equals(""))
		  	        			{
		  	        				if(errorFlag){
		  	        					errorText+=",";	
		  	        				}
		  	        				errorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound8", locale);
		  	        				errorFlag=true;
		  	        			}
		  	        			else{		  	        				
		  	        				email_address = EmployeeMasterMap.get(Emp).getEmailAddress().trim();
		  	        				first_name	  = EmployeeMasterMap.get(Emp).getFirstName().trim();
		  	        				last_name	  = EmployeeMasterMap.get(Emp).getLastName().trim();
		  	        				middle_name	  = EmployeeMasterMap.get(Emp).getMiddleName().trim();
		  	        				employee_id   = EmployeeMasterMap.get(Emp);		  	        				
			  	        		}
	  	        			}else{
	  	        				if(errorFlag){
	  	        					errorText+=",";	
	  	        				}
	  	        				errorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound9", locale);
	  	        				errorFlag=true;
	  	        			}
	  	        		}else{
	  	        			if(errorFlag){
	  							errorText+=",";	
	  						}
	  	        			errorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound9", locale);
		  	    			errorFlag=true;
	  	        		}
	  	        		
	  	        	}else{
	  	        		if(errorFlag){
  							errorText+=",";	
  						}
  	        			errorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound10", locale);;
	  	    			errorFlag=true;
	  	        	}
	  	        	 
	  	        	
	  	        	if(entity_id==3 && Loc.equals("9999")){
	  	        		if(errorFlag){
  							errorText+=",";	
  						}
  	        			errorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound11", locale);
	  	    			errorFlag=true;
	  	        	}
	  	        	
	  	        	if(entity_id==3 && Loc.equals("")){
	  	        		if(errorFlag){
  							errorText+=",";	
  						}
  	        			errorText+=Utility.getLocaleValuePropByKey("msgLocColumnNotFound11", locale);
	  	    			errorFlag=true;
	  	        	}
	  	        	
	  	        	if(!errorText.equals("")){
  	        			int row = i+1;
	  	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+Loc+","+Emp+","+AccessLevel+"<>";
	  	    			numOfError++;
	  	    		}
	  	        	UserMaster a1 = new UserMaster();
	  	        	
	  	        	String scoolid_sheet=null;
	  	        	UserMaster user_id=null;
	  	        	boolean updateFlag=false;
	  	        	boolean updateFlag2=false;
	  	        	boolean insertFlag=false;
	  	        	if(errorText.equals("")){
	  	        		if(Loc.equals("9999"))
	  	        			scoolid_sheet="null";
						 else
							 scoolid_sheet=schoolLocExistMap.get(Loc).getSchoolId()+"";
	  	        		
	  	        		
	  	        		String userAreExist=scoolid_sheet+"||"+employee_id.getEmployeeId()+"||"+role_id.getRoleId();
		  	        	/*if(usersListMap.containsKey(userAreExist)){
		  	        		System.out.println("update");
		  	        		//updateFlag=true;
		  	        		user_id = usersListMap.get(userAreExist);
		  	        	}else*/ 
	  	        		if(userMasterMap.containsKey(scoolid_sheet+"||"+employee_id.getEmployeeId())==true){
	  	        			if(!userMasterMap.get(scoolid_sheet+"||"+employee_id.getEmployeeId()).getRoleId().getRoleId().equals(role_id.getRoleId())){
	  	        				user_id = userMasterMap.get(scoolid_sheet+"||"+employee_id.getEmployeeId());
	  	        				updateFlag=true;
	  	        			}else if(userMasterMap.get(scoolid_sheet+"||"+employee_id.getEmployeeId()).getRoleId().getRoleId().equals(role_id.getRoleId())){
	  	        				
	  	        				if(!userMasterMap.get(scoolid_sheet+"||"+employee_id.getEmployeeId()).getFirstName().equalsIgnoreCase(first_name)){
	  	        					user_id = userMasterMap.get(scoolid_sheet+"||"+employee_id.getEmployeeId());
	  	        					updateFlag2=true;
	  	        				}else if(!userMasterMap.get(scoolid_sheet+"||"+employee_id.getEmployeeId()).getLastName().equalsIgnoreCase(last_name)){
	  	        					user_id = userMasterMap.get(scoolid_sheet+"||"+employee_id.getEmployeeId());
	  	        					updateFlag2=true;
	  	        				}else if(!userMasterMap.get(scoolid_sheet+"||"+employee_id.getEmployeeId()).getEmailAddress().trim().equalsIgnoreCase(email_address)){
	  	        					user_id = userMasterMap.get(scoolid_sheet+"||"+employee_id.getEmployeeId());
	  	        					updateFlag2=true;
	  	        				}else if(!userMasterMap.get(scoolid_sheet+"||"+employee_id.getEmployeeId()).getMiddleName().equalsIgnoreCase(middle_name)){
	  	        					user_id = userMasterMap.get(scoolid_sheet+"||"+employee_id.getEmployeeId());
	  	        					updateFlag2=true;
	  	        				}
	  	        			}
		  	        	}
		  	        	else{
		  	        		insertFlag=true;
		  	        	}

	  	        		try {
	  	        			if(insertFlag==true){
	  	        			
								int verificationCode=(int) Math.round(Math.random() * 2000000);
								a1.setEntityType(entity_id);													
								a1.setSchoolId(schoolLocExistMap.get(Loc));
								a1.setDistrictId(districtMaster);
								a1.setSalutation(0);
								a1.setFirstName(first_name);
								a1.setLastName(last_name);
								a1.setMiddleName(middle_name);
								a1.setTitle("");
								a1.setEmailAddress(email_address);
								a1.setPassword(MD5Encryption.toMD5("New@userte@cher2014"));
								a1.setPhoneNumber("");
								a1.setMobileNumber("");
								a1.setRoleId(role_id);
								a1.setAuthenticationCode(""+verificationCode);
								a1.setVerificationCode(""+verificationCode);
								a1.setEmployeeMaster(employee_id);							
								a1.setStatus("A");
								a1.setForgetCounter(0);
								a1.setIsQuestCandidate(false);
								a1.setCreatedDateTime(new Date());	            	
								userMasterMap.put(scoolid_sheet+"||"+employee_id.getEmployeeId(),a1);
								usersListMap.put(userAreExist,a1);
								statelesSsession.insert(a1);
								mapCount++;
								createCount++;
	  	        			}else if(updateFlag==true){
	  	        				a1.setUserId(user_id.getUserId());
								a1.setEntityType(entity_id);													
								a1.setSchoolId(schoolLocExistMap.get(Loc));
								a1.setDistrictId(districtMaster);
								a1.setSalutation(0);
								a1.setFirstName(user_id.getFirstName());
								a1.setLastName(user_id.getLastName());
								a1.setMiddleName(user_id.getMiddleName());
								a1.setTitle(user_id.getTitle());
								a1.setEmailAddress(user_id.getEmailAddress());
								a1.setPassword(user_id.getPassword());
								a1.setPhoneNumber(user_id.getPhoneNumber());
								a1.setMobileNumber(user_id.getMobileNumber());
								a1.setRoleId(role_id);
								a1.setAuthenticationCode(user_id.getAuthenticationCode());
								a1.setVerificationCode(user_id.getVerificationCode());
								a1.setEmployeeMaster(employee_id);	
								a1.setPhotoPath(user_id.getPhotoPath());
								a1.setIsQuestCandidate(false);
								a1.setPhotoPathFile(user_id.getPhotoPathFile());
								a1.setStatus(user_id.getStatus());
								a1.setFgtPwdDateTime(user_id.getFgtPwdDateTime());
								a1.setForgetCounter(user_id.getForgetCounter());
								a1.setIsExternal(user_id.getIsExternal());
								a1.setCreatedDateTime(new Date());	            	
								userMasterMap.put(scoolid_sheet+"||"+employee_id.getEmployeeId(),a1);
								usersListMap.put(userAreExist,a1);
								statelesSsession.update(a1);
								mapCount++;
								updateCount++;
	  	        			}
	  	        			
	  	        			if(updateFlag2==true){
	  	        				a1.setUserId(user_id.getUserId());
								a1.setEntityType(entity_id);													
								a1.setSchoolId(schoolLocExistMap.get(Loc));
								a1.setDistrictId(districtMaster);
								a1.setSalutation(0);
								a1.setFirstName(first_name);
								a1.setLastName(last_name);
								a1.setMiddleName(middle_name);
								a1.setTitle(user_id.getTitle());
								a1.setEmailAddress(email_address);
								a1.setPassword(user_id.getPassword());
								a1.setPhoneNumber(user_id.getPhoneNumber());
								a1.setMobileNumber(user_id.getMobileNumber());
								a1.setRoleId(role_id);
								a1.setAuthenticationCode(user_id.getAuthenticationCode());
								a1.setVerificationCode(user_id.getVerificationCode());
								a1.setEmployeeMaster(employee_id);	
								a1.setPhotoPath(user_id.getPhotoPath());
								a1.setPhotoPathFile(user_id.getPhotoPathFile());
								a1.setStatus(user_id.getStatus());
								a1.setFgtPwdDateTime(user_id.getFgtPwdDateTime());
								a1.setForgetCounter(user_id.getForgetCounter());
								a1.setIsQuestCandidate(false);
								a1.setIsExternal(user_id.getIsExternal());
								a1.setCreatedDateTime(new Date());	            	
								userMasterMap.put(scoolid_sheet+"||"+employee_id.getEmployeeId(),a1);
								usersListMap.put(userAreExist,a1);
								statelesSsession.update(a1);
								mapCount++;
								updateCount++;
	  	        			}
						} catch (Exception e) {
							e.printStackTrace();
						}		  	        	
		  	        }
		  	    }
		  	    
			 } // i for loop end
			 txOpen.commit();
	 	 	 statelesSsession.close();
	 	 	 
	 	 	UserUploadFolderAccess b1 = new UserUploadFolderAccess();
	 	 	b1.setUseuploadFolderId(rowId);
			b1.setDistrictId(districtMaster);
			b1.setFunctionality("userUpload");
			b1.setFolderPath(folderPath);
			b1.setNameOfLastFileUploaded(fileName);
			b1.setLastUploadDateTime(getCurrentDateFormart2(lastUploadDateTime));
			b1.setStatus("A");
			userUploadFolderAccessDAO.makePersistent(b1);
			
			FileUploadHistory fileUploadHistory = new FileUploadHistory();
			
			fileUploadHistory.setUseuploadFolderId(b1);
			fileUploadHistory.setInsertRecord(createCount);
			fileUploadHistory.setUpdateRecord(updateCount);
			fileUploadHistory.setNumOfError(numOfError);
			fileUploadHistory.setDateTime(new Date());
			fileUploadHistoryDAO.makePersistent(fileUploadHistory);

			deleteXlsAndXlsxFile(filePath);
	 	 	
	 	 	if(final_error_store.equalsIgnoreCase("")){
	 	 		String to= "gourav@netsutra.com";
				String subject = fileName+" successfully upload";
				String msg = "File successfully upload\n";
						msg+="File Name : "+fileName+"\n";
						msg+="New Records : "+createCount+"\n";
						msg+="Update Records : "+updateCount+"\n";
						msg+="Upload Date time : "+new Date()+"\n";
				String from= "gourav@netsutra.com";
				String errorFileName="excelUploadError.txt";
				System.out.println(msg);
				emailerService.sendMail(to, subject, msg);
	 	 	}
	 	 	 
	 	 	if(!final_error_store.equalsIgnoreCase("")){
	 	 		String content = final_error_store;		    				
				File file = new File(folderPath+"excelUploadError.txt");
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
				String[] parts = content.split("<>");	    				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write("loc,Emp. #,Access Level");
				bw.write("\r\n\r\n");
				int k =0;
				for(String cont :parts) {
					bw.write(cont+"\r\n\r\n");
				    k++;
				}
				bw.close();
				String to= "gourav@netsutra.com";
				String subject = fileName+" Upload Error";
				String msg = "Error in "+fileName+" file";
				String from= "gourav@netsutra.com";
				String errorFileName="excelUploadError.txt";			

				emailerService.sendMailWithAttachments(to,subject,from,msg,folderPath+errorFileName);
	 	 	}
		}catch (Exception e){
			e.printStackTrace();
		}
			return null;
	}

	 public static Vector readDataExcelCSV(String fileName) throws FileNotFoundException{
	
		Vector vectorData = new Vector();
		String filepathhdr=fileName;

		int rowshdr=0;
		int Loc_count=11;
		int AccessLevel_count=11;
		int Emp_count=11;
		
	        BufferedReader brhdr=new BufferedReader(new FileReader(filepathhdr));
	        
	        String strLineHdr="";	        
	        String hdrstr="";
	        
	        StringTokenizer sthdr=null;
	        StringTokenizer stdtl=null;
	        int lineNumberHdr=0;
	        try{
	            String indexCheck="";
	            while((strLineHdr=brhdr.readLine())!=null){               
	            	Vector vectorCellEachRowData = new Vector();
	                int cIndex=0;
	                boolean cellFlag=false,dateFlag=false;
	                Map<String,String> mapCell = new TreeMap<String, String>();
	                int i=1;
	                
	                String[] parts = strLineHdr.split(",");
	                
	                for(int j=0; j<parts.length; j++) {
	                	hdrstr=parts[j];
	                	cIndex=j;
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("Loc")){
	                    		Loc_count=cIndex;
	                    		indexCheck+="||"+cIndex+"||";
	                    	}
	                        
	                        if(Loc_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("Access Level")){
	                        	AccessLevel_count=cIndex;
	                    		indexCheck+="||"+cIndex+"||";
	                    	}
	                    	if(AccessLevel_count==cIndex){
	                    		cellFlag=true;
	                    		
	                    	}	
	                    	  
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("Emp. #")){
	                    		Emp_count=cIndex;
	                    		indexCheck+="||"+cIndex+"||";
	                    	}
	                    	if(Emp_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                        
	                    	if(cellFlag){
	                    		if(!dateFlag){
	                    			try{
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}catch(Exception e){
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}
	                    		}else{
	                    			mapCell.put(cIndex+"",hdrstr);
	                    		}
	                    	}
	                        
	                    }
	                    
	                    vectorCellEachRowData=cellValuePopulate(mapCell);
	                    vectorData.addElement(vectorCellEachRowData);
	                lineNumberHdr++;
	            }
	        }
	        catch(Exception e){
	            System.out.println(e.getMessage());
	        }
	        System.out.println("::::::::: read data from csv ::::::::::       "+vectorData);       
		return vectorData;
	}
	
	
	
	public static Vector cellValuePopulate(Map<String,String> mapCell){
	 Vector vectorCellEachRowData = new Vector();
	 Map<String,String> mapCellTemp = new TreeMap<String, String>();
	 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false;
	 for(Map.Entry<String, String> entry : mapCell.entrySet()){
		 String key=entry.getKey();
		String cellValue=null;
		if(entry.getValue()!=null)
			cellValue=entry.getValue().trim();
		
		if(key.equals("0")){
			mapCellTemp.put(key, cellValue);
			flag0=true;
		}
		if(key.equals("1")){
			flag1=true;
			mapCellTemp.put(key, cellValue);
		}
		if(key.equals("2")){
			flag2=true;
			mapCellTemp.put(key, cellValue);
		}
		
	 }
	 if(flag0==false){
		 mapCellTemp.put(0+"", "");
	 }
	 if(flag1==false){
		 mapCellTemp.put(1+"", "");
	 }
	 if(flag2==false){
		 mapCellTemp.put(2+"", "");
	 }
	 			 
	 for(Map.Entry<String, String> entry : mapCellTemp.entrySet()){
		 vectorCellEachRowData.addElement(entry.getValue());
	 }
	 return vectorCellEachRowData;
}

	public String deleteXlsAndXlsxFile(String fileName){
		
		String filePath="";
		File file=null;
		String returnDel="";
		try{			
			filePath=fileName;
			file = new File(filePath);
			file.delete();
			returnDel="deleted";
		}catch(Exception e){}
		return returnDel;
	}

	public static String monthReplace(String date){
	String currentDate="";
	try{
		
	   if(date.contains("Jan")){
		   currentDate=date.replaceAll("Jan","01");
	   }
	   if(date.contains("Feb")){
		   currentDate=date.replaceAll("Feb","02");
	   }
	   if(date.contains("Mar")){
		   currentDate=date.replaceAll("Mar","03");
	   }
	   if(date.contains("Apr")){
		   currentDate=date.replaceAll("Apr","04");
	   }
	   if(date.contains("May")){
		   currentDate=date.replaceAll("May","05");
	   }
	   if(date.contains("Jun")){
		   currentDate=date.replaceAll("Jun","06");
	   }
	   if(date.contains("Jul")){
		   currentDate=date.replaceAll("Jul","07");
	   }
	   if(date.contains("Aug")){
		   currentDate=date.replaceAll("Aug","08");
	   }
	   if(date.contains("Sep")){
		   currentDate=date.replaceAll("Sep","09");
	   }
	   if(date.contains("Oct")){
		   currentDate=date.replaceAll("Oct","10");
	   }
	   if(date.contains("Nov")){
		   currentDate=date.replaceAll("Nov","11");
	   }
	   if(date.contains("Dec")){
		   currentDate=date.replaceAll("Dec","12");
	   }
	   String uDate=currentDate.replaceAll("-","/");
	   if(Utility.isDateValid(uDate,0)==2){
		   currentDate=date; 
	   }else{
		   try{
			   String dateSplit[]=uDate.split("/");
			   currentDate=dateSplit[1]+"/"+dateSplit[0]+"/"+dateSplit[2];
		   }catch(Exception e){
			   currentDate=date; 
		   }
	   }
	}catch(Exception e){
		currentDate=date;
	}
	   return currentDate;
 }

	public static Date getCurrentDateFormart2(String oldDateString) throws ParseException{

		String OLD_FORMAT = "yyyy-MM-dd HH:mm:ss";
		String NEW_FORMAT = "yyyy-MM-dd HH:mm:ss";
		String newDateString;
		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date d = sdf.parse(oldDateString);
		sdf.applyPattern(NEW_FORMAT);
		newDateString = sdf.format(d);
	
		SimpleDateFormat stodate = new SimpleDateFormat(NEW_FORMAT);
		Date date = stodate.parse(newDateString);
		return date;
	}
	
	public static int isDateValidOnly(String date) 
	{
		String DATE_FORMAT ="mm-dd-yyyy";

        try {
        	SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        	sdf.setLenient(false);
        	sdf.parse(date);
        	//System.out.println(sdf.parse(date));
            return 1;
        } catch (Exception e) {
            return 2;
        }
	}

}
