package tm.controller.master;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobCertification;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.SchoolInJobOrder;
import tm.bean.UserUploadFolderAccess;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SdpCodesWithCertificates;
import tm.bean.master.StateMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.FileUploadHistoryDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SdpCodesWithCertificatesDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.EmailerService;
import tm.utility.Utility;
import edu.emory.mathcs.backport.java.util.Arrays;

@Controller
public class PhiladelphiaVacancyProcess {
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	  String TM_instructional_vacancies1= Utility.getLocaleValuePropByKey("TM_instructional_vacancies1", locale);
	  String msgNotFoundFileUpload1= Utility.getLocaleValuePropByKey("msgNotFoundFileUpload1", locale);
	  String msgRejectedTheCandidate21= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate21", locale);
	  String msgRejectedTheCandidate22= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate22", locale);
	  String msgRejectedTheCandidate23= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate23", locale);
	  String msgRejectedTheCandidate24= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate24", locale);
	  String msgRejectedTheCandidate25= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate25", locale);
	  String msgRejectedTheCandidate26= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate26", locale);
	  String msgRejectedTheCandidate27= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate27", locale);
	  String msgRejectedTheCandidate28= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate28", locale);
	  String msgRejectedTheCandidate29= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate29", locale);
	  String msgRejectedTheCandidate30= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate30", locale);
	  String msgRejectedTheCandidate31= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate31", locale);
	  String msgRejectedTheCandidate32= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate32", locale);
	  String msgRejectedTheCandidate33= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate33", locale);
	  String msgRejectedTheCandidate34= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate34", locale);
	  String msgRejectedTheCandidate35= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate35", locale);
	  String msgRejectedTheCandidate36= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate36", locale);
	  

	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired
	private UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	public void setUserUploadFolderAccessDAO(UserUploadFolderAccessDAO userUploadFolderAccessDAO) {
		this.userUploadFolderAccessDAO = userUploadFolderAccessDAO;
	}
	
	@Autowired
	private FileUploadHistoryDAO fileUploadHistoryDAO;
	public void setFileUploadHistoryDAO(FileUploadHistoryDAO fileUploadHistoryDAO){
		this.fileUploadHistoryDAO = fileUploadHistoryDAO;
	}
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService){
	    this.emailerService = emailerService;
	}
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	
	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private SdpCodesWithCertificatesDAO sdpCodesWithCertificatesDAO;
	
	@RequestMapping(value="/philadelphiavacancyprocess.do", method=RequestMethod.GET)
	public String getallactivedistrict(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		request.getSession();
			System.out.println("::::: Use philadelphiavacancyprocess controller :::::");
				
		List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
		
		try {
			Criterion functionality = Restrictions.eq("functionality", "philadelphiavacancyprocess");
			Criterion active = Restrictions.eq("status", "A");
			uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
			System.out.println("uploadFolderAccessesrec    "+uploadFolderAccessesrec.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for (UserUploadFolderAccess getRec : uploadFolderAccessesrec) {
			Integer districtId = getRec.getDistrictId().getDistrictId();
			File filesList = findLatestFilesByPath(getRec.getFolderPath());
		
		if(filesList==null){
			String to= "gourav@netsutra.com";
		String subject = TM_instructional_vacancies1;
		String msg = msgNotFoundFileUpload1;						
		String from= "gourav@netsutra.com";
		
		//System.out.println(msg);		
		//emailerService.sendMail(to, subject, msg);
		}else{
			try {
				String FileName = filesList.getName();
				//System.out.println("FileName   "+FileName);
				Integer rowId = getRec.getUseuploadFolderId();
				String folderPath = getRec.getFolderPath();
				String lastUploadDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(filesList.lastModified()));
						
						String[] actionValue = positionImport(FileName,districtId,rowId,folderPath,lastUploadDateTime);
						
						PrintWriter output = response.getWriter();
						/*if(actionValue[0].equalsIgnoreCase("noproblem")){
							
							
							//output.print("NoPorblem");
							String[] deleteRecords = deleteRecords(folderPath);
							output.print(actionValue[1]);
							output.print("\n"+deleteRecords[1]);
							output.print("\n"+deleteRecords[2]);
							
							
							List<String> lstTo = new ArrayList<String>();
							
							//lstTo.add("Harshbarger@dadeschools.net");
							//lstTo.add("clientservices@teachermatch.net");							
							lstTo.add("tmhotline@netsutra.com");
							lstTo.add("gourav@netsutra.com");
							
							String subject = "Import the data on Platform from Vacancy file";
							String msg = "Successfully imported the data on Platform.";
							
							String newRecords="";
							String delRecords="";
							if(actionValue[2]!=null)
								newRecords = actionValue[2];
							if(deleteRecords[2]!=null)
								delRecords = deleteRecords[2];
							
							String url = request.getRequestURL().toString();
							
							if(url.contains("platform")){
								for (String string : lstTo) {
									emailerService.sendMailAsHTMLText(string, subject, MailText.getMailTextInVacancy(request, newRecords, delRecords, msg));
								}
							}
						}*/
					} catch (Exception e) {
						e.printStackTrace();
					}
				}    
			}
			return null;
	}
	
	//private Vector vectorDataExcelXLSX = new Vector();
	public String[] positionImport(String fileName,Integer districtId,Integer rowId,String folderPath,String lastUploadDateTime){
		
		StringBuffer sb = new StringBuffer(); 
		
		try {
			
			String filePath=folderPath+fileName;
			
				 sb.append("\nRead records from csv\n");
				 /*if(fileName.contains(".csv") || fileName.contains(".CSV")){
					 vectorDataExcelXLSX = readDataExcelCSV(filePath);
				 }*/	
				 
				 InputStream ExcelFileToRead = new FileInputStream(filePath);
				 HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);
			 			
				 int totalCountWorkBooks=wb.getNumberOfSheets();
			
			
			int job_code_var_count=0;
			String job_code_store="";
			List<String> jobCodes=new ArrayList<String>();
				
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);		
			
			List<String> jbCatNames= new ArrayList<String>();
				jbCatNames.add("School Support--Facilities and Maintenance");
				jbCatNames.add("School Support--School-Based Support");
				jbCatNames.add("School Support--Transportation");
				jbCatNames.add("School Support--Food Service");
				jbCatNames.add("Teacher");
			Criterion criterion1=Restrictions.in( "jobCategoryName",jbCatNames);
			Criterion criterion2=Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion3=Restrictions.eq( "status","A");
			List<JobCategoryMaster> jobCategoryMaster = jobCategoryMasterDAO.findByCriteria(criterion1,criterion2,criterion3);
			/*AssessmentDetail assessmentDetail = new AssessmentDetail();
			try {
				assessmentDetail = assessmentDetailDAO.getAssessmentByCategoryName(jobCategoryMaster, districtMaster);	
			} catch (Exception e) {
				e.printStackTrace();
			}*/
			
			StateMaster stateMaster = new StateMaster();
			Long stateId = new Long(39);
			stateMaster.setStateId(stateId);
			List<CertificateTypeMaster> certificateTypeMaster = certificateTypeMasterDAO.findCertificationByJob(stateMaster, "0");
			
			List<JobOrder> existJObs = jobOrderDAO.getListByCateApiJobCatDist(jobCodes,districtMaster,jobCategoryMaster);
			
			List<JobCertification> jobCertifications= new ArrayList<JobCertification>();
			if(existJObs!=null && existJObs.size()>0){
				jobCertifications= jobCertificationDAO.findCertificationByJobOrders(existJObs);
			}
			Map<String, CertificateTypeMaster> certiMap = new HashMap<String, CertificateTypeMaster>();
				if(certificateTypeMaster!=null && certificateTypeMaster.size()>0){
					for (CertificateTypeMaster certificate : certificateTypeMaster) {
							certiMap.put(certificate.getCertTypeId()+"", certificate);
					}	
				}
				
			Map<Integer, String> jobCertMap = new HashMap<Integer, String>();
			if(jobCertifications!=null && jobCertifications.size()>0){
				for (JobCertification jobCertification : jobCertifications) {
						Integer jobId = jobCertification.getJobId().getJobId();						
						if(jobCertMap.containsKey(jobId)){
							String certList = jobCertMap.get(jobId); //jobCertification.getCertificateTypeMaster().getCertTypeCode();
							jobCertMap.put(jobId, jobCertification.getCertificateTypeMaster().getCertTypeCode()+"#"+certList);
						}else{
							jobCertMap.put(jobId, jobCertification.getCertificateTypeMaster().getCertTypeCode());
						}
				}
			}
			
			Map<String, JobCategoryMaster> jobCatMap = new HashMap<String, JobCategoryMaster>();			
			if(jobCategoryMaster!=null && jobCategoryMaster.size()>0){
				for (JobCategoryMaster jobCatMaster : jobCategoryMaster) {
					jobCatMap.put(jobCatMaster.getJobCategoryName().toLowerCase(), jobCatMaster);
				}
			}
			
			Map<String, JobOrder> existingJobsMap = new HashMap<String, JobOrder>();
			if(existJObs!=null && existJObs.size()>0){
				String certIficatesByJob = "";
				int dd=0;
				int tcount =existJObs.size();
				for (JobOrder jobOrder : existJObs) {
					//Integer jobId = jobOrder.getJobId();
					/*if(jobCertMap!=null &&  jobCertMap.size()> 0 && jobCertMap.containsKey(jobOrder.getJobId())){
						certIficatesByJob=jobCertMap.get(jobOrder.getJobId());
					}*/
					String jobDate = jobOrder.getJobStartDate().toString().replace(" 00:00:00.0", "");
					existingJobsMap.put(jobOrder.getJobTitle()+"#"+jobDate, jobOrder);					
				}
			}
			
			List<SdpCodesWithCertificates> sdpCodesWithCertificateList = new ArrayList<SdpCodesWithCertificates>();
			sdpCodesWithCertificateList = sdpCodesWithCertificatesDAO.findSdpCodesBySate(stateMaster);
			Map<String, String> sdpCodesMap = new HashMap<String, String>();
			Map<String, String> sdpCodesAndTitleMap = new HashMap<String, String>();
			if(sdpCodesWithCertificateList!=null && sdpCodesWithCertificateList.size()>0)
			{
				for(SdpCodesWithCertificates sdpCodesWithCertificates : sdpCodesWithCertificateList)
				{
					sdpCodesAndTitleMap.put(sdpCodesWithCertificates.getSdpCode(), sdpCodesWithCertificates.getSdpCodeTitle());
					
					if(sdpCodesMap.containsKey(sdpCodesWithCertificates.getSdpCode()))
						sdpCodesMap.put(sdpCodesWithCertificates.getSdpCode(), ","+sdpCodesWithCertificates.getCertificateTypeMaster().getCertTypeId());
					else
						sdpCodesMap.put(sdpCodesWithCertificates.getSdpCode(), ""+sdpCodesWithCertificates.getCertificateTypeMaster().getCertTypeId());
				}
			}
			
			
		    UserMaster userMaster = userMasterDAO.findById(1, false, false);
		    Criterion districtMaster_districtID = Restrictions.eq("districtMaster", districtMaster);
		   /* List<DistrictRequisitionNumbers> districtRequisitionNumber = districtRequisitionNumbersDAO.findByCriteria(districtMaster_districtID);
		    
		    Map<String,DistrictRequisitionNumbers>requisitionNumberMap=new HashMap<String, DistrictRequisitionNumbers>();
		    if(districtRequisitionNumber!=null && districtRequisitionNumber.size()>0){
				 for(DistrictRequisitionNumbers requisitionnumber:districtRequisitionNumber)
				 {	
					 requisitionNumberMap.put(requisitionnumber.getRequisitionNumber(), requisitionnumber);
				 }
		    }
		    List<JobRequisitionNumbers> jobRequisitionNumbers=null;
		    Map<String, JobRequisitionNumbers> jobReqMap = new HashMap<String, JobRequisitionNumbers>();
			 if(districtRequisitionNumber.size()>0){				
				 Criterion distReqIds = Restrictions.in("districtRequisitionNumbers", districtRequisitionNumber);
				 jobRequisitionNumbers = jobRequisitionNumbersDAO.findByCriteria(distReqIds);
				 if(jobRequisitionNumbers!=null && jobRequisitionNumbers.size()>0){
					 for (JobRequisitionNumbers jobRequisitionNumbers2 : jobRequisitionNumbers) {
						jobReqMap.put(jobRequisitionNumbers2.getDistrictRequisitionNumbers().getRequisitionNumber(), jobRequisitionNumbers2);
					}
				 }
			 }*/
			
			 List<SchoolMaster> schoolLoc = schoolMasterDAO.findSchoolListByDistrict(districtMaster);
			 Map<String, SchoolMaster>schoolLocExistMap = new HashMap<String, SchoolMaster>();			
			 if(schoolLoc.size()!=0){
				 for (SchoolMaster school : schoolLoc) {			
					 String ss=school.getLocationCode();
					 schoolLocExistMap.put(ss, school);
				}
			 }			 
			 List<SchoolInJobOrder> lstSchoolInJobOrders=null;
			 if(existJObs.size()!=0){
				 lstSchoolInJobOrders = schoolInJobOrderDAO.getSIJO(existJObs);
			 }
			Map<String, SchoolInJobOrder>schoolInJobOrderMap = new HashMap<String, SchoolInJobOrder>();
			String jobAndsch=null;
			if(lstSchoolInJobOrders!=null && lstSchoolInJobOrders.size()!=0){				
				for(SchoolInJobOrder schoolInJO :lstSchoolInJobOrders){
					jobAndsch=schoolInJO.getJobId().getJobId()+"||"+schoolInJO.getSchoolId().getSchoolId();
					schoolInJobOrderMap.put(jobAndsch, schoolInJO);
				}
			}
			
			SessionFactory sessionFactory=jobOrderDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			Transaction txOpen =statelesSsession.beginTransaction();
			
			for (int sheetI = 0; sheetI < totalCountWorkBooks; sheetI++) {
				 Vector vectorDataExcelXLSX = new Vector();
				 System.out.println("Read sheet  "+sheetI);
				 /*if(sheetI==1){
					 break;
				 }*/
				 String sheetName = wb.getSheetName(sheetI);
				 vectorDataExcelXLSX = readDataExcelXLS(wb.getSheetAt(sheetI));
				 
			int ZoneCode_count=1000;
			int SchoolCode_count=1000;
			int GradeLevel_count=1000;
			int HighNeedsStatus_count=1000;
			int DateAdded_count=1000;
			int Notes_count=1000;
			int JobCategory_count=1000;
			int Classification_count=1000;
			int code_0502_count=1000;
			int code_0807_count=1000;
			int code_0813_count=1000;
			int code_0815_count=1000;
			int code_0863_count=1000;
			int code_0885_count=1000;
			int code_1000_count=1000;
			int code_1052_count=1000;
			int code_1111_count=1000;
			int code_1405_count=1000;
			int code_1603_count=1000;
			int code_1668_count=1000;
			int code_1836_count=1000;
			int code_1837_count=1000;
			int code_2000_count=1000;
			int code_2825_count=1000;
			int code_3100_count=1000;
			int code_3101_count=1000;
			int code_3105_count=1000;
			int code_3108_count=1000;
			int code_3109_count=1000;
			int code_3230_count=1000;
			int code_4005_count=1000;
			int code_4030_count=1000;
			int code_4405_count=1000;
			int code_4410_count=1000;
			int code_4420_count=1000;
			int code_4430_count=1000;
			int code_4480_count=1000;
			int code_4490_count=1000;
			int code_4499_count=1000;
			int code_4805_count=1000;
			int code_6420_count=1000;
			int code_6800_count=1000;
			int code_7205_count=1000;
			int code_7207_count=1000;
			int code_7605_count=1000;
			int code_7606_count=1000;
			int code_7607_count=1000;
			int code_7610_count=1000;
			int code_7621_count=1000;
			int code_7650_count=1000;
			int code_8405_count=1000;
			int code_8420_count=1000;
			int code_8450_count=1000;
			int code_8470_count=1000;
			int code_8875_count=1000;
			int code_9201_count=1000;
			int code_9202_count=1000;
			int code_9203_count=1000;
			int code_9204_count=1000;
			int code_9205_count=1000;
			int code_9206_count=1000;
			int code_9307_count=1000;
			int code_9308_count=1000;
			int code_9309_count=1000;
			int code_9310_count=1000;
			int code_3288_count=1000;
			int code_6884_count=1000;

			int code_9235_count=1000;
			int code_9270_count=1000;
			int code_9290_count=1000;		
		
			boolean fields_error=false;
			String final_error_store="";
				for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
					String ZoneCode="";
					String SchoolCode="";
					String GradeLevel="";
					String HighNeedsStatus="";
					String DateAdded="";
					String Notes="";
					String JobCategory="";
					String Classification="";
					
					String code_0502="";
					String code_0807="";
					String code_0813="";
					String code_0815="";
					String code_0863="";
					String code_0885="";
					String code_1000="";
					String code_1052="";
					String code_1111="";
					String code_1405="";
					String code_1603="";
					String code_1668="";
					String code_1836="";
					String code_1837="";
					String code_2000="";
					String code_2825="";
					String code_3100="";
					String code_3101="";
					String code_3105="";
					String code_3108="";
					String code_3109="";
					String code_3230="";
					String code_4005="";
					String code_4030="";
					String code_4405="";
					String code_4410="";
					String code_4420="";
					String code_4430="";
					String code_4480="";
					String code_4490="";
					String code_4499="";
					String code_4805="";
					String code_6420="";
					String code_6800="";
					String code_7205="";
					String code_7207="";
					String code_7605="";
					String code_7606="";
					String code_7607="";
					String code_7610="";
					String code_7621="";
					String code_7650="";
					String code_8405="";
					String code_8420="";
					String code_8450="";
					String code_8470="";
					String code_8875="";
					String code_9201="";
					String code_9202="";
					String code_9203="";
					String code_9204="";
					String code_9205="";
					String code_9206="";
					String code_9307="";
					String code_9308="";
					String code_9309="";
					String code_9310="";
					String code_3288="";
					String code_6884="";
					String code_9235="";
					String code_9270="";
					String code_9290="";
					
					String num_hires="";
					String errorText="";
					String rowErrorText="";
					String certificatesStore="";
					boolean insertFlag=false;
					boolean updateFlag=false;
					
					
					Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
					
					for(int j=0; j<vectorCellEachRowData.size(); j++) {
						try {
							if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||Zone Code||")){
								ZoneCode_count=j;
							}
							if(ZoneCode_count==j)
								ZoneCode=vectorCellEachRowData.get(j).toString().trim();
							
							if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||School Code||")){
								SchoolCode_count=j;
								}
							if(SchoolCode_count==j)
								SchoolCode=vectorCellEachRowData.get(j).toString().trim();
							
							if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||gradelevel||")){
								GradeLevel_count=j;
								}
							if(GradeLevel_count==j)
								GradeLevel=vectorCellEachRowData.get(j).toString().trim();
							
							if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||highneedsstatus||")){
								HighNeedsStatus_count=j;
								}
							if(HighNeedsStatus_count==j)
								HighNeedsStatus=vectorCellEachRowData.get(j).toString().trim();

								if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||date added||")){
								DateAdded_count=j;
								}if(DateAdded_count==j)
								DateAdded=vectorCellEachRowData.get(j).toString().trim();
								
								if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||notes||")){
									Notes_count=j;
								}
								if(Notes_count==j)
									Notes=vectorCellEachRowData.get(j).toString().trim();

								if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||job category||")){
									JobCategory_count=j;
								}
								if(JobCategory_count==j)
									JobCategory=vectorCellEachRowData.get(j).toString().trim();

								if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||classification||")){
									Classification_count=j;
								}
								if(Classification_count==j)
									Classification=vectorCellEachRowData.get(j).toString().trim();
								
								if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||0502||")){
									code_0502_count=j;
								}
									if(code_0502_count==j){
									
									code_0502=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||0807||")){
									code_0807_count=j;
									}
									if(code_0807_count==j){
									code_0807=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||0813||")){
									code_0813_count=j;
									}
									if(code_0813_count==j){
									code_0813=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||0815||")){
									code_0815_count=j;
									}
									if(code_0815_count==j){
									code_0815=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||0863||")){
									code_0863_count=j;
									}
									if(code_0863_count==j){
									code_0863=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||0885||")){
									code_0885_count=j;
									}
									if(code_0885_count==j){
									code_0885=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||1000||")){
									code_1000_count=j;
									}
									if(code_1000_count==j){
									code_1000=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||1052||")){
									code_1052_count=j;
									}
									if(code_1052_count==j){
									code_1052=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||1111||")){
									code_1111_count=j;
									}
									if(code_1111_count==j){
									code_1111=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||1405||")){
									code_1405_count=j;
									}
									if(code_1405_count==j){
									code_1405=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||1603||")){
									code_1603_count=j;
									}
									if(code_1603_count==j){
									code_1603=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||1668||")){
									code_1668_count=j;
									}
									if(code_1668_count==j){
									code_1668=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||1836||")){
									code_1836_count=j;
									}
									if(code_1836_count==j){
									code_1836=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||1837||")){
									code_1837_count=j;
									}
									if(code_1837_count==j){
									code_1837=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||2000||")){
									code_2000_count=j;
									}
									if(code_2000_count==j){
									code_2000=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||2825||")){
									code_2825_count=j;
									}
									if(code_2825_count==j){
									code_2825=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||3100||")){
									code_3100_count=j;
									}
									if(code_3100_count==j){
									code_3100=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||3101||")){
									code_3101_count=j;
									}
									if(code_3101_count==j){
									code_3101=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||3105||")){
									code_3105_count=j;
									}
									if(code_3105_count==j){
									code_3105=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||3108||")){
									code_3108_count=j;
									}
									if(code_3108_count==j){
									code_3108=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||3109||")){
									code_3109_count=j;
									}
									if(code_3109_count==j){
									code_3109=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||3230||")){
									code_3230_count=j;
									}
									if(code_3230_count==j){
									code_3230=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||4005||")){
									code_4005_count=j;
									}
									if(code_4005_count==j){
									code_4005=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||4030||")){
									code_4030_count=j;
									}
									if(code_4030_count==j){
									code_4030=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||4405||")){
									code_4405_count=j;
									}
									if(code_4405_count==j){
									code_4405=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||4410||")){
									code_4410_count=j;
									}
									if(code_4410_count==j){
									code_4410=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||4420||")){
									code_4420_count=j;
									}
									if(code_4420_count==j){
									code_4420=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||4430||")){
									code_4430_count=j;
									}
									if(code_4430_count==j){
									code_4430=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||4480||")){
									code_4480_count=j;
									}
									if(code_4480_count==j){
									code_4480=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||4490||")){
									code_4490_count=j;
									}
									if(code_4490_count==j){
									code_4490=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||4499||")){
									code_4499_count=j;
									}
									if(code_4499_count==j){
									code_4499=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||4805||")){
									code_4805_count=j;
									}
									if(code_4805_count==j){
									code_4805=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||6420||")){
									code_6420_count=j;
									}
									if(code_6420_count==j){
									code_6420=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||6800||")){
									code_6800_count=j;
									}
									if(code_6800_count==j){
									code_6800=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||7205||")){
									code_7205_count=j;
									}
									if(code_7205_count==j){
									code_7205=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||7207||")){
									code_7207_count=j;
									}
									if(code_7207_count==j){
									code_7207=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||7605||")){
									code_7605_count=j;
									}
									if(code_7605_count==j){
									code_7605=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||7606||")){
									code_7606_count=j;
									}
									if(code_7606_count==j){
									code_7606=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||7607||")){
									code_7607_count=j;
									}
									if(code_7607_count==j){
									code_7607=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||7610||")){
									code_7610_count=j;
									}
									if(code_7610_count==j){
									code_7610=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||7621||")){
									code_7621_count=j;
									}
									if(code_7621_count==j){
									code_7621=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||7650||")){
									code_7650_count=j;
									}
									if(code_7650_count==j){
									code_7650=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||8405||")){
									code_8405_count=j;
									}
									if(code_8405_count==j){
									code_8405=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||8420||")){
									code_8420_count=j;
									}
									if(code_8420_count==j){
									code_8420=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||8450||")){
									code_8450_count=j;
									}
									if(code_8450_count==j){
									code_8450=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||8470||")){
									code_8470_count=j;
									}
									if(code_8470_count==j){
										code_8470=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||8875||")){
									code_8875_count=j;
									}
									if(code_8875_count==j){
									code_8875=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||9201||")){
									code_9201_count=j;
									}
									if(code_9201_count==j){
									code_9201=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||9202||")){
									code_9202_count=j;
									}
									if(code_9202_count==j){
									code_9202=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||9203||")){
									code_9203_count=j;
									}
									if(code_9203_count==j){
									code_9203=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||9204||")){
									code_9204_count=j;
									}
									if(code_9204_count==j){
									code_9204=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||9205||")){
									code_9205_count=j;
									}
									if(code_9205_count==j){
									code_9205=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||9206||")){
									code_9206_count=j;
									}
									if(code_9206_count==j){
									code_9206=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||9307||")){
									code_9307_count=j;
									}
									if(code_9307_count==j){
									code_9307=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||9308||")){
									code_9308_count=j;
									}
									if(code_9308_count==j){
									code_9308=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||9309||")){
									code_9309_count=j;
									}
									if(code_9309_count==j){
									code_9309=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||9310||")){
									code_9310_count=j;
									}
									if(code_9310_count==j){
									code_9310=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||3288||")){
									code_3288_count=j;
									}
									if(code_3288_count==j){
									code_3288=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||6884||")){
									code_6884_count=j;
									}
									if(code_6884_count==j){
									code_6884=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||9235||")){
									code_9235_count=j;
									}
									if(code_9235_count==j){
									code_9235=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||9270||")){
									code_9270_count=j;
									}
									if(code_9270_count==j){
									code_9270=vectorCellEachRowData.get(j).toString().trim();
									}
									if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("||9290||")){
									code_9290_count=j;
									}
									if(code_9290_count==j){
									code_9290=vectorCellEachRowData.get(j).toString().trim();
									}
									
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					if(i==0){						
						if(!ZoneCode.equalsIgnoreCase("||Zone Code||")){
							rowErrorText+=msgRejectedTheCandidate21;
							fields_error=true;
						}
						
						if(!SchoolCode.equalsIgnoreCase("||School Code||")){
							 if(fields_error){
									rowErrorText+=",";
							  }
							  
							 rowErrorText+=msgRejectedTheCandidate22;
							 fields_error=true;
						}
						
						if(!DateAdded.equalsIgnoreCase("||date added||")){
							 if(fields_error){
									rowErrorText+=",";
							  }
								  
							rowErrorText+=msgRejectedTheCandidate23;
							fields_error=true;
						}
						
						if(!JobCategory.equalsIgnoreCase("||job category||")){
							 if(fields_error){
									rowErrorText+=",";
							  }
								  
							rowErrorText+=msgRejectedTheCandidate24;
							fields_error=true;
						}						
						
						if(!Classification.equalsIgnoreCase("||classification||")){
							 if(fields_error){
									rowErrorText+=",";
							  }
								  
							rowErrorText+=msgRejectedTheCandidate25;
							fields_error=true;
						}
						
					}
					
					if(fields_error){
						  sb.append(msgRejectedTheCandidate26);
						  File file = new File(folderPath+"philadelphiaVacancyFileError.txt");
						
						if (!file.exists()) {
							file.createNewFile();
						}						
						
						String to= "gourav@netsutra.com";
						String subject = msgRejectedTheCandidate27;
						String msg = "Error in "+fileName+" file";
						String from= "gourav@netsutra.com";
						String errorFileName="philadelphiaVacancyFileError.txt";			
						//emailerService.sendMailWithAttachments(to,subject,from,msg,folderPath+errorFileName);
							 break;
					}
					boolean errorFlag=false;					
					
					if(i!=0 && fields_error==false){
						
						SchoolMaster schoolMaster = new SchoolMaster();
						String geoZoneName="";
						if(ZoneCode.equalsIgnoreCase("") && ZoneCode.equalsIgnoreCase("0") ){
							if(errorFlag){
								errorText+=",";	
							}
							errorText+=msgRejectedTheCandidate28;
							errorFlag=true;						
						}else{
							geoZoneName="LEARNING NETWORK "+ZoneCode;
						}
						
						if(SchoolCode.equalsIgnoreCase("") || SchoolCode.equalsIgnoreCase(" ")){
							if(errorFlag){
								errorText+=",";	
							}
							errorText+=msgRejectedTheCandidate29;
							errorFlag=true;
						}else if(!schoolLocExistMap.containsKey(SchoolCode)){
							if(errorFlag){
								errorText+=",";	
							}
							errorText+=msgRejectedTheCandidate29;
							errorFlag=true;
						}else if(schoolLocExistMap.containsKey(SchoolCode) && !schoolLocExistMap.get(SchoolCode).getGeoZoneMaster().getGeoZoneName().equals(geoZoneName)){
							
							if(errorFlag){
								errorText+=",";	
							}
							errorText+=msgRejectedTheCandidate30;
							errorFlag=true;
						}else{
							schoolMaster=schoolLocExistMap.get(SchoolCode);
						}
						
						if(DateAdded.equalsIgnoreCase("")){
							/*if(errorFlag){
								errorText+=",";	
							}
							errorText+="DateAdded is empty";
							errorFlag=true;*/
						}else{
							DateAdded = DateAdded.replace("/", "-");
							String[] dateSplit = DateAdded.split("-");
							DateAdded = dateSplit[1]+"-"+dateSplit[0]+"-"+dateSplit[2];
							
							/*if(!isValidDate(DateAdded)){
								if(errorFlag){
									errorText+=",";	
								}
								errorText+="DateAdded is not valid";
								errorFlag=true;							
							}*/
						}	
						
						//JobCategory = JobCategory.toLowerCase();
						/*if(!JobCategory.equalsIgnoreCase("") && !jobCatMap.containsKey(JobCategory.toLowerCase())){
							if(errorFlag){
								errorText+=",";	
							}
							errorText+="Job Category does not found";
							errorFlag=true;
						}*/
						
						if(JobCategory.equalsIgnoreCase("")){
							if(errorFlag){
								errorText+=",";	
							}
							errorText+=msgRejectedTheCandidate31;
							errorFlag=true;
							System.out.println("1   "+sheetName+"    |||    "+JobCategory+"    |||    "+jobCatMap.size());
						}else if(!JobCategory.equalsIgnoreCase("") && !jobCatMap.containsKey(JobCategory.toLowerCase())){
							if(errorFlag){
								errorText+=",";	
							}
							errorText+=msgRejectedTheCandidate31;
							errorFlag=true;
							System.out.println("2   "+sheetName+"    |||    "+JobCategory+"    |||    "+jobCatMap.size());
						}
						
						if(Classification.equalsIgnoreCase("")){
							if(errorFlag){
								errorText+=",";	
							}
							errorText+=msgRejectedTheCandidate32;
							errorFlag=true;
						}
						Integer totalHirsInJob=0;
						Integer testNew=0;
						//changeVal
						
						Map<String, Integer> tempHires = new HashMap<String, Integer>();
						if(!code_0502.equalsIgnoreCase("")){
						    certificatesStore+="#0502";
						    totalHirsInJob=totalHirsInJob+changeVal(code_0502);
						    tempHires.put("0502", changeVal(code_0502));
						}
						if(!code_0807.equalsIgnoreCase("")){
						    certificatesStore+="#0807";
						    totalHirsInJob=totalHirsInJob+changeVal(code_0807);
						    tempHires.put("0807", changeVal(code_0807));
						}
						if(!code_0813.equalsIgnoreCase("")){
						    certificatesStore+="#0813";
						    totalHirsInJob=totalHirsInJob+changeVal(code_0813);
						    tempHires.put("0813", changeVal(code_0813));
						}
						if(!code_0815.equalsIgnoreCase("")){
						    certificatesStore+="#0815";
						    totalHirsInJob=totalHirsInJob+changeVal(code_0815);
						    tempHires.put("0815", changeVal(code_0815));
						}
						if(!code_0863.equalsIgnoreCase("")){
						    certificatesStore+="#0863";
						    totalHirsInJob=totalHirsInJob+changeVal(code_0863);
						    tempHires.put("0863", changeVal(code_0863));
						}
						if(!code_0885.equalsIgnoreCase("")){
						    certificatesStore+="#0885";
						    totalHirsInJob=totalHirsInJob+changeVal(code_0885);
						    tempHires.put("0885", changeVal(code_0885));
						}
						if(!code_1000.equalsIgnoreCase("")){
						    certificatesStore+="#1000";
						    totalHirsInJob=totalHirsInJob+changeVal(code_1000);
						    tempHires.put("1000", changeVal(code_1000));
						}
						if(!code_1052.equalsIgnoreCase("")){
						    certificatesStore+="#1052";
						    totalHirsInJob=totalHirsInJob+changeVal(code_1052);
						    tempHires.put("1052", changeVal(code_1052));
						}
						if(!code_1111.equalsIgnoreCase("")){
						    certificatesStore+="#1111";
						    totalHirsInJob=totalHirsInJob+changeVal(code_1111);
						    tempHires.put("1111", changeVal(code_1111));
						}
						if(!code_1405.equalsIgnoreCase("")){
						    certificatesStore+="#1405";
						    totalHirsInJob=totalHirsInJob+changeVal(code_1405);
						    tempHires.put("1405", changeVal(code_1405));
						}
						if(!code_1603.equalsIgnoreCase("")){
						    certificatesStore+="#1603";
						    totalHirsInJob=totalHirsInJob+changeVal(code_1603);
						    tempHires.put("1603", changeVal(code_1603));
						}
						if(!code_1668.equalsIgnoreCase("")){
						    certificatesStore+="#1668";
						    totalHirsInJob=totalHirsInJob+changeVal(code_1668);
						    tempHires.put("1668", changeVal(code_1668));
						}
						if(!code_1836.equalsIgnoreCase("")){
						    certificatesStore+="#1836";
						    totalHirsInJob=totalHirsInJob+changeVal(code_1836);
						    tempHires.put("1836", changeVal(code_1836));
						}
						if(!code_1837.equalsIgnoreCase("")){
						    certificatesStore+="#1837";
						    totalHirsInJob=totalHirsInJob+changeVal(code_1837);
						    tempHires.put("1837", changeVal(code_1837));
						}
						if(!code_2000.equalsIgnoreCase("")){
						    certificatesStore+="#2000";
						    totalHirsInJob=totalHirsInJob+changeVal(code_2000);
						    tempHires.put("2000", changeVal(code_2000));
						}
						if(!code_2825.equalsIgnoreCase("")){
						    certificatesStore+="#2825";
						    totalHirsInJob=totalHirsInJob+changeVal(code_2825);
						    tempHires.put("2825", changeVal(code_2825));
						}
						if(!code_3100.equalsIgnoreCase("")){
						    certificatesStore+="#3100";
						    totalHirsInJob=totalHirsInJob+changeVal(code_3100);
						    tempHires.put("3100", changeVal(code_3100));
						}
						if(!code_3101.equalsIgnoreCase("")){
						    certificatesStore+="#3101";
						    totalHirsInJob=totalHirsInJob+changeVal(code_3101);
						    tempHires.put("3101", changeVal(code_3101));
						}
						if(!code_3105.equalsIgnoreCase("")){
						    certificatesStore+="#3105";
						    totalHirsInJob=totalHirsInJob+changeVal(code_3105);
						    tempHires.put("3105", changeVal(code_3105));
						}
						if(!code_3108.equalsIgnoreCase("")){
						    certificatesStore+="#3108";
						    totalHirsInJob=totalHirsInJob+changeVal(code_3108);
						    tempHires.put("3108", changeVal(code_3108));
						}
						if(!code_3109.equalsIgnoreCase("")){
						    certificatesStore+="#3109";
						    totalHirsInJob=totalHirsInJob+changeVal(code_3109);
						    tempHires.put("3109", changeVal(code_3109));
						}
						if(!code_3230.equalsIgnoreCase("")){
						    certificatesStore+="#3230";
						    totalHirsInJob=totalHirsInJob+changeVal(code_3230);
						    tempHires.put("3230", changeVal(code_3230));
						}
						if(!code_4005.equalsIgnoreCase("")){
						    certificatesStore+="#4005";
						    totalHirsInJob=totalHirsInJob+changeVal(code_4005);
						    tempHires.put("4005", changeVal(code_4005));
						}
						if(!code_4030.equalsIgnoreCase("")){
						    certificatesStore+="#4030";
						    totalHirsInJob=totalHirsInJob+changeVal(code_4030);
						    tempHires.put("4030", changeVal(code_4030));
						}
						if(!code_4405.equalsIgnoreCase("")){
						    certificatesStore+="#4405";
						    totalHirsInJob=totalHirsInJob+changeVal(code_4405);
						    tempHires.put("4405", changeVal(code_4405));
						}
						if(!code_4410.equalsIgnoreCase("")){
						    certificatesStore+="#4410";
						    totalHirsInJob=totalHirsInJob+changeVal(code_4410);
						    tempHires.put("4410", changeVal(code_4410));
						}
						if(!code_4420.equalsIgnoreCase("")){
						    certificatesStore+="#4420";
						    totalHirsInJob=totalHirsInJob+changeVal(code_4420);
						    tempHires.put("4420", changeVal(code_4420));
						}
						if(!code_4430.equalsIgnoreCase("")){
						    certificatesStore+="#4430";
						    totalHirsInJob=totalHirsInJob+changeVal(code_4430);
						    tempHires.put("4430", changeVal(code_4430));
						}
						if(!code_4480.equalsIgnoreCase("")){
						    certificatesStore+="#4480";
						    totalHirsInJob=totalHirsInJob+changeVal(code_4480);
						    tempHires.put("4480", changeVal(code_4480));
						}
						if(!code_4490.equalsIgnoreCase("")){
						    certificatesStore+="#4490";
						    totalHirsInJob=totalHirsInJob+changeVal(code_4490);
						    tempHires.put("4490", changeVal(code_4490));
						}
						if(!code_4499.equalsIgnoreCase("")){
						    certificatesStore+="#4499";
						    totalHirsInJob=totalHirsInJob+changeVal(code_4499);
						    tempHires.put("4499", changeVal(code_4499));
						}
						if(!code_4805.equalsIgnoreCase("")){
						    certificatesStore+="#4805";
						    totalHirsInJob=totalHirsInJob+changeVal(code_4805);
						    tempHires.put("4805", changeVal(code_4805));
						}
						if(!code_6420.equalsIgnoreCase("")){
						    certificatesStore+="#6420";
						    totalHirsInJob=totalHirsInJob+changeVal(code_6420);
						    tempHires.put("6420", changeVal(code_6420));
						}
						if(!code_6800.equalsIgnoreCase("")){
						    certificatesStore+="#6800";
						    totalHirsInJob=totalHirsInJob+changeVal(code_6800);
						    tempHires.put("6800", changeVal(code_6800));
						}
						if(!code_7205.equalsIgnoreCase("")){
						    certificatesStore+="#7205";
						    totalHirsInJob=totalHirsInJob+changeVal(code_7205);
						    tempHires.put("7205", changeVal(code_7205));
						}
						if(!code_7207.equalsIgnoreCase("")){
						    certificatesStore+="#7207";
						    totalHirsInJob=totalHirsInJob+changeVal(code_7207);
						    tempHires.put("7207", changeVal(code_7207));
						}
						if(!code_7605.equalsIgnoreCase("")){
						    certificatesStore+="#7605";
						    totalHirsInJob=totalHirsInJob+changeVal(code_7605);
						    tempHires.put("7605", changeVal(code_7605));
						}
						if(!code_7606.equalsIgnoreCase("")){
						    certificatesStore+="#7606";
						    totalHirsInJob=totalHirsInJob+changeVal(code_7606);
						    tempHires.put("7606", changeVal(code_7606));
						}
						if(!code_7607.equalsIgnoreCase("")){
						    certificatesStore+="#7607";
						    totalHirsInJob=totalHirsInJob+changeVal(code_7607);
						    tempHires.put("7607", changeVal(code_7607));
						}
						if(!code_7610.equalsIgnoreCase("")){
						    certificatesStore+="#7610";
						    totalHirsInJob=totalHirsInJob+changeVal(code_7610);
						    tempHires.put("7610", changeVal(code_7610));
						}
						if(!code_7621.equalsIgnoreCase("")){
						    certificatesStore+="#7621";
						    totalHirsInJob=totalHirsInJob+changeVal(code_7621);
						    tempHires.put("7621", changeVal(code_7621));
						}
						if(!code_7650.equalsIgnoreCase("")){
						    certificatesStore+="#7650";
						    totalHirsInJob=totalHirsInJob+changeVal(code_7650);
						    tempHires.put("7650", changeVal(code_7650));
						}
						if(!code_8405.equalsIgnoreCase("")){
						    certificatesStore+="#8405";
						    totalHirsInJob=totalHirsInJob+changeVal(code_8405);
						    tempHires.put("8405", changeVal(code_8405));
						}
						if(!code_8420.equalsIgnoreCase("")){
						    certificatesStore+="#8420";
						    totalHirsInJob=totalHirsInJob+changeVal(code_8420);
						    tempHires.put("8420", changeVal(code_8420));
						}
						if(!code_8450.equalsIgnoreCase("")){
						    certificatesStore+="#8450";
						    totalHirsInJob=totalHirsInJob+changeVal(code_8450);
						    tempHires.put("8450", changeVal(code_8450));
						}
						if(!code_8470.equalsIgnoreCase("")){
						    certificatesStore+="#8470";
						    totalHirsInJob=totalHirsInJob+changeVal(code_8470);
						    tempHires.put("8470", changeVal(code_8470));
						}
						if(!code_8875.equalsIgnoreCase("")){
						    certificatesStore+="#8875";
						    totalHirsInJob=totalHirsInJob+changeVal(code_8875);
						    tempHires.put("8875", changeVal(code_8875));
						}
						if(!code_9201.equalsIgnoreCase("")){
						    certificatesStore+="#9201";
						    totalHirsInJob=totalHirsInJob+changeVal(code_9201);
						    tempHires.put("9201", changeVal(code_9201));
						}
						if(!code_9202.equalsIgnoreCase("")){
						    certificatesStore+="#9202";
						    totalHirsInJob=totalHirsInJob+changeVal(code_9202);
						    tempHires.put("9202", changeVal(code_9202));
						}
						if(!code_9203.equalsIgnoreCase("")){
						    certificatesStore+="#9203";
						    totalHirsInJob=totalHirsInJob+changeVal(code_9203);
						    tempHires.put("9203", changeVal(code_9203));
						}
						if(!code_9204.equalsIgnoreCase("")){
						    certificatesStore+="#9204";
						    totalHirsInJob=totalHirsInJob+changeVal(code_9204);
						    tempHires.put("9204", changeVal(code_9204));
						}
						if(!code_9205.equalsIgnoreCase("")){
						    certificatesStore+="#9205";
						    totalHirsInJob=totalHirsInJob+changeVal(code_9205);
						    tempHires.put("9205", changeVal(code_9205));
						}
						if(!code_9206.equalsIgnoreCase("")){
						    certificatesStore+="#9206";
						    totalHirsInJob=totalHirsInJob+changeVal(code_9206);
						    tempHires.put("9206", changeVal(code_9206));
						}
						if(!code_9307.equalsIgnoreCase("")){
						    certificatesStore+="#9307";
						    totalHirsInJob=totalHirsInJob+changeVal(code_9307);
						    tempHires.put("9307", changeVal(code_9307));
						}
						if(!code_9308.equalsIgnoreCase("")){
						    certificatesStore+="#9308";
						    totalHirsInJob=totalHirsInJob+changeVal(code_9308);
						    tempHires.put("9308", changeVal(code_9308));
						}
						if(!code_9309.equalsIgnoreCase("")){
						    certificatesStore+="#9309";
						    totalHirsInJob=totalHirsInJob+changeVal(code_9309);
						    tempHires.put("9309", changeVal(code_9309));
						}
						if(!code_9310.equalsIgnoreCase("")){
						    certificatesStore+="#9310";
						    totalHirsInJob=totalHirsInJob+changeVal(code_9310);
						    tempHires.put("9310", changeVal(code_9310));
						}
						if(!code_3288.equalsIgnoreCase("")){
							certificatesStore+="#3288";
							totalHirsInJob=totalHirsInJob+changeVal(code_3288);
							tempHires.put("3288", changeVal(code_3288));
						}
						if(!code_6884.equalsIgnoreCase("")){
							certificatesStore+="#6884";
							totalHirsInJob=totalHirsInJob+changeVal(code_6884);
							tempHires.put("6884", changeVal(code_6884));
						}
						if(!code_9235.equalsIgnoreCase("")){
						    certificatesStore+="#9235";
						    totalHirsInJob=totalHirsInJob+changeVal(code_9235);
						    tempHires.put("9235", changeVal(code_9235));
						}
						if(!code_9270.equalsIgnoreCase("")){
						    certificatesStore+="#9270";
						    totalHirsInJob=totalHirsInJob+changeVal(code_9270);
						    tempHires.put("9270", changeVal(code_9270));
						}
						if(!code_9290.equalsIgnoreCase("")){
						    certificatesStore+="#9290";
						    totalHirsInJob=totalHirsInJob+changeVal(code_9290);
						    tempHires.put("9290", changeVal(code_9290));
						}						

							if(certificatesStore!=""){
								certificatesStore=certificatesStore.substring(1);								
								List<String> certLists = Arrays.asList(certificatesStore.split("#"));
								for (int k = 0; k < certLists.size(); k++) {
									String cert = certLists.get(k);
									if(sdpCodesMap!=null && sdpCodesMap.size()>0 && !sdpCodesMap.containsKey(cert)){
										if(errorFlag){
											errorText+=",";	
								}
								errorText+=cert+" Not Exist";
										errorFlag=true;
									}
								}
							}
							
						String title=JobCategory+" "+schoolMaster.getSchoolName();
						if(existingJobsMap!=null && existingJobsMap.size()>0 && existingJobsMap.containsKey(title)){
							if(errorFlag){
								errorText+=",";	
							}
							errorText+=msgRejectedTheCandidate33;
							errorFlag=true;
						}else{
							insertFlag=true;
						}
						
						if(JobCategory.toLowerCase().trim().equalsIgnoreCase("teacher") && certificatesStore.equalsIgnoreCase("")){
							if(errorFlag){
								errorText+=",";	
							}
							errorText+=msgRejectedTheCandidate33;
							errorFlag=true;						
						}
						if(errorText.equals("")){
							boolean jobOrderFlag=false;														
							if(insertFlag){
								try {					
									 for (Entry<String, Integer> entry : tempHires.entrySet()){
										 JobOrder jobOrder = new JobOrder();
										 jobOrder.setDistrictMaster(districtMaster);
											//jobOrder.setJobTitle(title);
											if(DateAdded.equalsIgnoreCase(""))
												jobOrder.setJobStartDate(new java.util.Date());
											else{
												String changeDate = monthReplace(DateAdded);
												jobOrder.setJobStartDate(Utility.getCurrentDateFormart(changeDate));
											}
											
											jobOrder.setJobEndDate(Utility.getCurrentDateFormart("12-25-2099"));
											jobOrder.setGeoZoneMaster(schoolMaster.getGeoZoneMaster());
											jobOrder.setJobCategoryMaster(jobCatMap.get(JobCategory.toLowerCase()));
											jobOrder.setWritePrivilegeToSchool(false);
											jobOrder.setAllSchoolsInDistrict(2);
											jobOrder.setAllGrades(2);
											jobOrder.setPkOffered(2);
											jobOrder.setKgOffered(2);
											jobOrder.setG01Offered(2);
											jobOrder.setG02Offered(2);
											jobOrder.setG03Offered(2);
											jobOrder.setG04Offered(2);
											jobOrder.setG05Offered(2);
											jobOrder.setG06Offered(2);
											jobOrder.setG07Offered(2);
											jobOrder.setG08Offered(2);
											jobOrder.setG09Offered(2);
											jobOrder.setG10Offered(2);
											jobOrder.setG11Offered(2);
											jobOrder.setG12Offered(2);
											jobOrder.setIsJobAssessment(false);
											jobOrder.setAttachDefaultDistrictPillar(2);
											jobOrder.setAttachDefaultSchoolPillar(2);
											jobOrder.setAttachDefaultJobSpecificPillar(2);
											jobOrder.setJobStatus("O");
											jobOrder.setJobType("F");
											jobOrder.setJobAssessmentStatus(0);
											jobOrder.setOfferVirtualVideoInterview(false);
											jobOrder.setOfferAssessmentInviteOnly(false);
											jobOrder.setAssessmentDocument(null);
											jobOrder.setAttachNewPillar(2);								
											jobOrder.setSelectedSchoolsInDistrict(1);
											jobOrder.setNoSchoolAttach(1);
											jobOrder.setNoOfExpHires(null);
											jobOrder.setSendAutoVVILink(false);
											jobOrder.setIsPoolJob(0);
											jobOrder.setApprovalBeforeGoLive(1);
											jobOrder.setCreatedBy(userMaster.getUserId());
											jobOrder.setCreatedByEntity(userMaster.getEntityType());
											jobOrder.setStatus("A");
											jobOrder.setIsInviteOnly(false);
											jobOrder.setPathOfJobDescription(Notes);
											jobOrder.setFlagForURL(districtMaster.getFlagForURL());
											jobOrder.setExitMessage(districtMaster.getExitMessage());
											jobOrder.setExitURL(districtMaster.getExitURL());
											jobOrder.setApiJobId("");
											jobOrder.setCreatedForEntity(2);
											jobOrder.setIsVacancyJob(true);
											jobOrder.setCreatedDateTime(new java.util.Date());							
											jobOrder.setIsPortfolioNeeded(true);
											
										 String CertificateCode = entry.getKey();
										 Integer noOfHires = entry.getValue();
										 
										 String jobTitleCerti = "";
										 if(certiMap.containsKey(CertificateCode)){					 
												if(certiMap.get(CertificateCode)==null){
													for (CertificateTypeMaster certificateTypeMaster2 : certificateTypeMaster) {														
														if(certificateTypeMaster2.getCertTypeCode()!=null && certificateTypeMaster2.getCertTypeCode().equalsIgnoreCase(CertificateCode))
														if(!certificateTypeMaster2.getCertType().equalsIgnoreCase("")){
															jobTitleCerti=jobTitleCerti+", "+certificateTypeMaster2.getCertType();
														}
													}
													jobTitleCerti.replaceFirst(",", "");
												}else{	
													jobTitleCerti = certiMap.get(CertificateCode).getCertType();
												}
										 }
										 
										 boolean internalInsert=false;
										 String jobTitle = jobOrder.getJobCategoryMaster().getJobCategoryName()+" - "+schoolMaster.getSchoolName()+" - "+sdpCodesAndTitleMap.get(CertificateCode);
										 
										 String[] tempChangeDate = monthReplace(DateAdded).split("-");
										 String checkexistiJObs = "";
										 String flflflflf="";
										 if(!DateAdded.equalsIgnoreCase("")){
											 checkexistiJObs = jobTitle+"#"+tempChangeDate[2]+"-"+tempChangeDate[0]+"-"+tempChangeDate[1];
											 flflflflf="1";
									 	}else{
											 checkexistiJObs = jobTitle;
											 flflflflf="2";
									 	}
										 
										 if(existingJobsMap!=null && existingJobsMap.size()>0 && existingJobsMap.containsKey(checkexistiJObs)){
												if(errorFlag){
													errorText+=",";	
												}
												errorText+="Job Already Exist";
												errorFlag=true;
											}else{
												internalInsert=true;
											}
										 
								if(internalInsert){
									 	/*******************Create Job **********************/
											 jobOrder.setJobTitle(jobTitle);
											 	txOpen =statelesSsession.beginTransaction();
												statelesSsession.insert(jobOrder);
									    	 	txOpen.commit();
									    	 	
									    	 	 
									    	 	 
									    	 	existingJobsMap.put(checkexistiJObs, jobOrder);
							    	 	/*******************End Create Job *****************/
									    	 
									   /*******************Create Number Of Hires **********************/
									    	 	SchoolInJobOrder schoolInJobOrder = new SchoolInJobOrder();			
												schoolInJobOrder.setJobId(jobOrder);
										 		schoolInJobOrder.setSchoolId(schoolMaster);
										 		schoolInJobOrder.setNoOfSchoolExpHires(noOfHires);
										 		schoolInJobOrder.setCreatedDateTime(new java.util.Date());
										 		txOpen =statelesSsession.beginTransaction();
									    	 	statelesSsession.insert(schoolInJobOrder);
									    	 	txOpen.commit();
									   /*******************End Number Of Hires ************************/
									    	 if(!Classification.equalsIgnoreCase("SBS"))	
									    	 	if(sdpCodesMap.containsKey(CertificateCode)){
													String sdpCodes = sdpCodesMap.get(CertificateCode);
													List<String> sdpCodeList = new ArrayList<String>();
													if(sdpCodes!=null){
														if(!sdpCodes.contains(","))
															sdpCodeList.add(sdpCodes);
														else
														{
															String[] sdpCodeArray = sdpCodes.split(",");
															for(String sdpCode : sdpCodeArray)
																sdpCodeList.add(sdpCode);
														}
													}
													
													for (String string : sdpCodeList) {
														if(certiMap.containsKey(string)){
															JobCertification jobCertification = new JobCertification();
															jobCertification.setJobId(jobOrder);
															jobCertification.setCertificateTypeMaster(certiMap.get(string));
															jobCertification.setCertType(null);
															jobCertification.setStateMaster(certiMap.get(string).getStateId());
															
															txOpen =statelesSsession.beginTransaction();
															statelesSsession.insert(jobCertification);
												    	 	txOpen.commit();
														}
													}
													
												}
											/* if(certiMap.containsKey(CertificateCode)){
												if(certiMap.get(CertificateCode)==null){
													for (CertificateTypeMaster certificateTypeMaster3 : certificateTypeMaster) {
														if(certificateTypeMaster3.getCertTypeCode().equalsIgnoreCase(CertificateCode)){
															JobCertification jobCertification = new JobCertification();
															jobCertification.setJobId(jobOrder);
															jobCertification.setCertificateTypeMaster(certificateTypeMaster3);
															jobCertification.setCertType(null);
															jobCertification.setStateMaster(certificateTypeMaster3.getStateId());
															
															txOpen =statelesSsession.beginTransaction();
															statelesSsession.insert(jobCertification);
												    	 	txOpen.commit();
														}
													}
												}else{						
													JobCertification jobCertification = new JobCertification();
													jobCertification.setJobId(jobOrder);
													jobCertification.setCertificateTypeMaster(certiMap.get(CertificateCode));
													jobCertification.setCertType(null);
													jobCertification.setStateMaster(certiMap.get(CertificateCode).getStateId());
													
													txOpen =statelesSsession.beginTransaction();
													statelesSsession.insert(jobCertification);
										    	 	txOpen.commit();
												} 
												 
											 }*/
										 }else{							
												int row = i+1;
												final_error_store+="Row "+row+" : "+errorText+"\r\n"+ZoneCode+","+SchoolCode+","+GradeLevel+","+HighNeedsStatus+","+DateAdded+","+Notes+","+Classification+","+JobCategory+"<>";								
										}
									 }
									 
								
									
								} catch (Exception e) {
									e.printStackTrace();
								}
								
			            	 	
							}
						}else{		
							//zone code,school code,date added,notes,classification,job category
								int row = i+1;
								final_error_store+="Row "+row+" : "+errorText+"\r\n"+ZoneCode+","+SchoolCode+","+DateAdded+","+Notes+","+Classification+","+JobCategory+"<>";								
						}
					}
				}
				
				java.util.Date d =new java.util.Date();
				
				deleteXlsAndXlsxFile(filePath);
				if(!final_error_store.equalsIgnoreCase("")){
					String content = final_error_store;		    				
					File file = new File(folderPath+sheetName+"_UploadError"+d.getTime()+".txt");
					
					// if file doesnt exists, then create it
					if (!file.exists()) {
						file.createNewFile();
					}
					String[] parts = content.split("<>");	    				
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write("zone code,school code,date added,notes,classification,job category");
					bw.write("\r\n\r\n");
					int k =0;
					for(String cont :parts) {
						bw.write(cont+"\r\n\r\n");
					    k++;
					}
					bw.close();
					sb.append("\n------------------Complete------------");
					String to= "gourav@netsutra.com";
					String subject = msgRejectedTheCandidate35;
					String msg =msgRejectedTheCandidate36;
					String from= "gourav@netsutra.com";
					String errorFileName=file.toString();						
					
					//emailerService.sendMailWithAttachments(to,subject,from,msg,errorFileName);
					}
			 }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
public Map<String, JobOrder> manageJobCertificationsAndHiresInschool(JobOrder jobOrderObj,Map<String, CertificateTypeMaster> certiMap,Map<String, JobOrder> existingJobsMap,List<CertificateTypeMaster> certFicatesList,Map<String,Integer> temMap,SchoolMaster schoolMaster, Map<String, String> sdpCodesMap){
		
		//List<String> certLists = Arrays.asList(certifications.split("#"));
	List<String> certLists = null;
		
		SessionFactory sessionFactory=jobCertificationDAO.getSessionFactory();
		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
		Transaction txOpen =statelesSsession.beginTransaction();
		
		try {
			 for (Entry<String, Integer> entry : temMap.entrySet()){
				 
				 JobOrder jobOrder = jobOrderObj;
				 String CertificateCode = entry.getKey();
				 Integer noOfHires = entry.getValue();
				 
				 String jobTitleCerti = "";
				 if(certiMap.containsKey(CertificateCode)){					 
						if(certiMap.get(CertificateCode)==null){
							for (CertificateTypeMaster certificateTypeMaster : certFicatesList) {
								if(certificateTypeMaster.getCertTypeCode().equalsIgnoreCase(CertificateCode))
								if(!certificateTypeMaster.getCertType().equalsIgnoreCase("")){
									jobTitleCerti=jobTitleCerti+", "+certificateTypeMaster.getCertType();
									//break;
								}
							}
							jobTitleCerti.replaceFirst(",", "");
						}else{	
							jobTitleCerti = certiMap.get(CertificateCode).getCertType();
						}
				 }
		 	/*******************Create Job **********************/
				 
				 String jobTitle = jobOrder.getJobCategoryMaster().getJobCategoryName()+" - "+schoolMaster.getSchoolName()+" - "+jobTitleCerti;
				 jobOrder.setJobTitle(jobTitle);
				 	txOpen =statelesSsession.beginTransaction();
					statelesSsession.insert(jobOrder);
		    	 	txOpen.commit();
    	 	/*******************End Create Job *****************/
		    	 
		   /*******************Create Number Of Hires **********************/
		    	 	SchoolInJobOrder schoolInJobOrder = new SchoolInJobOrder();			
					schoolInJobOrder.setJobId(jobOrder);
			 		schoolInJobOrder.setSchoolId(schoolMaster);
			 		schoolInJobOrder.setNoOfSchoolExpHires(noOfHires);
			 		schoolInJobOrder.setCreatedDateTime(new java.util.Date());
			 		txOpen =statelesSsession.beginTransaction();
		    	 	statelesSsession.insert(schoolInJobOrder);
		    	 	txOpen.commit();
		   /*******************End Number Of Hires ************************/
		    	 	
				/* if(certiMap.containsKey(CertificateCode)){
					 
					if(certiMap.get(CertificateCode)==null){
						for (CertificateTypeMaster certificateTypeMaster : certFicatesList) {
							if(certificateTypeMaster.getCertTypeCode().equalsIgnoreCase(CertificateCode)){
								JobCertification jobCertification = new JobCertification();
								jobCertification.setJobId(jobOrder);
								jobCertification.setCertificateTypeMaster(certificateTypeMaster);
								jobCertification.setCertType(null);
								jobCertification.setStateMaster(certificateTypeMaster.getStateId());
								
								txOpen =statelesSsession.beginTransaction();
								statelesSsession.insert(jobCertification);
					    	 	txOpen.commit();
							}
						}
					}else{						
						JobCertification jobCertification = new JobCertification();
						jobCertification.setJobId(jobOrder);
						jobCertification.setCertificateTypeMaster(certiMap.get(CertificateCode));
						jobCertification.setCertType(null);
						jobCertification.setStateMaster(certiMap.get(CertificateCode).getStateId());
						
						txOpen =statelesSsession.beginTransaction();
						statelesSsession.insert(jobCertification);
			    	 	txOpen.commit();
					} 
					 
				 }*/
				 
				if(sdpCodesMap.containsKey(CertificateCode)){
					String sdpCodes = sdpCodesMap.get(CertificateCode);
					List<String> sdpCodeList = new ArrayList<String>();
					if(sdpCodes!=null){
						if(!sdpCodes.contains(","))
							sdpCodeList.add(sdpCodes);
						else
						{
							String[] sdpCodeArray = sdpCodes.split(",");
							for(String sdpCode : sdpCodeArray)
								sdpCodeList.add(sdpCode);
						}
					}
					for (String string : sdpCodeList) {
						if(certiMap.containsKey(string)){
							JobCertification jobCertification = new JobCertification();
							jobCertification.setJobId(jobOrder);
							jobCertification.setCertificateTypeMaster(certiMap.get(CertificateCode));
							jobCertification.setCertType(null);
							jobCertification.setStateMaster(certiMap.get(CertificateCode).getStateId());
							
							txOpen =statelesSsession.beginTransaction();
							statelesSsession.insert(jobCertification);
				    	 	txOpen.commit();
						}
					}
					
				}
			 }
			 
		} catch (Exception e) {
			// TODO: handle exception
		}
			
		return existingJobsMap;
	}
	
	public Map<String, SchoolInJobOrder> updateSchoolInJobOrder(JobOrder jobOrder,SchoolMaster schoolMaster,Integer num_hires,Map<String, SchoolInJobOrder>schoolInJobOrderMap){
		try {
			SessionFactory sessionFactory=schoolInJobOrderDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			Transaction txOpen =statelesSsession.beginTransaction();
			SchoolInJobOrder schoolInJobOrder = new SchoolInJobOrder();			
			schoolInJobOrder.setJobId(jobOrder);
	 		schoolInJobOrder.setSchoolId(schoolMaster);
	 		schoolInJobOrder.setNoOfSchoolExpHires(num_hires);
	 		schoolInJobOrder.setCreatedDateTime(new java.util.Date());
	 		txOpen =statelesSsession.beginTransaction();
    	 	statelesSsession.insert(schoolInJobOrder);
    	 	txOpen.commit();
    	 	
    	 	String jobAndsch=jobOrder.getJobId()+"||"+schoolMaster.getSchoolId();
    	 	schoolInJobOrderMap.put(jobAndsch, schoolInJobOrder);
		} catch (Exception e) {
			e.printStackTrace();
		}
	return schoolInJobOrderMap;
	} 
	public Boolean createJobAssessment(AssessmentDetail assessmentDetail,JobOrder jobOrder,UserMaster userMaster){
		
		SessionFactory sessionFactory=jobOrderDAO.getSessionFactory();
		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
		Transaction txOpen =statelesSsession.beginTransaction();
		
			AssessmentJobRelation assessmentJobRelation = new AssessmentJobRelation();
			assessmentJobRelation.setAssessmentId(assessmentDetail);
			assessmentJobRelation.setJobId(jobOrder);
			assessmentJobRelation.setCreatedBy(userMaster);
			assessmentJobRelation.setStatus("A");
			assessmentJobRelation.setIpaddress("122.177.4.102");
			assessmentJobRelation.setCreatedDateTime(new java.util.Date());
			
			txOpen =statelesSsession.beginTransaction();
    	 	statelesSsession.insert(assessmentJobRelation);
    	 	txOpen.commit();
    	 	
    	 	if(assessmentJobRelation.getAssessmentId()!=null){
    	 		return true;
    	 	}
		return false;		
	}
	
	public String[] manageRequistiinNumbers(Map<String,DistrictRequisitionNumbers>requisitionNumberMap,Map<String, JobRequisitionNumbers> jobReqMap,Map<String, SchoolInJobOrder>schoolInJobOrderMap,JobOrder jobOrder,UserMaster userMaster,SchoolMaster schoolMaster,String requisitionNumbers,DistrictMaster districtMaster){
		List<String> reqList = Arrays.asList(requisitionNumbers.split(","));
		
		//jobReqMap
		SessionFactory sessionFactory=districtRequisitionNumbersDAO.getSessionFactory();
		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
		Transaction txOpen =statelesSsession.beginTransaction();
		
		String[] arr = new String[3];
		
		for (String string : reqList) {			
			String jobAndsch=jobOrder.getJobId()+"||"+schoolMaster.getSchoolId();
			DistrictRequisitionNumbers districtRequisitionNumbers = new DistrictRequisitionNumbers();
			JobRequisitionNumbers jobRequisitionNumbers = new JobRequisitionNumbers();
			SchoolInJobOrder schoolInJobOrder = new SchoolInJobOrder();
				if(!jobReqMap.containsKey(string)){
					if(requisitionNumberMap.containsKey(string)){
						districtRequisitionNumbers = requisitionNumberMap.get(string);
						districtRequisitionNumbers.setIsUsed(true);
						txOpen =statelesSsession.beginTransaction();
			    	 	statelesSsession.update(districtRequisitionNumbers);
			    	 	txOpen.commit();
			    	 	requisitionNumberMap.remove(string);
			    	 	requisitionNumberMap.put(string,districtRequisitionNumbers);
					}else{
						districtRequisitionNumbers.setDistrictMaster(districtMaster);
						districtRequisitionNumbers.setRequisitionNumber(string);
						districtRequisitionNumbers.setIsUsed(true);
						districtRequisitionNumbers.setUploadFrom("P");
						districtRequisitionNumbers.setUserMaster(userMaster);
						districtRequisitionNumbers.setCreatedDateTime(new java.util.Date());
						txOpen =statelesSsession.beginTransaction();
			    	 	statelesSsession.insert(districtRequisitionNumbers);
			    	 	txOpen.commit();
			    	 	requisitionNumberMap.put(string,districtRequisitionNumbers);
					}
					
					jobRequisitionNumbers.setJobOrder(jobOrder);
					jobRequisitionNumbers.setSchoolMaster(schoolMaster);
					jobRequisitionNumbers.setDistrictRequisitionNumbers(districtRequisitionNumbers);
					jobRequisitionNumbers.setStatus(0);
					txOpen =statelesSsession.beginTransaction();
		    	 	statelesSsession.insert(jobRequisitionNumbers);
		    	 	txOpen.commit();
		    	 	
		    	 	jobReqMap.put(string, jobRequisitionNumbers);
		    	 	
		    	 	Integer noOFexp = 0;
			    	 	if(schoolInJobOrderMap.containsKey(jobAndsch)){
			    	 		noOFexp=schoolInJobOrderMap.get(jobAndsch).getNoOfSchoolExpHires();			    	 		
			    	 		schoolInJobOrder.setJobId(jobOrder);
			    	 		schoolInJobOrder.setSchoolId(schoolMaster);
			    	 		schoolInJobOrder.setNoOfSchoolExpHires(noOFexp+1);
			    	 		schoolInJobOrder.setCreatedDateTime(new java.util.Date());
			    	 		txOpen =statelesSsession.beginTransaction();
				    	 	statelesSsession.update(schoolInJobOrder);
				    	 	txOpen.commit();
				    	 	schoolInJobOrderMap.remove(string);				    	 	
			    	 	}else{
			    	 		schoolInJobOrder.setJobId(jobOrder);
			    	 		schoolInJobOrder.setSchoolId(schoolMaster);
			    	 		schoolInJobOrder.setNoOfSchoolExpHires(noOFexp+1);
			    	 		schoolInJobOrder.setCreatedDateTime(new java.util.Date());
			    	 		txOpen =statelesSsession.beginTransaction();
				    	 	statelesSsession.insert(schoolInJobOrder);
				    	 	txOpen.commit();
			    	 	}
			    	 	schoolInJobOrderMap.put(string, schoolInJobOrder);
				}
				
				//List<String> reqList2= jobReqMap.getClass()
				for (Entry<String, JobRequisitionNumbers> entry : jobReqMap.entrySet()){					
					
					JobRequisitionNumbers jobReq = entry.getValue();
					SchoolInJobOrder sjs = schoolInJobOrderMap.get(jobReq.getJobOrder().getJobId()+"||"+jobReq.getSchoolMaster().getSchoolId());
					Integer nexph = sjs.getNoOfSchoolExpHires();
					String reqDl=jobReq.getDistrictRequisitionNumbers().getRequisitionNumber();
					if(jobReq.getJobOrder().getJobId().equals(jobOrder.getJobId())){
						if(!reqList.contains(reqDl))
						{
							DistrictRequisitionNumbers dReq = jobReq.getDistrictRequisitionNumbers();						 
							txOpen =statelesSsession.beginTransaction();
				    	 	statelesSsession.delete(dReq);
				    	 	statelesSsession.delete(jobReq);
				    	 	sjs.setNoOfSchoolExpHires(nexph-1);
				    	 	statelesSsession.update(sjs);
				    	 	txOpen.commit();
				    	 	requisitionNumberMap.remove(reqDl);
				    	 	jobReqMap.remove(reqDl);
						}
					}
				}
		}
		return arr;
	}
	
	public Map<Integer, String> manageJobCertifications(JobOrder jobOrderObj,String certifications,Map<String, CertificateTypeMaster> certiMap,Map<Integer, String> jobCertMap,Map<String, JobOrder> existingJobsMap,List<CertificateTypeMaster> certFicatesList){
		
		List<String> certLists = Arrays.asList(certifications.split("#"));
		
		SessionFactory sessionFactory=jobCertificationDAO.getSessionFactory();
		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
		Transaction txOpen =statelesSsession.beginTransaction();
			
		
		for (int i = 0; i < certLists.size(); i++) {
			
			
			
			String cert = certLists.get(i);
			if(certiMap!=null && certiMap.size()>0 && certiMap.containsKey(cert)){
				try {
					
					JobOrder jobOrder = jobOrderObj;
				
					txOpen =statelesSsession.beginTransaction();
					statelesSsession.insert(jobOrderObj);
		    	 	txOpen.commit();
					
				JobCertification jobCertification = new JobCertification();
				jobCertification.setJobId(jobOrder);
				jobCertification.setCertificateTypeMaster(certiMap.get(cert));
				jobCertification.setCertType(null);
				jobCertification.setStateMaster(certiMap.get(cert).getStateId());
				
				txOpen =statelesSsession.beginTransaction();
				statelesSsession.insert(jobCertification);
	    	 	txOpen.commit();
	    	 	    		
				
	    	 	Integer jobId = jobOrder.getJobId();
	    	 	if(jobCertMap.containsKey(jobId)){
					String certList = jobCertMap.get(jobId); //jobCertification.getCertificateTypeMaster().getCertTypeCode();
					jobCertMap.put(jobId, jobCertification.getCertificateTypeMaster().getCertTypeCode()+"#"+certList);
				}else{
					jobCertMap.put(jobId, jobCertification.getCertificateTypeMaster().getCertTypeCode());
				}    	 	
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		}
			
		return jobCertMap;
	}
	
	public File findLatestFilesByPath(String path)
	{
		String pathToScan = path;
		File dir = new File(pathToScan);	
		
		File[] files2 = dir.listFiles( new FilenameFilter() {
		    public boolean accept( File dir,String name ) {
		    	//return name.startsWith("TM_instructional_vacancies");
		    	//return name.endsWith("psd_vacancy_master.csv");
		    	return name.startsWith("TM vacancy list");
		    	
		    }
		} );
				        
        if(files2.length == 0)
            return null;       
		        
		 File lastModifiedFile = files2[0];
	        for(int i = 1; i < files2.length; i++){
		            if(lastModifiedFile.lastModified() < files2[i].lastModified()){	            	
		                lastModifiedFile = files2[i];	            	
		            }
	        }       
		        		return lastModifiedFile;
	}
	
	@SuppressWarnings("unchecked")
	public static Vector readDataExcelXLS(HSSFSheet sheetR) throws IOException {
		System.out.println(":::::::::::::::::readDataExcel XLS::::::::::::");
		Vector vectorData = new Vector();
		try{
		
		HSSFSheet sheet=sheetR;
		HSSFRow row; 
		HSSFCell cell;

		Iterator rows = sheet.rowIterator();
		int rowshdr_count=0;
		int ZoneCode_count=1000;
		int SchoolCode_count=1000;
		int GradeLevel_count=1000;
		int HighNeedsStatus_count=1000;
		int DateAdded_count=1000;
		int Notes_count=1000;
		int JobCategory_count=1000;
		int Classification_count=1000;		
		int code_0502_count=1000;
		int code_0807_count=1000;
		int code_0813_count=1000;
		int code_0815_count=1000;
		int code_0863_count=1000;
		int code_0885_count=1000;
		int code_1000_count=1000;
		int code_1052_count=1000;
		int code_1111_count=1000;
		int code_1405_count=1000;
		int code_1603_count=1000;
		int code_1668_count=1000;
		int code_1836_count=1000;
		int code_1837_count=1000;
		int code_2000_count=1000;
		int code_2825_count=1000;
		int code_3100_count=1000;
		int code_3101_count=1000;
		int code_3105_count=1000;
		int code_3108_count=1000;
		int code_3109_count=1000;
		int code_3230_count=1000;
		int code_4005_count=1000;
		int code_4030_count=1000;
		int code_4405_count=1000;
		int code_4410_count=1000;
		int code_4420_count=1000;
		int code_4430_count=1000;
		int code_4480_count=1000;
		int code_4490_count=1000;
		int code_4499_count=1000;
		int code_4805_count=1000;
		int code_6420_count=1000;
		int code_6800_count=1000;
		int code_7205_count=1000;
		int code_7207_count=1000;
		int code_7605_count=1000;
		int code_7606_count=1000;
		int code_7607_count=1000;
		int code_7610_count=1000;
		int code_7621_count=1000;
		int code_7650_count=1000;
		int code_8405_count=1000;
		int code_8420_count=1000;
		int code_8450_count=1000;
		int code_8470_count=1000;
		int code_8875_count=1000;
		int code_9201_count=1000;
		int code_9202_count=1000;
		int code_9203_count=1000;
		int code_9204_count=1000;
		int code_9205_count=1000;
		int code_9206_count=1000;
		int code_9307_count=1000;
		int code_9308_count=1000;
		int code_9309_count=1000;
		int code_9310_count=1000;
		int code_3288_count=1000;
		int code_6884_count=1000;
		int code_9235_count=1000;
		int code_9270_count=1000;
		int code_9290_count=1000;
		  String indexCheck="";
		  int lineNumberHdr=0;
		  while (rows.hasNext()){
				row=(HSSFRow) rows.next();
				Iterator cells = row.cellIterator();
				Vector vectorCellEachRowData = new Vector();
				int cIndex=0; 
				boolean cellFlag=false,dateFlag=false;
				String indexPrev="";
				Map<Integer,String> mapCell = new TreeMap<Integer, String>();
				while (cells.hasNext())
				{
					cell=(HSSFCell) cells.next();
					cIndex=cell.getColumnIndex();
					
					if(cell.toString().toLowerCase().trim().equalsIgnoreCase("date added")){
						DateAdded_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(DateAdded_count==cIndex){
						cellFlag=true;
						dateFlag=true;
					}

					if(!dateFlag){
						cell.setCellType(1);
					}
					
					if(cell.toString().toLowerCase().trim().equalsIgnoreCase("Zone Code")){
						ZoneCode_count=cIndex;
					indexCheck+="||"+cIndex+"||";
					}
					
					if(ZoneCode_count==cIndex){
						cellFlag=true;
					}
					
					if(cell.toString().trim().toLowerCase().equalsIgnoreCase("School Code")){
						SchoolCode_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(SchoolCode_count==cIndex){
						cellFlag=true;
					
					}	
					//GradeLevel_count
					if(cell.toString().toLowerCase().trim().equalsIgnoreCase("gradelevel")){
						GradeLevel_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(GradeLevel_count==cIndex){
						cellFlag=true;
					
					}
					//HighNeedsStatus_count
					if(cell.toString().toLowerCase().trim().equalsIgnoreCase("highneedsstatus")){
						HighNeedsStatus_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(HighNeedsStatus_count==cIndex){
						cellFlag=true;
					
					}
					
					//Notes_count
					
					if(cell.toString().toLowerCase().trim().equalsIgnoreCase("notes")){
						Notes_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(Notes_count==cIndex){
						cellFlag=true;
					}
					if(cell.toString().toLowerCase().trim().equalsIgnoreCase("job category")){
						JobCategory_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(JobCategory_count==cIndex){
						cellFlag=true;		
					}
					if(cell.toString().toLowerCase().trim().equalsIgnoreCase("classification")){
						Classification_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(Classification_count==cIndex){
						cellFlag=true;
					}
					
					if(cell.toString().trim().equalsIgnoreCase("0502")){
						code_0502_count=cIndex;
						indexCheck+="||"+cIndex+"||";
					}
					if(code_0502_count==cIndex){
						cellFlag=true;
					}
						if(cell.toString().trim().equalsIgnoreCase("0807")){
						code_0807_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_0807_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("0813")){
						code_0813_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_0813_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("0815")){
						code_0815_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_0815_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("0863")){
						code_0863_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_0863_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("0885")){
						code_0885_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_0885_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("1000")){
						code_1000_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_1000_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("1052")){
						code_1052_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_1052_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("1111")){
						code_1111_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_1111_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("1405")){
						code_1405_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_1405_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("1603")){
						code_1603_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_1603_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("1668")){
						code_1668_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_1668_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("1836")){
						code_1836_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_1836_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("1837")){
						code_1837_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_1837_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("2000")){
						code_2000_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_2000_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("2825")){
						code_2825_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_2825_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("3100")){
						code_3100_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_3100_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("3101")){
						code_3101_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_3101_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("3105")){
						code_3105_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_3105_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("3108")){
						code_3108_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_3108_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("3109")){
						code_3109_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_3109_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("3230")){
						code_3230_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_3230_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("4005")){
						code_4005_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_4005_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("4030")){
						code_4030_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_4030_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("4405")){
						code_4405_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_4405_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("4410")){
						code_4410_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_4410_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("4420")){
						code_4420_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_4420_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("4430")){
						code_4430_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_4430_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("4480")){
						code_4480_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_4480_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("4490")){
						code_4490_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_4490_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("4499")){
						code_4499_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_4499_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("4805")){
						code_4805_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_4805_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("6420")){
						code_6420_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_6420_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("6800")){
						code_6800_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_6800_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("7205")){
						code_7205_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_7205_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("7207")){
						code_7207_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_7207_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("7605")){
						code_7605_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_7605_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("7606")){
						code_7606_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_7606_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("7607")){
						code_7607_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_7607_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("7610")){
						code_7610_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_7610_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("7621")){
						code_7621_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_7621_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("7650")){
						code_7650_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_7650_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("8405")){
						code_8405_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_8405_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("8420")){
						code_8420_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_8420_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("8450")){
						code_8450_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_8450_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("8470")){
						code_8470_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_8470_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("8875")){
						code_8875_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_8875_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("9201")){
							code_9201_count=cIndex;
							indexCheck+="||"+cIndex+"||";
							}
							if(code_9201_count==cIndex){
							cellFlag=true;
							}
							if(cell.toString().trim().equalsIgnoreCase("9202")){
							code_9202_count=cIndex;
							indexCheck+="||"+cIndex+"||";
							}
							if(code_9202_count==cIndex){
							cellFlag=true;
							}
							if(cell.toString().trim().equalsIgnoreCase("9203")){
							code_9203_count=cIndex;
							indexCheck+="||"+cIndex+"||";
							}
							if(code_9203_count==cIndex){
							cellFlag=true;
							}
							if(cell.toString().trim().equalsIgnoreCase("9204")){
							code_9204_count=cIndex;
							indexCheck+="||"+cIndex+"||";
							}
							if(code_9204_count==cIndex){
							cellFlag=true;
							}
							if(cell.toString().trim().equalsIgnoreCase("9205")){
							code_9205_count=cIndex;
							indexCheck+="||"+cIndex+"||";
							}
							if(code_9205_count==cIndex){
							cellFlag=true;
							}
							if(cell.toString().trim().equalsIgnoreCase("9206")){
							code_9206_count=cIndex;
							indexCheck+="||"+cIndex+"||";
							}
							if(code_9206_count==cIndex){
							cellFlag=true;
							}
							if(cell.toString().trim().equalsIgnoreCase("9307")){
							code_9307_count=cIndex;
							indexCheck+="||"+cIndex+"||";
							}
							if(code_9307_count==cIndex){
							cellFlag=true;
							}
							if(cell.toString().trim().equalsIgnoreCase("9308")){
							code_9308_count=cIndex;
							indexCheck+="||"+cIndex+"||";
							}
							if(code_9308_count==cIndex){
							cellFlag=true;
							}
							if(cell.toString().trim().equalsIgnoreCase("9309")){
							code_9309_count=cIndex;
							indexCheck+="||"+cIndex+"||";
							}
							if(code_9309_count==cIndex){
							cellFlag=true;
							}
							if(cell.toString().trim().equalsIgnoreCase("9310")){
							code_9310_count=cIndex;
							indexCheck+="||"+cIndex+"||";
							}
							if(code_9310_count==cIndex){
							cellFlag=true;
							}
							if(cell.toString().trim().equalsIgnoreCase("3288")){
							code_3288_count=cIndex;
							indexCheck+="||"+cIndex+"||";
							}
							if(code_3288_count==cIndex){
							cellFlag=true;
							}
							if(cell.toString().trim().equalsIgnoreCase("6884")){
							code_6884_count=cIndex;
							indexCheck+="||"+cIndex+"||";
							}
							if(code_6884_count==cIndex){
							cellFlag=true;
							}
						if(cell.toString().trim().equalsIgnoreCase("9235")){
						code_9235_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_9235_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("9270")){
						code_9270_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_9270_count==cIndex){
						cellFlag=true;
						}
						if(cell.toString().trim().equalsIgnoreCase("9290")){
						code_9290_count=cIndex;
						indexCheck+="||"+cIndex+"||";
						}
						if(code_9290_count==cIndex){
						cellFlag=true;
						}
						/*if(dateFlag==false){
							cell.setCellType(1);
						}*/
							if(lineNumberHdr==0){
							if(cellFlag){
								if(!dateFlag){
									try{
										mapCell.put(Integer.valueOf(cIndex),"||"+cell.getStringCellValue()+"||");
									}catch(Exception e){
										mapCell.put(Integer.valueOf(cIndex),new Double("||"+cell.getNumericCellValue()).longValue()+"||");
									}
								}else{
										mapCell.put(Integer.valueOf(cIndex),"||"+cell+"||");
								}
							}
						}else{
							if(cellFlag){
								if(!dateFlag){
									
									try{
										mapCell.put(Integer.valueOf(cIndex),cell.getStringCellValue());
									}catch(Exception e){
										mapCell.put(Integer.valueOf(cIndex),new Double(cell.getNumericCellValue()).longValue()+"");
									}
								}else{

										mapCell.put(Integer.valueOf(cIndex),cell+"");
								}
							}
						
					}
			        	cellFlag=false;
			        	dateFlag=false;
				}
				vectorCellEachRowData=cellValuePopulateForExcel(mapCell);
				//System.out.println("vectorCellEachRowData   "+vectorCellEachRowData);
				vectorData.addElement(vectorCellEachRowData);
				lineNumberHdr++;
		  }
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("vectorData   "+vectorData);
		return vectorData;
	}
	
	
	private static HSSFCell setCellValue(Integer changeValCell) {
		// TODO Auto-generated method stub
		return null;
	}

	public static Vector cellValuePopulateForExcel(Map<Integer,String> mapCell){
		Vector vectorCellEachRowData = new Vector();
		Map<Integer,String> mapCellTemp = new TreeMap<Integer, String>();
		boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false,flag9=false,flag10=false,flag11=false,flag12=false,flag13=false,flag14=false,flag15=false,flag16=false,flag17=false,flag18=false,flag19=false,flag20=false,flag21=false,flag22=false,flag23=false,flag24=false,flag25=false,flag26=false,flag27=false,flag28=false,flag29=false,flag30=false,flag31=false,flag32=false,flag33=false,flag34=false,flag35=false,flag36=false,flag37=false,flag38=false,flag39=false,flag40=false,flag41=false,flag42=false,flag43=false,flag44=false,flag45=false,flag46=false,flag47=false,flag48=false,flag49=false,flag50=false,flag51=false,flag52=false,flag53=false,flag54=false,flag55=false,flag56=false,flag57=false,flag58=false,flag59=false,flag60=false,flag61=false;
		for(Map.Entry<Integer, String> entry : mapCell.entrySet()){
		 Integer key=entry.getKey();
		String cellValue=null;
		if(entry.getValue()!=null)
			cellValue=entry.getValue().trim();
		
		//System.out.println("cellValue   "+cellValue);
			if(key==0){
				mapCellTemp.put(key, cellValue);
				flag0=true;
			}
			if(key==1){
			mapCellTemp.put(key, cellValue);
			flag1=true;
			}
			if(key==2){
			mapCellTemp.put(key, cellValue);
			flag2=true;
			}
			if(key==3){
			mapCellTemp.put(key, cellValue);
			flag3=true;
			}
			if(key==4){
			mapCellTemp.put(key, cellValue);
			flag4=true;
			}
			if(key==5){
			mapCellTemp.put(key, cellValue);
			flag5=true;
			}
			if(key==6){
			mapCellTemp.put(key, cellValue);
			flag6=true;
			}
			if(key==7){
			mapCellTemp.put(key, cellValue);
			flag7=true;
			}
			if(key==8){
			mapCellTemp.put(key, cellValue);
			flag8=true;
			}
			if(key==9){
			mapCellTemp.put(key, cellValue);
			flag9=true;
			}
			if(key==10){
			mapCellTemp.put(key, cellValue);
			flag10=true;
			}
			if(key==11){
			mapCellTemp.put(key, cellValue);
			flag11=true;
			}
			if(key==12){
			mapCellTemp.put(key, cellValue);
			flag12=true;
			}
			if(key==13){
			mapCellTemp.put(key, cellValue);
			flag13=true;
			}
			if(key==14){
			mapCellTemp.put(key, cellValue);
			flag14=true;
			}
			if(key==15){
			mapCellTemp.put(key, cellValue);
			flag15=true;
			}
			if(key==16){
			mapCellTemp.put(key, cellValue);
			flag16=true;
			}
			if(key==17){
			mapCellTemp.put(key, cellValue);
			flag17=true;
			}
			if(key==18){
			mapCellTemp.put(key, cellValue);
			flag18=true;
			}
			if(key==19){
			mapCellTemp.put(key, cellValue);
			flag19=true;
			}
			if(key==20){
			mapCellTemp.put(key, cellValue);
			flag20=true;
			}
			if(key==21){
			mapCellTemp.put(key, cellValue);
			flag21=true;
			}
			if(key==22){
			mapCellTemp.put(key, cellValue);
			flag22=true;
			}
			if(key==23){
			mapCellTemp.put(key, cellValue);
			flag23=true;
			}
			if(key==24){
			mapCellTemp.put(key, cellValue);
			flag24=true;
			}
			if(key==25){
			mapCellTemp.put(key, cellValue);
			flag25=true;
			}
			if(key==26){
			mapCellTemp.put(key, cellValue);
			flag26=true;
			}
			if(key==27){
			mapCellTemp.put(key, cellValue);
			flag27=true;
			}
			if(key==28){
			mapCellTemp.put(key, cellValue);
			flag28=true;
			}
			if(key==29){
			mapCellTemp.put(key, cellValue);
			flag29=true;
			}
			if(key==30){
			mapCellTemp.put(key, cellValue);
			flag30=true;
			}
			if(key==31){
			mapCellTemp.put(key, cellValue);
			flag31=true;
			}
			if(key==32){
			mapCellTemp.put(key, cellValue);
			flag32=true;
			}
			if(key==33){
			mapCellTemp.put(key, cellValue);
			flag33=true;
			}
			if(key==34){
			mapCellTemp.put(key, cellValue);
			flag34=true;
			}
			if(key==35){
			mapCellTemp.put(key, cellValue);
			flag35=true;
			}
			if(key==36){
			mapCellTemp.put(key, cellValue);
			flag36=true;
			}
			if(key==37){
			mapCellTemp.put(key, cellValue);
			flag37=true;
			}
			if(key==38){
			mapCellTemp.put(key, cellValue);
			flag38=true;
			}
			if(key==39){
			mapCellTemp.put(key, cellValue);
			flag39=true;
			}
			if(key==40){
			mapCellTemp.put(key, cellValue);
			flag40=true;
			}
			if(key==41){
			mapCellTemp.put(key, cellValue);
			flag41=true;
			}
			if(key==42){
			mapCellTemp.put(key, cellValue);
			flag42=true;
			}
			if(key==43){
			mapCellTemp.put(key, cellValue);
			flag43=true;
			}
			if(key==44){
			mapCellTemp.put(key, cellValue);
			flag44=true;
			}
			if(key==45){
			mapCellTemp.put(key, cellValue);
			flag45=true;
			}
			if(key==46){
			mapCellTemp.put(key, cellValue);
			flag46=true;
			}
			if(key==47){
			mapCellTemp.put(key, cellValue);
			flag47=true;
			}
			if(key==48){
			mapCellTemp.put(key, cellValue);
			flag48=true;
			}
			if(key==49){
			mapCellTemp.put(key, cellValue);
			flag49=true;
			}
			if(key==50){
			mapCellTemp.put(key, cellValue);
			flag50=true;
			}
			if(key==51){
				mapCellTemp.put(key, cellValue);
				flag51=true;
			}
			if(key==52){
				mapCellTemp.put(key, cellValue);
				flag52=true;
			}
			if(key==53){
				mapCellTemp.put(key, cellValue);
				flag53=true;
			}
			if(key==54){
				mapCellTemp.put(key, cellValue);
				flag54=true;
			}
			if(key==55){
				mapCellTemp.put(key, cellValue);
				flag55=true;
			}
			if(key==56){
				mapCellTemp.put(key, cellValue);
				flag56=true;
			}
			if(key==57){
				mapCellTemp.put(key, cellValue);
				flag57=true;
			}
			if(key==58){
				mapCellTemp.put(key, cellValue);
				flag58=true;
			}
			if(key==59){
				mapCellTemp.put(key, cellValue);
				flag59=true;
			}
			if(key==60){
				mapCellTemp.put(key, cellValue);
				flag60=true;
			}
			if(key==61){
				mapCellTemp.put(key, cellValue);
				flag61=true;
			}
		}
		
		if(flag0==false){
			mapCellTemp.put(0, "");
			}
			if(flag1==false){
			mapCellTemp.put(1, "");
			}
			if(flag2==false){
			mapCellTemp.put(2, "");
			}
			if(flag3==false){
			mapCellTemp.put(3, "");
			}
			if(flag4==false){
			mapCellTemp.put(4, "");
			}
			if(flag5==false){
			mapCellTemp.put(5, "");
			}
			if(flag6==false){
			mapCellTemp.put(6, "");
			}
			if(flag7==false){
			mapCellTemp.put(7, "");
			}
			if(flag8==false){
			mapCellTemp.put(8, "");
			}
			if(flag9==false){
			mapCellTemp.put(9, "");
			}
			if(flag10==false){
			mapCellTemp.put(10, "");
			}
			if(flag11==false){
			mapCellTemp.put(11, "");
			}
			if(flag12==false){
			mapCellTemp.put(12, "");
			}
			if(flag13==false){
			mapCellTemp.put(13, "");
			}
			if(flag14==false){
			mapCellTemp.put(14, "");
			}
			if(flag15==false){
			mapCellTemp.put(15, "");
			}
			if(flag16==false){
			mapCellTemp.put(16, "");
			}
			if(flag17==false){
			mapCellTemp.put(17, "");
			}
			if(flag18==false){
			mapCellTemp.put(18, "");
			}
			if(flag19==false){
			mapCellTemp.put(19, "");
			}
			if(flag20==false){
			mapCellTemp.put(20, "");
			}
			if(flag21==false){
			mapCellTemp.put(21, "");
			}
			if(flag22==false){
			mapCellTemp.put(22, "");
			}
			if(flag23==false){
			mapCellTemp.put(23, "");
			}
			if(flag24==false){
			mapCellTemp.put(24, "");
			}
			if(flag25==false){
			mapCellTemp.put(25, "");
			}
			if(flag26==false){
			mapCellTemp.put(26, "");
			}
			if(flag27==false){
			mapCellTemp.put(27, "");
			}
			if(flag28==false){
			mapCellTemp.put(28, "");
			}
			if(flag29==false){
			mapCellTemp.put(29, "");
			}
			if(flag30==false){
			mapCellTemp.put(30, "");
			}
			if(flag31==false){
			mapCellTemp.put(31, "");
			}
			if(flag32==false){
			mapCellTemp.put(32, "");
			}
			if(flag33==false){
			mapCellTemp.put(33, "");
			}
			if(flag34==false){
			mapCellTemp.put(34, "");
			}
			if(flag35==false){
			mapCellTemp.put(35, "");
			}
			if(flag36==false){
			mapCellTemp.put(36, "");
			}
			if(flag37==false){
			mapCellTemp.put(37, "");
			}
			if(flag38==false){
			mapCellTemp.put(38, "");
			}
			if(flag39==false){
			mapCellTemp.put(39, "");
			}
			if(flag40==false){
			mapCellTemp.put(40, "");
			}
			if(flag41==false){
			mapCellTemp.put(41, "");
			}
			if(flag42==false){
			mapCellTemp.put(42, "");
			}
			if(flag43==false){
			mapCellTemp.put(43, "");
			}
			if(flag44==false){
			mapCellTemp.put(44, "");
			}
			if(flag45==false){
			mapCellTemp.put(45, "");
			}
			if(flag46==false){
			mapCellTemp.put(46, "");
			}
			if(flag47==false){
			mapCellTemp.put(47, "");
			}
			if(flag48==false){
			mapCellTemp.put(48, "");
			}
			if(flag49==false){
			mapCellTemp.put(49, "");
			}
			if(flag50==false){
			mapCellTemp.put(50, "");
			}
			if(flag51==false){
				mapCellTemp.put(51, "");
			}
			if(flag52==false){
				mapCellTemp.put(52, "");
			}
			if(flag53==false){
				mapCellTemp.put(53, "");
			}
			if(flag54==false){
				mapCellTemp.put(54, "");
			}
			if(flag55==false){
				mapCellTemp.put(55, "");
			}
			if(flag56==false){
				mapCellTemp.put(56, "");
			}
			if(flag57==false){
				mapCellTemp.put(57, "");
			}
			if(flag58==false){
				mapCellTemp.put(58, "");
			}
			if(flag59==false){
				mapCellTemp.put(59, "");
			}
			if(flag60==false){
				mapCellTemp.put(60, "");
			}
			if(flag61==false){
				mapCellTemp.put(61, "");
			}
		 			 
		 for(Map.Entry<Integer, String> entry : mapCellTemp.entrySet()){
			 vectorCellEachRowData.addElement(entry.getValue());
		 }
		 return vectorCellEachRowData;
	}
	
	public static Vector readDataExcelCSV(String fileName) throws FileNotFoundException{		
		Vector vectorData = new Vector();
		String filepathhdr=fileName;
		
		int rowshdr_count=0;
		int ZoneCode_count=1000;
		int SchoolCode_count=1000;
		int GradeLevel_count=1000;
		int HighNeedsStatus_count=1000;
		int DateAdded_count=1000;
		int Notes_count=1000;
		int JobCategory_count=1000;
		int Classification_count=1000;
		
		
		int code_0502_count=1000;
		int code_0807_count=1000;
		int code_0813_count=1000;
		int code_0815_count=1000;
		int code_0863_count=1000;
		int code_0885_count=1000;
		int code_1000_count=1000;
		int code_1052_count=1000;
		int code_1111_count=1000;
		int code_1405_count=1000;
		int code_1603_count=1000;
		int code_1668_count=1000;
		int code_1836_count=1000;
		int code_1837_count=1000;
		int code_2000_count=1000;
		int code_2825_count=1000;
		int code_3100_count=1000;
		int code_3101_count=1000;
		int code_3105_count=1000;
		int code_3108_count=1000;
		int code_3109_count=1000;
		int code_3230_count=1000;
		int code_4005_count=1000;
		int code_4030_count=1000;
		int code_4405_count=1000;
		int code_4410_count=1000;
		int code_4420_count=1000;
		int code_4430_count=1000;
		int code_4480_count=1000;
		int code_4490_count=1000;
		int code_4499_count=1000;
		int code_4805_count=1000;
		int code_6420_count=1000;
		int code_6800_count=1000;
		int code_7205_count=1000;
		int code_7207_count=1000;
		int code_7605_count=1000;
		int code_7606_count=1000;
		int code_7607_count=1000;
		int code_7610_count=1000;
		int code_7621_count=1000;
		int code_7650_count=1000;
		int code_8405_count=1000;
		int code_8420_count=1000;
		int code_8450_count=1000;
		int code_8470_count=1000;
		int code_8875_count=1000;
		int code_9205_count=1000;
		int code_9235_count=1000;
		int code_9270_count=1000;
		int code_9290_count=1000;
		
		BufferedReader brhdr=new BufferedReader(new FileReader(filepathhdr));
		
		String strLineHdr="";	        
		String hdrstr="";
		
		StringTokenizer sthdr=null;
		StringTokenizer stdtl=null;
		int lineNumberHdr=0;
		
		try{
			String indexCheck="";
				while((strLineHdr=brhdr.readLine())!=null){               
				Vector vectorCellEachRowData = new Vector();
				int cIndex=0;
				boolean cellFlag=false,dateFlag=false;
				Map<String,String> mapCell = new TreeMap<String, String>();
				int i=1;
				
				String[] parts = strLineHdr.split("\\|");
				
				for(int j=0; j<parts.length; j++) {
							hdrstr=parts[j];
							cIndex=j;
							
							
							if(hdrstr.toString().toLowerCase().trim().equalsIgnoreCase("Zone Code")){
								ZoneCode_count=cIndex;
							indexCheck+="||"+cIndex+"||";
							}
							
							if(ZoneCode_count==cIndex){
								cellFlag=true;
							}
							
							if(hdrstr.toString().trim().toLowerCase().equalsIgnoreCase("School Code")){
								SchoolCode_count=cIndex;
								indexCheck+="||"+cIndex+"||";
							}
							if(SchoolCode_count==cIndex){
								cellFlag=true;
							
							}	
							//GradeLevel_count
							if(hdrstr.toString().toLowerCase().trim().equalsIgnoreCase("gradelevel")){
								GradeLevel_count=cIndex;
								indexCheck+="||"+cIndex+"||";
							}
							if(GradeLevel_count==cIndex){
								cellFlag=true;
							
							}
							//HighNeedsStatus_count
							if(hdrstr.toString().toLowerCase().trim().equalsIgnoreCase("highneedsstatus")){
								HighNeedsStatus_count=cIndex;
								indexCheck+="||"+cIndex+"||";
							}
							if(HighNeedsStatus_count==cIndex){
								cellFlag=true;
							
							}
							if(hdrstr.toString().toLowerCase().trim().equalsIgnoreCase("date added")){
								DateAdded_count=cIndex;
								indexCheck+="||"+cIndex+"||";
							}
							if(DateAdded_count==cIndex){
								cellFlag=true;
							}
							//Notes_count
							if(hdrstr.toString().toLowerCase().trim().equalsIgnoreCase("notes")){
								Notes_count=cIndex;
								indexCheck+="||"+cIndex+"||";
							}
							if(Notes_count==cIndex){
								cellFlag=true;
							}
							if(hdrstr.toString().toLowerCase().trim().equalsIgnoreCase("job category")){
								JobCategory_count=cIndex;
								indexCheck+="||"+cIndex+"||";
							}
							if(JobCategory_count==cIndex){
								cellFlag=true;		
							}
							if(hdrstr.toString().toLowerCase().trim().equalsIgnoreCase("classification")){
								Classification_count=cIndex;
								indexCheck+="||"+cIndex+"||";
							}
							if(Classification_count==cIndex){
								cellFlag=true;
							}
							
							if(hdrstr.toString().trim().equalsIgnoreCase("0502")){
								code_0502_count=cIndex;
								indexCheck+="||"+cIndex+"||";
							}
							if(code_0502_count==cIndex){
								cellFlag=true;
							}
								if(hdrstr.toString().trim().equalsIgnoreCase("0807")){
								code_0807_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_0807_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("0813")){
								code_0813_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_0813_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("0815")){
								code_0815_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_0815_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("0863")){
								code_0863_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_0863_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("0885")){
								code_0885_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_0885_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("1000")){
								code_1000_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_1000_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("1052")){
								code_1052_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_1052_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("1111")){
								code_1111_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_1111_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("1405")){
								code_1405_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_1405_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("1603")){
								code_1603_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_1603_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("1668")){
								code_1668_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_1668_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("1836")){
								code_1836_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_1836_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("1837")){
								code_1837_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_1837_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("2000")){
								code_2000_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_2000_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("2825")){
								code_2825_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_2825_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("3100")){
								code_3100_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_3100_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("3101")){
								code_3101_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_3101_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("3105")){
								code_3105_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_3105_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("3108")){
								code_3108_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_3108_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("3109")){
								code_3109_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_3109_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("3230")){
								code_3230_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_3230_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("4005")){
								code_4005_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_4005_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("4030")){
								code_4030_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_4030_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("4405")){
								code_4405_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_4405_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("4410")){
								code_4410_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_4410_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("4420")){
								code_4420_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_4420_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("4430")){
								code_4430_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_4430_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("4480")){
								code_4480_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_4480_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("4490")){
								code_4490_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_4490_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("4499")){
								code_4499_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_4499_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("4805")){
								code_4805_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_4805_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("6420")){
								code_6420_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_6420_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("6800")){
								code_6800_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_6800_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("7205")){
								code_7205_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_7205_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("7207")){
								code_7207_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_7207_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("7605")){
								code_7605_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_7605_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("7606")){
								code_7606_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_7606_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("7607")){
								code_7607_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_7607_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("7610")){
								code_7610_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_7610_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("7621")){
								code_7621_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_7621_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("7650")){
								code_7650_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_7650_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("8405")){
								code_8405_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_8405_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("8420")){
								code_8420_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_8420_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("8450")){
								code_8450_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_8450_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("8470")){
								code_8470_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_8470_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("8875")){
								code_8875_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_8875_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("9205")){
								code_9205_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_9205_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("9235")){
								code_9235_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_9235_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("9270")){
								code_9270_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_9270_count==cIndex){
								cellFlag=true;
								}
								if(hdrstr.toString().trim().equalsIgnoreCase("9290")){
								code_9290_count=cIndex;
								indexCheck+="||"+cIndex+"||";
								}
								if(code_9290_count==cIndex){
								cellFlag=true;
								}
								
							if(cellFlag){
								if(lineNumberHdr==0){
									if(!dateFlag){
										try{
											mapCell.put(cIndex+"","||"+hdrstr+"||");
										}catch(Exception e){
											mapCell.put(cIndex+"","||"+hdrstr+"||");
										}
									}else{
											mapCell.put(cIndex+"","||"+hdrstr+"||");
									}
								}else{
									if(!dateFlag){
										try{
											mapCell.put(cIndex+"",hdrstr);
										}catch(Exception e){
											mapCell.put(cIndex+"",hdrstr);
										}
									}else{
											mapCell.put(cIndex+"",hdrstr);
									}
								}
							}		            
					}
				
			        //if(mapCell.size()==10){
			            vectorCellEachRowData=cellValuePopulate(mapCell);
			            vectorData.addElement(vectorCellEachRowData);
			        //}
				    lineNumberHdr++;
				}
			brhdr.close();
		}		
		catch(Exception e){
		}
		System.out.println("read data csv complete");
		System.out.println(vectorData);
		System.out.println("End read");
		return vectorData;
	}

	public static Vector cellValuePopulate(Map<String,String> mapCell){
		Vector vectorCellEachRowData = new Vector();
		Map<String,String> mapCellTemp = new TreeMap<String, String>();
		boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false,flag9=false,flag10=false,flag11=false,flag12=false,flag13=false,flag14=false,flag15=false,flag16=false,flag17=false,flag18=false,flag19=false,flag20=false,flag21=false,flag22=false,flag23=false,flag24=false,flag25=false,flag26=false,flag27=false,flag28=false,flag29=false,flag30=false,flag31=false,flag32=false,flag33=false,flag34=false,flag35=false,flag36=false,flag37=false,flag38=false,flag39=false,flag40=false,flag41=false,flag42=false,flag43=false,flag44=false,flag45=false,flag46=false,flag47=false,flag48=false,flag49=false,flag50=false;
		for(Map.Entry<String, String> entry : mapCell.entrySet()){
		 String key=entry.getKey();
		String cellValue=null;
		if(entry.getValue()!=null)
			cellValue=entry.getValue().trim();
		
		if(key.equals("0")){
			mapCellTemp.put(key, cellValue);
			flag0=true;
			}
			if(key.equals("1")){
			mapCellTemp.put(key, cellValue);
			flag1=true;
			}
			if(key.equals("2")){
			mapCellTemp.put(key, cellValue);
			flag2=true;
			}
			if(key.equals("3")){
			mapCellTemp.put(key, cellValue);
			flag3=true;
			}
			if(key.equals("4")){
			mapCellTemp.put(key, cellValue);
			flag4=true;
			}
			if(key.equals("5")){
			mapCellTemp.put(key, cellValue);
			flag5=true;
			}
			if(key.equals("6")){
			mapCellTemp.put(key, cellValue);
			flag6=true;
			}
			if(key.equals("7")){
			mapCellTemp.put(key, cellValue);
			flag7=true;
			}
			if(key.equals("8")){
			mapCellTemp.put(key, cellValue);
			flag8=true;
			}
			if(key.equals("9")){
			mapCellTemp.put(key, cellValue);
			flag9=true;
			}
			if(key.equals("10")){
			mapCellTemp.put(key, cellValue);
			flag10=true;
			}
			if(key.equals("11")){
			mapCellTemp.put(key, cellValue);
			flag11=true;
			}
			if(key.equals("12")){
			mapCellTemp.put(key, cellValue);
			flag12=true;
			}
			if(key.equals("13")){
			mapCellTemp.put(key, cellValue);
			flag13=true;
			}
			if(key.equals("14")){
			mapCellTemp.put(key, cellValue);
			flag14=true;
			}
			if(key.equals("15")){
			mapCellTemp.put(key, cellValue);
			flag15=true;
			}
			if(key.equals("16")){
			mapCellTemp.put(key, cellValue);
			flag16=true;
			}
			if(key.equals("17")){
			mapCellTemp.put(key, cellValue);
			flag17=true;
			}
			if(key.equals("18")){
			mapCellTemp.put(key, cellValue);
			flag18=true;
			}
			if(key.equals("19")){
			mapCellTemp.put(key, cellValue);
			flag19=true;
			}
			if(key.equals("20")){
			mapCellTemp.put(key, cellValue);
			flag20=true;
			}
			if(key.equals("21")){
			mapCellTemp.put(key, cellValue);
			flag21=true;
			}
			if(key.equals("22")){
			mapCellTemp.put(key, cellValue);
			flag22=true;
			}
			if(key.equals("23")){
			mapCellTemp.put(key, cellValue);
			flag23=true;
			}
			if(key.equals("24")){
			mapCellTemp.put(key, cellValue);
			flag24=true;
			}
			if(key.equals("25")){
			mapCellTemp.put(key, cellValue);
			flag25=true;
			}
			if(key.equals("26")){
			mapCellTemp.put(key, cellValue);
			flag26=true;
			}
			if(key.equals("27")){
			mapCellTemp.put(key, cellValue);
			flag27=true;
			}
			if(key.equals("28")){
			mapCellTemp.put(key, cellValue);
			flag28=true;
			}
			if(key.equals("29")){
			mapCellTemp.put(key, cellValue);
			flag29=true;
			}
			if(key.equals("30")){
			mapCellTemp.put(key, cellValue);
			flag30=true;
			}
			if(key.equals("31")){
			mapCellTemp.put(key, cellValue);
			flag31=true;
			}
			if(key.equals("32")){
			mapCellTemp.put(key, cellValue);
			flag32=true;
			}
			if(key.equals("33")){
			mapCellTemp.put(key, cellValue);
			flag33=true;
			}
			if(key.equals("34")){
			mapCellTemp.put(key, cellValue);
			flag34=true;
			}
			if(key.equals("35")){
			mapCellTemp.put(key, cellValue);
			flag35=true;
			}
			if(key.equals("36")){
			mapCellTemp.put(key, cellValue);
			flag36=true;
			}
			if(key.equals("37")){
			mapCellTemp.put(key, cellValue);
			flag37=true;
			}
			if(key.equals("38")){
			mapCellTemp.put(key, cellValue);
			flag38=true;
			}
			if(key.equals("39")){
			mapCellTemp.put(key, cellValue);
			flag39=true;
			}
			if(key.equals("40")){
			mapCellTemp.put(key, cellValue);
			flag40=true;
			}
			if(key.equals("41")){
			mapCellTemp.put(key, cellValue);
			flag41=true;
			}
			if(key.equals("42")){
			mapCellTemp.put(key, cellValue);
			flag42=true;
			}
			if(key.equals("43")){
			mapCellTemp.put(key, cellValue);
			flag43=true;
			}
			if(key.equals("44")){
			mapCellTemp.put(key, cellValue);
			flag44=true;
			}
			if(key.equals("45")){
			mapCellTemp.put(key, cellValue);
			flag45=true;
			}
			if(key.equals("46")){
			mapCellTemp.put(key, cellValue);
			flag46=true;
			}
			if(key.equals("47")){
			mapCellTemp.put(key, cellValue);
			flag47=true;
			}
			if(key.equals("48")){
			mapCellTemp.put(key, cellValue);
			flag48=true;
			}
			if(key.equals("49")){
			mapCellTemp.put(key, cellValue);
			flag49=true;
			}
			if(key.equals("50")){
			mapCellTemp.put(key, cellValue);
			flag50=true;
			}
		}
		
		if(flag0==false){
			mapCellTemp.put(0+"", "");
			}
			if(flag1==false){
			mapCellTemp.put(1+"", "");
			}
			if(flag2==false){
			mapCellTemp.put(2+"", "");
			}
			if(flag3==false){
			mapCellTemp.put(3+"", "");
			}
			if(flag4==false){
			mapCellTemp.put(4+"", "");
			}
			if(flag5==false){
			mapCellTemp.put(5+"", "");
			}
			if(flag6==false){
			mapCellTemp.put(6+"", "");
			}
			if(flag7==false){
			mapCellTemp.put(7+"", "");
			}
			if(flag8==false){
			mapCellTemp.put(8+"", "");
			}
			if(flag9==false){
			mapCellTemp.put(9+"", "");
			}
			if(flag10==false){
			mapCellTemp.put(10+"", "");
			}
			if(flag11==false){
			mapCellTemp.put(11+"", "");
			}
			if(flag12==false){
			mapCellTemp.put(12+"", "");
			}
			if(flag13==false){
			mapCellTemp.put(13+"", "");
			}
			if(flag14==false){
			mapCellTemp.put(14+"", "");
			}
			if(flag15==false){
			mapCellTemp.put(15+"", "");
			}
			if(flag16==false){
			mapCellTemp.put(16+"", "");
			}
			if(flag17==false){
			mapCellTemp.put(17+"", "");
			}
			if(flag18==false){
			mapCellTemp.put(18+"", "");
			}
			if(flag19==false){
			mapCellTemp.put(19+"", "");
			}
			if(flag20==false){
			mapCellTemp.put(20+"", "");
			}
			if(flag21==false){
			mapCellTemp.put(21+"", "");
			}
			if(flag22==false){
			mapCellTemp.put(22+"", "");
			}
			if(flag23==false){
			mapCellTemp.put(23+"", "");
			}
			if(flag24==false){
			mapCellTemp.put(24+"", "");
			}
			if(flag25==false){
			mapCellTemp.put(25+"", "");
			}
			if(flag26==false){
			mapCellTemp.put(26+"", "");
			}
			if(flag27==false){
			mapCellTemp.put(27+"", "");
			}
			if(flag28==false){
			mapCellTemp.put(28+"", "");
			}
			if(flag29==false){
			mapCellTemp.put(29+"", "");
			}
			if(flag30==false){
			mapCellTemp.put(30+"", "");
			}
			if(flag31==false){
			mapCellTemp.put(31+"", "");
			}
			if(flag32==false){
			mapCellTemp.put(32+"", "");
			}
			if(flag33==false){
			mapCellTemp.put(33+"", "");
			}
			if(flag34==false){
			mapCellTemp.put(34+"", "");
			}
			if(flag35==false){
			mapCellTemp.put(35+"", "");
			}
			if(flag36==false){
			mapCellTemp.put(36+"", "");
			}
			if(flag37==false){
			mapCellTemp.put(37+"", "");
			}
			if(flag38==false){
			mapCellTemp.put(38+"", "");
			}
			if(flag39==false){
			mapCellTemp.put(39+"", "");
			}
			if(flag40==false){
			mapCellTemp.put(40+"", "");
			}
			if(flag41==false){
			mapCellTemp.put(41+"", "");
			}
			if(flag42==false){
			mapCellTemp.put(42+"", "");
			}
			if(flag43==false){
			mapCellTemp.put(43+"", "");
			}
			if(flag44==false){
			mapCellTemp.put(44+"", "");
			}
			if(flag45==false){
			mapCellTemp.put(45+"", "");
			}
			if(flag46==false){
			mapCellTemp.put(46+"", "");
			}
			if(flag47==false){
			mapCellTemp.put(47+"", "");
			}
			if(flag48==false){
			mapCellTemp.put(48+"", "");
			}
			if(flag49==false){
			mapCellTemp.put(49+"", "");
			}
			if(flag50==false){
			mapCellTemp.put(50+"", "");
			}
		 			 
		 for(Map.Entry<String, String> entry : mapCellTemp.entrySet()){
			 vectorCellEachRowData.addElement(entry.getValue());
		 }
		 return vectorCellEachRowData;
	}

	public String deleteXlsAndXlsxFile(String fileName){	
		String filePath="";
		File file=null;
		String returnDel="";
		try{			
		filePath=fileName;
		file = new File(filePath);
		file.delete();
		returnDel="deleted";
		}catch(Exception e){}
		return returnDel;
	}
	
	 public boolean isValidDate(String inDate) {

         if (inDate == null)
             return false;

         // set the format to use as a constructor argument
         SimpleDateFormat dateFormat = new SimpleDateFormat("mm-dd-yyyy");

         if (inDate.trim().length() != dateFormat.toPattern().length())
             return false;
         dateFormat.setLenient(false);
         try {
             dateFormat.parse(inDate.trim());
         } catch (Exception pe) {
             return false;
         }
         return true;
     }
	 
	 @Transactional(readOnly=false)
	 public List<JobOrder> getListByCateApiJobCatDist(List<String> apiJobCodes,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster)
	 {
	 	List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
	 	
	 	if(apiJobCodes.size()>0)
	      {
	 		try 
	 		{
	 			Criterion criterion1 =	Restrictions.in("apiJobId",apiJobCodes);
	 			Criterion criterion2 =  Restrictions.eq("districtMaster", districtMaster); 
	 			Criterion criterion3 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
	 			listjobOrder = jobOrderDAO.findByCriteria(criterion1,criterion2,criterion3);	
	 		} 
	 		catch (Exception e) 
	 		{
	 			e.printStackTrace();
	 		}
	 }
	 	return listjobOrder;
	 }
	 
	 public static boolean isNumeric(String str)
	 {
	     return str.matches("-?\\d+(.\\d+)?");
	 }
	 
	 public static Integer changeValCell( String input )
	 {
		 int finalResult=0;
		 
		if(input.contains(".")){
			String[] breakString = input.split("\\.");
			finalResult=Integer.parseInt(breakString[0]);
			if(!breakString[1].toString().equalsIgnoreCase("0")){
				finalResult = finalResult+1;
			}
		}else{
			finalResult = Integer.parseInt(input);
		} 

	    return finalResult;
	 }
	 
	 public static Integer changeVal( String input )
	 {
		 Integer finalResult=0;
		 
		if(input.contains(".")){
			String[] breakString = input.split("\\.");
			finalResult=Integer.parseInt(breakString[0]);
			if(!breakString[1].toString().equalsIgnoreCase("0")){
				finalResult = finalResult+1;
			}
		}else{
			finalResult = Integer.parseInt(input);
		} 

	    return finalResult;
	 }
	 
	 public static String monthReplace(String date){
			String currentDate="";
			try{
				
			   if(date.contains("Jan")){
				   currentDate=date.replaceAll("Jan","01");
			   }
			   if(date.contains("Feb")){
				   currentDate=date.replaceAll("Feb","02");
			   }
			   if(date.contains("Mar")){
				   currentDate=date.replaceAll("Mar","03");
			   }
			   if(date.contains("Apr")){
				   currentDate=date.replaceAll("Apr","04");
			   }
			   if(date.contains("May")){
				   currentDate=date.replaceAll("May","05");
			   }
			   if(date.contains("Jun")){
				   currentDate=date.replaceAll("Jun","06");
			   }
			   if(date.contains("Jul")){
				   currentDate=date.replaceAll("Jul","07");
			   }
			   if(date.contains("Aug")){
				   currentDate=date.replaceAll("Aug","08");
			   }
			   if(date.contains("Sep")){
				   currentDate=date.replaceAll("Sep","09");
			   }
			   if(date.contains("Oct")){
				   currentDate=date.replaceAll("Oct","10");
			   }
			   if(date.contains("Nov")){
				   currentDate=date.replaceAll("Nov","11");
			   }
			   if(date.contains("Dec")){
				   currentDate=date.replaceAll("Dec","12");
			   }
			  /* String uDate=currentDate.replaceAll("-","/");
			   if(Utility.isDateValid(uDate,0)==2){
				   currentDate=date; 
			   }else{
				   try{
					   String dateSplit[]=uDate.split("/");
					   currentDate=dateSplit[1]+"/"+dateSplit[0]+"/"+dateSplit[2];
				   }catch(Exception e){
					   currentDate=date; 
				   }
			   }*/
			}catch(Exception e){
				currentDate=date;
			}
			   return currentDate;
		 }
}
