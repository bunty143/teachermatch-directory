package tm.controller.master;
/* @Author: Gagan  
 * @Discription: view of Admin dashboard Controller.
 */
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.EmployeeMaster;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.SchoolWiseCandidateStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.hqbranchesmaster.WorkFlowStatus;
import tm.bean.master.DistrictMaster;
import tm.bean.master.PanelAttendees;
import tm.bean.master.PanelSchedule;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.mq.MQEvent;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SchoolWiseCandidateStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.cgreport.TeacherStatusScoresDAO;
import tm.dao.hqbranchesmaster.WorkFlowStatusDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.PanelAttendeesDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.StatusSpecificScoreDAO;
import tm.dao.mq.MQEventDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.CommonService;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;


@Controller
public class ImpController 
{
	@Autowired
	private WorkFlowStatusDAO workFlowStatusDAO;
	@Autowired
	private MQEventDAO mqEventDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired 
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	private TeacherStatusNotesDAO teacherStatusNotesDAO;
	
	@Autowired
	private SchoolWiseCandidateStatusDAO schoolWiseCandidateStatusDAO;
	
	@Autowired
	private PanelAttendeesDAO panelAttendeesDAO;
	
	@Autowired
	private PanelScheduleDAO panelScheduleDAO;
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO; 
	
	@Autowired
	private StatusSpecificScoreDAO statusSpecificScoreDAO;
	
	@Autowired
	JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;
	
	@Autowired
	private TeacherStatusScoresDAO teacherStatusScoresDAO;
	
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO)
	{
		this.cityMasterDAO = cityMasterDAO;
	}
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	
	@Autowired
	private CommonService commonService;
	
	@RequestMapping(value="/imp.do", method=RequestMethod.GET)
	public String impGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println("\n =========== Imp Controller ===============");
		try 
		{
			String requisitionNumber=request.getParameter("reqNo");
			String teacherId=request.getParameter("teacherId");
			String jobId=request.getParameter("jobId");
			String districtId=request.getParameter("districtId");
			String password=request.getParameter("password");
			
			
			String ufname=request.getParameter("ufname");
			String ulname=request.getParameter("ulname");
			String uemail=request.getParameter("uemail");
			String uid=request.getParameter("uid");
			
			String tfname=request.getParameter("tfname");
			String tlname=request.getParameter("tlname");
			String temail=request.getParameter("temail");
			String tid=request.getParameter("tid");
			String schoolId=request.getParameter("schoolId");
			String sId=request.getParameter("sId");
			String sFlag=request.getParameter("sFlag");
			String empNum=request.getParameter("empnum");
			String locCode=request.getParameter("locCode");
			String jobTitle=request.getParameter("jobTitle");
			
			System.out.println(" Teacher Details:"+ tfname+" | "+tlname+" | "+temail+" | "+tid);
			System.out.println(" User Details:"+ ufname+" | "+ulname+" | "+uemail+" | "+uid);
			System.out.println("sId:::"+sId +" sFlag: "+sFlag);
			
			String dFlag=request.getParameter("dFlag");
			try{
				if(dFlag!=null && dFlag.equals("e")){
					districtId=Utility.decryptNo(Integer.parseInt(districtId))+"";
					System.out.println("districtId:::::::::::::::"+districtId);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			StringBuffer msgAll=new StringBuffer();
			System.out.println("schoolId -: "+schoolId+"districtId: "+districtId+" requisitionNumber: "+requisitionNumber+" teacherId: "+teacherId+" jobId"+jobId);
			DistrictMaster districtMaster =null;
			if(districtId!=null){
				districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId),false,false);
			}
			SchoolMaster schoolMaster =null;
			if(schoolId!=null){
				schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId),false,false);
			}
			
			boolean mainFlag=true;
			
			List<SchoolMaster> locWiseSchool = new ArrayList<SchoolMaster>();
			if(password!=null && password.equals("netSutra") && locCode!=null && !locCode.equals("")){
				mainFlag=false;
				Criterion locCri = Restrictions.like("locationCode", locCode);
				locWiseSchool=schoolMasterDAO.findWithLimit(Order.asc("locationCode"),0,50,locCri);
				
				msgAll.append("<table border=1 cellpadding=5 cellspacing=5>");
				msgAll.append("<tr bgcolor=yellow><td  colspan=14 align=left><font size=2 color=green>School Master</font></td></tr>");
				msgAll.append("<tr><td>School Id</td><td>District Name</td><td>School Name</td><td>#_of_teachers</td><td>#_of_students</td><td>Address</td><td>City</td><td>Zip</td><td>State</td><td>Phone</td><td>Phone_2</td><td>Fax</td><td>URL</td><td>GeoZone</td></tr>");
				if(locWiseSchool.size()>0)
				for (SchoolMaster schoolMaster2 : locWiseSchool) {
					msgAll.append("<tr><td>"+schoolMaster2.getSchoolId()+"</td><td>"+schoolMaster2.getDistrictName()+"</td><td>"+schoolMaster2.getSchoolName()+"</td><td>"+schoolMaster2.getNoOfTeachers()+"</td><td>"+schoolMaster2.getNoOfStudents()+"</td><td>"+schoolMaster2.getAddress()+"</td><td>"+schoolMaster2.getCityName()+"</td><td>"+schoolMaster2.getZip()+"</td><td>"+schoolMaster2.getStateMaster().getStateName()+"</td><td>"+schoolMaster2.getPhoneNumber()+"</td><td>"+schoolMaster2.getAlternatePhone()+"</td><td>"+schoolMaster2.getFaxNumber()+"</td><td>"+schoolMaster2.getWebsite()+"</td><td>"+schoolMaster2.getGeoZoneMaster().getGeoZoneCode()+"</td></tr>");
				}
				else
				msgAll.append("<tr bgcolor=red><td  colspan=8 align=left><font size=2 color=green>No Record Found</font></td></tr>");
				msgAll.append("</table>");
			}
			List<JobOrder> listjobnTitle= new ArrayList<JobOrder>();
			if(password!=null && password.equals("netSutra") && jobTitle!=null && !jobTitle.equals("")){
				mainFlag=false;				
				Criterion jobnTitleCri = Restrictions.like("jobTitle", jobTitle, MatchMode.ANYWHERE);
				Criterion jobStatus = Restrictions.like("status", "A");
				if(districtMaster!=null){
					Criterion criterionDis = Restrictions.eq("districtMaster", districtMaster);
					listjobnTitle=jobOrderDAO.findWithLimit(Order.asc("jobId"),0,50,jobnTitleCri,criterionDis,jobStatus);
				}else
					listjobnTitle=jobOrderDAO.findWithLimit(Order.asc("jobId"),0,50,jobnTitleCri,jobStatus);
				
				msgAll.append("<table border=1 cellpadding=5 cellspacing=5>");
				msgAll.append("<tr bgcolor=yellow><td  colspan=8 align=left><font size=2 color=green>Job Order</font></td></tr>");
				msgAll.append("<tr><td>Job Id</td><td>District Name</td><td colspan=4>Job Title</td><td>Status</td></tr>");
				if(listjobnTitle.size()>0)
				for (JobOrder jobOrder2 : listjobnTitle) {
					msgAll.append("<tr><td>"+jobOrder2.getJobId()+"</td><td>"+jobOrder2.getDistrictMaster().getDistrictName()+"</td><td colspan=4>"+jobOrder2.getJobTitle()+"</td><td>"+jobOrder2.getStatus()+"</td></tr>");
					//msgAll.append("<tr><td>"+schoolMaster2.getSchoolId()+"</td><td>"+schoolMaster2.getDistrictName()+"</td><td>"+schoolMaster2.getSchoolName()+"</td><td>"+schoolMaster2.getNoOfTeachers()+"</td><td>"+schoolMaster2.getNoOfStudents()+"</td><td>"+schoolMaster2.getAddress()+"</td><td>"+schoolMaster2.getCityName()+"</td><td>"+schoolMaster2.getZip()+"</td><td>"+schoolMaster2.getStateMaster().getStateName()+"</td><td>"+schoolMaster2.getPhoneNumber()+"</td><td>"+schoolMaster2.getAlternatePhone()+"</td><td>"+schoolMaster2.getFaxNumber()+"</td><td>"+schoolMaster2.getWebsite()+"</td><td>"+schoolMaster2.getGeoZoneMaster().getGeoZoneCode()+"</td></tr>");
				}
				else
				msgAll.append("<tr bgcolor=red><td  colspan=8 align=left><font size=2 color=green>No Record Found</font></td></tr>");
				msgAll.append("</table>");
			}
			
			
			if(password!=null && password.equals("netSutra") && ((requisitionNumber!=null && !requisitionNumber.equals(""))|| (jobId!=null && !jobId.equals("")) ||  (teacherId!=null && !teacherId.equals("")))){
				mainFlag=false;
				JobOrder jobOrder=null;
				if(jobId!=null){
					jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId),false,false);
				}
				TeacherDetail teacherDetail=null;
				if(teacherId!=null){
					teacherDetail=teacherDetailDAO.findById(Integer.parseInt(teacherId),false,false);
				}
				msgAll.append("<table border=1 cellpadding=5 cellspacing=5>");
				
				if(requisitionNumber!=null){
					List<DistrictRequisitionNumbers> drnList=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(districtMaster, requisitionNumber);
					msgAll.append("<tr bgcolor=yellow><td  colspan=3 align=left><font size=2 color=green>DistrictRequisitionNumbers</font></td><td  colspan=7 align=center><font size=2 color=green>DistrictRequisitionNumbers</font></td><td  colspan=3 align=right><font size=2 color=green>DistrictRequisitionNumbers</font></td></tr>");
					msgAll.append("<tr><td>District Requisition Id</td><td>Requisition Number</td><td>JobCode</td><td>JobTitle</td><td>PosType</td><td>IsUsed</td><td>DeleteFlag</td><td>UploadFrom()</td><td>UserId</td><td  colspan=4>CreatedDateTime</td></tr>");
					
					if(drnList.size()>0)
					for (DistrictRequisitionNumbers districtRequisitionNumbers : drnList) {
						try{
							msgAll.append("<tr><td>"+districtRequisitionNumbers.getDistrictRequisitionId()+"</td><td>"+districtRequisitionNumbers.getRequisitionNumber()+"</td><td>"+districtRequisitionNumbers.getJobCode()+"</td><td>"+districtRequisitionNumbers.getJobTitle()+"</td><td>"+districtRequisitionNumbers.getPosType()+"</td><td>"+districtRequisitionNumbers.getIsUsed()+"</td><td>"+districtRequisitionNumbers.getDeleteFlag()+"</td><td>"+districtRequisitionNumbers.getUploadFrom()+"</td><td>"+districtRequisitionNumbers.getUserMaster().getUserId()+"</td><td  colspan=4>"+districtRequisitionNumbers.getCreatedDateTime()+"</td></tr>");
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					msgAll.append("<tr><td  colspan=13 align=left><font size=2 color=red>"+drnList.size()+" Records Found&nbsp;</font></font></td></tr>");
				}
				if(requisitionNumber!=null){
					msgAll.append("<tr bgcolor=yellow><td  colspan=3 align=left><font size=2 color=green>JobRequisitionNumbers</font></td><td  colspan=7 align=center><font size=2 color=green>JobRequisitionNumbers</font></td><td  colspan=3 align=right><font size=2 color=green>JobRequisitionNumbers</font></td></tr>");
					List<JobRequisitionNumbers> reqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByDistrictOrJob(jobOrder,districtMaster, requisitionNumber);
					msgAll.append("<tr><td>JobRequisitionId</td><td colspan=4>JobId  ( IsExpHireNotEqualToReqNo, StartDate, EndDate, JobTitle,JobCategoryId ) </td><td>District Requisition Id</td><td>schoolId ( schoolName )</td><td colspan=6>Status</td></tr>");
					if(reqList.size()>0)
					for (JobRequisitionNumbers jobRequisitionNumbers : reqList) {
						try{
						String schoolIdReq="",schoolName="", multiHired="0";
						if(jobRequisitionNumbers.getSchoolMaster()!=null){
							schoolIdReq=jobRequisitionNumbers.getSchoolMaster().getSchoolId()+"";
							schoolName=jobRequisitionNumbers.getSchoolMaster().getSchoolName();
						}
						if(jobRequisitionNumbers.getJobOrder().getIsExpHireNotEqualToReqNo()!=null && jobRequisitionNumbers.getJobOrder().getIsExpHireNotEqualToReqNo()){
							multiHired="1";
						}
						msgAll.append("<tr><td>"+jobRequisitionNumbers.getJobRequisitionId()+"</td><td colspan=4>"+jobRequisitionNumbers.getJobOrder().getJobId()+" ( "+multiHired+", "+jobRequisitionNumbers.getJobOrder().getJobStartDate()+" , "+jobRequisitionNumbers.getJobOrder().getJobEndDate()+", "+jobRequisitionNumbers.getJobOrder().getJobTitle()+", "+jobRequisitionNumbers.getJobOrder().getJobCategoryMaster().getJobCategoryId()+" )</td><td>"+jobRequisitionNumbers.getDistrictRequisitionNumbers().getDistrictRequisitionId()+"</td><td nowrap>"+schoolIdReq+" ( "+schoolName+" )"+"</td><td colspan=6>"+jobRequisitionNumbers.getStatus()+"</td></tr>");
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					msgAll.append("<tr><td  colspan=13 align=left><font size=2 color=red>"+reqList.size()+" Records Found&nbsp;</font></td></tr>");
				}
				
				msgAll.append("<tr bgcolor=yellow><td  colspan=3 align=left><font size=2 color=green>JobForTeacher</font></td><td  colspan=7 align=center><font size=2 color=green>JobForTeacher</font></td><td  colspan=3 align=right><font size=2 color=green>JobForTeacher</font></td></tr>");
				List<JobForTeacher> jobForTeacherList=jobForTeacherDAO.getRequisitionNumbers(jobOrder,requisitionNumber,teacherDetail);
				
				msgAll.append("<tr><td>Job For TeacherId</br>DistrictId</td><td>TeacherId( FirstName, Email) </td><td>JobId ( JobTitle )</td><td>status</td><td>displayStatus</td><td> secondaryStatus ( secondary Status Name )</td><td>IsAffilated</td><td>Requisition Number</td><td>OfferReady</td><td>OfferMadeDate</td><td>OfferAccepted</td><td>OfferAccepted Date</td><td>CreatedDateTime</td></tr>");
				if(jobForTeacherList.size()>0)
				for (JobForTeacher jobForTeacher : jobForTeacherList) {
					try{
						if(requisitionNumber!=null){
							teacherDetail=jobForTeacher.getTeacherId();
							jobOrder=jobForTeacher.getJobId();
						}
						String reqNumber="",status="",displayStatus="",secondaryStatus="",secondaryStatusName="";
						if(jobForTeacher.getRequisitionNumber()!=null){
							reqNumber=jobForTeacher.getRequisitionNumber();
						}
						if(jobForTeacher.getStatus()!=null){
							status=jobForTeacher.getStatus().getStatusId()+" ( "+jobForTeacher.getStatus().getStatus()+" )";
						}
						if(jobForTeacher.getStatusMaster()!=null){
							displayStatus=jobForTeacher.getStatusMaster().getStatusId()+" ( "+jobForTeacher.getStatusMaster().getStatus()+" )";
						}
						if(jobForTeacher.getSecondaryStatus()!=null){
							secondaryStatus=jobForTeacher.getSecondaryStatus().getSecondaryStatusId()+"";
							secondaryStatusName=jobForTeacher.getSecondaryStatus().getSecondaryStatusName()+"";
						}
						msgAll.append("<tr><td>"+jobForTeacher.getJobForTeacherId()+"</br><b>"+jobForTeacher.getDistrictId()+"</b></td><td nowrap>"+jobForTeacher.getTeacherId().getTeacherId()+" ( "+jobForTeacher.getTeacherId().getFirstName()+", "+jobForTeacher.getTeacherId().getEmailAddress()+" ) "+"</td><td nowrap>"+jobForTeacher.getJobId().getJobId() +" ( "+jobForTeacher.getJobId().getJobTitle()+" ) "+"</td><td nowrap>"+status+"</td><td nowrap>"+displayStatus+"</td><td nowrap>"+secondaryStatus+" ( "+secondaryStatusName+" ) "+"</td><td>"+jobForTeacher.getIsAffilated()+"</td><td>"+reqNumber+"</td><td>"+jobForTeacher.getOfferReady()+"</td><td>"+jobForTeacher.getOfferMadeDate()+"</td><td>"+jobForTeacher.getOfferAccepted()+"</td><td>"+jobForTeacher.getOfferAcceptedDate()+"</td><td>"+jobForTeacher.getCreatedDateTime()+"</td></tr>");
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
				msgAll.append("<tr><td  colspan=13 align=left><font size=2 color=red>"+jobForTeacherList.size()+" Records Found&nbsp;</font></td></tr>");
				if(teacherDetail!=null){
					List<TeacherStatusHistoryForJob> tshJobs=teacherStatusHistoryForJobDAO.findByTeacherandJob(teacherDetail,jobOrder);
					msgAll.append("<tr bgcolor=yellow><td  colspan=3 align=left><font size=2 color=green>TeacherStatusHistoryForJob</font></td><td  colspan=7 align=center><font size=2 color=green>TeacherStatusHistoryForJob</font></td><td  colspan=3 align=right><font size=2 color=green>TeacherStatusHistoryForJob</font></td></tr>");
					msgAll.append("<tr><td>Teacher Status History ForJobId</td><td>TeacherId ( FirstName, EmailAddress ) </td><td nowrap>JobId ( JobTitle ) </td><td nowrap>displayStatus</td><td nowrap>secondaryStatus ( secondary StatusName ) </td><td>Status</td><td  colspan=3>UserId ( FirstName, EmailAddress ) </td><td  colspan=4> CreatedDateTime()</td></tr>");
					if(tshJobs.size()>0)
					for (TeacherStatusHistoryForJob tshObj : tshJobs) {
						try{
							String displayStatus="",secondaryStatus="",secondaryStatusName="";
							if(tshObj.getStatusMaster()!=null){
								displayStatus=tshObj.getStatusMaster().getStatusId()+" ( "+tshObj.getStatusMaster().getStatus()+" )";
							}
							if(tshObj.getSecondaryStatus()!=null){
								secondaryStatus=tshObj.getSecondaryStatus().getSecondaryStatusId()+"";
								secondaryStatusName=tshObj.getSecondaryStatus().getSecondaryStatusName()+"";
							}
							msgAll.append("<tr><td>"+tshObj.getTeacherStatusHistoryForJobId()+"</td><td>"+tshObj.getTeacherDetail().getTeacherId()+" ( "+tshObj.getTeacherDetail().getFirstName()+", "+tshObj.getTeacherDetail().getEmailAddress()+" ) "+"</td><td nowrap>"+tshObj.getJobOrder().getJobId() +" ( "+tshObj.getJobOrder().getJobTitle()+" ) "+"</td><td nowrap>"+displayStatus+"</td><td nowrap>"+secondaryStatus+" ( "+secondaryStatusName+" ) "+"</td><td>"+tshObj.getStatus()+"</td><td  colspan=3 nowrap>"+tshObj.getUserMaster().getUserId()+"( "+tshObj.getUserMaster().getFirstName()+", "+tshObj.getUserMaster().getEmailAddress()+" ) "+"</td><td colspan=4>"+tshObj.getCreatedDateTime()+"</td></tr>");
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					msgAll.append("<tr><td  colspan=13 align=left><font size=2 color=red>"+tshJobs.size()+" Records Found&nbsp;</font></td></tr>");
				}
				if(teacherDetail!=null){
					List<TeacherStatusNotes> notes=teacherStatusNotesDAO.getStatusNoteByJobOrTeacher(teacherDetail,jobOrder);
					msgAll.append("<tr bgcolor=yellow><td  colspan=3 align=left><font size=2 color=green>TeacherStatusNotes</font></td><td  colspan=7 align=center><font size=2 color=green>TeacherStatusNotes</font></td><td  colspan=3 align=right><font size=2 color=green>TeacherStatusNotes</font></td></tr>");
					msgAll.append("<tr><td>Teacher Status NoteId</td><td>TeacherId( FirstName,EmailAddress ) </td><td nowrap>JobId ( JobTitle )</td><td nowrap>displayStatus</td><td nowrap>secondaryStatus ( secondary Status Name ) </td><td>DistrictId</td><td  colspan=3 nowrap>UserId( FirstName,EmailAddress ) </td><td  colspan=2>StatusNotes</td><td>FinalizeStatus</td><td>CreatedDateTime</td></tr>");
					if(notes.size()>0)
					for (TeacherStatusNotes notesObj : notes) {
						try{
							String displayStatus="",secondaryStatus="",secondaryStatusName="";
							if(notesObj.getStatusMaster()!=null){
								displayStatus=notesObj.getStatusMaster().getStatusId()+" ( "+notesObj.getStatusMaster().getStatus()+" )";
							}
							if(notesObj.getSecondaryStatus()!=null){
								secondaryStatus=notesObj.getSecondaryStatus().getSecondaryStatusId()+"";
								secondaryStatusName=notesObj.getSecondaryStatus().getSecondaryStatusName()+"";
							}
							msgAll.append("<tr><td>"+notesObj.getTeacherStatusNoteId()+"</td><td>"+notesObj.getTeacherDetail().getTeacherId()+" ( "+notesObj.getTeacherDetail().getFirstName()+", "+notesObj.getTeacherDetail().getEmailAddress()+" ) "+"</td><td nowrap>"+notesObj.getJobOrder().getJobId() +" ( "+notesObj.getJobOrder().getJobTitle()+" ) "+"</td><td nowrap>"+displayStatus+"</td><td nowrap>"+secondaryStatus+" ( "+secondaryStatusName+" ) "+"</td><td>"+notesObj.getDistrictId()+"</td><td  colspan=3 nowrap>"+notesObj.getUserMaster().getUserId()+"( "+notesObj.getUserMaster().getFirstName()+", "+notesObj.getUserMaster().getEmailAddress()+" ) "+"</td><td  colspan=2>"+notesObj.getStatusNotes()+"</td><td>"+notesObj.isFinalizeStatus()+"</td><td>"+notesObj.getCreatedDateTime()+"</td></tr>");
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					msgAll.append("<tr><td  colspan=13 align=left><font size=2 color=red>"+notes.size()+" Records Found&nbsp;</font></td></tr>");
				}
				if(teacherDetail!=null){
					List<SchoolWiseCandidateStatus> schoolWises=schoolWiseCandidateStatusDAO.findByTeacherOrJob(teacherDetail,jobOrder);
					msgAll.append("<tr bgcolor=yellow><td  colspan=3 align=left><font size=2 color=green>SchoolWiseCandidateStatus</font></td><td  colspan=7 align=center><font size=2 color=green>SchoolWiseCandidateStatus</font></td><td  colspan=3 align=right><font size=2 color=green>SchoolWiseCandidateStatus</font></td></tr>");
					msgAll.append("<tr><td>School Wise StatusId</td><td>TeacherId( FirstName,EmailAddress ) </td><td nowrap>JobId ( JobTitle )</td><td nowrap>displayStatus</td><td nowrap>secondaryStatus ( secondary Status Name ) </td><td>DistrictId</td><td  colspan=3 nowrap>UserId( FirstName,EmailAddress ) </td><td  colspan=2>SchoolId ( SchoolName )</td><td>Status</td><td>CreatedDateTime</td></tr>");
					if(schoolWises.size()>0)
					for (SchoolWiseCandidateStatus wiseObj : schoolWises) {
						try{
							String displayStatus="",secondaryStatus="",secondaryStatusName="";
							if(wiseObj.getStatusMaster()!=null){
								displayStatus=wiseObj.getStatusMaster().getStatusId()+" ( "+wiseObj.getStatusMaster().getStatus()+" )";
							}
							if(wiseObj.getSecondaryStatus()!=null){
								secondaryStatus=wiseObj.getSecondaryStatus().getSecondaryStatusId()+"";
								secondaryStatusName=wiseObj.getSecondaryStatus().getSecondaryStatusName()+"";
							}
							msgAll.append("<tr><td>"+wiseObj.getSchoolWiseStatusId()+"</td><td>"+wiseObj.getTeacherDetail().getTeacherId()+" ( "+wiseObj.getTeacherDetail().getFirstName()+", "+wiseObj.getTeacherDetail().getEmailAddress()+" ) "+"</td><td nowrap>"+wiseObj.getJobOrder().getJobId() +" ( "+wiseObj.getJobOrder().getJobTitle()+" ) "+"</td><td nowrap>"+displayStatus+"</td><td nowrap>"+secondaryStatus+" ( "+secondaryStatusName+" ) "+"</td><td>"+wiseObj.getDistrictId()+"</td><td  colspan=3 nowrap>"+wiseObj.getUserMaster().getUserId()+"( "+wiseObj.getUserMaster().getFirstName()+", "+wiseObj.getUserMaster().getEmailAddress()+" ) "+"</td><td  colspan=2>"+wiseObj.getSchoolId()+" ( )</td><td>"+wiseObj.getStatus()+"</td><td>"+wiseObj.getCreatedDateTime()+"</td></tr>");
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					msgAll.append("<tr><td  colspan=13 align=left><font size=2 color=red>"+schoolWises.size()+" Records Found&nbsp;</font></td></tr>");
				}
				PanelSchedule panelSchedule=null;
				if(teacherDetail!=null && jobOrder!=null){
					List<PanelSchedule> panelJobList=panelScheduleDAO.getPanelSchedulesByTeacherJob(teacherDetail,jobOrder);
					msgAll.append("<tr bgcolor=yellow><td  colspan=3 align=left><font size=2 color=green>PanelSchedule</font></td><td  colspan=7 align=center><font size=2 color=green>PanelSchedule</font></td><td  colspan=3 align=right><font size=2 color=green>PanelSchedule</font></td></tr>");
					msgAll.append("<tr><td>PanelId</td><td>TeacherId( FirstName, Email )</td><td nowrap>JobId ( JobTitle )</td><td nowrap>DistrictId</td><td nowrap>PanelStatus</td><td>CreatedBy</td><td  colspan=8 nowrap>CreatedDateTime</td></tr>");
					if(panelJobList.size()>0)
					for (PanelSchedule panelScheduleObj : panelJobList) {
						try{
							if(panelScheduleObj.getPanelStatus().equals("Scheduled")){
								panelSchedule=panelScheduleObj;
							}
							int panelDistrict=0;
							if(panelScheduleObj.getDistrictId()!=null){
								panelDistrict=panelScheduleObj.getDistrictId();
							}
							msgAll.append("<tr><td>"+panelScheduleObj.getPanelId()+"</td><td>"+panelScheduleObj.getTeacherDetail().getTeacherId()+" ( "+panelScheduleObj.getTeacherDetail().getFirstName()+", "+panelScheduleObj.getTeacherDetail().getEmailAddress()+" ) "+"</td><td nowrap>"+panelScheduleObj.getJobOrder().getJobId() +" ( "+panelScheduleObj.getJobOrder().getJobTitle()+" ) "+"</td><td nowrap>"+panelDistrict+"</td><td nowrap>"+panelScheduleObj.getPanelStatus()+"</td><td>"+panelScheduleObj.getCreatedBy().getUserId()+"</td><td  colspan=8 nowrap>"+panelScheduleObj.getCreatedDateTime()+"</td></tr>");
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					msgAll.append("<tr><td  colspan=13 align=left><font size=2 color=red>"+panelJobList.size()+" Records Found&nbsp;</font></td></tr>");
				}
				if(panelSchedule!=null){
					List<PanelAttendees> panelAttendeesList=panelAttendeesDAO.getPanelAttendees(panelSchedule);
					msgAll.append("<tr bgcolor=yellow><td  colspan=3 align=left><font size=2 color=green>PanelAttendees</font></td><td  colspan=7 align=center><font size=2 color=green>PanelAttendees</font></td><td  colspan=3 align=right><font size=2 color=green>PanelAttendees</font></td></tr>");
					msgAll.append("<tr><td>Panel attendeeId</td><td>PanelInviteeId ( EntityType, FirstName, EmailAddress) "+"</td><td nowrap>PanelId</td><td nowrap>PanelStatus</td><td nowrap>PanelDate</td><td nowrap>HostId</td><td  colspan=7 nowrap>single signin URL</td></tr>");
					if(panelAttendeesList.size()>0)
					for (PanelAttendees panelAttendees : panelAttendeesList) {
						try{
							String URL="";
							UserMaster userMaster=null;
							try{
								userMaster=panelAttendees.getPanelInviteeId();
								if(userMaster.getEmployeeMaster()!=null){
									URL=Utility.getBaseURL(request)+"service/singlesignin.do?authKey=OEtKNlE1VE0yMDE0MDYxNjA0MjQxNg==&employeeCode="+userMaster.getEmployeeMaster().getEmployeeCode()+"&locationCode="+userMaster.getSchoolId().getLocationCode();
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							msgAll.append("<tr><td>"+panelAttendees.getPanelattendeeId()+"</td><td nowrap>"+panelAttendees.getPanelInviteeId().getUserId()+" ( "+panelAttendees.getPanelInviteeId().getEntityType()+", "+panelAttendees.getPanelInviteeId().getFirstName()+", "+panelAttendees.getPanelInviteeId().getEmailAddress()+" ) "+"</td><td nowrap>"+panelAttendees.getPanelSchedule().getPanelId()+"</td><td nowrap>"+panelAttendees.getPanelStatus()+"</td><td nowrap>"+panelAttendees.getPanelDate()+"</td><td nowrap>"+panelAttendees.getHostId().getUserId()+"</td><td  colspan=7 nowrap>"+URL+"</td></tr>");
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					msgAll.append("<tr><td  colspan=13 align=left><font size=2 color=red>"+panelAttendeesList.size()+" Records Found&nbsp;</font></td></tr>");
					
				}
				
				msgAll.append("</table>");
				//System.out.println("msgAll:::"+msgAll);
			}
			if(password!=null && password.equals("netSutra") && ( districtId!=null && !districtId.equals("") || schoolId!=null && !schoolId.equals("") || ufname!=null && !ufname.equals("") || ulname!=null && !ulname.equals("")|| uemail!=null && !uemail.equals("") || uid!=null && !uid.equals(""))){
				mainFlag=false;
				msgAll.append("<table border=1 cellpadding=5 cellspacing=5 width=100%>");
				List<UserMaster> userMasterList=userMasterDAO.findByUserMasters(ufname,ulname,uemail,uid,districtMaster,schoolMaster);
				msgAll.append("<tr   bgcolor=yellow><td  colspan=11 align=left><font size=2 color=green>UserMaster</font></td></tr>");
				msgAll.append("<tr><td>UserId</td><td nowrap>FirstName</td><td nowrap>LastName</td><td nowrap>EmailAddress</td><td nowrap>district Details</td><td nowrap>school Details</td><td nowrap>Single SignIn URL</td><td nowrap>EntityType</td><td nowrap>Status</td><td nowrap>ForgetCounter</td><td  nowrap>CreatedDateTime</td></tr>");
				if(userMasterList.size()>0)
				for (UserMaster userMaster : userMasterList) {
					try{
						String URL="",districtDetails="",schoolDetails="";
						
						try{
							if(userMaster.getEmployeeMaster()!=null){
								URL=Utility.getBaseURL(request)+"service/singlesignin.do?authKey=OEtKNlE1VE0yMDE0MDYxNjA0MjQxNg==&employeeCode="+userMaster.getEmployeeMaster().getEmployeeCode()+"&locationCode="+userMaster.getSchoolId().getLocationCode();
							}
						}catch(Exception e){
							//e.printStackTrace();
						}
						try{
							if(userMaster.getEntityType()!=1){
								districtDetails=userMaster.getDistrictId().getDistrictId()+"( "+userMaster.getDistrictId().getDistrictName()+" )";
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						try{
							if(userMaster.getEntityType()!=1){
								if(userMaster.getSchoolId()!=null){
									schoolDetails=userMaster.getSchoolId().getSchoolId()+"( "+userMaster.getSchoolId().getSchoolName()+" )";
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						msgAll.append("<tr><td>"+userMaster.getUserId()+"</td><td nowrap>"+userMaster.getFirstName()+"</td><td nowrap>"+userMaster.getLastName()+"</td><td nowrap>"+userMaster.getEmailAddress()+"</td><td nowrap>"+districtDetails+"</td><td nowrap>"+schoolDetails+"</td><td nowrap>"+URL+"</td><td nowrap>"+userMaster.getEntityType()+"</td><td nowrap>"+userMaster.getStatus()+"</td><td nowrap>"+userMaster.getForgetCounter()+"</td><td nowrap>"+userMaster.getCreatedDateTime()+"</td></tr>");
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				msgAll.append("<tr><td  colspan=11 align=left><font size=2 color=red>"+userMasterList.size()+" Records Found&nbsp;</font></td></tr>");
				msgAll.append("</table>");
			}
			if(password!=null && password.equals("netSutra") && (tfname!=null && !tfname.equals("") || tlname!=null && !tlname.equals("")|| temail!=null && !temail.equals("") || tid!=null && !tid.equals(""))){
				mainFlag=false;
				msgAll.append("<table border=1 cellpadding=5 cellspacing=5 width=100%>");
				List<TeacherDetail> teacherList=teacherDetailDAO.findByTeacherDetails(tfname,tlname,temail,tid);
				msgAll.append("<tr   bgcolor=yellow><td  colspan=8 align=left><font size=2 color=green>TeacherDetail</font></td></tr>");
				msgAll.append("<tr><td>TeacherId</td><td nowrap>FirstName</td><td nowrap>LastName</td><td nowrap>EmailAddress</td><td nowrap>UserType</td><td nowrap>Status</td><td nowrap>ForgetCounter</td><td  nowrap>CreatedDateTime</td></tr>");
				if(teacherList.size()>0)
				for (TeacherDetail teacherDetail : teacherList) {
					try{
						msgAll.append("<tr><td>"+teacherDetail.getTeacherId()+"</td><td nowrap>"+teacherDetail.getFirstName()+"</td><td nowrap>"+teacherDetail.getLastName()+"</td><td nowrap>"+teacherDetail.getEmailAddress()+"</td><td nowrap>"+teacherDetail.getUserType()+"</td><td nowrap>"+teacherDetail.getStatus()+"</td><td nowrap>"+teacherDetail.getForgetCounter()+"</td><td nowrap>"+teacherDetail.getCreatedDateTime()+"</td></tr>");
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				msgAll.append("<tr><td  colspan=8 align=left><font size=2 color=red>"+teacherList.size()+" Records Found&nbsp;</font></td></tr>");				
				msgAll.append("</table>");
			}
			if(password!=null && password.equals("netSutra") && empNum!=null && !empNum.equals("")){
				mainFlag=false;
				List<TeacherPersonalInfo> tprInfo = teacherPersonalInfoDAO.checkTeachersByEmpNmbr(empNum);
				msgAll.append("<table border=1 cellpadding=5 cellspacing=5 width=100%>");
				msgAll.append("<tr   bgcolor=yellow><td  colspan=8 align=left><font size=2 color=green>Teacher Personal Info</font></td></tr>");
				msgAll.append("<tr><td>TeacherId</td><td nowrap>FirstName</td><td nowrap>LastName</td><td nowrap>SSN</td></tr>");
				if(tprInfo.size()>0)
					for (TeacherPersonalInfo teacherPersonalInfo : tprInfo) {
						try{
							String ssn=""; 
							if(teacherPersonalInfo.getSSN()!=null && teacherPersonalInfo.getSSN()!=""){
								String tempssn = Utility.decodeBase64(teacherPersonalInfo.getSSN());
								ssn=tempssn.substring(tempssn.length() - 4);
							}
								// string.substring(string.length() - 1))
							msgAll.append("<tr><td>"+teacherPersonalInfo.getTeacherId()+"</td><td nowrap>"+teacherPersonalInfo.getFirstName()+"</td><td nowrap>"+teacherPersonalInfo.getLastName()+"</td><td nowrap>"+ssn+"</td></tr>");
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				else{
					msgAll.append("<tr bgcolor=red><td  colspan=8 align=left><font size=2 color=green>No Record Found</font></td></tr>");
				}
					msgAll.append("</table>");
					
					msgAll.append("<table border=1 cellpadding=5 cellspacing=5 width=100%>");
				Criterion criterionDis = Restrictions.eq("districtMaster", districtMaster);
				Criterion criterionEmpNum = Restrictions.eq("employeeCode", empNum);
				List<EmployeeMaster> empList = new ArrayList<EmployeeMaster>();
				if(districtMaster!=null)
						empList = employeeMasterDAO.findByCriteria(criterionDis,criterionEmpNum);
					else
						empList = employeeMasterDAO.findByCriteria(criterionEmpNum);
				
					msgAll.append("<tr   bgcolor=yellow><td  colspan=8 align=left><font size=2 color=green>Employee Detail</font></td></tr>");
					msgAll.append("<tr><td>EmployeeId</td><td nowrap>FirstName</td><td nowrap>LastName</td><td nowrap>SSN</td><td nowrap>Employee Number</td></tr>");
					if(empList.size()>0)
						for (EmployeeMaster employeeNumber : empList) {
							try{
								msgAll.append("<tr><td>"+employeeNumber.getEmployeeId()+"</td><td nowrap>"+employeeNumber.getFirstName()+"</td><td nowrap>"+employeeNumber.getLastName()+"</td><td nowrap>"+employeeNumber.getSSN()+"</td><td nowrap>"+employeeNumber.getEmployeeCode()+"</td></tr>");
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					else
						msgAll.append("<tr bgcolor=red><td  colspan=8 align=left><font size=2 color=green>No Record Found</font></td></tr>");
					
					msgAll.append("</table>");
				
			}
			
			if(password!=null && password.equals("netSutra") && (sId!=null && !sId.equals("") && sFlag!=null && !sFlag.equals(""))){
				mainFlag=false;
				msgAll.append("<table border=1 cellpadding=5 cellspacing=5 width=100%>");
				Criterion criterion1=null;
				if(sFlag.equals("s")){
					StatusMaster statusMaster = WorkThreadServlet.statusIdMap.get(Integer.parseInt(sId));
					criterion1 = Restrictions.eq("statusMaster",statusMaster);	
				}else{
					SecondaryStatus secondaryStatus = secondaryStatusDAO.findById(Integer.parseInt(sId),false,false);
					criterion1 = Restrictions.eq("secondaryStatus",secondaryStatus);
				}
				List<TeacherStatusHistoryForJob> tshJobs=teacherStatusHistoryForJobDAO.findWithLimit(Order.desc("createdDateTime"),1,50,criterion1);
				msgAll.append("<tr bgcolor=yellow><td  colspan=3 align=left><font size=2 color=green>TeacherStatusHistoryForJob</font></td><td  colspan=7 align=center><font size=2 color=green>TeacherStatusHistoryForJob</font></td><td  colspan=3 align=right><font size=2 color=green>TeacherStatusHistoryForJob</font></td></tr>");
				msgAll.append("<tr><td>Teacher Status History ForJobId</td><td>TeacherId ( FirstName, EmailAddress ) </td><td nowrap>JobId ( JobTitle ) </td><td nowrap>displayStatus</td><td nowrap>secondaryStatus ( secondary StatusName ) </td><td>Status</td><td  colspan=3>UserId ( FirstName, EmailAddress ) </td><td  colspan=4> CreatedDateTime()</td></tr>");
				if(tshJobs.size()>0)
				for (TeacherStatusHistoryForJob tshObj : tshJobs) {
					try{
						String displayStatus="",secondaryStatus="",secondaryStatusName="";
						if(tshObj.getStatusMaster()!=null){
							displayStatus=tshObj.getStatusMaster().getStatusId()+" ( "+tshObj.getStatusMaster().getStatus()+" )";
						}
						if(tshObj.getSecondaryStatus()!=null){
							secondaryStatus=tshObj.getSecondaryStatus().getSecondaryStatusId()+"";
							secondaryStatusName=tshObj.getSecondaryStatus().getSecondaryStatusName()+"";
						}
						msgAll.append("<tr><td>"+tshObj.getTeacherStatusHistoryForJobId()+"</td><td>"+tshObj.getTeacherDetail().getTeacherId()+" ( "+tshObj.getTeacherDetail().getFirstName()+", "+tshObj.getTeacherDetail().getEmailAddress()+" ) "+"</td><td nowrap>"+tshObj.getJobOrder().getJobId() +" ( "+tshObj.getJobOrder().getJobTitle()+" ) "+"</td><td nowrap>"+displayStatus+"</td><td nowrap>"+secondaryStatus+" ( "+secondaryStatusName+" ) "+"</td><td>"+tshObj.getStatus()+"</td><td  colspan=3 nowrap>"+tshObj.getUserMaster().getUserId()+"( "+tshObj.getUserMaster().getFirstName()+", "+tshObj.getUserMaster().getEmailAddress()+" ) "+"</td><td colspan=4>"+tshObj.getCreatedDateTime()+"</td></tr>");
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				msgAll.append("<tr><td  colspan=13 align=left><font size=2 color=red>"+tshJobs.size()+" Records Found&nbsp;</font></td></tr>");
				msgAll.append("</table>");
			}
			if(mainFlag){
				msgAll.append("Please enter password,reqNo,teacherId,empnum,jobId,jobTitle and districtId optional (dFlag=e) <br>TeacherDetails: tfname,tlname,temail,tid </br>UserDetails: ufname,ulname,uemail,uid,districtId,schoolId<br>sId,sFlag( s,ss )<br>School Detail: locCode</br>");	
			}
			map.addAttribute("msgAll",msgAll);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "imp";
	}
	@Transactional
	@RequestMapping(value="/updateFitScore.do", method=RequestMethod.GET)
	public void updateFitScore(ModelMap map,HttpServletRequest request)
	{
		System.out.println(":::::::::::::::::::::::::new:::::::updateFitScore.do::::::::>>:::::::::::::::::::::::");
		/*
		System.out.println(":::::::::::::::::::::::::>>:::::::updateFitScore.do::::::::>>:::::::::::::::::::::::");
		try 
		{
			String tIds="681,55243,55603,56154,56217,56218,56219,56256,56434,56461,56465,56483,56775,56807,56952,56987,57190,57210,57244,57540,57712,57745,57834,58144,58229,58233,58319,58337,58389,58452,58616,58670,58767,59008,59025,59174,59325,59338,59373,59472,59526,59762,59820,59941,59976,59994,60220,60267,60311,60352,60460,60476,60479,60541,60549,60557,60695,60709,60717,60739,60844,60983,61022,61313,61328,61458,61582,61603,61665,61875,61984,62185,62262,62350,62385,62387,62399,62470,62532,62751,62756,62905,63050,63227,63472,63771,63782,63943,63961,64022,64856,64875,65245,65353,65427,65498,66430,66524,66686,66807,66856,66877,66957,67149,67380,67381,67652,67694,68674,68737,68828,68894,69491,69716,69839,69951,70065,70421,70599,70877,70971,71074,71166,71603,71695,72284,72591,72644,72884,73005,73040,73260,73689,74125";
			String teacherIds[]=tIds.split(",");
			String jIds="7340,7342,7345,7346,7347,7348,7353,7360,7365,7387,7395,7397,7398,7399,7400,7401,7402,7404,7405,7406,7407,7408,7409,7410,7411,7412,7413,7414,7416,7418,7419,7420,7421,7422,7423,7424,7474,7475,7476,7477,7478,7479,7481,7482,7483,7484,7485,7486,7487,7488,7489,7490,7491,7492,7493,7494,7495,7496,7497,7498,7499,7500,7501,7515,7530,7531,7532,7533,7534,7535,7536,7551,7552,7553,7554,7555,7556,7557,7558,7559,7560,7561,7562,7563,7564,7572,7573,7574,7575,7576,7577,7578,7580,7587,7682,7704,8298,9961";
			String jobIds[]=jIds.split(",");
			String secondaryStatus[]="3815,3816,3852,3891,4065,4087,4113,4139,4165,4604,4631".split(",");
			Set<Integer> ssSet=new TreeSet<Integer>();
			Set<Integer> jobIdsSet=new TreeSet<Integer>();
			Set<Integer> teacherIdsSet=new TreeSet<Integer>();
			Map<String,String> teacherAndJobWiseScoreMap=new LinkedHashMap<String, String>();
			Map<String,Set<Integer>> teacherAndJobWiseUserCountMap=new LinkedHashMap<String, Set<Integer>>();
			Map<String,String> teacherJobAndStatusWiseScoreMap=new LinkedHashMap<String, String>();	
			Map<String,List<StatusSpecificScore>> teacherAndJobWiseSSSObjMap=new LinkedHashMap<String, List<StatusSpecificScore>>();
			List<JobOrder> lstJobOrder=new ArrayList<JobOrder>();
			List<TeacherDetail> lstTeacherDetail=new ArrayList<TeacherDetail>();
			for(String ssId:secondaryStatus){
				ssSet.add(Integer.parseInt(ssId));
			}
			for(String ssId:teacherIds){
				teacherIdsSet.add(Integer.parseInt(ssId));
			}
			for(String ssId:jobIds){
				jobIdsSet.add(Integer.parseInt(ssId));
			}
			List<StatusSpecificScore> lstSSS=statusSpecificScoreDAO.getSSSByDistrictAndSecondaryStatus(4218990,"Online Activity");
			System.out.println("Result Get"+lstSSS.size());
			for(StatusSpecificScore sss:lstSSS){
				System.out.println("sss  =="+sss.getTeacherDetail().getTeacherId()+"  "+sss.getJobOrder().getJobId()+" "+sss.getSecondaryStatus().getSecondaryStatusName()+" "+sss.getUserMaster().getUserId()+" "+sss.getSecondaryStatus().getSecondaryStatusId()+" "+sss.getScoreProvided()+" "+sss.getMaxScore());
				if(teacherJobAndStatusWiseScoreMap.get(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId()+"##"+sss.getSecondaryStatus().getSecondaryStatusId())==null)
				{
				teacherJobAndStatusWiseScoreMap.put(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId()+"##"+sss.getSecondaryStatus().getSecondaryStatusId(), sss.getScoreProvided()+"##"+sss.getMaxScore());
				}else{
					String score[]=teacherJobAndStatusWiseScoreMap.get(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId()+"##"+sss.getSecondaryStatus().getSecondaryStatusId()).split("##");
					int providedScore=Integer.parseInt(score[0])+sss.getScoreProvided();				
					int maxScore=Integer.parseInt(score[1])+sss.getMaxScore();
					teacherJobAndStatusWiseScoreMap.put(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId()+"##"+sss.getSecondaryStatus().getSecondaryStatusId(), providedScore+"##"+maxScore);
				}
				if(ssSet.contains(sss.getSecondaryStatus().getSecondaryStatusId())){
				if(teacherAndJobWiseScoreMap.get(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId())==null)
				{
					lstJobOrder.add(sss.getJobOrder());
					lstTeacherDetail.add(sss.getTeacherDetail());
					List<StatusSpecificScore> lstTAJ_SSS=new ArrayList<StatusSpecificScore> ();
					lstTAJ_SSS.add(sss);
					teacherAndJobWiseSSSObjMap.put(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId(), lstTAJ_SSS);
					teacherAndJobWiseScoreMap.put(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId(), sss.getScoreProvided()+"##"+sss.getMaxScore());
				}else{
					lstJobOrder.add(sss.getJobOrder());
					lstTeacherDetail.add(sss.getTeacherDetail());
					List<StatusSpecificScore> lstTAJ_SSS=teacherAndJobWiseSSSObjMap.get(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId());
					lstTAJ_SSS.add(sss);
					teacherAndJobWiseSSSObjMap.put(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId(), lstTAJ_SSS);
					String score[]=teacherAndJobWiseScoreMap.get(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId()).split("##");
					int providedScore=Integer.parseInt(score[0])+sss.getScoreProvided();				
					int maxScore=Integer.parseInt(score[1])+sss.getMaxScore();
					teacherAndJobWiseScoreMap.put(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId(), providedScore+"##"+maxScore);
				}
				if(teacherAndJobWiseUserCountMap.get(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId())==null)
				{
					Set<Integer> userIdSet=new TreeSet<Integer>();
					userIdSet.add(sss.getUserMaster().getUserId());
					teacherAndJobWiseUserCountMap.put(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId(), userIdSet);
				}else{
					Set<Integer> userIdSet=teacherAndJobWiseUserCountMap.get(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId());
					userIdSet.add(sss.getUserMaster().getUserId());
					teacherAndJobWiseUserCountMap.put(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId(),userIdSet);
				}
				}
			} 
			
			Map<String,JobWiseConsolidatedTeacherScore> jobWiseConsolidatedTeacherScoreMap=new LinkedHashMap<String, JobWiseConsolidatedTeacherScore>();
			List<JobWiseConsolidatedTeacherScore> lstJobWiseConsolidatedTeacherScore=jobWiseConsolidatedTeacherScoreDAO.findByCriteria(Restrictions.in("jobOrder", lstJobOrder),Restrictions.in("teacherDetail", lstTeacherDetail));
			for(JobWiseConsolidatedTeacherScore jwct:lstJobWiseConsolidatedTeacherScore){
				jobWiseConsolidatedTeacherScoreMap.put(jwct.getTeacherDetail().getTeacherId()+"##"+jwct.getJobOrder().getJobId(), jwct);
			}
			
			List<TeacherDetail> allTeacherforTSS=new ArrayList<TeacherDetail>();
			List<JobOrder> allJobOrderForTSS=new ArrayList<JobOrder>();
			List<SecondaryStatus> allSecondaryStatusForTSS=new ArrayList<SecondaryStatus>();
			for(Map.Entry<String, List<StatusSpecificScore>> entry:teacherAndJobWiseSSSObjMap.entrySet()){
				for(StatusSpecificScore sss:entry.getValue()){
					allTeacherforTSS.add(sss.getTeacherDetail());
					allJobOrderForTSS.add(sss.getJobOrder());
					allSecondaryStatusForTSS.add(sss.getSecondaryStatus());
					
				}
			}
			List<TeacherStatusScores> lstTSS=teacherStatusScoresDAO.findByCriteria(Restrictions.in("teacherDetail", allTeacherforTSS),Restrictions.in("jobOrder", allJobOrderForTSS),Restrictions.in("secondaryStatus", allSecondaryStatusForTSS));
			Map<String,List<TeacherStatusScores>> tssMap=new LinkedHashMap<String, List<TeacherStatusScores>>();
			for(TeacherStatusScores tss:lstTSS){
				if(tssMap.get(tss.getTeacherDetail().getTeacherId()+"##"+tss.getJobOrder().getJobId()+"##"+tss.getSecondaryStatus().getSecondaryStatusId())==null){
					List<TeacherStatusScores> lst_TSS=new ArrayList<TeacherStatusScores>();
					lst_TSS.add(tss);
					tssMap.put(tss.getTeacherDetail().getTeacherId()+"##"+tss.getJobOrder().getJobId()+"##"+tss.getSecondaryStatus().getSecondaryStatusId(), lst_TSS);
				}else{
					List<TeacherStatusScores> lst_TSS=tssMap.get(tss.getTeacherDetail().getTeacherId()+"##"+tss.getJobOrder().getJobId()+"##"+tss.getSecondaryStatus().getSecondaryStatusId());
					lst_TSS.add(tss);
					tssMap.put(tss.getTeacherDetail().getTeacherId()+"##"+tss.getJobOrder().getJobId()+"##"+tss.getSecondaryStatus().getSecondaryStatusId(), lst_TSS);
				}
			}
			System.out.println("---------------------------------------------teacherJobAndStatusWiseScoreMap---------------------------------------------------");
			for(Map.Entry<String, String> entry:teacherJobAndStatusWiseScoreMap.entrySet()){
				System.out.println("Status key====="+entry.getKey()+" value====="+entry.getValue());
			}
			System.out.println("---------------------------------------------teacherAndJobWiseScoreMap---------------------------------------------------");
			for(Map.Entry<String, String> entry:teacherAndJobWiseScoreMap.entrySet()){
				System.out.println("tId And JobId key====="+entry.getKey()+" value====="+entry.getValue());
			}
			System.out.println("---------------------------------------------teacherAndJobWiseUserCountMap---------------------------------------------------");
			for(Map.Entry<String, Set<Integer>> entry:teacherAndJobWiseUserCountMap.entrySet()){
				System.out.println("count User key====="+entry.getKey()+" value====="+entry.getValue().size());
			}
			
			System.out.println("---------------------------------------------final update and delete record---------------------------------------------------");			
				
			
			
			ssSet.clear();
			teacherJobAndStatusWiseScoreMap.clear();	
			lstJobOrder.clear();
			lstTeacherDetail.clear();		
			lstTSS.clear();
			allTeacherforTSS.clear();
			allJobOrderForTSS.clear();
			allSecondaryStatusForTSS.clear();
			lstJobWiseConsolidatedTeacherScore.clear();
			Session statelessSession=sessionFactory.openSession();
			statelessSession.beginTransaction();
			
			
			
			for(Map.Entry<String, String> entry:teacherAndJobWiseScoreMap.entrySet()){
				System.out.println("Status key====="+entry.getKey()+" value====="+entry.getValue());
				Integer teacherId=Integer.parseInt(entry.getKey().split("##")[0]);
				Integer jobId=Integer.parseInt(entry.getKey().split("##")[1]);
				System.out.println("___________________________________Checking________________________________________");
				if(teacherIdsSet.contains(teacherId) && jobIdsSet.contains(jobId)){
					System.out.println("teacherId=="+teacherId+"   jobId==="+jobId +" keyValue===="+entry.getValue());
					int userCount=teacherAndJobWiseUserCountMap.get(entry.getKey()).size();
					System.out.println("user count==="+userCount);
					int totalProvidedScore=Integer.parseInt(entry.getValue().split("##")[0]);
					int totalMaxScore=Integer.parseInt(entry.getValue().split("##")[1]);
					int substractProvidedScore=totalProvidedScore/userCount;
					int substractMaxScore=totalMaxScore/userCount;
					System.out.println("totalProvidedScore===="+totalProvidedScore+"  totalMaxScore===="+totalMaxScore+"   substractProvidedScore===="+substractProvidedScore+"   substractMaxScore====="+substractMaxScore);
					JobWiseConsolidatedTeacherScore jwct=jobWiseConsolidatedTeacherScoreMap.get(entry.getKey());
					System.out.println("jwct.getJobWiseConsolidatedScore()==1="+jwct.getJobWiseConsolidatedScore()+"      jwct.getJobWiseMaxScore()==1=="+jwct.getJobWiseMaxScore());
					jwct.setJobWiseConsolidatedScore(jwct.getJobWiseConsolidatedScore()-substractProvidedScore);
					jwct.setJobWiseMaxScore(jwct.getJobWiseMaxScore()-substractMaxScore);
					System.out.println("enter1");
					System.out.println("jwct.getJobWiseConsolidatedScore()==2="+jwct.getJobWiseConsolidatedScore()+"      jwct.getJobWiseMaxScore()===2="+jwct.getJobWiseMaxScore());
					//statelessSession.beginTransaction();
					statelessSession.update(jwct);		
					//statelessSession.getTransaction().commit();
					System.out.println("jwct.getJobWiseConsolidatedScore()==="+jwct.getJobWiseConsolidatedScore()+"      jwct.getJobWiseMaxScore()===="+jwct.getJobWiseMaxScore());
					System.out.println("enter2");
					List<StatusSpecificScore> lst=teacherAndJobWiseSSSObjMap.get(entry.getKey());
					if(lst!=null)
					for(StatusSpecificScore sss:lst){
						List<TeacherStatusScores> lst_TSS=tssMap.get(sss.getTeacherDetail().getTeacherId()+"##"+sss.getJobOrder().getJobId()+"##"+sss.getSecondaryStatus().getSecondaryStatusId());
						//statelessSession.beginTransaction();
						if(lst_TSS!=null)
						for(TeacherStatusScores tss:lst_TSS){
							System.out.println("enter3");							
							statelessSession.delete(tss);								
							System.out.println("enter4");
						}
						//statelessSession.getTransaction().commit();
						System.out.println("enter5");
						//statelessSession.beginTransaction();
						statelessSession.delete(sss);
						//statelessSession.getTransaction().commit();
						System.out.println("enter6");
					}					
				}
				System.out.println("___________________________________Checking End________________________________________");
			}
			statelessSession.getTransaction().commit();
			statelessSession.close();
			
		}catch(Exception e){
			e.printStackTrace();
			}
	*/}
	/*============ Get Method of ImpController ====================*/
	/*@RequestMapping(value="/imp.do", method=RequestMethod.GET)
	public String impGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
		System.out.println("\n =========== Imp Controller ===============");
			String startIdQuery="select min(teacherId) from teacherdetail where authenticationCode=1111 order by teacherId limit 1";
			Connection conn = getConnection();
			Statement stmt=(Statement) conn.createStatement();
			ResultSet rsStateId=stmt.executeQuery(startIdQuery);
			rsStateId.next();
			String startId=rsStateId.getString(1);
			System.out.println("startId::::"+startId);
			
			if(startId==null){
				startId="0";
			}
			
			String startIdQuerys="select * from teacherdetail where authenticationCode=1111 order by teacherId";
			ResultSet rsStateIds=stmt.executeQuery(startIdQuerys);
			int totRow=0;
			while(rsStateIds.next()){
				totRow++;
			}
	    	map.addAttribute("page",true);
			map.addAttribute("message","Welocme Upload Resume</br> startId:-"+startId +" No Of Records:-"+totRow);
			map.addAttribute("startId",startId);
			map.addAttribute("idCount",totRow);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "imp";
	}
	
	@RequestMapping(value="/impPref.do", method=RequestMethod.GET)
	public String impPref(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== impData ===============");
			Connection conn = getConnection();
			Statement stmt=(Statement) conn.createStatement();
			ResultSet rs=stmt.executeQuery("select * from teacherdetail where authenticationCode=1111 order by teacherId");
			while(rs.next()){ 
				Statement st=(Statement) conn.createStatement();
				String insertQuery="INSERT INTO `teacherpreference` (`teacherId`, `geoId`, `schoolTypeId`, `regionId`, `createdDateTime`) VALUES "+
				"("+rs.getString(1)+", '11|12|21|31|41', '1|2|3', '2|3|5|6|8|9|10|12|13|15|16', '2013-04-11 02:46:35')";
				st.executeUpdate(insertQuery);
				//System.out.println("insertQuery:::"+insertQuery);
			}
		    System.out.println("Insert SuccessFully.");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("page",false);
		map.addAttribute("message","SuccessFully Inserted TeacherPreference ");
		return "imp";
	}
	@RequestMapping(value="/impCityId.do", method=RequestMethod.GET)
	public String impCityId(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== impCityId.do ===============");
			Connection conn = getConnection();
			Statement stmt=(Statement) conn.createStatement();
			ResultSet rs=stmt.executeQuery("select * from city order by id");
			while(rs.next()){
				int cityId=0;
				Statement stmt2=(Statement) conn.createStatement();
				ResultSet rsCTM=stmt2.executeQuery("select * from citymaster where stateId="+rs.getString("stateId")+" order by cityName");
				Map<String,String> mapCityMaster = new TreeMap<String, String>();
				while(rsCTM.next()){
					mapCityMaster.put(""+rsCTM.getString("cityName"), rsCTM.getString("cityId"));
				}
				rsCTM.close();
				for(Map.Entry<String,String> entry : mapCityMaster.entrySet()) 
				{
					  String key = entry.getKey();
					 // System.out.println("value:::::"+key+"::::::::"+entry.getValue());
					  if(key.equalsIgnoreCase(rs.getString("cityName"))){
						  cityId=Integer.parseInt(entry.getValue()); 
						//  System.out.println("City Id:::::::"+cityId);
					  }
				}
				
				if(cityId!=0){
					Statement st=(Statement) conn.createStatement();
					String updateQuery="UPDATE  city  SET  cityId ="+cityId+" WHERE id = "+rs.getString(1);
					st.executeUpdate(updateQuery);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("page",false);
		map.addAttribute("message","SuccessFully Inserted CityId");
		return "imp";
	}
	
	@RequestMapping(value="/impZip.do", method=RequestMethod.GET)
	public String impZip(ModelMap map,HttpServletRequest request)
	{
		int cityId=0;
		try 
		{
			System.out.println("\n =========== impZip ===============");
			Connection conn = getConnection();
			Statement stmt=(Statement) conn.createStatement();
				Statement stmt2=(Statement) conn.createStatement();
				ResultSet rsCTM=stmt2.executeQuery("select * from citymaster where stateId=14 order by cityName");
				Map<String,String> mapCityMaster = new TreeMap<String, String>();
				while(rsCTM.next()){
					mapCityMaster.put(""+rsCTM.getString("cityName"), rsCTM.getString("cityId"));
				}
				rsCTM.close();
				for(Map.Entry<String,String> entry : mapCityMaster.entrySet()) 
				{
					  String key = entry.getKey();
					  if(key.equalsIgnoreCase("CHICAGO")){
						  cityId=Integer.parseInt(entry.getValue()); 
					  }
				}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("page",false);
		map.addAttribute("message","SuccessFully CityId:: "+cityId);
		return "imp";
	}
	
	@RequestMapping(value="/impSatteId.do", method=RequestMethod.GET)
	public String impSatteId(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== impSatteId.do ===============");
			String startIdQuery="select min(teacherId) from teacherdetail where authenticationCode=1111 order by teacherId limit 1";
			Connection conn = getConnection();
			Statement st=(Statement) conn.createStatement();
			String updateQuery="UPDATE  city c,statemaster c2 SET  c.stateId =c2.stateId WHERE c.stateName =c2.stateShortName";
			st.executeUpdate(updateQuery);
			System.out.println("updateQuery State:::"+updateQuery);
		    System.out.println("StateId Inserted SuccessFully.");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("page",false);
		map.addAttribute("message","SuccessFully Inserted StateId ");
		return "imp";
	}
	@RequestMapping(value="/impSCZip.do", method=RequestMethod.GET)
	public String impSCZip(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== impSCZip.do ===============");
			String cityId= request.getParameter("cityId");
			String zipCode= request.getParameter("zipCode");
			if(cityId!=null && zipCode!=null){
				Connection conn = getConnection();
				Statement st=(Statement) conn.createStatement();
				String updateQuery="UPDATE  city c SET  c.stateId =14,c.cityId ="+cityId+",zipCode ="+zipCode+" WHERE c.cityId =0";
				st.executeUpdate(updateQuery);
				System.out.println("updateQuery City , State and Zip Code:::"+updateQuery);
			    System.out.println("StateId Inserted SuccessFully.");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("page",false);
		map.addAttribute("message","SuccessFully Inserted City , State and Zip Code ");
		return "imp";
	}
	public int random(){
		int u=0;
		 Random r = new Random();
		   int Low = 0;
		   int High = 2;
		   u = r.nextInt(High-Low) + Low;
		   return u;
	}
	@RequestMapping(value="/impCG.do", method=RequestMethod.GET)
	public String impCG(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== impCG ===============");
			int startId=Integer.parseInt(request.getParameter("startId"));
			int endId=Integer.parseInt(request.getParameter("endId"));
			
			System.out.println("StartId::="+startId + "EndId::="+endId);
			Connection conn = getConnection();
			
			
			for(int i=startId;i<endId;i++){
				Statement st=(Statement) conn.createStatement();
				String v1="",v2="",v3="";
				int dId=1;
				for(int y=0;y<3; y++){
					int rr=random();
					if(y==0){
						dId=1;
					}
					if(y==1){
						dId=2;
					}
					if(y==2){
						dId=3;
					}
					Statement st2=(Statement) conn.createStatement();
					String insertQuery="INSERT INTO `teacherassessmentquestions` (`teacherId`, `teacherAssessmentId`, `teacherSectionId`, `domainId`, `competencyId`, `objectiveId`, `question`, `questionTypeId`, `questionWeightage`, `maxMarks`, `questionInstruction`, `validationType`, `fieldLength`, `isMandatory`, `isAttempted`, `questionPosition`, `createdDateTime`) VALUES"+
					"( "+startId+",1, 1, "+dId+", 7, 69, 'Complete the following sequence: 120,143,____,195,224', 1, 5, 5, 'Select any one option.\n\n(@#$%%^^)', NULL, NULL, 0, 1, 0, '2013-01-28 06:48:13')";
					st2.executeUpdate(insertQuery);
					
					ResultSet rs=st2.executeQuery("select max(teacherAssessmentQuestionId) from teacherassessmentquestions");
					rs.next();
					if(y==0){
						v1=rs.getString(1);
					}
					if(y==1){
						v2=rs.getString(1);
					}
					if(y==2){
						v3=rs.getString(1);
					}
				}
				
				for(int j=1;j<100;j++){
					int u=1;
					String qId=v1;
					Statement st1=(Statement) conn.createStatement();
					 Random r = new Random();
					   u =random();
					   if(u==0){
						   qId=v1; 
					   }
					   if(j%2==0){
						   u =random();
						   qId=v2; 
					   }
					   if(j%3==0){
						   u =random();
						   qId=v3;   
					   }
					
					System.out.println("qId:::"+qId +" u:"+u);
					System.out.println("V1::"+v1 +" V2::"+v2+" V3::"+v3);
					
					String insertQuery="INSERT INTO `teacheranswerdetail` ( `teacherId`, `questionWeightage`, `questionTypeId`, `teacherAssessmentQuestionId`, `assessmentId`, `teacherAssessmentId`, `jobId`, `selectedOptions`, `insertedRanks`, `insertedText`, `optionScore`, `totalScore`, `maxMarks`, `createdDateTime`) VALUES"+
					"( "+startId+", 0, 6, "+qId+", 110, 234, NULL, '33587', NULL, NULL, '3.0', "+u+", 1, '2013-04-01 03:02:49');";
					st1.executeUpdate(insertQuery);
				}
				
				
				String insertQuery2="INSERT INTO `teacherassessmentstatus` ( `teacherId`, `teacherAssessmentId`, `assessmentId`, `assessmentType`, `jobId`, `statusId`, `updatedBy`, `updatedByEntity`, `updatedDate`, `createdDateTime`) VALUES"+
				"("+startId+", 1, 110, 1, NULL, 4, NULL, NULL, NULL, '2013-01-28 06:48:13');";
				st.executeUpdate(insertQuery2);
				
				
				String insertQuery3="INSERT INTO `jobforteacher` (`teacherId`, `jobId`, `redirectedFromURL`, `status`, `coverLetter`, `updatedBy`, `updatedByEntity`, `updatedDate`, `createdDateTime`) VALUES"+
				"( "+startId+", 175, NULL, 3, 'dfgdfgdf', NULL, NULL, NULL, '2013-04-19 15:58:22');";
				st.executeUpdate(insertQuery3);
				startId++;
				System.out.println("ID:::::::::::"+startId);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("page",false);
		map.addAttribute("message","SuccessFully Inserted City , State and Zip Code ");
		return "imp";
	}
	
	
	
	
	@RequestMapping(value="/impCity.do", method=RequestMethod.GET)
	public String impCity(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== impCity.do ===============");
			String startIdQuery="select min(teacherId) from teacherdetail where authenticationCode=1111 order by teacherId limit 1";
			Connection conn = getConnection();
			Statement stmt=(Statement) conn.createStatement();
			ResultSet rsStateId=stmt.executeQuery(startIdQuery);
			rsStateId.next();
			String startId=rsStateId.getString(1);
			System.out.println("startId::::"+startId);
			if(startId!=null){
				
				ResultSet rs=stmt.executeQuery("select * from city order by id");
				int teacherId=Integer.parseInt(startId);
				while(rs.next()){
					Statement st=(Statement) conn.createStatement();
					String updateQuery="UPDATE  `teachermatch`.`city` SET  `teacherId` =  '"+teacherId+"' WHERE  `id` ="+rs.getString(1);
					st.executeUpdate(updateQuery);
					teacherId++;
					//System.out.println("updateQuery City:::"+updateQuery);
				}
			    System.out.println("City and State Inserted SuccessFully.");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("page",false);
		map.addAttribute("message","SuccessFully Inserted City and State ");
		return "imp";
	}
	@RequestMapping(value="/impInfo.do", method=RequestMethod.GET)
	public String impInfo(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== impInfo.do ===============");
			Connection conn = getConnection();
			Statement stmt=(Statement) conn.createStatement();
				ResultSet rs=stmt.executeQuery("select * from city order by id");
				while(rs.next()){
					Statement st=(Statement) conn.createStatement();
					String insertQuery="INSERT INTO `teacherpersonalinfo` (`teacherId`, `salutation`, `firstName`, `lastName`, `raceId`, `genderId`, `addressLine1`, `addressLine2`, `zipCode`, `stateId`, `cityId`, `phoneNumber`, `mobileNumber`, `isDone`, `createdDateTime`) VALUES"+
					"("+rs.getString("teacherId")+", 0, '"+rs.getString("firstName").replaceAll("'","''")+"', '"+rs.getString("lastName").replaceAll("'","''")+"', NULL, NULL, '"+rs.getString("addressLine1").replaceAll("'","''")+"', '"+rs.getString("addressLine2").replaceAll("'","''")+"', '"+rs.getString("zipCode")+"', "+rs.getString("stateId")+", "+rs.getString("cityId")+", '', '', 1, '2013-03-14 07:19:20')";
					st.executeUpdate(insertQuery);
					//System.out.println("insertQuery:::"+insertQuery);
				}
			    System.out.println("Teacherpersonalinfo Inserted SuccessFully.");

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("page",false);
		map.addAttribute("message","SuccessFully Inserted TeacherPersonalinfo");
		return "imp";
	}
	@RequestMapping(value="/impExp.do", method=RequestMethod.GET)
	public String impExp(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== impExp.do ===============");
			Connection conn = getConnection();
			Statement stmt=(Statement) conn.createStatement();
			ResultSet rs=stmt.executeQuery("select * from teacherdetail where authenticationCode=1111 order by teacherId");
			while(rs.next()){ 
				Statement st=(Statement) conn.createStatement();
				String insertQuery="INSERT INTO `teacherexperience` (`teacherId`, `expCertTeacherTraining`, `nationalBoardCert`, `nationalBoardCertYear`, `resume`, `createdDateTime`) VALUES"+
				"( "+rs.getString(1)+", 0, 0, NULL, 'Resume.pdf', '2013-04-11 14:29:41')";
				st.executeUpdate(insertQuery);
				//System.out.println("insertQuery:::"+insertQuery);
			}
		    System.out.println("Insert SuccessFully.");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("page",false);
		map.addAttribute("message","SuccessFully Inserted TeacherExperience ");
		return "imp";
	}
	@RequestMapping(value="/impPStatus.do", method=RequestMethod.GET)
	public String impPStatus(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== impPStatus.do ===============");
			Connection conn = getConnection();
			Statement stmt=(Statement) conn.createStatement();
			ResultSet rs=stmt.executeQuery("select * from teacherdetail where authenticationCode=1111 order by teacherId");
			while(rs.next()){ 
				Statement st=(Statement) conn.createStatement();
				String insertQuery="INSERT INTO `teacherportfoliostatus` ( `teacherId`, `isPersonalInfoCompleted`, `isAcademicsCompleted`, `isCertificationsCompleted`, `isExperiencesCompleted`, `isAffidavitCompleted`) VALUES"+
				"( "+rs.getString(1)+",1, 1, 1, 1, 1)";
				st.executeUpdate(insertQuery);
				//System.out.println("insertQuery:::"+insertQuery);
			}
		    System.out.println("Insert SuccessFully.");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("page",false);
		map.addAttribute("message","SuccessFully Inserted TeacherPortfolioStatus ");
		return "imp";
	}
	@RequestMapping(value="/impAff.do", method=RequestMethod.GET)
	public String impAff(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== impAff.do ===============");
			Connection conn = getConnection();
			Statement stmt=(Statement) conn.createStatement();
			ResultSet rs=stmt.executeQuery("select * from teacherdetail where authenticationCode=1111 order by teacherId");
			while(rs.next()){ 
				Statement st=(Statement) conn.createStatement();
				String insertQuery="INSERT INTO `teacheraffidavit` (`teacherId`, `affidavitAccepted`, `isDone`, `CreatedDateTime`) VALUES"+
				"( "+rs.getString(1)+",1, 1, '2013-01-28 18:18:04')";
				st.executeUpdate(insertQuery);
				//System.out.println("insertQuery:::"+insertQuery);
			}
		    System.out.println("Insert SuccessFully.");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("page",false);
		map.addAttribute("message","SuccessFully Inserted TeacherPortfolioStatus ");
		return "imp";
	}
	*/
	
	/*============ Get Method of ImpController ====================*/
	/*@RequestMapping(value="/dkey.do", method=RequestMethod.GET)
	public String dkeyGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
		System.out.println("\n =========== Dkey.do ===============");
		Connection conn = getConnection();
		Statement stmt=(Statement) conn.createStatement();
		ResultSet rs=stmt.executeQuery("select * from districtmaster");
		Statement st=(Statement) conn.createStatement();
		int i=0;
			while(rs.next()){ 
				String authorizationkey=(Utility.randomString(8)+Utility.getDateTime());
				
				String key=Utility.encodeInBase64(authorizationkey);
				String updateQuery="UPDATE  districtmaster  SET  authKey ='"+key+"' WHERE districtId = "+rs.getString("districtId");
				st.executeUpdate(updateQuery);
				i++;
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "imp";
	}
	
	@RequestMapping(value="/skey.do", method=RequestMethod.GET)
	public String skey(ModelMap map,HttpServletRequest request)
	{
		try 
		{
		System.out.println("\n =========== skey.do ===============");
		int temp=0;
		int i=0;
		for(int j=30000;j<150001;j+=30000){
			Connection conn = getConnection();
			Statement stmt=(Statement) conn.createStatement();
			System.out.println(temp+"::"+j);
			ResultSet rs=stmt.executeQuery("select * from schoolmaster where schoolId>"+temp+" and  schoolId<="+j);
			temp=j;
			Statement st=(Statement) conn.createStatement();
			
				while(rs.next()){ 
					String authorizationkey=(Utility.randomString(8)+Utility.getDateTime());
					String key=Utility.encodeInBase64(authorizationkey);
					String updateQuery="UPDATE  schoolmaster  SET  authKey ='"+key+"' WHERE schoolId = "+rs.getString("schoolId");
					st.executeUpdate(updateQuery);
					System.out.println(i);
					i++;
				}
			} 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "imp";
	}
	
	
	public Connection getConnection(){
		Connection conn=null;
		try{
			Class.forName("com.mysql.jdbc.Driver");

			//local
			conn =  (Connection) DriverManager.getConnection("jdbc:mysql://localhost/teachermatch","root","");
			
			//Dev & cloud
		 //	conn =  (Connection) DriverManager.getConnection("jdbc:mysql://localhost/teachermatch","root","summer2006");
			
			//Live
		//	conn =  (Connection) DriverManager.getConnection("jdbc:mysql://localhost/teachermatch","root","itHUHCv9Ue58V96fdbud");
			
			System.out.println("Connection SuccessFully.");
		} catch(ClassNotFoundException ce){ System.out.println("Class Not Found : "+ce);} catch(SQLException sqle){ System.out.println("SQL Exception Caught : "+sqle);}
		return conn;
	}*/
	
	/*@RequestMapping(value="/updateTeacherToWithdraw.do", method=RequestMethod.GET)
	public String updateTeacherToWithdraw(ModelMap map,HttpServletRequest request, HttpServletResponse response){
		
		SessionFactory sessionFactory =  secondaryStatusDAO.getSessionFactory();
		StatelessSession statelessSession1 = sessionFactory.openStatelessSession();
		Transaction transaction = statelessSession1.beginTransaction();
		String sql = "";
		sql = "SELECT t.teacherId, t.createdDateTime, t.noOfReminderSent, tm.MaxDate, jt.districtId,jt.jobForTeacherId, jt.status FROM messagetoteacher t INNER JOIN jobforteacher jt ON t.teacherId = jt.teacherId INNER JOIN ( SELECT teacherId, MAX( loginTime ) AS MaxDate FROM teacherloginhistory GROUP BY teacherId )tm ON t.teacherId = tm.teacherId AND t.createdDateTime > tm.MaxDate AND t.noOfReminderSent >=1 AND jt.districtId =1200390 AND jt.status !=6 AND jt.status !=10";
		System.out.println(sql);
		try {
				PreparedStatement  pstmt=null;
				CGReportService crs = new CGReportService();
				Connection con = crs.getConnection();
				Session session = sessionFactory.openSession();
				Query query = session.createSQLQuery(sql);
				List<Object[]> rows = query.list();
				for(Object[] object : rows){
					//System.out.println(":::::::::::::::::::::object::"+object[5]);
					pstmt = con.prepareStatement("update jobforteacher set status=7 where jobForTeacherId="+object[5]+"");
					pstmt.executeUpdate();
					//System.out.println("update jobforteacher set status=7 where jobForTeacherId="+object[5]+"");
				}
			
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/updateTeacherToWithdrawSecond.do", method=RequestMethod.GET)
	public String updateTeacherToWithdrawSecond(ModelMap map,HttpServletRequest request, HttpServletResponse response){
		
		SessionFactory sessionFactory =  secondaryStatusDAO.getSessionFactory();
		StatelessSession statelessSession1 = sessionFactory.openStatelessSession();
		Transaction transaction = statelessSession1.beginTransaction();
		String sql = "";
		sql = "SELECT m.teacherId,q.districtId,jt.jobforteacherId,jt.status FROM messagetoteacher m,districtspecificportfolioanswers q,teachercertificate c,teacherpersonalinfo t, jobforteacher jt WHERE m.noOfReminderSent >= 1 AND m.teacherId=q.teacherId AND m.teacherId=c.teacherId AND m.teacherId=t.teacherId AND (t.phoneNumber IS NULL) AND q.districtId = 1200390 AND t.teacherId=jt.teacherId AND jt.districtId=1200390 AND jt.status!=6 AND jt.status!=10 GROUP BY m.teacherId";
		System.out.println(":::::::::::::::"+sql);
		try {
				PreparedStatement  pstmt=null;
				CGReportService crs = new CGReportService();
				Connection con = crs.getConnection();
				Session session = sessionFactory.openSession();
				Query query = session.createSQLQuery(sql);
				List<Object[]> rows = query.list();
				for(Object[] object : rows){
					//System.out.println(":::::::::::::::::::::object::"+object[2]);
					pstmt = con.prepareStatement("update jobforteacher set status=7 where jobForTeacherId="+object[2]+"");
					pstmt.executeUpdate();
				}
			
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}*/
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/service/candidatehire.do", method=RequestMethod.GET)
	public String hireCandidate(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		StringBuffer msgAll=new StringBuffer();
		try {
			String requisitionNumber=request.getParameter("reqNo");
			String teacherId=request.getParameter("teacherId");
			String jobId=request.getParameter("jobId");
			String districtId=request.getParameter("districtId");
			String password=request.getParameter("password");
			String location=request.getParameter("locationCode");
			
			JobOrder jobOrder=null;
			if(jobId!=null){
				jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId),false,false);
			}
			
			DistrictMaster districtMaster =null;
			if(districtId!=null){
				districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId),false,false);
			}
			SchoolMaster schoolMaster =null;
			if(location!=null){
				Criterion locCr = Restrictions.eq("locationCode", location);
				Criterion disCr = Restrictions.eq("districtId", districtMaster);
				schoolMaster=schoolMasterDAO.findByCriteria(locCr,disCr).get(0);
				//schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId),false,false);
			}
			TeacherDetail teacherDetail=null;
			if(teacherId!=null){
				teacherDetail=teacherDetailDAO.findById(Integer.parseInt(teacherId),false,false);
			}
			msgAll.append("<table border=1 cellpadding=5 cellspacing=5>");
			
			if(requisitionNumber!=null){
				List<DistrictRequisitionNumbers> drnList=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(districtMaster, requisitionNumber);
				msgAll.append("<tr bgcolor=yellow><td  colspan=3 align=left><font size=2 color=green>DistrictRequisitionNumbers</font></td><td  colspan=7 align=center><font size=2 color=green>DistrictRequisitionNumbers</font></td><td  colspan=3 align=right><font size=2 color=green>DistrictRequisitionNumbers</font></td></tr>");
				msgAll.append("<tr><td>District Requisition Id</td><td>Requisition Number</td><td>JobCode</td><td>JobTitle</td><td>PosType</td><td>IsUsed</td><td>DeleteFlag</td><td>UploadFrom()</td><td>UserId</td><td  colspan=4>CreatedDateTime</td></tr>");
				
				if(drnList.size()>0)
				for (DistrictRequisitionNumbers districtRequisitionNumbers : drnList) {
					try{
						msgAll.append("<tr><td>"+districtRequisitionNumbers.getDistrictRequisitionId()+"</td><td>"+districtRequisitionNumbers.getRequisitionNumber()+"</td><td>"+districtRequisitionNumbers.getJobCode()+"</td><td>"+districtRequisitionNumbers.getJobTitle()+"</td><td>"+districtRequisitionNumbers.getPosType()+"</td><td>"+districtRequisitionNumbers.getIsUsed()+"</td><td>"+districtRequisitionNumbers.getDeleteFlag()+"</td><td>"+districtRequisitionNumbers.getUploadFrom()+"</td><td>"+districtRequisitionNumbers.getUserMaster().getUserId()+"</td><td  colspan=4>"+districtRequisitionNumbers.getCreatedDateTime()+"</td></tr>");
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				msgAll.append("<tr><td  colspan=13 align=left><font size=2 color=red>"+drnList.size()+" Records Found&nbsp;</font></font></td></tr>");
			}
			if(requisitionNumber!=null){
				msgAll.append("<tr bgcolor=yellow><td  colspan=3 align=left><font size=2 color=green>JobRequisitionNumbers</font></td><td  colspan=7 align=center><font size=2 color=green>JobRequisitionNumbers</font></td><td  colspan=3 align=right><font size=2 color=green>JobRequisitionNumbers</font></td></tr>");
				List<JobRequisitionNumbers> reqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByDistrictOrJob(jobOrder,districtMaster, requisitionNumber);
				msgAll.append("<tr><td>JobRequisitionId</td><td colspan=4>JobId  ( IsExpHireNotEqualToReqNo, StartDate, EndDate, JobTitle,JobCategoryId ) </td><td>District Requisition Id</td><td>schoolId ( schoolName )</td><td colspan=6>Status</td></tr>");
				if(reqList.size()>0)
				for (JobRequisitionNumbers jobRequisitionNumbers : reqList) {
					try{
					String schoolIdReq="",schoolName="", multiHired="0";
					if(jobRequisitionNumbers.getSchoolMaster()!=null){
						schoolIdReq=jobRequisitionNumbers.getSchoolMaster().getSchoolId()+"";
						schoolName=jobRequisitionNumbers.getSchoolMaster().getSchoolName();
					}
					if(jobRequisitionNumbers.getJobOrder().getIsExpHireNotEqualToReqNo()!=null && jobRequisitionNumbers.getJobOrder().getIsExpHireNotEqualToReqNo()){
						multiHired="1";
					}
					msgAll.append("<tr><td>"+jobRequisitionNumbers.getJobRequisitionId()+"</td><td colspan=4>"+jobRequisitionNumbers.getJobOrder().getJobId()+" ( "+multiHired+", "+jobRequisitionNumbers.getJobOrder().getJobStartDate()+" , "+jobRequisitionNumbers.getJobOrder().getJobEndDate()+", "+jobRequisitionNumbers.getJobOrder().getJobTitle()+", "+jobRequisitionNumbers.getJobOrder().getJobCategoryMaster().getJobCategoryId()+" )</td><td>"+jobRequisitionNumbers.getDistrictRequisitionNumbers().getDistrictRequisitionId()+"</td><td nowrap>"+schoolIdReq+" ( "+schoolName+" )"+"</td><td colspan=6>"+jobRequisitionNumbers.getStatus()+"</td></tr>");
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				msgAll.append("<tr><td  colspan=13 align=left><font size=2 color=red>"+reqList.size()+" Records Found&nbsp;</font></td></tr>");
			}
			msgAll.append("</table>");
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	@RequestMapping(value="/resetspstatus.do", method=RequestMethod.GET)
	public @ResponseBody String fixedSPStatus(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		String result = "";
		try{
			String teacherID = request.getParameter("teacherId");
			System.out.println(" teacherId ::: "+teacherID);
			TeacherDetail teacherDetail = null;
			List<TeacherDetail> teacherDetailList = null;
			String[] teacherArray = null;
			int singleMultiIds = 0;
			if(teacherID!=null && teacherID.trim().length()>0){
				if(teacherID.contains(",")){
					teacherArray = teacherID.split(",");
					singleMultiIds = 1;
				}
				else{
					singleMultiIds = 2;
				}
			}else{
				result= "Please provide teacherId(s).";
			}
			
			if(singleMultiIds==2){
				Integer teachId =0;
				try {
						teachId = Integer.parseInt(teacherID);
				} catch (Exception e) {
					result= "Please provide valid teacherId........";
				}
				if(result.length()==0){
					teacherDetail = teacherDetailDAO.findById(teachId, false, false);
					if(teacherDetail==null)
						result= "Please provide valid teacherId.";
					else{
						try{
							commonService.autoStatusForSPJobs(null, teacherDetail, null);
							result= "Reset sp status successfully.";
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
			}
			else if(singleMultiIds==1){
				if(teacherArray!=null && teacherArray.length>0){
					int temp = 0;
					for(String str : teacherArray){
						try {
							temp = Integer.parseInt(str);
							TeacherDetail teachDetail = new TeacherDetail();
							teachDetail.setTeacherId(temp);
							teacherDetailList.add(teachDetail);
						} catch (Exception e) {
						}
					}
				}
				
				if(teacherDetailList!=null && teacherDetailList.size()>0){
					for(TeacherDetail t : teacherDetailList){
						try{
							System.out.println(" teacherId : "+t.getTeacherId());
							commonService.autoStatusForSPJobs(null, t, null);
							result= "Reset sp status successfully.";
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
			}
			/*else{
				result= "Please provide valid teacherId.";
			}*/
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		System.out.println(" result ::::::: "+result);
		return result;
	}
	
	@RequestMapping(value="/impstatus.do", method=RequestMethod.GET)
	public String impstatusGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println("\n =========== impstatus.do Controller |===============");
		try 
		{
			String jobId=request.getParameter("jobId");
			String password=request.getParameter("password");
			System.out.println("jobId::::::::::::::::::::"+jobId +" password::::::::::::::::::::: "+password);
			if(password!=null && password.equals("sekhar") && jobId!=null && !jobId.equals("")){
				JobOrder jobOrder=null;
				if(jobId!=null){
					jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId),false,false);
				}
				if(jobOrder.getHeadQuarterMaster()!=null){
					List<JobOrder> jobOrders=new ArrayList<JobOrder>();
					jobOrders.add(jobOrder);
					List<TeacherDetail> teacherDetails=jobForTeacherDAO.getJobForTeacherByStatus(jobOrders);
					System.out.println("1::=::::::::::: teacherDetails:::::::::::::::"+teacherDetails.size());
					List<TeacherDetail> teacherDetailFinalList=new ArrayList<TeacherDetail>();
					if(teacherDetails.size()>0){
						List<MQEvent> mqEvents=new ArrayList<MQEvent>();
						Criterion criterion1 = Restrictions.in("teacherdetail",teacherDetails);
						Criterion criterion2 = Restrictions.eq("eventType","Talent Type");
						Criterion criterion3 = Restrictions.eq("status","C");
						Criterion criterionwork = Restrictions.eq("workFlowStatus","HIRED");
						List<WorkFlowStatus> workFlowStatus=workFlowStatusDAO.findByCriteria(criterionwork);
						System.out.println("1.1:::::::::::::::workFlowStatus::::::::::::::::::::::"+workFlowStatus.size());
						Criterion criterion4 = Restrictions.eq("workFlowStatusId",workFlowStatus.get(0));
						mqEvents=mqEventDAO.findByCriteria(Order.asc("eventId"),criterion1,criterion2,criterion3,criterion4);
						System.out.println("2:::::::::::: mqEvents::::::::::::::::::::::::::"+mqEvents.size());
						if(mqEvents.size()>0){
							for (MQEvent mqEvent : mqEvents) {
								teacherDetailFinalList.add(mqEvent.getTeacherdetail());
							}
						}
					}
					System.out.println("3::::::::::teacherDetailFinalList::::::::::::::::"+teacherDetailFinalList.size());
					
					Criterion criterionf1 = Restrictions.eq("jobId",jobOrder);
					Criterion criterionf2 = Restrictions.in("teacherId",teacherDetailFinalList);
					Criterion criterionf3 = Restrictions.eq("applicationStatus",6);
					List<JobForTeacher> lstJobForTeacheFinal=jobForTeacherDAO.findByCriteria(criterionf1,criterionf2,criterionf3);
					System.out.println("4::::::::::lstJobForTeacheFinal:::::::::::"+lstJobForTeacheFinal.size());
					
					
					SessionFactory sessionFactory=jobForTeacherDAO.getSessionFactory();
			   		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			   	    Transaction txOpen = statelesSsession.beginTransaction();
					if(lstJobForTeacheFinal.size()>0){
						for (JobForTeacher jobForTeacher : lstJobForTeacheFinal) {
							System.out.println("5:::::::::::::::::::::Update jobForTeacher::::::::::::::::::::::::::"+jobForTeacher.getTeacherId().getTeacherId());
							StatusMaster statusMasterHired=WorkThreadServlet.statusMap.get("hird");
							jobForTeacher.setStatus(statusMasterHired);
							jobForTeacher.setStatusMaster(statusMasterHired);
		                	jobForTeacher.setLastActivity("Hired");
		                	statelesSsession.update(jobForTeacher);
						}
					}
					
					List<TeacherStatusHistoryForJob> historyForJobs=new ArrayList<TeacherStatusHistoryForJob>();
					if(teacherDetailFinalList.size()>0){
						Criterion criterionh1 = Restrictions.in("teacherDetail",teacherDetailFinalList);
						Criterion criterionh2 = Restrictions.eq("status", "I");
						Criterion criterionh3 = Restrictions.eq("jobOrder",jobOrder);
						StatusMaster statusMasterHired=WorkThreadServlet.statusMap.get("hird");
						Criterion criterionh4 = Restrictions.eq("statusMaster", statusMasterHired);
						historyForJobs=teacherStatusHistoryForJobDAO.findByCriteria(criterionh1, criterionh2,criterionh3,criterionh4);
						
						System.out.println("6:::::::::::::::historyForJobs::::::::::::::::::::::"+historyForJobs.size());
						
						
						if(historyForJobs.size()>0){
							UserMaster userMaster=null;
							Map<Integer,Boolean> IsUpdateStatus = new HashMap<Integer,Boolean >();
							for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobs) {
								TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
								tSHJ=teacherStatusHistoryForJob;
								userMaster=teacherStatusHistoryForJob.getUserMaster();
								if(tSHJ!=null && IsUpdateStatus.get(tSHJ.getTeacherDetail().getTeacherId())==null){
									System.out.println("7:::::::::::::::::::::Update teacherStatusHistoryForJob::::::::::::::::::::::::::"+tSHJ.getTeacherDetail().getTeacherId());
									tSHJ.setStatus("A");
									tSHJ.setUpdatedDateTime(new Date());
									tSHJ.setUserMaster(userMaster);
									statelesSsession.update(tSHJ);
									IsUpdateStatus.put(tSHJ.getTeacherDetail().getTeacherId(),true);
								}
							}
						}
					}
					txOpen.commit();
					statelesSsession.close();
				}
				System.out.println("::::::::::::::::::Successfully Done:::::::::::::::::::::::");
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "imp";
	}
}
