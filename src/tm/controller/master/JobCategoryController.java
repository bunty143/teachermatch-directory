package tm.controller.master;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.DistrictPanelMaster;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictPanelMasterDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.Utility;


@Controller
public class JobCategoryController 
{
	String locale = Utility.getValueOfPropByKey("locale");
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	public void setHeadQuarterMasterDAO(HeadQuarterMasterDAO headQuarterMasterDAO) {
		this.headQuarterMasterDAO = headQuarterMasterDAO;
	}
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	
	public void setRoleAccessPermissionDAO(
			RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	public void setBranchMasterDAO(BranchMasterDAO branchMasterDAO) {
		this.branchMasterDAO = branchMasterDAO;
	}
	
	@Autowired
	private DistrictPanelMasterDAO districtPanelMasterDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	/*============ Get Method of TeacherController ====================*/
	@RequestMapping(value="/jobcategory.do", method=RequestMethod.GET)
	public String jobCategoryGET(ModelMap map,HttpServletRequest request)
	{
		UserMaster userSession=null;
		try 
		{
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			List<JobCategoryMaster> jobCategoryMasterlst =  new ArrayList<JobCategoryMaster>();
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("entityID", entityID);
			if(userSession.getEntityType()!=1){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(userSession.getDistrictId());
			}else{
				map.addAttribute("districtName",null);
			}
			
			if(userSession.getEntityType()==2){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
			}
			
			if(userSession.getEntityType()==3){
				map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
				map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
			}else{
				map.addAttribute("schoolName",null);
			}
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("lstJobCategoryMasters", jobCategoryMasterlst);
			if(userSession.getDistrictId()!=null && userSession.getDistrictId().getDistrictId()!=null)
			{
				DistrictMaster districtMaster = districtMasterDAO.findById(userSession.getDistrictId().getDistrictId(), false, false);
				map.addAttribute("districtMaster", districtMaster);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		if(userSession.getEntityType()==3){
			return "signin";
		}else{
			return "jobcategory";

		}	
	}
	
	/*============ Get Method of TeacherController Test====================*/
	@RequestMapping(value="/jobcategorynew.do", method=RequestMethod.GET)
	public String jobCategoryNewGET(ModelMap map,HttpServletRequest request)
	{
		UserMaster userSession=null;
		try 
		{
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			userSession		=	(UserMaster) session.getAttribute("userMaster");
			session.setAttribute("displayType", "0");
			map.addAttribute("userSession", userSession);
			List<JobCategoryMaster> jobCategoryMasterlst =  new ArrayList<JobCategoryMaster>();
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("entityID", entityID);
			if(userSession.getEntityType()!=1){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(userSession.getDistrictId());
			}else{
				map.addAttribute("districtName",null);
			}
			
			if(userSession.getEntityType()==2){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
			}
			
			if(userSession.getEntityType()==3){
				map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
				map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
			}else{
				map.addAttribute("schoolName",null);
			}
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("lstJobCategoryMasters", jobCategoryMasterlst);
			if(userSession.getDistrictId()!=null && userSession.getDistrictId().getDistrictId()!=null)
			{
				DistrictMaster districtMaster = districtMasterDAO.findById(userSession.getDistrictId().getDistrictId(), false, false);
				map.addAttribute("districtMaster", districtMaster);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		if(userSession.getEntityType()==3){
			return "signin";
		}else{
			return "jobcategorynew";

		}	
	}
	//********************************* Adding By Deepak *************************************
	public synchronized static void jobCategoryCalculator(ModelMap map,HttpServletRequest request, JobCategoryMasterDAO jobCategoryMasterDAO, UserMasterDAO userMasterDAO, DistrictMasterDAO districtMasterDAO)
	{
		HttpSession session = request.getSession(false);
		UserMaster userSession=null;
		/*if (session == null || session.getAttribute("userMaster") == null){
			return "redirect:index.jsp";
		}
*/
		userSession		=	(UserMaster) session.getAttribute("userMaster");
		map.addAttribute("userSession", userSession);
		List<JobCategoryMaster> jobCategoryMasterlst =  new ArrayList<JobCategoryMaster>();
		int entityID=0,roleId=0;
		if(userSession!=null){
			entityID=userSession.getEntityType();
			if(userSession.getRoleId().getRoleId()!=null){
				roleId=userSession.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		map.addAttribute("entityID", entityID);
		if(userSession.getEntityType()!=1){
			map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
			map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
			jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(userSession.getDistrictId());
		}else{
			map.addAttribute("districtName",null);
		}
		
		if(userSession.getEntityType()==2){
			map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
			map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
			map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
		}
		
		if(userSession.getEntityType()==3){
			map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
			map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
			map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
		}else{
			map.addAttribute("schoolName",null);
		}
		map.addAttribute("roleAccess", roleAccess);
		map.addAttribute("lstJobCategoryMasters", jobCategoryMasterlst);
		
		if(userSession.getDistrictId()!=null && userSession.getDistrictId().getDistrictId()!=null)
		{
			DistrictMaster districtMaster = districtMasterDAO.findById(userSession.getDistrictId().getDistrictId(), false, false);
			map.addAttribute("districtMaster", districtMaster);
		}
	}
	
	
	
	/*============End Of Get Method of Domain Controller ====================*/
	
	
	/* ===== Gagan [ Start ] : Functionality for Panel and their member (Add, edit and delete ) ======== */
	@RequestMapping(value="/panel.do", method=RequestMethod.GET)
	public String doPanelGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== panel.do in Panel Controller  ===============");
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			UserMaster userMaster		=	(UserMaster) session.getAttribute("userMaster");
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			map.addAttribute("userSession", userMaster);
			if(userMaster.getEntityType()!=1){
				districtMaster=userMaster.getDistrictId();
				map.addAttribute("DistrictName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userMaster.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictName",null);
			}
			if(userMaster.getEntityType()==3){
				schoolMaster=userMaster.getSchoolId();
				map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
			}else{
				map.addAttribute("SchoolName",null);
			}
			map.addAttribute("entityType",userMaster.getEntityType());
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "panel";
	}
	
	
	/* ===== Gagan [ Start ] : Functionality for Panel member (Add, edit and delete ) ======== */
	@RequestMapping(value="/panelmembers.do", method=RequestMethod.GET)
	public String doPanelmembersGET(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		try 
		{
			System.out.println("\n =========== panelmembers.do in Panel Controller  ===============");
			HttpSession session = request.getSession(false);
			boolean notAccesFlag=false;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			UserMaster userMaster		=	(UserMaster) session.getAttribute("userMaster");
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			DistrictPanelMaster districtPanelMaster	= new DistrictPanelMaster();
			List<UserMaster> lstuserMaster	=	new ArrayList<UserMaster>();
			
			if(userMaster!=null){
				districtMaster=userMaster.getDistrictId();
			}
			
			int panelId = request.getParameter("panelId")==null?0:Integer.parseInt(request.getParameter("panelId"));
			
			if(panelId!=0 || panelId>0)
			{
				districtPanelMaster	=districtPanelMasterDAO.findById(panelId, false, false);
				
				Criterion criterion1 = Restrictions.eq("districtId", districtPanelMaster.getDistrictMaster());
				Criterion criterion2 = Restrictions.eq("status", "A");
				lstuserMaster =	userMasterDAO.findByCriteria(Order.asc("firstName"), criterion1,criterion2);
				System.out.println("  lstuserMaster  size : "+lstuserMaster.size());
				map.addAttribute("districtPanelMaster", districtPanelMaster);
				map.addAttribute("lstuserMaster",lstuserMaster);
			
			    if(districtPanelMaster!=null && !userMaster.getEntityType().equals(1)){
					if(!districtPanelMaster.getDistrictMaster().getDistrictId().equals(districtMaster.getDistrictId())){
						notAccesFlag=true;
					}
				}
				if(notAccesFlag)
				{
					PrintWriter out = response.getWriter();
					response.setContentType("text/html"); 
					out.write("<script type='text/javascript'>");
					out.write("alert('You have no access to this panel.');");
					out.write("window.location.href='panel.do';");
					out.write("</script>");
					return null;
				}
			
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "panelmembers";
	}
	
	@RequestMapping(value="/hqjobcategory.do", method=RequestMethod.GET)
	public String hqjobCategoryGET(ModelMap map,HttpServletRequest request)
	{
		UserMaster userSession=null;
		try 
		{
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}

			userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			
			String  roleAccessU=null ;
			 
			if(userSession.getRoleId()!=null){
				roleId=userSession.getRoleId().getRoleId();
			}
			try{
				if(roleId!=1){
					roleAccessU=roleAccessPermissionDAO.getMenuOptionListByMenuId(roleId, 104,"hqjobcategory.do", 0,108);
				}
				else{
					roleAccessU=roleAccessPermissionDAO.getMenuOptionListByMenuId(1, 147, "hqjobcategory.do", 0, 154);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccessU", roleAccessU); 
			map.addAttribute("entityID", entityID);
			if(userSession.getEntityType()==2){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
			}
			
			if(userSession.getEntityType()==3){
				map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
				map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
			}else{
				map.addAttribute("schoolName",null);
			}
			if(userSession.getEntityType()==5){
				map.addAttribute("headQuarterName",userSession.getHeadQuarterMaster().getHeadQuarterName());
				map.addAttribute("headQuarterMaster",userSession.getHeadQuarterMaster().getHeadQuarterId());
				
				List<BranchMaster> branchMasterlst =  new ArrayList<BranchMaster>();
				Criterion criterion=Restrictions.eq("headQuarterMaster", userSession.getHeadQuarterMaster());
				branchMasterlst=branchMasterDAO.findByCriteria(criterion);
				map.addAttribute("branchSize",branchMasterlst.size());
				map.addAttribute("isBranchExist",userSession.getHeadQuarterMaster().getIsBranchExist());
				if(userSession.getHeadQuarterMaster().getAssessmentUploadURL()!=null)
					map.addAttribute("isHqJsi",userSession.getHeadQuarterMaster().getAssessmentUploadURL());
				else
					map.addAttribute("isHqJsi","");
				System.out.println("IsBranchExist() "+ userSession.getHeadQuarterMaster().getIsBranchExist());
				//System.out.println("branchMasterlst.size() "+branchMasterlst.size());
				
			}
			if(userSession.getEntityType()==6){
				map.addAttribute("branchName",userSession.getBranchMaster().getBranchName());
				map.addAttribute("branchId",userSession.getBranchMaster().getBranchId());
			}
			
			try{
				String kellyLiveFlag =Utility.getLocaleValuePropByKey("kellyupdatecodelive", locale);
				map.addAttribute("kellyLiveFlag",kellyLiveFlag);
			}catch(Exception e){e.printStackTrace();}
			
			
			 
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		if(userSession.getEntityType()==3){
			return "signin";
		}else{
			return "hqjobcategory";

		}	
	}
	
}
