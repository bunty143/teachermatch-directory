package tm.controller.master;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.user.UserMaster;
import tm.dao.master.TimeZoneMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.Utility;
@Controller
public class ScheduleController {
	
	@Autowired
	private TimeZoneMasterDAO timeZoneMasterDAO;
	public void setTimeZoneMasterDAO(TimeZoneMasterDAO timeZoneMasterDAO) {
		this.timeZoneMasterDAO = timeZoneMasterDAO;
	}
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	
	
	/*================  Rajendra : MySchedule Related Function Start Here =============          */
	@RequestMapping(value="/myschedule.do", method=RequestMethod.GET)
	public String doMySchduleGET(ModelMap map,HttpServletRequest request)
	{
		
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		StringBuffer sb 						=	new StringBuffer();
		try 
		{
			if(userSession.getEntityType()==2){
				map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictName",null);
			}
			if(userSession.getEntityType()==3){
				map.addAttribute("SchoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
			}else{
				map.addAttribute("SchoolName",null);
			}
			sb.append("</ul>");
			map.addAttribute("entityType",userSession.getEntityType());;
			map.addAttribute("tree",sb.toString());
			
			
			
		map.addAttribute("userMaster", userSession);
		
		Calendar fromDate = Calendar.getInstance();
		fromDate.setTime(new Date());
		Date fDate=fromDate.getTime();
		SimpleDateFormat sdf=new SimpleDateFormat("MM-dd-yyyy");
		map.addAttribute("fromDate",sdf.format(fDate));
		
		Calendar toDate = Calendar.getInstance();
		toDate.setTime(new Date());
		toDate.add(Calendar.MONTH,1);
		Date tDate=toDate.getTime();
		sdf=new SimpleDateFormat("MM-dd-yyyy"); 
		map.addAttribute("toDate", sdf.format(tDate));
		
		}catch (Exception e) {
			e.printStackTrace();
		}	
		System.out.println("user id :"+userSession.getUserId());
		System.out.println("inside TMUserController for myschedule");
		return "myschedule";
	}
}
