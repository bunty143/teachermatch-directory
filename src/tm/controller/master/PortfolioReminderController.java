package tm.controller.master;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.DistrictPortfolioConfig;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.PortfolioReminders;
import tm.bean.PortfolioRemindersDspq;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.master.ApplitrackDistricts;
import tm.bean.master.CertificationStatusMaster;
import tm.bean.master.CertificationTypeMaster;
import tm.bean.master.CityMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.EmpRoleTypeMaster;
import tm.bean.master.EthinicityMaster;
import tm.bean.master.EthnicOriginMaster;
import tm.bean.master.FieldMaster;
import tm.bean.master.GenderMaster;
import tm.bean.master.OrgTypeMaster;
import tm.bean.master.PeopleRangeMaster;
import tm.bean.master.RaceMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectAreaExamMaster;
import tm.bean.master.TFAAffiliateMaster;
import tm.bean.master.TFARegionMaster;
import tm.bean.user.UserMaster;
import tm.dao.ApplitrackDistrictsDAO;
import tm.dao.DistrictPortfolioConfigDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.PortfolioRemindersDAO;
import tm.dao.PortfolioRemindersDspqDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.master.CertificationStatusMasterDAO;
import tm.dao.master.CertificationTypeMasterDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.EmpRoleTypeMasterDAO;
import tm.dao.master.EthinicityMasterDAO;
import tm.dao.master.EthnicOriginMasterDAO;
import tm.dao.master.FieldMasterDAO;
import tm.dao.master.GenderMasterDAO;
import tm.dao.master.OrgTypeMasterDAO;
import tm.dao.master.PeopleRangeMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectAreaExamMasterDAO;
import tm.dao.master.TFAAffiliateMasterDAO;
import tm.dao.master.TFARegionMasterDAO;
import tm.services.MailText;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;
import tm.services.EmailerService;
import tm.services.MailText;
import java.util.HashMap;
import java.util.Map;

@Controller
public class PortfolioReminderController {
	
	
	@Autowired
	public TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	public TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	public CityMasterDAO cityMasterDAO;
	
	@Autowired
	public StateMasterDAO stateMasterDAO;
	
	@Autowired
	public JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	public GenderMasterDAO genderMasterDAO;
	
	@Autowired
	public RaceMasterDAO raceMasterDAO;
	
	@Autowired
	public EthinicityMasterDAO ethinicityMasterDAO;
	
	@Autowired
	public EthnicOriginMasterDAO ethnicOriginMasterDAO;
	
	@Autowired
	public CountryMasterDAO countryMasterDAO;
	
	@Autowired
	public TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	
	@Autowired
	public DistrictMasterDAO districtMasterDAO; 
	
	@Autowired
	public JobOrderDAO jobOrderDao; 
	
	@Autowired
	private ApplitrackDistrictsDAO applitrackDistrictsDAO;
	
	@Autowired
	private PortfolioRemindersDAO portfolioRemindersDAO;
	
	@Autowired
	private DistrictPortfolioConfigDAO districtPortfolioConfigDAO;
	
	@Autowired
	private CertificationTypeMasterDAO certificationTypeMasterDAO;
	
	@Autowired
	private OrgTypeMasterDAO orgTypeMasterDAO;
	
	@Autowired
	private PeopleRangeMasterDAO peopleRangeMasterDAO;
	
	@Autowired
	private CertificationStatusMasterDAO certificationStatusMasterDAO;
	public void setCertificationStatusMasterDAO(
			CertificationStatusMasterDAO certificationStatusMasterDAO) {
		this.certificationStatusMasterDAO = certificationStatusMasterDAO;
	}
	
	@Autowired
	private TFAAffiliateMasterDAO tfaAffiliateMasterDAO;
	public void setTfaAffiliateMasterDAO(TFAAffiliateMasterDAO tfaAffiliateMasterDAO) {
		this.tfaAffiliateMasterDAO = tfaAffiliateMasterDAO;
	}
	
	@Autowired
	private TFARegionMasterDAO tfaRegionMasterDAO;
	public void setTfaRegionMasterDAO(TFARegionMasterDAO tfaRegionMasterDAO) {
		this.tfaRegionMasterDAO = tfaRegionMasterDAO;
	}
	
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	public void setTeacherExperienceDAO(TeacherExperienceDAO teacherExperienceDAO) 
	{
		this.teacherExperienceDAO = teacherExperienceDAO;
	}
	
	@Autowired
	private FieldMasterDAO fieldMasterDAO;
	public void setFieldMasterDAO(FieldMasterDAO fieldMasterDAO) {
		this.fieldMasterDAO = fieldMasterDAO;
	}
	
	@Autowired
	private EmpRoleTypeMasterDAO empRoleTypeMasterDAO;
	public void setEmpRoleTypeMasterDAO(
			EmpRoleTypeMasterDAO empRoleTypeMasterDAO) {
		this.empRoleTypeMasterDAO = empRoleTypeMasterDAO;
	}
	
	@Autowired
	private SubjectAreaExamMasterDAO subjectAreaExamMasterDAO;
	
	@Autowired
	public PortfolioRemindersDspqDAO portfolioRemindersDspqDAO; 
	public void setPortfolioRemindersDspqDAO(PortfolioRemindersDspqDAO portfolioRemindersDspqDAO) 
	{
		this.portfolioRemindersDspqDAO = portfolioRemindersDspqDAO;
	}
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private MessageToTeacherDAO messageToTeacherDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;	
	
	/*******TPL-3973 start*************************/
	@RequestMapping(value="/candidatepoolrefresh.do", method=RequestMethod.GET)
	public String candidatePoolRefresh_GET(ModelMap map,HttpServletRequest request){
		String districtIdForEvents = request.getParameter("districtId");
		System.out.println("Entering in candidatePoolRefresh_GET .....  districtId:  "+districtIdForEvents);
		
		UserMaster userMaster			=	null;
		DistrictMaster distMaster 	= 	null;
		DistrictMaster districtMaster 	= 	null;
		Integer districtId 				=	null;
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("userMaster") == null){
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			distMaster = userMaster.getDistrictId();
			if(distMaster!=null)
			districtId = distMaster.getDistrictId();
		}		
		map.addAttribute("userMaster", userMaster);
		map.addAttribute("districtId", districtId);		
		districtMaster =	districtMasterDAO.findById(new Integer(districtId), false, false);		
		List<StatusMaster> statusMasterList	= null;
		List<StatusMaster> lstStatusMaster	=	null;		
		String[] statuss = {"hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
		String baseUrl=Utility.getBaseURL(request);			
		lstStatusMaster	=	statusMasterDAO.findStatusByStatusByShortNames(statuss);		
		statusMasterList	=new ArrayList<StatusMaster>();
		 List<StatusMaster> statusMasterList1	=new ArrayList<StatusMaster>();
		ArrayList<StatusMaster> statusMasterListRem	=new ArrayList<StatusMaster>();
		SecondaryStatus secondaryStatus=null;		
		List<SecondaryStatus> lstsecondaryStatus=  secondaryStatusDAO.findSecondaryStatusOnly(districtMaster);
		map.addAttribute("lstsecondaryStatus", lstsecondaryStatus);
		map.addAttribute("lstsecondaryStatus_PFC", lstsecondaryStatus);		
		if(districtMaster != null){
			map.addAttribute("reminderOfFirstFrequencyInDaysPortfolio", districtMaster.getReminderOfFirstFrequencyInDaysPortfolio());
			map.addAttribute("reminderFrequencyInDaysPortfolio", districtMaster.getReminderFrequencyInDaysPortfolio());			
		}		
		List<SecondaryStatus> lstSecStatus=	secondaryStatusDAO.findSecondaryStatusOnlyStatus(districtMaster);
		Map<Integer,SecondaryStatus> mapSStatus = new HashMap<Integer, SecondaryStatus>();
		if(lstSecStatus!=null)
		for(SecondaryStatus secStatus : lstSecStatus){
			mapSStatus.put(secStatus.getStatusMaster().getStatusId(),secStatus);
		}
		for(StatusMaster sm: lstStatusMaster){
			if(sm.getBanchmarkStatus()==1){
				secondaryStatus=mapSStatus.get(sm.getStatusId());
				if(secondaryStatus!=null){
					sm.setStatus(secondaryStatus.getSecondaryStatusName());
				}
			}
			if(baseUrl.contains("nccloud") || baseUrl.contains("nc.teachermatch")){				
				if(sm!=null && sm.getStatus()!=null && sm.getStatus().equals("Rejected"))
					statusMasterListRem.add(sm);
			}else
				statusMasterListRem.add(sm);
			statusMasterList.add(sm);
			if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==1200390 && (sm.getStatusId()==7 || sm.getStatusId()==10))
				statusMasterList1.add(sm);
		}
		try{
			String statusSendMailPortfolio = districtMaster.getStatusSendMailPortfolio();
			String secondaryStatusSendMailPortfolio = districtMaster.getSecondaryStatusSendMailPortfolio();
			if(statusSendMailPortfolio!=null && statusSendMailPortfolio!=""){
				String[] parts = statusSendMailPortfolio.split(",");
				List<String> statusList = new ArrayList<String>();
				for(String part : parts){
					statusList.add(part);
				}
				map.addAttribute("statusListPortfolio", statusList);
			}
			
			if(secondaryStatusSendMailPortfolio!=null && secondaryStatusSendMailPortfolio!=""){
				String[] parts = secondaryStatusSendMailPortfolio.split(",");
				List<String> secondaryStatusListReminder = new ArrayList<String>();
				for(String part : parts){
					secondaryStatusListReminder.add(part);
				}
				map.addAttribute("secondaryStatusListPortfolio", secondaryStatusListReminder);
			}			
		}catch(Exception exception){
			exception.printStackTrace();
		}
		String statusIdReminderExpireActionPortfolio = "";
		Integer noOfReminderPortfolio = null;
		try{
			if(districtMaster.getStatusIdReminderExpireActionPortfolio() != null){
				statusIdReminderExpireActionPortfolio	=	""+districtMaster.getStatusIdReminderExpireActionPortfolio();
			}			
			if(districtMaster.getNoOfReminderPortfolio()!=null){
				noOfReminderPortfolio = districtMaster.getNoOfReminderPortfolio();
			}			
			map.addAttribute("statusIdReminderExpireActionPortfolio", statusIdReminderExpireActionPortfolio);
			map.addAttribute("noOfReminderPortfolio", noOfReminderPortfolio);
			map.addAttribute("candidatePoolRefresh", districtMaster.getCandidatePoolRefresh());
			map.addAttribute("sendReminderForPortfolio", districtMaster.getSendReminderForPortfolio());
		} catch(Exception exception){
			exception.printStackTrace();
		}
		map.addAttribute("lstStatusMaster_PFC", statusMasterList);	
		map.addAttribute("lstStatusMaster_PFC1", statusMasterList1);	
		System.out.println("Leaving candidatePoolRefresh_GET ....."+"districtId="+districtId);
		return "candidatepoolrefresh";
	}
	/*******TPL-3973 end*************************/

	@RequestMapping(value="/portfolioreminder.do", method=RequestMethod.GET)
    public String onr(ModelMap map,HttpServletRequest request){
		System.out.println("Entering in onr get .....  districtId:  "+request.getParameter("districtId"));
		UserMaster userMaster			=	null;
		DistrictMaster districtMaster 	= 	null;
		Integer districtId 				=	null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}

			districtMaster = userMaster.getDistrictId();
			if(districtMaster!=null)
			districtId = districtMaster.getDistrictId();
		}
		
		map.addAttribute("userMaster", userMaster);
		map.addAttribute("districtId", districtId);
		
		/*******TPL-3973 Candidate Pool Refresh (Applicant Management) start*************************/		
		districtMaster =	districtMasterDAO.findById(new Integer(districtId), false, false);		
		List<StatusMaster> statusMasterList	= null;
		List<StatusMaster> lstStatusMaster	=	null;		
		String[] statuss = {"hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
		String baseUrl=Utility.getBaseURL(request);			
		lstStatusMaster	=	statusMasterDAO.findStatusByStatusByShortNames(statuss);		
		statusMasterList	=new ArrayList<StatusMaster>();
		 List<StatusMaster> statusMasterList1	=new ArrayList<StatusMaster>();
		ArrayList<StatusMaster> statusMasterListRem	=new ArrayList<StatusMaster>();
		SecondaryStatus secondaryStatus=null;		
		List<SecondaryStatus> lstsecondaryStatus=  secondaryStatusDAO.findSecondaryStatusOnly(districtMaster);
		map.addAttribute("lstsecondaryStatus", lstsecondaryStatus);
		map.addAttribute("lstsecondaryStatus_PFC", lstsecondaryStatus);		
		if(districtMaster != null){
			map.addAttribute("reminderOfFirstFrequencyInDaysPortfolio", districtMaster.getReminderOfFirstFrequencyInDaysPortfolio());
			map.addAttribute("reminderFrequencyInDaysPortfolio", districtMaster.getReminderFrequencyInDaysPortfolio());			
		}		
		List<SecondaryStatus> lstSecStatus=	secondaryStatusDAO.findSecondaryStatusOnlyStatus(districtMaster);
		Map<Integer,SecondaryStatus> mapSStatus = new HashMap<Integer, SecondaryStatus>();
		if(lstSecStatus!=null)
		for(SecondaryStatus secStatus : lstSecStatus){
			mapSStatus.put(secStatus.getStatusMaster().getStatusId(),secStatus);
		}
		for(StatusMaster sm: lstStatusMaster){
			if(sm.getBanchmarkStatus()==1){
				secondaryStatus=mapSStatus.get(sm.getStatusId());
				if(secondaryStatus!=null){
					sm.setStatus(secondaryStatus.getSecondaryStatusName());
				}
			}
			if(baseUrl.contains("nccloud") || baseUrl.contains("nc.teachermatch")){				
				if(sm!=null && sm.getStatus()!=null && sm.getStatus().equals("Rejected"))
					statusMasterListRem.add(sm);
			}else
				statusMasterListRem.add(sm);
			statusMasterList.add(sm);
			if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==1200390 && (sm.getStatusId()==7 || sm.getStatusId()==10))
				statusMasterList1.add(sm);
		}
		try{
			String statusSendMailPortfolio = districtMaster.getStatusSendMailPortfolio();
			String secondaryStatusSendMailPortfolio = districtMaster.getSecondaryStatusSendMailPortfolio();
			if(statusSendMailPortfolio!=null && statusSendMailPortfolio!=""){
				String[] parts = statusSendMailPortfolio.split(",");
				List<String> statusList = new ArrayList<String>();
				for(String part : parts){
					statusList.add(part);
				}
				map.addAttribute("statusListPortfolio", statusList);
			}
			
			if(secondaryStatusSendMailPortfolio!=null && secondaryStatusSendMailPortfolio!=""){
				String[] parts = secondaryStatusSendMailPortfolio.split(",");
				List<String> secondaryStatusListReminder = new ArrayList<String>();
				for(String part : parts){
					secondaryStatusListReminder.add(part);
				}
				map.addAttribute("secondaryStatusListPortfolio", secondaryStatusListReminder);
			}			
		}catch(Exception exception){
			exception.printStackTrace();
		}
		String statusIdReminderExpireActionPortfolio = "";
		Integer noOfReminderPortfolio = null;
		try{
			if(districtMaster.getStatusIdReminderExpireActionPortfolio() != null){
				statusIdReminderExpireActionPortfolio	=	""+districtMaster.getStatusIdReminderExpireActionPortfolio();
			}			
			if(districtMaster.getNoOfReminderPortfolio()!=null){
				noOfReminderPortfolio = districtMaster.getNoOfReminderPortfolio();
			}			
			map.addAttribute("statusIdReminderExpireActionPortfolio", statusIdReminderExpireActionPortfolio);
			map.addAttribute("noOfReminderPortfolio", noOfReminderPortfolio);
			map.addAttribute("candidatePoolRefresh", districtMaster.getCandidatePoolRefresh());
			map.addAttribute("sendReminderForPortfolio", districtMaster.getSendReminderForPortfolio());
		} catch(Exception exception){
			exception.printStackTrace();
		}
		map.addAttribute("lstStatusMaster_PFC", statusMasterList);	
		map.addAttribute("lstStatusMaster_PFC1", statusMasterList1);	
		System.out.println("Leaving onr _ get ....."+"districtId="+districtId);
		/*******TPL-3973 Candidate Pool Refresh (Applicant Management) ends*************************/
		
		return "portfolioreminder";
    }
	
	@RequestMapping(value="/personalinforeminder_test.do", method=RequestMethod.GET)
	public String doPersonalInfoGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}		
		
		List<StateMaster> listStateMasters = null;
		List<CityMaster> listCityMasters = null;
		TeacherPersonalInfo teacherpersonalinfo = null;
		TeacherDetail tDetail = null;
		
		List<EthnicOriginMaster> lstEthnicOriginMasters=null;
		List<EthinicityMaster> lstEthinicityMasters=null;
		List<RaceMaster> listRaceMasters = null;
		List<GenderMaster> listGenderMasters = null;
		
		try {
			
			tDetail =(TeacherDetail) session.getAttribute("teacherDetail");
			teacherpersonalinfo = teacherPersonalInfoDAO.findById(tDetail.getTeacherId(), false, false);					
			
			if(teacherpersonalinfo!=null)
			{	try{
					String[] splitRaceId = teacherpersonalinfo.getRaceId().split(",");
					map.addAttribute("splitRaceId",splitRaceId);
				}catch(Exception e){}
			}
			
			try
			{
				if(teacherpersonalinfo!=null)
				{
					if(teacherpersonalinfo.getPhoneNumber()!=null && !teacherpersonalinfo.getPhoneNumber().equals(""))
					{	
						map.addAttribute("ph1", teacherpersonalinfo.getPhoneNumber().subSequence(0, 3));
						map.addAttribute("ph2", teacherpersonalinfo.getPhoneNumber().subSequence(3, 6));
						map.addAttribute("ph3", teacherpersonalinfo.getPhoneNumber().subSequence(6, 10));
					}else
					{
						map.addAttribute("ph1", "");
						map.addAttribute("ph2", "");
						map.addAttribute("ph3", "");
					}
					
					if(teacherpersonalinfo.getMobileNumber()!=null && !teacherpersonalinfo.getMobileNumber().equals(""))
					{
						map.addAttribute("mb1", teacherpersonalinfo.getMobileNumber().subSequence(0, 3));
						map.addAttribute("mb2", teacherpersonalinfo.getMobileNumber().subSequence(3, 6));
						map.addAttribute("mb3", teacherpersonalinfo.getMobileNumber().subSequence(6, 10));
					}else
					{
						map.addAttribute("mb1", "");
						map.addAttribute("mb2", "");
						map.addAttribute("mb3", "");
					}
					
				}	
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			
			
			if(teacherpersonalinfo==null)
			{
				teacherpersonalinfo = new TeacherPersonalInfo();
				teacherpersonalinfo.setTeacherId(tDetail.getTeacherId());
				teacherpersonalinfo.setFirstName(tDetail.getFirstName());
				teacherpersonalinfo.setLastName(tDetail.getLastName());
			}
			else
			{
				listCityMasters = cityMasterDAO.findCityByState(teacherpersonalinfo.getStateId());
			}
			TeacherDetail teacherDetail=teacherDetailDAO.findById(tDetail.getTeacherId(), false,false);
			
			listRaceMasters = raceMasterDAO.findAllRaceByOrder();
			listGenderMasters = genderMasterDAO.findAllGenderByOrder();
			listStateMasters = stateMasterDAO.findAllStateByOrder();
			
			lstEthinicityMasters=ethinicityMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
			lstEthnicOriginMasters=ethnicOriginMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
			
			List<CountryMaster> countryMasters=new ArrayList<CountryMaster>();
			countryMasters=countryMasterDAO.findAllActiveCountry();
			
			map.addAttribute("lstEthinicityMasters", lstEthinicityMasters);
			map.addAttribute("lstEthnicOriginMasters", lstEthnicOriginMasters);
			
			map.addAttribute("teacherDetail", teacherDetail);
			map.addAttribute("listGenderMasters", listGenderMasters);
			map.addAttribute("listRaceMasters", listRaceMasters);
			map.addAttribute("listCityMasters", listCityMasters);
			map.addAttribute("listStateMasters", listStateMasters);
			map.addAttribute("listCityMasters", listCityMasters);	
			map.addAttribute("teacherpersonalinfo", teacherpersonalinfo);
			map.addAttribute("isPortfolioNeeded", getTeacherPortfolioStatusForNativeAndRefered(teacherDetail));
			map.addAttribute("listCountryMaster", countryMasters);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "personalinforeminder";
	}
	
	@RequestMapping(value="/personalinforeminder.do",method=RequestMethod.POST)
	public String doSignUpPOST(@ModelAttribute(value="teacherpersonalinfo") TeacherPersonalInfo teacherpersonalinfo,BindingResult result, ModelMap map,HttpServletRequest request)
	{
		System.out.println(":::::::::::::::::	personalinforeminder.do	:::::::::::::::::");
		try 
		{
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("teacherDetail") == null) 
			{
				return "redirect:index.jsp";
			}
			
			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

			TeacherPersonalInfo teacherPersonalInfo2=teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
			if(teacherPersonalInfo2!=null)
			{
				if(teacherPersonalInfo2.getMiddleName()!=null && !teacherPersonalInfo2.getMiddleName().equalsIgnoreCase(""))
					teacherpersonalinfo.setMiddleName(teacherPersonalInfo2.getMiddleName());
				
				if(teacherPersonalInfo2.getSSN()!=null && !teacherPersonalInfo2.getSSN().equalsIgnoreCase(""))
					teacherpersonalinfo.setSSN(teacherPersonalInfo2.getSSN());
				
				if(teacherPersonalInfo2.getDob()!=null)
					teacherpersonalinfo.setDob(teacherPersonalInfo2.getDob());
				
				if(teacherPersonalInfo2.getRetirementdate()!=null)
					teacherpersonalinfo.setRetirementdate(teacherPersonalInfo2.getRetirementdate());
				
				if(teacherPersonalInfo2.getMoneywithdrawaldate()!=null)
					teacherpersonalinfo.setMoneywithdrawaldate(teacherPersonalInfo2.getMoneywithdrawaldate());
				
				if(teacherPersonalInfo2.getEmployeeType()!=null)
					teacherpersonalinfo.setEmployeeType(teacherPersonalInfo2.getEmployeeType());
				
				if(teacherPersonalInfo2.getEmployeeNumber()!=null)
					teacherpersonalinfo.setEmployeeNumber(teacherPersonalInfo2.getEmployeeNumber());
				
				if(teacherPersonalInfo2.getIsCurrentFullTimeTeacher()!=null)
					teacherpersonalinfo.setIsCurrentFullTimeTeacher(teacherPersonalInfo2.getIsCurrentFullTimeTeacher());;
				
				if(teacherPersonalInfo2.getIsVateran()!=null)
					teacherpersonalinfo.setIsVateran(teacherPersonalInfo2.getIsVateran());
				
				
				
			}
			
			teacherpersonalinfo.setIsDone(true);
			teacherpersonalinfo.setCreatedDateTime(new Date());
			
			try
			{
				if(request.getParameter("ethnicOriginId")!=null && !request.getParameter("ethnicOriginId").equals(""))
				{
					Integer ethOrgId=Integer.parseInt(request.getParameter("ethnicOriginId"));
					if( ethOrgId>0){
						EthnicOriginMaster ethnicOriginMaster=ethnicOriginMasterDAO.findById(ethOrgId, false, false);
						teacherpersonalinfo.setEthnicOriginId(ethnicOriginMaster);
					}
				}
				
				if(request.getParameter("ethnicityId")!=null && !request.getParameter("ethnicityId").equals(""))
				{
					Integer ethMId=Integer.parseInt(request.getParameter("ethnicityId"));
					if( ethMId>0){
						EthinicityMaster ethinicityMaster=ethinicityMasterDAO.findById(ethMId, false, false);
						teacherpersonalinfo.setEthnicityId(ethinicityMaster);
					}
				}
				
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			if(teacherpersonalinfo.getRaceId()==null){
				RaceMaster raceMaster=raceMasterDAO.findById(0, false, false);
				teacherpersonalinfo.setRaceId(String.valueOf(raceMaster.getRaceId()));
			}else{
				String[] checkedRaceIds = request.getParameterValues("raceId");
				
				String raceIdsvalue="";				
				for(int i=0;i<checkedRaceIds.length;i++){
					raceIdsvalue+=checkedRaceIds[i]+",";
				}
				raceIdsvalue = raceIdsvalue.substring(0, raceIdsvalue.length() - 1);
				teacherpersonalinfo.setRaceId(raceIdsvalue);
				 System.out.println(raceIdsvalue);
			}
			
			if(teacherpersonalinfo.getGenderId()==null){
				GenderMaster genderMaster=genderMasterDAO.findById(0, false, false);
				teacherpersonalinfo.setGenderId(genderMaster);
			}
			System.out.println(":::::::::::::::::::::::::::: teacherpersonalinfo :::::::::::::::::::::"+teacherpersonalinfo.getCountryId());
			try{
				if(teacherpersonalinfo.getCountryId().getCountryId()>0)
				{
					CountryMaster countryMaster  = countryMasterDAO.findById(teacherpersonalinfo.getCountryId().getCountryId(), false, false);
					List<StateMaster> lstMasters = stateMasterDAO.findActiveStateByCountryId(countryMaster);
					
					if(teacherpersonalinfo.getCountryId().getCountryId()==223)
					{
						teacherpersonalinfo.setOtherState(null);
						teacherpersonalinfo.setOtherCity(null);
					}
					else
					{
						if(lstMasters!=null && lstMasters.size()>0)
						{
							teacherpersonalinfo.setOtherState(null);
							teacherpersonalinfo.setCityId(null);
						}
						else
						{
							teacherpersonalinfo.setStateId(null);
							teacherpersonalinfo.setCityId(null);
						}
					}
				}
				
				//teacherpersonalinfo
				
				teacherDetail.setFirstName(teacherpersonalinfo.getFirstName());
				teacherDetail.setLastName(teacherpersonalinfo.getLastName());				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			teacherPersonalInfoDAO.makePersistent(teacherpersonalinfo);
			teacherDetailDAO.makePersistent(teacherDetail);
			
			boolean bTeacherPortfolioStatus=false;
			TeacherPortfolioStatus portfolioStatus = null;
			portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			
			if(portfolioStatus==null)
			{
				bTeacherPortfolioStatus=true;
				portfolioStatus = new TeacherPortfolioStatus();
				portfolioStatus.setTeacherId(teacherDetail);
				portfolioStatus.setIsPersonalInfoCompleted(true);
				portfolioStatus.setIsAcademicsCompleted(false);
				portfolioStatus.setIsCertificationsCompleted(false);
				portfolioStatus.setIsExperiencesCompleted(false);
				portfolioStatus.setIsAffidavitCompleted(false);				
				//teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				TeacherPortfolioStatus tpsTemp=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				if(bTeacherPortfolioStatus && tpsTemp==null)
					teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				else
				{
					portfolioStatus.setId(tpsTemp.getId());
					teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				}
				
			}
			else
			{
				try{
					if(portfolioStatus.getIsPersonalInfoCompleted()==null)
					{
						portfolioStatus.setIsPersonalInfoCompleted(false);
					}
				}catch(Exception e){
					portfolioStatus.setIsPersonalInfoCompleted(false);
					e.printStackTrace();
				}

				try{
					if(portfolioStatus.getIsCertificationsCompleted()==null){
					portfolioStatus.setIsCertificationsCompleted(false);
					}
				}catch(Exception e){
					portfolioStatus.setIsCertificationsCompleted(false);
					e.printStackTrace();
				}

				try{
					if(portfolioStatus.getIsExperiencesCompleted()==null){
					portfolioStatus.setIsExperiencesCompleted(false);
					}
				}catch(Exception e){
					portfolioStatus.setIsExperiencesCompleted(false);
					e.printStackTrace();
				}

				try{
					if(portfolioStatus.getIsAffidavitCompleted()==null){
					portfolioStatus.setIsAffidavitCompleted(false);
					}
				}catch(Exception e){
					portfolioStatus.setIsAffidavitCompleted(false);
					e.printStackTrace();
				}

				try{
					if(portfolioStatus.getIsAcademicsCompleted()==null){
					portfolioStatus.setIsAcademicsCompleted(false);
					}
				}catch(Exception e){
					portfolioStatus.setIsAcademicsCompleted(false);
					e.printStackTrace();
				}
				
				
				
				
				portfolioStatus.setIsPersonalInfoCompleted(true);
				//teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				
				TeacherPortfolioStatus tpsTemp=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				if(bTeacherPortfolioStatus && tpsTemp==null)
					teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				else
				{
					portfolioStatus.setId(tpsTemp.getId());
					teacherPortfolioStatusDAO.makePersistent(portfolioStatus);
				}
				
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		
		return "redirect:academics.do";
		
	}
	
	
	@RequestMapping(value="/personalinforeminder.do", method=RequestMethod.GET)
	public String doPortfolioReminderGET(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{

		System.out.println("personalinforeminder.do");
		try 
		{
		HttpSession session = request.getSession(false);
		String p	 	= "";
		if(request.getParameter("p")!=null){
			p=request.getParameter("p").toString();
		}
		System.out.println(">--p--<"+p);
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			Cookie[] cookies = request.getCookies();
			for(int i=0;i<cookies.length;i++)
			{
				if(cookies[i].getName().equals("PortfolioUrl"))
				{
					System.out.println("Cookie already exists");
					cookies[i].setValue("");
				}
			}
			Cookie cookie = new Cookie ("PortfolioUrl",p);
			cookie.setMaxAge(1 * 60 * 60);
			response.addCookie(cookie);	    
			System.out.println("cookie set for slot url :: "+p);
			return "redirect:index.jsp";
		}else{
				
			Cookie[] cookies = request.getCookies();
			for(int i=0;i<cookies.length;i++)
			{
				if(cookies[i].getName().equals("PortfolioUrl"))
				{
					System.out.println("Cookie already exists");
					Cookie cookie2 = cookies[i];
					cookie2.setValue(null);
					cookie2.setMaxAge(0);
		            response.addCookie(cookie2);					
				}
			}
		
		List<StateMaster> listStateMasters 		= 	null;
		List<CityMaster> listCityMasters 		= 	null;
		TeacherPersonalInfo teacherpersonalinfo = 	null;
		TeacherDetail tDetail 					= 	null;
		boolean isMiami = false;
		List<EthnicOriginMaster> lstEthnicOriginMasters	=	null;
		List<EthinicityMaster> lstEthinicityMasters		=	null;
		List<RaceMaster> listRaceMasters 				= 	null;
		List<GenderMaster> listGenderMasters 			= 	null;
		
		DistrictMaster districtMaster	=	null;
		List<PortfolioReminders> portfolioRemindersList 		= null;
		List<DistrictPortfolioConfig> districtPortfolioConfig 	= null;
		DistrictPortfolioConfig portConfig 						= 	null;
		PortfolioReminders portReminder 						= 	null;
		Integer districtId = 0;
		Integer tId = 0;
		Integer jobId = 0;
		JobOrder jobOrder	=	null;
		
		
		
			
			p = Utility.decodeBase64(p);
			System.out.println("<--p-->"+p);
			String array[]		=	p.split("&");
			String arrayDist[]	=	array[0].split("=");
			String arrayTeach[]	=	array[1].split("=");
			
			districtId		= 	Utility.decryptNo(Integer.valueOf(arrayDist[1]));
			tId		= 	Utility.decryptNo(Integer.valueOf(arrayTeach[1]));
			System.out.println("districtId-->"+districtId);
			System.out.println("tId-->"+tId);
			districtMaster			=	districtMasterDAO.findByDistrictId(districtId.toString());

			map.addAttribute("jobId", jobId);
			
			
			portfolioRemindersList 	= 	portfolioRemindersDAO.getRecordByDistrict(districtMaster);
			
			
			
			tDetail =(TeacherDetail) session.getAttribute("teacherDetail");
			teacherpersonalinfo = teacherPersonalInfoDAO.findById(tDetail.getTeacherId(), false, false);
			districtPortfolioConfig = districtPortfolioConfigDAO.getPortfolioConfigsByDistrictAndCandidate(districtMaster,"I");
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
			listCityMasters	= cityMasterDAO.findCityByState(teacherpersonalinfo.getStateId());
			System.out.println("listCityMasters::"+listCityMasters.size());
			System.out.println("teacherpersonalinfo.getStateId::"+teacherpersonalinfo.getStateId().getStateId());
			System.out.println("districtMaster.getStateId::"+districtMaster.getStateId().getStateId());
			map.addAttribute("DiststatId", districtMaster.getStateId().getStateId());
			map.addAttribute("InfostatId", teacherpersonalinfo.getStateId().getStateId());
			try{
				if(districtPortfolioConfig!=null && districtPortfolioConfig.size()>0){
					portConfig = districtPortfolioConfig.get(0);
				}
				
				if(portfolioRemindersList != null && portfolioRemindersList.size()>0){
					portReminder = portfolioRemindersList.get(0);
				}
				
			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			if(portfolioRemindersList!=null && portfolioRemindersList.size()>0){
				map.addAttribute("portfolioReminders", portReminder);
			}
			
			if(districtPortfolioConfig!=null && districtPortfolioConfig.size()>0){
				map.addAttribute("districtPortfolioConfig", portConfig);
			}
			
			
			List<CountryMaster> countryMasters=new ArrayList<CountryMaster>();
			countryMasters=countryMasterDAO.findAllActiveCountry();
			
			map.addAttribute("districtMaster", districtMaster);
			map.addAttribute("lstMonth", Utility.getMonthList());
			map.addAttribute("lstYear", Utility.getLasterYearByYear(1955));
			map.addAttribute("lstDays", Utility.getDays());
			map.addAttribute("listCountryMaster", countryMasters);
			map.addAttribute("teacherpersonalinfo", teacherpersonalinfo);
			map.addAttribute("districtMasterDSPQ", districtMaster);
			map.addAttribute("teacherIdForDSPQ", teacherDetail);
			map.addAttribute("isPrinciplePhiladelphia","0");
			map.addAttribute("lstLastYear", Utility.getLasterYearByYear(1955));
			map.addAttribute("listCityMasters", listCityMasters);
			
			if(teacherpersonalinfo.getSSN()!=null){
			map.addAttribute("ssn",Utility.decodeBase64(teacherpersonalinfo.getSSN()));
			}else{
				map.addAttribute("ssn","");
			}
			String a=null;
			if(teacherpersonalinfo.getPhoneNumber()!=null && teacherpersonalinfo.getPhoneNumber()!=""){
			a=teacherpersonalinfo.getPhoneNumber();
			System.out.println("PhoneNumber::"+teacherpersonalinfo.getPhoneNumber());
			map.addAttribute("phone1",a.substring(0,3));
			map.addAttribute("phone2",a.substring(3,6));
			map.addAttribute("phone3",a.substring(6));
			}else{
				map.addAttribute("phone1","");
				map.addAttribute("phone2","");
				map.addAttribute("phone3","");
			}
			
			
			
			if(teacherpersonalinfo.getDob()!=null){
				DateFormat df = new SimpleDateFormat("MM-dd-yyyy");
				String dob[] =null;
				String reportDate = df.format(teacherpersonalinfo.getDob());
				dob=reportDate.split("-");
				map.addAttribute("month",Integer.parseInt(dob[0]));
				map.addAttribute("day",Integer.parseInt(dob[1]));
				map.addAttribute("year",dob[2]);
				System.out.println("reportDate-->"+reportDate);
				System.out.println("dob[0]-->"+dob[0]+"<--dob[1]-->"+dob[1]+"<--dob[2]-->"+dob[2]);
			}else{
				map.addAttribute("month","");
				map.addAttribute("day","");
				map.addAttribute("year","");
			}
			
			
			
			
			
			        String districtDName=null;
			
					if(districtMaster.getDisplayName() !=null && !districtMaster.getDisplayName().equals("")){
						districtDName=districtMaster.getDisplayName();
					}else{
						districtDName=districtMaster.getDistrictName();
					}
				
				map.addAttribute("districtDName", districtDName);
				List<EthnicOriginMaster> lstethnicOriginMasters = null;
				lstEthinicityMasters=ethinicityMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
				lstethnicOriginMasters=ethnicOriginMasterDAO.findByCriteria(Restrictions.eq("status", "A"));

				System.out.println(" lstEthinicityMasters "+lstEthinicityMasters.size()+" lstethnicOriginMasters "+lstethnicOriginMasters.size());

				map.addAttribute("lstEthinicityMasters", lstEthinicityMasters);
				map.addAttribute("lstethnicOriginMasters", lstethnicOriginMasters);
				
				if(districtMaster !=null){
					if(districtMaster.getDistrictId().equals(1200390))
						isMiami=true;
				}
				map.addAttribute("isMiami", isMiami);
				List<GenderMaster> lstGenderMasters  = genderMasterDAO.findAllGenderByOrder();
				map.addAttribute("lstGenderMasters", lstGenderMasters);
				List<CertificationStatusMaster> lstCertificationStatusMaster=certificationStatusMasterDAO.findAllCertificationStatusMasterByOrder();
				map.addAttribute("lstCertificationStatusMaster", lstCertificationStatusMaster);
				Criterion certIficatCri = Restrictions.eq("status", "A");
				List<CertificationTypeMaster>listCertificationTypeMasters=certificationTypeMasterDAO.findByCriteria(certIficatCri);
				map.addAttribute("listCertTypeMasters", listCertificationTypeMasters);
				List<StateMaster> listStateMaster=new ArrayList<StateMaster>();
				listStateMaster = stateMasterDAO.findAllStateByOrder();
				map.addAttribute("listStateMaster", listStateMaster);
				map.addAttribute("lstYear", Utility.getLasterYearByYear(1955));
				map.addAttribute("lstComingYear", Utility.getComingYearsByYear());
				Criterion criterionOrg = Restrictions.eq("status", "A");
				List<OrgTypeMaster> lstOrgTypeMasters = orgTypeMasterDAO.findByCriteria(Order.asc("orgType"),criterionOrg);
				map.addAttribute("lstOrgTypeMasters", lstOrgTypeMasters);
				List<PeopleRangeMaster> lstPeopleRangeMasters = peopleRangeMasterDAO.findAll();
				map.addAttribute("lstPeopleRangeMasters", lstPeopleRangeMasters);
				
				List<TFAAffiliateMaster> lstTFAAffiliateMaster	=	tfaAffiliateMasterDAO.findByCriteria(Order.asc("tfaAffiliateName"));
				List<TFARegionMaster> lstTFARegionMaster		=	tfaRegionMasterDAO.findByCriteria(Order.asc("tfaRegionName"));
				map.addAttribute("lstTFAAffiliateMaster", lstTFAAffiliateMaster);
				map.addAttribute("lstTFARegionMaster", lstTFARegionMaster);
				map.addAttribute("lstCorpsYear", Utility.getLasterYeartillAdvanceYear(1990));
				
				TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
				map.addAttribute("teacherExperience", teacherExperience);
				
				Criterion criterion = Restrictions.eq("status", "A");
				List<FieldMaster> lstFieldMaster = fieldMasterDAO.findByCriteria(Order.asc("fieldName"),criterion);
				Criterion criterionEmpRTstatus = Restrictions.eq("status", "A");
				List<EmpRoleTypeMaster> lstEmpRoleTypeMaster = empRoleTypeMasterDAO.findByCriteria(criterionEmpRTstatus);
				map.addAttribute("lstFieldMaster", lstFieldMaster);
				map.addAttribute("lstEmpRoleTypeMaster", lstEmpRoleTypeMaster);
				
				List<SubjectAreaExamMaster> subjectAreaExamMasters =new ArrayList<SubjectAreaExamMaster>();
				if(districtMaster!=null){
					subjectAreaExamMasters = subjectAreaExamMasterDAO.findActiveSubjectExamByDistrict(districtMaster);
				}
				map.addAttribute("subjectList",subjectAreaExamMasters);
				
				TeacherPortfolioStatus portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				
				map.addAttribute("tportfolioStatus",portfolioStatus );
				
				
				
				String isAffilated =null;
				if(request.getParameter("ok_cancelflag")!=null){
					isAffilated=request.getParameter("ok_cancelflag");
				}
				if(isAffilated==null){
					isAffilated =(String)(session.getAttribute("isAffilated")==null?"0":session.getAttribute("isAffilated"));
				}
				map.addAttribute("isAffilated",isAffilated);
				
				//Map<Integer,List<PortfolioRemindersDspq>> dSPQ1 = portfolioRemindersDspqDAO.getDSPQByDistrict(districtMaster);
				List<PortfolioRemindersDspq> dSPQ = portfolioRemindersDspqDAO.getRecordByDistrict(districtMaster);
				if(dSPQ!=null && dSPQ.size()>0){
					map.addAttribute("dSPQ",1);
				} else {
					map.addAttribute("dSPQ",0);	
				}
				
			System.out.println("::::::::::::::::::::::::::::::::::::"+teacherpersonalinfo.getCountryId());
			
			if(teacherpersonalinfo!=null)
			{	
				try{
					String[] splitRaceId = teacherpersonalinfo.getRaceId().split(",");
					map.addAttribute("splitRaceId",splitRaceId);
				}catch(Exception e){}
			}
			
		
		} 
	}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	
		return "personalinforeminder";
	}
	
	
	
	@RequestMapping(value="/portfolioreminderheader.do", method=RequestMethod.GET)
	public String doPortfolioHeaderGET(ModelMap map,HttpServletRequest request)throws Exception
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}
		TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
		
		TeacherPortfolioStatus portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
		
		map.addAttribute("portfolioStatus",portfolioStatus );
		
				
		return "portfolioreminderheader";
	}
	
	@Transactional(readOnly=false)
	public boolean getTeacherPortfolioStatusForNativeAndRefered(TeacherDetail teacherDetail){
		
		boolean bReturnValue=false;
		Criterion criterionTID=Restrictions.eq("teacherId",teacherDetail);
		List<JobForTeacher> forTeachers=new ArrayList<JobForTeacher>();
		forTeachers=jobForTeacherDAO.findByCriteria(criterionTID);
		if(forTeachers!=null && forTeachers.size()>0)
		{
			for(JobForTeacher forTeacher:forTeachers)
			{
				if(forTeacher!=null && forTeacher.getJobId()!=null && forTeacher.getJobId().getDistrictMaster()!=null && forTeacher.getJobId().getDistrictMaster().getDistrictId()!=1704170)
				{
					bReturnValue=true;
					break;
				}
			}
		}
		else
			bReturnValue=true;
			
		
		return bReturnValue;
	}
	
	@RequestMapping(value="/portfoliofinal.do", method=RequestMethod.GET)
	public String doPortfolioFinalGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println(":::::::::::::: portfoliofinal.do :::::::::::::::::");
		return "portfoliofinal";
	}

	@RequestMapping(value="/portfoliosubmit.do", method=RequestMethod.GET)
	public String doPortfolioFinalAnswerGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println(":::::::::::::: portfoliosubmit.do :::::::::::::::::");
		return "portfoliosubmit";
		
	}
	
	@Transactional(readOnly=false)
	@RequestMapping(value="/sendportfolioreminder.do", method=RequestMethod.GET)
    public String sendEmail(ModelMap map,HttpServletRequest request){
        
		System.out.println("::::::::::::: sendportfolioreminder.do :::::::::::::::::");
		
		UserMaster userMaster			=	null;
		DistrictMaster districtMaster 	= 	null;
		Integer districtId 				=	null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}

			districtMaster = userMaster.getDistrictId();
			if(districtMaster!=null)
			districtId = districtMaster.getDistrictId();
		}
		
		/*	URL */
		String path = request.getContextPath();
		int portNo = request.getServerPort();
		String showPortNo = null;
		if(portNo==80)
		{
			showPortNo="";
		}
		else
		{
			//showPortNo=":"+portNo;
			showPortNo="";
		}
		//String baseUrl = request.getScheme()+"://"+request.getServerName()+""+showPortNo+path+"/";
		String baseUrl = "https://platform.teachermatch.org/";
		
		String urlString		=	null;
		String mainURLEncode	=	null;
		String mailContent		=	null;
		String mailToReference =    "";
		
		if(userMaster.getDistrictId()!=null){
			districtMaster=userMaster.getDistrictId();
		}
		
		int disId   = districtMaster.getDistrictId();
		//int jbId    = 3159;
		String dId	=	null;
		String tId  =   null;
		//String jId	=	null;
		String messageSubject= "Reminder -- Update Required for Your Application with "+districtMaster.getDistrictName()+"";
		
		JobOrder   jobOrder =null;
		try{
			
			List<TeacherDetail> teacherDetailList 		= null;
			List<TeacherDetail> teacherDetailListMain 	= null;
			teacherDetailList = jobForTeacherDAO.getAllHiredTeacher(districtMaster);
			teacherDetailListMain = jobForTeacherDAO.getAllUnhiredTeacher(districtMaster, teacherDetailList);
			
			// Get Record From messagetoteacher Table
			
			List<MessageToTeacher> messageToTeacherList = null;
			Criterion criteria = null;
			Map<Integer,MessageToTeacher> msgMap = new HashMap<Integer, MessageToTeacher>();
			
			try{
				criteria = Restrictions.eq("portfolioReminder", "Y");
				messageToTeacherList = messageToTeacherDAO.findByCriteria(criteria);
				
				for(MessageToTeacher msgObj:messageToTeacherList){
					msgMap.put(msgObj.getTeacherId().getTeacherId(), msgObj);
				}
				
			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			SessionFactory factory = jobForTeacherDAO.getSessionFactory();
		    StatelessSession statelessSession=factory.openStatelessSession();
		    Transaction transaction = statelessSession.beginTransaction();

		    /*String emailAddress = "hanzala@netsutra.com";
		    String emailAddress = "sanjeev.arora@gmail.com";
		    teacherDetailListMain =	 teacherDetailDAO.findByCriteria(Restrictions.eq("emailAddress",emailAddress));*/
		    System.out.println("teacherDetailListMain::::::::::::"+teacherDetailListMain.size());
		    
			if(teacherDetailListMain.size()>0 && teacherDetailListMain!=null){
				for(TeacherDetail teacherDetail:teacherDetailListMain){
					
					boolean chkDup=false;
					// Check for duplicate Record
					MessageToTeacher messageToTeacher = new MessageToTeacher();
					MessageToTeacher messageToTeacher1 = msgMap.get(teacherDetail.getTeacherId());
					
					if(messageToTeacher1!=null && teacherDetail.getTeacherId().equals(messageToTeacher1.getTeacherId().getTeacherId()))
						chkDup = true;
					
					if(teacherDetail!=null)
					mailToReference	= teacherDetail.getEmailAddress();
					String to = teacherDetail.getEmailAddress();
					dId=Utility.encryptNo(disId);
					tId=Utility.encryptNo(teacherDetail.getTeacherId());
					urlString 		= 	"dId="+dId+"&tId="+tId+"";
					mainURLEncode	=	baseUrl+"personalinforeminder.do?p="+Utility.encodeInBase64(urlString);
					mailContent = MailText.PortfolioReminderMailByLink(teacherDetail, districtMaster, mainURLEncode);
					//System.out.println(messageSubject);
					//System.out.println(mailContent);
					emailerService.sendMailAsHTMLText(mailToReference,messageSubject, mailContent);
					
					messageToTeacher.setTeacherId(teacherDetail);
					messageToTeacher.setJobId(jobOrder);
					messageToTeacher.setTeacherEmailAddress(to);
					messageToTeacher.setMessageSubject(messageSubject);
					messageToTeacher.setMessageSend(mailContent);
					messageToTeacher.setSenderId(userMaster);
					messageToTeacher.setSenderEmailAddress("MDCPS");
					messageToTeacher.setEntityType(userMaster.getEntityType());
					messageToTeacher.setPortfolioReminder("Y");
					
					if(chkDup==true)
						messageToTeacher1.setNoOfReminderSent(messageToTeacher1.getNoOfReminderSent()+1);
					else
						messageToTeacher.setNoOfReminderSent(1);
					
					messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
					messageToTeacher.setCreatedDateTime(new Date());
					transaction = statelessSession.beginTransaction();
					
					if(chkDup==true)
						statelessSession.update(messageToTeacher1);
					else
						statelessSession.insert(messageToTeacher);
					
				}
		  }
			transaction.commit();
		}catch(Exception exception){
			exception.printStackTrace();
		}
		
		return "sendportfolioreminder";
    }

	
}
