package tm.controller.master;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tm.bean.user.UserMaster;


@Controller
public class ManageExamController 
{
	
	@RequestMapping(value="/manageexamcode.do", method=RequestMethod.GET)
	public String tagsGET(ModelMap map,HttpServletRequest request)
	{
		UserMaster userMaster=null;
		try 
		{
			HttpSession session = request.getSession(false);
			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			userMaster		=	(UserMaster) session.getAttribute("userMaster");
			
			int entityID=userMaster.getEntityType();
			map.addAttribute("entityID", entityID);
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "manageexamcode";
	}
	
}
