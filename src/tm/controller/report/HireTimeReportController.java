package tm.controller.report;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tm.bean.user.UserMaster;

@Controller
public class HireTimeReportController 
{
	 @RequestMapping(value="/hireTime.do", method=RequestMethod.GET)
		public String candidateEECData(ModelMap map,HttpServletRequest request)
		{
		 System.out.println("in the time controller::::::::::::::::::::::::::::::::");
		 try 
			{
				UserMaster userMaster=null;
				HttpSession session = request.getSession(false);
				if (session == null || session.getAttribute("userMaster") == null) 
				{
					return "redirect:index.jsp";
				}else{
					userMaster=	(UserMaster) session.getAttribute("userMaster");
					map.addAttribute("userMaster", userMaster);
					map.addAttribute("EntityType", userMaster.getEntityType());
				}
				
			 
				
				if(userMaster.getEntityType()!=1){
					map.addAttribute("districtName",userMaster.getDistrictId().getDistrictName());
					map.addAttribute("districtId",userMaster.getDistrictId().getDistrictId());
					map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
					map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
				}else{
					map.addAttribute("districtName",null);
					map.addAttribute("DistrictOrSchoolName",null);
				}
				
				if(userMaster.getEntityType()==2){
					map.addAttribute("districtName",userMaster.getDistrictId().getDistrictName());
					map.addAttribute("districtId",userMaster.getDistrictId().getDistrictId());
				}
//				System.out.println("in the mehtod last ::::::::::::::::::::::::::  "+entityID);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
		return "hireTime";
		}
}
