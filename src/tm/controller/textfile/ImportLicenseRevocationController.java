package tm.controller.textfile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import tm.bean.UserUploadFolderAccess;
import tm.bean.textfile.License;
import tm.bean.user.UserMaster;


import tm.dao.UserUploadFolderAccessDAO;

import tm.dao.textfile.LicenseDAO;
import tm.dao.user.UserMasterDAO;

/* 
 * @Author: Sandeep Yadav
 */

@Controller
public class ImportLicenseRevocationController {

	
	@Autowired
	private LicenseDAO licenseDAO;
	public void setLicenseDAO(LicenseDAO licenseDAO) {
		this.licenseDAO = licenseDAO;
	}

	@Autowired
	private UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	
	public void setUserUploadFolderAccessDAO(
			UserUploadFolderAccessDAO userUploadFolderAccessDAO) {
		this.userUploadFolderAccessDAO = userUploadFolderAccessDAO;
	}
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	

	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}



	@RequestMapping(value="/licenserevocation.do", method=RequestMethod.GET)
	public @ResponseBody String getAllLicenseRevocationData(ModelMap map,HttpServletRequest request)
	{
		    System.out.println("::::: Use ImportLicenseRevocation controller :::::");
		    List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
			try {
				Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
				Criterion active = Restrictions.eq("status", "A");
				uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
			} catch (Exception e) {
				e.printStackTrace();
			}	
		    String fileName = "HRMS--2260-REVOKE--HRMSRevocations.txt";
		    String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();
		  	//saveLicenseRevocation(fileName,folderPath);
			
		
		return saveLicenseRevocation(fileName,folderPath);
	}
	
	
	private Vector vectorDataTxt = new Vector();
    public String saveLicenseRevocation(String fileName,String folderPath){	
			
			 System.out.println("::::::::saveLicenseRevocation:::::::::");
			 
			 String filePath=folderPath+fileName;
		 	 System.out.println("folderPath  "+folderPath);
		 	 try {
		 		 if(fileName.contains(".txt") || fileName.contains(".TXT")){
		 			   vectorDataTxt = readDataTxt(filePath);
		 		 }else{
	    			 //break;
	    	 }
		} catch (Exception e) {
			e.printStackTrace();
		}
    	 
		SessionFactory sessionFactory=licenseDAO.getSessionFactory();
		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
 	    Transaction txOpen =statelesSsession.beginTransaction();
 	    
    	 int SSN_count=11;	    	 
    	 int RevocationRescindDate_count=11;
    	 int StateCode_count=11;
    	 int StatusCode_count=11;
    	 
    	 String final_error_store="";
    	 int numOfError=0;
    	 boolean row_error=false;
    	 int insertCount = 0;
 	     int updateCount = 0;
    	 
    	 List<License> licenseRevocation =new ArrayList<License>();
		try {
			licenseRevocation = licenseDAO.findAll();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
    	 Map<String, License> licenseRevocationList = new HashMap<String, License>();
    	 if(licenseRevocation!=null && licenseRevocation.size()>0)
    	 for (License license2 : licenseRevocation) {
    		 licenseRevocationList.put(license2.getSSN(), license2);
		}
    	 
    	 txOpen =statelesSsession.beginTransaction();
    	 UserMaster userMaster = userMasterDAO.findById(1, false, false);
    	 
		 for(int i=0; i<vectorDataTxt.size(); i++) {
			 Vector vectorCellEachRowData = (Vector) vectorDataTxt.get(i);
			 
			 
	            String SSN="";
	            String RevocationRescindDate="";
	            String StateCode="";
	            String errorText="";
		  	    String rowErrorText="";
		  	    String statusCode ="";
		  	    
	            
	  	    for(int j=0; j<vectorCellEachRowData.size(); j++) {
  	        	try{
  	        		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("personnel.per_ssn_txt")){
  	        			SSN_count=j;
	            	}
	            	if(SSN_count==j)
	            		SSN=vectorCellEachRowData.get(j).toString().trim();
  	        		
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("personnel.per_revoke_dte")){
	            		RevocationRescindDate_count=j;
	            	}
	            	if(RevocationRescindDate_count==j)
	            		RevocationRescindDate=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("personnel.per_revoke_st_cd")){
	            		StateCode_count=j;
					}
	            	if(StateCode_count==j)
	            		StateCode=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("Calculated value")){
	            		StatusCode_count=j;
					}
	            	if(StatusCode_count==j)
	            		statusCode=vectorCellEachRowData.get(j).toString().trim();
	            		
            	}catch(Exception e){
            		e.printStackTrace();
            	}
  	        }
	  	    
	  	  if(i==0){
	  		  
	  		  if(!SSN.equalsIgnoreCase("personnel.per_ssn_txt")){
	  			rowErrorText="personnel.per_ssn_txt column is not found";
	  			row_error=true;
	  		  }
	  		  if(!RevocationRescindDate.equalsIgnoreCase("personnel.per_revoke_dte")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="personnel.per_revoke_dte column is not found";
	  			row_error=true;
	  		  }
	  		  if(!StateCode.equalsIgnoreCase("personnel.per_revoke_st_cd")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="personnel.per_revoke_st_cd column is not found";
	  			row_error=true;  
	  		  }
	  		  
	  		if(!statusCode.equalsIgnoreCase("Calculated value")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="Calculated value column is not found";
	  			row_error=true;  
	  		  }
	  		
	  	  }   
	  	  
		  	if(row_error){
		  		numOfError++;
		  		File file = new File(folderPath+"licenseRevocationError.txt");
				// if file doesnt exists, then create it
		  		try {
			  		if (!file.exists()) {
			  			file.createNewFile();
					}	    				
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(rowErrorText);
		
					bw.close();
		  		} catch (IOException e) {
					
					e.printStackTrace();
				}
				
		  		 break;
		  	 }
	  	    
	  	    
		  	Boolean RevocationRescindDateFlag =true;
		  	
	  	    if(i != 0 && row_error==false){
	  	    	
	  	    	boolean errorFlag=false;
	  	    	
	  	    	if(!SSN.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(SSN.replaceAll("\"", "").length()>9){
	  	    			errorText+="SSN length does not greater than 9";
	  	    		    errorFlag=true;
	  	    		} 
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="SSN is empty";
  	    			    errorFlag=true;
	        	}
	  	    	
	  	    	
	  	    	if(!RevocationRescindDate.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			
	  	    		if (!RevocationRescindDate.replaceAll("\"", "").matches("([0-9]{2})/([0-9]{2})/([0-9]{4})")){
	  	    			RevocationRescindDateFlag=false;
	  	    			errorText+="Revocation Rescind Date should be 'MM/dd/yyyy' format";
	  	    			errorFlag=true;
	  	    		}	
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	  }else{
	  	    		   if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Revocation/ Rescind Date is empty";
  	    			    errorFlag=true;
  	    			    RevocationRescindDateFlag=false;
	        	}
	  	    	
                if(StateCode.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="State Code is empty";
  	    			    errorFlag=true;
	        	}
                
                if(statusCode.replaceAll("\"", "").equalsIgnoreCase("")){
  	    			if(errorFlag){
						errorText+=",";	
					}
        			errorText+="Status Code is empty";
	    			    errorFlag=true;
        	     }
                
                
               
                if(!errorText.equals("")){
	        			int row = i+1;
  	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+SSN+","+RevocationRescindDate+","+StateCode+","+statusCode+"<>";
  	    			numOfError++;
  	    		}
	  	    	
                try {
                	
                	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                	License licenseObj = null;
                	
                	if(SSN.replaceAll("\"", "").isEmpty()){
                		continue;
                	}
                	
                	if(licenseRevocationList.containsKey(SSN.replaceAll("\"", ""))){
                		licenseObj = licenseRevocationList.get(SSN.replaceAll("\"", ""));
                	}
                	
                   
                     
                     if(licenseObj == null){
                    	 licenseObj = new License();
                    	 licenseObj.setSSN(SSN.replaceAll("\"", ""));
                    	 if(RevocationRescindDateFlag){
                    		 licenseObj.setLicenseRevokeRescindDate((formatter.parse(RevocationRescindDate.replaceAll("\"", ""))));
                    	 }
                    	 licenseObj.setStateCode(StateCode.replaceAll("\"", ""));
                    	 
                    	 licenseObj.setStatus("A");
                    	 licenseObj.setCreatedDate(new Date());
                    	 licenseObj.setUserMaster(userMaster); 
                    	 licenseObj.setRevocationStatus(statusCode);
	            	 	statelesSsession.insert(licenseObj);
	            	 	insertCount++;
	            	 
                    	 
                     }else{
                    	 
                    	 if(RevocationRescindDateFlag){
                    		 licenseObj.setLicenseRevokeRescindDate((formatter.parse(RevocationRescindDate.replaceAll("\"", ""))));
                    	 }
                    	 licenseObj.setStateCode(StateCode.replaceAll("\"", ""));
                    	 licenseObj.setStatus("A");
                    	 licenseObj.setCreatedDate(new Date());
                    	 licenseObj.setUserMaster(userMaster);  
                    	 licenseObj.setRevocationStatus(statusCode.replaceAll("\"", ""));
	            	 	statelesSsession.update(licenseObj);
	            	 	updateCount++;
	            	
                     }
                     
                
	  	    		} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	  	    }
	  	    
	  	    
		

		 try{
			if(!final_error_store.equalsIgnoreCase("")){
	 	 		String content = final_error_store;		    				
				File file = new File(folderPath+"licenseRevocationError.txt");
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
				String[] parts = content.split("<>");	    				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write("personnel.per_ssn_txt,personnel.per_revoke_dte,personnel.per_revoke_st_cd,Calculated value");
				bw.write("\r\n\r\n");
				int k =0;
				for(String cont :parts) {
					bw.write(cont+"\r\n\r\n");
				    k++;
				}
				bw.close();
				
	 	 	}	
		  } catch (Exception e) {
				e.printStackTrace();
		  }
			
	}
		  txOpen.commit();
		  String message = "After File Upload,  "+insertCount+"  row inserted and  "+updateCount+"  row updated.";
		  System.out.print(message);
	      return message;
}

    
    
    
    public static Vector readDataTxt(String filePath) throws FileNotFoundException{
    	
		Vector vectorData = new Vector();
		String filepathhdr=filePath;

			 int SSN_count=11;	    	 
	    	 int RevocationRescindDate_count=11;
	    	 int StateCode_count=11;
	    	 int StatusCode_count=11;
		
		    
	        BufferedReader brhdr=new BufferedReader(new FileReader(filepathhdr));
	        
	        String strLineHdr="";	        
	        String hdrstr="";
	        
	        int lineNumberHdr=0;
	        try{
	            
	            while((strLineHdr=brhdr.readLine())!=null){  
	            	
	            	if(strLineHdr.isEmpty())
	            		continue;
	            	Vector vectorCellEachRowData = new Vector();
	                int cIndex=0;
	                boolean cellFlag=false;
	                Map<String,String> mapCell = new TreeMap<String, String>();
	                int i=1;
	                
	                String[] parts = strLineHdr.split("\\|");
	                
	                for(int j=0; j<parts.length; j++) {
	                	hdrstr=parts[j];
	                	cIndex=j;
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("personnel.per_ssn_txt")){
	                        	SSN_count=cIndex;
	                    		
	                    	}
	                        
	                        if(SSN_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("personnel.per_revoke_dte")){
	                        	RevocationRescindDate_count=cIndex;
	                    		
	                    	}
	                    	if(RevocationRescindDate_count==cIndex){
	                    		cellFlag=true;
	                    		
	                    	}	
	                    	  
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("personnel.per_revoke_st_cd")){
	                    		StateCode_count=cIndex;
	                    		
	                    	}
	                    	if(StateCode_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("Calculated value")){
	                    		StatusCode_count=cIndex;
	                    		
	                    	}
	                    	if(StatusCode_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	
	                    	if(cellFlag){
	                    		
	                    			try{
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}catch(Exception e){
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}
	                    		
	                    	}
	                        
	                    }
	                    
	                    vectorCellEachRowData=cellValuePopulate(mapCell);
	                    vectorData.addElement(vectorCellEachRowData);
	                lineNumberHdr++;
	            }
	        }
	        catch(Exception e){
	            System.out.println(e.getMessage());
	        }
	        //System.out.println("::::::::: read data from text ::::::::::       "+vectorData);       
		return vectorData;
	}
    
    
		    public static Vector cellValuePopulate(Map<String,String> mapCell){
		   	 Vector vectorCellEachRowData = new Vector();
		   	 Map<String,String> mapCellTemp = new TreeMap<String, String>();
		   	 
		   	 boolean flag0=false,flag1=false,flag2=false,flag3=false;
		   	 for(Map.Entry<String, String> entry : mapCell.entrySet()){
		   		 String key=entry.getKey();
		   		String cellValue=null;
		   		if(entry.getValue()!=null)
		   			cellValue=entry.getValue().trim();
		   		    
		   		    	
		   		
		   		if(key.equals("0")){
		   			mapCellTemp.put(key, cellValue);
		   			flag0=true;
		   		}
		   		if(key.equals("1")){
		   			flag1=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("2")){
		   			flag2=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("3")){
		   			flag3=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		
		   		
		   		
		   	 }
		   	 if(flag0==false){
		   		 mapCellTemp.put(0+"", "");
		   	 }
		   	 if(flag1==false){
		   		 mapCellTemp.put(1+"", "");
		   	 }
		   	 if(flag2==false){
		   		 mapCellTemp.put(2+"", "");
		   	 }
		   	 if(flag3==false){
		   		 mapCellTemp.put(2+"", "");
		   	 }
		   	
		   	 			 
		   	 for(Map.Entry<String, String> entry : mapCellTemp.entrySet()){
		   		 vectorCellEachRowData.addElement(entry.getValue());
		   	 }
		   	 return vectorCellEachRowData;
		   }
    
    
    
	
}
