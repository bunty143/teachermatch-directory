
/**
 * @author Anurag
 *
 */


package tm.controller.textfile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import tm.bean.UserUploadFolderAccess;
import tm.bean.textfile.LicensureNBPTS;
import tm.bean.textfile.NBPTSAreaDomain;
import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.textfile.LicensureNBPTSDao;
import tm.dao.textfile.NBPTSAreaDomainDAO;
import tm.utility.Utility;

@Controller
public class ImportLicense {

	@Autowired
	UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	
	@Autowired
	LicensureNBPTSDao licensureNBPTSDao;

	@Autowired
	NBPTSAreaDomainDAO nbptsAreaDomainDAO;

	

	@RequestMapping(value = "/licenseFilesUpload.do", method = RequestMethod.GET)
	public @ResponseBody String importNBPTS(HttpServletRequest request,HttpServletResponse response, ModelMap map) {

		  

		final long startTime = System.currentTimeMillis();
		String ssnCode="national_certification.ntc_ssn_txt",
		nbptsAreaCode="national_certification.ntc_area_cd",
		effectiveDate="national_certification.ntc_effect_dte",
		expirationDate="national_certification.ntc_expire_dte";  // header attribute in txt file.
		int colCount=4;  // it must be equal than total header column in txt file
		int ssnCodepos=0,nbptsAreaCodepos =0,
		effectiveDatepos=0,expirationDatepos=0;  // header attributes position
		
		int headerErrorFlag=0;  	 
		StringBuilder errorFlag = new StringBuilder();
		int newRec = 0, updateRec = 0, totalRec = 0;
		int rowcount = 0; 
		
		SessionFactory sessionFactory=licensureNBPTSDao.getSessionFactory();
		StatelessSession dbSession = sessionFactory.openStatelessSession();
		dbSession.beginTransaction();
		String string="";
		
		HashMap<String, LicensureNBPTS> dbdata = new HashMap<String, LicensureNBPTS>();
		
		//get folder access Location dynamic from DB -----------  Start
	    System.out.println("::::: Use ImportLicense controller :::::");
	      List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
	   try {
	    Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
	    Criterion active = Restrictions.eq("status", "A");
	    uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
	   } catch (Exception e) {
	    e.printStackTrace();
	   } 
	   
	      String fileName = "HRMS--2260-NBPTS--NationalBoard.txt" , errorFileName="HRMS--2260-NBPTS--NationalBoard_error_"+startTime+".txt";
	      String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();
	      
	  	//get folder access Location dynamic from DB -----------  end
		
		
		
		try  { 
			  
			File file = new File(folderPath+fileName);
			  string = FileUtils.readFileToString(file);
			
			// check file header format mismatch  
			
			  int headerLine=0;
			  System.out.println(string.split("\n").length);
			for (String row : string.split("\n")) {
				rowcount++;
				System.out.println(" Line : "+rowcount);
				System.out.println("total columns = " + row.split("\\|",colCount).length);

				// skip empty Line in txt files .......... Start				
				if (row.trim().length() < 1)
					continue;				
				// skip empty Line in txt files .......... End

				headerLine++; // for count Total record count
				
				int ccount = 0;  
				for (String col : row.split("\\|",colCount)) {
					col=col.replaceAll("\r", "");
					if(headerLine==1){
						ccount++;
						if(col.replaceAll("\"", "").equalsIgnoreCase(ssnCode))
						ssnCodepos=ccount;	
					
						if(col.replaceAll("\"", "").equalsIgnoreCase(nbptsAreaCode))
							nbptsAreaCodepos=ccount;
						if(col.replaceAll("\"", "").equalsIgnoreCase(effectiveDate))
							effectiveDatepos=ccount;
						if(col.replaceAll("\"", "").equalsIgnoreCase(expirationDate))
							expirationDatepos=ccount;
					
						

					}
				}
				
				if(ssnCodepos==0 || nbptsAreaCodepos==0 || effectiveDatepos==0 || expirationDatepos==0){
				headerErrorFlag=1;				
				}
				break;
			
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
			
		if(headerErrorFlag==0)
		{
		
		  
		try {
			
			List<LicensureNBPTS> nbpts= licensureNBPTSDao.findAll();
			for(LicensureNBPTS data : nbpts)
			{ dbdata.put(Utility.decodeBase64(data.getSSN()), data);}
			   nbpts=null; // for garbage
			
			 
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			String datePattern="(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/((19|20)\\d\\d)";
			
			System.out.println("no of line :- " + string.split("\n").length); 
 
			for (String row : string.split("\n")) {
				rowcount++;
//				System.out.println(row); 

				if (row.trim().length() <= 1)
					continue;

				
				totalRec++; // for count Total record count
				if(totalRec==1) continue;     // skip header line
				
				
				int ccount = 0;
				LicensureNBPTS lic = new LicensureNBPTS();
				int colerror = 0;
				for (String col : row.split("\\|",colCount)) {
				  col=col.replaceAll("\r", ""); col=col.replaceAll("\"", ""); col=col.trim();
					if (row.split("\\|",colCount).length == 4) {
						ccount++;
						try {
							if (ccount == ssnCodepos) {
								if (col.length() == 9)
									lic.setSSN(Utility.encodeInBase64(col));
								else {
									errorFlag.append(System.getProperty("line.separator"));
									errorFlag.append("SSN Id Format Mismatched At line : "+ rowcount+ "   ( "+ row+ "   )");
									errorFlag.append(System.getProperty("line.separator"));
									colerror = 1;
									break;
								}
							}

							if (ccount == nbptsAreaCodepos) {
								lic.setNbotsAreaCode(col);
							}

							if (ccount == effectiveDatepos) {
								
								lic.setEffectiveDate(col.matches(datePattern) ? sdf.parse(col) : null);
							}

							if (ccount == expirationDatepos) {
								 
								lic.setExpirationDate(col.matches(datePattern) ? sdf.parse(col) : null);
							}
						} catch (Exception e) {
							System.out.println(" Exception occured " + e);
							colerror = 1;
						}

					}

					else {
						colerror = 1; 
						errorFlag.append("Format Mismatched At line : " + rowcount + "   ( " + row + "   )");
						errorFlag.append(System.getProperty("line.separator"));
						break;
					}

				}

				if (colerror == 0) {

					lic.setCreatedBy(1);
					lic.setCreatedDate(new Date());
					lic.setStatus("A");
					 
					if (!dbdata.containsKey(Utility.decodeBase64(lic.getSSN()))) {
						try {dbSession.insert(lic);	} catch (Exception e) { System.out.println("Insert data failure  ::::::::::: "+e.getMessage()); }
						newRec++; // count new Records

					} else {

//						System.out.println("updating   ..............................");
						
						lic.setLicensurenbptsId(dbdata.get(Utility.decodeBase64(lic.getSSN())).getLicensurenbptsId());

						try {dbSession.update(lic);} catch (Exception e) {System.out.println("Update data failure  ::::::::::: "+e.getMessage());}

						updateRec++; // count Update Record.

					}
				}

//				System.out.println();

			}

			// System.out.println("Error happen while reading License \n "+errorFlag.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
			dbSession.getTransaction().commit();
			dbSession.close();
			
		}
		
		
		
		
		
		
		
		

		System.out.println("length of License Error File : - "+ errorFlag.length());

		if (errorFlag.length() == 0)
			map.addAttribute("SuccessMessage", "File Import Successfully");

		else {

			map.addAttribute("ErrorMessage", " Errors In File Import");

		}

		errorFlag.insert(0, " Total Existing Records :-  " + updateRec);
		errorFlag.insert(0, System.getProperty("line.separator"));
		errorFlag.insert(0, " Total Records in file :-" + (totalRec-1));
		errorFlag.insert(0, System.getProperty("line.separator"));
		errorFlag.insert(0, " Total New Records in file :-" + newRec);
		errorFlag.insert(0, System.getProperty("line.separator"));

		errorFlag.append(System.getProperty("line.separator"));
		errorFlag.append(System.getProperty("line.separator"));
		errorFlag.append(System.getProperty("line.separator"));

		// writes errors in .txt file

		File f = new File(folderPath+errorFileName);

		try {

			// if file not Exist
			if (!f.exists()) {
				f.createNewFile();
			}

			FileWriter fwriter = new FileWriter(f);
			BufferedWriter bwriter = new BufferedWriter(fwriter);
			bwriter.write(errorFlag.toString());
			bwriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		
}
		
		else{
			// when header mismatch in txt file . -----  write to error file
			
			File f = new File(folderPath+errorFileName);

			try {

				// if file not Exist
				if (!f.exists()) {
					f.createNewFile();
				}

				FileWriter fwriter = new FileWriter(f);
				BufferedWriter bwriter = new BufferedWriter(fwriter);
				bwriter.write("--------------- header format mismatch in txt file -----------------");
				bwriter.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			
		}

		final long endtime = System.currentTimeMillis();

		System.out.println("total time Taken : " + (endtime - startTime));

		// erase data from dbdata
		dbdata=null;
		
		
		return errorFlag.toString();

	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// for NBPTS Area Domain

	@RequestMapping(value = "/importNBPTSAreaDomain.do", method = RequestMethod.GET)
	public @ResponseBody String importNBPTSAreaDomain(ModelMap map) {
 

			
		 
		
		final long startTime = System.currentTimeMillis();
		String nbptsAreaCode="national_area_dom.ntad_cd", nbptsAreaDesc= "national_area_dom.ntad_desc";  // header attribute in txt file.
		int colCount=2 ; // it must be equal to total no of column in text file header
		int  nbptsAreaCodepos =0,nbptsAreaDescpos=0;  // header attributes position
		int headerErrorFlag=0;  	 
		StringBuilder errorFlag = new StringBuilder();
		errorFlag.append(System.getProperty("line.separator"));	errorFlag.append(System.getProperty("line.separator"));		errorFlag.append(System.getProperty("line.separator"));
		errorFlag.append("");
		int newRec = 0, updateRec = 0, totalRec = 0;
		int rowcount = 0; 
		
		SessionFactory sessionFactory=nbptsAreaDomainDAO.getSessionFactory();
		StatelessSession dbSession = sessionFactory.openStatelessSession();
		dbSession.beginTransaction();
		String string="";
		
		HashMap<String, NBPTSAreaDomain> dbdata = new HashMap<String, NBPTSAreaDomain>();
		
		
		
		
		
		
		
		//get folder access Location dynamic from DB -----------  Start 
	      List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
	   try {
	    Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
	    Criterion active = Restrictions.eq("status", "A");
	    uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
	   } catch (Exception e) {
	    e.printStackTrace();
	   } 
	   
	      String fileName = "HRMS--2260-NBPTSD--NBPTSDom.txt" , errorFileName="HRMS--2260-NBPTSD--NBPTSDom_error_"+startTime+".txt";
	      String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();
	      
	  	//get folder access Location dynamic from DB -----------  end
		
			
			
			try  { 
				  
				File file = new File(folderPath+fileName);
				  string = FileUtils.readFileToString(file);
				
				// check file header format mismatch  
				
				  int headerLine=0;
				  
				for (String row : string.split("\n")) {
					rowcount++; 

					// skip empty Line in txt files .......... Start				
					if (row.trim().length() < 1)
						continue;				
					// skip empty Line in txt files .......... End

					headerLine++; // for count Total record count
					
					int ccount = 0;  
					for (String col : row.split("\\|",colCount)) {
						col=col.replaceAll("\r", ""); col= col.replaceAll("\"", ""); col=col.trim();
						if(headerLine==1){
							ccount++;
							if(col.equalsIgnoreCase(nbptsAreaCode))
								nbptsAreaCodepos=ccount;	
						
							if(col.equalsIgnoreCase(nbptsAreaDesc))
								nbptsAreaDescpos=ccount;
							

						}
					}
					
					if(nbptsAreaCodepos==0 || nbptsAreaDescpos==0){
					headerErrorFlag=1;				
					}
					break;
				
				}
				
			}catch (Exception e) {
				e.printStackTrace();
			}
		
		
		
		if(headerErrorFlag==0)
		{
		
		try {
			
			List<NBPTSAreaDomain> arealist = nbptsAreaDomainDAO.findAll();
			for(NBPTSAreaDomain area : arealist){		 
			   dbdata.put(area.getNbptsAreaCode(),area);						
			}
			arealist=null;   // erase data from list
			

			File file = new File(folderPath+fileName);
			 string = FileUtils.readFileToString(file);
 
  
		 
			for (String row : string.split("\n")) { 
				rowcount++;
				
				// skip empty Line in txt files .......... Start				
				if (row.trim().length() <= 2)	continue;				
				// skip empty Line in txt files .......... End 
				totalRec++; // for count Total record count
				
				// escape first line 
			  if(totalRec==1) continue;	
			
				
			 
				int ccount = 0;
				NBPTSAreaDomain lic = new NBPTSAreaDomain();
				int colerror = 0;

				for (String col : row.split("\\|",colCount)) {  
					col=col.replaceAll("\r", ""); col= col.replaceAll("\"", ""); col=col.trim(); 
						if (row.split("\\|",colCount).length == 2) {   					
						
						ccount++;
						try {
							if (ccount == nbptsAreaCodepos) {
								lic.setNbptsAreaCode(col);
							}

							if (ccount == nbptsAreaDescpos) {
								lic.setNbptsAreaDescription(col);
							}

						} catch (Exception e) {
							System.out.println(" Exception occured " + e);
							colerror = 1;
						}

					}

					else {
						colerror = 1; 
						errorFlag.append(System.getProperty("line.separator"));
						errorFlag.append("Format Mismatched At line : "	+ rowcount + " ( " + row + "   )");
						errorFlag.append(System.getProperty("line.separator"));
						break;
					}

				}

				if (colerror == 0) {

					lic.setCreatedBy(1);
					lic.setCreatedDate(new Date());
					lic.setStatus("A");

				 
					if (!dbdata.containsKey(lic.getNbptsAreaCode())){
						
						System.out.println("New  Data insering   ..............................");
						
						try {
							dbSession.insert(lic);
						} catch (Exception e) { System.out.println(" Insert Data Failure "+e.getMessage());		}
						newRec++;
						
					}
					else {

						System.out.println("updating  existing record ..............................");
						lic.setNbptsAreaDomainId(dbdata.get(lic.getNbptsAreaCode()).getNbptsAreaDomainId());
						try {	dbSession.update(lic);} catch (Exception e) {	System.out.println(" Update Data Failure "+e.getMessage());			}
						updateRec++;
						
					}
				}
 

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
finally{
	dbSession.getTransaction().commit();
	dbSession.close();
	
} 	errorFlag.insert(0, " Total Existing Records :-  " + updateRec);
		errorFlag.insert(0, System.getProperty("line.separator"));
		errorFlag.insert(0, " Total Records in file :-" + (totalRec-1));
		errorFlag.insert(0, System.getProperty("line.separator"));
		errorFlag.insert(0, " Total New Records in file :-" + newRec);
		errorFlag.insert(0, System.getProperty("line.separator"));

		
		
		// writes errors in .txt file

		
		try {
			File f = new File(folderPath+errorFileName);

			// if file not Exist
			if (!f.exists()) {
				f.createNewFile();
			}

			FileWriter fwriter = new FileWriter(f);
			BufferedWriter bwriter = new BufferedWriter(fwriter);
			bwriter.write(errorFlag.toString());
			bwriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
		else{
			// when header mismatch in txt file . -----  write to error file
			
			File f = new File(folderPath+errorFileName);

			try {

				// if file not Exist
				if (!f.exists()) {
					f.createNewFile();
				}

				FileWriter fwriter = new FileWriter(f);
				BufferedWriter bwriter = new BufferedWriter(fwriter);
				bwriter.write("--------------- header format mismatch in txt file -----------------");
				bwriter.write("--------------- please insert the header (given below) at first line in your txt file  -----------------\r\n\n");
				
				bwriter.write("national_area_dom.ntad_cd|national_area_dom.ntad_desc");
				
				bwriter.close();
			} catch (Exception e) {
				e.printStackTrace();
			}	
			
		}
		
		
		final long endtime = System.currentTimeMillis();
		System.out.println("total time Taken : " + (endtime - startTime));
		
		//erase data from map
		  dbdata=null;		
		  
		return errorFlag.toString();
	}

}
