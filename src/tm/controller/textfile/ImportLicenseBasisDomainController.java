package tm.controller.textfile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import tm.bean.UserUploadFolderAccess;
import tm.bean.textfile.LicenseBasisDomain;
import tm.bean.user.UserMaster;
import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.textfile.LicenseBasisDomainDAO;
import tm.dao.user.UserMasterDAO;

/* 
 * @Author: Sandeep Yadav
 */

@Controller
public class ImportLicenseBasisDomainController {

	
	@Autowired
	private LicenseBasisDomainDAO licenseBasisDomainDAO;
	

	public void setLicenseBasisDomainDAO(
			LicenseBasisDomainDAO licenseBasisDomainDAO) {
		this.licenseBasisDomainDAO = licenseBasisDomainDAO;
	}

	@Autowired
	private UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	
	public void setUserUploadFolderAccessDAO(
			UserUploadFolderAccessDAO userUploadFolderAccessDAO) {
		this.userUploadFolderAccessDAO = userUploadFolderAccessDAO;
	}

	@Autowired
	private UserMasterDAO userMasterDAO;
	

	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}



	@RequestMapping(value="/licensebasisdomain.do", method=RequestMethod.GET)
	public @ResponseBody String getAllLicenseStatusData(ModelMap map,HttpServletRequest request)
	{
		    System.out.println("::::: Use ImportLicenseBasisDomainController  :::::");
		    List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
			try {
				Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
				Criterion active = Restrictions.eq("status", "A");
				uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
			} catch (Exception e) {
				e.printStackTrace();
			}	
		    String fileName = "HRMS--2260-PGBD--ProgramBasisDom.txt";
		    String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();
		   // saveLicenseBasis(fileName,folderPath);
			
		
		return saveLicenseBasis(fileName,folderPath);
	}
	
	
	private Vector vectorDataTxt = new Vector();
    public String saveLicenseBasis(String fileName,String folderPath){	
			
			 System.out.println("::::::::saveLicenseBasis:::::::::");
			 
			 String filePath=folderPath+fileName;
		 	 System.out.println("folderPath  "+folderPath);
		 	 try {
		 		 if(fileName.contains(".txt") || fileName.contains(".TXT")){
		 			   vectorDataTxt = readDataTxt(filePath);
		 		 }else{
	    			 //break;
	    	 }
		} catch (Exception e) {
			e.printStackTrace();
		}
    	 
		SessionFactory sessionFactory=licenseBasisDomainDAO.getSessionFactory();
		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
 	    Transaction txOpen =statelesSsession.beginTransaction();
 	   
  	    List<LicenseBasisDomain> licenseBasisDomainList =  licenseBasisDomainDAO.findAll();
  	    Map<String, LicenseBasisDomain> licenseBasisDomainMap = new HashMap<String, LicenseBasisDomain>();
  	    if(licenseBasisDomainList!=null && licenseBasisDomainList.size()>0)
  	    for (LicenseBasisDomain licenseBasisDomain2 : licenseBasisDomainList) {
  	    	licenseBasisDomainMap.put(licenseBasisDomain2.getProgramBasisCode(), licenseBasisDomain2);
		}
	  	 
  	    String final_error_store="";
	 	int numOfError=0;
	 	boolean row_error=false;
		
	 	int insertCount = 0;
	    int updateCount = 0;
		int programBasisCode_count=11;	    	 
	   	int programBasisDescription_count=11;
    	 
    	
	   	 txOpen =statelesSsession.beginTransaction();  
	   	 UserMaster userMaster = userMasterDAO.findById(1, false, false);
	   	 
		 for(int i=0; i<vectorDataTxt.size(); i++) {
			 Vector vectorCellEachRowData = (Vector) vectorDataTxt.get(i);
	           
			 String programBasisCode="";
	         String programBasisDescription="";
	         String errorText="";
		  	 String rowErrorText="";   
	  	     
	  	      
	  	      
	  	    for(int j=0; j<vectorCellEachRowData.size(); j++) {
  	        	try{
  	        		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("license_pgm_basis_dom.dom_pgm_basis_id")){
  	        			programBasisCode_count=j;
	            	}
	            	if(programBasisCode_count==j)
	            		programBasisCode=vectorCellEachRowData.get(j).toString().trim();
  	        		
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("license_pgm_basis_dom.dom_pgm_basis_desc")){
	            		programBasisDescription_count=j;
	            	}
	            	if(programBasisDescription_count==j)
	            		programBasisDescription=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	
	            	
            	}catch(Exception e){
            		e.printStackTrace();
            	}
  	        }
	  	    
           if(i==0){
	  		  
	  		  if(!programBasisCode.equalsIgnoreCase("license_pgm_basis_dom.dom_pgm_basis_id")){
	  			rowErrorText="license_pgm_basis_dom.dom_pgm_basis_id column is not found";
	  			row_error=true;
	  		  }
	  		  if(!programBasisDescription.equalsIgnoreCase("license_pgm_basis_dom.dom_pgm_basis_desc")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="license_pgm_basis_dom.dom_pgm_basis_desc column is not found";
	  			row_error=true;
	  		  }
	  		 
	  		 }
	  	    
	  	  
		  	if(row_error){
		  		numOfError++;
		  		File file = new File(folderPath+"licenseBasisDomainError.txt");
				// if file doesnt exists, then create it
		  		try {
			  		if (!file.exists()) {
			  			file.createNewFile();
					}	    				
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(rowErrorText);
		
					bw.close();
		  		} catch (IOException e) {
					
					e.printStackTrace();
				}
				
		  		 break;
		  	 }
	  	    
	  	  
	  	    
	  	  if(i != 0 && row_error==false){
	  	    	
	  	    	boolean errorFlag=false;
	  	    	
	  	    	if(programBasisCode.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Program Basis Code is empty";
	    			    errorFlag=true;
	        	}
	  	    	
	  	    	
	  	    	if(programBasisDescription.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Program Basis Description is empty";
	    			    errorFlag=true;
	        	}
	  	    	
           
            if(!errorText.equals("")){
	        			int row = i+1;
	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+programBasisCode+","+programBasisDescription+"<>";
	    			numOfError++;
	    		}
	  	    
	  	    
	  	    
	  	    
            try {
  	    		
                LicenseBasisDomain licBasisDomainObj = null;
            	
            	if(programBasisCode.replaceAll("\"", "").isEmpty()){
            		continue;
            	}
            	
            	if(licenseBasisDomainMap.containsKey(programBasisCode.replaceAll("\"", ""))){
            		licBasisDomainObj = licenseBasisDomainMap.get(programBasisCode.replaceAll("\"", ""));
            	}
  	    		
            	if(licBasisDomainObj == null){
            		licBasisDomainObj = new LicenseBasisDomain();
            		licBasisDomainObj.setProgramBasisCode(programBasisCode.replaceAll("\"", ""));
            		licBasisDomainObj.setProgramBasisDescription(programBasisDescription.replaceAll("\"", ""));
					
            		licBasisDomainObj.setStatus("A");
            		licBasisDomainObj.setCreatedDate(new Date());
            		licBasisDomainObj.setUserMaster(userMaster);
            		//txOpen =statelesSsession.beginTransaction();
            	 	statelesSsession.insert(licBasisDomainObj);
            		insertCount++;
            	 //	txOpen.commit();
            	}else{
            		
            		licBasisDomainObj.setProgramBasisDescription(programBasisDescription.replaceAll("\"", ""));
            		licBasisDomainObj.setStatus("A");
            		licBasisDomainObj.setCreatedDate(new Date());
            		licBasisDomainObj.setUserMaster(userMaster);
            		//txOpen =statelesSsession.beginTransaction();
            	 	statelesSsession.update(licBasisDomainObj);
            	 	updateCount++;
            	 	//txOpen.commit();
            		
            	}
            	
            	
	  	    	} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
  	    }
  	    
  	    
	 }   

	 try{
			if(!final_error_store.equalsIgnoreCase("")){
	 	 		String content = final_error_store;		    				
				File file = new File(folderPath+"licenseBasisDomainError.txt");
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
				String[] parts = content.split("<>");	    				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write("license_pgm_basis_dom.dom_pgm_basis_id,license_pgm_basis_dom.dom_pgm_basis_desc");
				bw.write("\r\n\r\n");
				int k =0;
				for(String cont :parts) {
					bw.write(cont+"\r\n\r\n");
				    k++;
				}
				bw.close();
				
	 	 	}	
		  } catch (Exception e) {
				e.printStackTrace();
			}	

		  txOpen.commit();
		  String message = "After File Upload,  "+insertCount+"  row inserted and  "+updateCount+"  row updated.";
		  System.out.print(message);
	      return message;
}

    
    
    
    public static Vector readDataTxt(String filePath) throws FileNotFoundException{
    	
		Vector vectorData = new Vector();
		String filepathhdr=filePath;

			 int programStatusCode_count=11;	    	 
		   	 int programStatusDescription_count=11;
		   	
		
		    
	        BufferedReader brhdr=new BufferedReader(new FileReader(filepathhdr));
	        
	        String strLineHdr="";	        
	        String hdrstr="";
	        
	       
	        int lineNumberHdr=0;
	        try{
	            
	            while((strLineHdr=brhdr.readLine())!=null){  
	            	
	            	if(strLineHdr.isEmpty())
	            		continue;
	            	Vector vectorCellEachRowData = new Vector();
	                int cIndex=0;
	                boolean cellFlag=false,dateFlag=false;
	                Map<String,String> mapCell = new TreeMap<String, String>();
	                
	                
	                String[] parts = strLineHdr.split("\\|");
	                
	                for(int j=0; j<parts.length; j++) {
	                	hdrstr=parts[j];
	                	cIndex=j;
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("license_pgm_basis_dom.dom_pgm_basis_id")){
	                        	programStatusCode_count=cIndex;
	                    		
	                    	}
	                        
	                        if(programStatusCode_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("license_pgm_basis_dom.dom_pgm_basis_desc")){
	                        	programStatusDescription_count=cIndex;
	                    		
	                    	}
	                    	if(programStatusDescription_count==cIndex){
	                    		cellFlag=true;
	                    		
	                    	}	
	                    	  
	                    	
	                    	
	                    	
	                    	if(cellFlag){
	                    		if(!dateFlag){
	                    			try{
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}catch(Exception e){
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}
	                    		}else{
	                    			mapCell.put(cIndex+"",hdrstr);
	                    		}
	                    	}
	                        
	                    }
	                    
	                    vectorCellEachRowData=cellValuePopulate(mapCell);
	                    vectorData.addElement(vectorCellEachRowData);
	                lineNumberHdr++;
	            }
	        }
	        catch(Exception e){
	            System.out.println(e.getMessage());
	        }
	        System.out.println("::::::::: read data from text ::::::::::       "+vectorData);       
		return vectorData;
	}
    
    
		    public static Vector cellValuePopulate(Map<String,String> mapCell){
		   	 Vector vectorCellEachRowData = new Vector();
		   	 Map<String,String> mapCellTemp = new TreeMap<String, String>();
		   	 boolean flag0=false,flag1=false;
		   	 for(Map.Entry<String, String> entry : mapCell.entrySet()){
		   		 String key=entry.getKey();
		   		String cellValue=null;
		   		if(entry.getValue()!=null)
		   			cellValue=entry.getValue().trim();
		   		
		   		if(key.equals("0")){
		   			mapCellTemp.put(key, cellValue);
		   			flag0=true;
		   		}
		   		if(key.equals("1")){
		   			flag1=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		
		   		
		   		
		   	 }
		   	 if(flag0==false){
		   		 mapCellTemp.put(0+"", "");
		   	 }
		   	 if(flag1==false){
		   		 mapCellTemp.put(1+"", "");
		   	 }
		   	 
		   	 			 
		   	 for(Map.Entry<String, String> entry : mapCellTemp.entrySet()){
		   		 vectorCellEachRowData.addElement(entry.getValue());
		   	 }
		   	 return vectorCellEachRowData;
		   }
    
    
    
	
}
