package tm.controller.textfile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import tm.bean.UserUploadFolderAccess; 
import tm.bean.textfile.DegreeTypeMaster;
import tm.dao.UserUploadFolderAccessDAO; 
import tm.dao.textfile.DegreeTypeMasterDAO;

 

/**
 * @author Anurag Kumar
 *
 */


@Controller
public class DegreeTypeMasterController {
	 
	@Autowired
	DegreeTypeMasterDAO degreeTypeMasterDAO;
	
	@Autowired
	UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	
	
	@RequestMapping( value="/degreetypedomain.do",method=RequestMethod.GET)
	public @ResponseBody String importFieldOfStudy(Model map){
		
		final long startTime = System.currentTimeMillis();
		String dtCode="education_level_dom.dom_educ_lvl_id",dtName="education_level_dom.dom_educ_lvl_desc";  // header attribute in txt file.
		int colCount=2;
		int dtCodepos=0,dtNamepos =0;  // header attributes position
		int headerErrorFlag=0;  	 
		StringBuilder errorFlag = new StringBuilder();
		int newRec = 0, updateRec = 0, totalRec = 0;
		int rowcount = 0; 
		
		SessionFactory sessionFactory=degreeTypeMasterDAO.getSessionFactory();
		StatelessSession dbSession = sessionFactory.openStatelessSession();
		dbSession.beginTransaction();
		String string="";
		
		HashMap<String, DegreeTypeMaster> dbdata = new HashMap<String, DegreeTypeMaster>();
		
		
		//get folder access Location dynamic from DB -----------  Start
	    System.out.println("::::: Use licensureFSDomain.do controller :::::");
	      List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
	   try {
	    Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
	    Criterion active = Restrictions.eq("status", "A");
	    uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
	   } catch (Exception e) {
	    e.printStackTrace();
	   } 
	   
	      String fileName = "HRMS--2260-DEGD--DegreeDom.txt" , errorFileName="HRMS--2260-DEGD--DegreeDom_error_"+startTime+".txt";
	      String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();
	     
	      uploadFolderAccessesrec=null ; // erase used data
	     
	  	//get folder access Location dynamic from DB -----------  end
		
		
		
		
		try  { 
			  
			File file = new File(folderPath+fileName);
			  string = FileUtils.readFileToString(file);
			
			// check file header format mismatch  
			
			  int headerLine=0;
			  
			for (String row : string.split("\n")) {
				rowcount++;
//				System.out.println(" Line : "+rowcount);
//				System.out.println("total columns = " + row.split("\\|",colCount).length);

				// skip empty Line in txt files .......... Start				
				if (row.trim().length() < 1)
					continue;				
				// skip empty Line in txt files .......... End

				headerLine++; // for count Total record count
				
				int ccount = 0;  
				for (String col : row.split("\\|",colCount)) {
					col=col.replaceAll("\r", ""); col=col.replaceAll("\"", "");
					if(headerLine==1){
						ccount++;
						if(col.equalsIgnoreCase(dtCode))
						dtCodepos=ccount;	
					
						if(col.equalsIgnoreCase(dtName))
							dtNamepos=ccount;
						

					}
				}
				
				if(dtCodepos==0 || dtNamepos==0){
				headerErrorFlag=1;				
				}
				break;
			
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
				
		if(headerErrorFlag==0)
		{
			
			try{
		
		// get all data from database and put in hashmap with SSN and id As key-value   ................ Start
		System.out.println("-------------------"+string.trim().length());
		if(string.trim().length()>10){
			
			List<DegreeTypeMaster> alldata = degreeTypeMasterDAO.findAll(); 
			if(alldata.size()>0){
				for(DegreeTypeMaster dgdata : alldata){						
					dbdata.put(dgdata.getDegreeTypeCode(), dgdata);						
				}
				
						alldata=null; // erase unused data
				
				 
			}
		}
		
		// get all data from database and put in hashmap with SSN and id As key-value   ................ End
		
		
//		System.out.println("no of lines :- " + string.split("\n").length); 

		
		
		for (String row : string.split("\n")) {
			rowcount++;
			System.out.println(" Line : "+rowcount);
			System.out.println("total columns = " + row.split("\\|",colCount).length);

			// skip empty Line in txt files .......... Start				
			if (row.trim().length() <= 1)
				continue;				
			// skip empty Line in txt files .......... End

			totalRec++; // for count Total record count
			
			// escape first line 
		  if(totalRec==1) continue;	
		
			
			
			int ccount = 0;
			DegreeTypeMaster fsdata = new DegreeTypeMaster();
			int colerror = 0;
			for (String col : row.split("\\|",colCount)) {
				 
				col=col.replaceAll("\r", ""); col=col.replaceAll("\"", ""); col=col.trim();
				 
				if (row.split("\\|",colCount).length == 2) {
					ccount++;
					try {
						if (ccount ==dtCodepos ) {								 
							fsdata.setDegreeTypeCode(col);	
						 }

						if (ccount == dtNamepos) {
							fsdata.setDegreeName(col);								
						}
					 		
					} catch (Exception e) {
						System.out.println(" Exception occured " + e);
						colerror = 1;
					}

				}

				else {
					colerror = 1; 
					errorFlag.append("Format Mismatched At line : "	+ rowcount + "   ( " + row + "   )");
					errorFlag.append(System.getProperty("line.separator"));
					break;
				}
				
				
					
					
				}

			 

			if (colerror == 0) {

				 
				fsdata.setStatus("A");
				  
				if (!dbdata.containsKey(fsdata.getDegreeTypeCode())) {
					
					System.out.println("new Data inserting ...........");
					try { dbSession.insert(fsdata); } catch (Exception e) { System.out.println(" Insert data Failure  :::: "+ e.getMessage()); } 

					newRec++; // count new Records

				} else {

					System.out.println("updating exting data...........");
					
					DegreeTypeMaster existData=dbdata.get(fsdata.getDegreeTypeCode());
					existData.setDegreeName( fsdata.getDegreeName()); 
					existData.setStatus("A");
				
					try { dbSession.update(existData); } catch (Exception e) { System.out.println(" Update Data Failure  :::::::  " + e.getMessage()); }
					
					updateRec++; // count Update Record.

				}
			} 

		}
		
		
		}
		
		

	 catch (Exception e) {
		e.printStackTrace();
	}
	finally{
		
		dbSession.getTransaction().commit();
		dbSession.close();
		
		
	}


	
	
	
	if (errorFlag.length() == 0)
		map.addAttribute("SuccessMessage", "File Import Successfully");

	else {

		map.addAttribute("ErrorMessage", " Errors In File Import");

	}

	errorFlag.insert(0, " Total Existing Records :-  " + updateRec);
	errorFlag.insert(0, System.getProperty("line.separator"));
	errorFlag.insert(0, " Total Records in file :-" + (totalRec-1));
	errorFlag.insert(0, System.getProperty("line.separator"));
	errorFlag.insert(0, " Total New Records in file :-" + newRec);
	errorFlag.insert(0, System.getProperty("line.separator"));

	errorFlag.append(System.getProperty("line.separator"));
	errorFlag.append(System.getProperty("line.separator"));
	errorFlag.append(System.getProperty("line.separator"));

	// writes errors in .txt file

	File f = new File(folderPath+errorFileName);

	try {

		// if file not Exist
		if (!f.exists()) {
			f.createNewFile();
		}

		FileWriter fwriter = new FileWriter(f);
		BufferedWriter bwriter = new BufferedWriter(fwriter);
		bwriter.write(errorFlag.toString());
		bwriter.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	
	
	
	
		}
		else{
			// header mismatch in txt file .
			
			File f = new File(folderPath+errorFileName);

			try {

				// if file not Exist
				if (!f.exists()) {
					f.createNewFile();
				}

				FileWriter fwriter = new FileWriter(f);
				BufferedWriter bwriter = new BufferedWriter(fwriter);
				bwriter.write("--------------- header format mismatch in txt file -----------------");
				bwriter.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			
		}
		
		final long endtime = System.currentTimeMillis();

		System.out.println("total time Taken : " + (endtime - startTime));
		
		
		dbdata=null;  // erase data from map
		string=null;   // erase read file data from string
		
		
		return errorFlag.toString();
		
	}
	
}
