/* 
 * @Author: Anurag Kumar
 */


package tm.controller.textfile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import tm.bean.UserUploadFolderAccess;
import tm.bean.textfile.LicensureEducation;
import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.textfile.LicensureEducationDAO;
import tm.utility.Utility;

@Controller
public class LicensureEducationController {

	
	@Autowired
	 LicensureEducationDAO  licensureEducationDAO;
	
	@Autowired
	UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	
	
	@RequestMapping(value="/licensureEducation.do", method = RequestMethod.GET)
	public @ResponseBody String licensureEducationmport(ModelMap map){

		 
		final long startTime = System.currentTimeMillis();

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String datePattern="(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/((19|20)\\d\\d)";
		
		
		
		StringBuilder errorFlag = new StringBuilder();
		int newRec = 0, updateRec = 0, totalRec = 0;
		int rowcount = 0; 
		
		// header of txt file.		
		String SSN="personnel_education.Ped_ssn_txt",DegreeType="personnel_education.ped_educ_lvl_cd",IHECode = "personnel_education.ped_ihe_cd",GraduationDate = "personnel_education.Ped_grad_date",   Major="personnel_education.ped_major1_cd";
		int ssnpos=0,degtypepos=0,ihecodepos=0,graddatepos=0,majorpos=0;
		int noOfCol=5;
		int headerErrorFlag=0;
		
		
		SessionFactory sessionFactory=licensureEducationDAO.getSessionFactory();
		StatelessSession dbSession = sessionFactory.openStatelessSession();
		dbSession.beginTransaction();
		String string="";
		
		HashMap<String, Integer> dbdata = new HashMap<String, Integer>();
		
		//get folder access Location dynamic from DB -----------  Start
	    System.out.println("::::: Use licensureEducation.do controller :::::");
	      List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
	   try {
	    Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
	    Criterion active = Restrictions.eq("status", "A");
	    uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
	   } catch (Exception e) {
	    e.printStackTrace();
	   } 
	   
	      String fileName = "HRMS--2260-EDUCATION--LicenseEducation.txt" , errorFileName="HRMS--2260-EDUCATION--LicenseEducation_error_"+startTime+".txt";
	      String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();
	      
	  	//get folder access Location dynamic from DB -----------  end
		
		
		try  { 
			  
			File file = new File(folderPath+fileName);
			  string = FileUtils.readFileToString(file);
			
			// check file header format mismatch  
			
			int headerLine=0;
			  
			for (String row : string.split("\n")) {
				rowcount++; 
				 System.out.println(row);
				// skip empty Line in txt files .......... Start				
				if (row.trim().length() < 1)
					continue;				
				// skip empty Line in txt files .......... End

				headerLine++; // for count Total record count
				
				int ccount = 0;  
				for (String col : row.split("\\|",noOfCol)) {
					col=col.replaceAll("\r", "");col=col.replaceAll("\"", ""); col=col.trim();
					if(headerLine==1){
						ccount++;
						if(col.equalsIgnoreCase(SSN))
						ssnpos=ccount;
						
						if(col.equalsIgnoreCase(DegreeType))
							degtypepos=ccount;
						
						
						if(col.equalsIgnoreCase(IHECode))
							ihecodepos=ccount;
						
						
						if(col.equalsIgnoreCase(GraduationDate))
							graddatepos=ccount;
						
						 
						if(col.equalsIgnoreCase(Major))
							majorpos=ccount;
	
					}
				}
				
				if(ssnpos==0 || degtypepos==0 || ihecodepos==0 || graddatepos==0 ||  majorpos==0){
				headerErrorFlag=1;				
				}
				break;
			
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
				
			
		
			if(headerErrorFlag==0)
			{
				
				try{
			
			// get all data from database and put in hashmap with SSN and id As key-value   ................ Start
			 
			if(string.trim().length()>10){
				
				List<LicensureEducation> alldata = licensureEducationDAO.findAll();
				if(alldata.size()>0){
					for(LicensureEducation licdata : alldata){						
						dbdata.put(Utility.decodeBase64( licdata.getSSN()), licdata.getLicensureeducationId());						
					}
				}
				alldata=null;    // erase list
			}
			
			// get all data from database and put in hashmap with SSN and id As key-value   ................ End
			  
			
			for (String row : string.split("\n")) {
				rowcount++;
				System.out.println(" Line : "+rowcount);
//				System.out.println("total columns = " + row.split("\\|",noOfCol).length);

				// skip empty Line in txt files .......... Start				
				if (row.trim().length() <= 1)
					continue;				
				// skip empty Line in txt files .......... End

				totalRec++; // for count Total record count
				
				// escape first line 
			  if(totalRec==1) continue;				
				
				
				int ccount = 0;
				LicensureEducation lic = new LicensureEducation();
				int colerror = 0;
				for (String col : row.split("\\|",noOfCol)) {  System.out.println(row.split("\\|",noOfCol).length);
					
					
					col=col.replaceAll("\r", ""); col=col.replaceAll("\"", ""); col=col.trim();
					 
					 
					if (row.split("\\|",noOfCol).length == noOfCol) {
						ccount++;
						try {
							if (ccount == ssnpos) {
								if (col.length() == 9)
									lic.setSSN( Utility.encodeInBase64(col));
								else {
									errorFlag.append(System.getProperty("line.separator"));
									errorFlag.append("SSN Id Format Mismatched At line : "+ rowcount+ "   ( "+ row + "   )");
									errorFlag.append(System.getProperty("line.separator"));
									colerror = 1;
									break;
								}								
							 
							}

							if (ccount == degtypepos) {
								lic.setDegreeType(col);								
								 
							}
							
							if(ccount==ihecodepos){
								lic.setIHECode(col);
								 
							}
						 

							if (ccount == graddatepos) { 
								if(col.matches(datePattern))
								lic.setGraduationDate(sdf.parse(col));	
								else{
									colerror = 1;
									
									errorFlag.append("\n At Line : "+rowcount+" Date field pattern mismatch MM/dd/yyyy   like 08/21/1999  \n");
									
								}
							}
							
						 							
							
							if (ccount == majorpos) {
								lic.setMajor(col);
								 
							}						
							
							
						} catch (Exception e) {
							System.out.println(" Exception occured " + e);
							colerror = 1;
						}

					}

					else {
						colerror = 1; 
						errorFlag.append("Format Mismatched At line : "	+ rowcount + "   ( " + row + "   )");
						errorFlag.append(System.getProperty("line.separator"));
						break;
					}				
							
					}

				 

				if (colerror == 0) {

					lic.setCreatedBy(1);
					lic.setCreatedDate(new Date());
					lic.setStatus("A");
					  
					if (!dbdata.containsKey( Utility.decodeBase64(lic.getSSN()))) {
						
						System.out.println("new data   ..............................");
						try { dbSession.insert(lic); } catch (Exception e) { System.out.println("Insert data Failure ::::::"+e.getMessage()); } 

						newRec++; // count new Records

					} else {

						System.out.println("updating data  ..............................");
						lic.setLicensureeducationId(dbdata.get( Utility.decodeBase64(lic.getSSN())));
					
						try { dbSession.update(lic); } catch (Exception e) { System.out.println(" Update Data failure " + e.getMessage()); 	}
						 
						updateRec++; // count Update Record.

					}
				}

				System.out.println();
			}
			
			
			dbdata=null;  // erase data from file
			}
			
		 catch (Exception e) {
			e.printStackTrace();
		}
		finally{			
			dbSession.getTransaction().commit();
			dbSession.close();			
		}
		
 

		errorFlag.insert(0, " Total Existing Records :-  " + updateRec);
		errorFlag.insert(0, System.getProperty("line.separator"));
		errorFlag.insert(0, " Total Records in file :-" + (totalRec-1));
		errorFlag.insert(0, System.getProperty("line.separator"));
		errorFlag.insert(0, " Total New Records in file :-" + newRec);
		errorFlag.insert(0, System.getProperty("line.separator"));
		errorFlag.append(System.getProperty("line.separator"));
		errorFlag.append(System.getProperty("line.separator"));
		errorFlag.append(System.getProperty("line.separator"));

		// writes errors in .txt file

		File f = new File(folderPath+errorFileName);

		try {

			// if file not Exist
			if (!f.exists()) {
				f.createNewFile();
			}

			FileWriter fwriter = new FileWriter(f);
			BufferedWriter bwriter = new BufferedWriter(fwriter);
			bwriter.write(errorFlag.toString());
			bwriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
			}
			else{
				// header mismatch in txt file .
				
				File f = new File(folderPath+errorFileName);

				try {

					// if file not Exist
					if (!f.exists()) {
						f.createNewFile();
					}

					FileWriter fwriter = new FileWriter(f);
					BufferedWriter bwriter = new BufferedWriter(fwriter);
					bwriter.write("--------------- header format mismatch in txt file -----------------\n");
					bwriter.write("--------------- please insert the header at first line in your txt file  -----------------\n\n");
					
					bwriter.write(SSN+"|"+DegreeType+"|"+IHECode+"|"+GraduationDate+"|"+Major);
					bwriter.close();
				} catch (Exception e) {
					e.printStackTrace();
				}							
				
			}

		final long endtime = System.currentTimeMillis();

		System.out.println("total time Taken : " + (endtime - startTime));

		dbdata=null;  // erase data from map
		string=null;   // erase read file data from string
		

		
		
		return errorFlag.toString();
	}
}
