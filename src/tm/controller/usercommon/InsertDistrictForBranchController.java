package tm.controller.usercommon;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HqBranchesDistricts;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HqBranchesDistrictsDAO;
import tm.dao.master.DistrictMasterDAO;

 
@Controller
public class InsertDistrictForBranchController {
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private HqBranchesDistrictsDAO hqBranchesDistrictsDAO;

	@RequestMapping(value="insertBranchDistrict.do")
	public @ResponseBody String insertDist(ModelMap map,	HttpServletRequest request){

		String result = "";
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;

		if (session == null	|| ((session.getAttribute("userMaster") == null ))) {
			System.out.println("redirect:index.jsp");
			return "redirect:index.jsp";
		}
		else{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		Map<Integer,HqBranchesDistricts> hqBranchMap = new HashMap<Integer, HqBranchesDistricts>();
		Map<String,DistrictMaster> branchDistrictMap = new HashMap<String, DistrictMaster>();
		List<String> branchNames = new ArrayList<String>();
		List<BranchMaster> branches = branchMasterDAO.findAll()	;
		if(branches!=null  && branches.size()>0){
			for(BranchMaster br : branches){
				branchNames.add(br.getBranchName()); 	// Branch names
			}
		}
		List<HqBranchesDistricts> listHqBranchesDistricts = hqBranchesDistrictsDAO.findAll();
		List<DistrictMaster> listBranchesDistrict = districtMasterDAO.getDistrictListByBranches(branchNames);
		if(listBranchesDistrict!=null && listBranchesDistrict.size()>0){
			for(DistrictMaster dist : listBranchesDistrict){
				branchDistrictMap.put(dist.getDistrictName(), dist);
			}
		}
		if(listHqBranchesDistricts!=null && listHqBranchesDistricts.size()>0){
			for(HqBranchesDistricts hqbr : listHqBranchesDistricts){
				if(hqbr.getBranchMaster()!=null)
					hqBranchMap.put(hqbr.getBranchMaster().getBranchId(), hqbr);
			}
		}
		SessionFactory sessionFactory=districtMasterDAO.getSessionFactory();
		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
		Transaction txOpen =statelesSsession.beginTransaction();
		HqBranchesDistricts hq = null;
		try {
			for(BranchMaster branch : branches)
			{
				DistrictMaster districtMasterObj= null;
				System.out.println(branch.getBranchId() + "   :   " + branch.getBranchName());
				if(branchDistrictMap.get(branch.getBranchName())!=null){
					System.out.println(" branch exists in districtmaster...");
					districtMasterObj = branchDistrictMap.get(branch.getBranchName());
				}
				else
				{
					districtMasterObj = new  DistrictMaster();
					//districtMasterObj.setHeadQuarterMaster(branch.getHeadQuarterMaster());
					//districtMasterObj.setBranchMaster(branch);
					districtMasterObj.setAreAllSchoolsInContract(false);
					districtMasterObj.setDisplayCGPA(false);
					districtMasterObj.setDisplayJSI(false);
					districtMasterObj.setOfferAssessmentInviteOnly(false);
					districtMasterObj.setPostingOnDistrictWall(2);
					districtMasterObj.setPostingOnTMWall(2);
					districtMasterObj.setCanTMApproach(1);
					districtMasterObj.setQuestDistrict(0);
					districtMasterObj.setHiringAuthority("");
					districtMasterObj.setExitURL(branch.getExitURL());
					districtMasterObj.setDistrictApproval(1);
					districtMasterObj.setIsPortfolioNeeded(false);
					districtMasterObj.setIsWeeklyCgReport(0);
					districtMasterObj.setIsResearchDistrict(false);
					districtMasterObj.setTotalNoOfSchools(1);
					districtMasterObj.setTotalNoOfTeachers(1);
					districtMasterObj.setTotalNoOfStudents(1);
					districtMasterObj.setDisplayName(branch.getBranchName());  // here
					districtMasterObj.setDmName("");
					districtMasterObj.setAcName("");
					districtMasterObj.setAnnualSubsciptionAmount(0.0);
					districtMasterObj.setIsReqNoRequired(false);
					districtMasterObj.setIsReqNoForHiring(false);
					districtMasterObj.setStatusNotes(false);
					districtMasterObj.setDisplayTMDefaultJobCategory(true);
					districtMasterObj.setSetAssociatedStatusToSetDPoints(false);
					districtMasterObj.setWritePrivilegeToSchool(false);
					districtMasterObj.setResetQualificationIssuesPrivilegeToSchool(false);
					districtMasterObj.setStatusPrivilegeForSchools(false);
					districtMasterObj.setCommunicationsAccess(2);
					districtMasterObj.setDisplayAchievementScore(false);
					districtMasterObj.setDisplayTFA(false);
					districtMasterObj.setDisplayYearsTeaching(false);
					districtMasterObj.setDisplayExpectedSalary(false);
					districtMasterObj.setDisplayFitScore(false);
					districtMasterObj.setDisplayDemoClass(false);
					districtMasterObj.setOfferDistrictSpecificItems(false);
					districtMasterObj.setOfferQualificationItems(false);
					districtMasterObj.setOfferEPI(false);
					districtMasterObj.setOfferJSI(false);
					districtMasterObj.setOfferVirtualVideoInterview(false);
					districtMasterObj.setSendAutoVVILink(false);
					districtMasterObj.setOfferPortfolioNeeded(false);
					districtMasterObj.setJobAppliedDate(false);
					districtMasterObj.setIsZoneRequired(0);
					districtMasterObj.setAutoNotifyCandidateOnAttachingWithJob(false);
					districtMasterObj.setDistrictName(branch.getBranchName());
					districtMasterObj.setAddress(branch.getAddress());
					districtMasterObj.setCityName(branch.getCityName());
					districtMasterObj.setStateId(branch.getStateMaster());
					districtMasterObj.setNcesStateId(0);
					districtMasterObj.setZipCode(branch.getZipCode());
					districtMasterObj.setPhoneNumber(branch.getPhoneNumber());
					districtMasterObj.setFaxNumber(branch.getFaxNumber()!=null?branch.getFaxNumber():"0");
					districtMasterObj.setWebsite("");			
					districtMasterObj.setStatus("A");
					districtMasterObj.setCreatedDateTime(new Date());
					txOpen = statelesSsession.beginTransaction();
					statelesSsession.insert(districtMasterObj);
				}

				System.out.println(" distr Id ::::::::::::: "+districtMasterObj.getDistrictId());
				if(hqBranchMap.get(branch.getBranchId())!=null)
				{
					System.out.println("exists entry in hqbranchesdistrict");
					hq = hqBranchMap.get(branch.getBranchId());
					if(hq.getDistrictId()==null || hq.getDistrictId().length()==0){
						hq.setDistrictId(districtMasterObj.getDistrictId()+"");
					}
					else if(!(hq.getDistrictId().contains(districtMasterObj.getDistrictId().toString()))){
						String stDt = hq.getDistrictId()+","+districtMasterObj.getDistrictId()+"";
						hq.setDistrictId(stDt);
					}
					txOpen =statelesSsession.beginTransaction();
					statelesSsession.update(hq);
				}
				else
				{
					System.out.println("new entry in hqbranchesdistrict");
					hq = new HqBranchesDistricts();
					hq.setDistrictId(districtMasterObj.getDistrictId()+"");
					hq.setHeadQuarterMaster(branch.getHeadQuarterMaster());
					hq.setBranchMaster(branch);
					hq.setUserMaster(userMaster);
					txOpen =statelesSsession.beginTransaction();
					statelesSsession.insert(hq);
				}

			}
			 

			txOpen.commit();
			statelesSsession.close();
			hqBranchMap.clear();
			branchDistrictMap.clear();
			branchNames.clear();
			result = "Inserted";
		} catch (Exception e) {
			result = "Error";
			e.printStackTrace();
		}
		return result;
	}
}
