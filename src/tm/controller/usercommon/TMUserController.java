package tm.controller.usercommon;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import tm.bean.TeacherDetail;
import tm.bean.TeacherNotes;
import tm.bean.master.SaveCandidatesToFolder;
import tm.bean.master.UserFolderStructure;
import tm.bean.user.UserMaster;
import tm.dao.TeacherDetailDAO;
import tm.dao.master.SaveCandidatesToFolderDAO;
import tm.dao.master.UserFolderStructureDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.services.social.Facebook;
import tm.services.social.SocialService;
import tm.utility.Utility;

/*
 * admin user settings page related url
 */
@Controller
public class TMUserController 
{
	String locale = Utility.getValueOfPropByKey("locale");
	
	
	
	private static final List<SaveCandidatesToFolder> SaveCandidatesToFolderDAO = null;
	

	@Autowired
	private SaveCandidatesToFolderDAO saveCandidatesToFolderDAO;
	
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) 
	{
		this.userMasterDAO = userMasterDAO;
	}
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) 
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	
	@Autowired 
	private SocialService socialService;
	public void setSocialService(SocialService socialService) {
		this.socialService = socialService;
	}
	
	@Autowired
	private UserFolderStructureDAO userFolderStructureDAO;
	
	@Autowired
	private SaveCandidatesToFolderDAO saveCandidateDAO;
	@RequestMapping(value="/changeusrpwd.do", method=RequestMethod.POST)
	public String changePWD(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		try 
		{
			
		
			PrintWriter out = response.getWriter();
			HttpSession session = request.getSession();
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				out.write("100001");
				return null;
			}
			
			response.setContentType("text/html");
			String password = request.getParameter("password");
			
			UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
			userMaster.setPassword(MD5Encryption.toMD5(password));
			
			UserMaster userSchoolId = userMasterDAO.findById(userMaster.getUserId(), false, false); 
			userMaster.setSchoolId(userSchoolId.getSchoolId());	
			
			userMasterDAO.makePersistent(userMaster);
			emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), "Your TeacherMatch password has been changed",MailText.getUserSettingPWDChangeMailText(request,userMaster));
			PrintWriter pw = response.getWriter();
			pw.write("");
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@RequestMapping(value="/usersettings.do", method=RequestMethod.GET)
	public String doPersonalInfoGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}

		UserMaster userMaster= (UserMaster) session.getAttribute("userMaster");
		map.addAttribute("userMaster", userMaster);
	
		if(userMaster.getDistrictId()!=null)
			map.addAttribute("emailCredentials",userMaster.getDistrictId().getEmailCredentials());
		
		List<String> socialList = socialService.checkSocialConfigDetails(request);
		
		if(socialList.size()>0)
		{
			if(socialList.contains("facebook"))
				map.addAttribute("facebook", "1");
			else
				map.addAttribute("facebook", "0");
			
			if(socialList.contains("twitter"))
				map.addAttribute("twitter", "1");
			else
				map.addAttribute("twitter", "0");
			
			if(socialList.contains("linkedIn"))
				map.addAttribute("linkedIn", "1");
			else
				map.addAttribute("linkedIn", "0");
		}else
		{
			map.addAttribute("facebook", "0");
			map.addAttribute("twitter", "0");
			map.addAttribute("linkedIn", "0");
		}
		map.addAttribute("appId", Utility.getValueOfPropByKey("appId"));
		map.addAttribute("redirectURL", Utility.getBaseURL(request));
		map.addAttribute("inAppId", Utility.getValueOfPropByKey("inAppId"));
		
		return "usersettings";
	}

	@RequestMapping(value="/linkedInsettings.do", method=RequestMethod.GET)
	public String linkedInsettingsGET(ModelMap map,HttpServletRequest request)
	{
		
		HttpSession session = request.getSession(false);

		String code = request.getParameter("code");
		String flag = request.getParameter("state");
		System.out.println("flag llllllllllllllllll : "+flag);
		
		if (session == null || session.getAttribute("userMaster") == null || code==null) 
		{
			return "redirect:index.jsp";
		}
		System.out.println("session: "+session.getAttribute("userMaster"));
		System.out.println("inAppId: "+Utility.getValueOfPropByKey("inAppId"));
		System.out.println("inAppSecret: "+Utility.getValueOfPropByKey("inAppSecret"));
		String content = Facebook.getLinkedInAccess_token(code,Utility.getValueOfPropByKey("inAppId"),Utility.getValueOfPropByKey("inAppSecret"),Utility.getBaseURL(request)+"linkedInsettings.do");
		
		JSONObject json =(JSONObject) JSONSerializer.toJSON(content);
		String access_token = json.getString("access_token")==null?"":json.getString("access_token");
		String expires_in = json.getString("expires_in")==null?"":json.getString("expires_in");
		System.out.println(json);
		System.out.println(access_token);
		//String url = "https://api.linkedin.com/v1/people/~?oauth2_access_token="+access_token;
		String url = "https://api.linkedin.com/v1/people-search:(people:(id,first-name,last-name,headline,picture-url),num-results)?oauth2_access_token="+access_token;
		String responseText = Facebook.sendRequestToURL(url);
		String linkedInId = null;
			
		try {
			String msg = responseText;
			DocumentBuilder newDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document parse = newDocumentBuilder.parse(new ByteArrayInputStream(msg.getBytes()));
			//System.out.println(parse.getFirstChild().getTextContent());
			//System.out.println(parse.getFirstChild().getFirstChild());
				NodeList nl = parse.getElementsByTagName("id");
				if (nl != null) {
					for (int i = 0; i < nl.getLength(); i++) {
					    Node item = nl.item(i);
					    String name = item.getNodeName();
					    String value = item.getTextContent();
					    linkedInId = value;
					    //System.out.println("name: "+name);
					    //System.out.println("value: "+value);
					}
				}
			
		} catch (DOMException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//System.out.println(Facebook.shareOnLinkedIn(access_token));
		
		System.out.println("linkedInId: "+linkedInId);
		int configId = 0;
		if(flag.equalsIgnoreCase("true"))
			configId = 1;
		
		String entityType = socialService.saveLinkedInConfig(configId, access_token, linkedInId, request);
		System.out.println("entityType: ::::::::: "+entityType);
		//saveLinkedInConfig
		
		////////////////////////////////////////////////////
		
		UserMaster userMaster= (UserMaster) session.getAttribute("userMaster");
		map.addAttribute("userMaster", userMaster);
		map.addAttribute("code", code);
		map.addAttribute("content", access_token);
		
		return "linkedInsettings";
	}
	@RequestMapping(value="/usersettings.do", method=RequestMethod.POST)
	//public String doPersonalInfoPOST(@ModelAttribute(value="teacherDetail") TeacherDetail teacherDetail,BindingResult result,ModelMap map,HttpServletRequest request)
	public String doPersonalInfoPOST(@ModelAttribute(value="userMaster") UserMaster userMaster,BindingResult result,ModelMap map,HttpServletRequest request)
	{
		
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		UserMaster userDetailSession = null;
		
		try 
		{
			boolean sendMailEmailChange = false;
			String oldEmail=null;
			userDetailSession = (UserMaster)session.getAttribute("userMaster");
			oldEmail=userDetailSession.getEmailAddress();
			
			
			if(!userMaster.getEmailAddress().trim().equalsIgnoreCase(userDetailSession.getEmailAddress().trim()))
			{
				sendMailEmailChange=true;
			}
			
			userDetailSession.setFirstName(userMaster.getFirstName());
			userDetailSession.setLastName(userMaster.getLastName());
			userDetailSession.setEmailAddress(userMaster.getEmailAddress());
			
			//System.out.println("schoolid   "+userDetailSession.getSchoolId());
			UserMaster userSchoolId = userMasterDAO.findById(userDetailSession.getUserId(), false, false); 
			userDetailSession.setSchoolId(userSchoolId.getSchoolId());	
			
			userMasterDAO.makePersistent(userDetailSession);
					
			
			if(sendMailEmailChange)
			{
				
				emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(),Utility.getLocaleValuePropByKey("msgTeacherMatchEmailchanged", locale),MailText.getUserSettingEmailChangeMailText(request,userMaster,oldEmail));
				emailerService.sendMailAsHTMLText(oldEmail, Utility.getLocaleValuePropByKey("msgTeacherMatchEmailchanged", locale),MailText.getUserSettingEmailChangeMailText(request,userMaster,oldEmail));
			}
			
			
		}		
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		
		
		if(userDetailSession.getEntityType().equals(1) || userDetailSession.getEntityType().equals(5))//admin and HeadQuarter
		{
			return "redirect:admindashboard.do";
		}
		else if(userDetailSession.getEntityType().equals(2))//District
		{
			return "redirect:dashboard.do";
		}
		else
		{
			return "redirect:dashboard.do";	
		}
	}
	
	@RequestMapping(value="/chkuserpwdexist.do", method=RequestMethod.POST)
	public String isPasswordExistPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		try 
		{

			PrintWriter out = response.getWriter();
			HttpSession session = request.getSession();
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				out.write("100001");
				return null;
			}
			
			String encryptedPWD=null;
			
			UserMaster userMaster=(UserMaster) session.getAttribute("userMaster");			
			String password = request.getParameter("password")==null?"":request.getParameter("password").trim();
			encryptedPWD=MD5Encryption.toMD5(password.trim());
			
			
			
			
			
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			if(userMaster.getPassword().equals(encryptedPWD))
			{
				pw.println("1");
			}
			else
			{
				pw.println("0");
			}
		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	
	@RequestMapping(value="/chkusersettingemailexist.do", method=RequestMethod.POST)
	public String isEmailExist(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		try 
		{
			HttpSession session = request.getSession(false);

			PrintWriter out = response.getWriter();
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				out.write("100001");
				return null;
			}
			
			UserMaster userMaster =(UserMaster) session.getAttribute("userMaster");				
			String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
			
			List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
			List<UserMaster> lstUsermaMasters= userMasterDAO.findByEmail(emailAddress);
			
			
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			
			
			if((  (lstTeacherDetails!=null && lstTeacherDetails.size()>0) || (lstUsermaMasters!=null && lstUsermaMasters.size()>0)   )&& !emailAddress.equals(userMaster.getEmailAddress().trim()))
			{
				pw.println("1");
			}
			else
			{
				pw.println("0");
			}
		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
/*================  Gagan : MyFolder Related Function Start Here =============          */
	@RequestMapping(value="/myfolder.do", method=RequestMethod.GET)
	public String doMyFolderGET(ModelMap map,HttpServletRequest request)
	{
		
		HttpSession session = request.getSession(false);
		
		ArrayList<Integer> folderIdList = new ArrayList<Integer>();
	 
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}

		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		List<UserFolderStructure> lstTreeStructure=	new ArrayList<UserFolderStructure>();
		List<SaveCandidatesToFolder> lstSavedCandidates= new ArrayList<SaveCandidatesToFolder>();
		StringBuffer sb 						=	new StringBuffer();
		try 
		{
			if(userSession.getEntityType()!=1){
				map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictName",null);
			}
			if(userSession.getEntityType()==3){
				map.addAttribute("SchoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
			}else{
				map.addAttribute("SchoolName",null);
			}
			try{
				Criterion criterion1 =	Restrictions.eq("usermaster", userSession);
				lstTreeStructure =	userFolderStructureDAO.findByCriteria(criterion1);
				Criterion criterion3	=	Restrictions.eq("saveUserMaster", userSession);
				Criterion criterion4	=	Restrictions.eq("sharedUserMaster", userSession);
				Criterion criterion5	=	Restrictions.or(criterion3, criterion4);
				lstSavedCandidates=saveCandidateDAO.findByCriteria(criterion5);
				
				
//===========================
                
                Criterion criterion34	=	null;
                Criterion criterion66	=	null;
                System.out.println("getDistrictId: "+userSession.getDistrictId().getDistrictId());
                if(userSession.getDistrictId().getDistrictId()!=null)
                {
                 criterion34	=	Restrictions.eq("districtMaster",userSession.getDistrictId() );
                 criterion66	=	Restrictions.eq("viewed",false);
                }else{
                 if(userSession.getSchoolId().getSchoolId()!=null)
                 {
                 criterion34	=	Restrictions.eq("schoolMaster",userSession.getSchoolId() );
                 criterion66	=	Restrictions.eq("viewed",false);
                }
                }
                
                List<SaveCandidatesToFolder>  lstSaveCanToFolders = saveCandidatesToFolderDAO.findByCriteria(criterion34,criterion66);
                if(lstSaveCanToFolders!=null && lstSaveCanToFolders.size()>0){
                	for(SaveCandidatesToFolder  listObj : lstSaveCanToFolders){
                		if(listObj.getUserFolder()!=null)
                		folderIdList.add(listObj.getUserFolder().getFolderId()); 
                	}
                }
				
			}catch(Exception e){
					e.printStackTrace();
			}
			sb.append("<ul>");
			boolean viewed=false;
			for(UserFolderStructure tree: lstTreeStructure)
			{
				if(tree.getUserFolderStructure()==null)
				{
					
					
					if(locale.equalsIgnoreCase("fr"))
					{
						tree.setFolderName("Mes dossiers");
					}
					
					sb.append("<li id='"+tree.getFolderId()+"' class=\"folder\"  >"+tree.getFolderName()+" ");
					if(tree.getChildren().size()>0)
					sb.append(printChild(request, tree.getChildren(),tree,lstSavedCandidates,folderIdList));
				}
				
			}
			sb.append("</ul>");
			map.addAttribute("entityType",userSession.getEntityType());;
			map.addAttribute("tree",sb.toString());
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}		
		map.addAttribute("userMaster", userSession);
		
		
		return "myfolder";
	}
	
	// for  save the Candidate of  DA/SA  .................
	public boolean getView(List<SaveCandidatesToFolder> lstSaveCandidatesToFolder,int folderId)
	{
		boolean viewed=false;
		try {
			if(lstSaveCandidatesToFolder!=null)
			for(SaveCandidatesToFolder sCTF:lstSaveCandidatesToFolder ){
				if(sCTF.getUserFolder().getFolderId()==folderId){
					if(!sCTF.isViewed()){
						viewed=true;
						break;
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return viewed;

	} 
	public String printChild(HttpServletRequest request,List<UserFolderStructure> lstuserChildren,UserFolderStructure tree,List<SaveCandidatesToFolder> lstSavedCandidates,List folderid)
	{
		StringBuffer sbchild=new StringBuffer();
		sbchild.append("<ul>");
		for(UserFolderStructure subfL: lstuserChildren ){
			String className="class=\"folder\"";
			 if(subfL.getFolderId()!=null)
			 {
				 if(folderid.contains(subfL.getFolderId()))
				 {
					 System.out.println("::::::::::::::::::::::View for Folder :::::::::::::::::");
						className="data=\"addClass:'cview'\"";
				 }
			 }
			 
			
			     if(getView(lstSavedCandidates,subfL.getFolderId())){
				   System.out.println("::::::::::::::::::::::View:::::::::::::::::");
				   className="data=\"addClass:'cview'\"";
			      }
			     /**
			      * @author Sanavvar khan
			      * @  only for franch ...    
			      * 
			      */
			     if(locale.equalsIgnoreCase("fr"))
				 {  
			            if(subfL.getFolderName().equalsIgnoreCase("Received Candidates"))
			            {
				           subfL.setFolderName("Dossiers re�us");
			            }
			            if(subfL.getFolderName().equalsIgnoreCase("Saved Candidates"))
			            {
				           subfL.setFolderName("Dossiers sauvegard�s");
			            }
			     }
			
			sbchild.append("<li id='"+subfL.getFolderId()+"' "+className+" >"+subfL.getFolderName()+"");
			if(subfL.getChildren().size()>0){
				sbchild.append(printChild(request, subfL.getChildren(),subfL,lstSavedCandidates,folderid));
			}
			sbchild.append("</li>");
		}
		sbchild.append("</ul>");
		return sbchild.toString();
	}
	
	/**
	 * Added by Amit
	 * @param map
	 * @param request
	 * @return changelocation
	 * @date 11-Mar-15
	 */
	@RequestMapping(value="/changelocation.do", method=RequestMethod.GET)
	public String doChangeLocationGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("userMaster") == null) 
			return "redirect:index.jsp";
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		try 
		{
			if(userMaster.getEntityType()!=1){
				if(userMaster.getHeadQuarterMaster()!=null)
					map.addAttribute("headQuarterId",userMaster.getHeadQuarterMaster().getHeadQuarterId());
				else if(userMaster.getBranchMaster()!=null)
					map.addAttribute("branchId",userMaster.getBranchMaster().getBranchId());
				else
					map.addAttribute("DistrictId",userMaster.getDistrictId().getDistrictId());
			}
			else{
				map.addAttribute("DistrictId",null);
			}
			map.addAttribute("emailAddress",userMaster.getEmailAddress());
			map.addAttribute("entityType",userMaster.getEntityType());;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		map.addAttribute("userMaster", userMaster);
		return "changelocation";
	}
}
