package tm.controller.es;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import tm.bean.JobCertification;
import tm.bean.JobOrder;
import tm.bean.master.SchoolMaster;
import tm.dao.JobCertificationDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.services.es.ElasticSearchService;
import tm.services.teacher.DWRAutoComplete;
import tm.utility.ElasticSearchConfig;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

@Controller
public class ElasticSearchController {
	
	//Use these 3 URL only
	/* 
	 * 1.	esearch/matchandupdate.do   				for Quest(questjobboard.do)
	 * 2.	esearch/updateESForDistrictJobBoard.do		for DistrictJobBoard(jobsboard.do)
	 * 3. 	esearch/updateESForSchoolJobOrder.do		for JobOrder(managejoborders.do) 
	 * */
	
	
		
	Map<String, String> indexMap=new HashMap<String, String>();
	@Autowired
	JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	public void setJobCertificationDAO(JobCertificationDAO jobCertificationDAO) {
		this.jobCertificationDAO = jobCertificationDAO;
	}

	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	
	
	@RequestMapping(value="/esearch/firsttime.do", method=RequestMethod.GET)
	public String doFirstTimeES(HttpServletRequest request, HttpServletResponse response1)
	{
		System.out.println("/es/ft.do");

		ClientConfig config = new DefaultClientConfig();
	    Client client = Client.create(config);
	    WebResource service = client.resource(getBaseURI());
	    ClientResponse response = null;
	    String input = "{\"type\" : \"jdbc\"," +
	    				"\"strategy\" : \"simple\"," +
	    				  		"\"jdbc\" : " +
				    		"[" +
				    			"{\"url\" : \""+ElasticSearchConfig.sElasticSearchDBURL+"\"," +
				    			"\"user\" : \""+ElasticSearchConfig.sElasticSearchDBUser+"\",\"password\" : \""+ElasticSearchConfig.sElasticSearchDBPass+"\"," +
				    			  // "\"sql\" : \"select jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address as distaddress,dm.stateId,scm.schoolName,scm.zip,scm.address as schooladdress,scm.stateId from joborder jo join subjectmaster sm on sm.subjectId=jo.subjectId join districtmaster dm on dm.districtId=jo.districtId join schoolinjoborder sij on sij.jobId =jo.jobId join schoolmaster scm on scm.schoolId= sij.schoolId \"," +
				    			  
				    			  
									"\"sql\" : \"SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId AS disStateId,IFNULL(scm.schoolId,0) AS schoolId,scm.schoolName,IFNULL(scm.zip,0) as zip,scm.address AS schooladdress,IFNULL(scm.stateId,0) AS schStateId,ctm.certType,stm.stateName,IFNULL(ctm.certTypeId,0) as certTypeId,jo.createdDateTime  FROM joborder jo  LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId LEFT JOIN jobcertification jc ON jc.jobid=jo.jobid LEFT JOIN certificatetypemaster ctm ON jc.certtypeid=ctm.certtypeid LEFT JOIN statemaster stm ON stm.stateId=dm.stateId WHERE jo.status='a' AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 AND jo.isInviteOnly!=TRUE\"," +
									//"\"sql\" : \"SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId AS disStateId,IFNULL(scm.schoolId,0) AS schoolId,scm.schoolName,IFNULL(scm.zip,0) as zip,scm.address AS schooladdress,IFNULL(scm.stateId,0) AS schStateId,ctm.certType,stm.stateName,IFNULL(ctm.certTypeId,0) as certTypeId  FROM joborder jo  LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId LEFT JOIN JobCertification jc ON jc.jobid=jo.jobid LEFT JOIN CertificateTypeMaster ctm ON jc.certtypeid=ctm.certtypeid LEFT JOIN statemaster stm ON stm.stateId=dm.stateId WHERE jo.status='a' AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 AND jo.isInviteOnly!=TRUE\"," +
				    			  
									
									//"\"sql\" : \"SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId AS disStateId,IFNULL(scm.schoolId,0) AS schoolId,scm.schoolName,scm.zip,scm.address AS schooladdress,scm.stateId AS schStateId,jc.certType,stm.stateName  FROM joborder jo  LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId LEFT JOIN JobCertification jc ON jc.jobid=jo.jobid LEFT JOIN statemaster stm ON stm.stateId=dm.stateId WHERE jo.status='a' AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 AND jo.isInviteOnly!=TRUE \"," +
				    			 
				    			  // "\"sql\" : \"SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId as disStateId,ifnull(scm.schoolId,0) as schoolId,scm.schoolName,scm.zip,scm.address AS schooladdress,scm.stateId as schStateId FROM joborder jo LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId WHERE jo.status='a' AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 AND jo.isInviteOnly!=TRUE \"," +
				    			  // "\"parameter\" : [ \"$river.state.last_active_begin\" ]," +
				    			// "\"sql\" : \"update taskviewedbyusers set chinese='1' where userid=35\"," +
				    			 //" \"write\" : \"true\"," +
				    			   //"\"parameter\" : [ \"$river.state.last_active_begin\" ]," +
				    			   "\"index\" : \"document\"," +
				    			   //"\"autocommit\" : true," +
				    			   " \"fetchsize\" : 30" +
				    			  "}" +
				    		  "]" +
	    			  "}";
	    
	    //System.out.println(input);
	    
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
	    //System.out.println("Form response11 " + response.getEntity(String.class));
	    System.out.println("***********************************************************");
	   // System.out.println(service.accept(MediaType.APPLICATION_XML).get(String.class));
		
		
		return null;
	}
	
	@RequestMapping(value="/esearch/matchandupdate.do", method=RequestMethod.GET)
	public String doMatchAndUpdateES(HttpServletRequest request, HttpServletResponse response1)
	{
		System.out.println("/es/matchandupdate_es.do");
		ElasticSearchService es=new ElasticSearchService();
		Map<Integer, String> elasticMap=es.searchByDocument("");
		Map<Integer, String> databaseMap=es.getDatabaseData();
		if(databaseMap!=null)
			 es.compareData(databaseMap, elasticMap);
		if(ElasticSearchConfig.serverName!=null && ElasticSearchConfig.serverName.equalsIgnoreCase("NC"))
		{
			//search data from platform not nc
			Map<Integer, String> elasticMapPlatformNC=es.searchByDocumentPlatformNC("");
			es.compareDataPlatformNC(databaseMap, elasticMapPlatformNC);
		}
		
		return null;
	}
	private static URI getBaseURI() {
	     URI uri=null;
		    try {
			
		    		uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/_river/document_jdbc_river/_meta");
		    		    	
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    return uri;
	  }
	
	
	/*public Map<String, String> getDatabaseData()
	{
		Map<String, String> map=new HashMap<String, String>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(ElasticSearchConfig.sElasticSearchDBURL,ElasticSearchConfig.sElasticSearchDBUser,ElasticSearchConfig.sElasticSearchDBPass);
			String sql="select jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,"
					+ "dm.cityname,dm.zipCode,dm.address as distaddress,dm.stateId,scm.schoolName,scm.zip,scm.address as schooladdress,"
					+ "scm.stateId from joborder jo "
					+ "join subjectmaster sm on sm.subjectId=jo.subjectId "
					+ "join districtmaster dm on dm.districtId=jo.districtId join schoolinjoborder sij on sij.jobId =jo.jobId join schoolmaster scm on scm.schoolId= sij.schoolId "
					+ " ";
			String sql="SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId as disStateId,scm.schoolId,scm.schoolName,scm.zip,scm.address AS schooladdress,scm.stateId as schStateId "
					+ "FROM joborder jo "
					+ "LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId "
					+ "LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId "
					+ "LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId "
					+ "LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId "
					+ "WHERE jo.status='a' AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 AND jo.isInviteOnly!=TRUE"
					+ " ";
			//sql="SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId AS disStateId,IFNULL(scm.schoolId,0) AS schoolId,scm.schoolName,scm.zip,scm.address AS schooladdress,scm.stateId AS schStateId,jc.certType FROM joborder jo  LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId LEFT JOIN JobCertification jc ON jc.jobid=jo.jobid WHERE jo.status='a' AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 AND jo.isInviteOnly!=TRUE";
			sql="SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId AS disStateId,IFNULL(scm.schoolId,0) AS schoolId,scm.schoolName,scm.zip,scm.address AS schooladdress,scm.stateId AS schStateId,ctm.certType,stm.stateName,ctm.certTypeId,jo.createdDateTime  FROM joborder jo  LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId LEFT JOIN jobcertification jc ON jc.jobid=jo.jobid LEFT JOIN certificatetypemaster ctm ON jc.certtypeid=ctm.certtypeid LEFT JOIN statemaster stm ON stm.stateId=dm.stateId WHERE jo.status='a' AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 AND jo.isInviteOnly!=TRUE";
			System.out.println(sql);
			PreparedStatement psmt=con.prepareStatement(sql);
			ResultSet rset=psmt.executeQuery();
			
			while(rset.next())
			{
				JSONObject obj=new JSONObject();
				obj.put("jobid", rset.getInt("jo.jobid"));
				obj.put("schoolId", rset.getInt("schoolId"));
				obj.put("jobDescription", rset.getString("jobDescription"));
				obj.put("districtid", rset.getInt("jo.districtid"));
				obj.put("jobtitle", rset.getString("jo.jobtitle"));
				obj.put("jobstartdate", rset.getString("jo.jobstartdate")+"T00:00:00.000+05:30");
				obj.put("jobenddate", rset.getString("jo.jobenddate")+"T00:00:00.000+05:30");
				obj.put("subjectName", rset.getString("sm.subjectName"));
				obj.put("districtName", rset.getString("dm.districtName"));
				obj.put("cityname", rset.getString("dm.cityname"));
				obj.put("zipCode", rset.getString("dm.zipCode"));
				obj.put("distaddress", rset.getString("distaddress"));
				obj.put("disStateId", rset.getInt("disStateId"));
				obj.put("schStateId", rset.getInt("schStateId"));
				obj.put("schoolName", rset.getString("scm.schoolName"));
				obj.put("zip", rset.getInt("scm.zip"));
				obj.put("schooladdress", rset.getString("schooladdress"));
				obj.put("certType", rset.getString("certType"));
				obj.put("stateName", rset.getString("stm.stateName"));
				obj.put("certTypeId", rset.getInt("ctm.certTypeId"));
				obj.put("createdDateTime", rset.getString("jo.createdDateTime").split(" ")[0]+"T"+rset.getString("jo.createdDateTime").split(" ")[1].subSequence(0, 8)+".000+05:30");
				//System.out.println(rset.getString("jo.createdDateTime").split(" ")[1].subSequence(0, 8)+"\t"+rset.getInt("jo.jobid"));
				map.put(rset.getInt("jo.jobid")+"_"+rset.getInt("schoolId")+"_"+rset.getInt("ctm.certTypeId"), obj.toString());
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}*/
	/*public Map<String, String> searchByDocument(String jobtilte)
	{
		Map<String, String> map=new HashMap<String, String>();
		indexMap=new HashMap<String, String>();
		ClientConfig config = new DefaultClientConfig();
		URI uri=null;
	    Client client = Client.create(config);
	    try {
			uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/document/_search?pretty");
		} catch (URISyntaxException e) {
			
			e.printStackTrace();
		}
	    WebResource service = client.resource(uri);
	    ClientResponse response = null;
	    String input = "{\"query\": { \"match_all\": {} },\"size\":100000}";
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
	    String s=response.getEntity(String.class);
	   // System.out.println(s);
	    //System.out.println("***********************************************************");
	    
	    JSONParser jsonParser =new JSONParser();
	    JSONObject jsonObject;
		try {
			jsonObject = (JSONObject) jsonParser.parse(s);
			//System.out.println(((JSONObject)jsonObject.get("hits")));
			 //System.out.println(((JSONObject)jsonObject.get("hits")).get("hits"));
			 JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
			 System.out.println("hits size==="+hits.size());
			 for(int i=0; i<hits.size(); i++)
			 {
				jsonObject=(JSONObject)hits.get(i);
				String index=(String)jsonObject.get("_index");
				String type=(String)jsonObject.get("_type");
				String id=(String)jsonObject.get("_id");
				jsonObject=(JSONObject)jsonObject.get("_source");
				//System.out.println(jsonObject.toString());
				map.put(jsonObject.get("jobid").toString()+"_"+jsonObject.get("schoolId")+"_"+jsonObject.get("certTypeId"), jsonObject.toString());
				indexMap.put(jsonObject.get("jobid").toString()+"_"+jsonObject.get("schoolId")+"_"+jsonObject.get("certTypeId"), index+":"+type+":"+id);
			}
			


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("document size="+map.size());
		
		return map;
		
	}*/
	
	public void update(String index,String type,String id,String json)
	{
		ClientConfig config = new DefaultClientConfig();
	    Client client = Client.create(config);
	    URI uri=null;
	    try {
			//uri=new URI("http://localhost:9200/joborder/jdbc/"+id+"/?pretty");
	    	uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+index+"/"+type+"/"+id+"/?pretty");
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    WebResource service = client.resource(uri);
	    ClientResponse response = null;//service.accept(MediaType.APPLICATION_XML).put(ClientResponse.class);
	    //String input = "{\"jobId\":8,\"jobtitle\":\"Music Teacher\"}";
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED)
	        .put(ClientResponse.class, json);
	    //System.out.println("Form response11 " + response.getEntity(String.class));
	   
		
	}

	public void delete(String index,String type,String id)
	{
		ClientConfig config = new DefaultClientConfig();
	    Client client = Client.create(config);
	    URI uri=null;
	    try {
			//uri=new URI("http://localhost:9200/joborder/jdbc/"+id+"/?pretty");
	    	uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+index+"/"+type+"/"+id);
	    	//uri=new URI("http://localhost:9200/document/jdbc/AUvPehrCbLY5SVw21NX8");
	    	
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    WebResource service = client.resource(uri);
	    ClientResponse response = null;//service.accept(MediaType.APPLICATION_XML).put(ClientResponse.class);
	    //String input = "{\"jobId\":8,\"jobtitle\":\"Music Teacher\"}";
	    response = service.type(MediaType.APPLICATION_FORM_URLENCODED)
	        .delete(ClientResponse.class);
	    //System.out.println("Form deleting " + response.getEntity(String.class));
	   
		
	}
	
	/*public void compareData(Map<String, String> databaseMap,Map<String, String> elasticMap)
	{
		int x=0;
		int y=0;
		int z=0;
		//System.out.println(databaseMap.get(1932));
		//System.out.println(elasticMap.get(1932));
		//System.out.println(databaseMap.get(1932).equalsIgnoreCase(databaseMap.get(1932)));
		Set<String> databaseSet=databaseMap.keySet();
		for(String i:databaseSet)
		{
			//System.out.println(elasticMap.get(i));
			//System.out.println(databaseMap.get(i));
			//System.out.println("*****************************");
			
			//if data is not present in elasticsearch then insert
			if(elasticMap.get(i)==null)
			{
				System.out.println("**********************creating**************************"+i.toString());
				System.out.println(databaseMap.get(i));
				x++;
				System.out.println(i.toString());
				update("document","jdbc",i.toString(),databaseMap.get(i));
			}
			//if data is present then update
			else if(!(elasticMap.get(i)).equals(databaseMap.get(i)))
			{
				
				y++;
				System.out.println("***************updating*****************");
					System.out.println("elasticMap key="+i);
					System.out.println(elasticMap.get(i));
					System.out.println(databaseMap.get(i));
					System.out.println("elastic key="+indexMap.get(i));
					//break;
					//System.out.println(elasticMap.get(i).replaceAll("\n", " "));
					//System.out.println(databaseMap.get(i).replaceAll("\n", " "));
					update(indexMap.get(i).split(":")[0],indexMap.get(i).split(":")[1],indexMap.get(i).split(":")[2],databaseMap.get(i));
					
				
				
			}
			
			
			
			
		}
		int m=0;
		System.out.println(elasticMap.size()+"\t"+databaseMap.size());
		//for deleting from elasticsearch
		Set<String> elasticSet=elasticMap.keySet();
		for(String i:elasticSet)
		{
			if(databaseMap.get(i)==null)
			{
				if(m<10)
				{m++;
					System.out.println("**************************deleting********************"+i);
					System.out.println(databaseMap.get(i));
					z++;
				}
				
				delete(indexMap.get(i).split(":")[0],indexMap.get(i).split(":")[1],indexMap.get(i).split(":")[2]);
			}
				
			
			
		}
		System.out.println("databaseMap size="+databaseMap.size());
		System.out.println("elasticMap size="+elasticMap.size());
		System.out.println("indexMap size="+indexMap.size());
		System.out.println("creating x=="+x);
		System.out.println("updating y=="+y);
		System.out.println("deleting z=="+z);
		System.out.println("iterating index map");
		Set<String> set=indexMap.keySet();
		for(String s:set)
		{
			System.out.println(s);
		}
	}*/
	
	@RequestMapping(value="/esearch/delete.do", method=RequestMethod.GET)
	public String doDeleteIndexES(@RequestParam("index") String index)
	{
		System.out.println("es/delete_es.do");
		/*System.out.println(index);
		
		System.out.println("to delete");
		try
		{
			ClientConfig config = new DefaultClientConfig();
		    Client client = Client.create(config);
		    WebResource service = client.resource(getURI(index));
		    service.type(MediaType.APPLICATION_FORM_URLENCODED).delete();
		    service = client.resource(getURI(ElasticSearchConfig.river+index+ElasticSearchConfig.jdbcRiver));
		    if(getURI(ElasticSearchConfig.river+index+ElasticSearchConfig.jdbcRiver)!=null)
		    service.type(MediaType.APPLICATION_FORM_URLENCODED).delete();
		    System.out.println("deletedddd");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}*/
	    
		
		return null;
	}
	
	
	
	 private static URI getURI(String index) {
		    URI uri=null;
			    try {
			    	uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/"+index);
					//uri=new URI("http://localhost:9200/document");
			    	//uri=new URI("http://localhost:9200/_river/document_jdbc_river");
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    return uri;
		  }
	 
/*	 @RequestMapping(value="/esearch/districtjobboardcreate.do", method=RequestMethod.GET)
		public String doFirstTimeESDistrictJobBoard(HttpServletRequest request, HttpServletResponse response1)
		{
			System.out.println("/esearch/districtjobboardcreate.do");

			ClientConfig config = new DefaultClientConfig();
		    Client client = Client.create(config);
		    WebResource service = client.resource(getDistrictJobBoardURI());
		    ClientResponse response = null;
		    String input = "{\"type\" : \"jdbc\"," +
		    				"\"strategy\" : \"simple\"," +
		    				  		"\"jdbc\" : " +
					    		"[" +
					    			"{\"url\" : \""+ElasticSearchConfig.sElasticSearchDBURL+"\"," +
					    			"\"user\" : \""+ElasticSearchConfig.sElasticSearchDBUser+"\",\"password\" : \""+ElasticSearchConfig.sElasticSearchDBPass+"\"," +
					    			 "\"sql\" : \"select jo.jobId,jo.jobTitle,dm.address,dm.zipcode,sjm.subjectName,gm.geoZoneName,scm.schoolName,scm.zip from joborder jo "
									+ "LEFT JOIN subjectmaster sjm ON sjm.subjectId=jo.subjectId "
									+ "left join districtmaster dm on jo.districtId= dm.districtId "
									+ "left join geoZoneMaster gm on jo.geoZoneId=gm.geoZoneId "
									+ "left join jobcategorymaster jm on jo.jobCategoryId=jm.jobCategoryId "
									+ "LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId "
									+ "LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId "
									+ "where jo.jobStartDate<= now() and jo.jobEndDate >=now() and jo.isPoolJob !=1 and jo.status='A' and jo.approvalBeforeGoLive=1 and jo.isInviteOnly !=true\"," +
									"\"index\" : \"districtjobboard\"," +
					    			   //"\"autocommit\" : true," +
					    			   " \"fetchsize\" : 30" +
					    			  "}" +
					    		  "]" +
		    			  "}";
		    
		    System.out.println(input);
		    
		    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
		    System.out.println("Form response11 " + response.getEntity(String.class));
		    System.out.println("***********************************************************");
		   // System.out.println(service.accept(MediaType.APPLICATION_XML).get(String.class));
			
			
			return null;
		}*/
	 
	 /*@RequestMapping(value="/esearch/districtjobboarddelete.do", method=RequestMethod.GET)
		public String doDistrictJobBoardES(@RequestParam("index") String index)
		{
			System.out.println("/esearch/districtjobboarddelete.do");
			System.out.println(index);
			
			System.out.println("to delete");
			try
			{
				ClientConfig config = new DefaultClientConfig();
			    Client client = Client.create(config);
			    WebResource service = client.resource(getURI(index));
			    service.type(MediaType.APPLICATION_FORM_URLENCODED).delete();
			    service = client.resource(getURI(ElasticSearchConfig.river+index+ElasticSearchConfig.jdbcRiver));
			    if(getURI(ElasticSearchConfig.river+index+ElasticSearchConfig.jdbcRiver)!=null)
			    service.type(MediaType.APPLICATION_FORM_URLENCODED).delete();
			    System.out.println("deletedddd");
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		    
			
			return null;
		}*/
	 
	 private static URI getDistrictJobBoardURI() {
	     URI uri=null;
		    try {
			
		    		uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/_river/districtjobboard_jdbc_river/_meta");
		    		    	
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    return uri;
	  }
	 
	 
	 //new for document
	 
	 @RequestMapping(value="/esearch/firsttimenew.do", method=RequestMethod.GET)
		public void voiddoFirstTimeEsNew()
		{
		 
		 ElasticSearchService es=new ElasticSearchService();
		
		System.out.println("/esearch/firsttimenew.do start");
		//Map<Integer, String> dataBaseMap=getDatabaseData();
		Map<Integer, String> dataBaseMap=es.getDatabaseData();
			createDocument(dataBaseMap);
			
			System.out.println("/esearch/firsttimenew.do end");
		}
	 
	 public void createDocument(Map<Integer, String> dataBaseMap)
		{
		 try
		 {
			 System.out.println("creating structure");
			 creatingStructrue();
			 System.out.println("creating  structure end");
			 
			 
			 
			 
			 int j=0;
				System.out.println("*****************insertion start**************");
				Set<Integer> set=dataBaseMap.keySet();
				for(Integer i:set)
				{
					ClientConfig config = new DefaultClientConfig();
				    Client client = Client.create(config);
				    URI uri=null;
				    try {
						//uri=new URI("http://localhost:9200/joborder/jdbc/"+id+"/?pretty");
				    	//uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/document/jdbc/"+i+"_"+ElasticSearchConfig.serverName+"/?pretty");
				    	uri=new URI(ElasticSearchConfig.sElasticSearchURL+"/document/jdbc/"+i+"/?pretty");
					} catch (URISyntaxException e) {
						j++;
						System.out.println("failed jobid="+i);
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				    WebResource service = client.resource(uri);
				    ClientResponse response = null;//service.accept(MediaType.APPLICATION_XML).put(ClientResponse.class);
				    //String input = "{\"jobId\":8,\"jobtitle\":\"Music Teacher\"}";
				    response = service.type(MediaType.APPLICATION_FORM_URLENCODED)
				        .put(ClientResponse.class, dataBaseMap.get(i));
				   // System.out.println("Form response11 " + response.getEntity(String.class));
				}
				System.out.println("Total job id fail in creating=="+j);
				System.out.println("****************insertion end************");
		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
			
			
		}
		public Map<Integer, String> getDatabaseData(int a)
		{
			Map<Integer, String> map=new HashMap<Integer, String>();
			try {
				Class.forName("com.mysql.jdbc.Driver");
				Connection con=DriverManager.getConnection(ElasticSearchConfig.sElasticSearchDBURL,ElasticSearchConfig.sElasticSearchDBUser,ElasticSearchConfig.sElasticSearchDBPass);
				
				String sql="SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId AS disStateId,IFNULL(scm.schoolId,0) AS schoolId,scm.schoolName,scm.zip,scm.address AS schooladdress,scm.stateId AS schStateId,ctm.certType,stm.stateName,ctm.certTypeId,jo.createdDateTime  "
						+ "FROM joborder jo  "
						+ "LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId "
						+ "LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId "
						+ "LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId "
						+ "LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId "
						+ "LEFT JOIN jobcertification jc ON jc.jobid=jo.jobid "
						+ "LEFT JOIN certificatetypemaster ctm ON jc.certtypeid=ctm.certtypeid "
						+ "LEFT JOIN statemaster stm ON stm.stateId=dm.stateId "
						+ "WHERE jo.status='a' AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 AND jo.isInviteOnly!=TRUE "
						//+ " and jo.jobid=2537 "
						+ "order by jo.jobid ";
						//+ "limit 0,20";
				//System.out.println(sql);
				PreparedStatement psmt=con.prepareStatement(sql);
				ResultSet rset=psmt.executeQuery();
				int jobid=0;
				
				while(rset.next())
				{
					int i=0;
					int j=0;
					jobid=rset.getInt("jo.jobid");
					
					JSONObject obj=new JSONObject();
					obj.put("jobid", rset.getInt("jo.jobid"));
				//	obj.put("schoolId", rset.getInt("schoolId"));
					obj.put("jobDescription", rset.getString("jobDescription"));
				//	obj.put("districtid", rset.getInt("jo.districtid"));
					obj.put("jobtitle", rset.getString("jo.jobtitle"));
					obj.put("jobstartdate", rset.getString("jo.jobstartdate")+"T00:00:00.000+05:30");
					obj.put("jobenddate", rset.getString("jo.jobenddate")+"T00:00:00.000+05:30");
					obj.put("subjectName", rset.getString("sm.subjectName"));
					obj.put("districtName", rset.getString("dm.districtName"));
					obj.put("cityname", rset.getString("dm.cityname"));
					obj.put("zipCode", rset.getString("dm.zipCode"));
					obj.put("distaddress", rset.getString("distaddress"));
				//	obj.put("disStateId", rset.getInt("disStateId"));
					JSONArray schoolName = new JSONArray();
					JSONArray zip = new JSONArray();
					JSONArray schoolAddress = new JSONArray();
					JSONArray certType = new JSONArray();
					while(jobid==rset.getInt("jo.jobid"))
					{
						jobid=rset.getInt("jo.jobid");
						
						if(!schoolName.contains(rset.getString("scm.schoolName")))
							schoolName.add(rset.getString("scm.schoolName"));
						if(!zip.contains(rset.getString("scm.schoolName")))
							zip.add(rset.getString("scm.zip"));
						if(!schoolAddress.contains(rset.getString("scm.schoolName")))
							schoolAddress.add(rset.getString("schoolAddress"));
						if(!certType.contains(rset.getString("scm.schoolName")))
							certType.add(rset.getString("certType"));
						
						//System.out.println(rset.getString("certType"));
						
						if(!rset.isLast())
						{
							rset.next();
						}
						else
							break;
						
								
						
					}
					obj.put("schoolName", schoolName);
					obj.put("zip", zip);
					obj.put("schoolAddress", schoolAddress);
					obj.put("certType", certType);
					if(!rset.isLast() )
						rset.previous();
					if(rset.isLast() && i!=1)
					{
						i=1;
						rset.previous();
						if(!(rset.getInt("jobid")==jobid))
						rset.next();
					}
					
					/*obj.put("schStateId", rset.getInt("schStateId"));
					obj.put("schoolName", rset.getString("scm.schoolName"));
					obj.put("zip", rset.getInt("scm.zip"));
					obj.put("schooladdress", rset.getString("schooladdress"));
					
					obj.put("certType", rset.getString("certType"));*/
					
					obj.put("stateName", rset.getString("stm.stateName"));
					//obj.put("certTypeId", rset.getInt("ctm.certTypeId"));
					obj.put("createdDateTime", rset.getString("jo.createdDateTime").split(" ")[0]+"T"+rset.getString("jo.createdDateTime").split(" ")[1].subSequence(0, 8)+".000+05:30");
					//System.out.println(rset.getString("jo.createdDateTime").split(" ")[1].subSequence(0, 8)+"\t"+rset.getInt("jo.jobid"));
					//System.out.println(obj);
					//map.put(rset.getInt("jo.jobid")+"_"+rset.getInt("schoolId")+"_"+rset.getInt("ctm.certTypeId"), obj.toString());
					map.put(rset.getInt("jo.jobid"), obj.toString());
				}
				System.out.println(map.size());
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return map;
		}
		
		
		public void creatingStructrue()
		{
			ClientConfig config = new DefaultClientConfig();
		    Client client = Client.create(config);
		    WebResource service = client.resource(getBaseURI());
		    ClientResponse response = null;
		    String input = "{\"type\" : \"jdbc\"," +
		    				"\"strategy\" : \"simple\"," +
		    				  		"\"jdbc\" : " +
					    		"[" +
					    			"{\"url\" : \""+ElasticSearchConfig.sElasticSearchDBURL+"\"," +
					    			"\"user\" : \""+ElasticSearchConfig.sElasticSearchDBUser+"\",\"password\" : \""+ElasticSearchConfig.sElasticSearchDBPass+"\"," +
					    			  // "\"sql\" : \"select jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address as distaddress,dm.stateId,scm.schoolName,scm.zip,scm.address as schooladdress,scm.stateId from joborder jo join subjectmaster sm on sm.subjectId=jo.subjectId join districtmaster dm on dm.districtId=jo.districtId join schoolinjoborder sij on sij.jobId =jo.jobId join schoolmaster scm on scm.schoolId= sij.schoolId \"," +
					    			  
					    			  
										"\"sql\" : \"SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,"
										+ "sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,"
										+ "dm.stateId AS disStateId,IFNULL(scm.schoolId,0) AS schoolId,scm.schoolName,"
										+ "IFNULL(scm.zip,0) as zip,scm.address AS schooladdress,IFNULL(scm.stateId,0) AS schStateId,"
										+ "ctm.certType,stm.stateName,IFNULL(ctm.certTypeId,0) as certTypeId,jo.createdDateTime,geo.geoZoneId,geo.geoZoneName "
										+ " FROM joborder jo  LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId "
										+ "LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId "
										+ "LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId "
										+ "LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId "
										+ "LEFT JOIN jobcertification jc ON jc.jobid=jo.jobid "
										+ "LEFT JOIN certificatetypemaster ctm ON jc.certtypeid=ctm.certtypeid "
										+ "LEFT JOIN statemaster stm ON stm.stateId=dm.stateId "
										+ " left join geozonemaster geo on geo.geoZoneId=jo.geoZoneId "
										+ " WHERE jo.status='a' "
										+ "AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 "
										+ "AND jo.isInviteOnly!=TRUE  limit 0,0\"," +
										//"\"sql\" : \"SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId AS disStateId,IFNULL(scm.schoolId,0) AS schoolId,scm.schoolName,IFNULL(scm.zip,0) as zip,scm.address AS schooladdress,IFNULL(scm.stateId,0) AS schStateId,ctm.certType,stm.stateName,IFNULL(ctm.certTypeId,0) as certTypeId  FROM joborder jo  LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId LEFT JOIN JobCertification jc ON jc.jobid=jo.jobid LEFT JOIN CertificateTypeMaster ctm ON jc.certtypeid=ctm.certtypeid LEFT JOIN statemaster stm ON stm.stateId=dm.stateId WHERE jo.status='a' AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 AND jo.isInviteOnly!=TRUE\"," +
					    			  
										
										//"\"sql\" : \"SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId AS disStateId,IFNULL(scm.schoolId,0) AS schoolId,scm.schoolName,scm.zip,scm.address AS schooladdress,scm.stateId AS schStateId,jc.certType,stm.stateName  FROM joborder jo  LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId LEFT JOIN JobCertification jc ON jc.jobid=jo.jobid LEFT JOIN statemaster stm ON stm.stateId=dm.stateId WHERE jo.status='a' AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 AND jo.isInviteOnly!=TRUE \"," +
					    			 
					    			  // "\"sql\" : \"SELECT jo.jobid,jobDescription,jo.districtid,jo.jobtitle,jo.jobstartdate,jo.jobenddate,sm.subjectName,dm.districtName,dm.cityname,dm.zipCode,dm.address AS distaddress,dm.stateId as disStateId,ifnull(scm.schoolId,0) as schoolId,scm.schoolName,scm.zip,scm.address AS schooladdress,scm.stateId as schStateId FROM joborder jo LEFT JOIN districtmaster dm ON dm.districtId=jo.districtId LEFT JOIN subjectmaster sm ON sm.subjectId=jo.subjectId LEFT JOIN schoolinjoborder sij ON sij.jobId =jo.jobId LEFT JOIN schoolmaster scm ON scm.schoolId= sij.schoolId WHERE jo.status='a' AND jo.jobStartDate<=NOW() AND jo.jobEndDate>=NOW() AND jo.approvalBeforeGoLive=1 AND jo.isInviteOnly!=TRUE \"," +
					    			  // "\"parameter\" : [ \"$river.state.last_active_begin\" ]," +
					    			// "\"sql\" : \"update taskviewedbyusers set chinese='1' where userid=35\"," +
					    			 //" \"write\" : \"true\"," +
					    			   //"\"parameter\" : [ \"$river.state.last_active_begin\" ]," +
					    			   "\"index\" : \"document\"," +
					    			   //"\"autocommit\" : true," +
					    			   " \"fetchsize\" : 30" +
					    			  "}" +
					    		  "]" +
		    			  "}";
		    
		    //System.out.println(input);
		    
		    response = service.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, input);
		    //System.out.println("Form response11 " + response.getEntity(String.class));
		    System.out.println("***********************************************************");
		}
		
		public static void main(String args[])
		{
			new ElasticSearchController().findAllActiveJobForJobboard();
		}
		public void findAllActiveJobForJobboard()
		{
			try
			{
				Order  sortOrderStrVal=null;
				sortOrderStrVal=Order.desc("jobId");

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
			    Date dateWithoutTime;
				dateWithoutTime = sdf.parse(sdf.format(new Date()));
				List<JobOrder> lstJobOrder	  =	 new ArrayList<JobOrder>();
				Criterion criterion1 = Restrictions.eq("status","A");
				Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
				Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
				Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
				lstJobOrder =jobOrderDAO.findAllActiveJobForJobboard(sortOrderStrVal,0,0,criterion1,criterion2,criterion3,criterion4,true);
				List<JobCertification> certJobOrderList	  =	 new ArrayList<JobCertification>();
				Map<Integer, String> databaseMap=new HashMap<Integer, String>();
				for(JobOrder jobOrder:lstJobOrder)
				{
					
					//	System.out.println("***********************Printing Json***************************************************");
					JSONObject obj=new JSONObject();
					//for deleting
					obj.put("jobid", jobOrder.getJobId());
					obj.put("jobDescription", jobOrder.getJobDescription());
					obj.put("jobtitle", jobOrder.getJobTitle());
					obj.put("jobstartdate", sdf.format(jobOrder.getJobStartDate())+"T00:00:00.000+05:30");
					obj.put("jobenddate", sdf.format(jobOrder.getJobEndDate())+"T00:00:00.000+05:30");
					if(jobOrder.getSubjectMaster()!=null)
					{
						
						obj.put("subjectName", jobOrder.getSubjectMaster().getSubjectName());
					}
					else
					{
						
						obj.put("subjectName", "null");
					}
					obj.put("districtName", jobOrder.getDistrictMaster().getDistrictName());
					obj.put("cityname", jobOrder.getDistrictMaster().getCityName());
					obj.put("zipCode", jobOrder.getDistrictMaster().getZipCode());
					obj.put("distaddress", jobOrder.getDistrictMaster().getAddress());
					obj.put("stateName", jobOrder.getDistrictMaster().getStateId().getStateName());
					
					//if joborder is creating the createddatetime will be null
					String createdDateTime=null;
					if(jobOrder.getCreatedDateTime()==null)
					{
						createdDateTime=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
					}
					else
					{
						createdDateTime=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(jobOrder.getCreatedDateTime());
					}
					obj.put("createdDateTime", createdDateTime.split(" ")[0]+"T"+createdDateTime.split(" ")[1].subSequence(0, 8)+".000+05:30");
					List<JobCertification> lstJobCertification= null;
					Criterion criterion6 = Restrictions.eq("jobId",jobOrder);
					lstJobCertification = jobCertificationDAO.findByCriteria(criterion6);
					
					JSONArray schoolName = new JSONArray();
					JSONArray zip = new JSONArray();
					JSONArray schoolAddress = new JSONArray();
					JSONArray certType = new JSONArray();
					for(SchoolMaster school:jobOrder.getSchool())
					{
						schoolName.add(school.getSchoolName());
						zip.add(school.getZip());
						schoolAddress.add(school.getAddress());
					}
					for(JobCertification certification:lstJobCertification)
					{
						
						obj.put("certType", certification.getCertificateTypeMaster().getCertType());
						certType.add(certification.getCertificateTypeMaster().getCertType());
						
					}
					obj.put("schoolName", schoolName);
					obj.put("zip", zip);
					obj.put("schoolAddress", schoolAddress);
					obj.put("certType", certType);
					databaseMap.put(jobOrder.getJobId(), obj.toString());
						
					
				}
				System.out.println(databaseMap.size());

			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
					
		
		}
	  
	 
	 
	 
	 //new for document end
		
		
		//for districtjobboard
		
		@RequestMapping(value="/esearch/createDistrictJobBoard.do", method=RequestMethod.GET)
		public void createDistrictJobBoard()
		{
		 
			ElasticSearchService es=new ElasticSearchService();
			System.out.println("/esearch/createDistrictJobBoard.do start");
			es.creatingStructureForDistrictJobBoard();
			Map<Integer, String> dataBaseMap=es.getDBDataForDistrictJobBoard();
			es.createDistrictJobBoard(dataBaseMap);
				
			System.out.println("/esearch/createDistrictJobBoard.do end");
		}
		
		@RequestMapping(value="/esearch/updateESForDistrictJobBoard.do", method=RequestMethod.GET)
		public void updateESForDistrictJobBoard()
		{
		 
			ElasticSearchService es=new ElasticSearchService();
			System.out.println("/esearch/updateESForDistrictJobBoard.do start");
			Map<Integer, String> dataBaseMap=es.getDBDataForDistrictJobBoard();
			Map<Integer,String> elasticMap=es.esForDistrictJobBoard();
			if(dataBaseMap!=null)
				es.updateESForDistrictJobBoard(dataBaseMap, elasticMap);
				
			System.out.println("/esearch/updateESForDistrictJobBoard.do end");
		}
		
		
		//districtjobboard end
		
		

		//schooljoborder start
		@RequestMapping(value="/esearch/createSchoolJobOrder.do", method=RequestMethod.GET)
		public void createSchoolJobOrder()
		{
		 
			ElasticSearchService es=new ElasticSearchService();
			System.out.println("/esearch/createSchoolJobOrder.do start");
			es.creatingStructureForSchoolJobOrder();
			Map<Integer, String> dataBaseMap=es.getDBDataForSchoolJobOrder(jobOrderDAO,jobRequisitionNumbersDAO,10000000);
			if(dataBaseMap!=null)
				es.createSchoolJobOrder(dataBaseMap);
			
				
			System.out.println("/esearch/createSchoolJobOrder.do end");
		}
		
		@RequestMapping(value="/esearch/updateESForSchoolJobOrder.do", method=RequestMethod.GET)
		public void updateESForSchoolJobOrder()
		{
			
			ElasticSearchService es=new ElasticSearchService();
			System.out.println("/esearch/updateESForSchoolJobOrder.do start");
			int jobCount=es.getJobRowCount(jobOrderDAO);
			while(jobCount!=0)
			{
				//System.out.println("jobcount="+jobCount);
				jobCount=jobCount-ElasticSearchConfig.rowSize;
				if(jobCount<0)
					jobCount=0;
				Map<Integer, String> dataBaseMap=new ElasticSearchService().getDBDataForSchoolJobOrder(jobOrderDAO,jobRequisitionNumbersDAO,jobCount);
				Set<Integer> jobId=dataBaseMap.keySet();
				Map<Integer,String> elasticMap=es.esForSchoolJobOrderNew(StringUtils.join(jobId, " "));
				//Map<Integer,String> elasticMap=es.esForSchoolJobOrder();
				if(dataBaseMap!=null && dataBaseMap.size()>0)
					es.updateESForSchoolJobOrder(dataBaseMap, elasticMap);
				
				
				
			}
						
						
		}
		//schooljoborder end
		
		

		//jobs of interest start
		
		//for not candidate
		@RequestMapping(value="/esearch/createJobsOfInterest.do", method=RequestMethod.GET)
		public void createJobsOfInterest()
		{
		 
			ElasticSearchService es=new ElasticSearchService();
			System.out.println("/esearch/createJobsOfInterest.do start");
			es.creatingStructureForJobsOfInterest();
			Map<Integer, String> dataBaseMap=es.getDBDataForJobsOfInterest(jobOrderDAO);
			if(dataBaseMap!=null)
			es.createJobsOfInterest(dataBaseMap);
			
				
			System.out.println("/esearch/createJobsOfInterest.do end");
		}
		
		@RequestMapping(value="/esearch/updateESForJobsOfInterestNotCandidate.do", method=RequestMethod.GET)
		@Transactional
		public void updateESForJobsOfInterestNotCandidate()
		{
			//ApplicationContext appContext = new ClassPathXmlApplicationContext("/WEB-INF/tm-data.xml");
			//jobOrderDAO=appContext.getBean("jobOrderDAO",JobOrderDAO.class);
			ElasticSearchService es=new ElasticSearchService();
			System.out.println("/esearch/updateESForJobsOfInterestNotCandidate.do start");
			Map<Integer, String> dataBaseMap=es.getDBDataForJobsOfInterest(jobOrderDAO);
			Map<Integer,String> elasticMap=es.esForJobsOfInterestNotCandidateAll();
			es.updateESForJobsOfInterestNotCandidate(dataBaseMap, elasticMap);
				
			System.out.println("/esearch/updateESForJobsOfInterestNotCandidate.do end");
		}
		
				//for not candidate end
		
		
				//for candidate
		@RequestMapping(value="/esearch/createJobsOfInterestForCandidate.do", method=RequestMethod.GET)
		public void createJobsOfInterestForCandidate()
		{
		 
			ElasticSearchService es=new ElasticSearchService();
			System.out.println("/esearch/createJobsOfInterestForCandidate.do start");
			es.creatingStructureForJobsOfInterestForCandidate();
			Map<Integer, String> dataBaseMap=es.getDBDataForJobsOfInterestForCandidate(jobOrderDAO);
			if(dataBaseMap!=null)
			es.createJobsOfInterestForCandidate(dataBaseMap);
			
				
			System.out.println("/esearch/createJobsOfInterestForCandidate.do end");
		}
		
		@RequestMapping(value="/esearch/updateESForJobsOfInterestForCandidate.do", method=RequestMethod.GET)
		public void updateESForJobsOfInterestForCandidate()
		{
			//ApplicationContext appContext = new ClassPathXmlApplicationContext("/WEB-INF/tm-data.xml");
			//jobOrderDAO=appContext.getBean("jobOrderDAO",JobOrderDAO.class);
			ElasticSearchService es=new ElasticSearchService();
			System.out.println("/esearch/updateESForJobsOfInterestForCandidate.do start");
			Map<Integer, String> dataBaseMap=es.getDBDataForJobsOfInterestForCandidate(jobOrderDAO);
			Map<Integer,String> elasticMap=es.esForJobsOfInterestForCandidateAll();
			es.updateESForJobsOfInterestForCandidate(dataBaseMap, elasticMap);
				
			System.out.println("/esearch/updateESForJobsOfInterestForCandidate.do end");
		}


				
}
