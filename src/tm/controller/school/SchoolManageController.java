package tm.controller.school;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoMapping;
import tm.bean.master.RegionMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SchoolStandardMaster;
import tm.bean.master.SchoolTypeMaster;
import tm.bean.master.StateMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobOrderDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.GeoMappingDAO;
import tm.dao.master.GeographyMasterDAO;
import tm.dao.master.RegionMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SchoolStandardMasterDAO;
import tm.dao.master.SchoolTypeMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.utility.IPAddressUtility;
import tm.utility.ImageResize;
import tm.utility.Utility;

@Controller
public class SchoolManageController 
{
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) 
	{
		this.schoolMasterDAO = schoolMasterDAO;
	}
		
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}

	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO) {
		this.cityMasterDAO = cityMasterDAO;
	}
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUsermasterdao(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	
	@Autowired
	private GeoMappingDAO geoMappingDAO;
	public void setGeoMappingDAO(GeoMappingDAO geoMappingDAO) {
		this.geoMappingDAO = geoMappingDAO;
	}

	@Autowired
	private GeographyMasterDAO geographyMasterDAO;
	public void setGeographyMasterDAO(GeographyMasterDAO geographyMasterDAO) {
		this.geographyMasterDAO = geographyMasterDAO;
	}

	@Autowired 
	private ContactTypeMasterDAO contactTypeMasterDAO;
	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
	{
		this.districtMasterDAO = districtMasterDAO;
	}
	@Autowired
	private RegionMasterDAO regionMasterDAO;
	public void setRegionMasterDAO(RegionMasterDAO regionMasterDAO) {
		this.regionMasterDAO = regionMasterDAO;
	}
	@Autowired
	private SchoolStandardMasterDAO schoolStandardMasterDAO; 
	public void setSchoolStandardMasterDAO(
			SchoolStandardMasterDAO schoolStandardMasterDAO) {
		this.schoolStandardMasterDAO = schoolStandardMasterDAO;
	}

	@Autowired
	private SchoolTypeMasterDAO schoolTypeMasterDAO;
	public void setSchoolTypeMasterDAO(SchoolTypeMasterDAO schoolTypeMasterDAO) {
		this.schoolTypeMasterDAO = schoolTypeMasterDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}
	@RequestMapping(value="/manageschool.do", method=RequestMethod.GET)
	public String SchoolGET(ModelMap map,HttpServletRequest request)
	{
		try{
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster userMaster =null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String authKeyVal = request.getParameter("authKeyVal")==null?"0":request.getParameter("authKeyVal").trim();
	
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,11,"manageschool.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("userMaster", userMaster);	
			map.addAttribute("authKeyVal", authKeyVal);
			try{
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in manage school");
			}catch(Exception e){
				e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "manageschool";
	}
	
	/* @Author: Gagan 
	 * @Discription: editschool.do Controller.
	 */	
	@RequestMapping(value="/editschool.do", method=RequestMethod.GET)
	public String doEditSchoolGET(ModelMap map,HttpServletRequest request)
	{
		try{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		UserMaster userSession									=	(UserMaster) session.getAttribute("userMaster");
		String schoolId = request.getParameter("schoolId")		==	null?"0":request.getParameter("schoolId").trim();
		SchoolMaster schoolMaster								=	null;
		StateMaster stateMaster									=	null;

		if(userSession.getEntityType()!=null && userSession.getEntityType()==3)
		{
			int iSchoolId=Utility.decryptNo(Utility.getIntValue(schoolId));
			if(userSession.getSchoolId()!=null && userSession.getSchoolId().getSchoolId()==iSchoolId)
			{
				// TODO
			}
			else
			{
				return "redirect:editschool.do?schoolId="+Utility.encryptNo(Utility.getIntValue(userSession.getSchoolId().getSchoolId()+""));
			}
				
		}
		
		if(schoolId.equals("0"))
		{
			schoolMaster										= 	new SchoolMaster();
		}
		else
		{
			schoolMaster=schoolMasterDAO.findById(Long.parseLong(Utility.decryptNo(Integer.parseInt(request.getParameter("schoolId")))+""), false, false);
			
			stateMaster											=	stateMasterDAO.findById(Long.parseLong(""+schoolMaster.getStateMaster().getStateId()), false, false);
			if(schoolMaster.getInitiatedOnDate()==null){
				map.addAttribute("initiatedOnDate",null);
			}else{
				map.addAttribute("initiatedOnDate", Utility.getCalenderDateFormart(schoolMaster.getInitiatedOnDate()+""));
			}
			if(schoolMaster.getContractStartDate()==null){
				map.addAttribute("contractStartDate",null);
			}else{
				map.addAttribute("contractStartDate", Utility.getCalenderDateFormart(schoolMaster.getContractStartDate()+""));
			}
			
			if(schoolMaster.getContractEndDate()==null){
				map.addAttribute("contractEndDate",null);
			}else{
				map.addAttribute("contractEndDate", Utility.getCalenderDateFormart(schoolMaster.getContractEndDate()+""));
			}
			map.addAttribute("stateMaster", stateMaster);
		}
		
		DistrictMaster districtMaster 					=	districtMasterDAO.findById(schoolMaster.getDistrictId().getDistrictId(), false, false);
		
		
		
		
		String roleAccess=null,roleAccessGI=null,roleAccessCI=null,roleAccessU=null,roleAccessAI=null;
		int roleId=0;
		if(userSession.getRoleId()!=null){
			roleId=userSession.getRoleId().getRoleId();
		}
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,12,"editschool.do",0);
			roleAccessGI=roleAccessPermissionDAO.getMenuOptionList(roleId,13,"editschool.do",0);
			roleAccessCI=roleAccessPermissionDAO.getMenuOptionList(roleId,14,"editschool.do",0);
			roleAccessU=roleAccessPermissionDAO.getMenuOptionList(roleId,15,"editschool.do",0);
			roleAccessAI=roleAccessPermissionDAO.getMenuOptionList(roleId,16,"editschool.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		map.addAttribute("roleAccess", roleAccess);
		map.addAttribute("roleAccessGI", roleAccessGI);
		map.addAttribute("roleAccessCI", roleAccessCI);
		map.addAttribute("roleAccessU", roleAccessU);
		map.addAttribute("roleAccessAI", roleAccessAI);
	
		if(schoolMaster.getCanTMApproach()!=0){
			schoolMaster.setCanTMApproach(1);
		}
		
	
		List<ContactTypeMaster> contactTypeMaster			=	contactTypeMasterDAO.findByCriteria(Order.asc("contactType"));
		// =============== for Getting the All database Field Values from database ====================
			
			//StringEncrypter se= new StringEncrypter();
			String authKey=null;
			if(schoolMaster.getAuthKey()!=null){
				//authKey=se.decrypt(schoolMaster.getAuthKey());
				authKey=Utility.decodeBase64(schoolMaster.getAuthKey());
			}
			
			map.addAttribute("authKey",authKey);
			map.addAttribute("schoolMaster", schoolMaster);
			map.addAttribute("userSession", userSession);
			map.addAttribute("contactTypeMaster", contactTypeMaster);
		
			int jobOrderSize=0;
			jobOrderSize=jobOrderDAO.checkDefaultDistrictAndSchool(null,schoolMaster,2);
			if(jobOrderSize==1){
				map.addAttribute("aJobRelation","disabled=\"disabled\"");
			}else{
				map.addAttribute("aJobRelation",null);
			}
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc",windowFunc);
			
			String JobBoardURL=null;
			try{
				JobBoardURL=Utility.getShortURL(Utility.getBaseURL(request)+"jobsboard.do?schoolId="+Utility.encryptNo(Integer.parseInt(schoolMaster.getSchoolId()+"")));
			}catch (Exception e) {
				// TODO: handle exception
			}
			map.addAttribute("JobBoardURL",JobBoardURL);
			
			if(schoolMaster.getPostingOnDistrictWall()==2){
				schoolMaster.setPostingOnDistrictWall(districtMaster.getPostingOnDistrictWall());
			}
			if(schoolMaster.getPostingOnTMWall()==2){
				schoolMaster.setPostingOnTMWall(districtMaster.getPostingOnTMWall());
			}
			try{
				userSession.setSchoolId(schoolMaster);
				userLoginHistoryDAO.insertUserLoginHistory(userSession,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in edit school");
			}catch(Exception e){
				e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "editschool";
	}
	
	@RequestMapping(value="/editschool.do", method=RequestMethod.POST)
	public String doEditSchoolPOST(@ModelAttribute(value="schoolMaster") SchoolMaster schoolMaster,BindingResult result, ModelMap map,HttpServletRequest request)
	{
		UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
		}
		String  authKeyVal="2";
		try 
		{
			String schoolId = request.getParameter("schoolId")		==	null?"0":request.getParameter("schoolId").trim();
			String initiatedOnDate =null;
			String contractStartDate =null;
			String contractEndDate =null;
				
			/*============= For updating Not Editable Field ===================*/
			SchoolMaster schlMaster 					=	schoolMasterDAO.findById(Long.parseLong(schoolId), false, false);
			
			if(schoolId!="")
			{
				schoolMaster.setSchoolId(Long.parseLong(schoolId));
			}
			
			/*********Logo Upload File And AssessmentUploadURLFile *********/
			try{
				if(schoolMaster.getLogoPathFile().getSize()>0){
					
					if(schlMaster.getLogoPath()!=null){
						try{
							String filePath="";
							filePath=Utility.getValueOfPropByKey("schoolRootPath")+schoolMaster.getSchoolId()+"/"+schlMaster.getLogoPath();
							File file = new File(filePath);
				    		if(file.delete()){
				    			System.out.println(file.getName() + " is deleted!");
				    		}else{
				    			System.out.println("Delete operation is failed.");
				    		}
						}catch(Exception e){
					 		e.printStackTrace();
					  	}
					}
					
					String filePath=Utility.getValueOfPropByKey("schoolRootPath")+schoolMaster.getSchoolId()+"/";
					
					File f=new File(filePath);
					if(!f.exists())
						 f.mkdirs();
					
					FileItem fileItem =schoolMaster.getLogoPathFile().getFileItem();
					String fileName="Logo"+Utility.getUploadFileName(fileItem);
					fileItem.write(new File(filePath, fileName));
					ImageResize.resizeImage(filePath+ fileName);
					schoolMaster.setLogoPath(fileName);
				}else{
					schoolMaster.setLogoPath(schlMaster.getLogoPath());
				}
				int cbxUploadAssessment=0;
				if(request.getParameter("cbxUploadAssessment")!=null){
					if(request.getParameter("cbxUploadAssessment").equals("on")){
						cbxUploadAssessment=1;
					}else{
						cbxUploadAssessment=0;
					}
				}else if(request.getParameter("aJobRelation")!=null && !request.getParameter("aJobRelation").equals("")){
					cbxUploadAssessment=1;
				}
				if(schoolMaster.getAssessmentUploadURLFile().getSize()>0 && cbxUploadAssessment==1){
					if(schlMaster.getAssessmentUploadURL()!=null){
						try{
							String filePath="";
							filePath=Utility.getValueOfPropByKey("schoolRootPath")+schoolMaster.getSchoolId()+"/"+schlMaster.getAssessmentUploadURL();
							System.out.println("::::::::::filePath::::::::::::="+filePath);
							File file = new File(filePath);
				    		if(file.delete()){
				    			System.out.println(file.getName() + " is deleted!");
				    		}else{
				    			System.out.println("Delete operation is failed.");
				    		}
						}catch(Exception e){
					 		e.printStackTrace();
					  	}
					}
					
					String filePath=Utility.getValueOfPropByKey("schoolRootPath")+schoolMaster.getSchoolId()+"/";
					
					File f=new File(filePath);
					if(!f.exists())
						 f.mkdirs();
					
					FileItem fileItem =schoolMaster.getAssessmentUploadURLFile().getFileItem();
					String fileName="Inventory"+Utility.getUploadFileName(fileItem);
					fileItem.write(new File(filePath, fileName));
					schoolMaster.setAssessmentUploadURL(fileName);
				}else{
					if(cbxUploadAssessment==1){
						schoolMaster.setAssessmentUploadURL(schlMaster.getAssessmentUploadURL());
					}else{
						schoolMaster.setAssessmentUploadURL(null);
					}
				}
			}catch(FileNotFoundException e){
				schoolMaster.setAssessmentUploadURL(schlMaster.getAssessmentUploadURL());
				schoolMaster.setLogoPath(schlMaster.getLogoPath());
				e.printStackTrace();
			}
			
			schoolMaster.setLogoPathFile(null);
			schoolMaster.setAssessmentUploadURLFile(null);
			
			if(schoolMaster.getPostingOnDistrictWall()==null){
				schoolMaster.setPostingOnDistrictWall(0);
			}
			if(schoolMaster.getPostingOnSchoolWall()==null){
				schoolMaster.setPostingOnSchoolWall(0);
			}	
			if(userMaster.getEntityType()==1){
				if(schoolMaster.getIsWeeklyCgReport()==null){
					schoolMaster.setIsWeeklyCgReport(0);
				}
			}else{
				schoolMaster.setIsPortfolioNeeded(schlMaster.getIsPortfolioNeeded());
				schoolMaster.setIsWeeklyCgReport(schlMaster.getIsWeeklyCgReport());
			}
			if(schoolMaster.getPostingOnTMWall()==null){
				if(userMaster.getEntityType()!=1){
					schoolMaster.setPostingOnTMWall(schlMaster.getPostingOnTMWall());
				}else{
					schoolMaster.setPostingOnTMWall(0);
				}
			}
			
			if(schoolMaster.getAllowMessageTeacher()==null){
				schoolMaster.setAllowMessageTeacher(0);
				schoolMaster.setEmailForTeacher(null);
			}
			
			//StringEncrypter se = new StringEncrypter();
			if(schlMaster.getAuthKey()==null || schlMaster.getAuthKey().trim().equals("")){
				String authorizationkey=(Utility.randomString(8)+Utility.getDateTime());
				authKeyVal=authorizationkey;
				//schoolMaster.setAuthKey(se.encrypt(authorizationkey));
				schoolMaster.setAuthKey(Utility.encodeInBase64(authorizationkey));
			}else{
				schoolMaster.setAuthKey(schlMaster.getAuthKey());
			}
			
			DistrictMaster districtMaster 					=	districtMasterDAO.findById(schlMaster.getDistrictId().getDistrictId(), false, false);
			RegionMaster regionMaster						=	regionMasterDAO.findById(schlMaster.getRegionId().getRegionId(), false, false);
			GeoMapping geoMapping							=	geoMappingDAO.findById(schlMaster.getGeoMapping().getActualGeoId(), false, false);
			SchoolStandardMaster schoolStandardMaster		=	schoolStandardMasterDAO.findById(schlMaster.getSchoolStandardId().getSchoolStandardId(), false, false);
			SchoolTypeMaster schoolTypeMaster				=	schoolTypeMasterDAO.findById(schlMaster.getSchoolTypeId().getSchoolTypeId(), false, false);
			
			/*********Edit Dates *********/
			if(request.getParameter("initiatedOnDate")!=null){
				if(!request.getParameter("initiatedOnDate").equals("")){
					initiatedOnDate=request.getParameter("initiatedOnDate").trim();
					schoolMaster.setInitiatedOnDate(Utility.getCurrentDateFormart(initiatedOnDate));
				}
			}else{
				if(userMaster.getEntityType()==3){
					schoolMaster.setInitiatedOnDate(schlMaster.getInitiatedOnDate());
				}
			}
			if(request.getParameter("contractStartDate")!=null){
				if(!request.getParameter("contractStartDate").equals("")){
					contractStartDate=request.getParameter("contractStartDate").trim();
					schoolMaster.setContractStartDate(Utility.getCurrentDateFormart(contractStartDate));
				}
			}else{
				if(userMaster.getEntityType()==3){
					schoolMaster.setContractStartDate(schlMaster.getContractStartDate());
				}
			}
			if(request.getParameter("contractEndDate")!=null){
				if(!request.getParameter("contractEndDate").equals("")){
					contractEndDate=request.getParameter("contractEndDate").trim();
					schoolMaster.setContractEndDate(Utility.getCurrentDateFormart(contractEndDate));
				}
			}else{
				if(userMaster.getEntityType()==3){
					schoolMaster.setContractEndDate(schlMaster.getContractEndDate());
				}
			}
			if(request.getParameter("teachersUnderContract")==null && userMaster.getEntityType()==3){
				schoolMaster.setTeachersUnderContract(schlMaster.getTeachersUnderContract());
			}
			if(request.getParameter("studentsUnderContract")==null && userMaster.getEntityType()==3){
				schoolMaster.setStudentsUnderContract(schlMaster.getStudentsUnderContract());
			}
			
			/*======== Setting  Non Editable Field Values ===========*/
			
			
			schoolMaster.setSchoolName(schlMaster.getSchoolName());
			schoolMaster.setNcesSchoolId(schlMaster.getNcesSchoolId());
			
			schoolMaster.setDistrictId(districtMaster);
			schoolMaster.setRegionId(regionMaster);
			schoolMaster.setGeoMapping(geoMapping);
			schoolMaster.setSchoolStandardId(schoolStandardMaster);
			schoolMaster.setSchoolTypeId(schoolTypeMaster);
			if(schlMaster!=null)
			schoolMaster.setLocationCode(schlMaster.getLocationCode());
			schoolMaster.setDistrictName(schlMaster.getDistrictName());
			schoolMaster.setAddress(schlMaster.getAddress());
			schoolMaster.setNcesStateId(schlMaster.getNcesStateId());
			schoolMaster.setZip(schlMaster.getZip());
			schoolMaster.setStateMaster(schlMaster.getStateMaster());
			schoolMaster.setCityName(schlMaster.getCityName());
			schoolMaster.setPhoneNumber(schlMaster.getPhoneNumber());
			schoolMaster.setStatus(schlMaster.getStatus());
			if(schlMaster.getDmPassword()!=	"" && schlMaster.getDmPassword()!=	null){
				schoolMaster.setDmPassword(schlMaster.getDmPassword());
			}else{
				String to=schoolMaster.getDmEmailAddress();
				String dmName= schoolMaster.getDmName();
				String dmPassword=schoolMaster.getDmPassword();
				String locale = Utility.getValueOfPropByKey("locale");
				String subject=Utility.getLocaleValuePropByKey("msgTeacherMatchAccountAdministrator1", locale);;
				emailerService.sendMailAsHTMLText(to,subject,MailText.messageForFinalDecisionMaker(dmName,dmPassword,userMaster));
				schoolMaster.setDmPassword(MD5Encryption.toMD5(dmPassword));
				System.out.println("Mail Send To DM");
			}
			if((schoolMaster.getFlagForURL()!=null))
			{
				if(schoolMaster.getFlagForURL()==1)
					schoolMaster.setFlagForURL(1);
				else
					schoolMaster.setFlagForURL(2);
			}
			else
			{
				schoolMaster.setFlagForURL(2);
			}
			if((schoolMaster.getFlagForMessage()!=null))
			{
				if(schoolMaster.getFlagForMessage()==1)
					schoolMaster.setFlagForMessage(1);
				else
					schoolMaster.setFlagForMessage(2);
			}
			else
			{
				schoolMaster.setFlagForMessage(2);
			}
				
			schoolMasterDAO.updatePersistent(schoolMaster);
			try{
				//userMaster.setSchoolId(schoolMaster);
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Edited school");
			}catch(Exception e){
				e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();	
		}
		
		return "redirect:manageschool.do?authKeyVal="+authKeyVal;
	}
}
