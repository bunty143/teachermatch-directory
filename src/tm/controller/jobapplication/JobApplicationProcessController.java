package tm.controller.jobapplication;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.InternalTransferCandidates;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAnswerDetail;
import tm.bean.TeacherAssessmentAttempt;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStrikeLog;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DspqJobWiseStatus;
import tm.bean.master.DspqPortfolioName;
import tm.bean.master.DspqRouter;
import tm.bean.master.JobCategoryMaster;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAssessmentAttemptDAO;
import tm.dao.TeacherAssessmentDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherStrikeLogDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.master.DspqJobWiseStatusDAO;
import tm.dao.master.DspqRouterDAO;
import tm.service.jobapplication.JobApplicationFlowConfig;
import tm.service.jobapplication.JobApplicationServiceAjax;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

@Controller
public class JobApplicationProcessController 
{
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired 
	private DspqRouterDAO dspqRouterDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private JobApplicationServiceAjax dspqServiceAjax;
	
	@Autowired
	private TeacherStrikeLogDAO teacherStrikeLogDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;

	@Autowired
	private TeacherAssessmentDetailDAO teacherAssessmentDetailDAO;

	@Autowired
	private TeacherAssessmentAttemptDAO assessmentAttemptDAO;

	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	@Autowired
	private DspqJobWiseStatusDAO dspqJobWiseStatusDAO;
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@RequestMapping(value="/managejobapplicationflow.do", method=RequestMethod.GET)
	public String ManageJobApplicationFlowGET(ModelMap map,HttpServletRequest request)
	{
		printdata("JobApplicationProcessController ( ManageJobApplicationFlowGET ) => managejobapplicationflow.do");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}	
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

		boolean isDspqRequired=false;
	
		String sJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
		String candidateTypeDec = request.getParameter("ct")==null?"":request.getParameter("ct").trim();
		String sCandidateType=validateCTAndDec(candidateTypeDec);
		printdata("sJobId "+sJobId +" candidateType "+sCandidateType);
		
		int iJobId=Utility.getIntValue(sJobId);
		
		map.addAttribute("iJobId", iJobId);
		map.addAttribute("candidateType", candidateTypeDec);
		
		boolean isCoverLetterG1=false;
		boolean isPersonalInformationG1=false;
		boolean isAffiliateG4=false;
		boolean isProfessionalG4=false;
		
		Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
		
		JobOrder jobOrder=jobOrderDAO.findById(iJobId, false, false);
		
		String sDistrictName="";
		if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictName()!=null && !jobOrder.getDistrictMaster().getDistrictName().equals(""))
			sDistrictName=jobOrder.getDistrictMaster().getDistrictName();
		printdata("sDistrictName "+sDistrictName);
		map.addAttribute("sDistrictName", sDistrictName);
		
		DspqPortfolioName dspqPortfolioNameNew =dspqServiceAjax.getDSPQByJobOrder(jobOrder,sCandidateType);
		
		if(dspqPortfolioNameNew!=null)
		{
			isDspqRequired=true;
			map.addAttribute("dspqPortfolioName", dspqPortfolioNameNew);
			
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, sCandidateType);
			printdata("dspqRouterList "+dspqRouterList.size());
			
			Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
			isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
			isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
			isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
			isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
		}
		
		JobCategoryMaster jobCategoryMaster=null;
		if(jobOrder!=null )
			jobCategoryMaster=jobOrder.getJobCategoryMaster();
		boolean isQQRequired=getPreSreen(jobCategoryMaster, sCandidateType);
		boolean isEPI=getEPI(jobCategoryMaster, sCandidateType);
		boolean isJSI=getCustomQuestion(jobOrder, jobCategoryMaster, sCandidateType, teacherDetail);
		printdata("sCandidateType "+sCandidateType +" isQQRequired "+isQQRequired);
		
		Map<Integer, String> mapNextAndPreURL=new HashMap<Integer, String>();
		Map<String,Integer> mapNextAndPreNavigater=new HashMap<String,Integer>();
		String sPrevious="",sNext="";
		
		setPrevAndNextNavigation(mapGroup, mapNextAndPreURL, mapNextAndPreNavigater, sJobId, candidateTypeDec, isDspqRequired, isCoverLetterG1, isPersonalInformationG1, isProfessionalG4, isAffiliateG4, isQQRequired,isEPI,isJSI);
		map.addAttribute("group", 1);
		map.addAttribute("section", 1);
		sNext=getNextNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "CL");
		sPrevious=getPrevNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "CL");
		
		map.addAttribute("sNextURL", sNext);
		map.addAttribute("sPreviousURL", sPrevious);
		map.addAttribute("sCurrentPageShortCode", "CL");
		
		printdata("sNextURL "+sNext+" sPreviousURL "+sPrevious);
		
		JobForTeacher jFTCover=jobForTeacherDAO.getLatestCL(teacherDetail, jobOrder);
		if(jFTCover==null)
			map.addAttribute("latestCoverletter","");
		else
			map.addAttribute("latestCoverletter",jFTCover.getCoverLetter());
		
		TeacherPersonalInfo teacherpersonalinfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
		if(teacherpersonalinfo!=null)
			map.addAttribute("teacherpersonalinfo", teacherpersonalinfo);
		
		boolean isMiami=false;
		if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1200390))
			isMiami=true;
		map.addAttribute("isMiami", isMiami);
		
		return "jafgroup01section01";
		
	}
	
	@RequestMapping(value="/jobapplicationflowpersonal.do", method=RequestMethod.GET)
	public String ManageJobApplicationFlowPersonalGET(ModelMap map,HttpServletRequest request)
	{
		printdata("JobApplicationProcessController ( ManageJobApplicationFlowPersonalGET ) => jobapplicationflowpersonal.do");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}	
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

		boolean isDspqRequired=false;
		
		String sJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
		String candidateTypeDec = request.getParameter("ct")==null?"":request.getParameter("ct").trim();
		String sCandidateType=validateCTAndDec(candidateTypeDec);
		
		int iJobId=Utility.getIntValue(sJobId);
		
		map.addAttribute("iJobId", iJobId);
		map.addAttribute("candidateType", candidateTypeDec);
		
		boolean isCoverLetterG1=false;
		boolean isPersonalInformationG1=false;
		boolean isAffiliateG4=false;
		boolean isProfessionalG4=false;
		
		Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
		
		JobOrder jobOrder=jobOrderDAO.findById(iJobId, false, false);

		DspqPortfolioName dspqPortfolioNameNew =dspqServiceAjax.getDSPQByJobOrder(jobOrder,sCandidateType);
		
		if(dspqPortfolioNameNew!=null)
		{
			isDspqRequired=true;

			map.addAttribute("dspqPortfolioName", dspqPortfolioNameNew);
			
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, sCandidateType);
			
			Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
			isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
			isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
			isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
			isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
		}
		
		JobCategoryMaster jobCategoryMaster=null;
		if(jobOrder!=null )
			jobCategoryMaster=jobOrder.getJobCategoryMaster();
		boolean isQQRequired=getPreSreen(jobCategoryMaster, sCandidateType);
		boolean isEPI=getEPI(jobCategoryMaster, sCandidateType);
		boolean isJSI=getCustomQuestion(jobOrder, jobCategoryMaster, sCandidateType, teacherDetail);
		printdata("sCandidateType "+sCandidateType +" isQQRequired "+isQQRequired);
		
		Map<Integer, String> mapNextAndPreURL=new HashMap<Integer, String>();
		Map<String,Integer> mapNextAndPreNavigater=new HashMap<String,Integer>();
		String sPrevious="",sNext="";
		
		setPrevAndNextNavigation(mapGroup, mapNextAndPreURL, mapNextAndPreNavigater, sJobId, candidateTypeDec, isDspqRequired, isCoverLetterG1, isPersonalInformationG1, isProfessionalG4, isAffiliateG4, isQQRequired,isEPI,isJSI);
		map.addAttribute("group", 1);
		sNext=getNextNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "PI");
		sPrevious=getPrevNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "PI");
		
		map.addAttribute("sNextURL", sNext);
		map.addAttribute("sPreviousURL", sPrevious);
		map.addAttribute("sCurrentPageShortCode", "PI");
		return "jafgroup01";
		
	}
	
	@RequestMapping(value="/jobapplicationflowacademic.do", method=RequestMethod.GET)
	public String ManageJobApplicationFlowAcademicGET(ModelMap map,HttpServletRequest request)
	{
		printdata("JobApplicationProcessController ( ManageJobApplicationFlowAcademicGET ) => jobapplicationflowacademic.do");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}	
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

		boolean isDspqRequired=false;
		
		String sJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
		String candidateTypeDec = request.getParameter("ct")==null?"":request.getParameter("ct").trim();
		String sCandidateType=validateCTAndDec(candidateTypeDec);
		
		int iJobId=Utility.getIntValue(sJobId);
		
		map.addAttribute("iJobId", iJobId);
		map.addAttribute("candidateType", candidateTypeDec);
		
		boolean isCoverLetterG1=false;
		boolean isPersonalInformationG1=false;
		boolean isAffiliateG4=false;
		boolean isProfessionalG4=false;
		
		Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
		
		JobOrder jobOrder=jobOrderDAO.findById(iJobId, false, false);
		DspqPortfolioName dspqPortfolioNameNew =dspqServiceAjax.getDSPQByJobOrder(jobOrder,sCandidateType);
		
		if(dspqPortfolioNameNew!=null)
		{
			isDspqRequired=true;
			map.addAttribute("dspqPortfolioName", dspqPortfolioNameNew);
			
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, sCandidateType);
			
			Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
			isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
			isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
			isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
			isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
		}
		
		JobCategoryMaster jobCategoryMaster=null;
		if(jobOrder!=null )
			jobCategoryMaster=jobOrder.getJobCategoryMaster();
		boolean isQQRequired=getPreSreen(jobCategoryMaster, sCandidateType);
		boolean isEPI=getEPI(jobCategoryMaster, sCandidateType);
		boolean isJSI=getCustomQuestion(jobOrder, jobCategoryMaster, sCandidateType, teacherDetail);
		printdata("sCandidateType "+sCandidateType +" isQQRequired "+isQQRequired);
		
		Map<Integer, String> mapNextAndPreURL=new HashMap<Integer, String>();
		Map<String,Integer> mapNextAndPreNavigater=new HashMap<String,Integer>();
		String sPrevious="",sNext="";
		
		setPrevAndNextNavigation(mapGroup, mapNextAndPreURL, mapNextAndPreNavigater, sJobId, candidateTypeDec, isDspqRequired, isCoverLetterG1, isPersonalInformationG1, isProfessionalG4, isAffiliateG4, isQQRequired,isEPI,isJSI);
		map.addAttribute("group", 2);
		sNext=getNextNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "AC");
		sPrevious=getPrevNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "AC");
		
		map.addAttribute("sNextURL", sNext);
		map.addAttribute("sPreviousURL", sPrevious);
		map.addAttribute("sCurrentPageShortCode", "AC");
		return "jafgroup02";
		
	}
	
	
	@RequestMapping(value="/jobapplicationflowcredentials.do", method=RequestMethod.GET)
	public String ManageJobApplicationFlowCredentialsGET(ModelMap map,HttpServletRequest request)
	{
		printdata("JobApplicationProcessController ( ManageJobApplicationFlowCredentialsGET ) => jobapplicationflowcredentials.do");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}	
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

		boolean isDspqRequired=false;
		
		String sJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
		String candidateTypeDec = request.getParameter("ct")==null?"":request.getParameter("ct").trim();
		String sCandidateType=validateCTAndDec(candidateTypeDec);
		
		int iJobId=Utility.getIntValue(sJobId);
		
		map.addAttribute("iJobId", iJobId);
		map.addAttribute("candidateType", candidateTypeDec);
		
		boolean isCoverLetterG1=false;
		boolean isPersonalInformationG1=false;
		boolean isAffiliateG4=false;
		boolean isProfessionalG4=false;
		
		Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
		
		JobOrder jobOrder=jobOrderDAO.findById(iJobId, false, false);
		
		DspqPortfolioName dspqPortfolioNameNew =dspqServiceAjax.getDSPQByJobOrder(jobOrder,sCandidateType);
		
		if(dspqPortfolioNameNew!=null)
		{
			isDspqRequired=true;
			map.addAttribute("dspqPortfolioName", dspqPortfolioNameNew);
			
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, sCandidateType);
			
			Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
			isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
			isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
			isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
			isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
		}
		
		JobCategoryMaster jobCategoryMaster=null;
		if(jobOrder!=null )
			jobCategoryMaster=jobOrder.getJobCategoryMaster();
		boolean isQQRequired=getPreSreen(jobCategoryMaster, sCandidateType);
		boolean isEPI=getEPI(jobCategoryMaster, sCandidateType);
		boolean isJSI=getCustomQuestion(jobOrder, jobCategoryMaster, sCandidateType, teacherDetail);
		printdata("sCandidateType "+sCandidateType +" isQQRequired "+isQQRequired);
		
		Map<Integer, String> mapNextAndPreURL=new HashMap<Integer, String>();
		Map<String,Integer> mapNextAndPreNavigater=new HashMap<String,Integer>();
		String sPrevious="",sNext="";
		
		setPrevAndNextNavigation(mapGroup, mapNextAndPreURL, mapNextAndPreNavigater, sJobId, candidateTypeDec, isDspqRequired, isCoverLetterG1, isPersonalInformationG1, isProfessionalG4, isAffiliateG4, isQQRequired,isEPI,isJSI);
		map.addAttribute("group", 3);
		sNext=getNextNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "CR");
		sPrevious=getPrevNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "CR");
		
		map.addAttribute("sNextURL", sNext);
		map.addAttribute("sPreviousURL", sPrevious);
		map.addAttribute("sCurrentPageShortCode", "CR");
		return "jafgroup03";
		
	}
	
	
	
	@RequestMapping(value="/jobapplicationflowprofessional.do", method=RequestMethod.GET)
	public String ManageJobApplicationFlowProfessionalGET(ModelMap map,HttpServletRequest request)
	{
		printdata("JobApplicationProcessController ( ManageJobApplicationFlowProfessionalGET ) => jobapplicationflowprofessional.do");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}	
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

		boolean isDspqRequired=false;
		
		String sJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
		String candidateTypeDec = request.getParameter("ct")==null?"":request.getParameter("ct").trim();
		String sCandidateType=validateCTAndDec(candidateTypeDec);
		
		int iJobId=Utility.getIntValue(sJobId);
		
		map.addAttribute("iJobId", iJobId);
		map.addAttribute("candidateType", candidateTypeDec);
		
		boolean isCoverLetterG1=false;
		boolean isPersonalInformationG1=false;
		boolean isAffiliateG4=false;
		boolean isProfessionalG4=false;
		
		Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
		
		JobOrder jobOrder=jobOrderDAO.findById(iJobId, false, false);
		DspqPortfolioName dspqPortfolioNameNew =dspqServiceAjax.getDSPQByJobOrder(jobOrder,sCandidateType);
		
		if(dspqPortfolioNameNew!=null)
		{
			isDspqRequired=true;
			map.addAttribute("dspqPortfolioName", dspqPortfolioNameNew);
			
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, sCandidateType);
			
			Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
			isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
			isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
			isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
			isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
		}
		
		JobCategoryMaster jobCategoryMaster=null;
		if(jobOrder!=null )
			jobCategoryMaster=jobOrder.getJobCategoryMaster();
		boolean isQQRequired=getPreSreen(jobCategoryMaster, sCandidateType);
		boolean isEPI=getEPI(jobCategoryMaster, sCandidateType);
		boolean isJSI=getCustomQuestion(jobOrder, jobCategoryMaster, sCandidateType, teacherDetail);
		printdata("sCandidateType "+sCandidateType +" isQQRequired "+isQQRequired);
		
		Map<Integer, String> mapNextAndPreURL=new HashMap<Integer, String>();
		Map<String,Integer> mapNextAndPreNavigater=new HashMap<String,Integer>();
		String sPrevious="",sNext="";
		
		setPrevAndNextNavigation(mapGroup, mapNextAndPreURL, mapNextAndPreNavigater, sJobId, candidateTypeDec, isDspqRequired, isCoverLetterG1, isPersonalInformationG1, isProfessionalG4, isAffiliateG4, isQQRequired,isEPI,isJSI);
		map.addAttribute("group", 4);
		sNext=getNextNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "PR");
		sPrevious=getPrevNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "PR");
		
		map.addAttribute("sNextURL", sNext);
		map.addAttribute("sPreviousURL", sPrevious);
		map.addAttribute("sCurrentPageShortCode", "PR");
		return "jafgroup04";
		
	}
	
	@RequestMapping(value="/jobapplicationflowAffidavit.do", method=RequestMethod.GET)
	public String ManageJobApplicationFlowAffidavitGET(ModelMap map,HttpServletRequest request)
	{
		printdata("JobApplicationProcessController ( ManageJobApplicationFlowAffidavitGET ) => jobapplicationflowAffidavit.do");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}	
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

		boolean isDspqRequired=false;
		
		String sJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
		String candidateTypeDec = request.getParameter("ct")==null?"":request.getParameter("ct").trim();
		String sCandidateType=validateCTAndDec(candidateTypeDec);
		
		int iJobId=Utility.getIntValue(sJobId);
		
		map.addAttribute("iJobId", iJobId);
		map.addAttribute("candidateType", candidateTypeDec);
		
		boolean isCoverLetterG1=false;
		boolean isPersonalInformationG1=false;
		boolean isAffiliateG4=false;
		boolean isProfessionalG4=false;
		
		Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
		
		JobOrder jobOrder=jobOrderDAO.findById(iJobId, false, false);
		DspqPortfolioName dspqPortfolioNameNew =dspqServiceAjax.getDSPQByJobOrder(jobOrder,sCandidateType);
		
		if(dspqPortfolioNameNew!=null)
		{
			isDspqRequired=true;
			map.addAttribute("dspqPortfolioName", dspqPortfolioNameNew);
			
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, sCandidateType);
			
			Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
			isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
			isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
			isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
			isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
		}
		
		JobCategoryMaster jobCategoryMaster=null;
		if(jobOrder!=null )
			jobCategoryMaster=jobOrder.getJobCategoryMaster();
		boolean isQQRequired=getPreSreen(jobCategoryMaster, sCandidateType);
		boolean isEPI=getEPI(jobCategoryMaster, sCandidateType);
		boolean isJSI=getCustomQuestion(jobOrder, jobCategoryMaster, sCandidateType, teacherDetail);
		printdata("sCandidateType "+sCandidateType +" isQQRequired "+isQQRequired);
		
		Map<Integer, String> mapNextAndPreURL=new HashMap<Integer, String>();
		Map<String,Integer> mapNextAndPreNavigater=new HashMap<String,Integer>();
		String sPrevious="",sNext="";
		
		setPrevAndNextNavigation(mapGroup, mapNextAndPreURL, mapNextAndPreNavigater, sJobId, candidateTypeDec, isDspqRequired, isCoverLetterG1, isPersonalInformationG1, isProfessionalG4, isAffiliateG4, isQQRequired,isEPI,isJSI);
		map.addAttribute("group", 4);
		sNext=getNextNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "AF");
		sPrevious=getPrevNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "AF");
		
		map.addAttribute("sNextURL", sNext);
		map.addAttribute("sPreviousURL", sPrevious);
		map.addAttribute("sCurrentPageShortCode", "AF");
		return "jafgroup05";
	}
	
	@RequestMapping(value="/jobapplicationflowprescreen.do", method=RequestMethod.GET)
	public String ManageJobApplicationFlowPreScreenGET(ModelMap map,HttpServletRequest request)
	{
		printdata("JobApplicationProcessController ( ManageJobApplicationFlowPreScreenGET ) => jobapplicationflowprescreen.do");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}	
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

		boolean isDspqRequired=false;
		
		String sJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
		String candidateTypeDec = request.getParameter("ct")==null?"":request.getParameter("ct").trim();
		String sCandidateType=validateCTAndDec(candidateTypeDec);
		printdata("sJobId "+sJobId +" candidateType "+sCandidateType);
		
		int iJobId=Utility.getIntValue(sJobId);
		
		map.addAttribute("iJobId", iJobId);
		map.addAttribute("candidateType", candidateTypeDec);
		
		boolean isCoverLetterG1=false;
		boolean isPersonalInformationG1=false;
		boolean isAffiliateG4=false;
		boolean isProfessionalG4=false;
		
		Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
		
		JobOrder jobOrder=jobOrderDAO.findById(iJobId, false, false);
		DspqPortfolioName dspqPortfolioNameNew =dspqServiceAjax.getDSPQByJobOrder(jobOrder,sCandidateType);
		
		if(dspqPortfolioNameNew!=null)
		{
			isDspqRequired=true;
			
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, sCandidateType);
			
			Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
			isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
			isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
			isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
			isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
			
		}
		if(dspqPortfolioNameNew!=null && dspqPortfolioNameNew.getDspqPortfolioNameId()!=null)
			map.addAttribute("dspqPortfolioName", dspqPortfolioNameNew.getDspqPortfolioNameId());
		else
			map.addAttribute("dspqPortfolioName", 0);
		
		JobCategoryMaster jobCategoryMaster=null;
		if(jobOrder!=null )
			jobCategoryMaster=jobOrder.getJobCategoryMaster();
		boolean isQQRequired=getPreSreen(jobCategoryMaster, sCandidateType);
		boolean isEPI=getEPI(jobCategoryMaster, sCandidateType);
		boolean isJSI=getCustomQuestion(jobOrder, jobCategoryMaster, sCandidateType, teacherDetail);
		printdata("sCandidateType "+sCandidateType +" isQQRequired "+isQQRequired +" isEPI "+isEPI);
		
		Map<Integer, String> mapNextAndPreURL=new HashMap<Integer, String>();
		Map<String,Integer> mapNextAndPreNavigater=new HashMap<String,Integer>();
		String sPrevious="",sNext="";
		
		setPrevAndNextNavigation(mapGroup, mapNextAndPreURL, mapNextAndPreNavigater, sJobId, candidateTypeDec, isDspqRequired, isCoverLetterG1, isPersonalInformationG1, isProfessionalG4, isAffiliateG4, isQQRequired,isEPI,isJSI);
		
		map.addAttribute("group", 4);
		
		sNext=getNextNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "QQ");
		sPrevious=getPrevNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "QQ");
		
		map.addAttribute("sNextURL", sNext);
		map.addAttribute("sPreviousURL", sPrevious);
		map.addAttribute("sCurrentPageShortCode", "QQ");
		printdata("sNextURL "+sNext +" sPreviousURL "+sPrevious);
		
		return "jafgroup06PreScreen";
	}
	
	public String validateCTAndDec(String sCandidateType)
	{
		String sDec_candidateType="E";
		try {
			sDec_candidateType=Utility.decodeBase64(sCandidateType);
		} catch (Exception e) { e.printStackTrace(); }
		if(sDec_candidateType==null || sDec_candidateType.equals(""))
		{
			sCandidateType="E";
		}
		else
		{
			sCandidateType=sDec_candidateType;
		}
		return sCandidateType;
	}
	
	
	
	
	public boolean getPreSreen(JobCategoryMaster jobCategoryMaster,String sCandidateType)
	{
		return dspqServiceAjax.getPreSreen(jobCategoryMaster, sCandidateType);
	}
	
	public boolean setPrevAndNextNavigation(Map<Integer, Boolean> mapGroup, Map<Integer, String> mapNextAndPreURL, Map<String,Integer> mapNextAndPreNavigater, String sJobId, String candidateTypeDec,boolean isDspqRequired,boolean isCoverLetterG1,boolean isPersonalInformationG1,boolean isProfessionalG4,boolean isAffiliateG4,boolean isQQRequired,boolean isEPI,boolean isJSI)
	{
		return dspqServiceAjax.setPrevAndNextNavigation(mapGroup, mapNextAndPreURL, mapNextAndPreNavigater, sJobId, candidateTypeDec, isDspqRequired, isCoverLetterG1, isPersonalInformationG1, isProfessionalG4, isAffiliateG4, isQQRequired,isEPI,isJSI);
	}
	
	public String getNextNavigation(Map<String,Integer> mapNextAndPreNavigater,Map<Integer, String> mapNextAndPreURL,String sInputShortCode)
	{
		return dspqServiceAjax.getNextNavigation(mapNextAndPreNavigater, mapNextAndPreURL, sInputShortCode);
	}
	
	public String getPrevNavigation(Map<String,Integer> mapNextAndPreNavigater,Map<Integer, String> mapNextAndPreURL,String sInputShortCode)
	{
		return dspqServiceAjax.getPrevNavigation(mapNextAndPreNavigater, mapNextAndPreURL, sInputShortCode);
	}
	
	public Map<String,Boolean> getPortfolioG1G4(Map<Integer, Boolean> mapGroup,Map<Integer, Boolean> mapSection,List<DspqRouter> dspqRouterList)
	{
		return dspqServiceAjax.getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
	}
	
	public boolean getPortfolioG1G4Required(Map<String,Boolean> mapPortfolioG1G4,String sInputName)
	{
		return dspqServiceAjax.getPortfolioG1G4Required(mapPortfolioG1G4, sInputName);
	}
	
	public boolean getEPI(JobCategoryMaster jobCategoryMaster,String sCandidateType)
	{
		return dspqServiceAjax.getEPI(jobCategoryMaster, sCandidateType);
	}
	
	public boolean getCustomQuestion(JobOrder jobOrder,JobCategoryMaster jobCategoryMaster,String sCandidateType,TeacherDetail teacherDetail)
	{
		return dspqServiceAjax.getCustomQuestion(jobOrder, jobCategoryMaster, sCandidateType, teacherDetail);
	}
	
	@RequestMapping(value="/jobapplicationflowepi.do", method=RequestMethod.GET)
	public String ManageJobApplicationFlowEPIGET(ModelMap map,HttpServletRequest request)
	{
		printdata("JobApplicationProcessController ( ManageJobApplicationFlowEPIGET ) => jobapplicationflowepi.do");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}	
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

		boolean isDspqRequired=false;
		
		String sJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
		String candidateTypeDec = request.getParameter("ct")==null?"":request.getParameter("ct").trim();
		String sts = request.getParameter("sts")==null?"":request.getParameter("sts").trim();
		String at = request.getParameter("at")==null?"":request.getParameter("at").trim();
		String sCandidateType=validateCTAndDec(candidateTypeDec);
		printdata("sJobId "+sJobId +" candidateType "+sCandidateType);
		
		int iJobId=Utility.getIntValue(sJobId);
		
		map.addAttribute("iJobId", iJobId);
		map.addAttribute("candidateType", candidateTypeDec);
		
		boolean isCoverLetterG1=false;
		boolean isPersonalInformationG1=false;
		boolean isAffiliateG4=false;
		boolean isProfessionalG4=false;
		
		Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
		
		JobOrder jobOrder=jobOrderDAO.findById(iJobId, false, false);
		DspqPortfolioName dspqPortfolioNameNew =dspqServiceAjax.getDSPQByJobOrder(jobOrder,sCandidateType);

		if(dspqPortfolioNameNew!=null)
		{
			isDspqRequired=true;
			
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, sCandidateType);
			
			Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
			isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
			isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
			isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
			isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
			
		}
		if(dspqPortfolioNameNew!=null && dspqPortfolioNameNew.getDspqPortfolioNameId()!=null)
			map.addAttribute("dspqPortfolioName", dspqPortfolioNameNew.getDspqPortfolioNameId());
		else
			map.addAttribute("dspqPortfolioName", 0);
		
		JobCategoryMaster jobCategoryMaster=null;
		if(jobOrder!=null )
			jobCategoryMaster=jobOrder.getJobCategoryMaster();
		boolean isQQRequired=getPreSreen(jobCategoryMaster, sCandidateType);
		boolean isEPI=getEPI(jobCategoryMaster, sCandidateType);
		boolean isJSI=getCustomQuestion(jobOrder, jobCategoryMaster, sCandidateType, teacherDetail);
		printdata("sCandidateType "+sCandidateType +" isQQRequired "+isQQRequired+" isJSI "+isJSI);
		
		Map<Integer, String> mapNextAndPreURL=new HashMap<Integer, String>();
		Map<String,Integer> mapNextAndPreNavigater=new HashMap<String,Integer>();
		String sPrevious="",sNext="";
		
		setPrevAndNextNavigation(mapGroup, mapNextAndPreURL, mapNextAndPreNavigater, sJobId, candidateTypeDec, isDspqRequired, isCoverLetterG1, isPersonalInformationG1, isProfessionalG4, isAffiliateG4, isQQRequired,isEPI,isJSI);
		
		map.addAttribute("group", 4);
		
		sNext=getNextNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "EPI");
		sPrevious=getPrevNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "EPI");
		
		printdata("sNext "+sNext);
		
		map.addAttribute("sNextURL", sNext);
		map.addAttribute("sPreviousURL", sPrevious);
		map.addAttribute("sts", sts);
		map.addAttribute("at", at);
		map.addAttribute("sCurrentPageShortCode", "EPI");
		printdata("sNextURL "+sNext +" sPreviousURL "+sPrevious);
		
		return "jafgroup07epi";
	}
	@RequestMapping(value="/jobapplicationflowrunassessmentstep2.do", method=RequestMethod.GET)
	public String ManageJobApplicationFlowEPIStep2GET(ModelMap map,HttpServletRequest request)
	{
		printdata("JobApplicationProcessController ( ManageJobApplicationFlowEPIStep2GET ) => jobapplicationflowrunassessmentstep2.do");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}	
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

		boolean isDspqRequired=false;
		
		String sJobId = request.getParameter("dspqJobId")==null?"":request.getParameter("dspqJobId").trim();
		String candidateTypeDec = request.getParameter("ct")==null?"":request.getParameter("ct").trim();
		String sCandidateType=validateCTAndDec(candidateTypeDec);
		printdata("sJobId "+sJobId +" candidateType "+sCandidateType);
		
		int iJobId=Utility.getIntValue(sJobId);
		
		map.addAttribute("iJobId", iJobId);
		map.addAttribute("candidateType", candidateTypeDec);
		
		boolean isCoverLetterG1=false;
		boolean isPersonalInformationG1=false;
		boolean isAffiliateG4=false;
		boolean isProfessionalG4=false;
		
		Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
		
		JobOrder jobOrder=jobOrderDAO.findById(iJobId, false, false);
		DspqPortfolioName dspqPortfolioNameNew =dspqServiceAjax.getDSPQByJobOrder(jobOrder,sCandidateType);
		
		if(dspqPortfolioNameNew!=null)
		{
			isDspqRequired=true;
			
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, sCandidateType);
			
			Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
			isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
			isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
			isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
			isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
			
		}
		if(dspqPortfolioNameNew!=null && dspqPortfolioNameNew.getDspqPortfolioNameId()!=null)
			map.addAttribute("dspqPortfolioName", dspqPortfolioNameNew.getDspqPortfolioNameId());
		else
			map.addAttribute("dspqPortfolioName", 0);
		
		JobCategoryMaster jobCategoryMaster=null;
		if(jobOrder!=null )
			jobCategoryMaster=jobOrder.getJobCategoryMaster();
		boolean isQQRequired=getPreSreen(jobCategoryMaster, sCandidateType);
		boolean isEPI=getEPI(jobCategoryMaster, sCandidateType);
		boolean isJSI=getCustomQuestion(jobOrder, jobCategoryMaster, sCandidateType, teacherDetail);
		printdata("sCandidateType "+sCandidateType +" isQQRequired "+isQQRequired);
		
		Map<Integer, String> mapNextAndPreURL=new HashMap<Integer, String>();
		Map<String,Integer> mapNextAndPreNavigater=new HashMap<String,Integer>();
		String sPrevious="",sNext="";
		
		setPrevAndNextNavigation(mapGroup, mapNextAndPreURL, mapNextAndPreNavigater, sJobId, candidateTypeDec, isDspqRequired, isCoverLetterG1, isPersonalInformationG1, isProfessionalG4, isAffiliateG4, isQQRequired,isEPI,isJSI);
		
		map.addAttribute("group", 4);
		
		sNext=getNextNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "EPI");
		sPrevious=getPrevNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "EPI");
		
		map.addAttribute("sNextURL", sNext);
		map.addAttribute("sPreviousURL", sPrevious);
		
		printdata("sNextURL "+sNext +" sPreviousURL "+sPrevious);
		
		//*************************************** Start EPI *********************************************
		
		map.addAttribute("teacherDetail", teacherDetail);
		
		String assessmentId = request.getParameter("assessmentId");
		String jobId = request.getParameter("jobId");
		String newJobId = request.getParameter("newJobId");
		AssessmentDetail assessmentDetail = null;
		TeacherAssessmentdetail teacherAssessmentdetail = null;
		int remainingSessionTime = 0;
		JobOrder jobOrderEPI = null;
		int attemptId = 0;
		int totalAttempted = 0;
		int assessmentType = 1;
		try 
		{
			if(assessmentId!=null && assessmentId.trim().length()>0)
			{
				int iassessmentId=Utility.getIntValue(assessmentId);
				assessmentDetail = assessmentDetailDAO.findById(iassessmentId, false, false);
				List<TeacherAssessmentdetail> teacherAssessmentdetails = null;
				assessmentType = assessmentDetail.getAssessmentType();
				teacherAssessmentdetails=teacherAssessmentDetailDAO.getTeacherAssessmentdetails(assessmentDetail, teacherDetail);
				if(teacherAssessmentdetails.size()!=0)
					teacherAssessmentdetail = teacherAssessmentdetails.get(0);
				
				int totalAssessmentSessionTime = teacherAssessmentdetail.getAssessmentSessionTime()==null?0:teacherAssessmentdetail.getAssessmentSessionTime();
				int consumedSessionTime = 0;
				TeacherAssessmentAttempt teacherAssessmentAttempt = null;
				List<TeacherAssessmentAttempt> teacherAssessmentAttempts = assessmentAttemptDAO.findNoOfAttempts(assessmentDetail, teacherDetail, teacherAssessmentdetail.getAssessmentTakenCount());
				
				if (!teacherAssessmentAttempts.isEmpty()) 
				{
					for (TeacherAssessmentAttempt teacherAssessmentAttempt1 : teacherAssessmentAttempts) {
						consumedSessionTime+=teacherAssessmentAttempt1.getAssessmentSessionTime();
						
						if(!teacherAssessmentAttempt1.getIsForced())
							totalAttempted++;
					}
					
					teacherAssessmentAttempt = teacherAssessmentAttempts.get(teacherAssessmentAttempts.size()-1);
					attemptId = teacherAssessmentAttempt.getAttemptId();
				}
				
				remainingSessionTime = totalAssessmentSessionTime-consumedSessionTime;
				
			}
			if(jobId!=null && jobId.trim().length()>0)
			{
				int ijobId=Utility.getIntValue(jobId);
				jobOrderEPI = jobOrderDAO.findById(ijobId, false, false);
			}


			List<TeacherAnswerDetail> teacherAnswerDetails = null;
			
			List<TeacherStrikeLog> teacherStrikeLogList= null;
			teacherStrikeLogList = teacherStrikeLogDAO.getTotalStrikesByTeacher(teacherDetail, teacherAssessmentdetail);

			map.addAttribute("assessmentDetail", assessmentDetail);	
			map.addAttribute("teacherAssessmentdetail", teacherAssessmentdetail);
			map.addAttribute("jobOrder", jobOrderEPI);
			map.addAttribute("remainingSessionTime", remainingSessionTime);
			map.addAttribute("attemptId", attemptId);
			map.addAttribute("totalAttempted",totalAttempted);

			map.addAttribute("totalSkippedQuestions", 0);
			map.addAttribute("newJobId", newJobId);
			map.addAttribute("isCompleted", true);
			int teacherStrikeLogListSize=0;
			if(teacherStrikeLogList!=null){
				teacherStrikeLogListSize=teacherStrikeLogList.size();
			}
			map.addAttribute("totalStrikes",teacherStrikeLogListSize);
			String rURL = "userdashboard.do";
			boolean epiStandalone = teacherDetail.getIsPortfolioNeeded();
			printdata("epiStandalone:::: "+epiStandalone);
			if(!epiStandalone)
				rURL = "portaldashboard.do";
			
			if(assessmentType==1)
			{
				List<JobForTeacher> lstJobForTeacher = jobForTeacherDAO.findMostRecentJobByTeacher(teacherDetail);
				if(lstJobForTeacher.size()>0)
				{
					boolean onDashboard = false;
					jobOrder = lstJobForTeacher.get(0).getJobId();
					int flagForURL = jobOrder.getFlagForURL();
					int flagForMessage  = jobOrder.getFlagForMessage();
					String exitMessage = jobOrder.getExitMessage();
					String exitURL = jobOrder.getExitURL();
					
					if(exitMessage!=null && !exitMessage.trim().equals("") && exitURL!=null && !exitURL.trim().equals(""))
					{
						onDashboard = true;
					}else if(flagForMessage==1)
					{
						onDashboard = false;
						rURL = "thankyoumessage.do?jobId="+jobOrder.getJobId();
						rURL=getURL(teacherDetail, jobOrder,rURL);
						//sb.append("window.location.href='thankyoumessage.do?jobId="+jobOrder.getJobId()+"'");

					}else if(flagForURL==1)
					{
						if(!epiStandalone)
							rURL = "portaldashboard.do?jobId="+jobOrder.getJobId()+"&es=1";
							//rURL=exitURL;
						else
						{
							rURL = "userdashboard.do?jobId="+jobOrder.getJobId()+"&es=1";;
							rURL=getURL(teacherDetail, jobOrder,rURL);
						}
							//rURL=exitURL;
						
					}
					
					/*if(rURL.indexOf("http")==-1)
						rURL="http://"+rURL;*/
				}
			}
			printdata("rURL::: "+rURL);
			map.addAttribute("rURL",rURL);
			
			map.addAttribute("EPITeacherBaseInventory1",Utility.getLocaleValuePropByKey("EPITeacherBaseInventory", locale));
	        
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		//*************************************** End EPI *********************************************
		if(assessmentType==1)
		{
			return "jafgroup07epistep2";
		}
		else
		{
			return "jafgroup08custquestionstep2";
		}
	}
	
	@RequestMapping(value="/jobapplicationflowcustomquestion.do", method=RequestMethod.GET)
	public String ManageJobApplicationFlowCustomQuestionGET(ModelMap map,HttpServletRequest request)
	{
		printdata("JobApplicationProcessController ( ManageJobApplicationFlowCustomQuestionGET ) => jobapplicationflowcustomquestion.do");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}	
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

		boolean isDspqRequired=false;
		
		String sJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
		String candidateTypeDec = request.getParameter("ct")==null?"":request.getParameter("ct").trim();
		String sts = request.getParameter("sts")==null?"":request.getParameter("sts").trim();
		String at = request.getParameter("at")==null?"":request.getParameter("at").trim();
		String sCandidateType=validateCTAndDec(candidateTypeDec);
		printdata("sJobId "+sJobId +" candidateType "+sCandidateType);
		
		int iJobId=Utility.getIntValue(sJobId);
		
		map.addAttribute("iJobId", iJobId);
		map.addAttribute("candidateType", candidateTypeDec);
		
		boolean isCoverLetterG1=false;
		boolean isPersonalInformationG1=false;
		boolean isAffiliateG4=false;
		boolean isProfessionalG4=false;
		
		Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
		
		JobOrder jobOrder=jobOrderDAO.findById(iJobId, false, false);
		DspqPortfolioName dspqPortfolioNameNew =dspqServiceAjax.getDSPQByJobOrder(jobOrder,sCandidateType);
		
		if(dspqPortfolioNameNew!=null)
		{
			isDspqRequired=true;
			
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, sCandidateType);
			
			Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
			isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
			isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
			isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
			isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
			
		}
		if(dspqPortfolioNameNew!=null && dspqPortfolioNameNew.getDspqPortfolioNameId()!=null)
			map.addAttribute("dspqPortfolioName", dspqPortfolioNameNew.getDspqPortfolioNameId());
		else
			map.addAttribute("dspqPortfolioName", 0);
		
		JobCategoryMaster jobCategoryMaster=null;
		if(jobOrder!=null )
			jobCategoryMaster=jobOrder.getJobCategoryMaster();
		boolean isQQRequired=getPreSreen(jobCategoryMaster, sCandidateType);
		boolean isEPI=getEPI(jobCategoryMaster, sCandidateType);
		boolean isJSI=getCustomQuestion(jobOrder, jobCategoryMaster, sCandidateType, teacherDetail);
		printdata("sCandidateType "+sCandidateType +" isQQRequired "+isQQRequired+" isJSI "+isJSI);
		
		Map<Integer, String> mapNextAndPreURL=new HashMap<Integer, String>();
		Map<String,Integer> mapNextAndPreNavigater=new HashMap<String,Integer>();
		String sPrevious="",sNext="";
		
		setPrevAndNextNavigation(mapGroup, mapNextAndPreURL, mapNextAndPreNavigater, sJobId, candidateTypeDec, isDspqRequired, isCoverLetterG1, isPersonalInformationG1, isProfessionalG4, isAffiliateG4, isQQRequired,isEPI,isJSI);
		
		map.addAttribute("group", 4);
		
		sNext=getNextNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "JSI");
		sPrevious=getPrevNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "JSI");
		
		printdata("sNext "+sNext);
		
		map.addAttribute("sNextURL", sNext);
		map.addAttribute("sPreviousURL", sPrevious);
		map.addAttribute("sts", sts);
		map.addAttribute("at", at);
		map.addAttribute("sCurrentPageShortCode", "JSI");
		printdata("sNextURL "+sNext +" sPreviousURL "+sPrevious);
		
		return "jafgroup08custquestion";
	}
	
	public String getURL(TeacherDetail teacherDetail, JobOrder jobOrder,String rURL)
	{
		
		int IsAffilated=0;
		JobForTeacher jobForTeacher = jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
		DistrictMaster districtMaster=jobOrder.getDistrictMaster();
		
		List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
		boolean internalFlag=false;
		boolean offerJSI =false;
		if(itcList.size()>0)
			internalFlag=true;
		
		if(jobOrder.getDistrictMaster()!=null && internalFlag)
		{
			districtMaster = jobOrder.getDistrictMaster();
			if(districtMaster.getOfferJSI())
				offerJSI=true;
		}
		try{
			
			 if(jobForTeacher!=null && jobForTeacher.getIsAffilated()!=null)
			 {
				 if(jobForTeacher.getIsAffilated()==1)
					 IsAffilated=1;
			 }
		    if(jobOrder.getJobCategoryMaster()!=null && IsAffilated==1 && internalFlag==false)
		    {
				if(jobOrder.getJobCategoryMaster().getOfferJSI()!=0)
				{
					offerJSI=true;
				}
				else
				{
					offerJSI=false;
				}
			 }
		}catch(Exception e){
			e.printStackTrace();
		}
		if(jobOrder.getJobAssessmentStatus()==1 && ((offerJSI && internalFlag )||(internalFlag==false && IsAffilated==0) ||(internalFlag==false && IsAffilated==1 && offerJSI)))
			if(jobOrder.getIsJobAssessment()!=null &&  jobOrder.getIsJobAssessment()==true)
				if((jobForTeacher!=null) && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("icomp"))
					rURL = "applyjob.do?jobId="+jobOrder.getJobId();
		
		return rURL;
	}
	
	@RequestMapping(value="/jobapplicationflowcompleted.do", method=RequestMethod.GET)
	public String ManageJAFCompletedGET(ModelMap map,HttpServletRequest request)
	{
		printdata("JobApplicationProcessController ( ManageJAFCompletedGET ) => jobapplicationflowcompleted.do");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}	
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

		boolean isDspqRequired=false;
		
		String sJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId").trim();
		String candidateTypeDec = request.getParameter("ct")==null?"":request.getParameter("ct").trim();
		String sCandidateType=validateCTAndDec(candidateTypeDec);
		printdata("sJobId "+sJobId +" candidateType "+sCandidateType);
		
		int iJobId=Utility.getIntValue(sJobId);
		
		map.addAttribute("iJobId", iJobId);
		map.addAttribute("candidateType", candidateTypeDec);
		
		boolean isCoverLetterG1=false;
		boolean isPersonalInformationG1=false;
		boolean isAffiliateG4=false;
		boolean isProfessionalG4=false;
		
		Map<Integer, Boolean> mapGroup=new HashMap<Integer, Boolean>();
		Map<Integer, Boolean> mapSection=new HashMap<Integer, Boolean>();
		
		JobOrder jobOrder=jobOrderDAO.findById(iJobId, false, false);
		DspqPortfolioName dspqPortfolioNameNew =dspqServiceAjax.getDSPQByJobOrder(jobOrder,sCandidateType);
		
		if(dspqPortfolioNameNew!=null)
		{
			isDspqRequired=true;
			
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getSectionByPortfolioNameAndCT(dspqPortfolioNameNew, sCandidateType);
			
			Map<String,Boolean> mapPortfolioG1G4=getPortfolioG1G4(mapGroup, mapSection, dspqRouterList);
			isCoverLetterG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.CoverLetterGroup01);
			isPersonalInformationG1=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.PersonalInformationGroup01);
			isProfessionalG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.ProfessionalGroup04);
			isAffiliateG4=getPortfolioG1G4Required(mapPortfolioG1G4, JobApplicationFlowConfig.AffidavitGroup04);
			
		}
		if(dspqPortfolioNameNew!=null && dspqPortfolioNameNew.getDspqPortfolioNameId()!=null)
			map.addAttribute("dspqPortfolioName", dspqPortfolioNameNew.getDspqPortfolioNameId());
		else
			map.addAttribute("dspqPortfolioName", 0);
		
		JobCategoryMaster jobCategoryMaster=null;
		if(jobOrder!=null )
			jobCategoryMaster=jobOrder.getJobCategoryMaster();
		boolean isQQRequired=getPreSreen(jobCategoryMaster, sCandidateType);
		boolean isEPI=getEPI(jobCategoryMaster, sCandidateType);
		boolean isJSI=getCustomQuestion(jobOrder, jobCategoryMaster, sCandidateType, teacherDetail);
		printdata("sCandidateType "+sCandidateType +" isQQRequired "+isQQRequired);
		
		Map<Integer, String> mapNextAndPreURL=new HashMap<Integer, String>();
		Map<String,Integer> mapNextAndPreNavigater=new HashMap<String,Integer>();
		String sPrevious="",sNext="";
		
		setPrevAndNextNavigation(mapGroup, mapNextAndPreURL, mapNextAndPreNavigater, sJobId, candidateTypeDec, isDspqRequired, isCoverLetterG1, isPersonalInformationG1, isProfessionalG4, isAffiliateG4, isQQRequired,isEPI,isJSI);
		
		map.addAttribute("group", 4);
		
		sNext=getNextNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "Comp");
		sPrevious=getPrevNavigation(mapNextAndPreNavigater, mapNextAndPreURL, "Comp");
		
		map.addAttribute("sNextURL", sNext);
		map.addAttribute("sPreviousURL", sPrevious);
		map.addAttribute("sCurrentPageShortCode", "Comp");
		
		printdata("sNextURL "+sNext +" sPreviousURL "+sPrevious);
		
		DspqJobWiseStatus dspqJobWiseStatus=dspqJobWiseStatusDAO.getDSPQStatus(teacherDetail, jobOrder);
		if(dspqJobWiseStatus==null)
		{
			dspqJobWiseStatus=new DspqJobWiseStatus();
			dspqJobWiseStatus.setTeacherDetail(teacherDetail);
			dspqJobWiseStatus.setJobOrder(jobOrder);
			dspqJobWiseStatus.setiPAddress(IPAddressUtility.getIpAddress(request));
			dspqJobWiseStatus.setCreatedDataTime(new Date());
			dspqJobWiseStatusDAO.makePersistent(dspqJobWiseStatus);
		}
		
		return "jafcomplete";
	}
	
	public void printdata(String sInputValue)
	{
		if(sInputValue!=null && !sInputValue.equals(""))
			System.out.println(new Date()+" => "+sInputValue);
	}
	
	
	@RequestMapping(value="/newjafexternal.do", method=RequestMethod.GET)
	public String setCoverLetter(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		try {	
			HttpSession session = request.getSession(false);
			session.setAttribute("newjafexternal", "1");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:signin.do";
	}
	
}
