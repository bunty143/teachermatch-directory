package tm.controller.jobapplication;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.master.SpokenLanguageMaster;
import tm.bean.teacher.TeacherLanguages;
import tm.dao.master.SpokenLanguageMasterDAO;
import tm.dao.teacher.TeacherLanguagesDAO;

@Controller
public class TeacherLanguageMasterController 
{
	
	@Autowired
	private TeacherLanguagesDAO teacherLanguagesDAO;
	
	@Autowired
	private SpokenLanguageMasterDAO spokenLanguageMasterDAO;
	
	@RequestMapping(value="/gnlanmaster.do", method=RequestMethod.GET)
	public String GenerateLanguageMasterGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println("start running gnlanmaster.do");
		String sMsg="";
		String password=request.getParameter("p");
		if(password!=null && !password.equals("") && password.equalsIgnoreCase("4GLanguage"))
		{
			List<String> lstTeacherLanguages=teacherLanguagesDAO.getDistinctTeacherLanguages();
			System.out.println("lstTeacherLanguages size "+lstTeacherLanguages.size());
			
			SessionFactory sessionFactory=spokenLanguageMasterDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	 	    Transaction txOpen =null;
	 	    
			int iCounter=1;
			for(String sTeacherLanguages:lstTeacherLanguages)
			{
				if(sTeacherLanguages!=null && !sTeacherLanguages.equalsIgnoreCase(""))
				{
					sTeacherLanguages=sTeacherLanguages.trim();
					
					SpokenLanguageMaster spokenLanguageMaster=new SpokenLanguageMaster();
					spokenLanguageMaster.setLanguageName(sTeacherLanguages);
					spokenLanguageMaster.setStatus("A");
					
					txOpen =statelesSsession.beginTransaction();
	        	 	statelesSsession.insert(spokenLanguageMaster);
	        	 	txOpen.commit();
	        	 	
					//spokenLanguageMasterDAO.makePersistent(spokenLanguageMaster);
					
					System.out.println(iCounter+". "+sTeacherLanguages +" Id "+spokenLanguageMaster.getSpokenlanguagemasterid());
					
					iCounter++;
					
				}
			}
			sMsg="Added "+iCounter;
		}
		else
		{
			sMsg="Something went wrong!!! We have catched issue and Fix it asap";
		}
		
		System.out.println("End running gnlanmaster.do");
		map.addAttribute("spdata",sMsg);
		return "sphelp";
	}
	
	
	@RequestMapping(value="/updateteacherlanguagesfrmmaster.do", method=RequestMethod.GET)
	public String UpdateTeacherLanguageFromMasterGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println("start running updateteacherlanguagesfrmmaster.do");
		String sMsg="";
		String password=request.getParameter("p");
		if(password!=null && !password.equals("") && password.equalsIgnoreCase("4GLanguageUpdate"))
		{
			Map<String, SpokenLanguageMaster> mapSpokenLanguageMaster=new TreeMap<String, SpokenLanguageMaster>();
			List<SpokenLanguageMaster> spokenLanguageMasters=spokenLanguageMasterDAO.findAllActiveSpokenLanguageMasterByOrder();
			for(SpokenLanguageMaster spknLngMstr:spokenLanguageMasters)
			{
				String sLanguageName=spknLngMstr.getLanguageName();
				sLanguageName=sLanguageName.trim();
				sLanguageName=sLanguageName.toUpperCase();
				mapSpokenLanguageMaster.put(sLanguageName, spknLngMstr);
			}
			
			List<TeacherLanguages> lstTeacherLanguages=teacherLanguagesDAO.getNullspokenLanguageMasterId();
			System.out.println("lstTeacherLanguages size "+lstTeacherLanguages.size());
			int iTotalCounter=lstTeacherLanguages.size();
			
			SessionFactory sessionFactory=teacherLanguagesDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	 	    Transaction txOpen =null;
	 	    
			int iCounter=1;
			for(TeacherLanguages teacherLanguages:lstTeacherLanguages)
			{
				if(teacherLanguages!=null && teacherLanguages.getSpokenLanguageMaster()==null)
				{
					if(teacherLanguages.getLanguage()!=null && !teacherLanguages.getLanguage().equals(""))
					{
						String sLanguageName=teacherLanguages.getLanguage();
						sLanguageName=sLanguageName.trim();
						sLanguageName=sLanguageName.toUpperCase();
						if(mapSpokenLanguageMaster!=null && mapSpokenLanguageMaster.get(sLanguageName)!=null)
						{
							SpokenLanguageMaster spokenLanguageMaster=mapSpokenLanguageMaster.get(sLanguageName);
							teacherLanguages.setSpokenLanguageMaster(spokenLanguageMaster);
							
							txOpen =statelesSsession.beginTransaction();
		            	 	statelesSsession.update(teacherLanguages);
		            	 	txOpen.commit();
		            	 	
							
							System.out.println("Update done "+iCounter +" of "+iTotalCounter);
							iCounter++;
						}
					}
				}
			}
			sMsg="Updated "+iCounter +" of "+iTotalCounter;
		}
		else
		{
			sMsg="Something went wrong!!! We have catched issue and Fix it asap";
		}
		
		System.out.println("End running updateteacherlanguagesfrmmaster.do");
		map.addAttribute("spdata",sMsg);
		return "sphelp";
	}
	
}
