package tm.controller.assessment;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificRefChkOptions;
import tm.bean.master.DistrictSpecificRefChkQuestions;
import tm.bean.master.QuestionTypeMaster;
import tm.bean.master.ReferenceQuestionSets;
import tm.bean.user.UserMaster;
import tm.dao.JobOrderDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentQuestionsDAO;
import tm.dao.assessment.AssessmentSectionDAO;
import tm.dao.assessment.StageOneStatusDAO;
import tm.dao.assessment.StageThreeStatusDAO;
import tm.dao.assessment.StageTwoStatusDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.CompetencyMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.DistrictSpecificRefChkQuestionsDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.ObjectiveMasterDAO;
import tm.dao.master.QuestionTypeMasterDAO;
import tm.dao.master.ReferenceQuestionSetsDAO;
import tm.dao.menu.RoleAccessPermissionDAO;

/* @Author: Hanzala Subahni
 * @Discription: Assessment Controller.
 */

@Controller
public class ReferenceChkController {

	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	public void setAssessmentDetailDAO(AssessmentDetailDAO assessmentDetailDAO) {
		this.assessmentDetailDAO = assessmentDetailDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private AssessmentSectionDAO assessmentSectionDAO;
	public void setAssessmentSectionDAO(
			AssessmentSectionDAO assessmentSectionDAO) {
		this.assessmentSectionDAO = assessmentSectionDAO;
	}
	
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) {
		this.domainMasterDAO = domainMasterDAO;
	}
	
	@Autowired
	private CompetencyMasterDAO competencyMasterDAO;
	public void setCompetencyMasterDAO(CompetencyMasterDAO competencyMasterDAO) {
		this.competencyMasterDAO = competencyMasterDAO;
	}
	
	@Autowired
	private ObjectiveMasterDAO objectiveMasterDAO;
	public void setObjectiveMasterDAO(ObjectiveMasterDAO objectiveMasterDAO) {
		this.objectiveMasterDAO = objectiveMasterDAO;
	}
	
	@Autowired
	private QuestionTypeMasterDAO questionTypeMasterDAO;
	public void setQuestionTypeMasterDAO(QuestionTypeMasterDAO questionTypeMasterDAO) {
		this.questionTypeMasterDAO = questionTypeMasterDAO;
	}
	
	@Autowired
	private AssessmentQuestionsDAO assessmentQuestionsDAO;
	public void setAssessmentQuestionsDAO(AssessmentQuestionsDAO assessmentQuestionsDAO) {
		this.assessmentQuestionsDAO = assessmentQuestionsDAO;
	}
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}

	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	
	@Autowired
	private StageOneStatusDAO stageOneStatusDAO;
	
	@Autowired
	private StageTwoStatusDAO stageTwoStatusDAO;
	
	@Autowired
	private StageThreeStatusDAO stageThreeStatusDAO;
	
	@Autowired
	private DistrictSpecificRefChkQuestionsDAO districtSpecificRefChkQuestionsDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private ReferenceQuestionSetsDAO referenceQuestionSetsDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@InitBinder
	public void initBinder(HttpServletRequest request,DataBinder binder) 
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true,10));
	}
	
	
	@Transactional
	@RequestMapping(value="/referencechkquestions.do", method=RequestMethod.GET)
	public String getDistrictQuestions(ModelMap map,HttpServletRequest request)
	{System.out.println("controller referencechkquestions.do ReferenceChkController getDistrictQuestions()");
	  try {
		   HttpSession session = request.getSession(false);
		   UserMaster userMaster=null;
		   DistrictMaster districtMaster = null;
		   HeadQuarterMaster headQuarterMaster = null;
		   Integer districtId =null;
		   Integer headQuarterId =null;
		   if (session == null || session.getAttribute("userMaster") == null) 
		   {
		    return "redirect:index.jsp";
		   }else{
		    userMaster= (UserMaster) session.getAttribute("userMaster");
		    if(userMaster.getEntityType()==3){
		     return "redirect:index.jsp";
		    }
		    districtMaster = userMaster.getDistrictId();
		    headQuarterMaster = userMaster.getHeadQuarterMaster();
		    
		    if(districtMaster!=null)
		    districtId = districtMaster.getDistrictId();
		    
		    if(headQuarterMaster!=null)
		     headQuarterId = headQuarterMaster.getHeadQuarterId();
		   }
		   
		   try{
		    String distId = "";
		    String hQId = "";
		    if(request.getParameter("districtId")!=null && request.getParameter("districtId")!="")
		     distId = request.getParameter("districtId");
		    
		    if(request.getParameter("headQuarterId")!=null && request.getParameter("headQuarterId")!="")
		     hQId = request.getParameter("headQuarterId");
		    
		    if(distId!=null && distId!="" && userMaster.getEntityType()==1)
		     districtId = Integer.parseInt(distId); 
		    
		    if(hQId!=null && hQId!="")		   // change @ Anurag
		     headQuarterId = Integer.parseInt(hQId);
		    
		   }catch(Exception e){
		    e.printStackTrace();
		   }

		   map.addAttribute("userMaster", userMaster);
		   map.addAttribute("districtId", districtId);
		   map.addAttribute("headQuarterId", headQuarterId);
		   map.addAttribute("dt",request.getParameter("dt"));
		   
		   /* 
		    *  Get Job Category
		     /
		   /*List<JobCategoryMaster> jobCategoryMaster = new ArrayList<JobCategoryMaster>();
		   try{
		    jobCategoryMaster  = jobCategoryMasterDAO.findActiveJobCategory();
		   }catch(Exception exception){}
		   map.addAttribute("jobCategoryMaster", jobCategoryMaster);
		   System.out.println("::::::::::::::::: jobCategoryMaster ::::::::::::::::::"+jobCategoryMaster.size());*/
		   
		   
		   
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  
		  return "referencechkquestions";
		  
	}
	
	@RequestMapping(value="/referencecheckspecificquestions.do", method=RequestMethod.GET)
	public String getDistrictQuestion(ModelMap map,HttpServletRequest request)
	{
		System.out.println("controller referencecheckspecificquestions.do AssessmentController getDistrictQuestion()");
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster;
		HeadQuarterMaster headQuarterMaster;  //@anurag
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			districtMaster = userMaster.getDistrictId();
			headQuarterMaster=userMaster.getHeadQuarterMaster();
		}
		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,23,"assessmentsectionquestions.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("roleAccess="+roleAccess);
		map.addAttribute("roleAccess", roleAccess);
		map.addAttribute("heaQuarterMaster",headQuarterMaster);   // @Anurag
		
		String questionId = request.getParameter("questionId");
		DistrictSpecificRefChkQuestions districtSpecificQuestion = null;
		try 
		{
			List<QuestionTypeMaster> questionTypeMasters = questionTypeMasterDAO.getActiveQuestionTypesByShortName(new String[]{"slsel","tf","ml","et","mlsel","mloet","SLD","sloet"});
			JSONObject json = new JSONObject();
			for (QuestionTypeMaster questionTypeMaster : questionTypeMasters) {
				json.put(""+questionTypeMaster.getQuestionTypeId()+"", questionTypeMaster.getQuestionTypeShortName());
			}

			map.addAttribute("questionTypeMasters", questionTypeMasters);
			map.addAttribute("json", json);
			
			if(questionId!=null && questionId.trim().length()>0)
			{
				districtSpecificQuestion = districtSpecificRefChkQuestionsDAO.findById(Integer.parseInt(questionId), false, false);
				List<DistrictSpecificRefChkOptions> optionsForDistrictSpecificQuestions = districtSpecificQuestion.getQuestionOptions();
				JSONArray assessmentQuestionsArray = new JSONArray();
				JSONObject jsonQuestion = new JSONObject();
				for (DistrictSpecificRefChkOptions optionsForDistrictSpecificQuestion : optionsForDistrictSpecificQuestions) {
					System.out.println(":::::::::::::::::::::::::::::::");
					jsonQuestion.put("optionId", optionsForDistrictSpecificQuestion.getOptionId());
					jsonQuestion.put("questionOption", optionsForDistrictSpecificQuestion.getQuestionOption());
					jsonQuestion.put("validOption", optionsForDistrictSpecificQuestion.getValidOption());
					assessmentQuestionsArray.add(jsonQuestion);
				}
				map.addAttribute("assessQues", assessmentQuestionsArray);
				map.addAttribute("districtMaster", districtMaster);
				map.addAttribute("heaQuarterMaster",headQuarterMaster);   // @Anurag
				map.addAttribute("districtSpecificQuestion", districtSpecificQuestion);
				if( request.getParameter("quesSetId")!=null){
					map.addAttribute("questSetId",Integer.parseInt(request.getParameter("quesSetId")));
				}else{
					map.addAttribute("questSetId",0);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "referencecheckspecificquestions";
	}
	
	@RequestMapping(value="/referencechkquestionsSet.do", method=RequestMethod.GET)
	public String I4QuestionsSetGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println(":::::::::::::::::: referencechkquestionsSet.do :::::::::::::::::::::");
		HttpSession session = request.getSession(false);
		UserMaster userSession	=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userSession		=	(UserMaster) session.getAttribute("userMaster");
		}
		
		String districtName = "";
		String districtId = "";
		String hqName="";
		String hqId="";
		
		if(userSession.getEntityType()==2)
		{
			districtId = userSession.getDistrictId().getDistrictId().toString();
			districtName = userSession.getDistrictId().getDistrictName();
		}
		
		if(userSession.getEntityType()==5)
		{
			hqId = userSession.getHeadQuarterMaster().getHeadQuarterId().toString();
			hqName = userSession.getHeadQuarterMaster().getHeadQuarterName();
		}
		
		System.out.println(" userSession.getEntityType() :: "+userSession.getEntityType());
		
		map.addAttribute("entityType",userSession.getEntityType());
		map.addAttribute("districtId",districtId);
		map.addAttribute("districtName",districtName);
		map.addAttribute("hqName", hqName);
		map.addAttribute("hqId", hqId);
		
		return "referencechkquestionsset";
	}
	
	@RequestMapping(value = "/refChkquestionSetQuestion.do", method=RequestMethod.GET)
	public String RefquestionSetQuestionSetGET(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		System.out.println("::::::::::::::: refChkquestionSetQuestion.do :::::::::::::::::");
		HttpSession session = request.getSession(false);
		UserMaster userSession	=null;
		boolean notAccesFlag=false;
		DistrictMaster dMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userSession		=	(UserMaster) session.getAttribute("userMaster");
			if(userSession!=null){
				dMaster=userSession.getDistrictId();
			}
		}
		
		try
		{
			ReferenceQuestionSets referenceQuestionSets = new ReferenceQuestionSets();
			DistrictMaster districtMaster = null;
			HeadQuarterMaster headQuarterMaster = null;
			
			int quesSetId=0;
			
			if(request.getParameter("quesSetId")!=null){
				quesSetId=Integer.parseInt(request.getParameter("quesSetId"));
				referenceQuestionSets = referenceQuestionSetsDAO.findById(quesSetId, false, false);
			}
			
			if(referenceQuestionSets!=null && !userSession.getEntityType().equals(1)){
				if(referenceQuestionSets.getDistrictMaster()!=null && !referenceQuestionSets.getDistrictMaster().getDistrictId().equals(dMaster.getDistrictId())){
					notAccesFlag=true;
				}
			}
			
			if(referenceQuestionSets!=null && !userSession.getEntityType().equals(1) && headQuarterMaster!=null){
				if(referenceQuestionSets.getHeadQuarterId()!=null && !referenceQuestionSets.getHeadQuarterId().equals(headQuarterMaster.getHeadQuarterId())){
					notAccesFlag=true;
				}
			}
			
			
			
			
			if(notAccesFlag)
			{
				PrintWriter out = response.getWriter();
				response.setContentType("text/html"); 
				out.write("<script type='text/javascript'>");
				out.write("alert('You have no access to this e-References Questions.');");
				out.write("window.location.href='referencechkquestionsSet.do';");
				out.write("</script>");
				return null;
			}
			
			Integer districtID = null;
			Integer headQuarterId = null;
			if(request.getParameter("districtId")!=null && request.getParameter("districtId")!="")
			{
				districtID = Integer.parseInt(request.getParameter("districtId"));
				districtMaster = districtMasterDAO.findById(districtID, false, false);
			}
			
			// @ Anurag 
			
			if(request.getParameter("headQuarterId")!=null && request.getParameter("headQuarterId")!="")
			{
				headQuarterId = Integer.parseInt(request.getParameter("headQuarterId"));
				headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
			}
			
			map.addAttribute("headQuarterId", headQuarterId);
			map.addAttribute("quesSetId", quesSetId);
			map.addAttribute("distID", districtID);
			map.addAttribute("quesSetName", referenceQuestionSets.getQuestionSetText());
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		DistrictSpecificRefChkQuestions districtSpecificQuestion = null;
		try 
		{
			List<QuestionTypeMaster> questionTypeMasters = questionTypeMasterDAO.getActiveQuestionTypesByShortName(new String[]{"slsel","tf","ml","et","mlsel","mloet","SLD","sloet"});
			JSONObject json = new JSONObject();
			for (QuestionTypeMaster questionTypeMaster : questionTypeMasters) {
				json.put(""+questionTypeMaster.getQuestionTypeId()+"", questionTypeMaster.getQuestionTypeShortName());
			}

			map.addAttribute("questionTypeMasters", questionTypeMasters);
			map.addAttribute("json", json);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "refchkquestionssetques";
	}
	
}

