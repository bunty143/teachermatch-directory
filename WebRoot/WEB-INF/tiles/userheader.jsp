<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>
<div class="container-fluid p0" id='testheader'>
	<div class="header-bg">
		<div class="container header-backround">

			<div class="span6 logo">
				<a href="http://www.teachermatch.org"  target="_blank">
				 <% if(request.getServerName().contains("myedquest.org")){  %>
						 <img src="images/QuestLogoTM.png" alt="" >
						 <%} if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>
						 	<img src="images/logowithouttagline.png" alt="" class="top14">
						 <%
						 }else{ %>
						 <img src="images/Logo with Beta300.png" alt="" >
						 <%} %>
				
				</a>
			</div>
			<div class="span8 pull-right">
				<div class="span2 pull-right pa">
	    			<div class="btn-group pull-right">
		    			<a class="btn dropdown-toggle John-Oliveri" data-toggle="dropdown" href="#">
		   					${teacherDetail.firstName}&nbsp;${teacherDetail.lastName}
		   					<span class="carethead"></span>
		    			</a>
		    			<ul class="dropdown-menu">    
							<li><a href="userpreference.do"><spring:message code="lnkJobPreferences"/> </a></li>
							<li><a href="settings.do"><spring:message code="lblSett"/></a></li>
							<li class="divider">&nbsp;</li>
							<li><a href="logout.do"><spring:message code="lnkSignout"/></a></li>                
		    			</ul>		    	
			    	</div>
				</div>
				<div class="span8 pull-right navbarp">
					<div class="navbar navbar-static" id="navbar-example">
						<div class="navwidth">
							<div style="width: auto;" class="container">
								<ul role="navigation" class="nav">
									
									<li class="dropdown">
									<a  class="dropdown-toggle active" role="button" href="userdashboard.do"><spring:message code="lblDashboard"/></b></a>
									</li>
									
									<li class="dropdown">
										<a  class="dropdown-toggle" role="button" href="portfolio.do" ><spring:message code="lblPortfolio1"/> </b></a>
									</li>
									
									<li class="dropdown">
									<a data-toggle="dropdown" class="dropdown-toggle" role="button" href="#" id="drop1"><spring:message code="lblInventories"/> <b class="caret"></b></a>
									<ul aria-labelledby="drop1" role="menu" class="dropdown-menu">
									<li><a href="#" tabindex="-1"><spring:message code="lblB"/></a></li>
									<li><a href="#anotherAction" tabindex="-1"><spring:message code="lblJobSpecificRequirements"/>
</a></li>
									</ul>
									</li>
									
									<li class="dropdown" style="background:none;">
									<a data-toggle="dropdown" class="dropdown-toggle job" role="button" id="drop2" href="#"><spring:message code="lblJo"/></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="height:68px;"></div>
