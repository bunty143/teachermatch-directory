<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<%@ page import="tm.utility.Utility" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><tiles:insertAttribute name="title"/></title>
		<script type="text/javascript">
		var resourceJSON = <%=Utility.getJSLocaleJSON()%>;
		</script>
		<link href="css/font-awesome.min.css?ver=${resourceMap['css/font-awesome.min.css']}"  rel="stylesheet" type="text/css" >
		<link href="css/font-awesome-ie7.css?ver=${resourceMap['css/font-awesome-ie7.css']}"  rel="stylesheet" type="text/css">
		<link href="css/font-awesome-ie7.min.css?ver=${resourceMap['css/font-awesome-ie7.min.css']}"  rel="stylesheet" type="text/css">
		<link href="css/font-awesome.css?ver=${resourceMap['css/font-awesome.css']}"  rel="stylesheet" type="text/css">
		<link href="css/tmcommon.css?ver=${resourceMap['css/tmcommon.css']}"  rel="stylesheet" type="text/css">
		<script type="text/javascript" src="js/tmhome.js?ver=${resourceMap['js/tmhome.js']}">
		</script>	

		<link href="css/bootstrap3.css?ver=${resourceMap['css/bootstrap3.css']}" rel="stylesheet" type="text/css">
		<link href="css/style3.css?ver=${resourceMap['css/style3.css']}" rel="stylesheet" type="text/css">	
		
		<script src="js/jquery.js?var=${resourceMap['js/jquery.js']}"></script>
		<script src="js/bootstrap.js?var=${resourceMap['js/bootstrap.js']}"></script>
		
		<script src="js/tmcommon.js?var=${resourceMap['js/tmcommon.js']}" type="text/javascript"></script>
		<script src="js/drg.js" type="text/javascript" language="javascript"></script>
		<script src="twitter-bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		
	</head>
	<body>
	
	<tiles:insertAttribute name="header" />
<!-- Start feedback and support -->
<jsp:include page="feedbackandsupport3BeforeLogin.jsp" flush="true" /> 
<!-- End feedback and support -->
	

	<div class="container" style="min-height: 430px;">
		<tiles:insertAttribute name="body" />
	</div>
	
		<c:set var="serverIpAddress" value="${initParam['serverIPAddress']}"/>
		<c:if test="${serverIpAddress =='166.78.100.150'}">
   			<div style="height: 1px; margin-top:23px; margin-bottom: -23px; z-index: 99999">
   		 		<img src="images/dot.gif" style="background-repeat: repeat-y;height: 1px; width: 100%;">
			</div>
		</c:if>	
	<tiles:insertAttribute name="footer" />	
	</body>
</html>
