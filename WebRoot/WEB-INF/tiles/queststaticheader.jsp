<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<link href="css/font-awesome.min.css?ver=${resourceMap['css/font-awesome.min.css']}"  rel="stylesheet" type="text/css">
<link href="css/font-awesome-ie7.css?ver=${resourceMap['css/font-awesome-ie7.css']}"  rel="stylesheet" type="text/css">
<link href="css/font-awesome-ie7.min.css?ver=${resourceMap['css/font-awesome-ie7.min.css']}"  rel="stylesheet" type="text/css">
<link href="css/font-awesome.css?ver=${resourceMap['css/font-awesome.css']}"  rel="stylesheet" type="text/css">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56937648-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/290901.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->

<div class="container">
	<div style="float: left;" class="top15">
		<div>
					 <% if(request.getServerName().contains("myedquest.org")){  %>
						 <a href="http://myedquest.org" target="_blank"><img src="images/QuestLogoTM.png" alt="" ></a>
						 <%}else{ %>
						 <a href="http://www.teachermatch.org" target="_blank"><img src="images/Logo with Beta300.png" alt="" ></a>
						 <%} %>
				 	</div>
	</div>	
	
	<div style="clear: both;"></div>
</div>
<div style="height:60px;" class="top10" >
<div class="custom-menu">
			<div class="container">
				<!-- Static navbar -->
				<nav class="navbar navbar-custom" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse" data-target="#navbar"
							aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">						
													
							<c:choose>
							<c:when test="${not empty educationjob}">
							<li class="dropdown open"><a href="educationjob.do"><b><spring:message code="lblHome"/></b></a></li>
							</c:when>
							<c:otherwise>
							<li class="dropdown"><a href="educationjob.do"><b><spring:message code="lblHome"/></b></a></li>
							</c:otherwise>
							</c:choose>				
							
							<c:choose>
							<c:when test="${not empty aboutus}">
							<li class="dropdown open"><a href="aboutus.do"><b><spring:message code="lnkAboutUs"/></b></a></li>
							</c:when>
							<c:otherwise>
							<li class="dropdown"><a href="aboutus.do"><b><spring:message code="lnkAboutUs"/></b></a></li>
							</c:otherwise>
							</c:choose>							
							<c:choose>
							<c:when test="${not empty meetyourmentor}">
							<li class="dropdown open" ><a href="meetyourmentor.do" style="color:#bcbec0;"><b><spring:message code="lnkResources"/></b></a></li>
							</c:when>
							<c:otherwise>
							<li class="dropdown"><a href="meetyourmentor.do"><b><spring:message code="lnkResources"/></b></a></li>
							</c:otherwise>
							</c:choose>
							<li class="dropdown"><a href="http://www.teachermatch.org/blog"><b><spring:message code="lnkBlog"/></b></a></li>
							
						</ul>
					</div>
					<div>					
					<!--/.nav-collapse -->
				</div>
				<!--/.container-fluid -->
				</nav>
			</div>
			<!-- /container -->
		</div>	
</div>
</div>

