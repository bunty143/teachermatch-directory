<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>
<header>
<div class="container-fluid p0" id='testheader' style='border:0px solid red;'>
<div class="header-bg">
     <div class="container">
     <div class="header-backround">       
          <div class="navbar navbar-default" role="navigation">         
              <div class="navbar-header">
                 <a href="http://www.teachermatch.org" target="_blank">
					 <% if(request.getServerName().contains("myedquest.org")){  %>
						 <img src="images/QuestLogoTM.png" alt="" >
						 
						 <!-- Google Analytics code:  -->
							 <script>
							  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
							  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
							  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
							  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');							
							  ga('create', 'UA-56937648-1', 'auto');
							  ga('send', 'pageview');							
							</script>
						 
							<!-- Start of Async HubSpot Analytics Code -->
							  <script type="text/javascript">
							    (function(d,s,i,r) {
							      if (d.getElementById(i)){return;}
							      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
							      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/290901.js';
							      e.parentNode.insertBefore(n, e);
							    })(document,"script","hs-analytics",300000);
							  </script>
						 
						 
						 <%}else{ %>
						 <% if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){ 
						 		if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){%>
									<img src="images/logoimage.png" alt="" style="width: 133px;height: 50px;">	
								<%}else{ %>
						 			<img src="images/logowithouttagline.png" alt="" class="top14">
						 		<%}
						 	}
						 	else if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){%>
									<img src="images/logoimage.png" alt="" style="width: 133px;height: 50px;">	
								<%}else{ %>
									<img src="images/Logo with Beta300.png" alt=""/>
								<%}%>
						 <script type="text/javascript">
							(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
							(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
							m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
							ga('create','UA-40462543-1','teachermatch.org');
							ga('send','pageview');
						</script>
						 <%} %>
				 </a>								
	             <a class="btn btn-navbar btn-default navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	              <span class="nb_left pull-left"> <span class="fa fa-reorder"></span> </span> 
	              <span class="nb_right pull-right"><spring:message code="lnkmenu"/></span> 
	             </a>
               </div>  			
          </div>        
         </div>     
     </div></div></div>
</header>
<div style="height: 83px;"></div>

