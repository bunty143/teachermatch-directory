<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<%@ page import="tm.utility.Utility" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<script type="text/javascript">
		var resourceJSON = <%=Utility.getJSLocaleJSON()%>;
		</script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><tiles:insertAttribute name="title"/></title>	   
	    <link href="css/bootstrap3.css?ver=${resourceMap['css/bootstrap3.css']}" rel="stylesheet" type="text/css">
		<link href="css/style3.css?ver=${resourceMap['css/style3.css']}" rel="stylesheet" type="text/css">	
		<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">		
		<!--<link href="css/style_new.css?ver=${resourceMap['css/style_new.css']}" rel="stylesheet" type="text/css">
		<link href="css/bootstrap.css?ver=${resourceMap['css/bootstrap.css']}" rel="stylesheet" type="text/css">				
		--><script src="js/jquery.js?var=${resourceMap['js/jquery.js']}"></script>	  								
	    <script src="twitter-bootstrap/js/bootstrap.min.js?var=${resourceMap['twitter-bootstrap/js/bootstrap.min.js']}" type="text/javascript"></script> 	      			    
	    <script type="text/javascript" src="dwr/engine.js"></script>
		<script type='text/javascript' src='dwr/util.js'></script>				
	</head>		
	<body>
		<tiles:insertAttribute name="header" />	
		<tiles:insertAttribute name="body" />		
		<tiles:insertAttribute name="footer" />
		<jsp:include page="../tiles/feedbackandsupport3AfterLogin.jsp" />	
	</body>
</html>
