<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page import="tm.utility.Utility" %>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><tiles:insertAttribute name="title"/></title>
	    <link href="css/bootstrap.css?ver=${resourceMap['css/bootstrap.css']}" rel="stylesheet" type="text/css">
		<link href="css/style.css?ver=${resourceMap['css/style.css']}" rel="stylesheet" type="text/css">
		
		<link href="css/font-awesome.min.css?ver=${resourceMap['css/font-awesome.min.css']}" rel="stylesheet" type="text/css">
		<link href="css/font-awesome-ie7.css?ver=${resourceMap['css/font-awesome-ie7.css']}" rel="stylesheet" type="text/css">
		<link href="css/font-awesome-ie7.min.css?ver=${resourceMap['font-awesome-ie7.min.css']}" rel="stylesheet" type="text/css">
		<link href="css/font-awesome.css?ver=${resourceMap['css/font-awesome.css']}" rel="stylesheet" type="text/css">
		
		<link href="css/tmcommon.css?ver=${resourceMap['css/tmcommon.css']}"  rel="stylesheet" type="text/css">
		<script type="text/javascript" src="js/tmcommon.js?var=${resourceMap['js/tmcommon.js']}"></script>		
	
		
		<script type="text/javascript" src="<tiles:insertAttribute name="dynamicJS"/>">
		</script>		
		
		<script src="js/jquery.js?var=${resourceMap['js/jquery.js']}"></script>		
		<script  type="text/javascript" language="javascript" src="js/drg.js?var=${resourceMap['js/drg.js']}"></script>		
		<script src="js/bootstrap.js?var=${resourceMap['js/bootstrap.js']}"></script>
		<!--[if IE 7]>
		<style type="text/css">
		.span8 {
		    width: 422px!important;
		.socialicon-Box{ width:85px; float:right!important; padding:0px; margin:0 0 0 200px!important;}  
		}
		</style>
		<![endif]-->	
		<script type="text/javascript">
		var resourceJSON = <%=Utility.getJSLocaleJSON()%>;
		</script>
	</head>
		
	<body onload="setFocus('<tiles:insertAttribute name="focusId" />') <tiles:insertAttribute name="bodyLoad" /> ">
	
	<tiles:insertAttribute name="header" />
<!-- Start feedback and support -->
	<jsp:include page="feedbackandsupport3AfterLogin.jsp" flush="true" /> 
<!-- End feedback and support --> 
	<div class="container" style="min-height: 400px;">
		<tiles:insertAttribute name="body" />
	</div>

		<c:set var="serverIpAddress" value="${initParam['serverIPAddress']}"/>
		<c:if test="${serverIpAddress =='166.78.100.150'}">
   			<div style="height: 1px; margin-top:11px; margin-bottom: -11px; z-index: 99999">
   		 		<img src="images/dot.gif" style="background-repeat: repeat-y;height: 1px; width: 100%;">
			</div>
		</c:if>	
		
<tiles:insertAttribute name="footer" />
	
	</body>
</html>
