<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style type="text/css">
@media screen and (max-width: 1023px) {
	#block_navigation {
		width: 350px;
		margin: 10px auto;
	}

}
#block_navigation {
	width: 77px;
	position: relative;
	background: #f7f7f7;
	height: 100%;
	color:#00658E;
	margin-bottom:100%;
<!--	margin: 40px 40px;-->
<!--	-webkit-box-shadow: 2px 2px 3px #999;-->
<!--	-moz-box-shadow: 2px 2px 3px #999;-->
<!--	box-shadow: 2px 2px 3px #999;-->
<!--	border-radius: 11px;-->
        overflow: hidden;
        box-shadow: 2px 0 18px rgba(0, 0, 0, 0.26);
}
#block_navigation  ul {
	width: 100%;
	margin: 0;
	padding: 0;
}
#block_navigation  ul li {
	margin: 0;
	padding: 0;
	display: block;
	position: relative;
	list-style: none;
	width: 100%;

}
#block_navigation ul li a {
	width: 100%;
	font-weight: normal;
	text-decoration: none;
	display: block;
	min-height: 53px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	text-align: center;
	border-bottom: 1px solid rgba(0, 0, 0, 0.05);
    transition: background 0.1s ease-in-out;
}
#block_navigation ul li:hover {
	background: #00658E;
}
#block_navigation ul li ul {
	z-index: 99999;
	opacity: 0;
	visibility: hidden;
	position: absolute;
	top: 0;
	left: 100%;
	display: block;
	border-radius: none;
	height: 100%;
}
#block_navigation ul li ul li {
	position: relative;
	display: block;
	float: none;
	background: #f7f7f7;
	color:blue;
	-webkit-transition: background 0.2s linear;
	-moz-transition: background 0.2s linear;
	-ms-transition: background 0.2s linear;
	-o-transition: background 0.2s linear;
	transition: background 0.2s linear;
}
#block_navigation ul ul li a {
	color: #007AB4;
	line-height: 42px;
	padding-left: 25px;
	width: 100%;
	
}
#block_navigation ul ul li:hover > a {
	color: white;
	line-height: 42px;
	padding-left: 30px;
	width: 100%;
}
#block_navigation ul li:hover>ul {
	z-index: 99999;
	opacity: 1;
	visibility: visible;
	color: white;
}
.img_center{
margin-top: 15px;
/*margin-left: 22px;*/
color:red;
}
</style>
<%
String menuid="";
String submenuid="";
try
{
menuid=session.getAttribute("menuId").toString();
submenuid=session.getAttribute("subMenuId").toString();
}
catch(NullPointerException e)
{
//e.printStackTrace();
}

 %>


<body>
	<div id="block_navigation">
  <input type="hidden" id="menuid" value="<%=menuid%>" />
  <input type="hidden" id="submenuid" value="<%=submenuid%>" />
		<ul class="navigation" style="display: block;" >
			<li class="menu1" id="menu1">
				<a href="#" title="Introduction" id="txt1" style="font-size:smaller;"><img  id="img1" class="img_center" src="images/Introductiondspq1.png" /><br/>Introduction </a>
				<ul style="width: 218px;z-index: 999;">
					<li class="menu1" id="menu1_1">
						<a href="home.do" title="Welcome to Configuration Wizard">Welcome to Wizard</a>
					</li>
				</ul>
			</li>
			<li class="menu2" id="menu2">
				<a href="#" title="District" id="txt2" style="font-size:smaller;"><img  id="img2" class="img_center" src="images/Experiencedpsq1.png" /><br/>District </a>
				<ul style="width: 218px;z-index: 999;">
					<li class="menu2"  id="menu2_1">
						<a href="managedistrictdspq.do" title="Configuration">Configuration</a>
					</li>
					<li class="menu2" id="menu2_2">
						<a href="managedocumentsdspq.do" title="Documents">Documents </a>
					</li>
					<li class="menu2" id="menu2_3">
						<a href="managetagsdspq.do" title="Tags">Tags </a>
					</li>
					<li class="menu2" id="menu2_4">
						<a href="geozonesetupdspq.do" title="Zones">Zones </a>
					</li>
				</ul>
			</li>
			<li class="menu3" id="menu3">
				<a href="#" title="Recruiting" id="txt3" style="font-size:smaller;"><img  id="img3" class="img_center" src="images/Recruitingdspq1.png" /><br/>Recruiting </a>
				<ul style="width: 218px;z-index: 999;">
					<li class="menu3" id="menu3_1">
						<a href="jobcategorydspq.do" title="Job Categories">Job Categories </a>
					</li>
					<li class="menu3" id="menu3_2">
						<a href="manageportfolio.do" title="Portfolios">Portfolios </a>
					</li>
					<li class="menu3" id="menu3_3">
						<a href="districtspecificquestionssetdspq.do" title="Pre-Screen">Pre-Screen </a>
					</li>
					<li class="menu3" id="menu3_4">
						<a href="districtquestionsdspq.do" title="Custom Questions">Custom Questions </a>
					</li>
					<li class="menu3" id="menu3_5">
						<a href="managestatusdspq.do" title="Hiring Workflow">Hiring Workflow </a>
					</li>
					<li class="menu3" id="menu3_6">
						<a href="onboardingworkflow.do" title="On-boarding Workflow">On-boarding Workflow </a>
					</li>

				</ul>
			</li>

			<li class="menu4" id="menu4">
				<a href="#" title="Communications" id="txt4" style="font-size:smaller;"><img  id="img4" class="img_center" src="images/DS_megaphone1.png" /><br/>Communications</a>

				<ul style="width: 218px;z-index: 999;">
					<li class="menu4" id="menu4_1">
						<a href="communicationsdspq.do" title="Emails">Emails </a>
					</li>
					<li class="menu4" id="menu4_2">
						<a href="notifications.do" title="Notifications">Notifications </a>
					</li>

				</ul>
			</li>


			<li class="menu5" id="menu5">
				<a href="#" title="Access & Visibility" id="txt5" style="font-size:smaller;"><img  id="img5" class="img_center" src="images/DS_glasses1.png"/><br/>Access & Visibility	</a>


				<ul style="width: 218px;z-index: 999;">
					<li class="menu5" id="menu5_1">
						<a href="manageuserdspq.do" title="Roles">Roles </a>
					</li>
					<li class="menu5" id="menu5_2">
						<a href="visibility.do" title="Visibility">Visibility </a>
					</li>

				</ul>
			</li>

			<li class="menu6" id="menu6">
				<a href="#" title="Insights" id="txt6" style="font-size:smaller;"><img  id="img6" class="img_center" src="images/Insightsdspq1.png" /><br/>Insights</a>
				<ul style="width: 218px;">
					<li class="menu6" id="menu6_1">
						<a href="pnrdashboarddspq.do" title="Dashboard">Dashboard </a>
					</li>
					<li class="menu6" id="menu6_2">
						<a href="actionfeeds.do" title="Action Feeds">Action Feeds </a>
					</li>

				</ul>
			</li>

		</ul>
	</div>
<script>
$(document).ready(function(){

          $('#img2').attr('src', 'images/Experiencedpsq1.png');
          $("#txt1").css('color','#007AB4');
			$('#img3').attr('src', 'images/Recruitingdspq1.png');
			 $("#txt3").css('color','#007AB4');
			$('#img4').attr('src', 'images/DS_megaphone1.png');
			 $("#txt4").css('color','#007AB4');
			$('#img5').attr('src', 'images/DS_glasses1.png');
			 $("#txt5").css('color','#007AB4');
			$('#img6').attr('src', 'images/Insightsdspq1.png');
			 $("#txt6").css('color','#007AB4');
			$('#img1').attr('src', 'images/Introductiondspq1.png');
			 $("#txt1").css('color','#007AB4');
			
//**************** Menu color only *************************	
	 var menuid="";
     var submenuid="";
	 menuid=$('#menuid').val();
	 submenuid=$('#submenuid').val();
	 
   if((menuid!="" || menuid!=null) && (submenuid!="" || submenuid!=null))
    {
    selectLastImages();
	$(menuid).css('background', '#00658E');
	$(submenuid).css('background', '#00658E');
	$(submenuid+' a').css('color', '#ffffff');
	}
	
//**************** End **************************************
         if(menuid!='#menu1')
		  {
	      $(".menu1").mouseover(function() {
			$('#img2').attr('src', 'images/Experiencedpsq1.png');
			 $("#txt2").css('color','#007AB4');
			$('#img3').attr('src', 'images/Recruitingdspq1.png');
			 $("#txt3").css('color','#007AB4');
			$('#img4').attr('src', 'images/DS_megaphone1.png');
			 $("#txt4").css('color','#007AB4');
			$('#img5').attr('src', 'images/DS_glasses1.png');
			 $("#txt5").css('color','#007AB4');
			$('#img6').attr('src', 'images/Insightsdspq1.png');
			 $("#txt6").css('color','#007AB4');
			$('#img1').attr('src', 'images/Introductiondspq.png');
			 $("#txt1").css('color','#ffffff');
			selectLastImages();
		});
		$(".menu1").mouseout(function() {
			$('#img1').attr('src', 'images/Introductiondspq1.png');
			 $("#txt1").css('color','#007AB4');
		});
       }
       if(menuid!='#menu2')
	   {
		$(".menu2").mouseover(function() {
		    
			$('#img1').attr('src', 'images/Introductiondspq1.png');
			$("#txt1").css('color','#007AB4');
			$('#img3').attr('src', 'images/Recruitingdspq1.png');
			$("#txt3").css('color','#007AB4');
			$('#img4').attr('src', 'images/DS_megaphone1.png');
			$("#txt4").css('color','#007AB4');
			$('#img5').attr('src', 'images/DS_glasses1.png');
			$("#txt5").css('color','#007AB4');
			$('#img6').attr('src', 'images/Insightsdspq1.png');
			$("#txt6").css('color','#007AB4');
			$('#img2').attr('src', 'images/Experiencedpsq.png');
			$("#txt2").css('color','#ffffff');
			selectLastImages();
		});
		$(".menu2").mouseout(function() {
			$('#img2').attr('src', 'images/Experiencedpsq1.png');
			$("#txt2").css('color','#007AB4');
		});
       }
       if(menuid!='#menu3')
	   {
		$(".menu3").mouseover(function() {
			$('#img1').attr('src', 'images/Introductiondspq1.png');
			$("#txt1").css('color','#007AB4');
			$('#img2').attr('src', 'images/Experiencedpsq1.png');
			$("#txt2").css('color','#007AB4');
			$('#img4').attr('src', 'images/DS_megaphone1.png');
			$("#txt4").css('color','#007AB4');
			$('#img5').attr('src', 'images/DS_glasses1.png');
			$("#txt5").css('color','#007AB4');
			$('#img6').attr('src', 'images/Insightsdspq1.png');
			$("#txt6").css('color','#007AB4');
			$('#img3').attr('src', 'images/Recruitingdspq.png');
			$("#txt3").css('color','#ffffff');
			selectLastImages();
		});
		$(".menu3").mouseout(function() {
			$('#img3').attr('src', 'images/Recruitingdspq1.png');
			$("#txt3").css('color','#007AB4');
		});
       }
       if(menuid!='#menu4')
	   {
		$(".menu4").mouseover(function() {
			$('#img1').attr('src', 'images/Introductiondspq1.png');
			$("#txt1").css('color','#007AB4');
			$('#img2').attr('src', 'images/Experiencedpsq1.png');
			$("#txt2").css('color','#007AB4');
			$('#img3').attr('src', 'images/Recruitingdspq1.png');
			$("#txt3").css('color','#007AB4');
			$('#img5').attr('src', 'images/DS_glasses1.png');
			$("#txt5").css('color','#007AB4');
			$('#img6').attr('src', 'images/Insightsdspq1.png');
			$("#txt6").css('color','#007AB4');
			$('#img4').attr('src', 'images/DS_megaphone.png');
			$("#txt4").css('color','#ffffff');
			selectLastImages();
		});
		$(".menu4").mouseout(function() {
			$('#img4').attr('src', 'images/DS_megaphone1.png');
			$("#txt4").css('color','#007AB4');
		});
       }
       if(menuid!='#menu5')
	   {
		$(".menu5").mouseover(function() {
			$('#img1').attr('src', 'images/Introductiondspq1.png');
			$("#txt1").css('color','#007AB4');
			$('#img2').attr('src', 'images/Experiencedpsq1.png');
			$("#txt2").css('color','#007AB4');
			$('#img3').attr('src', 'images/Recruitingdspq1.png');
			$("#txt3").css('color','#007AB4');
			$('#img4').attr('src', 'images/DS_megaphone1.png');
			$("#txt4").css('color','#007AB4');
			$('#img6').attr('src', 'images/Insightsdspq1.png');
			$("#txt6").css('color','#007AB4');
			$('#img5').attr('src', 'images/DS_glasses.png');
			$("#txt5").css('color','#ffffff');
			selectLastImages();
		});
		$(".menu5").mouseout(function() {
			$('#img5').attr('src', 'images/DS_glasses1.png');
			$("#txt5").css('color','#007AB4');
		});
       }
       if(menuid!='#menu6')
	   {
		$(".menu6").mouseover(function() {
			$('#img1').attr('src', 'images/Introductiondspq1.png');
			$("#txt1").css('color','#007AB4');
			$('#img2').attr('src', 'images/Experiencedpsq1.png');
			$("#txt2").css('color','#007AB4');
			$('#img3').attr('src', 'images/Recruitingdspq1.png');
			$("#txt3").css('color','#007AB4');
			$('#img4').attr('src', 'images/DS_megaphone1.png');
			$("#txt4").css('color','#007AB4');
			$('#img5').attr('src', 'images/DS_glasses1.png');
			$("#txt5").css('color','#007AB4');
			$('#img6').attr('src', 'images/Insightsdspq.png');
			$("#txt6").css('color','#ffffff');
			selectLastImages();
		});
		$(".menu6").mouseout(function() {
			$('#img6').attr('src', 'images/Insightsdspq1.png');
			$("#txt6").css('color','#007AB4');
		});
      }
	});
	function selectLastImages()
	{
		menuid=$('#menuid').val();
		if((menuid!="" || menuid!=null))
	    {
		    if(menuid=='#menu1')
		    {
		    $('#img1').attr('src', 'images/Introductiondspq.png');
		    $("#txt1").css('color','#ffffff');
		    }
		    if(menuid=='#menu2')
		    {
		    $('#img2').attr('src', 'images/Experiencedpsq.png');
		    $("#txt2").css('color','#ffffff');
		    }
		    if(menuid=='#menu3')
		    {
		    $('#img3').attr('src', 'images/Recruitingdspq.png');
		    $("#txt3").css('color','#ffffff');
		    }
		    if(menuid=='#menu4')
		    {
		    $('#img4').attr('src', 'images/DS_megaphone.png');
		    $("#txt4").css('color','#ffffff');
		    }
		    if(menuid=='#menu5')
		    {
		    $('#img5').attr('src', 'images/DS_glasses.png');
		    $("#txt5").css('color','#ffffff');
		    }
		    if(menuid=='#menu6')
		    {
		    $('#img6').attr('src', 'images/Insightsdspq.png');
		    $("#txt6").css('color','#ffffff');
		    }
		}
	}
    
</script>
</body>
