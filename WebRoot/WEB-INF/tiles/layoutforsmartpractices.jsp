<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><tiles:insertAttribute name="title"/></title>
		<link href="css/bootstrap3.css?ver=${resourceMap['css/bootstrap3.css']}" rel="stylesheet" type="text/css">		
		<link href="css/style3.css?ver=${resourceMap['css/style3.css']}" rel="stylesheet" type="text/css">		
		
		<link href="css/font-awesome.min.css?ver=${resourceMap['css/font-awesome.min.css']}"  rel="stylesheet" type="text/css" >
		<link href="css/font-awesome-ie7.css?ver=${resourceMap['css/font-awesome-ie7.css']}"  rel="stylesheet" type="text/css">
		<link href="css/font-awesome-ie7.min.css?ver=${resourceMap['css/font-awesome-ie7.min.css']}"  rel="stylesheet" type="text/css">
		<link href="css/font-awesome.css?ver=${resourceMap['css/font-awesome.css']}"  rel="stylesheet" type="text/css">
		
		<link href="css/tmcommon.css?ver=${resourceMap['css/tmcommon.css']}"  rel="stylesheet" type="text/css">		
		<script type="text/javascript" src="<tiles:insertAttribute name="dynamicJS"/>">
		</script>
		
		<script src="js/jquery.js?var=${resourceMap['js/jquery.js']}"></script>								
	    <script src="twitter-bootstrap/js/bootstrap.min.js?var=${resourceMap['twitter-bootstrap/js/bootstrap.min.js']}" type="text/javascript"></script> 	
		<!--[if IE 7]>
		<style type="text/css">
		.span8 {
		    width: 422px!important;
		.socialicon-Box{ width:85px; float:right!important; padding:0px; margin:0 0 0 200px!important;}  
		}
		</style>
		<![endif]-->	
	</head>
		
	<body onload="setFocus('<tiles:insertAttribute name="focusId" />') <tiles:insertAttribute name="bodyLoad" /> ">
		
	<c:if test="${redirectTopref and lstGeographyMasters eq null}">
		<c:redirect url="userpreference.do">
			 <c:param name="msg" value="#6@5a1p24m1gg"/>
		</c:redirect>
	</c:if>
	
	<c:choose>
		<c:when test="${epiStandalone}">		
			<tiles:insertAttribute name="portalHeader"/>
		</c:when>
		<c:otherwise>
			<tiles:insertAttribute name="header" />
		</c:otherwise>
	</c:choose>
	
<!-- Start feedback and support -->
<jsp:include page="feedbackandsupport3AfterLogin.jsp" flush="true" />	 
<!-- End feedback and support --> 

	<div class="container" style="min-height: 400px;">
		<tiles:insertAttribute name="body" />
	</div>

<tiles:insertAttribute name="footer" />
	
	</body>
</html>
