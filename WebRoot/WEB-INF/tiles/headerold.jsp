<%@ page import="tm.utility.Utility" %>
<div class="container-fluid p0" id='testheader'>
	<div class="header-bg">
		<div class="container header-backround">

			<div class="span6 logo">
				<a href="http://www.teachermatch.org"  target="_blank">
				<% if(request.getServerName().contains("myedquest.org")){  %>
						 <img src="images/QuestLogoTM.png" alt="" >
						 <%} if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){
								if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){ %>
							 		<img src="images/logoimage.png" alt="" style="width: 133px;height: 50px;">	
								<%}
								else{ %>
						 			<img src="images/logowithouttagline.png" alt="" class="top14">
						 		<%}
						 	}
						 	else{
						 	 	if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){%>
									<img src="images/logoimage.png" alt="" style="width: 133px;height: 50px;">	
								<%}
								else{ %>
									<img src="images/Logo with Beta300.png" alt="" >	
								<%}
							}%>
				</a>
			</div>
			
		</div>
	</div>
</div>
<div style="height:68px;"></div>
<script type="text/javascript">
							(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
							(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
							m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
							ga('create','UA-40462543-1','teachermatch.org');
							ga('send','pageview');
						</script>