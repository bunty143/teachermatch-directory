<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="tm.utility.Utility" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><tiles:insertAttribute name="title"/></title>			
		<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

		<link href="css/cGSpecificCss.css?ver=${resourceMap['css/cGSpecificCss.css']}"  rel="stylesheet" type="text/css">
		<link type="text/css" rel="stylesheet" href="css/jquery-te-1.3.2.2.css" charset="utf-8">
		 
		 <script type="text/javascript" src="js/report/cgSpecific.js?ver=${resourceMap['js/report/cgSpecific.js']}"></script>
		<script type="text/javascript" src="js/compress/c_jquery-1.4.1.js"></script>
		<script type="text/javascript" src="js/jquery.fixheadertable.js"></script> 
		<script type="text/javascript" src="dwr/engine.js"></script>
		<script type='text/javascript' src='dwr/util.js'></script>
					
         <script type="text/javascript" src="<tiles:insertAttribute name="dynamicJS"/>"></script>
        
        <script type="text/javascript">
		var resourceJSON = <%=Utility.getJSLocaleJSON()%>;
		</script>			    
	</head>		
	<body class="boxed" onload="setFocus('<tiles:insertAttribute name="focusId" />') <tiles:insertAttribute name="bodyLoad" /> ">
	
	<tiles:insertAttribute name="header" />
	<!-- Text Area Editor -->
<!-- Start feedback and support -->
<%-- <jsp:include page="feedbackandsupport3AfterLogin.jsp" flush="true" /> --%>
<!-- End feedback and support --> 
	<div class="container" style="min-height: 400px;">
		<tiles:insertAttribute name="body" />
	</div>
	
		<c:set var="serverIpAddress" value="${initParam['serverIPAddress']}"/>
		<c:if test="${serverIpAddress =='166.78.100.150'}">
   			<div style="height: 1px; margin-top:23px; margin-bottom: -23px; z-index: 99999">
   		 		<img src="images/dot.gif" style="background-repeat: repeat-y;height: 1px; width: 100%;">
			</div>
		</c:if>
		
<tiles:insertAttribute name="footer" />	
	</body>
</html>
