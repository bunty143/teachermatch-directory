<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>

<div class="container-fluid p0" id='testheader'>
	<div class="header-bg">
		<div class="container header-backround">
			<div class="span6 logo">
				<a href="http://www.teachermatch.org"  target="_blank">
				 <% if(request.getServerName().contains("myedquest.org")){  %>
						 <img src="images/QuestLogoTM.png" alt="" >
						 <%} if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){
						 		if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){%>
									<img src="images/logoimage.png" alt="" style="width: 133px;height: 50px;">	
								<%} else{ %>
						 			<img src="images/logowithouttagline.png" alt="" class="top14">
						 			<%}
						 	}
						 	else{ 
						 		if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){%>
									<img src="images/logoimage.png" alt="" style="width: 133px;height: 50px;">	
								<%}
								else{ %>
									<img src="images/Logo with Beta300.png" alt="" >	
								<%}
							}%>
				</a>
			</div>
			<div class="span8 pull-right">

				  <div class="pull-right">
					<div class="btn-group">					
					<a class="btn dropdown-toggle John-Oliveri" data-toggle="dropdown"
						href="#">${teacherDetail.firstName}&nbsp;${teacherDetail.lastName}<span class="carethead"></span> </a>
						<ul class="dropdown-menu pull-right">
							<li>
								<a href="settings.do"><spring:message code="lblSett"/></a>
							</li>
							<li class="divider">
								&nbsp;
							</li>
							<li>
								<a href="logout.do"><spring:message code="lnkSignout"/>
</a>
							</li>
						</ul>					
					</div>
				  </div>				  
			</div>
		</div>
	</div>
</div>
<div style="height:68px;"></div>
