
<%@page import="tm.bean.user.UserMaster"%>
<%@page import="tm.bean.TeacherDetail"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="tm.dao.JobForTeacherDAO"%><%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="tm.utility.Utility" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!-- Start feedback and support -->
	<script type="text/javascript" src="js/jquery-te-1.3.2.2.min.js" charset="utf-8"></script>
	<link type="text/css" rel="stylesheet" href="css/jquery-te-1.3.2.2.css" charset="utf-8">
	<!-- Editor 2 -->
	<link href="css/rating_simple.css" rel="stylesheet" type="text/css">
	<script src="js/rating_simple.js" type="text/javascript"></script>
	<script language="javascript" type="text/javascript">         
          function setRating()
          {
             $(function() {
                $("#rating_simple1").webwidget_rating_simple({
                    rating_star_length: '5',
                    rating_initial_value: '',
                    rating_function_name: '',//this is function name for click
                    directory: 'images/'
                });
             });
           }
           //setRating();
        </script>

        <!-- Feedback -->
        <div onclick="feedBack();" class="feedbackposition" style="cursor: pointer;background-image: url(images/tab-right-dark.png); border-top-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-style: solid none solid solid; border-top-color: rgb(255, 255, 255); border-bottom-color: rgb(255, 255, 255); border-left-color: rgb(255, 255, 255); border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; -webkit-box-shadow: rgba(255, 255, 255, 0.247059) 1px 1px 1px inset, rgba(0, 0, 0, 0.498039) 0px 1px 2px; box-shadow: rgba(255, 255, 255, 0.247059) 1px 1px 1px inset, rgba(0, 0, 0, 0.498039) 0px 1px 2px; font-style: normal; font-variant: normal; font-weight: bold; font-size: 14px; line-height: 1em; font-family: Arial, sans-serif; position: fixed; right: 0px;  z-index: 9999; background-color: rgb(0, 122, 180); display: block; background-position: 50% 0px; background-repeat: no-repeat no-repeat;">
        	<br><br><br>
		 <%if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>
						     <a href="javascript:void(0);"><img src="images/feedback-tab-fr.png" alt="" ></a>
                                       <%}else{ %>
						        <a href="javascript:void(0);"><img src="images/feedback-tab.png"></a>
                                      <%} %>

		<br><br>
	</div>
	<div class="modal" style="display: none; z-index: 2000; " id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-for-feedbackandsupport">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 id="myModalLabel"><spring:message code="lnkTeacherMatchFeedback"/>&amp; <spring:message code="lnkSupport"/></h3>
				</div>
				<% 
				Boolean kellyUser = false;
				if (session != null) {
				{
						if(session.getAttribute("userMaster") != null)
						{
							UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");
							if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==1){
								kellyUser = true;
							}
						}else if(session.getAttribute("teacherDetail") != null)
						{
						
							TeacherDetail teacherDetail =(TeacherDetail)session.getAttribute("teacherDetail");
							if(teacherDetail!=null)
							{
								ApplicationContext ac = RequestContextUtils.getWebApplicationContext(request);
								JobForTeacherDAO jobForTeacherDAO = (JobForTeacherDAO) ac.getBean("jobForTeacherDAO");
								kellyUser = jobForTeacherDAO.getJFTByTeachers(teacherDetail);
							}
						}
					}
				}
							
				if(kellyUser || request.getServerName().contains("kelly") || request.getServerName().toLowerCase().contains("smartpractice") || request.getRequestURL().toString().contains("smartpractice")){  %>
				<div class="modal-body" style="padding-top:50">
						<p>Please contact Kelly Educational Staffing via <a href="https://home-c4.incontact.com/inContact/ChatClient/ChatClient.aspx?poc=366af169-88ee-4e39-b4c9-3502581854ea&bu=4592562" target="_blank">Chat Support</a>, via email at <a href="mailto:KESTMSP@kellyservices.com" target="_top">KESTMSP@kellyservices.com</a> or via phone at 855-535-5915 for the following:</p>
						<ul>
						<li>Technical support or questions related TeacherMatch&trade;</li>
						<li>Technical support or questions related to Smart Practices&reg; (KES substitute teacher and paraprofessional talent only)</li>
						<li>Click <a href="http://kestraining.weebly.com/smart-practices.html" target="_blank">here</a> for quick video tutorials on topics such as creating or accessing your Smart Practices&reg; account, completing the assessment, or printing your certificate</li>
						<li>To request a copy of your Professional Development Profile(PDP) upon completion of the TeacherMatch Educators Professional Inventory (EPI) (KES substitute teacher talent only)</li>
						</ul>
						<p>Best,<br>
							Kelly Educational Staffing
						</p>
				</div>
					<% }else{%>
				<iframe id="uploadFrameID" name="uploadFrame" height="0" width="0" frameborder="0" scrolling="yes" sytle="display:none;"></iframe>
				<div class="modal-body" style="padding-top:0">

    					<h4 style="padding-bottom:0"><spring:message code="msgPleaseSelectOption"/>:</h4>

    					<table cellpadding="5" cellspacing="5">
    						<tr>
    							<td><span class="hdrSubject hidden" style="font-size:larger"></span></td>
    							<td style="padding-top:23px;"><button type="button" class="btn btn-mini pull-right btn-reset hidden"><spring:message code="lblchange"/></button></td>
    						</tr>
    					</table>

    					<form class="reqTypes form-horizontal" style="padding-top:20px">
						<div class="control-group">
							<h3><spring:message code="lblGeneral"/></h3>
							<label class="radio">
								<input type="radio" value="1a" name="reqType">
								<spring:message code="msgReceiveMoreInfo"/>
							</label>

							<h3><spring:message code="lblApplicants"/></h3>
							<label class="radio">
								<input type="radio" value="2a" name="reqType">
								<spring:message code="msgAuthenticateLogin"/>
							</label>

							<label class="radio">
								<input type="radio" value="2b" name="reqType">
								<spring:message code="msgQuestionConcern"/>
							</label>

							<h3><spring:message code="lnkDIstSchoolAdminUser"/></h3>
							<label class="radio">
								<input type="radio" value="3a" id="spt" name="reqType">
								<spring:message code="lblTechnocalSupport"/>
							</label>

							<label class="radio">
								<input type="radio" value="3c" name="reqType">
								<spring:message code="lblProvidefeedback"/>
							</label>
						</div>
					</form>

					<!-- info div -->
					<div class='fb-info hidden' style="padding-top:20px">
						<p><spring:message code="msgAssistanceByPhone"/> <span style="white-space:nowrap"><tel><spring:message code="msgPhoneNo"/></tel><spring:message code="msgKeyPress4"/></span> <spring:message code="msgFillFormBelow"/>.</p>
						<p><spring:message code="msgEnquiryForwared"/></p>
						<script charset="utf-8" type="text/javascript" src="https://js.hubspot.com/forms/current.js"></script>
						<script type="text/javascript">// <![CDATA[ 
							hbspt.forms.create({portalId: '290901',formId: 'c56cc312-30c0-4498-b089-fa180bbd7278'}); 
						// ]]></script>

					</div>
					<!-- end info div -->

					<form id="frmFeedbackUpload" enctype="multipart/form-data" method="post" target="uploadFrame" class="form-inline" onsubmit="return validateFeedBack();" accept-charset="UTF-8">
						<input type="hidden" id="visitedPageURL" name="visitedPageURL" value=""> 
						<input type="hidden" id="visitedPageTitle" name="visitedPageTitle" value="">
						<input type='hidden' id="msgSubject" name="msgSubject" value="">
						<input type="hidden" id="formType" name="formType" value="1"/>
						<input type="hidden" id="domainQuest" name="domainQuest" value=""/>
					   <!--ashish-->
					   <input type="hidden" id="reqType1" name="reqType1" value="aa"/>
                          
						<input type="hidden" id="" name="formType" value="1"/>

						<div class="control-group">
							<div class="divErrorMsg" id="errordivFeedBack" style="display: block;"></div>
						</div>

						<div id="feedback" class='fb-feedback hidden'>
							<div class="control-group">
								<div style="" id="ratingDiv">
									<input value="0" id="rating_simple1" name="starRating" type="hidden" style="float: left; ">
								</div>
								<div style="" id="checkFeedBackDiv"></div>
							</div>
							<div class="control-group">
								<div class="" id="msgFeed">
				    					<label><strong><spring:message code="lblFeedback"/></strong><span class="required">*</span></label>
			        					<textarea rows="5" class="span8" cols="" id="feedbackmsg" name="feedbackmsg" maxlength="1000"></textarea>
			        				</div>
			        			</div>
			        		</div>

			        		<div id="support" class='fb-support hidden'>

							<div class="tel-applicant hidden">
								<p><spring:message code="msgAssistanceByPhone"/> <span style="white-space:nowrap"><tel><spring:message code="msgPhoneNo2"/></tel></span> <spring:message code="msgFillFormBelow"/>.</p>
								<p><spring:message code="msgEnquiryForwared"/></p>
							</div>

							<div class="tel-partner hidden">
								<p><spring:message code="msgAssistanceByPhone"/> <span style="white-space:nowrap"><tel><spring:message code="msgPhoneNo3"/></tel></span> <spring:message code="msgFillFormBelow"/>.</p>
							</div>
				
							<div class='fb-contactinfo hidden'>
								<div class="col-sm-8 col-md-8" style=padding-left:0px;">
									<label for="inputFirstName"><spring:message code="lblFname"/><span class="required">*</span></label>
									<c:choose>
									    <c:when test="${userMaster.firstName!=null && userSession.firstName==null}">
									        <input type="text" id="inputFirstName" name="inputFirstName" class="form-control" value="${userMaster.firstName}" readonly="readonly" />
									    </c:when>
									    <c:when test="${teacherDetail.firstName!=null}">
									        <input type="text" id="inputFirstName" name="inputFirstName" class="form-control" value="${teacherDetail.firstName}" readonly="readonly" />
									    </c:when>
									    <c:otherwise>
									        <input type="text" id="inputFirstName" name="inputFirstName" class="form-control" placeholder="First Name" />
									    </c:otherwise>
									</c:choose>
								</div>

								<div class="col-sm-8 col-md-8" style=padding-left:0px;">
									<label for="inputLastName"><spring:message code="lblLname"/><span class="required">*</span></label>
									<c:choose>
									    <c:when test="${userMaster.lastName!=null && userSession.lastName==null}">
									        <input type="text" id="inputLastName" name="inputLastName" class="form-control" value="${userMaster.lastName}" readonly="readonly" />
									    </c:when>
									     <c:when test="${teacherDetail.lastName!=null}">
									        <input type="text" id="inputLastName" name="inputLastName" class="form-control" value="${teacherDetail.lastName}" readonly="readonly" />
									    </c:when>
									    <c:otherwise>
									        <input type="text" id="inputLastName" name="inputLastName" class="form-control" placeholder="Last Name" />
									    </c:otherwise>
									</c:choose>
								</div>

								<div class="col-sm-8 col-md-8" style=padding-left:0px;">
									<label for="inputEmail"><spring:message code="lblEmail1"/><span class="required">*</span></label>
									<c:choose>
									    <c:when test="${userMaster.emailAddress!=null && userSession.emailAddress==null}">
									        <input type="text" id="inputEmail" name="inputEmail" class="form-control" value="${userMaster.emailAddress}" readonly="readonly" />
									    </c:when>
									    <c:when test="${teacherDetail.emailAddress!=null}">
									        <input type="text" id="inputEmail" name="inputEmail" class="form-control" value="${teacherDetail.emailAddress}" readonly="readonly" />
									    </c:when>
									    <c:otherwise>
									        <input type="text" id="inputEmail" name="inputEmail" class="form-control" placeholder="Email" />
									    </c:otherwise>
									</c:choose>
								</div>
					
								<div class="col-sm-8 col-md-8" style=padding-left:0px;">
									<label for="inputPhone"><spring:message code="headPhone"/> </label>
									<c:choose>
									    <c:when test="${userMaster.phoneNumber!=null && userSession.phoneNumber==null}">
									        <input type="text" id="inputPhone" name="inputPhone" class="form-control" value="${userMaster.phoneNumber}" readonly="readonly" />
									    </c:when>
									     <c:when test="${teacherDetail.phoneNumber!=null}">
									        <input type="text" id="inputPhone" name="inputPhone" class="form-control" value="${teacherDetail.phoneNumber}" readonly="readonly" />
									    </c:when>
									    <c:otherwise>
									        <input type="text" id="inputPhone" name="inputPhone" class="form-control" placeholder="Phone" />
									    </c:otherwise>
									</c:choose>
								</div>
					
								<div class="col-sm-8 col-md-8" style=padding-left:0px;">
									<label for="inputDistrict"><spring:message code="lblDistrict"/><span class="required">*</span></label>
									<input type="text" id="inputDistrict" name="inputDistrict" class="form-control" placeholder="District">
								</div>
							</div>
					 
							<div class="" id="msgSpt">
								<div class="form-group">
									<label><strong><spring:message code="lblPleaseDescribeSupport"/>.</strong><span class="required">*</span></label>
									<textarea name="msg"></textarea>
									<!--<div style='background-color: #FFFFCC;' >-->
									<div id='file1' style="padding-top:8px;">
										<a href='javascript:void(0);' onclick='addFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addFileType();'><spring:message code="lnkAttachFile"/></a>
									</div>
								</div>
							</div>
						</div>
			 			<br>
			 			<div id="lodingImg" style="text-align: center; padding-top: 4px; display: none; "><img src="images/loadingAnimation.gif"> <spring:message code="msgMsgSending"/></div>
					</form>
				</div>
				<%} %>
				<div class="modal-footer">
        				<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>
 					<button id="buttonSubmit-frmFeedbackUpload" class="btn btn-primary hidden" onclick="saveFeedBack()"><spring:message code="btnSend"/></button>
				</div>
			</div>
		</div>
	</div>

	<script language="javascript">

		function getVisitedURL()
		{
			var ddd=document.URL;
			document.getElementById("visitedPageURL").value=""+ddd+"";
			document.getElementById("visitedPageTitle").value=""+document.title+"";
		}
		getVisitedURL();
		function showDisplay(dis)
		{
			$('#errordivFeedBack').empty();
			$('#lodingImg').hide();
			if(dis.value==1)
			{
				$('#feedback').hide();
				$('#support').show();
				$('#msgSubject').focus();
		
			}else
			{
				$('#feedback').show();
				$('#support').hide();
				$('#feedbackmsg').focus();
			}
	
			$('#ratingDiv').html("<input  value='0' id='rating_simple1' name='starRating' type='hidden'>");
			setRating();
			removeFile();
	
			$('#msgFeed').find(".jqte_editor").css("background-color", "");
			$('#msgSpt').find(".jqte_editor").css("background-color", "");
			$('#msgSubject').css("background-color", "");
			$('#msgFeed').find(".jqte_editor").html("");
			$('#msgSpt').find(".jqte_editor").html("");
			document.getElementById("msgSubject").value="";
		}
		
		function validateEmail(sEmail) {
			var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			if (filter.test(sEmail)) {
				return true;
			} else {
				return false;
			}
		}
		
		function validateFeedBack()
		{

			$('#errordivFeedBack').empty();
			$('#msgSubject').css("background-color", "");
			$('#f1').css("background-color", "");
			$('#msgFeed').find(".jqte_editor").css("background-color", "");
			$('#msgSpt').find(".jqte_editor").css("background-color", "");

			$('#inputFirstName').css("background-color", "");
			$('#inputLastName').css("background-color", "");
			$('#inputEmail').css("background-color", "");
			$('#inputDistrict').css("background-color", "");

			var cnt=0;
			var focs=0;

			var reqType=$('input[name=reqType]:radio:checked').val();
			if(reqType=="3c")
			{
				if ($('#msgFeed').find(".jqte_editor").text().trim()=="")
				{
					$('#errordivFeedBack').append("&#149; <spring:message code='msgPleaseEnterFeedback'/><br>");
					if(focs==0)
						$('#msgFeed').find(".jqte_editor").focus();
						$('#msgFeed').find(".jqte_editor").css("background-color", "#F5E7E1");
						cnt++;focs++;
				} else {
					var charCount=$('#msgFeed').find(".jqte_editor").text().trim();
					var count = charCount.length;
					if(count>1000)
					{
						$('#errordivFeedBack').append("&#149; <spring:message code='errMsgexceedcharlength'/><br>");
						if(focs==0)
							$('#msgFeed').find(".jqte_editor").focus();
						$('#msgFeed').find(".jqte_editor").css("background-color", "#F5E7E1");
						cnt++;focs++;
					}
				}

			} else {

				if(trim($('#inputFirstName').val())=="")
				{
					$('#errordivFeedBack').append("&#149; <spring:message code='msgPlzEtrFname'/>.<br>");
					if(focs==0)
						$('#inputFirstName').focus();
						$('#inputFirstName').css("background-color", "#F5E7E1");
					cnt++;focs++;
				}
				if(trim($('#inputLastName').val())=="")
				{
					$('#errordivFeedBack').append("&#149; <spring:message code='msgPlzEtrLname'/>.<br>");
					if(focs==0)
						$('#inputLastName').focus();
						$('#inputLastName').css("background-color", "#F5E7E1");
					cnt++;focs++;
				}
				if(trim($('#inputEmail').val())=="")
				{
					$('#errordivFeedBack').append("&#149; <spring:message code='msgPlzEtrEmail'/>.<br>");
					if(focs==0)
						$('#inputEmail').focus();
					$('#inputEmail').css("background-color", "#F5E7E1");
					cnt++;focs++;
				} else {
						if (validateEmail($('#inputEmail').val())) {
								//return true;
						} else {
								$('#errordivFeedBack').append("&#149; <spring:message code='errMsgInvalidEmailAddress'/>.<br>");
								if(focs==0)
								$('#inputEmail').focus();
								$('#inputEmail').css("background-color", "#F5E7E1");
								cnt++;focs++;
						}		
				}

				if(trim($('#inputDistrict').val())=="")
				{
					$('#errordivFeedBack').append("&#149; <spring:message code='errMsgPeaseEnterDistrict'/><br>");
					if(focs==0)
						$('#inputDistrict').focus();
					$('#inputDistrict').css("background-color", "#F5E7E1");
					cnt++;focs++;
				}
				
				if ($('#msgSpt').find(".jqte_editor").text().trim()=="")
				{
					$('#errordivFeedBack').append("&#149; <spring:message code='msgPleaseEnterFeedback'/><br>");
					if(focs==0)
						$('#msgSpt').find(".jqte_editor").focus();
					$('#msgSpt').find(".jqte_editor").css("background-color", "#F5E7E1");
					cnt++;focs++;
				}else
				{
					var charCount=$('#msgSpt').find(".jqte_editor").text().trim();
					var count = charCount.length;
					if(count>1000)
					{
						$('#errordivFeedBack').append("&#149; <spring:message code='errMsgFeedLengthExceed'/><br>");
						if(focs==0)
							$('#msgSpt').find(".jqte_editor").focus();
						$('#msgSpt').find(".jqte_editor").css("background-color", "#F5E7E1");
						cnt++;focs++;
					}
				}
			}
	
			if(cnt==0)
				return true;
			else
			{
				$('#errordivFeedBack').show();
				return false;
			}
		}
		function saveFeedBack()
		{
			if(!validateFeedBack())
				return;

			$('#lodingImg').show();
			var dom=window.location.host;
			document.getElementById('domainQuest').value=dom;
			document.getElementById('frmFeedbackUpload').action="feedBackUploadBeforeLoginServlet.do";
			$('#frmFeedbackUpload').submit();

		}
		function hideFeedBack(flg)
		{
			$('#lodingImg').hide();
			$('#myModal1').modal('hide');
			if(flg==1)
			{
				$('#message2show').html("<spring:message code='msgSuccessfullySent'/>");
				$('#myModal2').modal('show');
			}
			else if(flg==2)
			{
				$('#message2show').html("<spring:message code='msgFeedbackSuccess'/>");
				$('#myModal2').modal('show');
			}
			else if(flg==3)
			{
				alert("<spring:message code='msgSessionExpired'/>");  document.location = 'signin.do';
			}

			document.getElementById('msgSubject').value="";
			document.getElementById('msg').value="";
			document.getElementById('feedbackmsg').value="";
		}
		function addFileType()
		{
			$('#file1').empty();
			$('#file1').html("<a href='javascript:void(0);'onclick='removeFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='f1' class='left20 top-35' name='f1' size='20' title='Choose a File' type='file'>&nbsp;Maximum file size 10MB.");
		}
		function removeFile()
		{
			$('#file1').empty();
			$('#file1').html("<a href='javascript:void(0);' onclick='addFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addFileType();'>Attach a File</a>");
		}
		function feedBack()
		{
			$('#myModal1').modal('show');
			$('#file1').html("<a href='javascript:void(0);' onclick='addFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addFileType();'>Attach a File</a>");
			$('#msgSubject').focus();
			$('#errordivFeedBack').empty();
			$('#msgSubject').css("background-color", "");
			document.getElementById('msgSubject').value="";
	
		}
		function trim(s) {
			while ((s.substring(0, 1) == ' ') || (s.substring(0, 1) == '\n')
					|| (s.substring(0, 1) == '\r')) {
				s = s.substring(1, s.length);
			}
			while ((s.substring(s.length - 1, s.length) == ' ')
					|| (s.substring(s.length - 1, s.length) == '\n')
					|| (s.substring(s.length - 1, s.length) == '\r')) {
				s = s.substring(0, s.length - 1);
			}
		return s;
		}
	</script>

	<div class="modal" style="display: none;z-index: 5000" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">TeacherMatch</h3>
	      </div>
	      <div class="modal-body">
	      <div class="control-group" id='withdrawnReasonDiv' style="display: none;z-index: 5000">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<span class=""><label class=""><b><spring:message code="msgWithdrawal"/></b></label>
						<select class="form-control" id="withdrawnReasonId" name="withdrawnReasonId">
							 <c:if test="${not empty withdrawnReasonMaster}">
								<c:forEach var="reasonObj" items="${withdrawnReasonMaster}">
									<option value="${reasonObj[0]}">${reasonObj[1]}</option>
								</c:forEach>
							 </c:if>
						</select>
					</div>
				</div>
			</div>
	      	<div class="control-group" id='message2showConfirm'>
			</div>
	      </div>
	      <div class="modal-footer" id="footerbtn">
	        	<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="modal" id="myModal2" style="display: none;z-index: 5000" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 id="myModalLabel">TeacherMatch</h3>
				</div>
				<div class="modal-body">
					<div class="control-group" id="message2show"></div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
				</div>
			</div>
		</div>
	</div>
	<!-- End Feedback -->
	<div class="modal" style="display: none;z-index: 5000;top: 0px;" id="myModalv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog" style="width: 600px;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="vcloseBtn">x</button>
					<h3 id="myModalLabelId">TeacherMatch</h3>
				</div>
				<!--<div class="modal-body">
      				<div class="control-group" id='message2showConfirm'></div>
      			</div>
				--><div class="modal-body" style="max-height: 450px;overflow-y:auto;">
					<table width="100%">
						<tbody>
							<tr>
								<td width="15%" valign="top" style="padding-top: 0px;">
									<div class="control-group" id="warningImg1"></div>
								</td>
								<td width="85%" style="padding-left: 10px;">
									<div class="control-group" id="message2showConfirm1"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="control-group" id="nextMsg"></div>	
		 		</div>
				<div class="modal-footer" id="footerbtn1">
					<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" style="display: none;z-index: 5000" id="myModalvk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog-for-cgmessage">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="vcloseBtnk">x</button>
					<h3 id="myModalLabel">TeacherMatch</h3>
				</div>
				<div class="modal-body">
					<table width="100%">
						<tbody>
							<tr>
								<td width="15%" valign="top" style="padding-top: 8px;">
									<div class="control-group" id="warningImg1k"></div>
								</td>
								<td width="85%" style="padding-left: 10px;">
									<div class="control-group" id="message2showConfirm1k"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="control-group" id="nextMsgk"></div>
				</div>
				<div class="modal-footer" id="footerbtn1k">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button>
				</div>
			</div>
		</div>
	</div>
	<!-- By alok for tablet -->
	<div class="modal hide" id="docfileNotOpen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
  					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
					<h3 id="myModalLabel">Teachermatch</h3>
				</div>
				<div class="modal-body">		
					<div class="control-group">
						<div class="">
			 				<spring:message code="msgDocIsMSWord"/>
						</div>
					</div>
 				</div>
 				<div class="modal-footer">
 					<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();"><spring:message code="btnClose"/></button> 		
 				</div>
			</div>
		</div>
	</div>
	<div class="modal hide" id="exelfileNotOpen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
  					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
					<h3 id="myModalLabel">Teachermatch</h3>
				</div>
				<div class="modal-body">		
					<div class="control-group">
						<div class="">
			 				<spring:message code="msgDocIsExcel"/>.
						</div>
					</div>
 				</div>
 				<div class="modal-footer">
 					<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();"><spring:message code="btnClose"/></button> 		
 				</div>
			</div>
		</div>
	</div>
	<div class="modal hide" id="printmessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
					<h3 id="myModalLabel">Teachermatch</h3>
				</div>
				<div class="modal-body">		
					<div class="control-group">
						<div class="">
							<spring:message code="msgPlzPopAreAlloued"/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();"><spring:message code="btnClose"/></button> 		
				</div>
			</div>
		</div>
	</div>
	
	<script language='javascript'><!--
		$(document).ready(function(){

			// CDM edits 8/14/2014
		
			function resetFbModal() {
				$('.btn-reset').addClass('hidden');
				$('.tel-applicant').addClass('hidden');
				$('.tel-partner').addClass('hidden');
				$('.reqTypes').removeClass('hidden');
				subjectLine = "";
				$('.fb-info').addClass('hidden');
				$('.fb-feedback').addClass('hidden');
				$('.fb-support').addClass('hidden');
				$('.fb-contactinfo').addClass('hidden');
				$('#buttonSubmit-frmFeedbackUpload').addClass('hidden');
				$('.hdrSubject').html('').addClass('hidden');
				$('input:radio[name="reqType"]').prop('checked', false);
				$('#ratingDiv').html("<input  value='0' id='rating_simple1' name='starRating' type='hidden'>");
				setRating();
				removeFile();
				$('#msgFeed').find(".jqte_editor").css("background-color", "");
				$('#msgSpt').find(".jqte_editor").css("background-color", "");
				$('#msgSubject').css("background-color", "");
				$('#msgFeed').find(".jqte_editor").html("");
				$('#msgSpt').find(".jqte_editor").html("");
				$('.divErrorMsg').html('');

				$('#inputFirstName').css("background-color", "");
				$('#inputLastName').css("background-color", "");
				$('#inputEmail').css("background-color", "");
				$('#inputDistrict').css("background-color", "");

				/*$('#inputFirstName').val("");
				$('#inputLastName').val("");
				$('#inputEmail').val("");
				$('#inputPhone').val("");*/
				$('#inputDistrict').val("");

			}
		
			$('#myModal1').on('show.bs.modal', function () {
				resetFbModal();
			});
		
			$('.btn-reset').click(function(e) {
				e.preventDefault();
				resetFbModal();
			});
		
			$('#buttonSubmit-frmFeedbackUpload').click(function(e) {
				e.preventDefault();
				saveFeedBack();
			});
		
			$('input:radio[name="reqType"]').change(function(){
		      	var subjectLine = "";
				if ($(this).is(':checked') && $(this).val() == '1a') {
  			     	var reqType=$(this).val();
                    $('#reqType1').val(reqType);
				  subjectLine = "Support Request: Product Information";
					$('.tel-applicant').addClass('hidden');
					$('.tel-partner').addClass('hidden');
					$('.fb-info').removeClass('hidden');
					$('.fb-feedback').addClass('hidden');
					$('.fb-support').addClass('hidden');
					$('.fb-contactinfo').addClass('hidden');
					$('#buttonSubmit-frmFeedbackUpload').addClass('hidden');
					$('#checkFeedback').val("");
				} else if ($(this).is(':checked') && $(this).val() == '2a') {
				var reqType=$(this).val();
                    $('#reqType1').val(reqType);
					subjectLine = "Support Request by Applicant: Login Help";
					$('.tel-applicant').removeClass('hidden');
					$('.tel-partner').addClass('hidden');
					$('.fb-info').addClass('hidden');
					$('.fb-feedback').addClass('hidden');
					$('.fb-support').removeClass('hidden');
					$('.fb-contactinfo').removeClass('hidden');
					$('#buttonSubmit-frmFeedbackUpload').removeClass('hidden');
					$('#checkFeedback').val("");
				} else if ($(this).is(':checked') && $(this).val() == '2b') {
				var reqType=$(this).val();
                    $('#reqType1').val(reqType);
					subjectLine = "Support Request by Applicant: Question";
					$('.tel-applicant').removeClass('hidden');
					$('.tel-partner').addClass('hidden');
					$('.fb-info').addClass('hidden');
					$('.fb-feedback').addClass('hidden');
					$('.fb-support').removeClass('hidden');
					$('.fb-contactinfo').removeClass('hidden');
					$('#buttonSubmit-frmFeedbackUpload').removeClass('hidden');
					$('#checkFeedback').val("");
				} else if ($(this).is(':checked') && $(this).val() == '3a') {
				var reqType=$(this).val();
                    $('#reqType1').val(reqType);
					subjectLine = "Support Request by Partner: Technical Support";
					$('.tel-applicant').addClass('hidden');
					$('.tel-partner').removeClass('hidden');
					$('.fb-info').addClass('hidden');
					$('.fb-feedback').addClass('hidden');
					$('.fb-support').removeClass('hidden');
					$('.fb-contactinfo').removeClass('hidden');
					$('#buttonSubmit-frmFeedbackUpload').removeClass('hidden');
					$('#checkFeedback').val("");
				} else if ($(this).is(':checked') && $(this).val() == '3b') {
				var reqType=$(this).val();
                    $('#reqType1').val(reqType);
					subjectLine = "Support Request by Partner: Product Help";
					$('.tel-applicant').addClass('hidden');
					$('.tel-partner').removeClass('hidden');
					$('.fb-info').addClass('hidden');
					$('.fb-feedback').addClass('hidden');
					$('.fb-support').removeClass('hidden');
					$('.fb-contactinfo').addClass('hidden');
					$('#buttonSubmit-frmFeedbackUpload').removeClass('hidden');
					$('#checkFeedback').val("");
				} else if ($(this).is(':checked') && $(this).val() == '3c') {
				var reqType=$(this).val();
                    $('#reqType1').val(reqType);
					subjectLine = "Support Request by Partner: Feedback";
					$('.tel-applicant').addClass('hidden');
					$('.tel-partner').addClass('hidden');
					$('.fb-info').addClass('hidden');
					$('.fb-feedback').removeClass('hidden');
					$('.fb-support').addClass('hidden');
					$('.fb-contactinfo').addClass('hidden');
					$('#buttonSubmit-frmFeedbackUpload').removeClass('hidden');
					$('#checkFeedBackDiv').html("<input  value='-1' id='checkFeedback' name='checkFeedback' type='hidden'>");
				} else {
				var reqType=$(this).val();
                    $('#reqType1').val(reqType);
					subjectLine = "";
					$('.tel-applicant').addClass('hidden');
					$('.tel-partner').addClass('hidden');
					$('.fb-info').addClass('hidden');
					$('.fb-feedback').addClass('hidden');
					$('.fb-support').addClass('hidden');
					$('.fb-contactinfo').addClass('hidden');
					$('#buttonSubmit-frmFeedbackUpload').addClass('hidden');
					$('#checkFeedback').val("");
				}
				$('#msgSubject').val(subjectLine);
				$('.hdrSubject').html('<br />'+subjectLine).removeClass('hidden');
				$('.btn-reset').removeClass('hidden');
				$('.reqTypes').addClass('hidden');
			});
		});
	--></script>

	<!-- End feedback and support -->

	<!-- abbreviated script to initialize editor, may be removed -->
