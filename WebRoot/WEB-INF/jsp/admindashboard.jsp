<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/AdminDashboardAjax.js?ver=${resourceMap['AdminDashboardAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/admindashboard.js?ver=${resourceMap['js/admindashboard.js']}"></script>

                                <div class="row">						  
							        <div class="col-sm-6">
									   	    <div style="float: left;">
									         	<img src="images/dashboard-icon.png" width="41" height="41" alt="">
									        </div>        
									        <div style="float: left;">
									         	<div class="subheading">${userSession.firstName } ${userSession.lastName } Mosaic</div>	
									        </div>
									        <div style="clear: both;"></div>	
									</div>
								
								     <div class="col-sm-6">
								         <div class="changefloating">
								    	 	<a href='candidatesdetail.do'><spring:message code="lnkCandidateDetails"/></a>
								    	 	
								    	 </div>
								     </div>	
								       <div style="clear: both;"></div>							
												  	
	    				
							        <div class="col-sm-12">
									   	 <div class="centerline"></div>    
									</div>							 						
			                    </div>			



	<div class="row top10">		
	       
			    <div class="left8">
					<iframe id="ifrmChkLogin" src="chkactiveruser.do" scrolling="no" height="40px;" width="100%;" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;"></iframe>
				</div>			
			
		    <div class="col-sm-6" >			
				<table width="100%" border="0" class="table table-bordered">
					<thead class="bg">
						<tr>
							<th>
								<img src="images/joborders.png" width="33" height="35"
									class="fl districticon">
								<div class="left15">
									<div style="font-size: 15px;">						
											<spring:message code="divCandidates"/>
								    </div>																	
									<span class="clearfix pull-left font_normal"><spring:message code="spYest"/></span>
								</div>
							</th>
						</tr>
					</thead>
					<tr>
						<td class="dash_td" id='yesterdayGrid'>
						</td>
					</tr>
				</table>			  
	       </div>
	      
          <div class="col-sm-6">          
			<table width="100%" border="0" class="table table-bordered">
				<thead class="bg">
					<tr>
						<th>
							<img src="images/joborders.png" width="33" height="35"
								class="fl districticon">						
							   <div style="font-size: 15px;">									
								<spring:message code="divCandidates"/>								
							  </div>								
							  <span class="clearfix pull-left font_normal"><spring:message code="spTotalsDate"/></span>							
						</th>
					</tr>
				</thead>
				<tr>
					<td class="dash_td" id="totalsOfDateGrid">
					</td>
				</tr>
			 </table>
         </div> 
  </div>

<script type="text/javascript">
getYesterdayApplicants();
getTotalsOfDateApplicants();
</script>