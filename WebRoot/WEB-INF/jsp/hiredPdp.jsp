<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/HiredPdpAjax.js?ver=${resourceMap['HiredPdpAjax.ajax']}"></script>
<script type='text/javascript' src="js/hiredPdp.js?ver=${resourceMap['js/hiredPdp.js']}"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<style>
div.t_fixed_header div.body {
	overflow-x	: auto;
}
</style>


<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});


function applyScrollOnTblVisit()
{
   //alert("11")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridHired').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
       // height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[130,115,115,180,100,100,,100],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

function applyScrollOnPrintTable()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridHireTime').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 300,
         width: 940,
        minWidth: null,
        minWidthAuto: false,
     //  colratio:[105,60,100,80,70,90,85,70,70,70,70,70],
        //colratio:[175,175,110,120,200,100,85],
        colratio:[130,115,115,180,100,100,,100],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

</script>

<style>
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
</style>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/districtjobs-orders.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lblhiredPdpReport"/> </div>	
         </div>			
		 
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>
    		
					<div class="row" id=" row Searchbox">
					<!--condition for the district-->
		           		<!-- shriram -->
		           		<input type="hidden" id="districtiddd" value="${DistrictOrSchoolId}"/>
		           		
		          <div class="col-sm-4 col-md-4">
				     <label id="captionDistrictOrSchool"><spring:message code="lblDistrictName"/></label>
				      <c:if test="${DistrictOrSchoolName==null}">
					       <span>
					        <input type="text"  maxlength="255" id="districtORSchoolName" name="districtORSchoolName" class="help-inline form-control"
					            		 onfocus="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');chkschoolBydistrict();"
													onkeyup="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');chkschoolBydistrict();"
													onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');chkschoolBydistrict();getJobcategoryList1();"/>
							 </span>
							 <input type="hidden" id="districtOrSchooHiddenlId"/>							
				         </c:if>
				    		
							<c:if test="${DistrictOrSchoolName!=null}"> 
							<span>
					        <input type="text" maxlength="255"   class="help-inline form-control" value="${DistrictOrSchoolName}"  disabled="disabled"/>
							 </span>
				             		
				             	
				             	<input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
				             	<input type="hidden" id="districtORSchoolName" name="districtORSchoolName"/>
				             	<input type="hidden" id="districtHiddenlIdForSchool"/>
				             	<script>getJobcategoryList1()</script>
				            </c:if>
				            <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
							             	
               </div>
					<!-- Job category Filter-->		
							<div class="col-sm-3 col-md-3">
		             			<label id="lbljobCategoryID"><spring:message code="lblJobCat"/></label>
			             		<select id="jobCategoryIdd" name="jobCategoryName"  class="form-control" >
			             		</select>
			             		</div>
						<!--job Title-->
							<div class="col-sm-5 col-md-5">
		             			<label id="lbljobTitalIDD"><spring:message code="lblJoTil"/></label>
			             		<input type="text" id="jobTitleID" name="JobTitleName" class="form-control"
			             		onkeyup="getJobTitleList()" onblur="onblur1()"/>
		             		<div id='jobcategoryDiv'  style='display:none;position:absolute;z-index:5000;' class='result' ></div>
							
			             	</div>	
			            <!--job Status
							<div class="col-sm-3 col-md-3">
		             			<label id="lbljobStatusIDD"><spring:message code="lblJoStatus"/></label>
			             		
			             		<label id="lbljobStatusIDD">Application Status</label>
			             		<select id="jobstatusID" name="JobstatusName"  class="form-control" >
			             		</select>
			       		</div>	-->
			       		<input type="hidden" id="jobstatusID"  name="JobstatusName"/>
			       		<div class="row" style="margin-left:0">
			       		 <div class="col-sm-3 col-md-3">
	               		<label id="captionDistrictOrSchool">Start Date</label>
	               		<span>
						  <input type="text" id="fromDate" name="fromDate" class="help-inline form-control">
						</span>
	               </div>
					
       		 


		
			       		
	               <div class="col-sm-3 col-md-3">
	               		<label id="captionDistrictOrSchool">End Date</label>
	               		<span>
						  <input type="text" id="endtoDate" name="endtoDate" class="help-inline form-control">
						</span>
	               </div>
	               <!--teacher ID-->
							<div class="col-sm-3 col-md-3" style="display: none;">
	               		<label id="captionDistrictOrSchool">Teacher Id</label>
						  <input type="text" id="tcherId" name="tchername" class="help-inline form-control">
	               </div>
	          	
					<div class="col-sm-3 col-md-3" >
						<label>&nbsp;</label>
						<button class="btn btn-primary top25-sm2" type="button" onclick="searchRecordsByNobleCandidate()"><spring:message code="btnSearch"/> <i class="icon"></i></button>
					</div>
					

</div>

<!--filter end:::::::::::::::::::::::-->
		  
	<div style="display:none;" id="loadingDiv">
    <table  align="center">
	<tr><td style="padding-top:270px;" align="center"><img src="images/please.jpg"/></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
    
              <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
			 </iframe>  
		  </div> 
		 
       
    <input type="hidden" id="entityType" name="entityType" value="${userMaster.entityType}">
    
    <div style="clear: both;"></div>
     <br/>
         <table class="marginrightForTablet">
		      <tr>
		         <td  style='padding-left:5px;'>
					<a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="generateJobOrderEeocEXL();"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td>
				  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='generateJobOrderEeocPDF();'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				
				 <td>
					<a data-original-title='Print Preview' rel='tooltip' id='printId' href='javascript:void(0);' onclick="generateJobOrderEeocPrintPre();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td></tr>
		</table>
           <div style="clear: both;"></div>

  
  
  
  <div class="TableContent">  	
             <div class="table-responsive" id="divMainHired" >    
           </div>       
       </div>
  
 <!-- printing Start  -->
    
  <div style="clear: both;"></div>

   <div class="modal hide"  id="printHiredApplicantsDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1000px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   	<h3 id="">Print Preview</h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			<div class="" id="printhiredApplicantsDataTableDiv"  >          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='printNobleJobsDATA();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    
    
     <div style="clear: both;"></div>

   <div class="modal hide"  id="printmessage1"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   <h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" >
			<spring:message code="msgPlzPopAreAlloued"/>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'><spring:message code="btnOk"/></button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    <!-- printing End -->       

       
       <!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadHiredCandidates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Hired PDP Report</h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Close</button> 		
	 	</div>
     </div>
	 	</div>
	</div>
	<script>
/* Shriram	26 October*/
function endDateStartdate(msg)
{
	
	$('#dateValidation .modal-body').html(msg);
	$('#dateValidation').modal('show');
	$('#loadingDiv').hide();
}
</script>

<div class="modal hide" id="dateValidation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>
	<!-- End of 26October -->
	
	<!-- PDF End -->   
	<script type="text/javascript">
	   var cal = Calendar.setup({
	          onSelect: function(cal) { cal.hide() },
	          showTime: true
	      });
	      cal.manageFields("fromDate", "fromDate", "%m-%d-%Y");
	      var cal = Calendar.setup({
	          onSelect: function(cal) { cal.hide() },
	          showTime: true
	      });
	      cal.manageFields("endtoDate", "endtoDate", "%m-%d-%Y");
	</script>
	    
	<script type="text/javascript">
		searchRecordsByNobleCandidate();
	chkschoolBydistrict();
	getJobStatusList();
</script>
