<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/DistrictQuestionsAjax.js?ver=${resourceMap['DistrictQuestionsAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>
<script type='text/javascript' src="js/districtjobreport.js?ver=${resourceMap['js/districtjobreport.js']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.ajax']}"></script>

<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>

<div class="row col-sm-offset-2 col-md-offset-2 col-sm-8 col-md-8 modalTM1" id='searchItem' style="margin-top: 15px;padding-top: 15px;padding-bottom:15px;">	
     <div class='divErrorMsg' id='errordiv' style="padding-left: 15px;"></div>
        <div class="col-sm-9 col-md-9">
           <label><spring:message code="lblDistrictName"/></label>
           <span>
           <input type="text" id="districtName" autocomplete="off" maxlength="100"  name="districtName" class="help-inline form-control"
          		 onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
						onkeyup="getDistrictAuto(this,event,'divTxtShowData', 'districtName','districtId','');"
						onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
     	   	</span>
         	<input type="hidden" id="districtId" value="${districtId}"/>
         	<div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
         </div>
 
	  <div class="col-sm-3 col-md-3">
        <label  style="float: right;margin-right:-25px;" id='closePan'>&nbsp;</label>
         <button class="btn btn-primary top25-sm" style="width: 100px;" type="button" onclick="districtAlljobsReport();"><spring:message code="btnSearch"/> <i class="icon"></i></button>         	
      </div>     
 </div>

<div class="">
<div style="clear: both;"></div>
<div class="modal hide "  id="districtjobreportModelDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1200px; margin-top: 0px;">
	 <div class="modal-content">
		 <div class="modal-header">
		   	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrintData();">x</button>
		   	 <h3 id=""><spring:message code="headCandJobDet"/></h3>
		 </div>
	
		    <div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
				<div class="" id="districtAllJobReportTableDiv" >          
	             </div>
	 	   </div>
	   	 <div class="modal-footer">
		 	<table border=0 style="margin-left:850px;">
		 		<tr>
			 		<td  nowrap>
			 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='exlExportforDistrictJobs();'>&nbsp;<spring:message code="lblExpToExcel"/>&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
					</td>
					<td  nowrap>
			 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrintData();'><spring:message code="btnClr"/></button>
					</td>
		 		</tr>
		 	</table>
		 </div>
   </div>
 </div>
</div>
             <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
				 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
				 </iframe>  
			  </div> 
</div>


	<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
 