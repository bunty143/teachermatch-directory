<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<!-- Editor 2 -->`
<!--  <textarea name="text" class='editor' ></textarea> 
<script type="text/javascript" src="js/jquery-te-1.1.min.js" charset="utf-8"></script>
<link type="text/css" rel="stylesheet" href="css/jquery-te-1.1.css" charset="utf-8" />
<!-- Editor 2 -->

<script type="text/javascript" src="dwr/interface/DistrictAssessmentAjax.js?ver=${resourceMap['DistrictAssessmentAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/districtassessment.js?ver=${resourceMap['js/districtassessment.js']}"></script>

<script type="text/javascript" src="js/jquery-1.4.1.js" ></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
            
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: 965,
        minWidth: null,
        minWidthAuto: false,
       // colratio:[220,100,85,85,60,290], // table header width
        colratio:[245,115,180,210,240],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

 </script>
<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headMngSctForAss"/></div>	
         </div>			
		 <div class="pull-right add-employment1">
		
		<a href="javascript:void(0);" onclick="return addAssessmentSection()"><spring:message code="lnkAddSection"/></a>
		

		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div class="row">
<div class="mt10">
		
<div class="col-sm-8 col-md-8">
<a href="districtassessment.do"><spring:message code="headAss"/></a>: ${districtAssessmentDetail.districtAssessmentName}
</div>                  
</div>
</div>

<input type="hidden" id="districtassessmentId" value="${districtAssessmentDetail.districtAssessmentId}">

<div class="TableContent top15">        	
         <div class="table-responsive" id="divMain">          
         </div>            
</div> 


<div  id="divAssessmentRow" style="display: none;" onkeypress="chkForEnterSaveAssessmentSection(event);">
<div class="row top5">
<div class="col-sm-8 col-md-8">			                         
 			<div class='divErrorMsg' id='errordiv' ></div>
</div>
</div>

<div class="row">
<div class="col-sm-6 col-md-6"><label><strong><spring:message code="lblSectName"/><span class="required">*</span></strong></label>
<input type="hidden" name="districtSectionId" id="districtSectionId" >
<input type="hidden" name="pageIdNo" id="pageIdNo" value="2" >
<input type="text" name="sectionName" id="sectionName" maxlength="100" class="form-control" placeholder=""></div>
</div>

<div class="row">
<div class="col-sm-6 col-md-6" id="secDescription"><label><strong><spring:message code="headSctDes"/></strong></label>
<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/>
 </label>
<textarea class="form-control editor1" name="sectionDescription" id="sectionDescription" maxlength="750" rows="4" onkeyup="return chkLenOfTextArea(this,750);" onblur="return chkLenOfTextArea(this,750);"></textarea>
</div>
</div>

<div class="row">
<div class="col-sm-6 col-md-6" id="secInstruction"><label><strong><spring:message code="headSectInst"/></strong></label>
<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/>
 </label>
<textarea class="form-control editor2" name="sectionInstructions" id="sectionInstructions" maxlength="2500" rows="10" onkeyup="return chkLenOfTextArea(this,2500);" onblur="return chkLenOfTextArea(this,2500);"></textarea>
</div>
</div>



 <div class="row top20" style="display: none;" id="divManage">
  	<div class="col-sm-4 col-md-4">
  		
     	<button onclick="saveDistrictAssessmentSection();" class="btn btn-large btn-primary"><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
     	
     	&nbsp;<a href="javascript:void(0);" onclick="clearAssessmentSection();"><spring:message code="lnkCancel"/></a> 
     </div>
  </div>

<div class="row top15">
<div class="idone col-sm-4 col-md-4" id="divDone" style="display: none;">

<a href="javascript:void(0);" onclick="saveDistrictAssessmentSection();"><spring:message code="lnkImD"/></a>

&nbsp;<a href="javascript:void(0);" onclick="clearAssessmentSection();"><spring:message code="lnkCancel"/></a>
</div>
</div>
</div>

<script type="text/javascript"> getAssessmentSectionGrid(); </script>
<script>
/*
 $('textarea').jqte();
 $(".editor1").jqte(); 
 $(".editor2").jqte();
*/ 
</script>