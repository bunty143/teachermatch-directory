<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />

	
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script src="jquery/jquery-ui.custom.js" type="text/javascript"></script>
<script src="jquery/jquery.cookie.js" type="text/javascript"></script>
<link href="jquery/skin/ui.dynatree.css" rel="stylesheet" type="text/css">
<script src="jquery/jquery.dynatree.js" type="text/javascript"></script>
<!-- jquery.contextmenu,  A Beautiful Site (http://abeautifulsite.net/) -->
<script src="contextmenu/jquery.contextMenu-custom.js" type="text/javascript"></script>
<link href="contextmenu/jquery.contextMenu.css" rel="stylesheet" type="text/css" >

<!-- Start_Exclude: This block is not part of the sample code -->
<link href="contextmenu/prettify.css" rel="stylesheet">
<script src="contextmenu/prettify.js" type="text/javascript"></script>
<link href="contextmenu/sample.css" rel="stylesheet" type="text/css">
<script src="contextmenu/sample.js" type="text/javascript"></script>
<!-- End_Exclude -->

<script type="text/javascript" src="dwr/interface/CandidateReportAjax.js?ver=${resourceMap['CandidateReportAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.Ajax']}"></script>

<script type="text/javascript" src="dwr/interface/ManageStatusAjax.js?ver=${resourceMap['ManageStatusAjax.Ajax']}"></script>
<script type="text/javascript" src="js/managenode.js?ver=${resourceMap['js/managestatus.js']}"></script>
 
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
	<style>
	.hide
	{
		display: none;
	}
	 .icon-folder-open
	{
		 font-size: 1.5em;
		color:#007AB4;
	}
	.icon-copy
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	.icon-edit
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	.icon-cut
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	.icon-paste
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	
	.icon-remove-sign
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	.marginleft20
	{
		margin-left:-20px;
	}
	
	.scrollit {
    	max-height:100px;
    	overflow-y:auto;
    	overflow-x:hidden;
	}
	
	/*<!--add by Ram nath	-->*/
	#divGridQuestionList .table-bordered .net-widget-footer.net-corner-bottom{
	width: 100% !important;
	}	
	/*<!--end by Ram nath	-->*/
	
	</style>
	
	<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Manage Node</div>	
         </div>
       	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
    </div>
	
	<input type="hidden" id="districtId" name="districtId" value="${DistrictId}" />
	<input type="hidden" id="secondaryStatusId" value="" />
	<input type="hidden" id="skillAttributeId" value="" />
	<input type="hidden" id="score" value="" />
	<input type="hidden" id="questionId" value="" />
	<input type="hidden" id="pageSize" value="20" />
	
	
	<div class="row">
		<div class="col-sm-8 col-md-8">
			<div class='divErrorMsg' id='errordiv' style="padding-left: 20px; padding-top: 10px;"></div>
		</div>
	</div>
	
	<div class="row mt10">  
	<div class="col-sm-5 col-md-5">
	   	<label>District Name</label>
	   		<input type="text" id="districtName" name="districtName" disabled="disabled" maxlength="100"  value="${DistrictName}" readonly="readonly" class="form-control" />	
	</div>
	
	<div class="col-sm-5 col-md-5">
		<label>Job Category&nbsp;<a href="#" id="jobCategoryTooltip" rel="tooltip" data-original-title="Select a Job Category if you want to define the Status Life Cycle for that Job Category. No need to select a Job Category if you want to define District Specific Status Life Cycle."><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
		<div id="jobCategory_div">
			<select class="form-control" name="jobCategory" id="jobCategory">
				<option selected="selected" value="">Select Category</option>
				<c:forEach items="${jobCategoryMasterList}" var="jcml">
					<option value="${jcml.jobCategoryId}">${jcml.jobCategoryName}</option>
				</c:forEach>
			</select>
	    </div>
	</div>	    
	<div class="col-sm-2 col-md-2">	    	
    	<button type="button" class="btn btn-primary top25-sm"  onclick="return displaySLC();"><strong>Go <i class="icon" ></i></strong></button>
    </div>
    
	</div>
	
	<div class="row" style="height: 300px;margin-top: 71px;margin-left: 81px;">
		<div class="col-sm-6 col-md-6">
			<span id='statusdashboard'></span>
		</div>
	</div>
		
	<div class="modal hide" id="addQuestionDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width:845px;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="add-Question  pull-right" >
								<a href="javascript:void(0);" onclick="showQuestionForm();">+ Add a Question</a>
							</div>
						</div>
					</div>
				
					<div id="questionGridDiv"> </div>
					
					<div id="divQuestionList" class="mt10 " style="display: none;">
						<div class="row">
					       <div class="col-sm-8 col-md-8">
								<div class='divErrorMsg' id='errorQuestion'></div>
							</div>
				      	</div>

			    		<div class="row">
					       <div class="col-sm-8 col-md-8">
					       <span class=""><label>Question</label>
					       	<input type="text" id="question" name="question"  maxlength="2500" class="form-control"/>
					       </span>
					       </div>
				      	</div>
				      	
				    	<div class="row top10">
							<div class="col-sm-5 col-md-5" id="divDone">
							<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;" onclick="saveOrUpdateQuestion()">
								<spring:message code="lnkImD"/>
							</a>&nbsp;&nbsp;
							<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;" onclick="cancelQuestion()">
								Cancel
							</a>
						</div>
					   </div>
				 </div>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary"  data-dismiss="modal">Save <i class="icon"></i></button></span>&nbsp;&nbsp;
 				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
 				<!-- <span id=""><button class="btn btn-primary" data-dismiss="modal">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				 -->
			</div>
		</div>
	</div>
	</div>
	
	<div style="display:none; z-index: 5000;" id="loadingDiv" >
    <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'></td></tr>
	</table>
	</div>
	
	
	<div class="modal hide"  id="confirmationPopup"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="confimationCancel()">x</button>
					<h3 id="myModalLabel">Teachermatch</h3>
				</div>
				
				<div class="modal-body">
					The district has set the content for this node. Please be advised you can not add additional content outside of adding notes.
				</div>
				
				<div class="modal-footer">
					<!-- <span id=""><button class="btn  btn-primary" onclick="confimationOk()" >Continue <i class="icon"></i></button></span>&nbsp;&nbsp;  -->
					<button class="btn" aria-hidden="true" onclick="confimationCancel()">Ok</button>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<!-- Start Div -->	
 	<div class="modal hide"  id="deleteFolder"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group" id="deleteFolder_body">
			
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="deleteStatusConfirm()" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 	<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 		
 	</div>
	</div>	
	</div>
	</div>	
	<!-- End_Exclude -->
	
<!-- Start ... Add Question  -->