<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script type="text/javascript" src="js/manageuser.js?ver=${resourceMap['js/manageuser.js']}"></script>
<script type="text/javascript" src="dwr/interface/HeadQuarterAjax.js?ver=${resourceMap['HeadQuarterAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/JBServiceAjax.js?ver=${resourceMap['JBServiceAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/addbranch.js?ver=${resourceMap['js/addbranch.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tempDistrictTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 722,
        minWidth: null,
        minWidthAuto: false,
        colratio:[288,182,144,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

</script>


<style>
	.hide
	{
		display: none;
	}
</style>
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Add New Branch<span id="districtOrSchoolTitle"></span></div>	
         </div><!--
          <div class="pull-right add-employment1">	
		    <c:set var="url" value="addbranch.do?hQuarterId=${headQuarterMaster.headQuarterId}"></c:set>	
			<a href="javascript:void(0);" onclick="addnewBranch('${url}')">+Add New Branch</a>
		 </div>
		--><div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
					<div class="row">
							<div class="col-sm-8 col-md-8">
								<div id="errordiv" class="hide" style="color:red;"></div>
							</div>
					</div>

					<div class="row">
							<div class="col-sm-4 col-md-4 top10">
								<div class="">
									<span class=""><label class="">
											Branch Number<span class="required">*</span>
										</label> <input type="text" name="branchCode" id="branchCode"
											class="form-control" /> </span>
								</div>
							</div>
						
							<div class="col-sm-4 col-md-4 top10" style="margin-left: 150px;">
								<label>
									Email Address<span class="required">*</span>
								</label>
								<input type="text" class="form-control" maxlength="100"
									id="branchEmail" name="branchEmail" />
							</div>
					</div>
					
					<div class="row">
							<div class="col-sm-4 col-md-4">
								<div class="">
									<span class=""><label class="">
											Branch Business Name<span class="required">*</span>
										</label> <input type="text" name="bbName" id="bbName"
											class="form-control" /> </span>
								</div>
							</div>
						
							<div class="col-sm-4 col-md-4" style="margin-left: 150px;">
								<label>
									Time Zone<span class="required">*</span>
								</label>
								<select id="timezone" name="timezone"  class="form-control">
	  								<option value="0">Select Time Zone</option>
	   									<c:if test="${not empty timeZones}">
									        <c:forEach var="tz" items="${timeZones}" varStatus="status">
									        <option value="${tz.timeZoneId}"><c:out value="${tz.timeZoneName}  (${tz.timeZoneShortName})"/></option>
									        </c:forEach>
        								</c:if>
        						</select>
							</div>
					</div>
					
					<div class="row">
							<div class="col-sm-4 col-md-4">
								<div class="">
									<span class=""><label class="">
											Branch Display Name<span class="required">*</span>
										</label> <input type="text" name="bdName" id="bdName"
											class="form-control" /> </span>
								</div>
							</div>
						
							<div class="col-sm-4 col-md-4 hide" style="margin-left: 150px;">
								<label>
									Branch Contact 1<span class="required">*</span>
								</label>
								<input type="text" class="form-control" maxlength="30"
									id="contactNumber1" name="contactNumber1" />
							</div>
					</div>
					
					<div class="row">
							<div class="col-sm-4 col-md-4">
								<div class="">
									<span class=""><label class="">
											Address 1<span class="required">*</span>
										</label> <input type="text" name="branchAdress1" id="branchAdress1"
											class="form-control" /> </span>
								</div>
							</div>
						
							<div class="col-sm-4 col-md-4 hide" style="margin-left: 150px;">
								<label>
									Branch Contact 2
								</label>
								<input type="text" class="form-control" maxlength="30"
									id="contactNumber2" name="contactNumber2" />
							</div>
					</div>
					
					<div class="row">
							<div class="col-sm-4 col-md-4">
								<div class="">
									<span class=""><label class="">
											Address 2
										</label> <input type="text" name="branchAdress2" id="branchAdress2"
											class="form-control" /> </span>
								</div>
							</div>
					</div>
					
					<div class="row">
							<div class="col-sm-4 col-md-4">
								<div class="">
									<span class=""><label class="">
											State<span class="required">*</span>
										</label> <div id="stateDivId"></div> </span>
								</div>
							</div>													
					</div>
					
					<div class="row">
							<div class="col-sm-4 col-md-4">
								<div class="">
									<span class=""><label class="">
											City<span class="required">*</span>
										</label><select id="cityId" name="cityId" class="form-control" disabled="disabled">
														<option value="0" selected="selected">All</option>
												</select></span>
												

												</div>
							</div>													
					</div>
					
					<div class="row">
							<div class="col-sm-4 col-md-4">
								<div class="">
									<span class=""><label class="">
											Zip<span class="required">*</span>
										</label> <input type="text" name="zip" id="zip"	class="form-control" maxlength="6"/>
									</span>
								</div>
							</div>													
					</div>
					
					<div class="row">
							<div class="col-sm-4 col-md-4">
								<div class="">
									<span class=""><label class="">
											Phone Number<span class="required">*</span>
										</label> <input type="text" name="phoneNumber" id="phoneNumber"	class="form-control" maxlength="10"/> </span>
								</div>
							</div>													
					</div>
					
					<div class="row">
                     <div class="col-sm-3 col-md-3 mt10 left20"> 
	                        <div>
	                          <label class="radio inline" style="padding-left: 0px;">
								<input type="radio" name="selectDistrict" id="selectDistrict" value="1" onclick="uncheckedOtherRadio(4)"  />
	                            Selected Districts </label>
	                        </div>
	                 </div>    
                            <c:set var="showHideAddSchoolLink" value="hide"></c:set>		        			
			         			<c:set var="showHideAddSchoolLink" value=""></c:set>
			         		<div class="col-sm-6 col-md-6 left18 mt10">
				        		<div class="pull-right hide" id="addSchoolLink">
			                        <a href="javascript:void(0);" onclick="addDistict();">+ Add District</a>
	                             </div> 
			        		</div>
                        
                  </div>
                  <input type="hidden" id="gridNo" name="gridNo" value="">
                  <%-- ======== Branch District Grid ========= --%>
                 <div class="row col-sm-9 col-md-9">
                 	 <div  id="hqDistrict"  style="padding-bottom: 10px;" onclick="getHQBDGrid()">
                     </div>
                 </div>                   
                  <div id="addDistrictDiv" class="hide">
	                  <div class="row col-sm-10 col-md-10" style="max-width:755px;">
	                    <div>
	                      <label>District Name</label>
	             				<input type="text" id="districtName" name="districtName"  class="form-control"
			             		onfocus="getStateDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
								onkeyup="getStateDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
								onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
								<input type="hidden" id="districtId" value=""/>
							<div id='divTxtShowData' style='display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>	
							
	                    </div>
	                  </div>
	                  <div class="row col-sm-5 col-md-5">
	                  <div id="doneDiv">
                 		 <div class="span2 idone"><a href="javascript:void(0);" onclick="return addDistrict();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearHqBranchDistricts()">Cancel</a></div>
                	  </div>
                	  <div id="cancelDiv">
                 		 <div class="span2 idone"><a href="javascript:void(0);" onclick="clearHqBranchDistricts()">Cancel</a></div>
                	  </div>
                	  </div>
                  </div>
					<div style="clear: both;"></div>
					<div class="row">
							<div class="col-sm-4 col-md-4 top15">
								<div class="">
									 <button name="saveBranch" class="btn btn-primary" id="saveBranch" onclick="validateAddBranch();" >Save Branch</button>
									 &nbsp;&nbsp;<a href="javascript:void(0);" onclick="successRedirect();">Cancel</a><br>
								</div>
							</div>													
					</div>
					
    <div style="display:none;" id="loadingDiv">
	    <table  align="center">
	 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
	 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	</div>


<div  class="modal hide"  id="modalSuccessMsg"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="successRedirect();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='confirmDeactivate'>Branch has been added successfully.
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="successRedirect();">Ok</button> 		
 	</div>
</div> 	
</div>
</div>


<!-- END -->
<script>
//onLoadDisplayHideSearchBox(<c:out value="${userMaster.entityType}"/>);
//updateMsg('<c:out value="${authKeyVal}"/>',<c:out value="${userMaster.entityType}"/>);
	deleteTemp();
	displayState();
</script>

<input type="hidden" id="hqId" name="hqId" value="${param.headQId}"/>
