<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script type="text/javascript" src="dwr/interface/CGServiceAjax.js?ver=${resouceMap['CGServiceAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ExamAjax.js?ver=${resourceMap['ExamAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.Ajax']}"></script>
<!-- <script type='text/javascript' src="js/manageexamcode.js?ver=${resourceMap['js/managetags.js']}"></script>  -->

<!-- kendo css and js files -->
<link href="css/kendo.common.min.css" rel="stylesheet">
<link href="css/kendo.default.min.css" rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="js/kendo.timezones.min.js"></script>
<link href="css/kendo.rtl.min.css" rel="stylesheet" />
<link href="css/kendo.material.min.css" rel="stylesheet">  
<link href="css/kendo.dataviz.min.css" rel="stylesheet" />
<link href="css/kendo.dataviz.material.min.css" rel="stylesheet" />
<!-- kendo css and js files -->

<style type="text/css">
 .k-event{ 
		height: 21px !important;
  }
 .k-scheduler-footer,.k-view-workweek
  {
  display:none !important;
  }
.k-state-selected
{
      background-color: #ebebeb !important;
}
 .k-scheduler-content{
 height: 435px !important;
 } 
 
 td{
     text-align: center !important;
}
 
 #scheduler{
 height: 450px !important;
 margin-bottom: 40px;
 }
</style>
<style type="text/css">
.modalDialog {
		position: fixed;
		font-family: Arial, Helvetica, sans-serif;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		background: rgba(0,0,0,0.2);
		z-index: 99999;
		opacity:0;
		-webkit-transition: opacity 400ms ease-in;
		-moz-transition: opacity 400ms ease-in;
		transition: opacity 400ms ease-in;
		pointer-events: none;
	}
	
	.modalDialog:target {
		opacity:1;
		pointer-events: auto;
	}
	
	.tableborder
	{
		border: 1px solid #CCCCCC;
		font-family:Century Gothic,Open Sans,sans-serif;
		font-size: 11px;
		line-height: 18px;
		width: 100%;
		border-top-left-radius: 15px;
    	border-top-right-radius: 15px;
    }
</style>


<div style="display:none;" id="loadingDiv">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>

<!-- --------------------------------------------------------------- -->
<script>
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{	
	if(pageno!='')
	{
		page=pageno;	
	}else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	var entityID	=	document.getElementById("entityID").value;
	if(entityID==1){
		var searchTextId	=	document.getElementById("districtIdFilter").value;
	}	
	displayExamCodes();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	var entityID	=	document.getElementById("entityID").value;
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	if(entityID==1){
		var searchTextId	=	document.getElementById("districtIdFilter").value;
	}
	displayExamCodes123("","-1","-1");
}

function chkForEnterSearchTags(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		page = 1;
		displayExamCodes();
	}	
}
function chkForEnterSaveTags(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		saveAndUploadTagsIcon();
	}	
}

/*
function displayExamCodes()
{	
	var entityID = document.getElementById("entityID").value;
	var districtIdFilter=0;
	if(entityID==1)
	{
		districtIdFilter = document.getElementById("districtIdFilter").value;
	}	
	
	var startDate= "";
	var endDate = "";
	var firstName = "";
	var lastName = "";
	var emailAddress = "";
	var examCode = "";
	
	ExamAjax.displayData(entityID,districtIdFilter,noOfRows,page,sortOrderStr,sortOrderType,startDate,endDate,firstName,lastName,emailAddress,examCode,"",{ 
		async: true,
		callback: function(data){
		$('#examCodesGrid').html(data);
		applyScrollOnTbl();
	},
	});
}
*/

function displayExamCodes(teachersIdStr,startDateStr,endDateStr)
{
	var entityID = document.getElementById("entityID").value;
	var districtIdFilter=0;
	if(entityID==1)
	{
		districtIdFilter = document.getElementById("districtIdFilter").value;
	}	
	
	var startDate= "-1";
	var endDate = "-1";
	var firstName = "";
	var lastName = "";
	var emailAddress = "";
	var examCode = "";
	var teachersId = "";
	
	if(startDateStr=="-1")
		startDate = startDateStr;
	else
	if($("#examDate").val() !=null)
		startDate= $("#examDate").val();
	
	if(endDateStr=="-1")
		endDate = endDateStr;
	
	if($("#examCode").val() !=null)
		examCode = $("#examCode").val();
	
	if(teachersIdStr==null || teachersIdStr=="") //get Data from teacherIds input type;
		teachersIdStr = $("#teacherIds").val();
	
	if(teachersIdStr!=null && teachersIdStr!="")
	{
		$("#teacherIds").val(teachersIdStr);
		teachersId = teachersIdStr;
	}
	
	//alert("startDate:- "+startDate+"\nendDate:- "+endDate+"\nfirstName:- "+firstName+"\nlastName:- "+lastName+"\nemailAddress:- "+emailAddress+"\nexamCode:- "+examCode+"\nteacherIds:- "+teachersId);
	ExamAjax.displayData(entityID,districtIdFilter,noOfRows,page,sortOrderStr,sortOrderType,startDate,endDate,firstName,lastName,emailAddress,examCode,teachersId,{ 
		async: true,
		callback: function(data){
		$('#examCodesGrid').html(data);
		var win = $("#kendoWindow").data("kendoWindow");
		win.open();
		},
	});
}

function scheduler_save(e) {
	//alert('scheduler_save::::');
}

function scheduler_add (e) {
	//alert('scheduler_add::::');
}

function objToString (obj)
{
	var str = '';
	for (var p in obj) {
		if (obj.hasOwnProperty(p)) {
			str += p + '::' + obj[p] + '\n';
		}
	}
	return str;
}

var event = "";
function scheduler_navigate(e)
{
	if(e.action=="next" || e.action=="changeDate" || e.action=="previous" || e.action=="today")
	{
		var mondayDate = getMonday(e.date);
		var mydate = (mondayDate.getMonth()+1)+"-"+mondayDate.getDate()+"-"+mondayDate.getFullYear();
		$("#examDate").val(mydate);
		displayGridWithAssesmentTime();
		event = ""; 
	}
	
	if(e.action="workView")
	{
		event = "";
	}
}

function getMonday(d)
{
	d = new Date(d);
	var day = d.getDay(),
	diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
	return new Date(d.setDate(diff));
}

function scheduler_change(e)
{
	var events = e.events; //list of selected Scheduler events
	var centreScheduleId = 0;
	try{
		centreScheduleId =  events[events.length - 1].centerId;
	}
	catch(e){}
	//alert(" centreScheduleId ::: "+centreScheduleId);
	//alert(" event :: "+event);
	if((centreScheduleId!=null && centreScheduleId > 0 &&  event!="workView" ))
	{
		$('#loadingDiv').show();
		//var dt = e.start;
		//alert(" centreScheduleId ::: "+centreScheduleId);
		//var id = parseInt(centreScheduleId);
		//var dateTime = ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + ("0" + (dt.getDate())).slice(-2) + '/' +  dt.getFullYear()+" "+dt.getHours()+":"+addZero(dt.getMinutes());
		CGServiceAjax.getAvailability(centreScheduleId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data.length > 0)
			{			
				$('#availabilityGrid').html(data);
				/* overflow: auto; */
				var win = $("#availableKendoWindow").data("kendoWindow");
				win.open();
				$('#loadingDiv').hide();
				document.getElementById('previousPriority').value = $("#priorityIds option:selected").val();
			}
			else
			{
				$('#loadingDiv').hide();
			}
			}
		});
	}
	else
	{
		var teacherValues=events[events.length - 1].tIds;
		if(teacherValues!=null && teacherValues!="" && event!="workView")
		{
			displayExamCodes(teacherValues,'-1','-1');
			$("#teacherIds").val(teacherValues);
		}
	}
	
}

function displayGridWithAssesmentTime()
{

	$('#loadingDiv').show();
	var examDate = "";
	var examCode = "";
	
	if($("#examDate").val()!=null)
		examDate = $("#examDate").val();
	if($("#examCode").val()!=null)
		examCode = $("#examCode").val();
	
	//destroy the kendo Schedule if it is created.
	$("#scheduler").empty();
	var scheduler = $("#scheduler").data("kendoScheduler");
	if(scheduler!=null)
		scheduler.destroy();
	
	var current = new Date();     // get current date
	if( !(examDate==null || examDate=="") )	//examdate Format:- mm-dd-yy
	{
		current = new Date();
		var dateInArray = examDate.split("-")
		current.setDate(dateInArray[1]);
		current.setMonth(dateInArray[0]-1);
		current.setFullYear(dateInArray[2]);
	}
	
	var weekstart = current.getDate() - current.getDay() +1;
	//var weekend = weekstart + 4;       // end day is the first day + 6 
	var monday = new Date(current.setDate(weekstart));
	//var friday = new Date(current.setDate(weekend));
	
	var myDate = new Date();
	//if(examDate!="" && examDate!=null)
	//	myDate = new Date(examDate);
	
	ExamAjax.displayTeacherAssesmentTime(examDate,examCode,{ 
		async: false,
		callback: function(data){
			var bookings = [{field: "centerId",dataSource: data[1]}];
			//alert(JSON.stringify(data[1]));
			$("#scheduler").kendoScheduler({
			//timezone: "Central Daylight Time",
			//currentTimeMarker: {useLocalTimezone: false},
			date:monday,
			allDaySlot: false,
			showWorkHours:true,
			height: 400,
			//timezone: "CST",
			//majorTick: 60,
			views: [{type: "workWeek", selected: true}],
			selectable: true,
			editable: false,
			navigate:scheduler_navigate,
			save: scheduler_save,
			add: scheduler_add,
			change: scheduler_change,			
			dataSource: data[0],
			resources: bookings
		});
		
		$('#loadingDiv').hide();
	},
	});
}


/********************************** district auto completer ***********************************/

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
var districtNameFilter=0;
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		if(districtNameFilter==1)
			document.getElementById("districtNameFilter").focus();
		else
			document.getElementById("districtName").focus();
		
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	ManageJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}

function showConfirmationpopup(codeId,plock_unlock)
{
	var kwin = $("#kendoWindow").data("kendoWindow");
	var showpopupWindow = $("#showpopupKendoWindow").data("kendoWindow");
	
	kwin.close();	//default close window.
	
	$('#withoutPermissionClose').hide();
	$('#withoutPermissionCancel').hide();
	$('#withoutPermissionOk').hide();
	$('#withoutPermissionOk1').hide();
	$('#withoutPermissionOk2').hide();
	
	//add By Ram Nath
	if(plock_unlock==1){
		//end By Ram Nath
		$('#withoutPermission').hide();
		$('#withoutPermissionOk').hide();
		$('#withoutPermissionClose').hide();
		
	//$("#examCodesGridpopup").modal("hide");
	$("#codeId").attr("value",codeId);
	//$("#showpopup").modal("show");
	
	var value = $("#"+codeId).attr("name");
	if(value=="Approve")
	{
		$("#confirmationMessage1").show();
		$("#confirmationMessage2").hide();
		
		$('#withoutPermissionOk2').show();
		$('#withoutPermissionCancel').show();
		$('#withoutPermissionOk1').hide();
		$('#withoutPermissionOk').hide();
		
		
	}
	else
	{
		$("#confirmationMessage1").hide();
		$("#confirmationMessage2").show();
		
		$('#withoutPermissionOk1').show();
		$('#withoutPermissionCancel').show();
		$('#withoutPermissionOk').hide();
		$('#withoutPermissionOk2').hide();
	}
	}
	//add By Ram nath
	else{
		
		//showpopupWindow.open();
		
		$('#withoutPermissionOk1').hide();
		$('#withoutPermissionOk2').hide();
		$('#withoutPermissionCancel').hide();
		$('#withoutPermissionClose').hide();
		
		
		$("#confirmationMessage1").hide();
		$("#confirmationMessage2").hide();
		//$("#examCodesGridpopup").modal("hide");
		//$("#showpopup").modal("show");
		
		$('#withoutPermission').show();
		$('#withoutPermissionClose').show();
		
/*		$('#showpopup #withoutPermission').show();
		$('#showpopup .withoutPermission').show();
*/
	}
	showpopupWindow.open();
	//End By Ram Nath
}

function confirmationOk()
{
	var showpopupWindow = $("#showpopupKendoWindow").data("kendoWindow");
	showpopupWindow.close();
	//$("#showpopup").modal("hide");
	
	var codeId = $("#codeId").val();
	//$("#loadingDiv").modal("show");
	
	ExamAjax.ApproveAndDisapprove(codeId,{
		async: true,
		callback: function(data){
				var value = $("#"+codeId).attr("name");
				if(value=="Approve")
				{
					$("#"+codeId).attr("name","Disapprove");
					document.getElementById(""+codeId).innerHTML="Lock";
				}
				else
				{
					$("#"+codeId).attr("name","Approve");
					document.getElementById(""+codeId).innerHTML="Unlock";
				}
				
				//refresh the kendow window
				var tID = $("#teacherIds").val();
				var sDate = $("#examDate").val();
				var eDate = "-1";
				displayExamCodes(tID,sDate,eDate);
				
				//$("#examCodesGridpopup").modal("show");
			},
	});
}

function confirmationClose()
{
	var kwin = $("#kendoWindow").data("kendoWindow");
	var showpopupWindow = $("#showpopupKendoWindow").data("kendoWindow");
	
	
	showpopupWindow.close();
	kwin.open();
	
	//$("#showpopup").modal("hide");
	//$("#examCodesGridpopup").modal("show");
}

function confirmationGridClose()
{
	var kwin = $("#kendoWindow").data("kendoWindow");
	var showpopupWindow = $("#showpopupKendoWindow").data("kendoWindow");
	
	showpopupWindow.close();
	kwin.close();
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function closeAvailabilityKendo()
{
	document.getElementById('overrideFlag').value="";
	var availabilityWindow = $("#availableKendoWindow").data("kendoWindow");
	availabilityWindow.close();	
}
function setAvailabilityFlag(flag)
{
	document.getElementById('availFlag').value=flag;
}
function disabledPriorityList(val)
{
	if(val==0)
	{
		document.getElementById('priorityIds').disabled = true;
	}
	else
	{
		document.getElementById('priorityIds').disabled = false;
	}
}
function setlockUnlockFlag(flagVal)
{
	document.getElementById('lockUnlock').value = flagVal;
}

function setPriorityFlag()
{
	document.getElementById('priorityFlag').value=$("#priorityIds option:selected").val();
}
function saveDAAvailability()
{
	var errorCount = 0;
	var fromTime = 0;
	var toTime = 0;
	var dateTime = document.getElementById('dateTime').value;	
	var availabilityFlag = document.getElementById('priorityIds').value;

	fromTime = parseInt(document.getElementById('fromTime').value);

	toTime =  parseInt(document.getElementById('toTime').value);

	var overrideFlag = document.getElementById('overrideFlag').value;
	
	var lockVal = document.getElementById('lockUnlockFlag').value;
	if(lockVal=="")
	{
		lockVal = document.getElementById('lockUnlock').value;
	}
	
	//return false;
	$('#errorDiv').empty();
	if(availabilityFlag=="")
	{
		availabilityFlag = 1;
	}

	//if(overrideFlag!="")
	//{
		//fromTime=0;
		//toTime = 1;
	//}
	
	//return false;
	var errorId = "";
	if($("#priorityIds option:selected").val()==-1)
	{
		errorCount++;
		$('#errorDiv').show();
		$('#errorDiv').append("&#149; Please select priority.<br>");	
		if(errorCount==1)
			$('#priorityIds').focus();
		
	}
	if(fromTime=="-1")
	{
		errorCount++;
		$('#errorDiv').show();
		$('#errorDiv').append("&#149; Please select start time.<br>");
		if(errorCount==1)
			$('#fromTime').focus();		
	}
	if(toTime=="-1")
	{
		errorCount++;
		$('#errorDiv').show();
		$('#errorDiv').append("&#149; Please select end time.<br>");
		
		if(errorCount==1)
			$('#toTime').focus();
		
	}
	
	
	if(fromTime > toTime && toTime!="-1")
	{
		$('#errorDiv').show();
		$('#errorDiv').append("&#149; End time should not be less than start time.<br>");
		$('#fromTime').focus();
		errorCount++;
	}
	else if(fromTime!="-1" && toTime!="-1" &&  fromTime == toTime)
	{
		$('#errorDiv').show();
		$('#errorDiv').append("&#149; End time should not be equal to start time.<br>");
		$('#fromTime').focus();
		errorCount++;
	}
	if(overrideFlag.length>0 || availabilityFlag.length>0)
	{
		
	}
	if(errorCount==0)
	{	
		ExamAjax.saveDaAvailabilities(dateTime,availabilityFlag,fromTime,toTime,overrideFlag,lockVal,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			resetSlot();
			var win = $("#availableKendoWindow").data("kendoWindow");
			win.close();
			
			displayGridWithAssesmentTime();
		}
		});
	}
}
function resetSlot(){
	document.getElementById('lockUnlockFlag').value="";
	document.getElementById('lockUnlock').value="";
	document.getElementById('fromTime').value="";
	document.getElementById('toTime').value="";
	document.getElementById('overrideFlag').value="";
	document.getElementById('priorityIds').value='-1';
}

function forcelyChange(date)
{
 	var priority = $("#priorityIds option:selected").html();
 	var priorityId =  $("#priorityIds option:selected").val();
 	if(priorityId!="-1")
 	{
		document.getElementById('priorityFlag').value = priorityId;
		document.getElementById('overrideFlag').value = priorityId;
		var win = $("#prioMSGKendoWindow").data("kendoWindow");
		win.open();
		document.getElementById('changePriorDate').innerHTML=date;
		document.getElementById('changePriorValue').innerHTML=priority;
		//document.getElementById('lckbtn').style.display="none";
		document.getElementById('lockUnlock1').disabled=true;
	}
	else
	{
		document.getElementById('fromTime').disabled=true;
		document.getElementById('toTime').disabled=true;
	}
	//$('input:radio[id=lockUnlock]')[1].attr('disabled', true);
}

function showlockBtn()
{
	var win = $("#prioMSGKendoWindow").data("kendoWindow");
	win.close();
	//document.getElementById('cnclbtn').style.display="none";
	//document.getElementById('lckbtn').style.display="block";
	//document.getElementById('fromTime').disabled=true;
	//document.getElementById('toTime').disabled=true;
	 
}

function closePrioInfo()
{
	var flagValue = document.getElementById('overrideFlag').value;	
	document.getElementById('priorityFlag').value=flagValue;
	document.getElementById('overrideFlag').value="";
	document.getElementById('fromTime').disabled=false;
	document.getElementById('toTime').disabled=false;
	document.getElementById('lockUnlock1').disabled=false;
	//alert("previous flag ::"+document.getElementById('previousPriority').value);
	document.getElementById("priorityIds").value = document.getElementById('previousPriority').value;
	var win = $("#prioMSGKendoWindow").data("kendoWindow");
	win.close();
	
}
</script>
<!-- --------------------------------------------------------------- -->

<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lblManageAssessmentCode1"/></div>	
         </div>
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
	<div  onkeypress="return chkForEnterSearchTags(event);">
		<form class="bs-docs-example" onsubmit="return false;">
			<c:if test="${entityID eq 1}">
				<div class="col-sm-4 col-md-4">
					<label id="captionDistrictOrSchool"><spring:message code="optDistrict"/></label>
					<input type="text" id="districtNameFilter" name="districtNameFilter" class="form-control"
						onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
						onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
						onblur="hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');"	/>
					<input type="hidden" id="districtIdFilter" value=""/>
					
					<div id='divTxtShowDataFilter'  onmouseover="mouseOverChk('divTxtShowDataFilter','districtNameFilter')"
						style=' display:none;position:absolute;z-index: 5000' class='result' >
					</div>
				</div>
			</c:if>
			
			<div class="row top10" style="margin-bottom: 25px;">
				<div class="col-sm-3 col-md-3" id='jaFrom'>
					<label><spring:message code="msgAssessmentDate"/></label>
					<input type='text' id='examDate' name='examDate' class='help-inline form-control DynarchCalendar-time-hour'>
				</div>
				
				<div class="col-sm-3 col-md-3">
					<label><spring:message code="lblAssessmentCode"/></label>
					<input type='text' id='examCode' name='examCode' class='help-inline form-control'>
				</div>
				
				<div class="col-sm-3 col-md-3">
					<label>&nbsp;</label>
					 <!-- <button class="btn btn-primary  top25-sm2" type="button" onclick="temp()">Search <i class="icon"></i></button> -->
					 <button class="btn btn-primary  top25-sm2" type="button" onclick="displayGridWithAssesmentTime()"><spring:message code="btnSearch"/> <i class="icon"></i></button>
				</div>
			</div>
		</form>
	</div>
</div>

<input type="hidden" id="entityID" value="${entityID}"/>
<div style="display:none;" id="loadingDiv">
	<table  align="center" >
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='' id='spnMpro' align='center'><spring:message code="msgYrFileBegUpLod"/></td></tr>
 	</table>
</div>

<input type="hidden" id="codeId" name="codeId" value="">
<input type="hidden" id="teacherIds" name="teacherIds" value="">
<div id="kendoWindow">
	<div class="table-responsive" id="examCodesGrid" style="text-align:center"> </div>
	<br><br>
	<button class="btn withoutPermission" onclick="confirmationGridClose();" style="float: right;margin-right: 10px;" ><spring:message code="btnClose"/></button>
</div>


<div id="showpopupKendoWindow">
	<div id="confirmationMessage1" style="padding-left: 3%;"> <spring:message code="msgDoyouwant1"/> </div>
	<div id="confirmationMessage2" style="padding-left: 3%;"> <spring:message code="msgDoyouwant2"/> </div>
	<div id="withoutPermission" style="padding-left: 3%;display:none;"><spring:message code="msgDoyouwant3"/></div>
	<br><br>
	
	<button type="button" class="btn btn-default" id="withoutPermissionClose" onclick="confirmationClose()" style="float: right;margin-right: 10px;"><spring:message code="btnClose"/></button>
	<button type="button" class="btn btn-default" id="withoutPermissionCancel" onclick="confirmationClose()" style="float: right;margin-right: 10px;"><spring:message code="lnkCancel"/></button>
	<button class="btn btn-primary withoutPermission" id="withoutPermissionOk" onclick="confirmationClose();" style="display:none;float: right;margin-right: 10px;" ><spring:message code="btnOk"/></button>
	<button class="btn btn-primary" id="withoutPermissionOk1" onclick="confirmationOk()" style="float: right;margin-right: 10px;"><spring:message code="msgLock"/></button>
	<button class="btn btn-primary" id="withoutPermissionOk2" onclick="confirmationOk()" style="float: right;margin-right: 10px;"><spring:message code="msgUnlock"/></button>
</div>

<div id="example">
	<div id="scheduler"></div>
</div>

<!-- This script(event-template) is used as a template in scheduler. -->
 
<!--<script id="event-template" type="text/x-kendo-template">
	<div class="movie-template">
		<a href="#= linkValue #" onclick="#= functionValue #"> #: title # </a>
	</div>
</script>
-->

<!--
<script id="editor" type="text/x-kendo-template">

   <div style="padding-left:50px;width:250px;" id="myGridData">
       hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh
   
   </div>
</script>
-->

<div id="availableKendoWindow">
	<div id="availabilityGrid"></div>
</div>

<script type="text/javascript">
	var cal = Calendar.setup({
				onSelect: function(cal) { cal.hide() },
				showTime: true
			  });
		cal.manageFields("examDate", "examDate", "%m-%d-%Y");
</script>




<script type="text/javascript">

	displayGridWithAssesmentTime();
	//displayExamCodes();
</script>
<style type="text/css">
	#availableKendoWindow{overflow:hidden !important;}
</style>
<input type='hidden' name='availFlag' id='availFlag'/>
<input type='hidden' name='priorityFlag' id='priorityFlag'/>
<input type='hidden' name='overrideFlag' id='overrideFlag'/>
<input type='hidden' name='previousPriority' id='previousPriority'/>
<input type='hidden' name='lockUnlockFlag' id='lockUnlockFlag'/>
<div style="clear:both;"></div>
<div  id="prioMSGKendoWindow" style="padding-left:20px;padding-top:20px;">
	This action will set all your slots for <span id="changePriorDate"></span> with <span id="changePriorValue"></span>.
<br/>
<div class="col-sm-6 col-md-6 top10" style="float:right;padding-bottom:0px;padding-right:0px;"><button class="btn btn-primary" onclick="showlockBtn();">Continue</button>&nbsp;&nbsp;&nbsp;<button class="btn btn primary" onclick="closePrioInfo();">Close</button></div>

</div>

<script>
$("#kendoWindow").kendoWindow({
  	//actions: [ "Minimize", "Maximize","Close" ]
  	position: { top: 2,left:"5%",right:"5%"},
  	actions: ["Close"],
	modal:true,
	autoFocus: true,
	draggable: false,
	maxHeight: 600,
	//minHeight: 600,
	//maxWidth: 1200,
	//minWidth: "80%",//1200,
	width: "90%",//1200,
	pinned: true,
	resizable: false,
	title: true,
	title: "Assessment Details",
});

$("#showpopupKendoWindow").kendoWindow({
  	//actions: [ "Minimize", "Maximize","Close" ]
  	//position: { top: 2,left: "40%"},
  	actions: ["Close"],
	modal:true,
	autoFocus: true,
	draggable: false,
	//maxHeight: 600,
	minHeight: 100,
	//maxWidth: 1200,
	minWidth: 350,
	pinned: true,
	resizable: false,
	title: true,
	title: "Assessment Details",
});

$("#availableKendoWindow").kendoWindow({
  	position: { top: "20%",left:"35%",right:"30%"},
  	actions: ["Close"],
	modal:true,
	autoFocus: true,
	draggable: false,
	maxHeight: 600,
	minHeight: 120,
	maxWidth: 550,
	minWidth: 330,
	pinned: true,
	resizable: false,
	title: true,
	title: "Select Availability",
});

$("#prioMSGKendoWindow").kendoWindow({
  	position: { top: "30%",left:"35%",right:"30%"},
  	actions: ["Close"],
	modal:true,
	autoFocus: true,
	draggable: false,
	maxHeight: 200,
	minHeight: 105,
	maxWidth: 350,
	minWidth: 300,
	pinned: true,
	resizable: false,
	title: true,
	title: "Priority Info",
});
//$("#kendoWindow").data("kendoWindow").center();	//set default position to center
$("#showpopupKendoWindow").data("kendoWindow").center();	//set default position to center

$("#kendoWindow").data("kendoWindow").close();	//default close window.
$("#showpopupKendoWindow").data("kendoWindow").close();	//default close window.

$("#availableKendoWindow").data("kendoWindow").close();	//default close window.

$("#prioMSGKendoWindow").data("kendoWindow").close();
</script>