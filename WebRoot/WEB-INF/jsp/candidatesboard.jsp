<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>


<script type="text/javascript" src="dwr/interface/AdminDashboardAjax.js?ver=${resourceMap['AdminDashboardAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/admindashboard.js?ver=${resourceMap['js/admindashboard.js']}"></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        //width: 840,
         width: 945,
        minWidth: null,
        minWidthAuto: false,
        //colratio:[24,210,263,115,100,100,170],
        <c:choose>
			<c:when test="${param.r eq 2}"> 
				colratio:[24,200,283,115,100,130,90],
			</c:when>
			<c:otherwise>			
				colratio:[24,220,313,115,100,170],
			</c:otherwise>
		</c:choose>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

 </script>
 <div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">
         	    <c:set var="cType" value="Native" />
				<c:choose>
					<c:when test="${param.q eq 'n'}"> 
						<c:set var="cType" value="Native" />
					</c:when>
					<c:otherwise>			
					<c:set var="cType" value="Referral" />
					</c:otherwise>
				</c:choose>
				
				<c:choose>
					<c:when test="${param.r eq 1}"> 
						<div class="span10 subheading"><spring:message code="headAuthEpiNotComp"/>(${cType})</div>
					</c:when>
					<c:when test="${param.r eq 0}"> 
						<div class="span10 subheading"><spring:message code="headUnAuth"/> (${cType})</div>
					</c:when>
					<c:otherwise>			
						<div class="span10 subheading"><spring:message code="headAuthEPIOut"/> (${cType})</div>
					</c:otherwise>
				</c:choose>
         	</div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
			<div  onkeypress="return chkForEnterSearchTeacher(event);">
				<form class="bs-docs-example" onclick="return false;" >
			      <div class="row top10">
				       <div class="col-sm-3 col-md-3">
				       <span class=""><label class=""><spring:message code="lblFname"/></label><input type="text" id="firstName" name="firstName" maxlength="50" class="help-inline form-control"></span>
				       </div>
				       <div class="col-sm-3 col-md-3">
					       <div class="">
					       	<span class=""><label class=""><spring:message code="lblLname"/></label><input type="text" id="lastName" name="lastName" maxlength="50" class="help-inline form-control"></span>
					       </div>
				       </div>
				       
				      <div class="col-sm-3 col-md-3">
					       <span><label class=""><spring:message code="lblEmailAddress"/></label><input type="text" id="emailAddress" name="emailAddress" maxlength="75" class="form-control fl"></span>					      
				      </div>
				      <div class="col-sm-2 col-md-2">
				       <button class="btn btn-primary top25-sm" type="button" onclick="searchTeacher()"><spring:message code="btnSearch"/> <i class="icon"></i></button>
				      </div>
			      </div>
				</form>
			</div>            
		
			<input type="hidden" id="p" value="${param.p}">
			<input type="hidden" id="q" value="${param.q}">
			<input type="hidden" id="r" value="${param.r}">
			<input type="hidden" id="messageId" value="${messageId}">

			<div class="row top15">
				<div class="col-sm-9 col-md-9 top15-sm">
				<a  href="javascript:void(0);" onclick='checkAll(true);'><spring:message code="lnkChAll"/></a>&nbsp;|&nbsp;<a href="javascript:void(0);" onclick='checkAll(false);'><spring:message code="lnkUnChAll"/></a>
				&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="getDefaultMsg();"><spring:message code="lnkSetDefMsg"/></a>
				</div>
				
				<div class="col-sm-3 col-md-3">				
					<div class="left70" style="min-width: 200px;">
					<button class="btn btn-primary fl" onclick="sendMessages();" type="button"><spring:message code="btnSendMsg"/> <i class="icon"></i></button>
					</div>
				</div>
			</div>
			
			<div class="TableContent top15">        	
			            <div class="table-responsive" id="divMain">          
			                	         
			            </div>            
			</div> 
			
			
<script type="text/javascript">
getApplicants();
</script>
<div class="modal hide"id="myModal1setMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog-for-cgreference">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="lblAdd/EditMsg"/></h3>
	</div>
	<div  class="modal-body">
		<div class="control-group">
			<div class='divErrorMsg' id='errordivDefaultMsg' style="display: none;"></div>
		</div>
		<div class="control-group">
			<div class="">
		    	<label><strong><spring:message code="lblSub"/></strong><span class="required">*</span></label>
	        	<input id="defaultSubject" name="defaultSubject" type="text" class="form-control" maxlength="200" />
			</div>
		</div>
        <div class="control-group">
			<div class="span8" id="msgDash">
		    	<label><strong><spring:message code="lblMsg"/></strong><span class="required">*</span></label>
		    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>
	        	<textarea rows="5" class="form-control" cols="" id="defaultMsg" name="defaultMsg" maxlength="1000"></textarea>
			</div>
		</div>
 		<div id="lodingImg2" style='display:none;text-align:center;padding-top:4px;'><img src="images/loadingAnimation.gif" /> <spring:message code="msgMsgSending"/></div>
 		
 	</div>
 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>
 		<button class="btn btn-primary" onclick="saveDefaultMsg()" ><spring:message code="btnSave"/></button>
 	</div>
</div>
</div>
</div>

<div style="display:none;" id="loadingDiv">
   <table  align="center">
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		<tr id='paymentMessage'><td style='padding-top:0px;' align='center'><spring:message code="msgMsgBngSending"/></td></tr>
</table>
</div>


 