<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">

<script type="text/javascript">

$(document).ready(function(){
	
	$("#newsDiv").hide();
	$("#defaultDiv").hide();
	$("#questhighDiv").hide();
	$("#questmiddleDiv").hide();
	$("#questElementoryDiv").hide();
	$("#interviewpage").hide();
	
	$("#defaultDiv").show();
	
	$("#associationNews").click(function(){
		
		$("#newsDiv").hide();
		$("#defaultDiv").hide();
		$("#questhighDiv").hide();
		$("#questmiddleDiv").hide();
		$("#questElementoryDiv").hide();
		$("#interviewpage").hide();
		
		$("#newsDiv").show();
	});
	
	$("#elementaryTeacherResources").click(function(){
		
		$("#newsDiv").hide();
		$("#defaultDiv").hide();
		$("#questhighDiv").hide();
		$("#questmiddleDiv").hide();
		$("#questElementoryDiv").hide();
		$("#interviewpage").hide();
		
		$("#questElementoryDiv").show();
	});
	
	$("#middleSchoolTeacherResources").click(function(){
		
		$("#newsDiv").hide();
		$("#defaultDiv").hide();
		$("#questhighDiv").hide();
		$("#questmiddleDiv").hide();
		$("#questElementoryDiv").hide();
		$("#interviewpage").hide();
		
		$("#questmiddleDiv").show();
	});
	
	$("#highSchoolTeacherResources").click(function(){
		
		$("#newsDiv").hide();
		$("#defaultDiv").hide();
		$("#questhighDiv").hide();
		$("#questmiddleDiv").hide();
		$("#questElementoryDiv").hide();
		$("#interviewpage").hide();
		
		$("#questhighDiv").show();
	});
	
	$("#exclusiveinterview").click(function(){
		
		$("#mainpage").hide();
		$("#interviewpage").show();
		$("#back").focus();
		
	});
	
	$("#back").click(function(){
		
		$("#interviewpage").hide();
		$("#mainpage").show();			
	});
});

</script>

<style type="text/css">

.contentDiv
{
	text-align: left;
	min-height: 135px;
	display: inline-block;
	min-width: 219px;
	/*margin-left: -4%;*/
}

.imageDiv
{
	margin:auto;
	min-width: 200px;
	min-height: 200px;
	text-align:center;
}

.imageDiv .imagetext
{
	display: inline-block;
	margin:auto;
	max-width: 220px;
	max-height: 250px;
	padding: 2px;
}

.imageDiv .imagetext img
{
	width: 100%;
	height: 150px;
}

.imageDiv .imagetext .contentDiv
{
	width: 250px;
	height: 90px;
}

.bluetext
{
	color:  #007ab4;
	font-weight: 700;
	text-align: left;
	cursor: pointer;
}

.bottomDiv
{
	background-color: #eaedee;
	width: 100%;
	padding-top: 10px;	
	height: auto;
	padding-bottom: 10px;
	
}

.bottomInnerDiv
{
	width: 100%;	
	height: 100%;
	padding-top:20px;
	padding-bottom:10px;
	padding-right:10px;
	margin-left:30px;
}

.news
{
	min-height:720px;
	max-height:720px;
	width:50%;
	overflow:hidden;	
}
.news1
{
	float:left;
	
	min-width:50%;
	min-height:200px;
	height:200px;	
	background-color: #0094DA;
	display:block;
	color:  #ffffff;
	text-align:center;
	font-weight: bold;
	overflow:hidden;	
	z-index:2;
	position: relative;
}

.news2
{
	width:100%;
	max-height: 350px;
	background-color:#ffffff;
	margin-right:-10px;
	float:left;
	color:#4d4d4f;	
	overflow-x: hidden;
	overflow-y: scroll;
	padding:10px;
	text-align:left;
	z-index:2;
	position: relative;
}

.news3
{
	text-align:center;
	min-width:85%;
	max-width:85%;
	/*height:100%;*/
	background-color: inherit;
	display:block;
	/*color:  #ffffff;*/
	font-weight: bold;
	overflow:hidden;
	
}

.news4
{
	height:324px;
	width:92%;
	overflow-y:scroll;
	overflow-x:hidden;
	background-color: #ffffff;
}

.row1
{
	background-color:#D2D5D7;
	width: 100%;
	color:  #007ab4;
	font-weight: 700;
	text-align: left;
	padding-left: 10px
}

.row2
{
	bgcolor:#ffffff;
	width: 100%;
	color:  #007ab4;
	font-weight: 700;
	text-align: left;
	padding-left: 10px
}

.rowheading
{
	bgcolor:#ffffff;
	width: 100%;
	font-weight: bold;
	text-align: left;
	padding-left: 10px
}

.top-left-corner-radius
{
	border-top-left-radius: 10px;
}

.top-right-corner-radius
{
	border-top-right-radius: 10px;
}

.bottom-left-corner-radius
{
	border-bottom-left-radius: 10px;
}

.bottom-right-corner-radius
{
	border-bottom-right-radius: 10px;
}

.heading1
{
	font-size:25px;
}

.heading2
{
	text-align:left;
	margin-top: -20px;
	color: inherit;
	font-weight: normal;
}

.heading3
{
	font-size:18px;
	font-weight:700;
}

</style>
<div class="container" style="font-size:12px">
<div id="mainpage">
	<div align="center">
		<p style="color:#007ab4; font-size: 25px; font-weight: 400; line-height: 25px;width: 255px;height: 26px; margin-top: 22px;">
			<spring:message code="lblQuestCot" /><br>
			<img src="images/questconnect.png" style="margin-top:-35px;" class="img-responsive"> <br>
		</p> <br><br><br>
		<p style="font-size:13px;">
			<spring:message code="msgQuestConn" /><br>
			<spring:message code="msgQuestConn2" />
		</p>
	</div>
	
	<div class="imageDiv">
		<div class="imagetext" id="associationNews"> <img src="images/information.png" class="img-responsive" style="cursor: pointer; margin-bottom:5px;"> <p class="bluetext" style="font-size: 12px;text-align:center"> <spring:message code="msgAssociationNews" /> </p> </div>
		<div class="imagetext" id="elementaryTeacherResources"> <img src="images/elementaryTeacherResources.png" class="img-responsive" style="cursor: pointer; margin-bottom:5px;"> <p class="bluetext" style="font-size: 12px;text-align:center"> <spring:message code="msgElementaryResource" /> </p> </div>
		<div class="imagetext" id="middleSchoolTeacherResources"> <img src="images/middleSchoolTeacherResources.png" class="img-responsive" style="cursor: pointer; margin-bottom:5px;"> <p class="bluetext" style="font-size: 12px;text-align:center"> <spring:message code="msgMiddleSchoolResource" /> </p> </div>
		<div class="imagetext" id="highSchoolTeacherResources"> <img src="images/highSchoolteacherResource.png" class="img-responsive" style="cursor: pointer; margin-bottom:5px;"> <p class="bluetext" style="font-size: 12px;text-align:center"> <spring:message code="msgHighSchoolResource" /> </p> </div>
	</div>

<div class="bottomDiv">

<!-- -----------------------------------Start of default Div-------------------------------- -->

	<div id="defaultDiv" class="bottomInnerDiv" style="width:100%;margin-left:4%;">
		<div class="contentDiv">
			<p>
			<spring:message code="msgQuestConn3" />
			</p>
		</div>
		
		<div class="contentDiv">
			<p>
				<spring:message code="msgQuestConn4" />
			</p>
		</div>
		
		<div class="contentDiv">
			<p>
				<spring:message code="msgQuestConn5" />
			</p>
		</div>
		
		<div class="contentDiv">
			<p>
				<spring:message code="msgQuestConn6" />
			</p>
		</div>
	</div>

<!-- -------------------------------------End of default Div-------------------------------- -->
	
	
<!-- ---------------------------------------Start of news Div------------------------------ -->
		<div class="bottomInnerDiv" style="padding-left:20px; text-align: center;" id="newsDiv">
			<p class="heading1" align="center"> <spring:message code="msgAssociationNews" /> </p><br>
			<p class="heading2"> <spring:message code="msgQuestConn3_1" /> </p>			
			
			<div class="row news" style="float:left;">
				<div class="col-sm-12 col-md-12">
					<div id="exclusiveinterview" style="cursor: pointer" class="news1 top-left-corner-radius bottom-left-corner-radius">
						<p style="font-size: 16px">
							<br><spring:message code="msgCheckoutOurExecIntv" /><br>
						</p>
						
						<spring:message code="msgQuestConn7" />
					</div>
					
					<div class="news1" style="background-color:#eaedee;width: 50%;">
						<img src="images/mic.png" class="img-responsive" style="height:inherit;width: 100%;">
					</div>
					
					<b class="news2 top-left-corner-radius top-right-corner-radius" style="font-weight:700;color:#ffffff;font-size: 16px;background-color:#0094DA;overflow-y:hidden;margin-top:30px;text-align:center">
						<spring:message code="msgQuestConn8" />
					</b>
					
					<div class="news2 bottom-left-corner-radius bottom-right-corner-radius">
						<p>
							<b> <spring:message code="msgAAEE1" /> </b> <br>
							<b class="bluetext"><a href="https://twitter.com/AAEEorg" target="_blank" >@AAEEorg</a> </b> <br>
							<spring:message code="msgQuestConn9" /><br>
						</p>
						
						<p>
							<b> <spring:message code="msgAAofEducators" /> </b> <br>
							<b class="bluetext"> <a href="https://twitter.com/AAEteachers" target="_blank">@AAEteachers</a> </b> <br>
							<spring:message code="msgQuestConn10" />
						</p>
						
						
						<p>
							<b> <spring:message code="msgMiddleLvlEdu" /> </b> <br>
							<b class="bluetext"> <a href="https://twitter.com/AMLE" target="_blank">@AMLE</a> </b> <br>
							<spring:message code="msgQuestConn11" />
						</p>
						
						<p>
							<b> <spring:message code="msgSurvCurrDev" /> </b> <br>
							<b class="bluetext"> <a href="https://twitter.com/ASCD" target="_blank"> @ASCD </a> </b> <br>
							<spring:message code="msgQuestConn12" /><br>
						</p>
					</div>
					
					<div class="news2" style="padding: 0px;text-align:justify;margin-top:15px;overflow:hidden;background-color: initial;">
						<p>
							<spring:message code="msgQuestConn13" /> 
							<a href="https://twitter.com/TeacherMatch" target="_blank">@TeacherMatch</a> <spring:message code="msgusinghashtag" /> #EdTweets
						</p>
					</div>
				</div>
			</div>
			
			<div class="row news" style="float:right;">
				<div class="col-sm-12 col-md-12" style="float:right;margin-right: 17%;">
					<div class="news3 news top-left-corner-radius top-right-corner-radius"  style="font-size: 16px;margin-left: 15%;">
						<div style="color:#ffffff;background-color: #0094DA;">
							<br><br><spring:message code="msgQuestConn14" /><br><br><br>
						</div>
						
						<div class="bottom-left-corner-radius bottom-right-corner-radius" style="background-color:#ffffff; height:550px;">
							<br><br>
							<div align="center" style="padding-left: 15%;">
								<img src="images/jobsearchhandbook.png" class="img-responsive" width="90%" height="100%">
							</div>
							<br><br>
							
							<p>
								<spring:message code="msgQuestConn15" /><br><br><br>
							</p>
							
							<button type="button" class="btn btn-primary btn-lg" id="login">
								<spring:message code="msgDOWNLOADNOW" />
							</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div style="display:block; height:10px; overflow:hidden; background-color:initial;"  class="news4">
					</div>
					
					<div  class="news4 top-left-corner-radius top-right-corner-radius" style="display:block; height:auto; font-weight:700;color:#ffffff;font-size:16px;background-color:#0094DA;overflow-y:hidden;height:37px;margin-top:30px;padding-top: 8px;padding-left: 25px;">
							<spring:message code="msgQuestConn16" />
					</div>
					
					<div class="news4 bottom-left-corner-radius bottom-right-corner-radius">
						<table style="width:100%;">
							<tr>
								<td class="rowheading"> <spring:message code="msgMapOfWorld" /> </td>
							</tr>
								
							<tr>
								<td class="row1">
									<a href="http://alliance.la.asu.edu/maps/World-at.pdf" target="_blank">
										<spring:message code="msgWorldonAtlanticOcean" />
									</a>
								</td>
							</tr>
							
							<tr>
								<td class="row2">
									<a href="http://alliance.la.asu.edu/maps/World-pa.pdf" target="_blank">
										<spring:message code="msgWorldonPacificOcean" />
									</a>
								</td>
							</tr>
							
							<tr>
								<td class="row1">
									<a href="http://alliance.la.asu.edu/maps/WEST-HEM.pdf" target="_blank">
										<spring:message code="msgWesternHemisphere" />
									</a>
								</td>
							</tr>
								
							<tr>
								<td class="row2">
									<a href="http://alliance.la.asu.edu/maps/EAST-HEM.pdf" target="_blank">
										<spring:message code="msgEasternHemisphere" />
									</a>
								</td>
							</tr>
							
							<tr>
								<td class="row1">
									<a href="http://alliance.la.asu.edu/maps/N-HEMI.pdf" target="_blank">
										<spring:message code="msgNorthenHemisphere" />
									</a>
								</td>
							</tr>
							
							<tr>
								<td class="row2">
									<a href="http://alliance.la.asu.edu/maps/S-HEMI.pdf" target="_blank"> <spring:message code="msgSouthernHemisphere" /></a>
								</td>
							</tr>
							
							<tr>
								<td class="row1">
									<a href="http://alliance.la.asu.edu/maps/timezone.pdf" target="_blank"><spring:message code="msgStdTimeZone" /></a>
								</td>
							</tr>
							
							<tr>
								<td class="row2">
									<a href="http://alliance.la.asu.edu/maps/WRLDMTNS.pdf" target="_blank"><spring:message code="msgMajorMoutain" /> </a>
								</td>
							</tr>
							
							<tr>
								<td class="row1">
									<a href="http://alliance.la.asu.edu/maps/Storm_Trackers.pdf" target="_blank"><spring:message code="msgTropicalStorms" /> </a>
								</td>
							</tr>
							
							<tr>
								<td class="row2"> 
									<a href="http://alliance.la.asu.edu/maps/Rainforests.pdf" target="_blank">  <spring:message code="msgRainforests" /> </a>
								</td>
							</tr>
							
							<tr class="row1" style="display:block;height:20px">
								<td></td>
							</tr>
								
							<tr>
								<td class="rowheading"> <spring:message code="msgMapsContinents" /> </td>
							</tr>
							
							<tr>
								<td class="row1">
									<a href="http://alliance.la.asu.edu/maps/AFREG03.pdf" target="_blank"> <spring:message code="msgAfricanRegions" />  </a>
								</td>
							</tr>
							
							<tr>
								<td class="row2">
									<a href="http://alliance.la.asu.edu/maps/ANTART~1.PDF" target="_blank"><spring:message code="msgAntarctica" /> </a>
								</td>
							</tr>
							
							<tr>
								<td class="row1">
									<a href="http://alliance.la.asu.edu/maps/ASIA.PDF" target="_blank"><spring:message code="msgAsia" /> </a>
								</td>
							</tr>
							
							<tr>
								<td class="row2">
								<a href="http://alliance.la.asu.edu/maps/AUSTRA~1.PDF" target="_blank"> <spring:message code="msgAustralia" /> </a> </td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div style="margin-top:30px; width:92%; text-align: left;">
						
						<spring:message code="msgQuestConn17" /><br><br>
						
						<img src="images/globe.png" width="120px" style="float:right;" class="img-responsive">
						
						<b> <spring:message code="msgArizonaGeographicAlliance" />  </b>
						<div class="bluetext"><a href="http://www.geoalliance.asu.edu/" target="_blank">www.geoalliance.asu.edu</a></div>
						<spring:message code="msgQuestConn18" /><br>
					</div>
				</div>
			</div>
		</div>

<!-- ------------------------------------End of news Div------------------------------------ -->

<!-- ------------------------------------Start of high Div---------------------------------- -->

 		<div id="questhighDiv" class="bottomInnerDiv" style="padding-right:40px;">
		<p class="heading1"> <spring:message code="msgQuestConn19" /> </p><br>
		<div class="heading2"><spring:message code="msgQuestConn20" /> </div>	
		
		<p class="heading3"> <spring:message code="msgNSTAssosiation" /> </p>
		<div class="bluetext"> <a href="http://www.nsta.org" target="_blank"> http://www.nsta.org </a> </div>
		<p>
			<spring:message code="msgQuestConn21" /><br> <br>
			
			<spring:message code="msgQuestConn22" />
		</p>
		
		<div>
			<b><spring:message code="msgScience_Children" /> </b>
			<p> <spring:message code="msgQuestConn23_1" /> <b class="bluetext"> <a href="http://www.nsta.org/elementaryschool/" target="_blank"><spring:message code="msgScience_Children" />.</a></b></p>
				<p><spring:message code="msgQuestConn23_2" />
			</p>
		</div>
		</div>
		
<!-- ------------------------------------End of high Div------------------------------------ -->
	

<!-- ------------------------------------Start of middle Div-------------------------------- -->
   		
		<div id="questmiddleDiv" class="bottomInnerDiv" style="padding-right:40px;">
		<p class="heading1"> <spring:message code="msgFreeResource4MCS" /> </p><br>
		<p class="heading2"> <spring:message code="msgQuestConn24" /> </p>	
		
		<p class="heading3"> <spring:message code="msgMiddleLvlEdu" /></p>
		<div class="bluetext"> <a href="http://www.amle.org" target="_blank">http://www.amle.org </a> </div>
		<p>
			<spring:message code="msgQuestConn25" /> <b class="bluetext"> <a href="http://www.amle.org/membership.aspx" target="_blank"> <spring:message code="msgMembershipOptionspage" /> </a> </b>
		</p>
		
		<p>
			<spring:message code="msgAccessAMLE" />:<br>
			<b class="bluetext"> <a href="http://www.amle.org/BrowsebyTopic/WhatsNew/WNDet/TabId/270/ArtMID/888/ArticleID/438/The-School-Is-a-Teacher-But-What-Are-the-Lessons.aspx" target="_blank"> <spring:message code="msgQuestConn67" /> </a></b> <br>
			<b><spring:message code="msgStudentremberPos" /></b><br>
			<spring:message code="msgJohnH" /><br><br>
			
			<b class="bluetext"> <a href="http://www.amle.org/BrowsebyTopic/WhatsNew/WNDet/TabId/270/ArtMID/888/ArticleID/431/Patient-and-Tenacious-Teaching.aspx" target="_blank"><spring:message code="msgPatientTeaching" /> </a> </b><br>
			<b><spring:message code="msgEffectingTeaching" /></b><br>
			<spring:message code="msgRickWormeli" /><br> <br>
			
			<b class="bluetext"> <a href="http://www.amle.org/BrowsebyTopic/WhatsNew/WNDet/TabId/270/ArtMID/888/ArticleID/436/Growth-Buddies-Priceless-Professional-Development.aspx" target="_blank"><spring:message code="msgQuestConn66" /> </a> </b><br>
			<b><spring:message code="msgHAvingColluge" /> </b><br>
			<spring:message code="msgRebeccaKordatzky" /><br><br>
			
			<b class="bluetext"><a href="http://www.amle.org/BrowsebyTopic/WhatsNew/WNDet/TabId/270/ArtMID/888/ArticleID/443/The-Importance-of-Classroom-Structure.aspx" target="_blank"> <spring:message code="msgTheClassroomStructure" /></a> </b><br>
			<b><spring:message code="msgClassRoomStrct" /> </b><br>
			<spring:message code="msgDruTomlin" />
		</p><br>
		
		<p class="heading3"> <spring:message code="msgMathTeach" /></p>
		<div class="bluetext"> <a href="http://www.nctm.org/publications/content.aspx?id=9416" target="_blank"> <spring:message code="msgMTMSJourna" /> </a> </div>
		<p>
			<spring:message code="msgQuestConn26" />
		</p>
		
		<p>
			<b class="bluetext"> <a href="http://www.nctm.org/publications/article.aspx?id=43418" target="_blank"> <spring:message code="msgWebClass" /></a> </b> <br>
			<b><spring:message code="msgNOVolum20" /></b><br>
			<spring:message code="msgQuestConn27" /><br><br>
			
			<b class="bluetext"> <a href="http://www.nctm.org/publications/article.aspx?id=43205" target="_blank"> <spring:message code="msgVocabularySupport" /> </a> </b><br>
			<b><spring:message code="msgOCtVol20" /></b><br>
			<spring:message code="msgMTMSProviders" /><br> <br>
			
			<b class="bluetext"><a href="http://www.nctm.org/publications/article.aspx?id=43056" target="_blank"> <spring:message code="msgLessTravelled" /> </a> </b><br>
			<b><spring:message code="msgSepVol20" /></b><br>
			<spring:message code="msgLessTravelled2" /><br><br>
			
			<b class="bluetext"><a href="http://www.nctm.org/publications/article.aspx?id=42746" target="_blank"> <spring:message code="msgSupportEngLernr" /> </a> </b><br>
			<b><spring:message code="msgAugVol20" /></b><br>
			<spring:message code="msgArtProvided" />
		</p>
		</div>
		
<!-- -------------------------------------End of middle Div---------------------------------- -->
	
<!-- ----------------------------------Start of Elementory Div------------------------------- -->
 	
		<div id="questElementoryDiv" class="bottomInnerDiv" style="padding-right:40px;">
		<p class="heading1"> <spring:message code="msgFreeRes4ECST" /> </p><br>
		<p class="heading2"> <spring:message code="msgDIscourCompil" /> </p>	
		
		<p class="heading3"> <spring:message code="msgNYSElemClassrm" /></p>
		<div class="bluetext"><a href="http://www.nysecta.org/" target="_blank"> http://www.nysecta.org </a> </div>
		<p>
			<spring:message code="msgQuestConn28" />
		</p>
		<p>
			<spring:message code="msgQuestConn29" />
		</p>
		
			<p>
			<div class="bluetext"> <a href="http://www.nysecta.org/Newsletter/Spring 2014 Newsletter.pdf" target="_blank"> <spring:message code="msgQuestConn30" /> </a> </div>
			<spring:message code="msgQuestConn31" />
			</p>
		
		
		<p>
			<div class="bluetext"> <a href="http://www.nysecta.org/Winter 2014 Newsletter.pdf" target="_blank"><spring:message code="msgQuestConn32" /></a> </div>
			<spring:message code="msgQuestConn33" />
		</p>
		
		<p>
			<div class="bluetext"> <a href="http://www.nysecta.org/Newsletter/Fall 2013 NYSECTA Newsletter.pdf" target="_blank"> <spring:message code="msgQuestConn34" /> </a> </div>
			<spring:message code="msgQuestConn35" />
		</p>
		
		<p class="heading3"> <spring:message code="msgQuestConn36" /></p>
		<div class="bluetext"> <a href="http://www.nsta.org/" target="_blank"> http://www.nsta.org </a> </div>
		<p>
			<spring:message code="msgQuestConn37" />

		</p>
		<p>
			<spring:message code="msgQuestConn38" />
		</p>
		
		<div>
			<b><spring:message code="msgScience_Children" /> </b>
			<p><spring:message code="msgQuestConn23_1" /> <b class="bluetext"> <a href="http://www.nsta.org/elementaryschool/" target="_blank"><spring:message code="msgScience_Children" />.</a> </b></p>
				<p><spring:message code="msgQuestConn23_2" />.
			</p>
		</div>
		</div>

<!-- -------------------------------------End of Elementory Div------------------------------ -->
	</div>
</div>
</div>

<!-- -------------------------------------Start of Interview Div------------------------------ -->
 
<div id="interviewpage">
	<p><br></p><p><strong><a href="javascript:void(0);" id="back"><spring:message code="msgBacktoNews" /></a></strong> </p>
	<p style="color: black;font-size: 14px; font-weight: bold"> 
		<spring:message code="msgQuestConn39" />,<br> <spring:message code="msgQuestConn40" /> <strong>
	</strong></p>
	
	<p>
		<spring:message code="msgQuestConn41" />
	</p>
	
	<p>
		<spring:message code="msgQuestConn42" />
	</p>
		
	<p>
		<spring:message code="msgQuestConn43" />
	</p>
	
	<p>
		<spring:message code="msgQuestConn44" />
	</p>
	
	<p>
		<spring:message code="msgQuestConn45" />

	</p>
	
	<p>
		<b><spring:message code="msgQuestConn40" />:</b><b>&nbsp;&nbsp;&nbsp; </b><b><spring:message code="msgQuestConn46" /> </b>
	</p>
	
	<p>
		<b><spring:message code="msgBacktoNews" />:</b><b>&nbsp;&nbsp;&nbsp; </b><b><spring:message code="msgQuestConn47" /> </b>
	</p>
	
	<ol>
		<li>
			<p> <spring:message code="msgQuestConn48" /></p>
			<p><spring:message code="msgQuestConn49" /></p>
		</li>
		
		<li>
			<p><b><spring:message code="msgQuestConn50" />
    	    </p>
    	</li>
    	
    	<li>
    		<p><b><spring:message code="msgQuestConn51" /></b> <spring:message code="msgQuestConn52" /><br/> </p>
            	
            <p> <spring:message code="msgQuestConn53" />
 </p>
		</li>
	</ol>
	
	<p> <b><spring:message code="sltTM" />:</b><b>&nbsp;&nbsp;&nbsp; </b><b><spring:message code="msgQuestConn54" /> </b> </p>
	
	<p> <b><spring:message code="msgDP" />:</b><b>&nbsp;&nbsp;&nbsp; </b><b><spring:message code="msgQuestConn55" /></b> </p>
	
	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <spring:message code="msgQuestConn56" /><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<spring:message code="msgQuestConn57" /><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
		<spring:message code="msgQuestConn58" /> 
	</p>
	
	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
		<spring:message code="msgQuestConn59" /><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
		<spring:message code="msgQuestConn60" />
	</p>
	
	<p> <b> <spring:message code="sltTM" />:</b><b>&nbsp; </b><b><spring:message code="msgQuestConn64" /> </b> </p>
	
	<p>
		<b><spring:message code="msgPD" />:&nbsp;&nbsp; </b><spring:message code="msgQuestConn61" /> <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <spring:message code="msgQuestConn62" /><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
		<spring:message code="msgQuestConn63" /><br>
	</p>
	
	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
		<spring:message code="msgQuestConn65" /> 
	</p>
</div>

<!-- -------------------------------------End of Interview Div------------------------------ -->

