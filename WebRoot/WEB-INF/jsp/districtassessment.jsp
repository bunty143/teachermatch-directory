
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>


<script type="text/javascript" src="dwr/interface/DistrictAssessmentAjax.js?ver=${resourceMap['DistrictAssessmentAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/districtassessment.js?ver=${resourceMap['js/districtassessment.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />

<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.Ajax']}"></script> 

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
            
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        //width: 840,
         width: 945,
        minWidth: null,
        minWidthAuto: false,
       // colratio:[150,100,85,85,70,60,290], // table header width
       // colratio:[220,100,85,85,60,290],
        colratio:[320,160,98,90,260],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

 </script>
<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headMngAss"/></div>	
         </div>			
		 <div class="pull-right add-employment1">
		<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
				<a href="javascript:void(0);" onclick="return addAssessment()"><spring:message code="lnkAddAss"/></a>
		</c:if>

		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div class="TableContent top15">        	
         <div class="table-responsive" id="divMain">          
         </div>            
</div> 

<div  id="divAssessmentRow" style="display: none;" onkeypress="chkForEnterSaveAssessment(event);">
	<div class="row top5">
	<div class="col-sm-8 col-md-8">		                         
	 			<div class='divErrorMsg' id='errordiv' ></div>
	</div>
	</div>
	<div class="row">
		<div class="col-sm-8 col-md-8"><label><strong><spring:message code="lblAssName"/><span class="required">*</span></strong></label>
		<input type="hidden" name="districtAssessmentId" id="districtAssessmentId" >
		<input type="hidden" name="status" id="status" >
		<input type="hidden" name="pageIdNo" id="pageIdNo" value="1" >
		
		<input type="text" name="districtAssessmentName" id="districtAssessmentName" maxlength="100" class="form-control" placeholder=""></div>
	</div>

	<div class="row">
		<div class="col-sm-8 col-md-8" id="assDescription"><label><strong><spring:message code="lblAssDescr"/><span class="required">*</span></strong></label>
		<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>	
		<textarea class="form-control" name="districtAssessmentDescription" id="districtAssessmentDescription" maxlength="750" rows="4" onkeyup="return chkLenOfTextArea(this,750);" onblur="return chkLenOfTextArea(this,750);"></textarea>
		</div>
	</div>
	

         <div class="row">
			<div class="col-sm-6 col-md-6">     
	             <label id="captionDistrict"><spring:message code="lblDistrictName"/></label>
			             <span>
			             <input value="" type="text" maxlength="255" id="districtName" name="districtName" class="help-inline form-control"
			            		 onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
											onkeyup="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
											onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');getJobCategoryByDistrict();" onkeypress="return inputOnEnter(event);"/>
					      </span>
					      <input type="hidden" id="districtHiddenlId"/>
		        	 <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
               </div>
         </div>      	
	
	
       <div class="row" id="jsjo3">
	        <div class="col-sm-6 col-md-6">
	        <label id=""><spring:message code="lblJoCatN"/></label>
	          <select  class="form-control " name="jobCategoryMaster" id="jobCategoryMaster" >
	          <option value="0"><spring:message code="optStrJobCat"/></option>
			</select>
	        </div>
	   </div>

	<div class="row " id="jsjo3">
	 <div class="col-sm-7 col-md-7" id='schoolLbl' style="display: none;">
	            <label><strong><spring:message code="lblSchool"/></strong></label>
	 </div> 	               
	</div>
	
	<div class="row">  
		<div class="col-sm-3 col-md-3">
		<label><strong><spring:message code="lblInvSessTi"/></strong></label>
		<input type="text" name="assessmentSessionTime" onkeypress='return checkForInt(event)' id="assessmentSessionTime" maxlength="6" class="form-control" placeholder="">
		</div>  
		<div style="float: left;margin-top: 30px;margin-left: -15px;">&nbsp;<spring:message code="lbls"/></div>
		<div class="col-sm-3 col-md-3"><label><strong><spring:message code="lblTiAlPerQues"/></strong></label>
		<input type="text" name="questionSessionTime" onkeypress='return checkForInt(event)' id="questionSessionTime" maxlength="3" class="form-control" placeholder="">		
		</div>
		<div style="float: left;margin-top: 30px;margin-left: -15px;">&nbsp;<spring:message code="lbls"/></div>
		<div style="clear: both;"></div>
	</div> 

	<div class="row">  
		<div class="col-sm-3 col-md-3">
		<label><strong><spring:message code="lblRandQues"/></strong></label></br>
		<input class="span0" type="checkbox" name="questionRandomization" id="questionRandomization"  class="form-control" placeholder="">
		</div>  
		<div style="float: left;margin-top: 30px;margin-left: -15px;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
		<div class="col-sm-3 col-md-3"><label><strong><spring:message code="lblRandOpt"/></strong></label></br>
		<input class="span0" type="checkbox" name="optionRandomization" id="optionRandomization" class="form-control" placeholder="">
		</div>
	</div> 

    <div class="row top15" style="display: none;" id="divManage">
	  	<div class="col-sm-4 col-md-4">
	  		<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
	     	<button onclick="saveAssessment();" class="btn btn-large btn-primary"><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
	     	</c:if>
	     	 &nbsp;<a href="javascript:void(0);" onclick="clearAssessment();"><spring:message code="btnClr"/></a> 
	     </div>
	    
	</div>
  
	<div class="idone" id="divDone" style="display: none;">
		<div class="row top5">
			<div class="col-sm-4 col-md-4">
			<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}"><a href="javascript:void(0);" onclick="saveAssessment();"><spring:message code="lnkImD"/></a></c:if>
			 &nbsp;<a href="javascript:void(0);" onclick="clearAssessment();"><spring:message code="lnkCancel"/></a> 
			</div>
		</div>	
	</div>
</div>

              
 <div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
    <br><br>                

<script type="text/javascript">
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
$('#iconpophover3').tooltip();
$('#iconpophover4').tooltip();
getAssessmentGrid();
</script>
