<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap" value="${applicationScope.resouceMap}" />
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<link rel="stylesheet" href="css/dialogbox.css" />
<script type="text/javascript" src="js/tmhome.js?ver=${resourceMap['js/tmhome.js']}"></script>	
<link href="css/tmcommon.css?ver=${resourceMap['css/tmcommon.css']}"  rel="stylesheet" type="text/css">
<script type="text/javascript" src="dwr/interface/JobsAjax.js?ver=${resourceMap['JobsAjax.ajax']}"></script>
 <!-- for certification auto complete  -->
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.ajax']}"></script>
<script type="text/javascript" src="dwr/interface//ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>
<script type='text/javascript' src="js/teacher/pfAutoCompCertificate.js?ver=${resourceMap['js/teacher/pfAutoCompCertificate.js']}"></script>
<!-- .....  -->       

 <!--  for school -->
 <script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.Ajax']}"></script>
 <!-- ........ --> 
<script type='text/javascript'
	src="js/jobs.js?ver=${resouceMap['js/jobs.js']}"></script>
<script type="text/javascript"
	src="dwr/interface/JobsAjax.js?ver=${resouceMap['JobsAjax.ajax']}"></script>
<link href="css/font-awesome.css?ver=${resourceMap['css/font-awesome.css']}"  rel="stylesheet" type="text/css" >
<link href="css/font-awesome-ie7.css?ver=${resourceMap['css/font-awesome-ie7.css']}"  rel="stylesheet" type="text/css">
<link href="css/font-awesome-ie7.min.css?ver=${resourceMap['css/font-awesome-ie7.min.css']}"  rel="stylesheet" type="text/css">    	    
<script type="text/javascript" src="js/jquery-te-1.3.2.2.min.js" charset="utf-8"></script>
<link type="text/css" rel="stylesheet" href="css/jquery-te-1.3.2.2.css" charset="utf-8" />





<style>
.table th, .table td {
    padding: 8px;
    line-height: 20px;
    text-align: left;
    vertical-align: top;  
}
</style>

<div id="displayDiv"></div>
<script><!--
$(document).ready(function(){
   <c:if test="${allSchoolGradeDistrictVal=='1'}">		
		$('input:radio[name=allSchoolGradeDistrict]')[0].checked=true;
		//<c:set var="allSchoolGradeDistrictVal" value="1"/>	
		 
	</c:if>
	<c:if test="${allSchoolGradeDistrictVal=='2'}">
		$("#exceptedhireDiv").hide();
		$("#addSchoolLink").show();
		$('input:radio[name=allSchoolGradeDistrict]')[1].checked=true;
			document.getElementById("noOfExpHires").value="";
		//<c:set var="allSchoolGradeDistrictVal" value="2"/>
		ShowSchool();		
	</c:if>

});

	

	function addCertification()
	{	
		$("#addCertificationDiv").fadeIn();			
		$("#stateMaster").focus();		
	}
	
	function cancelCertification()
	{
		$('#errordiv').empty();
		$("#addCertificationDiv").hide();
		document.getElementById("certType").value="";
		document.getElementById("certificateTypeMaster").value="";	
	}
	
	function cancelJobOrder()
	{
		window.location.href="jobs.do";
	}
	function checkMSG(flag){
		if(flag==1){		
			document.getElementById("textDSMessage2").innerHTML="You have already selected the option \"Administrators from Selected School(s)\". If you will select the option \"Only District Administrator(s)\", you will lose the information from \"Administrators from Selected School(s)\".";
	    	$("#textDSChangeMessageModal2").modal("show");
	    	deleteReqByJobId();	
		}else if(flag==2){	
			document.getElementById("textDSMessage").innerHTML="You have already selected the option \"Only District Administrator(s)\". If you will select the option \"Administrators from Selected School(s)\" "+"you will lose the information from \"Only District Administrator(s)\".";
		    $("#textDSChangeMessageModal").modal("show");
	       	deleteReqByJobId();	
	       	
		}
		sortSelectList();
	}
	
--></script>
<style>
	.hide
	{
		display: none;
	}
	.inline
	{
		display: inline;
	}
	.nowrap
	{
		white-space: nowrap
	}
	
</style>
<input type="hidden" id="currentUrl" value="${currentUrl}">
<div class="row top15"
	style="margin-left: 0px; margin-right: 0px; margin-top: ">
	<div style="float: left;">
		<img src="images/add-editdistrictjob-order.png" width="41" height="41"
			alt="">
	</div>
	<div style="float: left;">
		<div class="subheading" style="font-size: 13px;">
			<spring:message code="headA/E" />
			<span><spring:message code="optDistrict" />
			</span>
			<spring:message code="lblJobOrder" />
		</div>
	</div>

	<div style="clear: both;"></div>
	<div class="centerline"></div>
</div>


<div class="row">
<div class="col-sm-12 col-md-12" onkeypress="return chkForEnterAddEditJobOrder(event);" ><br>
<div class="">
			<div class="control-group">			                         
			 	<div class='divErrorMsg' id='errordiv' ></div>
			</div>			
			<div class="control-group">	     			                         
			 	<div class='divErrorMsg' id='jobDescriptionErrordiv' ></div>
			</div>
</div>	</div>	</div>	
<div class="row">
	<div class="col-sm-8 col-md-8">
		<label>
			<spring:message code="lblDistrictName" />
			<span class="required">*</span>
		</label>
		<br />
		${DistrictOrSchoolName}
		<input type="hidden" id="districtOrSchooHiddenlId"
			value="${DistrictOrSchoolId}" />
		<input type="hidden" id="districtORSchoolName"
			name="districtORSchoolName" value="${DistrictOrSchoolName}" />
	</div>
</div>


<div class="row">
	<div class="col-sm-8 col-md-8">
		<label>
			<spring:message code="lblJoTil" />
			<span class="required">*</span>
		</label>
		<input type="text" id="jobTitle"
			value="${fn:escapeXml(jobOrder.jobTitle)}" maxlength="200"
			name="jobTitle" class="form-control">
	</div>
	<c:choose>
		<c:when test="${JobId!=0}">
			<div class="col-sm-3 col-md-3">
				<label>
					<spring:message code="lblJoI/JCode" />
				</label>
				<br />
				<input type="text" class="form-control" value="${JobId}"
					readonly="readonly" />
			</div>
		</c:when>
	</c:choose>
</div>
<c:set var="endDateRadio1Checked" value=""></c:set>
<c:set var="endDateRadio2Checked" value=""></c:set>
<c:set var="jobendDate" value="${jobEndDate}"></c:set>
<c:set var="jobendDateDis" value=""></c:set>
<c:choose>
	<c:when test="${jobEndDate eq '12-25-2099'}">
		<c:set var="endDateRadio2Checked" value="checked='checked'"></c:set>
		<c:set var="jobendDate" value=""></c:set>
		<c:set var="jobendDateDis" value="disabled='disabled'"></c:set>
	</c:when>
	<c:otherwise>
		<c:set var="jobendDateDis" value=""></c:set>
		<c:set var="endDateRadio1Checked" value="checked='checked'"></c:set>
	</c:otherwise>
</c:choose>
<div class="row">
	<div class="col-sm-3 col-md-3">
		<label>
			<spring:message code="lblPostStDate" />
			<span class="required">*</span>
		</label>
		<input type="text" id="jobStartDate" name="jobStartDate" maxlength="0"
			value="${jobStartDate}" class="form-control" style="width: 92%;">
	</div>
	<div class="col-sm-3 col-md-3" style="padding-left: 0px;">
		<label>
			<spring:message code="lblPostEnDate" />
			<span class="required">*</span>
		</label>
		<label class="radio" style="min-height:0px;margin-bottom:-6px;margin-top:6px;"><input type="radio" name="endDateRadio" ${endDateRadio1Checked}  id="endDateRadio1" onclick="return unDisableEndDate();"/></label>
	        <input type="text" id="jobEndDate" name="jobEndDate"   value="${jobendDate}" maxlength="0"   class="form-control" style="width:82%;margin-left:20px;">


	</div>
	<input type="hidden" id="changeDateHid" value="">
	<div class="col-sm-2 col-md-2" style="padding-left: 0px;">
		<label>
			&nbsp;
		</label>
		<label class="radio"
			style="min-height: 0Px; margin-bottom: -6px; margin-top: 6px;">
			<input type="radio" name="endDateRadio" id="endDateRadio2"
				${endDateRadio2Checked} onclick="return disableEndDate();" />
		</label>
		<input type="text" id="" name="" disabled value="Until Filled"
			class="form-control" style="width: 86%; margin-left: 20px;">

	</div>
	<div class="col-sm-3 col-md-3">
		<label>
			<spring:message code="lblJoTy" />
			<span class="required">*</span>
		</label>
		<select id="jobType" class="form-control">
			<c:if test="${jobType eq 'F'}">
				<option value="F" selected="selected">
					<spring:message code="optFullTime" />
				</option>
				<option value="P">
					<spring:message code="optPartTime" />
				</option>
				<option value="FP">
					<spring:message code="optF/PTime" />
				</option>
			</c:if>

			<c:if test="${jobType eq 'P'}">
				<option value="P" selected="selected">
					<spring:message code="optPartTime" />
				</option>
				<option value="F">
					<spring:message code="optFullTime" />
				</option>
				<option value="FP">
					<spring:message code="optF/PTime" />
				</option>
			</c:if>

			<c:if test="${jobType eq 'FP'}">
				<option value="FP" selected="selected">
					<spring:message code="optF/PTime" />
				</option>
				<option value="P">
					<spring:message code="optPartTime" />
				</option>
				<option value="F">
					<spring:message code="optFullTime" />
				</option>
			</c:if>

			<c:if test="${empty  jobType}">
				<option value="F">
					<spring:message code="optFullTime" />
				</option>
				<option value="P">
					<spring:message code="optPartTime" />
				</option>
				<option value="FP">
					<spring:message code="optF/PTime" />
				</option>
			</c:if>
		</select>
	</div>
</div>

<div class="row">

	<div class="col-sm-5 col-md-5  mt10">
		<a href="javascript:void(0);" onclick="addCertification();"><spring:message
				code="lnkAddCerti/LiceN" />
		</a>
	</div>
</div>

<c:choose>
	<c:when test="${ not empty JobId}">
		<input type="hidden" id="jobId" name="jobId" value="${JobId}" />
	</c:when>
	<c:otherwise>
		<input type="hidden" id="jobId" name="jobId" value="" />
	</c:otherwise>
</c:choose>
<input type="hidden" id="JobOrderType" name="jobOrderType"
	value="${JobOrderType}">
<div class="row">
	<div class="col-sm-12 col-md-12">
		<div id="showCertificationDiv"></div>
	</div>
	<div class="col-sm-12 col-md-12 top20">
		<div id="certificationRecords"></div>
	</div>
</div>

<div class="row">
	<div id="addCertificationDiv" class="hide">
		<div class="col-sm-4 col-md-4">
			<label>
				<strong><spring:message code="lblSt"/><span class="required">*</span>
				</strong>
			</label>
			<select class="form-control" id="stateMaster" name="stateMaster">
				<option value="0">
					<spring:message code="optSltSt" />
				</option>
				<c:forEach items="${listStateMaster}" var="st">
					<option id="st${st.stateId}" value="${st.stateId}"${selected} >
						${st.stateName}
					</option>
				</c:forEach>
			</select>
		</div>
		<div class="col-sm-7 col-md-7">
			<label>
				<spring:message code="lblCert/LiceName" />
				<span class="required">*</span><a href="#" id="iconpophover11"
					rel="tooltip"
					data-original-title="Name of certification as per certifying body such as a state department of education">
					<img width="15" height="15" alt="" src="images/qua-icon.png">
				</a>
			</label>
			<input type="hidden" id="certificateTypeMaster" value="" />
			<input type="text"
				placeholder="<spring:message code="lblCert/LiceName"/>"
				class="form-control" maxlength="100" id="certType" value=""
				name="certType"
				onfocus="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
				onkeyup="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
				onblur="hideCertificateType(this,'certificateTypeMaster','divTxtCertTypeData');" />
			<div id='divTxtCertTypeData'
				style='display: none; position: absolute;'
				onmouseover="mouseOverChk('divTxtCertTypeData','certType')"
				class='result'></div>
		</div>
		<div class="col-sm-4 col-md-4 top5">
			<div>
				<a href="javascript:void(0);" onclick="validateAddCertification();"><spring:message
						code="lnkImD" />
				</a>&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="javascript:void(0)" onclick="cancelCertification();"><spring:message
						code="btnClr" />
				</a>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-sm-10 col-md-10">
		<label>
			<spring:message code="headJobDescrip" />
			<a href="#" id="iconpophover2" rel="tooltip"
				data-original-title="<spring:message code="msgHowToCopyPastCutDouc"/>"><img
					width="15" height="15" alt="" src="images/qua-icon.png">
			</a>
		</label>
		<label class="redtextmsg">
			<spring:message code="msgHowToCopyPastCutDouc" />
		</label>
	</div>


	<div class="col-sm-11 col-md-11">
		<div class="" id="jDescription">
			<textarea rows="8" id="jobDescription" name="jobDescription"
				class="form-control">${jobOrder.jobDescription}</textarea>
		</div>
	</div>
</div>

<div class="row top10">
	<div class="col-sm-4 col-md-4">
		<label class="">
			<spring:message code="lblJoCatN" />
			<span class="required">*</span>
		</label>
		<div id="jobCategoryDiv" class="">
			<select id="jobCategoryId" name="jobCategoryId" class="form-control">
				<option value="0">
					<spring:message code="optSltJoCatN" />
				</option>
				<c:if test="${not empty jobCategoryMasters}">
					<c:forEach var="jobCategoryMaster" items="${jobCategoryMasters}"
						varStatus="status">
						<c:set var="jCIdSvalue" value=""></c:set>
						<c:if
							test="${jobOrder.jobCategoryMaster.jobCategoryId==jobCategoryMaster.jobCategoryId}">
							<c:set var="jCIdSvalue" value="selected"></c:set>
						</c:if>
						<option value="${jobCategoryMaster.jobCategoryId}"${jCIdSvalue}>
							<c:out
								value="${fn:replace(jobCategoryMaster.jobCategoryName,' - TM Default','')}" />
						</option>
					</c:forEach>
				</c:if>
			</select>
		</div>
	</div>
</div>

<div class="row top10">
	<div class="col-sm-4 col-md-4">
		<label class="">
			<spring:message code="lblSub" />
		</label>
		<div id="subjectsDiv" class="">
			<select id="subjectId" name="subjectId" class="form-control">
				<option value="0" selected="selected">
					<spring:message code="optStlSub" />
				</option>
				<c:if test="${not empty subjectMasters}">
					<c:forEach var="subjectMasters" items="${subjectMasters}"
						varStatus="status">
						<c:set var="subIdSel" value=""></c:set>
						<c:if
							test="${jobOrder.subjectMaster.subjectId==subjectMasters.subjectId}">
							<c:set var="subIdSel" value="selected"></c:set>
						</c:if>
						<option value="${subjectMasters.subjectId}"${subIdSel}>
							<c:out value="${subjectMasters.subjectName}" />
						</option>
					</c:forEach>
				</c:if>
			</select>
		</div>
	</div>
</div>

<div style="display: none;">
	<div class="row col-sm-10 col-md-10 top10">
		<spring:message code="msgWhAdminCanHiCandFrThJoOrdAppPo" />
	</div>
	<div class="row col-sm-4 col-md-4">
		<div>
			<label class="radio">
				<input type="radio" name="allSchoolGradeDistrict"
					id="allSchoolGradeDistrict" value="1"
					onclick="checkMSG(1);hideaddSchoolDiv();checkValue(1);">
				<spring:message code="msgOnlyDistAdnin" />
			</label>
		</div>
	</div>
	<div style="clear: both;"></div>
	<div class="row left15" id="exceptedhireDiv">
		<div class="col-sm-4 col-md-4">
			<label>
				<spring:message code="lblOfExpHi" />
			</label>
			<input type="text" class="form-control" maxlength="3"
				id="noOfExpHires" name="noOfExpHires" value="${noOfExpHires}"
				onkeypress="return checkForInt(event);">
		</div>
		<input type="hidden" name="allSchoolGradeDistrictVal"
			id="allSchoolGradeDistrictVal" value="${allSchoolGradeDistrictVal}" />
	</div>


	<div class="row left5 top10">
		<div class="col-sm-11 col-md-11">
			<label class="radio inline" style="padding-left: 0px;">
				<input type="radio" name="allSchoolGradeDistrict"
					id="allSchoolGradeDistrict" value="2"
					onclick="checkMSG(2);showaddSchoolLink();checkValue(2);">
				<spring:message code="lblAdmnfrmDiffSchls" />
			</label>
			<div class="pull-right hide" id="addSchoolLink">
				<a href="javascript:void(0);" onclick="addSchool();"><spring:message
						code="lnkAddSchl" />
				</a>
			</div>
		</div>
	</div>



	<div class="hide" id="addSchoolDiv">
		<div class="row left15">
			<div class="col-sm-6 col-md-6">
				<label>
					<spring:message code="lblSchoolName"/>
				</label>
				<input type="text" id="schoolName" maxlength="100" name="schoolName"
					class="form-control" placeholder=""
					onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
					onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
					onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');" />
				<input type="hidden" id="schoolName" name="schoolName" />
				<input type="hidden" id="schoolId" name="schoolId" value="" />
				<div id='divTxtShowData2'
					onmouseover="mouseOverChk('divTxtShowData2','schoolName')"
					style='display: none; position: absolute; z-index: 5000;'
					class='result'></div>
			</div>
			<div class="col-sm-4 col-md-4">
				<label>
					<spring:message code="lblOfExpHi" />
				</label>
				<input type="text" class="form-control" maxlength="3"
					id="noOfSchoolExpHires" name="noOfSchoolExpHires" placeholder="">
			</div>
		</div>
		<div class="col-sm-4 col-md-4 add-employment2 left30">
			<a href="javascript:void(0);" onclick="validateAddSchool();"><spring:message
					code="lnkImD" />
			</a> &nbsp;&nbsp;&nbsp;&nbsp;
			<a href="javascript:void(0)" onclick="cancelSchool();"><spring:message
					code="btnClr" />
			</a>
		</div>
	</div>


	<div class="row col-sm-10 col-md-10 left10" id="showAndHideSchoolDiv">
		<div id="showSchoolDiv"></div>
		<div id="showTempScuserhoolDiv" class="hide"></div>
		<div id=schoolRecords></div>
	</div>
</div>

<div class="row">
	<c:if test="${not empty bitly}">
		<div class="col-sm-10 col-md-10 mt10">
			<spring:message code="lblJoBrdURL" />
			<br />
			<a href="${bitly}" target="_blank">${bitly}</a>
		</div>
	</c:if>
</div>
<div class="row" id="showExitURLDiv">
	<div class="col-sm-10 col-md-10 mt10">
		<spring:message code="lblRe-DirU" />
	</div>

	<div class="col-sm-11 col-md-11" style="margin-top: -5px;">
		<input type="hidden" name="exitURLMessageVal" id="exitURLMessageVal" />
		<label class="radio">
			<input type="radio" name="exitUrlMessage" id="exitUrlMessage1"
				value="1" checked>
			<spring:message code="lblforExitUrlMessage" />
			<a href="#" id="iconpophover4" rel="tooltip"
				data-original-title="URL address where job applicant can complete the remaining requirements for this job post completion of TeacherMatch base EPI"><img
					width="15" height="15" alt="" src="images/qua-icon.png">
			</a>
		</label>
		<input type="text" id="exitURL" maxlength="250" name="exitURL"
			value="${DistrictExitURL}" class="form-control" />
	</div>
</div>
<div class="row top5">
	<div class="col-sm-10 col-md-10 mt10">
		<button onclick="validateAddEditJobOrderforquest();"
			class="btn btn-large btn-primary">
			<spring:message code="btnSJOOrdr" />
			<i class="icon"></i>
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="javascript:void(0)" onclick="cancelJobOrder();"><spring:message
				code="btnClr" />
		</a>
	</div>
</div>





<iframe src="" id="iframeJSI" width="100%" height="480px"
	style="display: none;">
</iframe>
<div style="display: none;" id="loadingDiv">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
	</table>
</div>

<div class="modal hide" id="jobOrderAdd" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="actionForward();">
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" style="height: 120px;">
						<span id="jobOrderText"></span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="actionForward();">
					<spring:message code="btnOk" />
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="jobOrderUpdate" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="actionForward();">
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" style="height: 120px;">
						<span id="jobOrderUpdateText"></span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="actionForward();">
					<spring:message code="btnOk" />
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="textDSChangeMessageModal" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3>
					Teachermatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="">
						<span id="textDSMessage"></span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnOk" />
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="EPIMessage" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					Teachermatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="">
						<span id="EPIMsg"></span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showSchoolError();">
					<spring:message code="btnOk" />
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="textDSChangeMessageModal2" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3>
					Teachermatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="">
						<span id="textDSMessage2"></span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="getRequisiontForJobOrder();">
					<spring:message code="btnOk" />
				</button>
			</div>
		</div>
	</div>
</div>







<script type="text/javascript">
		//checkValue(${allSchoolGradeDistrictVal});
		//displayJobRecords();
		DisplayCertification();
		ShowSchool();		
	</script>
<script type="text/javascript">
	$('#iconpophover1').tooltip();
	$('#iconpophover11').tooltip();
	$('#iconpophover2').tooltip();
	$('#iconpophover5').tooltip();
	$('#iconpophover4').tooltip();
	$('#iconpophover6').tooltip();
	$('#iconpophover7').tooltip();
	$('#iconpophoverReqEg').tooltip();
	$('#iconpophoverReqEgSchool').tooltip();
	//getJobCategories();
	</script>

<script type="text/javascript">		 
	 var checkForAdministration=0;
	 var now = new Date();
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          min:now,
          showTime: true
      });
     cal.manageFields("jobStartDate", "jobStartDate", "%m-%d-%Y");
     cal.manageFields("jobEndDate", "jobEndDate", "%m-%d-%Y");  
    
     function jobStartDateDiv(){
          cal.manageFields("jobStartDate", "jobStartDate", "%m-%d-%Y");
     }
   
   function sortSelectList(){
		  	var selectListAvlbList = $('#avlbList option');
		  	var selectListavlbListSchool = $('#avlbListSchool option');
			selectListAvlbList.sort(function(a,b){
			    a = a.value;
			    b = b.value;
			    return a-b;
			});
			$('#avlbList').html(selectListAvlbList);
			selectListavlbListSchool.sort(function(a,b){
			    a = a.value;
			    b = b.value;
			    return a-b;
			});
			$('#avlbListSchool').html(selectListavlbListSchool);
		  }
	
	function showaddSchoolLink()
	{
	   checkForAdministration=1; 
	   $("#addSchoolLink").show();
	   $("#exceptedhireDiv").hide();
	   $('#noOfExpHires').css("background-color", "");
	   document.getElementById("noOfExpHires").value="";
	   
	}
	function hideaddSchoolDiv()
	{	
	    deleteSchoolForTempTable();
	    checkForAdministration=0;
		$("#addSchoolLink").hide();
		$("#addSchoolDiv").hide();
		$("#exceptedhireDiv").show();
		
		
	}
	
	
	
	
	
	
	document.getElementById("allSchoolGradeDistrict").checked = true;		
	$("#jobTitle").focus();
     </script>

