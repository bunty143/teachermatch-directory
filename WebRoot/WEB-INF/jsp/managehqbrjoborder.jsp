<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="tm.bean.user.UserMaster"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/HeadQuarterAjax.js?ver=${resourceMap['HeadQuarterAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/managehqbrjoborder.js?ver=${resourceMap['js/managehqbrjoborder.js']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.Ajax']}"></script>

<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.ajax']}"></script>
<script type='text/javascript' src="js/branchautoSuggest.js?ver=${resourceMap['js/branchautoSuggest.js']}"></script>

<script type="text/javascript" src="js/jquery-1.4.1.js?ver=${resourceMap['']}"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js?ver=${resourceMap['']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[65,130,60,70,65,90,100,50,75,75,170],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

function applyScrollOnTblJobs()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblJobsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[60,165,150,60,90,90,50,80,215],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}
</script>
<style>
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
</style>

<style>
	.hide
	{
		display: none;
	}
	.icon-question-sign{
 	 font-size: 1.3em;
 }
</style>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Manage <span id="districtOrSchoolTitle"></span> Job Orders</div>	
         </div>			
		 <div class="pull-right add-employment1">
			<c:set var="url" value="hqbrjoborder.do?JobOrderType=${JobOrderType}&jobAuthKey=${jobAuthKey}"></c:set>
			<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
			<a href="javascript:void(0);" onclick="addnewJob('${url}')">+Add Job Order</a>
			</c:if>

		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>
	 <c:if test="${not empty userMaster}">
		<c:if test="${userMaster.entityType eq 3}">	        			
			<c:set var="hide" value=""></c:set>
			<c:set var="hide" value="hide"></c:set>
		</c:if>
	</c:if>
	<input type="hidden" id="entityType" name="entityType" value="${userMaster.entityType}">

<div class="row top2">
             	<div class="col-sm-3 col-md-3">
					<div class="">
						<span class=""><label class="">Status</label>
						     <select id="status" name="status" class="form-control">
							      <option value="All">All Job Orders</option>
							      <option value="A" selected>Active Job Orders</option>
							      <option value="I">Inactive Job Orders</option>
						      </select>
					     </span>
			 		</div>
				</div>
				<div class="col-sm-2 col-md-2">
					<label class="">Job Id/Job Code #</label>
					<input type="text" id="jobOrderId" name="jobOrderId" class="form-control fl" maxlength="6" onkeypress="return checkForInt(event);"/>
				</div>
				<c:if test="${userMaster.entityType eq 5}">
				<div class="col-sm-5 col-md-5">
					<label class="">Branch Name</label>
					<input type="text" id="branchName" name="branchName"  class="form-control" value="${branchName}"
       					onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchSearchId','');"
						onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchSearchId','');"
						onblur="hideBranchMasterDiv(this,'branchSearchId','divTxtShowDataBranch');"	/>
						<div id='divTxtShowDataBranch' style='display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>
						<input  type="hidden" id="branchSearchId" name="branchSearchId" value=""/>								      
		
				</div>
				</c:if>
				
				<div class="col-sm-2 col-md-2 top25-sm22">
					<c:if test="${fn:indexOf(roleAccess,'|5|')!=-1}">
		             	<label class=""></label>
	            	 	<button class="btn btn-primary" type="button"  onclick="displayJobs();">&nbsp;&nbsp;&nbsp;&nbsp;Search &nbsp;<i class="icon"></i>&nbsp;&nbsp;&nbsp;&nbsp;</button>
	             	</c:if>
				</div>
	         </div> 

	         
	         <div class="row hide">
		          <div class="col-sm-5 col-md-5">
			           	<div id="searchLinkDiv" class="">
					    	<a href="javascript:void:(0);" onclick="displayAdvanceSearch()">Open Advanced Search</a>
			     	 	</div> 
					
						<div id="hidesearchLinkDiv" class="hide">
					   		<a href="javascript:void:(0);" onclick="hideAdvanceSearch()">Close Advanced Search</a>
						</div>
	     		  </div>
	         </div>
	         
	         <div class="hide" id="advanceSearchDiv">
		         <div class="row" >
		         	
		         		<div class="col-sm-5 col-md-5">
				             	<label class="">Requisition/Position#</label>
				             	<input type="text" id="reqno" name="reqno" maxlength="8" class="form-control fl" />
				         </div>
			             <div class="col-sm-5 col-md-5">
				         		<span class=""><label class="">Zone</label></span>
			           			<select id="zone" name="zone" class="form-control" disabled="disabled"></select>
				        </div>
			        
		         </div>
		         <div class="row" >
		         	<div class="col-sm-5 col-md-5">
		         			<label>
								Job Category
							</label>
							<select multiple="multiple" class="form-control" name="jobCateSelect"
								id="jobCateSelect" size="5" style="height:94px;" onblur="getJobSubcategory();" >
								<option value="0">
									Select district for relevant jobCategory list
								</option>
						</select>
		         	</div>
		         	<div class="col-sm-5 col-md-5">
		         			<label>
								Job Sub Category
							</label>
							<select multiple="multiple" class="form-control" name="jobSubCateSelect"
								id="jobSubCateSelect" size="5" style="height:94px;">
								<option value="0">
									Select Job Category for job Sub-Category list
								</option>
						</select>
		         	</div>
		         </div>
	         </div>
 
             <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
				 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
				 </iframe>  
			  </div> 


 <!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadJobOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><span id="pdfDistrictOrSchoolTitle"></span> Job Orders </h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Close</button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End -->   
 <div class="row mt5 top10 hide" style="padding-right:20px;">
       <table class="marginrightForTablet">
		      <tr>
		         <td  style='padding-bottom:5px;'>
					<a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="downloadJobOrderExcelReport();"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td style='padding-left:2px;padding-bottom:5px;'>
				  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='downloadJobOrderPDFReport();'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td style='padding-bottom:5px;'>
				 <td>
					<a data-original-title='Print' rel='tooltip' id='printId' href='javascript:void(0);' onclick="printAllJobs();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td>  
			</tr>
		</table>
		</div>


 
        <div class="TableContent mt20 top10">        	
            <div class="table-responsive" id="divMain">          
            </div>            
        </div> 
 	 

<div class="modal hide "  id="geozoneschoolDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:650px; margin-top: 20px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="$('#geozoneschoolFlag').val(0);">x</button>
		<h3  id="myModalLabelGeo"></h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
				<input type="hidden" id="geozoneId" value="0"/>
				<input type="hidden" id="geozoneschoolFlag" value="0"/>
				<div id="geozoneschoolGrid"></div>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id="">
 		</span>&nbsp;&nbsp;<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="$('#geozoneschoolFlag').val(0);">Close</button> 		
 	</div>
  </div>
 </div>
</div>



<div  class="modal hide distAttachment"  id="distAttachment"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameEEOC11">Attachment</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divPnrAttachment">
					 
					</div>
					<iframe src="" id="ifrmAttachment" width="100%" height="480px"> </iframe>   
				</div>
				
		 	</div>
		 	<input type="hidden" id="eligibilityMasterId" value=""/>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideAttachmentDiv();">Close</button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>



<!--	 Modal for displaying job Order  history  -->
<div  class="modal hide"  id="jobOrderHistoryModalId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 850px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="hideById('jobOrderHistoryModalId')">x</button>
		<h3 id="myModalLabel">Job Order History</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div class="" id="divJobHisTxt">
			
			
		</div>
 	</div>
 	
   </div>
   <div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick="hideById('jobOrderHistoryModalId')">Close</button> 		
 	</div>
 </div>
</div>
</div>



	<div style="display:none;z-index: 5000;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
	
 <input type="hidden" id="headQuarterId"  value="${headQuarterId}"/> 
 <input type="hidden" id="branchId"  value="${branchId}"/>
 
<c:if test="${userMaster.entityType eq 5 or userMaster.entityType eq 6}">
	<script type="text/javascript">
			displayJobs();
	</script>
 </c:if>             
 <script type="text/javascript">
   $(document).ready(function () {
      $('#jobOrderId').bind('paste', function (e) {
         e.preventDefault();
        // alert("You cannot paste text");
      });
   });
</script>