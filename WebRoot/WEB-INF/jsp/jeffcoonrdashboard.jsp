<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/TeacherProfileViewInDivAjax.js?ver=${resourceMap['TeacherProfileViewInDivAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ONBServiceAjax.js?ver=${resourceMap['ONBServiceAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateGridAjaxNew.js?ver=${resouceMap['CandidateGridAjaxNew.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateGridAjax.js?ver=${resouceMap['CandidateGridAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateGridSubAjax.js?ver=${resouceMap['CandidateGridSubAjax.ajax']}"></script>
<script type='text/javascript' src="js/report/candidategrid.js?ver=${resouceMap['js/report/candidategrid.js']}"></script>
<script type='text/javascript' src="js/report/candidategridnew.js?ver=${resouceMap['js/report/candidategridnew.js']}"></script>
<script type='text/javascript' src="js/teacher/onb.js?ver=${resouceMap['js/teacher/onb.js']}"></script>
<script type="text/javascript" src="js/onr_common.js?ver=${resouceMap['js/onr_common.js']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src='dwr/util.js'></script>
<link rel="stylesheet" type="text/css" href="css/onrdashboard.css?ver=${resouceMap['css/onrdashboard.css']}" />
<link rel="stylesheet" href="css/dialogbox.css?ver=${resouceMap['css/dialogbox.css']}" />
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<style>
.scrollspy_profile { height:500px; overflow-y:auto; overflow-x:hidden;padding-left:5px;
.profileContent
{
	padding: 0px;
	padding-left: 12px;
	padding-top: 10px;
	color: #474747;
	font-weight: normal;
	font-size: 10px;
}

.dropdown-menu11{
 	min-width:40px;
 	margin-left: -60px; 
 }
 
.dropdown-menu11 {  
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1000;
  display: none;
  float: left;
  min-width: 170px;
  padding: 5px 0;
  margin-top: 10px;
  list-style: none;
  background-color: #ffffff;
  border: 1px solid #ccc;
  /*border: 1px solid rgba(0, 0, 0, 0.2);*/
  *border-right-width: 2px;
  *border-bottom-width: 2px;
  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;
  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding;
  background-clip: padding-box;
  min-width:40px;

}
.navbar .nav > li > .dropdown-menu11:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-bottom: 7px solid #ccc;
/*border-bottom-color: rgba(0, 0, 0, 0.2);*/
position: absolute;
top: -7px;
left: 10px;      
}
.navbar .nav > li > .dropdown-menu11:after {
content: '';
display: inline-block;
border-left: 6px solid transparent;
border-right: 6px solid transparent;
border-bottom: 6px solid #ffffff;
position: absolute;
top: -6px;
left: 10px;
}
.dropdown-menu11.pull-right {
  right: 0;
  left: auto;
}
.dropdown-menu11.divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5;
}
.dropdown-menu11 > li > a {
  display: block;
  padding: 0px 20px;
  margin-top:5px;
  clear: both;
  font-weight: normal;
  line-height: 1.428571429;
  color: #333333;
  white-space: nowrap;
}
.dropdown-menu11 > li > a:hover,
.dropdown-menu11 > li > a:focus {
  text-decoration: none;
  color: #428bca;
  //background-color: #428bca;
}
.dropdown-menu11 > .active > a,
.dropdown-menu11 > .active > a:hover,
.dropdown-menu11 > .active > a:focus {
  color: #ffffff;
  text-decoration: none;
  outline: 0;
  background-color: #428bca;
}
.dropdown-menu11 > .disabled > a,
.dropdown-menu11 > .disabled > a:hover,
.dropdown-menu11 > .disabled > a:focus {
  color: #999999;
}
.dropdown-menu11 > .disabled > a:hover,
.dropdown-menu11 > .disabled > a:focus {
  text-decoration: none;
  background-color: transparent;
  background-image: none;
  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
  cursor: not-allowed;
}
.open > .dropdown-menu11 {
  display: block;
}
.open > a {
  outline: 0;
}

.pull-right > .dropdown-menu11 {
  right: 0;
  left: auto;
}

.dropup .dropdown-menu11,
.navbar-fixed-bottom .dropdown .dropdown-menu11 {
  top: auto;
  bottom: 100%;
  margin-bottom: 1px;
}
</style>
<script type="text/javascript">


function applyScrollOnAssessments()
{		
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentTable').fixheadertable({ 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        colratio:[180,120,150,185], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function applyScrollOnTblLanguage()
{
    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#teacherLanguageGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         // table header width
        
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[277,276,276],
           </c:when>
           <c:otherwise>
             colratio:[244,243,243],
           </c:otherwise>
        </c:choose>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });         
}
var $j=jQuery.noConflict();
	$j(document).ready(function() {
});
function applyScrollOnTbl_AssessmentDetails()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentDetails').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        width: 830,
	    minWidth: null,
        minWidthAuto: false,
        olratio:[230,200,200,200],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}


function applyScrollOnHonors()
{
		var $j=jQuery.noConflict();
      $j(document).ready(function() {
      $j('#honorsGrid').fixheadertable({ //table id 
      caption: '',
      showhide: false,
      theme: 'ui',
      height: 350,
      width: 730,
      minWidth: null,
      minWidthAuto: false,
      colratio:[250,250,175,176,111], // table header width
      addTitles: false,
      zebra: true,
      zebraClass: 'net-alternative-row',
      sortable: false,
      sortedColId: null,
      //sortType:[],
      dateFormat: 'd-m-y',
      pager: false,
      rowsPerPage: 10,
      resizeCol: false,
      minColWidth: 100,
      wrapper: false
      });            
      });
}


function applyScrollOnInvl()
{
		var $j=jQuery.noConflict();
      $j(document).ready(function() {
      $j('#involvementGrid').fixheadertable({ //table id 
      caption: '',
      showhide: false,
      theme: 'ui',
      height: 350,
      width: 730,
      minWidth: null,
      minWidthAuto: false,
      colratio:[250,250,175,176,111], // table header width
      addTitles: false,
      zebra: true,
      zebraClass: 'net-alternative-row',
      sortable: false,
      sortedColId: null,
      //sortType:[],
      dateFormat: 'd-m-y',
      pager: false,
      rowsPerPage: 10,
      resizeCol: false,
      minColWidth: 100,
      wrapper: false
      });            
      });
}





function applyScrollOnTblOnr(userRoleId)
{	
if(userRoleId == 8){
 	var columnSpan = [360,100,120,80,180,60,80];
 	var tableWidth = 980;
 }else if(userRoleId == 9){
 	var columnSpan = [260,80,80,80,130,50,50,50,50,50,50,50,50,70];
 	var tableWidth = 1100;
 }else {
 	var columnSpan = [30,260,80,80,80,130,50,50,50,50,50,50,50,50,50,50,70];
 	var tableWidth = 1230;
 }
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#onrGridTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: tableWidth,
        minWidth: null,
        minWidthAuto: false,
        colratio:columnSpan, // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 10,
        wrapper: false
        });
        });			
}
function applyScrollOnTblI9()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#firstTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[90,200,100,180,256], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblFP()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#fpTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 830,
        minWidth: null,
        minWidthAuto: false,
        colratio:[80,130,80,80,100,130,150,80], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}
function applyScrollOnTblDT()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#dtTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[90,180,80,80,130,266], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblTrans()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#transTableHistory').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[90,200,100,180,256], // table header width
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}

function applyScrollOnTblTransAcademic()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#academicGridAcademic').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[35,150,180,200,185,76],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });	
}

function applyScrollOnTblEEOC()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#EEOCTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[90,200,100,180,256], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblTeacherCertifications_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherCertificates_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[200,100,100,130,200],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblTeacherAcademics_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherAcademics_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,    
        colratio:[410,180,60,60],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblWorkExp()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblWorkExp_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[148,278,90,103,111],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblEleRef_profile()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tbleleReferences_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[120,85,90,160,80,82,65,45],    //changed: by rajendra	
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTbl_AdditionalDocuments()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 728,
        minWidth: null,
        minWidthAuto: false,
        colratio:[500,228,],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblVideoLinks_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblelevideoLink_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[450,150,130,],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

//////////Message scroll 
function applyScrollOnMessageTbl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblMessages').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 70,
        width: 614,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,140,100,215,60], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
</script>

<style>

</style>

<div id="loadingDiv"> <!-- style="display:none; z-index: 5000;"  -->
  	<table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>

<div class="row">
	<div class="col-sm-6">
		<div style="float: left;">
			<img src="images/dashboard-icon.png" width="41" height="41" alt="">
		</div>
		<div style="float: left;">
			<div class="subheading">
				Onboarding Dashboard
			</div>
			<input type="hidden" name="districtId" id="districtId"	value="${districtId}" />
			<input type="hidden" name="hdId" id="hdId"	value="${hdId}" />
			<input type="hidden" name="flagCanEditOrView" value="${flagCanEditOrView}" />
		</div>
	</div>	
	<div style="clear: both;margin-left:15px;border-bottom: 1px solid black;padding-bottom: 10px;width:100%;"></div>
</div>			
<div id="panelDiv">
<div class="row mt10">
<input type="hidden" name="statusType" id="statusType" /> 
<table width='100%'  border='0' cellspacing='1' cellpadding='1'>
  <tr>
    <td>
		<div class="col-sm-3 col-md-3">
			<span class=""><label class=""><spring:message code="lblFname"/></label><input type="text" id="firstName" name="firstName"  maxlength="50"  class="help-inline form-control" onkeypress="return chkForEnterDisplayPanel(event);" /></span>
		</div>
		<div class="col-sm-3 col-md-3">
		 <div class="">
		 	<span class=""><label class=""><spring:message code="lblLname"/></label><input type="text" id="lastName" name="lastName"  maxlength="50"  class="help-inline form-control" onkeypress="return chkForEnterDisplayPanel(event);"/></span>
		 </div>
		</div>			       
		<div class="col-sm-3 col-md-2">
		 <div><label class=""><spring:message code="lblPosition"/> #</label><input type="text" id="position" name="position"  maxlength="75"  class="form-control fl" onkeypress="return chkForEnterDisplayPanel(event);"/>
		</div>				       
		</div>
		<div class="col-sm-2 col-md-2" style="padding-left: 20px;">
			<button class="btn btn-primary top25-sm2" type="button" onclick="searchDisplayHiredCandidate()" style="width: 95px;"><spring:message code="lnkSearch"/><i class="icon"></i></button> 				      
		</div>
		
		<div style="float:left;width:100%;">
		<div class="col-sm-3 col-md-5">
			<span class=""><label class="">School Name</label>
				<select class="form-control fl" name='schoolId'>
					<c:if test="${entityType eq 2}">
						<option value='0'>All</option>
					</c:if>
					<c:if test="${not empty allSchool}">
						        <c:forEach var="allSchool" items="${allSchool}" varStatus="status">
									<c:choose> 
								       <c:when test="${allSchool.schoolId!=null}">	
									        <option value="${allSchool.schoolId}">${allSchool.schoolName}</option> <%--(${allSchool.locationCode}) --%>
								       </c:when>
								    </c:choose>
								</c:forEach>
						 </c:if>
				</select>
			</span>
		</div>
		<div class="col-sm-3 col-md-5">
			<span class=""><label class="">ES Tech</label>
				<select class="form-control fl" name="estechId" >
					<option value='0'>All</option>
						<c:if test="${not empty empST}">
						        <c:forEach var="empST" items="${empST}" varStatus="status">
									<c:choose> 
								       <c:when test="${empST.primaryESTech!=null}">	
									        <option value="${empST.employmentservicestechnicianId}">${empST.primaryESTech}</option>
								       </c:when>
								    </c:choose>
								</c:forEach>
						 </c:if>
				</select> 
			</span>
		</div>
		</div>
    </td>
    <td style="width:10%;padding-right:0px;">
    	<table width='145px'  border='0' cellspacing='5' cellpadding='5'>
		  <tr>
			<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #00882B;"></span></td>
			<td align="left"><spring:message code="optCmplt"/></td>
			<td>&nbsp;</td>
			
		  </tr>
		   <tr>
			<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #FFFF00;"></span></td>
			<td align="left"><spring:message code="lblWaived"/></td>
			<td>&nbsp;</td>
		    <!--<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #000000;"></span></td>
		    <td align="left"><spring:message code="lblReprint"/></td>-->
		  </tr>
		  <tr>
			<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #0000CC;"></span></td>
			<td align="left"><spring:message code="lblPending"/></td>
			<td>&nbsp;</td>
			
		  </tr>
		 <tr>
		 	<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #7F7F7F;"></span></td>
			<td align="left"><spring:message code="optIncomlete"/></td>
			<td>&nbsp;</td>
			<!--<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #473F3F;"></span></td>
			<td align="left"><spring:message code="lblPrinted"/></td>
			<td>&nbsp;</td>
			 <td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #FFA500;"></span></td>
			<td align="left"><spring:message code="lblCHRPending"/></td>-->
		  </tr>
		  <tr>
		  	<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #FF0000;"></span></td>
			<td align="left"><spring:message code="lblFailed"/></td>
			<td>&nbsp;</td>
		  </tr>
		   <tr>
		  	<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color:  #800080;"></span></td>
			<td align="left">Provided by CDE</td>
			<td>&nbsp;</td>
		  </tr>
		</table>
    </td>
  </tr>
</table>

</div>
       <div class="row"> 
	       <div class="col-sm-12">
         <table class="">
		      <tr>
		         <td  style='padding-left:5px;'>
					<a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="generateOnboardingExel();"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td>
				  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='generateOnboardingPDF();'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				
				 <td>
					<a data-original-title='Print' rel='tooltip' id='printId' href='javascript:void(0);' onclick="printOnboardingDATA();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td>  
			</tr>
		</table>
    </div>
   </div> 
       	

<div class="row" style="">		
	    <div class="">
	    	<div class="table-responsive" id="panelGrid" style="padding-left:15px;">		
		    </div>
		    <c:if test="${flagCanEditOrView eq 1}">
			    <div class="table-responsive" style="padding-left:15px;padding-top:25px;">	
			    	<button class="btn btn-primary" type="button" onclick="sendToPeopleSoft();">Send to PeopleSoft</button>
			    </div>
		    </c:if>
       </div>
  </div>
 </div>

 
<div  class="modal hide in onr_popup"  id="onr_popup"  tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true' data-backdrop="static">
		<div class='modal-dialog onr-dialog' style="height:650px;">
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="onb_popupHide();">x</button>
				<h3><span id="myModalLabel"></span></h3>
				<input type="hidden" id="myModalLabelI9Hidden" name="myModalLabelI9Hidden" />
			</div>
			<div class="modal-body">
			<div id='addStatusId' style="padding-left:760px;display:block;"><a href="#" onclick="return open_divONB()">Add Status</span></a></div>
			<input type="hidden" id="addStatusHidden" name="addStatusHidden" />
				<div class="control-group">
					<div class="" id="allInfo">
					 
					</div>
				 </div>
					<div class="row ">
						  <div class="control-group span16">                   
					 			<div class='divErrorMsg' id='errordiv' style="padding-left:15px;"></div>
						  </div>
					</div>
					<div class="row EmpOrSalary">
						  <input type="hidden" value='' name='empOrSalary'> 
						  <input type="hidden" value='' name='eligibilityId'>
						  <input type="hidden" value='' name='teacherHistoryId'>
						  <input type="hidden" value='' name='isSendToPeopleSoft'>  
						  <div class="control-group span16" id='allTextField' style="width: 100%;">                 
					 			
						  </div>
					</div>
					<div class="" id="add_action" class="add_action" style="display:none;">
						 <table width="100%"  border="0">
							  <tr>
							    <td width="17%" id="statusRadioLabel"><spring:message code="optStatus"/><span class="required">*</span></td>
							    <td id="statusRadioDiv">
									<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
					    				<input type="radio" value="Yes" id="status1" name="status"> <spring:message code="optY"/>
				        			</label>
				        			<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
					    				<input type="radio" value="No" id="status2" name="status"> <spring:message code="optN"/>
				        			</label>
				        			<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
					    				<input type="radio" value="Waived" id="status3" name="status"> <spring:message code="lblWaived"/>
				        			</label>
								</td>
							  </tr>
							  <tr>
							    <td><spring:message code="headNot"/></td>
							    <td><div id="notesmsg"><textarea rows="5" class="form-control" cols="25" name="notes" id="notes" maxlength="1000"></textarea></div></td>
							  </tr>
							  <tr><td>&nbsp;</td></tr>
							  <tr>
							    <!--<td><spring:message code="tdHeadingAF"/></td>-->
							    <td></td><td>
								    <!--<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
									</iframe>
									<form id="frmonrI9Upload" enctype='multipart/form-data' method='post' target='uploadFrame' action='onrI9UploadServlet.do'>						    	
									</form>
									<input id="eligibilyFile" name="eligibilyFile" type="file" width="20px;" style="">-->
										<input type="hidden" id="jsiFileName" name="jsiFileName" value="" />
										<input type="hidden" id="teacherId" name="teacherId" />
										<input type="hidden" id="eligibilityId" name="eligibilityId" />
										<input type="hidden" id="jobId" name="jobId"/>
							    		
							  		
							  	</td>
							  </tr>
							  <tr><td>&nbsp;</td></tr>
							  <tr>
							    <td>
							    	<a href="javascript:void(0);" onclick="saveNotesONB()" style='font-size:16px;'><b><spring:message code="lnkImD"/></b></a>&nbsp;&nbsp;&nbsp;&nbsp;
							    </td>
							    <td>
							    	<a href="javascript:void(0);" onclick="cancelDivONB()"><spring:message code="btnClr"/></a>
							    </td>
							  </tr>
						</table>
					</div>
		 	</div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="onb_popupHide();"><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
</div>


<!------------------------------------------Added for Teacher profile--------------------------------------------->

<input type="hidden"  name="checkboxshowHideFlag" id="checkboxshowHideFlag"/>
<input type="hidden" name="teachersharedId" id="teachersharedId"/>
<input type="hidden" name="pageFlag" id="pageFlag" value="0"/>
<input type="hidden" name="userFPD" id="userFPD"/>
<input type="hidden" name="userFPS" id="userFPS"/>
<input type="hidden" name="userCPD" id="userCPD"/>
<input type="hidden" name="userCPS" id="userCPS"/>
<input type="hidden" name="folderId" id="folderId"/>
<input type="hidden" name="teacherIdForprofileGrid" id="teacherIdForprofileGrid"/>
<input type="hidden" name="teacherIdForprofileGridVisitLocation" id="teacherIdForprofileGridVisitLocation" value="CG View" />
<input type="hidden" name="hideIcons" id="hideIcons" value="-1"/>
<input type="hidden" name="teacherDetailId" id="teacherDetailId">
<input type="hidden" name="phoneType" id=phoneType value="0"/>
<input type="hidden" name="msgType" id=msgType value="0"/>
<input type="hidden" name="jobForTeacherGId" id="jobForTeacherGId" value=""/>
<input type="hidden" name="commDivFlag" id="commDivFlag" value=""/>
<input type="hidden" name="commDivFlagPnrCheck" id="commDivFlagPnrCheck" value="1"/>

<div  class="modal hide" id="myModalProfileVisitHistoryShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:100px;" id="mydiv">
		<div class='modal-content'>
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
			<h3 id="myModalLabel"><spring:message code="msgWhAdminCanHiCandFrThJoOrdAppPo"/></h3>
		</div>
		<div class="modal-body" style="max-height: 400px;overflow-y:scroll;padding-right: 18px;">		
			<div class="control-group">
				<div id="divteacherprofilevisithistory" class="">
			    		        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
	 	</div>
		</div>
	</div>
</div>


<div class="modal hide custom-div-border1" id="draggableDivMaster"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div id="draggableDiv"></div>
</div>
<input type="hidden" id="pageNumberSet" name="pageNumberSet" value="1" />


<!--  Communication Div For User Profile -->
<div  class="modal hide"  id="myModalCommunications" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 700px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForSave();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headCom"/></h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div class="" id="divCommTxt">
			</div> 
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" aria-hidden="true" onclick='printCommunicationslogs();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick='showCommunicationsDivForSave();'><spring:message code="btnClose"/></button> 		
 	</div>
   </div>
 </div>
</div>


<!-- -------------------for folder  -- ------------------->
<div class="modal hide"  id="saveToFolderDiv"   tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog" style="width:900px;">
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="defaultTeacherGrid()";>x</button>
		<h3 id="myModalLabel"><spring:message code="headSaveCandidate"/></h3>
	</div>
	<div class="modal-body" style="max-height: 500px;overflow-y: scroll;"> 	 		
		<div class="control-group">
		<div  style="border: 0px solid green;width:220px;height:450px;float: left;" class="span5">	
		<div class="span5" id="tree_menu" style=" padding: 8px 0px 10px 2px; " >
			<a data-original-title='Create' rel='tooltip' id='createIcon'><span id="btnAddCode" style="cursor: pointer;" class='icon-folder-open  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Rename' rel='tooltip' id='renameIcon'><span id="renameFolder" class='icon-edit  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Cut' rel='tooltip' id='cutIcon'><span id="cutFolder" class='icon-cut  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Copy' rel='tooltip' id='copyIcon'><span id="copyFolder" class='icon-copy  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Paste' rel='tooltip' id='pasteIcon'><span id="pasteFolder" class='icon-paste  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Delete' rel='tooltip' id='deleteIcon'><span id="deletFolder" class='icon-remove-sign  icon-large iconcolor'></span></a>
		</div>
		<iframe id="iframeSaveCandidate"  src="tree.do" class="pull-left" scrolling="auto"  frameBorder="1" style="border: 0px; padding: 0px; margin: 0px; overflow:visible; text-align: top; vertical-align: top;width:220px;height:420px;"></iframe>
	</div>
	<%-- ======================================== Right Div ========================================================================= --%>			
		<div class="span10" style="padding-top:39px; border: 0px solid green;float: left;">
				<input type="hidden" id="savecandidatearray" name="savecandidatearray">
				<input type="hidden" id="txtoverrideFolderId" name="txtoverrideFolderId">
				<input type="hidden" id="teachetIdFromPoPUp" name="teachetIdFromPoPUp" value="">
				<input type="hidden" id="teacherIdForHover" name="teacherIdForHover" value="">
				
				<input type="hidden" id="txtflagpopover" name="txtflagpopover" value=""> <%-- If It is 1 -> that means User Clicked on Pop up --%>
				<div id="savedCandidateGrid">
				
				</div>
		</div>
		<div style="clear: both"></div>	
		</div>
		
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="saveCandidateToFolderByUserForONB()" ><spring:message code="btnSave"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="defaultTeacherGrid()";><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>
<div class="hide">
<input type="hidden" id="jobforteacherId" name="jobforteacherId"/>
</div>

<!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadJobList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
				<h3 id="myModalLabel">Onboarding Dashboard</h3>
			</div>
			<div class="modal-body">		
				<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px"></iframe>        	
					</div>
				</div>
	 		</div>
	 		<div class="modal-footer">
	 			<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
	 		</div>
     	</div>
	</div>
</div>
<!-- PDF End -->

<!-- Excel Start -->
<div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
	 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
	 </iframe>  
 </div> 
<!-- Excel End -->
<!------------------------------------------Ended for Teacher profile--------------------------------------------->


<!------------------------------------------Added for Message--------------------------------------------->
<!--  Message Div -->
<div class="modal hide"  id="myModalMessage"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:690px">
	<div class="modal-content">
	<input type="hidden" id="emailId" name="emailId">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headPostMsgToCandidate"/></h3>
	</div>
	<iframe id='uploadMessageFrameID' name='uploadMessageFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	<form id='frmMessageUpload' enctype='multipart/form-data' method='post' target='uploadMessageFrame'  class="form-inline" onsubmit="return validateMessage();" action='messageCGUploadServlet.do' accept-charset="UTF-8">
	
	<div class="modal-body" style="max-height: 450px;overflow-y:auto;">
		<div class="" id="divMessages">						
		</div>
		<div class="control-group" style="margin-top: 5px;">
			<div class='divErrorMsg' id='errordivMessage' style="display: block;"></div>
		</div>
		
		<%---------- Gagan :District wise Dynamic Template [Start]  --%>
			<div id="templateDiv" style="display: none;">		     	
		    	<div class="row col-md-12" >
		    	<div class="row col-sm-6" style="max-width:300px;">
			    	<label><strong>Template</strong></label>
		        	<br/>
		        	<select class="form-control" id="districttemplate" onchange="setTemplate()">
		        		<option value="0"><spring:message code="sltTemplate"/></option>
		        	</select>
		         </div>	
		        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="btn btn-primary hide top26" id="btnChangetemplatelink" onclick="return getTemplate();"><spring:message code="btnAplyTemplate"/></button> <input type="hidden" class="span1" id="confirmFlagforChangeTemplate" name="confirmFlagforChangeTemplate" value="0">
			
		    	</div>
		    </div>
		<%---------- Gagan :District wise Dynamic Template [END]  --%>
		
		<div class="control-group row" style="padding-left: 15px;padding-top: 5px;">
    		<label><strong><spring:message code="lblTo"/><br/></strong><span id="emailDiv" style="width:612px;word-wrap:break-word;padding-left:1px;"></span>
   		</div>
		<div id='support' class="row" style="padding-left:15px;" >
		<div class="control-group">
			<div class="">
		    	<label><strong><spring:message code="lblSub"/></strong><span class="required">*</span></label>
	        	<input id="messageSubject" name="messageSubject" type="text" class="form-control" style='width:612px;' maxlength="100" />
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="messageSend" style="width: 612px;">
		    	<label><strong><spring:message code="lblMsg"/></strong><span class="required">*</span></label>
		    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/>
 				</label>
	        	<textarea rows="5" class="form-control" cols="" id="msg" name="msg" maxlength="1000"></textarea>
	        </div>
	        <input type="hidden" id="teacherIdForMessage" name="teacherIdForMessage" value="">
    		<input type="hidden"  name="messageDateTime" id="messageDateTime" value="${dateTime}"/>
	    	<div id='fileMessages' style="padding-top:12px;">
        		<a href='javascript:void(0);' onclick='addMessageFileType();'>
        		<img src='images/attach.png'/></a> 
        		<a href='javascript:void(0);' onclick='addMessageFileType();'><spring:message code="lnkAttachFile"/></a>
        	</div>   
		</div>
 		</div>
 		<div id='lodingImage' style="display: block;text-align:center;padding-top:4px;"></div>
 		<!-- end support div -->
  	</div>
  	
  	</form>
 	<c:set var="chkSendDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSendDisp" value="none" />
			</c:when>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSendDisp" value="inline" />
			</c:when>
			<c:otherwise>
				<c:set var="chkSendDisp" value="inline" />
			</c:otherwise>
	</c:choose>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'><spring:message code="btnClr"/></button>&nbsp;
 		<button class="btn btn-primary" onclick="validateMessage()" style="display: ${chkSendDisp}"><spring:message code="btnSend"/></button>&nbsp;
 	</div>
</div>
</div>
</div>

<!------------------------------------------Ended for Message--------------------------------------------->



<!-- --------------------------------------------------Unknown ------------------------------------------->
<!--  Starts Notes DIV -->
<input type="hidden" id="noteId" name="noteId" value=""/>
<div class="modal hide" id="myModalNotes"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
<input type="hidden" id="teacherId" name="teacherId" value="">
	<div class="modal-dialog" style="width:935px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='showCommunicationsDiv();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headNot"/></h3>
	</div>
	
	<div class="modal-body">	
		<div class="row" id="divNotes" style="padding-left: 15px; ">						
		</div>
		<iframe id='uploadNoteFrameID' name='uploadNoteFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='frmNoteUpload' enctype='multipart/form-data' method='post' target='uploadNoteFrame'  class="form-inline" onsubmit="return saveNotes();" action='noteUploadServlet.do' accept-charset="UTF-8">
			
		<div class="mt10">
			<div class='span10 divErrorMsg' id='errordivNotes' style="display: block;"></div>
			<div class="span10" >
		    	<label><strong><spring:message code="lblEtrNot"/><span class="required">*</span></strong></label>
		    	<div class="span10" id='divTxtNode' style="padding-left: 0px; margin-left: 0px;" >
		    		<textarea style="width: 100%" readonly id="txtNotes" name="txtNotes" class="span10" rows="4"></textarea>
		    	</div>  
		    	<input type="hidden" id="teacherIdForNote" name="teacherIdForNote" value="">
		    	<input type="hidden"  name="noteDateTime" id="noteDateTime" value="${dateTime}"/>
		    	<div id='fileNotes' style="padding-top:10px;">
	        		<a href='javascript:void(0);' onclick='addNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addNoteFileType();'><spring:message code="lnkAttachFile"/></a>
	        	</div>      	
			</div>						
		</div>
		</form>
 	</div>
 	 	
 	<div class="modal-footer">
 		<c:set var="chkSaveDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSaveDisp" value="none" />
			</c:when>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSaveDisp" value="inline" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSaveDisp" value="none" />
			</c:otherwise>
		</c:choose>
 		<!-- <span id="spnBtnCancel" style="display: ${chkSaveDisp}"><a href="#"	onclick="return cancelNotes()"><spring:message code="btnClr"/></a></span> -->
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='showCommunicationsDiv();'><spring:message code="btnClr"/></button>&nbsp; 
 		<span id="spnBtnSave" style="display: ${chkSaveDisp}"><button class="btn btn-primary"  onclick="saveNotes()"><spring:message code="btnSave"/> <i class="icon"></i></button>&nbsp;</span>
 	</div>
   </div>
</div>
</div>
<!------------------------------------------- ended unknown ------------------------------------------->

<!------------------------------------------- Start :: Qualification Questions ------------------------------------------->
<div class="modal hide" id="qualificationDiv"
	style="border: 0px solid blue;" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 520px;">
		<div class='modal-content'>
			<div class="modal-header" id="qualificationDivHeader">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick=''>
					x
				</button>
				<h3>
				<spring:message code="headQualif"/>
				</h3>
			</div>

			<div class="modal-body"
				style="max-height: 400px; padding-top: 0px; overflow-y: scroll;">
				<input type="hidden" id="teacherIdforPrint" name="teacherIdforPrint" />
				<input type="hidden" id="districtIdforPrint" name="districtIdforPrint" />
				<input type="hidden" id="headQuarterIdforPrint" name="headQuarterIdforPrint" />
				<input type="hidden" id="branchIdforPrint" name="branchIdforPrint" />
				<div class='divErrorMsg' id='errorQStatus' style="display: block;"></div>
				<div id="qualificationDivBody"></div>
			</div>
			<div class="modal-footer">
				<table width=470 border=0>
					<tr>
						<td width=100 nowrap align=left>
<!--							<span id="qStatusSave"><button-->
<!--									class="btn  btn-large btn-orange"-->
<!--									onclick='saveQualificationNoteForTP(0)'>-->
<!--									<spring:message code="btnIn-Progress"/>-->
<!--								</button>&nbsp;&nbsp;</span>-->
						</td>

						<td width=200 nowrap>
							<span id="qStatusFinalize"><button
									class="btn  btn-large btn-primary"
									onclick='saveQualificationNoteForONB(1)'>
									<spring:message code="btnFinalize"/>
									<i class="iconlock"></i>
								</button>&nbsp;&nbsp;</span>
								<button class="btn btn-large btn-primary" data-dismiss="modal"
								aria-hidden="true" onclick="printQualificationDivForONB();">
								<spring:message code="btnP"/>
							</button>
							
							<button class="btn btn-large" data-dismiss="modal"
								aria-hidden="true" onclick='cancelQualificationIssuse();'>
								<spring:message code="btnClr"/>
							</button>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<!------------------------------------------- End :: Qualification Questions ------------------------------------------->