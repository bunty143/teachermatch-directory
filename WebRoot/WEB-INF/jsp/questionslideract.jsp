<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
   <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
   <script src="js/jquery.js"></script>
   <script src="js/jquery-ui.js"></script>
   <link rel="stylesheet" href="css/jquery.ui.labeledslider.css">
   <script src="js/jquery.ui.labeledslider.js"></script>
   
   <script type="text/javascript" src="dwr/engine.js"></script>
	<script type='text/javascript' src='dwr/util.js'></script>
   <script type='text/javascript' src='js/report/cgstatus.js?ver=${resourceMap["js/report/cgstatus.js"]}'></script>
   <script type="text/javascript" src="dwr/interface/ManageStatusAjax.js?ver=${resourceMap['ManageStatusAjax.Ajax']}"></script>
   
      <style>

   *{
      font-size:     8pt;
   }
   .ui-spinner { width: 4em; }

   </style>
   <script>
  $(document).ready(function(){
       $('#frmQslider').labeledslider({ 
       	  value:${svalue},
		  max: ${max},
		  tickInterval:${tickInterval},
		  range:"min",  
		  step:${step},
		  slide: function( event, ui ) { 
		  document.getElementById('${hiddenval}').value=ui.value;
		  }
	  });
	  if(${dslider}==0){
	  	$('#frmQslider').labeledslider( 'disable');
	  }
   });
   
   function handleError(message, exception)
	{
		if(exception.javaClassName=="java.lang.IllegalStateException")
		{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
		else{alert("Server Error: "+exception.javaClassName);}
	}
   
   function saveStatusSpecificScore()
   {
   	updateStatusSpecificScore(${answerId},${max},document.getElementById('${hiddenval}').value);
   }
   </script>
   
<table border=0 cellpadding="0" cellspacing="0"><tr><td><div id="frmQslider" style='width:${swidth};' onclick="saveStatusSpecificScore()"></div></td><td  style='text-align: right;padding-left:12px;height:54px; ' valign="top">
<input style='width:25px;border: 0px none;' readonly="readonly" type='text' name='${hiddenval}' id='${hiddenval}' value='${svalue}'>
</td></tr></table>


