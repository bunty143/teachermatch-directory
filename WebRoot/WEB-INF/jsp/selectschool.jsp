<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/SchoolSelectionAjax.js?ver=${resourceMap['SchoolSelectionAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/teacher/selectschool.js?ver=${resourceMap['js/teacher/selectschool.js']}"></script>

<script type="text/javascript">
var $j=jQuery.noConflict();
$j(document).ready(function(){});
function applySelectedScrolOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridSelectedSchoolJob').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[361,270,125,65,150], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
</script>

<!-- End ... Dynamic Portfolio -->

<link rel="stylesheet" type="text/css" href="css/base.css" />  

	<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
	     <div style="float: left;">
	     	<img src="images/applyfor-job.png" width="41" height="41" alt="">
	     </div>        
	     <div style="float: left;">
	     	<div class="subheading" style="font-size: 13px;"><spring:message code="headJobToSltSchl"/></div>	
	     </div>		
		<div style="clear: both;"></div>	
		<div class="centerline"></div>
	</div>
	 <div class="TableContent top15">        	
	     <div class="table-responsive" id="divSelectedSchoolList"></div>            
	</div> 

	<div style="display:none;" id="loadingDiv">
		<table  align="center">
				<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
				<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
				<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
		</table>
	</div>
<script type="text/javascript">
	getSelectedSchoolJobs();
</script>

