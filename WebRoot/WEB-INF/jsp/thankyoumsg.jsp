
<!--
@author Amit 
@date 11-Mar-15
-->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- @author Amit @date 11-Mar-15 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script type="text/javascript" language="javascript" src="../dwr/engine.js"></script>
<script type="text/javascript" language="javascript" src='../dwr/util.js'></script>


<div class="row offset4 msgPage" style="padding-right: 175px; padding-left: 175px;">
   <div class="span8 mt30" style="text-align: center;">
	
	  <div align="left" style = "background-color: white; box-shadow: 2px 2px 5px #888888; border-radius: 5px;">
		<div class="accordion-heading" > <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle collapsed"> <spring:message code="lblSignedOut11"/> </a> </div><br/>
		  <div style="height: 60px;">
			<b style="margin-left: 80px" ><spring:message code="msgSignedOudTm"/></b><br>

			<c:choose>
				<c:when test="${isPhili}">
					<!-- <button class="btn fl btn-primary" type="button" onclick="javascript:window.close();">Close <i class="icon"></i></button> -->
				</c:when>
				<c:when test="${isSmartPractices}">
					<a href="smartpractices.do" style="margin-left: 80px"><spring:message code="lnkRtnToSignInPsge"/></a>
				</c:when>
				<c:otherwise >
				<a  href="signin.do" style="margin-left: 80px;" ><spring:message code="lnkRtnToSignInPsge"/></a><br/>
				</c:otherwise>
			</c:choose>
			</div>
		</div>
	</div>
</div>
