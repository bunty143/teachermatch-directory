<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
 <%@page buffer="1500kb" autoFlush="true" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="/js/teacherinfonew.js?v=1"></script>
<c:set var="resourceMap" value="${applicationScope.resouceMap}" />
<script src="js/LAB.min.js" async></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/CGServiceAjax.js?ver=${resouceMap['CGServiceAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherInfotAjax.js?ver=${resouceMap['TeacherInfoAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherProfileViewInDivAjax.js?ver=${resouceMap['TeacherProfileViewInDivAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateGridAjaxNew.js?ver=${resouceMap['CandidateGridAjaxNew.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/MassStatusUpdateService.js?ver=${resouceMap['MassStatusUpdateService.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageStatusAjax.js?ver=${resouceMap['ManageStatusAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PortfolioReminderAjax.js?ver=${resouceMap['PortfolioReminderAjax.ajax']}"></script>
<script type='text/javascript' src="js/report/candidategridnew.js?ver=${resouceMap['js/report/candidategridnew.js']}"></script>
<script type='text/javascript' src="js/certtypeautocomplete.js?ver=${resouceMap['js/certtypeautocomplete.js']}"></script>
<script type="text/javascript" src="js/massStatusUpdate/massstatusupdate.js?ver=${resouceMap['js/massStatusUpdate/massstatusupdate.js']}"></script>
<script type="text/javascript" src="js/searchjoborder.js?ver=${resouceMap['js/searchjoborder.js']}"></script>
<script type='text/javascript' src="js/teacherinfonew.js?ver=${resouceMap['js/teacherinfonew.js']}"></script>
<script type="text/javascript" src="js/compress/c_jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<script type='text/javascript' src="js/report/cgstatusNew.js?ver=${resouceMap['js/report/cgstatusNew.js']}"></script>
<script type='text/javascript' src="js/report/schoolAutoSuggestForDJO.js?ver=${resouceMap['js/report/schoolAutoSuggestForDJO.js']}"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.ajax']}"></script>
<script type='text/javascript' src="js/branchautoSuggest.js?ver=${resourceMap['js/branchautoSuggest.js']}"></script>
<script type="text/javascript" src="dwr/interface/PrintCertificateAjax.js?ver=${resourceMap['PrintCertificateAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateGridSubAjax.js?ver=${resouceMap['CandidateGridSubAjax.ajax']}"></script>

<script type='text/javascript' src="js/report/sspfCandidategridnew.js?ver=${resourceMap['js/report/sspfCandidategridnew.js']}"></script>
<script type="text/javascript" src="dwr/interface/SelfServiceCandidateProfileService.js?ver=${resourceMap['SelfServiceCandidateProfileService.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax .ajax']}"></script>
<style>
textarea {
	width: 420px;
	height: 100px;
}
.table-bordered {
	border-collapse: separate;
	border-color: #cccccc;
}
.table-bordered td,th {
	border-left: 0px solid #dddddd;
}
.tdscore0 {
	width: 1px;
	height: 40px;
	float: left;
}
.tddetails0 {
	width: 1px;
	height: 40px;
	float: left;
}
tr.bgimage {
	background-image: url(images/color-gradients.png);
	background-repeat: no-repeat no-repeat;
}
.dropdown-menu {
	min-width: 40px;
}
.popover-title {
	margin: 0;
	padding: 0px 0px 0px 0px;
	font-size: 0px;
	line-height: 0px;
}
.popover-content {
	margin: 0;
	padding: 0px 0px 0px 0px;
}
.popover.right .arrow {
	top: 100px;
	left: -10px;
	margin-top: -10px;
	border-width: 10px 10px 10px 0;
	border-right-color: #ffffff;
}
.popover.right .arrow:after {
	border-width: 11px 11px 11px 0;
	border-right-color: #007AB4;;
	bottom: -11px;
	left: -1px;
}
.pull-left {
	margin-left: -52px;
	margin-top: 7px;
}
.tblborder {
	
}
.nobground2 {
	padding: 6px 7px 6px 7px;
	-webkit-border-top-left-radius: 30px;
	border-top-left-radius: 30px;
	-moz-border-radius-topleft: 30px;
	-webkit-border-top-right-radius: 30px;
	border-top-right-radius: 30px;
	-moz-border-radius-topright: 30px;
	-webkit-border-bottom-right-radius: 30px;
	border-bottom-right-radius: 30px;
	-moz-border-radius-bottomright: 30px;
	-webkit-border-bottom-left-radius: 30px;
	border-bottom-left-radius: 30px;
	-moz-border-radius-bottomright: 30px;
}
.nobground1 {
	padding: 6px 10px 6px 10px;
	-webkit-border-top-left-radius: 30px;
	border-top-left-radius: 30px;
	-moz-border-radius-topleft: 30px;
	-webkit-border-top-right-radius: 30px;
	border-top-right-radius: 30px;
	-moz-border-radius-topright: 30px;
	-webkit-border-bottom-right-radius: 30px;
	border-bottom-right-radius: 30px;
	-moz-border-radius-bottomright: 30px;
	-webkit-border-bottom-left-radius: 30px;
	border-bottom-left-radius: 30px;
	-moz-border-radius-bottomright: 30px;
}
.nobground3 {
	padding: 6px 4px 6px 4px;
	-webkit-border-top-left-radius: 30px;
	border-top-left-radius: 30px;
	-moz-border-radius-topleft: 30px;
	-webkit-border-top-right-radius: 30px;
	border-top-right-radius: 30px;
	-moz-border-radius-topright: 30px;
	-webkit-border-bottom-right-radius: 30px;
	border-bottom-right-radius: 30px;
	-moz-border-radius-bottomright: 30px;
	-webkit-border-bottom-left-radius: 30px;
	border-bottom-left-radius: 30px;
	-moz-border-radius-bottomright: 30px;
}
.icon-ok-circle {
	font-size: 3em;
	color: #00FF00;
}
.icon-circle {
	font-size: 3em;
	color: #E46C0A;
}
.icon-remove-circle {
	font-size: 3em;
	color: red;
}
.icon-circle-blank {
	font-size: 3em;
	color: red;
}
.icon-question-sign {
	font-size: 1.3em;
}
.icon-inner {
	font-size: 2.3333333333333333em;
	font-family: 'Bauhaus 93';
	color: red;
	margin-left: -26px;
	vertical-align: 5%;
}
.icon-inner2 {
	font-size: 2.3333333333333333em;
	font-family: 'Bauhaus 93';
	color: red;
	margin-left: -24px;
	vertical-align: 8%;
}
.circle {
	width: 48px;
	height: 48px;
	margin: 0em auto;
}
.subheading {
	font-weight: none;
}
.icon-folder-open,icon-copy,icon-cut,icon-paste,icon-remove-sign,icon-edit
	{
	color: #007AB4;
}
.marginleft20 {
	margin-left: -20px;
}
.margintop20 {
	margin-top: -20px;
}
div.t_fixed_header_main_wrapper {
	position: relative;
	overflow: visible;
}
.modal_header_profile {
	border-bottom: 1px solid #eee;
	background-color: #0078b4;
	text-overflow: ellipsis;
	border: 1px solid #999;
	border: 1px solid rgba(0, 0, 0, 0.3); *
	border: 1px solid #999;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	outline: none;
	-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-webkit-background-clip: padding-box;
	-moz-background-clip: padding-box;
	background-clip: padding-box;
}
.scrollspy_profile {
	height: 500px;
	overflow-y: auto;
	overflow-x: hidden;
	padding-left: 5px;
}
.divwidth {
	width: 728px;
}
.tablewidth {
	width: 900px;
}
.net-widget-footer {
	border-bottom: 1px solid #cccccc;
	border-left: 1px solid #cccccc;
	border-right: 1px solid #cccccc;
	line-height: 40px;
	background-color: #F2FAEF;
	background-image: -moz-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#FFFFFF),
		to(#FFFFFF) );
	background-image: -webkit-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: -o-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: linear-gradient(to bottom, #FFFFFF, #FFFFFF);
	background-repeat: repeat-x;
	color: #4D4D4E;
	vertical-align: middle;
}
.modaljob {
	position: fixed;
	top: 40%;
	left: 45%;
	z-index: 2000;
	overflow: auto;
	width: 980px;
	margin: -250px 0 0 -440px;
	background-color: #ffffff;
	border: 1px solid #999;
	border: 1px solid rgba(0, 0, 0, 0.3); *
	border: 1px solid #999;
	/* IE6-7 */
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-webkit-background-clip: padding-box;
	-moz-background-clip: padding-box;
	background-clip: padding-box;
}
.net-corner-bottom {
	-moz-border-radius-bottomleft: 12px;
	-webkit-border-bottom-left-radius: 12px;
	border-bottom-left-radius: 12px;
	-moz-border-radius-bottomright: 12px;
	-webkit-border-bottom-right-radius: 12px;
	border-bottom-right-radius: 12px;
}
.custom-div-border1 {
	border: 1px solid #999;
	border: 1px solid rgba(0, 0, 0, 0.3);
	-moz-border-radius: 6px;
	border-radius: 6px;
}
.modal-border { /*border: 1px solid #0A619A;*/
	
}
.custom-div-border {
	padding: 1px;
	border-bottom-left-radius: 2em;
	border-bottom-right-radius: 2em;
}
.pdfDivBorder {
	z-index: 5000;
	border: 1px solid #999;
	border: 1px solid rgba(0, 0, 0, 0.3);
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-webkit-background-clip: padding-box;
	-moz-background-clip: padding-box;
	background-clip: padding-box;
}
.custom-border {
	margin-left: -32px;
	margin-top: 2px;
}
.dropdown-menu a {
	display: block;
	padding: 3px 15px;
	margin-padding: 100px;
	clear: both;
	font-weight: normal;
	line-height: 20px;
	color: #333333;
	white-space: nowrap;
}
.dropdown-menu a:hover {
	color: #007AB4;
}
.status-notes-image {
	margin-left: 40px;
}
.status-notes-text {
	margin-top: -20px;
	margin-left: 80px;
}

.profileContent
{
	padding: 0px;
	padding-left: 12px;
	padding-top: 10px;
	color: #474747;
	font-weight: normal;
	font-size: 10px;
}

/*added by Ram nath*/
#divStatusNoteGrid .row, #divStatusNoteGrid .jqte{
	width: 100% !important;
}
/*ended by Ram nath*/
.portfolio-dialog {
    left: 50%;
    right: auto;
    width: 600px;
    padding-top: 0px;
    padding-left: 10px;
    padding-right: 10px;
    padding-bottom: 30px;
    margin-left: auto;
    margin-right: auto;
    z-index: 1050;
}
#content_del{
	table-layout:fixed; 
	overflow:auto;
	word-break: break-all;
}
#content_del td{
	vertical-align:top;
}
</style>
<c:set var="numcols" value="0" />

<c:if test="${prefMap['achievementScore']}">
	<c:set var="numcols" value="${numcols + 2}" />
</c:if>
<c:if test="${prefMap['tFA']}">
	<c:set var="numcols" value="${numcols + 1}" />
</c:if>
<c:if test="${prefMap['expectedSalary']}">
	<c:set var="numcols" value="${numcols + 1}" />
</c:if>
<c:if test="${prefMap['teachingOfYear']}">
	<c:set var="numcols" value="${numcols + 1}" />
</c:if>
<c:if test="${prefMap['NFScore']}">
	<c:set var="numcols" value="${numcols + 1}" />
</c:if>
<c:if test="${prefMap['MAScore']}">
	<c:set var="numcols" value="${numcols + 1}" />
</c:if>
<script type="text/javascript"><!--


 function applyScrollOnLEACandidatePorfolio()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLEACandidatePorfolioGridD').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
       	<c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[200,100,200,150,100,80],
           </c:when>
           <c:otherwise>
             colratio:[200,100,150,150,80,50],
           </c:otherwise>
        </c:choose>
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}


 function applyScrollOnTblLicenseDate()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLicneseGridCertificationsGridD').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[400,220,210],
           </c:when>
           <c:otherwise>
             colratio:[350,220,160],
           </c:otherwise>
        </c:choose>
       	addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}




function applyScrollOnHonors()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j("#honorsGrid").fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         // table header width
        
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[830],
           </c:when>
           <c:otherwise>
             colratio:[730],
           </c:otherwise>
        </c:choose>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });         
}
function applyScrollOnTblSubjectAreaExam()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#subjectAreasGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
          //colratio:[148,222,111,119,111],
          
           <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[80,100,200,100,350],
           </c:when>
           <c:otherwise>
	         colratio:[148,278,90,103,111],
	       </c:otherwise>
       </c:choose>
          
          
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
function applyScrollOnInvl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j("#involvementGrid").fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         // table header width
        
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[245,245,170,170],
           </c:when>
           <c:otherwise>
             colratio:[220,220,145,145],
           </c:otherwise>
        </c:choose>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });
}

var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });

function applyScrollOnTblGeoZoneSchool()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#geozoneSchoolTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 588,
        minWidth: null,
        minWidthAuto: false,
        //colratio:[55,266,70,90,100,105,70,66,151], // table header width
        //mukesh to set the table
        colratio:[380,250],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
} 
function applyScrollOnTblLanguage()
{
    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#teacherLanguageGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         // table header width
        
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[277,276,276],
           </c:when>
           <c:otherwise>
             colratio:[244,243,243],
           </c:otherwise>
        </c:choose>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });         
}


function applyScrollOnStatusNotes(id)
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#'+id).fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 670,
        minWidth: null,
        minWidthAuto: false,
        colratio:[250,110,110,120,500], // table header width
        // colratio:[220,100,120,120,130], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}



function applyScrollOnTbl()
{//alert("${noEPIFlag}" +" LLL " + "${numcols}");
	    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#teacherTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 280,        
        width: 940,    
        minWidth: null,
        minWidthAuto: false,    
      //roleId
		<c:if test="${entityID==1}">
               colratio:[300,110,60,100,90,100,80,110],
        </c:if>
        <c:if test="${entityID==5 || entityID==6}">
               colratio:[400,110,100,100,200],
        </c:if>
               
        <c:if test="${numcols==1 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==true}">
          colratio:[380,150,150,150,139],
        </c:if>
       <c:if test="${numcols==1 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==false}">
          colratio:[410,200,200,159],
        </c:if>
        
		<c:if test="${numcols==2 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==true}">
         colratio:[340,115,120,125,135,103],
        </c:if>
        <c:if test="${numcols==2 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==false}">
         colratio:[363,143,148,158,126],
        </c:if>
        /*
        <c:if test="${numcols==2 && entityID==3 && noEPIFlag==true}">
         colratio:[340,150,150,150,150],
        </c:if>
        <c:if test="${numcols==2 && entityID==3 && noEPIFlag==false}">
         colratio:[370,190,190,190],
        </c:if>
        
        <c:if test="${numcols==1 && entityID==3 && noEPIFlag==true}">
          colratio:[340,200,200,200],
        </c:if>
        <c:if test="${numcols==1 && entityID==3 && noEPIFlag==false}">
          colratio:[440,250,250],
        </c:if>
        */
        <c:if test="${numcols==3 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==true}">
         colratio:[310,110,90,105,110,130,103],
        </c:if>
        <c:if test="${numcols==3 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==false}">
         colratio:[320,110,125,130,150,123],
        </c:if>
        
         <c:if test="${numcols==4 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==true}">
          colratio:[310,90,75,85,105,100,90,100],
        </c:if>
		 <c:if test="${numcols==4 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==false}">
          colratio:[340,85,95,115,110,100,110],
        </c:if>
        
        <c:if test="${numcols==7 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==true}">
           colratio:[260,70,70,60,60,60,50,70,70,70,100],
        </c:if>   
        <c:if test="${numcols==7 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==false}">
          colratio:[280,75,65,65,75,50,75,75,75,105],
        </c:if>
		<c:if test="${numcols==6 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==true}">
          colratio:[260,70,70,70,70,70,70,70,90,100],
        </c:if>   
        <c:if test="${numcols==6 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==false}">
          colratio:[280,75,75,75,75,75,75,95,115],
        </c:if>
       
        <c:if test="${numcols==5 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==true}">
           colratio:[280,90,90,90,50,85,85,80,100],
        </c:if>
         <c:if test="${numcols==5 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==false}">
           colratio:[300,100,100,60,95,95,90,110],
        </c:if>
        
		<c:if test="${numcols==0 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==true}">
          colratio:[300,310,220,100],
        </c:if>
        <c:if test="${numcols==0 && entityID!=1 && entityID!=5 && entityID!=6 && noEPIFlag==false}">
          colratio:[410,320,200],
        </c:if>
	    addTitles: false,
        zebra: true,
        //zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTranscript()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTrans').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 875,
        minWidth: null,
        minWidthAuto: false,
        colratio:[150,150,150,150,150,125], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnCertification()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblCert').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 875,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,50,350,200,175], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnJob()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#jobTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 235,
        width: 950,
        minWidth: null,
        minWidthAuto: false,
       // colratio:[35,35,200,100,120,80,100,75,100,110], // table header width
        
        <c:choose>
    	<c:when test="${entityID==1 || entityID==5}">
         colratio:[60,55,185,110,120,80,100,65,70,110], // table header width
        </c:when>
        <c:otherwise>
        <c:choose>
		<c:when test="${prefMap['fitScore'] && prefMap['demoClass']}">
        colratio:[60,55,185,110,130,80,100,65,70,110],
        </c:when>
        <c:when test="${!prefMap['fitScore'] && !prefMap['demoClass']}">
        colratio:[60,55,290,110,130,80,110,120],
        </c:when>
        <c:otherwise>
        colratio:[50,60,150,60,90,110,80,110,60,80,100],
        </c:otherwise>
        </c:choose>
        </c:otherwise>
     </c:choose>
     
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 50,
        wrapper: false
        });
            
        });		
}

function applyScrollOnTblWorkExp()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblWorkExp_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        
       <c:choose>
          <c:when test="${entityID==2 ||entityID==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        //colratio:[148,222,111,119,111],
       // colratio:[148,278,90,103,111],
        <c:choose>
          <c:when test="${entityID==2 ||entityID==3}">
             colratio:[168,298,110,123,131],
           </c:when>
           <c:otherwise>
	         colratio:[148,278,90,103,111],
	       </c:otherwise>
       </c:choose>
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
//applyScrollOnTblWorkExp();
function applyScrollOnTblEleRef_profile()
{
		
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tbleleReferences_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
         <c:choose>
          <c:when test="${entityID==2 || entityID==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
          //colratio:[150,130,100,160,100,80],
          //colratio:[188,87,90,170,80,65,50],
         // colratio:[120,85,90,160,80,82,65,45],		//changed by Ashish 14-012014
         <c:choose>
          <c:when test="${(entityID==2 && DistrictId==7800038) || (entityID==3 && DistrictId==7800038)}">
             colratio:[160,130,160,135,90,45,110],
           </c:when>
           <c:when test="${(entityID==2 && DistrictId ne 7800038) || (entityID==3 && DistrictId ne 7800038)}">
             colratio:[110,75,90,150,80,85,90,50,100],
           </c:when>
            <c:when test="${(entityID==1)}">
              colratio:[120,100,100,105,70,85,80,70],
           </c:when>
           <c:otherwise>
	         colratio:[160,130,160,135,90,45,110],
	       </c:otherwise>
       </c:choose>
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
//applyScrollOnTblEleRef_profile();


function applyScrollOnTblVideoLinks_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblelevideoLink_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        <c:choose>
          <c:when test="${entityID==2 || entityID==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
          //colratio:[600,110],
         <c:choose>
          <c:when test="${entityID==2 || entityID==3}">
              colratio:[450,130,150,100],
           </c:when>
           <c:otherwise>
	          colratio:[450,130,150],
	       </c:otherwise>
       </c:choose>
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
//applyScrollOnTblVideoLinks_profile();

function applyScrollOnTbl_AdditionalDocuments()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
       // width: 728,
        <c:choose>
          <c:when test="${entityID==2 || entityID==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        //colratio:[500,228,],
         <c:choose>
          <c:when test="${entityID==2 || entityID==3}">
             colratio:[550,280],
           </c:when>
           <c:otherwise>
	         colratio:[500,230],
	       </c:otherwise>
       </c:choose>
        
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblBackgroundCheck_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tbleBgCheck_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
	    width: 830,
        minWidth: null,
        minWidthAuto: false,
	    colratio:[332,243,255],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}


function applyScrollOnTblProfileVisitHistory()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblProfileVisitHistory_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 500,
        width: 570,
        minWidth: null,
        minWidthAuto: false,
          colratio:[460,110],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
//applyScrollOnTblProfileVisitHistory();

function applyScrollOnTblTeacherAcademics_profile()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherAcademics_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        <c:choose>
          <c:when test="${entityID==2 ||entityID==3 }">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         <c:choose>
          <c:when test="${entityID==2 ||entityID==3}">
            colratio:[225,100,125,400],
           </c:when>
           <c:otherwise>
	        colratio:[410,180,60,60],
	       </c:otherwise>
       </c:choose>
       // colratio:[350,260,60,60],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}
//applyScrollOnTblTeacherAcademics_profile();

function applyScrollOnTblTeacherCertifications_profile()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherCertificates_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        <c:choose>
          <c:when test="${entityID==2 ||entityID==3 }">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        //  colratio:[500,230],
         <c:choose>
          <c:when test="${entityID==2 ||entityID==3}">
             colratio:[225,125,125,155,255],
           </c:when>
           <c:otherwise>
	         colratio:[200,100,100,130,200],
	       </c:otherwise>
       </c:choose>
	
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}
//applyScrollOnTblTeacherCertifications_profile();

function applyScrollOnJobsTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#jobTableLst').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 950,
        minWidth: null,
        minWidthAuto: false,
        
        <c:choose>
           <c:when test="${CheckNC=='2'}">
       	colratio:[30,70,140,55,160,100,110,160,125],
       	  </c:when>    
     <c:otherwise>
     	colratio:[30,70,55,290,100,110,160,130],
         </c:otherwise>
  </c:choose>
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}
function applyepiAndJsiTable()
{		
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#epiAndJsiTable').fixheadertable({ 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 635,
        minWidth: null,
        minWidthAuto: false,
        colratio:[180,120,150,185], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}


function applyScrollonActivityTable()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#onActivityTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        width: 500,
	    minWidth: null,
        minWidthAuto: false,
        olratio:[400,100],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTbl_AssessmentDetails()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentDetails').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        width: 830,
	    minWidth: null,
        minWidthAuto: false,
        olratio:[230,200,200,200],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}



function applyScrollOnEducation()
{
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#gridDataTeacherEducationsForLablel').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[200,200,220,210],
           </c:when>
           <c:otherwise>
             colratio:[200,170,200,160],
           </c:otherwise>
        </c:choose>
       	addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });		
}
function applyScrollOnAssessments()
{		
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentTable').fixheadertable({ 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        colratio:[180,120,150,185], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function applyScrollOnTblVerifyTranscript()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#transcriptVerificationTab').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[35,150,180,200,185,76],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });	
}
function applyScrollOnTb()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#schoolListGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 200,
        width: 680,
	    minWidth: null,
        minWidthAuto: false,
        olratio:[350,330],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
 --></script>

<style>
.icon-large2 {
	font-size: 2em;
}
.icon-large3 {
	font-size: 1.33em;
}
</style>
<c:if test="${DistrictName!=null}"><input type="hidden" id="showDistrictLabel" value="1" ></c:if>
<input type="hidden" id="schoolAddDistrictSpecificTag" name="schoolAddDistrictSpecificTag" value="${schoolAddDistrictSpecificTag}">
<input type="hidden" id="schoolRemoveDistrictSpecificTag" name="schoolRemoveDistrictSpecificTag" value="${schoolRemoveDistrictSpecificTag}">
<input type="hidden" id="schoolAddJobSpecificTag" name="schoolAddJobSpecificTag" value="${schoolAddJobSpecificTag}">
<input type="hidden" id="schoolRemoveJobSpecificTag" name="schoolRemoveJobSpecificTag" value="${schoolRemoveJobSpecificTag}">
<input type="hidden"  name="isReqNoForHiring" id="isReqNoForHiring" value="${isReqNoForHiring}"/>
<input type="hidden" id="referenceValue" value=""/>
<div class="row">
	<input type="hidden" id="teacherIdForprofileGrid" name="teacherIdForprofileGrid" />
	<input type="hidden" id="teacherIdForprofileGridVisitLocation"
		name="teacherIdForprofileGridVisitLocation" value="Teacher Pool" />
</div>
<input type="hidden" id="headQuarterId" value="${headQuarterMaster.headQuarterId}"/>
<input type="hidden" id="branchId" value="${branchMaster.branchId}"/>
<div class="row" style="margin-left: 0px; margin-right: 0px;">
	<div style="float: left;">
		<img src="images/districtjobs-orders.png" width="41" height="41"
			alt="">
	</div>
	<div style="float: left;">
		<div class="subheading" style="font-size: 13px;">
			<c:set var="hideSSN" value=""></c:set>
			<c:set var="empNoCss" value=""></c:set>
			<c:set var="empNo"><spring:message code="lblEmpNum"/></c:set>
			<c:set var="caption"><spring:message code="headTeachAppPool"/></c:set>
			<c:set var="candidateCaption"><spring:message code="lblCndAppForJobOurDistL"/></c:set>
			<c:set var="filterCaption"><spring:message code="msgFilterCandidatesAppFJob"/></c:set>
			<c:set var="distBranchFlag" value="1"></c:set>
			<c:set var="branchExistFlag" value="1"></c:set>
			
		<c:if test="${headQuarterMaster!=null || branchMaster!=null}">
			<c:set var="hideSSN" value="hide"></c:set>
			<c:set var="empNo" value="KSN ID"></c:set>
			<c:set var="caption" value="Kelly Applicant Pool"></c:set>
			<c:set var="candidateCaption" value="Talent that applied within the past"></c:set>
			<c:set var="filterCaption" value="Add filter for first-time applicants"></c:set>
			<c:set var="distBranchFlag" value="0"></c:set>
			
            <input type="hidden" id="districtName" name="districtName" value=""/>
			<input type="hidden" id="universityName" name="universityName" value=""/>
			<input type="hidden" id="degreeName" name="degreeName" value=""/>
			<input type="hidden" id="certType" name="certType" value=""/>
			<input type="hidden" id="schoolName" name="schoolName" value=""/>
			<input type="hidden" id="certificateTypeMaster" name="certificateTypeMaster" value=""/>
			<input type="hidden" id="universityId" name="universityId" value=""/>
			<input type="hidden" id="degreeId" name="degreeId" value=""/>
			<input type="hidden" id="districtId" name="districtId" value=""/>
			<input type="hidden" id="references" name="references" value=""/>
			<input type="hidden" id="resume" name="resume" value=""/>
			<input type="hidden" id="fitScoreSelectVal" name="fitScoreSelectVal" value=""/>
			<input type="hidden" id="YOTESelectVal" name="YOTESelectVal" value=""/>
			<input type="hidden" id="AScoreSelectVal" name="AScoreSelectVal" value=""/>
			<input type="hidden" id="LRScoreSelectVal" name="LRScoreSelectVal" value=""/>
			<input type="hidden" id="canServeAsSubTeacherYes" name="canServeAsSubTeacherYes" value=""/>
			<input type="hidden" id="canServeAsSubTeacherNo" name="canServeAsSubTeacherNo" value=""/>
			<input type="hidden" id="canServeAsSubTeacherDA" name="canServeAsSubTeacherDA" value=""/>
			<input type="hidden" id="TFAA" name="TFAA" value=""/>
			<input type="hidden" id="TFAC" name="TFAC" value=""/>
			<input type="hidden" id=TFAN name="TFAN" value=""/>
			<input type="hidden" id="ifrmYOTE" name="ifrmYOTE" value=""/>
			<input type="hidden" id="YOTE" name="YOTE" value=""/>
			<input type="hidden" id="ifrmAScore" name="ifrmAScore" value=""/>
			<input type="hidden" id="AScore" name="AScore" value=""/>
			<input type="hidden" id="ifrmLRScore" name="ifrmLRScore" value=""/>
			<input type="hidden" id="LRScore" name="LRScore" value=""/>
			<input type="hidden" id="stateIdForcandidate" name="stateIdForcandidate" value=""/>
			<input type="hidden" id="certificateTypeMasterForCandidate" name="certificateTypeMasterForCandidate" value=""/>			
			<input type="hidden" id="ifrmCGPA" name="ifrmCGPA" value=""/>
			<input type="hidden" id="CGPA" name="CGPA" value=""/>
			<input type="hidden" id="CGPASelectVal" name="CGPASelectVal" value=""/>
			<!--<input type="hidden" id="tagsearchId" name="tagsearchId" value=""/>-->
			<input type="hidden" id="subjects" name="subjects" value=""/>
			<input type="hidden" id="stateId" name="stateId" value=""/>
		</c:if>
		
	<c:if test="${headQuarterMaster!=null && headQuarterMaster.isBranchExist eq false}">
	<c:set var="branchExistFlag" value="0"></c:set>
	</c:if>
	
	<c:if test="${branchMaster!=null}">
	<c:set var="empNoCss" value="hide"></c:set>
	</c:if>
			${caption} 
		</div>
	</div>
	<div style="clear: both;"></div>
	<div class="centerline"></div>
</div>
<div class="top10">
	<div class="span16"
		onkeypress="return chkForEnterSearchTeacher(event);">
		<form class="bs-docs-example">
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<span class=""><label class="">
						<spring:message code="lblFname"/>
						</label>
						<input type="text" id="firstName" name="firstName" maxlength="50"
							class="help-inline form-control">
					</span>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="">
						<span class=""><label class="">
								<spring:message code="lblLname"/>
							</label>
							<input type="text" id="lastName" name="lastName" maxlength="50"
								class="help-inline form-control">
						</span>
					</div>
				</div>
				<div class="col-sm-4 col-md-4" style='white-space: nowrap;'>
					<div>
						<label class="">
							<spring:message code="lblEmailAddress"/>
						</label>
						<input type="text" id="emailAddress" name="emailAddress"
							maxlength="75" class="form-control fl">
					</div>
				</div>
				<div class="col-sm-2 col-md-2" style="padding-left: 20px;">
					<button class="btn btn-primary top25-sm2" type="button"
						onclick="searchTeacher()" style="width: 95px;">
						<spring:message code="btnSearch"/>
						<i class="icon"></i>
					</button>
				</div>
			</div>
			<div class="row top7">
				<div class="col-sm-8 col-md-8">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<label>
								<strong>
${candidateCaption}</strong>
							</label>
						</div>
						<div class="col-sm-12 col-md-12">
						<label class="col-sm-2 col-md-2 radio"
								style="margin-top: 0px; width: 86px;">
								<input type="radio" value="6" id="jb6" name="jobApplied"
									 onclick="showHideJbApld(0);">
							<spring:message code="lblyesterday"/>
							</label>
							<label class="col-sm-2 col-md-2 radio"
								style="margin-top: 0px; width: 70px;">
								<input type="radio" value="7" id="jb7" name="jobApplied"
									 onclick="showHideJbApld(0);">
							<spring:message code="lbl3D"/>
							</label>
							<label class="col-sm-2 col-md-2 radio"
								style="margin-top: 0px; width: 70px;">
								<input type="radio" value="0" id="jb1" name="jobApplied"
									checked="checked" onclick="showHideJbApld(0);">
							<spring:message code="lbl7D"/>
							</label>
							<label class="col-sm-2 col-md-2 radio "
								style="margin-top: 0px; width: 80px;">
								<input type="radio" value="1" id="jb1" name="jobApplied"
									onclick="showHideJbApld(0);">
							<spring:message code="lbl15D"/>
							</label>
							<label class="col-sm-2 col-md-2 radio"
								style="margin-top: 0px; width: 80px;">
								<input type="radio" value="2" id="jb2" name="jobApplied"
									onclick="showHideJbApld(0);">
						<spring:message code="lbl30D"/>
							</label>
							<label class="col-sm-2 col-md-2 radio"
								style="margin-top: 0px; width: 80px;">
								<input type="radio" value="3" id="jb3" name="jobApplied"
									onclick="showHideJbApld(0);">
								<spring:message code="lbl60D"/>
							</label>
							<!--<label class=" span60px radio">
									<input type="radio" value="4" id="jb4" name="jobApplied" onclick="showHideJbApld(0);">
							    	90 Days
						        </label>
						        -->
							<label class="col-sm-2 col-md-2 radio"
								style="margin-top: 0px; width: 70px;">
								<input type="radio" value="5" id="jb5" name="jobApplied"
									onclick="showHideJbApld(1);">
							<spring:message code="lblOthr"/>
							</label>
						</div>
					</div>
				</div>

				<div class="col-sm-4 col-md-4" style="margin-left: -100px;">
					<div class="col-sm-6 col-md-6" style="display: none;" id="jaFrom">
						<span class=""><label class="">
								<spring:message code="lblJobAppF"/>
							</label> <input type="text" id="jobAppliedFromDate" name="fromDate"
								class="help-inline form-control">
						</span>
					</div>
					<div class="col-sm-6 col-md-6" style="display: none;" id="jaTo">
						<span class=""><label class="">
								<spring:message code="lblJobAppTo"/>
							</label> <input type="text" id="jobAppliedToDate" name="toDate"
								class="help-inline form-control">
						</span>
					</div>
				</div>
			</div>
	
			<div class="row">
				<div class="col-sm-10 col-md-10" style="margin-bottom:8px;">
						<table>
							<tr>
								<td>
									<input type="checkbox" value="0" id='newcandidatesonly'
										name='newcandidatesonly'>
								</td>
								<td>
									&nbsp;${filterCaption}
								</td>
							</tr>
						</table>
				</div>
			</div>


			<div class="row">
				<div class="col-sm-2 col-md-2 ${hideSSN}" >
					<label class="">
						<spring:message code="lblSSN"/>
					</label>
					<input type="text" maxlength="9" class="form-control input-small"
						id="ssn" autocomplete="off" />
				</div>
				<div class="col-sm-2 col-md-2" ${empNoCss}>
					<label class="">
						${empNo}
					</label>
					<input type="text" maxlength="" class="form-control input-small"
						id="empNo" autocomplete="off" />
				</div>
			</div>
			
			<div  class="row hide" style="margin-top: 10px;" id="advLabel">
				<div class="col-sm-10 col-md-10 " >
				 	<spring:message code="opedAdvSearchLabel"></spring:message>
				 </div>	
			</div> 
			<div  class="row">
			 	<div id='advanceCheckTooltip' class="col-sm-16 col-md-16 " style=" padding-left: 14px;" >
			 	</div>
			</div>
			<br/>
			<div class="row">
			
			  <div class="col-sm-5 col-md-5">
					<div id="searchLinkDiv">
                   
                    <a href="#" onclick="displayAdvanceSearch1()"><div id="advsearch" class="show"><spring:message code="lnkOpAdvSch"/></div></a>
					<a href="#" onclick="displayAdvanceSearch1()"><div id="filterActiveLbl" class="hide"><spring:message code="lnkfilterActive"/><spring:message code="lnkOpAdvSch"/></div></a>
					</div> 
					
					
						<div id="hidesearchLinkDiv" class="hide">
							<a href="#" onclick="hideAdvanceSearch()"><spring:message code="lnkClAdvSch"/></a>
						</div>
			  </div>
			</div>

			<div id="advanceSearchDiv" class="hide">
				<div class="row">
				
				<c:if test="${distBranchFlag eq 0}">				
				<div class="col-sm-6 col-md-6">
						<label>
							HeadQuarter Name
						</label>						
                    <input type="text" class="help-inline form-control" value="${headQuarter}" disabled="disabled" />
                    </div>
				<c:if test="${branchExistFlag eq 1}">
					<div class="col-sm-6 col-md-6">
						<label>
							Branch Name
						</label>
						<br />
						<c:if test="${branchMaster==null}">
							<span> 
								<input type="text" id="branchName"
									maxlength="100" name="branchName"
									class="help-inline form-control"
									onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch1', 'branchName','branchSearchId','');getJobCategoryByHQAndBranch();"
                                    onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch1', 'branchName','branchSearchId','');getJobCategoryByHQAndBranch();"
                                    onblur="hideBranchMasterDiv(this,'branchSearchId','divTxtShowDataBranch1');getJobCategoryByHQAndBranch();"
                                    onchange="getJobCategoryByHQAndBranch();"/>
                                    </span>
                                    <input  type="hidden" id="branchSearchId" name="branchSearchId" value="">
                        
                            <div id='divTxtShowDataBranch1' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch1','branchName')" class='result' ></div> 
							
						</c:if>
						 <c:if test="${branchMaster!=null}">
										<input type="text" class="form-control"
											value="${branchMaster.branchName}" disabled="disabled" />
										<input type="hidden" id="branchSearchId" value="${branchMaster.branchId}" />
										<input type="hidden" id="branchName" value="${branchMaster.branchName}"
											name="branchName" />
									</c:if>
					</div>
					</c:if>
                    </c:if>
                    
				<c:if test="${distBranchFlag eq 1}">
					<div class="col-sm-6 col-md-6">
						<label>
							<spring:message code="lblDistrictName"/>
						</label>
						<br />
						<c:if test="${DistrictName==null}">
							<span> 
								<input type="text" id="districtName"
									maxlength="100" name="districtName"
									class="help-inline form-control"
									onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId',''); getSubjectByDistrict();getJobCategoryByDistrict();"
									onkeyup="getDistrictAuto(this,event,'divTxtShowData', 'districtName','districtId',''); getSubjectByDistrict();getJobCategoryByDistrict();"
									onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData'); getSubjectByDistrict();getJobCategoryByDistrict();" />
							</span>
							<input type="hidden" id="districtId" value="0" />
						</c:if>
						<c:if test="${DistrictName!=null}">
				             	${DistrictName}	
				             	<input type="hidden" id="districtId"
								value="${DistrictId}" />
							<input type="hidden" id="districtName" value="${DistrictName}"
								name="districtName" />
						</c:if>
						<div id='divTxtShowData'
							onmouseover="mouseOverChk('divTxtShowData','districtName')"
							style='display: none; position: absolute; z-index: 5000;'
							class='result'></div>
					</div>

					<div class="col-sm-6 col-md-6">
						<label>
							<spring:message code="lblSchoolName"/>
						</label>
						<c:if test="${SchoolName==null or writePrivilegeToSchool}">
							<input type="text" id="schoolName" maxlength="100"
								name="schoolName" class="form-control" placeholder=""
								onfocus="getSchoolAuto(this, event, 'divTxtShowData3', 'schoolName','districtId','');"
								onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
								onblur="hideSchoolMasterDiv(this,'schoolId','divTxtShowData2');" />
							<input type="hidden" id="schoolId" value="0" />
							<c:set var="SchoolName"></c:set>
						</c:if>
						<c:if test="${SchoolName!=null or writePrivilegeToSchool==false}">
				             	${SchoolName}	
				             	<input type="hidden" id="schoolId" value="${SchoolId}" />
							<input type="hidden" id="schoolName" value="${SchoolName}"
								name="schoolName" />
						</c:if>
						<div id='divTxtShowData2'
							onmouseover="mouseOverChk('divTxtShowData2','schoolName')"
							style='display: none; position: absolute; z-index: 5000;'
							class='result'></div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<span class=""><label class="">
								<spring:message code="msgCerti/LiceOfStateOfReqForJob"/>
							</label> <select class="form-control" id="stateId" name="stateId"
								onchange="activecityType(0);">
								<!-- loading from ajax By Anurag
								<option value="0">
									<spring:message code="lblAllCerti/LiceState"/>
								</option>
								<c:if test="${not empty lstStateMaster}">
									<c:forEach var="stateObj" items="${lstStateMaster}">
										<option value="${stateObj.stateId}">
											${stateObj.stateName}
										</option>
									</c:forEach>
								</c:if>
							--></select> </span>
					</div>
					<div class="col-sm-6 col-md-6">
						<span><label class="">
								<spring:message code="lblCerto/LiceNameReqForJob"/>
							</label> </span>
						<div>
							<input type="hidden" id="certificateTypeMaster" value="0">
							<input type="text" class="form-control" maxlength="200"
								id="certType" value="" name="certType"
								onfocus="getFilterCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
								onkeyup="getFilterCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
								onblur="hideFilterCertificateTypeDiv(this,'certificateTypeMaster', 'divTxtCertTypeData');" />
							<div id='divTxtCertTypeData'
								style='display: none; position: absolute;' class='result'
								onmouseover="mouseOverChk('divTxtCertTypeData','certType')"></div>
						</div>
					</div>
				</div>
				<div class="row top10" id="divCertificate" style="margin-right: 0px;margin-left: -15px;"></div>
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<span class=""><label class="">
								<spring:message code="lblCerto/LiceSataeOfCand"/>
							</label> <select class="form-control" id="stateIdForcandidate"
								name="stateIdForcandidate" onchange="activecityType(1);">
								<!-- Loading from Ajax   <option value="0">
									<spring:message code="optAllCert/LiceState"/>
								</option>
								<c:if test="${not empty lstStateMaster}">
									<c:forEach var="stateObj1" items="${lstStateMaster}">
										<option value="${stateObj1.stateId}">
											${stateObj1.stateName}
										</option>
									</c:forEach>
								</c:if>
							--></select> </span>
					</div>
					<div class="col-sm-6 col-md-6">
						<span><label class="">
								<spring:message code="lblCerti/LiceNameOfCand"/>
							</label> </span>
						<div>
							<input type="hidden" id="certificateForCandidate" value="0">
							<input type="hidden" id="certificateTypeMasterForCandidate"
								value="0">
							<input type="text" class="form-control" maxlength="200"
								id="certTypeForCandidate" value="" name="certTypeForCandidate"
								onfocus="getFilterCertificateTypeAutoCompForCan(this, event, 'divTxtCertTypeDataForCandidate', 'certTypeForCandidate','certificateTypeMasterForCandidate','');"
								onkeyup="getFilterCertificateTypeAutoCompForCan(this, event, 'divTxtCertTypeDataForCandidate', 'certTypeForCandidate','certificateTypeMasterForCandidate','');"
								onblur="hideFilterCertificateTypeDiv(this,'certificateTypeMasterForCandidate', 'divTxtCertTypeDataForCandidate');" />
							<div id='divTxtCertTypeDataForCandidate'
								style='display: none; position: absolute;' class='result'
								onmouseover="mouseOverChk('divTxtCertTypeDataForCandidate','certTypeForCandidate')"></div>
						</div>
					</div>
					<div class="col-sm-6 col-md-6" id="addCertificate"><a href="javascript:void(0);" onclick="addCertificate(1)" >+ Add Certification/Licensure Name</a></div>
				</c:if>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="">
							<label class="">
								<spring:message code="lblRegion"/>
							</label>
							<input type="hidden" id="regionId" value="0">
							<input type="text" class="form-control" maxlength="100"
								id="regionName" value="" name="regionName"
								onfocus="getRegionAutoComp(this, event, 'divTxtRegionData', 'regionName','regionId','');"
								onkeyup="getRegionAutoComp(this, event, 'divTxtRegionData', 'regionName','regionId','');"
								onblur="hideRegionDiv(this,'regionId','divTxtRegionData');" />
							<div id='divTxtRegionData'
								style='display: none; position: absolute;'
								onmouseover="mouseOverChk('divTxtRegionData','regionName')"
								class='result'></div>
						</div>
					</div>

				</div>
                  <c:if test="${distBranchFlag eq 1}"> 
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<label class="">
							<spring:message code="lblHighDegAttained"/>
						</label>
						<input type="text" maxlength="50" class="form-control input-small"
							id="degreeName" autocomplete="off" value="" name="degreeName"
							onfocus="getDegreeMasterAutoComp(this, event, 'divTxtShowEducation', 'degreeName','degreeId','');"
							onkeyup="getDegreeMasterAutoComp(this, event, 'divTxtShowEducation', 'degreeName','degreeId','');"
							onblur="hideDegreeMasterDiv(this,'degreeId','divTxtShowEducation');" />
						<div id='divTxtShowEducation'
							style='display: none; position: absolute;' class='result'
							onmouseover="mouseOverChk('divTxtShowEducation','degreeName')"></div>
						<input type="hidden" id="degreeId" value="0">
						<input type="hidden" id="degreeType" value="">

					</div>
					<div class="col-sm-6 col-md-6">
						<span class=""><label class="">
								<spring:message code="lblClgAttainded"/>
							</label> <input type="hidden" id="universityId" value="0"> <input
								type="text" class="form-control" maxlength="100"
								id="universityName" autocomplete="off" value=""
								name="universityName"
								onfocus="getUniversityAutoComp(this, event, 'divTxtUniversityData', 'universityName','universityId','');"
								onkeyup="getUniversityAutoComp(this, event, 'divTxtUniversityData', 'universityName','universityId','');"
								onblur="hideUniversityDiv(this,'universityId','divTxtUniversityData');" />
							<div id='divTxtUniversityData'
								style='display: none; position: absolute;'
								onmouseover="mouseOverChk('divTxtUniversityData','universityName')"
								class='result'></div> </span>
					</div>
				</div>
                </c:if>
				<div class="row">
					<div class="col-md-6">
						<div class="">
							<label class="">
								<spring:message code="lblNormScore"/>
							</label>
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td valign="top">
										<select style='width: 70px;' id="normScoreSelectVal"
											name="contacted" class="form-control">
											<option value="0" selected="selected">
											<spring:message code="optAll"/>
											</option>
											<option value="6">
											<spring:message code="optN/A"/>
											</option>
											<option value="5">
												>=
											</option>
											<option value="1">
												=
											</option>
											<option value="2">
												<
											</option>
											<option value="3">
												<=
											</option>
											<option value="4">
												>
											</option>
										</select>
									</td>
									<td valign="top" id="sliderNormScoreTP">
									</td>
									<input type="hidden" name="normScore" id="normScore" />
								</tr>
							</table>
						</div>
					</div>
					<c:if test="${distBranchFlag eq 1}">
					<div class="col-md-6">
						<div class="">
							<label class="">
								<spring:message code="lblCGPA"/>
							</label>
							<table>
								<tr>
									<td>
										<select style='width: 70px;' id="CGPASelectVal"
											name="contacted" class="form-control">
											<option value="0" selected="selected">
										<spring:message code="optAll"/>
											</option>
											<option value="6">
												<spring:message code="optN/A"/>
											</option>
											<option value="5">
										               >=
											</option>
											<option value="1">
												=
											</option>
											<option value="2">
												<
											</option>
											<option value="3">
												<=
											</option>
											<option value="4">
												>
											</option>

										</select>
									</td>
									<td valign="top" id="sliderCGPATP">
									</td>
									<input type="hidden" name="CGPA" id="CGPA" />
								</tr>
							</table>
						</div>
					</div>
				</div>


				<div class="row">
				</c:if>
					<div class="col-md-6">
						<div class="">
							<label class="">
								<spring:message code="lblFitScore"/>
							</label>
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td valign="top">
										<select style='width: 70px;' id="fitScoreSelectVal"
											name="fitScoreSelectVal" class="form-control">
											<option value="0" selected="selected">
											<spring:message code="optAll"/>
											</option>
											<option value="6">
											<spring:message code="optN/A"/>
											</option>
											<option value="5">
											>=
											</option>
											<option value="1">
												=
											</option>
											<option value="2">
												<
											</option>
											<option value="3">
												<=
											</option>
											<option value="4">
												>
											</option>
										</select>
									</td>
									<td valign="top" id="sliderFitScoreTP">
									</td>
									<input type="hidden" name="fitScore" id="fitScore" />
								</tr>
							</table>
						</div>
					</div>
					<c:if test="${distBranchFlag eq 1}">
					<div class="col-md-6">
						<div class="">
							<label class="">
								<spring:message code="lblYOfTeachExp"/>
							</label>
							<table>
								<tr>
									<td>
										<select style='width: 70px;' id="YOTESelectVal"
											name="YOTESelectVal" class="form-control">
											<option value="0" selected="selected">
												<spring:message code="optAll"/>
											</option>
											<option value="6">
											<spring:message code="optN/A"/>
											</option>
											<option value="5">
											>=
											</option>
											<option value="1">
												=
											</option>
											<option value="2">
												<
											</option>
											<option value="3">
												<=
											</option>
											<option value="4">
												>
											</option>

										</select>
									</td>
									<td valign="top" id="sliderYOTETP">
									</td>
									<input type="hidden" name="YOTE" id="YOTE" />
									</tr>
							</table>
						</div>
					</div>
					</c:if>
				</div>
				<c:if test="${distBranchFlag eq 1}">
				<div class="row">
					<div class="col-md-6">
						<div class="">
							<label class="">
								<spring:message code="lblAScore"/>
							</label>
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td valign="top">
										<select style='width: 70px;' id="AScoreSelectVal"
											name="AScoreSelectVal" class="form-control">
											<option value="0" selected="selected">
											<spring:message code="optAll"/>
											</option>
											<option value="6">
											<spring:message code="optN/A"/>
											</option>
											<option value="5">
											>=
											</option>
											<option value="1">
								             	=
											</option>
											<option value="2">
												<
											</option>
											<option value="3">
												<=
											</option>
											<option value="4">
												>
											</option>
										</select>
									</td>
									<td valign="top" id="sliderAScoreTP">
									</td>
									<input type="hidden" name="AScore" id="AScore" />
								</tr>
							</table>
						</div>
					</div>
					<div class="col-md-6">
						<div class="">
							<label class="">
								<spring:message code="lblL/RScore"/>
							</label>
							<table>
								<tr>
									<td>
										<select style='width: 70px;' id="LRScoreSelectVal"
											name="LRScoreSelectVal" class="form-control">
											<option value="0" selected="selected">
												<spring:message code="optAll"/>
											</option>
											<option value="6">
											<spring:message code="optN/A"/>
											</option>
											<option value="5">
											>=
											</option>
											
											<option value="1">
												=
											</option>
											
											<option value="2">
												<
											</option>
											<option value="3">
												<=
											</option>
											<option value="4">
												>
											</option>

										</select>
									</td>
									<td valign="top" id="sliderLRScoreTP">
									</td>
									<input type="hidden" name="LRScore" id="LRScore" />
								</tr>
							</table>
						</div>
					</div>
				</div>
              </c:if>

				<div class="row">
				<c:if test="${distBranchFlag eq 1}">
					<div class="col-sm-3 col-md-3">
						<div class="">
							<label class="">
								<spring:message code="lblRef"/>
							</label>
							<select class="form-control" id="references" name="references">
								<option value="0" selected="selected">
							<spring:message code="optAll"/>
								</option>
								<option value="1">
									<spring:message code="optOnFile"/>
								</option>
								<option value="2">
									<spring:message code="optNotOnFile"/>
								</option>
							</select>

						</div>
					</div>
					<div class="col-sm-3 col-md-3">
						<div class="">
							<label class="">
						<spring:message code="lblResume"/>
							</label>
							<select class="form-control" id="resume" name="resume">
								<option value="0" selected="selected">
								<spring:message code="optAll"/>
								</option>
								<option value="1">
								<spring:message code="optOnFile"/>
								</option>
								<option value="2">
									<spring:message code="optNotOnFile"/>
								</option>
							</select>

						</div>
					</div>
					</c:if>
					<div class="col-sm-3 col-md-3">
						<span class=""><label class="">
							<spring:message code="lblCandStatus"/>
							</label> <select class="form-control " id="candidateStatus"
								name="candidateStatus" multiple="multiple"  onchange="statusSearchUtil.withdraw(this);">
								<option value="0">
								<spring:message code="optAll"/>
								</option>
								<c:if test="${not empty lstStatusMasters}">
									<c:forEach var="statusObj" items="${lstStatusMasters}">
										<option value="${statusObj.statusShortName}">
											${statusObj.status}
										</option>
									</c:forEach>
								</c:if>
							</select> </span>
					</div>
					<div class="col-md-3 top12-sm">
						<div class="">
							<label class="">
								&nbsp;
							</label>
							<table>
								<tr>
									<td>
										<input type="checkbox" name="internalCandidate"
											id="internalCandidate" value="0">
									</td>
									<td>
										&nbsp;<spring:message code="lblInterCand"/>
									</td>
									<td>
										<a href="#"
											data-original-title='Internal Candidates are those who are currently holding a teaching position with a valid teaching certification in my District'
											rel='tooltip' id='internalCand'><span
											class='icon-question-sign'></span>
										</a>
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" name="frmlCandidate"
											id="frmlCandidate" value="2">
									</td>
									<td>
										&nbsp;<spring:message code="lblformalEmpOnly"/>
									</td>
									<td>
										<a href="#"
											data-original-title='Former Candidates are those who are currently holding a teaching position with a valid teaching certification in my District'
											rel='tooltip' id='formalCand'><span
											class='icon-question-sign'></span>
										</a>
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" name="retirdCandidate"
											id="retirdCandidate" value="3">
									</td>
									<td>
										&nbsp;<spring:message code="lblretiredEmpOnly"/>
									</td>
									<td>
										<a href="#"
											data-original-title='Retired Candidates are those who are currently holding a teaching position with a valid teaching certification in my District'
											rel='tooltip' id='retiredCand'><span
											class='icon-question-sign'></span>
										</a>
									</td>
								</tr>
							</table>

						</div>
					</div>	
				<c:if test="${distBranchFlag eq 1}">
				</div>
				</c:if>				
				<div class='row hide' id="withdrawType">
					<div class="col-sm-6 col-md-6">
					</div>
					<div class="col-sm-6 col-md-6">
						<span class=""><label class="">
						Withdraw Type
						</label> <select class="form-control " id="withdrawTypeBox" name="withdrawTypeBox" multiple="multiple" style="height: 50px;">
						</select> </span>
					</div>
				</div>
				<c:if test="${distBranchFlag eq 1}">
				<div class="row">
				</c:if>
					<div class="col-sm-6 col-md-6">
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<span class=""><label class="">
										<spring:message code="lblJobCreatedF"/>
									</label> <input type="text" id="fromDate" name="fromDate"
										class="help-inline form-control">
								</span>
							</div>
							<div class="col-sm-6 col-md-6">
								<span class=""><label class="">
										<spring:message code="lblJobCreatedTo"/>
									</label> <input type="text" id="toDate" name="toDate"
										class="help-inline form-control">
								</span>
							</div>
						</div>
					</div>
					<c:if test="${distBranchFlag eq 1}">
					<div class="col-sm-6 col-md-6">
						<div class="row">
						<div class="col-sm-6 col-md-6">
							<div class="">
									<label class="">
										<spring:message code="lblWillingToSubsti"/>
									</label>
									<table>
										<tr>
											<td>
												<input type="checkbox" name="canServeAsSubTeacherYes"
													id="canServeAsSubTeacherYes" value="1">
											</td>
											<td>
											<spring:message code="lblYes"/>
											</td>
											<td style='padding-left: 10px;'>
												<input type="checkbox" name="canServeAsSubTeacherNo"
													id="canServeAsSubTeacherNo" value="0">
											</td>
											<td>
												<spring:message code="lblNo"/>
											</td>
											<td style='padding-left: 10px;'>
												<input type="checkbox" name="canServeAsSubTeacherDA"
													id="canServeAsSubTeacherDA" value="2">
											</td>
											<td>
												<spring:message code="lblDeclToAns"/>
											</td>
										</tr>
									</table>
								</div>
						</div>
						<div class="col-sm-6 col-md-6">
								<div class="">
									<label class="">
										<spring:message code="lblTFA"/>
									</label>
									<table>
										<tr>
											<td>
												<input type="checkbox" name="TFAA" id="TFAA" value="A">
											</td>
											<td>
												<spring:message code="lblAlum"/>
											</td>
											<td style='padding-left: 20px;'>
												<input type="checkbox" name="TFAC" id="TFAC" value="C">
											</td>
											<td>
												<spring:message code="lblCurrent"/>
											</td>
											<td style='padding-left: 20px;'>
												<input type="checkbox" name="TFAN" id="TFAN" value="N">
											</td>
											<td>
												<spring:message code="lblNo"/>
											</td>
										</tr>
									</table>
								</div>
							</div>
							</div>
					</div>
					</c:if>
				</div>
				
				<div class="row">
				<c:if test="${distBranchFlag eq 1}">
					<div class="col-sm-6 col-md-6">
						<label>
							<spring:message code="lblSub"/>
						</label>
						<select multiple="multiple" class="form-control" name="subjects"
							id="subjects" size="4">
							<option value="0">
								<spring:message code="optSltDistForReleventSubjectList"/>
							</option>

						</select>
					</div>
					</c:if>
					<div class="col-sm-6 col-md-6">
						<label>
							<spring:message code="lblJobCat"/>
						</label>
						<c:if test="${distBranchFlag eq 1}">
						<select multiple="multiple" class="form-control" name="jobCateSelect"
							id="jobCateSelect" size="4" onblur="getJobSubcategory();">
							<option value="0">
								<spring:message code="msgSltDistForRlvtJobCtay"/>
							</option>
						</select>
						</c:if>
						<c:if test="${distBranchFlag eq 0}">
						<select multiple="multiple" class="form-control" name="jobCateSelect"
							id="jobCateSelect" size="4" onblur="getJobSubCategoryByCategory();" onclick="getJobSubCategoryByCategory();">
							<option value="0">
								Select branch for relevant jobCategory list
							</option>
						</select>
						</c:if>
						
					</div>
					<c:if test="${distBranchFlag eq 0}">
					<div class="col-sm-6 col-md-6">
							<div class="">
									<label>
											<spring:message code="lblJobSubCategory"/>
										</label>
										<select multiple="multiple" class="form-control" name="jobSubCateSelect"
											id="jobSubCateSelect" size="5" style="height:94px;">
											<option value="0">
												<spring:message code="msgJobSubCatlist"/>
											</option>
									</select>
							</div>
					</div>
					</c:if>
				</div>
				<c:if test="${distBranchFlag eq 1}">
				<c:if test="${userSession.entityType!=1}">
					<div class="row">
					<%--Mukesh Tags Search only for DA's --%>
					     <div class="col-sm-6 col-md-6">
						       	<span class=""><label class=""><spring:message code="lblTags"/> </label>
						       	     <select  id="tagsearchId" name="tagsearchId" class="form-control tagsearchClass">
						       	     
						       	     <!-- Loading from Ajax Anurag
						       	       <option value=''><spring:message code="optSltTag"/></option>
						       		      ${tagslist} 
						        -->		     
						       		     </select>	
						      
						       </span>
						  </div> 
						  <%---------Thumb Search by Ravindra--------------%> 
						  <div class="col-sm-6 col-md-6">
						       	<span class=""><label class=""><spring:message code="optQualiQuest"/></label>
						       	     <select  id="qualQues" name="qualQues" class="form-control">
						       	       <option value='1'><spring:message code="optAll"/></option>
						       	       <option value='2'><spring:message code="optGreenThumbOnly"/> </option>
						       	       <option value='3'><spring:message code="optRedThumbOnly"/> </option>
						       	       <option value='4'><spring:message code="optBlueThumbOnly"/></option>
								     </select>	
						       </span>
						  </div>   
					 </div>
				 </c:if>
			</c:if>
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<span class=""><label class=""><spring:message code="lblStateName"/></label>
							<select class="form-control" id="stateId2" name="stateId2" onchange="activecityType(0);">
								<option value="0"><spring:message code="lblAllState"/></option>
								<c:if test="${not empty lstStateMaster}">
									<c:forEach var="stateObj" items="${lstStateMaster}">
										<option value="${stateObj.stateId}">${stateObj.stateName}</option>
									</c:forEach>
								</c:if>
							</select>
						</span>
					</div>
					<div class="col-sm-6 col-md-6">
						<span class=""><label class=""><spring:message code="lblZepCode"/></label>
							<input id="zipCode" name="" maxlength="10" class="help-inline form-control" type="text" onkeypress="return checkForInt(event);">
					    </span>
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<span class=""><label class=""><spring:message code="lblEPIDateFrom1"/></label>
									<input type="text" id="epiFromDate" name="fromDate" class="help-inline form-control">
								</span>
							</div>
							<div class="col-sm-6 col-md-6">
								<span class=""><label class=""><spring:message code="lblEPIDateTo1"/></label>
									<input type="text" id="epiToDate" name="toDate"	class="help-inline form-control">
								</span>
							</div>
						</div>
					</div>
					<c:if test="${distBranchFlag eq 1}">
					<div class="col-sm-6 col-md-6">
							<div class="">
									<label>
											<spring:message code="lblJobSubCategory"/>
										</label>
										<select multiple="multiple" class="form-control" name="jobSubCateSelect"
											id="jobSubCateSelect" size="5" style="height:94px;">
											<option value="0">
												<spring:message code="msgJobSubCatlist"/>
											</option>
									</select>
							</div>
					</div>
					</c:if>
				</div>
				
				
				<br>
				
			</div>
		</form>
	</div>
</div>
  <!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadApplicantpool" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTeachAppPool"/></h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End -->  
<div class="row"> 
<div class="col-sm-3 col-md-">  
<div id='tpMenuActionDiv' style='display:none;'>
	<table  class='tblborder' cellpadding=0>
		<tr>
			<td style='font-size: 12px;' vertical-align='middle' ><spring:message code="lblAct"/>&nbsp;&nbsp;</td>
			<td>
				<span class="btn-group">
					<a data-toggle="dropdown" href="javascript:void(0);">
						<span class='icon-collapse  iconcolor icon-large'></span>
					</a>
					<ul class="dropdown-menu pull-left">
						<li>
							<a href="javascript:void(0);" onclick='sendMessageMassTP()'><spring:message code="lnkSendMsg"/></a>
						</li>
						
						<li>

							<a href="javascript:void(0);" onclick='applyTags()'><spring:message code="lblAplyTag"/></a>
						</li>
						
						<li>
							<a href="javascript:void(0);" onclick='confirmPortfolioReminderBulkMail()'><spring:message code="lblApplicantMgmnt"/></a>
						</li>
						
						<li>
							<a href="javascript:void(0);" onclick='printResume()'><spring:message code="lblPrintResume1"/></a>
						</li>
					</ul>
				</span>
			</td>
		</tr>
	</table>
</div>

<div class="modal hide" id="tagsForMultipleCandidateDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x </button>
				<h3> <spring:message code="headAplyTags"/>  </h3>
			</div>
			
			<div class="modal-body">
				<div class="control-group">
					<div id="applyTagsId" name="applyTagsId"> </div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="button" onclick="saveApplyTags()"> <spring:message code="btnAplyTags"/>&nbsp;<i class="icon"></i> </button>
				<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button>
			</div>
		</div>
	</div>
</div>
</div>

	<c:if test="${userSession.entityType==2}"> 
		<div class="col-sm-3 col-md-" id="reminderAllMail" style="display: none;"> 
			<div><a href="javascript:void(0);" onclick='return confirmPortfolioReminderAllMail();'>Send Candidate Pool Refresh Link</a></div>
		</div>
	</c:if>
<input type="hidden" id="canrePoolVal" value="${CandidatePoolRefresh}">
	<c:if test="${userSession.entityType!=1}"> 
	<div class="col-sm-6 col-md-6">
	 <div class="row hide mt5" style="padding-right:20px;" id="exportIconsDiv">
	       <table class="marginrightForTablet">
			      <tr>
			         <td  style='padding-left:5px;'>
						<a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="downloadApplicantPoolExcelReport();"><span class='icon-file-excel icon-large iconcolor'></span></a>
					 </td>
					 <td>
					  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='downloadApplicantPoolPDFReport();'><span class='pdf-eoc icon-large iconcolor'></span></a>
					 </td>
					 <td>
						<a data-original-title='Print' rel='tooltip' id='printId' href='javascript:void(0);' onclick="printApplicantPoolPreview();"><span class='icon-print icon-large iconcolor'></span></a>
					 </td>  
				</tr>
			</table>
			</div>
	</div>
	</c:if>
</div>		
        
<div class="TableContent">
	<div class="table-responsive" id="teacherGrid">
		
	</div>
</div>

<div id="teacherGridProfile">

</div>
<br>
<br>

<div style="display: none; z-index: 5000;" id="loadingDiv">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!--<img src="images/please.jpg" />
			--></td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
		<!-- <tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'>Your report is being generated...</td></tr> -->
	</table>
</div>

<div class="modal hide" id="myModalProfile" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="hideProfile()">
					x
				</button>
				<h3 id="myModalLabel">
				<spring:message code="lblProf"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="divProfile" class="">
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						&nbsp;
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="hideProfile()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>
<!--@Start
	@Ashish :: Add Qualification Div -->
<div class="modal hide" id="qualificationDiv"
	style="border: 0px solid blue;" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 520px;">
		<div class='modal-content'>
			<div class="modal-header" id="qualificationDivHeader">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick=''>
					x
				</button>
				<h3>
				<spring:message code="headQualif"/>
				</h3>
			</div>

			<div class="modal-body"
				style="max-height: 400px; padding-top: 0px; overflow-y: scroll;">
				<input type="hidden" id="teacherIdforPrint" name="teacherIdforPrint" />
				<input type="hidden" id="districtIdforPrint" name="districtIdforPrint" />
				<input type="hidden" id="headQuarterIdforPrint" name="headQuarterIdforPrint" />
				<input type="hidden" id="branchIdforPrint" name="branchIdforPrint" />
				<div class='divErrorMsg' id='errorQStatus' style="display: block;"></div>
				<div id="qualificationDivBody"></div>
			</div>
			<div class="modal-footer">
				<table width=470 border=0>
					<tr>
						<td width=100 nowrap align=left>
<!--							<span id="qStatusSave"><button-->
<!--									class="btn  btn-large btn-orange"-->
<!--									onclick='saveQualificationNoteForTP(0)'>-->
<!--									<spring:message code="btnIn-Progress"/>-->
<!--								</button>&nbsp;&nbsp;</span>-->
						</td>

						<td width=200 nowrap>
							<span id="qStatusFinalize"><button
									class="btn  btn-large btn-primary"
									onclick='saveQualificationNoteForTP(1)'>
									<spring:message code="btnFinalize"/>
									<i class="iconlock"></i>
								</button>&nbsp;&nbsp;</span>
								<button class="btn btn-large btn-primary" data-dismiss="modal"
								aria-hidden="true" onclick="printQualificationDivForTP();">
								<spring:message code="btnP"/>
							</button>
							
							<button class="btn btn-large" data-dismiss="modal"
								aria-hidden="true" onclick='cancelQualificationIssuse();'>
								<spring:message code="btnClr"/>
							</button>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<!--@End
	@Ashish :: Add Qualification Div -->


<!-- @Start
@Ashish :: draggable div for Teacher Profile -->
<div class="modal hide custom-div-border1" id="draggableDivMaster"
	tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	aria-hidden="true" data-backdrop="static">
	<div id="draggableDiv">

	</div>
</div>

<!-- @End
@Ashish :: draggable div for Teacher Profile -->
<div class="modal hide" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					×
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headTm"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='blockMessage'>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
				<spring:message code="btnOk"/>
				</button>
			</div>
		</div>
	</div>
</div>
<div class="modal hide" id="divAlert" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static" style="z-index: 5000;">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" id='vcloseBtnk'>
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headTm"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divAlertText">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<span><button class="btn" data-dismiss="modal"
						aria-hidden="true" onclick="setZIndexActDiv()">
						<spring:message code="btnClose"/>
					</button>
				</span>
			</div>
		</div>
	</div>
</div>
<div class="modal hide" id="myModalPhoneShow" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headPhone"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="divPhone" class="">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalPicture" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
				<spring:message code="headPic"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="divPicture" class="">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>
<div class="modal hide" id="modalDownloadPDR" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
		<div class="modal-content" style="background-color: #ffffff;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showProfilePopover()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headPdRep"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="" class="">
						<iframe src="" id="ifrmPDR" width="100%" height="450px">
						</iframe>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showProfilePopover()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>


<div class="modal hide" id="myModalJobList" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 1012px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="setPageFlag();">
					x
				</button>
				<h3 id="jobDetailHeaderText">
					<spring:message code="hdJDetail"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id='divJobHeaderError' class='divErrorMsg'
						style="display: block;"></div>
					<div id="divJobHeader" class='row mt10'></div>
					<div class='top15'>
						<div class="span6" id="divJobHeaderCheckBox"
							style="display: none;">
							<label class="checkbox inline">
								<input type="checkbox" id="isOverrideForAllJob"
									name="isOverrideForAllJob" />
								<spring:message code="lblOverChangForStrJob"/>
							</label>
						</div>
					</div>
					<div id="divJob" class="table-responsive"
						style="margin-bottom: 15px; min-width: 920px; overflow: hidden;">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="setPageFlag();">
				<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>


<div class="modal hide" id="modalDownloadsTranscript" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
		<div class="modal-content" style="background-color: #ffffff;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="setZIndexTrans()">
					x
				</button>
				<h3 id="myModalLabel">
				<spring:message code="headTranscript"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="" class="">
						<iframe src="" id="ifrmTrans" width="100%" height="450px">
						</iframe>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="setZIndexTrans()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div  class="modal hide pdfDivBorder"  id="modalDownloadsCommon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-contenat" style="background-color: #ffffff;">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()">x</button>
		<h3 id="myModalLabelText"></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
				 <iframe src="" id="ifrmTransCommon" width="100%" height="100%" scrolling="auto">
				 </iframe>  
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
 	</div>
</div>
<div class="modal hide" id="myModalTranscript" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class='modal-dialog' style="width: 936px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="defaultTeacherGrid()";>
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headTranscript"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="divTranscript" class="">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="defaultTeacherGrid()";>
						<spring:message code="btnClsoe"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalCertification" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="defaultTeacherGrid()";>
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headCertifi/Lice"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="divCertification" class="">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="defaultTeacherGrid()";>
						<spring:message code="btnClsoe"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div style="display: none; z-index: 5000;" id="loadingDiv">
	<table align="left">
		<tr>
			<td style="padding-top: 270px; padding-left: 450px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px; padding-left: 450px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
		<tr id='paymentMessage'>
			<td style='padding-top: 0px; padding-left: 450px;' id='spnMpro'
				align='center'></td>
		</tr>
	</table>
</div>


<div class="modal hide" id="myModalMsg" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static" style="z-index: 5000;">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" id='vcloseBtnk' onclick="setZIndexJobDiv()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headTm"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='myModalMsgShow'>
				</div>
			</div>
			<div class="modal-footer">
				<span><button class="btn" data-dismiss="modal"
						aria-hidden="true" onclick="setZIndexJobDiv()">
						<spring:message code="btnClsoe"/>
					</button>
				</span>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalCoverLetter" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class="modal-dialog-for-cgcoverletter">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="setZIndexJobDiv()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headCoverLetr"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="">
						<span id="lblCL"></span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="setZIndexJobDiv()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalDesc" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="setZIndexJobDiv()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headJobDescrip"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="">
						<span id="description"></span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="setZIndexJobDiv()">
					<spring:message code="btnClsoe"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalCommunications" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 700px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="closeCommunication();">
					x
				</button>
				<h3 id="myModalLabel">
				<spring:message code="headCom"/>
				</h3>
			</div>
			<div class="modal-body" style="max-height: 450px; overflow-y: auto">
				<div class="control-group">
					<div class="" id="divCommTxt">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" aria-hidden="true" onclick='printCommunicationslogs();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="closeCommunication();"> <spring:message code="btnClsoe"/> </button>
			</div>
		</div>
	</div>
</div>

<!--Start Profile Div-->
<div class="modal hide" id="myModalPhoneShowPro" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="max-width: 370px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showProfilePopover()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headPhone"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="divPhoneByPro" class="">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showProfilePopover()">
					<spring:message code="btnClsoe"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalProfileVisitHistoryShow"
	tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	aria-hidden="true" data-backdrop="static">

	<div class="modal-dialog" style="width: 100px;" id="mydiv">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showProfilePopover()">
					x
				</button>
				<h3 id="myModalLabel">
				<spring:message code="headTeacherProfVisitHistory"/>
				</h3>
			</div>
			<div class="modal-body"
				style="max-height: 400px; overflow-y: scroll; padding-right: 18px;">
				<div class="control-group">
					<div id="divteacherprofilevisithistory" class="">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showProfilePopover()">
					<spring:message code="btnClsoe"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalReferenceNoteView" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class='modal-dialog' style="width: 520px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showProfilePopover()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headRefrNot"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divRefNotesInner">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showProfilePopover()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<!-- Ref Note -->
<div class="modal hide" id="myModalReferenceNotesEditor" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog-for-cgreference">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick='closeRefNotesDivEditor();'>
					x
				</button>
				<h3 id="myModalLabel">
				<spring:message code="headNot"/>
				</h3>
			</div>

			<div class="modal-body" style="margin: 10px;">
				<div class="row" id="divNotes" style="padding-left: 15px;">
				</div>
				<iframe id='uploadNoteFrameID_ref' name='uploadNoteFrame_ref'
					height='0' width='0' frameborder='0' scrolling='yes'
					sytle='display:none;'>
				</iframe>
				<form id='frmNoteUpload_ref' enctype='multipart/form-data'
					method='post' target='uploadNoteFrame_ref' class="form-inline"
					onsubmit="return saveReferenceNotes();"
					action='referenceNoteUploadServlet.do' accept-charset="UTF-8">
					<input type="hidden" name="eleRefId" id="eleRefId">
					<input type="hidden" id="teacherIdForNote_ref"
						name="teacherIdForNote_ref">

					<div class="row mt10">
						<div class='span10 divErrorMsg' id='errordivNotes_ref'
							style="display: block;"></div>
						<div class="span10">
							<label>
								<strong><spring:message code="lblEtrNot"/><span class="required">*</span>
								</strong>
							</label>
							<div class="span10" id='divTxtNode_ref'
								style="padding-left: 0px; margin-left: 0px;">
								<textarea readonly id="txtNotes_ref" name="txtNotes_ref"
									class="span10" rows="4"></textarea>
							</div>
							<div id='fileRefNotes' style="padding-top: 10px;">
								<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img
										src='images/attach.png' />
								</a>
								<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><spring:message code="lnkAttachFile"/></a>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick='closeRefNotesDivEditor();'>
					<spring:message code="btnClr"/>
				</button>
				&nbsp;
				<span id="spnBtnSave" style="display: inner"><button
						class="btn btn-primary" onclick="saveReferenceNotes()">
					<spring:message code="btnSave"/>
						<i class="icon"></i>
					</button>&nbsp;</span>
			</div>
		</div>
	</div>
</div>
<!-- End Ref Note -->
<!--End Profile Div-->

<div class="modal hide" id="myModalNotes" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<input type="hidden" id="teacherId" name="teacherId" value="">
	<div class="modal-dialog" style="width: 673px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick='showCommunicationsDiv();'>
					x
				</button>
				<h3 id="myModalLabel">
			<spring:message code="headNot"/>
				</h3>
			</div>

			<div class="modal-body">
				<div class="row" id="divNotes" style="padding-left: 15px;">
				</div>
				<iframe id='uploadNoteFrameID' name='uploadNoteFrame' height='0'
					width='0' frameborder='0' scrolling='yes' sytle='display:none;'>
				</iframe>
				<form id='frmNoteUpload' enctype='multipart/form-data' method='post'
					target='uploadNoteFrame' class="form-inline"
					onsubmit="return saveNotes();" action='noteUploadServlet.do'
					accept-charset="UTF-8">

					<div class="mt10">
						<div class='span10 divErrorMsg' id='errordivNotes'
							style="display: block;"></div>
						<div class="span10">
							<label>
								<strong><spring:message code="lblEtrNot"/><span class="required">*</span>
								</strong>
							</label>
							<div class="span10" id='divTxtNode'
								style="padding-left: 0px; margin-left: 0px;">
								<label class="redtextmsg">
									<spring:message code="msgHowToCopyPastCutDouc"/>
								</label>
								<textarea readonly id="txtNotes" name="txtNotes" class="span10"
									rows="4"></textarea>
							</div>
							<input type="hidden" id="teacherIdForNote"
								name="teacherIdForNote" value="">
							<input type="hidden" name="noteDateTime" id="noteDateTime"
								value="${dateTime}" />
							<div id='fileNotes' style="padding-top: 8px;">
								<a href='javascript:void(0);' onclick='addNoteFileType();'><img
										src='images/attach.png' />
								</a>
								<a href='javascript:void(0);' onclick='addNoteFileType();'>Attach
									a File</a>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<c:set var="chkSaveDisp" value="inline" />
				<c:choose>
					<c:when test="${userMaster.entityType eq 1}">
						<c:set var="chkSaveDisp" value="none" />
					</c:when>
					<c:otherwise>
						<c:set var="chkSaveDisp" value="inline" />
					</c:otherwise>
				</c:choose>
				<!-- <span id="spnBtnCancel" style="display: ${chkSaveDisp}"><a href="#"	onclick="return cancelNotes()">Cancel</a></span> -->
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick='showCommunicationsDiv();'>
					Cancel
				</button>
				&nbsp;
				<span id="spnBtnSave" style="display: ${chkSaveDisp}"><button
						class="btn btn-primary" onclick="saveNotes()">
						Save
						<i class="icon"></i>
					</button>&nbsp;</span>
			</div>
		</div>
	</div>
</div>
<!--Add message Div by  Sekhar  -->
<div class="modal hide" id="myMsgShow" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showMessageDiv()">
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='message2show'>
					Your message is successfully sent to the Candidate.
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showMessageDiv()">
					Ok
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="tfaMsgShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='closeTFAMsgBox();'>
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='showTfaMsgBody'>
					You are attempting to change candidate portfolio information. This means you are altering the candidates application, which should only be done when there is confirmation that the candidate has included inaccurate information. The candidate will be alerted you are making this change. Do you really want to change this candidate's TFA status? 
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" onclick="saveTFAbyUser();">Save<i class="icon"></i></button>
				<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeTFAMsgBox();'>Cancel</button>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="teacherDetailId" name="teacherDetailId">
<div class="modal hide" id="myModalMessage" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">

	<input type="hidden" id="emailId" name="emailId">
	<div class="modal-dialog" style="width: 690px">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick='showCommunicationsDivForMsg();'>
					x
				</button>
				<h3 id="myModalLabel">
					Post a message to the Candidate
				</h3>
			</div>
			<iframe id='uploadMessageFrameID' name='uploadMessageFrame'
				height='0' width='0' frameborder='0' scrolling='yes'
				sytle='display:none;'>
			</iframe>
			<form id='frmMessageUpload' enctype='multipart/form-data'
				method='post' target='uploadMessageFrame' class="form-inline"
				onsubmit="return validateMessage();"
				action='messageCGUploadServlet.do' accept-charset="UTF-8">

				<div class="modal-body-cgstatusnotes" style="max-height: 500px;">
					<div class="" id="divMessages">
					</div>
					<div class="control-group" style="margin-top: 5px">
						<div class='divErrorMsg' id='errordivMessage'
							style="display: block;"></div>
					</div>

					<%---------- Gagan :District wise Dynamic Template [Start]  --%>
					<div id="templateDiv" style="display: none;">
						<div class="row col-md-12">
							<div class="row col-sm-6" style="max-width: 300px;">
								<label>
									<strong>Template</strong>
								</label>
								<br />
								<select class="form-control" id="districttemplate"
									onchange="setTemplate()">
									<option value="0">
										Select Template
									</option>
								</select>
							</div>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<button class="btn btn-primary hide top26"
								id="btnChangetemplatelink" onclick="return getTemplate();">
								Apply Template
							</button>
							<input type="hidden" class="span1"
								id="confirmFlagforChangeTemplate"
								name="confirmFlagforChangeTemplate" value="0">
						</div>
					</div>
					<%---------- Gagan :District wise Dynamic Template [END]  --%>

					<%---------- Rahul :District wise Document [START]  --%>

                    	<div class="row col-md-12" id="documentdiv">
		    	<div class="row col-sm-6" style="max-width:300px;">
			    	<label><strong>Document</strong></label>
		        	<br/>
		        	<select class="form-control" id="districtDocument" onchange="showDocumentLink()">
		        		<option value="">Select Document</option>
		        	</select>
		        	
			
		         </div>	<br><br>
		         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		        	<a href="javascript:void(0);" id="viewDoc" class="top26 hide" onclick="return vieDistrictFile(0)">View File</a>
		    	</div>
                    <%---------- Rahul :District wise Document [END]  --%>
					<div class="control-group row"
						style="padding-left: 15px; padding-top: 5px;">
						<label>
							<strong>To<br />
							</strong><span id="emailDiv"
								style="width: 612px; word-wrap: break-word; padding-left: 1px;"></span>
					</div>
					<div id='support' class="row" style="padding-left: 15px;">
						<div class="control-group">
							<div class="">
								<label>
									<strong><spring:message code="lblSub"/></strong><span class="required">*</span>
								</label>
								<input id="messageSubject" name="messageSubject" type="text"
									class="form-control" style='width: 612px;' maxlength="100" />
							</div>
						</div>

						<div class="control-group">
							<div class="" id="messageSend" style="width: 612px;">
								<label>
									<strong>Message</strong><span class="required">*</span>
								</label>
								<label class="redtextmsg">
									<spring:message code="msgHowToCopyPastCutDouc"/>
								</label>
								<textarea rows="5" class="form-control" cols="" id="msg"
									name="msg" maxlength="1000"></textarea>
							</div>
							<input type="hidden" id="teacherIdForMessage"
								name="teacherIdForMessage" value="">
							<input type="hidden" name="messageDateTime" id="messageDateTime"
								value="${dateTime}" />
							<div id='fileMessages' style="padding-top: 10px;">
								<a href='javascript:void(0);' onclick='addMessageFileType();'><img
										src='images/attach.png' />
								</a>
								<a href='javascript:void(0);' onclick='addMessageFileType();'>Attach
									a File</a>
							</div>
						</div>
					</div>
					<div id='lodingImage'
						style="display: block; text-align: center; padding-top: 4px;"></div>
					<!-- end support div -->
				</div>

			</form>
			<c:set var="chkSendDisp" value="inline" />
			<c:choose>
				<c:when test="${userMaster.entityType eq 1}">
					<c:set var="chkSendDisp" value="none" />
				</c:when>
				<c:otherwise>
					<c:set var="chkSendDisp" value="inline" />
				</c:otherwise>
			</c:choose>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick='showCommunicationsDivForMsg();'>
					Cancel
				</button>
				&nbsp;
				<button class="btn btn-primary" onclick="validateMessage()"
					style="display: ${chkSendDisp}">
					Send
				</button>
				&nbsp;
			</div>
		</div>
	</div>
</div>

<!--  Reference Check Model -->
<div class="modal hide" id="myModalReferenceCheck" >
<input type="hidden" name="teacherDetailsForReference" id="teacherDetailsForReference" value="" />
<input type="hidden" name="teacherIdForReference" id="teacherIdForReference" value="" />
		<div class="modal-dialog" style="width:728px;">
        <div class="modal-content">		
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeReferenceCheck(1)'>x</button>
				<h3 id="statusTeacherTitle"></h3>
			</div>
			<div class='divErrorMsg left2' id='errorStatusReferenceCheck' style="display: block;padding-left: 15px;"></div>
			<div  class="modal-body">
				<div class="" id="referenceRecord">

				</div>
		 	</div>
		 	
		 	<div class="modal-footer">
		 		<span id="statusSentRefChk">&nbsp;&nbsp;</span>
		 		<button type="button" class="btn"  onclick='closeReferenceCheck(1)'>Cancel</button>
		 	</div>
	    </div>
	</div>
</div>

<%-- ---------- GAgan : Apply Template Confirm Pop up [Start] ----------------%>
<div class="modal hide" id="confirmChangeTemplate" tabindex="-1" style="z-index: 1200 !important;"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog-for-cgmessage">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" id=''>
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="ddd">
						Do you really want to override the existing template in Message
						field with the selected template? You may lose any change done in
						existing template if you override it with selected template. If
						yes, click on "OK" button otherwise click "Cancel" button.
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-large btn-primary"
						onclick="confirmChangeTemplate()">
						Ok
						<i class="icon"></i>
					</button>
				</span>
				<span><button class="btn" data-dismiss="modal"
						aria-hidden="true">
						Cancel
					</button>
				</span>
			</div>
		</div>
	</div>
</div>
<%------- GAgan : Apply Template Confirm Pop up [END] ---------%>

<div class="modal hide" id="myModalPhone" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class='modal-dialog-for-teachercontented'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showCommunicationsForPhone();">
					x
				</button>
				<h3 id="myModalLabel">
					Phone
				</h3>
			</div>

			<div class="modal-body">
				<div class="control-group">
					<div id="divPhoneGrid"></div>
					<iframe id='uploadPhoneFrameID' name='uploadPhoneFrame' height='0'
						width='0' frameborder='0' scrolling='yes' sytle='display:none;'>
					</iframe>
					<form id='frmPhoneUpload' enctype='multipart/form-data'
						method='post' target='uploadPhoneFrame' class="form-inline"
						onsubmit="return savePhone();" action='phoneUploadServlet.do'
						accept-charset="UTF-8">
						<div class="mt10">
							<div class="span6 hide" id='calldetrailsdiv'>
								<div class='divErrorMsg' id='errordivPhone'
									style="display: block;"></div>
								<label>
									<strong>Enter Call Detail<span class="required">*</span>
									</strong>
								</label>
								<div class="span10" id='divTxtPhone'
									style="padding-left: 0px; margin-left: 0px;">
									<label class="redtextmsg">
										<spring:message code="msgHowToCopyPastCutDouc"/>
									</label>
									<textarea readonly id="divTxtPhone" name="divTxtPhone"
										class="span6" rows="4"></textarea>
								</div>
								<input type="hidden" id="teacherIdForPhone"
									name="teacherIdForPhone" value="">
								<input type="hidden" name="phoneDateTime" id="phoneDateTime"
									value="${dateTime}" />
								<div id='filePhones' style="padding-top: 8px;">
									<a href='javascript:void(0);' onclick='addPhoneFileType();'><img
											src='images/attach.png' />
									</a>
									<a href='javascript:void(0);' onclick='addPhoneFileType();'>Attach
										a File</a>
								</div>
							</div>
						</div>
					</form>

				</div>
			</div>
			<c:set var="chkSavePhone" value="inline" />
			<c:choose>
				<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
					<c:set var="chkSavePhone" value="inline" />
				</c:when>
				<c:otherwise>
					<c:set var="chkSavePhone" value="none" />
				</c:otherwise>
			</c:choose>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showCommunicationsForPhone();">
					Cancel
				</button>
				<button class="btn btn-primary hide"
					style="display: ${chkSavePhone}" id='calldetrailsbtn'
					onclick="savePhone()">
					Save&nbsp;
				</button>
			</div>

		</div>
	</div>
</div>

<div class="modal hide" id="saveToFolderDiv" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 900px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="defaultTeacherGrid()";>
					x
				</button>
				<h3 id="myModalLabel">
					Save Candidate
				</h3>
			</div>
			<div class="modal-body"
				style="max-height: 500px; overflow-y: scroll;">
				<div class="control-group">
					<div
						style="border: 0px solid green; width: 220px; height: 450px; float: left;"
						class="span5">
						<div class="span5" id="tree_menu"
							style="padding: 8px 0px 10px 2px;">
							<a data-original-title='Create' rel='tooltip' id='createIcon'><span
								id="btnAddCode" style="cursor: pointer;"
								class='icon-folder-open  icon-large iconcolor'></span>
							</a>&nbsp;&nbsp;
							<a data-original-title='Rename' rel='tooltip' id='renameIcon'><span
								id="renameFolder" class='icon-edit  icon-large iconcolor'></span>
							</a>&nbsp;&nbsp;
							<a data-original-title='Cut' rel='tooltip' id='cutIcon'><span
								id="cutFolder" class='icon-cut  icon-large iconcolor'></span>
							</a>&nbsp;&nbsp;
							<a data-original-title='Copy' rel='tooltip' id='copyIcon'><span
								id="copyFolder" class='icon-copy  icon-large iconcolor'></span>
							</a>&nbsp;&nbsp;
							<a data-original-title='Paste' rel='tooltip' id='pasteIcon'><span
								id="pasteFolder" class='icon-paste  icon-large iconcolor'></span>
							</a>&nbsp;&nbsp;
							<a data-original-title='Delete' rel='tooltip' id='deleteIcon'><span
								id="deletFolder" class='icon-remove-sign  icon-large iconcolor'></span>
							</a>
						</div>						
						<span id="treeiFrameTP">
<!-- hide by Anurag	and load dynamic from js teacherinfonew => saveToFolderJFTNULL()				<iframe id="iframeSaveCandidate"  src="tree.do" class="pull-left" scrolling="auto"  frameBorder="1" style="border: 0px; padding: 0px; margin: 0px; overflow:visible; text-align: top; vertical-align: top;width:220px;height:420px;"></iframe>-->
						</span>
					</div>
					<%-- ======================================== Right Div ========================================================================= --%>
					<div class="span10"
						style="padding-top: 39px; border: 0px solid green; float: left;">
						<input type="hidden" id="savecandidatearray"
							name="savecandidatearray">
						<input type="hidden" id="txtoverrideFolderId"
							name="txtoverrideFolderId">
						<input type="hidden" id="teachetIdFromPoPUp"
							name="teachetIdFromPoPUp" value="">
						<input type="hidden" id="teacherIdForHover"
							name="teacherIdForHover" value="">

						<input type="hidden" id="txtflagpopover" name="txtflagpopover"
							value="">
						<%-- If It is 1 -> that means User Clicked on Pop up --%>
						<div id="savedCandidateGrid">

						</div>
					</div>
					<div style="clear: both"></div>
				</div>

			</div>
			<div class="modal-footer">
				<span id=""><button class="btn  btn-primary"
						onclick="saveCandidateToFolderByUser()">
						<spring:message code="btnSave"/>
						<i class="icon"></i>
					</button>
				</span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="defaultTeacherGrid()";>
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<%--- ============================================ Gagan: Share Folder Div ======================================================== --%>
<input type="hidden" id="entityType" name="entityType"
	value="${userMaster.entityType}">
<div class="modal hide" id="shareDiv" style="border: 0px solid blue;"
	tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 935px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="defaultTeacherGrid()";>
					x
				</button>
				<h3 id="myModalLabel">
				<spring:message code="headShareCandidate"/>
				</h3>

				<input type="hidden" id="savecandiadetidarray"
					name="savecandiadetidarray" />
				<input type="hidden" id="JFTteachetIdFromSharePoPUp"
					name="JFTteachetIdFromSharePoPUp" value="">
				<input type="hidden" id="txtflagSharepopover"
					name="txtflagSharepopover" value="">
				<%-- If It is 1 -> that means User Clicked on Pop up --%>
			</div>
			<div class="modal-body">
				<div class="control-group">

					<div class="">
						<div class="row">
							<div class="col-md-10">
								<div class='divErrorMsg span12' id='errorinvalidschooldiv'></div>
							</div>
							<div class="col-sm-4 col-md-4">
								<label>
							<spring:message code="lblDistrictName"/>
								</label>
								</br>
								<c:if test="${DistrictName!=null}">
				             	${DistrictName}
				             	<input type="hidden" id="districtId"
										value="${DistrictId}" />
									<input type="hidden" id="districtName" value="${DistrictName}"
										name="districtName" />
								</c:if>
								<div id='divTxtShowData'
									onmouseover="mouseOverChk('divTxtShowData','districtName')"
									style='display: none; position: absolute; z-index: 5000;'
									class='result'></div>

							</div>

							<div class="col-sm-6 col-md-6">
								<label>
									<spring:message code="lblSchoolName"/>
								</label>
								<c:if test="${SchoolName==null}">
									<input type="text" id="shareSchoolName" maxlength="100"
										name="shareSchoolName" class="form-control" placeholder=""
										onfocus="getSchoolAuto(this, event, 'divTxtShowData3', 'shareSchoolName','districtId','');"
										onkeyup="getSchoolAuto(this, event, 'divTxtShowData3', 'shareSchoolName','districtId','');"
										onblur="hideSchoolMasterDiv(this,'schoolId','divTxtShowData3');" />
									<input type="hidden" id="schoolId" value="0" />
								</c:if>
								<c:if test="${SchoolName!=null}">
									<%-- ${SchoolName}--%>
									<input type="text" id="shareSchoolName" maxlength="100"
										name="shareSchoolName" class="form-control" placeholder=""
										onfocus="getSchoolAuto(this, event, 'divTxtShowData3', 'shareSchoolName','districtId','');"
										onkeyup="getSchoolAuto(this, event, 'divTxtShowData3', 'shareSchoolName','districtId','');"
										onblur="hideSchoolMasterDiv(this,'schoolId','divTxtShowData3');"
										value="${SchoolName}" />
									<input type="hidden" id="schoolId" value="${SchoolId}" />
									<input type="hidden" id="loggedInschoolId" value="${SchoolId}" />
									<input type="hidden" id="loggedInschoolName"
										value="${SchoolName}" />
								</c:if>
								<div id='divTxtShowData3'
									onmouseover="mouseOverChk('divTxtShowData3','shareSchoolName')"
									style='display: none; position: absolute; z-index: 5000;'
									class='result'></div>

							</div>
							<div class="col-md-2 top25">
								<label>
									&nbsp;
								</label>
								<input type="button" id="" name="" value="Go"
									onclick="searchUserthroughPopUp(1)" class="btn  btn-primary">
							</div>
						</div>
					</div>


					<div id="divShareCandidateToUserGrid"
						style="border: 0px solid green;" class="mt30">
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary"
						onclick="shareCandidatethroughPopUp()">
						<spring:message code="btnShare"/>
						<i class="icon"></i>
					</button>
				</span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="defaultTeacherGrid()";>
					<spring:message code="btnClr"/>
				</button>
			</div>
		</div>
	</div>
</div>
<div class="modal hide" id="shareConfirm" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headTm"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="saveAndShareConfirmDiv" class="">
						<spring:message code="msgYuHavaSuccssSharedCandidateToStrUser"/>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClsoe"/>
				</button>
			</div>
		</div>
	</div>
</div>
</div>

<div class="modal hide" id="deleteShareCandidate"
	style="border: 0px solid blue;" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
				 <spring:message code="headTm"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
				<spring:message code="msgDeletetStrCondidate"/>

				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary"
						onclick="deleteCandidate()">
					 <spring:message code="btnOk"/>
						<i class="icon"></i>
					</button>
				</span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					 <spring:message code="btnClr"/>
				</button>
			</div>
		</div>
	</div>
</div>
</div>

<div class="modal hide" id="duplicatCandidate"
	style="border: 0px solid blue;" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headTm"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<spring:message code="msgCandidateSvav/Cancel"/>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary"
						onclick="saveWithDuplicateRecord()">
						<spring:message code="btnOk"/>
						<i class="icon"></i>
					</button>
				</span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClr"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="deleteFolder"
	style="border: 0px solid blue;" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headTm"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<spring:message code="msgDoyouWantToDeleteFolder"/>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary"
						onclick="deleteconfirm()">
						<spring:message code="btnOk"/>
						<i class="icon"></i>
					</button>
				</span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					 <spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalQAEXEditor" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showPreviousModel();">
					x
				</button>
				<h3>
					
              <spring:message code="headExp"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="row mt10">
					<div class="span10">
						<div id='divExplain' style="margin-top: -15px;">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showPreviousModel();">
			
					<spring:message code="btnClr"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myJobDetailList" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class='modal-dialog' style="width: 1012px">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="defaultTeacherGrid();">
					x
				</button>
				<h3>
					<spring:message code="headAply/AndCandToOthrJobs"/>
				</h3>
			</div>
			<input type='hidden' id='tId4Job'>
			<div class="modal-body">
				<div class="control-group">

					<div class="row">
						<div class="col-sm-8 col-md-8">
							<div class="row">
								<div class="col-sm-6 col-md-6">
									<label>
									<c:if test="${DistrictName!=null}">
										<strong><spring:message code="lblDistrictName"/></strong>
									</c:if>
									<c:if test="${headQuarterMaster!=null}">
										<strong>HeadQuarter Name</strong>
									</c:if>
									<c:if test="${branchMaster!=null}">
										<strong>HeadQuarter Name</strong>
									</c:if>
									</label>
									<c:if test="${headQuarterMaster!=null}">
										<input type="text" class="form-control"
											value="${headQuarterMaster.headQuarterName}" disabled="disabled" />
										<input type="hidden" id="headQuarterId" value="${headQuarterMaster.headQuarterId}" />
										<input type="hidden" id="headQuarterName" value="${headQuarterMaster.headQuarterName}"
											name="headQuarterName" />
									</c:if>
									<c:if test="${branchMaster!=null}">
										<input type="text" class="form-control"
											value="${branchMaster.branchName}" disabled="disabled" />
										<input type="hidden" id="branchId" value="${branchMaster.branchId}" />
										<input type="hidden" id="branchName" value="${branchMaster.branchName}"
											name="branchName" />
									</c:if>
									<c:if test="${DistrictName!=null}">
										<input type="text" class="form-control"
											value="${DistrictName}" disabled="disabled" />
										<input type="hidden" id="districtId" value="${DistrictId}" />
										<input type="hidden" id="districtName" value="${DistrictName}"
											name="districtName" />
									</c:if>
								</div>
								<c:choose>
								<c:when test="${DistrictName!=null}">
								<div class="col-sm-6 col-md-6">
									<c:choose>
										<c:when test="${schoolName==null}">
											<label>
												<spring:message code="lblSchoolName"/>
											</label>
											<input type="text" id="schoolName1" maxlength="100"
												name="schoolName1" class="form-control" placeholder=""
												onfocus="getSchoolAuto(this, event, 'divTxtShowData4', 'schoolName','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowData4', 'schoolName1','districtId','');"
												onblur="hideSchoolMasterDiv(this,'schoolId1','divTxtShowData4');" />
											<input type="hidden" id="schoolId1" value="0" />
											<div class="span5" id='divTxtShowData4'
												onmouseover="mouseOverChk('divTxtShowData4','shareSchoolName')"
												style='display: none; position: absolute; z-index: 5000; margin: 0px;'
												class='result'></div>
										</c:when>
										<c:otherwise>
											<label>
												<spring:message code="lblSchoolName"/>
											</label>
											<input type="text" class="form-control" id="schoolName1"
												value="${schoolName}" disabled="disabled" />
											<input type="hidden" id="schoolId1" value="${SchoolId}" />
										</c:otherwise>
									</c:choose>
								</div>
								</c:when>
									<c:otherwise>
									<div class="col-sm-6 col-md-6">
									<label>Branch Name</label>
										<c:choose>
											<c:when test="${branchMaster==null}">
											<input type="text" id="bnchName" name="bnchName"  class="form-control" value="${branchName}"
       					onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'bnchName','bnchId','');"
						onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'bnchName','bnchId','');"
						onblur="hideBranchMasterDiv(this,'bnchId','divTxtShowDataBranch');"/>
						<div id='divTxtShowDataBranch' style='display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','bnchName')" class='result' ></div>
						<input  type="hidden" id="bnchId" name="bnchId" value=""/>	
											</c:when>
											<c:otherwise>
											<input type="text" id="bnchName" name="bnchName"  class="form-control" value="${branchMaster.branchName}" readonly/>
											<input  type="hidden" id="bnchId" name="bnchId" value="${branchMaster.branchId}"/>
											</c:otherwise>
										</c:choose>
										</div>
									</c:otherwise>
								</c:choose>
								<div class="col-sm-6 col-md-6">
									<span class=""><label class="">
										<spring:message code="lblJobCat"/>
										</label> <select class="form-control " id="jobCategoryId"
											name="jobCategoryId" onchange="activecityType();">
											<option value="0">
										<spring:message code="optStrJobCat"/>
											</option>
											<c:if test="${not empty lstJobCategoryMasters}">
												<c:forEach var="jobCateObj" items="${lstJobCategoryMasters}">
													<option value="${jobCateObj.jobCategoryId}">
														${jobCateObj.jobCategoryName}
													</option>
												</c:forEach>
											</c:if>
										</select> </span>
								</div>
								<div class="col-sm-6 col-md-6">
									<label>
										<spring:message code="lblJobId"/>
									</label>
									<input class="form-control" type="text" name="jobId" id="jobId"
										onkeypress="return checkForInt(event);" />
								</div>
							</div>
						</div>

						<div class="col-sm-4 col-md-4">
							<label>
								<spring:message code="lblSub"/>
							</label>
							<select class="form-control" multiple="multiple" name="subjects1"
								id="subjects1" style="height: 90px;">
								<option value="0">
								<spring:message code="optAll"/>
								</option>
								<c:if test="${not empty subjectMasters}">
									<c:forEach var="subjectMaster" items="${subjectMasters}">
										<option value="${subjectMaster.subjectId}">
											${subjectMaster.subjectName}
										</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
					</div>
					<div class="row top15" style="margin-top: 0px;">
						<!-- zone added by mukesh -->
						<div class="col-sm-4 col-md-4">
							<label>
								<spring:message code="lblZone"/>
							</label>
							<select class="form-control" name="zone" id="zone"
								disabled="disabled">

							</select>
						</div>
						
						 <c:choose>
                              <c:when test="${CheckNC=='2'}">
                 <div class="col-sm-4 col-md-4">
							<label>
								<spring:message code="lblReq/Posi"/>
							</label>
							<input type="text" class="form-control" id="regpos"	value=""  />
					</div>
                        	  </c:when>    
                          </c:choose>
											
						<div class="col-sm-3 col-md-3">
							<button class="btn  btn-primary top25-sm"
								onclick="searchJobDetailList($('#tId4Job').val());">
								<spring:message code="btnSearch"/>
								<i class="icon"></i>
							</button>
						</div>
					</div>

					<div id="divJobList" class="top15">

					</div>
				</div>
			</div>
			<div class="modal-footer">
			<div class="col-sm-3 col-md-3" align="left">
			<label class="checkbox">
						 <input type="checkbox" name="sendNotification" id="sendNotification" value="1">
                        <spring:message code="lblAutoNotifyTheCand"/></label>
				</div>
				<span id=""><button class="btn btn-primary"
						onclick="applyJob()">
						<spring:message code="btnAplyToJob"/>
						<i class="icon"></i>
					</button>
				</span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="defaultTeacherGrid();">
					<spring:message code="btnClr"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="jobApplied" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headTm"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="jobSuccess" class="">
						<spring:message code="msgYuHaveSuccApplTheStrJob"/>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClsoe"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="epiAndJsi" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'style="width: 700px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
				<spring:message code="headAssDetl"/>
				</h3>
			</div>
			<div class="modal-body" style="max-height: 450px;overflow-y:auto">
				<div id="epiAndJsiData"></div>
				
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClsoe"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide " id="geozoneschoolDiv" tabindex="-1"
	role="dialog" data-keyboard="false" aria-labelledby="myModalLabel"
	aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 650px; margin-top: 20px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="zoneSchoolFlags();">
					x
				</button>
				<h3 id="myModalLabelGeo"></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<input type="hidden" id="geozoneId" value="0" />
					<input type="hidden" id="geozoneschoolFlag" value="0" />
					<div id="geozoneschoolGrid"></div>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""> </span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="zoneSchoolFlags();">
					<spring:message code="btnClsoe"/>
				</button>
			</div>
		</div>
	</div>
</div>
<!--  -->
<div class="modal hide"  id="myModalMessageMassTP"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:690px">
	<div class="modal-content">
	<input type="hidden" id="emailIdsMassTP" name="emailIdsMassTP">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headPostMsgToCand"/></h3>
	</div>
	<iframe id='uploadMessageFrameIDMassTP' name='uploadMessageFrameMassTP' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	<form id='frmMessageUploadMassTP' enctype='multipart/form-data' method='post' target='uploadMessageFrameMassTP'  class="form-inline" onsubmit="return validateMessageMassTP(1);" action='messageUploadServletMassTP.do' accept-charset="UTF-8">
	
	<div class="modal-body" style="max-height: 450px;overflow-y:auto;">
		<div class="" id="divMessagesMassTP">						
		</div>
		<div class="control-group" style="margin-top: 5px;">
			<div class='divErrorMsg' id='errordivMessageMassTP' style="display: block;"></div>
		</div>
		<div id="templateDivTP" style="display: none;">		     	
		    	<div class="row col-md-12" >
		    	<div class="row col-sm-6" style="max-width:300px;">
			    	<label><strong><spring:message code="lblTemplate"/></strong></label>
		        	<br/>
		        	<select class="form-control" id="districttemplateTP" onchange="setTemplateTP()">
		        		<option value="0"><spring:message code="sltTemplate"/></option>
		        	</select>
		         </div>	
		        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="btn btn-primary hide top26" id="btnChangetemplatelinkTP" onclick="return getTemplateTP();">Apply Template</button> <input type="hidden" class="span1" id="confirmFlagforChangeTemplateTP" name="confirmFlagforChangeTemplateTP" value="0">
			
		    	</div>
		    </div>
		    <!-- Rahul:17/11/2014    Document Div------>
		    
		       	<div class="row col-md-12" id="documentdivTP">
		    	<div class="row col-sm-6" style="max-width:300px;">
			    	<label><strong><spring:message code="lblDoc"/></strong></label>
		        	<br/>
		        	<select class="form-control" id="districtDocumentTP" onchange="showDocumentLinkTP()">
		        		<option value="0"><spring:message code="sltDoc"/></option>
		        	</select>
		         </div>	<br><br>
		         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		        	<a href="javascript:void(0);" id="viewDocTP" class="top26 hide" onclick="return vieDistrictFile(1)"><spring:message code="lnkVFile"/></a>
		    	</div>
		    <!-- END------>
		<div class="control-group row" style="padding-left: 15px;padding-top: 5px;">
    		<label><strong><spring:message code="lblTo"/><br/></strong><span id="emailDivMassTP" style="width:612px;word-wrap:break-word;padding-left:1px;"></span>
   		</div>
		<div id='support' class="row" style="padding-left:15px;" >
		<div class="control-group">
			<div class="">
		    	<label><strong><spring:message code="lblSub"/></strong><span class="required">*</span></label>
	        	<input id="messageSubjectMassTP" name="messageSubjectMassTP" type="text" class="form-control" style='width:612px;' maxlength="100" />
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="messageSendMassTP" style="width: 612px;">
		    	<label><strong><spring:message code="lblMsg"/></strong><span class="required">*</span></label>
		    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/>
 				</label>
	        	<textarea rows="5" class="form-control" cols="" id="msgMassTP" name="msgMassTP" maxlength="1000"></textarea>
	        </div>
	        <input type="hidden" id="teacherIdForMessageMassTP" name="teacherIdForMessageMassTP" value="">
	    	<div id='fileMessagesMassTP' style="padding-top:12px;">
        		<a href='javascript:void(0);' onclick='addMessageFileTypeMassTP();'>
        		<img src='images/attach.png'/></a> 
        		<a href='javascript:void(0);' onclick='addMessageFileTypeMassTP();'><spring:message code="lnkAttachFile"/></a>
        	</div>   
		</div>
 		</div>
 		<div id='lodingImage' style="display: block;text-align:center;padding-top:4px;"></div>
 		<!-- end support div -->
  	</div>
  	
  	</form>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>&nbsp;
 		<button class="btn btn-primary" onclick="validateMessageMassTP(1)"><spring:message code="btnSend"/></button>&nbsp;
 	</div>
</div>
</div>
</div>

<input type="hidden" id="sourcefileNameMassTP" value=""/>
<input type="hidden" id="sourcefilePathMassTP" value=""/>

<!--  -->

<div  class="modal hide"  id="myModalMsgShow_SendMessageMassTP"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog' style="width:35%;">
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2show_SendMessage'><spring:message code="msgYuMsgSuccessSentToCand"/>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
   </div>
  </div>
</div>

<!--  -->
<!-- add epi timer div  -->
<div class="modal hide" id="epiTimeModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					×
				</button>
				<h3 id="myModalLabel"><span id="teachername"></span>					
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id="assesmentRec">
					
				</div>
			</div>
			<div class="modal-footer">
			<span><button type="button" class="btn btn-large btn-primary" id="assessmentSave" onclick="return insertOrUpdateAssessmentTime();"><strong>Save <i class="icon" style='margin-top:5px!important;'></i></strong></button>&nbsp;&nbsp;</span>		 		
				<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>
			</div>
		</div>
	</div>
</div>

<!--Rahul:17/11/2014 district attachment div-->

<div  class="modal hide distAttachmentShowDiv"  id="distAttachmentShowDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameEEOC11"><spring:message code="hdAttachment"/></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divPnrAttachment">
					 
					</div>
					<iframe src="" id="ifrmAttachment" width="100%" height="480px"> </iframe>   
				</div>
				
		 	</div>
		 	<input type="hidden" id="eligibilityMasterId" value=""/>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv1()" ><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>
<!-- end timer div -->

<!--  Model For showing Question Detail -->
<div class="modal hide" id="myModalReferenceCheckFeedBack">
<input type="hidden" name="teacherDetailsCG" id="teacherDetailsCG" value=""/>
<input type="hidden" name="candidateFullName" id="candidateFullName" value=""/>
<input type="hidden" name="teacherIdCG" id="teacherIdCG" value=""/>
		<div class="modal-dialog" style="width:728px;">
        <div class="modal-content">		
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeReferenceCheck(2)'>x</button>
				<h3 id="statusTeacherTitleReferenceChk"></h3>
			</div>
			<div class='divErrorMsg left2' id='errorStatusReferenceCheck' style="display: block; padding-left: 15px;"></div>
			<div  class="modal-body">
				<div class="" id="referenceFeedBack">

				</div>
		 	</div>
		 	
		 	<div class="modal-footer">
		 			<button class="btn"  onclick='closeReferenceCheck(2)'><spring:message code="btnClr"/></button>
		 	</div>
	    </div>
	</div>
</div>

<div class="modal hide" id="myModalReferenceCheckFeedBackSLC">
	<div class="modal-dialog" style="width:728px;">
        <div class="modal-content">		
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeReferenceCheck(3)'>x</button>
				<h3 id="statusTeacherTitleReferenceChkSLC"></h3>
			</div>
			<div class='divErrorMsg left2' id='errorStatusReferenceCheck' style="display: block; padding-left: 15px;"></div>
			<div  class="modal-body">
				<div class="" id="referenceFeedBackSLC">

				</div>
		 	</div>

		 	<div class="modal-footer">
		 		<span id="buttonFinalizeScoreSLC">
		 		<button class="btn  btn-large btn-primary" onclick="saveMaxScoreSLC('SLC')"><spring:message code="btnFinalize"/> <i class="iconlock"></i></button>&nbsp;&nbsp;</span>
		 		</span>
		 		<button class="btn"  onclick='closeReferenceCheck(3)'><spring:message code="btnClr"/></button>
		 	</div>
	    </div>
	</div>
</div>

<div class="modal hide" id="myModalReferenceCheckConfirm" >
	<div class="modal-dialog" style="width:728px;">
        <div class="modal-content">		
			<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeReferenceCheck(4)'>x</button>
				<h3 id="statusTeacherTitleForConfirmation"></h3>
			</div>
			<div class='divErrorMsg left2' id='errorStatusReferenceCheck' style="display: block; padding-left: 15px;"></div>
			<div  class="modal-body">
				<div class="" id="referenceRecord">
					<spring:message code="headScoreForQuesSetZero"/><BR/>
				</div>
		 	</div>

		 	<div class="modal-footer">
		 		<button class="btn btn-large btn-primary" type="button" onclick="saveMaxScorePool('auto');"><strong><spring:message code="btnConti"/> <i class="icon"></i></strong></button>
		 		<button type="button" class="btn"  onclick='closeReferenceCheck(4)'><spring:message code="btnClr"/></button>
		 	</div>

	    </div>
	</div>
</div>

<iframe src="" id="ifrmRef" width="100%" height="480px"
	style="display: none;"></iframe>
<input type="hidden" id="jobId" name="jobId" value="" />
<input type="hidden" id=phoneType name="phoneType" value="0" />
<input type="hidden" id=msgType name="msgType" value="0" />
<input type="hidden" id="noteId" name="noteId" value="" />
<input type="hidden" id="jobForTeacherGId" name="jobForTeacherGId" value="" />
<input type="hidden" id="commDivFlag" name="commDivFlag" value="" />
<input type="hidden" name="userFPD" id="userFPD" />
<input type="hidden" name="userFPS" id="userFPS" />
<input type="hidden" name="userCPD" id="userCPD" />
<input type="hidden" name="userCPS" id="userCPS" />
<input type="hidden" name="folderId" id="folderId" />
<input type="hidden" name="checkboxshowHideFlag"
	id="checkboxshowHideFlag" />
<input type="hidden" name="teachersharedId" id="teachersharedId" />
<input type="hidden" name="pageFlag" id="pageFlag" value="0" />
<input type="hidden"  name="currentPopUpId" id="currentPopUpId" value=""/>
<jsp:include page="statuscgupdatefrom_tp.jsp"></jsp:include>

<script type="text/javascript">
$('#internalCand').tooltip();
$('#formalCand').tooltip();
$('#retiredCand').tooltip();
$('#createIcon').tooltip();
$('#renameIcon').tooltip();
$('#cutIcon').tooltip();
$('#copyIcon').tooltip();
$('#pasteIcon').tooltip();
$('#deleteIcon').tooltip();
//displayTeacherGrid();
$('#myModal').modal('hide');
$(document).ready(function(){
$('#divTxtNode').find(".jqte").width(612);
$('#messageSend').find(".jqte").width(612);
$('#statusNotes').find(".jqte").width(670);
});
</script>
<c:choose>
	<c:when test="${isTeacherPoolOnLoad or entityID==1}">
		<script>
				displayTeacherGrid();
		</script>
	</c:when>
<c:otherwise>
	<script>
			$('#message2show').html("To search for Candidates, please enter the search criteria and click on \"Search\" button");
			//Anurag  $('#myModal2').modal('show');
	</script>      
</c:otherwise>
</c:choose>

<script>
$(document).ready(function(){
	 // $('#divTxtPhone').find(".jqte").width(614);
}) 
 /* ---------------------- Create Folder Method Start here --------------- */
 $("#btnAddCode").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.createUserFolder();
 });
/*---------------- ------------- Create Folder Method End  here --------------- */
/*-----------------  Rename Folder ------------------------*/
 $("#renameFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.renameFolder();
 });
/*-----------------  Cut Copy Paste Folder ------------------------*/
 $("#cutFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.cutFolder(); 
 });
 
 $("#copyFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.copyFolder();  
 });
 
 $("#pasteFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.pasteFolder(); 
 });
  
 $("#deletFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.deletFolder();
 }); 
</script>

<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("fromDate", "fromDate", "%m-%d-%Y");
     cal.manageFields("toDate", "toDate", "%m-%d-%Y");
     
     cal.manageFields("jobAppliedFromDate", "jobAppliedFromDate", "%m-%d-%Y");
     cal.manageFields("jobAppliedToDate", "jobAppliedToDate", "%m-%d-%Y");
     
     cal.manageFields("epiFromDate", "epiFromDate", "%m-%d-%Y");
     cal.manageFields("epiToDate", "epiToDate", "%m-%d-%Y");
     
    //]]>
</script>
<!--Start : @ashish :: for Teacher Profile {draggableDiv} -->

<script>
$(function() {
	$( "#draggableDivMaster" ).draggable({
		
		handle:'#teacherDiv', 
		containment:'window',
		
		revert: function(){
		var $this = $(this);
		var thisPos = $this.position();
		var parentPos = $this.parent().position();
		var x = thisPos.left - parentPos.left;
		var y = thisPos.top - parentPos.top;
		
			if(x<0 || y<180)
			{
				return true; 
				}else{
				return false;
			}
		}
	});
});
 getSubjectByDistrict();
 getJobCategoryByDistrict();
</script>
<jsp:include page="massstatusupdate.jsp"></jsp:include>
<!--End : @ashish :: for Teacher Profile {draggableDiv} -->
<script type="text/javascript">
  $('#pdfId').tooltip();
  $('#exlId').tooltip();
  $('#printId').tooltip();
</script>

<!-- Popup window -->
<div class="modal hide"  id="chgstatusRef1"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
		<spring:message code="msgDoUActRef"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="chgstatusRef2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeActRef"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="delVideoLnk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeleteVideoLnk"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="delVideoLnkConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="saveDataLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDataSaved"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	
 		<button class="btn  btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button> 		
 	</div>
</div>
	</div>
</div>

<div class='modal hide'  id='tagDivTP' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
</div>


<div class="modal hide" id="certTextDivDSPQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="certTextDivClose();">x</button>
		<h3 id="myModalLabel"><spring:message code="btnOk"/></h3>
	</div>
	<div class="modal-body" style="max-height: 400px;overflow-y:auto;"> 		
		<div class="control-group certTextContent">
			
		</div>
 	</div>
 	<div class="modal-footer">
 		<span id=""><button class="btn" data-dismiss="modal" onclick="certTextDivClose();" aria-hidden="true"><spring:message code="btnClose"/></button></span>&nbsp;&nbsp; 		
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="printmessage1"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	  <div class="modal-content">
	   <div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
		</div>
		 <div  class="modal-body"> 		
			<div class="control-group">
			<div class="" >
			<spring:message code="msgPlzPopAreAlloued"/>
			</div>
			</div>
	 	 </div>
	 	<div class="modal-footer">
	       <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
	 	</div>
     </div>
	</div>
</div>
<div  class="modal hide" id="myModalVideoInterview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog" style="width: 80%; " >
<div class="modal-content">        
<div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="">x</button>
    <h3 id="myModalLabel"><spring:message code="headVideoIntr"/></h3>
</div>
<div class="modal-body">        
<div class="control-group">-
        <div id="videoInterViewDiv"></div>
</div>
</div>
<div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true"  onclick=""><spring:message code="btnClr"/></button> 
</div>
</div>
</div>
</div>

<div class="modal hide" id="resetOnlineActivityModal" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 42%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headTm"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="resetOnlineActivityDiv"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="closeOnlineActivity();">
				<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>


  <div class="modal hide"  id="candidatestatusDivMain" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   <span id="myModalLabelStatus" ></span>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="row">
      			<div class="col-sm-12 col-md-12">
					<div class='divErrorMsg' id='statusErrorPoolDiv' style="display: block;"></div>
				</div>
			</div>
			
			<div class="row">
		      	<div class="col-sm-12 col-md-12">
		      	<input type="hidden" id='candidateCurrentStatus' name='candidateCurrentStatus' />
				  <div id="candidatestatusddDiv">
				
				  </div>
			</div>
		    </div>
 	</div>
 	<div class="modal-footer">
	 		<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='changeCandidateStatusFromPool();'>&nbsp;&nbsp;&nbsp;<spring:message code="btnChngStatus"/> &nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>

<div class="modal hide" id="myModal222" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog-for-cgsearch" style="width: 380px;">
		<div class="modal-content">		
			<div class="modal-header">
 				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showAndHideModal();">x</button>
				<h3 id="myModalLabel"><spring:message code="headTm"/> </h3>
			</div>
			<div class="modal-body"> 		
				<div id='msg2show'><spring:message code="msgCertiIsAleradySlt"/></div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary"" data-dismiss="modal" aria-hidden="true"  onclick="showAndHideModal();"><spring:message code="btnOk"/> </button> 
			</div>
		</div>
	</div>
</div>
<!--  Display  Video Play Div -->
<div  class="modal hide" id="videovDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%; " >
		<div class="modal-content">		
			<div class="modal-header">
			 		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="">x</button>
				<h3 id="myModalLabel">Video</h3>
			</div>
			<div class="modal-body">		
				<div class="control-group">
						<div style="display:none;" id="loadingDivWaitVideo">
						     <table  align="center" >						 		
						 		<tr><td align="center"><img src="images/loadingAnimation.gif"/></td></tr>
							</table>
						</div>						
						<div id="interVideoDiv"></div>
				</div>
			</div>
			<div class="modal-footer">	 			
	 		<button class="btn" id="closeBtn" data-dismiss="modal" aria-hidden="true">Close</button>
 			</div>			
		</div>
	</div>
</div>

<div class="modal hide"  id="mailconfrmModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="mailconf" class="modal-body"> 		
		<div class="control-group">
			Do you really want to send applicant management link?
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="teachId" value=""/>
 	<span id=""><button class="btn btn-large btn-primary" onclick="sendPortfolioReminderMail()" >Yes <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 		
 	</div>
</div>
	</div>
</div>

 <div class="modal hide"  id="bulkmailconfrmModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="mailconf" class="modal-body"> 		
		<div class="control-group">
			Do you really want to send applicant management link?
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="teachId" value=""/>
 	<span id=""><button class="btn btn-large btn-primary" onclick="sendPortfolioReminderBulkMail()" >Yes <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 		
 	</div>
</div>
	</div>
</div>


<div class="modal hide"  id="mailSuccessModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="mailconf" class="modal-body"> 		
		<div class="control-group">
			Candidate Pool Refresh link has been emailed to selected candidate(s).
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="teachId" value=""/>
 	<span id=""></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="octContentDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:950px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" onclick="showPrevProfile();" aria-hidden="true">x</button>
	   <h3 id="myModalLabel" ><span id="changeByName"></span></h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" id="octmsgToShow">
				
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" onclick="showPrevProfile();" aria-hidden="true">Ok</button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>

<div class="modal hide" id="confirmationMessageForPrintResume" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showConfirmationMessageForPrintResumeOk()">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<!-- SLC Report -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadCGSLCReport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >x</button>
		<h3 id="myModalLabelSLCNotes">SLC Notes</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
		
		  <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
		   <iframe id="ifrmCGSLCR" width="100%" height="480px" src=""></iframe>		  
		  </div>   
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Close</button> 		
   </div>
  </div>
 </div>

</div>

<!--  Model for Send Mail to All -->
 <div class="modal hide"  id="allCandidateSendMailModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="portfolio-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="mailconf" class="modal-body">
		<div class="control-group" style="margin-top: 5px;">
			<div class='divErrorMsg' id='errordivMessagePortfolio' style="display: block;"></div>
		</div> 		
		<div class="control-group">
			Do you want to send the Candidate Pool Refresh link to the selected candidate(s)?
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="teachId" value=""/>
 		<span id=""><button class="btn btn-large btn-primary" onclick="sendPortfolioReminderMailToSelectedUser()" >Send To Selected <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<span id=""><button class="btn btn-large btn-primary" onclick="sendPortfolioReminderMailToAllUser()" >Send To All <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="allCandidateMailconfrmModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="mailconf" class="modal-body"> 		
		<div class="control-group">
			Do you really want to send applicant management link?
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="teachId" value=""/>
 	<span id=""><button class="btn btn-large btn-primary" onclick="sendPortfolioReminderBulkMail()" >Yes <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 		
 	</div>
</div>
	</div>
</div>
<!--  End Model -->

</div>

<input type="hidden" name="userId" id="userId" value='${userMaster.userId}'>
<c:if test="${branchMaster!=null || headQuarterMaster!=null}">
<script type="text/javascript">
 getJobCategoryByHQAndBranch();
</script>	
</c:if>

<!-- Smart Practices Certificate modal div start -->
<div class="modal hide pdfDivBorder" id="modalDownloadSPC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">Smart Practices Certificate</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
		  			<div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
		   				<iframe id="ifrmSPC" width="100%" height="480px" src=""></iframe>
		  			</div>
				</div>
 			</div>
 			<div class="modal-footer">
 				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
   			</div>
  		</div>
	</div>
</div>
<!-- Smart Practices Certificate modal div end -->

<!-- Smart Practices Reset modal div start -->
<div class="modal hide" id="modalResetSp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<input type="hidden" id="teacherIdForResetSp" name="teacherIdForResetSp" value="">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeResetSp();">x</button>
				<h3 id="myModalLabel">Smart Practices Reset</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errordivResetSp' style="display: block;"></div>
					<div  id="resetSpDescription"><label><strong><spring:message code="lblResetSp"/><span class="required">*</span></strong></label><br/><br/>
<!--						<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>-->
						<textarea class="form-control" name="msgResetSp" id="msgResetSp" maxlength="750" rows="4" onkeyup="return chkLenOfTextArea(this,750);" onblur="return chkLenOfTextArea(this,750);"></textarea>
					</div>
				</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" id="btnResetSp" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				<button class="btn" onclick="closeResetSp();">Cancel</button>
   			</div>
  		</div>
	</div>
</div>
<!-- Reset message modal div start -->
<div class="modal" style="display: none;z-index: 5000" id="resetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">TeacherMatch</h3>
	      </div>
	      <div class="modal-body" id="epireset">
	      	<div class="control-group" id='messageConfirm'>
			</div>
<!--			shriram-->
			<div class="errorMsg" id="errorDivReset" style="display: block; color: red;"></div>
			<div>
			<label><b>Add reason</b><span class="required">*</span></label>
			<textarea></textarea></div>
	      </div>
	      <div class="modal-footer" id="footerbtn2">
	        	<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
	      </div>
	    </div>
	  </div>
</div>
<div class="modal hide" id="modalResetSpMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeResetSp()">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id="resetMsg2Show"></div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"	onclick="closeResetSp()">Ok</button>
			</div>
		</div>
	</div>
</div>
<!-- Smart Practices Reset modal div end -->

<script>
function endDateStartdate(msg)
{
	
	$('#dateValidation .modal-body').html(msg);
	$('#dateValidation').modal('show');
	$('#loadingDiv').hide();
}
</script>

<div class="modal hide" id="dateValidation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<!-- Assessment Reset modal div start -->
<div class="modal hide" id="modalResetAssessment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<input type="hidden" id="teacherIdForResetAssessment" name="teacherIdForResetAssessment" value="">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeAssessmentResetDiv();">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errordivResetAssessment' style="display: block;"></div>
					<div  id="resetAssessmentDescription"><label><strong><spring:message code="lblResetSp"/><span class="required">*</span></strong></label><br/><br/>
<!--						<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>-->
						<textarea class="form-control" name="msgResetAssessment" id="msgResetAssessment" maxlength="750" rows="4" onkeyup="return chkLenOfTextArea(this,750);" onblur="return chkLenOfTextArea(this,750);"></textarea>
					</div>
				</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" id="btnResetAssessment" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				<button class="btn" onclick="closeAssessmentResetDiv();">Cancel</button>
   			</div>
  		</div>
	</div>
</div>

<div class="modal hide" id="modalResetAssessmentMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeAssessmentResetDiv()">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id="resetAssessmentMsg2Show"></div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"	onclick="closeAssessmentResetDiv()">Ok</button>
			</div>
		</div>
	</div>
</div>
<!-- Assessment Reset modal div end -->

<div  class="modal hide verifyTranscriptDiv"  id="verifyTranscriptDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDivTranscript(1);">x</button>
				<h3><span id="myModalLabelTrans"></span></h3></h3>
			</div>
			 <div class="modal-body">
			 	<div class='divErrorMsg left2' id='errorStatusVerifyTranscript' style="display: block;"></div>
				<div class="control-group">
					<div class="" id="divVerifyTrans">

					</div>
				</div>
			  </div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDivTranscript(1);"><spring:message code="btnClose"/></button>
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<div class="modal" id="myModaTranscript" style="display: none;z-index: 5000" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 id="myModalLabel">TeacherMatch</h3>
				</div>
				<div class="modal-body">
					<div class="control-group" id="messageTranscriptShow"></div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="hideDivTranscript(2);"><spring:message code="btnOk"/></button>
				</div>
			</div>
		</div>
</div>
<input type="hidden" id="jobIdSchool" name="jobIdSchool" value=""/>
<div  class="modal hide"  id="myModalforSchholList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 742px;">
	<div class="modal-content">
		<div class="modal-header" id="schoolHeader">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel"><spring:message code="headSchL"/></h3>
		</div>
		<div class="modal-body" id="schoolListDivNew">
			
		</div> 	
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
	 	</div>
    </div>
  </div>
</div>

<div class="modal hide" id="disconnectResponse" style="display: none;z-index: 5000" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id="disconnectRespMsg"></div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="hideDisconnectResponse();"><spring:message code="btnOk"/></button>
			</div>
		</div>
	</div>
</div>
<div class="hide" id="allPartialProfilesDiv"></div>
<!--   TPL-3973 starts ----->
<div  class="modal hide"  id="candidatepoolrefreshstatusbox"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 400px;">
		<div class="modal-content">		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hide_box();">x</button>
				<h3 id="myModalLabel"><span class="left7"></span>TeacherMatch</h3>
			</div>	
			<div class="modal-body">		
				<div class="control-group">
					<table id="content_del" cellspacing="0" cellpadding="1" style="">
						<tr><td>
							<div id="status"><spring:message code="msgCandidatePoolRefreshStatus" /></div>
						</td></tr>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<input type="button" onclick="hide_box();" value="Close" class="form-class"/>	
			</div>
		</div>
	</div>
</div>
<div class="hide" id="allPartialProfilesDiv"></div>

<jsp:include page="selfserviceapplicantprofilecommon.jsp"></jsp:include>

<!--   TPL-3973 ends ----->
<!--   Add district list in popup starat ----->
<div class="modal hide" id="selectDistrictDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeDistrictlist();">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errorDivDistrictlist' style="display: block;"></div>
					<div id="districtlistdiv">
						<label><strong><spring:message code="msgSelectDistrict"/><span class="required">*</span></strong></label>
						<select name="modalDistrictDetails" id="modalDistrictDetails" class="form-control"></select>
	              		<input type="hidden" name="modeldistrictid" id="modeldistrictid" value="">
					</div>
				</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" id="btnDefaultEPIGroup" onclick="openDistrictlist();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				<button class="btn" onclick="closeDistrictlist();">Cancel</button>
   			</div>
   			<input type="hidden" name="districtlistdivteacherid" id="districtlistdivteacherid" value="">
  		</div>
	</div>
</div>
<!--   Add district list in popup end ----->

<script>
getAdvanceSerachData();
function closeDistrictlist()
{
	$("#selectDistrictDiv").modal("hide");
}

/****TPL-4833  Configuration and Logic for Candidates Not Reviewed starts*******************/
var candidateNotReviewedUtil = {
	teacherId:null,
	jobId:null,
	event:null,
	setNotReviewedFlag:function fNotReviewedFlag(i,e){
		this.setJobId();
		this.teacherId = i;
		this.event = e;
		if(this.jobId==null)this.jobId='';
		CandidateGridAjaxNew.updateCandidateNotReviewed(this.teacherId,this.jobId,this.event,{
			async: false,
			errorHandler:handleError,
			callback:function(data){
				try{
					var tteacherIds  = i.split("##");
					for(var x=0; x<tteacherIds.length; x++){
						if(data == "1")
							if(tteacherIds[x]!="" && tteacherIds[x]!=null)
								$("#cndNotReviewed"+$("#hidCndNotReviewed"+tteacherIds[x]).val()).hide();
						else
							if(tteacherIds[x]!="" && tteacherIds[x]!=null)
								$("#cndNotReviewed"+$("#hidCndNotReviewed"+tteacherIds[x]).val()).show();
					}
				}catch(e){}
			}
		});
	},
	setJobId:function fJobId(){
		if($('#jobId').val()!=null || $('#jobId').val()!='')
			this.jobId = $('#jobId').val();
	}
};
/****TPL-4833  Configuration and Logic for Candidates Not Reviewed ends*******************/
/*******TPL-3682 DA/SA: Advanced Search starts********************************************/
var statusSearchUtil={
	element:null,
	values:[],	
	count:0,
	optionText:function(i){
		if(this.element.id=="candidateStatus"){
			var opt = '';
			switch(i){
				case 0:
					opt = 'Applicant Withdrew Manually';
					break;
				case 1:
					opt = 'Candidate Pool Refresh Withdraw';
					break;
				case 2:
					opt = 'Incomplete Application Feature Withdraw';
					break;
				case 3:
					opt = 'DA/SA Withdraw';
					break;
				case 4:
					opt = 'ALL';
					break;
			}
			return opt;
		}
	},
	selectValue:function(){
		  var options = this.element && this.element.options;
		  var opt;
		  for (var i=0, iLen=options.length; i<iLen; i++) {
			opt = options[i];
			if (opt.selected) {
			  this.values.push(opt.value || opt.text);
			}
		  }
	},
	withdraw:function(e){
		this.element = e;
		this.selectValue();
			if(this.values.length>0 && this.values.indexOf('widrw')>-1){
				$('#withdrawTypeBox').find('option').remove().end();
				for(var i=4;i>=0;i--){
					var option = document.createElement('option');
					option.id='widrw_'+i;
					option.name='widrw_'+i;
					option.value='widrw_'+i;
					option.text=this.optionText(i);
					$('#withdrawTypeBox').append(option);
				}
				$('#withdrawType').show();
			}else{
				$('#withdrawType').hide();
			}
		this.flush();
	},
	flush:function(){
		this.element=null;
		this.values=[];
	}
};
/*******TPL-3682 DA/SA: Advanced Search ends********************************************/
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">