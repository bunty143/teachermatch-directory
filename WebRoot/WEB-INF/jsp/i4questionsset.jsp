<!-- @Author: Gagan 
 * @Discription: view of edit domain Page.
 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type='text/javascript' src="js/jobcategory.js?ver=${resourceMap['js/jobcategory.js']}"></script>
<script type="text/javascript" src="dwr/interface/I4QuestionSetAjax.js?ver=${resourceMap['I4QuestionSetAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/i4/i4questionsset.js?ver=${resourceMap['js/i4/i4questionsset.js']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.Ajax']}"></script>

<style type="text/css">
textarea{
	width:500px;
	height:100px;
   }
   
   .jqte{
   }
   .errMsg {
   color: red;
}
</style>

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        
            
        });
        
function applyScrollOnTbl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
	        $j('#i4QuesTable').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 280,
		        width: 945,
		        minWidth: null,
		        minWidthAuto: false,
		        colratio:[436,146,120,246], // table header width
		        addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 100,
		        wrapper: false
	        });
        });			
}

</script>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">

         	<!-- <div class="subheading" style="font-size: 13px;"><spring:message code="hesdManageI4QuesSet"/></div> -->	
         	<div class="subheading" style="font-size: 13px;"><spring:message code="msgManageVirtualVideo"/> </div>	

         </div>			
		 <div class="pull-right add-employment1">
			<a href="javascript:void(0);" onclick="return addNewQuesSet()"><spring:message code="lnkAddQuesSet"/></a>
		</div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<!-- Search  -->
<div class="row col-sm-12 col-md-12">
		<div class="row">
				<div  id="Searchbox" class="">
					<c:if test="${entityType eq 1}">	         		 
			           		<div class="col-sm-6 col-md-6">
			           			<label id="captionDistrictOrSchool"><spring:message code="lblDistrict"/></label>
			            		<input type="text" id="districtNameFilter" name="districtNameFilter" class="form-control"
			            		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onblur="hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');"	/>
								<input type="hidden" id="districtIdFilter" value=""/>
								<div id='divTxtShowDataFilter'  onmouseover="mouseOverChk('divTxtShowDataFilter','districtNameFilter')"
								 style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	
				            </div>
				     </c:if>
				     <c:if test="${entityType eq 2}">
	     		   		<div  class="col-sm-6 col-md-6">
					   		<label id="captionDistrictOrSchool"><spring:message code="lblDistrict"/></label>
		     				<input type="hidden" id="districtIdFilter" value="${districtId}" />
		     				<input type="text"   class="form-control" id="districtNameFilter"  disabled="disabled" value="${districtName}" />
	     				</div>
	     			</c:if>		             
		     	</div>
		  </div>
		<div class="row">
			<div class="mt10">
				 <div class="col-sm-6 col-md-6">
					<label> <spring:message code="lblQuesSet"/></label>
					<input type="text" class="form-control fl" name="quesSetSearchText" id="quesSetSearchText" >
				 </div>
				 <div class="col-sm-2 col-md-2">
					<label><spring:message code="lblStatus"/></label>
					<select id="i4QuesSetStatus" name="i4QuesSetStatus" class="form-control">
						<option value="0" selected="selected"><spring:message code="optAll"/></option>
						<option value="A"><spring:message code="optAct"/></option>
						<option value="I"><spring:message code="optInActiv"/></option>
					</select>
			 	</div>
			 	<div class="col-sm-2 col-md-2" style="margin-top:26px;">
			 		<button class="btn btn-primary " type="button" onclick="searchi4Ques()"><spring:message code="btnSearch"/> <i class="icon"></i></button>
			 	</div>
			 </div>
		 </div> 
</div>

<div class="TableContent top15">        	
         <div class="table-responsive" id="i4QuesSetGrid"></div>            
</div> 
<div style="display: none; z-index: 5000;" id="loadingDiv">
	<table align="left">
		<tr>
			<td style="padding-top: 270px; padding-left: 450px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px; padding-left: 450px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
		<tr id='paymentMessage'>
			<td style='padding-top: 0px; padding-left: 450px;' id='spnMpro'
				align='center'></td>
		</tr>
	</table>
</div>
	
<div class="hide" id="addQuesSetDiv">
	
		<div class="row col-sm-12 col-md-12">			                         
		 	<div class='errMsg' id="errQuesSetdiv" ></div>
		</div>
		<div class="row">
		<c:if test="${entityType eq 1}">
			<div  class="col-sm-6 col-md-6">
		        		<label><spring:message code="lblDistrict"/></label>
		         		<input type="text" id="districtName" name="districtName"  class="form-control"
			         		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
							onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
							onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
							<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>
						<input type="hidden" id="districtId"/>
			</div>
		</c:if>
		<c:if test="${entityType eq 2}">
	     		   	<div  class="col-sm-6 col-md-6">
				   		<label id="captionDistrictOrSchool"><spring:message code="lblDistrict"/></label>
	     				<input type="hidden" id="districtId" value="${districtId}" />
	     				<input type="text"   class="form-control" id="districtName" disabled="disabled" value="${districtName}" />
	     			</div>
	     </c:if>
		</div>
		<div class="row">
			 <div class="col-sm-6 col-md-6">
				<div >
					<label><strong><spring:message code="lblQuesSet"/><span class="required">*</span></strong></label>
				</div>
				<div >
					<input type="hidden" name="quesSetId" id="quesSetId" >
					<input type="hidden" name="quesSetDate" id="quesSetDate" >
					<span id="i4QuesTxt"></span>
					<input type="text" class="form-control fl" maxlength="100" name="quesSetName" id="quesSetName" >
				</div>
			 </div>
		 </div> 
		<div class="row top5">
			<div class="col-sm-2 col-md-2">
                 <label><strong><spring:message code="lblStatus"/><span class="required">*</span></strong></label>					 
		         <div class="left20">								 
					<label class="radio p0 top5">
						<input type="radio"  value="A" id="quesSetActive"  name="quesSetStatus" checked="checked" > <spring:message code="optAct"/>
					</label>							
				</div>
							
				  <div style="margin-left:80px;margin-top: -30px;">						
			        <label class="radio p0 top5">
			        	<input type="radio" value="I" id="quesSetInactive" name="quesSetStatus" ><spring:message code="lblInActiv"/>
					</label>  						             
	     	      </div>
			</div>
		</div>
		
			
		 <div class="row" id="divManage">
		  	<div class="col-sm-4 col-md-4">
		  			<button onclick="saveQuestion();" class="btn  btn-primary"><strong><spring:message code="btnSave"/><i class="icon"></i></strong></button>
		     	 &nbsp;<a href="javascript:void(0);" onclick="clearQues();"><spring:message code="lnkCancel"/></a>  
		    </div>
		  </div>
</div>
<input type="hidden" id="entityType" value="${entityType}" />
           


<script type="text/javascript">
	displayDomain();
</script>


<!-- @Author: Gagan 
 * @Discription: view of edit domain Page.
 -->