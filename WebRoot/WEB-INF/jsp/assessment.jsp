
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/AssessmentAjax.js?ver=${resourceMap['AssessmentAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/assessment.js?ver=${resourceMap['js/assessment.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />

<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.Ajax']}"></script> 

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
            
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        //width: 840,
         width: 945,
        minWidth: null,
        minWidthAuto: false,
       // colratio:[150,100,85,85,70,60,290], // table header width
       // colratio:[220,100,85,85,60,290],
       //colratio:[320,115,98,85,70,240],
        colratio:[250,100,110,93,82,70,240],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

 </script>
<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headMagInv"/></div>	
         </div>			
		 <div class="pull-right add-employment1">
		<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
				<a href="javascript:void(0);" onclick="return addAssessment()"><spring:message code="lnkAddInv"/></a>
		</c:if>

		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div onkeypress="return chkForEnterGetAssessmentGrid(event);" >
<form class="bs-docs-example">
     <div class="row top10">
     <div class="col-sm-3 col-md-3">             
		 <label><spring:message code="lblInvT"/></label>
	  	 <input type="hidden" name="pageIdNo" id="pageIdNo" value="1" >
	  	 <select class="form-control " id="assessmentType1">
			<option value="0"><spring:message code="optStrAll"/></option>
			<option value="1"><spring:message code="lblB"/></option>
			<option value="4"><spring:message code="lblIPI"/></option>
			<option value="2"><spring:message code="lblJSpe"/></option>
			<option value="3"><spring:message code="lblSmartP"/></option>
			</select>             
     </div>     
     <div class="col-sm-3 col-md-3">     
             <div class="row">
	             <label><spring:message code="lblInvSt"/></label>
	            <select class="form-control " id="assessmentStatus1">
					<option value=""><spring:message code="optStrAll"/></option>
					<option value="A"><spring:message code="optAct"/></option>
					<option value="I"><spring:message code="lblInActiv"/></option>
				</select>			
			</div>
    </div>
     <div class="col-sm-4 col-md-4">     
             <div class="row" style="padding-left:15px;">
	             <label id="captionDistrict"><spring:message code="lblDistrictName"/></label>
			             <span>
			             <input value="" type="text" maxlength="255" id="districtName" name="districtName" class="help-inline form-control"
			            		 onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
											onkeyup="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
											onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');" onkeypress="return inputOnEnter(event);"/>
					      </span>
					      <input type="hidden" id="districtHiddenlId"/>
		        	 <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
			</div>			 
    </div>
	<div class="col-sm-2 col-md-2">	
	<c:if test="${fn:indexOf(roleAccess,'|5|')!=-1}">
	<button class="btn btn-primary fl top25-sm" onclick="searchInventory();" type="button"><spring:message code="btnSearch"/> <i class="icon"></i>
	</button></c:if>
	</div>
          
</div>         
</form>                  
</div>

<div class="TableContent top15">        	
         <div class="table-responsive" id="divMain">          
         </div>            
</div> 

<form>
<div  id="divAssessmentRow" style="display: none;" onkeypress="chkForEnterSaveAssessment(event);">
	<div class="row top5">
	<div class="col-sm-10 col-md-10">		                         
	 			<div class='divErrorMsg' id='errordiv' ></div>
	</div>
	</div>
	<div class="row">
		<div class="col-sm-10 col-md-10"><label><strong><spring:message code="lblInvN"/><span class="required">*</span></strong></label>
		<input type="hidden" name="assessmentId" id="assessmentId" >
		<input type="hidden" name="status" id="status" >
		<input type="text" name="assessmentName" id="assessmentName" maxlength="100" class="form-control" placeholder=""></div>
	</div>

	<div class="row">
		<div class="col-sm-10 col-md-10" id="assDescription"><label><strong><spring:message code="lblInvDesc"/><span class="required">*</span></strong></label>
		<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>	
		<textarea class="form-control" name="assessmentDescription" id="assessmentDescription" maxlength="750" rows="4" onkeyup="return chkLenOfTextArea(this,750);" onblur="return chkLenOfTextArea(this,750);"></textarea>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-10 col-md-10" id="instruction1"><label><strong><spring:message code="lblSplashScreenInstruction1"/><span class="required">*</span></strong></label>
		<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>	
		<textarea class="form-control" name="splashInstruction1" id="splashInstruction1" maxlength="750" rows="4" onkeyup="return chkLenOfTextArea(this,750);" onblur="return chkLenOfTextArea(this,750);"></textarea>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-10 col-md-10" id="instruction2"><label><strong><spring:message code="lblSplashScreenInstruction2"/><span class="required">*</span></strong></label>
		<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>	
		<textarea class="form-control" name="splashInstruction2" id="splashInstruction2" maxlength="750" rows="4" onkeyup="return chkLenOfTextArea(this,750);" onblur="return chkLenOfTextArea(this,750);"></textarea>
		</div>
	</div>
	
	<div class="row top10">
		<div class="col-sm-3">
			<label><strong><spring:message code="lblInvT"/><span class="required">*</span></strong></label>
		</div>
		<div class="col-sm-3 col-md-3" id="new_gp" style="padding-left: 50px;padding-right: 0px;">
			<label><strong><spring:message code="lblAddNewGpName"/><span class="required">*</span></strong></label>
		</div>
		<div class="col-sm-3 col-md-3" id="sel_gp" style="padding-left: 81px;padding-right: 0px;">
			<label><strong><spring:message code="optSelectGp"/><span class="required">*</span></strong></label>
		</div>
		<div class="col-sm-3 col-md-3" id="isDefaultEPIGroupDiv1" style="padding-left: 45px;padding-right: 0px;">
			<label for="isDefaultEPI"><strong><spring:message code="lblDefaultEPI"/>&nbsp;<a href="#" id="iconpophover5" rel="tooltip" data-original-title="<spring:message code="tooltipMsgDefaultEPI"/>"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
		</div>
		<div class="col-sm-3 col-md-3" id="jot1">
			<label><strong><spring:message code="lblJoOdrT"/> </strong></label>
		</div>  
		<div class="col-sm-2 col-md-2" id="def1">
			<label><strong><spring:message code="lblIsDef"/><a  href="#" id="iconpophover2" rel="tooltip" data-original-title="<spring:message code="tooltipMsgDftInvtForDist"/>"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
		</div> 
	</div>
	
	<div class="row ">
	          <div class="col-sm-3" >
	          
	            <select name="assessmentType" class="form-control"  onchange="getJobs(this.value);">
	                     <option value="1"><spring:message code="lblB"/></option>
	                     <option value="4"><spring:message code="lblIPI"/></option>
	                     <option value="2" selected="selected"><spring:message code="lblJSpe"/></option>
	                     <option value="3"><spring:message code="lblSmartP"/></option>	                     
	            </select> 
	            
	            <!-- 
	             <label class="radio p0" style="margin-top: 0px;">
	                <input type="radio"  value="1"  name="assessmentType" onclick="getJobs(1);">
	                Base
	   		     </label>	
		          <div style="margin-left: 60px;margin-top: -30px;">
		          	 <label class="radio p0" style="margin-top: 0px;">
		            <input type="radio" checked="checked" value="2"  name="assessmentType" onclick="getJobs(2);">
		            Job Specific
			  	    </label>
		          </div>  -->
	          </div>
	          
	          <div class="col-sm-3 col-md-3" id="gp1">
	            <div class="col-sm-2" style="padding-left:0">
	                <label class="radio"><input type="radio" value="1" name="rec_group" id="rec1" onchange="radioChange();"/></label>
	            </div>
	            <div class="col-sm-10" style="padding-left:0; padding-right:0;">
	                <input type="text" name="assessmentGroupName" id="assessmentGroupName" style="width:210px" class="form-control" />
	            </div>              	              
	          </div>
	          
	          <div class="col-sm-3 col-md-3" id="gp2" style="margin-left:30px">
	          	<div class="col-sm-2" style="padding-left:0">
	                <label class="radio"><input type="radio" value="2" name="rec_group"  id="rec2" onchange="radioChange();"/></label>
	            </div>
	            <div class="col-sm-10" style="padding-left:0">
	              <select name="assessmentGroupDetails" id="assessmentGroupDetails" class="form-control" onchange="checkDefaultEPIGroup();">
	                   <option value=""><spring:message code="optSelectGp"/></option>
	              </select>
	              <input type="hidden" name="defaultEPIGroupId" id="defaultEPIGroupId" value="">
	            </div>
	          </div>
	          
	          <div class="col-sm-1 col-md-1" id="isDefaultEPIGroupDiv2">
			  	<input class="span0" type="checkbox" name="isDefaultEPI" id="isDefaultEPI" class="form-control" placeholder="" style="margin-top: 12px;">
			  </div>
	          
	          <div class="col-sm-3 col-md-3" id="jot2">
	            <select  class="form-control " name="jobOrderType" id="jobOrderType" onchange="getJobOrderDistricts(),clearJobOrderTypeData();" >
	            <option value='1'><spring:message code="optDisJoOtr"/></option>
	            <option value='2'><spring:message code="optSchJoOtr"/></option>
			    </select>
	          </div>
	          
	          <div class="col-sm-2 col-md-2" id="def2">
	          <input class="span0" type="checkbox" name="isDefault" id="isDefault"  class="form-control" placeholder="">
	          </div>
	</div>
	<!-- ADD PDP Configuration start PDP -->
	   <div class="row top10">
		<div class="col-sm-3">
			<label><strong><spring:message code="msgpdpassociated"/><span class="required">*</span></strong></label>
		</div>
		<div class="col-sm-3 col-md-3" id="new_gp" style="padding-left: 50px;padding-right: 0px;">
			<label><strong><spring:message code="msgpdpselect"/><span class="required">*</span></strong></label>
		</div>
								
		</div>
		<div class="row top10">
	           <!--<div class="col-sm-3" id="PDPassociated">
	            <select  class="form-control " name="PDPassociatedopt" id="PDPassociatedopt" onchange="checkPDPAssociation();">
	            <option value='1' selected="selected"><spring:message code="lblYes"/></option>
	            <option value='2'><spring:message code="lblNo"/></option>
			    </select>
	          </div>
			  -->
			  <div class="col-sm-3" style="margin-right: 33px;">
			   <label class="radio-inline" style="margin-right: 35px;">
				<input id="PDPassociatedoptyes" name="PDPassociatedopt" value="1" type="radio" onclick="checkPDPAssociation();"> Yes
			   </label>
			   <label class="radio-inline">
				<input id="PDPassociatedoptno" name="PDPassociatedopt" value="2" type="radio" onclick="checkPDPAssociation();"> No
			</label>
			</div>
			  <div class="col-sm-3 col-md-3">
				<select class="form-control" name="PDPselect" id="PDPselect">
	                     <option value="0" selected="selected"><spring:message code="optSlt"/></option>
	                    <c:if test="${not empty pdpDetailList}">
	                    <c:forEach var="pdpDetail" items="${pdpDetailList}">
	                    <option value="${pdpDetail.pdpdId}">${pdpDetail.pdpname}</option>
	                    </c:forEach>
	                    </c:if>
	            </select>
	          </div>
		</div>
	<!-- ADD PDP Configuration End PDP -->
	<div class="row top5" id="scoreDiv">
		<div class="col-sm-3" style="margin-right: 33px;">
			<label><strong>No Of Experiment Questions<span class="required">*</span></strong></label><br>
			<input type="text" name="noOfExperimentQuestions" id="noOfExperimentQuestions" onkeypress='return checkForInt(event)' maxlength="2" style="width:210px" class="form-control" />
		</div>
		<div class="col-sm-3" style="margin-right: 33px;">
			<label><strong>Want To Score<span class="required">*</span></strong></label><br>
			<label class="radio-inline" style="margin-right: 35px;">
				<input id="score1" name="score" value="1" type="radio" onchange="showAndHideScoringMethods(1);"> Yes
			</label>
			<label class="radio-inline">
				<input id="score2" name="score" value="2" type="radio" onchange="showAndHideScoringMethods(2);"> No
			</label>
		</div>
		<div class="col-sm-3" id="selectScoreDiv">
			<label><strong>Select Scoring Method<span class="required">*</span></strong></label><br/>
			<select name="scoreLookupMaster" id="scoreLookupMaster" class="form-control">
	        	<option value="">Select Method</option>
	        </select>
		</div>
	</div>
	
	<div class="row " id="jsjo1">
		<div class="col-sm-7 col-md-7" >
		            <label><strong><spring:message code="optDistrict"/></strong></label>
		</div>          
	</div>
	<div class="row " id="jsjo2">
	      <div class="col-sm-7 col-md-7">
	          <select  class="form-control " name="districtMaster" id="districtMaster" onchange="getJobOrderTypeData(this.value);">
			  </select>
	      </div>	       
	</div>

	<div class="row " id="jsjo3">
	 <div class="col-sm-7 col-md-7" id='schoolLbl' style="display: none;">
	            <label><strong><spring:message code="optSchool"/></strong></label>
	 </div> 	               
	</div>
	
	<div class="row " id="jsjo4">	
	        <div class="col-sm-7 col-md-7">
	          <select  class="form-control " name="schoolMaster" style="display: none;" id="schoolMaster" onchange="getJobOrderTypeDataSOJ();">
	          <option value="0"><spring:message code="msgStSchool"/></option>
			</select>
	        </div>
	</div>
	
	<div class="row " id="jsjo5">
	 <div class="col-sm-6 col-md-6" id='jobCategoryLbl' style="display: none;">
	           <label><spring:message code="lblJobCat"/>&nbsp;<a href="#" id="iconpophover4" rel="tooltip" data-original-title="<spring:message code="tooltipMsgJobSpecInvt"/>"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
	  </div> 	               
	</div>	
	
	<div class="row " id="jsjo6">
	        <div class="col-sm-7 col-md-7">
	          <select  class="form-control " name="jobCategoryMaster" id="jobCategoryMaster" style="display: none;" >
	          <option value="0"><spring:message code="optStrJobCat"/></option>
			</select>
	        </div>
	</div>

	<div class="row ">
		<div class="col-sm-7 col-md-7" id="infoData" style="display: none;">
		            <label><strong><spring:message code="lblJoOdrs"/> <a  href="#" id="iconpophover1" rel="tooltip" data-original-title="<spring:message code="tooltipMsgOMJobOrder"/><br/><spring:message code="tooltipMsgOMJobOrder2"/>"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>&nbsp;</strong></label>
		</div>          
	</div>

	<div class="row">
		<div  id='jobs'>
		
		</div>	        
	</div>

	<div class="row ">
	        <div class="col-sm-7 col-md-7">
	          <select multiple="multiple" class="form-control" style="display: none;" name="jobOrders" id="jobOrders" >
			  </select>
			  <div onclick="UncheckJobs();" style="display: none;"><label><strong><spring:message code="lnkUnchJobs"/></strong></label></div>
	        </div>
	</div>

	<div class="row">  
		<div class="col-sm-3 col-md-3">
		<label><strong><spring:message code="lblInvSessTi"/></strong></label>
		<input type="text" name="assessmentSessionTime" onkeypress='return checkForInt(event)' id="assessmentSessionTime" maxlength="6" class="form-control" placeholder="">
		</div>  
		<div style="float: left;margin-top: 30px;margin-left: -15px;">&nbsp;<spring:message code="lbls"/></div>
		<div class="col-sm-3 col-md-3"><label><strong><spring:message code="lblTiAlPerQues"/></strong></label>
		<input type="text" name="questionSessionTime" onkeypress='return checkForInt(event)' id="questionSessionTime" maxlength="3" class="form-control" placeholder="">		
		</div>
		<div style="float: left;margin-top: 30px;margin-left: -15px;">&nbsp;<spring:message code="lbls"/></div>
		<div style="clear: both;"></div>
	</div> 

	<div class="row" style="margin-top: 12px;">  
		<div class="col-sm-3 col-md-3">
			<input class="span0" type="checkbox" name="questionRandomization" id="questionRandomization"  class="form-control" placeholder="">
			<label for="questionRandomization"><strong><spring:message code="lblRandQues"/></strong></label>
		</div>  
		<div style="float: left;margin-top: 30px;margin-left: -15px;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
		<div class="col-sm-3 col-md-3">
			<input class="span0" type="checkbox" name="optionRandomization" id="optionRandomization" class="form-control" placeholder="">
			<label for="optionRandomization"><strong><spring:message code="lblRandOpt"/></strong></label>
		</div>
		<div style="float: left;margin-top: 30px;margin-left: -15px;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
		<div class="col-sm-3 col-md-3" id="def3">
			<input class="span0" type="checkbox" name="isResearchEPI" id="isResearchEPI"  class="form-control" placeholder="">
			<label for="isResearchEPI"><strong><spring:message code="lblIsResEPI"/><a href="#" id="iconpophover3" rel="tooltip" data-original-title="<spring:message code="tooltipMsgEPI"/>"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
		</div>
	</div>
	
    <div class="row top15" style="display: none;" id="divManage">
	  	<div class="col-sm-4 col-md-4">
	  		<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
	     	<button onclick="saveAssessment();" type="button" class="btn btn-large btn-primary"><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
	     	</c:if>
	     	 &nbsp;<a href="javascript:void(0);" onclick="clearAssessment();"><spring:message code="btnClr"/></a> 
	     </div>
	    
	</div>
  
	<div class="idone" id="divDone" style="display: none;">
		<div class="row top5">
			<div class="col-sm-4 col-md-4">
			<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}"><a href="javascript:void(0);" onclick="saveAssessment();"><spring:message code="lnkImD"/></a></c:if>
			 &nbsp;<a href="javascript:void(0);" onclick="clearAssessment();"><spring:message code="btnClr"/></a> 
			</div>
		</div>	
	</div>
</div>
</form>
              
<div style="display:none;" id="loadingDiv">
	<table  align="center">
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div><br><br>                

<!-- Default EPI Group modal div start -->
<div class="modal hide" id="modalDefaultEPIGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeDefaultEPIGroup();">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errorDivDefaultEPIGroup' style="display: block;"></div>
					<div id="defaultEPIGroupDiv">
						<label><strong><spring:message code="optSelectGp"/><span class="required">*</span></strong></label>
						<select name="modalAssessmentGroupDetails" id="modalAssessmentGroupDetails" class="form-control"></select>
	              		<input type="hidden" name="modalDefaultEPIGroupId" id="modalDefaultEPIGroupId" value="">
					</div>
				</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" id="btnDefaultEPIGroup" onclick="updateDefaultEPIGroup();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				<button class="btn" onclick="closeDefaultEPIGroup();">Cancel</button>
   			</div>
  		</div>
	</div>
</div>
<!-- Default EPI Group modal div end -->

<script type="text/javascript">
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
$('#iconpophover3').tooltip();
$('#iconpophover4').tooltip();
$('#iconpophover5').tooltip();
getAssessmentGrid();
</script>
