<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<style>
	p
	{
		text-align: justify;
		padding-right: 5px;
		/*text-indent: 20px;*/
	}
	.spnIndent
	{
		padding-left: 20px;
	}
	.spnIndent40
	{
		padding-left: 40px;
	}
	ul
	{
	margin-right: 10px;
	}
</style>
			

		
<div class="container">
			<div class='divErrorMsg' id='errordiv' style="display: block;">				
			</div>
		</div>
	

		<div class="container">
			 <div class="row centerline">
					<img src="images/affidavit.png" width="47" height="48" alt="">
					<b class="subheading" style="font-size: 13px;">
					<spring:message code="lblPoliciesProceConsumption"/>
				    </b>
		     </div>
		     
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br><span>&nbsp;&nbsp;&nbsp;<spring:message code="lblOurInnovativeSolution"/></span>
		    </br></br><div style="margin-left:27px;"><spring:message code="lblAssessmentDevelopment1"/></div>
		   
		    </div>		
		</div>
        <div class="container top15">
             <div class="row" align="justify">
             <div class="termsheight right10">
				<p style="text-indent: 0px;padding:0px 10px 0px 40px"><spring:message code="msgTeacherMatchAssessmentsProvideHiringAuthorities"/>
				</p>
				<p style="text-indent: 0px;padding:0px 10px 0px 40px"><spring:message code="msgPoliciesAndProceduresDemo1"/>
				</p>
				<p style="text-indent: 0px;padding:0px 10px 0px 40px">
				<spring:message code="msgPoliciesAndProceduresDemo2"/> 
				</p>
				<p style="text-indent: 0px;padding:0px 10px 0px 40px">
					<spring:message code="msgPoliciesAndProceduresDemo3"/>
				</p>
				<p style="text-indent: 0px;padding:0px 10px 0px 40px">
					<b><spring:message code="msgPoliciesAndProceduresDemo4"/></b></br>
					<b><spring:message code="msgPoliciesAndProceduresDemo5"/></b></br>
					<b><spring:message code="msgPoliciesAndProceduresDemo6"/></b></br>
					<b><spring:message code="msgPoliciesAndProceduresDemo7"/></b></br>
					<b><spring:message code="msgPoliciesAndProceduresDemo8"/></b></br>
					<b><spring:message code="msgPoliciesAndProceduresDemo9"/></b></br>
					<b><spring:message code="msgPoliciesAndProceduresDemo10"/></b></br>
					<b><spring:message code="msgPoliciesAndProceduresDemo11"/></b>
				</p>
			</div>
		</div>				  
	</div>
	
	<div class="container">
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span NAME="tr"><spring:message code="msgPoliciesAndProceduresDemo12"/></span>
		    </div>	
		    
		    <ul style="margin-left:-18px">
			    <li><spring:message code="msgPoliciesAndProceduresDemo13"/></li>
			    <li><spring:message code="msgPoliciesAndProceduresDemo14"/></li>
			    <li><spring:message code="msgPoliciesAndProceduresDemo15"/>.</li>
			    <li><spring:message code="msgPoliciesAndProceduresDemo16"/>.</li>
			    <li><spring:message code="msgPoliciesAndProceduresDemo17"/>.</li>
		    
		    </ul>
		    	
		    <p style="text-indent: 0px;"><spring:message code="msgPoliciesAndProceduresDemo18"/>
		    </p>
		    <p><a href="mailto:clientservices@teachermatch.net">clientservices@teachermatch.net</a> <i><spring:message code="msgPoliciesAndProceduresDemo19"/></i></p>
		</div>
		
		<div class="container">
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span id="psychometric"><spring:message code="msgPoliciesAndProceduresDemo20"/></span>
		    </div>	
		    <p style="text-indent: 0px;"><spring:message code="msgPoliciesAndProceduresDemo21"/>
		    </p>
		    <ul style="margin-left:-18px">
		    <li><spring:message code="msgPoliciesAndProceduresDemo22"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo23"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo24"/></li>
		    </ul>
		    
		    <p style="text-indent: 0px;">
		    	<spring:message code="msgPoliciesAndProceduresDemo25"/>
		    </p>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span><spring:message code="msgPoliciesAndProceduresDemo26"/></span>
		    </div>
		    <p style="text-indent: 0px;">
		    	<spring:message code="msgPoliciesAndProceduresDemo27"/>
		    </p>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span><spring:message code="msgPoliciesAndProceduresDemo28"/></span>
		    </div>
		    <p style="text-indent: 0px;">
		    	<spring:message code="msgPoliciesAndProceduresDemo29"/> 
		    	</p>
		    	
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span><spring:message code="msgPoliciesAndProceduresDemo30"/></span>
		    </div>
		    <p style="text-indent: 0px;"><spring:message code="msgPoliciesAndProceduresDemo31"/> 
		    </p>
		    <ul style="margin-left:-18px">
		    <li><spring:message code="msgPoliciesAndProceduresDemo32"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo33"/></li>
		    </ul>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span><spring:message code="msgPoliciesAndProceduresDemo34"/></span>
		    </div>
		    <p style="text-indent: 0px;"><spring:message code="msgPoliciesAndProceduresDemo35"/> 
		    </p>
		    <ul style="margin-left:-18px">
		    <li><spring:message code="msgPoliciesAndProceduresDemo36"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo37"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo38"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo39"/></li>
		    </ul>
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo40"/>
		    </p>
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo41"/>
		    </p>
		    <ul style="margin-left:-18px">
		    <li><spring:message code="msgPoliciesAndProceduresDemo42"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo43"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo44"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo45"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo46"/></li>		    
		    </ul>
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo47"/>
		    </p>
		    <ul style="margin-left:-18px">
		    <li><spring:message code="msgPoliciesAndProceduresDemo48"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo49"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo50"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo51"/></li>
		    </ul>
		    <p style="text-indent: 0px;">
		    	<spring:message code="msgPoliciesAndProceduresDemo52"/>
		    </p>
		    <p style="text-indent: 0px;">
		    	<spring:message code="msgPoliciesAndProceduresDemo53"/>
		    </p>
		    <ul style="margin-left:-18px">
		    <li><spring:message code="msgPoliciesAndProceduresDemo54"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo55"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo56"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo57"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo58"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo59"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo60"/></li>
		    </ul>
		    
		    <p style="text-indent: 0px;">
		    	<spring:message code="msgPoliciesAndProceduresDemo61"/>
		    </p>
		    <ul style="margin-left:-18px">
	    	<li><spring:message code="msgPoliciesAndProceduresDemo62"/></li>
			<li><spring:message code="msgPoliciesAndProceduresDemo63"/></li>
			<li><spring:message code="msgPoliciesAndProceduresDemo64"/></li>
			<li><spring:message code="msgPoliciesAndProceduresDemo65"/></li>
			<li><spring:message code="msgPoliciesAndProceduresDemo66"/> </li>
			</ul>
			<p style="text-indent: 0px;">
			<spring:message code="msgPoliciesAndProceduresDemo67"/>
			</p>
			<p style="text-indent: 0px;">
			<spring:message code="msgPoliciesAndProceduresDemo68"/>
			</p>
			<p style="text-indent: 0px;">
			<spring:message code="msgPoliciesAndProceduresDemo69"/>
			</p>
			<p style="text-indent: 0px;">
			<spring:message code="msgPoliciesAndProceduresDemo70"/>
			</p>
			
			<div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span><spring:message code="msgPoliciesAndProceduresDemo71"/></span>
		    </div>
		    
		    <p style="text-indent: 0px;"><spring:message code="msgPoliciesAndProceduresDemo72"/>
		    </p>
		    <ul style="margin-left:-18px">
		    <li><spring:message code="msgPoliciesAndProceduresDemo73"/></li>
			<li><spring:message code="msgPoliciesAndProceduresDemo74"/></li>
			<li><spring:message code="msgPoliciesAndProceduresDemo75"/></li>
			<li><spring:message code="msgPoliciesAndProceduresDemo76"/> </li>
			<li><spring:message code="msgPoliciesAndProceduresDemo77"/></li>
			<li><spring:message code="msgPoliciesAndProceduresDemo78"/></li>
		    </ul> 
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span><spring:message code="msgPoliciesAndProceduresDemo79"/></span>
		    </div>
		    
		    <p style="text-indent: 0px;"><spring:message code="msgPoliciesAndProceduresDemo80"/>
		    </p>
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo81"/>
		    </p>
		    <ul style="margin-left:-18px">
		    <li><spring:message code="msgPoliciesAndProceduresDemo83"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo84"/></li>
		    </ul>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span><spring:message code="msgPoliciesAndProceduresDemo85"/></span>
		    </div>
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo86"/>
		    </p>
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo87"/> 
		    </p>
		    
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo88"/>
		    </p>
		    
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo89"/>
		    </p>
		    <ul style="margin-left:-18px"><li><spring:message code="msgPoliciesAndProceduresDemo90"/></li></ul>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span><spring:message code="msgPoliciesAndProceduresDemo91"/></span>
		    </div> 
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo92"/>
		    </p>
		    <ul style="margin-left:-18px">
		    <li><spring:message code="msgPoliciesAndProceduresDemo93"/></li>
			<li><spring:message code="msgPoliciesAndProceduresDemo94"/></li>
		    </ul>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span ><spring:message code="msgPoliciesAndProceduresDemo95"/></span>
		    </div>
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo96"/> 
		    </p>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span ><spring:message code="msgPoliciesAndProceduresDemo97"/></span>
		    </div>
		    
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo98"/>
		    </p>
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo99"/>
		    </p>
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo100"/>
		    </p>
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo101"/>

		    </p>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span id="assessment"><spring:message code="msgPoliciesAndProceduresDemo102"/></span>
		    </div>
		    
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo103"/> 
		    </p>
		    <ul style="margin-left:-18px">
		    <li><spring:message code="msgPoliciesAndProceduresDemo104"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo105"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo106"/></li>
		    <li><spring:message code="msgPoliciesAndProceduresDemo107"/></li>
		    </ul>
		    
		    <p style="text-indent: 0px;">
		    <spring:message code="msgPoliciesAndProceduresDemo108"/>  
		    </p>
		    
		</div>