<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/SendMailAjax.js?ver=${resourceMap['SendMailAjax.ajax']}"></script>
<script type='text/javascript' src="js/sendmail.js?ver=${resourceMap['js/sendMail.js']}"></script>	
  
  
  <script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>


<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css"
	href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
  
    <div  id="displayError" style="display:none;" class="alert alert-danger" >  </div>
   <div  id="displayMsg" style="display:none;" class="alert alert-success" >  </div>   
   
   <div class="row">
		<div class="col-sm-7 col-md-7">
		        <label><spring:message code="lblEmailAddress"/><span class="required">*</span></label>
		        <input type="text" id="txtEmail" name="email"   class="form-control" placeholder="Enter Email Address" >
		      </div>
      	</div>
   <div class="row">
		<div class="col-sm-7 col-md-7">
		        <label><spring:message code="lblSub"/><span class="required">*</span></label>
		        <input type="text" id="txtSubject" name="subject"   class="form-control" placeholder="Enter Subject" >
		      </div>
      	</div>	
    <div class="row">
		<div class="col-sm-7 col-md-7">
		        <label><spring:message code="lblLocti"/><span class="required">*</span></label>
		        <input type="text" id="txtLocation" name="location"    class="form-control" placeholder="Enter Location" >
		      </div>
      	</div>
      	 <div class="row">
		<div class="col-sm-7 col-md-7">
		        <label><spring:message code="lblDecr"/><span class="required">*</span></label>
		        <input type="text" id="txtDescription" name="description"    class="form-control" placeholder="Enter Description" >
		      </div>
      	</div>
      	<div class="row">
		<div class="col-sm-4 col-md-4">
		        <label><spring:message code="lblStartDate"/><span class="required">*</span></label>
		        <input type="text" id="sfromDate" name="sfromDate"     class="form-control" placeholder="Enter Start Date" >
		      </div>
		      <div class="col-sm-3 col-md-3">
		        <label><spring:message code="lblStartTime"/><span class="required">*</span></label>
		        <select id="sfromtime" class="form-control">
		        	<option ><spring:message code="lblStartTime"/></option>
					<option ><spring:message code="opt1"/> <spring:message code="optAM"/></option>
					<option ><spring:message code="opt2"/> <spring:message code="optAM"/></option>
					<option ><spring:message code="opt3"/> <spring:message code="optAM"/></option>
					<option ><spring:message code="opt4"/> <spring:message code="optAM"/></option>
					<option ><spring:message code="opt5"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt6"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt7"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt8"/> <spring:message code="optAM"/></option>	
					<option  ><spring:message code="opt9"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt10"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt11"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt12"/> <spring:message code="optAM"/></option>	
					<option  ><spring:message code="opt13"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt14"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt15"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt16"/> <spring:message code="optAM"/></option>	
					<option  ><spring:message code="opt17"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt18"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt19"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt20"/> <spring:message code="optAM"/></option>	
					<option  ><spring:message code="opt21"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt22"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt23"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt24"/> <spring:message code="optAM"/></option>
						
					<option ><spring:message code="opt1"/> <spring:message code="optPM"/></option>
					<option ><spring:message code="opt2"/> <spring:message code="optPM"/></option>
					<option ><spring:message code="opt3"/> <spring:message code="optPM"/></option>
					<option ><spring:message code="opt4"/> <spring:message code="optPM"/></option>
					<option ><spring:message code="opt5"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt6"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt7"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt8"/> <spring:message code="optPM"/></option>	
					<option  ><spring:message code="opt9"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt10"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt11"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt12"/> <spring:message code="optPM"/></option>	
					<option  ><spring:message code="opt13"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt14"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt15"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt16"/> <spring:message code="optPM"/></option>	
					<option  ><spring:message code="opt17"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt18"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt19"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt20"/> <spring:message code="optPM"/></option>	
					<option  ><spring:message code="opt21"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt22"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt23"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt24"/> <spring:message code="optPM"/></option>	
					
											
				</select>		        
		      </div>
      	</div>
      	<div class="row">
		<div class="col-sm-4 col-md-4">
		        <label><spring:message code="lblEndDate"/><span class="required">*</span></label>
		        <input type="text" id="stoDate" name="stoDate"    class="form-control" placeholder="Enter End Date" >
		      </div>
		      <div class="col-sm-3 col-md-3">
		        <label><spring:message code="lblEndTime"/><span class="required">*</span></label>
		        <select id="sToTime" class="form-control">
		        	<option ><spring:message code="lblEndTime"/></option>					
					<option ><spring:message code="opt1"/> <spring:message code="optAM"/></option>
					<option ><spring:message code="opt2"/> <spring:message code="optAM"/></option>
					<option ><spring:message code="opt3"/> <spring:message code="optAM"/></option>
					<option ><spring:message code="opt4"/> <spring:message code="optAM"/></option>
					<option ><spring:message code="opt5"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt6"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt7"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt8"/> <spring:message code="optAM"/></option>	
					<option  ><spring:message code="opt9"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt10"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt11"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt12"/> <spring:message code="optAM"/></option>	
					<option  ><spring:message code="opt13"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt14"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt15"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt16"/> <spring:message code="optAM"/></option>	
					<option  ><spring:message code="opt17"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt18"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt19"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt20"/> <spring:message code="optAM"/></option>	
					<option  ><spring:message code="opt21"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt22"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt23"/> <spring:message code="optAM"/></option>
					<option  ><spring:message code="opt24"/> <spring:message code="optAM"/></option>
						
					<option ><spring:message code="opt1"/> <spring:message code="optPM"/></option>
					<option ><spring:message code="opt2"/> <spring:message code="optPM"/></option>
					<option ><spring:message code="opt3"/> <spring:message code="optPM"/></option>
					<option ><spring:message code="opt4"/> <spring:message code="optPM"/></option>
					<option ><spring:message code="opt5"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt6"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt7"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt8"/> <spring:message code="optPM"/></option>	
					<option  ><spring:message code="opt9"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt10"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt11"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt12"/> <spring:message code="optPM"/></option>	
					<option  ><spring:message code="opt13"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt14"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt15"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt16"/> <spring:message code="optPM"/></option>	
					<option  ><spring:message code="opt17"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt18"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt19"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt20"/> <spring:message code="optPM"/></option>	
					<option  ><spring:message code="opt21"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt22"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt23"/> <spring:message code="optPM"/></option>
					<option  ><spring:message code="opt24"/> <spring:message code="optPM"/></option>	
															
				</select>		        
		      </div>
      	</div><br/>				
   <table >
    <tr>   
     <td><input type="button" value="<spring:message code="btnSub"/>" name="submit" id="submit" onclick="sendMail();" class="btn btn-primary" >
     <input type="button" value="<spring:message code="btnClr"/>" name="cancel" id="cancel" onclick="cancelAction();" class="btn btn-primary" >
     <input type="hidden" id="id" placeholder="id">
     </td>
    </tr>
   </table>  

 <script type="text/javascript">

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      //cal.manageFields("sfromDate", "sfromDate", "%m-%d-%Y");
      cal.manageFields("sfromDate", "sfromDate", "%m-%d-%Y");
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("stoDate", "stoDate", "%m-%d-%Y");
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("endfromDate", "endfromDate", "%m-%d-%Y");
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("endtoDate", "endtoDate", "%m-%d-%Y");
       
       var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("appsfromDate", "appsfromDate", "%m-%d-%Y");
       var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("appstoDate", "appstoDate", "%m-%d-%Y");
      
       var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("hiredfromDate", "hiredfromDate", "%m-%d-%Y");
      
       var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("hiredtoDate", "hiredtoDate", "%m-%d-%Y");
      
  	
     </script>	