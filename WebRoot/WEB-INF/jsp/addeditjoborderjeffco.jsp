<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface//ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/JobCategoryAjax.js?ver=${resourceMap['JobCategoryAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/addeditjoborder.js?ver=${resourceMap['js/addeditjoborder.js']}"></script>
<script type='text/javascript' src="js/districtRequisationNumberAutoComplete.js?ver=${resourceMap['js/districtRequisationNumberAutoComplete.js']}"></script>
<script type='text/javascript' src="js/certtypeautocomplete.js?ver=${resourceMap['js/certtypeautocomplete.js']}"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<link rel="stylesheet" href="css/dialogbox.css" />
<script type="text/javascript" src="dwr/interface/PFCertifications.js?ver=${resourceMap['PFCertifications.ajax']}"></script>
<script type='text/javascript' src="js/teacher/pfCertifications.js?ver=${resourceMap['js/teacher/pfCertifications.js']}"></script>
<script type='text/javascript' src="js/teacher/pfAutoCompCertificate.js?ver=${resourceMap['js/teacher/pfAutoCompCertificate.js']}"></script>
<script type="text/javascript" src="dwr/interface/I4QuestionSetAjax.js?ver=${resourceMap['I4QuestionSetAjax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAssessmentAjax.js?ver=${resourceMap['DistrictAssessmentAjax']}"></script>
<style>
.table th, .table td {
    padding: 8px;
    line-height: 20px;
    text-align: left;
    vertical-align: top;  
}
</style>

<div id="displayDiv"></div>
<script>
$(document).ready(function(){
     var locationurl=window.location.href;
     var districtId= $("#districtOrSchooHiddenlId").val();
	 if(locationurl.indexOf("addeditjoborder") > -1 && $('#entityType').val()==3 && districtId==806900)
	 {
			  // $('#loadingDiv').show();
			 	 $("#showAndHideSchoolDiv").show();
			 	 $("#addNoSchoolDiv").hide();
			 	 $("#schoolTable .fa-trash-o").hide();
		
				addSchool();
				$("#hideForSchool .allSchoolGradeDistrict").prop("checked",true);
				$("#hideForSchool #allSchoolGradeDistrict").prop("disabled",true);
				$("#hideForSchool .allSchoolGradeDistrict").prop("disabled",false);
				
				$("#schoolName").val('<c:out value="${userMaster.schoolId.schoolName}"/>');
				$("#schoolId").val('<c:out value="${userMaster.schoolId.schoolId}"/>');
				$("#noOfSchoolExpHires").val(1);
				validateAddSchool();
		
		
	   }else if(locationurl.indexOf("editjoborder") > -1 && $('#entityType').val()==3 && districtId==806900){
	   
	   		//$("#showAndHideSchoolDiv").hide();
	   		$("#hideForSchool #allSchoolGradeDistrict").prop("disabled",true);
			$("#hideForSchool .allSchoolGradeDistrict").prop("disabled",false);
	   		//displaySchool();
	   }else if(districtId==806900){
	   		//$("#showAndHideSchoolDiv").hide();
	   		$("#hideForSchool #allSchoolGradeDistrict").prop("disabled",false);
			$("#hideForSchool .allSchoolGradeDistrict").prop("disabled",false);
	   		//displaySchool();
	   }
	});
	function addCertification()
	{
		$("#addCertificationDiv").fadeIn();
		
		//alert("districtOrSchooHiddenlId"+$('#districtOrSchooHiddenlId').val()+" Entity Type : "+$('#entityType').val()+" districtORSchoolName : "+$('#districtORSchoolName').val())
		if($('#entityType').val()==1 && $('#districtORSchoolName').val()=="")
		{
			$("#stateMaster").val("");
		}
		else
		{
			/* ===== GAgan : For Da & Sa State ======*/
			//alert(" Else Block stateId : "+$('#stateId').val()+"  entityType :"+$('#entityType').val());
			if( ($('#entityType').val()==2 || $('#entityType').val()==3) || ($('#entityType').val()==1 && $('#districtORSchoolName').val()!="" && $('#jobId').val()!="") )
			{
				var optstate = document.getElementById('stateMaster').options;
				for(var i = 0, j = optstate.length; i < j; i++)
				{
					  if($('#stateId').val()==optstate[i].value)
					  {
						  optstate[i].selected	=	true;
						  break;
					  }
				}
			}
			else
			{
				if($('#entityType').val()==1 && $('#districtORSchoolName').val()!="" && $('#jobId').val()!="")
				{
					//alert("========= Edit Batch Job Order Mode For Admin  =========");
					getDistrictObjByDistrictId();
				}
			}
		}	
		$("#stateMaster").focus();
		
	}
	
	function cancelCertification()
	{
		$('#errordiv').empty();
		$("#addCertificationDiv").hide();
		document.getElementById("certType").value="";
		document.getElementById("certificateTypeMaster").value="";
	}
	function checkValue(val)
	{  	
	
		try{
			document.getElementById("reqNumberDiv").style.display='inline';
		}catch(err){}
		
		try{
			document.getElementById("reqNumberForSchoolDiv").style.display='inline';
		}catch(err){}
		
		try{
			if(document.getElementById("isReqNoRequired").value=="true"){
				//document.getElementById("reqNumberDiv").style.display='inline';
				//document.getElementById("reqNumberForSchoolDiv").style.display='inline';
			}else{
				//document.getElementById("reqNumberDiv").style.display='none';
				//document.getElementById("reqNumberForSchoolDiv").style.display='none';
			}
		}catch(err){}
	
	
		document.getElementById("allSchoolGradeDistrictVal").value=val;
		var isReqNoFlagByDistrict =	$("#isReqNoFlagByDistrict").val();
		
		if(val==1){
			$("#addNoSchoolDiv").fadeIn();
			if(isReqNoFlagByDistrict==1)
				$("#addReqLink").fadeIn();
				
			$("#addSchoolDiv").hide();
			$("#addSchoolLink").hide();
			$("#showAndHideSchoolDiv").hide();
			$("#reqNumberSchoolDiv").hide();
			//$("#districtReqNumberSchoolDiv").hide();
			//displayRequisationNumber();
		}
		if(val==2){
			$("#addSchoolLink").fadeIn();
			$("#showAndHideSchoolDiv").fadeIn();
			$("#addNoSchoolDiv").hide();
			
			if(isReqNoFlagByDistrict==1)
				$("#addReqLink").hide();
				
			//$("#districtReqNumberSchoolDiv").show();
			document.getElementById("noOfExpHires").value="";
			//$("#reqNumberSchoolDiv").fadeIn();
			//displayRequisationNumber();
			ShowSchool();
		}
	}
	function checkMSG(flag){
		if(flag==1){
			document.getElementById("textDSMessage2").innerHTML="You have already selected the option \"Administrators from Selected School(s)\". If you will select the option \"Only District Administrator(s)\", you will lose the information from \"Administrators from Selected School(s)\".";
	    	$("#textDSChangeMessageModal2").modal("show");
	    	deleteReqByJobId();	
		}else if(flag==2){
			document.getElementById("textDSMessage").innerHTML="You have already selected the option \"Only District Administrator(s)\". If you will select the option \"Administrators from Selected School(s)\" "+"you will lose the information from \"Only District Administrator(s)\".";
		    $("#textDSChangeMessageModal").modal("show");
	       	deleteReqByJobId();	
	       	
		}
		sortSelectList();
	}
	
	function exitUrlMessage(val)
	{  
		document.getElementById("exitURLMessageVal").value=val;
	}
	function isJobAssessment(val)
	{  
		document.getElementById("isJobAssessmentVal").value=val;
		if(val==2){
			$("#isJobAssessmentDiv").hide();
		}else{
			$("#isJobAssessmentDiv").fadeIn();
			$("#attachJobAssessmentDiv").show();
			
			// Script Implemented By Hanzala Dated 14 Nov 2014
			//$('#offerJSIRadioMaindatory').prop("checked",true);
			
			
			
		}
	}
	function attachJobAssessment(val)
	{  
		//alert(" val :  "+val);
		document.getElementById("attachJobAssessmentVal").value=val;
		if(val==2){
			$("#attachJobAssessmentDiv").fadeIn();
		}else{
			$("#attachJobAssessmentDiv").hide();
		}
	}
	var isCloneJob = false;
</script>
<style>
	.hide
	{
		display: none;
	}
	.inline
	{
		display: inline;
	}
	.nowrap
	{
		white-space: nowrap
	}
	
</style>
<script type="text/javascript" src="swf/ZeroClipboard.js"></script>
	<script language="JavaScript">
		var clip = null;
		function init_swf() {
		function $(id) { return document.getElementById(id); }
			clip = new ZeroClipboard.Client();
			clip.setHandCursor( true );
			
			clip.addEventListener('load', function (client) {
				debugstr("Flash movie loaded and ready.");
			});
			
			clip.addEventListener('mouseOver', function (client) {
				// update the text on mouse over
				clip.setText($('copy_text').value);
			});
			
			clip.addEventListener('complete', function (client, text) {
				debugstr("Copied text to clipboard: " + text );
			});
			
			clip.glue( 'd_clip_button', 'd_clip_container' );
		}
	</script>
	<input type="hidden" id="currentUrl" value="${currentUrl}">
	<input type="hidden" id="isJobApplied" value="${isJobApplied}">
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/add-editdistrictjob-order.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">
         	Add/Edit <span id="titleDistrictOrSchool">District</span> Job Order
         	</div>	
         </div>
          		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
 </div>

	
<div class="row">
<div class="col-sm-12 col-md-12" onkeypress="return chkForEnterAddEditJobOrder(event);" ><br>
<div class="">
			<div class="control-group">			                         
			 	<div class='divErrorMsg' id='errordiv' ></div>
			</div>
			
			<div class="control-group">	     			                         
			 	<div class='divErrorMsg' id='jobDescriptionErrordiv' ></div>
			</div>
</div>	</div>	</div>	
			<div class="row">
            <div class="col-sm-8 col-md-8">
               <label>District Name<span class="required">*</span></label>  <br/>          
                <c:if test="${SchoolDistrictNameFlag==0}">	             
	             <span>
	             <input type="text" value="${DistrictOrSchoolName}" maxlength="100" id="districtORSchoolName" name="districtORSchoolName" class="help-inline form-control"
	            		 onfocus="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId',''); getSubjectByDistrict(); getStatusListForVII();"
									onkeyup="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId',''); getSubjectByDistrict(); getStatusListForVII();"
									onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData'); getSubjectByDistrict();setDistrictReqFlag();getZoneName();lockGeozone(2); getStatusListForVII();chkJobSubCategory();"
									onchange="removeJobDescriptionFile(2); "/>
			      </span>
			    <c:if test="${JobOrderType==3}">
			      <input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
              	</c:if>
              	 <c:if test="${JobOrderType==2}">
			        <input type="hidden" id="districtOrSchooHiddenlId" value="${jobOrder.districtMaster.districtId}"/>
              	</c:if>
                   <c:if test="${addJobFlag && entityType==3 && JobOrderType==2}">
			        <input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
		           </c:if>
             </c:if>
 <!-- mukesh -->   		
			<c:if test="${SchoolDistrictNameFlag!=0}">
             	${DistrictOrSchoolName}	
             	<c:if test="${jobOrder.jobId eq null}">
	             	<script type="text/javascript">
	             	getZoneNameDA(${DistrictOrSchoolId });
	             	</script>
             	</c:if>
             	
             	<input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
             	<input type="hidden" id="districtORSchoolName" name="districtORSchoolName"/>
             	  
             </c:if>
       		  <div id='divTxtShowData' style=' display:none;position:absolute;' onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" class='result' ></div>	
       		  <c:if test="${JobOrderType==3}">
			  <label>School Name<span class="required">*</span></label>
 				<input type="text" value="${SchoolName}" id="schoolName" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
									onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
									onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');"	/>
			
			 	 <input type="hidden" value="${SchoolId}" id="schoolId"/>
				 <div id='divTxtShowData2' style=' display:none;position:absolute;' onmouseover="mouseOverChk('divTxtShowData2','schoolName')" class='result' ></div>
			 </c:if>
         </div>
   <!-- mukesh -->
   
   
   		<c:if test="${jobOrder.districtMaster.isZoneRequired eq 1}">
   				<div class="col-sm-3 col-md-3" id="displayZone" >
		         	<label id="zoneLable">Zone</label>
					<select id="zone" class="form-control">
					<option selected value='0'>Select Zone</option>
						${jobzones}
					</select>
	     		</div>	
   		</c:if> 
   		
		<div class="col-sm-3 col-md-3 hide" id="displayZone" >
        	<label id="zoneLable">Zone</label>
			<select id="zone" class="form-control">
			<option selected value='0'>Select Zone</option>
				${jobzones}
		 	</select>
   		</div>	
   		      
         
 		</div>
		<c:if test="${entityType==1}">
			<input type="hidden" id=isReqNoRequired name="isReqNoRequired" value="${jobOrder.districtMaster.isReqNoRequired}" />
		</c:if>
		<c:if test="${entityType==2}">
			<input type="hidden" id=isReqNoRequired name="isReqNoRequired" value="${userMaster.districtId.isReqNoRequired}" />
		</c:if>
		<c:if test="${entityType==3}">
			<input type="hidden" id=fillSchoolId name="fillSchoolId" value="${userMaster.schoolId.schoolId}" />
			<input type="hidden" id=fillSchoolName name="fillSchoolName" value="${userMaster.schoolId.schoolName}" />
		</c:if>
		<c:if test="${addJobFlag && entityType==3}">			
			<input type="hidden" id=isReqNoRequired name="isReqNoRequired" value="${userMaster.districtId.isReqNoRequired}" />
		</c:if>
		<c:if test="${jobOrder.districtMaster.isZoneRequired eq 1}">
			<input type="hidden" id="isZoneAvailable" name="isZoneAvailable" value="1"/>	
		</c:if>
		
		
		<c:choose>
	<c:when test="${fn:contains(currentUrl, 'clonejoborder')}">
	<input type="hidden" id="jobId" name="jobId" value="" />
	<input type="hidden" id="clonejobId" name="clonejobId" value="${jobOrder.jobId}" />
	<script>
	isCloneJob = true;
	</script>
    </c:when>
    <c:when test="${JobId!=0}">
    <input type="hidden" id="clonejobId" name="jobId" value="" />
	<input type="hidden" id="jobId" name="jobId" value="${jobOrder.jobId}" />	
    </c:when>
    <c:otherwise>
    <input type="hidden" id="jobId" name="jobId" value="" />
    <input type="hidden" id="clonejobId" name="clonejobId" value="" />    
    </c:otherwise>    
</c:choose>

		<input type="hidden" id="isZoneAvailable" name="isZoneAvailable" value=""/>
		<input type="hidden" id="windowFunc" name="windowFunc" value="${windowFunc}" />		
		<input type="hidden" id="jobCatId" name="jobCatId" value="${jobOrder.jobCategoryMaster.jobCategoryId}" />
		<input type="hidden" id="entityType" name="entityType" value="${entityType}">
		<input type="hidden" id="jobOrderType" name="jobOrderType" value="${JobOrderType}">
		<input type="hidden" id="distHiddenId" name="distHiddenId" value="0" />
		<input type="hidden" id="districtHiddenId" name="districtHiddenId" value="${districtHiddenId}" />
		<input type="hidden" id="writePrivilegeValue" name="writePrivilegeValue" value="" />
		<input type="hidden" id="stateId" name="stateId" value="${stateId}">
	
	<div class="row">
	 <c:if test="${userMaster.districtId.districtId==804800}">
		    <div class="col-sm-3 col-md-3">
		        <label>Position Number</label>
						<c:choose>
      <c:when test="${fn:contains(currentUrl, 'clonejoborder')}">
     <%-- <div>
      <select style="float:left;"  id="avlbList" name="avlbList" class="form-control reqPositionNum"  onchange="populateJobOrderInfo(${JobId})">
	    <option value="0">Select Position Number</option> 
	  <c:if test="${not empty avlbList}">
		<c:forEach var="avlst" items="${avlbList}" varStatus="status">
				<c:choose> 
			       <c:when test="${avlst.jobTitle!=null}">	
					<option value="${avlst.requisitionNumber}">${avlst.requisitionNumber}  (${avlst.jobTitle})</option>
			       </c:when>
			       <c:otherwise>
			       <option value="${avlst.requisitionNumber}">${avlst.requisitionNumber}</option>
			       </c:otherwise>
			    </c:choose>
			</c:forEach>
	    </c:if>
	</select>
	
	<c:if test="${userMaster.districtId.districtId!=806900}">
		<a style="cursor:pointer;" href='javascript:void(0);'><i data-original-title='Search By Position Number/School Name' rel='tooltip' id='idFilter' class="fa fa-filter fa-2"  style="position:absolute;font-size:25px;vertical-align:middle;padding:3px;cursor:pointer;"></i></a>
	</c:if>
	
	</div>
	--%>
 </c:when>
      <c:otherwise>
<c:choose>
      <c:when test="${JobId!=0}">
		<input type="text" class="form-control reqPositionNum" disabled value="${jobOrder.requisitionNumber}">
      </c:when>
      <c:otherwise>
      <div>
	<select style="float:left;" id="avlbList" name="avlbList" class="form-control reqPositionNum"  onchange="populateJobOrderInfo(${JobId})">
	    <option value="0">Select Position Number</option> 
	   <c:if test="${not empty avlbList}">
		<c:forEach var="avlst" items="${avlbList}" varStatus="status">
				<c:choose> 
			       <c:when test="${avlst.jobTitle!=null}">	
					<option value="${avlst.requisitionNumber}">${avlst.requisitionNumber}  (${avlst.jobTitle})</option>
			       </c:when>
			       <c:otherwise>
			       <option value="${avlst.requisitionNumber}">${avlst.requisitionNumber}</option>
			       </c:otherwise>
			    </c:choose>
			</c:forEach>
	    </c:if>
	</select>
	<c:if test="${userMaster.districtId.districtId!=806900}">
		<a  href='javascript:void(0);'><i data-original-title='Search By Position Number/School Name' rel='tooltip' id='idFilter' class="fa fa-filter fa-2"  style="position:absolute;font-size:25px;vertical-align:middle;padding:3px;cursor:pointer;"></i></a>
	</c:if>
	</div>
      </c:otherwise>
</c:choose>
 </c:otherwise>
</c:choose>
<c:if test="${fn:contains(currentUrl, 'clonejoborder')}">
<input type="text" class="form-control reqPositionNum" disabled value="${jobOrder.requisitionNumber}">
</c:if>
		      </div>
	    </c:if>
	     <c:if test="${userMaster.districtId.districtId==806900}">
	     <div class="col-sm-3 col-md-3">
					<label><spring:message code="msgJobCode"/><span class="required">*</span></label>
				<c:choose>
				<c:when test="${fn:contains(currentUrl, 'clonejoborder')}">
				    <input type="text" value="${jobOrder.jobCode}" id="refno" maxlength="50" name="refno" class="form-control" style="width:92%;"
				      onfocus="getJobCodeAutoComp(this, event, 'divTxtAccountCode', 'accountCode','accountId','');"
					  onkeyup="getJobCodeAutoComp(this, event, 'divTxtAccountCode', 'accountCode','accountId','');" >
				    </c:when>
				      <c:when test="${JobId!=0}">
					 <input type="text" value="${jobOrder.jobCode}" id="refno" maxlength="50" name="refno" class="form-control" style="width:92%;" readonly>
				      </c:when>
				      <c:otherwise>
				      	<input type="text" value="${jobOrder.apiJobId}" id="refno" maxlength="50" name="refno" class="form-control" style="width:92%;"
				      	onfocus="getJobCodeAutoComp(this, event, 'divTxtDiscrictJobCode', 'refno','distjobcodeId','');"
					  	onkeyup="getJobCodeAutoComp(this, event, 'divTxtDiscrictJobCode', 'refno','distjobcodeId','');">
				      </c:otherwise>
				</c:choose>
				<input  type="hidden" id="distjobcodeId" name="distjobcodeId" value="">
				<div id='divTxtDiscrictJobCode' style='display:none;position:absolute; z-index: 2000;width:195px' class='result'></div>
			</div>
	     </c:if>
	    
	    <c:choose>
		   
			   <c:when test="${userMaster.districtId.districtId==804800 || userMaster.districtId.districtId==806900}">
			     <div class="col-sm-4 col-md-4">
			   </c:when>
		   
			   <c:otherwise>
			     <div class="col-sm-8 col-md-8">
			   </c:otherwise>
		
		</c:choose>
		
		<label>Job Title<span class="required">*</span></label>
		<input type="text" value="${fn:escapeXml(jobOrder.jobTitle)}" id="jobTitle" maxlength="200" name="jobTitle" class="form-control">
		</div>
		<c:choose>
		<c:when test="${fn:contains(currentUrl, 'clonejoborder')}">
    </c:when>
   
    <c:when test="${JobId!=0}">
    </c:when>    
    
     
    
</c:choose>
<c:if test="${userMaster.districtId.districtId==804800}">
		<div class="col-sm-3 col-md-3">
					<label>Job Code</label>
<c:choose>
<c:when test="${fn:contains(currentUrl, 'clonejoborder')}">
      <input type="text" value="${jobOrder.apiJobId}" id="refno" maxlength="50" name="refno" class="form-control" style="width:92%;" readonly>
    </c:when>
      <c:when test="${JobId!=0}">
	<input type="text" value="${jobOrder.apiJobId}" id="refno" maxlength="50" name="refno" class="form-control" style="width:92%;" readonly>
      </c:when>
      <c:otherwise><input type="text" value="${jobOrder.apiJobId}" id="refno" maxlength="50" name="refno" class="form-control" style="width:92%;">
      </c:otherwise>
</c:choose>
		</div>
  </c:if>
  <c:if test="${userMaster.districtId.districtId==806900}">
    <div class="col-sm-3 col-md-3">
		        <label><spring:message code="msgPositionNumber"/></label>
			<input type="text" value="${positionNumber}" class="form-control" name="positionNumber" id="positionNumber">			
  </div>
  </c:if>
	</div>
	  <c:if test="${userMaster.districtId.districtId!=806900}">
		<div class="row">
			<div class="col-sm-5 col-md-5">
			<label>School Name</label>
			<input type="text" class="form-control" id="jeffcoSpcSchlName" value="${jeffcoSpecificSchool}" readonly="readonly"/>
			</div>
		</div>
	</c:if>
    <!-- 
    <c:if test="${userMaster.districtId.districtId==806900}">
		<div class="row">
			<div class="col-sm-5 col-md-5">
			<label>School/Department Name</label>
			<input type="text" class="form-control" id="jeffcoSpcSchlName" value="${jeffcoSpecificSchool}" readonly="readonly"/>
			</div>
		</div>
	</c:if>
	-->
	<input type="hidden" id="copy_text" />
	<c:set var="hide" value="hide"></c:set>
		<div class="row ${hide}" id="requisitionDiv">
				<div class="col-sm-2 col-md-2"  id="" >
					<label>Requisition #</label>
					<input type="text" class="form-control" maxlength="25" id="requisitionNumber1" name="requisitionNumber1" value="${jobOrder.requisitionNumber}"  <c:if test="${jobOrder.requisitionNumber ne null}">disabled="disabled"  </c:if>	>
				</div>
		</div>	
		<div class="row">
		<c:if test="${userMaster.districtId.districtId!=806900}">	 
		<div class="col-sm-4 col-md-4">
<label class="">Subject Name</label>
<div id="subjectDiv" class=""> 
<select id="subjectId" name="subjectId" class="form-control">
 	   <option value="0">Select Subject</option>	
	   <c:if test="${not empty subjectList}">
        <c:forEach var="subjectList" items="${subjectList}" varStatus="status">
        	<c:choose>
			    <c:when test="${jobOrder.subjectMaster.subjectId eq subjectList.subjectId}">
			    	<c:set var="subEditId" value="selected"></c:set>
			    </c:when>
			    <c:otherwise>
			    	<c:set var="subEditId" ></c:set>
			    </c:otherwise>              
			</c:choose>
        	<option value="${subjectList.subjectId}" ${subEditId}><c:out value="${subjectList.subjectName}" /></option>
		</c:forEach>
	   </c:if>	
</select>
</div>
</div>
</c:if>

<c:if test="${userMaster.districtId.districtId==806900}">	 
<div class="col-sm-2 col-md-2">
<label class=""><spring:message code="lblAccountCode"/><span class="required">*</span></label>
<div id="accountCodeDiv" class=""> 
<input type="text" name="accountCode" id="accountCode" class="form-control" 
value="${accountCode}"
onfocus="getAccountingCodeAutoComp(this, event, 'divTxtAccountCode', 'accountCode','accountId','');"
onkeyup="getAccountingCodeAutoComp(this, event, 'divTxtAccountCode', 'accountCode','accountId','');" 
onblur=""
onchange="">
</div>
<input  type="hidden" id="accountId" name="accountId" value="">
<div id='divTxtAccountCode' style=' display:none;position:absolute; z-index: 2000;width:195px' 
							onmouseover="mouseOverChk('divTxtAccountCode','accountCode')" class='result' ></div>
</div>
</c:if>
<c:if test="${userMaster.districtId.districtId==806900}">	 
	<div class="col-sm-3 col-md-3">
<label class=""><spring:message code="lblPositionType"/><span class="required">*</span></label>
<div id="subjectDiv" class=""> 
<select id="positionType" name="positionType" class="form-control">
	
   <option value="">Select Position Type</option>
   <option value="New Position" <c:if test="${not empty positionType && positionType eq 'New Position'}">selected</c:if>>New Position</option>
   <option value="Replacement Position" <c:if test="${not empty positionType && positionType eq 'Replacement Position'}">selected</c:if>>Replacement Position</option>	
</select>
</div>
</div>
</c:if>	

	
	<div class="col-sm-2 col-md-2">
			<label class=""><c:if test="${userMaster.districtId.districtId==804800}"><spring:message code="lblHourPerDay"/> </c:if><c:if test="${userMaster.districtId.districtId==806900}"><spring:message code="lblHourPerWeek"/> <span class="required">*</span></c:if></label>
			<input type="text" id="hourPrday" class="form-control" name="hourPrday" maxlength="3" value="${hourPrDay}">
	</div>

				   <div class="col-sm-2 col-md-2">
	      	   <label>Job Type <span class="required">*</span> </label>
	      	     <select id="jobType" class="form-control">
		       		<c:if test="${jobType eq 'F'}">
		        		<option value="F" >Full Time</option>
		        		<option value="P">Part Time</option>
		       		</c:if>
		       		
	       		    <c:if test="${jobType eq 'P'}">
	         		<option value="P">Part Time</option>
	         		<option value="F" >Full Time</option>
	           	    </c:if>	 
	           	              	   

	           	   
	           	    <c:if test="${empty  jobType}">
	         		<option value="F">Full Time</option>
	         		<option value="P" >Part Time</option>
	           	    </c:if>
	            </select> 
             </div>	
       <c:if test="${userMaster.districtId.districtId==806900}">	 
		<div class="col-sm-3 col-md-3">
			<label class=""><spring:message code="lblMonthsPerYear"/><span class="required"></span></label>
			<div id="subjectDiv" class=""> 
			<input type="text" name="monthsPerYear" id="monthsPerYear" class="form-control" value="${monthsPerYear}">
			</div>
		</div>
		<div class="col-sm-3 col-md-3">
			<label class=""><spring:message code="lblSalaryAdminPlan"/></label>
			<div id="subjectDiv" class=""> 
			<input type="text" name="salaryAdminPlan" id="salaryAdminPlan" class="form-control" value="${salaryAdminPlan}">
			</div>
		</div>
		<div class="col-sm-3 col-md-3">
			<label class=""><spring:message code="lblJobApprovalProcess"/><span class="required">*</span></label>
			<div id="subjectDiv" class=""> 
<!--			hafeezjob-->
				<select name="jobApprovalProcessId" id="jobApprovalProcessId" class="form-control" >
					<option value=""><spring:message code="lblSelectJobApprovalProcess"/></option>
					<c:forEach items="${jobApprovalProcessList}" var="jobApprovalProcess">
					  <c:choose>
					    <c:when test="${not empty jobApporvalProcessSelected && jobApporvalProcessSelected.jobApprovalProcessId.jobApprovalProcessId eq jobApprovalProcess.jobApprovalProcessId}">
					    <option value="<c:out value='${jobApprovalProcess.jobApprovalProcessId}'/>" selected><c:out value='${jobApprovalProcess.jobApprovalProcess}'/></option>
					    </c:when>
					    <c:otherwise>
					 	   <option value="<c:out value='${jobApprovalProcess.jobApprovalProcessId}'/>"><c:out value='${jobApprovalProcess.jobApprovalProcess}'/></option>
					    </c:otherwise>
					</c:choose>
					</c:forEach>
				</select>
			</div>
		</div>
	</c:if>	
		</div>	
<c:set var="endDateRadio1Checked" value=""></c:set>
<c:set var="endDateRadio2Checked" value=""></c:set>
<c:set var="jobendDate" value="${jobEndDate}"></c:set>
<c:set var="jobendDateDis" value=""></c:set>
<c:choose>
    <c:when test="${jobEndDate eq '12-25-2099'}">
        <c:set var="endDateRadio2Checked" value="checked='checked'"></c:set>
        <c:set var="jobendDate" value=""></c:set>
        <c:set var="jobendDateDis" value="disabled='disabled'"></c:set>
    </c:when>
    <c:otherwise>
    	<c:set var="jobendDateDis" value=""></c:set>
        <c:set var="endDateRadio1Checked" value="checked='checked'"></c:set>
    </c:otherwise>
</c:choose>
    <c:set var="hiddenJobRadio1Checked" value=""></c:set>
    <c:set var="hiddenJobRadio2Checked" value=""></c:set>
    <c:if test="${hideJobFlag eq true}">
        <c:set var="hiddenJobRadio2Checked" value="checked='checked'"></c:set>
    </c:if>
    <c:if test="${hideJobFlag eq false}">
        <c:set var="hiddenJobRadio1Checked" value="checked='checked'"></c:set>
    </c:if>
<div class="row">
	<div class="col-sm-2 col-md-2" style="margin-left: 0px;padding-right:0px;">
	      	   <label >Job Status<span class="required">*</span> </label>
	      	     <select id="jobApplicationStatus" class="form-control"  <c:if test="${userMaster.districtId.districtId!=806900}">onchange="showHideVisibilityOFTemp()"</c:if>>
		       		 <option value="0" ${jobOrder.jobApplicationStatus == 0 ? 'selected' : ''}>Select Status</option>
		       		 <option value="1" ${jobOrder.jobApplicationStatus == 1 ? 'selected' : ''}>
		       		 <c:choose>
			       		 <c:when test="${userMaster.districtId.districtId!=806900}">
			       		 	Ongoing
			       		 </c:when>
			       		 <c:otherwise>
							Regular
						</c:otherwise>
					 </c:choose>
		       		 </option>
		        	 <option value="2" ${jobOrder.jobApplicationStatus == 2 ? 'selected' : ''}>Temporary</option>
		        </select> 
        </div>
  		   <div class="col-sm-3 col-md-3 hide" style="margin-left: 0px;" id="temporaryType">
	      	   <label >Temporary Type <span class="required">*</span></label>
	      	     <select id="tempType" class="form-control">
	      	     	 <option value="0" ${jobOrder.tempType == 0 ? 'selected' : ''} >Select Temporary Type</option>
		       		 <option value="1" ${jobOrder.tempType == 1 ? 'selected' : ''} >Funding</option>
		       		 <option value="2" ${jobOrder.tempType == 2 ? 'selected' : ''}>Late Posting</option>
		        	<option value="3" ${jobOrder.tempType == 3 ? 'selected' : ''} >Protected</option>	       		
	            </select> 
             </div>
      <div class="col-sm-3 col-md-3" id="changeCss1">
        <label>Posting Start Date<span class="required">*</span></label>
        <input type="text" id="jobStartDate" name="jobStartDate"   maxlength="0"   value="${jobStartDate}" class="form-control" style="width:92%;">
      </div>
      <div class="col-sm-3 col-md-3" style="padding-left:0px;" id="changeCss2">
        <label>Posting End Date<span class="required">*</span></label>
        
        <label class="radio" style="min-height:0px;margin-bottom:-6px;margin-top:6px;"><input type="radio" name="endDateRadio" ${endDateRadio1Checked} id="endDateRadio1" onclick="return unDisableEndDate();"/></label>
        <input type="text" id="jobEndDate" name="jobEndDate"   maxlength="0" ${jobendDateDis}   value="${jobEndDate}" class="form-control" style="width:82%;margin-left:20px;">
        
      </div>
      <input type="hidden" id="changeDateHid" value="">
      <div class="col-sm-2 col-md-2" style="padding-left:0px;">
        <label>&nbsp;</label>
        <label class="radio" style="min-height:0Px;margin-bottom:-6px;margin-top:6px;"><input type="radio" name="endDateRadio" ${endDateRadio2Checked} id="endDateRadio2" onclick="return disableEndDate();"/></label><input type="text" id="" name="" disabled  value="Until Filled" class="form-control" style="width:86%;margin-left:20px;">
        
      </div>
      <!--<div class="col-sm-3 col-md-3">
			<label>Reference No./Job Code</label>
			<input type="text" value="${jobOrder.apiJobId}" id="refno" maxlength="50" name="refno" class="form-control">
		</div>
      
      --><!-- Mukesh -->
<!-- 
		   <div class="col-sm-3 col-md-3">
	      	   <label>Job Type <span class="required">*</span> </label>
	      	     <select id="jobType" class="form-control">
		       		<c:if test="${jobType eq 'F'}">
		        		<option value="F" >Full Time</option>
		        		<option value="P">Part Time</option>
		        		<option value="FP">Full Time/ Part Time </option>
		       		</c:if>
		       		
	       		    <c:if test="${jobType eq 'P'}">
	         		<option value="P">Part Time</option>
	         		<option value="F" >Full Time</option>
	         		<option value="FP">Full Time/ Part Time</option>
	           	    </c:if>	 
	           	              	   
	           	     <c:if test="${jobType eq 'FP'}">
	           	     <option value="FP">Full Time/ Part Time</option>
	         		<option value="P">Part Time</option>
	         		<option value="F" >Full Time</option>	         		
	           	    </c:if>
	           	   
	           	    <c:if test="${empty  jobType}">
	         		<option value="F">Full Time</option>
	         		<option value="P" >Part Time</option>
	         		<option value="FP">Full Time/ Part Time</option>
	           	    </c:if>
	            </select> 
             </div>
-->
</div>

<!--           Deepak        -->
<div class="row onlyjeffco">
      <div class="col-sm-3 col-md-3">
        <label>Position Start Date</label><span class="required">*</span>
        <input type="text" id="positionStartDate" name="positionStartDate" value="${positionStartDate}"   maxlength="0"    class="form-control" style="width:92%;">
        
      </div>
      <div class="col-sm-3 col-md-3" style="padding-left:0px;" id="posEndDateDiv">
        <label>Position End Date </label><span class="required">*</span>
        <input type="text" id="positionEndDate" name="positionEndDate" value="${positionEndDate}"   maxlength="0"    class="form-control" style="width:92%;"> 
             
      </div>
          <!--
		Deepak 
		-->
      	 <!--Gaurav Kumar-->
      	 <!--   
       <div class="col-sm-2 col-md-2">
	      	   <label>Grant Flag</label>
	      	  
	      	     <select id="jeffcoGrantFlag" class="form-control" disabled>
		       	    <option value="N" >N</option>
	         		<option value="Y">Y</option>
	            </select> 
	           
             </div>  
        -->    
          
       <div class="col-sm-1 col-md-2">
      	   <label>Special Ed Flag</label>
      	     <select id="jeffcoSpecialEdFlag" class="form-control" disabled>
	       	 	<option value="N">N</option>
         		<option value="Y">Y</option>
         	</select> 
      </div>        
               
</div>
<c:if test="${userMaster.districtId.districtId==806900}">	 
	 <!-- <div  class="row"> 
    <div class="col-sm-3 col-md-3">
			<label class="">Department Approval</label>
			<div id="subjectDiv" class=""> 
				<select id="departmentApproval" name="departmentApproval" class="form-control">
				 	   <option value="">Select Department Approval</option>	
					   <option value="Athletics Approval" <c:if test="${not empty departmentApproval && departmentApproval eq 'Athletics Approval'}">selected</c:if>>Athletics Approval</option>	
					   <option value="Custodial Approval" <c:if test="${not empty departmentApproval && departmentApproval eq 'Custodial Approval'}">selected</c:if>>Custodial Approval</option>	
					   <option value="Enterprise Services Approval" <c:if test="${not empty departmentApproval && departmentApproval eq 'Enterprise Services Approval'}">selected</c:if>>Enterprise Services Approval</option>	
					   <option value="Language Acquisition Approval" <c:if test="${not empty departmentApproval && departmentApproval eq 'Language Acquisition Approval'}">selected</c:if>>Language Acquisition Approval</option>	
					   <option value="Special Education Approval" <c:if test="${not empty departmentApproval && departmentApproval eq 'Special Education Approval'}">selected</c:if>>Special Education Approval</option>	
				</select>
			</div>
	</div>  
	--> 
	<!-- 
	<div class="col-sm-1 col-md-2">
      	<label>Approval Code</label>
      	<input type="text" id="approvalCode" name="approvalCode" value="${approvalCode}" class="form-control" style="width:92%;"> 
      </div>   
	</div>
	-->
</c:if>
<c:if test="${userMaster.districtId.districtId!=806900}">	
<div class="row hide" id="extraFieldJeffco">
	<div class="col-sm-1 col-md-2">
	<c:if test="${entityType==2}">
		<c:set var="editOrNot" value="" />
	</c:if>
	<c:if test="${entityType!=2}">
		<c:set var="editOrNot" value="disabled=\"disabled\"" />
	</c:if>
		<label>FTE</label>
		<input class="form-control" type="text" name="fte" maxlength="100" value="${fte}" onkeypress="return keydownAmount(event,this);"  ${editOrNot}/>
	</div>
	<div class="col-sm-1 col-md-2">
		<label>Days Worked</label>
		<input class="form-control" type="text" name="daysworked" maxlength="100" value="${daysworked}"  disabled="disabled"/>
	</div>
	<div class="col-sm-1 col-md-2 hide" id="fsalaryId">
		<label>Salary</label>
		<input class="form-control" type="text" name="fsalary" maxlength="100" value="${fsalary}"  disabled="disabled"/>
		<input class="form-control" type="text" name="fsalaryA" maxlength="100" value="${fsalaryA}"  disabled="disabled"/>
	</div>
	<div class="col-sm-1 col-md-2 hide" id="ssalaryId">
		<label>&nbsp;</label>
		<input class="form-control" type="text" name="ssalary" maxlength="100" value="${ssalary}"  disabled="disabled"/>
	</div>
	<div class="col-sm-1 col-md-2">
		<label>Pay Grade</label>
		<input class="form-control" type="text" name="paygrade" maxlength="100" value="${paygrade}"  disabled="disabled"/>
	</div>
</div>
</c:if>
<c:if test="${userMaster.districtId.districtId==806900}">	 
<div class="row" id="extraFieldAdams">
	<div class="col-sm-1 col-md-2">
	<c:if test="${entityType==2 || entityType==3}">
		<c:set var="editOrNot" value="" />
	</c:if>
	<c:if test="${entityType!=2 && entityType!=3}">
		<c:set var="editOrNot" value="disabled=\"disabled\"" />
	</c:if>
		<label>FTE<span class="required"></span></label>
		<input class="form-control" type="text" id="fteforAdams" name="fteforAdams" maxlength="100" value="${fte}" onkeypress="return keydownAmount(event,this);"  ${editOrNot}/>
	</div>
	
	<div class="col-sm-3 col-md-3">
		<label>Minimum Salary/Hourly Rate</label>
		<input class="form-control" type="text" name="monthlySalary" maxlength="100" id="monthlySalary" value="${mSalary}"/>
	</div>
	<div class="col-sm-1 col-md-2">
		<label>Pay Grade</label>
		<input class="form-control" type="text" name="paygrade" maxlength="100" value="${paygrade}" id="paygrade" disabled="disabled"/>
	</div>
	 <div class="col-sm-1 col-md-2">
      	    <label>Step</label>
      	    <input type="text" name="step" id="step" class="form-control" value="${step}" disabled="disabled"/>
      </div>
</div>
</c:if>

<div class="row jeffcoCombo" style='display:none;'>
       <div class="col-sm-4 col-md-4">
		        <label>Description Template</label>
	              <select id="jobList" name="jobList" class="form-control"  onchange="jobListDiscription()">
	                  <option value="0">Select Job Title</option> 
	                  <c:if test="${not empty jobTitleList}">
		              <c:forEach var="joblst" items="${jobTitleList}" varStatus="status">
		        	  <option value="${joblst.jobTitleWithDescritionId}">${joblst.jobTitle}</option>
		              </c:forEach>
	                  </c:if>
          	</select>
        </div>
 </div>

<!-- --------------------------------- -->

<!-- 
	<div class="row">
		<div class="col-sm-3 col-md-3">
					<label>Reference No./Job Code</label>
					<input type="text" value="${jobOrder.apiJobId}" id="refno" maxlength="50" name="refno" class="form-control" style="width:92%;">
		</div>
		<div class="col-sm-5 col-md-5">
		</div>
		<div class="col-sm-3 col-md-3  mt10">
			<c:if test="${addJobFlag && fn:indexOf(roleAccess,'|1|')!=-1 || entityType==2}">
			<a href="javascript:void(0);" onclick="addCertification();">+ Add Certification/Licensure Name</a>
			</c:if>
	</div>
	</div>
-->

 <div  class="modal hide"  id="myModalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">TeacherMatch</h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="Msg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn btn-primary" id="delBtn" data-dismiss="modal" aria-hidden="true" onclick="return deleteRequisationNumber();">Ok</button>
	 		<button class="btn btn-primary hide" id="msjBtn"  data-dismiss="modal" aria-hidden="true">Ok</button> 		
	 		<button class="btn" id="closeBtn" data-dismiss="modal" aria-hidden="true">Close</button>
 		</div>
	</div>
	</div>
	</div>	

<div  class="modal hide"  id="jobOrderSend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="actionForward();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="" style="height:120px;">
		    	<span id="jobOrderText"></span>	        	
			</div>
		</div>
 	</div>
	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="actionForward();">Ok</button> 		
 	</div>
</div>
	</div>
	</div>	
	
<!--<div class="row">
	<div class="col-sm-8 col-md-8"></div>
	<div class="col-sm-4 col-md-4  mt10">
	<c:if test="${addJobFlag && fn:indexOf(roleAccess,'|1|')!=-1}">
	<a href="javascript:void(0);" onclick="addCertification();">+ Add Certification/Licensure Name</a>
	</c:if>
	</div>
</div>

--><div class="row">	
	<div class="col-sm-12 col-md-12">
		<div id="showCertificationDiv"></div>
	</div>	
	<div class="col-sm-12 col-md-12 top20">
		<div id="certificationRecords"></div>
	</div>
</div>	

<div class="row">		
	<div id="addCertificationDiv"   class="hide">		
			<div class="col-sm-4 col-md-4"><label><strong>State<span class="required">*</span></strong></label>
					<select  class="form-control" id="stateMaster" name="stateMaster" onchange="getPraxis(),clearCertType();">   
						<option value="0">Select State</option>
						<c:forEach items="${listStateMaster}" var="st">
			        		<option id="st${st.stateId}" value="${st.stateId}" ${selected} >${st.stateName}</option>
						</c:forEach>				
					</select>	
			</div>
			<div class="col-sm-7 col-md-7">
				<label>Certification/Licensure Name<span class="required">*</span><a href="#" id="iconpophover11" rel="tooltip" data-original-title="Name of certification as per certifying body such as a state department of education">
					<img width="15" height="15" alt="" src="images/qua-icon.png"></a>
				</label>
				<input type="hidden" id="certificateTypeMaster" value=""/>
				<input type="text" placeholder="<Certification/Licensure Name>"
					class="form-control"
					maxlength="100"
					id="certType" 
					value=""
					name="certType" 
					onfocus="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
					onkeyup="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');" 
					onblur="hideCertificateType(this,'certificateTypeMaster','divTxtCertTypeData');"/>
				<div id='divTxtCertTypeData' style=' display:none;position:absolute;' onmouseover="mouseOverChk('divTxtCertTypeData','certType')" class='result' ></div>
			</div>
		
		<div class="col-sm-4 col-md-4 top5">
			<div><a href="javascript:void(0);" onclick="validateAddCertification();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="cancelCertification();">Cancel</a></div>             
		</div>
	</div>
</div>
<!--//////////5555555///////-->
<div class="row top10">
<div class="col-sm-4 col-md-4">
<label class="">Job Category Name <span class="required">*</span></label>
<div id="jobCategoryDiv" class=""> 

<select id="jobCategoryId" name="jobCategoryId" ${jobCategoryDisabled} class="form-control" onchange=" showDivMsg(this.value);getJobSubcategory();">
	  <option linkShow="false" value="0">Select Job Category Name</option>
	   <c:if test="${not empty jobCategoryMasterlst}">
        <c:forEach var="jobCategoryMaster" items="${jobCategoryMasterlst}" varStatus="status">
        	<c:set var="jCIdSvalue" value=""></c:set>
        	<c:choose>
        		<c:when test="${(jobOrder.jobCategoryMaster.parentJobCategoryId != null) && (jobOrder.jobCategoryMaster.parentJobCategoryId.jobCategoryId > 0) }">
        			<c:if test="${jobOrder.jobCategoryMaster.parentJobCategoryId.jobCategoryId==jobCategoryMaster.jobCategoryId}">
		        		<c:set var="jCIdSvalue" value="selected"></c:set>
		        	</c:if>
        		</c:when>
        		<c:otherwise>
        			<c:if test="${jobOrder.jobCategoryMaster.jobCategoryId==jobCategoryMaster.jobCategoryId}">
		        		<c:set var="jCIdSvalue" value="selected"></c:set>
		        	</c:if>
        		</c:otherwise>
        	</c:choose>
        	<option linkShow="${jobCategoryMaster.approvalByPredefinedGroups}" value="${jobCategoryMaster.jobCategoryId}||${jobCategoryMaster.baseStatus}||${jobCategoryMaster.assessmentDocument}" ${jCIdSvalue}><c:out value="${jobCategoryMaster.jobCategoryName}"/></option>
		</c:forEach>
        </c:if>
</select>
</div>
</div>
<input type="hidden" id="chkCase" value="${chkCase}" />
<input type="hidden" id="chksubCate" value="${chksubCate}" />

	<div class="col-sm-4 col-md-4" id="jobSubCateDiv">
		<label class="">Job Sub Category Name</label>
		<div id="jobSubCategoryDiv">
		 	<select id="jobSubCategoryId" name="jobSubCategoryId" ${jobCategoryDisabled} class="form-control"">
				  <option value="0">Select Job Sub Category Name</option>
				   <c:if test="${not empty jobSubCategoryMasterlst}">
			        <c:forEach var="jobSubCategoryList" items="${jobSubCategoryMasterlst}" varStatus="status">
			        	<c:set var="jCIdSvalue" value=""></c:set>
			        	<c:if test="${jobOrder.jobCategoryMaster.jobCategoryId==jobSubCategoryList.jobCategoryId}">
			        		<c:set var="jCIdSUBvalue" value="selected"></c:set>
			        	</c:if>
						<option linkShow="${jobCategoryMaster.approvalByPredefinedGroups}" value="${jobSubCategoryList.jobCategoryId}||${jobSubCategoryList.baseStatus}||${jobSubCategoryList.assessmentDocument}" ${jCIdSUBvalue}><c:out value="${jobSubCategoryList.jobCategoryName}"/></option>
					</c:forEach>
			       </c:if>
			</select>
		</div>
	</div>
	<div class="col-sm-4 col-md-4 hide" id="districtjobdescriptionLibrery">
				     <label id="captionDistrictOrSchool"><spring:message code="headJobDescrip"/></label>
				     <div id="districtjobdescriptionLibreryname"></div>
				    <!--<select id="jobcategoryId" class="form-control">
				     	<option value="0" >Select Job Category</option>

					</select>
	     	 
	     	--></div>
</div>
<!--///////////-->
<div class="row" >
			<div class="col-sm-10 col-md-10">
				<label>Job Description <a href="#" id="iconpophover2" rel="tooltip" data-original-title="Detailed job description that will be displayed to the job applicants on TeacherMatch platform"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></label>
				<label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document, please ensure
					   to paste that text first into a simple text editor such as Notepad to remove the special characters and
 					   then copy and paste it into the field below. Otherwise, the text directly cut and pasted from a web page 
 					   and/or a Microsoft document may not display properly to the reader)</label>
			</div>	
			
			
	<div class="col-sm-11 col-md-11">			
		<div class="" id="jDescription">
			<textarea rows="8" id="jobDescription"   name="jobDescription"  class="form-control">${jobOrder.jobDescription}</textarea>
		</div>			
	</div>
</div>

<div class="row top5" >
			<div class="col-sm-4 col-md-4">
				<label>Upload Job Description</label>
				<input type="hidden" id="uploadedJobDiscriptionFile" name="uploadedJobDiscriptionFile"/>
				<form id='jobDescriptionDocUpload' enctype='multipart/form-data' method='post' target='uploadFrame' action='fileuploadservletjobdescription.do' class="form-inline">
					<input type="hidden" id="disHiddenId" name="disHiddenId" value="${DistrictOrSchoolId}"/>
					<input  type="file" name="jobDescriptionDoc" id="jobDescriptionDoc" onchange="uploadJobDescriptionFile(this.value);">
				</form>
			</div>
			<div class="col-sm-5 col-md-5" style="padding-top: 21px;">			
			<c:if test="${not empty jobOrder.pathOfJobDescription}">
					<a href="#" id="iconpophover16" rel="tooltip" data-original-title="Click here to view job description">
						<span id="hrefJobDesc" onclick="viewJobDescription('${jobOrder.jobId}');if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;">View</span>
					</a>	
				</c:if>	
				</div>
</div>


<div class="row top10" >
			<div class="col-sm-10 col-md-10">
				<label>Additional Information <a href="#" id="iconpophover7" rel="tooltip" data-original-title="Please use this area to include any additional information related to the position or the school/department."><img width="15" height="15" alt="" src="images/qua-icon.png"></a></label>
				<label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document, please ensure
					   to paste that text first into a simple text editor such as Notepad to remove the special characters and
 					   then copy and paste it into the field below. Otherwise, the text directly cut and pasted from a web page 
 					   and/or a Microsoft document may not display properly to the reader)</label>
			</div>
			
			<div class="col-sm-11 col-md-11">	
				<div class="" id="jQualification">
				<textarea rows="8" id="jobQualification"   name="jobQualification"  class="form-conntrol">${jobOrder.jobQualification}</textarea>
				</div>	
			</div>
</div>

	<div class="row top10" >
			<div class="col-sm-10 col-md-10">
				<label>Internal Notes</label>
				<label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document, please ensure
					   to paste that text first into a simple text editor such as Notepad to remove the special characters and
 					   then copy and paste it into the field below. Otherwise, the text directly cut and pasted from a web page 
 					   and/or a Microsoft document may not display properly to the reader)</label>
			</div>
			
			<div class="col-sm-11 col-md-11">	
				<div class="" id="jobIntNotes">
				<textarea rows="8" class="form-conntrol">${jobOrder.jobInternalNotes}</textarea>
				</div>	
			</div>
</div>



<!--<div class="row">
<div class=" col-sm-4 col-md-4">
<label class="">Subject Name</label>
<div id="subjectDiv" class=""> 
<select id="subjectId" name="subjectId" class="form-control">
 	   <option value="0">Select Subject</option>	
	   <c:if test="${not empty subjectList}">
        <c:forEach var="subjectList" items="${subjectList}" varStatus="status">
        	<c:choose>
			    <c:when test="${jobOrder.subjectMaster.subjectId eq subjectList.subjectId}">
			    	<c:set var="subEditId" value="selected"></c:set>
			    </c:when>
			    <c:otherwise>
			    	<c:set var="subEditId" ></c:set>
			    </c:otherwise>              
			</c:choose>
        	<option value="${subjectList.subjectId}" ${subEditId}><c:out value="${subjectList.subjectName}" /></option>
		</c:forEach>
	   </c:if>	
</select>
</div>
</div>
</div>-->

<div class="row">
<div class="col-sm4 col-md-4 hide"  id="addHiresDiv" >
	<label># of Expected Hire(s)<span class="required">*</span></label>
	<input type="text" class="form-control" maxlength="3" id="noOfExpHiresSchool" name="noOfExpHiresSchool" value="${noOfExpHires}"  onkeypress="return checkForInt(event);">
</div>
</div>


    <input type="hidden" name="allSchoolGradeDistrictVal" id="allSchoolGradeDistrictVal" value="${allSchoolGradeDistrictVal}"/>
	<div class="hide" id="hideForSchool">
	
	<c:set var="hideVar" value="hide"/>
	<c:if test="${entityType==1}">
		<c:if test="${jobOrder.districtMaster.isReqNoRequired}">
			<c:set var="hideVar" value=""/>
		</c:if>
	</c:if>
	<c:if test="${entityType==2}">
		<c:if test="${userMaster.districtId.isReqNoRequired}">
			<c:set var="hideVar" value=""/>
		</c:if>
	</c:if>
	
	<c:set var="expHireFlagforMultiple" value=""/>
	<c:if test="${expHireNotEqualToReqNoFlag}">
		<c:set var="expHireFlagforMultiple" value="checked=checked"/>
	</c:if>
	
	
	<!-- <div class="row left7 top15  ${hideVar}" id="checkbox_Multiple_hires_req" > -->
	<div class="row left7 top15 hide">
		   <div class="col-sm-10 col-md-10">
           <label class="checkbox inline" style="padding-left: 0px;">
					<input type="checkbox" id="isExpHireNotEqualToReqNo" name="isExpHireNotEqualToReqNo" ${expHireFlagforMultiple}/>
            		Multiple hires will be allocated to a single requisition number
           </label>
           </div>
	</div>
	<c:set var="isExpHireNotEqualToReqNo" value=""></c:set>
	<c:choose>
	    <c:when test="${jobOrder.isExpHireNotEqualToReqNo ne null}">
		    <c:if test="${jobOrder.isExpHireNotEqualToReqNo eq true}">
	    		 <c:set var="isExpHireNotEqualToReqNo" value="checked='checked'"></c:set>
	    	</c:if> 
	    </c:when>
	</c:choose>
	<div class="row col-sm-10 col-md-10 top10 hide">
		Which Administrator(s) can hire Candidate(s) from this job order applicant pool?
	</div>
	
	<c:set var="disableRadioDACH" value=""></c:set>
	<c:set var="disableRadioSACH" value=""></c:set>
	<c:if test="${disableRadioDA eq 0}">
		<c:set var="disableRadioDACH" value="disabled='disabled'"></c:set>
	</c:if>
	
	<c:if test="${disableRadioSA eq 0}">
		<c:set var="disableRadioSACH" value="disabled='disabled'"></c:set>
	</c:if>
	
	<div class="hide">
			<select multiple id="tempList" name="tempList" class="span4" style=> 
				<c:if test="${not empty attachedList}">
				        <c:forEach var="attlst" items="${attachedList}" varStatus="status">
				        	<c:set var="disable" value=""></c:set>
				        	<c:if test="${attlst.status eq 1}">
				        		<c:set var="disable" value="disabled"></c:set>
				        	</c:if> 
							<option value="${attlst.districtRequisitionNumbers.districtRequisitionId}" ${disable} >${attlst.districtRequisitionNumbers.requisitionNumber}</option>
						</c:forEach>
				    </c:if>
			</select>
	</div>
	
	
	<div class="row col-sm-4 col-md-4 hide">
		<div>
			<label class="radio">
				<input type="radio" name="allSchoolGradeDistrict" onclick="checkMSG(1);checkValue(1);" id="allSchoolGradeDistrict" value="1" ${(jobOrder.noSchoolAttach == '1' || PageFlag==0) ? 'checked' : ''} ${disableRadioDACH} >
				Only District Administrator(s)
			</label>
		</div>
		<c:set var="hideAddReqLink" value="hide"></c:set>
		<c:if test="${isReqNoFlagByDistrict eq 1 && jobOrder.jobId>0}">
			<c:set var="hideAddReqLink" value="hide"></c:set>
		</c:if>		
	</div>
   <c:if test="${userMaster.districtId.districtId==806900}">
		<div class="row col-sm-4 col-md-4">
			<div>
				<label class="radio">
					<input type="radio" name="allSchoolGradeDistrict" onclick="checkMSG(1);checkValue(1);" id="allSchoolGradeDistrict" value="1" ${(jobOrder.noSchoolAttach == '1' || PageFlag==0) ? 'checked' : ''} ${disableRadioDACH} >
					Only District Administrator(s)
				</label>
			</div>
			<c:set var="hideAddReqLink" value="hide"></c:set>
			<c:if test="${isReqNoFlagByDistrict eq 1 && jobOrder.jobId>0}">
				<c:set var="hideAddReqLink" value="hide"></c:set>
			</c:if>		
		</div>
	</c:if>
	
	
	
	<div  id="addNoSchoolDiv" >
		 <div class="row">
	        <div class="col-sm-4 col-md-4 hide" style="border:0px solid blue;">
	          <div id="districtReqNumberDiv" class="">
	             </div>
	        </div>
	     </div>
	     
	    
	    
	    		
		<div class="row left15">
		<div class="col-sm-4 col-md-4">
		<label># of Expected Hire(s)</label>
		<input type="text" class="form-control" maxlength="3" id="noOfExpHires" name="noOfExpHires" value="${jobOrder.noOfExpHires}"  onkeypress="return checkForInt(event);">
		
		<%-- ====== Gagan : Only Checking Here on District Set up [ isReqNoRequired ] is checked or not --%>
		<input type="hidden" class="span1" id="districtReqNumFlag" name="districtReqNumFlag" value="${jobOrder.districtMaster.isReqNoRequired}">
		<input type="hidden" class="span4" id="arrReqNum" name="arrReqNum" value="">
		<input type="hidden" class="span1" id="delReqId" name="delReqId" value="">
		<input type="hidden" class="span1" id="isReqNoFlagByDistrict" name="isReqNoFlagByDistrict" value="${isReqNoFlagByDistrict}">
		</div>
		</div>
		<c:set var="hideReq" value="hide"></c:set>
		<c:if test="${(jobOrder.districtMaster.isReqNoRequired eq true) || (entityType eq 2 && districtReqNumFlag eq true)}">
	    		 <c:set var="hideReq" value=""></c:set>
	    </c:if> 
		
		
		 
		<div class="row left15">
	    	<div class="col-sm-6 col-md-6">
				<label class="radio">
					<input type="radio" name="rdReqNoSource" id="rdReqNoSource1" value="1" onclick="validateNewReq()">
					Enter Requisition Number(s)
					<a href="#" id=iconpophoverReqEg rel="tooltip" data-original-title="(Comma separated) Eg. 09000006,53100008,53107111"><img width="15" height="15" alt="" src="images/qua-icon.png"></a>
				</label>
			</div>
	    </div>
	    
	    <div class="row left15">
	    	<div class="col-sm-10 col-md-10" style="max-width: 725px;">
				<label></label>
				<input type="text" class="form-control" id="newTextRequisitionNumbers" name="newTextRequisitionNumbers" onblur="validateNewReq()">
			</div>
	    </div>
	    
	     <div class="row left15">
	    	<div class="col-sm-4 col-md-4">
				<label class="radio">
					<input type="radio" name="rdReqNoSource" id="rdReqNoSource2" value="2">
					Select Requisition Number(s)
				</label>
			</div>
	    </div>
		 
		
		<div id="reqNumberDiv">	
		<div class="row left15">		 
		 <div class="col-sm-4 col-md-4" style="border: 0px solid red;">
		 		<label>Available Requisition #</label>
				<select multiple id="avlbList" name="avlbList" class="form-control" style="height: 150px;" > 
				   <c:if test="${not empty avlbList}">
				        <c:forEach var="avlst" items="${avlbList}" varStatus="status">
							<option value="${avlst.districtRequisitionId}">${avlst.requisitionNumber}</option>
						</c:forEach>
				    </c:if>
				</select>
			</div>		
			<div class="col-sm-1 col-md-1 left20-sm"> 
				<div class="span2"> <span id="addPop" style="cursor:pointer;"><img alt="" src="images/rightarrow.jpg" width="30px"> </span></div>
				<div class="span2"> <span id="removePop" style="cursor:pointer;"><img alt="" src="images/leftarrow.jpg" width="30px"> </span></div>
			</div>
			<div class="col-sm-4 col-md-4" style="border: 0px solid green;">
				<label>Attached Requisition #</label>	
				<select multiple class="form-control" id="attachedList" name="attachedList" style="height: 150px;">
				   <c:if test="${not empty attachedList}">
				        <c:forEach var="attlst" items="${attachedList}" varStatus="status">
				        	<c:set var="disable" value=""></c:set>
				        	<c:if test="${attlst.status eq 1}">
				        		<c:set var="disable" value="disabled"></c:set>
				        	</c:if> 
							<option value="${attlst.districtRequisitionNumbers.districtRequisitionId}" ${disable} >${attlst.districtRequisitionNumbers.requisitionNumber}</option>
						</c:forEach>
				    </c:if>
				</select>
			</div>	
		  </div>
        </div>
        
	</div>
	
	
	<div class="row left5 top10">
	<div class="col-sm-11 col-md-11">
			<label class="radio inline" style="padding-left: 0px;"><!--
				<input type="radio" 
				name="allSchoolGradeDistrict" id="allSchoolGradeDistrict" onclick="checkMSG(2);checkValue(2);" value="2" ${jobOrder.selectedSchoolsInDistrict == '1' ? 'checked' : ''} ${disableRadioSACH}>
			
			-->
			<c:if test="${entityType ne 3}">
					<input type="radio" 
					name="allSchoolGradeDistrict" id="allSchoolGradeDistrict" onclick="checkMSG(2);checkValue(2);" value="2" ${jobOrder.selectedSchoolsInDistrict == '1' ? 'checked' : ''} ${disableRadioSACH}>
		   </c:if>
		   <c:if test="${entityType eq 3}">
		  		 <input type="radio" 
					name="allSchoolGradeDistrict" id="allSchoolGradeDistrict" class="allSchoolGradeDistrict" value="2" ${jobOrder.selectedSchoolsInDistrict == '1' ? 'checked' : ''} ${disableRadioSACH}>
		   </c:if>
		   	Administrators&nbsp;from&nbsp;Selected&nbsp;School(s)
			</label>
			<input type="hidden" class="span4" id="jobAuthKey" name="jobAuthKey" value="${param.jobAuthKey}">
			<input type="hidden" class="span2" id="currentSchoolId" name="currentSchoolId" value="">
			<input type="hidden" class="span1" id="currentSchoolExpectedHires" name="currentSchoolExpectedHires" value="">
			<input type="hidden" class="span4" id="arrReqNumforSchool" name="arrReqNumforSchool" value=""/> 
			<input type="hidden" class="span2" id="arrReqNumAndSchoolId" name="arrReqNumAndSchoolId" value=""/> 
			<input type="hidden" class="span6" id="arrTotalReqNumforSchool" name="arrTotalReqNumforSchool" value=""/>
			<input type="hidden" class="span8" id="arrTotalReqNumAndSchoolId" name="arrTotalReqNumAndSchoolId" value=""/>
			<input type="hidden" class="span1" id="flagIfEqualSchoolReqNoAndNoofExpHire" name="flagIfEqualSchoolReqNoAndNoofExpHire" value="">
			<input type="hidden" class="span1" id="requiSchoolIdsForHire" value="${requisitionSchoolList}">
			<div class="pull-right hide" id="addSchoolLink">
				<c:if test="${addJobFlag && fn:indexOf(roleAccess,'|1|')!=-1 || entityType==2}">
					<a href="javascript:void(0);" onclick="addSchool();">+  Add School</a>
				</c:if>			
			</div>
	</div>
	</div>

<div class="hide"  id="addSchoolDiv">	
	<div class="row left15">
	<div class="col-sm-5 col-md-5 mt10">	
	<label>School Name</label>
	 <input type="text" id="schoolName" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
										onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
										onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2'); lockGeozone(1);saveSchoolId();"	/>
	 	 <input type="hidden" id="schoolId"/>
		 <div id='divTxtShowData2' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')"  class='result' ></div>
	</div>
	<div class="col-sm-4 col-md-4 mt10">
	<label># of Expected Hire(s)</label>
	<input type="text" class="form-control" maxlength="3" id="noOfSchoolExpHires" name="noOfSchoolExpHires"  onkeypress="return checkForInt(event);" placeholder="">
	
	</div>
	</div>
	
	<!--  -->
	<div class="row left15">
    	<div class="col-sm-6 col-md-6">
			<label class="radio">
				<input type="radio" name="rdReqNoSourceSchool" id="rdReqNoSourceSchool1" value="1" onclick="validateNewReqSchool()">
				Enter Requisition Number(s)
				<a href="#" id="iconpophoverReqEgSchool" rel="tooltip" data-original-title="(Comma separated) Eg. 09000006,53100008,53107111"><img width="15" height="15" alt="" src="images/qua-icon.png"></a>
			</label>
		</div>
    </div>
    
    <div class="row left15">
    	<div class="col-sm-10 col-md-10" style="max-width: 705px;">
			<label></label>
			<input type="text" class="form-control" id="newTextRequisitionNumbersSchool" name="newTextRequisitionNumbersSchool" onblur="validateNewReqSchool()">
		</div>
    </div>
    
     <div class="row left15">
    	<div class="col-sm-4 col-md-4">
			<label class="radio">
				<input type="radio" name="rdReqNoSourceSchool" id="rdReqNoSourceSchool2" value="2">
				Select Requisition Number(s)
			</label>
		</div>
    </div>
	<!--  -->	
	<div class="row" id="reqNumberForSchoolDiv">		 
		   <div class="col-sm-4 col-md-4  left15" style="border: 0px solid red;">
		 		<label>Available Requisition #</label>
				<select multiple id="avlbListSchool" name="avlbListSchool" class="form-control" style="height: 150px;" > 
					<c:if test="${not empty avlbList}">
				        <c:forEach var="avlst" items="${avlbList}" varStatus="status">
							<option value="${avlst.districtRequisitionId}">${avlst.requisitionNumber}</option>
						</c:forEach>
				    </c:if>
				</select>
			</div>		
			<div class="col-sm-1 col-md-1 left5-sm"> 
				<div class="span2"> <span id="addPopSchool" style="cursor: pointer;"><img alt="" src="images/rightarrow.jpg" width="30px"> </span></div>
				<div class="span2"> <span id="removePopSchool" style="cursor: pointer;"><img alt="" src="images/leftarrow.jpg" width="30px"> </span></div>
			</div>
			<div class="col-sm-4 col-md-4" style="border: 0px solid green;margin-left: -5px;">
				<label>Attached Requisition #</label>	
				<select multiple class="form-control" id="attachedListSchool" name="attachedListSchool" style="height: 150px;">
				</select>
				<%-- <div class="span2"> <a href="javascript:void(0);" onclick="der()" id="addPop">der() </a></div>--%>
				<input type="hidden" id="editOption"/>
			</div>
	</div>	
	
	
		<div class="col-sm-4 col-md-4 add-employment2 left30">
			<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1 || entityType==2 || entityType==3}">
			<a  href="javascript:void(0);" onclick="validateAddSchool();"><spring:message code="lnkImD"/></a>
			</c:if>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="cancelSchool();">Cancel</a>
		</div>  
</div>
<div class="hide" id ="loadingSchoolRowDiv"><img src="images/loadingAnimation.gif" style="margin-left: 45%;"></div>
<div class="row col-sm-11 col-md-11 hide left10"  id="showAndHideSchoolDiv">
	<div id="showSchoolDiv"></div>
	<div id="showTempSchoolDiv" class="hide"></div>
	<div id=schoolRecords></div>
</div>

</div>

	<div class="row col-sm-10 col-md-10 mt10">
		Which Job Specific Inventory is needed?
	</div>
 	<div class="row col-sm-10 col-md-10">
      	<label class="radio">
       <input type="radio" name="isJobAssessment" onclick="isJobAssessment(2);" id="isJobAssessment1" value="2" ${(jobOrder.isJobAssessment == false || PageFlag==0)? 'checked' : ''}>
       No job specific inventory is needed
		</label>
     </div>

	<div>
	    <div class="row col-sm-10 col-md-10" style="margin-top: -10px;">
			   <label class="radio">
		     <input type="hidden" name="isJobAssessmentVal" id="isJobAssessmentVal" value="${jobOrder.isJobAssessment == false || PageFlag==0 ? '2':'1'}"/>  
		     <input type="radio" name="isJobAssessment" onclick="isJobAssessment(1);" id="isJobAssessment2" value="1"  ${(jobOrder.isJobAssessment == true) ? 'checked' : ''}>
		       	 This job requires a job specific inventory to be taken by applicants
		        </label>
        </div>
	     <div  id="isJobAssessmentDiv">
	     	
	     	 <input type="hidden" name="attachJobAssessmentVal" id="attachJobAssessmentVal" value="${attachJobAssessmentVal}"/>
	     	 <input type="hidden" name="districtRootPath" id="districtRootPath" value="${districtRootPath}"/>
	     	 <input type="hidden" name="schoolRootPath" id="schoolRootPath" value="${schoolRootPath}"/>  
	     	 <input type="hidden" name="DAssessmentUploadURL" id="DAssessmentUploadURL" value="${DAssessmentUploadURL}"/>
	   		
	   		 <div class="row col-sm-10 col-md-10 hide left15" id="hideDAUploadURL" >
		      <label class="radio" style="margin-top: 0px; margin-bottom: 5px;">
		       <input type="radio" name="attachJobAssessment" onclick="attachJobAssessment(1);" id="attachJobAssessment1" value="1"  ${(jobOrder.attachDefaultDistrictPillar == '1'|| PageFlag==0) ? 'checked' : ''}>
		       Use the default job specific inventory set at the District&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="hide" id="ViewDAURL"></span>
		     </label>
		     </div>
		     
		     <div class="row col-sm-10 col-md-10 hide left15" id="hideSAUploadURL">
		      <label class="radio" style="margin-top: 0px; margin-bottom: 5px;">
		       <input type="radio" name="attachJobAssessment" onclick="attachJobAssessment(3);" id="attachJobAssessment2" value="3" ${jobOrder.attachDefaultSchoolPillar == '1' ? 'checked' : ''}>
		      Use the default job specific inventory set at the School&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="hide" id="ViewSAURL"></span>
		     </label>
		     </div>
		    
		    <c:set var="ishidejcjsidiv" value="hide"></c:set>
		    <c:if test="${jobOrder.attachDefaultJobSpecificPillar==1}">
		    	<c:set var="ishidejcjsidiv" value="show"></c:set><%-- Gagan : show means No need to hide in case of Edit Mode --%>
		    </c:if>
		    
		     <div class="row col-sm-10 col-md-10 hide left15 ${ishidejcjsidiv}" id="jcjsidiv">
		      <label class="radio" style="margin-top: 0px; margin-bottom: 5px;">
		       <input type="radio" name="attachJobAssessment" onclick="attachJobAssessment(4);" id="attachJobAssessment4" value="4" ${jobOrder.attachDefaultJobSpecificPillar == '1' ? 'checked' : ''}>
		      Use the default job specific inventory set at the Job Category&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="${ishidejcjsidiv}" id="ViewJSIURL"></span>
		     </label>
		     </div>
		    <iframe src="" id="ifrmJsi" width="100%" height="480px" style="display: none;">
				</iframe> 
				
			<div style="clear: both;">	
			<div id="jsiDiv" class="left30">
				    <div class="span11" style="margin-bottom: 5px;">
				      <label class="radio">
				       <input type="radio" name="attachJobAssessment" onclick="attachJobAssessment(2);" id="attachJobAssessment3" value="2" ${jobOrder.attachNewPillar == '1' || attachJobAssessmentVal==2 ? 'checked' : ''}>
				       Let me attach a job specific inventory
				     </label>
			  		</div>
			  		<div class="hide" id="attachJobAssessmentDiv">
					<div class="row col-sm-10 col-md-10">
					Acceptable Inventory formats include PDF, MS-Word, GIF, PNG, and JPEG  files.
					Maximum file size 10MB
					<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
					</iframe>
					</div>
					<form id='frmJobAssessmentUpload' enctype='multipart/form-data' method='post' target='uploadFrame' action='jobResumeUploadServlet.do' class="form-inline">
					 <input type="hidden" id="dateTime" name="dateTime"  value="${dateTime}"/>
					 <input type="hidden" id="JobOrderType" name="JobOrderType"  value="${JobOrderType}"/>
						<c:if test="${JobOrderType==3}">
					      <input type="hidden" id="districtSchoolHiddenId" name="districtSchoolHiddenId" value="${SchoolId}"/>
				 	  	</c:if>
		              	<c:if test="${JobOrderType==2}">
					       <input type="hidden" id="districtSchoolHiddenId" name="districtSchoolHiddenId" value="${DistrictOrSchoolId}"/>
				 	 	</c:if>
				 	<div class="row">
						<div class="col-sm-5 col-md-5">
						          <c:choose>
							    <c:when test="${addJobFlag==true && entityType==3}">
							      <input name="assessmentDocument" id="assessmentDocument" type="file" >
							    </c:when>
							   
							    <c:otherwise>
							       <c:if test="${fn:indexOf(roleAccess,'|9|')!=-1 || entityType==2}">
							         <input name="assessmentDocument" id="assessmentDocument" type="file" >
							     </c:if>
							    </c:otherwise>
						     	</c:choose>
						</div>
						<div class=" hide" id="divAssessmentDocument" name="divAssessmentDocument">
							<label>
								&nbsp;
							</label>
							<div class="col-sm-2 col-md-2">
								<c:if test="${JobOrderType==2}">
									<a data-original-title='JSI' rel='tooltip' id='JSIdistrict1' href='javascript:void(0)' onclick="showFile('district','${DistrictOrSchoolId}','${jobOrder.assessmentDocument}','JSIdistrict1');${windowFunc}"><c:out value="${(jobOrder.assessmentDocument==''||jobOrder.assessmentDocument==null)?'':'view'}"/></a>
									<input name="assessmentDocumentFileName" id="assessmentDocumentFileName" type="hidden" value="${jobOrder.assessmentDocument}">
								</c:if>
								<c:if test="${JobOrderType==3}">
									<a data-original-title='JSI' rel='tooltip' id='JSIschool1' href='javascript:void(0)' onclick="showFile('school','${SchoolId}','${jobOrder.assessmentDocument}','JSIschool1');${windowFunc}"><c:out value="${(jobOrder.assessmentDocument==''||jobOrder.assessmentDocument==null)?'':'view'}"/></a>
									<input name="assessmentDocumentFileName" id="assessmentDocumentFileName" type="hidden" value="${jobOrder.assessmentDocument}">
								</c:if>
							</div>
						</div>
					</div>				
					</form>
					</div>
					
					<div class="row hide1 JSIRadionDiv" id="JSIRadionDiv" style="margin-bottom: 5px; margin-top: 10px;">
						<div class="col-sm-10 col-md-10 checkbox inline top1">
							<span class="col-sm-4 col-md-4" id="epicheck2Span">
								<input type="radio" name="offerJSIRadio" id="offerJSIRadioMaindatory" ${jobOrder.jobAssessmentStatus == 0 || jobOrder.jobAssessmentStatus == 1 || jobOrder.jobAssessmentStatus == null ? 'checked' : ''}>
								<label><strong>JSI is mandatory</strong></label>
							</span>
							<span class="col-sm-4 col-md-4" id="epicheck2Span">
								<input type="radio" name="offerJSIRadio" id="offerJSIRadioOptional" ${jobOrder.jobAssessmentStatus == 2 ? 'checked' : ''}>
								<label><strong>JSI is optional</strong></label>
							</span>
						 </div>	
					</div>
			</div>	
	     </div> 
     </div>
     <c:if test="${JobOrderType==2}">
     <div class="row col-sm-10 col-md-10 top8 hide">
		Read / Write privilege
	 </div>
     
     <c:set var="isprivilegeChecked" value=""></c:set>
     <c:choose>
	    <c:when test="${jobOrder.writePrivilegeToSchool ne null}">
	   <%-- GAgan:  jobOrderis null means (Edit mode DJO from DA )--%>
	    	<c:if test="${jobOrder.writePrivilegeToSchool eq true}">
	    		 <c:set var="isprivilegeChecked" value="checked='checked'"></c:set>
	    	</c:if> 
	    </c:when>
	    <c:otherwise> <%-- Gagan: jobOrderis null means (Add DJO from DA )! If DA is going to add new DJO then writeprivilege flag will show acording to district set up flag --%>
	    	<c:if test="${writePrivilegfordistrict eq true}">
	    		 <c:set var="isprivilegeChecked" value="checked='checked'"></c:set>
	    	</c:if> 
	    </c:otherwise>              
	</c:choose>

	     <div class="row top15 left7 hide" >
	        <div class="col-sm-10 col-md-10">
	              <label class="checkbox inline" style="padding-left: 0px;">
					<input type="checkbox" id="writePrivilegeToSchool" name="writePrivilegeToSchool" value="" ${isprivilegeChecked}  />
	             	District and attached school(s) have read/write privilege  <a href="#" id="iconpophover6" rel="tooltip" data-original-title="District and attached School with this Job Order have read/write privileges and other School(s) of the District have read privileges."><img width="15" height="15" alt="" src="images/qua-icon.png"></a>
				</label>
			</div>
		</div>
	
     	</c:if>
   
 <c:if test="${JobPostingURL!=null}">
	<div class="row">
	<div class="col-sm-10 col-md-10 mt10">
	TeacherMatch Job Posting URL:<br>
	<a target="blank" href="${JobPostingURL}">${JobPostingURL}</a>
	</div>
	</div>
</c:if>


<div class="row hide" id="interviewPanel">
	 <div class="col-sm-10 col-md-10 mt10 checkbox inline" style="margin-left: -3px;">
		Panel
	</div>
	<div class="col-sm-10 col-md-10" style="margin-top: -10px;">
	   <div id="secStatusList"></div>
	</div>
</div>


<div class="mt10 hide" id="offerVVIForinternalDiv">
			<div class="row">
				<div class="col-sm-5 col-md-5" id="">
							<input type="checkbox" name="offerVirtualVideoInterview" id="offerVirtualVideoInterview"  ${offerVVIFlag} onclick="showOfferVVI();">
							<label>Offer Virtual Video Interview</label>
				</div>
			</div>
			<div class=" left15" id="offerVVIDiv" >
				<div class="row">
					<div class="col-sm-6 col-md-6" id="">
						<label id="captionDistrictOrSchool">Please select a question set</label>
	            		<input type="hidden" id="quesId" name="quesId" value="${jobOrder.i4QuestionSets.ID}"> 
	             		<input type="text" id="quesName" name="districtName"  class="form-control"
	             		onfocus="getQuestionSetAutoComp(this, event, 'divTxtShowData1', 'quesName','quesId','');"
						onkeyup="getQuestionSetAutoComp(this, event, 'divTxtShowData1', 'quesName','quesId','');"
						onblur="hideDistrictMasterDivForVVI(this,'quesId','divTxtShowData1');"	
						value="${jobOrder.i4QuestionSets.questionSetText}"/>
						<div id='divTxtShowData1' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData1','quesName')" class='result' ></div>
					</div>
				</div>
				<div class="row mt10">
					<div class="col-sm-10 col-md-10 " >
								<input type="checkbox" name="wantScore" id="wantScore" ${wantScoreVVIFlag} onclick="showMarksDiv();"/>
								<label>Want to score</label>
					</div>
				</div>
				<div class="hide row left15" id="marksDiv">
					<div class="col-sm-2 col-md-2" id="">
						<label>Max Marks</label>
						<input type="text" id="maxScoreForVVI" name="maxScoreForVVI" class="form-control" maxlength="3" value="${jobOrder.maxScoreForVVI}" onkeypress="return checkForInt(event)" >
					</div>
				</div>
				<div class="row mt10">
					<div class="col-sm-10 col-md-10 " >
								<input type="checkbox" name="sendAutoVVILink" id="sendAutoVVILink" ${sendAutoVVILinkFlag} onclick="showLinkDiv();">
								<label>Want to send the interview link automatically</label>
								
					</div>
				</div>
				<div class="hide row" id="autolinkDiv">
					<div class="col-sm-11 col-md-11" >
								<div class="col-sm-5 col-md-5 top5" style="max-width:430px;">
									<label>Link will be sent automatically once admin sets</label>
								</div>
								<div class="col-sm-3 col-md-3 top25-sm23"" style="margin-left:-85px;">
									<div id="statusDiv"></div>
								</div>
								<div class="col-sm-3 col-md-3">
									<label>&nbsp;status against the applicant.</label>
								</div>
					</div>
				</div>
				<div class="row mt10">
					<div class="col-sm-3 col-md-3" id="">
						<label>Time Allowed Per Question In Seconds</label>
						<input type="text" class="form-control" maxlength="4" id="timePerQues" value="${jobOrder.timeAllowedPerQuestion}" onkeypress="return checkForInt(event)" />
					</div>
					<div class="col-sm-3 col-md-3" id="">
						<label>Video Video interview Expires In Days</label>
						<input type="text" class="form-control" maxlength="2" id="vviExpDays" value="${jobOrder.VVIExpiresInDays}" onkeypress="return checkForInt(event)" />
					</div>
				</div>
			</div>
		</div>


<div class="hide">
					<div class="row">
						<div class="col-sm-5 col-md-5" id="">
									<input type="checkbox" name="offerAssessmentInviteOnly" id="offerAssessmentInviteOnly" onclick="showOfferAMT();">
			           				<span style="display: inline-block;">Offer Assessment to candidates on invite only</span>
						</div>
					</div>
					<div class="left15 hide" id="multiDisAdminDiv">
							   	<div class="row left5">
										<div class="col-sm-5 col-md-5" style="border: 0px solid red;">
											<label>Select District Assessment</label>
										</div>
									</div>
									<div class="row left5">
										 <div class="col-sm-5 col-md-5" style="border: 0px solid red;">
											<div id="1stSchoolDiv">
												<select multiple id="lstDistrictAdmins" name="lstDistrictAdmins" class="form-control" style="height: 150px;" > 
												</select>
											</div>
										</div>		
										<div class="col-sm-1 col-md-1 left20-sm"> 
											<div class="span2"> <span id="addPop1" style="cursor:pointer;"><img alt="" src="images/rightarrow.jpg" width="30px"> </span></div>
											<div class="span2"> <span id="removePop1" style="cursor:pointer;"><img alt="" src="images/leftarrow.jpg" width="30px"> </span></div>
										</div>
										<div class="col-md-5 col-md-5" style="border: 0px solid green;">
											<div id="2ndSchoolDiv">
												<select multiple class="form-control" id="attachedDAList" name="attachedDAList" style="height: 150px;">
												</select>
											</div>
										</div>	
									</div>
					</div>
			    </div>

      <div class="mt10">
			<div class="row">
				<div class="col-sm-5 col-md-5" id="">
							<input type="checkbox" name="isInviteOnly" id="isInviteOnly"  onclick="unableOrDisableInviteJob();" ${IsInviteOnlyFlag} >
							<label>Hidden job</label>
				</div>
			</div>
			
			 <div class="row left5 hide hiddenInviteDiv">
	            <div class="col-sm-4 col-md-4">
			         <label class="radio">
                       <input type="radio" name="hiddenJob" id="hiddenJob1" value="0" ${hiddenJobRadio1Checked}> This job is invite-only.
                     </label>
                 </div>
                 <div class="col-sm-8 col-md-8">
                    <label class="radio">
                      <input type="radio" name="hiddenJob" id="hiddenJob2" value="1" ${hiddenJobRadio2Checked}> This job is hidden job but applicants with job URL can apply.
                   </label>
                 </div>
	         </div>
			
			<div class="row ${notIficationFlag}">
                <div class="col-sm-5 col-md-5" id="">
                            <input type="checkbox" name="isSendNotification" id="isSendNotification" ${notIficationToschool}>
                            <label>Do not send notification to attached schools</label>
                </div>
            </div>
	</div>

<div class="hide" id="showExitURLDiv">
<div class="row">
 <div class="col-sm-10 col-md-10 mt10">
	Re-direct URL${DistrictExitURLMessageVal}
</div>

<div class="col-sm-11 col-md-11" style="margin-top: -5px;" >
<input type="hidden" name="exitURLMessageVal" id="exitURLMessageVal" value="${DistrictExitURLMessageVal}"/> 
<label  class="radio"><input type="radio" name="exitUrlMessage" onclick="exitUrlMessage(1);" id="exitUrlMessage1" value="1" checked> After completion  of TM Base Inventory, the applicant should be directed to <a href="#" id="iconpophover4" rel="tooltip" data-original-title="URL address where job applicant can complete the remaining requirements for this job post completion of TeacherMatch base EPI"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></label>
<input type="text" id="exitURL" maxlength="250" name="exitURL" value="${DistrictExitURL}" class="form-control"/>
</div>
</div>
</div>


<div class="hide" id="showExitMessageDiv">
<div class="row">
<div class="col-sm-10 col-md-10  mt10"  id="eMessage">
<label  class="radio">
<input type="radio" name="exitUrlMessage" onclick="exitUrlMessage(2);" id="exitUrlMessage2" value="2" >
TM Inventory is the last step in job application and after completion, the candidate should be shown <a href="#" id="iconpophover5" rel="tooltip" data-original-title="Message that should be displayed to the job applicant after completion of the TeacherMatch base EPI "><img width="15" height="15" alt="" src="images/qua-icon.png"></a><br> 
Completion Message</label>
<textarea id="exitMessage" name="exitMessage"  maxlength="1000" onkeyup="return chkLenOfTextArea(this,1000);" onblur="return chkLenOfTextArea(this,1000);" class="span8" rows="0">${DistrictExitMessage}</textarea>
</div>
</div>
</div>

</div>

<!--  <div class="row onlyjeffco">
	<div class="col-sm-10 col-md-10 mt10">
		<label>
			Attached Additional Document
		</label>
		<input type="file" id="additionalDocument" name="additionalDocument"  class="" />
	</div>
	<div class="col-sm-4 col-md-4">
		<a href="javascript:void(0);" onclick="#"><spring:message code="lnkImD"/></a>
			&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="javascript:void(0)" onclick="clearadditionalDocument();">Clear</a>
	</div>
</div>
-->
<div class="row top5 mt5 hide">
			<div class="col-sm-4 col-md-4" style="height: 35px;">
				<label>Upload Additional Document</label>
				<input type="hidden" id="uploadedAdditionalDocumentFile" name="uploadedAdditionalDocumentFile"/>
				<form id='additionalDocumentDocUpload' enctype='multipart/form-data' method='Post' target='uploadFrame' action='FileUploadAdditionDocument.do' class="form-inline">
					<input type="hidden" id="disHiddenId1" name="disHiddenId1" value="${DistrictOrSchoolId}"/>
					<input  type="file" name="additionalDocumentDoc" id="additionalDocumentDoc" onchange="uploadAdditionalDocumentFile(this.value);" style="float:left;">  				
				</form>
			</div>
			<div class="col-sm-2 col-md-2" style="padding-top: 21px;">			
			<c:if test="${not empty jobOrder.additionalDocumentPath}">
					<a href="#" class="iconpophover16" rel="tooltip" data-original-title="Click here to view Additional Document" onmouseover="$(this).tooltip();">
						<span id="hrefAdditionalDocumentDesc" onclick="viewAdditionalDocument('${jobOrder.jobId}');if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;">View</span>
					</a>	
				</c:if>	
			</div>
			<div class="col-sm-6 col-md-6" style='padding-top: 21px;'><label class='divErrorMsg' id='additionalDocumentErrorLabel'  style="float:right;"></label> </div>
			
</div>
<div class="row mt15 onlyjeffco">
	<div class="col-sm-4 col-md-4">
		<label>
			Primary Employment Services Technician
		</label><span class="required">*</span>
		<input type="hidden" name="primaryESTechnicianHidden"  value="${primaryESTechnicianHidden}"/> 
		<select name="primaryESTechnician" id="primaryESTechnician" class="form-control" onchange="setBackupESTechnician(this);"></select>
	</div>
	<div class="col-sm-4 col-md-4">
		<label>
			Backup Employment Services Technician
		</label>		
		<select name="backupESTechnician" id="backupESTechnician" class="form-control" disabled="disabled"></select>
	</div>
</div>
<div class="row onlyjeffco">
	<div class="col-sm-4 col-md-4 hide">
		<label>
			Salary Range
		</label><span class="required">*</span>
		<input type="text" name="salaryRange" id="salaryRange"	class="form-control" maxlength="300" value="${salaryRange}" />
	</div>
</div>

<c:if test="${userMaster.districtId.districtId!=806900}">
<div class="row linkApprovalDetails">
	<div class="col-sm-4 col-md-4" id="showApprovalGroupDetailsId" style="padding-top: 13px;">
		<a href="javascript:void(0)" onclick="showApprovalGroupDetails(${jobOrder.jobId})"> Details of Approval Groups</a>
	</div>
</div>
</c:if>
<c:if test="${userMaster.districtId.districtId==806900}">
<div class="row linkApprovalDetails">
	<div class="col-sm-4 col-md-4" id="showApprovalGroupDetailsId" style="padding-top: 13px;">
		<a href="javascript:void(0)" onclick="showApprovalInfoDetailsForAdams(${jobOrder.jobId})"> Details of Approval Groups</a>
	</div>
</div>
</c:if>



<div class="row top5">
   	<div class="col-sm-10 col-md-10 mt10 hide" id="hideSave">
   		<c:if test="${addJobFlag ||(addJobFlag && fn:indexOf(roleAccess,'|8|')!=-1 && miamiShowFlag || entityType==2)}">
   		
      	<button onclick="validateApprovalGroupDetails(1);" class="btn btn-large btn-primary">Save Job Order <i class="icon"></i></button>
      	
      	&nbsp;&nbsp;&nbsp;&nbsp; </c:if>
      	
      	<a href="javascript:void(0)" onclick="cancelJobOrder();removeJobDescriptionFile(2);">Cancel</a>
      </div>
</div>




	
			
			<iframe src="" id="iframeJSI" width="100%" height="480px" style="display: none;">
 			</iframe>
<div style="display:none;" id="loadingDiv">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>

<div  class="modal hide"  id="EPIMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span id="EPIMsg"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Ok</button> 		
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  id="textMessageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3>Teachermatch</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span id="textMessage"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Ok</button> 		
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  id="textDSChangeMessageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3>Teachermatch</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span id="textDSMessage"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Ok</button>    	
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  id="textDSChangeMessageModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3>Teachermatch</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span id="textDSMessage2"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="getRequisiontForJobOrder();">Ok</button> 		
 	</div>
</div>
</div>
</div>
<div  class="modal hide"  id="textJsiDate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3>Teachermatch</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span id="textJsiDateMsg"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<!-- <button class="btn" data-dismiss="modal" aria-hidden="true" onclick="getRequisiontForJobOrder();">Ok</button> -->
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button> 		
 	</div>
</div>
</div>
</div>


<div  class="modal hide"  id="duplicateReqModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
				<h3>Teachermatch</h3>
			</div>
			<div class="modal-body">	
				<div class="control-group">
					<div class="">
				    	<span id="textDuplicateReqSpan"></span>	        	
					</div>
				</div>
		 	</div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="#">Ok</button> 		
		 	</div>
		</div>
	</div>
</div>

<div  class="modal hide"  id="finalValidateReqModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
				<h3>Teachermatch</h3>
			</div>
			<div class="modal-body">	
				<div class="control-group">
					<div class="">
				    	<span id="textfinalValidateSpan"></span>	        	
					</div>
				</div>
		 	</div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="#">Ok</button> 		
		 	</div>
		</div>
	</div>
</div>
<div  class="modal hide distAttachment"  id="distAttachment"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameEEOC11">Attachment</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divPnrAttachment">
					 
					</div>
					<iframe src="" id="ifrmAttachment" width="100%" height="480px"> </iframe>   
				</div>
				
		 	</div>
		 	<input type="hidden" id="eligibilityMasterId" value=""/>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideAttachmentDiv();">Close</button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<input type="hidden" id="jobIdNC" name="jobIdNC" value="${jobOrder.jobId}" />
<input type="hidden" id="ncStatusId" value="${NCHEADQUARTERId}">
<!--<input type="hidden" value="N" id="jeffcoGrantFlag"/>
--><script>
//${jobOrder.jobId}
var sval ="${jobOrder.specialEdFlag}";
//alert("gval:- "+gval+", sval:- "+sval);
//$("").val()
$(document).ready(function() {
                
                if(sval==null | sval=="")
                                sval="N";
                                
               //alert("gval:- "+gval+", sval:- "+sval);
  //  $("#jeffcoGrantFlag option[value='"+gval+"']").prop('selected', true);
  //$("#jeffcoGrantFlag").val(gval);
    $("#jeffcoSpecialEdFlag option[value='"+sval+"']").prop('selected', true);
    // you need to specify id of combo to set right combo, if more than one combo
});
</script>
	<div id='d_clip_container'><div id='d_clip_button'></div></div>
	
	<c:if test="${empty DistrictOrSchoolId}">
		<script>$('#districtORSchoolName').focus();</script>
	</c:if>
	<c:if test="${not empty DistrictOrSchoolId}">
		<script>$('#jobTitle').focus();</script>
	</c:if>
	
	<input type="hidden" id="tempSchoolId" name="tempSchoolId" value="">
	<input type="hidden" id="tempAllSchoolId" name="tempAllSchoolId" value="">
	
		<script>
			init_swf();
		</script>	
		
		<c:if test="${PageFlag!=0 || DistrictExitURLMessageVal!=null }">
        	<script>
        		if(document.getElementById("exitURLMessageVal").value==2){
					document.getElementById("exitURLMessageVal").value=2;
					document.getElementById("exitUrlMessage2").checked=true;
					document.getElementById("showExitURLDiv").style.display='none';
					document.getElementById("showExitMessageDiv").style.display='inline';
					
				}else{
					document.getElementById("exitURLMessageVal").value=1;
					document.getElementById("exitUrlMessage1").checked=true;
					document.getElementById("showExitURLDiv").style.display='inline';
					document.getElementById("showExitMessageDiv").style.display='none';
				}
			</script>	           
        </c:if>
        
        
        <c:if test="${jobOrder.isJobAssessment == false  || PageFlag==0}">
        	<script>
					document.getElementById("isJobAssessmentDiv").style.display='none';
			</script>	
        </c:if>
        <c:if test="${jobOrder.isJobAssessment == true }">
        	<script>
        			isJobAssessment(1);
					document.getElementById("isJobAssessmentDiv").style.display='inline';
			</script>	
        </c:if>
        <c:if test="${PageFlag==0 && DistrictExitURLMessageVal==null}">
        	<script>
        			document.getElementById("exitURLMessageVal").value=1;
					document.getElementById("exitUrlMessage1").checked=true;
					document.getElementById("showExitURLDiv").style.display='inline';
					document.getElementById("showExitMessageDiv").style.display='none';
			</script>	
        </c:if>
        <c:if test="${jobOrder.selectedSchoolsInDistrict == '1'}">
     		 <script>
     		 	document.getElementById("addSchoolLink").style.display='inline';
     		 	document.getElementById("showAndHideSchoolDiv").style.display='inline';
     		 </script>	
		</c:if>
		<c:if test="${DAssessmentUploadURL!=''  && DAssessmentUploadURL!=null}">
		 <script>
			document.getElementById("hideDAUploadURL").style.display='inline';
			document.getElementById("ViewDAURL").style.display='inline';
			document.getElementById("ViewDAURL").innerHTML="<a data-original-title='JSI' rel='tooltip' id='JSIdistrict2' href='javascript:void(0)' onclick=\"showFile('district','${districtHiddenId}','${DAssessmentUploadURL}','JSIdistrict2');${windowFunc}\" href='#'><c:out value="${(DAssessmentUploadURL==''||DAssessmentUploadURL==null)?'':'view'}"/></a>";
		 </script>		
		</c:if>
		<c:if test="${SAssessmentUploadURL!='' && SAssessmentUploadURL!=null}">
		 <script>
			document.getElementById("hideSAUploadURL").style.display='inline';
			document.getElementById("ViewSAURL").style.display='inline';
			document.getElementById("ViewSAURL").innerHTML="<a data-original-title='JSI' rel='tooltip' id='JSIschool2' href='javascript:void(0)' onclick=\"showFile('school','${SchoolId}','${SAssessmentUploadURL}','JSIschool2');${windowFunc}\" href='#'><c:out value="${(SAssessmentUploadURL==''||SAssessmentUploadURL==null)?'':'view'}"/></a>";
			 </script>		
		</c:if>
		<c:if test="${jobOrder.attachNewPillar == '1'}">
		 <script>
			document.getElementById("attachJobAssessmentDiv").style.display='inline';
			document.getElementById("divAssessmentDocument").style.display='inline';
			
		</script>
		</c:if>
		<c:if test="${ attachJobAssessmentVal=='2'}">
		 <script>
			document.getElementById("attachJobAssessmentDiv").style.display='inline';
		</script>				
		</c:if>
		<c:if test="${jobOrder.attachNewPillar != '1' && attachJobAssessmentVal!='2'}">
		 <script>
			document.getElementById("attachJobAssessmentDiv").style.display='none';
			document.getElementById("divAssessmentDocument").style.display='none';
		 </script>		
		</c:if>
		<script type="text/javascript">
		checkValue(${allSchoolGradeDistrictVal});
		displayJobRecords();
		DisplayCertification();
		ShowSchool();
		//displayRequisationNumber();
	</script>
	<script type="text/javascript">
	$('#iconpophover1').tooltip();
	$('#iconpophover11').tooltip();
	$('#iconpophover2').tooltip();
	$('#iconpophover5').tooltip();
	$('#iconpophover4').tooltip();
	$('#iconpophover6').tooltip();
	$('#iconpophover7').tooltip();
	$('#iconpophoverReqEg').tooltip();
	$('#iconpophoverReqEgSchool').tooltip();
	$('#idFilter').tooltip();
	
	</script>
<script type="text/javascript">
	//	getZoneNameCheck(0);
				$('#addPop').click(function() {
				if ($('#avlbList option:selected').val() != null) {
					 $('#avlbList option:selected').remove().appendTo('#attachedList');
					 $("#avlbList").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
			         $("#attachedList").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
			       // removeFromAvailableForSchoolItemList();
			 } else {
			   document.getElementById("textMessage").innerHTML="Please select any position from Available Requisition # before add.";
			   $("#textMessageModal").modal("show");
			 }
			});
			
			$('#removePop').click(function() {
			       if ($('#attachedList option:selected').val() != null) {
			       		 //MultiSelect();	
			       		 //addToAvailableForSchoolItemList();
			             $('#attachedList option:selected').remove().appendTo('#avlbList');
			             sortSelectList();
			             $("#attachedList").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
			             $("#avlbList").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
			             $("#avlbList").attr('selectedIndex', '-1').addAttr("selected");
			             
			} else {
			   document.getElementById("textMessage").innerHTML="Please select any position from Attached Requisition # before remove.";
			    $("#textMessageModal").modal("show");
			}
			});
			
			function removeFromAvailableForSchoolItemList(){
				var optsDA = document.getElementById('attachedList').options;
				var optsSA = document.getElementById('avlbListSchool').options;
				var tempList = document.getElementById('tempList');
				for(var x = 0;x<optsDA.length;x++){
					for(var i = 0;i<optsSA.length;i++){
						if(optsSA[i].value==optsDA[x].value){
							var x1 = new Option(optsDA[x].text,optsDA[x].value,false,false)
							tempList.options.add(x1);
							optsSA.remove(i);
						}
					}		
			    }
		   }	
		   
		   
		   var arList=new Array();
		   function MultiSelect(){
		   		var optsAttachedDA = document.getElementById('attachedList').options;
		   		var c=0;
				for(var x=0;x<optsAttachedDA.length;x++){
					if(optsAttachedDA[x].selected){
						arList[c++]=optsAttachedDA[x];
					}
				}
		   }
		   function addToAvailableForSchoolItemList(){
		   		var optsAttachedDA = document.getElementById('attachedList').options;
				var optsDA = document.getElementById('avlbList');
				var optsSA = document.getElementById('avlbListSchool');
				var tempList = document.getElementById('tempList').options;
				var count = $('#attachedList option:selected').length;
				if(count>1){
					for(var x = 0;x<arList.length;x++){
						//var x1 = new Option(tempList[arList[x]].text,tempList[arList[x]].value,false,false)
						var x1=arList[x];
						var x2=arList[x];
						optsSA.options.add(x1);
						optsDA.options.add(x2);
					}
					var cnt=0;
					//$('#tempList option').remove();
					//for(var x = 0;x<arList.length;x++){
						//tempList.removeChiled(arList[cnt++]);
					//}
				}else{
					if(optsAttachedDA.length>0){
					for(var x = 0;x<optsAttachedDA.length;x++){
						if(optsAttachedDA[x].selected){
							var x1 = new Option(tempList[x].text,tempList[x].value,false,false)
								optsSA.options.add(x1);
								tempList.remove(x);
							}
						}
					}
			   }
		   }
		   
		   	
		  function sortSelectList(){
		  	var selectListAvlbList = $('#avlbList option');
		  	var selectListavlbListSchool = $('#avlbListSchool option');
			selectListAvlbList.sort(function(a,b){
			    a = a.value;
			    b = b.value;
			    return a-b;
			});
			$('#avlbList').html(selectListAvlbList);
			selectListavlbListSchool.sort(function(a,b){
			    a = a.value;
			    b = b.value;
			    return a-b;
			});
			$('#avlbListSchool').html(selectListavlbListSchool);
		  }
		  
			
		/***********************************************************************************************************************************/
			$('#addPopSchool').click(function() {
			    if ($('#avlbListSchool option:selected').val() != null) {
			         $('#avlbListSchool option:selected').remove().appendTo('#attachedListSchool');
			         $("#avlbListSchool").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
			         $("#attachedListSchool").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
			        // removeFromAvailableForDistrictItemList();
			 } else {
			    document.getElementById("textMessage").innerHTML="Please select any position from Available Requisition # before add.";
			    $("#textMessageModal").modal("show");
			 }
			});
		
			$('#removePopSchool').click(function() {
			       if ($('#attachedListSchool option:selected').val() != null) {
			       		 //MultiSelectSCH();	
			       		 //addToAvailableForDistrictItemList();	
			             $('#attachedListSchool option:selected').remove().appendTo('#avlbListSchool');
			             sortSelectList();
			             $("#attachedListSchool").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
			             $("#avlbListSchool").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
			             $("#avlbListSchool").attr('selectedIndex', '-1').addAttr("selected");
			             
			} else {
			   //alert("Please select any position from Attached Requisition # before remove.");
			   document.getElementById("textMessage").innerHTML="Please select any position from Attached Requisition # before remove.";
			    $("#textMessageModal").modal("show");
			}
			});
			
			function removeFromAvailableForDistrictItemList(){
				var optsDATCH = document.getElementById('attachedListSchool').options;
				var optsDAAVL = document.getElementById('avlbList').options;
				var tempList = document.getElementById('tempList');
				for(var x = 0;x<optsDAAVL.length;x++){
					//alert(x)
					//alert("1>>> "+optsDAAVL[i].value +" x="+x);
					for(var i = 0;i<optsDATCH.length;i++){
						//alert("2>>> "+optsDATCH[x].value+"  i="+i);
						if(optsDAAVL[x].value==optsDATCH[i].value){
							var x1 = new Option(optsDATCH[x].text,optsDATCH[x].value,false,false)
							tempList.options.add(x1);
							optsDAAVL.remove(i);
						}
					}		
			    }
		   }	
		   
		   var arSCHList=new Array();
		   function MultiSelectSCH(){
		   		var optsAttachedDA = document.getElementById('attachedListSchool').options;
		   		var c=0;
				for(var x=0;x<optsAttachedDA.length;x++){
					if(optsAttachedDA[x].selected){
						arSCHList[c++]=optsAttachedDA[x];
					}
				}
		   }
		   function addToAvailableForDistrictItemList(){
		   		var optsAttachedSA = document.getElementById('attachedListSchool').options;
				var optsDA = document.getElementById('avlbList');
				var optsSA = document.getElementById('avlbListSchool');
				var tempList = document.getElementById('tempList').options;
				var count = $('#attachedListSchool option:selected').length;
				if(count>1){
					for(var x = 0;x<arSCHList.length;x++){
						//var x1 = new Option(tempList[arList[x]].text,tempList[arList[x]].value,false,false)
						var x1=arSCHList[x];
						var x2=arSCHList[x];
						optsDA.options.add(x1);
						optsSA.options.add(x2);
					}
					var cnt=0;
					//$('#tempList option').remove();
					//for(var x = 0;x<arSCHList.length;x++){
						//tempList.remove(arList[cnt++]);
					//}
				}else{
					if(optsAttachedSA.length>0){
					var optsAttachedSA = document.getElementById('attachedListSchool').options;
					for(var x = 0;x<optsAttachedSA.length;x++){
						if(optsAttachedSA[x].selected){
							var x1 = new Option(tempList[x].text,tempList[x].value,false,false)
								optsDA.options.add(x1);
								tempList.remove(x);
							}
						}
					}
			   }
		   }
		  
		/*****************************************************************************************************************************************/	
		</script> 
		
	
	
   <script type="text/javascript">//<![CDATA[

	document.getElementById("hideSave").style.display='inline';
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
       var cal1 = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("jobEndDate", "jobEndDate", "%m-%d-%Y");
      cal1.manageFields("positionStartDate", "positionStartDate", "%m-%d-%Y");
     cal1.manageFields("positionEndDate", "positionEndDate", "%m-%d-%Y");
    //]]>
    
     function jobStartDateDiv(){
          cal.manageFields("jobStartDate", "jobStartDate", "%m-%d-%Y");
     }
  	$(document).ready(function(){
	  		$('#eMessage').find(".jqte").width(850);
	  		//$('#jDescription').find(".jqte").width(850);
	  		//$('#jQualification').find(".jqte").width(850);
	}); 
			
     </script>
<script  type="text/javascript" language="javascript" src="js/dialogboxforaction.js"></script>
<script>
if($("#jobId").val()!="" && $("#jobCatId").val()!=""){
	setStatusList($("#jobId").val(),$("#districtOrSchooHiddenlId").val(),$("#jobCatId").val());
}
if($("#clonejobId").val()!="" && $("#jobCatId").val()!=""){
	setStatusList($("#clonejobId").val(),$("#districtOrSchooHiddenlId").val(),$("#jobCatId").val());
}
disableJobCategory();
$(document).ready(function(){
  $('#noOfExpHires').bind("paste",function(e) {
      e.preventDefault();
  });
 $('#noOfSchoolExpHires').bind("paste",function(e) {
     e.preventDefault();
 });
});

getStatusListForVII();
showOfferVVI();
showMarksDiv();
showLinkDiv();

deleteTempReq();
setDistrictReqFlag();
chkTimeAndDayForVVI();
showOfferAMT();
chkJobSubCategory();


$( "#rdReqNoSource2" ).prop( "checked", true );
</script>

<input type="hidden" id="districtReqFlag" name="districtReqFlag" value="0"/>

<input type="hidden" id="newReqCountFlag" name="newReqCountFlag" value="0"/>
<input type="hidden" id="newReqDuplicateFlag" name="newReqDuplicateFlag" value="0"/>

<script type="text/javascript">
	$('#addPop1').click(function() {
	//	opts = document.getElementById('attachedDAList').options
	//	if(opts.length==0 && $('#lstDistrictAdmins option:selected').length==1)//Only one assessment can attach
	//	{
			if ($('#lstDistrictAdmins option:selected').val() != null) {
				 $('#lstDistrictAdmins option:selected').remove().appendTo('#attachedDAList');
				 $("#attachedDAList").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
		         $("#attachedDAList").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
		 	}
	//	}
		
	});
	
	$('#removePop1').click(function() {
	       if ($('#attachedDAList option:selected').val() != null) {
	             $('#attachedDAList option:selected').remove().appendTo('#lstDistrictAdmins');
	           //  sortSelectList();
	             $("#attachedDAList").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').addAttr("selected");
	             
	}
	});
	displayAllDistrictAssessment();
	$(document).ready(function(){
		$('.onlyjeffco').hide();
		var entityType=$('#entityType').val();
		var districtId=$('#districtOrSchooHiddenlId').val();
		if((entityType==3||entityType==2 || entityType==1) && districtId=='804800'){
			callForPrimaryESTech();
		}
	})	;
</script>

<script>
loadSubCategoryDiv();
function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#virusDivId').modal('show');
}
function loadSubCategoryDiv()
{
	var chkCase = document.getElementById("chkCase").value;
	
	if(chkCase=='addcase')
		$("#jobSubCateDiv").hide();
	else
		getJobSubcategory();
	
}
showHideVisibilityOFTemp();
jobShowDiscriptionOnchng();
</script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showDSPQDiv();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="info322" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<c:if test="${IsInviteOnlyFlag eq 'checked'}">
		 <script>
			$('.hiddenInviteDiv').show();
		</script>				
</c:if>
 <script>
if(isCloneJob){
  showTempCloneSchool("${jobOrder.jobId}");
}

	
</script>
