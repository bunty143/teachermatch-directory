<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="javax.swing.Spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />
<script>
var $j=jQuery.noConflict();
        $j(document).ready(function() {
       });
</script>
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/group01.js?ver=${resouceMap['js/jobapplicationflow/group01.js']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jafInventory.js?ver=${resouceMap['js/jobapplicationflow/jafInventory.js']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jobapplflowcommon.js?ver=${resouceMap['js/jobapplicationflow/jobapplflowcommon.js']}"></script>

<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />

<style>
.table th, .table td {
border-top: 0px solid #cccccc;
font-weight: normal;
line-height: 20px;
padding: 7px 4px;
text-align: left;
vertical-align: top;
font-family: 'Century Gothic','Open Sans', sans-serif;
font-size: 11px;
}
p {
margin: 0 0 10px;
}
</style>

<p id="portfolioHeader"></p>
<input type="hidden" id="sNextURL" name="sNextURL" value="${sNextURL}"/>
<input type="hidden" id="sPreviousURL" name="sPreviousURL" value="${sPreviousURL}"/>

<input type="hidden" id="assessmentId" value="${assessmentDetail.assessmentId}">
<input type="hidden" id="teacherAssessmentId" value="${teacherAssessmentdetail.teacherAssessmentId}">
<input type="hidden" id="teacherAssessmentType" value="${teacherAssessmentdetail.assessmentType}">
<input type="hidden" id="jobId" value="${jobOrder.jobId}">
<input type="hidden" id="attemptId" value="${attemptId}">
<input type="hidden" id="totalAttempted" value="${totalAttempted}">
<input type="hidden" id="newJobId" value="${newJobId}">
<input type="hidden" id="epiStandAlone" value="${teacherDetail.isPortfolioNeeded}"/>
<input type="hidden" id="rURL" value="${rURL}"/>

<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:30px;">
         <div style="float: left;">
         	<img src="images/affidavit.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="DSPQlblEPIInner"/></div>	
         </div>		
         
         <div style="float: right;" id="topRightInvDivIdInner">
          	<div style='text-align:right; padding-right: 0px;' class="col-sm-3 col-md-3">
          		<button id="toprightbtnInvInner" class='flatbtn' style='width:100px;'  type='button' onclick='return getAssessmentSectionCampaignQuestionsGrid(1);'>
          			<spring:message code="btnNextDSPQ"/> <i class='icon'></i>
          		</button>
          		
          	</div>
         </div>
         
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<!-- ------------------------------------------------------ -->
<!-- ------------------------------ Start  EPI ------------ -->
<!-- ------------------------------------------------------ -->
<jsp:include page="jafInventoryStep2Common.jsp"></jsp:include>
<!-- ------------------------------------------------------ -->
<!-- ------------------ End EPI --------------------------- -->
<!-- ------------------------------------------------------ -->

										
<p id="epiFooter"></p>

<!-- start ... Attach jaf jsp files -->
	<jsp:include page="jafinnercommon.jsp"></jsp:include>
<!-- end ... Attach jaf jsp files -->

<style>
.notClk
{
	pointer-events:none;
}
</style>

<script>

var dspqPortfolioNameId=${dspqPortfolioName};
var candidateType="${candidateType}";
var groupId=${group};
var iJobId=${iJobId};

getPortfolioHeader(iJobId,dspqPortfolioNameId,candidateType,groupId);
checkAssessmentDone(${teacherAssessmentdetail.teacherAssessmentId});
//getEPIData(iJobId,dspqPortfolioNameId,candidateType,groupId);
//getEPIFooter(iJobId,dspqPortfolioNameId,candidateType,groupId);

$( document ).ready(function() {
$(".stepDSPQ1").addClass("notClk");
$(".topmenu").addClass("notClk");
$(".pull-right").addClass("notClk");
$(".tmlogo").addClass("notClk");
$(".unClickInvenFooter").addClass("notClk");

$(".notClk").click(function() {
  alert("You can not click on this during EPI(Educator's Professional Inventory).");
  return false;
});

});
</script>