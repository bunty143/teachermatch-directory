<%@ page language="java" import="java.util.*,tm.services.district.GlobalServices" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
response.setHeader("pragma", "no-cache");
response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
response.setHeader("Expires", "0");
 %>
<style>
.justify{
    text-justify: inter-word;
    text-align: left;
    text-align: right;
    text-align:center;
    text-align: justify;
}
.content_agreement{
	width:103.4%;
	min-height:50px;
	max-height: 235px;
	overflow: auto;
}
.heading_certification{
	text-align: center;
	padding-bottom: 4px;
	font-family:Times New Roman;
	font-weight:bold;
	font-size:16px;
}
</style>
<script type="text/javascript">
$(document).ready(function() { 
  /* Get browser */
    $.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());
    $.browser.msie = /msie/.test(navigator.userAgent.toLowerCase()); 
	$.browser.mozilla = /firefox/.test(navigator.userAgent.toLowerCase()); 
	$.browser.safari = /safari/.test(navigator.userAgent.toLowerCase()); 

    /* Detect Chrome */
    if($.browser.chrome){
        /* Do something for Chrome at this point */
        //alert("You are using Chrome!");
        location.reload();
        /* Finally, if it is Chrome then jQuery thinks it's 
           Safari so we have to tell it isn't */
    }
    /* Detect Safari */
    if($.browser.safari){
			        window.location.reload() 
        /* Do something for Safari */
        //alert("You are using Safari!");
    }

window.history.forward();
changeHashOnLoad();
});
</script>
<script type = "text/javascript" >
function changeHashOnLoad() {
     window.location.href += "#";
     setTimeout("changeHashAgain()", "50"); 
}

function changeHashAgain() {
  window.location.href += "1";
}

var storedHash = window.location.hash;
window.setInterval(function () {
    if (window.location.hash != storedHash) {
         window.location.hash = storedHash;
    }
}, 1);
</script>
 <script type="text/javascript">
gg = function(e) {  
var keycode =(window.event) ? event.keyCode : e.keyCode; 
   switch (keycode) {  
        case 116 : //F5 button
            e.returnValue = false;
            e.keyCode = 0;
            return false; 
        case 82 : //R button
            if (e.ctrlKey) { 
                e.returnValue = false; 
                //event.keyCode = 0;
                e.keyCode = 0;  
                return false; 
            } 
    }
}
document.onkeydown = gg;

function confirm(){
	var resultText=$.ajax({
		url: "ncaffidavitconfirm.do",
		data : {'jobId':$('[name="jobId"]').val()},
		type : 'POST',
        async: false,
        beforeSend: function() {$('#loadingDiv').show()},
    	complete: function() {$('#loadingDiv').hide()},
        success:function(data){	$.get('jobsofinterest.do');
        }
	}).responseText;
	if(resultText!='exception')
	 document.location.href=resultText;
	 else
	 alert(resultText);
	//$.get('jobsofinterest.do');
}




        $(document).ready(function() {
        callContinue();
         $(document).on("contextmenu",function(e){
	         if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA")
	             e.preventDefault();
     	 });
     	 $('[name="acceptornot"]').on("contextmenu",function(e){
	             e.preventDefault();
     	 });
        /*var myEvent = window.attachEvent || window.addEventListener;
            var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload'; /// make IE7, IE8 compatable
            myEvent(chkevent, function(e) { // For >=IE7, Chrome, Firefox
                var confirmationMessage = 'Warning: If you close this page then your appling job marked as a imcomplete. If you want to complete this job then click on "Stay on this page" button and confirm affidavit Applicant\'s Certification and click on "Continue" button.';  // a space
                (e || window.event).returnValue = confirmationMessage;
                return confirmationMessage;
          });*/
		});
</script>

<script>
function callContinue(){
//alert($('[name="acceptornot"]').is(':checked'));
	if($('[name="jobId"]').val()!=''){
	if($('[name="acceptornot"]').is(':checked')){
	$('#continueButton').show();
	}else{
		$('#continueButton').hide();
	}
	}
}
</script>


<input type="hidden" name="jobId" value="${jobId}" />
<c:if test="${empty jobId}">
<div style="height: 380px;"></div>
</c:if>
<c:if test="${not empty jobId}">
<div id="loadingDiv"> <!-- style="display:none; z-index: 5000;"  -->
  	<table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>
<div class="row" >
         <div style="float: left;">
         		<img src="images/affidavit.png" width="41" height="40" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Affidavit</div>	
         </div>		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<div class="row heading_certification">
		    <br>
		   ${headingCertification}
</div>
<div class="row content_agreement justify">
	<div style="width: 99%;">${affidavit_content}</div>
</div>
<div class="row offset4">
		<label class="checkbox" ><input type="checkbox" value="1" name="acceptornot" onclick="callContinue();" /><spring:message code="pConfirm" /></label>
		<div style="text-align: left;position: fixed;">
				<button class="btn btn-primary hide" id="continueButton" type="button" onclick="confirm();"><strong><spring:message code="btnConti" /> <i class="icon"></i></strong></button>
				<a href="jobsofinterest.do">Cancel</a>
		</div>
</div>
<br/>
</c:if>

