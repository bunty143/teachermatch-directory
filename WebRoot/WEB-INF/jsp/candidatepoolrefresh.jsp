<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/PortfolioReminderAjax.js?ver=${resourceMap['PortfolioReminderAjax.ajax']}"></script>
<script type='text/javascript' src="js/portfolioreminder.js?ver=${resourceMap['js/portfolioreminder.js']}"></script>

<style>
	#content_del{
		table-layout:fixed; 
		overflow:auto;
		word-break: break-all;
	}
</style>

<div class="row">
         <div style="float: left;">
         	<img src="images/add-district.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="msgCondidatePoolRefresh" /></div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div class="mt10" align="justify">
	<form id="districtMaster" action="candidatepoolrefresh.do" method="post" enctype="multipart/form-data" onsubmit="return false;">
	<input type="hidden" id="districtId" name="districtId" value=""/>

	<div class="row">
		<div class="span12 top5" >  
		<label class="checkbox" style="margin-left: 15px;">
			<input type="checkbox" id="candidatepoolrefresh" name="candidatepoolrefresh" onclick="candidatePoolRefreshClick(this);"></input>
			<span style="display: inline-block;"><spring:message code="msgActCandpoolrefresh"/></span>
		</label>
		</div>
	</div>

	<div class="row" style="margin-top:20px;">
		<div class="" >  
		<label class="checkbox" style="margin-left: 15px;">
			<input type="checkbox" id="sendReminderToPortCand" name="sendReminderToPortCand"></input>
			<span style="display: inline-block;"><spring:message code="msgSendReminderToPortCand"/></span>
		</label>
		</div>
	</div>
		
	<div class="row">
		<div class="col-sm-6 col-md-6 top5">
			<div id="errNoOfReminderPortfolio"  class="required" style="display:none;">
			<spring:message code="msgErrNoOfReminderPortfolio"/></div>
			<spring:message code="msgNoOfReminderPortfolio"/>
		</div>
		<div class="col-sm-3 col-md-3 top5">
			
			<input type="text"  id="noOfReminderPortfolio" name="noOfReminderPortfolio" class="form-control" maxlength="3" placeholder="" onkeypress="return checkForInt(event);" />
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-md-6 top5">
			<div id="errReminderFrequencyInDaysPortfolio"  class="required" style="display:none;">
			<spring:message code="msgErrReminderFrequencyInDaysPortfolio"/></div>
			<spring:message code="msgNoOfDaysRemain" /> 
		</div>
		<div class="col-sm-3 col-md-3 top5">
			
			<input type="text" id="reminderFrequencyInDaysPortfolio" name="reminderFrequencyInDaysPortfolio" class="form-control" maxlength="3" placeholder="" onkeypress="return checkForInt(event);"/>
		</div>
		<div class="col-sm-2 col-md-2 top25-sm23" style="padding-top: 10px;"><spring:message code="lblDays" /></div>
	</div>			 	 
	<div class="row">
		<div class="col-sm-6 col-md-6 top5">
			<div id="errReminderOfFirstFrequencyInDaysPortfolio"  class="required" style="display:none;">
			<spring:message code="msgErrReminderOfFirstFrequencyInDaysPortfolio"/></div>
			<spring:message code="msgDaysBtwReminder" />
		</div>
						
		<div class="col-sm-3 col-md-3 top5">
			
		<input  type="text" id="reminderOfFirstFrequencyInDaysPortfolio" name="reminderOfFirstFrequencyInDaysPortfolio" class="form-control" maxlength="3" placeholder="" onkeypress="return checkForInt(event);"/>
		</div>
		<div class="col-sm-2 col-md-2 top25-sm23" style="padding-top: 10px;"><spring:message code="lblDays" /></div>
	</div>
					
	<div class="row"> 
		<div class="col-sm-6 col-md-6 top5">
			<div id="errStatusIdReminderExpireActionPortfolio"  class="required" style="display:none;">
			<spring:message code="msgErrStatusIdReminderExpireActionPortfolio"/></div>
			<spring:message code="msgStatusIdReminderExpireActionPortfolio"/>		
		</div>
		<div class="col-sm-3 col-md-3 top5">
			
			<select class="form-control" id="statusIdReminderExpireActionPortfolio" name="statusIdReminderExpireActionPortfolio">
				<option value="0"><spring:message code="msgNoactiontaken" /> </option>
				<c:forEach items="${lstStatusMaster_PFC1}" var="lstsmForReminder">
					<c:set var="selected" value=""></c:set>	
					<c:if test="${statusIdReminderExpireActionPortfolio ne null and statusIdReminderExpireActionPortfolio ne ''}">					
						<c:if test="${lstsmForReminder.statusId eq statusIdReminderExpireActionPortfolio}">
							<c:set var="selected" value="selected"></c:set>
						</c:if>
					</c:if>
					<option value="${lstsmForReminder.statusId}" ${selected} >${lstsmForReminder.status}</option>
				</c:forEach>
			</select>
		</div>
	</div>
					
	<div class="row">
		<div class="col-sm-6 col-md-6 top5">
			<div id="errStatusSendMailPortfolio" class="required" style="display:none;">
			<spring:message code="msgErrStatusSendMailPortfolio"/></div>
			<spring:message code="msgStatusSendMailPortfolio"/>
		</div>
		<div class="col-sm-3 col-md-3 top5">			
			<select class="form-control" id="statusSendMailPortfolio" name="statusSendMailPortfolio" multiple>
				<option value=""><spring:message code="msgNoactiontaken" /> </option>
				<c:forEach items="${lstStatusMaster_PFC}" var="lstsmForReminder">
					<c:set var="selected" value=""></c:set>
					<c:if test="${statusListPortfolio ne null}">						
						<c:forEach items="${statusListPortfolio}" var="statusListPortfolio1">
							<c:if test="${lstsmForReminder.statusId eq statusListPortfolio1}">
							<c:set var="selected" value="selected"></c:set>
							</c:if>
						</c:forEach>
					</c:if>
					<option value="${lstsmForReminder.statusId}" ${selected} >${lstsmForReminder.status}</option>
				</c:forEach>
				<c:forEach items="${lstsecondaryStatus_PFC}" var="lstSecSMForReminder">
					<c:set var="selected" value=""></c:set>
					<c:if test="${secondaryStatusListPortfolio ne null}">						
						<c:forEach items="${secondaryStatusListPortfolio}" var="secondaryStatusListPortfolio1">
							<c:if test="${lstSecSMForReminder.secondaryStatusId eq secondaryStatusListPortfolio1}">
							<c:set var="selected" value="selected"></c:set>
							</c:if>
						</c:forEach>
					</c:if>
					<option value="SSIDForReminder_${lstSecSMForReminder.secondaryStatusId}" ${selected} >${lstSecSMForReminder.secondaryStatusName}</option>
				</c:forEach>
			</select>
		</div>
	</div>
		<div class="row">
			<input type="button" class="btn btn-large btn-primary" value="Update District" onclick="savecandidatepoolrefresh();"/>
		</div>
	</form>
</div>
<div  class="modal hide"  id="candidatepoolrefreshstatusbox"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 400px;">
	<div class="modal-content">		
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hide_box();">x</button>
		<h3 id="myModalLabel"><span class="left7"></span>TeacherMatch</h3>
	</div>	
	<div class="modal-body">		
		<div class="control-group">
			<form>
				<table id="content_del" cellspacing="0" cellpadding="1">
					<tr><td>
						<div id="status"></div>
					</td></tr>
				</table>
			<form>
		</div>
 	</div>
 	<div class="modal-footer">
 		<input type="button" onclick="hide_box();" value="Close" class="form-class"/>	
 	</div>
	</div>
	</div>
</div>
<script>
if("${districtId}" != ""){
	document.getElementById("districtId").value = "${districtId}";
}
if("${noOfReminderPortfolio}" != ""){
	document.getElementById("noOfReminderPortfolio").value="${noOfReminderPortfolio}";
}
if("${reminderOfFirstFrequencyInDaysPortfolio}" != ""){
	document.getElementById("reminderOfFirstFrequencyInDaysPortfolio").value="${reminderOfFirstFrequencyInDaysPortfolio}";
}
if("${reminderFrequencyInDaysPortfolio}" != ""){
	document.getElementById("reminderFrequencyInDaysPortfolio").value="${reminderFrequencyInDaysPortfolio}";
}
if( "${candidatePoolRefresh}" == "true"){
	document.getElementById("candidatepoolrefresh").checked = true;
}else{
	 option_visibility(true);
}
if("${sendReminderForPortfolio}" == "true"){
	document.getElementById("sendReminderToPortCand").checked = true;
}
function candidatePoolRefreshClick(element){
		option_visibility(!element.checked);
}

function hide_box(){
	try { $('#candidatepoolrefreshstatusbox').modal('hide'); } catch (e) {}
}
function savecandidatepoolrefresh(){
	var candidatepoolrefresh = document.getElementById("candidatepoolrefresh").checked;
		var sendReminderToPortCand = document.getElementById("sendReminderToPortCand").checked;
		var noOfReminderPortfolio = document.getElementById("noOfReminderPortfolio").value;
		var reminderFrequencyInDaysPortfolio = document.getElementById("reminderFrequencyInDaysPortfolio").value;
		var reminderOfFirstFrequencyInDaysPortfolio = document.getElementById("reminderOfFirstFrequencyInDaysPortfolio").value;

		var e1 = document.getElementById("statusIdReminderExpireActionPortfolio");
		var statusIdReminderExpireActionPortfolio = e1.options[e1.selectedIndex].value;

		var e2 = document.getElementById("statusSendMailPortfolio");
		var statusSendMailPortfolio = getSelectValues(e2);
		var districtId = document.getElementById("districtId").value;
		var data = [];
		data[0] = sendReminderToPortCand;
		data[1] = noOfReminderPortfolio;
		data[2] = reminderFrequencyInDaysPortfolio;
		data[3] = reminderOfFirstFrequencyInDaysPortfolio;
		data[4] = statusIdReminderExpireActionPortfolio;
		data[5] = districtId;
		data[6] = candidatepoolrefresh;		
		$('#errNoOfReminderPortfolio').hide();
		$('#errReminderFrequencyInDaysPortfolio').hide();
		$('#errReminderOfFirstFrequencyInDaysPortfolio').hide();
		$('#errStatusIdReminderExpireActionPortfolio').hide();
		$('#errStatusSendMailPortfolio').hide();
		if(noOfReminderPortfolio != "" && reminderFrequencyInDaysPortfolio != "" && reminderOfFirstFrequencyInDaysPortfolio !="" && statusIdReminderExpireActionPortfolio != "" && statusSendMailPortfolio[0] != "No action taken" && districtId != ""){
			PortfolioReminderAjax.saveCandidatePoolRefreshPool(data,statusSendMailPortfolio,{ 
				async: false,
				callback: function(data){
					document.getElementById("status").innerHTML = data;
					try { $('#candidatepoolrefreshstatusbox').modal('show'); } catch (e) {}
				},
				errorHandler:handleError 
			});
		}else if(noOfReminderPortfolio == ""){
			$('#errNoOfReminderPortfolio').show();
			$('#noOfReminderPortfolio').focus();
			return false;
		}else if(reminderFrequencyInDaysPortfolio == "" ){
			$('#errReminderFrequencyInDaysPortfolio').show();
			$('#reminderFrequencyInDaysPortfolio').focus();
			return false;
		}else if(reminderOfFirstFrequencyInDaysPortfolio == "" ){
			$('#errReminderOfFirstFrequencyInDaysPortfolio').show();
			$('#reminderOfFirstFrequencyInDaysPortfolio').focus();
			return false;
		}else if(statusIdReminderExpireActionPortfolio == "" ){
			$('#errStatusIdReminderExpireActionPortfolio').show();
			$('#statusIdReminderExpireActionPortfolio').focus();
			return false;
		}else if(statusSendMailPortfolio[0].trim() == 'No action taken' ){
			//console.log(statusSendMailPortfolio);
			$('#errStatusSendMailPortfolio').show();
			$('#statusSendMailPortfolio').focus();
			return false;
		}
}
function option_visibility(hidden){
		document.getElementById("sendReminderToPortCand").disabled = hidden;
		document.getElementById("noOfReminderPortfolio").disabled = hidden;
		document.getElementById("reminderFrequencyInDaysPortfolio").disabled = hidden;
		document.getElementById("reminderOfFirstFrequencyInDaysPortfolio").disabled = hidden;
		document.getElementById("statusIdReminderExpireActionPortfolio").disabled = hidden;
		document.getElementById("statusSendMailPortfolio").disabled = hidden;
}
function getSelectValues(select) {
  var result = [];
  var options = select && select.options;
  var opt;
  for (var i=0, iLen=options.length; i<iLen; i++) {
    opt = options[i];
    if (opt.selected) {
      result.push(opt.value || opt.text);
    }
  }
  return result;
}
</script>