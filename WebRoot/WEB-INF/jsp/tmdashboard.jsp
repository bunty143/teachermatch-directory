<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/UserLastVisitDemoAjax.js?ver=${resourceMap['UserLastVisitDemoAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CommonDashboardAjax.js?ver=${resouceMap['CommonDashboardAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateGridAjax.js?ver=${resouceMap['CandidateGridAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateGridSubAjax.js?ver=${resouceMap['CandidateGridSubAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherProfileViewInDivAjax.js?ver=${resouceMap['TeacherProfileViewInDivAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateReportAjax.js?ver=${resouceMap['CandidateReportAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PFCertifications.js?ver=${resouceMap['PFCertifications.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/myschedule.js?ver=${resourceMap['js/myschedule.js']}"></script>
<script type='text/javascript' src="js/commondashboard.js?ver=${resourceMap['js/commondashboard.js']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resouceMap['BatchJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherInfotAjax.js?ver=${resouceMap['TeacherInfotAjax.ajax']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script type='text/javascript' src="js/certtypeautocomplete.js?ver=${resourceMap['js/certtypeautocomplete.js']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resouceMap['DWRAutoComplete.ajax']}"></script>

<script type="text/javascript" src="dwr/interface/SelfServiceCandidateProfileService.js?ver=${resourceMap['SelfServiceCandidateProfileService.ajax']}"></script>
<script type='text/javascript' src="js/report/sspfCandidategridnew.js?ver=${resourceMap['js/report/sspfCandidategridnew.js']}"></script>
<script type="text/javascript" src="dwr/interface/CGServiceAjax.js?ver=${resourceMap['CGServiceAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax .ajax']}"></script>

<style>
a {
  color: #007AB4;
  text-decoration: none;  
  font-size: 11px; /* it is uncommented for tm dashboard text*/
}
div#wrapper {
	margin:0 auto;
	width:986px;
	background:#FFF;
	height: 1200px;
}
div#feed {
	//width:230px;
	//padding:10px;
	float:left;
}
div#sideBar {
	width:684px;
	//padding:10px;
	margin-left:10px;
	float:left;
	//border: 1px solid red;
}
.clear { 
	clear:both; 
}
div#sticker {
	padding:10px;
	//margin:10px 0;
	//background:#AAA;
	width:684px;
}
.stick {
	position:fixed;
	top:0px;
}

<!-- ---------------------------- -->

.items{
margin-top: 0px;
}
.dcard{
//height: 125px;
//border: solid #F8F8F8 1px;
border: 1px solid #dddddd;
//border-color:#cccccc!important;
width: 285px;
margin-bottom: 7px;
//background-color: #F2FAEF;
//background-color: #F8F8F8;
background-color: #F0F8FF;
//background-color: #F0F0F0;
}

#scrolling {
    position: absolute;
    top: 110px;
    right: 20px;
    border: 1px solid black;
    //background: #eee;
}
#scrolling.fixed {
    position: fixed;
    top: 0px;
}
.circle1 {
  display: block;
  width: 60px;
  height: 60px;
  margin: 1em auto;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  -webkit-border-radius: 99em;
  -moz-border-radius: 99em;
  border-radius: 99em;
  border: 1px solid #eee;
  box-shadow: 0 3px 2px rgba(0, 0, 0, 0.3);  
  background-color: pink;
}

.test_content{}
.scroller_anchor{}
.scroller{}

.circle_img{
 -moz-border-radius:52px;
 -webkit-border-radius:52px;
 border-radius:52px;
width:60px;
height:60px;

}
.tool{}
.profile{}
.icon-circle{
font-size: 6em;
color:pink;
}
  .popover-title {
  margin: 0;
  padding: 0px 0px 0px 0px;
  font-size: 0px;
  line-height: 0px;
  }
  .popover-content {
   margin: 0;
   padding: 0px 0px 0px 0px;
  }
.popover.right .arrow {
  top: 100px;
  left: -10px;
  margin-top: -10px;
  border-width: 10px 10px 10px 0;
  border-right-color: #ffffff;
}
.popover.right .arrow:after {
  border-width: 11px 11px 11px 0;
  border-right-color: rgba(0, 0, 0, 0.25);
  bottom: -11px;
  left: -1px;
}

.popover.bottom {
  margin-left: 10px;
}
.popover.bottom .arrow {
  top: -10px;
  left: 15%;
  margin-left: -10px;
  border-width: 0 10px 10px;
  border-bottom-color: #007AB4;
}
.popover.bottom .arrow:after {
  border-width: 0 11px 11px;
  border-bottom-color: rgba(0, 0, 0, 0.25);
  top: -1px;
  left: -11px;
}
.modal_header_profile {

  border-bottom: 1px solid #eee;
  background-color: #0078b4;
  text-overflow: ellipsis; 
  border: 1px solid #999;
  border: 1px solid rgba(0, 0, 0, 0.3);
  *border: 1px solid #999;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
  outline: none;
  -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
     -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  -webkit-background-clip: padding-box;
     -moz-background-clip: padding-box;
          background-clip: padding-box;
}

.net-widget-footer 
{
	border-bottom: 1px solid #cccccc; 
	border-left: 1px solid #cccccc;
	border-right: 1px solid #cccccc;
	line-height:40px; 
	background-color: #F2FAEF;
	background-image: -moz-linear-gradient(top,  #FFFFFF, #FFFFFF);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#FFFFFF), to(#FFFFFF));
	background-image: -webkit-linear-gradient(top,  #FFFFFF, #FFFFFF);
	background-image: -o-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: linear-gradient(to bottom,#FFFFFF, #FFFFFF);
	background-repeat: repeat-x;
	color:#4D4D4E;
	vertical-align: middle;
	
}
.net-corner-bottom 
{ 
	-moz-border-radius-bottomleft: 12px; -webkit-border-bottom-left-radius: 12px; border-bottom-left-radius: 12px; -moz-border-radius-bottomright: 12px; -webkit-border-bottom-right-radius: 12px; border-bottom-right-radius: 12px; 
}
.joborderwidth
{
	width: 990px;
	margin-left:-446px;
}


.scrollspy_profile { height:500px; overflow-y:auto; overflow-x:hidden;padding-left:5px;
}
.divwidth
{
	width:728px;
}
.tablewidth
{
width: 900px;
}
.custom-div-border1
{
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-moz-border-radius: 6px;
border-radius: 6px;
}
.custom-div-border
{
padding:1px;
border-bottom-left-radius:2em;
border-bottom-right-radius:2em;
}
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
.custom-border
{
margin-left: -32px;
margin-top: 2px;
}

 .fa-circle:before {
  content: "\f04d";
}
.iconcolor1{
color: #FF1919
}

.iconcolor2{
color: #FFF019
}
.iconcolor3{
color:#A4D53A
}

.toolTip {
    position: absolute;
    display: none;
    width: auto;
    height: auto;
    background: none repeat scroll 0 0 white;
    border: 0 none;
    border-radius: 8px 8px 8px 8px;
    box-shadow: -3px 3px 15px #888888;
    color: blue;
    font: 15px sans-serif;
    padding: 5px;
    text-align: center;
    
}
 .legend {
	            padding: 5px;
	            font: 10px sans-serif;
	            background: yellow;
	            box-shadow: 2px 2px 1px #888;
            }

</style>


<script>
function divhover()
{
$(document).ready(function(){
    
    $("div#profilebox").mouseover(function(){
        //$(this).css("background-color","#ccc");
        try{
        	$("#lyrics",this).show();
        }catch (e) {
        	var $re=jQuery.noConflict();
			$re("#lyrics",this).show();
		}
    }).mouseout(function(){
        //$(this).css("background-color","#eee");
        try{
        	$("#lyrics",this).hide();
        }catch (e) {
        	var $re1=jQuery.noConflict();
			$re1("#lyrics",this).hide();
		}
    });
    
});
}
var dddis=null;
function divClose(dis,removedEntityId,flg)
{
	dddis=dis;
	var sh="";
	if(flg==0)
		sh="teacher";
	else
		sh="job";
		
	$('#message2showConfirm').html("Do you want to remove this "+sh+" from the Action Feed? Removal of this "+sh+" from Action Feed can not be undone. ");
	$('#footerbtn').html("<button class='btn btn-primary' onclick=\"removeEntity("+removedEntityId+","+flg+")\" >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'><spring:message code="btnClr"/></button>");
	$('#myModal3').modal('show');
	
}

function removeEntity(removedEntityId,flg)
{
	CommonDashboardAjax.removeFromFeed(removedEntityId,flg,{ 
			async: true,
			callback: function(data){		
			$(dddis).closest('.dcard').remove();
			$('#myModal3').modal('hide');
			lastAddedLiveFunc();
	},
	errorHandler:handleError  
	});
}

function namehover(dis,teacherId,normscore)
{
	//$("#tea"+teacherId).show();
	CommonDashboardAjax.showCandidateDetails(teacherId,normscore,{ 
			async: true,
			callback: function(data){
			//$("#tea"+teacherId).hide();
			
			$(dis).closest('.profile').popover({ 
				 	trigger: "manual",
				    html : true,
				    placement: 'right',
				    content: data,
				  }).on("mouseenter", function () {
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 10);
				    });
			
	},
	errorHandler:handleError  
	});
} 

 function applyScrollOnLEACandidatePorfolio()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLEACandidatePorfolioGridD').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
       	<c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[200,100,200,150,100,80],
           </c:when>
           <c:otherwise>
             colratio:[200,100,150,150,80,50],
           </c:otherwise>
        </c:choose>
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}


 function applyScrollOnTblLicenseDate()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLicneseGridCertificationsGridD').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[400,220,210],
           </c:when>
           <c:otherwise>
             colratio:[350,220,160],
           </c:otherwise>
        </c:choose>
       	addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}

function applyScrollOnTblBackgroundCheck_profile()
{
        $(document).ready(function() {
        $('#tbleBgCheck_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
	    width: 830,
        minWidth: null,
        minWidthAuto: false,
	    colratio:[332,243,255],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}




function applyScrollOnTblLanguage()
{
    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#teacherLanguageGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         // table header width
        
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[277,276,276],
           </c:when>
           <c:otherwise>
             colratio:[244,243,243],
           </c:otherwise>
        </c:choose>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });         
}
function applyScrollOnHonors()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j("#honorsGrid").fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         // table header width
        
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[830],
           </c:when>
           <c:otherwise>
             colratio:[730],
           </c:otherwise>
        </c:choose>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });         
}
function applyScrollOnTbl_AssessmentDetails()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentDetails').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        width: 830,
	    minWidth: null,
        minWidthAuto: false,
        olratio:[230,200,200,200],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

function applyScrollOnHonors_br()
{
		var $j=jQuery.noConflict();
		$j(document).ready(function() {
        $j('#honorsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 830,
        minWidth: null,
        minWidthAuto: false,
        colratio:[830], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnAssessments()
{	
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentTable').fixheadertable({ 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        colratio:[180,120,150,185], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

// Start for Teacher profile Div
</script>

<c:set var="notShow" />
<c:if test="${userSession.entityType==1}">
<c:set var="notShow" value="display:none;"/>
<div class="row col-sm-12 col-md-12 modalTM" id='searchItem' style="margin-top: 15px;margin-left:0px;padding: 15px;min-width:800px;">	
	  <div class='divErrorMsg' id='errordiv' style="padding-left: 15px;"></div> 	  
        <div class="col-sm-5 col-md-5">
       
           <label><spring:message code="lblDistrictName"/></label>
         
          <span>
           <input type="text" id="districtName" autocomplete="off" maxlength="100"  name="districtName" class="help-inline form-control"
          		 onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
						onkeyup="getDistrictAuto(this,event,'divTxtShowData', 'districtName','districtId','');"
						onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
     	   </span>
      <input type="hidden" id="districtId" value="0"/>
      <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
     </div>
 
      <div class="col-sm-5 col-md-5">
        <label><spring:message code="lblSchoolName"/></label>        	
         	<input type="text" id="schoolName" autocomplete="off" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
						onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
						onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');"	/>
	    <input type="hidden" id="schoolId" value="0"/>
           	<input type="hidden" id="schoolName" value="" name="schoolName"/>
           
	 <div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
      </div>
      <div class="col-md-2">
        <label  style="float: right;margin-right:-10px;" id='closePan' class="">&nbsp;</label>
		<button class="btn btn-primary top25-sm" style="width: 100px;" type="button" onclick="searchData();"><spring:message code="btnSearch"/> <i class="icon"></i></button>         	
      </div>
   </div>  


   
   <div class="modalsa" style="display: none;" id="sa">

	<div class="" style="padding-top: 55px;">
	<a href='javascript:void(0);' onclick="getSearchPan()"><span class='icon-search icon-large iconcolor' style="font-size: 0.99em;"></span><b>&nbsp;<spring:message code="lnkSearchAg"/><img src="images/arrow_left_animated.gif"/></b></a>
	</div>
	</div>
</c:if>	




<div id="wrapper" style="margin-top: -15px;">
	<div id="feed" >
		<div class="subheadingnew" style="padding-top: 8px;padding-bottom:3px;border: solid red 0px;">
			<span style="cursor: pointer;"> 
				<span class='btn-group'><a data-toggle='dropdown' ><spring:message code="lnkActionFd"/><span class="carethead"></span></a></span>
			</span>
		</div>	
		<span class="items"></span>		
		<input type="hidden" id='loadingCounter' >
		<input type="hidden" id='jobLoadingCounter' >
		<input type="hidden" id='yellowJobLoadingCounter' >
		<input type="hidden" id='loadingFlag' value='1'>
		<div id="lastPostsLoader" style="text-align:center;width: 285px;"></div>    	
    </div>
    <div id="sideBar">    	
      <div id="sticker"> 
      <div class="row">          
		<div class="col-sm-6 col-md-6" style="width: 330px;">
		<!-- mukesh -->
			<div class="subheadingnew" style="padding-bottom:3px;border: solid red 0px;"><span style="cursor: pointer;"> 
			<span class='btn-group'><a data-toggle='dropdown' ><spring:message code="lnkJobOdrStatus"/><span class="carethead"></a>
				<!--<ul class='dropdown-menu  pull-right'  style=" margin-top: 0px;">
				    <li><a href="javascript:void(0);" onclick="getjoborderDetailsInAllJob()">All Job Orders</a></li>
				    <li><a href="javascript:void(0);" onclick="chartjobOrderByCertification()">By Certification</a></li>
				    <li><a href="javascript:void(0);" onclick="chartjobOrderByOpening()">By # of Opening</a></li>
				    <li><a href="javascript:void(0);" onclick="getjoborderDetailsBySchoolsDefault()">By Schools</a></li>
				 </ul>
			 --> </span>
			 </span>
			</div>	
			<div id='container1' class="table-bordered img-responsive" style="height: 250px;margin-left: 0px;">
			</div>
		</div>	
		
		<div class="col-sm-6 col-md-6" style="width: 330px;">
		  <div class="subheadingnew" style="padding-bottom:3px;border: solid red 0px;">
				<span style="cursor: pointer;"> 

					<span class='btn-group'><a data-toggle='dropdown' ><spring:message code="lnkPQuality"/><span class="carethead"></span></a>

					

					</span>
				</span>
				<c:choose>
				<c:when test="${userSession.entityType!=1}">
				<div class='pull-right' style="padding-bottom:3px;border: solid red 0px;">
						
						<c:if test="${userSession.entityType eq 2}">	
							<a href="managejoborders.do"  target="_blank" id="jobOrderIcon" rel="tooltip" data-original-title="<spring:message code="lblDistrictJobOrders"/>" data-placement="bottom">
								<span class='fa-suitcase iconcolor icon-large'></span> &nbsp;
							</a>
						</c:if>
						<c:if test="${userSession.entityType eq 3}">	
							<a href="managejoborders.do"  target="_blank" id="jobOrderIcon" rel="tooltip" data-original-title="<spring:message code="lblDistrictJobOrders"/>" data-placement="bottom">
								<span class='fa-suitcase iconcolor icon-large'></span> &nbsp;
							</a>
						</c:if>
						
							<c:choose>
						      <c:when test="${userVisitFlag}">
						      <a href="myschedule.do" target="_blank" onclick="updateUserLastVisit();" id="scheduleIcon" rel="tooltip" data-original-title="<spring:message code="headDemoSchedule"/>" data-placement="bootom">
							      	<span class='icon-calendar iconcolor icon-large' ></span>
						      	</a>
						      </c:when>
						      <c:otherwise>
						     	<a href="myschedule.do" target="_blank" id="scheduleIcon" rel="tooltip" data-original-title="<spring:message code="headDemoSchedule"/>" data-placement="bottom">
						     		<span class='icon-calendar icon-large' style="color:#2a2a2a;"></span>
						     	</a>   
						      </c:otherwise>
							</c:choose>
				</div>
				</c:when>
			    <c:otherwise>
			       
			    </c:otherwise>
				</c:choose>		
			  </div>			
			<div id='container3' class="table-bordered" style="height: 250px;margin-left: 0px;padding-bottom: 5px;"></div>
		</div>		
	  </div>
	  
	   <div class="row">  		
			<div class="col-sm-12 col-md-12" style="width: 660px;">
				<div class="subheadingnew" style="padding-top: 5px;padding-bottom:3px;border: solid red 0px;">
					<span style="cursor: pointer;"> 
					<span class='btn-group'><a data-toggle='dropdown' ><spring:message code="HiredCondState"/> <span class="carethead"></span></a></span>

					</span>
				</div>	
			    <div id='container4' class="table-bordered" style="height: 250px;margin-left: 0px;"></div>
			</div>			
	<%-- 		
			<div class="col-sm-6 col-md-6" style="width: 330px;">
				<div class="subheadingnew" style="padding-top: 5px;padding-bottom:3px;border: solid red 0px;">
					<span style="cursor: pointer;"> 
	

					<span class='btn-group'><a data-toggle='dropdown' >Hiring Velocity<span class="carethead"></span></a></span>

					</span>
				</div>	
				<div id='container2' class="table-bordered" style="height: 250px;margin-left: 0px;"></div>
			</div>	
			--%>	
	   </div> 
    <div class="clear"></div>
</div>
</div>
</div>


<div  class="modal hide"  id="modalDownloadsTranscript" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content" style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()">x</button>
		<h3 id="myModalLabel"><spring:message code="HeadTran"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="">
<iframe src="" id="ifrmTrans" width="100%" height="480px">
 </iframe>        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>
<!-- @Start
@Ashish :: draggable div for Mosaic Teacher Profile -->
<div class="modal hide" id="draggableDivMaster" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div id="draggableDiv"></div>
</div>
<!-- @End@Ashish :: draggable div for Mosaic Teacher Profile -->
<div  class="modal hide"  id="myModalCommunications"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 700px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel"><spring:message code="headCom"/></h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div class="" id="divCommTxt">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" aria-hidden="true" onclick='printCommunicationslogs();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>

<div class="modal hide"  id="myModalNotes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
	<input type="hidden" id="teacherId" name="teacherId" value="">
	<div class="modal-dialog" style="width:674px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='showCommunicationsDiv();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headNot"/></h3>
	</div>
	
	<div class="modal-body">	
		<div class="row" id="divNotes" style="padding-left: 15px; ">						
		</div>
		<iframe id='uploadNoteFrameID' name='uploadNoteFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='frmNoteUpload' enctype='multipart/form-data' method='post' target='uploadNoteFrame'  class="form-inline" onsubmit="return saveNotes();" action='noteUploadServlet.do' accept-charset="UTF-8">
			
		<div class="mt10">
			<div class='span10 divErrorMsg' id='errordivNotes' style="display: block;"></div>
			<div class="span10" >
		    	<label><strong><spring:message code="lblEtrNot"/><span class="required">*</span></strong></label>
		    	<div class="span10" id='divTxtNode' style="padding-left: 0px; margin-left: 0px; " >
		    		<textarea readonly id="txtNotes" name="txtNotes" class="span10" rows="4"   ></textarea>
		    	</div>  
		    	<input type="hidden" id="teacherIdForNote" name="teacherIdForNote" value="">
		    	<input type="hidden"  name="noteDateTime" id="noteDateTime" value="${dateTime}"/>
		    	<div id='fileNotes' style="padding-top:10px;">
	        		<a href='javascript:void(0);' onclick='addNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addNoteFileType();'><spring:message code="lnkAttachFile"/></a>
	        	</div>      	
			</div>						
		</div>
		</form>
 	</div>
 	 	
 	<div class="modal-footer">
 		<c:set var="chkSaveDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSaveDisp" value="none" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSaveDisp" value="inline" />
			</c:otherwise>
		</c:choose>
 		<!-- <span id="spnBtnCancel" style="display: ${chkSaveDisp}"><a href="#"	onclick="return cancelNotes()">Cancel</a></span> -->
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='showCommunicationsDiv();'><spring:message code="btnClr"/></button>&nbsp; 
 		<span id="spnBtnSave" style="display: ${chkSaveDisp}"><button class="btn btn-primary"  onclick="saveNotes()"><spring:message code="btnSave"/><i class="icon"></i></button>&nbsp;</span>
 	</div>
</div>
</div>
</div>
<!--Add message Div by  Sekhar  -->
<div  class="modal hide"  id="myMsgShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2show'><spring:message code="msgYuMsgIsfullySentToCandidate"/>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()"><spring:message code="btnOk"/>
</button>
 	</div>
</div>
</div>
</div>
<input type="hidden" id="teacherDetailId" name="teacherDetailId">
<div class="modal hide"  id="myModalMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:690px">
	<div class="modal-content">
	<input type="hidden" id="emailId" name="emailId">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headPostMsgToCandidate"/></h3>
	</div>
	<iframe id='uploadMessageFrameID' name='uploadMessageFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	<form id='frmMessageUpload' enctype='multipart/form-data' method='post' target='uploadMessageFrame'  class="form-inline" onsubmit="return validateMessage();" action='messageCGUploadServlet.do' accept-charset="UTF-8">
	
	<div  class="modal-body" style="max-height: 430px;overflow-y:auto;">
		<div class="" id="divMessages">						
		</div>
		<div class="control-group" style="margin-top: 5px;">
			<div class='divErrorMsg' id='errordivMessage' style="display: block;"></div>
		</div>
		<%---------- Gagan :District wise Dynamic Template [Start]  --%>
			<div id="templateDiv" style="display: none;">
			    <div class="row col-md-12" >
			    	<div class="row col-sm-6" style="max-width:300px;">
				    	<label><strong><spring:message code="lblTemplate"/></strong></label>
			        	<br/>
			        	<select class="form-control" id="districttemplate" onchange="setTemplate()">
			        		<option value="0"><spring:message code="sltTemplate"/></option>
			        	</select>
			        </div>	
			        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="btn btn-primary hide top26"  id="btnChangetemplatelink" onclick="return getTemplate();"><spring:message code="btnAplyTemplate"/></button> <input type="hidden" class="span1" id="confirmFlagforChangeTemplate" name="confirmFlagforChangeTemplate" value="0">
				  </div>
		     </div>	
		<%---------- Gagan :District wise Dynamic Template [END]  --%>
		
               <!-- -Rahul Tyagi:Document Div  18/11/2014 -->
	
	
		    	 	<div class="row col-md-12" id="documentdiv">
		    	<div class="row col-sm-6" style="max-width:300px;">
			    	<label><strong><spring:message code="lblDoc"/></strong></label>
		        	<br/>
		        	<select class="form-control" id="districtDocument" onchange="showDocumentLink()">
		        		<option value=""><spring:message code="sltDoc"/></option>
		        	</select>
		        	
			
		         </div>	<br><br>
		         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		        	<a href="javascript:void(0);" id="viewDoc" class="hide top26" onclick="return vieDistrictFile('ifrmAttachment')"><spring:message code="lnkVFile"/></a>
		    	</div>
		    	
	<!-- END -->	

		<div class="control-group row" style="padding-left: 15px;padding-top: 5px;">
    		<label><strong>
<spring:message code="lblTo"/><br/></strong><span id="emailDiv" style="width:612px;word-wrap:break-word;padding-left:1px;"></span>
   		</div>
		<div id='support' class="row" style="padding-left:15px;" >
		<div class="control-group">
			<div class="">
		    	<label><strong><spring:message code="lblSub"/></strong><span class="required">*</span></label>
	        	<input id="messageSubject" name="messageSubject" type="text" class="form-control" style='width:612px;' maxlength="100" />
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="messageSend" style="width: 612px;">
		    	<label><strong><spring:message code="lblMsg"/></strong><span class="required">*</span></label>
	        	<textarea rows="5" class="form-control" cols="" id="msg" name="msg" maxlength="1000"></textarea>
	        </div>
	        <input type="hidden" id="teacherIdForMessage" name="teacherIdForMessage" value="">
    		<input type="hidden"  name="messageDateTime" id="messageDateTime" value="${dateTime}"/>
	    	<div id='fileMessages' style="padding-top:10px;">
        		<a href='javascript:void(0);' onclick='addMessageFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addMessageFileType();'>Attach a File</a>
        	</div>   
		</div>
 		</div>
 		<div id='lodingImage' style="display: block;text-align:center;padding-top:4px;"></div>
 		<!-- end support div -->
  	</div>
  	
  	</form>
 	<c:set var="chkSendDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSendDisp" value="none" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSendDisp" value="inline" />
			</c:otherwise>
	</c:choose>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'><spring:message code="btnClr"/></button>&nbsp;
 		<button class="btn btn-primary" onclick="validateMessage()" style="display: ${chkSendDisp}"><spring:message code="btnSend"/></button>&nbsp;
 	</div>
</div>
</div>
</div>

<%-- ---------- GAgan : Apply Template Confirm Pop up [Start] ----------------%>
<div class="modal hide"  id="confirmChangeTemplate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id=''>x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="ddd"><spring:message code="tmpForExistingTemplateAndMsgOverrideClickOnOk/Cancel"/>
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<span id=""><button class="btn btn-large btn-primary" onclick="confirmChangeTemplate()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button></span> 		
 	</div>
</div>
</div>
</div>
<%------- GAgan : Apply Template Confirm Pop up [END] ---------%>

<iframe src="" id="ifrmCert" width="100%" height="480px" style="display: none;"></iframe>  
<div class="modal hide"  id="saveToFolderDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:900px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headSaveCandidate"/></h3>
	</div>
	<div class="modal-body" style="max-height: 500px;overflow-y: scroll;"> 		
		<div class="control-group">
		<div  style="border: 0px solid green;width:220px;height:450px;float: left;" class="span5">	
		<div class="span5" id="tree_menu" style=" padding: 8px 0px 10px 2px; " >
			<a data-original-title='Create' rel='tooltip' id='createIcon'><span id="btnAddCode" style="cursor: pointer;" class='icon-folder-open  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Rename' rel='tooltip' id='renameIcon'><span id="renameFolder" class='icon-edit  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Cut' rel='tooltip' id='cutIcon'><span id="cutFolder" class='icon-cut  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Copy' rel='tooltip' id='copyIcon'><span id="copyFolder" class='icon-copy  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Paste' rel='tooltip' id='pasteIcon'><span id="pasteFolder" class='icon-paste  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Delete' rel='tooltip' id='deleteIcon'><span id="deletFolder" class='icon-remove-sign  icon-large iconcolor'></span></a>
		</div>
		<iframe id="iframeSaveCandidate"  src="tree.do" class="pull-left" scrolling="auto"  frameBorder="1" style="border: 0px; padding: 0px; margin: 0px; overflow:visible; text-align: top; vertical-align: top;width:220px;height:420px;"></iframe>
	</div>
	<%-- ======================================== Right Div ========================================================================= --%>			
		    <div class="span10" style="padding-top:39px; border: 0px solid green;float: left;"">
				<input type="hidden" id="savecandidatearray" name="savecandidatearray">
				<input type="hidden" id="txtoverrideFolderId" name="txtoverrideFolderId">
				<input type="hidden" id="teachetIdFromPoPUp" name="teachetIdFromPoPUp" value="">
				<input type="hidden" id="teacherIdForHover" name="teacherIdForHover" value="">
				
				<input type="hidden" id="txtflagpopover" name="txtflagpopover" value=""> <%-- If It is 1 -> that means User Clicked on Pop up --%>
				<div id="savedCandidateGrid">
				
				</div>
			</div>	
			<div style="clear: both"></div>
		</div>
		
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="saveCandidateToFolderByUser()" ><spring:message code="btnSave"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>

<%--- ============================================ Gagan: Share Folder Div ======================================================== --%>

<div class="modal hide"  id="shareDiv" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 935px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="defaultSaveDiv()"> x</button>
		<h3 id="myModalLabel"><spring:message code="headShareCandidate"/></h3>
		<input type="hidden" id="entityType" name="entityType" value="${entityType}">
		<input type="hidden" id="savecandiadetidarray" name="savecandiadetidarray"/>
		<input type="hidden" id="JFTteachetIdFromSharePoPUp" name="JFTteachetIdFromSharePoPUp" value="">
		<input type="hidden" id="txtflagSharepopover" name="txtflagSharepopover" value=""> <%-- If It is 1 -> that means User Clicked on Pop up --%>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">			
			<div class="">
					<div class="row">
					    <div class="col-md-10">
						<div class='divErrorMsg' id='errorinvalidschooldiv' ></div>
				        </div>
				        <div class="col-sm-4 col-md-4">
					       <label><spring:message code="lblDistrictName"/></label></br>
				          <c:if test="${DistrictName!=null}">
				             	${DistrictName}
				             	<input type="hidden" id="districtId" value="${DistrictId}"/>
				             	<input type="hidden" id="districtName" value="${DistrictName}" name="districtName"/>
				             </c:if>
			              <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
						
					      </div>
					      
					      <div class="col-sm-6 col-md-6">
					        <label><spring:message code="lblSchoolName"/></label>
				          	<c:if test="${SchoolName==null}">
				           	<input type="text" id="schoolName" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');"	/>
							    <input type="hidden" id="schoolId" value="0"/>
							</c:if>
							  <c:if test="${SchoolName!=null}">
				             	<%-- ${SchoolName}--%>	
				             	<input type="text" id="schoolName" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');" value="${SchoolName}"	/>
				             	<input type="hidden" id="schoolId" value="${SchoolId}"/>
				             	<input type="hidden" id="loggedInschoolId" value="${SchoolId}"/>
				             	<input type="hidden" id="loggedInschoolName" value="${SchoolName}"/>
				             </c:if>
							 <div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
				        
					      </div>
					       <div class="col-md-2 top25">
					        <label>&nbsp;</label>
					        <input type="button" id="" name="" value="Go" onclick="searchUserthroughPopUp(1)" class="btn  btn-primary" >
					      </div>
  					</div>
				</div>
		
		
			<div id="divShareCandidateToUserGrid" style="border: 0px solid green;" class="mt30">
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="shareCandidatethroughPopUp()" ><spring:message code="btnShare"/><i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="defaultSaveDiv()"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
</div>
</div>
 
<div  class="modal hide"  id="shareConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="saveAndShareConfirmDiv" class="">
		    	<spring:message code="msgYuHavaSuccssSharedCandidateToStrUser"/>      	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>

<div class="modal hide"  id="deleteShareCandidate" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			
<spring:message code="msgDeletetStrCondidate"/>
		
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="deleteCandidate()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
</div>
</div>


<div class="modal hide"  id="duplicatCandidate" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			
    <spring:message code="msgCandidateSvav/Cancel"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="saveWithDuplicateRecord()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
</div>
</div>

<div class="modal hide"  id="deleteFolder" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoyouWantToDeleteFolder"/>
		
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="deleteconfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>



<!--Start Profile Div-->
<div  class="modal hide"  id="myModalPhoneShowPro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel"><spring:message code="headPhone"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="divPhoneByPro" class="">
		    		        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>

<div  class="modal hide" id="myModalProfileVisitHistoryShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:100px;" id="mydiv">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel"><spring:message code="headTeacherProfVisitHistory"/></h3>
	</div>
	<div class="modal-body" style="max-height: 400px;overflow-y: scroll;padding-right: 18px;">		
		<div class="control-group">
			<div id="divteacherprofilevisithistory" class="">
		    		        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
 	</div>
  </div>
 </div>
</div>

<div  class="modal hide pdfDivBorder"  id="modalDownloadsCommon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-contenat" style="background-color: #ffffff;">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()">x</button>
		<h3 id="myModalLabelText"></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
				 <iframe src="" id="ifrmTransCommon" width="100%" height="100%" scrolling="auto">
				 </iframe>  
				<!--<embed  src="" id="ifrmTrans" width="100%" height="100%" scrolling="no"/> -->     	
			    <!--<object data="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
				    <p>Insert your error message here, if the PDF cannot be displayed.</p>
				    </object>
			    -->	  
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
 	</div>
</div>

<div  class="modal hide"  id="modalDownloadPDR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content" style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel"><spring:message code="headPdRep"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="">
<iframe src="" id="ifrmPDR" width="100%" height="480px">
 </iframe>        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>

<div style="display:none; z-index: 5000;" id="loadingDiv" >
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
 </div>

 
<div  class="modal hide"  id="myModalReferenceNoteView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 520px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel"><spring:message code="headRefrNot"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="" id="divRefNotesInner">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>



<!-- Ref Note -->
<div class="modal hide"  id="myModalReferenceNotesEditor"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
	<div class="modal-dialog-for-cgreference">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='closeRefNotesDivEditor();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headNot"/></h3>
	</div>
	
	<div class="modal-body" style="margin:10px;">	
		<div class="row" id="divNotes" style="padding-left: 15px; ">						
		</div>
		<iframe id='uploadNoteFrameID_ref' name='uploadNoteFrame_ref' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='frmNoteUpload_ref' enctype='multipart/form-data' method='post' target='uploadNoteFrame_ref'  class="form-inline" onsubmit="return saveReferenceNotes();" action='referenceNoteUploadServlet.do' accept-charset="UTF-8">
			<input type="hidden" name="eleRefId" id="eleRefId">
			<input type="hidden" id="teacherIdForNote_ref" name="teacherIdForNote_ref">
			
			<div class="row mt10">
				<div class='span10 divErrorMsg' id='errordivNotes_ref' style="display: block;"></div>
				<div class="span10" >
			    	<label><strong><spring:message code="lblEtrNot"/><span class="required">*</span></strong></label>
			    	<div class="span10" id='divTxtNode_ref' style="padding-left: 0px; margin-left: 0px; " >
			    		<textarea readonly id="txtNotes_ref" name="txtNotes_ref" class="span10" rows="4"   ></textarea>
			    	</div>  
			    	<div id='fileRefNotes' style="padding-top:10px;">
		        		<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addRefeNoteFileType();'>Attach a File</a>
		        	</div>      	
				</div>						
			</div>
		</form>
 	</div> 	 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeRefNotesDivEditor();'><spring:message code="btnClr"/></button>&nbsp; 
 		<span id="spnBtnSave" style="display:inner"><button class="btn btn-primary"  onclick="saveReferenceNotes()"><spring:message code="btnSave"/> <i class="icon"></i></button>&nbsp;</span>
 	</div>
</div>
</div>
</div>

<div class="modal hide"  id="myModalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" style="z-index: 5000;" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'  onclick="setZIndexJobDiv()">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='myModalMsgShow'>
		</div>
 	</div>	
 	<div class="modal-footer"> 		
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setZIndexJobDiv()"><spring:message code="btnClose"/></button></span> 		
 	</div>
</div>
</div>
</div>

<!-- End Ref Note -->
<!--End Profile Div-->
<div class="modal hide"  id="myModalQAEXEditor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showPreviousModel();">x</button>
		<h3><spring:message code="headExp"/></h3>
	</div>
	    <div class="modal-body">	
			<div class="row mt10">
				<div class="span10" >
			    	<div id='divExplain' style="margin-top:-15px;">
		        	</div>      	
				</div>						
			</div>
     	 </div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showPreviousModel();"><spring:message code="btnClr"/></button>
 	</div>
</div>
</div>
</div>



<div class="modal hide" id="tfaMsgShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='closeTFAMsgBox();'>
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headTm"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='showTfaMsgBody'>
					<spring:message code="msgchangeCandidatePortfolioAndTFAStatus"/>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" onclick="saveTFAbyUser();"><spring:message code="btnSave"/><i class="icon"></i></button>
				<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeTFAMsgBox();'><spring:message code="btnClr"/></button>
			</div>
		</div>
	</div>
</div>



<!-- mukesh chart -->

<script type='text/javascript' src='js/d3.js'></script>


<div class="modal hide"  id="joborderchartDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1000px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearText()">x</button>
	   	<h3 id=""><spring:message code="headJobOdrStatus"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 	
	     	<div class="" style="padding-top: 5px; float: right" id="searchIdjob">
		      <a href='javascript:void(0);' onclick="getSearchPan1()"><span class='icon-search icon-large iconcolor' style="font-size: 0.99em;"></span><b>&nbsp;Search Again<img src="images/arrow_left_animated.gif"/></b></a>
		   </div>
		 
		 <div class="hide row col-sm-12 col-md-12 modalTM" id='searchItems' style="margin-top: 15px;margin-left:0px;padding: 15px;min-width:800px; z-index: 1300;"> 
		       
		       <div class="row" id="searchPanM">
		        <a style='padding-right: 6px; float: right;margin-right:10px;'   onclick='hideSearchPan();' href='javascript:void(0);' ><i class='icon-remove icon-large' ></i></a>
		       </div>
		        
		        
		       <div class="row hide" id="districtSchoolDiv"> 
		       <div class="col-sm-12 col-md-12" id="cerErrorDiv"></div>
		        <div class="col-sm-6 col-md-6">
				     <label id="captionDistrictOrSchool"><spring:message code="lblDistrictName"/></label>
				         <c:if test="${DistrictName==null}"> <br/>
					        <label class="" id="dName"></label>
				         </c:if>
				    		
							<c:if test="${DistrictName!=null}"> <br/>
				             	${DistrictName}	
				             	
				             	<input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictId}"/>
				             	<input type="hidden" id="districtORSchoolName" name="districtORSchoolName"/>
				             	<input type="hidden" id="districtHiddenlIdForSchool"/>
				            </c:if>
				             <c:if test="${DistrictName==null && DistrictId!=null}">
				              	<input type="hidden" id="districtHiddenlIdForSchool" value="${DistrictId}"/>
				              </c:if>
				               <c:if test="${DistrictName==null && DistrictId==null}">
				              	<input type="hidden" id="districtHiddenlIdForSchool"/>
				              </c:if>
				       		 <input type="hidden" id="JobOrderType" value="${JobOrderType}"/>
				        	 <div id='divTxtShowData555'  onmouseover="mouseOverChk('divTxtShowData555','districtORSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
							             	
               </div>
		        
		         <div class="col-sm-6 col-md-6">
	              <label><spring:message code="lblSchoolName"/></label>        	
		          	<c:if test="${SchoolName==null}">
		           	<input type="text" id="schoolName1" maxlength="100" name="schoolName1" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData12', 'schoolName1','districtId','');"
										onkeyup="getSchoolAuto(this, event, 'divTxtShowData12', 'schoolName1','districtId','');"
										onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData12');"	/>
					    <input type="hidden" id="schoolId" value="0"/>
						
					</c:if>
				   <c:if test="${SchoolName!=null}">
		             	<%-- ${SchoolName}--%>	
		             	<input type="text" id="schoolName1" maxlength="100" name="schoolName1" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData12', 'schoolName1','districtId','');"
										onkeyup="getSchoolAuto(this, event, 'divTxtShowData12', 'schoolName1','districtId','');"
										onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData12');" value="${SchoolName}"	disabled="disabled"/>
		             	<input type="hidden" id="schoolId" value="${SchoolId}"/>
		             	<input type="hidden" id="loggedInschoolId" value="${SchoolId}"/>
		             	<input type="hidden" id="loggedInschoolName" value="${SchoolName}"/>
		             </c:if>	 
				   <div id='divTxtShowData12'  onmouseover="mouseOverChk('divTxtShowData12','schoolName1')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	 
		         </div>
         
         </div>
        <div class="row hide" id="jobCertDiv">   
        
                <div class="col-sm-6 col-md-6">
		        
					       	<span class=""><label class=""><spring:message code="lblCert/LiceState"/></label>
					       	<select class="form-control" id="stateId" name="stateId" onchange="activecityType();" >
							 <option value="0"><spring:message code="optAllCert/LiceState"/></option>  
							 <c:if test="${not empty lstStateMaster}">
							 	<c:forEach var="stateObj" items="${lstStateMaster}">
									<option value="${stateObj.stateId}">${stateObj.stateName}</option>
								</c:forEach>	
				        	 </c:if>
							</select>	
					       	</span>
				       </div>	
		       <div class="col-sm-6 col-md-6">
			       <span><label class=""><spring:message code="lblCert/LiceName"/></label>
			       </span>
			       <div>
					<input type="hidden" id="certificateTypeMaster" value="0">
					<input type="text" 
						class="form-control"
						maxlength="200"
						id="certType" 
						value=""
						name="certType" 
						onfocus="getFilterCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
						onkeyup="getFilterCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');" 
						onblur="hideFilterCertificateTypeDiv(this,'certificateTypeMaster', 'divTxtCertTypeData');"/>
					<div id='divTxtCertTypeData' style=' display:none;position:absolute;' class='result' onmouseover="mouseOverChk('divTxtCertTypeData','certType')"></div>
				</div>       
		      </div>
		      
	   </div> 
	   
	   <div class="row " >  
	     <div class="col-sm-6 col-md-6 hide"  id="numberofopeningDiv">
	        <span><label class=""><spring:message code="lblNumOfOpenings"/></label></span>
			     <table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td valign="top" width="20%">
										<select  id="numOpeningSelectVal"
											name="numOpeningSelectVal" class="form-control" onchange="chknoOfOpeningValue() ">
											<option value="0" selected="selected">
												All
											</option>
											<option value="5">
												>=
											</option>
											<option value="1">
												=
											</option>
											<option value="2">
												<
											</option>
											<option value="3">
												<=
											</option>
											<option value="4">
												>
											</option>
										</select>
									</td><td width="4%"> </td>
									<td valign="top" width="60%">
										<input id="numberofopening" name="numberofopening"   disabled="disabled" maxlength="100" class="help-inline form-control" type="text" onkeypress="return checkForInt1(event);"/>
									</td>
								</tr>
							</table>
	           </div>
		   <div class="col-sm-2 col-md-2" >
				<button class="btn btn-primary top25-sm" style="width: 100px; padding-right: " type="button" onclick="getjoborderDetailsBySchools();">Search <i class="icon"></i></button>         	
		    </div>
	   
	   </div>
	    
	   <div class="row">
	    
	    </div>
      </div>   
   <!-- mukesh -->
         <div class="hide" id="legendforchartDiv">
            <table>
				<tr>
			       <td><span class='fa-circle  icon-large iconcolor1'></span></td>
			       <td><spring:message code="tbCritical"/></td>
				</tr>
				<tr>
			       <td><span class='fa-circle icon-large iconcolor2'></span></td>
			       <td><spring:message code="tbAttention"/></td>
				</tr>
				<tr>
			       <td><span class='fa-circle icon-large iconcolor3'></span></td>
			       <td><spring:message code="tbOnPace"/></td>
				</tr>
				
		</table>
         </div>
        	<div class="" id="joborderbySchoolscontainer" >  
		      
		 </div>
		
	</div><!-- model body -->
	
 	<div class="modal-footer">
	 	<table border=0 style="margin-left:850px;">
	 		<tr>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick="clearText()"><spring:message code="btnClose"/></button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>


<!-- End -->


<!--DRahul Tyagi:18/11/2014 DisitrictAttachmentshow div -->
<div  class="modal hide distAttachmentShowDiv"  id="distAttachmentShowDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameEEOC11"><spring:message code="hdAttachment"/></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divPnrAttachment">
					 
					</div>
					<iframe src="" id="ifrmAttachment" width="100%" height="480px"> </iframe>   
				</div>
				
		 	</div>
		 	<input type="hidden" id="eligibilityMasterId" value=""/>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv1()" ><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<!-- End -->


<input type="hidden" id="jobId" name="jobId" value="0"/>
<input type="hidden" id=phoneType name="phoneType" value="0"/>
<input type="hidden" id=msgType name="msgType" value="0"/>	
<input type="hidden" id="noteId" name="noteId" value=""/>
<input type="hidden" id="jobForTeacherGId" name="jobForTeacherGId" value=""/>
<input type="hidden" id="commDivFlag" name="commDivFlag" value=""/>


<input type="hidden"  name="userFPD" id="userFPD"/>
<input type="hidden"  name="userFPS" id="userFPS"/>
<input type="hidden"  name="userCPD" id="userCPD"/>
<input type="hidden"  name="userCPS" id="userCPS"/>
<input type="hidden"  name="folderId" id="folderId"/>
<input type="hidden"  name="checkboxshowHideFlag" id="checkboxshowHideFlag"/>
<input type="hidden"  name="teachersharedId" id="teachersharedId"/>
<input type="hidden"  name="pageFlag" id="pageFlag" value="0"/>

<input type="hidden" id="teacherIdForprofileGrid" name="teacherIdForprofileGrid"/>
<input type="hidden" id="teacherIdForprofileGridVisitLocation" name="teacherIdForprofileGridVisitLocation" value="Mosaic" />

<script type="text/javascript">
function lastAddedLiveFunc()
  {

		//$("#temp").html($("#loadingFlag").val());
		//alert($("#loadingFlag").val());
		if($("#loadingFlag").val()==1 && $('#feed').is(':visible'))
		{
			var val = $("#loadingCounter").val();
	        $('div#lastPostsLoader').html('<img src="images/loadingAnimation.gif">');
			var jobval = $("#jobLoadingCounter").val();
			var yJobval = $("#yellowJobLoadingCounter").val();
	    CommonDashboardAjax.getEndlessData(val,5,jobval,yJobval,districtMaster,schoolMaster,headQuarterMaster,branchMaster,{ 
			async: false,
			callback: function(data){
			
			var dataarray = data.split("@###@");
			var ddtta=""+dataarray[0];
			
			$("#loadingFlag").val(dataarray[4]);
			$(".items").append(ddtta);
			$("#loadingCounter").val(dataarray[1]);
			$("#jobLoadingCounter").val(dataarray[2]);
			$("#yellowJobLoadingCounter").val(dataarray[3]);
			$('div#lastPostsLoader').empty();
			divhover();
			$(".tool").tooltip();
			
			},
			errorHandler:handleError  
			});
	
		}
	
   } 
/*
// This function will be executed when the user scrolls the page.
$(window).scroll(function(e) {
    // Get the position of the location where the scroller starts.
    var scroller_anchor = $(".scroller_anchor").offset().top;
    var scroller_anchor_width = $(".test_content").innerWidth();
    //alert(scroller_anchor_width);
    var resWidth = window.innerWidth;
    var percent = 35;
    if(resWidth==1024)
    	percent=30;
    else if(resWidth==930)
    	percent = 31;
    else if(resWidth==1137)
    	percent = 32;
    else if(resWidth==1537)
    	percent = 36;
    	
     var docWidth = parseInt((resWidth*percent)/100);
    //$("#test").html("width: "+resWidth+" "+percent+" "+docWidth+" "+(parseInt((resWidth/1024)*100)));

    // Check if the user has scrolled and the current position is after the scroller's start location and if its not already fixed at the top
   var is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
	var is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
	var is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
	var is_Opera = navigator.userAgent.indexOf("Presto") > -1; 
	var is_safari = navigator.userAgent.indexOf("Safari") > -1;
	var lefft=30.6;
	if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {lefft=29.8;}
	
    if ($(this).scrollTop() >= scroller_anchor && $('.scroller').css('position') != 'fixed') 
    {    // Change the CSS of the scroller to hilight it and fix it at the top of the screen.
        $('.scroller').css({
            'position': 'fixed',
            'top': '68px',
            'left': docWidth+'px'
            //'left': percent+'%'
        });
        // Changing the height of the scroller anchor to that of scroller so that there is no change in the overall height of the page.
        $('.scroller_anchor').css('height', '50px');
    } 
    else if ($(this).scrollTop() < scroller_anchor && $('.scroller').css('position') != 'relative') 
    {    // If the user has scrolled back to the location above the scroller anchor place it back into the content.
        
        // Change the height of the scroller anchor to 0 and now we will be adding the scroller back to the content.
        $('.scroller_anchor').css('height', '0px');
        
        // Change the CSS and put it back to its original position.
        $('.scroller').css({
           'position': 'relative',
           'top': '0px',
           'left': '0%'
        });
    }
    
    var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();
        var �scrolltrigger = 0.95;

�       if �((wintop/(docheight-winheight)) > scrolltrigger) {
		//alert("bottom");
�        lastAddedLiveFunc();
� }
});*/
/*$(document).ready(function() {
	var s = $("#sticker");
	var pos = s.position();	
	
	$(window).scroll(function() {
		var windowpos = $(window).scrollTop();
		//s.html("Distance from top:" + pos.top + "<br />Scroll position: " + windowpos+" left:"+pos.left);
		if (windowpos >= pos.top) {
			s.addClass("stick");
		} else {
			s.removeClass("stick");	
		}
	});
});*/

$(document).ready(function() {

//by alok for dynamic div width
try
{
	var mydiv = document.getElementById("mydiv");
    var curr_width = parseInt(mydiv.style.width);
   
    if(deviceType)
    {
 	   mydiv.style.width = 630+"px";  
    }
    else
	   {
 	   mydiv.style.width = 647+"px";  
	   } 
}
catch(e){alert(e)}

	var s = $("#sticker");
	var pos = s.position();	
	try{
		$(window).scroll(function() {
		//////////// auto feed ///////////
		var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();
        var scrolltrigger = 0.95;

   		if ((wintop/(docheight-winheight)) > scrolltrigger) {
		//alert("bottom");  lastAddedLiveFunc();
		}
		///////////////// 
		var windowpos = $(window).scrollTop();
		if (windowpos >= pos.top) {
			s.addClass("stick");
			s.css({
            'top': '115px',
       		 });
		} else {
			s.removeClass("stick");	
		}
	});
	}catch(e){
	var $scw=jQuery.noConflict();
		$scw(window).scroll(function() {
		var wintop = $scw(window).scrollTop(), docheight = $scw(document).height(), winheight = $scw(window).height();
		var scrolltrigger = 0.95;
   		if ((wintop/(docheight-winheight)) > scrolltrigger) {
		//alert("bottom");  lastAddedLiveFunc();
		}
		///////////////// 
		var windowpos = $scw(window).scrollTop();
		if (windowpos >= pos.top) {
			s.addClass("stick");
			s.css({
            'top': '115px',
       		 });
		} else {
			s.removeClass("stick");	
		}
	});
	}
});

/*$(document).ready(function(){
�
�  $(window).scroll(function(){

�  var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();
�  var �scrolltrigger = 0.95;

�   if  ((wintop/(docheight-winheight)) > scrolltrigger) {
		//alert("bottom");
�    lastAddedLiveFunc();
�   }
� });
});*/


</script>

<script src="highcharts/js/highcharts.js"></script>
<script src="highcharts/js/modules/exporting.js"></script>
<script src="highslide/highslide-full.min.js"></script>
<script src="highslide/highslide.config.js"></script>
<link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
<script type="text/javascript">
function createGraph(chartOptions) {
    // open the basic chart
    try{
    $(document).ready(function () {
        // set the click event for the parent chart
        chartOptions.chart.events.click = function () {
            hs.htmlExpand(document.getElementById(chartOptions.chart.renderTo), {
                width: 9999,
                height: 9999,
                allowWidthReduction: true,
                preserveContent: false
            }, {
                chartOptions: chartOptions
            });
        };
        var chart = new Highcharts.Chart(chartOptions);
    });
    }catch (e) {
		var $crg=jQuery.noConflict();
		$crg(document).ready(function () {
        // set the click event for the parent chart
        chartOptions.chart.events.click = function () {
            hs.htmlExpand(document.getElementById(chartOptions.chart.renderTo), {
                width: 9999,
                height: 9999,
                allowWidthReduction: true,
                preserveContent: false
            }, {
                chartOptions: chartOptions
            });
        };
        var chart = new Highcharts.Chart(chartOptions);
    });
	}
}

// Create a new chart on Highslide popup open
hs.Expander.prototype.onAfterExpand = function () {
    if (this.custom.chartOptions) {
        var chartOptions = this.custom.chartOptions;
        if (!this.hasChart) {
        try{
	            chartOptions.chart.renderTo = $('.highslide-body')[0];
	            chartOptions.chart.height = $('.highslide-body').parent().height();
            }catch (e) {
				var $ep=jQuery.noConflict();
				 chartOptions.chart.renderTo = $ep('.highslide-body')[0];
           		 chartOptions.chart.height = $ep('.highslide-body').parent().height();
			}
            chartOptions.chart.events.click = function () {};
            var hsChart = new Highcharts.Chart(chartOptions);
        }
        this.hasChart = true;
    }
};
try{
	$(document).ready(function(){
		$('#divTxtNode').find(".jqte").width(612);
		$('#messageSend').find(".jqte").width(612);
	});
	$('#commMsg').tooltip();
	$('#commNotes').tooltip();
	$('textarea').jqte();
	}
	catch (e)
	{
	var $rew=jQuery.noConflict();
	$rew(document).ready(function(){
		$rew('#divTxtNode').find(".jqte").width(612);
		$rew('#messageSend').find(".jqte").width(612);
	});
	$rew('#commMsg').tooltip();
	$rew('#commNotes').tooltip();
	$rew('textarea').jqte();
	}
</script>
<c:choose>
<c:when test="${userSession.entityType!=1}">
<script type="text/javascript">
getDefaultData();
getCandidateStats();
getJobOrderStatus();
getPoolQuality();
//getHiringVelocity();
try{
$('#createIcon').tooltip();
$('#renameIcon').tooltip();
$('#cutIcon').tooltip();
$('#copyIcon').tooltip();
$('#pasteIcon').tooltip();
$('#deleteIcon').tooltip();
 /* ---------------------- Create Folder Method Start here --------------- */
 $("#btnAddCode").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.createUserFolder();
 });
/*---------------- ------------- Create Folder Method End  here --------------- */
/*-----------------  Rename Folder ------------------------*/
 $("#renameFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.renameFolder();
 });
/*-----------------  Cut Copy Paste Folder ------------------------*/
 $("#cutFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.cutFolder(); 
 });
 
 $("#copyFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.copyFolder();  
 });
 
 $("#pasteFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.pasteFolder(); 
 });
  
 $("#deletFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.deletFolder();
 }); 
 }
 catch (e)
 {
 var $tm=jQuery.noConflict();
$tm('#createIcon').tooltip();
$tm('#renameIcon').tooltip();
$tm('#cutIcon').tooltip();
$tm('#copyIcon').tooltip();
$tm('#pasteIcon').tooltip();
$tm('#deleteIcon').tooltip();
 /* ---------------------- Create Folder Method Start here --------------- */
 $tm("#btnAddCode").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.createUserFolder();
 });
/*---------------- ------------- Create Folder Method End  here --------------- */
/*-----------------  Rename Folder ------------------------*/
 $tm("#renameFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.renameFolder();
 });
/*-----------------  Cut Copy Paste Folder ------------------------*/
 $tm("#cutFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.cutFolder(); 
 });
 
 $tm("#copyFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.copyFolder();  
 });
 
 $tm("#pasteFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.pasteFolder(); 
 });
  
 $tm("#deletFolder").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.deletFolder();
 }); 
 
}
 
</script>
</c:when>
    <c:otherwise>
       <script type="text/javascript">
		setTMDefaultView();
	   </script>
    </c:otherwise>
</c:choose>
<iframe src="" id="ifrmRef" width="100%" height="480px" style="display: none;"></iframe>
<div  class="modal hide"  id="myModalJobList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 962px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="setPageFlag();showProfilePopover();">x</button>
		<h3 id="myModalLabel"><spring:message code="hdJDetail"/></h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto;">		
		<div class="control-group">
			<div id="divJob" style="">		    		        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setPageFlag();showProfilePopover();"><spring:message code="btnClose"/></button> 		
 	</div>
   </div>
  </div>
</div>
<!-- Popup window -->
<div class="modal hide"  id="chgstatusRef1"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUActRef"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="chgstatusRef2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeActRef"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="delVideoLnk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeleteVideoLnk"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="delVideoLnkConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="saveDataLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			
              <spring:message code="msgDataSaved"/>
		
		</div>
 	</div>
 	<div class="modal-footer">
 	<button class="btn  btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide" id="certTextDivDSPQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="certTextDivClose();">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body" style="max-height: 400px;overflow-y:auto;"> 		
		<div class="control-group certTextContent">
			
		</div>
 	</div>
 	<div class="modal-footer">
 		<span id=""><button class="btn" data-dismiss="modal" onclick="certTextDivClose();" aria-hidden="true"><spring:message code="btnClose"/></button></span>&nbsp;&nbsp; 		
 	</div>
</div>
	</div>
</div>



<jsp:include page="selfserviceapplicantprofilecommon.jsp"></jsp:include>

<!--   Add district list in popup starat ----->
<div class="modal hide" id="selectDistrictDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeDistrictlist();">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errorDivDistrictlist' style="display: block;"></div>
					<div id="districtlistdiv">
						<label><strong><spring:message code="msgSelectDistrict"/><span class="required">*</span></strong></label>
						<select name="modalDistrictDetails" id="modalDistrictDetails" class="form-control"></select>
	              		<input type="hidden" name="modeldistrictid" id="modeldistrictid" value="">
					</div>
				</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" id="btnDefaultEPIGroup" onclick="openDistrictlist();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				<button class="btn" onclick="closeDistrictlist();">Cancel</button>
   			</div>
   			<input type="hidden" name="districtlistdivteacherid" id="districtlistdivteacherid" value="">
  		</div>
	</div>
</div>
<!--   Add district list in popup end ----->




<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type="text/javascript">
function closeDistrictlist()
{
	$("#selectDistrictDiv").modal("hide");
}
// New code by Ramesh
// Start Div Grid
function showProfileContentClose(){	
	try{
	 	$("#draggableDivMaster").hide();
	 }catch(e){
		 var $spj=jQuery.noConflict();
		 $spj("#draggableDivMaster").hide();
	 }
	 document.getElementById("teacherIdForprofileGrid").value="";
	 currentPageFlag="";
	}
	//id="jobOrderIcon"
try{
	$('#jobOrderIcon').tooltip();
	$('#scheduleIcon').tooltip();
}catch (e) {
 var $spj1=jQuery.noConflict();
	$spj1('#jobOrderIcon').tooltip();
	$spj1('#scheduleIcon').tooltip();
}




</script>
<!--Start : @ashish :: for Mosaic Teacher Profile {draggableDiv} -->
<script>
try{
$(function() {
$( "#draggableDivMaster" ).draggable({
handle:'#teacherDiv', 
containment:'window',
revert: function(){
var $this = $(this);
var thisPos = $this.position();
var parentPos = $this.parent().position();
var x = thisPos.left - parentPos.left;
var y = thisPos.top - parentPos.top;
if(x<0 || y<180)
{
return true; 
}else{
return false;
}
}
});
});
}catch(e)
{
var $tmd=jQuery.noConflict();
$tmd(function() {
$tmd( "#draggableDivMaster" ).draggable({
handle:'#teacherDiv', 
containment:'window',
revert: function(){
var $this = $(this);
var thisPos = $this.position();
var parentPos = $this.parent().position();
var x = thisPos.left - parentPos.left;
var y = thisPos.top - parentPos.top;
if(x<0 || y<180)
{
return true; 
}else{
return false;
}
}
});
});
}
</script>

<!--End : @ashish :: for Mosaic Teacher Profile {draggableDiv} --> 

