<!-- @Author: Amit
	 @Discription: view of assessment message page.
-->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/AssessmentMessageAjax.js?ver=${resourceMap['AssessmentMessageAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/AssessmentAjax.js?ver=${resourceMap['AssessmentAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type='text/javascript' src="js/assessmentmessage.js?ver=${resourceMap['js/assessmentmessage.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  

<style>
.tooltip {
  position: absolute;
  z-index: 1030;
  display: block;
  font-size: 12px;
  line-height: 1.4;
  opacity: 0;
  filter: alpha(opacity=0);
  visibility: visible;
}

.tooltip.in {
  opacity: 0.9;
  filter: alpha(opacity=90);
}

.tooltip.top {
  padding: 5px 0;
  margin-top: -3px;
}

.tooltip.right {
  padding: 0 5px;
  margin-left: 3px;
}

.tooltip.bottom {
  padding: 5px 0;
  margin-top: 3px;
}

.tooltip.left {
  padding: 0 5px;
  margin-left: -3px;
}

.tooltip-inner {
  max-width: 200px;
  padding: 3px 8px;
  color: #ffffff;
  text-align: center;
  text-decoration: none;
  background-color: #000000;
  border-radius: 4px;
}

.tooltip-arrow {
  position: absolute;
  width: 0;
  height: 0;
  border-color: transparent;
  border-style: solid;
}

.tooltip.top .tooltip-arrow {
  bottom: 0;
  left: 50%;
  margin-left: -5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-left .tooltip-arrow {
  bottom: 0;
  left: 5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-right .tooltip-arrow {
  right: 5px;
  bottom: 0;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.right .tooltip-arrow {
  top: 50%;
  left: 0;
  margin-top: -5px;
  border-right-color: #000000;
  border-width: 5px 5px 5px 0;
}

.tooltip.left .tooltip-arrow {
  top: 50%;
  right: 0;
  margin-top: -5px;
  border-left-color: #000000;
  border-width: 5px 0 5px 5px;
}

.tooltip.bottom .tooltip-arrow {
  top: 0;
  left: 50%;
  margin-left: -5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-left .tooltip-arrow {
  top: 0;
  left: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-right .tooltip-arrow {
  top: 0;
  right: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}
</style>

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTblTemplate()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#templateTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 786,
        minWidth: null,
        minWidthAuto: false,
        colratio:[386,130,130,140], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblMessage()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#messageTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
      	height: 280,
        width: 786,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,140,140,160,110,136], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblAssessment()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentMessageTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 786,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,100,113,113,130,100,130], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
</script>  

<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
	<div style="float: left;">
		<img src="images/add-district.png" width="41" height="41" alt="">
	</div>
	<div style="float: left;">
		<div class="subheading" style="font-size: 13px;">Manage Assessment Messages</div>
	</div>
	<div style="clear: both;"></div>	
	<div class="centerline"></div>
</div>

<input type="hidden" name="pageIdNo" id="pageIdNo" value="1" >
<input type='hidden' id='gridNameFlag' name='gridNameFlag'/>

<div>
	<div class="mt10">
    	<div class="panel-group" id="accordion">
        	<div class="panel panel-default">
          		<div class="accordion-heading"> <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle minus"> Assessment Template Master </a> </div>
          		<div class="accordion-body in" id="collapseOne" style="height:auto;">
            		<div class="accordion-inner">
            			<div class="offset1">
              				<div class="col-sm-12 col-md-12">
              					<div class="row">
 									<div class="col-sm-11 col-md-11 right">
										<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
											<a href="javascript:void(0);" onclick="return addTemplate();">+ Add Template</a>
										</c:if>
									</div>
 								</div>
 								<div class="row">
 									<div class="col-sm-11 col-md-11" id="templateGrid" onmouseover="setGridNameFlag('templateGrid')"></div>
 								</div>
								<div id="templateDiv" style="display: none;">
									<div class="row top5">
										<div class="col-sm-10 col-md-10">
	 										<div class='divErrorMsg' id='errordivTemplate' ></div>
										</div>
									</div>
									<div class="row top5">
										<div class="col-sm-3 col-md-3">             
		 									<label><strong><spring:message code="lblInvT"/><span class="required">*</span></strong></label>
									  		<select class="form-control " id="assessmentType" name="assessmentType">
												<option value="0"><spring:message code="optStrAll"/></option>
												<option value="1"><spring:message code="lblB"/></option>
												<option value="4"><spring:message code="lblIPI"/></option>
												<option value="3"><spring:message code="lblSmartP"/></option>
											</select>             
     									</div>
										<div class="col-sm-10 col-md-10"><label><strong>Template Name<span class="required">*</span></strong></label>
										<input type="text" name="templateName" id="templateName" maxlength="100" class="form-control" placeholder=""></div>
										<input type="hidden" name="assessmentTemplateId" id="assessmentTemplateId" >
									</div>
									<div class="top5">
	          							<input class="span0" type="checkbox" name="isDefault" id="isDefault"  class="form-control" placeholder="">
										<label for="isDefault"><strong>Is Default<a href="#" id="iconpophover1" rel="tooltip" data-original-title="Please select the check box if you want to make it a Default Template for select Inventory Type">&nbsp;<img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
	          						</div>
									<div class="row top5">
										<div class="col-sm-4 col-md-4">
											<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}"><a href="javascript:void(0);" onclick="saveTemplate();"><spring:message code="lnkImD"/></a></c:if>
		 									&nbsp;<a href="javascript:void(0);" onclick="closeTemplate();"><spring:message code="btnClr"/></a> 
										</div>
									</div>	
								</div>
							</div>
              			</div>
              			<div class="clearfix"></div>
            		</div>
          		</div>
        	</div>
        	<div class="panel panel-default">
				<div class="accordion-heading"> <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Assessment Message Master </a> </div>
				<div class="accordion-body collapse" id="collapseTwo" style="height: 0px;">   
					<div class="accordion-inner">
						<div class="span14 offset1">
							<div class="col-sm-12 col-md-12">
              					<div class="row">
 									<div class="col-sm-11 col-md-11 right">
										<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
											<a href="javascript:void(0);" onclick="return addMessage();">+ Add Message</a>
										</c:if>
									</div>
 								</div>
 								<div class="row">
 									<div class="col-sm-11 col-md-11" id="messageGrid" onmouseover="setGridNameFlag('messageGrid')"></div>
 								</div>
								<div id="messageDiv" style="display: none;">
									<input type="hidden" name="templateWiseStepMessageId" id="templateWiseStepMessageId" >
									<div class="row top5">
										<div class="col-sm-10 col-md-10">
	 										<div class='divErrorMsg' id='errordivMessage' ></div>
										</div>
									</div>
									<div class="row top5">
										<div class="col-sm-3 col-md-3">             
		 									<label><strong><spring:message code="lblInvT"/><span class="required">*</span></strong></label>
									  		<select class="form-control " id="assessmentTypeM" name="assessmentTypeM" onchange="getTemplatesByInventoryType(this.value,'assessmentTemplateMaster');">
												<option value="0"><spring:message code="optStrAll"/></option>
												<option value="1"><spring:message code="lblB"/></option>
												<option value="4"><spring:message code="lblIPI"/></option>
												<option value="3"><spring:message code="lblSmartP"/></option>
											</select>
     									</div>
     									<div class="col-sm-8 col-md-8">             
		 									<label><strong>Select Template<span class="required">*</span></strong></label>
									  		<select class="form-control " id="assessmentTemplateMaster" name="assessmentTemplateMaster" onchange="getSteps('assessmentStepMaster',this.value);">
												<option value="0">Select Template</option>
											</select>             
     									</div>
     									<div class="col-sm-11 col-md-11 top5">             
		 									<label><strong>Select Step<span class="required">*</span></strong></label>
									  		<select class="form-control " id="assessmentStepMaster" name="assessmentStepMaster">
												<option value="0">Select Step</option>
											</select>
											<div id="stepmaster"></div>
											         
     									</div>
										<div class="col-sm-11 col-md-11 top5" id="stpMessage"><label><strong>Default Message<span class="required">*</span></strong></label>
											<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>	
											<textarea class="form-control" name="stepMessage" id="stepMessage" maxlength="750" rows="4" onkeyup="return chkLenOfTextArea(this,750);" onblur="return chkLenOfTextArea(this,750);"></textarea>
										</div>
									</div>
									<div class="row top5">
										<div class="col-sm-4 col-md-4">
											<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}"><a href="javascript:void(0);" onclick="saveMessage();"><spring:message code="lnkImD"/></a></c:if>
		 									&nbsp;<a href="javascript:void(0);" onclick="closeMessage();"><spring:message code="btnClr"/></a> 
										</div>
									</div>	
								</div>
							</div>
						</div>
						<div class="clearfix">&nbsp;</div>
					</div>
				</div>
			</div>
        	<div class="panel panel-default">
				<div class="accordion-heading"> <a href="#collapseThree" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Assessment Messages </a> </div>
				<div class="accordion-body collapse" id="collapseThree" style="height: 0px;">   
					<div class="accordion-inner">
						<div class="span14 offset1">
							<div class="col-sm-12 col-md-12">
              					<div class="row">
 									<div class="col-sm-11 col-md-11 right">
										<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
											<a href="javascript:void(0);" onclick="return addAssessmentMessage();">+ Add Assessment Message</a>
										</c:if>
									</div>
 								</div>
 								<div class="row">
 									<div class="col-sm-11 col-md-11" id="assessmentMessageGrid" onmouseover="setGridNameFlag('assessmentMessageGrid')"></div>
 								</div>
								<div id="assessmentMessageDiv" style="display: none;">
									<input type="hidden" name="assessmentWiseStepMessageId" id="assessmentWiseStepMessageId" value="0">
									<div class="row top5">
										<div class="col-sm-10 col-md-10">
	 										<div class='divErrorMsg' id='errordivAssessmentMessage' ></div>
										</div>
									</div>
									<div class="row top5">
										<div class="col-sm-3 col-md-3">
		 									<label><strong><spring:message code="lblInvT"/><span class="required">*</span></strong></label>
									  		<select class="form-control " id="assessmentTypeAm" name="assessmentTypeAm" onchange="getGroupsByAssessmentType(this.value,1);">
												<option value="0"><spring:message code="optStrAll"/></option>
												<option value="1"><spring:message code="lblB"/></option>
												<option value="4"><spring:message code="lblIPI"/></option>
												<option value="3"><spring:message code="lblSmartP"/></option>
											</select>
     									</div>
     									<div class="col-sm-3 col-md-3">
     										<label><strong>Select Group<span class="required">*</span></strong></label>
     										<select name="assessmentGroupDetails" id="assessmentGroupDetails" class="form-control" onchange="getAssessmentByGroup(this.value)">
	                   							<option value="0"><spring:message code="optSelectGp"/></option>
	              							</select>
	              						</div>
	              						<div class="col-sm-5 col-md-5">
	              							<label><strong>Select Assessment<span class="required">*</span></strong></label>
	              							<select name="assessmentDetail" id="assessmentDetail" class="form-control">
	                   							<option value="0">Select Assessment</option>
	              							</select>
	              						</div>
	              						<div class="col-sm-3 col-md-3 top5">
											<label><strong>Use Default Template<span class="required">*</span></strong></label><br>
											<label class="radio-inline" style="margin-right: 35px;">
												<input id="useDefault1" name="useDefault" value="1" type="radio" checked="checked" onchange="showAndHideUseDefaultDiv(1);"> Yes
											</label>
											<label class="radio-inline">
												<input id="useDefault2" name="useDefault" value="2" type="radio" onchange="showAndHideUseDefaultDiv(2);"> No
											</label>
										</div>
										<div id="useDefaultDiv" style="display: none;">
	     									<div class="col-sm-11 col-md-11 top5">             
			 									<label><strong>Select Template<span class="required">*</span></strong></label>
										  		<select class="form-control " id="assessmentTemplateMasterAm" name="assessmentTemplateMasterAm" onchange="getStepByAssessmentId(this.value,0);">
													<option value="0">Select Template</option>
												</select>             
	     									</div>
	     									<div class="col-sm-11 col-md-11 top5">             
			 									<label><strong>Select Step<span class="required">*</span></strong></label>
										  		<select class="form-control " id="assessmentStepMasterAm" name="assessmentStepMasterAm" onchange="getMessageByAssessmentAndStep(this.value)">
													<option value="0">Select Step</option>
												</select>             
	     									</div>
											<div class="col-sm-11 col-md-11 top5" id="stpMessageAm"><label><strong>Default Message<span class="required">*</span></strong></label>
												<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>	
												<textarea class="form-control" name="stepMessageAm" id="stepMessageAm" maxlength="750" rows="4" onkeyup="return chkLenOfTextArea(this,750);" onblur="return chkLenOfTextArea(this,750);"></textarea>
											</div>
										</div>
									</div>
									<div class="row top5">
										<div class="col-sm-4 col-md-4">
											<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}"><a href="javascript:void(0);" onclick="saveAssessmentMessage();"><spring:message code="lnkImD"/></a></c:if>
		 									&nbsp;<a href="javascript:void(0);" onclick="closeAssessmentMessage();"><spring:message code="btnClr"/></a> 
										</div>
									</div>	
								</div>
							</div>
						</div>
						<div class="clearfix">&nbsp;</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
  
<div style="display:none;" id="loadingDiv">
	<table  align="center" >
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>

<script>
	getTemplateGrid();
	getMessageGrid();
	getAssessmentMessageGrid();
</script>


<script type="text/javascript">
var selectIds = $('#collapseOne,#collapseTwo,#collapseThree');
$(function ($) {
    selectIds.on('show.bs.collapse hidden.bs.collapse', function () {
        $(this).prev().find('.accordion-toggle').toggleClass('plus minus');
    })
});

$('.accordion').collapse();
$('.accordion').on('show', function (e) {
$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
});

$('.accordion').on('hide', function (e) {
$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
});
</script>

<script type="text/javascript">
$('#myModal').modal('hide');
$('#iconpophover1').tooltip();
</script>

<!-- Use Default modal div start -->
<div class="modal hide" id="modalUseDefault" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="modalMsg"></div>
				</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" id="btnUseDefault" onclick="closeAssessmentMessage();" data-dismiss="modal" aria-hidden="true">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
   			</div>
  		</div>
	</div>
</div>
<!-- Use Default modal div end -->
