
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PFAcademics.js?ver=${resourceMap['PFAcademics.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/teacher/pfacademics.js?ver=${resourceMap['js/teacher/pfacademics.js']}"></script>
<script type='text/javascript' src="js/teacher/pfAutoCompAcademics.js?ver=${resourceMap['js/teacher/pfAutoCompAcademics.js']}"></script>
 <script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  
<script type="text/javascript">

var $j=jQuery.noConflict();
        $j(document).ready(function() {
        
            
        });
        
function applyScrollOnTbl()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#academicGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[205,120,136,106,144,100,137],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}
applyScrollOnTbl();
</script>
<jsp:include page="/portfolioheader.do"></jsp:include>

<!-- add by ankit -->
<div  class="modal hide"  id="myID" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class='modal-dialog'>
      <div class='modal-content'>
      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideProfile()">x</button>
            <h3 id="myModalLabel">Message</h3>
      </div>
      <div class="modal-body">            
            
      </div>
      <div class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button> 
      </div>
  </div>
 </div>
</div>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/academics.png" alt="" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headAcad"/></div>	
         </div>			
		<div class="pull-right add-employment1">
			<a href="#" onclick="return showUniversityForm()"><spring:message code="lnkAddSch"/> </a>
		</div>
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div id="errorAcDiv" class='divErrorMsg'></div>

<div class="TableContent top15">        	
            <div class="table-responsive" id="divDataGrid">          
                	         
            </div>            
</div>



<div id="divAcademicRow" style="display: none;" class="top15">
	<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	<form  id="frmAcadamin" enctype='multipart/form-data' method='post' target='uploadFrame' action='fileuploadservlet.do'>
		<input type="hidden" value="" id="academicId" name="academicId"/>	
		<div class="row">
			<div class="col-sm-12 col-md-12">			
	      				<div  id='divServerError' class='divErrorMsg' style="display: block;">${msgError}</div>
	      				<div class='divErrorMsg' id='errordiv' style="display: block;"></div>				              	
			</div>
		</div>
		<div class="row">			
				<div class="col-sm-2 col-md-2" style="margin: 0px;">
					<label>
						<spring:message code="lblDgr"/><span class="required nondgrReq">*</span><a href="#" id="left-sm15" style="margin-left: 7px;" onclick="document.getElementById('degreeName').value='<spring:message code="lblNoDgr"/>';document.getElementById('degreeName').focus();return false;"><spring:message code="lblNoDgr"/></a>
					</label>					
				 	<input  type="hidden" id="degreeId" value="">
					<input  type="hidden" id="degreeType" value="">
					<input  type="text"
						maxlength="50" 
						class="form-control input-small"
						id="degreeName" 
						autocomplete="off"
						value=""
						name="degreeName" 
						onfocus="getDegreeMasterAutoComp(this, event, 'divTxtShowData', 'degreeName','degreeId','');"
						onkeyup="getDegreeMasterAutoComp(this, event, 'divTxtShowData', 'degreeName','degreeId','');" 
						onblur="hideDegreeMasterDiv(this,'degreeId','divTxtShowData');checkGED(); "/>
					<div id='divTxtShowData' style=' display:none;position:absolute;' class='result' 
					onmouseover="mouseOverChk('divTxtShowData','degreeName')"></div>				
				</div>
				
				<div class="col-sm-5 col-md-5">
					<label >
						<spring:message code="lblSchool"/><span class="required nondgrReq">*</span><a href="#" id="left-sm130" onclick="document.getElementById('universityName').value='<spring:message code="lblOthr"/>';document.getElementById('universityName').focus();return false;"><spring:message code="lnkMySchNotLi"/></a>
					</label>					
					<input  type="hidden" id="universityId" value="">
					<input  type="text" 
						class="form-control"
						maxlength="100"
						id="universityName" 
						autocomplete="off"
						value=""
						name="universityName" 
						onfocus="getUniversityAutoComp(this, event, 'divTxtUniversityData', 'universityName','universityId','');"
						onkeyup="getUniversityAutoComp(this, event, 'divTxtUniversityData', 'universityName','universityId','');" 
						onblur="hideUniversityDiv(this,'universityId','divTxtUniversityData');showHideOtherUniversity();"/>
					<div id='divTxtUniversityData' style=' display:none;position:absolute;' 
					onmouseover="mouseOverChk('divTxtUniversityData','universityName')" class='result' ></div>
					<input type="text" 
						class="form-control"
						maxlength="100"
						value=""
						name="otherUniversityName" id="otherUniversityName" style="display: none;margin-top: 10px">					
				</div>
				
				<div class="col-sm-5 col-md-5">
					<label >
						<spring:message code="lblFildOfStudy"/><span class="required nondgrReq">*</span><a  href="#" id="left-sm130" style="margin-left: 71px;"  onclick="document.getElementById('fieldName').value='<spring:message code="lblOthr"/>'; document.getElementById('fieldName').focus();return false;"><spring:message code="lnkMyFldNotLi"/></a>
					</label>						
					<input  type="hidden" id="fieldId" value="">
					<input  type="text" 
						class="form-control"
						maxlength="50"
						autocomplete="off"
						id="fieldName"
						value=""
						name="fieldName" 
						onfocus="getFieldOfStudyAutoComp(this, event, 'divTxtFieldOfStudyData', 'fieldName','fieldId','');"
						onkeyup="getFieldOfStudyAutoComp(this, event, 'divTxtFieldOfStudyData', 'fieldName','fieldId','');" 
						onblur="hideFeildOfStudyDiv(this,'fieldId','divTxtFieldOfStudyData');"/>
					<div id='divTxtFieldOfStudyData' style=' display:none;position:absolute;' 
					onmouseover="mouseOverChk('divTxtFieldOfStudyData','fieldName')"  class='result' ></div>					
				</div>
		
		</div>
		
				<div class="row">
		
		
			<c:choose>
			<c:when test="${isFR ne true}">
				</c:when>
				<c:otherwise>
				</c:otherwise>
				</c:choose>
			     	<div class="col-sm-12 col-md-12">
					<label>
						<span class="required nondgrReq1">*</span>
					</label>
					 </div>
					 <c:choose>
			<c:when test="${isFR ne true}">
			<div class="col-sm-2 col-md-2">	
			</c:when>
			<c:otherwise>
				<div class="col-sm-2 col-md-2">
			</c:otherwise>
		</c:choose>
					<select class="form-control" id="attendedInYear" name="attendedInYear">
						<option id="attendedInYearSelect" value=""><spring:message code="optSlt"/></option>
						<c:forEach var="year" items="${lstLastYear}">							
							<option id="attendedInYear${year}" value="${year}">${year }</option>
						</c:forEach>					
					</select>
				    </div>
				    <c:choose>
				    <c:when test="${isFR ne true}">
			<div class="fl to" style="margin-left:-7px;">
			</c:when>
			<c:otherwise>
				<div class="fl to" style="margin-left:-7px;">
			</c:otherwise>
		</c:choose>
				
							<spring:message code="lblTos"/>
						</div>	
						<c:choose>
			<c:when test="${isFR ne true}">
			<div class="col-sm-2 col-md-2" style="margin-left:-7px;">	
			</c:when>
			<c:otherwise>
				<div class="col-sm-2 col-md-2" style="margin-left:-7px;">	
			</c:otherwise>
		</c:choose>			
				
					
					<select class="span1 form-control" id="leftInYear" name="leftInYear"">
						<option id="attendedInYearSelect" value=""><spring:message code="optSlt"/></option>
						<c:forEach var="year" items="${lstLastYear}">
							<option id="leftInYear${year}" value="${year}">${year }</option>
						</c:forEach>						
					</select>
				</div>
				
				<div class="col-sm-3 col-md-3"  style="margin-top: -23px;">
				<label>
						<spring:message code="HeadTran"/> <span class="required nondgrReq2">*</span>
					</label>
					<input type="hidden" id="hiddenged" name="hiddenged"/>
					<input type="hidden" id="sbtsource_aca" name="sbtsource_aca" value="0"/>
					<input id="pathOfTranscriptFile" name="pathOfTranscriptFile" type="file" width="20px;">
					<a href="javascript:void(0)" onclick="clearTranscript()"><spring:message code="lnkClear"/></a>
				</div>
				<input type="hidden" id="pathOfTranscript"/>				
				<div class="col-sm-4 col-md-4" id="removeTranscript" name="removeTranscript" style="display: none;">
					<label>
						&nbsp;&nbsp;
					</label>
					<span id="divResumeName"></span>
					&nbsp;&nbsp;<br><a href="javascript:void(0)" onclick="removeTranscript()" style="margin-left:11px;"><spring:message code="lnkRmovS"/></a>
					<a href="#" id="iconpophover6" rel="tooltip" data-original-title="Remove transcript file !">
					<img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</div>	
		</div>
	</form>
</div>

<div class="hide"><input type="checkbox" id="international" name="international"></div>

<!--<div class="row" id="divGPARow" style="display: none; padding-left: 63px;">-->
<div id="divGPARow" style="display: none; " onkeypress="chkForEnter(event)">
	<form  id="frmGPA">
		 <div class="row">
				<div class="col-sm-2 col-md-2">
					GPA
					<a href="#" id="iconpophover1" rel="tooltip"
						data-original-title='<spring:message code="GradePointAverage1"/>'><img
							src="images/qua-icon.png" width="15" height="15" alt="">
					</a>
				</div>
		</div>

		<div class="row">
		<div class="col-sm-2 col-md-2">
			<label>
		<spring:message code="lblFresh"/>
			</label>
			<input type="text" id="gpaFreshmanYear"  name="gpaFreshmanYear"  onkeypress='return checkForDecimalTwo(event)' class="input-small form-control" id="gpaFreshman" maxlength="4">
		</div>
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblSoph"/>
			</label>
			<input type="text" id="gpaSophomoreYear"  name="gpaSophomoreYear" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaSophomore" maxlength="4">
		</div>
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblJunior"/>
			</label>
			<input type="text" id="gpaJuniorYear"  name="gpaJuniorYear" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaJunior" maxlength="4">
		</div>
		
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblSenior"/>
			</label>
			<input type="text" id="gpaSeniorYear"  name="gpaSeniorYear" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaSenior" maxlength="4">

		</div>
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblCumulative"/>
			</label>
			<input type="text" id="gpaCumulative"  name="gpaCumulative" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaCumulative" maxlength="4">
		</div>
		</div>
	</form>
</div>

<!--<div class="row" id="divGPARowOther" style="display: none; padding-left: 63px;">-->

<div class="span16" id="divGPARowOther" style="display: none; margin: 0px; " onkeypress="chkForEnter(event)">
	<form id="frmGPA1">
		<div class="row" >
			<div class="col-sm-2 col-md-2">
			<spring:message code="lblGPA"/>
					<a href="#" id="iconpophover2" rel="tooltip"
						data-original-title="Grade Point Average"><img
							src="images/qua-icon.png" width="15" height="15" alt="">
					</a>
				</div>
		</div>
		<div class="row" >
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblCumulative"/>
			</label>
			<input type="text" id="gpaCumulative1"  name="gpaCumulative1" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaCumulative" maxlength="4">
		</div>
		</div>
	</form>
</div>



<div class="row"  >
	<div class="col-sm-4 col-md-4" id="divDone" style="display: none; ">
		<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;" onclick="insertOrUpdate()">
		<spring:message code="lnkImD"/>
		</a>&nbsp;&nbsp;
		<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;"	onclick="resetUniversityForm()">
			<spring:message code="lnkCancel"/>
		</a>
	</div>
</div>

<div class="">
	<div class="mt30">
		<button class="btn btn-large btn-primary" href="#" onclick="return saveAndContinuPFAcademics()"><spring:message code="btnSv&Conti"/> <i
			class="icon"></i>
		</button>
		<br>
		<br>
	</div>
</div>

<iframe src="" id="ifrmTrans" width="100%" height="480px" style="display: none;">
 </iframe>  
<div style="display:none;" id="loadingDiv">
     <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'><spring:message code="msgYrFileBegUpLod"/></td></tr>
	</table>
</div>

<script type="text/javascript">
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
$('#iconpophover6').tooltip();
showGrid();
</script>

<script>
function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#myModalDymanicPortfolio').modal('hide');
	$('#virusDivId').modal('show');
}
</script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>
