<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="tm.bean.user.UserMaster"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type='text/javascript' src="js/certtypeautocomplete.js?ver=${resourceMap['js/certtypeautocomplete.js']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.Ajax']}"></script>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<link href="css/font-awesome.css?ver=${resourceMap['css/font-awesome.css']}"  rel="stylesheet" type="text/css" >
<link href="css/font-awesome-ie7.css?ver=${resourceMap['css/font-awesome-ie7.css']}"  rel="stylesheet" type="text/css">
<link href="css/font-awesome-ie7.min.css?ver=${resourceMap['css/font-awesome-ie7.min.css']}"  rel="stylesheet" type="text/css">   

<script type="text/javascript" src="js/jquery-1.4.1.js?ver=${resourceMap['']}"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js?ver=${resourceMap['']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />

<script type="text/javascript" src="js/tmhome.js?ver=${resourceMap['js/tmhome.js']}"></script>	
<link href="css/tmcommon.css?ver=${resourceMap['css/tmcommon.css']}"  rel="stylesheet" type="text/css">

<script type="text/javascript" src="dwr/interface/JobsAjax.js?ver=${resourceMap['JobsAjax.Ajax']}"></script>
<script type='text/javascript' src="js/jobs.js?ver=${resourceMap['js/jobs.js']}"></script>

<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 340,
         width: 940,
        minWidth: null,
        minWidthAuto: false,
        colratio:[145,300,100,170,100,130],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}
</script>
<div class="row top15" style="margin-left: 0px;margin-right: 0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headManage"/> <span id="districtOrSchoolTitle"></span> <spring:message code="lblJobOrders"/></div>	
         </div>			
		 <div class="pull-right add-employment1">	
		    <c:set var="url" value="addjob.do?JobOrderType=${JobOrderType}&jobAuthKey=${jobAuthKey}"></c:set>	
			<a href="javascript:void(0);" onclick="addnewJob('${url}')"><spring:message code="lnkAddJobOrder"/></a>
		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>	
	<div onkeypress="return chkForEnterSearchJobOrder(event);">					
			 <div class="row top5">
                <div class="col-sm-6 col-md-6">
		             <label id="captionDistrictOrSchool"><spring:message code="lblDistrictName"/></label>		            
		    		<br/>
					<c:if test="${DistrictOrSchoolName!=null}">
		             	${DistrictOrSchoolName}			             	
		             	<input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
		             	<input type="hidden" id="districtORSchoolName" name="districtORSchoolName"/>
		             	<input type="hidden" id="districtHiddenlIdForSchool"/>
		             </c:if>
		             <c:if test="${DistrictOrSchoolName==null && DistrictOrSchoolId!=null}">
		              	<input type="hidden" id="districtHiddenlIdForSchool" value="${DistrictOrSchoolId}"/>
		              </c:if>
		               <c:if test="${DistrictOrSchoolName==null && DistrictOrSchoolId==null}">
		              	<input type="hidden" id="districtHiddenlIdForSchool"/>
		              </c:if>
		       		 <input type="hidden" id="JobOrderType" value="${JobOrderType}"/>
		        	 <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
					             	
                </div>
                <div class="col-sm-6 col-md-6"  style="display: none;">            
			          <label> <spring:message code="lblSchoolName"/></label>
			            
								<input type="text" id="schoolName" maxlength="100"
									name="schoolName" class="form-control" placeholder=""
									onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
									onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
									onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');" 
								/>	
								<input type="hidden" id="schoolName" name="schoolName"/>
	         					<input type="hidden" id="schoolId" name="schoolId" value="0"/> 						
						<div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
			        </div>
			 </div>  
			     		 
             <div class="row">  
                   <div class="col-sm-6 col-md-6">
			             <div class="">
			             	<span class=""><label class=""><spring:message code="headStatus"/></label>
					             <select id="status" name="status" class="form-control">
						              <option value=""><spring:message code="optAllJobOdr"/></option>
						              <option value="A" selected><spring:message code="optActJobOdr"/></option>
						              <option value="I"><spring:message code="optInactJobOdr"/></option>
					              </select>
				             </span>
				         </div>
              		 </div>
             
             
              <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmTrans1" width="100%" height="100%" scrolling="no">
			 </iframe>  
		  </div> 
             <div class="col-sm-6 col-md-6">                      
	           	<label>
	            <spring:message code="lblCert/LiceName"/>
	            </label>
	            <input type="hidden" id="certificateTypeMaster" value="">
				<input type="text" 
					class="form-control"
					maxlength="100"
					id="certType" 
					value=""
					name="certType" 
					onfocus="getAllCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
					onkeyup="getAllCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');" 
					onblur="hideAllCertificateTypeDiv(this,'certificateTypeMaster', 'divTxtCertTypeData');"/>
				<div id='divTxtCertTypeData' style=' display:none;position:absolute;' onmouseover="mouseOverChk('divTxtCertTypeData','certType')" class='result' ></div>
             </div>
            </div>
            
            <div class="row">   	 
	            	
              		 <div class="col-sm-6 col-md-6">
		             	<label class=""><spring:message code="lblJoI/JCode"/></label>
		             	<input type="text" id="jobOrderId" name="jobOrderId" class="form-control fl" >
	             	</div> 
	             	
	             	 <div class="col-sm-3 col-md-3 top30-sm" >
	             	  <button style="margin-top: -4px;" class="btn fl btn-primary" type="button" onclick="return displayJobRecordsEnterOnButton()"><spring:message code="btnSearch"/>&nbsp;<i class="icon"></i></button>			                
	             	 </div> 	  	
    	   </div>	
    		   
   </div>	
 
        <div class="TableContent top20">        	
            <div class="table-responsive" id="divMain">          
            </div>            
        </div> 	    
 	 
	<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
    <br><br>
    
<script type="text/javascript"> 
displayJobRecords();
$('#iconpophover1').tooltip();  
</script>

		

            
                 