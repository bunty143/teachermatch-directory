<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<style>
.candidatePortfolioView{
    right: auto;
    padding-top: 0px;
    padding-left: 10px;
    padding-right: 10px;
    padding-bottom: 30px;
}
</style>
<!-- candidate profile div Adding by deepak -->
<div class="modal hide custom-div-border1" id="cgTeacherMasterDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div class='candidatePortfolioView' id="sspfDiv">
	<div class='modal-content modal-border'>		
		<div class='modal-header'> 
		<button type='button' class='close' onclick='showProfileContentCloseNew();'>x</button>
	    <h3 id="candidateName"></h3>
		</div>
         <div id="cgTeacherBodyDiv">
         </div>
   </div>
</div>
</div>

<!-- Start Teacher Profile New-->
<div  class="modal hide" id="myModalProfileVisitHistoryShowNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width:647px;" id="mydivNew">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopoverNew()">x</button>
		<h3 id="myModalLabel">Teacher Profile Visit History</h3>
	</div>
	<div class="modal-body" style="max-height: 400px;overflow-y: scroll;padding-right: 18px;">		
		<div class="control-group">
			<div id="divteacherprofilevisithistoryNew" class="">
		    		        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopoverNew()">Close</button> 		
 	</div>
  </div>
 </div>
</div>
<!-- End Teacher Profile New-->
<!--  Adding by deepak -->
<div  class="modal hide"  id="myModalJobListNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 980px;">
	<div class="modal-content">		
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="setPageFlagNew()">x</button>
		<h3 id="myModalLabel">Job Detail</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="divJobNew" class="table-responsive" style=" margin-bottom:15px;min-width:920px;overflow-x:hidden;">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setPageFlagNew()">Close</button> 		
 	</div>
</div>
</div></div>

<div  class="modal hide"  id="editSaveCustomQuestion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 480px;">
	<div class="modal-content">		
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeSaveCustomDiv()">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			Changes Saved Successfully!
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="closeSaveCustomDiv()">Close</button> 		
 	</div>
</div>
</div></div>
<!--  End  -->