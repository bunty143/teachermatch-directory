<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/AssessmentAjax.js?ver=${resourceMap['AssessmentAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/InventoryInvitationsAjax.js?ver=${resourceMap['InventoryInvitationsAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/inventoryinvitations.js?ver=${resourceMap['js/inventoryinvitations.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />

<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.Ajax']}"></script> 

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
            
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        //width: 840,
         width: 945,
        minWidth: null,
        minWidthAuto: false,
       // colratio:[150,100,85,85,70,60,290], // table header width
       // colratio:[220,100,85,85,60,290],
       //colratio:[320,115,98,85,70,240],
        colratio:[250,100,110,93,82,70,240],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
</script>
<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Inventory Invitations</div>	
         </div>
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div>
	<div class="row top5">
		<div class="col-sm-8 col-md-8">		                         
	 		<div class='divErrorMsg' id='errordiv' ></div>
		</div>
	</div>
	<div class="row top10">
		<div class="col-sm-3 col-md-3">     
        	<label>OrgType<span class="required">*</span></label>
            <select class="form-control " id="orgType">
				<option value="0">Select OrgType</option>
				<option value="D">D</option>
			</select>			
		</div>
		<div class="col-sm-3 col-md-3">             
			<label>Inventory Type<span class="required">*</span></label>
			<select class="form-control " id="assessmentType" onchange="getGroups(this.value);">
				<option value="0">Select Inventory Type</option>
				<option value="4">IPI</option>
				<option value="3">Smart Practices</option>
			</select>
		</div>
		<div class="col-sm-3 col-md-3">     
        	<label>Group Name<span class="required">*</span></label>
            <select name="assessmentGroupDetails" id="assessmentGroupDetails" class="form-control" >
				<option value="0">Select Group</option>
			</select>			
		</div>
		<div class="col-sm-3 col-md-3">
       		<label id="captionDistrict">District Name<span class="required">*</span></label>
			<span>
				<input value="" type="text" maxlength="255" id="districtName" name="districtName" class="help-inline form-control"
			    		onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
						onkeyup="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
						onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');" onkeypress="return inputOnEnter(event);"/>
			</span>
			<input type="hidden" id="districtHiddenlId"/>
			<div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
		</div>
	</div>                 
</div>

<div class="row top20">
	<div class="col-sm-3 col-md-3"></div>
	<div class="col-sm-5 col-md-5 importborder" style="margin-left: 15px;margin-right: 15px;">
		<form id='teacherUploadServlet' enctype='multipart/form-data' method='post' target='uploadFrame' action='inventoryInvitationsServlet.do' class="form-inline">
		<iframe id="uploadFrameID" name="uploadFrame" height="0" width="0" frameborder="0" scrolling="yes" sytle="display:none;"></iframe>
			<table cellpadding="5" align="center" >
				<c:if test="${entityType ne 1}">
					<c:set var="showOrHide" scope="session" value="none"/>
				</c:if>
				<c:if test="${entityType eq null || entityType eq '' || entityType eq 1}">
					<c:set var="showOrHide" scope="session" value="fixed"/>
				</c:if>
				<tr><td><label>Candidate Details<span class="required">*</span></label></td><td><input name="teacherfile" id="teacherfile" type="file"></td></tr>	
				<tr><td><button class="btn btn-primary fl" type="button" onclick="return validateInvitations();" style="width: 100px;">Import <i class="icon"></i></button></td></tr>
			</table>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-sm-3 col-md-3"></div>
	<div class="col-sm-8 col-md-8 mt10" style="margin-left: 5px;margin-right: 5px;">
			<table>
			<tr><td></td><td class="required">NOTE:</td></tr>
			<tr><td valign="top">*</td><td> Make sure that first row of excel file (xls/xlsx) has the column names and subsequent rows has data.</td></tr>
			<tr><td valign="top">*</td><td>Excel file must have following column names -</td></tr>
			<tr><td></td><td>${job_Id_discription}</td></tr>
			<tr><td></td><td>user_first_name</td></tr>
			<tr><td></td><td>user_last_name</td></tr>
			<tr><td></td><td>user_email</td></tr>
			<tr><td valign="top">*</td><td>Column names can be in any order and must match exactly as above.</td></tr>
			<tr><td valign="top">*</td><td>Column names are case insensitive.</td></tr>
		</table>
	</div>
</div>
              
<div style="display:none;" id="loadingDiv">
	<table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>

<script type="text/javascript">
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
$('#iconpophover3').tooltip();
$('#iconpophover4').tooltip();
</script>
