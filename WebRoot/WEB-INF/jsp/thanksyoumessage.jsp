<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="row offset4 msgPage">
	<!--<div class="span8 mt30">
			You have been successfully registered with TeacherMatch. <br/>We have sent you an email with login details and an authentication link.<br/> 
			Please check your email to authenticate. 
			<br/><a href="signin.do">Click here</a> to go back.
		</div>
	 -->
	<div class="span8 mt30" style="text-align: left;">
			<b><spring:message code="msgThnkForUrConfi"/></b>
	</div>
</div>

<div class="modal hide" id="errorMessagePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="ok();"><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>
<script>
var msg = "${msg}";
$(document).ready(function(){
	if(msg!=null && msg!="")
	{
		$('#errorMessagePopup .modal-body').html(msg);
		$('#errorMessagePopup').modal('show');
	}
});

function ok()
{
	window.location.assign("http://www.teachermatch.org");
}
</script>