<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resourceMap['AssessmentCampaignAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/assessment-campaign.js?ver=${resourceMap['js/assessment-campaign.js']}"></script>
<script type='text/javascript' src="js/tmhome.js?ver=${resourceMap['js/tmhome.js']}"></script>

<script type='text/javascript'>
if("${param.w}"!='')
{
	var msg ="You have been returned to the Dashboard because you did not acknowledge that you timed out. Failure to respond to a question within the time limits again may result in timed out status."
	var nextMsg =" When you are ready to complete EPI without interruption and within the time parameters, click the Resume EPI on the Dashboard.";
	if("${param.w}"==1){
		if("${param.attempt}"==1){
			msg = "<b>You Did Not Take Action!</b><br>";
			nextMsg = "You have been returned to your Dashboard because you did not acknowledge your <b>FIRST </b><font color='red'>&#8220;Timed Out&#8221;</font> alert. You have <b>ONE MORE</b> attempt. Be sure to respond to questions within the <font color='red'>75-second</font> time limit or you may get a <font color='red'>&#8220;Timed Out&#8221;</font> message again. When you are ready to complete the <b>EPI</b> without interruption, click <b>&#8220;Resume EPI&#8221;</b> on your Dashboard.";
		}
		else if("${param.attempt}"==2){
			msg = "<b>Are you still here?</b><br>";
			nextMsg = "You have been returned to your Dashboard because you did not acknowledge your <b>SECOND </b><font color='red'>&#8220;Timed Out&#8221;</font> alert. You have <b>ONE FINAL</b> attempt. Be sure to respond to questions within the <font color='red'>75-second</font> time limit or you may get your last <font color='red'>&#8220;Timed Out&#8221;</font> message. After that, you will need to wait 12 months before taking the EPI again. When you are ready to complete the <b>EPI</b> without interruption, click <b>&#8220;Resume EPI&#8221;</b> on your Dashboard.";
		}
	}
	else if("${param.w}"==2){
		msg ="You have been returned to the Dashboard because you did not acknowledge that you timed out. Failure to respond to a question within the time limits again may result in timed out status."
		nextMsg =" When you are ready to complete Job Specific Inventory without interruption and within the time parameters, click the Complete Now icon to the right on Job Specific Inventory on the Dashboard. If you have multiple incomplete Job Specific Inventories, it will display you a list of incomplete Job Specific Inventories. Click the Complete Now icon against the Job Title.";
	}
	showInfo(msg+nextMsg);
}
function showInfo(msg)
{
	//$("#myModalv").css({ top: '60%' });
	
	$('#warningImg1').html("<img src='images/info.png' align='top'>");
	$('#message2showConfirm1').html(msg);
	$('#nextMsg').html("");
	
	var onclick = "onclick=window.location.href='epiboard.do'";
	
	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+"><spring:message code="btnOk"/></button>");
	$('#vcloseBtn').html("<span "+onclick+" >x</span>");
	$('#myModalv').modal('show');
	
}
</script>

<c:if test="${jobOrder.jobId>0}">
	<script language="javascript">	
		var url="${jobOrder.exitURL}";
		if(url.indexOf("http")==-1)
		url="http://${jobOrder.exitURL}";
		
		var showurl="";
		if(url.length>70)
		{
		  for(var i=0;i<url.length;i=i+70)
		  {
			  showurl=showurl+"\n"+url.substring(i, i+70)
		  } 
		}
		
		var sts = "0";
		var mf="${mf}";
		//alert(mf);
		var divMsg="Thank you for completing";
		if(mf=='n')
		{
			divMsg="You have not completed";
			sts = "2";
		}
		
		divMsg=divMsg+" the TeacherMatch part of the job application. You will be directed to <a href='"+url+"' target='_blank'>"+showurl+"</a> in a new browser window, however please note that your existing TeacherMatch session is also available in your current browser window.";
		showInfoAndRedirect(sts,divMsg,'',url,1);
		
		function redirectTo()
		{
			window.location.href='epiboard.do';
			window.open(url,"TeacherMatch","toolbar=yes,location=yes,scrollbars=yes,menubar=yes");
		}
		
		function showInfoAndRedirect(sts,msg,nextmsg,url,epiFlag)
		{
			//$("#myModalv").css({ top: '60%' });
			//$("#myModalv").css({ width: '50%' });
			var img="info";
			if(sts==1)
				img = "warn";
			else if(sts==2)
				img = "stop";
			
				$('#warningImg1').html("<img src='images/"+img+".png' align='top'>");
			$('#message2showConfirm1').html(msg);
			$('#nextMsg').html(nextmsg);
			
			var onclick = "";
			if(url!="")
				onclick = "onclick='redirectTo()'";
			
			
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+"><spring:message code="btnOk"/></button>");
			$('#vcloseBtn').html("<span "+onclick+" >x</span>");
			$('#myModalv').modal('show');
		}
	</script>
</c:if>

<div class="row">
	<div class="col-sm-9 col-md-offset-2  mt30" style="text-align: justify">
		<div class="control-group mt25">
			<div class="controls" style=" text-align:left">
				<c:choose>
					<c:when test="${baseStatus eq 'comp'}"> 
						<b><spring:message code="msgCompleted"/></b>
					</c:when>
					<c:when test="${baseStatus eq 'vlt'}">
						<b><spring:message code="optTOut"/></b>
					</c:when>
					<c:when test="${baseStatus eq 'start'}">
						${assessmentDetail.splashInstruction1}
						<br/><br/>
						<c:if test="${baseStatus eq 'start'}">
							<button class="btn fl btn-primary" type="submit" onclick="return checkInventory('0',${epiJobId});"><spring:message code="btnStartEPI"/> <i class="icon"></i></button>
						</c:if>
					</c:when>
					<c:when test="${baseStatus eq 'icomp'}">
						${assessmentDetail.splashInstruction1}
						<br/><br/>
						<c:if test="${baseStatus eq 'icomp'}">
							<button class="btn fl btn-primary" type="submit" onclick="return checkInventory('0',${epiJobId});"><spring:message code="btnResume"/> <i class="icon"></i></button>							
						</c:if>
					</c:when>
				</c:choose>			                
	        </div>			            
		</div>		
	</div>
</div>
		
<div style="display:none;" id="loadingDivInventory">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'><spring:message code="lblInventBengLoaded"/></td></tr>
	</table>
 </div>