<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/smartpracticesassessment.js?ver=${resourceMap['js/smartpracticesassessment.js']}"></script>
<script type="text/javascript" src="dwr/interface/SmartPracticesAssessmentAjax.js?ver=${resourceMap['SmartPracticesAssessmentAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css"
	href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />

<!-- 
<style>
div.t_fixed_header div.body {
	overflow-x	: auto;
}
</style>
-->
<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});


function applyScrollOnTblVisit()
{
  
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridSmartPractices').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
       // height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[120,130,65,60,60,130,90,100,50,70,70],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

function applyScrollOnPrintTable()
{
try{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridNoblePrint').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 300,
         width: 940,
        minWidth: null,
        minWidthAuto: false,
       colratio:[120,130,65,60,60,130,90,100,50,70,70],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
        }catch(e){alert(e);}
}

</script>

<style>
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
</style>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/districtjobs-orders.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Smart Practices Assessment</div>	
         </div>			
		 
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>

<div class="row " >
			 	<div class="col-sm-4 col-md-4">
			 	 <label>
				    First Name
				  </label>
				  <input id="firstName" name="firstName" maxlength="50" class="help-inline form-control" type="text" />
			    </div>
			     <div class="col-sm-4 col-md-4">
			 	 <label>
				    Last Name
				  </label>
				  <input id="lastName" name="lastName" maxlength="50" class="help-inline form-control" type="text" />
			    </div>
		     <div class="col-sm-4 col-md-4">
		 	 <label>
			    Email Address
			  </label>
			  <input id="emailAddress" name="emailAddress" maxlength="50" class="help-inline form-control" type="text" />
		    </div>
		  	    
	    </div>
	    
	      <div class="row " >
			 	<div class="col-sm-4 col-md-4">
			 	 <label>
				    Assessment Date
				  </label>
				   <input type="text" id="assessmentDate"   name="assessmentDate" value="${assessmentDate}" class="help-inline form-control"></span>
			    </div>
			      <div class="col-sm-2 col-md-2 top25-sm2" style="width: 150px;">
		        <label class=""></label>
		        <button class="btn btn-primary " type="button" onclick=" searchRecordsBySmartPractices();">&nbsp;&nbsp;Search&nbsp;&nbsp; <i class="icon"></i></button>
		   </div>
			    		    
	    </div>
        
         
         <div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
    
              <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
			 </iframe>  
		  </div> 
    
    <input type="hidden" id="entityType" name="entityType" value="${userMaster.entityType}">
    <div style="clear: both;"></div>
     <br/>
   
          
   
      
         <table class="marginrightForTablet">
          	      <tr>
		         <td  style='padding-left:5px;'>
				 <a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="generateSmartPracticesEXL();"><span class='icon-file-excel icon-large iconcolor'></span></a> 
				 </td>
				 <td>
				  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='generateSmartPracticesPDF();'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				
				 <td>
					<a data-original-title='Print Preview' rel='tooltip' href="javascript:void(0);" id='printId'  onclick='printSmartPracticesDATA();'><span class='icon-print icon-large iconcolor'></span></a>
				 </td> 
				
			   <!-- <td  style='padding-left:5px;'>
					<a data-original-title='Chart' rel='tooltip' id='chartId' href='javascript:void(0);' onclick="generateGenderChart1();generateEthnicityChart();generateRaceChart();generateEthnicOriginChart();"><span class='fa-bar-chart-o icon-large iconcolor'></span></a>
				   </td>
				  -->
			</tr>
		</table>
     
       	
 
           <div style="clear: both;"></div>
           
           
           
     <!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadHiredCandidates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Smart Practices Assessment</h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Close</button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End -->      
           
<!-- printing Start  -->
    
  <div style="clear: both;"></div>

   <div class="modal hide"  id="printHiredApplicantsDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1000px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   	<h3 id="">Print Preview</h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			<div class="" id="printhiredApplicantsDataTableDiv"  >          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='printSmartPracticesDATA();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    
    
     <div style="clear: both;"></div>

   <div class="modal hide"  id="printmessage1"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   <h3 id="myModalLabel">Teachermatch</h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" >
			 Please make sure that pop ups are allowed on your browser.
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'>Ok</button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    
  
    <!-- printing End -->       
       <div class="TableContent">  	
             <div class="table-responsive" id="divMainHired" >    
           </div>       
       </div>

<script type="text/javascript">
  $('#chartId').tooltip();
  $('#pdfId').tooltip();
  $('#exlId').tooltip();
  $('#printId').tooltip();
   $('#cvsId').tooltip();
</script>

<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("assessmentDate", "assessmentDate", "%m-%d-%Y");
     
    //]]>
</script>


<script type="text/javascript">
	//chkschoolBydistrict();
	searchRecordsBySmartPractices();
</script>



