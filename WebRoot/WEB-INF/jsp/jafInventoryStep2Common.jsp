<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="javax.swing.Spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script>
var $j=jQuery.noConflict();
        $j(document).ready(function() {
       });
</script>
<!-- ------------------------------------------------------ -->
<!-- ------------------------------ Start  EPI ------------ -->
<!-- ------------------------------------------------------ -->
<div id="tm-root"></div>
 
<script type="text/javascript">
gg = function(e) {  
var keycode =(window.event) ? event.keyCode : e.keyCode; 
   switch (keycode) {  
        case 116 : //F5 button
            e.returnValue = false;
            e.keyCode = 0;
            return false; 
        case 82 : //R button
            if (e.ctrlKey) { 
                e.returnValue = false; 
                //event.keyCode = 0;
                e.keyCode = 0;  
                return false; 
            } 
    }
}
document.onkeydown = gg;

function ltrim(stringToTrim) {
return stringToTrim.replace(/^\s+/,"");
}

var epiStandalone = false;
</script>

<c:if test="${sessionScope.epiStandalone}">
	<script language="javascript">
		epiStandalone = true;
	</script>
</c:if>
 
<script language="JavaScript">
	var SealoffSource = true; /* to disable the right mouse button, so the source can not been seen. */
</script>

<script type="text/javascript" language="javascript">

 jobOrder={jobId:dwr.util.getValue("jobId")};
 teacherAssessmentdetail={teacherAssessmentId:dwr.util.getValue("teacherAssessmentId"),assessmentType:dwr.util.getValue("teacherAssessmentType")};
 
  var ff=function() 
  {
  	return "Your work will be lost!"; 
  };
  
 $(window).bind('beforeunload',function()
 {
   var inven="Base";
     if(${teacherAssessmentdetail.assessmentType}==2)
     inven="Job Specific"
    return 'Warning: Guidelines state you must complete the '+inven+' Inventory in one sitting. Please do not close your browser or do not go back.';
});

var totalSkippedQuestions=${totalSkippedQuestions};
var attep=${totalAttempted};
var totalStrikes=${totalStrikes};
</SCRIPT>
 
<script type="text/javascript" language="javascript">

String.prototype.toHHMMSS = function () {
    sec_numb    = parseInt(this);
    var hours   = Math.floor(sec_numb / 3600);
    var minutes = Math.floor((sec_numb - (hours * 3600)) / 60);
    var seconds = sec_numb - (hours * 3600) - (minutes * 60);

    var time = "";
    var hrs="";
    var mins="";
    
  if(hours>1)
   	hrs="<spring:message code='apis'/>";
   if(minutes>1)
    mins="<spring:message code='apis'/>";
    
    if(hours==0)
    	time    = minutes+'<spring:message code="apimin"/>'+mins+' '+seconds+'<spring:message code="apisecs"/>';
    if(hours==0 && minutes==0)
    	time    = seconds+'<spring:message code="apisecs"/>';
    else if(hours>0 && minutes==0)
        time    = hours+'<spring:message code="apihour"/>'+hrs+' '+seconds+'<spring:message code="apisecs"/>';
    else if(hours>0)
        time    = hours+' '+'<spring:message code="apihour"/>'+hrs+' '+minutes+' '+'<spring:message code="apimin"/>'+mins+' '+seconds+' '+'<spring:message code="apisecs"/>';
    return time;
}


var count =${remainingSessionTime}  
var redirect="logout.do"  
  
function countDown()
{  
	 if(count <=0)
	 {  
	  	window.onbeforeunload = function() {};
		$('#warningImg1k').html("<img src='images/stop.png' align='top'>");
		$('#message2showConfirm1k').html("Your session time is up!");
		$('#nextMsgk').html("");
		$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick='finishAssessment(1)' >Ok</button>");
		$('#vcloseBtnk').html("<span onclick='finishAssessment(1)'>x</span>");
		$('#myModalvk').modal('show');
				
	  	setTimeout("finishAssessment(1)", 4000);
	 }
	 else
	 {  
	  	count--;  
	  	document.getElementById("totalTime").innerHTML = count.toString().toHHMMSS();	  
	  	setTimeout("countDown()", 1000)  
	 }  
}  

var intervalID;

function tmTimer(totalSec,yes)
{
	$('#questionTimer').show();
	var mm=0;
	var ss=0;
	if(totalSec>60)
	{
		mm=parseInt(totalSec/60);
		ss=totalSec%60;
	}
	else
	{
		ss=totalSec;
	}
	
	$(document).ready (function () {
	    $('.clock').each(function () {
	       var clock = $(this);
	       
	       clock.html('<span class="min">0</span> min <span class="sec">0</span> <spring:message code="apisecs"/>');
	       
	       $('.min').html(mm);
	       $('.sec').html(ss);
	       
	       
	      if(totalSec<60)
	      {
	       	clock.html('<span class="sec">'+ss+'</span> <spring:message code="apisecs"/>');
	      }
	        intervalID=setInterval(function () {
	            var m = $('.min', clock),
	                s = $('.sec', clock);
	                
	            if ((m.length == 0 || parseInt(m.html())==0) && parseInt(s.html()) <= 0)  
	                {
	                	clock.html('0 secs.');
	                	getAssessmentSectionCampaignQuestionsGrid(0);
	                }
	             
	            if (parseInt(s.html()) <= 0 && parseInt(m.html())>0) {
	                m.html(parseInt(m.html() - 1));
	                s.html(60);
	            }
	            
	            if (parseInt(m.html()) <= 0) 
	                clock.html('<span class="sec">59</span> <spring:message code="apisecs"/>');
	         
	        	s.html(parseInt(s.html() -1));
	         
	        }, 1000);
	    });
	});
}

var timeout;
</script>  

<div class="row">
	<div class="col-sm-12 col-md-12">
		<div class='divErrorMsg' id='errordiv' style="display: block;">				
		</div>
	</div>
</div>	

<%-- 	
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/affidavit.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
     
     		   <c:set var="assType" value="${EPITeacherBaseInventory1}"/>
            <c:if test="${teacherAssessmentdetail.assessmentType==2}">
			<c:set var="assType" value="${teacherAssessmentdetail.assessmentName}" />
			</c:if>
         	<div class="subheading" style="font-size: 13px;">         	
         	${assType}          	
         	</div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
--%>

<div class="row" id="mainDiv">
	<div class="col-sm-6 col-md-6">
		<span id="sectionName" style="display: block;" class="span4 portfolio_Subheading"></span>
	</div>
	
	<div class="col-sm-12 col-md-12 mt10">
		<div id="sessionTimer" style="display: none;text-align: right;" class="epiTimerFontSize"><b><spring:message code="headRemainingTimeForSession"/> <span id="totalTime" class="epiTimerFontColor" ></span></b></div>	
	</div>
	
	<div class="col-sm-12 col-md-12">
		<div id="questionTimer" style="display: none;text-align: right;" class="epiTimerFontSize"><b><spring:message code="msgRemainingTimeForTheFQues"/></b> <span class="clock epiTimerFontColor"><span class="min">0</span>:<span class="sec">0</span> mins</span></div>
	</div>
	
	<c:if test="${teacherAssessmentdetail.assessmentType==1}">
		<div id="questionDiv" class="mt10" style="margin-left:15px;margin-right:15px;">
			<div id="tblGrid" style="border: 1px solid #007AB4;padding:5px;"></div>
		</div>
	</c:if>
	
	<c:if test="${teacherAssessmentdetail.assessmentType==2}">
		<div id="questionDiv" style="margin-left:15px;margin-right:15px; margin-top:20px;">
			<div id="tblGrid" style="border: 1px solid #007AB4;padding:5px;"></div>
		</div>
	</c:if>
		              
	<div class='col-sm-9 col-md-9'></div>
	<div class='col-sm-3 col-md-3'>
		<div class='mt10' style='text-align:right;'>
			<button type='submit' id='sbmtbtn' class='flatbtn' style='width:100px;' onclick='return getAssessmentSectionCampaignQuestionsGrid(1);'><spring:message code="btnNext"/>&nbsp;<i class="icon"></i></button>
		</div>
	</div>
</div>

<div style="display:none;" id="loadingDivInventory">
	 <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'><spring:message code="lblInventBengLoaded"/></td></tr>
	</table>
</div>

<script type="text/javascript" language="javascript">

if((parseInt(attep)+totalStrikes)>3)
{
	finishAssessment(0);
}
//checkAssessmentDone(${teacherAssessmentdetail.teacherAssessmentId});

</script>
<c:if test="${teacherAssessmentdetail.assessmentSessionTime>=0}">
	<script type="text/javascript" language="javascript">
	countDown();  
	$("#sessionTimer").show();
	
	</script>
</c:if>
<!-- ------------------------------------------------------ -->
<!-- ------------------ End EPI --------------------------- -->
<!-- ------------------------------------------------------ -->