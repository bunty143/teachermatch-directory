<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/ManageSchoolAjax.js?ver=${resourceMap['SchoolAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/manageschools.js?ver=${resourceMap['js/manageschools.js']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<link href="css/tmcommon.css?ver=${resourceMap['css/tmcommon.css']}"  rel="stylesheet" type="text/css">
<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});
function applyScrollOnTbl(){
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[250,150,250,150,145],       
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

</script>
<style>
	.hide
	{
		display: none;
	}
</style>


<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headManageSchool"/></div>	
         </div>	
         <div class="pull-right add-employment1">		 	
			<a href="javascript:void(0);" onclick="return showschoolDiv()"><spring:message code="lnkAddSchl"/></a>
		 </div>			
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>	
          
<input type="hidden"  id="MU_EntityType" name="MU_EntityType" value="${userMaster.entityType}">	
<div  class="modal hide"  id="myModalUpdateMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
 <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel"><spring:message code="headManageSchool"/></h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="updateMsg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" class="close"><spring:message code="btnOk"/></button> 		
 		</div>
	</div>
  </div>
</div>
<div  class="modal hide"  id="myModalactMsgShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='confirmDeactivate'><spring:message code="msgAreYuSuWantToDeActShol"/>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="activateDeactivateSchoolstart()"><spring:message code="btnYes"/></button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true" class="close"><spring:message code="btnNo"/></button>
 	</div>
 	<input type="hidden" id="entitystat" name="entitystat">
 	<input type="hidden" id="actschool" name="actschool">
 	<input type="hidden" id="diststat" name="diststat">
 	
</div> 	
</div>
</div>


<div class="row top15">
     <div class="col-sm-5 col-md-5">            
          <label><spring:message code="lblSchoolName"/></label>
            
					<input type="text" id="schoolName" maxlength="100"
						name="schoolName" class="form-control" placeholder=""
						onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
						onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
						onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');" 
					/>	
					<input type="hidden" id="schoolName" name="schoolName"/>
       				<input type="hidden" id="schoolId" name="schoolId" value="0"/> 	
       		<input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>					
			<div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
        </div>
        <div class="col-sm-5 col-md-5"> 
        <label class=""><spring:message code="lblStatus"/></label>
         <select id="status" name="status" class="form-control">
            <option value=""><spring:message code="optAllSchool"/></option>
            <option value="A" selected><spring:message code="optActSchool"/></option>
            <option value="I"><spring:message code="optInactiveSchool"/></option>
           </select>         
        </div>
        <div class="col-sm-2 col-md-2 top25-sm22">
          <button  class="btn fl btn-primary" type="button" onclick="return displaySchoolMasterRecords(<c:out value="${userMaster.entityType}"/>)"><spring:message code="btnSearch"/>&nbsp;<i class="icon"></i></button>	
        </div>
</div>


  <div class="TableContent top15">        	
            <div class="table-responsive" id="divMain">          
                	         
            </div>            
  </div> 

    <br>
	<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
	<div class="hide" id="addschoolDiv">
		<div class="row">
			<div class="col-sm-5 col-md-5">
			 <div class='divErrorMsg' id='errordiv' style="display: block;margin-bottom: 10px;"></div>	
			</div>
		</div>
		<div class="row" >
			<div class="col-sm-5 col-md-5">
			<input type="text" class="form-control" id="enterSchoolName">
			</div>
		</div>
		<br/>
		<div class="row top5">
		<div class="col-sm-4 col-md-4">
		 <button id="saveSchool" style="margin-top: -4px; display: none;" class="btn fl btn-primary" type="button" onclick="return saveSchool(<c:out value="${userMaster.entityType}"/>)">Save&nbsp;<i class="icon"></i></button><a ></a>	
		 <button id="updateSchool" style="margin-top: -4px;display: none;" class="btn fl btn-primary" type="button" onclick="return updateSchool(<c:out value="${userMaster.entityType}"/>)">Update&nbsp;<i class="icon"></i></button><a ></a>	
		&nbsp;&nbsp;&nbsp;&nbsp;<a  href="javascript:void(0)" onclick="cancelsaveuser();" class="top10">Cancel</a>
		</div>
		</div>
	</div>
	
<!-- END -->
<script type="text/javascript">
	displaySchoolMasterRecords(<c:out value="${userMaster.entityType}"/>);
</script>
                 
                 