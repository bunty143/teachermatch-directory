<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/addeditbatchjoborder.js?ver=${resourceMap['js/addeditbatchjoborder.js']}"></script>
<script type='text/javascript' src="js/certtypeautocomplete.js?ver=${resourceMap['js/certtypeautocomplete.js']}"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>

<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<link rel="stylesheet" href="css/dialogbox.css" />

<script type="text/javascript" src="dwr/interface/PFCertifications.js?ver=${resourceMap['PFCertifications.ajax']}"></script>
<script type='text/javascript' src="js/teacher/pfCertifications.js?ver=${resourceMap['js/teacher/pfCertifications.js']}"></script>
<script type='text/javascript' src="js/teacher/pfAutoCompCertificate.js?ver=${resourceMap['js/teacher/pfAutoCompCertificate.js']}"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<div id="displayDiv"></div>
<script>
function applyScrollOnTblSchool_List(){
		var $j;//=jQuery.noConflict(false);
        jQuery(document).ready(function($j) {
        $j('#tblGridschool_list').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 785,
        minWidth: null,
        minWidthAuto: false,
        colratio:[188,200,270,127],	 
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,      
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        }); 
	   });
	 
      
}

</script>

<script>
	function addCertification()
	{
		//alert(" aaaaaaaaaa"+$('#districtOrSchooHiddenlId').val());
		$("#addCertificationDiv").fadeIn();
		//$("#stateMaster").val("");
		//alert(" entityType : "+$('#entityType').val()+" districtORSchoolName : "+$('#districtORSchoolName').val())
		if($('#entityType').val()==1 && $('#districtORSchoolName').val()=="")
		{
			$("#stateMaster").val("");
		}
		else
		{
			/* ===== GAgan : Foe Da & Sa State ======*/
			//alert(" Else Block ");
			if($('#entityType').val()==2 || $('#entityType').val()==3)
			{
				var optstate = document.getElementById('stateMaster').options;
				for(var i = 0, j = optstate.length; i < j; i++)
				{
					  if($('#stateId').val()==optstate[i].value)
					  {
						  optstate[i].selected	=	true;
						  break;
					  }
				}
			}
			else
			{
				if($('#entityType').val()==1 && $('#districtORSchoolName').val()!="" && $('#batchJobId').val()!="")
				{
					//alert("========= Edit Batch Job Order Mode For Admin  =========");
					getDistrictObjByDistrictId();
				}
			}
		}	
		$("#stateMaster").focus();
		document.getElementById("certType").value="";
		document.getElementById("certificateTypeMaster").value="";
	}
	function addSchool()
	{   $('#errordiv').empty();
		if(document.getElementById("districtOrSchooHiddenlId").value!=""){
			$("#addSchoolDiv").fadeIn();
			$("#schoolName").focus();
			
		}else{
			$('#errordiv').show();
			$('#errordiv').append("&#149; Please enter District<br>");
			$('#districtORSchoolName').focus();
		}
	}
	function cancelCertification()
	{
		$('#errordiv').empty();
		$("#addCertificationDiv").hide();
		document.getElementById("certType").value="";
		document.getElementById("certificateTypeMaster").value="";
	}
	function cancelSchool()
	{
		$("#addSchoolDiv").hide();
		document.getElementById("schoolName").value="";
		document.getElementById("noOfSchoolExpHires").value="";
		document.getElementById("schoolId").value="";	
	}
	function isJobAccepted(isJobAccepted,allSchoolGradeDistrictVal){
		if(isJobAccepted){
			document.getElementById("pkOffered").disabled=true;
			document.getElementById("kgOffered").disabled=true;
			document.getElementById("g01Offered").disabled=true;
			document.getElementById("g02Offered").disabled=true;
			document.getElementById("g03Offered").disabled=true;
			document.getElementById("g04Offered").disabled=true;
			document.getElementById("g05Offered").disabled=true;
			document.getElementById("g06Offered").disabled=true;
			document.getElementById("g07Offered").disabled=true;
			document.getElementById("g08Offered").disabled=true;
			document.getElementById("g09Offered").disabled=true;
			document.getElementById("g10Offered").disabled=true;
			document.getElementById("g11Offered").disabled=true;
			document.getElementById("g12Offered").disabled=true;
			
			if(allSchoolGradeDistrictVal!=1)
			document.getElementById("allSchoolGradeDistrict1").disabled=true;
			
			document.getElementById("allSchoolGradeDistrict2").disabled=true;
			
			if(allSchoolGradeDistrictVal!=3)
			document.getElementById("allSchoolGradeDistrict3").disabled=true;
		}
	}
	function checkValue(val)
	{ 
		document.getElementById("allSchoolGradeDistrictVal").value=val;
		var JobOrderType=document.getElementById("JobOrderType").value;
		if(val==1){
			document.getElementById("allSchoolGradeDistrict1").checked=true;
		}
		if(val!=3){
			$("#addSchoolDiv").hide();
			$("#addSchoolLink").hide();
			$("#showAndHideSchoolDiv").hide();
			document.getElementById("addHiresDiv").style.display='inline';
		}else{
			if(JobOrderType!=3){
				document.getElementById("noOfExpHires").value="";
				document.getElementById("addHiresDiv").style.display='none';
			}
		}
		if(val==3){
			$("#addSchoolLink").fadeIn();
			$("#showAndHideSchoolDiv").fadeIn();
		}
		if(val==2){
			document.getElementById("pkOffered").disabled=false;
			document.getElementById("kgOffered").disabled=false;
			document.getElementById("g01Offered").disabled=false;
			document.getElementById("g02Offered").disabled=false;
			document.getElementById("g03Offered").disabled=false;
			document.getElementById("g04Offered").disabled=false;
			document.getElementById("g05Offered").disabled=false;
			document.getElementById("g06Offered").disabled=false;
			document.getElementById("g07Offered").disabled=false;
			document.getElementById("g08Offered").disabled=false;
			document.getElementById("g09Offered").disabled=false;
			document.getElementById("g10Offered").disabled=false;
			document.getElementById("g11Offered").disabled=false;
			document.getElementById("g12Offered").disabled=false;
		}
		if(val!=2){
			document.getElementById("pkOffered").checked=false;
			document.getElementById("kgOffered").checked=false;
			document.getElementById("g01Offered").checked=false;
			document.getElementById("g02Offered").checked=false;
			document.getElementById("g03Offered").checked=false;
			document.getElementById("g04Offered").checked=false;
			document.getElementById("g05Offered").checked=false;
			document.getElementById("g06Offered").checked=false;
			document.getElementById("g07Offered").checked=false;
			document.getElementById("g08Offered").checked=false;
			document.getElementById("g09Offered").checked=false;
			document.getElementById("g10Offered").checked=false;
			document.getElementById("g11Offered").checked=false;
			document.getElementById("g12Offered").checked=false;
			
			document.getElementById("pkOffered").disabled=true;
			document.getElementById("kgOffered").disabled=true;
			document.getElementById("g01Offered").disabled=true;
			document.getElementById("g02Offered").disabled=true;
			document.getElementById("g03Offered").disabled=true;
			document.getElementById("g04Offered").disabled=true;
			document.getElementById("g05Offered").disabled=true;
			document.getElementById("g06Offered").disabled=true;
			document.getElementById("g07Offered").disabled=true;
			document.getElementById("g08Offered").disabled=true;
			document.getElementById("g09Offered").disabled=true;
			document.getElementById("g10Offered").disabled=true;
			document.getElementById("g11Offered").disabled=true;
			document.getElementById("g12Offered").disabled=true;
		}
		
	}
	
	
	function exitUrlMessage(val)
	{  
		document.getElementById("exitURLMessageVal").value=val;
	}
	function isJobAssessment(val)
	{  
		document.getElementById("isJobAssessmentVal").value=val;
		if(val==2){
			$("#isJobAssessmentDiv").hide();
		}else{
			$("#isJobAssessmentDiv").fadeIn();
		}
	}
	function attachJobAssessment(val)
	{  
		document.getElementById("attachJobAssessmentVal").value=val;
		if(val==2){
			$("#attachJobAssessmentDiv").fadeIn();
		}else{
			$("#attachJobAssessmentDiv").hide();
		}
	}
	
</script>
<style>
	.hide
	{
		display: none;
	}
	.inline
	{
		display: inline;
	}
	.nowrap
	{
		white-space: nowrap
	}
	.divwidth
	{
	float: left;
	width: 40px;
	}
</style>
<script type="text/javascript" src="swf/ZeroClipboard.js"></script>
	<script language="JavaScript">
		var clip = null;
		function init_swf() {
		function $(id) { return document.getElementById(id); }
			clip = new ZeroClipboard.Client();
			clip.setHandCursor( true );
			
			clip.addEventListener('load', function (client) {
				debugstr("Flash movie loaded and ready.");
			});
			
			clip.addEventListener('mouseOver', function (client) {
				// update the text on mouse over
				clip.setText($('copy_text').value);
			});
			
			clip.addEventListener('complete', function (client, text) {
				debugstr("Copied text to clipboard: " + text );
			});
			
			clip.glue( 'd_clip_button', 'd_clip_container' );
		}
	</script>
 <div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/add-editdistrictjob-order.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">
         	 <c:if test="${userMaster.entityType==1 && JobOrderType==2}">
			<spring:message code="headBaJoOdrForDist"/>
				  </c:if>
				  <c:if test="${userMaster.entityType!=1 || JobOrderType==3}">
				 <spring:message code="headBaJoOdrForSch"/>
				  </c:if>
         	</div>	
         </div>
          		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
 </div>



		<div class="top10" onkeypress="return chkForEnterAddEditBatchJobOrder(event);" >
			<div class="row">
						<div class="col-sm-12 col-md-12">			                         
						 	<div class='divErrorMsg' id='errordiv' ></div>
						</div>
			</div>			
			<div class="row">
			<label class="col-sm-12 col-md-12" id="captionDistrictOrSchool"><spring:message code="optDistrict"/> ${DistrictOrSchoolName}<span class="required">*</span></label>
			</div>
			 <div class="row">
			 <div class=" col-sm-7 col-md-7">
             <c:if test="${SchoolDistrictNameFlag==0}">
             	<c:if test="${addBatchJobFlag}">
	             <span>
	             <input type="text" value="${DistrictOrSchoolName}" maxlength="100" id="districtORSchoolName" name="districtORSchoolName" class="help-inline form-control"
	            		 onfocus="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
									onkeyup="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
									onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');"	/>
			      </span>
			     </c:if>  
			     <c:if test="${!addBatchJobFlag}">
			      <input type="hidden" id="districtORSchoolName" value="${DistrictOrSchoolName}"/>
			     </c:if>  
			      <input type="hidden" id="districtOrSchooHiddenlId" value="${batchJobOrder.districtMaster.districtId}"/>
             </c:if>
    		
			<c:if test="${SchoolDistrictNameFlag!=0}">
				<c:if test="${addBatchJobFlag}">
			      ${DistrictOrSchoolName}	
			    </c:if>  
             	<input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
             	<input type="hidden" id="districtORSchoolName" name="districtORSchoolName"/>
             </c:if>
       		 <div id='divTxtShowData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')"  class='result' ></div>			             	
             </div>
             </div>
             
			<input type="hidden" id="windowFunc" name="windowFunc" value="${windowFunc}" />            
			<input type="hidden" id=schoolBatchId name="schoolBatchId" value="${schoolBatchId}" />              
			<input type="hidden" id="copyBatchJobId" name="copyBatchJobId" value="${copyBatchJobId}" />             
			<input type="hidden" id="addBatchJobFlag" name="addBatchJobFlag" value="${addBatchJobFlag}" />             
			<input type="hidden" id="batchJobId" name="batchJobId" value="${batchJobOrder.batchJobId}" />
			<input type="hidden" id="districtHiddenId" name="districtHiddenId" value="${districtHiddenId}" />
			<input type="hidden" id="entityType" name="entityType" value="${entityType}">
			<input type="hidden" id="stateId" name="stateId" value="${stateId}">
            <div style="clear: both;"></div>
            
			<div class="row">
			<div class="col-sm-7 col-md-7">
			<label class=""><spring:message code="lblJoTil"/><span class="required">*</span></label>
			<input type="hidden" id="copy_text" />			
			<input type="text" value="${batchJobOrder.jobTitle}" id="jobTitle" maxlength="200" name="jobTitle" class="form-control">
		
			</div>
			</div>
			


			<div class="hide"  id="hideDateDiv">
			<div class="row">
			      <div class="col-sm-4 col-md-4 mt10">
			        <label><spring:message code="lblPostStDate"/><span class="required">*</span></label>
			        <input type="text" id="jobStartDate" name="jobStartDate"  maxlength="0"  value="${jobStartDate}" class="form-control">	 </div>
			      <div class="col-sm-4 col-md-4 mt10">
			        <label><spring:message code="lblPostEnDate"/><span class="required">*</span></label>
			        <input type="text" id="jobEndDate" name="jobEndDate"   maxlength="0"   value="${jobEndDate}" class="form-control">
				</div>
			  </div>
			</div>
			<div  class="modaljoborder hide"  id="jobOrderSend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
				<div class="modal-header">
			  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="actionForward();"><spring:message code="btnX"/></button>
					<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
				</div>
				<div class="modal-body">		
					<div class="control-group">
						<div class="" style="height:120px;">
					    	<span id="jobOrderText"></span>	        	
						</div>
					</div>
			 	</div>
				<div class="modal-footer">
			 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="actionForward();"><spring:message code="btnOk"/></button> 		
			 	</div>
			</div>	

			<div class="row">
			<div class="col-sm-7 col-md-7"></div>
			<div class="col-sm-5 col-md-5  mt10">
			<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
				<a href="javascript:void(0);" onclick="addCertification();"><spring:message code="lnkAddCerti/LiceN"/></a>
			</c:if>
			</div>                  
			</div>


			<div class="row">
			<div class="col-sm-12 col-md-12">
				<div id="showCertificationDiv"></div>
				<div id="certificationRecords"></div>
				</div>
			</div>
			<div id="addCertificationDiv"  class="hide">
					<div  class="row">
					<div class="col-sm-3 col-md-3"><label><strong><spring:message code="lblSt"/><span class="required">*</span></strong></label>
							<select  class="form-control" id="stateMaster" name="stateMaster" onchange="getPraxis(),clearCertType();">   
								<option value="0"><spring:message code="optSltSt"/></option>
								<c:forEach items="${listStateMaster}" var="st">
					        		<option id="st${st.stateId}" value="${st.stateId}" ${selected} >${st.stateName}</option>
								</c:forEach>				
							</select>	
					</div>
					<div class="col-sm-7 col-md-7">
						<label><spring:message code="lblCert/LiceName"/><span class="required">*</span> <a href="#" id="iconpophover1" rel="tooltip" data-original-title="Name of certification as per certifying body such as a state department of education">
							<img width="15" height="15" alt="" src="images/qua-icon.png"></a>
						</label>
						<input type="hidden" id="certificateTypeMaster" value=""/>
						<input type="text" placeholder="<Certification/Licensure Name>"
							class="form-control"
							maxlength="100"
							id="certType" 
							value=""
							name="certType" 
							onfocus="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
							onkeyup="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');" 
							onblur="hideCertificateType(this,'certificateTypeMaster','divTxtCertTypeData');"/>
						<div id='divTxtCertTypeData' style=' display:none;position:absolute;' onmouseover="mouseOverChk('divTxtCertTypeData','certType')" class='result' ></div>
					</div>
				</div>
				<div class="row top5">
				<div class=" col-sm-1 col-md-1">
					<a href="javascript:void(0);" onclick="validateAddCertification();"><spring:message code="lnkImD"/></a>
				</div>
				<div class=" col-sm-1 col-md-1">
				<a href="javascript:void(0)" onclick="cancelCertification();"><spring:message code="lnkCancel"/></a> 
				</div>				           
				</div>
			</div>



			<div class="row top5">
			<div class="col-sm-3 col-md-3">
			<label><spring:message code="headJobDescrip"/><a href="#" id="iconpophover2" rel="tooltip" data-original-title="Detailed job description that will be displayed to the job applicants on TeacherMatch platform"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></label>
			</div>
			</div>

			<div class="row" id="jDescription">
			<div class="col-sm-10 col-md-10">
			<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>	
			<textarea rows="8" id="jobDescription" name="jobDescription" class="form-control">${batchJobOrder.jobDescription}</textarea>
			</div>
			</div>
			
			<div style="clear: both;"></div>
			<div class="row top10">
				<div class="col-sm-12 col-md-12">		
							<label><spring:message code="lblJoQuali"/><a href="#" id="iconpophover5" rel="tooltip" data-original-title="Detailed job description that will be displayed to the job applicants on TeacherMatch platform"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></label>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-10 col-md-10">	
					<div class="" id="jQualification">
					<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>	
					<textarea rows="8" id="jobQualification" name="jobQualification" class="form-control">${batchJobOrder.jobQualification}</textarea>
					</div>
				</div>
			</div>

			<div class="hide"  id="categoryForSchool">
				<div class="row">
				<label class="col-sm-3 col-md-3 mt10"><spring:message code="lblJoCatN"/><span class="required">*</span></label>
				</div>
				<div class="">
				<select id="jobCategoryId" name="jobCategoryId" class="form-control" onchange="showDivMsg(this.value);">
					  <option value="0"><spring:message code="optSltJoCatN"/></option>
					    <c:if test="${not empty jobCategoryMasterlst}">
				         <c:forEach var="jobCategoryMaster" items="${jobCategoryMasterlst}" varStatus="status">
							<option value="${jobCategoryMaster.jobCategoryId}||${jobCategoryMaster.baseStatus}"><c:out value="${jobCategoryMaster.jobCategoryName}"/></option>
						 </c:forEach>
				        </c:if>
				</select>
				</div>
			</div>

			<div class="row top10">
			<div class="col-sm-3 col-md-3  hide"  id="addHiresDiv" >
				<label><spring:message code="lblOfExpHi"/><span class="required">*</span></label>
				<input type="text" class="form-control  left15-sm" maxlength="3" id="noOfExpHires" name="noOfExpHires" value="${noOfExpHires}"  onkeypress="return checkForInt(event);">
			</div>
			</div>


    		 <input type="hidden" name="allSchoolGradeDistrictVal" id="allSchoolGradeDistrictVal" value="${allSchoolGradeDistrictVal}"/>
	<div class="hide" id="hideForSchool">
	<div class="row col-sm-12 col-md-12 mt10">
<spring:message code="msgWhAdminCanHiCandFrThJoOrdAppPo"/>
	</div>
	<div class="row col-sm-12 col-md-12">
				<label class="radio">
				<input type="radio" name="allSchoolGradeDistrict" onclick="checkValue(1);" id="allSchoolGradeDistrict1" value="1" ${batchJobOrder.allSchoolsInDistrict == '1' ? 'checked' : ''}>
				<spring:message code="msgAllSchInMDist"/>
				</label>
	</div>
	 <div class="row">
	 <div class=" col-sm-12 col-md-12">
	 	<div style="width:70px;float: left;">
                  	<label class="radio">
					<input type="radio"  value="2" id="allSchoolGradeDistrict2" onclick="checkValue(2);" name="allSchoolGradeDistrict" ${batchJobOrder.allGrades == '1' ? 'checked' : ''}>
					Grades 
					</label>
		</div>
		<div class="divwidth" >			
					<label class="checkbox">
					<input type="checkbox" value="1" id="pkOffered" name="pkOffered" ${batchJobOrder.pkOffered == '1' ? 'checked' : ''}>
					PK 
					</label>
		</div>
		<div class="divwidth">				
					<label class="checkbox">
					<input type="checkbox" value="1" id="kgOffered" name="kgOffered" ${batchJobOrder.kgOffered == '1' ? 'checked' : ''}>
					KG
					</label>
		</div>
		<div class="divwidth">				
					<label class="checkbox">
					<input type="checkbox" value="1" id="g01Offered" name="g01Offered" ${batchJobOrder.g01Offered == '1' ? 'checked' : ''}>
					1 
					</label>
		</div>
		<div class="divwidth">				
					<label class="checkbox">
					<input type="checkbox" value="1" id="g02Offered" name="g02Offered" ${batchJobOrder.g02Offered == '1' ? 'checked' : ''}>
					2 
					</label>
		</div>
		<div class="divwidth">				
					<label class="checkbox">
					<input type="checkbox" value="1" id="g03Offered" name="g03Offered" ${batchJobOrder.g03Offered== '1' ? 'checked' : ''}>
					3 
					</label>
		</div>
		<div class="divwidth">				
					<label class="checkbox">
					<input type="checkbox" value="1" id="g04Offered" name="g04Offered" ${batchJobOrder.g04Offered == '1' ? 'checked' : ''}>
					4 
					</label>
		</div>
		<div class="divwidth">				
					<label class="checkbox">
					<input type="checkbox" value="1" id="g05Offered" name="g05Offered" ${batchJobOrder.g05Offered== '1' ? 'checked' : ''}>
					5 
					</label>
		</div>
		<div class="divwidth">				
					<label class="checkbox">
					<input type="checkbox" value="1" id="g06Offered" name="g06Offered" ${batchJobOrder.g06Offered== '1' ? 'checked' : ''}>
					6 
					</label>
		</div>
		<div class="divwidth">				
					<label class="checkbox">
					<input type="checkbox" value="1" id="g07Offered" name="g07Offered" ${batchJobOrder.g07Offered == '1' ? 'checked' : ''}>
					7 
					</label>
		</div>
		<div class="divwidth">				
					<label class="checkbox">
					<input type="checkbox" value="1" id="g08Offered" name="g08Offered" ${batchJobOrder.g08Offered == '1' ? 'checked' : ''}>
					8 
					</label>
		</div>
		<div class="divwidth">				
					<label class="checkbox">
					<input type="checkbox" value="1" id="g09Offered" name="g09Offered" ${batchJobOrder.g09Offered == '1' ? 'checked' : ''}>
					9 
					</label>
		</div>
		<div class="divwidth">				
					<label class="checkbox">
					<input type="checkbox" value="1" id="g10Offered" name="g10Offered" ${batchJobOrder.g10Offered == '1' ? 'checked' : ''}>
					10 
					</label>
		</div>
		<div class="divwidth">				
					<label class="checkbox">
					<input type="checkbox" value="1" id="g11Offered" name="g11Offered" ${batchJobOrder.g11Offered == '1' ? 'checked' : ''}>
					11 
					</label>
		</div>
		<div class="divwidth">				
					<label class="checkbox">
					<input type="checkbox" value="1" id="g12Offered" name="g12Offered" ${batchJobOrder.g12Offered == '1' ? 'checked' : ''}>
					12 
					</label>
		</div>			
         </div>
         </div>
      	<div class="row">
      	<div class=" col-sm-8 col-md-8">
			<label class="radio">
				<input type="radio" name="allSchoolGradeDistrict" id="allSchoolGradeDistrict3" onclick="checkValue(3);" value="3" ${batchJobOrder.selectedSchoolsInDistrict == '1' ? 'checked' : ''} >
				Administrators&nbsp;from&nbsp;Selected&nbsp;School(s)
			</label>
		</div>
		<div class="col-sm-3 col-md-3 left10-sm">		
			<div class="hide" id="addSchoolLink">
				<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
					<a href="javascript:void(0);" onclick="addSchool();"><spring:message code="lnkAddSch"/></a>
				</c:if>	
			</div>
		</div>
		</div>
	

		<div class="hide"  id="addSchoolDiv">
			<div class="row" style="margin-top: 0px;">
				<div class="col-sm-5 col-md-5">
				<label><spring:message code="lblSchoolName"/></label>
				 <input type="text" id="schoolName" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
													onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
													onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');"	/>
							
				 	 <input type="hidden" id="schoolId"/>
					 <div id='divTxtShowData2' style=' display:none;position:absolute;' class='result' ></div>
				</div>
				<div class="col-sm-5 col-md-5">
				<label><spring:message code="lblOfExpHi"/></label>
				<input type="text" class="form-control" maxlength="3" id="noOfSchoolExpHires" name="noOfSchoolExpHires"  onkeypress="return checkForInt(event);" placeholder="">
				
				</div>
			                   
			</div>
				<div class="row top5">
				<div class="col-sm-3 col-md-3">
				<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
				<a  href="javascript:void(0);" onclick="validateAddSchool();"><spring:message code="lnkImD"/></a></c:if>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="cancelSchool();"><spring:message code="lnkCancel"/></a>
				</div>             
			    </div>
		</div>

	 
	
		<div class="row col-sm-12 col-md-12 hide"  id="showAndHideSchoolDiv">
			<div id="showSchoolDiv"></div>
			<div id=schoolRecords></div>
		</div>
		
		 <input type="hidden" name="attachJobAssessmentVal" id="attachJobAssessmentVal" value="${attachJobAssessmentVal}"/>
     	 <input type="hidden" name="districtRootPath" id="districtRootPath" value="${districtRootPath}"/>
     	 <input type="hidden" name="schoolRootPath" id="schoolRootPath" value="${schoolRootPath}"/>  
     	 <input type="hidden" name="DAssessmentUploadURL" id="DAssessmentUploadURL" value="${DAssessmentUploadURL}"/>
     	 <input type="hidden" name="isJobAssessmentVal" id="isJobAssessmentVal" value="${batchJobOrder.isJobAssessment == false || PageFlag==0 ? '2':'1'}"/>
   	
	</div>



	<div class="hide" id="hideForDistrict">
	  <div class="row col-sm-8 col-md-8 mt15">
      	<label class="radio">
       <input type="radio" name="isJobAssessment" onclick="isJobAssessment(2);" id="isJobAssessment1" value="2" ${(batchJobOrder.isJobAssessment == false  || PageFlag==0) ? 'checked' : ''}>
      <spring:message code="msgNoJoSpeciInveIsNed"/>
		</label>
     </div>
	<div class="row col-sm-8 col-md-8 mt10">
	   <label class="radio">
      
     <input type="radio" name="isJobAssessment" onclick="isJobAssessment(1);" id="isJobAssessment2" value="1"  ${(batchJobOrder.isJobAssessment == true) ? 'checked' : ''}>
       	<spring:message code="msgTJoQeqJoSpeciInveTaByApp"/>
     </label>
     </div>
	     <div class="row" id="isJobAssessmentDiv">     	
	   		 <div class="col-sm-12 col-md-12 hide" id="hideDAUploadURL">
		      <label class="radio">
		       <input type="radio" name="attachJobAssessment" onclick="attachJobAssessment(1);" id="attachJobAssessment1" value="1"  ${(batchJobOrder.attachDefaultDistrictPillar == '1'|| PageFlag==0) ? 'checked' : ''}>
		       <spring:message code="msgUsTheDefJoSpeciInveSetAtDist"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="hide" id="ViewDAURL"></span>
		     </label>
		     </div>
		     <div class="col-sm-12 col-md-12 hide" id="hideSAUploadURL">
		      <label class="radio">
		       <input type="radio" name="attachJobAssessment" onclick="attachJobAssessment(3);" id="attachJobAssessment2" value="3" ${batchJobOrder.attachDefaultSchoolPillar == '1' ? 'checked' : ''}>
		      <spring:message code="msgUsTheDefJoSpeciInveSetAtCate"/>l&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="hide" id="ViewSAURL"></span>
		     </label>
		     </div>
		        
		    <div class="col-sm-12 col-md-12">
		      <label class="radio">
		       <input type="radio" name="attachJobAssessment" onclick="attachJobAssessment(2);" id="attachJobAssessment3" value="2" ${batchJobOrder.attachNewPillar == '1' || attachJobAssessmentVal==2 ? 'checked' : ''}>
		       <spring:message code="msgLMAttJoSpeciInve"/>
		     </label>
	  		</div>
	  		<div class="hide" id="attachJobAssessmentDiv">
			<div class="col-sm-12 col-md-12">
			<spring:message code="msgAccptInvenFormatsInclPDF/MS'W/GIF/PNG/JPEG/FMaxF10MB"/>
			<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
			</iframe>
			</div>
			<form id='frmJobAssessmentUpload' enctype='multipart/form-data' method='post' target='uploadFrame' action='jobResumeUploadServlet.do' class="form-inline">
			 <input type="hidden" id="dateTime" name="dateTime"  value="${dateTime}"/>
			 <input type="hidden" id="JobOrderType" name="JobOrderType"  value="${JobOrderType}"/>
			<input type="hidden" id="districtSchoolHiddenId" name="districtSchoolHiddenId" value="${DistrictOrSchoolId}"/>
			<div class="span7"><span><c:if test="${fn:indexOf(roleAccess,'|9|')!=-1}">
			<input name="assessmentDocument" id="assessmentDocument" type="file"></c:if><br><!-- <a href="#" onclick="return clearAssessment()">Clear</a>--></span></div>
			<span class="col-sm-12 col-md-12 hide" id="divAssessmentDocument" name="divAssessmentDocument">
				<label>
					&nbsp;&nbsp;
				</label>
				${batchJobOrder.assessmentDocument}
				<!-- a href="javascript:void(0)" onclick="removeAssessment()">remove</a> -->
			</span>
			</form>
			</div>
	     </div>                         
 	</div>


	<div class="hide" id="showExitURLDiv">
	<div class="row">
	<div class="col-sm-8 col-md-8 mt10">
	<input type="hidden" name="exitURLMessageVal" id="exitURLMessageVal" value="${DistrictExitURLMessageVal}"/> 
	<label  class="radio"><input type="radio" name="exitUrlMessage" onclick="exitUrlMessage(1);" id="exitUrlMessage1" value="1" checked> <spring:message code="msgAfComOfTMBInveAppDrct"/> <a href="#" id="iconpophover4" rel="tooltip" data-original-title="URL address where job applicant can complete the remaining requirements for this job post completion of TeacherMatch base EPI"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></label>
	<input type="text" id="exitURL" maxlength="250" name="exitURL" value="${DistrictExitURL}" class="form-control"/>
	</div>
	</div>
	</div>
	
	<div class="hide" id="showExitMessageDiv">
	<div class="row">
	<div class="col-sm-8 col-md-8 mt10" id="eMessage">
	<label  class="radio">
	<input type="radio" name="exitUrlMessage" onclick="exitUrlMessage(2);" id="exitUrlMessage2" value="2" >
	<spring:message code="msgTmInveLstepJoAppAftrCompl"/><a href="#" id="iconpophover5" rel="tooltip" data-original-title="Message that should be displayed to the job applicant after completion of the TeacherMatch base EPI"><img width="15" height="15" alt="" src="images/qua-icon.png"></a><br> 
	<spring:message code="lblComMsg"/></label>
	<textarea id="exitMessage" name="exitMessage"  maxlength="1000" onkeyup="return chkLenOfTextArea(this,1000);" onblur="return chkLenOfTextArea(this,1000);" class="span8" rows="0">${DistrictExitMessage}</textarea>
	</div>
	</div>
	</div>

	<div class="row top15-sm">
	   	<div class="col-sm-8 col-md-8 mt10 hide" id="hideSave">
	   	<c:if test="${(isAcceptStatus && !addBatchJobFlag && fn:indexOf(roleAccess,'|6|')!=-1) || (JobOrderType==3 && addBatchJobFlag)}">
	      	<button onclick="validateAddEditJobOrder(1);" class="btn btn-large btn-primary">Accept<i class="icon"></i></button>&nbsp;&nbsp;&nbsp;&nbsp;</c:if><c:if test="${addBatchJobFlag && fn:indexOf(roleAccess,'|8|')!=-1}"><button onclick="validateAddEditJobOrder(1);" class="btn btn-large btn-primary"><spring:message code="btnSave"/><i class="icon"></i></button>&nbsp;&nbsp;&nbsp;&nbsp;<button onclick="validateAddEditJobOrder(2);" class="btn btn-large btn-primary" type="button"> <c:if test="${userMaster.entityType==1}">
			<spring:message code="msgSa&SeToDistAdmin"/>
			  </c:if>
			   <c:if test="${userMaster.entityType!=1}">
			  <spring:message code="msgSa&SeToSchAdmin"/>
			  </c:if><i class="icon"></i></button>&nbsp;&nbsp;&nbsp;</c:if><a href="javascript:void(0)" onclick="cancelBatchJobOrder();"><spring:message code="lnkCancel"/></a>
	      </div>
	   </div>	

</div>


<iframe src="" id="iframeJSI" width="100%" height="480px" style="display: none;"></iframe>
<div style="display:none;" id="loadingDiv">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 	</table>
</div>
<div  class="modal hide"  id="EPIMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  ><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headJoCatAlert"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="">
		    	<span id="EPIMsg"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnOk"/></button> 		
 	</div>
</div>	
</div>
</div>	


		<div id='d_clip_container'><div id='d_clip_button'></div></div>
		<script>
			init_swf();
			//$('#districtORSchoolName').focus();
		</script>	
		<c:if test="${PageFlag!=0 ||DistrictExitURLMessageVal!=null }">
        	<script>
        		if(document.getElementById("exitURLMessageVal").value==2){
					document.getElementById("exitURLMessageVal").value=2;
					document.getElementById("exitUrlMessage2").checked=true;
					document.getElementById("showExitURLDiv").style.display='none';
					document.getElementById("showExitMessageDiv").style.display='inline';
					
				}else{
					document.getElementById("exitURLMessageVal").value=1;
					document.getElementById("exitUrlMessage1").checked=true;
					document.getElementById("showExitURLDiv").style.display='inline';
					document.getElementById("showExitMessageDiv").style.display='none';
				}
			</script>	
        </c:if>
       <c:if test="${batchJobOrder.isJobAssessment == false  || PageFlag==0}">
        	<script>
					document.getElementById("isJobAssessmentDiv").style.display='none';
			</script>	
        </c:if>
        <c:if test="${batchJobOrder.isJobAssessment == true }">
        	<script>
					document.getElementById("isJobAssessmentDiv").style.display='inline';
			</script>	
        </c:if>
        
        
        
        <c:if test="${PageFlag==0 && DistrictExitURLMessageVal==null}">
        	<script>
        			document.getElementById("exitURLMessageVal").value=1;
					document.getElementById("exitUrlMessage1").checked=true;
					document.getElementById("showExitURLDiv").style.display='inline';
					document.getElementById("showExitMessageDiv").style.display='none';
			</script>	
        </c:if>
        <c:if test="${batchJobOrder.selectedSchoolsInDistrict == '1'}">
     		 <script>
     		 	document.getElementById("addSchoolLink").style.display='inline';
     		 	document.getElementById("showAndHideSchoolDiv").style.display='inline';
     		 </script>	
		</c:if>
		<c:if test="${DAssessmentUploadURL!=''  && DAssessmentUploadURL!=null}">
		 <script>
			document.getElementById("hideDAUploadURL").style.display='inline';
			document.getElementById("ViewDAURL").style.display='inline';
			document.getElementById("ViewDAURL").innerHTML="<a data-original-title='JSI' rel='tooltip' id='JSIdistrict2' href='javascript:void(0)' onclick=\"showFile('district','${districtHiddenId}','${DAssessmentUploadURL}','JSIdistrict2');${windowFunc}\"><c:out value="${(DAssessmentUploadURL==''||DAssessmentUploadURL==null)?'':'view'}"/></a>";
		 </script>		
		</c:if>
		<c:if test="${SAssessmentUploadURL!='' && SAssessmentUploadURL!=null}">
		 <script>
			
			document.getElementById("isJobAssessmentVal").value=1;
			document.getElementById("isJobAssessment2").checked=true;
			document.getElementById("attachJobAssessmentVal").value=3;
			document.getElementById("attachJobAssessment2").checked=true;
			document.getElementById("isJobAssessmentDiv").style.display='inline';
			
			document.getElementById("hideSAUploadURL").style.display='inline';
			document.getElementById("ViewSAURL").style.display='inline';
			document.getElementById("ViewSAURL").innerHTML="<a data-original-title='JSI' rel='tooltip' id='JSIschool2' href='javascript:void(0)' onclick=\"showFile('school','${DistrictOrSchoolId}','${SAssessmentUploadURL}','JSIschool2');${windowFunc}\"><c:out value="${(SAssessmentUploadURL==''||SAssessmentUploadURL==null)?'':'view'}"/></a>";
		</script>		
		</c:if>
		<c:if test="${batchJobOrder.attachNewPillar == '1'}">
		 <script>
			document.getElementById("attachJobAssessmentDiv").style.display='inline';
		</script>
		</c:if>
		<c:if test="${batchJobOrder.assessmentDocument!=null }">
		 <script>
			document.getElementById("attachJobAssessmentDiv").style.display='inline';
			document.getElementById("divAssessmentDocument").style.display='inline';
			
		</script>
		</c:if>
		<c:if test="${ attachJobAssessmentVal=='2'}">
		 <script>
			document.getElementById("attachJobAssessmentDiv").style.display='inline';
		</script>				
		</c:if>
		<c:if test="${batchJobOrder.attachNewPillar != '1' && attachJobAssessmentVal!='2'}">
		 <script>
			document.getElementById("attachJobAssessmentDiv").style.display='none';
			document.getElementById("divAssessmentDocument").style.display='none';
		 </script>		
		</c:if>
		
		<script type="text/javascript">
			checkValue(${allSchoolGradeDistrictVal});
			displayJobRecords();
			DisplayCertification();
			ShowSchool();
			isJobAccepted(${isJobAcceptedFlag},${allSchoolGradeDistrictVal});
		</script>
		<script type="text/javascript">
		$('#iconpophover1').tooltip();
		$('#iconpophover2').tooltip();
		$('#iconpophover5').tooltip();
		$('#iconpophover4').tooltip();
		$('#iconpophover5').tooltip();
		$(document).ready(function(){
	  		//$('#eMessage').find(".jqte").width(710);
	  	//	$('#jDescription').find(".jqte").width(710);
	  	//	$('#jQualification').find(".jqte").width(710);
		}) 
		</script>

   <script type="text/javascript">//<![CDATA[
	document.getElementById("hideSave").style.display='inline';
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("jobEndDate", "jobEndDate", "%m-%d-%Y");
    //]]>
    
     function jobStartDateDiv(){
          cal.manageFields("jobStartDate", "jobStartDate", "%m-%d-%Y");
     }
     </script>
<script  type="text/javascript" language="javascript" src="js/dialogboxforaction.js"></script>