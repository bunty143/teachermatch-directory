<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<style>
.table th, .table td {
 padding:10px 4px;
}
.table thead > tr > th {
//vertical-align: middle; 
}
</style>

<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src='js/assessment-campaign.js'></script>
<script type="text/javascript" src="dwr/interface/CandidateReportAjax.js"></script>

<script type='text/javascript'>
var isPortal = true;
if("${param.w}"!='')
{
	var msg ="You have been returned to the Dashboard because you did not acknowledge that you timed out. Failure to respond to a question within the time limits again may result in timed out status."
	var nextMsg =" When you are ready to complete EPI without interruption and within the time parameters, click the Resume EPI on the Dashboard.";
	if("${param.w}"==2)
	nextMsg =" When you are ready to complete Job Specific Inventory without interruption and within the time parameters, click the Complete Now icon to the right on Job Specific Inventory on the Dashboard. If you have multiple incomplete Job Specific Inventories, it will display you a list of incomplete Job Specific Inventories. Click the Complete Now icon against the Job Title.";
	showInfo(msg+nextMsg);
}
function showInfo(msg)
{
	//$("#myModalv").css({ top: '60%' });
	
	$('#warningImg1').html("<img src='images/info.png' align='top'>");
	$('#message2showConfirm1').html(msg);
	$('#nextMsg').html("");
	
	var onclick = "onclick=window.location.href='epiboard.do'";
	
	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+">Ok</button>");
	$('#vcloseBtn').html("<span "+onclick+" >x</span>");
	$('#myModalv').modal('show');
	
}
</script>

<c:if test="${jobOrder.jobId>0}">
	<script language="javascript">
		var url="${jobOrder.exitURL}";
		if(url.indexOf("http")==-1)
		url="http://${jobOrder.exitURL}";
		
		var showurl="";
		if(url.length>70)
		{
		  for(var i=0;i<url.length;i=i+70)
		  {
			  showurl=showurl+"\n"+url.substring(i, i+70)
		  } 
		}
		
		var sts = "0";
		var mf="${mf}";
		//alert(mf);
		var divMsg="Thank you for completing";
		if(mf=='n')
		{
			divMsg="You have not completed";
			sts = "2";
		}
		
		divMsg=divMsg+" the TeacherMatch part of the job application. You will be directed to <a href='"+url+"' target='_blank'>"+showurl+"</a> in a new browser window, however please note that your existing TeacherMatch session is also available in your current browser window.";
		showInfoAndRedirect(sts,divMsg,'',url,1);
		
		function redirectToPortal()
		{
			window.location.href='portaldashboard.do';
			window.open(url,"TeacherMatch","toolbar=yes,location=yes,scrollbars=yes,menubar=yes");
		}
		
	</script>
</c:if>

<c:if test="${jobOrder.jobId>0}">
<c:if test="${param.epiFlag ne 'on'}">
<script language="javascript">
var url="${jobOrder.exitURL}";

if(url.indexOf("http")==-1)
url="http://${jobOrder.exitURL}";

var showurl="";
if(url.length>70)
{
  for(var i=0;i<url.length;i=i+70)
  {
	  showurl=showurl+"\n"+url.substring(i, i+70)
  } 
}
var sts = "0";
var mf="${mf}";
//alert(mf);
var divMsg="Thank you for completing";
if(mf=='n')
{
	divMsg="You have not completed";
	sts = "2";
}

divMsg=divMsg+" the TeacherMatch part of the job application. You will be directed to <a href='"+url+"' target='_blank'>"+showurl+"</a> in a new browser window, however please note that your existing TeacherMatch session is also available in your current browser window.";
</script>
<c:if test="${compltd eq 'Y' || param.es eq 1}">
<script language="javascript">
showInfoAndRedirect(sts,divMsg,'',url,2);
</script>
</c:if>
</c:if>
</c:if>




<div id="displayDiv"></div>
<c:if test="${chkRefer}">
<div class="modal hide"  id="divAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" style="z-index: 5000;" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk' onclick="window.location.href='portaldashboard.do'">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="divAlertText" >
			<spring:message code="msgJobRequiresAdditionalInf"/>
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer"> 		
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="window.location.href='portaldashboard.do'"><spring:message code="btnClose"/></button></span> 		
 	</div>
</div>
</div>
</div>
</c:if>

<c:if test="${startEPIflag}">
	<div class="modal hide"  id="myConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >		
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">	  		
			<h3 id="myModalLabel">TeacherMatch</h3>
		</div>
		<div class="modal-body">				
			<div class="control-group">
				<div class="" id="ddd">
					<c:if test="${baseStatus eq 'start'}">
						<spring:message code="MSGYouhavenotstartedyourEPIyet"/>
					</c:if>
					<c:if test="${baseStatus eq 'icomp'}">
						<spring:message code="MSGYouhavenotstartedyourEPIyet1"/>
					</c:if>
				</div>
			</div>
	 	</div> 	
	 	<div class="modal-footer">
	 		<span id="spnRemoveTeacher"><button class="btn btn-large btn-primary" onclick="window.location.href='epiboard.do'"><spring:message code="btnOk"/> <i class="icon"></i></button></span>
	 		<span><button class="btn" data-dismiss="modal" aria-hidden="false"  onclick="window.location.href='logout.do'"><spring:message code="btnClr"/></button></span> 		
	 	</div>
	</div>
	</div>
	</div>
</c:if>
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/dashboard-icon.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lnkDashboard"/></div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div class="row top15">
	<div class="box col-sm-6 col-md-6" id="divMilestones">
		
	</div>
		
	<div class="box col-sm-6 col-md-6" id="divJbInterest">
		
	</div>
	
</div>
 	
<script type="text/javascript" src="js/portaldahsboard.js">
</script>

<script type="text/javascript">
	getPortalMilestones();
	getPortalJobsofIntrest();
	
	$('#iconpophover3').tooltip();
	$('#iconpophoverBase').tooltip({
		toggle: 'toolip',
	    placement: 'right'
	});
	$('#myConfirm').modal('show');
	$('#divAlert').modal('show');
	
</script>