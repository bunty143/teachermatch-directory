<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/QuestAjax.js?ver=${resourceMap['QuestAjax.ajax']}"></script>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src="dwr/util.js"></script>
<script type='text/javascript' src="js/quest.js?ver=${resouceMap['js/quest.js']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" /> 
<script type="text/javascript">	
	displayPowerProfile();
</script> 


<style type="text/css">
#circulardiv
{
border-radius: 50%;
text-align:center;
padding: 15px;

}
.row b
{
color:black;
}
ul
{
    list-style-type: none;
}
.fontleft
{
text-align: left;
}
.fontleft li
{
margin-bottom:5px;
font-size:12px;
font-weight: bold;
}
td
{
width: :10%;
}
.leftalign
{
text-align: :left;
}
.head
{
color: Maroon;
font-weight: bold;
}
.head a
{
color: Maroon;
font-weight: bold;
}
.head a:hover
{
color:#007AB4;
font-weight: bold;
}
</style>
<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
<div class="container">
<center>

<div style="margin-top:18px; padding-right:50px;"> 
<b  class="font25" style="color:black;"><spring:message code="headEasyReExLvlStatus"/></b>
<div style=" font-size: 14px; margin-top:8px;">
<p><spring:message code="msgHowYoCnPoitUnlockExclusReso"/><br/>
<spring:message code="lblGoToYur"/> <a href="userdashboard.do"><b style="color: #007AB5; "><spring:message code="lblPersonalPlng"/> </b></a> <spring:message code="Fdr&CmltInfoL"/><br/>
<spring:message code="lblLMkYurQuestSucc"/></p>
</div>
</div>

<div  class="row" style="width:80%; margin: auto; padding-right:55px; padding-top: 5px;">
<div class="col-sm-3 col-md-3">
<div id="circulardiv">
<img src="images/ppi1.png" width="154px" height="134px"/>
</div>
</div>

<div class="col-sm-3 col-md-3">
<div id="circulardiv">
<img src="images/ppi2.png" width="154px" height="134px"/>
</div>
</div>

<div class="col-sm-3 col-md-3">
<div id="circulardiv">
<img src="images/ppi3.png" width="154px" height="134px"/>
</div>
</div>

<div class="col-sm-3 col-md-3">
<div id="circulardiv">
<img src="images/ppi4.png" width="154px" height="134px"/>
</div>
</div>

<div class="col-sm-12 col-md-12">
<div id="circulardiv" style="padding-left: 40px;">
<img src="images/quest_mark.png" width="550px;"/>
</div>
</div>
</div>

<div id="loadingDivs" ><div style='text-align:center;padding-top:40px;'><img src='images/loadingAnimation.gif' /> <br><spring:message code="lblLoding"/></div></div>
<div id="dispPowerProfileGrid"></div>

</center>
</div>
<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resouceMap['AssessmentCampaignAjax.ajax']}"></script>
<script type='text/javascript' src="js/assessment-campaign.js?ver=${resouceMap['js/assessment-campaign.js']}"></script>
<jsp:include page="../tiles/feedbackandsupport3AfterLogin.jsp" />


