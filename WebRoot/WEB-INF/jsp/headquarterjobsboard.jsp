<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:if test="${redirectTo eq 1}">
<link rel="stylesheet" href="css/dialogbox.css" />

<div id="displayDiv"></div>

<script type="text/javascript" language="javascript" src="js/dialogboxforaction.js"></script>


<script>

var divMsg="You are using Invalid URL. Please check it again or contact to the concerned person";

addDialogContent("dm","TeacherMatch",divMsg,"displayDiv");

showDialog("dm",my,"650","200");

function my(){

window.location.href="signin.do";

}

</script>

</c:if>
<script type="text/javascript" src="dwr/interface/BranchesAjax.js?ver=${resourceMap['BranchesAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/HeadQuarterAjax.js?ver=${resourceMap['HeadQuarterAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.ajax']}"></script>
<script type='text/javascript' src="js/editheadquarter.js?ver=${resourceMap['js/editheadquarter.js']}"></script>
<script type='text/javascript' src="js/headquarterjobsboard.js?ver=${resourceMap['js/headquarterjobsboard.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js"></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
 
<!--<script type='text/javascript' src='js/addeditjoborder.js'></script>-->

<script>
function applyScrollOnTbl()
{
	    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[62,175,90,100,145,160,100,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
     });			
}

 function applyScrollOnTb()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#schoolListGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 200,
        width: 680,
        minWidth: null,
        minWidthAuto: false,
        colratio:[350,330],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTblGeoZoneSchool()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#geozoneSchoolTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 588,
        minWidth: null,
        minWidthAuto: false,
        //colratio:[55,266,70,90,100,105,70,66,151], // table header width
        //mukesh to set the table
        colratio:[380,250],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}
</script>


<link rel="stylesheet" type="text/css" href="css/base.css" />  
<style>
.tmlogo{
	pointer-events: none;
}
</style>   

<input type="hidden" id="gridNo" name="gridNo" value="">
<input id="redirectTo" name="redirectTo" type="hidden" value="${redirectTo}"/>
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="${logoPath}">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 21px;">${Name} Job Board</div>	
         	<div style="font-size: 10px;">${address}</div>
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>


<div  onkeypress="chkForEnterJobSearch(event)" >
  <c:set var="readOnlyCheck" value=""/>
    <div class="row">
	                    <div class="col-sm-4 col-md-4">
	                      <label>HeadQuarter Name</label><br/>
	             			<label>${headQuarterMaster.headQuarterName}</label>		
							
	                    </div>
	                    
	                     <div class="col-sm-4 col-md-4">
	                      <label>Branch Name</label>
           					<input type="text" id="branchName" name="branchName"  class="form-control"
           					onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
							onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
							onblur="hideBranchMasterDiv(this,'branchId','divTxtShowDataBranch');"	/>
							<input  type="hidden" id="branchId" name="branchId" value="">
							<div id='divTxtShowDataBranch' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>							
	                    </div>
	                    
	                    <div class="col-sm-4 col-md-4">
	                      <label>District Name</label>
           					<input type="text" id="districtName" name="districtName"  class="form-control"
           					onfocus="getDistrictAuto(this, event, 'divTxtShowDataDistrict', 'districtName','districtId','');"
							onkeyup="getDistrictAuto(this, event, 'divTxtShowDataDistrict', 'districtName','districtId','');"
							onblur="hideDistMasterDiv(this,'districtId','divTxtShowDataDistrict');"	/>
							<input  type="hidden" id="districtId" name="districtId" value="">
							<div id='divTxtShowDataDistrict' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataDistrict','districtName')" class='result' ></div>							
	                    </div>
	    </div>
    
     		<div class="row">
					<div class="col-sm-4 col-md-4">
						<label>State</label>
						<div id="stateDivId"></div> </span>
					</div>
					
					<div class="col-sm-4 col-md-4">
							<label>Jobs Category</label>
						    <select id="jobCategoryId" name="jobCategoryId" class="form-control">
								<option value="0">All</option>
								<c:forEach var="jbCategory" items="${jobCategoryMasterlst}">
								<option value="${jbCategory.jobCategoryId}">${jbCategory.jobCategoryName}</option>				
								</c:forEach>			
							</select>
					</div>
					
					<div class="col-sm-4 col-md-4">
					<label>ZipCode</label>
						<input id="zipCode" name="zipCode" type="text" class="form-control"/>	
					</div>				
			
				<div class="row">
					<div class="col-sm-6 col-md-6 hide">
							<label id="zoneLable">Zone</label>
							<select id="zone" class="form-control" >							
						 	</select>					 	
					</div>	
					
						<div class="col-sm-6 col-md-6 hide">			
							<c:if test="${empty schoolMaster}">
								<div class="span5">
									<label>
									</label>
								</div>
							</c:if>		
						</div>
					
				</div>
			<div class="col-sm-4 col-md-4 hide">
					<label>
						Subject(s)
					</label>			
					<select id="subjectId" name="subjectId" class="form-control" multiple="multiple" size="4">
						<option value="0">All</option>	
						<c:forEach var="subjectList" items="${lstSubjectMasters}">
							<option value="${subjectList.subjectId}">${subjectList.subjectName}</option>				
						</c:forEach>
					</select>
		    </div>
	    </div>
     
	    <div class="row top10">				
        <input type="hidden" id="isZoneAvailable" name="isZoneAvailable"/>
			<div class="col-sm-3 col-md-3">
				<button class="btn btn-large btn-primary"  onclick="return searchHQJob()">Search <i class="icon"></i>
				</button>
			</div>
			<div class="col-sm-3 col-md-3">&nbsp;</div>
			<div class="col-sm-6 col-md-6" style="text-align: right;"><br>
			Already registered with TeacherMatch, please <a href="javascript:void(0)" onclick="window.open('signin.do');">click here</a> to login</div>	
	    </div>	
</div>	



	<div class="TableContent top15">        	
            <div class="table-responsive"  id="divJobsBoard" onclick="getSortFirstGrid()">          
                	         
            </div>            
    </div> 
   


<div style="display:none;" id="loadingDiv">
    <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>
<br/>Interested in receiving automated alerts for jobs from this District? Please <a href="javascript:void(0);" onclick="showJobAlertForm()">sign-up</a>
<br/>&nbsp;

<input type="hidden" id="hqId" value="${headQuarterMaster.headQuarterId}"/>
<script>
displayState();
searchHQJob();
$('#myModal').modal('hide');
</script>
