
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/StatusUploadTempAjax.js?ver=${resourceMap['StatusUploadTempAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/teacher/statusupload.js?ver=${resourceMap['js/teacher/statusupload.js']}"></script>
<script type='text/javascript' src="js/branchautoSuggest.js?ver=${resourceMap['js/branchautoSuggest.js']}"></script>

	<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Import Status Details</div>	
         </div>         		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
     </div>
	 <input type="hidden" id="headQuarterId" name="headQuarterId" value="${headQuarterId}" />
	 <input type="hidden" id="branchId" name="branchId" value=0 />
	 <input type="hidden" id="districtName" name="districtName" />
	   	<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
	<div class="row top20">
			<div class="col-sm-3 col-md-3" style="max-width: 260px;"></div>
			<div class="col-sm-5 col-md-5 importborder" style="margin-left: 15px;margin-right: 15px;">
			<form id='statusUploadServlet' enctype='multipart/form-data' method='post' target='uploadFrame' action='statusUploadServlet.do' class="form-inline">
			<table cellpadding="5" align="center" >
			<tr><td colspan="2" style="padding-top:10px;">&nbsp;
				 <div class="control-group">			                         
				 	<div class='divErrorMsg' id='errordiv' ></div>
				</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label>Branch Name<span class="required">*</span></label>
				</td>
			</tr>
			<tr><td colspan="2">
				<input type="text" id="branchName" name="branchName" value="${BranchName}" class="form-control"
          						onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
						onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
						onblur="hideBranchMasterDiv(this,'branchId','divTxtShowDataBranch');resetDistrictAndJobCategory();displayJobCategoryByDistrict();"	/>
			<div id='divTxtShowDataBranch' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>	
				</td>
			</tr>	
			
			
			  <c:if test="${entityType ne 1}">
			  
			  <c:set var="showOrHide" scope="session" value="none"/>
			  
			  </c:if>
			  <c:if test="${entityType eq null || entityType eq '' || entityType eq 1}">
			     <c:set var="showOrHide" scope="session" value="fixed"/>
			   </c:if>
			<tr><td ><label>Status Details<span class="required">*</span></label></td><td><input name="statusfile" id="statusfile" type="file"></td></tr>	
			<tr><td><button class="btn btn-primary fl" type="button" onclick="return validateStatusFile();" style="width: 100px;">Import <i class="icon"></i></button></td></tr>
			</table>
			</form>
		</div>
	</div>
<div class="row">	
	<div class="col-sm-3 col-md-3"></div>
	<div class="col-sm-8 col-md-8 mt10" style="margin-left: 5px;margin-right: 5px;">
			<table>
			<tr><td></td><td class="required">NOTE:</td></tr>
			<tr><td valign="top">*</td><td> Make sure that first row of excel file (xls/xlsx) has the column names and subsequent rows has data.</td></tr>
			<tr><td valign="top">*</td><td>Excel file must have following column names -</td></tr>
			
			<tr><td></td><td>talent_email</td></tr>
			
			<tr><td></td><td>job_id</td></tr>
			
			<tr><td></td><td>node_name</td></tr>
			
			<tr><td></td><td>status</td></tr>
			
			<tr><td valign="top">*</td><td>Column names can be in any order and must match exactly as above.</td></tr>
			<tr><td valign="top">*</td><td>Column names are case insensitive.</td></tr>
		</table>
	</div>
</div>
<div style="display:none;" id="loadingDiv">
	<table  align="center" >
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div> 
