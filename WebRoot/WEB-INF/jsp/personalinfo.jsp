
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="javax.swing.Spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

 <script type="text/javascript" src="dwr/interface/MyAjax.js?ver=${resourceMap['MyAjax.js']}"></script>
 <script type="text/javascript" src="dwr/interface/TeacherInfotAjax.js?ver=${resourceMap['TeacherInfoAjax.js']}"></script>
 <script type="text/javascript" src="dwr/engine.js"></script>
 <script type='text/javascript' src='dwr/util.js'></script>
 
 <script type='text/javascript' src="js/dwrajax.js?ver=${resourceMap['js/dwrajax.js']}" ></script>
 <script type="text/javascript" src="dwr/interface/StateAjax.js?ver=${resourceMap['StateAjax.js']}"></script>
 <jsp:include page="/portfolioheader.do"></jsp:include>

<style>
	.hide
	{
		display: none;
	}
	.inline
	{
		display: inline;
	}
	.nowrap
	{
		white-space: nowrap
	}
	
label, input, button, select, textarea {
    font-size: 11px;
    font-weight: normal;
    line-height: 20px;
    }	
</style>

<link rel="stylesheet" type="text/css" href="https://ws1.postescanada-canadapost.ca/css/addresscomplete-2.30.min.css?key=pb91-de65-dk59-gw17" /><script type="text/javascript" src="https://ws1.postescanada-canadapost.ca/js/addresscomplete-2.30.min.js?key=pb91-de65-dk59-gw17"></script>

<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:30px;">
         <div style="float: left;">
         	<img src="images/personalinformation.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headPrsnlInfo"/></div>	
         </div>		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>	
	
	<div class="accordion" id="accordion2">
			<%-- Personal Information section start --%>				
				<div id="collapseOne" style="height:auto;"  onkeypress="return chkForEnterSavePersonalInfo(event);" >
                	<div class="accordion-inner">
							
								<input type="hidden" id='identityVPicture' name='identityVPicture' >
								<form:form id="teacherpersonalinfoform" name="teacherpersonalinfoform" commandName="teacherpersonalinfo" method="post"  onsubmit="return  validatePersonalInfo();">
						
										<div class="row left10">
										<div class="col-sm-12 col-md-12">
											<div class='divErrorMsg' id='errordiv' style="display: block;"></div>					
										</div>
									   </div>
										
									    <div class="row left10">
										<div class="col-sm-2 col-md-2">
											<label><strong><spring:message code="lblSalutation"/>&nbsp;<a href="#" id="iconpophoverSolutation" rel="tooltip" data-original-title='<spring:message code="msgSalutationstrictlyoptional"/>'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>					
											<form:select path="salutation" cssClass="form-control" >
												<option <c:if test="${teacherpersonalinfo.salutation eq 0}">selected</c:if> value="0"></option>   
												<option <c:if test="${teacherpersonalinfo.salutation eq 4}">selected</c:if> value="4"><spring:message code="optDr"/></option>
												<option <c:if test="${teacherpersonalinfo.salutation eq 3}">selected</c:if> value="3"><spring:message code="optMiss"/></option>
												<option <c:if test="${teacherpersonalinfo.salutation eq 2}">selected</c:if> value="2"><spring:message code="optMr"/></option>
												<option <c:if test="${teacherpersonalinfo.salutation eq 1}">selected</c:if> value="1"><spring:message code="optMrs"/></option>
												<option <c:if test="${teacherpersonalinfo.salutation eq 5}">selected</c:if> value="5"><spring:message code="optMs"/></option>											
											</form:select>
										</div>							
										<div class="col-sm-3 col-md-3">
											<label><strong><spring:message code="lblFname"/><span class="required">*</span></strong></label>
											<form:hidden path="teacherId" />											
							    			   <c:choose>              
												  <c:when test="${isKelly eq 'true'}">					
														  <form:input path="firstName" cssClass="form-control" maxlength="40"/>		
													</c:when>					
													<c:otherwise>
													    <form:input path="firstName" cssClass="form-control" maxlength="50"/>		
													</c:otherwise>		
											</c:choose>		
										</div>			
										<div class="col-sm-3 col-md-3">
											<label><strong><spring:message code="lblLname"/><span class="required">*</span></strong></label>
											    <c:choose>              
												  <c:when test="${isKelly eq 'true'}">					
														<form:input path="lastName" cssClass="form-control" maxlength="40" />	
													</c:when>					
													<c:otherwise>
													    <form:input path="lastName" cssClass="form-control" maxlength="50" />
													</c:otherwise>		
											</c:choose>
										</div>
										
										<div class="col-sm-3 col-md-3 hide">
											<label><strong><spring:message code="lblAname"/><span class="required"></span></strong></label>											
											<form:input path="anotherName" cssClass="form-control" maxlength="50" />
										</div>
									    </div>
									    
									<c:if test="${isPortfolioNeeded}">
											<div class="row top15 left10">
												<div class="col-sm-8 col-md-8" >
													<label><strong>
															<spring:message code="lblEEOCSlction"/>
															<a href="#" id="iconpophoverOECQ" rel="tooltip"
															 data-original-title="'<spring:message code="DistrictEqualOpportunityEmployer1"/>'"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
															</strong>
													</label>					
												</div>
											</div>
											
											<div class="row top15 left10" >
											<!-- 
												<div class="col-sm-3 col-md-3">
													<label><strong><spring:message code="lblEthOrig"/></strong></label>
														<c:forEach items="${lstEthnicOriginMasters}" var="org">
																<c:if test="${org.ethnicOriginId eq teacherpersonalinfo.ethnicOriginId.ethnicOriginId}">	        			
											         				<c:set var="checked" value="checked"></c:set>	         			
											        			</c:if>
												        		<label class="radio">		
																	<input type="radio" name="ethnicOriginId" id="ethnicOriginId" ${checked} value="${org.ethnicOriginId}">${org.ethnicOriginName}</br>
																</label>
																<c:set var="checked" value=""></c:set>	         			
														</c:forEach>				
												</div>
												
												<div class="col-sm-3 col-md-3">
													<label><strong><spring:message code="lblEth"/></strong></label>
														<c:forEach items="${lstEthinicityMasters}" var="ecm">
																<c:if test="${ecm.ethnicityId eq teacherpersonalinfo.ethnicityId.ethnicityId}">	        			
											         				<c:set var="checked" value="checked"></c:set>	         			
											        			</c:if>
												        		<label class="radio">		
																	<input type="radio" name="ethnicityId" id="ethnicityId" ${checked} value="${ecm.ethnicityId}">${ecm.ethnicityName}</br>
																</label>
																<c:set var="checked" value=""></c:set>	         			
														</c:forEach>				
													
												</div>		
											-->
												<div class="col-sm-3 col-md-3">
													<label><strong><spring:message code="lblRac"/></strong></label>
														<c:forEach items="${listRaceMasters}" var="race">
																<c:forEach items="${splitRaceId}" var="raceSP">														
																	<c:if test="${race.raceId eq raceSP}">	        			
																		<c:set var="checked" value="checked"></c:set>	         			
																	</c:if>												
																								
																</c:forEach>
												        		<label class="checkbox">		
																	<input type="checkbox" name="raceId" id="raceId" ${checked} value="${race.raceId}" onclick="manageRaces(this,false);">${race.raceName} <a href="#" id="iconpophoverRace${count.index}" rel="tooltip" data-original-title="${race.description}"><img src="images/qua-icon.png" width="15" height="15" alt=""></a><br/>
																</label>
																<c:set var="checked" value=""></c:set>	         			
														</c:forEach>				
													
												</div>	
												<script type="text/javascript">
												function manageRaces(dis,flg)
												{
												var checkBox = document.getElementsByName("raceId");
													if(dis.value=="6" && dis.checked==true)
													{
														for(i=0;i<checkBox.length;i++)
															checkBox[i].checked=flg;
															
														dis.checked=true;
													}else
													{
														 for(i=0;i<checkBox.length;i++)
														 {	
														 	if(checkBox[i].value=="6" && dis.checked==true)
														 	{
														 		checkBox[i].checked=false;
														 		break;
														 	}
														 }
													}
												}
												</script>	
												<div class="col-sm-3 col-md-3">
												<label><strong><spring:message code="lblGend"/></strong></label>
														<c:forEach items="${listGenderMasters}" var="gender">
															<c:if test="${gender.genderId eq teacherpersonalinfo.genderId.genderId}">		        			
											         				<c:set var="checked" value="checked"></c:set>	         			
											        		</c:if>	
											        		<label class="radio">	
																<input type="radio" name="genderId" id="genderId" ${checked} value="${gender.genderId}">	${gender.genderName}</br>
															</label>
															<c:set var="checked" value=""></c:set>	
														</c:forEach>	
												</div>
											
											</div>
									</c:if>									
									
															
									<div class="row left10"> 
										<div class="col-sm-2 col-md-2">
											<label><strong><spring:message code="lblZepCode"/>
											<span class="required">*</span></strong></label>
											<form:input 
												Class="form-control"
												path="zipCode"
												onkeypress="return checkForInt(event);"
												onblur="changeCityStateByZip()" 
												maxlength="5" />
										</div>
										
										<div class="col-sm-2 col-md-2" id="divUSAState" style="display: none;">
											<label><strong><spring:message code="lblSt"/><span class="required">*</span></strong></label>
											<form:select path="stateId" Class="form-control" onchange="getCityListByState()">   
											<!--	<option value="">Select State</option>
												<c:forEach items="${listStateMasters}" var="st">
													<c:set var="selected" value=""></c:set>					
									        		<c:if test="${st.stateId eq teacherpersonalinfo.stateId.stateId}">	        			
									         			<c:set var="selected" value="selected"></c:set>	         			
									        		</c:if>									
													<option id="st${st.stateId}" value="${st.stateId}" ${selected} >${st.stateName}</option>
												</c:forEach>
											-->												
											</form:select>
										</div>				
										<div class="col-sm-2 col-md-2" id="divUSACity" style="display: none;">
											<label><strong><spring:message code="lblCity"/><span class="required">*</span></strong></label>
											<form:select path="cityId" Class="form-control">   
												<option id="slcty" value="">Select City</option>
												<c:forEach items="${listCityMasters}" var="city">	
													<c:set var="selected" value=""></c:set>					
									        		<c:if test="${city.cityId eq teacherpersonalinfo.cityId.cityId}">	        			
									         			<c:set var="selected" value="selected"></c:set>	         			
									        		</c:if>							
													<option id="ct${city.cityId}" value="${city.cityId}" ${selected}  >${city.cityName}</option>
												</c:forEach>	
											</form:select>
										</div>
										
										
										<div class="col-sm-2 col-md-2" id="divotherstate" style="display: none;">
											 <label><strong><spring:message code="lblSt/Cnty"/><span class="required">*</span></strong></label>
									    <form:input path="otherState" Class="form-control" maxlength="100" />
										</div>				
										<div class="col-sm-2 col-md-2" id="divothercity" style="display: none;">
											<label><strong><spring:message code="lblCity"/><span class="required">*</span></strong></label>
											<form:input path="otherCity" Class="form-control" maxlength="100" />
										</div>
										
									</div> 

									<div class="row left10">  
										<div class="col-sm-6 col-md-6">
											<label><strong><spring:message code="lblCnty"/><span class="required">*</span></strong></label>
											<form:select path="countryId" Class="form-control" onchange="getStateByCountry('portfolio')">   
												<option value="">Select Country</option>
												<c:forEach items="${listCountryMaster}" var="ctry">
													<c:set var="selected" value=""></c:set>	
													<c:if test="${ctry.countryId eq teacherpersonalinfo.countryId.countryId}">	        			
									         			<c:set var="selected" value="selected"></c:set>	         			
									        		</c:if>				
													<option id="ctry${ctry.countryId}" value="${ctry.countryId}" ${selected} >${ctry.name}</option>
												</c:forEach>				
											</form:select>
										</div>
									</div>
										
									<div class="row top15 left10">
										<div class="col-sm-6 col-md-6">
											<label><strong><spring:message code="lblAddrL1"/><span class="required">*</span></strong></label>
											<form:input path="addressLine1" Class="form-control" maxlength="50" />
										</div>
									</div>
									<div class="row left10">
										<div class="col-sm-6 col-md-6">
											<label><strong><spring:message code="lblAddrL2"/></strong></label>
											<form:input path="addressLine2" Class="form-control" maxlength="50" />
										</div>
									</div>
									<div class="row left10" id="SelectNonUSnumberId">
									<div class="col-sm-6 col-md-6">
										<label><strong><spring:message code="lblphonNumberType"/></strong></label>
<!--										<select id="typeOfPhonId" class="form-control" onchange="getPhonDivMethod();">-->
										<form:select path="phoneTypeUSNonUS" id="typeOfPhonId" Class="form-control" onchange="getPhonDivMethod();">    
											<option value="1" <c:if test="${not empty PhoneTypeUSNonUS && PhoneTypeUSNonUS==1}">selected</c:if>>US/Canada</option>
											<option value="2" <c:if test="${not empty PhoneTypeUSNonUS && PhoneTypeUSNonUS==2}">selected</c:if>>Other</option>
										</form:select>
										</div>
									</div>
										
											<div class="row left10 top10" >
												<div class="col-sm-4 col-md-4" id="NonUSNumberId" style="display: none;">
													<label><strong><spring:message code="lblPhone"/> <a href="#" id="iconpophover1" rel="tooltip" data-original-title='"<spring:message code="tooltipPhoneNo"/>"'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
													<form:hidden path="phoneNumber" Class="form-control" maxlength="20" />
<!--													<input type="text" id="UsPhonNumnerId" name="UsPhonNumnerId" value="${phonNumber}" class="form-control" maxlength="20"/>-->
													<div>
														<div style="float: left;"><input type="text" name="UsPhonNumnerId1" id="UsPhonNumnerId1" class="form-control" maxlength="3"  value="${phonNumber1}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
<!--														<div style="float: left;margin-left: 5px;"><input type="text" name="UsPhonNumnerId2" id="UsPhonNumnerId2" class="form-control" maxlength="3"  value="${phonNumber2}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>-->
														<div style="float: left;margin-left: 5px;"><input type="text" name="UsPhonNumnerId3" id="UsPhonNumnerId3" class="form-control" maxlength="17"  value="${phonNumber3}" style='width:150px; padding-left:10px;' onkeypress="return checkForInt(event);"/></div>
													</div>
											</div>
										
								
									     
											<div class="col-sm-3 col-md-3" style="min-width: 235px;display: none;" id="USNumberDivID" >
												<label><strong><spring:message code="lblPhone"/> <a href="#" id="iconpophover1" rel="tooltip" data-original-title='"<spring:message code="tooltipPhoneNo"/>"'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
												<form:hidden path="phoneNumber" Class="form-control" maxlength="20" />
												<div>
													<div style="float: left;"><input type="text" name="phoneNumber1" id="phoneNumber1" class="form-control" maxlength="3"  value="${ph1}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
													<div style="float: left;margin-left: 5px;"><input type="text" name="phoneNumber2" id="phoneNumber2" class="form-control" maxlength="3"  value="${ph2}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
													<div style="float: left;margin-left: 5px;"><input type="text" name="phoneNumber3" id="phoneNumber3" class="form-control" maxlength="4"  value="${ph3}" style='width:66px; padding-left:10px;' onkeypress="return checkForInt(event);"/></div>
												</div>
											</div>
											
											<div class="col-sm-3 col-md-3" style="min-width: 235px;display: none;" id="mobilenumberDivId" >
												<label><strong><spring:message code="lblMobile"/> <a href="#" id="iconpophover2" rel="tooltip" data-original-title='"<spring:message code="lblMobilePhoneNumber1"/>"'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>					
												<form:hidden path="mobileNumber" Class="form-control" maxlength="20" />
											    <div>
												<div style="float: left;">	<input type="text" name="mobileNumber1" id="mobileNumber1" class="form-control" maxlength="3"  value="${mb1}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
												<div style="float: left;margin-left: 5px;"><input type="text" name="mobileNumber2" id="mobileNumber2" class="form-control" maxlength="3"  value="${mb2}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
												<div style="float: left;margin-left: 5px;"><input type="text" name="mobileNumber3" id="mobileNumber3" class="form-control" maxlength="4"  value="${mb3}" style='width:66px; padding-left:10px;' onkeypress="return checkForInt(event);"/></div>
												</div>
											</div>
										</div>
									<c:if test="${isExpectedSalNeeded}">
									<div class="row left10">	
										<div class="col-sm-3 col-md-3">
										<label>
											<strong><spring:message code="ExptSalary"/></strong>
										</label>
										
										$<form:input path="expectedSalary" Class="form-control" maxlength="8"  onkeypress="return checkForInt(event);" />
										</div>		
									</div>
									</c:if>
								</form:form>							
						<div class="clearfix">&nbsp;</div>
                   	</div>
				</div>
        	
        	<%-- Personal Information section start --%>
        	
        		<input type="hidden" id="divNo" name="divNo" value="">
        		
        	<%-- Verification section start --%>
            <%-- <div class="accordion-group">
			<div class="accordion-heading"> <a href="#collapseTwo" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle plus"> Identity Verification </a> </div>
          	<div class="accordion-body collapse" id="collapseTwo" style="height: 0px;" onkeypress="chkForEnterEmployment(event)">
      			<div class="accordion-inner">
                		<div class="span16 ">
									<div class="row">
										<div class="span">
											<table width="900" border="0">
												<tr>
													<td colspan="2" valign="top">
													 Take a picture of any one of these:
												  </td>
												</tr>
												<tr>
													<td colspan="2" valign="top">
													 <ol type="1" style="list-style-position:inside;">
												        <li>Valid, unexpired, U.S. photo driver license or photo ID card</br></li>
												        <li>Valid, unexpired, license from another country</li>
														<li>Valid, unexpired, United States passport</li>
														<li>Valid, unexpired, active duty, retiree, or reservist military ID card</li> 
														<li>Valid, unexpired, foreign passport</li>
												    </ol>  
												  </td>
												</tr>
												<tr>
													<td valign="top" colspan="2"> </br></br>Previous Picture</br></td>
												</tr>
												<tr>
													<td valign="bottom">
														<div id="teacherPicture"> </div>
													</td>
													<td valign="bottom" align="left">
													  <div id="screen"></div>
													</td>
												</tr>
												<tr><td valign="top" colspan="1" width="50%">&nbsp;</td>
													<td valign="top" colspan="2" align="left" style="white-space:nowrap;">
																<table border="0">
																	<tr><td colspan="2" height="40" valign="middle">
																				<div class='divErrorMsg' id='errorPic' style="display: block;"></div>					
																		</td>
																	</tr>
																	<tr>
																		<td nowrap>
																		 <div  id="takeapicweb"   style="display:none;">
																	    	<button type="button" href="javascript:void(0)" id="shootButton" onclick="takeAPicture();" class="btn btn-large btn-primary">Take a picture <i class="icon"></i></button>&nbsp;
																	      </div>
																	     </td>
																	    <td nowrap>
																		<div id="browsepic"> 
																			<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
																			</iframe>
																			<form id='frmIdentityVerificationUpload' name="frmIdentityVerificationUpload" enctype='multipart/form-data' method='post' target='uploadFrame' action='IdentityVerificationServlet.do' class="form-inline">
																			<input type="hidden" id="teacherIdForPicture" name="teacherIdForPicture"  value="${teacherpersonalinfo.teacherId}"/>
																			<input name="identityVerificationPicture"  id="identityVerificationPicture" type="file" onchange="SubmitForm();">
																			</form>
																		</div>
																		</td>
																		<td>
																			<div id="takeapic"  style="display:none;">
																			<button type="button" href="javascript:void(0)" onclick="tryAgain();" class="btn btn-large btn-primary">Try again <i class="icon"></i></button>&nbsp; 
																				<button type="button" href="javascript:void(0)" onclick="savePicture();" class="btn btn-large btn-primary">Use this picture <i class="icon"></i></button>
																			 </div>
																		 </td>
														   			 </tr>
													   			 </table>
																
															 <div id="buttons">
															   <div  id="usepicweb"  style="display:none;">	
														    	<button type="button" href="javascript:void(0)" id="cancelButton" onclick="tryAgainWeb();" class="btn btn-large btn-primary">Try again <i class="icon"></i></button>
														    	<button type="button" href="javascript:void(0)" id="uploadButton"  onclick="tryAgainWeb();"  class="btn btn-large btn-primary">Use this picture <i class="icon"></i></button>
														       </div>
														  	</div>
														
														</td>
												</tr>
											</table>							
										</div>
									</div>
									<div id="camera1" class="hide"> </div>
						</div>			
                	   	<div class="clearfix">&nbsp;</div>
					</div>

			</div>
			--%>
			<%-- Verification section End --%>			
			<div class="mt30">			
				<button class="btn btn-large btn-primary"  type="button" onclick="return validatePersonalInfo();">
					            <spring:message code="btnSv&Conti"/> <i class="icon"></i>
				</button>
				<br><br>
			</div>
		</div>
         



<%if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>

<!--
<label>Address</label>
 <div class="input-line">
	<label for="street-address"></label>
	<input id="street-address" type="text" placeholder="Street address" autofocus size="50" />
</div> 
<div class="input-line">
	<label for="street-address2"></label>
	<input id="street-address2" type="text" placeholder="" size="50" />
</div>
<div class="input-line">
	<label for="city"></label>
	<input id="city" type="text" placeholder="City" size="50" />
</div>
<div class="input-line">
	<label for="state"></label>
	<input id="state" type="text" placeholder="State/Province" size="50" />
</div>
<div class="input-line">
	<label for="postcode"></label>
	<input id="postcode" type="text" placeholder="Zip/Postcode" size="50" />
</div>
<div class="input-line">
	<label for="country"></label>
	<input id="country" type="text" placeholder="Country" size="50" />
</div>
-->
<link rel="stylesheet" type="text/css" href="https://ws1.postescanada-canadapost.ca/css/addresscomplete-2.30.min.css?key=pb91-de65-dk59-gw17" />
<script type="text/javascript" src="https://ws1.postescanada-canadapost.ca/js/addresscomplete-2.30.min.js?key=pb91-de65-dk59-gw17"></script>

<script type="text/javascript">

//pca.fieldMode.POPULATE
	var fields = [
		{ element: "addressLine1", field: "Line1" },
		{ element: "addressLine2", field: "Line2", mode: pca.fieldMode.POPULATE },
		{ element: "otherCity", field: "City", mode: pca.fieldMode.POPULATE },
		{ element: "otherState", field: "ProvinceName", mode: pca.fieldMode.POPULATE },
		{ element: "zipCode", field: "PostalCode" },
		{ element: "countryId", field: "CountryName", mode: pca.fieldMode.COUNTRY }
	],
	options = {
		//key: "TY49-DT54-WE25-EJ53"
		key: "pb91-de65-dk59-gw17"
	},
	control = new pca.Address(fields, options);
	
var tempCityText = $("#otherCity").val().toUpperCase();                                                                                  
var tempstateId = $("#stateId" + " option").filter(function() { return this.text == tempCityText}).val();                                                                                           
$("#stateId").val(tempstateId).attr("selected","selected");
</script>

    <script type="text/javascript">
     
    	addressComplete.listen('load', function(control) {
    	            control.listen("populate", function (address) {
    	                document.getElementById("line1").value = address.Line1;
    	            });
    	        });
$(document).mouseup(function (e)
{
    var container = $(".pcaitem");
    if (container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {    
	     $(".pcaitem").click(function () {
	        var title = $(this).attr("title");
	        if(title.indexOf(",") > -1){
	        	var splitarray = title.split(",");
	        	
	        	if(splitarray.length>0){
	        		var selectstate = splitarray[splitarray.length-1].trim();	        		        	
	        		var tempstateId = $("#stateId" + " option").filter(function() {  return $(this).attr("shortname") == selectstate}).val();	        	
	        		$("#stateId").val(tempstateId).attr("selected","selected");
	        	}
	        	 
	        }
	    });
    }
});      
    </script>


<%} %>
















	<div style="display:none;" id="loadingDiv">
     <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'><spring:message code="msgYrFileBegUpLod"/></td></tr>
	</table>
   </div>
   <input type="hidden" id="countryCheck" value="">
<script type="text/javascript">

$('.accordion').collapse();
$('.accordion').on('show', function (e) {
$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
});
$('.accordion').on('hide', function (e) {
$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
});
</script>
<script type="text/javascript" src="js/teacher/portfolio.js"></script>			
			
			<div style="display:none; z-index: 5000;" id="loadingDiv" >
				<table  align="center" >
					<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
					<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>					
				</table>
			</div>
			
			


<script type="text/javascript">
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
$('#iconpophover3').tooltip();
$('#iconpophover4').tooltip();
$('#iconpophoverOECQ').tooltip();
$('#iconpophoverSolutation').tooltip();
document.getElementById("firstName").focus();
for(i=0;i<=7;i++)
$('#iconpophoverRace'+i+'').tooltip();

/*
showTeacherPicture(${teacherDetail.teacherId},'${teacherDetail.identityVerificationPicture}'); 
*/

</script>
<script src="assets/webcam/webcam.js"></script>
<script src="assets/js/script.js"></script>

<script type="text/javascript">
getStateByCountry("portfolio");
getPhonDivMethod();
</script>
