<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/JobSpecInventoryAjax.js?ver=${resourceMap['JobSpecInventoryAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resourceMap['AssessmentCampaignAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DashboardAjax.js?ver=${resourceMap['DashboardAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src='js/assessment-campaign.js?ver=${resourceMap["js/assessment-campaign.js"]}'></script>
<script type="text/javascript" src="js/teacher/dashboard.js?ver=${resourceMap['js/teacher/dashboard.js']}"></script>
<script type="text/javascript" src="js/teacher/jobSpecInventory.js?ver=${resourceMap['js/teacher/jobSpecInventory.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 

<script type='text/javascript' src='js/districtquestioncampaign.js?ver=${resourceMap["js/districtquestioncampaign.js"]}'></script>
<script type="text/javascript" src="dwr/interface/PFExperiences.js?ver=${resourceMap['PFExperiences.Ajax']}"></script>
<script type='text/javascript' src='js/teacher/dynamicPortfolio_Experiences.js?ver=${resourceMap["js/teacher/dynamicPortfolio_Experiences.js"]}'></script>
<script type='text/javascript' src='js/teacher/jobcompletenow.js?ver=${resourceMap["js/teacher/jobcompletenow.js"]}'></script>

<!-- Start ... Dynamic Portfolio -->
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictPortfolioConfigAjax.js?ver=${resourceMap['DistrictPortfolioConfigAjax.Ajax']}"></script>
<script type='text/javascript' src='js/teacher/districtportfolioconfig_ExternalJob.js?ver=${resourceMap["js/teacher/districtportfolioconfig_ExternalJob.js"]}'></script> <!-- For External Job Apply -->
<script type="text/javascript" src="js/teacher/dynamicPortfolio_Common.js?ver=${resourceMap['js/teacher/dynamicPortfolio_Common.js']}"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<!-- Academics -->
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PFAcademics.js?ver=${resourceMap['PFAcademics.Ajax']}"></script>
<script type='text/javascript' src='js/teacher/dynamicPortfolio_Academics.js?ver=${resourceMap["js/teacher/dynamicPortfolio_Academics.js"]}'></script>
<script type='text/javascript' src='js/teacher/dynamicPortfolio_AutoCompAcademics.js?ver=${resourceMap["js/teacher/dynamicPortfolio_AutoCompAcademics.js"]}'></script>
<!-- Certifications -->
<script type='text/javascript' src='js/teacher/dynamicPortfolio_Certifications.js?ver=${resourceMap["js/teacher/dynamicPortfolio_Certifications.js"]}'></script>
<script type="text/javascript" src="dwr/interface/PFCertifications.js?ver=${resourceMap['PFCertifications.Ajax']}"></script>
<script type='text/javascript' src='js/teacher/dynamicPortfolio_AutoCompCertificate.js?ver=${resourceMap["js/teacher/dynamicPortfolio_AutoCompCertificate.js"]}'></script>

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        
            
        });
        
function applyScrollOnTbl()
{
	//alert("Hi");
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridJSI').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[195,195,195,195,195], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

</script> 
<jsp:include page="dp_common.jsp"></jsp:include>
<input type="hidden" id="isaffilatedstatushiddenId" name="isaffilatedstatushiddenId" value="0"/>
<input type="hidden" id="savejobFlag" name="savejobFlag" value=""/>	
<input type="hidden" id="completeNow" name="completeNow" value="0"/>
<input type="hidden" id="inventory" name="inventory" value=""/>
<input type="hidden" id="portfolioStatus" name="portfolioStatus" value=""/>
<input type="hidden" id="prefStatus" name="prefStatus" value=""/>	
<input type="hidden" id="jobId" name="jobId" value=""/>
<input type="hidden" id="noCl" name="noCl" value="0"/>
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/status-icon.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headTchJobSpeiInve"/></div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>


<div class="row  mt15">
	<div class="span16"></div>
</div>

 <div class="TableContent top15">        	
            <div class="table-responsive" id="jobInventoryGrid">          
            </div>            
</div> 
<div style="display:none;z-index: 9999999" id="loadingDivJSI">
     <table  align="left" >
 		<tr><td style="padding-top:150px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>
<script type="text/javascript">
	displayJobSpecInventoryGrid();
	//tpJbIEnable();
</script>
<div  class="modal hide"  id="myModalDASpecificQuestions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
		<div class="modal-dialog-for-cgdemoschedule">
	    <div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel"><spring:message code="headMandatoryDecler"/></h3>
		</div>
		<div class="modal-body" style="max-height: 450px;overflow-y: auto;overflow-x: hidden;">
		<span id='textForDistrictSpecificQuestions' class="divlablevalins"><spring:message code="msgDistReqAllAnsOfQues"/></span>
			<div class='divErrorMsg' id='errordiv4question' style="display: block;padding-bottom: 7px;"></div>
			<table width="100%" border="0" class="table table-bordered table-striped" id="tblGrid">
			</table>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn btn-large btn-primary" type="button" onclick="setDistrictQuestions('dashboard');" ><strong><spring:message code="btnConti"/> <i class="icon"></i></strong></button>
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="cancelDSQdiv()"><spring:message code="btnClose"/></button> 		
	 	</div>
</div>
 	</div>
</div>
	
