<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/spendingtimeincandidateprofilereport.js?ver=${resourceMap['js/spendingtimeincandidateprofilereport.js']}"></script>
<script type="text/javascript" src="dwr/interface/SpendingTimeInCandidateProfileReportAjax.js?ver=${resourceMap['SpendingTimeInCandidateProfileReportAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>


<!-- 
<style>
div.t_fixed_header div.body {
	overflow-x	: auto;
}
</style>
 -->
<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});


function applyScrollOnTblVisit()
{
   //alert("11")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridHired').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
       // height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[175,175,160,90,80,78,82,125],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

function applyScrollOnPrintTable()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridHired').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 300,
         width: 940,
        minWidth: null,
        minWidthAuto: false,
        colratio:[190,175,160,85,80,60,90,120],
        //colratio:[175,175,110,120,200,100,85],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

</script>

<style>
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
</style>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/districtjobs-orders.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headAppProfVisit"/></div>	
         	<!--<div class="subheading" style="font-size: 13px;">Time Spent in Applicants Profile</div>	
         --></div>			
		 
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>


          <div class="row">
		       <div class="col-sm-5 col-md-5">
				     <label id="captionDistrictOrSchool"><spring:message code="lblDistrictName"/></label>
				        <c:if test="${DistrictOrSchoolName==null}">
					       <span>
					        <input type="text" maxlength="255" id="districtORSchoolName" name="districtORSchoolName" class="help-inline form-control"
					            		 onfocus="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');chkschoolBydistrict();"
													onkeyup="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');chkschoolBydistrict();"
													onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');chkschoolBydistrict();"/>
							 </span>
							 <input type="hidden" id="districtOrSchooHiddenlId"/>							
				         </c:if>
				    		
							<c:if test="${DistrictOrSchoolName!=null}"> 
							<span>
					        <input type="text" maxlength="255"  class="help-inline form-control" value="${DistrictOrSchoolName}"  disabled="disabled"/>
							 </span>
				             		
				             	
				             	<input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
				             	<input type="hidden" id="districtORSchoolName" name="districtORSchoolName"/>
				             	<input type="hidden" id="districtHiddenlIdForSchool"/>
				            </c:if>
				             <c:if test="${DistrictOrSchoolName==null && DistrictOrSchoolId!=null}">
				              	<input type="hidden" id="districtHiddenlIdForSchool" value="${DistrictOrSchoolId}"/>
				              </c:if>
				               <c:if test="${DistrictOrSchoolName==null && DistrictOrSchoolId==null}">
				              	<input type="hidden" id="districtHiddenlIdForSchool"/>
				              </c:if>
				       		 <input type="hidden" id="JobOrderType" value="${JobOrderType}"/>
				        	 <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
							             	
               </div>
            
	      
			          <c:choose>
			              <c:when  test="${DistrictOrSchoolName==null}">
			               <div class="col-sm-5 col-md-5">
			           </c:when>
		                 <c:otherwise>
		                   <div class="col-sm-5 col-md-5">
		                 </c:otherwise>
		              </c:choose> 
			          <label><spring:message code="lblSchoolName"/></label>
			            <c:choose>
						    <c:when test="${(userMaster.entityType eq 3 && JobOrderType eq 2 && writePrivilegFlag eq false) || (userMaster.entityType eq 3 && JobOrderType eq 3)}">
						      <br/> ${schoolName}
						       	<input type="hidden" id="schoolName" name="schoolName" value="${schoolName}"/>
	         					<input type="hidden" id="schoolId" name="schoolId" value="${schoolId}"/> 
						    </c:when>
						    <c:otherwise>
								<input type="text" id="schoolName" maxlength="100"
									name="schoolName" class="form-control" placeholder=""
									onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
									onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
									onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');" 
								/>	
								<input type="hidden" id="schoolName" name="schoolName"/>
	         					<input type="hidden" id="schoolId" name="schoolId" value="0"/> 
						    </c:otherwise>
						</c:choose>
						<div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
			        </div>
			          <div class="col-sm-2 col-md-2 top25-sm2" style="width: 150px;text-align:right;padding:0px;">
		        <label class=""></label>
		        <button class="btn btn-primary " type="button" onclick="recordsByCandidateSpendingTimeInProfile();">&nbsp;&nbsp;<spring:message code="btnSearch"/>&nbsp;&nbsp; <i class="icon"></i></button>
		   </div>
	
         </div>


	  

 <div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
    
              <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
			 </iframe>  
		  </div> 
    
    <input type="hidden" id="entityType" name="entityType" value="${userMaster.entityType}">
    <div style="clear: both;"></div>
     <br/>
   
          
   
      
         <table class="marginrightForTablet">
		      <tr>
		         <td  style='padding-left:5px;'>
					<a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="generateCandidateSpendingTimeInProfileEXL();"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td>
				  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='generateCandidateSpendingTimeInProfilePDF();'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				
				 <td>
					<a data-original-title='Print Preview' rel='tooltip' id='printId' href='javascript:void(0);' onclick="generateCandidateSpendingTimeInProfilePrintPre();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td>  
			   <!-- <td  style='padding-left:5px;'>
					<a data-original-title='Chart' rel='tooltip' id='chartId' href='javascript:void(0);' onclick="generateGenderChart1();generateEthnicityChart();generateRaceChart();generateEthnicOriginChart();"><span class='fa-bar-chart-o icon-large iconcolor'></span></a>
				   </td>
				  -->
			</tr>
		</table>
     
       	
 
           <div style="clear: both;"></div>
           
           
           
     <!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadHiredCandidates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Profile Visit Report </h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End -->      
           
<!-- printing Start  -->
    
  <div style="clear: both;"></div>

   <div class="modal hide"  id="printHiredApplicantsDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1000px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   	<h3 id=""><spring:message code="headPrintPreview"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			<div class="" id="printhiredApplicantsDataTableDiv"  >          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='printCandidateSpendingTimeInProfileDATA();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    
    
     <div style="clear: both;"></div>

   <div class="modal hide"  id="printmessage1"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   <h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" >
			<spring:message code="msgPlzPopAreAlloued"/>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'><spring:message code="btnOk"/></button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    
  
    <!-- printing End -->       
       <div class="TableContent">  	
             <div class="table-responsive" id="divMainHired" >    
           </div>       
       </div>

<script type="text/javascript">
  $('#chartId').tooltip();
  $('#pdfId').tooltip();
  $('#exlId').tooltip();
  $('#printId').tooltip();
</script>


<script type="text/javascript">
	chkschoolBydistrict();
	
searchRecordsByCandidateSpendingTimeInProfile();
</script>


