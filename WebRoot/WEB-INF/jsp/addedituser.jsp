<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/UserAjax.js?ver=${resourceMap['UserAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/addedituser.js?ver=${resourceMap['js/addedituser.js']}"></script>
<style>
	.hide
	{
		display: none;
	}
</style>
	<script type="text/javascript">
	$( document ).ready(function() {
		$('#addkey').click(function() {
				if ($('#availableContactTypeAdduser option:selected').val() != null) {
					 $('#availableContactTypeAdduser option:selected').remove().appendTo('#attachContactTypeAdduser');
					 $("#availableContactTypeAdduser").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
			         $("#attachContactTypeAdduser").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");      
					 } 
			});
	$('#removeKey').click(function() {
			       if ($('#attachContactTypeAdduser option:selected').val() != null) {
			             $('#attachContactTypeAdduser option:selected').remove().appendTo('#availableContactTypeAdduser');
			            $("#attachContactTypeAdduser").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
			          $("#availableContactTypeAdduser").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");       
						}
			});
			});
	</script>					
							
	<input type="hidden" id="isBracnchExists" value="${isBracnchExists}"/>
	<c:if test="${(userSession.entityType == 5 || userSession.entityType == 6)}">
	<input type="hidden" id="headQuarterId" value="${userSession.headQuarterMaster.headQuarterId}"/>
	</c:if>
	<c:if test="${(userSession.entityType == 6)}">
		<input type="hidden" id="branchId" value="${userSession.branchMaster.branchId}"/>
	</c:if>
	
	<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/add-edituser.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headAdd/EditUser"/></span></div>	
         </div>          			
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
     </div>
	 <c:if test="${not empty userSession}">
		<c:if test="${userSession.entityType eq 3}">	        			
			<c:set var="hide" value=""></c:set>
			<c:set var="hide" value="hide"></c:set>
		</c:if>
	</c:if>
	<div onkeypress="return chkForEnterAddEditUser(event);">	
			<div class="row 10 col-sm-12 col-md-12">			                         
			 	<div class='divErrorMsg' id='errordiv' ></div>
			</div>
			<form:form commandName="userMaster" onsubmit="return validateAddEditUser();" method="post" enctype="multipart/form-data">
				<div class="row mt10">
	            	<div class="col-sm-3 col-md-3 <c:out value="${hide}"/>">
						<label><strong>Entity Type</strong></label>
						<select class="form-control" id="AEU_EntityType" name="AEU_EntityType" onchange="getRoleByEntityType(<c:out value="${userSession.entityType}"/>,true)">						
					<c:if test="${not empty userSession}">
							
							<c:if test="${(userSession.entityType == 5) || (userSession.entityType == 6) || (userSession.entityType == 1) }">							
							<c:forEach items="${roleCombo}" var="roleCombo">
								<option value="${roleCombo.key}"
									${roleCombo.key==selectedRoleCombo? 'selected' : ''}>
										${roleCombo.value}
									</option>
							</c:forEach>
							</c:if>	
							
						 	 <c:set var="hide" value=""></c:set>		
						 	 <c:if test="${userSession.entityType eq 1}">	        			
		         			 	<c:set var="hide" value="hide"></c:set>
	         			 		<c:set var="selected" value=""></c:set>					
				        		<c:if test="${userMaster.entityType eq 1}">	        			
				         			<c:set var="selected" value="selected"></c:set>	     
				         			<option value="2" ><spring:message code="optDistrict"/></option>
									<option value="3" ><spring:message code="optSchools"/></option>
									<option value="1" ${selected}><spring:message code="sltTM"/></option>        			
				        		</c:if>
				        		<c:if test="${userMaster.entityType eq 2}">	        			
				         			<c:set var="selected" value="selected"></c:set>	   
				         			<option value="2" ${selected}><spring:message code="optDistrict"/></option>
									<option value="3" ><spring:message code="optSchools"/></option>
									<option value="1" ><spring:message code="sltTM"/></option>   
									<script> getRoleNameByEntityType(<c:out value="${userMaster.entityType}"/>,<c:out value="${userMaster.roleId.roleId}"/>)</script>       			
				        		</c:if>					
				        		<c:if test="${userMaster.entityType eq 3}">	        			
				         			<c:set var="selected" value="selected"></c:set>	 
				         			<option value="2" ><spring:message code="optDistrict"/></option>
									<option value="3" ${selected}><spring:message code="optSchools"/></option>
									<option value="1" ><spring:message code="sltTM"/></option>
									<script> getRoleNameByEntityType(<c:out value="${userMaster.entityType}"/>,<c:out value="${userMaster.roleId.roleId}"/>)</script>            			
				        		</c:if>					
			         			<c:if test="${userMaster.entityType eq null}">	        			
				         			<c:set var="selected" value="selected"></c:set>	 
				         			<option value="2" ><spring:message code="optDistrict"/></option>
									<option value="3" ><spring:message code="optSchools"/></option>
									<option value="1" ${selected}><spring:message code="sltTM"/></option>            			
				        		</c:if>			
			        		</c:if>	
			        		<c:if test="${userSession.entityType eq 2 && userMaster.entityType eq null}">	
			        			<c:set var="hide" value=""></c:set>
			         			<c:set var="hide" value="hide"></c:set>        			
			         			<option value="2" selected="selected"><spring:message code="optDistrict"/></option>
								<option value="3"><spring:message code="optSchools"/></option>
								<%-- <option value="1" >TM</option>--%>         			
			        		</c:if>
			        		<c:if test="${userSession.entityType eq 2 && userMaster.entityType eq 2}">	
			        			<c:set var="hide" value=""></c:set>
			         			<c:set var="hide" value="hide"></c:set>        			
			         			<option value="2" selected="selected"><spring:message code="optDistrict"/></option>
								<option value="3"><spring:message code="optSchools"/></option>
								<script> getRoleNameByEntityType(<c:out value="${userMaster.entityType}"/>,<c:out value="${userMaster.roleId.roleId}"/>)</script>       			
				          	</c:if>
			        		<c:if test="${userSession.entityType eq 2  && userMaster.entityType eq 3}">	
			        			<c:set var="hide" value=""></c:set>
			         			<c:set var="hide" value="hide"></c:set>        			
			         			<option value="2"><spring:message code="optDistrict"/></option>
								<option value="3"  selected="selected"><spring:message code="optSchools"/></option>
								<script> getRoleNameByEntityType(<c:out value="${userMaster.entityType}"/>,<c:out value="${userMaster.roleId.roleId}"/>)</script>       			
				        	</c:if>
			        		<c:if test="${userSession.entityType eq 3}">	        			
			         			<c:set var="hide" value=""></c:set>
			         			<c:set var="hide" value="hide"></c:set>
			         			<%--<option value="2">District</option>  --%>
								<option value="3" selected="selected"><spring:message code="optSchools"/></option>
								<%-- <option value="1">TM</option>   --%>         			
			        		</c:if>
			              </c:if>
							<%-- <option value="2">District</option>
							<option value="3">Schools</option>
							<option value="1">TM</option>--%>
							
							
<!--							<option>TM</option>-->
<!--							<option>District</option>-->
<!--							<option>School</option>-->
						</select>
					</div>
					
					<div class="col-sm-4 col-md-4 hide hqClass" id="headQuarterClass">
                    	<label>Head Quarter</label>
                    	<input class="form-control clearback" type="text" id="onlyHeadQuarterName" name="onlyHeadQuarterName"   value="${headQuarterId}"
                    			onfocus="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterHiddenId','');"
								onkeyup="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterHiddenId','');"
								onblur="hideHeadQuarterDiv(this,'headQuarterHiddenId','divTxtHeadQuarterData');"	/>
								
								<div id='divTxtHeadQuarterData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtHeadQuarterData','onlyHeadQuarterName')"  class='result' ></div>
								<input type="hidden" name="headQuarterHiddenId" id="headQuarterHiddenId" value="${headQuarterHiddenId}"/>
								<input type="hidden" id="isBranchExistsHiddenId" value="${isBranchExistsHiddenId}"/>	
					</div>
					<div class="col-sm-4 col-md-4 hide branchClass" id="branchClass">
                    	<label>Branch</label>
                    	<input class="form-control clearback" type="text" id="onlyBranchName" name="onlyBranchName"   value="${branchId}"
                    			onfocus="getOnlyBranchAutoComp(this, event, 'divTxtBranchData', 'onlyBranchName','branchHiddenId','');"
								onkeyup="getOnlyBranchAutoComp(this, event, 'divTxtBranchData', 'onlyBranchName','branchHiddenId','');"
								onblur="hideBranchDiv(this,'branchHiddenId','divTxtBranchData');"	/>
								
								<div id='divTxtBranchData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtBranchData','onlyBranchName')"  class='result' ></div>
								<input type="hidden" name="branchHiddenId" id="branchHiddenId" value="${branchHiddenId}"/>	
					</div>
					<div class="col-sm-4 col-md-4 hide schoolClass">
                    	<label><spring:message code="lblDistrict"/><span class='required'>*</span></label>
                    	<input class="form-control clearback" type="text" id="onlyDistrictName" name="onlyDistrictName"   value="${onlyDistrictName}"
                    			onfocus="getOnlyDistrictAutoComp(this, event, 'divTxtDistrictData', 'onlyDistrictName','districtHiddenId','');"
								onkeyup="getOnlyDistrictAutoComp(this, event, 'divTxtDistrictData', 'onlyDistrictName','districtHiddenId','');"
								onblur="hideDistrictDiv(this,'districtHiddenId','divTxtDistrictData');"	/>
								
								<div id='divTxtDistrictData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtDistrictData','onlyDistrictName')"  class='result' ></div>	
					</div>
					
					<div class="col-sm-4 col-md-4 DSClass <c:out value="${hide}"/>" id="AEU_searchbox">
                    	<label  id="AEU_captionDistrictOrSchool"><strong><spring:message code="lblDisOrSchoolName"/></strong></label>
                    	<input class="form-control clearback" type="text" id="districtORSchoolName" name="districtORSchoolName"   value="${SchoolOrDistrictName}"
                    			onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
								onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');"	/>

								
								<div id='divTxtShowData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')"  class='result' ></div>	
					</div>
								<input type="hidden" name="districtOrSchooHiddenlId" value="${districtOrSchooHiddenlId}" id="districtOrSchooHiddenlId"/>
								<input type="hidden" id="userSessionEntityTypeId" value="${userSession.entityType}"/>
								<input type="hidden" id="schoolSessionId" value="${userSession.schoolId.schoolId}"/>
			             	    <input type="hidden" id="districtSessionId" value="${userSession.districtId.districtId}"/>
			             	    <input type="hidden" name="schoolHiddenId" id="schoolHiddenId" name="schoolHiddenId" value="${userMaster.schoolId.schoolId}"/>
			             	    <input type="hidden" name="districtHiddenId" id="districtHiddenId" value="${districtHiddenId}"/> <!--value="${userMaster.districtId.districtId}" -->
				</div>
				<div class="row">
					<div class="col-sm-3 col-md-3">
						<label><strong><spring:message code="lblSalutation"/></strong></label>
						<select class="form-control" id="salutation" name="salutation">
							<c:set var="selected" value=""></c:set>					
				        		<c:if test="${userMaster.salutation == null|| userMaster.salutation ==0}">	
				        			<option value="0" ><spring:message code="optSlt"/></option>        			
				         			<option value="1" ><spring:message code="optMr"/></option>
									<option value="2"><spring:message code="optMs"/></option>
									<option value="3"><spring:message code="optMrs"/></option>
									<option value="4"><spring:message code="optDr"/></option>
				        		</c:if>
				        		<c:if test="${userMaster.salutation eq 1}">	
				        			<option value="0" ><spring:message code="optSlt"/></option>         			
				         			<c:set var="selected" value="selected"></c:set>	     
				         			<option value="1" ${selected}><spring:message code="optMr"/></option>
									<option value="2"><spring:message code="optMs"/></option>
									<option value="3"><spring:message code="optMrs"/></option>
									<option value="4"><spring:message code="optDr"/></option>
				        		</c:if>
							<c:if test="${userMaster.salutation eq 2}">	 
									<option value="0" ><spring:message code="optSlt"/></option>        			
				         			<c:set var="selected" value="selected"></c:set>	     
				         			<option value="1" ><spring:message code="optMr"/></option>
									<option value="2" ${selected}><spring:message code="optMs"/></option>
									<option value="3"><spring:message code="optMrs"/></option>
									<option value="4"><spring:message code="optDr"/></option>
				        		</c:if>
				        		<c:if test="${userMaster.salutation eq 3}">	
				        			<option value="0" ><spring:message code="optSlt"/></option>         			
				         			<c:set var="selected" value="selected"></c:set>	     
				         			<option value="1" ><spring:message code="optMr"/></option>
									<option value="2" ><spring:message code="optMs"/></option>
									<option value="3" ${selected}><spring:message code="optMrs"/></option>
									<option value="4"><spring:message code="optDr"/></option>
				        		</c:if>
				        		<c:if test="${userMaster.salutation eq 4}">	 
				        			<option value="0" ><spring:message code="optSlt"/></option>        			
				         			<c:set var="selected" value="selected"></c:set>	     
				         			<option value="1" ><spring:message code="optMr"/></option>
									<option value="2" ><spring:message code="optMs"/></option>
									<option value="3" ><spring:message code="optMrs"/></option>
									<option value="4" ${selected}><spring:message code="optDr"/></option>
				        		</c:if>
							</select>
					</div>
					<div class="col-sm-3 col-md-3">
                    	<label><strong><spring:message code="lblFname"/></strong><span class="required">*</span></label>
                    	<%-- <input class=" span3" type="text" placeholder="">--%>
                    	<form:input type="hidden" path="userId" id="userId" />
                    	<form:input path="firstName" Class="form-control" maxlength="50" placeholder=""/>
					</div>
					<div class="col-sm-3 col-md-3">
                    	<label><strong><spring:message code="lblLname"/></strong><span class="required">*</span></label>
                    	<%-- <input class=" span3" type="text" placeholder="">--%>
                    	<form:input path="lastName" Class="form-control" maxlength="50" placeholder=""/>
					</div>
				</div>
                <div class="row">
					<div class="col-sm-6 col-md-6">
                    	<label><strong><spring:message code="lblTitle"/></strong></label>
                    	<%-- <input class=" span6" type="text" placeholder="">--%>
                   	 	<form:input path="title" Class="form-control" maxlength="200" placeholder=""/>
                  </div>
                </div>
                <div class="row">
					<div class="col-sm-6 col-md-6">
                    	<label><strong><spring:message code="lblEmail"/></strong><span class="required">*</span></label>
                    	<%-- <input class=" span6" type="text" placeholder="">--%>
                   		<form:input path="emailAddress" Class="form-control" maxlength="75" />
                  </div>
                </div>
                <div class="row">
                	<c:if test="${userMaster.userId gt 0}">	        			
						<div class="col-sm-3 col-md-3">
	                    	<label><strong><spring:message code="lblPass"/></strong><span class="required">*</span></label>
	                    	<form:password Class="fl form-control" path="password" maxlength="12" value="********" readonly="true" tabindex="-1" />
	                     </div>	
	                     <div class="col-sm-3 col-md-3 top30-sm">	                   	
	                   		<c:if test="${fn:indexOf(roleAccess,'|11|')!=-1}">
	                   		<a href="javascript:void(0);" onclick="sendNewPwdToUser(<c:out value="${userMaster.userId}"/>);">Send New Password</a>
	                   		</c:if>	                   	
	                  	</div>
			    	</c:if>
                </div>
                <div class="row">
					<div class="col-sm-3 col-md-3">
                    	<label><strong><spring:message code="lblPhone"/></strong></label>
                    	<form:input path="phoneNumber" Class="form-control" maxlength="20" />
                  	</div>
                  	<div class="col-sm-3 col-md-3">
                    	<label><strong><spring:message code="lblMobile"/></strong></label>
                    	<form:input path="mobileNumber" Class="form-control" maxlength="20" />
                  	</div>
                </div>
                <div class="row">
					<div class="col-sm-3 col-md-3">
                    	<label><strong><spring:message code="lblRole"/></strong><span class="required">*</span></label>
                    	<div id="roleNamePopulate">
	                    	<select class="form-control" id="AEU_RoleName" name="AEU_RoleName"><!-- AEU stands for AddEditUser  -->
								<option value="0"><spring:message code="optStrRole"/></option>
								<c:if test="${not empty roleMaster}">
									<c:forEach var="roleMaster" items="${roleMaster}" varStatus="status">
										<c:set var="selected" value=""></c:set>					
						        		<c:if test="${roleMaster.roleId eq userMaster.roleId.roleId}">	        			
						         			<c:set var="selected" value="selected"></c:set>	         			
				        				</c:if>		
										<option id="${roleMaster.roleId}" value="${roleMaster.roleId}" ${selected}><c:out value="${roleMaster.roleName}"/></option>
									</c:forEach>
				                </c:if>
							</select>
						</div>		
                </div>
                <div id="keyContactTypeAdduser" class=" col-sm-3 col-md-3 hide mt10"> 
                  	
                      <label ><a href="javascript:void(0);" onclick="showKeyContact();"><spring:message code="lnkAddKyContact" /></a> </label>
                      </div>
                  	</div>  
                  	
                
                <!--                  	code written by hafeez-->
<!--                <c:set var="disabled" value="disabled"></c:set>	-->
<c:set var="display" value="none"></c:set>	
<c:if test="${ not empty attachContactType}">
<c:set var="display" value=""></c:set>	
<c:set var="display" value="block"></c:set>	
</c:if>
<div id="keyContactTable" class="row" style="display:${display};">
                <input type="hidden" id="roleIdUserLogin" name="roleIdUserLogin" value="${ userSession.roleId.roleId}">
                <c:if test="${ userSession.roleId.roleName eq 'TM Admin' || ( userSession.roleId.roleName eq 'TM Analyst')|| ( userSession.roleId.roleName eq 'Operations Administrator')}">
			 <div class="col-sm-2 col-md-2 "><label><strong><spring:message code="keyEntityType"/></strong></label>
                 <select class="form-control" id="dropkeyEntityType" name="dropkeyEntityType" >
                      <c:set var="selected" value=""></c:set>					
				        		<c:if test="${keyEntity eq 2}">	        			
				         			<c:set var="selected" value="selected"></c:set>	
				         				</c:if>
				         				<option value="2" ${selected}><spring:message code="optDistrict"/></option> 
				         		
                      <c:set var="selected" value=""></c:set>
                      <c:if test="${keyEntity eq 3}">	        			
				         			<c:set var="selected" value="selected"></c:set>
				         			</c:if>
							<option value="3" ${selected}><spring:message code="optSchools"/></option>	
				         		
                      
                      </select>
                 </div>
                <div class="col-sm-4 col-md-4 ">
                    	<label><strong><spring:message code="availablekeyContact"/></strong></label>
                      <select multiple class="form-control" id="availableContactTypeAdduser" name="availableContactTypeAdduser" style="height: 150px;">
                        <c:forEach items="${availableContactType}" var="ctm">   
							<option id="${ctm.contactTypeId}" value="${ctm.contactTypeId}" >${ctm.contactType}</option>
						</c:forEach>
                      </select>
                  	</div>
                  	<div class="col-sm-1 col-md-1 left20-sm"> 
				<div class="span2"> <span id="addkey" style="cursor:pointer;"><img alt="" src="images/rightarrow.jpg" width="30px"> </span></div>
				<div class="span2"> <span id="removeKey" style="cursor:pointer;"><img alt="" src="images/leftarrow.jpg" width="30px"> </span></div>
			</div>
			<div class="col-sm-4 col-md-4 ">
                    	<label><strong><spring:message code="attachedkeyContact"/></strong></label>
                      <select multiple class="form-control" id="attachContactTypeAdduser" name="attachContactTypeAdduser" style="height: 150px;">

                        <c:forEach items="${attachContactType}" var="ctm">				
							<option id="${ctm.contactTypeId}" value="${ctm.contactTypeId}" >${ctm.contactType}</option>
						</c:forEach>
                      </select>
                  	</div>
<!--                  	<c:set var="keyContactTypeIdValue" value=""></c:set>-->
                 	 </c:if>
<!--              <input type="hidden" id='keyContactTypeIdOfEDitUser' name='keyContactTypeIdOfEDitUser' value="${fn:escapeXml(keyContactTypeIdValue)}"/>-->
                    </div>
		  
                 <!--                  	code written by hafeez--> 
                <div class="row">
					<div class="col-sm-3 col-md-3">
                    	<label><strong><spring:message code="lblAuthPin"/></strong><span class="required">*</span></label>
                    	<form:input path="authenticationCode" Class="form-control" maxlength="10" placeholder="" value="${authenticationCode}" readonly="true" tabindex="-1"/>
                    	
                  	</div>
                </div>
                <div class="row mt10">
					<div class="col-sm-3 col-md-3"><span><label><spring:message code="lblPhoto"/></label>
					</span>
						<c:if test="${fn:indexOf(roleAccess,'|9|')!=-1}">
						<form:input path="photoPathFile" type="file"/><br>
						<!-- <a href="#" onclick="return clearFile()">Clear</a> -->
						</c:if>
						
                   	</div>
                    	<div class="span1"><label>&nbsp;</label><span id="showLogo">&nbsp;</span></div>
				</div>
				<div class="row mb30">
					<div class="col-sm-4 col-md-4 hide top3" id="hideSave">
						<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">                   		
                   		<button type="button" class="btn btn-primary"  onclick="return validateAddEditUser();"><strong>Save User <i class="icon"></i></strong>
                   		</button>                   	                 		
                   		</c:if>                   		
                   		<a href="javascript:void(0);" onclick="cancelUser();"><spring:message code="lnkCancel"/></a>                   	                   	
                    </div>
                 </div>
             </form:form>		
	</div>
	<div style="display:none;" id="loadingDiv">
		<table  align="center" >
			<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
			<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	</div>
	
	
	<!--
	<input type="hidden" id='selectRole' name='selectRole' value="${selectrole}"/>
	<input type="hidden" id='userId' name='userId' value="${userId}"/>
	-->
	<input type="hidden" id='roleId' name='roleId' value="${userMaster.roleId.roleId}"/>

<script type="text/javascript">
document.getElementById("hideSave").style.display='inline';
showFile('${userMaster.userId}','${userMaster.photoPath}');
getRoleByEntityType(<c:out value="${userSession.entityType}"/>,false);
/*
if($('#userId').val()!=''){
getRoleNameByEntityType(<c:out value="${userMaster.entityType}"/>,<c:out value="${userMaster.roleId.roleId}"/>);
$('#AEU_RoleName').val($('#selectRole').val());
}*/
</script>
