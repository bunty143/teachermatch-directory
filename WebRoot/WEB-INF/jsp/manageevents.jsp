<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript"
	src="dwr/interface/EventAjax.js?ver=${resourceMap['EventAjax.ajax']}"></script>
<script type="text/javascript"
	src="js/manageevents.js?ver=${resourceMap['js/manageevents.js']}"></script>
	
	
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type="text/javascript"
	src="dwr/interface/I4QuestionSetAjax.js?ver=${resourceMap['I4QuestionSetAjax.ajax']}"></script>
	<script type="text/javascript"
	src="dwr/interface/CGInviteInterviewAjax.js?ver=${resourceMap['CGInviteInterviewAjax.ajax']}"></script>
<script type="text/javascript"
	src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>

<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.ajax']}"></script>

<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.ajax']}"></script>

<script type='text/javascript' src="js/branchautoSuggest.js?ver=${resourceMap['js/branchautoSuggest.js']}"></script>	

<script type="text/javascript" src="js/jquery-1.4.1.js?ver=${resourceMap['']}"></script>

<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>

<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css"
	href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<script type='text/javascript' src="js/bootstrap-slider.js"></script>
<link rel="stylesheet" type="text/css"
	href="css/base.css?ver=${resourceMap['css/base.css']}" />
<link rel="stylesheet" type="text/css" href="css/slider.css" />

<style>
.btn-dangerevents {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #da4f49;
  *background-color: #bd362f;
  background-image: -moz-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#bd362f));
  background-image: -webkit-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -o-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: linear-gradient(to bottom, #ee5f5b, #bd362f);
  background-repeat: repeat-x;
  border-color: #bd362f #bd362f #802420;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffbd362f', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.btn-dangerevents:hover,
.btn-dangerevents:focus,
.btn-dangerevents:active,
.btn-dangerevents.active,
.btn-dangerevents.disabled,
.btn-danger[disabled] {
  color: #ffffff;
  background-color: #bd362f;
  *background-color: #a9302a;
}

.btn-dangerevents:active,
.btn-dangerevents.active {
  background-color: #942a25 \9;
}

.btn-warningevents {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #faa732;
  *background-color: #f89406;
  background-image: -moz-linear-gradient(top, #fbb450, #f89406);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fbb450), to(#f89406));
  background-image: -webkit-linear-gradient(top, #fbb450, #f89406);
  background-image: -o-linear-gradient(top, #fbb450, #f89406);
  background-image: linear-gradient(to bottom, #fbb450, #f89406);
  background-repeat: repeat-x;
  border-color: #f89406 #f89406 #ad6704;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fffbb450', endColorstr='#fff89406', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}

.btn-warningevents:hover,
.btn-warningevents:focus,
.btn-warningevents:active,
.btn-warningevents.active,
.btn-warningevents.disabled,
.btn-warningevents [disabled] {
  color: #ffffff;
  background-color: #f89406;
  background-color: #df8505;
}

.btn-warningevents:active,
.btn-warningevents.active {
  background-color: #c67605 \9;
}

strong {
font-weight: normal;
}

</style>
<script>

var c1=100;
var c2=120;
var c3=90;
var c4=80;
var c5=90;
var c6=80;

var c7=70;
var c8=80;
var c9=80;
var c10=80;
var c11=75;

var districtId="${districtId}";
var entityType="${entityID}";

function applyScrollOnTblEvent()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#eventTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 280,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
   });			
}

function applyScrollOnTblEventPreview()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#eventTablePreview').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 280,
        width: 980,
        minWidth: null,
        minWidthAuto: false,
        colratio:[c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
   });			
}
</script>
<style>
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
</style>

<style>
	.hide
	{
		display: none;
	}
	.icon-question-sign{
 	 font-size: 1.3em;
 }
</style>


<div class="row"
	style="margin-left: 0px; margin-right: 0px; margin-top: ">
	<div style="float: left;">
		<img src="images/manageusers.png" width="41" height="41" alt="">
	</div>
	<div style="float: left;">
		<div class="subheading" style="font-size: 13px;">
			<spring:message code="msgManageEvent"/>
		</div>
	</div>
	
	<c:if test="${entityID eq 3}">
		<c:if test="${userSession ne null && userSession.districtId ne null && userSession.districtId.sACreateEvent ne null}">
			<c:if test="${userSession.districtId.sACreateEvent}">
				<div class="pull-right add-employment1">
					<a href="javascript:void(0);" onclick="return addEvent()"><spring:message code="lnkAddEvent"/></a>
				</div>
			</c:if>
		</c:if>
	</c:if>
	
	<c:if test="${entityID ne 3}">
		<div class="pull-right add-employment1">
			<a href="javascript:void(0);" onclick="return addEvent()"><spring:message code="lnkAddEvent"/></a>
		</div>
	</c:if>
	
	<div style="clear: both;"></div>
	<div class="centerline"></div>
</div>


    <div class="row" id="eventsearchDiv">
       <div class="col-xs-5 col-sm-5">
        <c:if test="${entityID==1 || entityID==2 || entityID==3}">
		     <label id="captionDistrictOrSchool"><spring:message code="lblDistrictName"/></label>
		        <c:if test="${districtName==null}">
					<span>
					  <input type="text" id="districtNameForSearch" name="districtNameForSearch"
							class="help-inline form-control"
							onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData1', 'districtNameForSearch','districtIdForSearch','');;"
							onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData1', 'districtNameForSearch','districtIdForSearch','');"
							onblur="hideDistrictMasterDiv(this,'districtIdForSearch','divTxtShowData1')" />
							 </span>
							 <input type="hidden" id="districtIdForSearch"/>							
				 </c:if>
			     <c:if test="${districtName!=null}"> 
					<span>
					 <input type="text" maxlength="255"  class="help-inline form-control" value="${districtName}"  disabled="disabled"/>
					</span>
				   <input type="hidden" id="districtIdForSearch" value="${districtId}"/>
				 </c:if>
				 
			<div id='divTxtShowData1'  onmouseover="mouseOverChk('divTxtShowData1','districtNameForSearch')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
        </c:if>
        </div>
        
        <div class="col-xs-5 col-sm-5">
        <!-- Dhananjay Verma -->
        
         <c:if test="${entityID==1 || entityID==2 || entityID==3}">
       <label id="captionDistrictOrSchool"><spring:message code="lblSchoolName"/></label>
			<c:choose>
				<c:when	test="${(userMaster.entityType eq 3 ) }">
				     <span> <input type="text"  value="${schoolName}"  maxlength="255" class="help-inline form-control" disabled="disabled"/></span>
					<input type="hidden" id="schoolName" name="schoolName" value="${schoolName}" />
					<input type="hidden" id="schoolId" name="schoolId"	value="${schoolId}" />
				</c:when>
				
				<c:when	test="${(userMaster.entityType eq 2 ) }">
				    <input type="text" id="schoolName" maxlength="255"
						name="schoolName" class="form-control" placeholder=""
						onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
						onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
						onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');"  />
					<input type="hidden" id="schoolName" name="schoolName" />
					<input type="hidden" id="schoolId" name="schoolId" value="0" />
				</c:when>
				
				<c:otherwise>
					<input type="text" id="schoolName" maxlength="255"
						name="schoolName" class="form-control" placeholder=""
						onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
						onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
						onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');"  readonly="false"/>
					<input type="hidden" id="schoolName" name="schoolName" />
					<input type="hidden" id="schoolId" name="schoolId" value="0" />
				</c:otherwise>
				
				
			</c:choose>
			<div id='divTxtShowData2'
				onmouseover="mouseOverChk('divTxtShowData2','schoolName')"
				style='display: none; position: absolute; z-index: 5000;'
				class='result'>
				</div>
			      </c:if>  
        <!-- End -->
        </div>
        </div>
        <div class="row" id="eventsearchDiv">
        
        <c:if test="${entityID==5 || entityID==6}">
        <div class="col-sm-5" >
          <label id="captionDistrictOrSchool">Branch Name</label>
             <c:if test="${branchName==null}">
					 <input type="text" id="branchNameForSearch" name="branchNameForSearch"  class="form-control" value="${branchName}"
		       					onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch1', 'branchNameForSearch','branchIdForSearch','');"
								onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch1', 'branchNameForSearch','branchIdForSearch','');"
								onblur="hideBranchMasterDiv(this,'branchIdForSearch','divTxtShowDataBranch1');"/>
						<div id='divTxtShowDataBranch1' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch1','branchNameForSearch')" class='result' ></div>
							 <input type="hidden" id="branchIdForSearch"/>							
			 </c:if>
			 <c:if test="${branchName!=null}"> 
					<span>
					 <input type="text" maxlength="255"  class="help-inline form-control" value="${branchName}"  disabled="disabled"/>
					</span>
				   <input type="hidden" id="branchIdForSearch" value="${branchId}"/>
				 </c:if>
				  </div>
         </c:if>
       
        
          <div class="col-sm-5" >
        
		      <label><spring:message code="lblEventName"/></label>
				 <input type="text" maxlength="200" id="eventName" name="eventName" class="help-inline form-control"/>
	     	
	      </div>
	     
	      <div class="col-sm-5" >
		  <label><spring:message code="lblEventType"/></label>
			<select class="form-control " id="interactionTypeForSearch" name="interactionTypeForSearch" class="span3">
				<option value="" selected="selected">
					<spring:message code="lblAllEventType"/>
				</option>
			</select>
		</div>
		<div class="col-sm-5 col-md-5 top26" style="width: 150px;">
		        <label class=""></label>
		        <button class="btn btn-primary " type="button" onclick="searchEventByButton();">&nbsp;&nbsp;&nbsp;<spring:message code="btnSearch"/>&nbsp;&nbsp;&nbsp; <i class="icon"></i></button>
		 </div>
         </div>

	 <div style="clear: both;"></div>
	              <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
			 </iframe>  
		  </div> 

<!--********************** Main Grid *************************-->
 

  <div class="row mt5 top15 hide"  id="xportdataPdfExlPrint">
	<table class="marginrightForTablet" >
         
		      <tr >
		      
		         <td  style='padding-left:5px;'>
					<a data-toggle="tooltip" title="Export to Excel" href='javascript:void(0);' onclick="generateManageEventsEXL();"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td>
				  <a title='Export to PDF' data-toggle='tooltip'  href="javascript:void(0);"  onclick='generateManageEventsPDF();'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				
				 <td >
					<a title='Print Preview' data-toggle='tooltip' href='javascript:void(0);' onclick="generateManageEventsPrintPre();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td>  
			</tr>
		</table>
		</div>

    <div class="TableContent top15" id="maineventtableDiv" >
     	
             <div class="table-responsive" id="eventGrid" >    
             </div>       
    </div>



<!--*********************** End ******************************-->
 <div style="clear: both;"></div>

<input type="hidden" id="gridNo" name="gridNo" value="">
<div class="col-sm-12 col-md-12 top10">
	<div class='divErrorMsg' id='errordiv'></div>
</div>
<div id="addeditevent" class="hide">
	<input type="hidden" id="eventId" name="eventscheduleId" />
	<input type="hidden" id="districtId" value="${districtId}" />
	<input type="hidden" id="headQuarterId" value="${headQuarterId}"/>
	<input type="hidden" id="branchId" value="${branchId}"/>
	
	<c:if test="${entityID eq 1}">
		<div class="row">

			<div class="col-sm-12 col-md-12">
				<label>
					<spring:message code="optDistrict"/><span class="required">*</span>
					
				</label>
				<input type="text" id="districtName" name="districtName"
					class="form-control"
					onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');getQuestionSetByDistrict();getEmailTemplatesByDistrictId();"
					onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');getQuestionSetByDistrict();getEmailTemplatesByDistrictId();"
					onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');getDistrictEventTemplate();getQuestionSetByDistrict();getEmailTemplatesByDistrictId();" />
				<div id='divTxtShowData'
					style='display: none; position: absolute; z-index: 5000;'
					onmouseover="mouseOverChk('divTxtShowData','districtName')"
					class='result'></div>

			</div>

		</div>
	</c:if>
	
		
	<c:if test="${entityID eq 5}">
	  <div class="row">
	    <div class="col-sm-12 col-md-12">
		   <label>Branch Name</label>
			
			 <input type="text" id="branchName" name="branchName"  class="form-control" value="${branchName}"
       					onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
						onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');getEmailTemplatesByDistrictId();getEmailTemplatesFacilitatorsByDistrict();"
						onblur="hideBranchMasterDiv(this,'branchId','divTxtShowDataBranch');getEmailTemplatesByDistrictId();getEmailTemplatesFacilitatorsByDistrict();"/>
				
			 <div id='divTxtShowDataBranch' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>
			
	     </div>
	  </div>  
	</c:if>

	<div class="row">
		<div class="col-sm-12 col-md-12">
			<label>
				<spring:message code="lblEventName"/><span class="required">*</span>
				
			</label>
			<input class="form-control" type="text" id="eventtitle"
				name="eventtitle" maxlength="200" />
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4 col-md-4">
			<label>
			  <spring:message code="lblEventType"/><span class="required">*</span> <a href="#"  id="evtType" rel="tooltip" data-original-title="Event Type"><img width="15" height="15" alt="" src="images/qua-icon.png"></a>
			</label>
			<select class="form-control " id="interactionType"
				name="interactionType" class="span3" onchange="checkType(0)">
				<option value="" selected="selected">
					<spring:message code="lblEventType"/>
				</option>
			</select>
		</div>
		
		<div class="col-sm-8 col-md-8 hide" id="questionSet">
			<label>
				<spring:message code="lblPlzSeltQuesSet"/><span class="required">*</span>
			</label>
			<select class="form-control" id="quesId" name="quesId" class="span3">
				<option value="" selected="selected">
					<spring:message code="lblSelectDistictForQnSet"/>
				</option>
			</select>
		</div>

	</div>

	<div class="row" id="scheduleTypeDiv">
		<div class="col-sm-6 col-md-6">
			<label>
				<spring:message code="lblScheduleType"/><span class="required">*</span> <a href="#"  id="evtsch" rel="tooltip" data-original-title="Event Schedule"><img width="15" height="15" alt="" src="images/qua-icon.png"></a>
			</label>
			  <span id="intschedule">
			  </span>
			
			</div>
	</div>



	<div class="row" id="logoDiv">
		<div class="col-sm-6 col-md-6">
			<label>
				<spring:message code="lblLg"/><span class="required">*</span>
			</label>
			<input type="file" id="logo" name="logo">
		</div>
	</div>

	<div class="row top10">
		<div class="col-sm-4 col-md-4" id="selectdesc">
			<label>
				<spring:message code="lblSelectDescription"/>
			</label>
			<select class="form-control " id="description" name="description"
				class="span3" onchange="getDescription()">

			</select>
		</div>
	</div>

	<div class="row" id="desDiv">
		<div class="control-group col-sm-8 col-md-8">
			<div class="" id="descriptiontext">
				<label>
					<strong> <spring:message code="lblDecr"/></strong><span class="required">*</span>
				</label>
				<textarea rows="8" class="form-control" cols="" id="desctext"
					name="msg" maxlength="1000"></textarea>
			</div>
		</div>
	</div>


	<div class="row" id="messagetoprincipleDiv">
		<div class="col-sm-4 col-md-4" id="">
			<label>
				<spring:message code="msgTempltForCand"/>
			</label>
			<select class="form-control " id="messagetoprinciple"
				name="messagetoprinciple" class="span3"
				onchange="getEmailDescription()">
				<option value="" selected="selected">
					<spring:message code="lblSelectMsg"/>
				</option>
			</select>
		</div>
	</div>

	<div class="row" id="subjectDiv">
		<div class="col-sm-8 col-md-8" id="">
			<label>
				<spring:message code="lblSub"/><span class="required">*</span>
			</label>
			<input type="text" id="subjectforParticipants"
				name="subjectforParticipants" maxlength="100" class="form-control" />
		</div>
	</div>

	<div class="row" id="msgtoparticipantdiv">
		<div class="control-group col-sm-8 col-md-8">
			<div class="" id="messagetext">
				<label>
					<strong><spring:message code="lblMessageToCand"/><span class="required">*</span> <a href="#"  id="tooltip1" rel="tooltip" data-html="true" data-original-title="
									<table style='text-align: left;'>
										<tr><td>Along with the static text, the following dynamic fields are available in this exact format:</td></tr>
										<tr><td>&lt; First Name &gt;</td></tr>
										<tr><td>&lt; Last Name &gt;</td></tr>
										<tr><td>&lt; Email &gt;</td></tr>
										<tr><td>&lt; Job Title &gt;</td></tr>
										<tr><td>&lt; District or School Name  &gt;</td></tr>
										<tr><td>&lt; District or School Address  &gt;</td></tr>
									</table>"><img width="15" height="15" alt="" src="images/qua-icon.png"></a>

					
					</strong>
				</label>
				<textarea rows="8" class="form-control" cols="" id="msgtoprncpl"
					name="msg" maxlength="1000"></textarea>
			</div>
		</div>

		<div class="col-sm-4 col-md-4">
			<div class="" id="qrAndurlheading" style="color: #007AB4;"></div>
			<div class="" id="qrcode"></div>
			<div class="" id="qrcode1" style="padding-left: 12px;">
				<label id="eventUrl">
					<strong></strong>
				</label>
			</div>
		</div>


	</div>

	<!-- Msg to Facilatator Div Start -->
	<div class="row" id="msgtofacilitatorsDiv1">
		<div class="col-sm-4 col-md-4" id="">
			<label>
				<spring:message code="lblMSgTempForFaciltr"/>
			</label>
			<select class="form-control " id="msgtofacilitators"
				name="msgtofacilitators" class="span3"
				onchange="getEmailDescription1()">
				<option value="" selected="selected">
					<spring:message code="sltTemplate"/>
				</option>
			</select>
		</div>

	</div>

	<div class="row" id="subjectforFacilatorDiv">
		<div class="col-sm-8 col-md-8" id="">
			<label>
				<spring:message code="lblSub"/><span class="required">*</span>
			</label>
			<input type="text" id="subjectforFacilator"
				name="subjectforFacilator" maxlength="100" class="form-control" />
		</div>
	</div>

	<div class="row" id="msgtofacilitatorsDiv2">
		<div class="control-group col-sm-8 col-md-8">
			<div class="" id="facilitatorsmessagetext">
				<label>
					<strong><spring:message code="lblMsgToFaciltr"/><span class="required">*</span> <a href="#"  id="tooltip2" rel="tooltip" data-html="true" data-original-title="
									<table style='text-align: left;'>
										<tr><td>Along with the static text, the following dynamic fields are available in this exact format:</td></tr>
										<tr><td>&lt; First Name &gt;</td></tr>
										<tr><td>&lt; Last Name &gt;</td></tr>
										<tr><td>&lt; Email &gt;</td></tr>
										<tr><td>&lt; Job Title &gt;</td></tr>
										<tr><td>&lt; District or School Name  &gt;</td></tr>
										<tr><td>&lt; District or School Address  &gt;</td></tr>
									</table>"><img width="15" height="15" alt="" src="images/qua-icon.png"></a>

					
					</strong>
				</label>
				<textarea rows="8" class="form-control" cols="" id="msgtofsltr"
					name="msg1" maxlength="1000"></textarea>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="control-group col-sm-8 col-md-8 hide">
			<label class="checkbox">
				<input type="checkbox" name="sendmessage" id="sendmessage" value="1">
				<spring:message code="lblDNM"/>
			</label>
		</div>
	</div>



	<div class="row">
		<div class="col-sm-8 col-md-8 top15">
			<div class="row">
				<div class="col-sm-6 col-md-6">
				
					<button onclick="checkDuplicacy(0)" class="btn btn-warningevents"
						id="save">
						<strong><spring:message code="btnSaveExit"/> <i class="icon"></i>
						</strong>
					</button>
					&nbsp;&nbsp;
					<button onclick="cancelEvent()" class="btn btn-dangerevents" id="save">
						<strong><spring:message code="btnClr"/> <i class="icon"></i>
						</strong>
					</button>
					&nbsp;&nbsp;
				</div>	
				<div class="col-sm-6 col-md-6" style="text-align: right;">
					<button onclick="checkDuplicacy(1)" class="btn btn-primary"
						id="save">
						<strong><spring:message code="btnNext"/> <i class="icon"></i>
						</strong>
					</button>
				</div>
				</div>
			</div>
		</div>
	

</div>
<div style="display: none; z-index: 5000;" id="loadingDiv">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
	</table>
</div>


<!-- printing Start  -->

<div style="clear: both;"></div>

<div class="modal hide" id="printEventDiv" tabindex="-1" role="dialog"
	data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 950px; margin-top: 0px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="canelPrint();">
					x
				</button>
				<h3 id="">
					<spring:message code="headPrRrv"/>
				</h3>
			</div>

			<div class="modal-body" style="max-height: 500px; overflow-y: auto;">
				<div class="" id="pritEventDataTableDiv">
				</div>
			</div>
			<div class="modal-footer">
				<table border=0 align="right">
					<tr>
						<td nowrap>
							<button class="btn  btn-large btn-primary" aria-hidden="true"
								onclick='printEventDetails();'>
								&nbsp;&nbsp;&nbsp;<spring:message code="btnP"/>&nbsp;&nbsp;&nbsp;
							</button>
							&nbsp;&nbsp;
						</td>
						<td nowrap>
							<button class="btn btn-large" data-dismiss="modal"
								aria-hidden="true" onclick='canelPrint();'>
								<spring:message code="btnClr"/>
							</button>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="vviDiv" tabindex="-1" role="dialog"
	data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 80%;">
		<div class="modal-content">
			<div class="modal-header" >
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="canelPrint();">
					x
				</button>
				<h3 id="">
					<spring:message code="lblVVI"/>
				</h3>
			</div>

			<div class="modal-body" style="max-height: 500px; overflow-y: auto;">
				<div id="interViewDiv">
					<iframe src='' id='ifrmTrans' width='100%' height='480px'></iframe>
				</div>
			</div>
			
			
			<div class="modal-footer">
				<table border=0 align="right">
					<tr>
						<td nowrap>
							<button class="btn btn-large" data-dismiss="modal"
								aria-hidden="true" onclick=''>
								<spring:message code="btnOk"/>
							</button>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<div style="clear: both;"></div>

<div class="modal hide" id="printmessage1" tabindex="-1" role="dialog"
	data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 400px; margin-top: 0px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="canelPrint();">
					x
				</button>
				<h3 id="myModalLabel">
					Teachermatch
				</h3>
			</div>

			<div class="modal-body" style="max-height: 400px; overflow-y: auto;">
				<div class="control-group">
					<div class="">
						<spring:message code="msgPlzPopAreAlloued"/>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<table border=0 align="right">
					<tr>
						<td nowrap>
							<button class="btn btn-large" data-dismiss="modal"
								aria-hidden="true" onclick='canelPrint();'>
								<spring:message code="btnOk"/>
							</button>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="eventIdM" value="0" />
<input type="hidden" id="entityTypeId" value="${entityID}" />

<div style="clear: both;"></div>
<!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadHiredCandidates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headMsgHiresBySchool"/></h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End -->  

<!-- printing Start  -->
    
  <div style="clear: both;"></div>

   <div class="modal hide"  id="printHiredApplicantsDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1000px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   	<h3 id=""><spring:message code="headPrintPreview"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			<div class="" id="printhiredApplicantsDataTableDiv"  >          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='printManageEventsDATA();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    
    
   <div style="clear: both;"></div>
   <div class="modal hide"  id="printmessage1"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   <h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" >
			<spring:message code="msgPlzPopAreAlloued"/>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'><spring:message code="btnOk"/></button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
<!-- printing End -->    

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal"><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="cancelEventpopupId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				You are deleting an event. Please confirm that you want to delete this event. A notice will be sent to all candidates associated with this event informing them that this event has been canceled.
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="deleteEvent()"><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
				<span id=""><button class="btn btn-large" data-dismiss="modal"><spring:message code="btnClr"/> </button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>
 
<script>
displayEventGrids();
applyScrollOnTblEvent();
getEventType();

<c:if test="${entityID ne 1}">
  //getEmailTemplatesByDistrictId(${districtId});
  getEmailTemplatesFacilitatorsByDistrict();
 //template for Participants
  getEmailTemplatesByDistrictId();
</c:if>
//Template for description
getTemplatesByDistrictId();
getQuestionSetByDistrict();
 </script>
 
 <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<script>
function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#virusDivId').modal('show');
}
</script>