<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script>
function verify(){
	var offerUrl    =   document.getElementById("offerUrl").value;
	window.location.href=offerUrl;
}
</script>
<div class="row offset4 msgPage">
	<div class="span8 mt30">
		${msgpage}
	</div>
</div>

<input type="hidden" id="offerUrl" name="offerUrl" value="${offerUrl}"/>
<div  class="modal hide"  id="myModalVerify" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgmessage">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.location.href='logout.do'"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='messageVerify'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick='verify()'><spring:message code="btnYes"/></button>&nbsp;
 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick="window.location.href='logout.do'"><spring:message code="btnNo"/></button>
 	</div>
</div>
</div>
</div>

<script type="text/javascript">
	<c:if test="${ verifyFlag=='1'}">
		$('#messageVerify').html('${msg}');
		$('#myModalVerify').modal('show');
	</c:if>
	
</script>

