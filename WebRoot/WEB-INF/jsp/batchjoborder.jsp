 <!-- @Author: Sekhar 
 * @Discription: view of Batch Job Order.
 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/batchjoborder.js?ver=${resourceMap['js/batchjoborder.js']}"></script>
<script type='text/javascript' src="js/certtypeautocomplete.js?ver=${resourceMap['js/certtypeautocomplete.js']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.ajax']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script>
var $j=jQuery.noConflict();      
$j(document).ready(function() {
});
function applyScrollOnTbl(){
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
       	  <c:if test="${addBatchJobFlag==true}">
	          colratio:[88,100,245,300,60,170],
	        </c:if>
	        <c:if test="${addBatchJobFlag==false}">
	         colratio:[88,100,245,242,120,75,98],
	        </c:if>
	        <c:if test="${JobOrderType==3}">
	         colratio:[88,100,245,242,120,75,98],
	        </c:if>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

</script>
 <style>
.hide
	{
		display: none;
	}
</style>  
<!--   <h1> Welcome Gagan </h1> <br>-->
 <div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/batchcreatedschooljoborders.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headBtCreSchJoOrders"/></div>	
         </div>
          <div class="pull-right add-employment1">
				<c:if test="${addBatchJobFlag && fn:indexOf(roleAccess,'|1|')!=-1}">
				<a href="addeditbatchjoborder.do?JobOrderType=${JobOrderType}"><spring:message code="lnkAddBtJobOdr"/></a>
			</c:if>
		 </div>			
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
 </div>

	
	<div class="mt10" onkeypress="return chkForEnterSearchBatchJobOrder(event);">
		
		<div class="row col-sm-12 col-md-12">			                         
		 	<div class='divErrorMsg' id='errordiv' ></div>
		</div>
		<div class="row">
	        <div class="col-sm-5 col-md-5">
	          <label><spring:message code="lblDistrictName"/></label><br/>
	          <c:if test="${DistrictOrSchoolName==null}">
	             <span>
	             <input type="text" id="districtORSchoolName"  maxlength="100"  name="districtORSchoolName" class="help-inline form-control"
	            		 onfocus="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
									onkeyup="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
									onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');"	/>
			      </span>
			      <input type="hidden" id="districtlId" value="0"/>
             </c:if>
             <c:if test="${DistrictOrSchoolName!=null}">
             	${DistrictOrSchoolName}	
             	<input type="hidden" id="districtlId" value="${DistrictOrSchoolId}"/>
             	<input type="hidden" id="districtORSchoolName" name="districtORSchoolName"/>
             </c:if>
              <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
			</div>
			<c:if test="${addBatchJobFlag}">
	        <div class="col-sm-5 col-md-5">
	          <label><spring:message code="lblSchoolName"/></label>
	           <input type="text" id="schoolName" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
									onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
									onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');"	/>
			
			 	
				 <div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
	        </div>
	        </c:if>
	        <c:if test="${addBatchJobFlag==false}">
	        <input type="hidden" id="schoolName" name="schoolName"/>
	        </c:if>
	         <input type="hidden" id="schoolId" value="0"/>
	        <div class="col-sm-5 col-md-5">
	          <label><spring:message code="lblCert/LiceName"/></label>
	          <!--<input type="text" class=" span6" name="certifications" id="certifications" placeholder="">
	         -->
	      
				<input type="hidden" id="certificateTypeMaster" value="">
				<input type="text" 
					class="form-control"
					maxlength="100"
					id="certType" 
					value=""
					name="certType" 
					onfocus="getAllCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
					onkeyup="getAllCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');" 
					onblur="hideAllCertificateTypeDiv(this,'certificateTypeMaster', 'divTxtCertTypeData');"/>
				<div id='divTxtCertTypeData' style=' display:none;position:absolute;' class='result' ></div>
		          
	         </div>
	         <div class="col-sm-3 col-md-3">
	          <label></label>
	          <c:if test="${fn:indexOf(roleAccess,'|5|')!=-1}">
	          <button class="btn btn-primary top25-sm2" type="button"  onclick="searchBatchJobOrder()"><spring:message code="btnSearch"/><i class="icon"></i></button>
	          </c:if>
	        </div>        
	    </div>
</div>

<div class="TableContent top15">        	
            <div class="table-responsive" id="divMain">          
            </div>            
</div> 	
	

<div style="display:none;" id="loadingDiv">
	<table  align="center" >
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>


 <input type="hidden" id="addBatchJobFlag" name="addBatchJobFlag"  value="${addBatchJobFlag}"/>
 <input type="hidden" id="JobOrderType" name="JobOrderType"  value="${JobOrderType}"/>
<script type="text/javascript">
	displayBatchJobRecords();
	$("#districtORSchoolName").focus();
</script>
           
