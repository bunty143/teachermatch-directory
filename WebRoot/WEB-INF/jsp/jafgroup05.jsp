<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="javax.swing.Spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />
<script>
var $j=jQuery.noConflict();
        $j(document).ready(function() {
       });
</script>
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/group01.js?ver=${resouceMap['js/jobapplicationflow/group01.js']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jobapplflowcommon.js?ver=${resouceMap['js/jobapplicationflow/jobapplflowcommon.js']}"></script>

<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resouceMap['DWRAutoComplete.ajax']}"></script>

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />

<input type="hidden" id="gridNameFlag" name="gridNameFlag"/>
<p id="portfolioHeader"></p>
<input type="hidden" id="sNextURL" name="sNextURL" value="${sNextURL}"/>
<input type="hidden" id="sPreviousURL" name="sPreviousURL" value="${sPreviousURL}"/>
<input type="hidden" id="sCurrentPageShortCode" name="sCurrentPageShortCode" value="${sCurrentPageShortCode}"/>

<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:30px;">
         <div style="float: left;">
         	<img src="images/affidavit.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="DSPQlblAffidavit"/></div>	
         </div>
         
         <div style="float: right;">
          	<div style='text-align:right; padding-right: 0px;' class="col-sm-3 col-md-3"><button class='flatbtn' style='width:190px;'  type='button' onclick='return validateDSPQG4Section25(0);'><spring:message code="btnSaveAndNextDSPQ"/> <i class='icon'></i></button></div>
         </div>
         
         		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<p id="group04Section25Name"></p>

<div style="text-align: center; padding-bottom: 5px;font-weight:bold;font-size:13px;">
	    </br>
	    <span><spring:message code="headTmCandP"/></span>
	    </br>
	    <spring:message code="headTAndCondOfUSE"/>
  </div>

 <div class="container top15">
             <div class="row" align="justify">  
		   <div class="scrollspy-example termsheight right10" data-offset="0" data-target="#navbarExample" data-spy="scroll">
			
			<p style="text-indent: 0px;"><spring:message code="pUseCarefullyBeforeUsingTheTm"/>.
			</p>
			
			<div style="text-align: center; padding-bottom: 5px;">
				<spring:message code="pCopyrightTm"/><br/>
				<spring:message code="pAllRiRe"/>.<br/>
			</div>
			
			<p>
				<b><spring:message code="pOWNERSHIP"/>.</b>  <spring:message code="pEachOfItsCompIsCopyrightedPropertyOfTm"/>  
			</p>
			<p>
				<b><spring:message code="pAgeAdRespo"/>.</b>  <spring:message code="pYuRepresntAtLeast(18)Yr"/>.  
			</p>
			<p>
				<b><spring:message code="pAccesingThePorAdAccSecurity"/></b>  <spring:message code="pEachTchCandMayBeProvidedWithUseraAdPss"/>.			
				<br/><br/><span  class="spnIndent"></span><spring:message code="pInConnWithEstablishingYurAccWithUs"/></span> 
				<br/><br/><span  class="spnIndent"></span><spring:message code="pYuAcknowledgeAdAgreeThatTmHasNoResponsibility"/></span>
			</p>
			<p>
				<b><spring:message code="pSubmissions"/>.</b> <spring:message code="pAllInfoOfTm"/>
				<br/><br/><span  class="spnIndent"></span><spring:message code="pNoConfidentialRelEstabBySubMinssionByAnyCandTch"/>  
			</p>
			<p>
				<b><spring:message code="pRulOfCon"/>.</b> <spring:message code="pWhileUThPortYuWillApplicableLaws"/>
				<div class="spnIndent40">
				<ul>  
					<li><spring:message code="pManyInfoOfTm"/></li>
					<li><spring:message code="pCivilLiability"/></li>
					<li><spring:message code="pVirusWormTrHorsEasterEggTime"/>.</li>
					<li><spring:message code="pAnyPersIdentiInfoOfAthrIndividual"/></li>
					<li><spring:message code="pMateralNonPuInfoAbtCompany"/></li>
				</ul>
				</div>
				<spring:message code="pFurtherInConnWithYurUseOfPortal"/>
				<div class="spnIndent40" style="padding-top: 5px;">
				<ul>
					<li><spring:message code="pUseThePorForFraudulent"/>.</li>
					<li><spring:message code="pImperAnyPersonOrEntity"/></li>
					<li><spring:message code="pInterfaceWiDisruptUseNet"/></li>
					<li><spring:message code="pRestOrInhibit"/></li>
					<li><spring:message code="pModiAdTransReveEngineer"/></li>
			 </ul>
				</div>
			</p>
			<p>
				<b><spring:message code="pUSEOFPORTAL"/>
			</p>  
			<p>
				<b><spring:message code="pPRIVACY"/></b><br/>				
				
					<div  style="padding-left: 50px; "><spring:message code="pPRIVACY1"/> <br/>
					</div>
					<div   style="padding-left: 50px; padding-right: 10px;"><spring:message code="pPRIVACY2"/><br/>  
					<br/><spring:message code="pPRIVACY3"/>   <br/><br/>
					</div>					
					<div  style="padding-left: 50px; margin-right: 10px;" ><spring:message code="pPRIVACY4"/><br/><br/>  
					</div>
					<div class="right10"  style="padding-left: 50px;padding-right: 10px; "><spring:message code="pPRIVACY5"/><br/>
				<ul>
						<li><spring:message code="pPRIVACYL1"/></li>
						<li><spring:message code="pPRIVACYL2"/></li>
						<li><spring:message code="pPRIVACYL3"/></li>
						<li><spring:message code="pPRIVACYL4"/></li>
						<li><spring:message code="pPRIVACYL5"/></li>
					</ul>
					</div>
				
			</p>
			<p>
				<b><spring:message code="p8USEOFDATA"/></b><spring:message code="p8USEOFDATA1"/>    
				<br/><br/><span  class="spnIndent"></span><spring:message code="p8USEOFDATA2"/>  
				<br/><br/><span  class="spnIndent"></span><spring:message code="p8USEOFDATA3"/>  
				<br/><br/><span  class="spnIndent"><spring:message code="p8USEOFDATA4"/>  
				<br/><br/><span  class="spnIndent"><spring:message code="p8USEOFDATA5"/>  
			</p>
			<p>
				<b><spring:message code="head9EXCLUSIONOFWARRANTY"/></b> <spring:message code="pEXCLUSIONOFWARRANTY"/>
			</p>
			<p>
				<b><spring:message code="head10LIMITATIONOFLIABILITY"/>.</b>  <spring:message code="p10LIMITATIONOFLIABILITY"/> 

				<span  class="spnIndent"></span><br/><br/><spring:message code="p10LIMITATIONOFLIABILITY2"/>
				
				<span  class="spnIndent"></span><br/><br/><spring:message code="p10LIMITATIONOFLIABILITY3"/>
			</p>
			<p>
				<b><spring:message code="head11INDEMNIFICATION"/></b> <spring:message code="p11INDEMNIFICATION"/>
			</p>
			<p>
				<b><spring:message code="head12RELATIONSHIP"/> </b><spring:message code="p12RELATIONSHIP"/>
			</p>
			<p>
				<b><spring:message code="head13GOVERNINGLAW"/></b> <spring:message code="p13GOVERNINGLAW"/>
			</p>
			<p>
				<b><spring:message code="head14ASSIGNMENT"/></b>  <spring:message code="p14ASSIGNMENT"/>
			</p>
			<p>
				<b><spring:message code="head15MODIFICATION"/></b> <spring:message code="p15MODIFICATION"/>  
			</p>
			<p>
				<b><spring:message code="head16SEVERABILITY"/></b> <spring:message code="p16SEVERABILITY"/>
			</p>
			<p>
				<b><spring:message code="head17HEADINGS"/></b>  <spring:message code="p17HEADINGS"/>
			</p>
			<p>
				<b><spring:message code="head18ENTIREAGREEMENT"/></b> <spring:message code="p18ENTIREAGREEMENT"/>
			</p>	
			<p  style="text-indent: 0px;">
				<b><spring:message code="headHOWTOCONTACTUS"/></b><spring:message code="pComments"/>
			</p>
			
				<div style="padding-left:40%">				
				<spring:message code="pTmLLC"/><br/>
				<spring:message code="p4611NRaveU"/><br/>
				<spring:message code="pChiIL60640"/><br/>
				<spring:message code="pAttentionTe"/><br/><br/>
				</div>
				
			
			<spring:message code="pLaRevJune"/>
			<hr/>
			<p style="text-indent: 0px;" >
				<spring:message code="pUnderstandTheForegoingTerms"/>    
			</p>
			<hr/>
			<p style="text-indent: 0px;" >
				<spring:message code="pTmPotentialEmp"/>
			</p>  
		</div>
	
	</div>	
</div>

<p id="group04Section25"></p>

<!-- start ... Attach jaf jsp files -->
	<jsp:include page="jafinnercommon.jsp"></jsp:include>
<!-- end ... Attach jaf jsp files -->

<script type="text/javascript">//<![CDATA[
 var cal = Calendar.setup({
  onSelect: function(cal) { cal.hide() },
  showTime: true
 });
 //]]>
    
</script>

<script>
var dspqPortfolioNameId=${dspqPortfolioName.dspqPortfolioNameId};
var candidateType="${candidateType}";
var groupId=${group};
var iJobId=${iJobId};

try { getPortfolioHeader(iJobId,dspqPortfolioNameId,candidateType,groupId); } catch (e) {}
//try { getGroup04Section25Name(iJobId,dspqPortfolioNameId,candidateType,groupId) } catch (e) {}
try { getGroup04Section25(iJobId,dspqPortfolioNameId,candidateType,groupId) } catch (e) {}

</script>  