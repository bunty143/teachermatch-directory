   <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
   <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
   <script src="js/jquery.js"></script>
   <script src="js/jquery-ui.js"></script>
   <link rel="stylesheet" href="css/jquery.ui.labeledslider.css">
   <script src="js/jquery.ui.labeledslider.js"></script>
   <script src="js/jquery.ui.touch-punch.js"></script>
   <style>
	  *{
	      font-size:     8pt;
	      color:#007AB4;
	   }
       .ui-spinner { width: 4em;  }
   </style>
   <script>
  $(document).ready(function(){
       $('#frmslider').labeledslider({ 
       	  value:${svalue},
		  max: ${max},
		  tickInterval:${tickInterval},
		  range:"min",  
		  step:${step},
		  slide: function( event, ui ) { 
		  document.getElementById('${hiddenval}').value=ui.value;
		  }
	  });
	  if(${dslider}==0){
	  	$('#frmslider').labeledslider( 'disable');
	  }
   });
   </script>   
<table border=0 cellpadding="0" cellspacing="0"><tr>
<td>
<div id="frmslider" style='width:${swidth};'></div>
</td>
<td  style='text-align: right;padding-left:12px;height:54px; ' valign="top">
<input style='width:25px;border: 0px none;color:#007AB4;' readonly="readonly" type='text' name='${hiddenval}' id='${hiddenval}' value='${svalue}'>
<input readonly="readonly" type='hidden' name='answerId' id='answerId' value='${answerId}'>
<input readonly="readonly" type='hidden' name='answerId_msu' id='answerId_msu' value='${answerId_msu}'>
</td>
</tr>
</table>

