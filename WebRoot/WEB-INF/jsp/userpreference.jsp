<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resourceMap['AssessmentCampaignAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src='js/assessment-campaign.js?ver=${resourceMap["js/assessment-campaign.js"]}'></script>
<script type='text/javascript' src='js/teacher/dashboard.js?ver=${resourceMap["js/teacher/dashboard.js"]}'></script>
<script type="text/javascript" src="dwr/interface/DistrictPortfolioConfigAjax.js?ver=${resourceMap['DistrictPortfolioConfigAjax.ajax']}"></script>
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/my-preference.png"" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lblSetPerfer"/></div>	
         </div>		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<input type="hidden" name="isKelly" id="isKelly" value="${isKelly}"/>
 
<form action="savepreference.do" method="post" id="userpreferences" onsubmit="return chkPref()">
<div class="row">		
		<div class="col-sm-12 col-md-12">	
			<c:set var="serverMsg" value=""></c:set>
			<c:if test="${param.msg eq '#6@5a1p24m1gg'}">
				<c:set var="serverMsg" value="Please set your preferences for job"></c:set>		
			</c:if>		
			
			<div class='divErrorMsg' id='divSelectPref' style="display: block;">${serverMsg}</div>	
		
			<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
			
		</div>
		<div class="col-sm-12 col-md-12"><br>
			<p><spring:message code="msgPlzSltYurPerferforallTpeOfPlaces"/>
			<br/><spring:message code="msgBasedOnPerferSltdByYu"/>
			</p>
		</div>
</div>
<div class="row mt10 ${hide1}">			
			<div class="col-sm-3 col-md-3">
				<div>
					<strong><spring:message code="lblGeography"/>&nbsp;<a href="#" id="iconpophover1" rel="tooltip" data-original-title='<spring:message code="toolGeographiccategories"/>'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong>
				</div>
				<div style="padding: 0px; margin: 0px; height: 10px;">&nbsp;</div>
				<c:forEach var="geography" items="${lstGeographyMasters}" varStatus="status">
					<div class="control-group ">
						<div class="controls">
							<label class="checkbox">							
								<input type="checkbox" 
									name="geo" 
									<c:if test="${fn:contains(teacherPreference.geoId, geography.geoId)}">
										checked
									</c:if>
									value="${geography.geoId}" > 
								${geography.geoName}
							</label>
						</div>
					</div>
				</c:forEach>				
			</div>

			<div class="col-sm-3 col-md-3">
				<div>
					<strong><spring:message code="lblTpe"/> <a href="#" id="iconpophover2" rel="tooltip" data-original-title='<spring:message code="toolTypesofschools1"/>'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong>
				</div>
				
				<c:forEach var="schoolType" items="${lstSchoolTypeMasters}" varStatus="status">
					<div class="control-group mt10">
						<div class="controls">
							<label class="checkbox">
								<input type="checkbox" 
									name="schoolType" 
									<c:if test="${fn:contains(teacherPreference.schoolTypeId, schoolType.schoolTypeId)}">
											checked
									</c:if>
									value="${schoolType.schoolTypeId}">
								${schoolType.schoolTypeName}
							</label>
						</div>
					</div>
				</c:forEach>	
			</div>
			
			<div class="col-sm-6 col-md-6">
				<div>
					<strong><spring:message code="lblRgn"/> <a href="#" id="iconpophover3" rel="tooltip" data-original-title='<spring:message code="toolGeographicregionsschools"/>'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong>
				</div>
				
				<c:forEach var="pregion" items="${lstPairentRegion}" varStatus="status">
					<div class="control-group mt10">
						<div class="controls">
							<label class="checkbox">
								<input type="checkbox" 
								id="p${pregion.regionId}rgn"
								onclick="checkSubRegionsOnOff(this)"								
								value="${pregion.regionId}">
								${pregion.regionName}								
							</label>
							<c:forEach var="region" items="${lstRegionMasters}" varStatus="status">
								<c:if test="${region.parentRegionId eq pregion.regionId}">
									<div class="left25">
										<label class="checkbox">
											<c:set var="subrgn" value="|${teacherPreference.regionId}|"/>
											<c:set var="chkrgn" value="|${region.regionId}|"/>
											
											<input type="checkbox" 
											id="p${pregion.regionId}rgn${region.regionId}"
											name="regions"
											onclick="checkTopRegionsOnOff('p${pregion.regionId}rgn')";
											
											<c:if test='${fn:contains(subrgn, chkrgn)}'>
												checked
											</c:if> 
											value="${region.regionId}">
											
											${region.regionName}
										</label>
										
									</div>
								</c:if>
							</c:forEach>							
							<div class="clearfix"></div>
							</div>
					</div>
				</c:forEach>
			</div>           
		</div>
<%-- HeadQuarter Preferences--%>

<div class="row mt10 ${hide2}">	

		<div class="col-sm-3 col-md-3">
				<div>
					<strong>Job Category</strong>
				</div>
				<div style="padding: 0px; margin: 0px; height: 10px;">&nbsp;</div>
				<div class="control-group ">
				<c:forEach var="category" items="${lstJobCategoryMasters}" varStatus="status">
					
						<div class="controls">
							<label class="checkbox">							
								<input type="checkbox" 
									name="jobCategory" 
									<c:if test="${not empty teacherPreference.jobCategoryId && fn:contains(teacherPreference.jobCategoryId,category.jobCategoryId)}">
										checked
									</c:if>
									value="${category.jobCategoryId}" > 
								${category.jobCategoryName}
							</label>
						</div>
					
				</c:forEach>	
				</div>			
			</div>
		
			<div class="col-sm-9 col-md-9">
				<div>
					<strong>States</strong>
				</div>
				<div style="padding: 0px; margin: 0px; height: 10px;">&nbsp;</div>
				<c:forEach var="state" items="${lstStateMasters}" varStatus="status">
					<div class="col-sm-3 col-md-3" style="padding-left: 0px;">
					<c:set var="stateCheck" value="|${state.stateId}|"/>
							<label class="checkbox">							
								<input type="checkbox" 
									name="state" 
									<c:if test="${not empty teacherPreference.stateId && fn:contains(teacherPreference.stateId,stateCheck)}">
										checked
									</c:if>
									value="${state.stateId}" > 
								${state.stateName}
							</label>
						</div>
				</c:forEach>				
			</div>
</div>
<%--End Preference UI--%>		
		
		<div class="row top15">
			<div class="col-sm-4 col-md-4">
			<c:choose>
			<c:when test="${kellyUrl && not empty jobId}">
				<button class="btn btn-large btn-primary" type="button" onclick="openCvrForKelly();">
					<strong><spring:message code="lblSvPerfer"/><i class="icon"></i></strong>
				</button>
			</c:when>
			<c:otherwise>
			<button class="btn btn-large btn-primary" type="submit">
					<strong><spring:message code="lblSvPerfer"/><i class="icon"></i></strong>
				</button>
			</c:otherwise>
			</c:choose>
				<br><br>
			</div>
		</div> 	

<c:set var="isaffilatedstatus" value="0"></c:set>
<c:if test="${param.isaffilatedstatus eq ''}">
	<c:set var="isaffilatedstatus" value="0"></c:set>
</c:if>
<c:if test="${param.isaffilatedstatus eq 1}">
	<c:set var="isaffilatedstatus" value="1"></c:set>
</c:if>

<c:set var="ok_cancelflag" value="0"></c:set>
<c:if test="${param.ok_cancelflag eq ''}">
	<c:set var="ok_cancelflag" value="0"></c:set>
</c:if>
<c:if test="${param.ok_cancelflag eq 1}">
	<c:set var="ok_cancelflag" value="1"></c:set>
</c:if>

<input type="hidden" id="isaffilatedstatus" name="isaffilatedstatus" value="${isaffilatedstatus}"/>
<input type="hidden" id="jobId" name="jobId" value="${param.jobId}"/>
<input type="hidden" id="ok_cancelflag" name="ok_cancelflag" value="${ok_cancelflag}">
<input type="hidden" id="epimsjstatus" name="epimsjstatus" value="${param.epimsjstatus }"> 
<input type="hidden" id="savejobFlag" name="savejobFlag" value="${param.savejobFlag}"/>
</form>
<script type="text/javascript" src="js/teacher/user.js">
</script>
<script type="text/javascript">
setTopPref();
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
$('#iconpophover3').tooltip();
$('#iconpophover4').tooltip();
$('#epiIncompAlert').modal('show');


function openCvrForKelly(){
	
	$("#myModalCL").modal("show");
	$("#forKellyCvrLtr").hide();
	if($("#myModalCL #myModalLabel").html()=="Cover Letter"){
		$("#myModalCL #myModalLabel").html("Kelly Educational Staffing");
	}
	$("#kellYcvRLtrMain").show();
	$("#kellYnxtInst").hide();
	$("#kellYnxtDisDivInf").hide();
	$("#forAllDistCvrLtr").show();
	$("#workForKelly2").prop("checked",false);
	$("#workForKelly1").prop("checked",false);
	$("#contactedKelly1").prop("checked",false);
	$("#contactedKelly2").prop("checked",false);
	$(".forAllDistCvrLtr").hide();
	$(".forKellyCvrLtr").show();										
	$(".continueBtnNxt").hide();											
	$("#dspqQuestionHeading").html("Application Questions");
	
	return false;
}

$(function(){
	$("#workForKelly1").click(function() {  
		$("#kellYnxtQ").show();
	});

	$("#workForKelly2").click(function() {  
			$("#kellYnxtQ").hide();
			$("#contactedKelly1").prop("checked",false);
			$("#contactedKelly2").prop("checked",false);
			//continueBtnNxt
			var isDspqReqForKelly=document.getElementById("isDspqReqForKelly").value;
			if(isDspqReqForKelly=="true")
			{
				if (typeof checkCL_dynamicPortfolio != 'undefined' && $.isFunction(checkCL_dynamicPortfolio)) {
					checkCL_dynamicPortfolio();
				} else {
					$('#userpreferences').submit();
				}
			}
			else
			{
				var isExternal=document.getElementById("isExternal").value;
				if(isExternal=="1")
					$('#userpreferences').submit();	
				else
					getDistrictSpecificQuestion();
			}
			
			
	});

	$("#contactedKelly1").click(function() {  
		$("#kellYcvRLtrMain").hide();
		$("#kellYnxtQ").hide();
		$("#kellYnxtInst").show();
		$(".continueBtnNxt").show();
	});

	$("#contactedKelly2").click(function() {  
		//$(".cvrLtrClose").html("Ok");
		var jobId=document.getElementById("jobId").value;
			DistrictPortfolioConfigAjax.getDetailOnJObLevel(jobId,{ 
				async: true,
				errorHandler:handleError,
				callback: function(data)
				{
					$("#kellYcvRLtrMain").hide();
					$("#kellYnxtQ").hide();
					$("#kellYnxtDisDivData").html(data);
					$("#kellYnxtDisDivInf").show();
				}});
	});
	
	$(".cvrLtrClose").click(function() {  		
		$("#forKellyCvrLtr").hide();
		$("#kellYcvRLtrMain").show();
		$("#kellYnxtInst").hide();
		$("#kellYnxtDisDivInf").hide();
		$("#forAllDistCvrLtr").show();
		$("#workForKelly2").prop("checked",false);
		$("#workForKelly1").prop("checked",false);
		$("#contactedKelly1").prop("checked",false);
		$("#contactedKelly2").prop("checked",false);
		if($(".cvrLtrClose").html()=="Ok"){
			$('#userpreferences').submit();
		}
	});
})

function confirmContinue(){
	$('#userpreferences').submit();
}
</script>

<input type="hidden" name="kellyUrl"  id="kellyUrl" value="${kellyUrl}" />

							<div  class="modal hide"  id="myModalCL" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
								<div class="modal-dialog-for-cgmessage"> 
								<div class="modal-content"> 
								<div class="modal-header">
							  		<button type="button" class="close cvrLtrClose" data-dismiss="modal" aria-hidden="true">x</button>
									<h3 id="myModalLabel"><spring:message code="DSPQlblCoverLetter" /></h3>
								</div>
								<div class="modal-body hide forKellyCvrLtr" style="max-height: 450px;overflow-y:auto; overflow-x: hidden;">
								<div id="kellYcvRLtrMain">
										<div class="row">
											<div class="col-sm-12 col-md-12"><b>Outside of the TeacherMatch system</b>, have you ever applied to or worked for Kelly Services(KES&reg;)?
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 col-md-12">
												<label class="checkbox-inline">
												  <input type="radio" id="workForKelly1" name="workForKelly"  value="option1"> <spring:message code="lblYes" />
												</label>
												<label class="checkbox-inline">
												  <input type="radio" id="workForKelly2" name="workForKelly" value="option2"><spring:message code="lblNo" />
												</label>
											</div>
										</div>
									</div>
									<!-- next question -->
									<div class="hide" id="kellYnxtQ">
										<div class="row">
											<div class="col-sm-12 col-md-12"><spring:message code="lblThisJobnotavailablemoment2" />
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 col-md-12">
												<label class="checkbox-inline">
												  <input type="radio" id="contactedKelly1" name="contactedKelly"  value="option1"> <spring:message code="lblYes" />
												</label>
												<label class="checkbox-inline">
												  <input type="radio" id="contactedKelly2" name="contactedKelly" value="option2"><spring:message code="lblNo" />
												</label>
											</div>
										</div>
									</div>
									<!-- end next question -->
									<div class="hide" id="kellYnxtInst">
										<div class="row">
											<div class="col-sm-12 col-md-12"><spring:message code="lblThisJobnotavailablemoment3" />
											</div>
										</div>
									</div>
									<div class="hide" id="kellYnxtDisDivInf">
										<div class="row">
											<div class="col-sm-12 col-md-12" id="kellYnxtDisDivData">
											</div>
										</div>
									</div>
								</div>
								<div class="modal-body forAllDistCvrLtr" style="max-height: 450px;overflow-y:auto; overflow-x: hidden;">
								<p id="cvrltrTxt" class="hide"><spring:message code="msgIfYuApplyCentealOfficePosition" /></p>
									<div class='divErrorMsg' id='errordivCL' style="display: block;"></div>
									<div class="top10"> 
									<label class="radio" id="noCoverLetter">
										<input type="radio" value="1" id="rdoCL2" name="reqType"
											onclick='setClBlank()'>
										<spring:message code="msgIdoNotWantToAddCorLetter" />
									</label>
								</div>
								<label class="radio">
									<input type="radio" value="1" id="rdoCL1" name="reqType"
										onclick="setCLEnable()">
									<spring:message code="msgPlzTyYurCoverLtr" />
								</label>
								<div id="divCoverLetter">
									<label class="redtextmsg">
										<spring:message code="msgHowToCopyPastCutDouc" />
									</label>
									<textarea id="coverLetter" name="coverLetter" rows="7"
										cols="150"></textarea>
								</div>

								<%--<c:out value="${fn:length(latestCoverletter)}"></c:out>--%>
								<c:if test="${fn:length(latestCoverletter)gt 0}">
									<div class="top10">
										<label class="radio">
											<input type="radio" value="3" id="rdoCL3" name="reqType"
												onclick='setLatestCoverLetter()'>
											<spring:message code="msgApplyJob6" />
										</label>
									</div>
								</c:if>
								<div class="top10">
									<label class="checkbox">
										<input type="checkbox" value="1" id="isAffilated"
											name="isAffilated" ${(internalFlag==
											true) ? 'checked' : ''}    ${(internalFlag==
											true) ? 'disabled' : ''}  onclick="validateCandidateType();">
										<spring:message code="msgApplyJob7" />
										<br />
										<!--<span class="redtextmsg">(If you are currently a substitute Teacher, please don't check this box.)</span>&nbsp; <a href="#" id="iconpophover101" rel="tooltip" data-original-title="Please select this box if you are currently working with this District and/or any School within this District as a full time or part time employee or consultant/contractor"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> -->
									</label>
								</div>
								<div class="row left25 hide empCvrrLtrDiv">
									<div class="col-sm-5 col-md-5">
									<label><strong><spring:message code="lblEmpNum" /><span class="required">*</span></strong></label>
									<input type="text" autocomplete="off" id="empNumCoverLtr" name="empNumCoverLtr" class="form-control" value="${teacherpersonalinfo.employeeNumber}" maxlength="50"/>
									</div>
									<div class="col-sm-5 col-md-5">
									<label><strong><spring:message code="lblZepCode" /><span class="required">*</span></strong></label>
									<input type="text" id="zipCodeCoverLtr" autocomplete="off" name="zipCodeCoverLtr" class="form-control" value="${teacherpersonalinfo.zipCode}" maxlength="10"/>
									</div>
									</div>
								<div style="padding-left: 15px;">
									<label class="radio" id="inst" style="display: none;">
										<input type="radio" value="I" name="staffType">
										<spring:message code="msgApplyJob8" />
									</label>
									<label class="radio" id="partinst" style="display: none;">
										<input type="radio" value="PI" name="staffType">
										<spring:message code="msgApplyJob9" />
									</label>
									<label class="radio" id="noninst" style="display: none;">
										<input type="radio" value="N" name="staffType">
										<spring:message code="msgApplyJob10" />
									</label>
								</div>
							</div>
							<div class="modal-footer">
								<button class="btn btn-large btn-primary continueBtnNxt" id="continueBtn" type="button"
									onclick="confirmContinue();">
									<strong><spring:message code="btnConti" /> <i
										class="icon"></i>
									</strong>
								</button>
								<button class="btn" data-dismiss="modal" aria-hidden="true">
									<spring:message code="btnClose" />
								</button>
							</div>
						</div>
					</div>
				</div>

<input type="hidden" id="isDspqReqForKelly" value="${isDspqReqForKelly}"/>
<input type="hidden" id="isKelly" value="${isKelly}"/>
<input type="hidden" id="isExternal" value="1"/>